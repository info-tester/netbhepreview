
    <div class="dshbrd-lftmnu">
            <div class="prflinfo-sec">
               <!-- <span><img src="{{ URL::to('public/frontend/images/id.png') }}"> ID : UP000011</span> -->
            <div class="prflpic"><img src="{{ Auth::guard('web')->user()->profile_pic ? URL::to('storage/app/public/uploads/profile_pic').'/'.@Auth::guard('web')->user()->profile_pic: URL::to('public/frontend/images/no_img.png')}}"></div>
            {{__('site.wallet_balance')}} - {{Auth::guard('web')->user()->wallet_balance}}
            </div>

            <div class="dashmnu">
                <span class="one-ul">
                    @if(Auth::guard('web')->user()->menu_type == "U")
                    <a href="javascript:void(0);" style="cursor: default" id="us" @if(Request::segment(1)=="my-dashboard" || Request::segment(1)=="edit-my-profile"
                        || Request::segment(1)=="my-orders" || Request::segment(1)=="order-details" || Request::segment(1)=="my-purchases"
                        || Request::segment(1)=="view-cerificate" || Request::segment(1)=="my-affiliation-links" || Request::segment(1)=="my-affiliation-earnings")
                        class="prof_dash prof_dash_background" @else class="prof_dash" @endif>
                        @lang('site.user')
                    </a>
                    @else

                    <a href="javascript:void(0);" style="cursor: default" id="prof" @if(Request::segment(1)=="dashboard" || Request::segment(1)=="edit-profile"
                        || Request::segment(1)=="qualification"|| Request::segment(1)=="availability"|| Request::segment(1)=="my-experience"
                        || Request::segment(1)=="bookings"|| Request::segment(1)=="my-blog"|| Request::segment(1)=="edit-blog-post"
                        || Request::segment(1)=="view-blog-post"|| Request::segment(1)=="add-blog-post" || Request::segment(1)=="dashboard-bookings"
                        || Request::segment(1)=="payments"|| Request::segment(1)=="professional-form" || Request::segment(1)=="professional-view-form"
                        || Request::segment(1)=="professional-form-question" || Request::segment(1)=="professional-importing-tools-view"
                        || Request::segment(1)=="user-view-form-submit-details" || Request::segment(1)=="user-evaluation360-assign-userlist"
                        || Request::segment(1)=="products" || Request::segment(1)=="store-product" || Request::segment(1)=="my-sales" || Request::segment(1)=="preview-template"
                        || Request::segment(1)=="view-order" || Request::segment(1)=="product-file" || Request::segment(1)=="product-file-upload"
                        || Request::segment(1)=="introductory-video" || Request::segment(1)=="my-coupons" || Request::segment(1)=="add-coupon"
                        || Request::segment(1)=="upload-signature" || Request::segment(1)=="professional-certificate-template"
                        || Request::segment(1)=="professional-certificate-template-view" || Request::segment(1)=="professional-certificate-template-edit"
                        || Request::segment(1)=="assign-product-certificate-template")
                        class="prof_dash prof_dash_background" @else class="prof_dash" @endif>
                        @lang('site.professional')
                    </a>
                    @endif
                </span>
                @if(Auth::guard('web')->user()->menu_type == "U")
                <ul class="user-ul" >
                    <li><a href="{{ route('load_user_dashboard') }}" @if(Request::segment(1)=="my-dashboard")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/dsh1.jpg') }}"> @lang('site.dashboard')</a></li>
                    <li><a href="{{ route('user.profile') }}" @if(Request::segment(1)=="edit-my-profile")style="color: #007bff;"@endif ><img src="{{ URL::to('public/frontend/images/dsh2.jpg') }}"> @lang('site.edit_profile')</a></li>

                    <li><a href="{{ route('favourite.list') }}" @if(Request::segment(1)=="favourite-expert")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/favourite-expert.png') }}">@lang('site.favourite_expert')</a></li>

                    <li><a href="{{ route('user.my.booking', ['type' => 'UC']) }}" @if(Request::segment(1)=="bookings" && Request::segment(2)!="PC")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/dsh6.jpg') }}"> @lang('site.upcoming_classes')</a></li>
                    <li><a href="{{ route('prof.my.booking', ['type' => 'PC']) }}" @if(Request::segment(2)=="PC")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/dsh7.jpg') }}"> @lang('site.past_classes')</a></li>

                    <li><a href="{{route('refer.page')}}" class="copy_refer_link"><img src="{{ URL::to('public/frontend/images/invite-freinds-icon.png') }}">@lang('site.refer')</a></li>

                    <li><a href="{{route('user.wishlist')}}" @if(Request::segment(1)=="my-wishlist")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/heart.png') }}">@lang('site.my_wishlist')</a></li>
                    <li><a href="{{ route('user.products') }}" @if( Request::segment(1)=="my-purchases" || Request::segment(1)=="view-cerificate" || Request::segment(1)=="order-details") style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/dsh11.png') }}"> @lang('site.my_purchases')</a></li>

                    @if(Auth::user()->is_professional!="Y")
                        <li><a href="{{ route('be.professional') }}" @if(Request::segment(1)=="became-professional")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/become-professional.png') }}"> @lang('site.become_professional')</a></li>
                    @endif

                    <li><a href="{{ route('message') }}"><img src="{{ URL::to('public/frontend/images/msg.png') }}"> @lang('site.my_messages')</a></li>
                    <li><a href="{{ route('chats') }}"><img src="{{ URL::to('public/frontend/images/chats.png') }}"> @lang('site.chats')</a></li>

                    {{-- <li><a href="#"><img src="{{ URL::to('public/frontend/images/dsh8.jpg') }}"> @lang('site.my_experts')</a></li>
                    <li><a href="#"><img src="{{ URL::to('public/frontend/images/dsh9.jpg') }}"> @lang('site.payments')</a></li>
                    <li><a href="#"><img src="{{ URL::to('public/frontend/images/dsh10.jpg') }}"> Withdrawal Request</a></li>
                    <li><a href="#"><img src="{{ URL::to('public/frontend/images/dsh10.jpg') }}"> @lang('site.withdrawal_request')</a></li> --}}
                    <li><a href="{{ route('user.view.form') }}"><img src="{{ URL::to('public/frontend/images/tools-icon.png') }}"> @lang('site.coaching_tools')</a></li>
                    <li><a href="{{ route('user-content-temp.index') }}"><img src="{{ URL::to('public/frontend/images/content-template-icon.png') }}">Content templates</a></li>
                    {{-- @if(Auth::user()->is_video_affiliate=="Y")
                        <li><a href="{{ route('video.affiliate.earning.list') }}" @if(Request::segment(1)=="video-affiliate-earning" ) style="color:#007bff;"@endif ><img src="{{ URL::to('public/frontend/images/affiliate.png') }}">Video Affiliate Earning</a></li>
                    @endif --}}
                </ul>
                @else
                @php
                    $allowedMenus=allowed_menus();
                @endphp
                <ul class="Professional-ul">

                    <li><a href="{{ route('prof.dash.my.booking',['type'=>'UC']) }}" @if(Request::segment(1)=="dashboard" || Request::segment(1)=="dashboard-bookings")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/dsh1.jpg') }}"> @lang('site.dashboard')</a></li>
                    <li><a href="{{ route('professional.profile') }}" @if(Request::segment(1)=="edit-profile")style="color: #007bff;"@endif ><img src="{{ URL::to('public/frontend/images/dsh2.jpg') }}"> @lang('site.edit_profile')</a></li>
                    <li><a href="{{ route('professional.verify.identity') }}" @if(Request::segment(1)=="verify-identity")style="color: #007bff;"@endif ><img src="{{ URL::to('public/frontend/images/dsh2.jpg') }}"> @lang('client_site.verify_identity')</a></li>
                    @if($allowedMenus->has_category == "Y")
                        <li><a href="{{ route('professional.change.category') }}" @if(Request::segment(1)=="profile-category")style="color: #007bff;"@endif ><img src="{{ URL::to('public/frontend/images/change-category-icon.png') }}"> @lang('site.change_category')</a></li>
                    @endif
                    <li><a href="{{ route('professional_qualification') }}" @if(Request::segment(1)=="qualification")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/proffessional-information-icon.png') }}"> @lang('site.personal_info_and_Education')</a></li>
                    <li><a href="{{ route('professional.exp') }}" @if(Request::segment(1)=="my-experience") style="color:#007bff !important;"@endif><img src="{{ URL::to('public/frontend/images/dsh4.jpg') }}" > @lang('site.experience')</a></li>
                    <li><a href="{{route('professional.profile.introductory.video')}}" class="copy_refer_link" @if(Request::segment(1)=="introductory-video")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/vdo.png') }}">@lang('site.introductory_video')</a></li>

                    @if($allowedMenus->has_availability == "Y")
                        <li><a href="{{ route('user_availability') }}"  @if(Request::segment(1)=="availability")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/dsh3.jpg') }}"> @lang('site.availability')</a></li>
                    @endif
                    @if($allowedMenus->show_free_session == "Y")
                        <li><a href="{{route('invite.page')}}" class="copy_refer_link"><img src="{{ URL::to('public/frontend/images/free-session-icon.png') }}">{{__('site.free_session')}}</a></li>
                    @endif
                    <li><a href="{{route('refer.page')}}" class="copy_refer_link"><img src="{{ URL::to('public/frontend/images/invite-freinds-icon.png') }}">@lang('site.refer')</a></li>

                    @if($allowedMenus->my_bookings == "Y")
                        <li><a href="{{ route('prof.my.booking', ['type' => 'UC']) }}" @if(Request::segment(1)=="bookings" && Request::segment(2)!="PC")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/dsh6.jpg') }}"> @lang('site.upcoming_classes')</a></li>
                    @endif

                    @if($allowedMenus->past_bookings == "Y")
                        <li><a href="{{ route('prof.my.booking', ['type' => 'PC']) }}" @if(Request::segment(2)=="PC")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/dsh7.jpg') }}"> @lang('site.past_classes')</a></li>
                    @endif

                    @if($allowedMenus->has_products == "Y")
                        <li><a href="{{ route('professional.products') }}" @if(Request::segment(1)=="products" || Request::segment(1)=="store-product" || Request::segment(1)=="assign-product-certificate-template" || (Request::segment(1)=="professional-certificate-template-view" && Request::segment(3) != "")) style="color: #007bff;"@endif ><img src="{{ URL::to('public/frontend/images/dsh11.png') }}"> @lang('site.my_products')</a></li>
                    @endif
                    @if($allowedMenus->has_certificates == "Y")
                        <li><a href="{{ route('index.certificate.template') }}"@if(Request::segment(1)=="professional-certificate-template" || (Request::segment(1)=="professional-certificate-template-view" && Request::segment(3) == "") || Request::segment(1)=="professional-certificate-template-edit") style="color: #007bff;"@endif ><img src="{{ URL::to('public/frontend/images/content-template-icon.png') }}">@lang('site.certificate_template')</a></li>
                    @endif
                    @if($allowedMenus->has_certificates == "Y")
                        <li><a href="{{ route('professional.coupons') }}" @if(Request::segment(1)=="my-coupons" || Request::segment(1)=="add-coupon") style="color: #007bff;"@endif ><img src="{{ URL::to('public/frontend/images/coupon-icon.png') }}"> @lang('site.my_coupons')</a></li>
                    @endif
                    @if($allowedMenus->has_product_order == "Y")
                        <li><a href="{{ route('professional.orders') }}" @if(Request::segment(1)=="my-sales"  || Request::segment(1)=="preview-template" || Request::segment(1)=="view-order" || Request::segment(1)=="preview-template")style="color: #007bff;"@endif ><img src="{{ URL::to('public/frontend/images/sale-cion.png') }}"> @lang('site.my_sales')</a></li>
                    @endif

                    <li><a href="{{ route('list.landing.page.templates') }}" @if(Request::segment(1)=="landing-page")style="color: #007bff;"@endif ><img src="{{ URL::to('public/frontend/images/landingicon.png') }}"> @lang('site.landing_page')</a></li>
                    <li><a href="{{ route('my.blog') }}" @if(Request::segment(1)=="my-blog"|| Request::segment(1)=="edit-blog-post"|| Request::segment(1)=="view-blog-post"|| Request::segment(1)=="add-blog-post")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/blg.png') }}"> @lang('site.my_blogs')</a></li>

                    @if($allowedMenus->messages == "Y")
                        <li><a href="{{ route('message') }}"><img src="{{ URL::to('public/frontend/images/msg.png') }}"> @lang('site.my_messages')</a></li>
                    @endif
                    @if($allowedMenus->chats == "Y")
                        <li><a href="{{ route('chats') }}"><img src="{{ URL::to('public/frontend/images/chats.png') }}"> @lang('site.chats')</a></li>
                    @endif

                    {{-- <li><a href="#"><img src="{{ URL::to('public/frontend/images/dsh8.jpg') }}"> @lang('site.my_users')</a></li> --}}
                    <li><a href="{{route('my.payments')}}" @if(Request::segment(1)=="payments")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/dsh9.jpg') }}"> @lang('site.payments')</a></li>
                    {{-- <li><a href="#"><img src="{{ URL::to('public/frontend/images/dsh10.jpg') }}"> Withdrawal Request</a></li> --}}

                    @if(Auth::user()->sell != "PS")
                    <!-- <li><a href="{{ route('professional.view.form') }}"><img src="{{ URL::to('public/frontend/images/dsh10.jpg') }}"> Coaching tools</a></li> -->
                    @endif
                    @if($allowedMenus->coaching_tools == "Y")
                        <li><a href="{{ route('professional-form.index') }}" @if(Request::segment(1)=="professional-form")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/tools-icon.png') }}">@lang('site.coaching_tools')</a></li>
                    @endif
                    @if($allowedMenus->content_temp == "Y")
                        <li><a href="{{ route('user-content-temp.index') }}"><img src="{{ URL::to('public/frontend/images/content-template-icon.png') }}">@lang('site.content_template')</a></li>
                    @endif
                    @if($allowedMenus->document == "Y")
                        <li><a href="{{route('document')}}" class="copy_refer_link" @if(Request::segment(1)=="document")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/digital-signature.png') }}">{{__('site.document_Signature')}}</a></li>
                    @endif

                    <li><a href="{{route('upload.signature')}}" class="copy_refer_link" @if(Request::segment(1)=="upload-signature")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/digital-signature.png') }}">{{__('site.your_Signature')}}</a></li>
                    {{-- @if(Auth::user()->is_video_affiliate=="Y")
                        <li><a href="{{ route('video.affiliate.earning.list') }}" @if(Request::segment(1)=="video-affiliate-earning" ) style="color:#007bff;"@endif ><img src="{{ URL::to('public/frontend/images/affiliate.png') }}">Video Affiliate Earning</a></li>
                    @endif --}}
                </ul>
                @endif
            </div>
        </div>


@if(Request::segment(1)=="dashboard" || Request::segment(1)=="edit-profile" || Request::segment(1)=="qualification"
|| Request::segment(1)=="availability"|| Request::segment(1)=="my-experience" || Request::segment(1)=="bookings"
|| Request::segment(1)=="my-blog"|| Request::segment(1)=="edit-blog-post"|| Request::segment(1)=="view-blog-post"
|| Request::segment(1)=="add-blog-post"|| Request::segment(1)=="dashboard-bookings"|| Request::segment(1)=="payments"
|| Request::segment(1)=="professional-view-form" || Request::segment(1)=="professional-form"
|| Request::segment(1)=="professional-form-question" || Request::segment(1)=="professional-importing-tools"
|| Request::segment(1)=="professional-importing-tools-view" || Request::segment(1)=="user-view-form-submit-details"
|| Request::segment(1)=="professional-360evaluation" || Request::segment(1)=="prof-smartgoal-tools"
|| Request::segment(1)=="prof-360asign-view" || Request::segment(1)=="prof-smartgoal-user-assigne-view"
|| Request::segment(1)=="prof-contract-temp" || Request::segment(1)=="prof-importing-tools-view-details"
|| Request::segment(1)=="user-content-temp" ||Request::segment(1) == "professional-certificate-template"
|| Request::segment(1)=="prof-view-alluser-feedback" || Request::segment(1)=="prof-evaluation360"
|| Request::segment(1)=="prof-log-book" || Request::segment(1)=="prof-logbook-assigne-list"
|| Request::segment(1)=="user-evaluation360-assign-userlist" || Request::segment(1)=="profile-category"
|| Request::segment(1)=="document" || Request::segment(1)=="products" || Request::segment(1)=="store-product"
|| Request::segment(1)=="my-sales" || Request::segment(1)=="preview-template" || Request::segment(1)=="view-order" || Request::segment(1)=="product-file"
|| Request::segment(1)=="product-file-upload" || Request::segment(1)=="introductory-video" || Request::segment(1)=="my-coupons"
|| Request::segment(1)=="add-coupon"|| Request::segment(1)=="profile-category"|| Request::segment(1)=="upload-signature"
|| Request::segment(1)=="professional-certificate-template-view" || Request::segment(1)=="professional-certificate-template-edit"
|| Request::segment(1)=="assign-product-certificate-template")
<!-- <script>
    $('.user-ul').hide();
    $('.Professional-ul').show();
</script> -->

@endif
{{-- <script>
    $('.copy_refer_link').click(function (e) {
        e.preventDefault();
        var copyText = $(this).attr('href');
        document.addEventListener('copy', function(e) {
            e.clipboardData.setData('text/plain', copyText);
            e.preventDefault();
        }, true);
        document.execCommand('copy');
        alert('copied Refer Link: ' + copyText);
    });
</script> --}}
