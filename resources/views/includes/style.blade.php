<link href="{{ URL::to('public/frontend/css/style.css') }}" type="text/css" rel="stylesheet" media="all"/>
    @if(Request::segment(1)!="video-call")
        <link href="{{ URL::to('public/frontend/css/responsive.css') }}" type="text/css" rel="stylesheet" media="all"/>
    @endif

    <!--bootstrape-->
    <link href="{{ URL::to('public/frontend/css/bootstrap.min.css') }}" type="text/css" rel="stylesheet" media="all"/>

    <!--font-awesome-->
    <link href="{{ URL::to('public/frontend/css/font-awesome.min.css') }}" type="text/css" rel="stylesheet" media="all"/>

    <!--fonts-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;700;800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@400;500;600;700&display=swap" rel="stylesheet">

    <!-- Owl Stylesheets -->
    <link rel="stylesheet" href="{{ URL::to('public/frontend/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('public/frontend/css/owl.theme.default.min.css') }}">
    <link href="{{URL::to('public/admin/css/jquery.toast.css')}}" rel="stylesheet" type="text/css">
    <link href="{{URL::to('public/admin/css/chosen.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <link href="https://vjs.zencdn.net/7.10.2/video-js.css" rel="stylesheet" />

<style>
.error{
color: red !important;
}
.prof_dash_background{
	background: black !important;
}
</style>

