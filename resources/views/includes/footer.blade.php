<script>
	// navigator.__defineGetter__('userAgent', function () {
	//     return "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"
	// });
	// navigator.__defineGetter__('appName', function () {
	//     return "Netbhe.com"
	// });
</script>

<button onclick="playAudio()" type="button" id="playAudioBtn" style="display: none">Play Audio</button>
<script type="text/javascript">
	var player;
	$(document).ready(function() {
		player = new Audio("{{ url('public/video_call_ringtone.mp3') }}");
		console.log(player);
	})
	function playAudio() {
		player.play();
	}
	function pauseAudio() {
		player.pause();
	}

	@if(Request::segment(1)=="video-call")
		if(!navigator.getUserMedia || !navigator.webkitGetUserMedia || !navigator.mozGetUserMedia || !navigator.msGetUserMedia){
			navigator.getUserMedia = ( navigator.getUserMedia || // use the proper vendor prefix
	                       navigator.webkitGetUserMedia ||
	                       navigator.mozGetUserMedia ||
	                       navigator.msGetUserMedia);
		}


		if(localStorage.getItem('videoStart') && localStorage.getItem('userChatToken')){
            checkLocalResourceAvailability('audio')
            checkLocalResourceAvailability('video')
		}

		if(localStorage.getItem('userId') && localStorage.getItem('token')){
            checkLocalResourceAvailability('audio')
            checkLocalResourceAvailability('video')
		}
	function checkLocalResourceAvailability(type) {
	    if(typeof navigator.getUserMedia === undefined) {
	        if(type == 'audio') {
	            navigator.mediaDevices.getUserMedia({type: true}).then((a) => console.log(a)).catch(() => {
	                swal("Conecte seu fone de ouvido ou ative seu microfone.",{icon:"info"});
	            });
	        } else {
	            navigator.mediaDevices.getUserMedia({type: true}).then((a) => console.log(a)).catch(() => {
	                swal("Por favor, conecte sua webcam ou ative sua câmera.",{icon:"info"});
	            });
	        }
	    } else {
	        if(type == 'audio') {
	            navigator.getUserMedia({type: true}, function() {}, function() {
	               swal("Conecte seu fone de ouvido ou ative seu microfone.",{icon:"info"});
	           });
	        } else {
	            navigator.getUserMedia({type: true}, function() {}, function() {
	               swal("Por favor, conecte sua webcam ou ative sua câmera.",{icon:"info"});
	           });
	        }
	    }
	}
	@endif
</script>


<script type="text/javascript">
  	$('#news_btn').click(function(){

    if($('#news_email').val()==""){
      toastr.error("Digite seu email");
    }

    var reqData = {
                    'jsonrpc' : '2.0',
                    '_token'  : '{{ @csrf_token() }}',
                    'params'  : {
                        'email'   : $('#news_email').val()
                      }
                  };
    $.ajax({
        url      : '{{ route('news_letter') }}',
        method   : 'post',
        dataType : 'json',
        data     : reqData,
        success  : function(response){
            if(response.success==1){
                toastr.success("Sua conta de e-mail foi assinada com sucesso em nossa newsletter.");
            }
            if(response.error==1){
                toastr.error("Formato de email inválido.");
            }
            if(response.error==0){
                toastr.info("Você já assinou nossa carta de notícias.");
            }
        },
        error:function(error) {

        }
    });
  });

</script>
@if(@Auth::guard('web')->user()->id)
<script>

	$(function(){
		var timer=0, minutes=0, seconds=0;
		var yTime = xTime = 0;
		var chatFlag = 0;
		var timeOutVar;
		var domian = options = api = "";
		domain = 'phpwebdevelopmentservices.com';
		var myTimeInterval ="";
		var session = null,
		imClient = null,
		my_id = {{ @Auth::guard('web')->user()->id }},
		webRTCClient = null,
		dataClient = null;

		session = apiRTC.init({
			apiKey : "47716e958466549153f429fed4b9fd80",
			apiCCId : my_id,
			onReady : sessionReadyHandler,
			senderNickname: '{{  @Auth::guard("web")->user()->nick_name ? @Auth::guard("web")->user()->nick_name : @Auth::guard("web")->user()->name  }}'
		});

		//
		function sessionReadyHandler(e) {
		    apiRTC.addEventListener("receiveIMMessage", receiveIMMessageHandler);
		    apiRTC.addEventListener("receiveData", receiveData);
		    imClient = apiCC.session.createIMClient();
		    dataClient = apiCC.session.createDataClient();
		}


		// for videocall.....
		var userAnswer=0;
		$('.videoCallStart').click(function(){
			swal({
				title: "Video chamada",
				text: "Deseja iniciar uma videochamada ?",
				icon: "info",
				buttons: ['Cancelar', 'OK'],
				dangerMode: true,
			})
			.then((willDelete) => {
				if (willDelete) {
					localStorage.setItem('token',$(this).data('token'));
					localStorage.setItem('userId',$(this).data('id'));
					localStorage.setItem('maxTime', $(this).data('dur')*60);
					localStorage.setItem('userType', 'P');
					userAnswer = checkUserOnline($(this).data('id'), localStorage.getItem('token'));
					$('.parent_loader').show();
					timeOut();
				} else {
				  	return false;
				}
			});
		});

		// validating chat attachments.
	    $("#chat_attachment").change(function () {
	        var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'pdf', 'txt', 'doc', 'docx', 'xls', 'xlsx', 'csv'];
	        if ($.inArray($('#chat_attachment').val().split('.').pop().toLowerCase(), fileExtension) == -1) {
	            swal("Somente este formato de arquivo é permitido : "+fileExtension.join(', '),{icon:'info'});
	            $('#chat_attachment').val("");
	        }
	    });

	   	// For messaging purpose......................
	    var res_id = null;
	    res_id = $('#recipents').val();
	    $('.sndms').click(function(){
	        if($('#recipents').val()==null || $('#recipents').val()==""){
	            $('#nameerr').text('Por favor, selecione seus socorristas');
	            return false;
	        }

	        if($('#msg').val()==""){
	            $('#msgerr').text('Por favor insira a sua mensagem');
	            return false;
	        }

	        $(".parent_loader").show();
	        var reqData = {
	          	'jsonrpc' : '2.0',
	          	'_token' : '{{csrf_token()}}',
	          	'params' : {
	                'res' : $('#recipents').val(),
	                'sub' : $('#subject').val(),
	                'msg' : $('#msg').val()
	            }
	        };
	        var chat_form = new FormData($("#user_chat_form")[0]);
	        var userValId = $('#recipents').val();
	        var obj = {
	            message: "Você tem uma nova mensagem de {{ @Auth::guard('web')->user()->nick_name ? @Auth::guard('web')->user()->nick_name : @Auth::guard('web')->user()->name }}",
	            code: 2008
	        };
	        $.ajax({
	            url: "{{ route('insrt.msg.prf') }}",
	            method: 'post',
	            dataType: 'json',
	          	data: reqData,
	            success: function(response){
	                if(response.status==1){
	                	$('#subject').val("");
						$('#msg').val("");
	                    if(document.getElementById("chat_attachment").files.length>0){
			            	uploadFile(response.id, chat_form, 'M');
			            	toastr.success('Mensagem enviada com sucesso.');
			            }
			            else{
			            	$(".parent_loader").hide();
			            	toastr.success('Mensagem enviada com sucesso.');
	                    	imClient.sendMessage(userValId, obj);
			            }
	                }
	            }, error: function(error) {
	                toastr.info('Tente novamente depois de algum tempo');
	            }
	        });
	    });

	    function uploadFile(id, atch, type){
			atch.append("conv_id", id);
			atch.append("type", type);
			atch.append("_token", '{{csrf_token()}}');

	      	$.ajax({
	         	url: "{{ route('insrt.atch') }}",
	         	method: 'post',
	          	dataType: 'json',
	          	data: atch,
	          	processData: false,
	          	cache: false,
	          	contentType: false,
	          	success: function(response){
	            	if(response.status == 1){
	              		$("#chat_attachment").val("");
	              		$(".parent_loader").hide();
	              		return true;
	            	}
	          	}
	      	});
	    }

	    $('.sndreply').click(function(){
	        if($('#recipents').val()==null || $('#recipents').val()==""){
	            $('#nameerr').text('Por favor, selecione seus socorristas');
	            return false;
	        }

	        if($('#msg').val()==""){
	            $('#msgerr').text('Por favor insira a sua mensagem');
	            return false;
	        }

	        $(".parent_loader").show();
	        var reqData = {
	          'jsonrpc' : '2.0',
	          '_token' : '{{csrf_token()}}',
	          'params' : {
	                'res' : $('#recipents').val(),
	                'msg' : $('#msg').val(),
	                'conv_id' : $('#conv_id').val()
	            }
	        };
	        var chat_form = new FormData($("#user_chat_form")[0]);
	        var userValId = $('#recipents').val();
	        var obj = {
	            message: "Você tem uma nova mensagem de {{ @Auth::guard('web')->user()->nick_name ? @Auth::guard('web')->user()->nick_name : @Auth::guard('web')->user()->name }}",
	            code: 2008
	        };
	        $.ajax({
	            url: "{{ route('insrt.msg.reply') }}",
	            method: 'post',
	            dataType: 'json',
	          	data: reqData,
	            success: function(response){
	                if(response.status==1){
	                    if(document.getElementById("chat_attachment").files.length>0){
			            	uploadFile($('#conv_id').val(), chat_form,'R');
			            	toastr.success('Mensagem enviada com sucesso.');
			            }
			            else{
			            	$(".parent_loader").hide();
			            	toastr.success('Mensagem enviada com sucesso.');
	                    	imClient.sendMessage(userValId, obj);
			            }
	                }
	            }, error: function(error) {
	                toastr.info('Tente novamente depois de algum tempo');
	            }
	        });
	    });

		function checkUserOnline(id, token){
		    var obj = {
				message: "Você tem uma nova solicitação de videochamada de {{ @Auth::guard('web')->user()->nick_name ? @Auth::guard('web')->user()->nick_name : @Auth::guard('web')->user()->name }}",
				chatToken:localStorage.getItem('token'),
				duration:localStorage.getItem('maxTime'),
				code:100
		    };
		    imClient.sendMessage(id, obj);
		}

		function isRequestAccsepted(id, choice){
		    // Y for yes
		    if(choice=='Y'){

		      	var obj = {
			        message: "A solicitação de chamada de vídeo é aceita",
			        code:101
		      	};
		      	imClient.sendMessage(id, obj);
		    }
		    // N for No
		    if(choice=='N'){
		        var obj = {
		          message: "Parece que o usuário está ocupado no momento.",
		          code:102
		        };
		      	imClient.sendMessage(id, obj);
		    }
		}

		function receiveIMMessageHandler(e) {
		    // for video call accseptence...
		    if(e.detail.message.code==100){
		    	// ring remote site
		    	playAudio();
		      	swal({
		            title: "Nova solicitação de chamada de vídeo",
		            text: e.detail.message.message,
		            icon: "info",
		            buttons: true,
		            dangerMode: true,
		        })
		        .then((willDelete) => {
		            if (willDelete) {
						localStorage.setItem('userChatId',e.detail.senderId);
						localStorage.setItem('userChatToken',e.detail.message.chatToken);
						localStorage.setItem('maxTime',e.detail.message.duration);
						localStorage.setItem('videoStart',1);
						localStorage.setItem('startTime',timer);
						localStorage.setItem('userType', 'C');
						isRequestAccsepted(e.detail.senderId, 'Y');
						startvideoCall(e.detail.message.chatToken);
		            } else {
		              	isRequestAccsepted(e.detail.senderId, 'N');
		              	pauseAudio();
		            }
		        });
		    }
		    if(e.detail.message.code==101){
				$('.parent_loader').hide();
				clearTimeout(timeOutVar);
				swal('A solicitação de chamada de vídeo foi aceita',{ icon: "success"});
				localStorage.setItem('startTime',timer);
				localStorage.setItem('userChatId', e.detail.senderId);
				startvideoCall(localStorage.getItem('token'));
		    }
		    if(e.detail.message.code==102){
				$('.parent_loader').hide();
				//swal('It seems user is currently offline.',{ icon: "info"});
				//$('.parent_loader').hide();
		    }

		    if(e.detail.message.code==108){
				localStorage.removeItem('videoStart');
				localStorage.removeItem('userChatToken');
				localStorage.removeItem('userChatId');
				localStorage.removeItem('userId');
				localStorage.removeItem('token');
				swal('A videoconferência terminou.',{ icon: "info"});
		    }
		    if(e.detail.message.code==404){
		      	swal.close();
		    }
		    if(e.detail.message.code==500){
				swal(e.detail.message.message,{ icon: "info"});
				window.localStorage.removeItem('videoStart');
				window.localStorage.removeItem('userChatToken');
				window.localStorage.removeItem('startTime');
				window.localStorage.removeItem('maxTime');
				window.localStorage.removeItem('userChatId');
				window.localStorage.removeItem('userId');
				window.localStorage.removeItem('token');
				clearInterval(myTimeInterval);
		    }

		    if(e.detail.message.code==2008){
		      swal(e.detail.message.message,{ icon: "info"});
		    }
		}

		function timeOut(){
		    timeOutVar = setTimeout(function(){
				var obj = {
					message: "Minimizar solicitação de bate-papo",
					code:404
				};
				imClient.sendMessage(localStorage.getItem('userId'), obj);
				localStorage.removeItem('userId');
				localStorage.removeItem('token');
				$('.parent_loader').hide();
				swal("No momento, o usuário não está disponível para atender esta chamada",{icon:"info"});
		    }, 30000);
		}

		function receiveData(e) {
		    console.log(e);
		}

		function startvideoCall(token){
	       	location.href="{{route('my.video')}}";
		}

		$('.videoClose').click(function(){
	        localStorage.removeItem('token');
	        var obj = {
				message:"A videoconferência terminou.",
				code:108
	        }
	        imClient.sendMessage(localStorage.getItem('userChatId'), obj);
	        localStorage.removeItem('videoStart');
	        localStorage.removeItem('userChatToken');
	        localStorage.removeItem('userChatId');
	        localStorage.removeItem('userId');
	        localStorage.removeItem('token');
		});

	  	function eraseAll(){
		    var obj = {
				message:"A videoconferência terminou.",
				code:108
		    }
		    imClient.sendMessage(localStorage.getItem('userChatId'), obj);
		    localStorage.removeItem('token');
		    localStorage.removeItem('videoStart');
		    localStorage.removeItem('userChatToken');
		    localStorage.removeItem('userChatId');
		    localStorage.removeItem('userId');
		    localStorage.removeItem('token');
	  	}

		function incomingMessageListener(obj)
		{
			if(chatFlag==0){
				api.executeCommand('toggleChat');
				chatFlag=1;
			}
			toastr.success('Você tem uma nova mensagem de '+obj.nick);
		}

		function outgoingMessageListener(object)
		{
			if(chatFlag==1){
				chatFlag=0;
			}
			toastr.success('Mensagem enviada com sucesso');
		}

	 	function videoCloseFun(){
			updateVideoStatus(localStorage.getItem('userChatToken') ? localStorage.getItem('userChatToken'): localStorage.getItem('token'));
			swal('A videoconferência terminou.',{icon:"info"});

			var obj = {
				message:"Sua vídeo chamada está sendo desconectada.",
				code:500
			};
			imClient.sendMessage(localStorage.getItem('userChatId'), obj);
			imClient.sendMessage(localStorage.getItem('userId'), obj);
			window.localStorage.removeItem('videoStart');
			window.localStorage.removeItem('userChatToken');
			window.localStorage.removeItem('startTime');
			window.localStorage.removeItem('maxTime');
			window.localStorage.removeItem('userChatId');
			window.localStorage.removeItem('userId');
			window.localStorage.removeItem('token');
			clearInterval(myTimeInterval);
	 	}
	});

	function updateVideoStatus(token){
		if(token!=null || token!=""){
			var reqData = {
              'jsonrpc' : '2.0',
              '_token' : '{{csrf_token()}}',
              'params' : {
                    'token' : token
                }
            };
			$.ajax({
                url: "{{ route('update.video.status') }}",
                method: 'post',
                dataType: 'json',
	          	data: reqData,
                success: function(response){
                    if(response.status==1){
	                    return 1;
                    }
                }, error: function(error) {
                    toastr.info('Tente novamente depois de algum tempo');
                }
            });
		}
	}
</script>
@endif
<footer class="footer-area">
	<div class="top-footer">
		<div class="container">
			<div class="row">
				<div class="fot-links">
					<div class="row">
						<!-- <div class="col-lg-4 col-md-6 col-sm-12">
							<div class="fot-box">
								<h3>@lang('site.about_netbhe')</h3>
								<p>{!! substr($about_us->short_description, 0, 250) !!}</p>
								<a class="red_more" href="{{ url('about-us') }}">@lang('site.read_more') <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
							</div>
						</div> -->
						<div class="col-lg-9 col-md-12 col-sm-12 mid-box">
							<div class="fot-box row">
								<div class="col-md-4">
									<h3>@lang('client_site.for_professional')</h3>
									<ul style="width: 100%;">
										<li><a href="{{route('product.landing.page')}}">Crie seu curso</a></li>
										<li><a href="{{ route('exp.landing') }}">@lang('client_site.professional_landing')</a></li>
										<li><a href="{{ route('landing.pages') }}">@lang('client_site.landing_pages')</a></li>
									</ul>
									@if(count(@$category_landing_prof) <= 6)
										<ul style="width: 100%;" class="in_xtra_rows_one">
											@foreach(@$category_landing_prof as $k=>$landing)
												<li>
													<a href="{{route('category.landing.page.f',['slug'=>@$landing->categoryDetails->slug])}}">
														{{ @$landing->categoryDetails->name }}
													</a>
												</li>
											@endforeach
										</ul>
									@else
										<ul style="width: 100%;" class="@if(count(@$category_landing_prof) < 12) in_xtra_rows_two @else in_xtra_rows_three @endif">
											@foreach(@$category_landing_prof as $k=>$landing)
												<li>
													<a href="{{route('category.landing.page.f',['slug'=>@$landing->categoryDetails->slug])}}">
														@if(strlen(@$landing->categoryDetails->name) <= 17)
															{{ @$landing->categoryDetails->name }}
														@else
															{{substr(@$landing->categoryDetails->name, 0, 14 ) . '...'}}
														@endif
													</a>
												</li>
											@endforeach
										</ul>
									@endif
								</div>
								<div class="col-md-4">
									<h3>@lang('client_site.for_users')</h3>
									<ul style="width: 100%;">
										<li><a href="{{ route('all.product.search') }}">@lang('site.view_all_products')</a></li>
                                        <li><a href="{{route('affiliate.landing.page')}}">@lang('client_site.affiliate_landing_page')</a></li>
										@if(!Auth::check())
											<li><a href="{{route('consultant.categories')}}">@lang('site.experts')</a></li>
										@endif
                                        @if(!Auth::check())
											<li><a href="{{route('register')}}">@lang('site.signup')</a></li>
										@else
											<li><a class="logout" href="{{route('logout')}}">@lang('site.signout')</a></li>
										@endif

									</ul>
									@if(count(@$category_landing_user) <= 6)
										<ul style="width: 100%;" class="in_xtra_rows_one">
											@foreach(@$category_landing_user as $k=>$landing)
												<li>
													<a href="{{route('category.landing.page.f',['slug'=>@$landing->categoryDetails->slug])}}">
														{{ @$landing->categoryDetails->name }}
													</a>
												</li>
											@endforeach
										</ul>
									@else
										<ul style="width: 100%;" class="@if(count(@$category_landing_user) < 12) in_xtra_rows_two @else in_xtra_rows_three @endif">
											@foreach(@$category_landing_user as $k=>$landing)
												<li>
													<a href="{{route('category.landing.page.f',['slug'=>@$landing->categoryDetails->slug])}}">
														@if(strlen(@$landing->categoryDetails->name) <= 17)
															{{ @$landing->categoryDetails->name }}
														@else
															{{substr(@$landing->categoryDetails->name, 0, 14 ) . '...'}}
														@endif
													</a>
												</li>
											@endforeach
										</ul>
									@endif
								</div>
								<div class="col-md-4" style="border: none;">
									<h3>@lang('client_site.pages')</h3>
									<ul style="width: 100%; height: 80%;">
										<li><a href="{{route('home')}}">@lang('site.home')</a></li>
										<li><a href="{{route('frontend.about')}}">@lang('site.about_us')</a></li>
										{{-- @if(!Auth::check())
											<li><a href="{{route('register')}}">@lang('site.signup')</a></li>
										@else
											<li><a class="logout" href="{{route('logout')}}">@lang('site.signout')</a></li>
										@endif --}}
										<li><a href="{{ route('terms.of.services') }}">@lang('site.terms_of_service')</li>
                                        <li><a href="{{route('help.section')}}">@lang('site.help_section')</a></li>
										{{-- <li><a href="{{ route('privacy.policy') }}">@lang('site.privacy_policy')</a></li> --}}
										<li><a href="{{route('faq')}}">@lang('site.faq')</a></li>
										<li><a href="{{route('blog.frontend')}}">@lang('site.popular_blogs')</a></li>

									</ul>
								</div>
								<!-- <h3>@lang('site.quick_links')</h3>
								<ul>
									<li><a href="{{route('home')}}">@lang('site.home')</a></li>
									<li><a href="{{route('frontend.about')}}">@lang('site.about_us')</a></li>
									@if(!Auth::check())
										<li><a href="{{route('register')}}">@lang('site.signup')</a></li>
									@else
										<li><a class="logout" href="{{route('logout')}}">@lang('site.signout')</a></li>
									@endif
										<li><a href="{{ route('all.product.category.search') }}">@lang('site.view_all_products')</a></li>
									@if(!Auth::check())
										<li><a href="{{route('exp.register')}}">@lang('site.experts')</a></li>
										<li><a href="{{route('exp.landing')}}">@lang('site.become_an_expert')</a></li>
									@endif
								</ul>
								<h3>@lang('site.quick_links')</h3>
								<ul style="border:none;">
									<li><a href="#">@lang('site.testimonials')</a></li>
									<li><a href="{{ route('terms.of.services') }}">@lang('site.terms_of_service')</li>
									<li><a href="{{ route('privacy.policy') }}">@lang('site.privacy_policy')</a></li>
									<li><a href="{{route('blog.frontend')}}">@lang('site.blog')</a></li>
									<li><a href="{{route('faq')}}">@lang('site.faq')</a></li>
									<li><a href="{{route('help.section')}}">@lang('site.help_section')</a></li>
									<li><a href="{{route('product.landing.page')}}">Crie seu curso</a></li>

								</ul> -->
							</div>
						</div>
						<div class="col-lg-3 col-md-12 col-sm-12">
							<div class="fot-box" style="border:none;">
								<h3>@lang('site.get_in_touch')</h3>
								<p>{{-- @lang('site.china_office'):  <br>--}}{!! $footer->address !!}</p>
								<p>@lang('site.phone') : {{ $footer->ph_no }}</p>
								<p>Whatsapp : {{ $footer->whatsapp }}</p>
								<p>@lang('site.email') :  {{ $footer->email }} </p>
								<ul class="fot-social" style="width: 270px !important">
									@if(@$footer->facebook)
									<li><a target="_blank" href="{{  $footer->facebook }}"><img src="<?php echo e(URL::to('public/frontend/images/social1.png')); ?>" alt=""></a></li>
									@endif
									@if(@$footer->twitter)
									<li><a target="_blank" href="{{  $footer->twitter }}"><img src="<?php echo e(URL::to('public/frontend/images/social2.png')); ?>" alt=""></a></li>
									@endif
									<!-- <li><a href="#"><img src="<?php echo e(URL::to('public/frontend/images/social3.png')); ?>" alt=""></a></li> -->
									@if(@$footer->google_plus)
									<li><a target="_blank" href="{{  $footer->google_plus }}"><img src="<?php echo e(URL::to('public/frontend/images/social4.png')); ?>" alt=""></a></li>
									@endif
									@if(@$footer->instagram)
									<li><a target="_blank" href="{{  $footer->instagram }}"><img src="<?php echo e(URL::to('public/frontend/images/social5.png')); ?>" alt=""></a></li>
									@endif
									@if(@$footer->linkedin)
									<li><a target="_blank" href="{{  $footer->linkedin }}"><img src="<?php echo e(URL::to('public/frontend/images/social6.png')); ?>" alt=""></a></li>
									@endif
									@if(@$footer->youtube)
									<li><a target="_blank" href="{{  $footer->youtube }}"><img src="<?php echo e(URL::to('public/frontend/images/social7.png')); ?>" alt=""></a></li>
									@endif

								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="below-footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<p>© {{date('Y')}} netbhe.com.br @lang('site.all_rights_reserved')</p>
				</div>
			</div>
		</div>
	</div>
	<input type="text" class="message_master_id" hidden >
	<div class="cht-popup"></div>
</footer>
@if(@Auth::guard('web')->user()->id)
<script>
	if(localStorage.getItem('slug')){
	    var slug = localStorage.getItem('slug');
	    localStorage.removeItem('slug');
	    location.href = "<?php echo e(url('public-profile')); ?>"+'/'+slug;
	}
</script>
@endif
