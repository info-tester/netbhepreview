@if(@Auth::guard('web')->user()->is_professional=="Y")
  @include('includes.professional_header')
@else
 <header class="header-area">
    <div class="container">
        <div class="row">
            <nav class="navbar navbar-expand-lg navbar-light {{ @Auth::guard('web')->user() ? 'after_login_nav' : ''}}">
              <a class="navbar-brand" href="{{ route('home') }}"><img src="{{ URL::to('public/frontend/images/logo.png') }}" alt=""></a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>

              <div class="collapse navbar-collapse " id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                  @if(Request::segment(1)!="video-call"&&@Auth::guard('web')->user()->id)
                    <li class="">
                      <a class="become" href="{{ route('consultant.categories') }}"> <!-- <img src="{{ URL::to('public/frontend/images/becom-logo2.png') }}" alt=""> --> @lang('site.find_a_professional')</a>
                    </li>
                    <li class="">
                      <a class="become" href="{{ route('all.product.search') }}"> <!-- <img src="{{ URL::to('public/frontend/images/icon21.PNG') }}" alt=""> --> @lang('site.view_all_products')</a>
                    </li>
                  @elseif(Request::segment(1)=="video-call" &&@Auth::guard('web')->user()->id)
                    <li class="">
                      <a class="become" href="{{ route('home') }}"> <!-- <img src="{{ URL::to('public/frontend/images/becom-logo2.png') }}" alt=""> --> @lang('site.home')</a>
                    </li>
                  @elseif(!@Auth::guard('web')->user()->id && Request::segment(1)!="video-call")
                  <li class="">
                    <a class="become" href="{{ route('consultant.categories') }}"> <!-- <img src="{{ URL::to('public/frontend/images/becom-logo2.png') }}" alt=""> --> @lang('site.find_a_professional')</a>
                  </li>
                    <li class="">
                      <a class="become" href="{{ route('exp.landing') }}"> <!-- <img src="{{ URL::to('public/frontend/images/becom-logo.png') }}" alt=""> --> @lang('site.become_an_expert')</a>
                    </li>
                    <li class="">
                      <a class="become" href="{{ route('all.product.search') }}"> <!-- <img src="{{ URL::to('public/frontend/images/icon21.PNG') }}" alt=""> --> @lang('site.view_all_products')</a>
                    </li>
                    <li class="">
                      <a class="login-btn" href="{{ route('login') }}">@lang('site.login')</a>
                    </li>
                    <li class="">
                      <a class="submit-btn" href="{{ route('register') }}">@lang('site.signup')</a>
                    </li>
                    @endif
                  </ul>
                  </div>
                  @if(Request::segment(1)!="video-call" && @Auth::guard('web')->user()->id)
                    <div class="dashboard_header">
                        <div class="menu_dash">
                           <span class="uu_roundd"><img alt="" src="{{ Auth::guard('web')->user()->profile_pic ? URL::to('storage/app/public/uploads/profile_pic').'/'.@Auth::guard('web')->user()->profile_pic: URL::to('public/frontend/images/no_img.png')}}"></span>
                           <p>{{__('site.hi')}}, {{ @Auth::guard('web')->user()->nick_name ? @Auth::guard('web')->user()->nick_name : @Auth::guard('web')->user()->name }} <i class="fa fa-sort-desc" aria-hidden="true"></i> </p>
                        </div>
                        <div class="dropdown_dash" style="display:none;">
                           <ul>
                              @if(Auth::guard('web')->user()->is_professional=="N")
                                <li><a class="" href="{{ route('load_user_dashboard') }}"> @lang('site.dashboard')</a></li>
                              @else
                                <li><a class="" href="{{ route('prof.dash.my.booking',['type'=>'UC']) }}"> @lang('site.dashboard')</a></li>
                              @endif
                              <li><a href="{{ route('user.profile') }}"> @lang('site.edit_profile')</a></li>
                              <li><a href="{{ route('message') }}"> @lang('site.my_messages')</a></li>
                              <li><a href="{{ route('user.my.booking', ['type' => 'UC']) }}"> @lang('site.my_conference')</a></li>
                              <li><a href="{{ route('user.change.password') }}"> @lang('site.change_password')</a></li>
                              <li><a href="{{ route('faq') }}"> @lang('site.faq')</a></li>
                              @if(Auth::user()->is_join_affiliate=="Y")
                                <li><a href="{{ route('user.affiliation.links.list') }}"> @lang('site.my_affiliate_products')</a></li>
                                <li><a href="{{ route('affiliate.products.search') }}"> @lang('site.products_for_affiliation')</a></li>
                                <li><a href="{{ route('user.affiliation.earnings') }}"> @lang('site.my_affiliation_earnings')</a></li>

                              @endif
                              @if(Auth::user()->is_video_affiliate=="Y")
                              <li><a href="{{ route('video.affiliate.earning.list') }}"
                                @if(Request::segment(1)=="video-affiliate-program" || Request::segment(1)=="video-affiliate-earning-list" ||Request::segment(1)=="video-affiliate-payment-list") style="color:#007bff;"@endif > @lang('site.video_affiliate_program')</a></li>
                                @endif
                              <li><a class="logout" href="{{route('logout')}}">@lang('site.log_out')</a></li>
                           </ul>
                        </div>
                    </div>
                  @endif
                {{-- </ul> --}}
              {{-- </div> --}}
              <?php
              $cartProduct=getAllCart();
              ?>
              <div class="cart_icon">
                <a href="{{route('product.cart')}}" class="cart_icon_link">
                    <i class="fa fa-shopping-cart"></i>
                    <span class="cou_cart">{{count(@$cartProduct)}}</span>
                </a>

            </div>

            </nav>

        </div>
    </div>
</header>
@endif
