<header class="header-area">
<style>
.onoffswitch {
    position: relative; width: 110px; text-align: center;
    -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
    margin-left: 20px;
}
.onoffswitch-checkbox {
    position: absolute;
    opacity: 0;
    pointer-events: none;
}
.onoffswitch-label {
    display: block; overflow: hidden; cursor: pointer;
     border-radius: 4px;
}
.onoffswitch-inner {
    display: block; width: 200%; margin-left: -100%;
    transition: margin 0.3s ease-in 0s;
}
.onoffswitch-inner:before, .onoffswitch-inner:after {
    display: block; float: left; width: 50%; height: 40px; padding: 0; line-height: 40px;
    font-size: 15px; color: white;
    box-sizing: border-box;
}
.onoffswitch-inner:before {
    content: "@lang('site.user')";
    background-color: #1781d2; color: #FFFFFF;
}
.onoffswitch-inner:after {
    content: "@lang('site.professional')";
    background-color: #fa4243;
    color: #ffffff;
}

.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
    margin-left: 0;
}
.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
    right: 0px;
}

   </style>
   <div class="container">
      <div class="row">
         <nav class="navbar navbar-expand-lg navbar-light {{ @Auth::guard('web')->user() ? 'after_login_nav' : ''}}">
            <a class="navbar-brand" href="{{ route('home') }}"><img src="{{ URL::to('public/frontend/images/logo.png') }}" alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
               <span class="navbar-toggler-icon"></span>
            </button>
               <div class="collapse navbar-collapse " id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                  @if(Request::segment(1)!="video-call")
                  <li class=""><a class="become one-lins" href="{{ route('consultant.categories') }}"> <!-- <img src="{{ URL::to('public/frontend/images/becom-logo2.png') }}" alt=""> --> @lang('site.find_a_professional')</a></li>
                  <li class="">
                     <a class="become one-lins" href="{{ route('all.product.search') }}"> <!-- <img src="{{ URL::to('public/frontend/images/icon21.PNG') }}" alt=""> --> @lang('site.view_all_products')</a>
                  </li>
                  @else
                  <li class=""><a class="become one-lins" href="{{ route('home') }}"> <!-- <img src="{{ URL::to('public/frontend/images/becom-logo2.png') }}" alt=""> --> @lang('site.home')</a></li>
                  @endif
                </ul>
               </div>
                @if(Request::segment(1)!="video-call")
                  {{-- <div class="alertt">
                    <a href="#" class="noticon"> <i class="fa fa-bell" aria-hidden="true"></i> <span>25</span></a>
                  </div> --}}
                  <div class="dashboard_header">
                    <div class="menu_dash">
                       <span class="uu_roundd"><img alt="" src="{{ Auth::guard('web')->user()->profile_pic ? URL::to('storage/app/public/uploads/profile_pic').'/'.@Auth::user()->profile_pic: URL::to('public/frontend/images/no_img.png')}}"></span>
                       <p>{{__('site.hi')}}, {{ @Auth::guard('web')->user()->nick_name ? @Auth::guard('web')->user()->nick_name : @Auth::guard('web')->user()->name }} <i class="fa fa-sort-desc" aria-hidden="true"></i> </p>
                    </div>
                    <div class="dropdown_dash" style="display:none;">
                       <ul>
                           @if(Auth::guard('web')->user()->is_professional == "Y")
                           @if(Auth::guard('web')->user()->menu_type == 'U')
                               <li> <a href="javascript:;" class="switch_sidebar" data-menu="P">@lang('site.switch_to_prof')</a></li>

                            @endif
                            @if(Auth::guard('web')->user()->menu_type == 'P')
                               <li> <a href="javascript:;" class="switch_sidebar"data-menu="U">@lang('site.switch_to_user')</a></li>
                            @endif
                            @if(Auth::guard('web')->user()->menu_type == null)
                                <li> <a href="javascript:;" class="switch_sidebar"data-menu="U">@lang('site.switch_to_user')</a></li>
                            @endif

                              <!-- <div class="onoffswitch">
                                 <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" tabindex="0" @if(Auth::guard('web')->user()->menu_type == 'U') checked @endif value="P">
                              <label class="onoffswitch-label" for="myonoffswitch">
                                 <span class="onoffswitch-inner"></span>
                                 <span class="onoffswitch-switch"></span>
                              </label>
                              </div>  -->
                              <!-- <div>
                                 <span>
                                    <input type="radio" name="menu_type" class="menu_type" value="U" style="color:black" id="user" @if(Auth::guard('web')->user()->menu_type == "U") checked @endif>
                                    <label for="user">User</label>
                                    <input type="radio" name="menu_type" class="menu_type" value="P" style="color:black" id="prof" @if(Auth::guard('web')->user()->menu_type == "P") checked else @endif>
                                    <label for="prof">Professional</label><br>
                                    <input type="hidden" name="" id=user_id value="{{ Auth::user()->id }}">
                                 </span>
                              </div> -->
                           @endif
                          @if(Auth::guard('web')->user()->is_professional=="N")
                          <li><a class="" href="{{ route('load_user_dashboard') }}"> @lang('site.dashboard')</a></li>
                          @else
                          <li><a class="" href="{{ route('prof.dash.my.booking',['type'=>'UC']) }}"> @lang('site.dashboard')</a></li>
                          @endif
                          <li><a href="{{ route('professional.profile') }}"> @lang('site.edit_profile')</a></li>
                          <li><a href="{{ route('message') }}"> @lang('site.my_messages')</a></li>
                          <li><a href="{{ route('prof.my.booking', ['type' => 'UC']) }}"> @lang('site.my_conference')</a></li>
                          <li><a href="{{ route('change.professional.password') }}"> @lang('site.change_password')</a></li>
                          <li><a href="{{ route('faq') }}"> @lang('site.faq')</a></li>
                          <li><a href="{{ route('public.profile.preview', ['self' => 'self', 'slug' => auth()->user()->slug]) }}">@lang('site.preview')</a></li>
                           @if(Auth::user()->is_join_affiliate=="Y")
                              <li><a href="{{ route('user.affiliation.links.list') }}"> @lang('site.my_affiliate_products')</a></li>
                              <li><a href="{{ route('affiliate.products.search') }}"> @lang('site.products_for_affiliation')</a></li>
                              <li><a href="{{ route('user.affiliation.earnings') }}"> @lang('site.my_affiliation_earnings')</a></li>

                           @endif
                           <li><a href="{{ route('all.product.search') }}"> @lang('site.view_all_products')</a></li>
                           @if(Auth::user()->is_video_affiliate=="Y")
                           <li><a href="{{ route('video.affiliate.earning.list') }}"
                            @if(Request::segment(1)=="video-affiliate-program" || Request::segment(1)=="video-affiliate-earning-list" ||Request::segment(1)=="video-affiliate-payment-list") style="color:#007bff;"@endif >@lang('client_site.video_affiliate_program')</a></li>
                           @endif
                          <li><a class="logout" href="{{route('logout')}}"> @lang('site.log_out')</a></li>
                       </ul>
                    </div>
                  </div>
                @endif

            <?php
            $cartProduct=getAllCart();
            ?>

            <div class="cart_icon">
                <a href="{{route('product.cart')}}" class="cart_icon_link">
                    <i class="fa fa-shopping-cart"></i>
                    <span class="cou_cart">{{count(@$cartProduct)}}</span>
                </a>
            </div>


         </nav>

      </div>
   </div>
</header>
<script>
   $('.menu_type').change(function(){

      var user_type = $('input[name="menu_type"]:checked').val();
      var user_id = $('#user_id').val();
      var reqData = {
            'jsonrpc' : '2.0',
            '_token' : '{{csrf_token()}}',
            'params' : {
                  'user_type': user_type,
                  'user_id':user_id,
               }
         };
      $.ajax({
            url: "{{ route('change.sidebar') }}",
            method: 'post',
            dataType: 'json',
            data: reqData,
            success: function(response){
               if(response.status == 1){

                  console.log('Meny Type changed Sucessfully');
                  location.reload();
               }else{
                  console.log('Something went wrong');
               }
            }, error: function(error) {
                console.error(error);
            }
         });



   });
</script>

<script>
   $('.onoffswitch-checkbox').change(function(){

      var user_id = "{{Auth::user()->id}}"

       if ($('.onoffswitch-checkbox').is(':checked')) {
         var user_type = 'U'

        }
        else{
         var user_type = 'P';

        }

        var reqData = {
            'jsonrpc' : '2.0',
            '_token' : '{{csrf_token()}}',
            'params' : {
                  'user_type': user_type,
                  'user_id':user_id,
               }
         };
      $.ajax({
            url: "{{ route('change.sidebar') }}",
            method: 'post',
            dataType: 'json',
            data: reqData,
            success: function(response){
               if(response.status == 1){

                  console.log('Meny Type changed Sucessfully');
                  location.reload();
               }else{
                  console.log('Something went wrong');
               }
            }, error: function(error) {
                console.error(error);
            }
         });

   });
</script>
<script>
    $('.switch_sidebar').click(function(){

        var user_id = "{{Auth::user()->id}}";
        var user_type= $(this).data('menu');

       // if ($('.onoffswitch-checkbox').is(':checked')) {
       //   var user_type = 'U'

       //  }
       //  else{
       //   var user_type = 'P';

       //  }

        var reqData = {
            'jsonrpc' : '2.0',
            '_token' : '{{csrf_token()}}',
            'params' : {
                  'user_type': user_type,
                  'user_id':user_id,
               }
         };
      $.ajax({
            url: "{{ route('change.sidebar') }}",
            method: 'post',
            dataType: 'json',
            data: reqData,
            success: function(response){
               if(response.status == 1){

                  console.log('Meny Type changed Sucessfully');
                  location.reload();
               }else{
                  console.log('Something went wrong');
               }
            }, error: function(error) {
                console.error(error);
            }
         });


    });
</script>
