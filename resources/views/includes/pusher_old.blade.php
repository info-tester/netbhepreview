
{{-- Pusher Scripts Start --}}
<script src="https://js.pusher.com/6.0/pusher.min.js"></script>
<script>
    $('body').on('keydown','.type-msgs',function(e){
    // $(".type-msgs").keydown(function(e){
         // Enter was pressed without shift key
        if (e.key == 'Enter' && e.ctrlKey)
        {
            test=$(this).val()+"\r\n";
            $(this).val(test);
            e.preventDefault();
        }else if(e.key == 'Enter'){
            console.log($(this).data('id'));
            if($(this).data('type')=="P"){
                sendMsg1($(this).data('id'));
            }else{
                sendMsg($(this).data('id'));
            }
        }
    })
    $('body').on('change','.file-upload-chat',function(){
       var id=$(this).data('id');
       var filename = $('.file-'+id).val().split('\\').pop();
        $('.file-name-'+id).html(filename);
    })
    function nl2br (str, is_xhtml) {
    if (typeof str === 'undefined' || str === null) {
        return '';
    }
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
    }
    var message_master_id;
     function getTimeDiff(datetime) {
            var datetime = new Date( datetime ).getTime();
            var Current = new Date().getTime();
            var CalcTime = Current - datetime;  
            var Years = Math.floor(CalcTime / 1000 / 60 / 60 / 24 / 7 / 4 / 12);
            CalcTime -= Years * (1000 * 60 * 60 * 24 * 7 * 4 * 12);
            var Months = Math.floor(CalcTime / 1000 / 60 / 60 / 24 / 7 / 4);
            CalcTime -= Months * (1000 * 60 * 60 * 24 * 7 * 4);
            var Days = Math.floor(CalcTime / 1000 / 60 / 60 / 24);
            CalcTime -= Days * (1000 * 60 * 60 * 24);
            var Hours = Math.floor(CalcTime / 1000 / 60 / 60);
            CalcTime -= Hours * (1000 * 60 * 60);
            var Minutes = Math.floor(CalcTime / 1000 / 60);
            CalcTime -= Minutes * (1000 * 60);
            var Seconds = Math.floor(CalcTime / 1000 / 60);
            if(Years>1) {
                timeDiff = Years+" Years ago";
            }else if (Months>1) {
                timeDiff = Months+" Months ago";
            }else if (Days>1) {
                timeDiff = Days+" Days ago";
            }else if (Hours>1) {
                timeDiff = Hours+' Hours ago';
            }else if (Minutes>1) {
                timeDiff = Minutes+' Minutes ago';
            }else {
                timeDiff = Seconds+' Seconds ago';
            }   
            return timeDiff;
        }
    // Enable pusher logging - don't include this in production
    var name = "{{@Auth::user()->username}}"; 
        var my_id = {{@Auth::user()->id}}; 
        var user_arr=[];
        console.log(name);       
        Pusher.logToConsole = false;

        var pusher = new Pusher('{{ env("PUSHER_APP_KEY") }}', {
            cluster: '{{ env("PUSHER_APP_CLUSTER") }}'
        });

        var socketId = null;
        pusher.connection.bind('connected', function() {
            socketId = pusher.connection.socket_id;
        });
        var channel = pusher.subscribe('Dignifedme');

function sendMsg1(id){
    // alert($('.type-'+id).val());
    var replymsg = $('.chat-msg-'+id).val();
    var files = $('.file-'+id).prop('files');
    // alert(id);
    data = new FormData();
            data.append('_token', "{{ csrf_token() }}");
            data.append('project_id',id);
            data.append('type','I');
            data.append('from', $('.username-'+id).html());
            data.append('message',replymsg);
            data.append('user_id',$('.frrid'+id).val());
            data.append('socket_id',socketId);
            console.log(data);
            $.each(files, function(k,file){
                data.append('file', file);
            });
    var filecnt = Object.keys(files).length;
    if (replymsg || filecnt>0) {
        $.ajax({
            url: '{{ route("send.ajax") }}',
            type: 'POST',
            dataType: 'JSON',
            data: data,
            enctype: 'multipart/form-data',
            processData: false,  
            contentType: false
        })
        .done(result => {
            if (result.result) {
                console.log(user_arr);
                console.log(id);
                index=user_arr.indexOf(id);
                console.log(index);
                message_master_id=result.result.message_master_id;
                user_arr[index]=message_master_id;
                type="I";
                $('.main-chat-box-'+id).remove();
                project_title=result.result.project_title;
                var style='';
                if(result.result.online_status=='N'){
                    style='style="color:red"';
                }
                html=`<div class="main-chat-box main-chat-box-${message_master_id}"  style="margin-right: ${(index) * 300}px">
                            <div class="chat-heads">
                                <div class="chat-title">
                                <input type="hidden" id="type-${message_master_id}" value="${type}">
                                    <span><i aria-hidden="true" class="fa fa-circle" ${style}></i> <b class="username-${message_master_id}">${result.result.username}</b></span>
                                </div>
                                <div class="chat-action-des"  data-id="${message_master_id}">
                                    <a href="javascript:;" class="minimise-chat-box" data-id="${message_master_id}"><i aria-hidden="true" class="fa fa-minus"></i></a>
                                    <a href="javascript:;" class="chat-action" data-id="${message_master_id}"><i aria-hidden="true" class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="buildme">
                                <p>${project_title}</p>
                            </div>
                            <div class="chat-bodys chat-bodys-${message_master_id}"></div>
                            <form class="frm_msg" onsubmit="return sendMsg(${message_master_id})" data-id="${message_master_id}">
                                <div class="chat-fots">
                                    <textarea class="type-msgs chat-msg-${message_master_id}" data-id="${message_master_id}" placeholder="Type your message.."></textarea>
                                    <div class="upload_box">
                                      <input type="file" id="file" data-id="${message_master_id}" class="file-upload-chat file-${message_master_id}">
                                      <label for="file" class="btn-2"></label>
                                    </div>
                                    <p class="file-name-foots file-name-${message_master_id}"></p>
                                </div>
                            </form>
                        </div>`;
                    console.log(html);
                        $('.cht-popup').append(html);
                        // user_arr.push(message_master_id);
                    $.ajax({
                        url: '{{ route("user.message") }}',
                        type: 'POST',
                        dataType: 'JSON',
                        data: {
                            message_master_id:message_master_id,
                            type:type,
                            _token: '{{ csrf_token() }}',
                        }
                    })
                    .done(result => {
                    if (result.result) {
                        result.result.forEach(function(item, index){
                            var imgg = "{{url('public/blank.png')}}";
                            var file='';
                            // if(item.get_user.image){
                            //     imgg = "{{url('storage/app/public/userImage/')}}";
                            //     imgg = imgg+'/'+item.get_user.image;
                            // }
                            if(item.file !== null){
                                file = "{{url('storage/app/public/message_files/')}}";
                                file = file+'/'+item.file;
                                filename=item.file.substr(item.file.lastIndexOf("_")+1);
                                if(item.user_id==my_id){
                                    attach_file=`<br><a href="${file}" class="msg-link-color" target=_blank>${filename}</a>`;
                                }else{
                                    attach_file=`<br><a href="${file}" target=_blank>${filename}</a>`;
                                }
                                // attach_file=`hi`; 
                            }else{
                                attach_file=``; 
                            }
                            if(item.button_link !== null){
                                a_dis = '';
                                if(item.button_status=='D' || item.user_id==my_id){
                                    a_dis = 'a_disabled';
                                }
                                button_link=`<br><br><a href="${item.button_link}" style="border-radius: 5px;font-size: 15px;   padding: 4px 6px;" class="hire-submit msgbtn_${item.id} ${a_dis}" data-id="${item.id}" target=_blank>${item.button_name}</a>`;
                                // attach_file=`hi`; 
                            }else{
                                button_link=``; 
                            }

                            if(item.button_link1 !== null){
                                a_dis = '';
                                if(item.button_status=='D' || item.user_id==my_id){
                                    a_dis = 'a_disabled';
                                }
                                button_link1=`<a href="${item.button_link1}" style="border-radius: 5px;font-size: 15px;   padding: 4px 6px;margin-left: 7px;" class="hire-submit msgbtn_${item.id} ${a_dis}" data-id="${item.id}" target=_blank>${item.button_name1}</a>`;
                                // attach_file=`hi`; 
                            }else{
                                button_link1=``; 
                            }
                            var img_div1 = `<a class="avatar avatar-online"><img src="${imgg}" alt=""><i></i></a>`;
                            var img_div2 = `<a class="avatar avatar-online recive_img" href="javascript:;"><img src="${imgg}" alt=""></a>`;
                            var username_div = `<span class="username">${item.get_user.username}</span>`;
                            if(item.button_status=='D' || item.button_status=='A'){
                                img_div1 = ``;
                                img_div2 = ``;
                                username_div = ``;
                            }  
                            if(item.user_id==my_id){
                                var msgg = `<div class="chat">
                                    <div class="chat-avatar">${img_div1}</div>
                                    <div class="chat-body">
                                        <div class="chat-content">
                                            <p class="send_msgg">${nl2br(item.message)}${attach_file}${button_link}${button_link1}</p>
                                        </div>
                                        <time class="chat-time">${getTimeDiff(item.created_at)}</time>
                                    </div>
                                </div>`;
                            }
                            else{
                                var msgg = `<div class="chat chat-left">
                                    <div class="chat-avatar">${img_div2}</div>
                                    <div class="chat-body">
                                        ${username_div}
                                        <div class="chat-content">
                                            <p class="recive_msg">${nl2br(item.message)}${attach_file}${button_link}${button_link1}</p>
                                        </div>
                                        <time class="chat-time">${getTimeDiff(item.created_at)}</time>
                                    </div>
                                </div>`;
                            }
                            $('.chat-bodys-'+message_master_id).append(msgg);
                        });
                        $('.chat-bodys-'+message_master_id).scrollTop(1000000); 
                    } 
                    });
                // return false;
            } else {
                alert(result.error.message);
            }
        });
    } else{
        alert('Please type some message or insert file.');
    }
    // alert(id);
    return false;
}
function sendMsg(id){
    var replymsg = $('.chat-msg-'+id).val();
    var files = $('.file-'+id).prop('files');
    data = new FormData();
            data.append('_token', "{{ csrf_token() }}");
            data.append('message_master_id',id);
            data.append('type','I');
            data.append('from', $('.username-'+id).html());
            data.append('message',replymsg);
            data.append('user_id',$('.frrid'+id).val());
            data.append('socket_id',socketId);
            console.log(data);
            $.each(files, function(k,file){
                data.append('file', file);
            });
    var filecnt = Object.keys(files).length;
    if (replymsg || filecnt>0) {
        $('.chat-msg-'+id).val("");
        $('.file-'+id).val('');
        $('.file-name-'+id).html('');
        $.ajax({
            url: '{{ route("send.ajax") }}',
            type: 'POST',
            dataType: 'JSON',
            data: data,
            enctype: 'multipart/form-data',
            processData: false,  
            contentType: false
        })
        .done(result => {
            if (result.result) {
                // if(data.to_id[0]!=my_id){
                    if(result.result.file !== null){
                        file = "{{url('storage/app/public/message_files/')}}";
                        file = file+'/'+result.result.file;
                        filename=result.result.file.substr(result.result.file.lastIndexOf("_")+1)
                        attach_file=`<br><a href="${file}" class="msg-link-color" target=_blank>${filename}</a>`
                        // attach_file=`hi`; 
                    }else{
                        attach_file=``; 
                    }
                    var msgg2 = `<div class="chat">
                                    <div class="chat-avatar">
                                        <a class="avatar avatar-online">
                                        <img src="{{ @Auth::user()->image ? url('storage/app/public/userImage/'.@Auth::user()->image) : url('public/blank.png') }}" alt="">
                                        <i></i>
                                        </a>
                                    </div>
                                    <div class="chat-body">
                                        <div class="chat-content">
                                            <p class="send_msgg">${nl2br(replymsg)}${attach_file}</p>
                                        </div>
                                        <time class="chat-time">Just a second</time>
                                    </div>
                                </div>`;
                $('.chat-bodys-'+id).append(msgg2);
                $('.chat-msg-'+id).val(""); 
                $('.chat-bodys-'+id).scrollTop(1000000); 
                // return false;
            } else {
                alert(result.error.message);
            }
        });
    } else{
        alert('Please type some message or insert file.');
    }
    // alert(id);
    return false;
}
    function refershListing(){
    var keyword = $('.hasDatepicker').val();
    var reqData = {
                'jsonrpc' : '2.0',                
                '_token'  : '{{csrf_token()}}',
                'data'    : {
                'keyword'    : keyword
                }
            };
            $.ajax(
            {
                url: '{{ route('message.search') }}',
                dataType: 'json',
                data: reqData,
                type: 'post',
                success: function(response) 
                {
                    console.log(response.result.user);
                    var html='';
                    var my_id={{@Auth::user()->id}}; 
                    response.result.user.forEach(function(item, index){
                        var user='';
                        var len=item.get_project_to_user.length;
                        var title='';
                        var i=-1;
                        console.log(len);
                        if(len>1){
                        user=item.get_project_to_user[0].get_user.username+", "+item.get_project_to_user[1].get_user.username;
                        if(len>1)
                        user+=",..."
                        }else{
                            user=item.get_project_to_user[0].get_user.username; 
                        }
                        if(item.get_project.title.length>40){
                            title=item.get_project.title.substr(0,40)+".."
                        }else{
                            title=item.get_project.title
                        }
                        if(item.get_project.title.length>25){
                            body_title=item.get_project.title.substr(0,25)+".."
                        }else{
                            body_title=item.get_project.title
                        }
                       var listing='';
                        for (i = 0; i < item.get_project_to_user.length; i++) {
                            listing+='data-username'+i+'="'+item.get_project_to_user[i].get_user.username+'" data-useronls'+i+'="'+item.get_project_to_user[i].get_user.online_status+'" data-usertype'+i+'="'+item.get_project_to_user[i].is_removable+'" data-addby'+i+'="'+item.get_project_to_user[i].added_by+'"';
                        }
                        var imgg = "{{url('public/blank.png')}}";
                        html+=`<div class="chat-holders" data-id="${item.message_master_id}" data-type="${item.type}" data-maxuser="${(i-1)}" ${listing} data-name="${title}" data-username="" data-email=""> 
                            <span class="holder-image">
                                <img src="${imgg}">
                            </span>
                            <h4>${user}</h4>
                            <h5>${getTimeDiff(item.created_at)}</h5>
                            <p>${body_title}</p>
                        </div>`;
                    });
                    $('.all-chat-lists').html(html);
                },
                error:function(error) 
                {
                    console.log(error.responseText);
                }
            });
}
    $(document).ready(function() {

       


        
        // alert(channel)
        channel.bind('receive-event', function(data) {
            // console.log(my_id);
            newmsg = data.message;
            if(data.file !== null){
                file = "{{url('storage/app/public/message_files/')}}";
                file = file+'/'+data.file;
                filename=data.file.substr(data.file.lastIndexOf("_")+1);
                attach_file=`<br><a href="${file}"  target=_blank>${filename}</a>`;
                // attach_file=`hi`; 
            }else{
                attach_file=``; 
            }
            if(data.button_link !== null){
                a_dis = '';
                if(item.button_status=='D' || item.user_id==my_id){
                    a_dis = 'a_disabled';
                }
                button_link=`<br><br><a href="${data.button_link}" style="border-radius: 5px;font-size: 15px;   padding: 4px 6px;" class="hire-submit msgbtn_${item.id} ${a_dis}" data-id="${item.id}" target=_blank>${data.button_name}</a>`;
                // attach_file=`hi`; 
            }else{
                button_link=``; 
            }
            if(data.button_link1 !== null){
                a_dis = '';
                if(item.button_status=='D' || item.user_id==my_id){
                    a_dis = 'a_disabled';
                }
                button_link1=`<a href="${data.button_link1}" style="border-radius: 5px;font-size: 15px;   padding: 4px 6px;margin-left: 7px;" class="hire-submit msgbtn_${item.id} ${a_dis}" data-id="${item.id}" target=_blank>${data.button_name1}</a>`;
                // attach_file=`hi`; 
            }else{
                button_link1=``; 
            }
            console.log(data);
            var rmsgg = `<div class="media w-100 mb-3 sender-div">
                <img src="${data.image}" alt="" width="32" class="rounded-circle">
                <div class="media-body ml-3">
                    <h5>${data.from}</h5>
                    <div class="bg-light rounded py-2 px-3 mb-1">
                        <p class="text-small mb-0 text-muted">${data.message}${attach_file}${button_link}${button_link1}</p>
                    </div>
                    <p class="small">Just a second</p>
                </div>
            </div>`;
            if(jQuery.inArray(my_id, data.to_id) !== -1 && data.message_master_id==$('.message_master_id').val()){
                 $('.chat-box').append(rmsgg);
                 $('.sp-viewport').scrollTop(1000000000); 
                refershListing();
            }
            
            // alert("no message found");
            // alert(data.to_name);
            // if((jQuery.inArray(my_id, data.to_id) !== -1)){
                // console.log(user_arr);
                if(jQuery.inArray(my_id, data.to_id) !== -1){
                var curroute = "{{Route::is('message')}}";
                if(curroute!=1){
                if(!(jQuery.inArray(data.message_master_id, user_arr) !== -1)){
                    var style='';
                        if(data.online_status=='N'){
                            style='style="color:red"';
                        }
                    html=`<div class="main-chat-box main-chat-box-${data.message_master_id}" style="margin-right: ${user_arr.length * 300}px">
                        <div class="chat-heads">
                            <div class="chat-title">
                            <input type="hidden" id="type-${data.message_master_id}" value="${data.type}">
                                <span><i aria-hidden="true" class="fa fa-circle" ${style}></i> <b class="username-${data.message_master_id}">${data.from}</b></span>
                            </div>
                            <div class="chat-action-des"  data-id="${data.message_master_id}">
                                <a href="javascript:;" class="minimise-chat-box" data-id="${data.message_master_id}"><i aria-hidden="true" class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="chat-action" data-id="${data.message_master_id}"><i aria-hidden="true" class="fa fa-times"></i></a>
                            </div>
                        </div>
                        <div class="buildme">
                            <p>${data.project_title}</p>
                        </div>
                        <div class="chat-bodys chat-bodys-${data.message_master_id}"></div>
                        <form class="frm_msg" onsubmit="return sendMsg(${data.message_master_id})" data-id="${data.message_master_id}">
                            <div class="chat-fots">
                                <textarea class="type-msgs chat-msg-${data.message_master_id}" data-id="${data.message_master_id}" placeholder="Type your message.."></textarea>
                                <div class="upload_box">
                                  <input type="file" id="file" data-id="${data.message_master_id}" class="file-upload-chat file-${data.message_master_id}">
                                  <label for="file" class="btn-2"></label>
                                </div>
                                <p class="file-name-foots file-name-${data.message_master_id}"></p>
                            </div>
                        </form>
                    </div>`;
                    $('.cht-popup').append(html);
                    user_arr.push(data.message_master_id);
                    $.ajax({
                        url: '{{ route("user.message") }}',
                        type: 'POST',
                        dataType: 'JSON',
                        data: {
                            message_master_id:data.message_master_id,
                            type:data.type,
                            _token: '{{ csrf_token() }}',
                        }
                    })
                    .done(result => {
                        // $('.chat-bodys').html('');
                        console.log(result);
                        if (result.result) {
                            result.result.forEach(function(item, index){
                                var imgg = "{{url('public/blank.png')}}";
                                var file='';
                            // if(item.get_user.image){
                            //     imgg = "{{url('storage/app/public/userImage/')}}";
                            //     imgg = imgg+'/'+item.get_user.image;
                            // }
                            if(item.file !== null){
                                file = "{{url('storage/app/public/message_files/')}}";
                                file = file+'/'+item.file;
                                filename=item.file.substr(item.file.lastIndexOf("_")+1);
                                if(item.user_id==my_id){
                                    attach_file=`<br><a href="${file}" class="msg-link-color" target=_blank>${filename}</a>`;
                                }else{
                                    attach_file=`<br><a href="${file}" target=_blank>${filename}</a>`;
                                }
                                // attach_file=`hi`; 
                            }else{
                                attach_file=``; 
                            }
                            if(item.button_link !== null){
                                a_dis = '';
                                if(item.button_status=='D' || item.user_id==my_id){
                                    a_dis = 'a_disabled';
                                }
                                button_link=`<br><br><a href="${item.button_link}" style="border-radius: 5px;font-size: 15px;   padding: 4px 6px;" class="hire-submit msgbtn_${item.id} ${a_dis}" data-id="${item.id}" target=_blank>${item.button_name}</a>`;
                                // attach_file=`hi`; 
                            }else{
                                button_link=``; 
                            }

                            if(item.button_link1 !== null){
                                a_dis = '';
                                if(item.button_status=='D' || item.user_id==my_id){
                                    a_dis = 'a_disabled';
                                }
                                button_link1=`<a href="${item.button_link1}" style="border-radius: 5px;font-size: 15px;   padding: 4px 6px;margin-left: 7px;" class="hire-submit msgbtn_${item.id} ${a_dis}" data-id="${item.id}" target=_blank>${item.button_name1}</a>`;
                                // attach_file=`hi`; 
                            }else{
                                button_link1=``; 
                            }
                            var img_div1 = `<a class="avatar avatar-online"><img src="${imgg}" alt=""><i></i></a>`;
                            var img_div2 = `<a class="avatar avatar-online recive_img" href="javascript:;"><img src="${imgg}" alt=""></a>`;
                            var username_div = `<span class="username">${item.get_user.username}</span>`;
                            if(item.button_status=='D' || item.button_status=='A'){
                                img_div1 = ``;
                                img_div2 = ``;
                                username_div = ``;
                            }
                            if(item.user_id==my_id){
                            var msgg = `<div class="chat">
                                <div class="chat-avatar">${img_div1}</div>
                                <div class="chat-body">
                                    <div class="chat-content">
                                        <p class="send_msgg">${nl2br(item.message)}${attach_file}${button_link}${button_link1}</p>
                                    </div>
                                    <time class="chat-time">${getTimeDiff(item.created_at)}</time>
                                </div>
                            </div>`;
                            }else{
                                var msgg = `<div class="chat chat-left">
                                    <div class="chat-avatar">${img_div2}</div>
                                    <div class="chat-body">
                                        ${username_div}
                                        <div class="chat-content">
                                            <p class="recive_msg">${nl2br(item.message)}${attach_file}${button_link}${button_link1}</p>
                                        </div>
                                        <time class="chat-time">${getTimeDiff(item.created_at)}</time>
                                    </div>
                                </div>`;
                            }
                            $('.chat-bodys-'+data.message_master_id).append(msgg);
                        });
                        $('.chat-bodys-'+data.message_master_id).scrollTop(1000000); 
                        } 
                    });
                }else{
                    
                    var msgg = `<div class="chat chat-left">
                                <div class="chat-avatar">
                                    <a class="avatar avatar-online recive_img" href="javascript:;">
                                        <img src="${data.image}" alt="">
                                    </a>        
                                </div>
                                <div class="chat-body">
                                    <span class="username">${data.from}</span>
                                    <div class="chat-content">
                                        <p class="recive_msg">${nl2br(data.message)}${attach_file}${button_link}${button_link1}</p>
                                    </div>
                                    <time class="chat-time">Just a second</time>
                                </div>
                            </div>`;
                    $('.chat-bodys-'+data.message_master_id).append(msgg);
                    $('.chat-bodys-'+data.message_master_id).scrollTop(1000000); 
                }
            }
            }
        });
        
        $('body').on('click','.chat-action',function(){
            id=$(this).data('id');
            index=user_arr.indexOf(id);
            // console.log(user_arr);
            // alert(id);
            // alert(index);
            if(index>-1){
                if(index!=(user_arr.length-1)){
                    for (let i = index+1; i < user_arr.length; i++) {
                        var elem = user_arr[i];
                        $('.main-chat-box-'+elem).css("margin-right", ((i-1)*300)+"px");
                        
                    }
                }
                user_arr.splice(index, 1);
            }
            $('.main-chat-box-'+id).remove();
        });
        $('body').on('click','.minimise-chat-box',function(){
            id=$(this).data('id');
            if($('.main-chat-box-'+id).css("margin-bottom")==-322+"px")
            $('.main-chat-box-'+id).css("margin-bottom",0+"px");
            else
            $('.main-chat-box-'+id).css("margin-bottom",-322+"px");
        });

        

        $('body').on('keyup','.type-msgs',function(){
            message_master_id = $('.frm_msg').data('id');
            $('.message_master_id').val(message_master_id);
            // console.log(message_master_id)
        });
        // Typing
        var textarea = $('.message');
        var typingStatus = $('.typing');
        var lastTypedTime = new Date(0); // it's 01/01/1970
        var typingDelayMillis = 500; // how long user can "think about his spelling" before we show "No one is typing -blank space." message
        var isTyping = false;
        
        var curroute = "{{Route::is('message')}}";
        console.log(curroute)
        if(curroute){
            function refreshTypingStatus() {
                if (!textarea.is(':focus') || textarea.val() == '' || new Date().getTime() - lastTypedTime.getTime() > typingDelayMillis) {
                    if (isTyping) { 
                        startStopTypingAJAX('end');
                        isTyping = false;
                        console.log(isTyping);
                    }
                } else {
                    if (!isTyping) { 
                        startStopTypingAJAX('start')
                        isTyping = true;
                        console.log(isTyping);
                    }
                }
            }
        }
        else{
            function refreshTypingStatus() {
                var textarea = $('.chat-msg-0');
                if(message_master_id!=''){
                    textarea = $('.chat-msg-'+message_master_id);
                } 
                if (!textarea.is(':focus') || textarea.val() == '') {
                    if (isTyping) { 
                        startStopTypingAJAX('end');
                        isTyping = false;
                        console.log(isTyping);
                    }
                } else {
                    if (!isTyping) { 
                        startStopTypingAJAX('start')
                        isTyping = true;
                        console.log(isTyping);
                    }
                }
            }            
        }
        function updateLastTypedTime() {
            lastTypedTime = new Date();
        }
        
        setInterval(refreshTypingStatus, 2000);
        textarea.keypress(updateLastTypedTime);
        textarea.blur(refreshTypingStatus);
        
        // console.log(textarea+"PPPPPPPPPPPPPPPP")
        channel.bind('start-end-typing', function(data) {
            if (data.typing == 'end') {
                $('.typing').html('').hide(100);
            } else {
                if(jQuery.inArray(my_id, data.to_id) !== -1 && data.message_master_id==$('.message_master_id').val())
                $('.typing').html(`<p><b>${data.from}</b> is typing...</p>`).show(100);
            }
            if (data.typing == 'end') {
                // $('.typing-'+data.message_master_id).remove();
                $('.typpppp').remove();
            } else {
                if(jQuery.inArray(my_id, data.to_id) !== -1){
                    var msgg = `<div class="chat chat-left typing-${data.message_master_id}"> 
                        <div class="chat-body">
                            <div class="chat-content typpppp">
                                <p class="recive_msg"><b>${data.from}</b> is typing...</p>
                            </div>
                        </div>
                    </div>`;
                }
                $('.chat-bodys-'+data.message_master_id).append(msgg);
                $('.chat-bodys-'+data.message_master_id).scrollTop(1000000); 
            }
        });
        
        
        function startStopTypingAJAX(typing) {
            message_master_id = $('.message_master_id').val();
            $.ajax({
                url: '{{ route("typing.ajax") }}',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    message_master_id:message_master_id,
                    from: name,
                    type:'I',
                    typing: typing,
                    _token: '{{ csrf_token() }}',
                    socket_id: socketId,
                }
            });
        }

        $('body').on('click', '.chat-holders', function() {
            message_master_id = $(this).data('id');
            type=$(this).data('type');
            maxuser = $(this).data('maxuser');
            $('.maxuser').val(maxuser);
            $('.message_master_id').val(message_master_id);
            $('.type').val(type);
            $('.msg_title').html($(this).data('name'));
            $('.msg_dec').html($(this).data('email'));
            people_in_chat='<p>People in chat <a class="chat_add_btn Invite" ';
            if(maxuser>=3)
                people_in_chat+='style="display:none;"';
            people_in_chat+='>Add +</a></p>';
            if(maxuser>-1){
                for (let i = 0; i <=maxuser; i++) {
                    username=$(this).data('username'+i);
                    useronls=$(this).data('username'+i);
                    if(useronls=="Y"){
                        color='#24c624'
                    }else{
                        color='red';
                    }
                    people_in_chat+='<span class="u-'+username+'"><i aria-hidden="true" class="fa fa-circle" style="color:'+color+'"></i>'+username;
                    addby=$(this).data('addby'+i);
                    utype=$(this).data('usertype'+i);
                    if(addby==my_id && utype=='Y')
                        people_in_chat+='<a href="javascript:;" class="remove_user" data-username="'+username+'" title="Remove"><i class="fa fa-ban" aria-hidden="true"></i></a>';
                        people_in_chat+='</span>';
                }
            }
            $('.l_rightt').html(people_in_chat);
            $.ajax({
                url: '{{ route("user.message") }}',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    message_master_id:message_master_id,
                    type:type,
                    _token: '{{ csrf_token() }}',
                }
            })
            .done(result => {
                $('.chat-box').html('');
                // console.log(result.result[0]);
                    if (result.result) {
                        result.result.forEach(function(item, index){
                            var imgg = "{{url('public/blank.png')}}";
                            var file='';
                        if(item.get_user.image){
                            imgg = "{{url('storage/app/public/userImage/')}}";
                            imgg = imgg+'/'+item.get_user.image;
                        }
                        if(item.file !== null){
                            file = "{{url('storage/app/public/message_files/')}}";
                            file = file+'/'+item.file;
                            filename=item.file.substr(item.file.lastIndexOf("_")+1);
                            if(item.user_id==my_id){
                                attach_file=`<br><a href="${file}" class="msg-link-color" target=_blank>${filename}</a>`;
                            }else{
                                attach_file=`<br><a href="${file}" target=_blank>${filename}</a>`;
                            }
                            // attach_file=`hi`; 
                        }else{
                            attach_file=``; 
                        }
                        if(item.button_link !== null){
                            a_dis = '';
                            if(item.button_status=='D' || item.user_id==my_id){
                                a_dis = 'a_disabled';
                            }
                            button_link=`<br><br><a href="${item.button_link}" style="border-radius: 5px;font-size: 15px;   padding: 4px 6px;" class="hire-submit msgbtn_${item.id} ${a_dis}" data-id="${item.id}" target=_blank>${item.button_name}</a>`;
                            // attach_file=`hi`; 
                        }else{
                            button_link=``; 
                        }
                        if(item.button_link1 !== null){
                            a_dis = '';
                            if(item.button_status=='D' || item.user_id==my_id){
                                a_dis = 'a_disabled';
                            }
                            button_link1=`<a href="${item.button_link1}" style="border-radius: 5px;font-size: 15px;   padding: 4px 6px;margin-left: 7px;" class="hire-submit msgbtn_${item.id} ${a_dis}" data-id="${item.id}" target=_blank>${item.button_name1}</a>`;
                            // attach_file=`hi`; 
                        }else{
                            button_link1=``; 
                        }
                        
                        if(item.user_id==my_id){
                        var msgg = `<div class="media w-100 ml-auto mb-3 reciever-div">
                                            <div class="media-body mr-3">
                                                <div class="bg-primary rounded py-2 px-3 mb-1">
                                                    <p class="text-small mb-0 text-white">${item.message}${attach_file}${button_link}${button_link1}
                                                    </p>
                                                </div>
                                                <div class="w-100"></div>
                                                <p class="small">${getTimeDiff(item.created_at)}</p>
                                            </div>
                                            <img width="30" class="rounded-circle" src="${imgg}" alt="">
                                        </div>`;
                        }else{
                            var msgg = `<div class="media w-100 mb-3 sender-div">
                            <img src="${imgg}" alt="" width="32" class="rounded-circle">
                            <div class="media-body ml-3">
                                <h5>${item.get_user.username}</h5>
                                <div class="bg-light rounded py-2 px-3 mb-1">
                                    <p class="text-small mb-0 text-muted">${item.message}${attach_file}${button_link}${button_link1}</p>
                                </div>
                                <p class="small">${getTimeDiff(item.created_at)}</p>
                            </div>
                        </div>`;
                        }
                        $('.chat-box').append(msgg);
                        $('.sp-viewport').scrollTop(1000000000); 
                        
                    });
                    } 
                });
        });

        $('.send').click(function() {
            const message = $('.message').val();
            var files = $('#file').prop('files');
            message_master_id = $('.message_master_id').val();
            type=$('.type').val();
            var filecnt = Object.keys(files).length;
            if ((message != ''||filecnt>0) && message_master_id !='') {
            data = new FormData();
            data.append('_token', "{{ csrf_token() }}");
            data.append('message_master_id',message_master_id);
            data.append('type',type);
            data.append('from',name);
            data.append('message',message);
            data.append('socket_id',socketId);
            console.log(data);
            $.each(files, function(k,file){
                data.append('file', file);
            });
                $.ajax({
                    url: '{{ route("send.ajax") }}',
                    type: 'POST',
                    dataType: 'JSON',
                    data:data,
                    enctype: 'multipart/form-data',
                    processData: false,  
                    contentType: false
                })
                .done(result => {
                    // console.log(result.result.file);
                    if (result.result) {
                        if(result.result.file !== null){
                            file = "{{url('storage/app/public/message_files/')}}";
                            file = file+'/'+result.result.file;
                            filename=result.result.file.substr(result.result.file.lastIndexOf("_")+1)
                            attach_file=`<br><a href="${file}" class="msg-link-color" target=_blank>${filename}</a>`
                            // attach_file=`hi`; 
                        }else{
                            attach_file=``; 
                        }

                       var msgg = `<div class="media w-100 ml-auto mb-3 reciever-div">
                                        <div class="media-body mr-3">
                                            <div class="bg-primary rounded py-2 px-3 mb-1">
                                                <p class="text-small mb-0 text-white">${message}${attach_file}</p>
                                            </div>
                                            <div class="w-100"></div>
                                            <p class="small">Just a second</p>
                                        </div>
                                        <img width="30" class="rounded-circle" src="{{ @Auth::user()->image ? url('storage/app/public/userImage/'.@Auth::user()->image) : url('public/blank.png') }}" alt="">
                                    </div>`;
                        $('.chat-box').append(msgg);
                        $('.message').val('');
                        $('#file').val('');
                        $('.file_name').html('Attach File');
                        $('.sp-viewport').scrollTop(1000000000); 
                        refershListing();
                        var channel = pusher.subscribe('my-channel');
                        console.log(channel)
                    } else {
                        // alert(result.error.message);
                    }
                });
            } else {
                alert('please select user &  type some message or inser file');
            }
        });



        //Milestone Page Chat link && Proposal Page Chat link
        $('.chat_btn').click(function(){
            var frrid = "{{@$freelancer->id}}";
            var frrusername="{{@$freelancer->username}}";
            if($(this).data('freelancerid')){
                frrid = $(this).data('freelancerid');
                frrusername= $(this).data('name');
            }
            var frrname = "{{@$projectdetails->title}}";
            var projectslug = "{{@$projectdetails->slug}}";
            var project_id = "{{@$projectdetails->id}}";
            if(frrname.length>40){
                frrname=frrname.substr(0,40)+".."
            }
            var type = 'I';
            var message_master_id = 0;
            $.ajax({
                url: '{{ route("get.message.master") }}',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    user_id:frrid,
                    project_id:project_id,
                    _token: '{{ csrf_token() }}',
                }
            })
            .done(result => {
                if (result.result) {
                    message_master_id=result.result.id;
                    console.log(result);
                    if(!(jQuery.inArray(message_master_id, user_arr) !== -1)){
                        var style='';
                        if(result.users.online_status=='N'){
                            style='style="color:red"';
                        }
                        html=`<div class="main-chat-box main-chat-box-${message_master_id}"  style="margin-right: ${user_arr.length * 300}px">
                            <div class="chat-heads">
                                <div class="chat-title">
                                <input type="hidden" id="type-${message_master_id}" value="${type}">
                                    <span><i aria-hidden="true" class="fa fa-circle" ${style}></i> <b class="username-${message_master_id}">${result.username}</b></span>
                                </div>
                                <input type="hidden" class="frrid${message_master_id}" value="${frrid}">
                                <div class="chat-action-des"  data-id="${message_master_id}">
                                    <a href="javascript:;" class="minimise-chat-box" data-id="${message_master_id}"><i aria-hidden="true" class="fa fa-minus"></i></a>
                                    <a href="javascript:;" class="chat-action" data-id="${message_master_id}" style="margin-left: 5px;"><i aria-hidden="true" class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="buildme">
                            <p><a href="{{url('project-details')}}/${projectslug}" target="_blank">${frrname}</a></p>
                        </div>
                            <div class="chat-bodys chat-bodys-${message_master_id}"></div>
                            <form class="frm_msg" onsubmit="return sendMsg(${message_master_id})" data-id="${message_master_id}">
                                <div class="chat-fots">
                                    <textarea  class="type-msgs chat-msg-${message_master_id}" data-id="${message_master_id}" placeholder="Type your message.."></textarea>
                                    <div class="upload_box">
                                      <input type="file" id="file" data-id="${message_master_id}" class="file-upload-chat file-${message_master_id}">
                                      <label for="file" class="btn-2"></label>
                                    </div>
                                    <p class="file-name-foots file-name-${message_master_id}"></p>
                                </div>
                            </form>
                        </div>`;
                        $('.cht-popup').append(html);
                        user_arr.push(message_master_id);
                    $.ajax({
                        url: '{{ route("user.message") }}',
                        type: 'POST',
                        dataType: 'JSON',
                        data: {
                            message_master_id:message_master_id,
                            type:type,
                            _token: '{{ csrf_token() }}',
                        }
                    })
                    .done(result => {
                    if (result.result) {
                        result.result.forEach(function(item, index){
                            var imgg = "{{url('public/blank.png')}}";
                            var file='';
                            // if(item.get_user.image){
                            //     imgg = "{{url('storage/app/public/userImage/')}}";
                            //     imgg = imgg+'/'+item.get_user.image;
                            // }
                            if(item.file !== null){
                                file = "{{url('storage/app/public/message_files/')}}";
                                file = file+'/'+item.file;
                                filename=item.file.substr(item.file.lastIndexOf("_")+1);
                                if(item.user_id==my_id){
                                  attach_file=`<br><a href="${file}" class="msg-link-color" target=_blank>${filename}</a>`;
                                }else{
                                    attach_file=`<br><a href="${file}" target=_blank>${filename}</a>`;
                                }
                                // attach_file=`hi`; 
                            }else{
                                attach_file=``; 
                            }
                            if(item.button_link !== null){
                                a_dis = '';
                                if(item.button_status=='D' || item.user_id==my_id){
                                    a_dis = 'a_disabled';
                                }
                                button_link=`<br><br><a href="${item.button_link}" style="border-radius: 5px;font-size: 15px;   padding: 4px 6px;" class="hire-submit msgbtn_${item.id} ${a_dis}" data-id="${item.id}" target=_blank>${item.button_name}</a>`;
                                // attach_file=`hi`; 
                            }else{
                                button_link=``; 
                            }

                            if(item.button_link1 !== null){
                                a_dis = '';
                                if(item.button_status=='D' || item.user_id==my_id){
                                    a_dis = 'a_disabled';
                                }
                                button_link1=`<a href="${item.button_link1}" style="border-radius: 5px;font-size: 15px;   padding: 4px 6px;margin-left: 7px;" class="hire-submit msgbtn_${item.id} ${a_dis}" data-id="${item.id}" target=_blank>${item.button_name1}</a>`;
                                // attach_file=`hi`; 
                            }else{
                                button_link1=``; 
                            }
                            var img_div1 = `<a class="avatar avatar-online"><img src="${imgg}" alt=""><i></i></a>`;
                            var img_div2 = `<a class="avatar avatar-online recive_img" href="javascript:;"><img src="${imgg}" alt=""></a>`;
                            var username_div = `<span class="username">${item.get_user.username}</span>`;
                            if(item.button_status=='D' || item.button_status=='A'){
                                img_div1 = ``;
                                img_div2 = ``;
                                username_div = ``;
                            }

                                
                            if(item.user_id==my_id){
                                var msgg = `<div class="chat">
                                    <div class="chat-avatar">${img_div1}</div>
                                    <div class="chat-body">
                                        <div class="chat-content">
                                            <p class="send_msgg">${nl2br(item.message)}${attach_file}${button_link}${button_link1}</p>
                                        </div>
                                        <time class="chat-time">${getTimeDiff(item.created_at)}</time>
                                    </div>
                                </div>`;
                            }
                            else{
                                var msgg = `<div class="chat chat-left">
                                    <div class="chat-avatar">${img_div2}</div>
                                    <div class="chat-body">
                                        ${username_div}
                                        <div class="chat-content">
                                            <p class="recive_msg">${nl2br(item.message)}${attach_file}${button_link}${button_link1}</p>
                                        </div>
                                        <time class="chat-time">${getTimeDiff(item.created_at)}</time>
                                    </div>
                                </div>`;
                            }
                            $('.chat-bodys-'+message_master_id).append(msgg);
                        });
                        $('.chat-bodys-'+message_master_id).scrollTop(1000000); 
                    } 
                    });
                }
                } else {
                    if(!(jQuery.inArray(project_id, user_arr) !== -1)){
                    html=`<div class="main-chat-box main-chat-box-${project_id}"  style="margin-right: ${user_arr.length * 300}px">
                        <div class="chat-heads">
                            <div class="chat-title">
                            <input type="hidden" id="type-${project_id}" value="${type}">
                                <span><i aria-hidden="true" class="fa fa-circle"></i> <b class="username-${project_id}">${frrusername}</b></span>
                            </div>
                            <input type="hidden" class="frrid${project_id}" value="${frrid}">
                            <div class="chat-action-des"  data-id="${project_id}">
                                <a href="javascript:;" class="minimise-chat-box" data-id="${project_id}"><i aria-hidden="true" class="fa fa-minus"></i></a>
                                <a href="javascript:;"  class="chat-action" data-id="${project_id}"><i aria-hidden="true" class="fa fa-times"></i></a>
                            </div>
                        </div>
                        <div class="buildme">
                            <p><a href="{{url('project-details')}}/${projectslug}" target="_blank">${frrname}</a></p>
                        </div>
                        <div class="chat-bodys chat-bodys-${project_id}"></div>
                        <form class="frm_msg" onsubmit="return sendMsg1(${project_id})" data-id="${project_id}">
                            <div class="chat-fots">
                                <textarea class="type-msgs chat-msg-${project_id}" data-id="${project_id}" data-type="P" placeholder="Type your message.."></textarea>
                                <div class="upload_box">
                                  <input type="file" id="file" data-id="${project_id}" class="file-upload-chat file-${project_id}">
                                  <label for="file" class="btn-2"></label>
                                </div>
                                <p class="file-name-foots file-name-${project_id}"></p>
                            </div>
                        </form>
                    </div>`;
                    $('.cht-popup').append(html);
                    user_arr.push(Number(project_id));
                    }
                }
            });               
        });
        
        $('body').on('change','#file',function(){
            var filename = $('#file').val().split('\\').pop();
        });

        $('body').on('click','.hire-submit',function(){
            var msgid = $(this).data('id');
            $('.msgbtn_'+msgid).addClass('a_disabled');
        });
});
</script>
{{-- Pusher Scripts End --}}


