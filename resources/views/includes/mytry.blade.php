{{-- Pusher Scripts Start --}}
<script>       
    // Pusher.logToConsole = false;
    // var message_master_id;
    // function getTimeDiff(datetime) {
    //     var datetime = new Date( datetime ).getTime();
    //     var Current = new Date().getTime();
    //     var CalcTime = Current - datetime;  
    //     var Years = Math.floor(CalcTime / 1000 / 60 / 60 / 24 / 7 / 4 / 12);
    //     CalcTime -= Years * (1000 * 60 * 60 * 24 * 7 * 4 * 12);
    //     var Months = Math.floor(CalcTime / 1000 / 60 / 60 / 24 / 7 / 4);
    //     CalcTime -= Months * (1000 * 60 * 60 * 24 * 7 * 4);
    //     var Days = Math.floor(CalcTime / 1000 / 60 / 60 / 24);
    //     CalcTime -= Days * (1000 * 60 * 60 * 24);
    //     var Hours = Math.floor(CalcTime / 1000 / 60 / 60);
    //     CalcTime -= Hours * (1000 * 60 * 60);
    //     var Minutes = Math.floor(CalcTime / 1000 / 60);
    //     CalcTime -= Minutes * (1000 * 60);
    //     var Seconds = Math.floor(CalcTime / 1000 / 60);
    //     if(Years>1) {
    //         timeDiff = Years+" Years ago";
    //     }else if (Months>1) {
    //         timeDiff = Months+" Months ago";
    //     }else if (Days>1) {
    //         timeDiff = Days+" Days ago";
    //     }else if (Hours>1) {
    //         timeDiff = Hours+' Hours ago';
    //     }else if (Minutes>1) {
    //         timeDiff = Minutes+' Minutes ago';
    //     }else {
    //         timeDiff = Seconds+' Seconds ago';
    //     }   
    //     return timeDiff;
    // }
    // Enable pusher logging - don't include this in production
    // var pusher = new Pusher('{{ env("PUSHER_APP_KEY") }}', {
    //     cluster: '{{ env("PUSHER_APP_CLUSTER") }}'
    // });

    $(document).ready(function() {
        var name = "{{@Auth::user()->username}}"; 
        var my_id = {{@Auth::user()->id}}; 
        var user_arr=[];
        var socketId = 45;
        console.log(socketId); 

        $("body").delegate(".type-msgs", "keyup", function() {
        // $(".type-msgs").keydown(function(e){
            // Enter was pressed without shift key
            // console.log($(this).data('id'));
            e.preventDefault();
            console.log("Hello");
        });

        $('.chat_btn').click(function(){
            var frrusername = $(this).data('username');
            if($(this).data('userid')){
                frrid = $(this).data('userid');
                frrusername= $(this).data('username');
            }
            var frrname = $(this).data('token');
            var booking_id = $(this).data('bkid');
            var type = 'I';
            var message_master_id = 0;
            $.ajax({
                url: '{{ route("get.message.master") }}',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    booking_id:booking_id,
                    _token: '{{ csrf_token() }}',
                }
            })
            .done(result => {
                if (result.result) {
                    message_master_id=result.result.id;
                    console.log(message_master_id);
                    if(!(jQuery.inArray(message_master_id, user_arr) !== -1)){
                        var style='';
                        if(result.users.online_status=='N'){
                            style='style="color:red"';
                        }
                        html=`<div class="main-chat-box main-chat-box-${message_master_id}"  style="margin-right: ${user_arr.length * 300}px">
                            <div class="chat-heads">
                                <div class="chat-title">
                                <input type="hidden" id="type-${message_master_id}" value="${type}">
                                    <span><i aria-hidden="true" class="fa fa-circle" ${style}></i> <b class="username-${message_master_id}">${result.username}</b></span>
                                </div>
                                <input type="hidden" class="frrid${message_master_id}" value="${frrid}">
                                <div class="chat-action-des"  data-id="${message_master_id}">
                                    <a href="javascript:;" class="minimise-chat-box" data-id="${message_master_id}"><i aria-hidden="true" class="fa fa-minus"></i></a>
                                    <a href="javascript:;" class="chat-action" data-id="${message_master_id}" style="margin-left: 5px;"><i aria-hidden="true" class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="buildme">
                                <p><a href="{{url('view-booking-details')}}/${booking_id}" target="_blank">${frrname}</a></p>
                            </div>
                            <div class="chat-bodys chat-bodys-${message_master_id}"></div>
                            <form class="frm_msg" onsubmit="return sendMsg(${message_master_id})" data-id="${message_master_id}">
                                <div class="chat-fots">
                                    <textarea  class="type-msgs chat-msg-${message_master_id}" data-id="${message_master_id}" placeholder="Type your message.."></textarea>
                                    <div class="upload_box">
                                      <input type="file" id="file" data-id="${message_master_id}" class="file-upload-chat file-${message_master_id}">
                                      <label for="file" class="btn-2"></label>
                                    </div>
                                    <p class="file-name-foots file-name-${message_master_id}"></p>
                                </div>
                            </form>
                        </div>`;
                        $('.cht-popup').append(html);
                        user_arr.push(message_master_id);
                    // $.ajax({
                    //     url: '{{ route("user.message") }}',
                    //     type: 'POST',
                    //     dataType: 'JSON',
                    //     data: {
                    //         message_master_id:message_master_id,
                    //         type:type,
                    //         _token: '{{ csrf_token() }}',
                    //     }
                    // })
                    // .done(result => {
                    // if (result.result) {
                    //     result.result.forEach(function(item, index){
                    //         var imgg = "{{url('public/blank.png')}}";
                    //         var file='';
                    //         // if(item.get_user.image){
                    //         //     imgg = "{{url('storage/app/public/userImage/')}}";
                    //         //     imgg = imgg+'/'+item.get_user.image;
                    //         // }
                    //         if(item.file !== null){
                    //             file = "{{url('storage/app/public/message_files/')}}";
                    //             file = file+'/'+item.file;
                    //             filename=item.file.substr(item.file.lastIndexOf("_")+1);
                    //             if(item.user_id==my_id){
                    //               attach_file=`<br><a href="${file}" class="msg-link-color" target=_blank>${filename}</a>`;
                    //             }else{
                    //                 attach_file=`<br><a href="${file}" target=_blank>${filename}</a>`;
                    //             }
                    //             // attach_file=`hi`; 
                    //         }else{
                    //             attach_file=``; 
                    //         }

                    //         if(item.button_link1 !== null){
                    //             a_dis = '';
                    //             if(item.button_status=='D' || item.user_id==my_id){
                    //                 a_dis = 'a_disabled';
                    //             }
                    //             button_link1=`<a href="${item.button_link1}" style="border-radius: 5px;font-size: 15px;   padding: 4px 6px;margin-left: 7px;" class="hire-submit msgbtn_${item.id} ${a_dis}" data-id="${item.id}" target=_blank>${item.button_name1}</a>`;
                    //             // attach_file=`hi`; 
                    //         }else{
                    //             button_link1=``; 
                    //         }
                    //         var img_div1 = `<a class="avatar avatar-online"><img src="${imgg}" alt=""><i></i></a>`;
                    //         var img_div2 = `<a class="avatar avatar-online recive_img" href="javascript:;"><img src="${imgg}" alt=""></a>`;
                    //         var username_div = `<span class="username">${item.get_user.username}</span>`;
                    //         if(item.button_status=='D' || item.button_status=='A'){
                    //             img_div1 = ``;
                    //             img_div2 = ``;
                    //             username_div = ``;
                    //         }

                                
                    //         if(item.user_id==my_id){
                    //             var msgg = `<div class="chat">
                    //                 <div class="chat-avatar">${img_div1}</div>
                    //                 <div class="chat-body">
                    //                     <div class="chat-content">
                    //                         <p class="send_msgg">${nl2br(item.message)}${attach_file}${button_link}${button_link1}</p>
                    //                     </div>
                    //                     <time class="chat-time">${getTimeDiff(item.created_at)}</time>
                    //                 </div>
                    //             </div>`;
                    //         }
                    //         else{
                    //             var msgg = `<div class="chat chat-left">
                    //                 <div class="chat-avatar">${img_div2}</div>
                    //                 <div class="chat-body">
                    //                     ${username_div}
                    //                     <div class="chat-content">
                    //                         <p class="recive_msg">${nl2br(item.message)}${attach_file}${button_link}${button_link1}</p>
                    //                     </div>
                    //                     <time class="chat-time">${getTimeDiff(item.created_at)}</time>
                    //                 </div>
                    //             </div>`;
                    //         }
                    //         $('.chat-bodys-'+message_master_id).append(msgg);
                    //     });
                    //     $('.chat-bodys-'+message_master_id).scrollTop(1000000); 
                    // } 
                    // });
                    }
                } else {
                    if(!(jQuery.inArray(booking_id, user_arr) !== -1)){
                    html=`<div class="main-chat-box main-chat-box-${booking_id}"  style="margin-right: ${user_arr.length * 300}px">
                        <div class="chat-heads">
                            <div class="chat-title">
                            <input type="hidden" id="type-${booking_id}" value="${type}">
                                <span><i aria-hidden="true" class="fa fa-circle"></i> <b class="username-${booking_id}">${frrusername}</b></span>
                            </div>
                            <input type="hidden" class="frrid${booking_id}" value="${frrid}">
                            <div class="chat-action-des"  data-id="${booking_id}">
                                <a href="javascript:;" class="minimise-chat-box" data-id="${booking_id}"><i aria-hidden="true" class="fa fa-minus"></i></a>
                                <a href="javascript:;"  class="chat-action" data-id="${booking_id}"><i aria-hidden="true" class="fa fa-times"></i></a>
                            </div>
                        </div>
                        <div class="buildme">
                            <p><a href="{{url('view-booking-details')}}/${booking_id}" target="_blank">${frrname}</a></p>
                        </div>
                        <div class="chat-bodys chat-bodys-${booking_id}"></div>
                        <form class="frm_msg" onsubmit="return sendMsg1(${booking_id})" data-id="${booking_id}">
                            <div class="chat-fots">
                                <textarea class="type-msgs chat-msg-${booking_id}" data-id="${booking_id}" data-type="P" placeholder="Type your message.."></textarea>
                                <div class="upload_box">
                                  <input type="file" id="file" data-id="${booking_id}" class="file-upload-chat file-${booking_id}">
                                  <label for="file" class="btn-2"></label>
                                </div>
                                <p class="file-name-foots file-name-${booking_id}"></p>
                            </div>
                        </form>
                    </div>`;
                    $('.cht-popup').append(html);
                    user_arr.push(Number(booking_id));
                    }
                }
            });   
        });
    });
</script>
{{-- Pusher Scripts End --}}