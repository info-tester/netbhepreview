@if(@Auth::guard('web')->user()->is_professional=="N")
    <div class="dshbrd-lftmnu">
        <div class="prflinfo-sec">
            <!-- <span><img src="{{ URL::to('public/frontend/images/id.png') }}"> ID : UP000011</span> -->
            <div class="prflpic"><img src="{{ Auth::guard('web')->user()->profile_pic ? URL::to('storage/app/public/uploads/profile_pic').'/'.@Auth::guard('web')->user()->profile_pic: URL::to('public/frontend/images/no_img.png')}}"></div>
            {{__('site.wallet_balance')}} - {{Auth::guard('web')->user()->wallet_balance}}
        </div>
        <div class="dashmnu">

            <ul>
                <li><a href="{{ route('load_user_dashboard') }}" @if(Request::segment(1)=="my-dashboard")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/dsh1.jpg') }}"> @lang('site.dashboard')</a></li>
                <li><a href="{{ route('user.profile') }}" @if(Request::segment(1)=="edit-my-profile")style="color: #007bff;"@endif ><img src="{{ URL::to('public/frontend/images/dsh2.jpg') }}"> @lang('site.edit_profile')</a></li>
                <li><a href="{{ route('be.professional') }}" @if(Request::segment(1)=="became-professional")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/become-professional.png') }}"> @lang('site.become_professional')</a></li>
                <li><a href="{{ route('favourite.list') }}" @if(Request::segment(1)=="favourite-expert")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/favourite-expert.png') }}">@lang('site.favourite_expert')</a></li>

                <li><a href="{{ route('user.my.booking', ['type' => 'UC']) }}" @if(Request::segment(2)=="UC")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/dsh6.jpg') }}"> @lang('site.upcoming_classes')</a></li>
                <li><a href="{{ route('user.my.booking', ['type' => 'PC']) }}" @if(Request::segment(2)=="PC")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/dsh7.jpg') }}"> @lang('site.past_classes')</a></li>
                {{-- <li><a href="#"><img src="{{ URL::to('public/frontend/images/dsh8.jpg') }}"> @lang('site.my_users')</a></li>
                <li><a href="#"><img src="{{ URL::to('public/frontend/images/dsh9.jpg') }}"> @lang('site.payments')</a></li>
                <li><a href="#"><img src="{{ URL::to('public/frontend/images/dsh10.jpg') }}"> @lang('site.withdrawal_request')</a></li> --}}
                <li><a href="{{route('refer.page')}}" class="copy_refer_link"><img src="{{ URL::to('public/frontend/images/invite-freinds-icon.png') }}">@lang('site.refer')</a></li>

                <li><a href="{{route('user.wishlist')}}" @if(Request::segment(1)=="my-wishlist")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/heart.png') }}">@lang('site.my_wishlist')</a></li>
                <li><a href="{{ route('user.products') }}" @if(Request::segment(1)=="my-purchases" || Request::segment(1)=="order-details")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/dsh11.png') }}"> @lang('site.my_purchases')</a></li>

                <li><a href="{{ route('message') }}"><img src="{{ URL::to('public/frontend/images/msg.png') }}"> @lang('site.my_messages')</a></li>
                <li><a href="{{ route('chats') }}"><img src="{{ URL::to('public/frontend/images/chats.png') }}"> @lang('site.chats')</a></li>

                <li><a href="{{ route('user.view.form') }}"><img src="{{ URL::to('public/frontend/images/tools-icon.png') }}"> @lang('site.coaching_tools')</a></li>
                {{-- <li><a href="{{ route('user-content-temp.index') }}"><img src="{{ URL::to('public/frontend/images/content-template-icon.png') }}">Content templates</a></li> --}}
                {{-- @if(Auth::user()->is_join_affiliate=="Y")
                    <li><a href="{{ route('user.affiliation.links.list') }}" @if(Request::segment(1)=="my-affiliation-links" || Request::segment(1)=="my-affiliation-earnings") style="color:#007bff;"@endif ><img src="{{ URL::to('public/frontend/images/affiliate.png') }}" > @lang('site.my_affiliate_products')</a></li>
                    <li><a href="{{ route('affiliate.products.search') }}"><img src="{{ URL::to('public/frontend/images/affiliate.png') }}"> @lang('site.products_for_affiliation')</a></li>
                @endif --}}
                {{-- @if(Auth::user()->is_video_affiliate=="Y")
                <li><a href="{{ route('video.affiliate.earning.list') }}" @if(Request::segment(1)=="video-affiliate-earning" ) style="color:#007bff;"@endif ><img src="{{ URL::to('public/frontend/images/affiliate.png') }}">Video Affiliate Earning</a></li>
                @endif --}}
            </ul>
        </div>
    </div>
@endif
@if(Auth::guard('web')->user()->is_professional=="Y")

    <div class="dshbrd-lftmnu">
            <div class="prflinfo-sec">

               {{-- <span><img src="{{ URL::to('public/frontend/images/id.png') }}"> ID : UP000011</span> --}}
            <div class="prflpic"><img src="{{ Auth::guard('web')->user()->profile_pic ? URL::to('storage/app/public/uploads/profile_pic').'/'.@Auth::guard('web')->user()->profile_pic: URL::to('public/frontend/images/no_img.png')}}"></div>
            {{__('site.wallet_balance')}} - {{Auth::guard('web')->user()->wallet_balance}}
            </div>

            <div class="dashmnu">
                <span class="one-ul">
                    @if(Auth::guard('web')->user()->menu_type == "U")
                    <a href="javascript:void(0);" id="us"  @if(Request::segment(1)=="my-dashboard" || Request::segment(1)=="edit-my-profile"
                        || Request::segment(1)=="favourite-expert" || Request::segment(1)=="my-bookings"|| Request::segment(1)=="message"
                        || Request::segment(1)=="payments" || Request::segment(1)=="my-orders" || Request::segment(1)=="order-details"
                        || Request::segment(1)=="my-purchases" || Request::segment(1)=="my-wishlist" || Request::segment(1)=="my-affiliation-links"
                        || Request::segment(1)=="my-affiliation-earnings") class="prof_dash prof_dash_background" @else class="prof_dash" @endif>
                        @lang('site.user')
                    </a>
                    @else
                    <a href="javascript:void(0);" @if(Request::segment(1)=="dashboard" || Request::segment(1)=="dashboard" || Request::segment(1)=="dashboard-bookings"
                        || Request::segment(1)=="professional-view-form" || Request::segment(1)=="professional-importing-tools" || Request::segment(1)=="professional-importing-tools-view"
                        || Request::segment(1)=="user-view-form-submit-details" || Request::segment(1)=="user-evaluation360-assign-userlist"
                        || Request::segment(1)=="professional-certificate-template-view" || Request::segment(1)=="professional-certificate-template-edit"
                        || Request::segment(1)=="assign-product-certificate-template")
                        class="prof_dash prof_dash_background"  @else class="prof_dash" @endif id="prof" >
                        @lang('site.professional')
                    </a>
                    @endif
                </span>
                 @if(Auth::guard('web')->user()->menu_type == "U")
                <ul class="user-ul" >
                    <li><a href="{{ route('load_user_dashboard') }}" @if(Request::segment(1)=="my-dashboard")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/dsh1.jpg') }}"> @lang('site.dashboard')</a></li>
                    <li><a href="{{ route('user.profile') }}" @if(Request::segment(1)=="edit-my-profile")style="color: #007bff;"@endif ><img src="{{ URL::to('public/frontend/images/dsh2.jpg') }}"> @lang('site.edit_profile')</a></li>
                    @if(Auth::user()->is_professional!="Y")
                        <li><a href="{{ route('be.professional') }}" @if(Request::segment(1)=="became-professional")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/become-professional.png') }}"> @lang('site.become_professional')</a></li>
                    @endif
                    <li><a href="{{ route('favourite.list') }}" @if(Request::segment(1)=="favourite-expert")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/favourite-expert.png') }}">@lang('site.favourite_expert')</a></li>

                    <li><a href="{{ route('user.my.booking', ['type' => 'UC']) }}"@if(Request::segment(1)=="my-bookings" && Request::segment(2)!="PC")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/dsh6.jpg') }}"> @lang('site.upcoming_classes')</a></li>
                    <li><a href="{{ route('user.my.booking', ['type' => 'PC']) }}" @if(Request::segment(2)=="PC")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/dsh7.jpg') }}"> @lang('site.past_classes')</a></li>
                    {{-- <li><a href="#"><img src="{{ URL::to('public/frontend/images/dsh8.jpg') }}"> @lang('site.my_experts')</a></li>
                    <li><a href="#"><img src="{{ URL::to('public/frontend/images/dsh9.jpg') }}"> @lang('site.payments')</a></li> --}}
                    {{-- <li><a href="#"><img src="{{ URL::to('public/frontend/images/dsh10.jpg') }}"> Withdrawal Request</a></li> --}}
                    <li><a href="{{route('refer.page')}}" class="copy_refer_link"><img src="{{ URL::to('public/frontend/images/invite-freinds-icon.png') }}">@lang('site.refer')</a></li>

                    <li><a href="{{route('user.wishlist')}}" @if(Request::segment(1)=="my-wishlist")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/heart.png') }}">@lang('site.my_wishlist')</a></li>
                    <li><a href="{{ route('user.products') }}" @if(Request::segment(1)=="my-purchases" || Request::segment(1)=="order-details") style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/dsh11.png') }}"> @lang('site.my_purchases')</a></li>

                    <li><a href="{{ route('message') }}"><img src="{{ URL::to('public/frontend/images/msg.png') }}"> @lang('site.my_messages')</a></li>
                    <li><a href="{{ route('chats') }}"><img src="{{ URL::to('public/frontend/images/chats.png') }}"> @lang('site.chats')</a></li>

                    <li><a href="{{ route('user.view.form') }}"><img src="{{ URL::to('public/frontend/images/tools-icon.png') }}"> @lang('site.coaching_tools')</a></li>
                    {{-- <li><a href="{{ route('user-content-temp.index') }}"><img src="{{ URL::to('public/frontend/images/content-template-icon.png') }}">Content templates</a></li> --}}
                    {{-- @if(Auth::user()->is_join_affiliate=="Y")
                        <li><a href="{{ route('user.affiliation.links.list') }}" @if(Request::segment(1)=="my-affiliation-links" || Request::segment(1)=="my-affiliation-earnings") style="color:#007bff;"@endif ><img src="{{ URL::to('public/frontend/images/affiliate.png') }}"> @lang('site.my_affiliate_products')</a></li>
                        <li><a href="{{ route('affiliate.products.search') }}"><img src="{{ URL::to('public/frontend/images/affiliate.png') }}"> @lang('site.products_for_affiliation')</a></li>
                    @endif --}}
                    {{-- @if(Auth::user()->is_video_affiliate=="Y")
                        <li><a href="{{ route('video.affiliate.earning.list') }}" @if(Request::segment(1)=="video-affiliate-earning" ) style="color:#007bff;"@endif ><img src="{{ URL::to('public/frontend/images/affiliate.png') }}">Video Affiliate Earning</a></li>
                    @endif --}}

                </ul>
                @endif
                @if(Auth::guard('web')->user()->menu_type == "P")
                <ul class="Professional-ul">

                    <li><a href="{{ route('prof.dash.my.booking',['type'=>'UC']) }}" @if(Request::segment(1)=="dashboard" || Request::segment(1)=="dashboard-bookings")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/dsh1.jpg') }}"> @lang('site.dashboard')</a></li>
                    <li><a href="{{ route('professional.profile') }}" @if(Request::segment(1)=="edit-profile")style="color: #007bff;"@endif ><img src="{{ URL::to('public/frontend/images/dsh2.jpg') }}"> @lang('site.edit_profile')</a></li>
                    <li><a href="{{ route('professional.verify.identity') }}" @if(Request::segment(1)=="verify-identity")style="color: #007bff;"@endif ><img src="{{ URL::to('public/frontend/images/dsh2.jpg') }}"> @lang('client_site.verify_identity')</a></li>

                    {{-- @if(Auth::user()->sell != "PS")
                        <li><a href="{{ route('professional.change.category') }}" @if(Request::segment(1)=="profile-category")style="color: #007bff;"@endif ><img src="{{ URL::to('public/frontend/images/change-category-icon.png') }}"> @lang('site.change_category')</a></li>
                    @endif --}}

                    <li><a href="{{ route('professional_qualification') }}" @if(Request::segment(1)=="qualification")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/proffessional-information-icon.png') }}"> @lang('site.personal_info_and_Education')</a></li>
                    <li><a href="{{ route('professional.exp') }}"><img src="{{ URL::to('public/frontend/images/dsh4.jpg') }}" @if(Request::segment(1)=="my-experience") style="color:#007bff;"@endif> @lang('site.experience')</a></li>
                    <li><a href="{{route('professional.profile.introductory.video')}}" class="copy_refer_link" @if(Request::segment(1)=="introductory-video")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/vdo.png') }}">@lang('site.introductory_video')</a></li>

                    @if(Auth::user()->sell != "PS")
                        <li><a href="{{ route('user_availability') }}"  @if(Request::segment(1)=="availability")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/dsh3.jpg') }}">  @lang('site.availability')</a></li>
                    @endif
                    <li><a href="{{route('refer.page')}}" class="copy_refer_link"><img src="{{ URL::to('public/frontend/images/invite-freinds-icon.png') }}">@lang('site.refer')</a></li>

                    @if(Auth::user()->sell != "PS")
                        <li><a href="{{ route('prof.my.booking', ['type' => 'UC']) }}"@if(Request::segment(1)=="bookings" && Request::segment(2)!="PC")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/dsh6.jpg') }}"> @lang('site.upcoming_classes')</a></li>
                        <li><a href="{{ route('prof.my.booking', ['type' => 'PC']) }}" @if(Request::segment(2)=="PC")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/dsh7.jpg') }}"> @lang('site.past_classes')</a></li>
                    @endif

                    @if(Auth::user()->sell != "VS")
                        <li><a href="{{ route('professional.products') }}" @if(Request::segment(1)=="products" || Request::segment(1)=="store-product" || Request::segment(1)=="assign-product-certificate-template" || (Request::segment(1)=="professional-certificate-template-view" && Request::segment(3) != "")) style="color: #007bff;"@endif ><img src="{{ URL::to('public/frontend/images/dsh11.png') }}"> @lang('site.my_products')</a></li>
                        <li><a href="{{ route('index.certificate.template') }}"@if(Request::segment(1)=="professional-certificate-template" || (Request::segment(1)=="professional-certificate-template-view" && Request::segment(3) == "") || Request::segment(1)=="professional-certificate-template-edit") style="color: #007bff;"@endif ><img src="{{ URL::to('public/frontend/images/content-template-icon.png') }}">@lang('site.certificate_template')</a></li>
                        <li><a href="{{ route('professional.orders') }}" @if(Request::segment(1)=="product-orders" || Request::segment(1)=="view-order")style="color: #007bff;"@endif ><img src="{{ URL::to('public/frontend/images/sale-cion.png') }}"> @lang('site.my_sales')</a></li>
                        <li><a href="{{ route('professional.coupons') }}" @if(Request::segment(1)=="my-coupons" || Request::segment(1)=="add-coupon") style="color: #007bff;"@endif ><img src="{{ URL::to('public/frontend/images/coupon-icon.png') }}"> @lang('site.my_coupons')</a></li>
                    @endif

                    <li><a href="{{ route('list.landing.page.templates') }}" @if(Request::segment(1)=="landing-page")style="color: #007bff;"@endif ><img src="{{ URL::to('public/frontend/images/landingicon.png') }}"> @lang('site.landing_page')</a></li>
                    <li><a href="{{ route('my.blog') }}"><img src="{{ URL::to('public/frontend/images/blg.png') }}"> @lang('site.my_blogs')</a></li>
                    @if(Auth::user()->sell != "PS")
                        <li><a href="{{ route('message') }}"><img src="{{ URL::to('public/frontend/images/msg.png') }}"> @lang('site.my_messages')</a></li>
                        <li><a href="{{ route('chats') }}"><img src="{{ URL::to('public/frontend/images/chats.png') }}"> @lang('site.chats')</a></li>
                    @endif

                    {{-- <li><a href="#"><img src="{{ URL::to('public/frontend/images/dsh8.jpg') }}"> @lang('site.my_users')</a></li> --}}
                    <li><a href="{{route('my.payments')}}" @if(Request::segment(1)=="payments")style="color: #007bff;"@endif><img src="{{ URL::to('public/frontend/images/dsh9.jpg') }}"> @lang('site.payments')</a></li>
                    {{-- <li><a href="#"><img src="{{ URL::to('public/frontend/images/dsh10.jpg') }}"> Withdrawal Request</a></li> --}}
                    {{-- <li><a href="{{ route('professional.view.form') }}"><img src="{{ URL::to('public/frontend/images/dsh10.jpg') }}"> Coaching tools</a></li> --}}
                    <li><a href="{{ route('professional-form.index') }}"><img src="{{ URL::to('public/frontend/images/tools-icon.png') }}"> @lang('site.coaching_tools')</a></li>
                    {{-- @if(Auth::user()->is_video_affiliate=="Y")
                        <li><a href="{{ route('video.affiliate.earning.list') }}" @if(Request::segment(1)=="video-affiliate-earning" ) style="color:#007bff;"@endif ><img src="{{ URL::to('public/frontend/images/affiliate.png') }}">Video Affiliate Earning</a></li>
                    @endif --}}
                </ul>
                @endif
            </div>
        </div>
@endif

@if(Request::segment(1)=="dashboard" || Request::segment(1)=="edit-profile" || Request::segment(1)=="qualification"
|| Request::segment(1)=="availability" || Request::segment(1)=="my-experience" || Request::segment(1)=="bookings"
|| Request::segment(1)=="my-blog"|| Request::segment(1)=="edit-blog-post"|| Request::segment(1)=="view-blog-post"
|| Request::segment(1)=="add-blog-post"|| Request::segment(1)=="dashboard-bookings"|| Request::segment(1)=="payments"
|| Request::segment(1)=="professional-form" || Request::segment(1)=="professional-view-form"
|| Request::segment(1)=="professional-form-question" || Request::segment(1)=="professional-importing-tools"
|| Request::segment(1)=="professional-importing-tools-view" || Request::segment(1)=="user-view-form-submit-details"
|| Request::segment(1)=="prof-smartgoal-tools" || Request::segment(1)=="prof-360asign-view"
|| Request::segment(1)=="prof-smartgoal-user-assigne-view" || Request::segment(1)=="prof-importing-tools-view-details"
|| Request::segment(1)=="user-content-temp" || Request::segment(1)=="prof-view-alluser-feedback"
|| Request::segment(1)=="prof-logbook-assigne-list" || Request::segment(1)=="user-evaluation360-assign-userlist"
|| Request::segment(1)=="professional-certificate-template-view" || Request::segment(1)=="professional-certificate-template-edit"
|| Request::segment(1)=="assign-product-certificate-template")
<!-- <script>
    $('.user-ul').hide();
    $('.Professional-ul').show();
</script> -->


@endif
{{-- <script>
    $('.copy_refer_link').click(function (e) {
        e.preventDefault();
        var copyText = $(this).attr('href');
        document.addEventListener('copy', function(e) {
            e.clipboardData.setData('text/plain', copyText);
            e.preventDefault();
        }, true);
        document.execCommand('copy');
        alert('Copied Refer Link: ' + copyText);
    });
</script> --}}
