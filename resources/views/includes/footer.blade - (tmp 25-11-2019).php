<script>
	// navigator.__defineGetter__('userAgent', function () {
	//     return "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"
	// });
	// navigator.__defineGetter__('appName', function () {
	//     return "Netbhe.com"
	// });
</script>


<script type="text/javascript">
	@if(Request::segment(1)=="video-call")
		if(!navigator.getUserMedia || !navigator.webkitGetUserMedia || !navigator.mozGetUserMedia || !navigator.msGetUserMedia){
			navigator.getUserMedia = ( navigator.getUserMedia || // use the proper vendor prefix
	                       navigator.webkitGetUserMedia ||
	                       navigator.mozGetUserMedia ||
	                       navigator.msGetUserMedia);	
		}
		

		if(localStorage.getItem('videoStart') && localStorage.getItem('userChatToken')){
            checkLocalResourceAvailability('audio')
            checkLocalResourceAvailability('video')
		}

		if(localStorage.getItem('userId') && localStorage.getItem('token')){
            checkLocalResourceAvailability('audio')
            checkLocalResourceAvailability('video')
		}
	function checkLocalResourceAvailability(type) {
	    if(typeof navigator.getUserMedia === undefined) {
	        if(type == 'audio') {
	            navigator.mediaDevices.getUserMedia({type: true}).then((a) => console.log(a)).catch(() => {
	                swal("Please plugeed in your headset or enable your microphone.",{icon:"info"});
	            });
	        } else {
	            navigator.mediaDevices.getUserMedia({type: true}).then((a) => console.log(a)).catch(() => {
	                swal("Please plugeed in your Webcam or enable your Camera.",{icon:"info"});
	            });
	        }
	    } else {
	        if(type == 'audio') {
	            navigator.getUserMedia({type: true}, function() {}, function() {
	               swal("Please plugeed in your headset or enable your microphone.",{icon:"info"});
	           });
	        } else {
	            navigator.getUserMedia({type: true}, function() {}, function() {
	               swal("Please plugeed in your Webcam or enable your Camera.",{icon:"info"});
	           });
	        }
	    }
	}
	@endif
</script>


<script type="text/javascript">
  $('#news_btn').click(function(){
    
    if($('#news_email').val()==""){
      toastr.error("Please type your email");
    }

    var reqData = { 
                    'jsonrpc' : '2.0',
                    '_token'  : '{{ @csrf_token() }}',
                    'params'  : {
                        'email'   : $('#news_email').val()
                      }
                  };
    $.ajax({
        url      : '{{ route('news_letter') }}',
        method   : 'post',
        dataType : 'json',
        data     : reqData,
        success  : function(response){
            if(response.success==1){
                toastr.success("Your email account have been successfully subscribed to our news letter.");
            }
            if(response.error==1){
                toastr.error("Invalid email format.");
            }
            if(response.error==0){
                toastr.info("You have already subscribed our news letter.");
            }
        },
        error:function(error) {
            
        }
    });
  });

</script>
@if(@Auth::guard('web')->user()->id)
<script>

	$(function(){
		var timer=0, minutes=0, seconds=0;
		var yTime = xTime = 0;
		var chatFlag = 0;
		var timeOutVar;
		var domian = options = api = "";
		domain = 'phpwebdevelopmentservices.com';
		var myTimeInterval ="";
		var session = null,
		imClient = null,
		my_id = {{ @Auth::guard('web')->user()->id }},
		webRTCClient = null,
		dataClient = null;
		  
		session = apiRTC.init({
			apiKey : "47716e958466549153f429fed4b9fd80",
			apiCCId : my_id,
			onReady : sessionReadyHandler,
			senderNickname: '{{  @Auth::guard("web")->user()->name  }}'
		});
	
		// 
		function sessionReadyHandler(e) {
		    apiRTC.addEventListener("receiveIMMessage", receiveIMMessageHandler);
		    apiRTC.addEventListener("receiveData", receiveData);
		    imClient = apiCC.session.createIMClient();      
		    dataClient = apiCC.session.createDataClient();
		}
	
		if(localStorage.getItem('videoStart') && localStorage.getItem('token')){

		    $('#meet').html("");
	        options = {
	            roomName: localStorage.getItem('token'),
	            height:'500px',
	            width:'450px',
	            noSsl: true,
	            // configOverwrite:config,
	            interfaceConfigOverwrite:interfaceConfig,
	            parentNode: document.querySelector('#meet')
	      	};
		    // $('#myModal1').show();
		    api = new JitsiMeetExternalAPI(domain, options);
		       
		    api.executeCommand('password', '{{ str_random(28) }}');
			console.log('dev'+api.getAvailableDevices());
		    api.addEventListeners({
			    readyToClose: videoCloseFun
			});
		    api.executeCommand('displayName', '{{ @Auth::guard('web')->user()->name }}');
		}
		
		// for videocall.....
		var userAnswer=0;
		$('.videoCallStart').click(function(){
			swal({
				title: "Video call",
				text: "Do you want to start video call ?",
				icon: "info",
				buttons: true,
				dangerMode: true,
			})
			.then((willDelete) => {
				if (willDelete) {
					localStorage.setItem('token',$(this).data('token'));
					localStorage.setItem('userId',$(this).data('id'));
					localStorage.setItem('maxTime', $(this).data('dur')*60);
					localStorage.setItem('userType', 'P');
					userAnswer = checkUserOnline($(this).data('id'), localStorage.getItem('token'));
					$('.parent_loader').show();
					timeOut();
				} else {
				  	return false;
				}
			});
		});
		 
		// validating chat attachments.
	    $("#chat_attachment").change(function () {
	        var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'pdf', 'txt', 'doc', 'docx', 'xls', 'xlsx', 'csv'];
	        if ($.inArray($('#chat_attachment').val().split('.').pop().toLowerCase(), fileExtension) == -1) {
	            swal("Only this file formats are allowed : "+fileExtension.join(', '),{icon:'info'});
	            $('#chat_attachment').val("");
	        }
	    });

	   	// For messaging purpose......................
	    var res_id = null;
	    res_id = $('#recipents').val();
	    $('.sndms').click(function(){
	        if($('#recipents').val()==null || $('#recipents').val()==""){
	            $('#nameerr').text('Please select your rescipents');
	            return false;
	        }

	        if($('#msg').val()==""){
	            $('#msgerr').text('Please enter your message');
	            return false;
	        }

	        $(".parent_loader").show();
	        var reqData = {
	          	'jsonrpc' : '2.0',
	          	'_token' : '{{csrf_token()}}',
	          	'params' : {
	                'res' : $('#recipents').val(),
	                'sub' : $('#subject').val(),
	                'msg' : $('#msg').val()
	            }
	        };
	        var chat_form = new FormData($("#user_chat_form")[0]);
	        var userValId = $('#recipents').val();
	        var obj = {
	            message: "You have new message from {{ @Auth::guard('web')->user()->name }}",
	            code: 2008
	        };
	        $.ajax({
	            url: "{{ route('insrt.msg.prf') }}",
	            method: 'post',
	            dataType: 'json',
	          	data: reqData,
	            success: function(response){
	                if(response.status==1){
	                	$('#subject').val("");
						$('#msg').val("");
	                    if(document.getElementById("chat_attachment").files.length>0){
			            	uploadFile(response.id, chat_form, 'M');
			            	toastr.success('Message send successfully.');
			            }
			            else{
			            	$(".parent_loader").hide();
			            	toastr.success('Message send successfully.');
	                    	imClient.sendMessage(userValId, obj);
			            }
	                }
	            }, error: function(error) {
	                toastr.info('Try again after sometime');
	            }
	        });
	    });

	    function uploadFile(id, atch, type){
			atch.append("conv_id", id);
			atch.append("type", type);
			atch.append("_token", '{{csrf_token()}}');
			
	      	$.ajax({
	         	url: "{{ route('insrt.atch') }}",
	         	method: 'post',
	          	dataType: 'json',
	          	data: atch,
	          	processData: false,
	          	cache: false,
	          	contentType: false,
	          	success: function(response){
	            	if(response.status == 1){
	              		$("#chat_attachment").val("");
	              		$(".parent_loader").hide();
	              		return true;
	            	}
	          	}
	      	});
	    }

	    $('.sndreply').click(function(){
	        if($('#recipents').val()==null || $('#recipents').val()==""){
	            $('#nameerr').text('Please select your rescipents');
	            return false;
	        }

	        if($('#msg').val()==""){
	            $('#msgerr').text('Please enter your message');
	            return false;
	        }

	        $(".parent_loader").show();
	        var reqData = {
	          'jsonrpc' : '2.0',
	          '_token' : '{{csrf_token()}}',
	          'params' : {
	                'res' : $('#recipents').val(),
	                'msg' : $('#msg').val(),
	                'conv_id' : $('#conv_id').val()
	            }
	        };
	        var chat_form = new FormData($("#user_chat_form")[0]);
	        var userValId = $('#recipents').val();
	        var obj = {
	            message: "You have new message from {{ @Auth::guard('web')->user()->name }}",
	            code: 2008
	        };
	        $.ajax({
	            url: "{{ route('insrt.msg.reply') }}",
	            method: 'post',
	            dataType: 'json',
	          	data: reqData,
	            success: function(response){
	                if(response.status==1){
	                    if(document.getElementById("chat_attachment").files.length>0){
			            	uploadFile($('#conv_id').val(), chat_form,'R');
			            	toastr.success('Message send successfully.');
			            }
			            else{
			            	$(".parent_loader").hide();
			            	toastr.success('Message send successfully.');
	                    	imClient.sendMessage(userValId, obj);
			            }
	                }
	            }, error: function(error) {
	                toastr.info('Try again after sometime');
	            }
	        });
	    });

		function checkUserOnline(id, token){
		    var obj = {
				message: "You have new video call request from {{ @Auth::guard('web')->user()->name }}",
				chatToken:localStorage.getItem('token'),
				duration:localStorage.getItem('maxTime'),
				code:100
		    };  
		    imClient.sendMessage(id, obj);
		}
		
		function isRequestAccsepted(id, choice){
		    // Y for yes
		    if(choice=='Y'){
		      
		      var obj = {
		        message: "Video call request is accsepted",
		        code:101
		      };
		
		      imClient.sendMessage(id, obj);
		    }
		    // N for No
		    if(choice=='N'){
		        var obj = {
		          message: "It seems user is currently busy.",
		          code:102
		        };
		      imClient.sendMessage(id, obj);
		    }
		}
		
		function receiveIMMessageHandler(e) {
		    // for video call accseptence...
		    if(e.detail.message.code==100){
		      	swal({
		            title: "New Video call Request",
		            text: e.detail.message.message,
		            icon: "info",
		            buttons: true,
		            dangerMode: true,
		        })
		        .then((willDelete) => {
		            if (willDelete) {
						localStorage.setItem('userChatId',e.detail.senderId);
						localStorage.setItem('userChatToken',e.detail.message.chatToken);
						localStorage.setItem('maxTime',e.detail.message.duration);
						isRequestAccsepted(e.detail.senderId, 'Y');

						localStorage.setItem('videoStart',1);
						localStorage.setItem('startTime',timer);
						localStorage.setItem('userType', 'C');
						startvideoCall(e.detail.message.chatToken);
						start_timer();
		            } else {
		              	isRequestAccsepted(e.detail.senderId, 'N');
		            }
		        }); 
		    }
		    if(e.detail.message.code==101){
				$('.parent_loader').hide();
				clearTimeout(timeOutVar);
				swal('Video call request is been accsepted',{ icon: "success"});
				localStorage.setItem('startTime',timer);
				localStorage.setItem('userChatId', e.detail.senderId);
				start_timer();
				startvideoCall(localStorage.getItem('token'));
		    }
		    if(e.detail.message.code==102){
				$('.parent_loader').hide();
				//swal('It seems user is currently offline.',{ icon: "info"});
				//$('.parent_loader').hide();
		    }
		
		    if(e.detail.message.code==108){
				$('#meet').html("");
				// $('#myModal1').hide();
				localStorage.removeItem('videoStart');
				localStorage.removeItem('userChatToken');
				localStorage.removeItem('userChatId');
				localStorage.removeItem('userId');
				localStorage.removeItem('token');
				swal('Video conference is over.',{ icon: "info"});
		    }
		    if(e.detail.message.code==404){
		      	swal.close();
		    }
		    if(e.detail.message.code==500){
				$('#meet').html("");
				var html = '<center><img src="{{URL::to('public/frontend/images/link.png')}}" class="img-responsive img-fluid" style="height: 100%; width: 100%;">';
				var text = "OOPS! Currently you don't have any live video conference.";
				html+='<span><h4 class="alert alert-info">'+text+'</h4></span></center>';
				$('#meet').html(html);
				swal(e.detail.message.message,{ icon: "info"});
				domian = options = api = "";
				window.localStorage.removeItem('videoStart');
				window.localStorage.removeItem('userChatToken');
				window.localStorage.removeItem('startTime');
				window.localStorage.removeItem('maxTime');
				window.localStorage.removeItem('userChatId');
				window.localStorage.removeItem('userId');
				window.localStorage.removeItem('token');
				clearInterval(myTimeInterval);
		    }

		    if(e.detail.message.code==2008){
		      swal(e.detail.message.message,{ icon: "info"});
		    }
		}
		
		function timeOut(){
		    timeOutVar = setTimeout(function(){
				var obj = {
					message: "Minimize Chat Request",
					code:404
				};
				imClient.sendMessage(localStorage.getItem('userId'), obj);
				localStorage.removeItem('userId');
				localStorage.removeItem('token');
				$('.parent_loader').hide();
				swal("User is currently unavailable to take this call",{icon:"info"});
		    }, 30000);
		}
		
		function receiveData(e) {
		    console.log(e);
		}

		function startvideoCall(token){
	  		$('#meet').html("");
	    	options = {
	            roomName: token,
	            height:'500px',
	            width:'450px',
	            noSsl: true,
	            // configOverwrite:config,
	            interfaceConfigOverwrite:interfaceConfig,
	            parentNode: document.querySelector('#meet')
	      	};
	      	// $('#myModal1').show();
	       	location.href="{{route('my.video')}}";
	       	api = new JitsiMeetExternalAPI(domain, options);
	       	api.executeCommand('password', '<?php echo e(str_random(28)); ?>');
	       
	       	api.isAudioMuted().then(muted => {
			   api.executeCommand('toggleAudio');
			});
	       
	       	api.isVideoMuted().then(muted => {
			   api.executeCommand('toggleVideo');
		   	});
	       
	       	api.addEventListeners({
		        readyToClose: videoCloseFun
		   	});

		   	// api.addEventListener(cameraError, swal("Please grant video permission or please check that your web cam or camera is working properly.",{icon:"info"}));

		   	api.isAudioAvailable().then(available => {
			    swal("Please grant audio permission or enable it.",{icon:"info"});
			});

		   	api.isVideoAvailable().then(available => {
		   		swal("Please grant video permission or please enable it.",{icon:"info"});
			});
		    api.executeCommand('displayName', '{{ @Auth::guard('web')->user()->name }}');
		}

		$('.videoClose').click(function(){
			console.log('close');
		    $('#meet').html("");
			var html = '<center><img src="{{URL::to('public/frontend/images/link.png')}}" class="img-responsive img-fluid" style="height: 100%; width: 100%;">';
			var text = "OOPS! Currently you don't have any live video conference.";
			html+='<span><h4 class="alert alert-info">'+text+'</h4></span></center>';
			$('#meet').html(html);

	        domian = options = api = "";
	        localStorage.removeItem('token');
	        var obj = {
				message:"Video conference is over.",
				code:108
	        }
	        imClient.sendMessage(localStorage.getItem('userChatId'), obj);
	        localStorage.removeItem('videoStart');
	        localStorage.removeItem('userChatToken');
	        localStorage.removeItem('userChatId');
	        localStorage.removeItem('userId');
	        localStorage.removeItem('token');
		});
		
	  	function eraseAll(){
		    $('#meet').html("");
		    domian = options = api = "";
		    localStorage.removeItem('token');
		    var obj = {
				message:"Video conference is over.",
				code:108
		    }
		    imClient.sendMessage(localStorage.getItem('userChatId'), obj);
		    localStorage.removeItem('videoStart');
		    localStorage.removeItem('userChatToken');
		    localStorage.removeItem('userChatId');
		    localStorage.removeItem('userId');
		    localStorage.removeItem('token');
	  	}
		
		function incomingMessageListener(obj)
		{
			if(chatFlag==0){
				api.executeCommand('toggleChat');
				chatFlag=1;
			}

			toastr.success('You have new message from '+obj.nick);
		}
		
		function outgoingMessageListener(object)
		{ 
			if(chatFlag==1){
				chatFlag=0;
			}
			toastr.success('Message sended successfully');
		}
		
		
	  	/*// Customer side
	  	if(localStorage.getItem('videoStart') && localStorage.getItem('userChatToken')){
	  		console.log('videoStart', 'userChatToken')
	     	$('#meet').html("");
			options = {
				roomName: localStorage.getItem('userChatToken'),
				height:'500px',
				width:'450px',
				// configOverwrite:config,
				interfaceConfigOverwrite:interfaceConfig,
				noSsl: true,
				parentNode: document.querySelector('#meet')
			};
		    // $('#myModal1').show();
		    api = new JitsiMeetExternalAPI(domain, options);
		     
		    api.executeCommand('password', '{{ str_random(28) }}');
			console.log('test');
		    api.addEventListeners({
		        readyToClose: videoCloseFun
		    });
		    api.isAudioMuted().then(muted => {
	            if(muted) {
	                api.executeCommand('toggleAudio');
	            }
			});
	        api.isVideoMuted().then(muted => {
	            if(muted) {
	                api.executeCommand('toggleVideo');
	            }
	        });
			api.isAudioAvailable().then(available => {
	            if(! available) {
	                swal("Please grant audio permission or enable it.",{icon:"info"});
	            }
			});
		   	api.isVideoAvailable().then(available => {
	           	if(! available) {
	               swal("Please grant video permission or please enable it.",{icon:"info"});
	           	}
			});
		  	api.executeCommand('displayName', '{{ @Auth::guard('web')->user()->name }}');
		}
		
	    // Profesional side
		if(localStorage.getItem('userId') && localStorage.getItem('token')){
			console.log('userId', 'token')
	  		$('#meet').html("");
	      	options = {
				roomName: localStorage.getItem('token'),
				height:'500px',
				width:'450px',
				// configOverwrite:config,
				interfaceConfigOverwrite:interfaceConfig,
				noSsl: true,
				parentNode: document.querySelector('#meet')
			};
	    	// $('#myModal1').show();
		     
		    api = new JitsiMeetExternalAPI(domain, options);
		    api.executeCommand('password', '{{ str_random(28) }}');
			
		    api.addEventListeners({
		        readyToClose: videoCloseFun
		    });
		    api.isAudioMuted().then(muted => {
	            if(muted) {
	                api.executeCommand('toggleAudio');
	            }
			});
	       	api.isVideoMuted().then(muted => {
	           if(muted) {
	               api.executeCommand('toggleVideo');
	           }
		   	});
			api.isAudioAvailable().then(available => {
	            if(! available) {
	                swal("Please grant audio permission or enable it.",{icon:"info"});
	            }
			});
	        api.isVideoAvailable().then(available => {
	            if(! available) {
	                swal("Please grant video permission or please enable it.",{icon:"info"});
	            }
			});
		  	api.executeCommand('displayName', '{{ @Auth::guard('web')->user()->name}}');
		}*/
		
		if(localStorage.getItem('startTime')){
		  	start_timer();
		}
		function start_timer() {
			
		  	if(window.localStorage.getItem('startTime')<window.localStorage.getItem('maxTime')){

				if(window.localStorage.getItem('startTime')){
					timer = parseInt(window.localStorage.getItem('startTime'));
					window.localStorage.removeItem("startTime");
					window.localStorage.setItem('startTime', timer);
				}
				else{
					window.localStorage.removeItem("startTime");
					timer = 0;
					window.localStorage.setItem('startTime', timer);
				}

			    myTimeInterval = setInterval(function () {
			    	if(parseInt(window.localStorage.getItem('startTime'))<parseInt(window.localStorage.getItem('maxTime'))){
			    		minutes = parseInt(timer / 60, 10);
				      	seconds = parseInt(timer % 60, 10);
				      	minutes = minutes < 10 ? "0" + minutes : minutes;
				      	seconds = seconds < 10 ? "0" + seconds : seconds;
				      	timer++;
				      	localStorage.removeItem("startTime");
				      	localStorage.setItem('startTime', timer);
			    	}
			    	else{
			    		updateVideoStatus(localStorage.getItem('userChatToken') ? localStorage.getItem('userChatToken'): localStorage.getItem('token'));
			    		swal('Video conference is over.',{icon:"info"});
			    		$('#meet').html("");
						var html = '<center><img src="{{URL::to('public/frontend/images/link.png')}}" class="img-responsive img-fluid" style="height: 100%; width: 100%;">';
						var text = "OOPS! Currently you don't have any live video conference.";
						html+='<span><h4 class="alert alert-info">'+text+'</h4></span></center>';
						$('#meet').html(html);
						domian = options = api = "";
						var obj = {
							message:"Video conference is over.",
							code:500
						};	  
						imClient.sendMessage(localStorage.getItem('userChatId'), obj);
						imClient.sendMessage(localStorage.getItem('userId'), obj);
						window.localStorage.removeItem('videoStart');
						window.localStorage.removeItem('userChatToken');
						window.localStorage.removeItem('startTime');
						window.localStorage.removeItem('maxTime');
						window.localStorage.removeItem('userChatId');
						window.localStorage.removeItem('userId');
						window.localStorage.removeItem('token');
						clearInterval(myTimeInterval);
			    	}
			    }, 1000);
			}
			else{
				$('#meet').html("");
				domian = options = api = "";
				var obj = {
					message:"Video conference is over.",
					code:500
				};
				imClient.sendMessage(localStorage.getItem('userChatId'), obj);
				imClient.sendMessage(localStorage.getItem('userId'), obj);
				window.localStorage.removeItem('videoStart');
				window.localStorage.removeItem('userChatToken');
				window.localStorage.removeItem('startTime');
				window.localStorage.removeItem('maxTime');
				window.localStorage.removeItem('userChatId');
				window.localStorage.removeItem('userId');
				window.localStorage.removeItem('token');
				clearInterval(myTimeInterval);
			}
		}

	    
	 	function videoCloseFun(){
			updateVideoStatus(localStorage.getItem('userChatToken') ? localStorage.getItem('userChatToken'): localStorage.getItem('token'));
			swal('Video conference is over.',{icon:"info"});
			$('#meet').html("");
			var html = '<center><img src="{{URL::to('public/frontend/images/link.png')}}" class="img-responsive img-fluid" style="height: 100%; width: 100%;">';
			var text = "OOPS! Currently you don't have any live video conference.";
			html+='<span><h4 class="alert alert-info">'+text+'</h4></span></center>';
			$('#meet').html(html);
			domian = options = api = "";
			var obj = {
				message:"Your video call is being disconnected.",
				code:500
			};	  
			imClient.sendMessage(localStorage.getItem('userChatId'), obj);
			imClient.sendMessage(localStorage.getItem('userId'), obj);
			window.localStorage.removeItem('videoStart');
			window.localStorage.removeItem('userChatToken');
			window.localStorage.removeItem('startTime');
			window.localStorage.removeItem('maxTime');
			window.localStorage.removeItem('userChatId');
			window.localStorage.removeItem('userId');
			window.localStorage.removeItem('token');
			clearInterval(myTimeInterval);
	 	}
	});
		
	function updateVideoStatus(token){
		if(token!=null || token!=""){
			var reqData = {
              'jsonrpc' : '2.0',
              '_token' : '{{csrf_token()}}',
              'params' : {
                    'token' : token
                }
            };
			$.ajax({
                url: "{{ route('update.video.status') }}",
                method: 'post',
                dataType: 'json',
	          	data: reqData,
                success: function(response){
                    if(response.status==1){
	                    return 1;	
                    }
                }, error: function(error) {
                    toastr.info('Try again after sometime');
                }
            });
		}
	}
</script>
@endif
<footer class="footer-area">
	<div class="top-footer">
		<div class="container">
			<div class="row">
				<div class="fot-links">
					<div class="row">
						<div class="col-lg-4 col-md-6 col-sm-12">
							<div class="fot-box">
								<h3>@lang('site.about_netbhe')</h3>
								<p>{!! substr($about_us->short_description, 0, 250) !!}</p>
								<a class="red_more" href="{{ url('about-us') }}">@lang('site.read_more') <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
							</div>
						</div>
						<div class="col-lg-5 col-md-6 col-sm-12 mid-box">
							<div class="fot-box">
								<h3>@lang('site.quick_links')</h3>
								<ul>
									<li><a href="<?php echo e(route('home')); ?>">@lang('site.home')</a></li>
									<li><a href="{{route('frontend.about')}}">@lang('site.about_us')</a></li>
									@if(!Auth::check())
										<li><a href="{{route('register')}}">@lang('site.signup')</a></li>
									@else
										<li><a href="{{route('logout')}}">@lang('site.signout')</a></li>
									@endif
									
									<!-- <li><a href="#">FAQ</a></li> -->
									
										<li><a href="javascript:;" class="how_it_works">@lang('site.how_it_works')</a></li>
									@if(!Auth::check())
										<li><a href="{{route('exp.register')}}">@lang('site.experts')</a></li>
										<li><a href="{{route('exp.register')}}">@lang('site.become_an_expert')</a></li>
									@endif
								</ul>
								<ul style="border:none;">
									<li><a href="#">@lang('site.testimonials')</a></li>
									<!-- <li><a href="#">Team</a></li> -->
									<li><a href="{{ route('terms.of.services') }}">@lang('site.terms_of_service')</li>
									<li><a href="{{ route('privacy.policy') }}">@lang('site.privacy_policy')</a></li>
									<!-- <li><a href="#">Jobs</a></li> -->
									<li><a href="{{route('blog.frontend')}}">@lang('site.blog')</a></li>
									<li><a href="{{route('faq')}}">@lang('site.faq')</a></li>
								</ul>
							</div>
						</div>
						<div class="col-lg-3 col-md-12 col-sm-12">
							<div class="fot-box" style="border:none;">
								<h3>@lang('site.get_in_touch')</h3>
								<p>{{-- @lang('site.china_office'):  <br>--}}{!! $footer->address !!}</p>
								<p>@lang('site.phone') : {{ $footer->ph_no }}</p>
								<p>@lang('site.email') :  {{ $footer->email }} </p>
								<ul class="fot-social">
									<li><a target="_blank" href="{{  $footer->facebook }}"><img src="<?php echo e(URL::to('public/frontend/images/social1.png')); ?>" alt=""></a></li>
									<li><a target="_blank" href="{{  $footer->twitter }}"><img src="<?php echo e(URL::to('public/frontend/images/social2.png')); ?>" alt=""></a></li>
									<!-- <li><a href="#"><img src="<?php echo e(URL::to('public/frontend/images/social3.png')); ?>" alt=""></a></li> -->
									<li><a target="_blank" href="{{  $footer->google_plus }}"><img src="<?php echo e(URL::to('public/frontend/images/social4.png')); ?>" alt=""></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="below-footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<p>© {{date('Y')}} netbhe.com @lang('site.all_rights_reserved')</p>
				</div>
			</div>
		</div>
	</div>
</footer>
@if(@Auth::guard('web')->user()->id)
<script>
	if(localStorage.getItem('slug')){
	    var slug = localStorage.getItem('slug');
	    localStorage.removeItem('slug');
	    location.href = "<?php echo e(url('public-profile')); ?>"+'/'+slug;
	}
</script>
@endif
