<script type="text/javascript">
navigator.__defineGetter__('userAgent', function () {
    return "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:28.0) Gecko/20100101 Firefox/28.0)"
});
navigator.__defineGetter__('appName', function () {
    return "Netbhe.com"
});
</script>
<script type="text/javascript" src="{{URL::to('public/frontend/js/jquery-3.2.1.js') }}"></script>
<script type="text/javascript" src="{{URL::to('public/frontend/js/bootstrap.min.js') }}"></script>
<script src="{{URL::to('public/frontend/js/owl.carousel.js') }}"></script>
<script src="{{URL::to('public/frontend/js/interface_config.js') }}"></script>
<script src="{{URL::to('public/admin/js/jquery.validate.js') }}"></script>
<script src="{{URL::to('public/frontend/js/jquery.cpfcnpj.js') }}"></script>
<script src="{{URL::to('public/admin/js/jquery.toast.js')}}"></script>
<link href="{{ URL::to('public/frontend/css/calender.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::to('public/frontend/css/jquery.classyscroll.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<script src="{{ URL::to('public/frontend/js/jquery-ui.js') }}"></script>
<script src="{{ URL::to('public/admin/js/chosen.jquery.min.js') }}"></script>
<script src="{{ URL::to('public/frontend/js/jquery.ddslick.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.js"></script>
{{-- <script src='https://meet.jit.si/external_api.js'></script> --}}
<script src="https://cloud.apizee.com/apiRTC/apiRTC-latest.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/froala-editor@3.1.0/js/froala_editor.pkgd.min.js"></script>
<script src="{{ URL::to('public/frontend/tiny_mce/tinymce.min.js') }}"></script>
<script src="{{ URL::to('public/frontend/js/jquery.classyscroll.js') }}"></script>
<script src="{{ URL::to('public/frontend/js/jquery.mousewheel.js') }}"></script>
<script src="https://js.pusher.com/7.0/pusher.min.js"></script>

<script src="https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>


{{-- Update User timezone when its not set --}}
{{-- @dd(session()->get('timezone_id')) --}}
@if ((auth()->check() && auth()->user()->timezone_id == 0) || (!auth()->check() && session()->get('timezone') == ''))
    <script>
        // Update User timezone
        $(document).ready(function() {
            // let s = swal({
            //     title:'Detectando fuso horário...',
            //     button: false,
            //     closeOnClickOutside: false,
            //     closeOnEsc: false,
            //     className: 'with-loader',
            // });
            var timezone_offset_minutes = new Date().getTimezoneOffset();
            timezone_offset_minutes = timezone_offset_minutes == 0 ? 0 : -timezone_offset_minutes;
            console.log(timezone_offset_minutes);
            $.post("{{ route('update.timezone') }}",{
                jsonrpc: 2.0,
                _token: "{{ csrf_token() }}",
                params: {
                    timezone_offset_minutes: timezone_offset_minutes
                }
            })
            .done(response => {
                // swal.close();
                if (response.error && response.error.message) {
                    swal({
                        title: 'Erro!',
                        text: response.error.message,
                        icon: 'error',
                    });
                } else if (response.result) {
                    window.location.reload(true);
                } else {
                    swal({
                        title: 'Erro!',
                        text: 'Ops! Algo deu errado.',
                        icon: 'error',
                    });
                }
            })
            .fail(error => {
                // swal.close();
                swal({
                    title: 'Erro!',
                    text: 'Ops! Algo deu errado.',
                    icon: 'error',
                });
            })
        });
    </script>
@endif

<script>
    var productCount = 0;
    (function($) {
    $.fn.inputFilter = function(inputFilter) {
        return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
        if (inputFilter(this.value)) {
            this.oldValue = this.value;
            this.oldSelectionStart = this.selectionStart;
            this.oldSelectionEnd = this.selectionEnd;
        } else if (this.hasOwnProperty("oldValue")) {
            this.value = this.oldValue;
            this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        } else {
            this.value = "";
        }
        });
    };
    }(jQuery));
</script>

<script type="text/javascript">
    $(document).ready(function(){
        function getItems(items) {
            if(productCount <= 3){
                return productCount;
            } else {
                return items;
            }
        }
        function getLoop(items) {
            if(productCount <= items){
                return false;
            } else {
                return true;
            }
        }
        $("#owl-demo-1").owlCarousel({
            margin: 25,
            nav: true,
            loop: true,
            responsive: {
                0: {
                    items:1
                },
                768: {
                    items:2
                },
                1078: {
                    items:3
                },
                1200: {
                    items:3
                },
            }
        });
        $("#owl-demo-2").owlCarousel({
            margin: 30,
            nav: true,
            loop: true,
            responsive: {
                0: {
                    items: 1
                },
                576: {
                    items:1
                },
                992: {
                    items:2
                },
            }
        });
        $("#owl-demo-6").owlCarousel({
            margin: 25,
            nav: true,
            responsive: {
                0: {
                    items: 1,
                    loop: getLoop(1),
                },
                768: {
                    items: 2,
                    loop: getLoop(2),
                },
                1078: {
                    items: 3,
                    loop: getLoop(3),
                },
                1200: {
                    items: 3,
                    loop: getLoop(3),
                },
            }
        });

        $(".mobile_filter").click(function(){
            $(".dshbrd-lftmnu").slideToggle();
        });

        $(".menu_dash").mouseenter(function(){
            $(".dropdown_dash").slideDown();
        });
        $(".dropdown_dash").mouseleave(function(){
            $(".dropdown_dash").slideUp();
        });

        $('#us').click(function(){
            $('.user-ul').show();
            $('.Professional-ul').hide();
        });

        $('#prof').click(function(){
            $('.user-ul').hide();
            $('.Professional-ul').show();
        });

        $('.prof_dash').on('click', function(){
        $('.one-ul a').removeClass('prof_dash_background');
            $(this).addClass('prof_dash_background');
        });

        $(".how_it_works").click(function() {
            $('html,body').animate({
                scrollTop: $(".how-work").offset().top},
            'slow');
        });
    });
    @if(@Auth::guard('web')->user()->id)
        if(localStorage.getItem('bookSlug')){
            var sl = localStorage.getItem('bookSlug');
            localStorage.removeItem('bookSlug');
            location.href = "{{ url('booking') }}"+'/'+sl;
        }
    @endif
</script>

{{-- Pusher Scripts Start --}}
<script>
    var message_master_id;
    var name = "{{@Auth::user()->username}}";
    var my_id = parseInt("{{@Auth::user()->id}}");
    var user_arr=msgmaster_arr=[];
    var keyup = 0;
    var emoji_array = ["😀", "😃", "😄", "😁", "😆", "😅", "😂", "🤣","😊", "😇", "🙂", "🙃", "😉", "😌", "😍", "🥰", "😘", "😗", "😙", "😚", "😋", "😛", "😝", "😜", "🤪", "🤨", "🧐", "🤓", "😎", "🤩", "🥳", "😏", "😒", "😞", "😔", "😟", "😕", "🙁", "☹️", "😣", "😖", "😫",
        "😩", "🥺", "😢", "😭", "😤", "😠", "😡", "🤬", "🤯", "😳", "🥵", "🥶", "😱", "😨", "😰", "😥", "😓", "🤗", "🤔", "🤭", "🤫", "🤥", "😶", "😐", "😑", "😬", "🙄", "😯", "😦", "😧", "😮", "😲", "🥱", "😴", "🤤", "😪", "😵", "🤐", "🥴", "🤢", "🤮", "🤧", "😷", "🤒", "🤕",
        "🤑", "🤠", "😈", "👿", "👹", "👺", "🤡", "💩", "👻", "💀", "☠️", "👽", "👾", "🤖", "🎃", "😺", "😸", "😹", "😻", "😼", "😽", "🙀", "😿", "😾", "👋", "🤚", "🖐", "✋", "🖖", "👌", "🤏", "✌️", "🤞", "🤟", "🤘", "🤙", "👈", "👉", "👆", "🖕", "👇", "☝️", "👍", "👎", "✊", "👊", "🤛",
        "🤜", "👏", "🙌", "👐", "🤲", "🤝", "🙏",   "✍️", "💅", "🤳", "💪", "🦾", "🦵", "🦿", "🦶", "👣", "👂", "🦻", "👃", "🧠", "🦷", "🦴", "👀", "👁", "👅", "👄", "💋", "🩸", "👶", "👧", "🧒", "👦", "👩", "🧑", "👨", "👩‍🦱", "👨‍🦱", "👩‍🦰", "👨‍🦰", "👱‍♀️", "👱", "👱‍♂️", "👩‍🦳", "👨‍🦳", "👩‍🦲", "👨‍🦲",
        "🧔", "👵", "🧓", "👴", "👲", "👳‍♀️", "👳", "👳‍♂️", "🧕", "👮‍♀️", "👮", "👮‍♂️", "👷‍♀️", "👷", "👷‍♂️", "💂‍♀️", "💂", "💂‍♂️", "🕵️‍♀️", "🕵️", "🕵️‍♂️", "👩‍⚕️", "👨‍⚕️", "👩‍🌾", "👨‍🌾", "👩‍🍳", "👨‍🍳", "👩‍🎓", "🧑‍🎓", "👨‍🎓", "👩‍🎤", "👨‍🎤", "👩‍🏫", "👨‍🏫", "👩‍🏭", "👨‍🏭", "👩‍💻", "💻",
        "👨‍💻", "👩‍💼", "👨‍💼", "👩‍🔧", "🧑‍🔧", "👨‍🔧", "👩‍🔬", "🧑‍🔬", "👨‍🔬", "👩‍🎨", "🧑‍🎨", "👨‍🎨", "👩‍🚒", "👨‍🚒", "👩‍✈️", "🧑‍✈️", "👨‍✈️", "👩‍🚀", "👨‍🚀", "👩‍⚖️", "👨‍⚖️", "👰", "🤵", "👸", "🤴", "🦸‍♀️", "🦸", "🦸‍♂️", "🦹‍♀️", "🦹", "🦹‍♂️", "🤶", "🎅", "🧙‍♀️",
        "🧙", "🧙‍♂️", "🧝‍♀️", "🧝", "🧝‍♂️", "🧛‍♀️", "🧛", "🧛‍♂️", "🧟‍♀️", "🧟", "🧟‍♂️", "🧞‍♀️", "🧞", "🧞‍♂️", "🧜‍♀️", "🧜", "🧜‍♂️", "🧚‍♀️", "🧚", "🧚‍♂️", "👼", "🤰", "🤱", "🙇‍♀️", "🙇", "🙇‍♂️", "💁‍♀️", "💁", "💁‍♂️", "🙅‍♀️", "🙅", "🙅‍♂️", "🙆‍♀️", "🙆", "🙆‍♂️", "🙋‍♀️", "🙋", "🙋‍♂️", "🧏‍♀️", "🧏", "🧏‍♂️", "🤦‍♀️", "🤦",
        "🤦‍♂️", "🤷‍♀️", "🤷", "🤷‍♂️", "🙎‍♀️", "🙎", "🙎‍♂️", "🙍‍♀️", "🙍", "🙍‍♂️", "💇‍♀️", "💇", "💇‍♂️", "💆‍♀️", "💆", "💆‍♂️", "🧖‍♀️", "🧖", "🧖‍♂️", "💅", "🤳", "💃", "🕺", "👯‍♀️", "👯", "👯‍♂️", "🕴", "👩‍🦽", "👨‍🦽", "👩‍🦼", "👨‍🦼", "🚶‍♀️", "🚶", "🚶‍♂️", "👩‍🦯", "👨‍🦯", "🧎‍♀️", "🧎", "🧎‍♂️", "🏃‍♀️", "🏃", "🏃‍♂️", "🧍‍♀️", "🧍",
        "🧍‍♂️", "👭", "🧑‍🤝‍🧑", "👬", "👫", "👩‍❤️‍👩", "💑", "👨‍❤️‍👨", "👩‍❤️‍👨", "👩‍❤️‍💋‍👩", "💏", "👨‍❤️‍💋‍👨", "👩‍❤️‍💋‍👨", "👪", "👨‍👩‍👦", "👨‍👩‍👧", "👨‍👩‍👧‍👦", "👨‍👩‍👦‍👦", "👨‍👩‍👧‍👧", "👨‍👨‍👦", "👨‍👨‍👧", "👨‍👨‍👧‍👦", "👨‍👨‍👦‍👦", "👨‍👨‍👧‍👧", "👩‍👩‍👦", "👩‍👩‍👧", "👩‍👩‍👧‍👦", "👩‍👩‍👦‍👦", "👩‍👩‍👧‍👧", "👨‍👦", "👨‍👦‍👦", "👨‍👧", "👨‍👧‍👦", "👨‍👧‍👧", "👩‍👦", "👩‍👦‍👦", "👩‍👧", "👩‍👧‍👦", "👩‍👧‍👧", "🗣", "👤", "👥",
    "🧳", "🌂", "☂️", "🧵", "🧶", "👓", "🕶", "🥽", "🥼", "🦺", "👔", "👕", "👖", "🧣", "🧤", "🧥", "🧦", "👗", "👘", "🥻", "🩱", "🩲", "🩳", "👙", "👚", "👛", "👜", "👝", "🎒", "👞", "👟", "🥾", "🥿", "👠", "👡", "🩰", "👢", "👑", "👒", "🎩", "🎓", "🧢", "💄", "💍", "💼"
    ];

   var emojis = "";
    for(var _k = 0; _k < emoji_array.length; _k++){
        emojis+=`<li class="emoticon">${emoji_array[_k]}</li>`;
    }

    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('8b0b4b0f1112e526c284', {
        cluster: 'ap2'
    });

    var socketId = null;
    pusher.connection.bind('connected', function() {
        socketId = pusher.connection.socket_id;
    });
    var channel = pusher.subscribe('netbhe');
    // channel.bind('my-event', function(data) {
    //   alert(JSON.stringify(data));
    // });

    //Typing
    //setup before functions
    var typingTimer;                //timer identifier
    var doneTypingInterval = 3000;  //time in ms (3 seconds)

    function nl2br (str, is_xhtml) {
        if (typeof str === 'undefined' || str === null) {
            return '';
        }
        var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
        return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
    }
    function getTimeDiff(datetime) {
        var datetime = new Date( datetime ).getTime();
        var Current = new Date().getTime();
        var CalcTime = Current - datetime;
        var Years = Math.floor(CalcTime / 1000 / 60 / 60 / 24 / 7 / 4 / 12);
        CalcTime -= Years * (1000 * 60 * 60 * 24 * 7 * 4 * 12);
        var Months = Math.floor(CalcTime / 1000 / 60 / 60 / 24 / 7 / 4);
        CalcTime -= Months * (1000 * 60 * 60 * 24 * 7 * 4);
        var Days = Math.floor(CalcTime / 1000 / 60 / 60 / 24);
        CalcTime -= Days * (1000 * 60 * 60 * 24);
        var Hours = Math.floor(CalcTime / 1000 / 60 / 60);
        CalcTime -= Hours * (1000 * 60 * 60);
        var Minutes = Math.floor(CalcTime / 1000 / 60);
        CalcTime -= Minutes * (1000 * 60);
        var Seconds = Math.floor(CalcTime / 1000 / 60);
        if(Years>1) {
            timeDiff = Years+" Years ago";
        }else if (Months>1) {
            timeDiff = Months+" Months ago";
        }else if (Days>1) {
            timeDiff = Days+" Days ago";
        }else if (Hours>1) {
            timeDiff = Hours+' Hours ago';
        }else if (Minutes>1) {
            timeDiff = Minutes+' Minutes ago';
        }else {
            timeDiff = Seconds+' Seconds ago';
        }
        return timeDiff;
    }
    function sendMsg1(id){
        var replymsg = "";
        var err = 0;
        var filecnt = 0;
        if($('.chat-msg-'+id).val()!="")
            replymsg = $.trim($('.chat-msg-'+id).val());
        var files = $('.file-'+id).prop('files');
        if(Object.keys(files).length>0){
            $('.typing-text-'+id).text("uploading...");
            $('.typing-text-chats').text("uploading...");
            $('.chat-bodys-'+id).css("height", "228px");
        }
        data = new FormData();
        data.append('_token', "{{ csrf_token() }}");
        data.append('booking_id',id);
        data.append('message',replymsg);
        data.append('socket_id',socketId);
        console.log(data);
        console.log("SEND MSG 1");
        var err = 0;
        var filecnt = 0;
        console.log("Stopped typing");
        startStopTypingAJAX('end');
        keyup = 0;
        $.each(files, function(k,file){
            var fileExt = file.name.split('.').pop();
            if(fileExt == "bat" || fileExt == "img" || fileExt == "exe") {
                alert("This file cannot be uploaded.");
                err = 1;
            } else {
                data.append('file', file);
            }
        });
        $('.file-append-'+id).empty();
        $('.file-append-chats').empty();
        if(err == 0){
            filecnt = Object.keys(files).length;
        }
        if (replymsg || filecnt>0) {
            $.ajax({
                url: '{{ route("send.ajax") }}',
                type: 'POST',
                dataType: 'JSON',
                data: data,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
            })
            .done(result => {
                $('.typing-text-'+id).text("");
                $('.typing-text-chats').text("");
                $('.chat-bodys-'+id).css("height", "242px");
                if (result.result) {
                    console.log(user_arr);
                    console.log(id);
                    index=user_arr.indexOf(id);
                    console.log(index);
                    message_master_id=result.result.message_master_id;
                    user_arr[index]=message_master_id;
                    type="I";
                    token=result.result.token;
                    $('.main-chat-box-'+id).remove();
                    project_title=result.result.project_title;
                    var style='';
                    if(result.result.online_status=='N'){
                        style='style="color:red"';
                    }
                        var curroute = "{{Route::is('chats')}}";
                        if(curroute!=1){
                            html=`<div class="main-chat-box main-chat-box-${message_master_id}" style="margin-right: ${(index) * 300}px">
                                <div class="chat-heads">
                                    <div class="chat-title">
                                    <input type="hidden" id="type-${message_master_id}" value="${type}">
                                        <span><b class="username-${message_master_id}">${result.result.username}</b></span>
                                    </div>
                                    <div class="chat-action-des" data-id="${message_master_id}">
                                        <a href="javascript:;" class="minimise-chat-box" data-id="${message_master_id}"><i aria-hidden="true" class="fa fa-minus"></i></a>
                                        <a href="javascript:;" class="chat-action" data-id="${message_master_id}"><i aria-hidden="true" class="fa fa-times"></i></a>
                                    </div>
                                </div>
                                <div class="buildme">
                                    <p>${token}<span class="id_text pull-right">${result.result.booking_id}</span></p>
                                </div>
                                <div class="chat-bodys chat-bodys-${message_master_id}"></div>
                                <div class="file-append file-append-${message_master_id}"></div>
                                <div class="typing-text typing-text-${message_master_id}"></div>
                                <form class="frm_msg" onsubmit="return sendMsg(${message_master_id})" data-id="${message_master_id}">
                                    <div class="rm_emoji_area rm_emoji_area-${message_master_id}"><ul>${emojis}</ul></div>
                                    <div class="chat-fots">
                                        <textarea class="type-msgs chat-msg-${message_master_id}" data-id="${message_master_id}" placeholder="Type your message.."></textarea>
                                        <div class="upload_box">
                                            <input type="file" id="file-${message_master_id}" data-id="${message_master_id}" class="file-upload-chat file-${message_master_id}">
                                            <label for="file-${message_master_id}" class="btn-2"></label>
                                        </div>
                                        <a href="#" class="rm_emoji" data-id="${message_master_id}">🙂</a>
                                        <p class="file-name-foots file-name-${message_master_id}"></p>
                                        <a href="#" class="chat-send" data-id="${message_master_id}"><i class="fa fa-paper-plane"></i></a>
                                    </div>
                                </form>
                            </div>`;
                            $('.cht-popup').append(html);
                            // user_arr.push(message_master_id);
                            sessionStorage['popup_arr'] = JSON.stringify(msgmaster_arr);
                        } else {
                            console.log('supposed to attach');
                            var subcat = "";
                            if(result.subcategory !== null || result.subcategory !== undefined){
                                subcat = ` > ${result.subcategory}`;
                            }
                            var chat_body_html=`<div class="px-1 py-1 chat-box bg-white chat-box-${message_master_id}"></div>`
                            var text_area_html=`<textarea class="hire-type chat-msg-${message_master_id}" data-id="${message_master_id}" placeholder="{{__('site.type_messages_here')}}" style="height: 112px;resize: none;"></textarea>
                                        <a href="#" class="rm_emoji new-top">🙂</a>
                                        <div class="mesg-btns">
                                            <div class="upload_box edit-upload">
                                                <input type="file" id="file-${message_master_id}" data-id="${message_master_id}" class="file-upload-chat file-${message_master_id}">
                                                <label for="file-${message_master_id}" class="btn-2">{{__('site.attach_file')}}</label>
                                            </div>
                                        <button type="submit" class="reply-btns" data-id="${message_master_id}"><img src="{{ URL::to('public/frontend/images/reply.png') }}" alt=""> {{__('site.reply')}}</button>
                                        </div>`;
                            var close_button = `<button type="button" class="close-chat pull-right m-0" data-id="${message_master_id}">End Chat</button>`;
                            // var emoji_html = `<div class="rm_emoji_area new-emoji-top" ><ul>${emojis}</ul></div>`;
                            $('#conv').find('.common-top').find('h4').text(`${result.result.username}`);
                            $('#conv').find('.common-top').find('p').text(`${result.result.category}`);
                            $('#conv').find('.common-top').find('#button_div').empty();
                            $('#conv').find('.common-top').find('#button_div').append(close_button);
                            $('#conv').find('.chat_chat_body').empty();
                            $('#conv').find('.chat_chat_body').append(chat_body_html);
                            $('#conv').find('.attachedd-type').empty();
                            $('#conv').find('.attachedd-type').append(text_area_html);
                            $('#conv').find('.attachedd-type').parent().find('.new-emoji-top').find('ul').empty();
                            $('#conv').find('.attachedd-type').parent().find('.new-emoji-top').find('ul').append(emojis);
                            $('#conv').find('.attachedd-type').find('#file').attr('id', `file-${message_master_id}`);
                            $('#conv').find('.attachedd-type').find('.btn-2').attr('for', `file-${message_master_id}`);
                        }
                        $.ajax({
                            url: '{{ route("user.message") }}',
                            type: 'POST',
                            dataType: 'JSON',
                            data: {
                                message_master_id:message_master_id,
                                type:type,
                                _token: '{{ csrf_token() }}',
                            }
                        })
                        .done(result => {
                        if (result.result) {
                            if(curroute!=1){
                                result.result.forEach(function(item, index){
                                    var imgg1 = "@if(@Auth::user()->profile_pic){{url('storage/app/public/uploads/profile_pic/').'/'.Auth::user()->profile_pic}}@else{{url('public/frontend/images/blank.png')}}@endif";
                                    var imgg2 = "{{url('public/frontend/images/blank.png')}}";
                                    if(item.get_user.profile_pic!=null){
                                        imgg2 = "{{url('storage/app/public/uploads/profile_pic/')}}"+item.get_user.profile_pic;
                                    }
                                    var file='';
                                    // if(item.get_user.image){
                                    //     imgg = "{{url('storage/app/public/userImage/')}}";
                                    //     imgg = imgg+'/'+item.get_user.image;
                                    // }
                                    if(item.file !== null){
                                        file = "{{url('storage/app/public/uploads/message_files/')}}";
                                        file = file+'/'+item.file;
                                        filename=item.file.substr(item.file.lastIndexOf("_")+1);
                                        if(item.user_id==my_id){
                                            if(item.message == null)
                                                attach_file=`<a href="${file}" class="msg-link-color" target=_blank download>${filename}</a>`;
                                            else
                                                attach_file=`<br><a href="${file}" class="msg-link-color" target=_blank download>${filename}</a>`;
                                        } else {
                                            if(item.message == null)
                                                attach_file=`<a href="${file}" target=_blank download>${filename}</a>`;
                                            else
                                                attach_file=`<br><a href="${file}" target=_blank download>${filename}</a>`;
                                        }
                                        // attach_file=`hi`;
                                    }else{
                                        attach_file=``;
                                    }

                                    var img_div1 = `<a class="avatar avatar-online"><img src="${imgg1}" alt=""><i></i></a>`;
                                    var img_div2 = `<a class="avatar avatar-online recive_img" href="javascript:;"><img src="${imgg2}" alt=""></a>`;
                                    var username_div = `<span class="username">${item.get_user.name}</span>`;
                                    if(item.user_id==my_id) {
                                        var msgg = `<div class="chat">
                                            <div class="chat-avatar">${img_div1}</div>
                                            <div class="chat-body">
                                                <div class="chat-content">
                                                    <p class="send_msgg">${nl2br(item.message)}${attach_file}</p>
                                                </div>
                                                <time class="chat-time">${getTimeDiff(item.created_at)}</time>
                                            </div>
                                        </div>`;
                                    } else {
                                        var msgg = `<div class="chat chat-left">
                                            <div class="chat-avatar">${img_div2}</div>
                                            <div class="chat-body">
                                                ${username_div}
                                                <div class="chat-content">
                                                    <p class="recive_msg">${nl2br(item.message)}${attach_file}</p>
                                                </div>
                                                <time class="chat-time">${getTimeDiff(item.created_at)}</time>
                                            </div>
                                        </div>`;
                                    }
                                    $('.chat-bodys-'+message_master_id).append(msgg);
                                });
                                $('.chat-bodys-'+message_master_id).scrollTop(1000000);
                            } else {
                                result.result.forEach(function(item, index){
                                    var imgg1 = "@if(@Auth::user()->profile_pic){{url('storage/app/public/uploads/profile_pic/').'/'.Auth::user()->profile_pic}}@else{{url('public/frontend/images/blank.png')}}@endif";
                                    var imgg2 = "{{url('public/frontend/images/blank.png')}}";
                                    if(item.get_user.profile_pic!=null){
                                        imgg2 = "{{url('storage/app/public/uploads/profile_pic/')}}"+item.get_user.profile_pic;
                                    }
                                    var file='';
                                    // if(item.get_user.image){
                                    //     imgg = "{{url('storage/app/public/userImage/')}}";
                                    //     imgg = imgg+'/'+item.get_user.image;
                                    // }
                                    if(item.file !== null) {
                                        file = "{{url('storage/app/public/uploads/message_files/')}}";
                                        file = file+'/'+item.file;
                                        filename=item.file.substr(item.file.lastIndexOf("_")+1);
                                        if(item.user_id==my_id) {
                                            if(item.message == null)
                                                attach_file=`<a href="${file}" class="msg-link-color" target=_blank download>${filename}</a>`;
                                            else
                                                attach_file=`<br><a href="${file}" class="msg-link-color" target=_blank download>${filename}</a>`;
                                        } else {
                                            if(item.message == null)
                                                attach_file=`<a href="${file}" target=_blank download>${filename}</a>`;
                                            else
                                                attach_file=`<br><a href="${file}" target=_blank download>${filename}</a>`;
                                        }
                                        // attach_file=`hi`;
                                    } else {
                                        attach_file=``;
                                    }
                                    var img_div1 = `<a class="avatar avatar-online"><img src="${imgg1}" alt=""><i></i></a>`;
                                    var img_div2 = `<a class="avatar avatar-online recive_img" href="javascript:;"><img src="${imgg2}" alt=""></a>`;
                                    var username_div = `<span class="username">${item.get_user.name}</span>`;
                                    if(item.user_id==my_id){
                                        var msgg = `<div class="media w-100 ml-auto mb-3 reciever-div">
                                                        <div class="media-body mr-3">
                                                            <div class="bg-styled rounded py-2 px-3 mb-1">
                                                                <p class="text-small mb-0 text-white">${nl2br(item.message)}${attach_file}</p>
                                                            </div>
                                                            <div class="w-100"></div>
                                                            <p class="small">${getTimeDiff(item.created_at)}</p>
                                                        </div>
                                                    <img src="${imgg1}" alt="" width="30" class="rounded-circle">
                                        </div>`;
                                    } else {
                                        var msgg = `<div class="media w-100 mb-3 sender-div"><img src="${imgg2}" alt="" width="32" class="rounded-circle">
                                                    <div class="media-body ml-3">
                                                        <h5>${item.get_user.name}</h5>
                                                        <div class="bg-light rounded py-2 px-3 mb-1">
                                                            <p class="text-small mb-0 text-muted">${nl2br(item.message)}${attach_file}</p>
                                                        </div>
                                                        <br>
                                                        <p class="small">${getTimeDiff(item.created_at)}</p>
                                                    </div>
                                                </div>`;
                                    }
                                    $('.chat-box-'+message_master_id).append(msgg);
                                });
                                // $('.all-conversation').find('.scrollbar-content').css('top', 0);
                                var indexxx = $('.all-conversation').find('.scrollbar-path-vertical').height();
                                indexxx -= (indexxx*30)/100;
                                $('.scrollbar-handle').animate({top: indexxx});
                                var content_height = (-1) * parseInt($('.all-conversation').find('.scrollbar-content').height() - $('.all-conversation').height());
                                $('.all-conversation').find('.scrollbar-content').animate({top: content_height});
                                console.log('Scrolled');
                            }
                        }
                        });
                    // return false;
                } else {
                    console.log(result);
                }

            });
        } else {
            alert('Please type some message or insert file.');
        }
        // alert(id);
        return false;
    }
    function sendMsg(id){
        var replymsg = "";
        var err = 0;
        var filecnt = 0;
        if($('.chat-msg-'+id).val()!="")
            replymsg = $.trim($('.chat-msg-'+id).val());
        var files = $('.file-'+id).prop('files');
        if(Object.keys(files).length>0){
            $('.typing-text-'+id).text("uploading...");
            $('.typing-text-chats').text("uploading...");
            $('.chat-bodys-'+id).css("height", "228px");
        }
        data = new FormData();
        data.append('_token', "{{ csrf_token() }}");
        data.append('message_master_id',id);
        data.append('message',replymsg);
        data.append('socket_id',socketId);
        console.log("SEND MSG");
        console.log("Stopped typing");
        startStopTypingAJAX('end');
        keyup = 0;
        $.each(files, function(k,file){
            var fileExt = file.name.split('.').pop();
            if(fileExt == "bat" || fileExt == "img" || fileExt == "exe") {
                alert("This file cannot be uploaded.");
                err = 1;
            } else {
                data.append('file', file);
            }
        });
        $('.file-append-'+id).empty();
        $('.file-append-chats').empty();
        if(err == 0){
            filecnt = Object.keys(files).length;
        }
        if (replymsg || filecnt>0) {
            $('.chat-msg-'+id).val("");
            $('.file-'+id).val('');
            $('.file-name-'+id).html('');
            $.ajax({
                url: '{{ route("send.ajax") }}',
                type: 'POST',
                dataType: 'JSON',
                data: data,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false
            })
            .done(result => {
                $('.typing-text-'+id).text("");
                $('.typing-text-chats').text("");
                $('.chat-bodys-'+id).css("height", "242px");
                if (result.result) {
                    var curroute = "{{Route::is('chats')}}";
                    // if(data.to_id[0]!=my_id){
                    if(result.result.file !== null){
                        file = "{{url('storage/app/public/uploads/message_files/')}}";
                        file = file+'/'+result.result.file;
                        filename=result.result.file.substr(result.result.file.lastIndexOf("_")+1)
                        if(replymsg == "")
                            attach_file=`<a href="${file}" class="msg-link-color" target=_blank download>${filename}</a>`
                        else
                            attach_file=`<br><a href="${file}" class="msg-link-color" target=_blank download>${filename}</a>`
                        // attach_file=`hi`;
                    } else {
                        attach_file=``;
                    }
                    if(curroute!=1){
                        var msgg2 = `<div class="chat">
                                        <div class="chat-avatar">
                                            <a class="avatar avatar-online">
                                            <img src="{{ @Auth::user()->profile_pic ? url('storage/app/public/uploads/profile_pic/'.@Auth::user()->profile_pic) : url('public/frontend/images/blank.png') }}" alt="">
                                            <i></i>
                                            </a>
                                        </div>
                                        <div class="chat-body">
                                            <div class="chat-content">
                                                <p class="send_msgg">${nl2br(replymsg)}${attach_file}</p>
                                            </div>
                                            <time class="chat-time">Just a second</time>
                                        </div>
                                    </div>`;
                        $('.chat-bodys-'+id).append(msgg2);
                        $('.chat-msg-'+id).val("");
                        $('.chat-bodys-'+id).scrollTop(1000000);
                        // return false;
                    } else {
                        var imgg1 = "{{ @Auth::user()->profile_pic ? url('storage/app/public/uploads/profile_pic/').'/'.@Auth::user()->profile_pic : url('public/frontend/images/blank.png') }}";
                        var msgg2 = `<div class="media w-100 ml-auto mb-3 reciever-div">
                                        <div class="media-body mr-3">
                                            <div class="bg-styled rounded py-2 px-3 mb-1">
                                                <p class="text-small mb-0 text-white">${nl2br(replymsg)}${attach_file}</p>
                                            </div>
                                            <div class="w-100"></div>
                                            <p class="small">Just a second</p>
                                        </div>
                                        <img src="${imgg1}" alt="" width="30" class="rounded-circle">
                        </div>`;
                        $('.chat-box-'+id).append(msgg2);
                        $('.chat-msg-'+id).val("");
                        $('.chat-box-'+id).scrollTop(1000000);
                        // $('.all-conversation').find('.scrollbar-content').css('top', 0);
                        var indexxx = $('.all-conversation').find('.scrollbar-path-vertical').height();
                        indexxx -= (indexxx*30)/100;
                        $('.scrollbar-handle').animate({top: indexxx});
                        var content_height = (-1) * parseInt($('.all-conversation').find('.scrollbar-content').height() - $('.all-conversation').height());
                        $('.all-conversation').find('.scrollbar-content').animate({top: content_height});
                    }
                } else {
                    console.log("sendID function return error.");
                    console.log(result);
                }
            });
        } else {
            alert('Please type some message or insert file.');
        }
        // alert(id);
        return false;
    }
    //user is "finished typing," do something
    function doneTyping () {
        console.log("Stopped typing");
        startStopTypingAJAX('end');
        keyup = 0;
    }

    function startStopTypingAJAX(typing) {
        message_master_id = $('.message_master_id').val();
        $.ajax({
            url: '{{ route("typing.ajax") }}',
            type: 'POST',
            dataType: 'JSON',
            data: {
                message_master_id:message_master_id,
                from: name,
                typing: typing,
                _token: '{{ csrf_token() }}',
                socket_id: socketId,
            }
        });
    }

    $(document).ready(function() {
        var booking_id_array = @if(@$bookings){!! json_encode(@$bookings->pluck('id') ?? []) !!}@else[]@endif;
        // var popup_arr = JSON.parse(sessionStorage['popup_arr']);
        console.log("MESSAGE MASTER ID: "+$('.message_master_id').val());
        var popup_arr = sessionStorage.getItem('popup_arr') ? JSON.parse(sessionStorage.getItem('popup_arr')) : [];
        msgmaster_arr = sessionStorage.getItem('popup_arr') ? JSON.parse(sessionStorage.getItem('popup_arr')) : [];
        console.log(popup_arr);
        var curroute = "{{Route::is('chats')}}";
        if(curroute!=1){
            $.each(popup_arr, function(iii, message_master_id) {
                user_arr.push(message_master_id);
                $.ajax({
                    url: '{{ route("user.message") }}',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        message_master_id:message_master_id,
                        _token: '{{ csrf_token() }}',
                    }
                })
                .done(result => {
                    if (result.result) {
                        console.log("user messages");
                        console.log(result);
                        var html=`<div class="main-chat-box main-chat-box-${message_master_id}"  style="margin-right: ${iii * 300}px">
                            <div class="chat-heads">
                                <input type="hidden" id="type-${message_master_id}">
                                    <span><b class="username-${message_master_id}">${result.username}</b></span>
                                <div class="chat-action-des"  data-id="${message_master_id}">
                                    <a href="javascript:;" class="minimise-chat-box" data-id="${message_master_id}"><i aria-hidden="true" class="fa fa-minus"></i></a>
                                    <a href="javascript:;" class="chat-action" data-id="${message_master_id}" style="margin-left: 5px;"><i aria-hidden="true" class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="buildme">
                                <p><a href="{{url('view-booking-details')}}/${result.booking_id}" target="_blank">${result.token}</a><span class="id_text pull-right">${result.booking_id}</span></p>
                            </div>
                            <div class="chat-bodys chat-bodys-${message_master_id}"></div>
                            <div class="file-append file-append-${message_master_id}"></div>
                            <div class="typing-text typing-text-${message_master_id}"></div>
                            <form class="frm_msg" onsubmit="return sendMsg(${message_master_id})" data-id="${message_master_id}">
                                <div class="rm_emoji_area rm_emoji_area-${message_master_id}"><ul>${emojis}</ul></div>
                                <div class="chat-fots">
                                    <textarea  class="type-msgs chat-msg-${message_master_id}" data-id="${message_master_id}" placeholder="Type your message.."></textarea>
                                    <div class="upload_box">
                                        <input type="file" id="file-${message_master_id}" data-id="${message_master_id}" class="file-upload-chat file-${message_master_id}">
                                        <label for="file-${message_master_id}" class="btn-2"></label>
                                    </div>
                                    <a href="#" class="rm_emoji" data-id="${message_master_id}">🙂</a>
                                    <p class="file-name-foots file-name-${message_master_id}"></p>
                                    <a href="#" class="chat-send" data-id="${message_master_id}"><i class="fa fa-paper-plane"></i></a>
                                </div>
                            </form>
                        </div>`;
                        $('.cht-popup').append(html);
                        result.result.forEach(function(item, index){
                            var imgg1 = "@if(@Auth::user()->profile_pic){{url('storage/app/public/uploads/profile_pic/').'/'.Auth::user()->profile_pic}}@else{{url('public/frontend/images/blank.png')}}@endif";
                            var imgg2 = "{{url('public/frontend/images/blank.png')}}";
                            if(item.get_user.profile_pic!=null){
                                imgg2 = "{{url('storage/app/public/uploads/profile_pic/')}}"+"/"+item.get_user.profile_pic;
                            }
                            var file='';
                            // if(item.get_user.image){
                            //     imgg = "{{url('storage/app/public/userImage/')}}";
                            //     imgg = imgg+'/'+item.get_user.image;
                            // }
                            if(item.file !== null){
                                file = "{{url('storage/app/public/uploads/message_files/')}}";
                                file = file+'/'+item.file;
                                filename=item.file.substr(item.file.lastIndexOf("_")+1);
                                if(item.user_id==my_id){
                                    if(item.message == null)
                                        attach_file=`<a href="${file}" class="msg-link-color" target=_blank download>${filename}</a>`;
                                    else
                                        attach_file=`<br><a href="${file}" class="msg-link-color" target=_blank download>${filename}</a>`;
                                }else{
                                    if(item.message == null)
                                        attach_file=`<a href="${file}" target=_blank download>${filename}</a>`;
                                    else
                                        attach_file=`<br><a href="${file}" target=_blank download>${filename}</a>`;
                                }
                                // attach_file=`hi`;
                            }else{
                                attach_file=``;
                            }
                            var img_div1 = `<a class="avatar avatar-online"><img src="${imgg1}" alt=""><i></i></a>`;
                            var img_div2 = `<a class="avatar avatar-online recive_img" href="javascript:;"><img src="${imgg2}" alt=""></a>`;
                            var username_div = `<span class="username">${item.get_user.name}</span>`;
                            if(item.user_id==my_id){
                                var msgg = `<div class="chat">
                                    <div class="chat-avatar">${img_div1}</div>
                                    <div class="chat-body">
                                        <div class="chat-content">
                                            <p class="send_msgg">${nl2br(item.message)}${attach_file}</p>
                                        </div>
                                        <time class="chat-time">${getTimeDiff(item.created_at)}</time>
                                    </div>
                                </div>`;
                            } else {
                                var msgg = `<div class="chat chat-left">
                                    <div class="chat-avatar">${img_div2}</div>
                                    <div class="chat-body">
                                        ${username_div}
                                        <div class="chat-content">
                                            <p class="recive_msg">${nl2br(item.message)}${attach_file}</p>
                                        </div>
                                        <time class="chat-time">${getTimeDiff(item.created_at)}</time>
                                    </div>
                                </div>`;
                            }
                            $('.chat-bodys-'+message_master_id).append(msgg);
                        });
                        $('.chat-bodys-'+message_master_id).scrollTop(1000000);
                    }
                });
            });
        }
        console.log("user_arr");
        console.log(user_arr);

        channel.bind('receive-event', function(data) {
            console.log("receive event triggered");
            newmsg = data.message;
            if(data.file !== null){
                file = "{{url('storage/app/public/uploads/message_files/')}}";
                file = file+'/'+data.file;
                filename=data.file.substr(data.file.lastIndexOf("_")+1);
                if(data.message==null)
                    attach_file=`<a href="${file}"  target=_blank download>${filename}</a>`;
                else
                    attach_file=`<br><a href="${file}"  target=_blank download>${filename}</a>`;
                // attach_file=`hi`;
            } else {
                attach_file=``;
            }
            var curroute = "{{Route::is('chats')}}";
            if(curroute!=1){
                var rmsgg = `<div class="media w-100 mb-3 sender-div">
                    <img src="${data.image}" alt="" width="32" class="rounded-circle">
                    <div class="media-body ml-3">
                        <h5>${data.from}</h5>
                        <div class="bg-light rounded py-2 px-3 mb-1">
                            <p class="text-small mb-0 text-muted">${data.message}${attach_file}</p>
                        </div>
                        <br>
                        <p class="small">Just a second</p>
                    </div>
                </div>`;

                if(my_id == data.to_id && data.message_master_id==$('.message_master_id').val()){
                    $('.chat-box').append(rmsgg);
                    $('.sp-viewport').scrollTop(1000000000);
                    // refershListing();
                }

                // if(jQuery.inArray(my_id, data.to_id) !== -1){
                if(my_id == data.to_id){
                    console.log("getting in");
                    console.log(data);
                    console.log(data.pusher_message_master_id);
                    if(!(jQuery.inArray(data.pusher_message_master_id, user_arr) !== -1)){
                        html=`<div class="main-chat-box main-chat-box-${data.pusher_message_master_id}" style="margin-right: ${user_arr.length * 300}px">
                            <div class="chat-heads">
                                <div class="chat-title">
                                <input type="hidden" id="type-${data.pusher_message_master_id}" >
                                    <span><b class="username-${data.pusher_message_master_id}">${data.from}</b></span>
                                </div>
                                <div class="chat-action-des"  data-id="${data.pusher_message_master_id}">
                                    <a href="javascript:;" class="minimise-chat-box" data-id="${data.pusher_message_master_id}"><i aria-hidden="true" class="fa fa-minus"></i></a>
                                    <a href="javascript:;" class="chat-action" data-id="${data.pusher_message_master_id}"><i aria-hidden="true" class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="buildme">
                                <p><a href="{{url('view-booked-details')}}/${data.booking_id}" target="_blank">${data.token}</a><span class="id_text pull-right">${data.booking_id}</span></p>
                            </div>
                            <div class="chat-bodys chat-bodys-${data.pusher_message_master_id}"></div>
                            <div class="file-append file-append-${data.pusher_message_master_id}"></div>
                            <div class="typing-text typing-text-${data.pusher_message_master_id}"></div>
                            <form class="frm_msg" onsubmit="return sendMsg(${data.pusher_message_master_id})" data-id="${data.pusher_message_master_id}">
                                <div class="rm_emoji_area rm_emoji_area-${data.pusher_message_master_id}"><ul>${emojis}</ul></div>
                                <div class="chat-fots">
                                    <textarea class="type-msgs chat-msg-${data.pusher_message_master_id}" data-id="${data.pusher_message_master_id}" placeholder="Type your message.."></textarea>
                                    <div class="upload_box">
                                        <input type="file" id="file-${data.pusher_message_master_id}" data-id="${data.pusher_message_master_id}" class="file-upload-chat file-${data.pusher_message_master_id}">
                                        <label for="file-${data.pusher_message_master_id}" class="btn-2"></label>
                                    </div>
                                    <a href="#" class="rm_emoji" data-id="${data.pusher_message_master_id}">🙂</a>
                                    <p class="file-name-foots file-name-${data.pusher_message_master_id}"></p>
                                    <a href="#" class="chat-send" data-id="${data.pusher_message_master_id}"><i class="fa fa-paper-plane"></i></a>
                                </div>
                            </form>
                        </div>`;
                        $('.cht-popup').append(html);
                        user_arr.push(data.pusher_message_master_id);
                        msgmaster_arr.push(data.pusher_message_master_id);
                        sessionStorage['popup_arr'] = JSON.stringify(msgmaster_arr);
                        $.ajax({
                            url: '{{ route("user.message") }}',
                            type: 'POST',
                            dataType: 'JSON',
                            data: {
                                message_master_id:data.pusher_message_master_id,
                                _token: '{{ csrf_token() }}',
                            }
                        })
                        .done(result => {
                            // $('.chat-bodys').html('');
                            console.log(result);
                            if (result.result) {
                                result.result.forEach(function(item, index){
                                    var imgg1 = "@if(@Auth::user()->profile_pic){{url('storage/app/public/uploads/profile_pic/').'/'.Auth::user()->profile_pic}}@else{{url('public/frontend/images/blank.png')}}@endif";
                                    var file='';
                                    // if(item.get_user.image){
                                    //     imgg = "{{url('storage/app/public/userImage/')}}";
                                    //     imgg = imgg+'/'+item.get_user.image;
                                    // }
                                    if(item.file !== null){
                                        file = "{{url('storage/app/public/uploads/message_files/')}}";
                                        file = file+'/'+item.file;
                                        filename=item.file.substr(item.file.lastIndexOf("_")+1);
                                        if(item.user_id==my_id){
                                            if(item.message == null)
                                                attach_file=`<a href="${file}" class="msg-link-color" target=_blank download>${filename}</a>`;
                                            else
                                                attach_file=`<br><a href="${file}" class="msg-link-color" target=_blank download>${filename}</a>`;
                                        } else {
                                            if(item.message == null)
                                                attach_file=`<a href="${file}" target=_blank download>${filename}</a>`;
                                            else
                                                attach_file=`<br><a href="${file}" target=_blank download>${filename}</a>`;
                                        }
                                        // attach_file=`hi`;
                                    } else {
                                        attach_file=``;
                                    }
                                    var img_div1 = `<a class="avatar avatar-online"><img src="${imgg1}" alt=""><i></i></a>`;
                                    var img_div2 = `<a class="avatar avatar-online recive_img" href="javascript:;"><img src="${data.image}" alt=""></a>`;
                                    var username_div = `<span class="username">${item.get_user.name}</span>`;
                                    if(item.user_id==my_id){
                                        var msgg = `<div class="chat">
                                            <div class="chat-avatar">${img_div1}</div>
                                            <div class="chat-body">
                                                <div class="chat-content">
                                                    <p class="send_msgg">${nl2br(item.message)}${attach_file}</p>
                                                </div>
                                                <time class="chat-time">${getTimeDiff(item.created_at)}</time>
                                            </div>
                                        </div>`;
                                    } else {
                                        var msgg = `<div class="chat chat-left">
                                            <div class="chat-avatar">${img_div2}</div>
                                            <div class="chat-body">
                                                ${username_div}
                                                <div class="chat-content">
                                                    <p class="recive_msg">${nl2br(item.message)}${attach_file}</p>
                                                </div>
                                                <time class="chat-time">${getTimeDiff(item.created_at)}</time>
                                            </div>
                                        </div>`;
                                    }
                                    $('.chat-bodys-'+data.pusher_message_master_id).append(msgg);
                                });
                            $('.chat-bodys-'+data.pusher_message_master_id).scrollTop(1000000);
                            }
                        });
                    } else {
                        var msgg = `<div class="chat chat-left">
                                    <div class="chat-avatar">
                                        <a class="avatar avatar-online recive_img" href="javascript:;">
                                            <img src="${data.image}" alt="">
                                        </a>
                                    </div>
                                    <div class="chat-body">
                                        <span class="username">${data.from}</span>
                                        <div class="chat-content">
                                            <p class="recive_msg">${nl2br(data.message)}${attach_file}</p>
                                        </div>
                                        <time class="chat-time">Just a second</time>
                                    </div>
                                </div>`;
                        console.log($('.chat-bodys-'+data.pusher_message_master_id)[0]);
                        $('.chat-bodys-'+data.pusher_message_master_id).append(msgg);
                        $('.chat-bodys-'+data.pusher_message_master_id).scrollTop(1000000);
                    }
                }
                // alert("no message found");
                // alert(data.to_name);
                // if((jQuery.inArray(my_id, data.to_id) !== -1)){
                // console.log(user_arr);
            } else {
                $(".chat_btn[data-bkid="+data.booking_id+"]").trigger( "click" );
                if(jQuery.inArray(data.booking_id, booking_id_array) !== -1){
                    booking_id_array.push(data.booking_id);
                    $(".chat_btn[data-bkid="+data.booking_id+"]").remove();
                    var holder_html = `<div class="chat-holders chat_btn" data-bkid="${data.booking_id}" data-msgid="${data.pusher_message_master_id}">
                        <span class="holder-image">
                            <img src="${data.image}" alt="" style="border-radius:50%;">
                            <img class="online-image" src="{{ URL::to('public/frontend/images/online.png') }}" alt="">
                        </span>
                        <h4>${data.from}</h4>
                        <h5>${data.booking_id}</h5>
                        <br>
                        <p>${data.token}</p>
                    </div>`;
                    if(my_id == data.to_id){
                        $('.all-chat-lists').find('.scrollbar-content').prepend(holder_html);
                    }
                    var text_html = `<div class="media w-100 mb-3 sender-div"><img src="${data.image}" alt="" width="32" class="rounded-circle">
                        <div class="media-body ml-3">
                            <h5>${data.from}</h5>
                            <div class="bg-light rounded py-2 px-3 mb-1">
                                <p class="text-small mb-0 text-muted">${nl2br(data.message)}${attach_file}</p>
                            </div>
                            <br>
                            <p class="small">Just a second</p>
                        </div>
                    </div>`;
                    $(".chat-box-"+data.pusher_message_master_id).append(text_html);
                } else {
                    var holder_html = `<div class="chat-holders chat_btn" data-bkid="${data.booking_id}" data-msgid="${data.pusher_message_master_id}">
                        <span class="holder-image">
                            <img src="${data.image}" alt="" style="border-radius:50%;">
                            <img class="online-image" src="{{ URL::to('public/frontend/images/online.png') }}" alt="">
                        </span>
                        <h4>${data.from}</h4>
                        <h5>${data.booking_id}</h5>
                        <br>
                        <p>${data.token}</p>
                    </div>`;
                    if(my_id == data.to_id){
                        $('.all-chat-lists').prepend(holder_html);
                    }
                }
                // $('.all-conversation').find('.scrollbar-content').css('top', 0);
                var indexxx = $('.all-conversation').find('.scrollbar-path-vertical').height();
                indexxx -= (indexxx*30)/100;
                $('.scrollbar-handle').animate({top: indexxx});
                var content_height = (-1) * parseInt($('.all-conversation').find('.scrollbar-content').height() - $('.all-conversation').height());
                $('.all-conversation').find('.scrollbar-content').animate({top: content_height});
            }
        });

        channel.bind('close-event', function(data) {
            var id = data.message_master_id;
            console.log($('.main-chat-box-'+id).find('.chat-heads').find('.chat-action-des').find('.chat-action')[0]);
            $('.main-chat-box-'+id).find('.chat-heads').find('.chat-action-des').find('.chat-action').trigger('click');
            var box = $('.chat-msg-'+id).parent();
            console.log(box);
            box.empty();
            var text_area_html=`<p class="closed-text">{{__('site.chat_closed')}}</p>
            <textarea class="hire-type chat-msg-${id}" data-id="${id}" placeholder="{{__('site.type_messages_here')}}" style="height: 112px;resize: none;" readonly></textarea>
                <a href="#" class="rm_emoji new-top disabled">🙂</a>
                <div class="mesg-btns">
                    <div class="upload_box edit-upload disabled">
                        <input type="file" id="file-${id}" data-id="${id}" class="file-upload-chat file-${id}" disabled>
                        <label for="file-${id}" class="btn-2 disabled">{{__('site.attach_file')}}</label>
                    </div>
                <button type="submit" class="reply-btns disabled" data-id="${id}" disabled><img src="{{ URL::to('public/frontend/images/reply.png') }}" alt=""> {{__('site.reply')}}</button>
            </div>`;
            box.append(text_area_html);
            var mediamatch = window.matchMedia("(min-width: 991px)");
            if(mediamatch.matches) $('.all-chat-lists').height('780px');
        });

        channel.bind('start-end-typing', function(data) {
            var curroute = "{{Route::is('chats')}}";
            if(curroute!=1){
                if(data.typing == 'start'){
                    $('.typing-text-'+data.message_master_id).text("typing...");
                    $('.chat-bodys-'+data.message_master_id).css("height", "228px");
                    $('.typing-text-chats').text("typing...");
                } else if(data.typing == 'end') {
                    $('.typing-text-'+data.message_master_id).text("");
                    $('.chat-bodys-'+data.message_master_id).css("height", "242px");
                    $('.typing-text-chats').text("");
                }
            } else {
                if($('#conv').find('textarea').data('id') == data.message_master_id){
                    if(data.typing == 'start'){
                        $('.typing-text-chats').text("typing...");
                    } else if(data.typing == 'end') {
                        $('.typing-text-chats').text("");
                    }
                }
            }
        });

        $("body").delegate(".rm_emoji", "click", function(e) {
            e.preventDefault();
            var curroute = "{{Route::is('chats')}}";
            if(curroute!=1){
                var iiiddd = $(this).data('id');
                $(".rm_emoji_area-"+iiiddd).slideToggle("slow");
            } else {
                $(".rm_emoji_area").slideToggle("slow");
            }
		});

        $("body").delegate(".emoticon", "click", function() {
            var emoticon = $(this).text();
            var textvalue = "";
            if($(this).parent().parent().hasClass('new-emoji-top')){
                textvalue = $(this).parent().parent().parent().find('.attachedd-type').find('textarea').val();
                $(this).parent().parent().parent().find('.attachedd-type').find('textarea').val(textvalue + emoticon);
            } else {
                textvalue = $(this).parent().parent().parent().find('.type-msgs').val();
                $(this).parent().parent().parent().find('.type-msgs').val(textvalue + emoticon);
            }
        });

        $("body").delegate(".chat-send", "click", function(e) {
            e.preventDefault();
            if($(this).data('type')=="P") {
                sendMsg1($(this).data('id'));
            } else {
                sendMsg($(this).data('id'));
            }
            $(this).val("");
        });

        $("body").delegate(".type-msgs", "keyup", function(e) {
            // Enter was pressed without shift key
            e.preventDefault();
            if(keyup == 0)
                keyup = 1;
            $('.message_master_id').val($(this).data('id'));
            console.log($('.message_master_id').val());
            if(e.key == 'Enter' && e.ctrlKey) {
                test=$(this).val()+"\r\n";
                $(this).val(test);
                e.preventDefault();
            } else if(e.key == 'Enter') {
                // console.log($(this).data('id'));
                e.preventDefault();
                if($(this).data('type')=="P") {
                    sendMsg1($(this).data('id'));
                } else {
                    sendMsg($(this).data('id'));
                }
                $(this).val("");
            }
            if(keyup == 1){
                // fire typing event
                startStopTypingAJAX('start');
                keyup = 2;
            }
            clearTimeout(typingTimer);
            if ($(this).val()) {
                typingTimer = setTimeout(doneTyping, doneTypingInterval);
            }
        });

        $("body").delegate(".hire-type", "keyup", function() {
            if(keyup == 0)
                keyup = 1;
            $('.message_master_id').val($(this).data('id'));
            console.log($('.message_master_id').val());
            if(keyup == 1){
                // fire typing event
                startStopTypingAJAX('start');
                keyup = 2;
            }
            clearTimeout(typingTimer);
            if ($(this).val()) {
                typingTimer = setTimeout(doneTyping, doneTypingInterval);
            }
        });

        $("body").delegate(".reply-btns", "click", function(e) {
            e.preventDefault();
            console.log("reply-btns");
            console.log($(this).data('id'));
            if($(this).data('id') == undefined){
                alert('Please select somebody for a chat.');
                return false;
            }
            if($(this).data('type')=="P") {
                sendMsg1($(this).data('id'));
            } else {
                sendMsg($(this).data('id'));
            }
        });

        $("body").delegate(".chat_btn", "click", function(e) {
            console.log("CHAT BUTTON HIT");
            var mediamatch = window.matchMedia("(max-width: 991px)");
            $(".rm_emoji").show();
            $('.typing-text-chats').text("");
            if($(this).hasClass('chat-holders') && mediamatch.matches){
                $(".mobile_filter").trigger('click');
            }
            var frrusername = $(this).data('username');
            if($(this).data('userid')){
                frrid = $(this).data('userid');
                frrusername= $(this).data('username');
            }
            var frrname = $(this).data('token');
            var booking_id = $(this).data('bkid');
            var type = 'I';
            var message_master_id = 0;
            $.ajax({
                url: '{{ route("get.message.master") }}',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    booking_id:booking_id,
                    _token: '{{ csrf_token() }}',
                }
            })
            .done(result => {
                var curroute = "{{Route::is('chats')}}";
                if (result.result) {
                    console.log('getting message master');
                    message_master_id=result.result.id;
                    console.log(user_arr);
                    if(curroute!=1){
                        console.log('before opening');
                        if(!(jQuery.inArray(message_master_id, user_arr) != -1)){
                            console.log('opening');
                            var style='';
                            var html=`<div class="main-chat-box main-chat-box-${message_master_id}"  style="margin-right: ${user_arr.length * 300}px">
                                <div class="chat-heads">
                                    <input type="hidden" id="type-${message_master_id}" value="${type}">
                                        <span><b class="username-${message_master_id}">${result.username}</b></span>
                                    <div class="chat-action-des"  data-id="${message_master_id}">
                                        <a href="javascript:;" class="minimise-chat-box" data-id="${message_master_id}"><i aria-hidden="true" class="fa fa-minus"></i></a>
                                        <a href="javascript:;" class="chat-action" data-id="${message_master_id}" style="margin-left: 5px;"><i aria-hidden="true" class="fa fa-times"></i></a>
                                    </div>
                                </div>
                                <div class="buildme">
                                    <p><a href="{{url('view-booking-details')}}/${booking_id}" target="_blank">${frrname}</a><span class="id_text pull-right">${booking_id}</span></p>
                                </div>
                                <div class="chat-bodys chat-bodys-${message_master_id}"></div>
                                <div class="file-append file-append-${message_master_id}"></div>
                                <div class="typing-text typing-text-${message_master_id}"></div>
                                <form class="frm_msg" onsubmit="return sendMsg(${message_master_id})" data-id="${message_master_id}">
                                    <div class="rm_emoji_area rm_emoji_area-${message_master_id}"><ul>${emojis}</ul></div>
                                    <div class="chat-fots">
                                        <textarea  class="type-msgs chat-msg-${message_master_id}" data-id="${message_master_id}" placeholder="Type your message.."></textarea>
                                        <div class="upload_box">
                                            <input type="file" id="file-${message_master_id}" data-id="${message_master_id}" class="file-upload-chat file-${message_master_id}">
                                            <label for="file-${message_master_id}" class="btn-2"></label>
                                        </div>
                                        <a href="#" class="rm_emoji" data-id="${message_master_id}">🙂</a>
                                        <p class="file-name-foots file-name-${message_master_id}"></p>
                                        <a href="#" class="chat-send" data-id="${message_master_id}"><i class="fa fa-paper-plane"></i></a>
                                    </div>
                                </form>
                            </div>`;
                            $('.cht-popup').append(html);
                            user_arr.push(message_master_id);
                            msgmaster_arr.push(message_master_id);
                            sessionStorage['popup_arr'] = JSON.stringify(msgmaster_arr);
                        }
                    } else {
                        console.log('message route in result');
                        $(this).find('.online-image').remove();
                        var subcat = "";
                        if(result.subcategory !== null || result.subcategory !== undefined){
                            subcat = ` > ${result.subcategory}`;
                        }
                        var chat_body_html=`<div class="px-1 py-1 chat-box bg-white chat-box-${message_master_id}"></div>`;
                        var text_area_html = "", close_button = "", emoji_html = "";
                        if(result.result.status == "CLOSED"){
                            text_area_html=`<p class="closed-text">{{__('site.chat_closed')}}</p>
                            <textarea class="hire-type chat-msg-${message_master_id}" data-id="${message_master_id}" placeholder="{{__('site.type_messages_here')}}" style="height: 112px;resize: none;" readonly></textarea>
                                <a href="#" class="rm_emoji new-top disabled">🙂</a>
                                    <div class="mesg-btns">
                                        <div class="upload_box edit-upload disabled">
                                            <input type="file" id="file-${message_master_id}" data-id="${message_master_id}" class="file-upload-chat file-${message_master_id}" disabled>
                                            <label for="file-${message_master_id}" class="btn-2 disabled">{{__('site.attach_file')}}</label>
                                        </div>
                                    <button type="submit" class="reply-btns disabled" data-id="${message_master_id}" disabled><img src="{{ URL::to('public/frontend/images/reply.png') }}" alt=""> {{__('site.reply')}}</button>
                            </div>`;
                            // emoji_html = `<div class="rm_emoji_area new-emoji-top" ><ul>${emojis}</ul></div>`;
                            var mediamatch = window.matchMedia("(min-width: 991px)");
                            if(mediamatch.matches) $('.all-chat-lists').height('780px');
                        } else {
                            text_area_html=`<textarea class="hire-type chat-msg-${message_master_id}" data-id="${message_master_id}" placeholder="{{__('site.type_messages_here')}}" style="height: 112px;resize: none;"></textarea>
                                    <a href="#" class="rm_emoji new-top">🙂</a>
                                    <div class="mesg-btns">
                                        <div class="upload_box edit-upload">
                                            <input type="file" id="file-${message_master_id}" data-id="${message_master_id}" class="file-upload-chat file-${message_master_id}">
                                            <label for="file-${message_master_id}" class="btn-2">{{__('site.attach_file')}}</label>
                                        </div>
                                    <button type="submit" class="reply-btns" data-id="${message_master_id}"><img src="{{ URL::to('public/frontend/images/reply.png') }}" alt=""> {{__('site.reply')}}</button>
                            </div>`;
                            close_button = `<button type="button" class="close-chat pull-right m-0" data-id="${message_master_id}">End Chat</button>`;
                            // emoji_html = `<div class="rm_emoji_area new-emoji-top" ><ul>${emojis}</ul></div>`;
                        }
                        $('#conv').find('.common-top').find('h4').text(`${result.username}`);
                        $('#conv').find('.common-top').find('p').text(`${result.category}`);
                        $('#conv').find('.common-top').find('#button_div').empty();
                        $('#conv').find('.common-top').find('#button_div').append(close_button);
                        $('#conv').find('.chat_chat_body').empty();
                        $('#conv').find('.chat_chat_body').append(chat_body_html);
                        $('#conv').find('.attachedd-type').empty();
                        $('#conv').find('.attachedd-type').append(text_area_html);
                        $('#conv').find('.attachedd-type').parent().find('.new-emoji-top').find('ul').empty();
                        $('#conv').find('.attachedd-type').parent().find('.new-emoji-top').find('ul').append(emojis);
                        $('#conv').find('.attachedd-type').find('#file').attr('id', `file-${message_master_id}`);
                        $('#conv').find('.attachedd-type').find('.btn-2').attr('for', `file-${message_master_id}`);
                        console.log("Changed");
                        user_arr.push(message_master_id);
                        // sessionStorage['popup_arr'] = JSON.stringify(user_arr);
                    }
                    $.ajax({
                        url: '{{ route("user.message") }}',
                        type: 'POST',
                        dataType: 'JSON',
                        data: {
                            message_master_id:message_master_id,
                            type:type,
                            _token: '{{ csrf_token() }}',
                        }
                    })
                    .done(result => {
                        if (result.result) {
                            console.log("user messages");
                            console.log(result);
                            if(curroute!=1){
                                result.result.forEach(function(item, index){
                                    var imgg1 = "@if(@Auth::user()->profile_pic){{url('storage/app/public/uploads/profile_pic/').'/'.Auth::user()->profile_pic}}@else{{url('public/frontend/images/blank.png')}}@endif";
                                    var imgg2 = "{{url('public/frontend/images/blank.png')}}";
                                    if(item.get_user.profile_pic!=null){
                                        imgg2 = "{{url('storage/app/public/uploads/profile_pic/')}}"+"/"+item.get_user.profile_pic;
                                    }
                                    var file='';
                                    // if(item.get_user.image){
                                    //     imgg = "{{url('storage/app/public/userImage/')}}";
                                    //     imgg = imgg+'/'+item.get_user.image;
                                    // }
                                    console.log(item.message);
                                    if(item.file !== null){
                                        file = "{{url('storage/app/public/uploads/message_files/')}}";
                                        file = file+'/'+item.file;
                                        filename=item.file.substr(item.file.lastIndexOf("_")+1);
                                        if(item.user_id==my_id){
                                            if(item.message == null)
                                                attach_file=`<a href="${file}" class="msg-link-color" target=_blank download>${filename}</a>`;
                                            else
                                                attach_file=`<br><a href="${file}" class="msg-link-color" target=_blank download>${filename}</a>`;
                                        }else{
                                            if(item.message == null)
                                                attach_file=`<a href="${file}" target=_blank download>${filename}</a>`;
                                            else
                                                attach_file=`<br><a href="${file}" target=_blank download>${filename}</a>`;
                                        }
                                        // attach_file=`hi`;
                                    }else{
                                        attach_file=``;
                                    }
                                    var img_div1 = `<a class="avatar avatar-online"><img src="${imgg1}" alt=""><i></i></a>`;
                                    var img_div2 = `<a class="avatar avatar-online recive_img" href="javascript:;"><img src="${imgg2}" alt=""></a>`;
                                    var username_div = `<span class="username">${item.get_user.name}</span>`;
                                    if(item.user_id==my_id){
                                        var msgg = `<div class="chat">
                                            <div class="chat-avatar">${img_div1}</div>
                                            <div class="chat-body">
                                                <div class="chat-content">
                                                    <p class="send_msgg">${nl2br(item.message)}${attach_file}</p>
                                                </div>
                                                <time class="chat-time">${getTimeDiff(item.created_at)}</time>
                                            </div>
                                        </div>`;
                                    } else {
                                        var msgg = `<div class="chat chat-left">
                                            <div class="chat-avatar">${img_div2}</div>
                                            <div class="chat-body">
                                                ${username_div}
                                                <div class="chat-content">
                                                    <p class="recive_msg">${nl2br(item.message)}${attach_file}</p>
                                                </div>
                                                <time class="chat-time">${getTimeDiff(item.created_at)}</time>
                                            </div>
                                        </div>`;
                                    }
                                    $('.chat-bodys-'+message_master_id).append(msgg);
                                });
                                $('.chat-bodys-'+message_master_id).scrollTop(1000000);
                            } else {
                                result.result.forEach(function(item, index){
                                    var imgg1 = "@if(@Auth::user()->profile_pic){{url('storage/app/public/uploads/profile_pic/').'/'.Auth::user()->profile_pic}}@else{{url('public/frontend/images/blank.png')}}@endif";
                                    var imgg2 = "{{url('public/frontend/images/blank.png')}}";
                                    if(item.get_user.profile_pic!=null){
                                        imgg2 = "{{url('storage/app/public/uploads/profile_pic/')}}"+"/"+item.get_user.profile_pic;
                                    }
                                    var file='';
                                    // if(item.get_user.image){
                                    //     imgg = "{{url('storage/app/public/userImage/')}}";
                                    //     imgg = imgg+'/'+item.get_user.image;
                                    // }
                                    if(item.file !== null) {
                                        file = "{{url('storage/app/public/uploads/message_files/')}}";
                                        file = file+'/'+item.file;
                                        filename=item.file.substr(item.file.lastIndexOf("_")+1);
                                        if(item.user_id==my_id) {
                                            if(item.message == null)
                                                attach_file=`<a href="${file}" class="msg-link-color" target=_blank download>${filename}</a>`;
                                            else
                                                attach_file=`<br><a href="${file}" class="msg-link-color" target=_blank download>${filename}</a>`;
                                        } else {
                                            if(item.message == null)
                                                attach_file=`<a href="${file}" target=_blank download>${filename}</a>`;
                                            else
                                                attach_file=`<br><a href="${file}" target=_blank download>${filename}</a>`;
                                        }
                                        // attach_file=`hi`;
                                    } else {
                                        attach_file=``;
                                    }
                                    var img_div1 = `<a class="avatar avatar-online"><img src="${imgg1}" alt=""><i></i></a>`;
                                    var img_div2 = `<a class="avatar avatar-online recive_img" href="javascript:;"><img src="${imgg2}" alt=""></a>`;
                                    var username_div = `<span class="username">${item.get_user.name}</span>`;
                                    if(item.user_id==my_id){
                                        var msgg = `<div class="media w-100 ml-auto mb-3 reciever-div">
                                                        <div class="media-body mr-3">
                                                            <div class="bg-styled rounded py-2 px-3 mb-1">
                                                                <p class="text-small mb-0 text-white">${nl2br(item.message)}${attach_file}</p>
                                                            </div>
                                                            <div class="w-100"></div>
                                                            <p class="small">${getTimeDiff(item.created_at)}</p>
                                                        </div>
                                                    <img src="${imgg1}" alt="" width="30" class="rounded-circle">
                                        </div>`;
                                    } else {
                                        var msgg = `<div class="media w-100 mb-3 sender-div"><img src="${imgg2}" alt="" width="32" class="rounded-circle">
                                                    <div class="media-body ml-3">
                                                        <h5>${item.get_user.name}</h5>
                                                        <div class="bg-light rounded py-2 px-3 mb-1">
                                                            <p class="text-small mb-0 text-muted">${nl2br(item.message)}${attach_file}</p>
                                                        </div>
                                                        <br>
                                                        <p class="small">${getTimeDiff(item.created_at)}</p>
                                                    </div>
                                        </div>`;
                                    }
                                    $('.chat-box-'+message_master_id).append(msgg);
                                });
                                $('.all-conversation').find('.scrollbar-content').css('top', 0);
                                var indexxx = $('.all-conversation').find('.scrollbar-path-vertical').height();
                                indexxx -= (indexxx*30)/100;
                                $('.scrollbar-handle').animate({top: indexxx});
                                var content_height = (-1) * parseInt($('.all-conversation').find('.scrollbar-content').height() - $('.all-conversation').height());
                                $('.all-conversation').find('.scrollbar-content').animate({top: content_height});
                            }
                        }
                    });
                } else {
                    if(curroute!=1){
                        if(!(jQuery.inArray(booking_id, user_arr) !== -1)){
                            html=`<div class="main-chat-box main-chat-box-${booking_id}"  style="margin-right: ${user_arr.length * 300}px">
                                <div class="chat-heads">
                                    <div class="chat-title">
                                    <input type="hidden" id="type-${booking_id}" value="${type}">
                                        <span><b class="username-${booking_id}">${frrusername}</b></span>
                                    </div>
                                    <input type="hidden" class="frrid${booking_id}" value="${frrid}">
                                    <div class="chat-action-des"  data-id="${booking_id}">
                                        <a href="javascript:;" class="minimise-chat-box" data-id="${booking_id}"><i aria-hidden="true" class="fa fa-minus"></i></a>
                                        <a href="javascript:;"  class="chat-action" data-id="${booking_id}"><i aria-hidden="true" class="fa fa-times"></i></a>
                                    </div>
                                </div>
                                <div class="buildme">
                                    <p><a href="{{url('view-booking-details')}}/${booking_id}" target="_blank">${frrname}</a><span class="id_text pull-right">${booking_id}</span></p>
                                </div>
                                <div class="chat-bodys chat-bodys-${booking_id}"></div>
                                <div class="file-append file-append-${booking_id}"></div>
                                <div class="typing-text typing-text-${booking_id}"></div>
                                <form class="frm_msg" onsubmit="return sendMsg1(${booking_id})" data-id="${booking_id}">
                                    <div class="rm_emoji_area rm_emoji_area-${booking_id}"><ul>${emojis}</ul></div>
                                    <div class="chat-fots">
                                        <textarea class="type-msgs chat-msg-${booking_id}" data-id="${booking_id}" data-type="P" placeholder="Type your message.."></textarea>
                                        <div class="upload_box">
                                            <input type="file" id="file-${booking_id}" data-id="${booking_id}" class="file-upload-chat file-${booking_id}">
                                            <label for="file-${booking_id}" class="btn-2"></label>
                                        </div>
                                        <a href="#" class="rm_emoji" data-id="${booking_id}">🙂</a>
                                        <p class="file-name-foots file-name-${booking_id}"></p>
                                        <a href="#" class="chat-send" data-id="${booking_id}" data-type="P"><i class="fa fa-paper-plane"></i></a>
                                    </div>
                                </form>
                            </div>`;
                            $('.cht-popup').append(html);
                            user_arr.push(Number(booking_id));
                            // sessionStorage['popup_arr'] = JSON.stringify(user_arr);
                        }
                    } else {
                        $(this).find('.online-image').remove();
                        var subcat = "";
                        if(result.subcategory !== null || result.subcategory !== undefined){
                            subcat = ` > ${result.subcategory}`;
                        }
                        var chat_body_html=`<div class="px-1 py-1 chat-box bg-white chat-box-${booking_id}"></div>`
                        var text_area_html=`<textarea class="hire-type chat-msg-${booking_id}" data-id="${booking_id}" placeholder="{{__('site.type_messages_here')}}" style="height: 112px;resize: none;"></textarea>
                                    <a href="#" class="rm_emoji new-top">🙂</a>
                                    <div class="mesg-btns">
                                        <div class="upload_box edit-upload">
                                            <input type="file" id="file-${booking_id}" data-id="${booking_id}" class="file-upload-chat file-${booking_id}">
                                            <label for="file-${booking_id}" class="btn-2">{{__('site.attach_file')}}</label>
                                        </div>
                                    <button type="submit" class="reply-btns" data-id="${booking_id}" data-type="P"><img src="{{ URL::to('public/frontend/images/reply.png') }}" alt=""> {{__('site.reply')}}</button>
                                    </div>`;
                        var close_button = `<button type="button" class="close-chat pull-right m-0" data-bkid="${booking_id}">End Chat</button>`;
                        // var emoji_html = `<div class="rm_emoji_area new-emoji-top" ><ul>${emojis}</ul></div>`;
                        $('#conv').find('.common-top').find('h4').text(`${result.username}`);
                        $('#conv').find('.common-top').find('p').text(`${result.category}`);
                        $('#conv').find('.common-top').find('#button_div').empty();
                        $('#conv').find('.common-top').find('#button_div').append(close_button);
                        $('#conv').find('.chat_chat_body').empty();
                        $('#conv').find('.chat_chat_body').append(chat_body_html);
                        $('#conv').find('.attachedd-type').empty();
                        $('#conv').find('.attachedd-type').append(text_area_html);
                        $('#conv').find('.attachedd-type').parent().find('.new-emoji-top').find('ul').empty();
                        $('#conv').find('.attachedd-type').parent().find('.new-emoji-top').find('ul').append(emojis);
                        $('#conv').find('.attachedd-type').find('#file').attr('id', `file-${booking_id}`);
                        $('#conv').find('.attachedd-type').find('.btn-2').attr('for', `file-${booking_id}`);
                        user_arr.push(Number(booking_id));
                        // sessionStorage['popup_arr'] = JSON.stringify(user_arr);
                    }
                }
            });
        });

        $("body").delegate(".close-chat", "click", function(e){
            var id = $(this).data('id');
            $('.typing-text-chats').text("");
            if(id == undefined){
                alert('You can\'t close a chat before starting.');
                return false;
            } else {
                $.ajax({
                    url: '{{ route("close.chat") }}',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        id:id,
                        _token: '{{ csrf_token() }}',
                    }
                })
                .done(result => {
                    if(result.status == 'success'){
                        console.log("Chat Closed");
                        var box = $('chat-msg-'+id).parent();
                        $('chat-msg-'+id).attr('readonly', true);
                        box.prepend(`<p class="closed-text">{{__('site.chat_closed')}}</p>`);
                        $('#conv').find('.common-top').find('#button_div').empty();
                        box.find('.rm_emoji').addClass('disabled');
                        box.find('.reply-btns').addClass('disabled');
                        box.find('.reply-btns').attr('disabled', true);
                        box.find('.upload-box').addClass('disabled');
                        box.find('.upload-box').find('input').attr('disabled', true);
                        box.find('.upload-box').find('.btn-2').addClass('disabled');
                        var mediamatch = window.matchMedia("(min-width: 991px)");
                        if(mediamatch.matches) $('.all-chat-lists').height('780px');
                    }
                });
            }
        });

        $('body').on('click','.chat-action',function(){
            id=$(this).data('id');
            index=user_arr.indexOf(id);
            if(index>-1){
                if(index!=(user_arr.length-1)){
                    for (let i = index+1; i < user_arr.length; i++) {
                        var elem = user_arr[i];
                        $('.main-chat-box-'+elem).css("margin-right", ((i-1)*300)+"px");

                    }
                }
                user_arr.splice(index, 1);
            }
            $('.main-chat-box-'+id).remove();

            // update local storage variable
            indexx=msgmaster_arr.indexOf(id);
            if(indexx>-1){
                if(indexx!=(msgmaster_arr.length-1)){
                    for (let i = indexx+1; i < msgmaster_arr.length; i++) {
                        var elem = msgmaster_arr[i];
                        $('.main-chat-box-'+elem).css("margin-right", ((i-1)*300)+"px");

                    }
                }
                msgmaster_arr.splice(indexx, 1);
            }
            sessionStorage['popup_arr'] = JSON.stringify(msgmaster_arr);
        });

        $('body').on('click','.minimise-chat-box',function(){
            id=$(this).data('id');
            if($('.main-chat-box-'+id).css("margin-bottom")==-322+"px")
            $('.main-chat-box-'+id).css("margin-bottom",0+"px");
            else
            $('.main-chat-box-'+id).css("margin-bottom",-322+"px");
        });

        $('body').on('change','.file-upload-chat',function(){
            console.log($(this)[0]);
            var files = $(this).prop('files');
            filecnt = Object.keys(files).length;
            if(filecnt>0){
                console.log('file appending');
                var id = $(this).data('id');
                var curroute = "{{Route::is('chats')}}";
                if(curroute!=1)
                    var html = `<div class="file_display">${$(this)[0].files[0].name}<i aria-hidden="true" class="fa fa-times rem_file"></i></div>`;
                else
                    var html = `<div class="file_display" style="bottom:10px !important; left:30px !important">${$(this)[0].files[0].name}<i aria-hidden="true" class="fa fa-times rem_file"></i></div>`;
                $('.file-append-'+id).append(html);
                $('.file-append-chats').append(html);
            }
        });

        $('body').on('click','.rem_file',function(){
            console.log("Removed file");
            var curroute = "{{Route::is('chats')}}";
            if(curroute!=1)
                var id = $(this).parent().parent().parent().find('form').data('id');
            else
                var id = $('#conv').find('textarea').data('id');
            $('.file-'+id).val('');
            $('.file-append-'+id).empty();
            $('.file-append-chats').empty();
            console.log($('#file-'+id).val());
        });

        $('.logout').click(function(e){
            e.preventDefault();
            sessionStorage.removeItem('popup_arr');
            window.location = "{{route('logout')}}";
        });
    });
</script>
{{-- Pusher Scripts End --}}
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    ga('create', 'G-4X2GG30GN8', 'auto');
    ga('send', 'pageview');

</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-4X2GG30GN8"></script>
<script>
    window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'G-4X2GG30GN8');
</script>
<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '516188839506759');
    fbq('track', 'PageView');
  </script>
  <noscript>
    <img height="1" width="1" style="display:none"
         src="https://www.facebook.com/tr?id=516188839506759&ev=PageView&noscript=1"/>
  </noscript>
  <!-- End Facebook Pixel Code -->

<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '871542530070577');
    fbq('track', 'PageView');
  </script>
  <noscript>
    <img height="1" width="1" style="display:none"
         src="https://www.facebook.com/tr?id=871542530070577&ev=PageView&noscript=1"/>
  </noscript>
  <!-- End Facebook Pixel Code -->
<!-- Meta Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1115408532341519');
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=1115408532341519&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Meta Pixel Code -->


<!-- Facebook Pixel Code old-->
{{-- <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '871542530070577');
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=871542530070577&ev=PageView&noscript=1" /></noscript> --}}
<!-- End Facebook Pixel Code -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-185027894-1">
</script>
<script>
    window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-185027894-1');
</script>
<!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-KQCVFDD');</script>
<!-- End Google Tag Manager -->
