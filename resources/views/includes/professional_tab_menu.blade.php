<div class="like-tab">
	<ul>
    	<li><a @if(Request::segment(1)=="professional-form" || Request::segment(1)=="professional-view-form" || Request::segment(1)=="professional-form-question")class="active"@endif href="{{ route('professional-form.index') }}">@lang('site.form_tools')</a></li>
        <li><a @if(Request::segment(1)=="professional-importing-tools" || Request::segment(1)=="professional-importing-tools-view" || Request::segment(1)=="user-view-form-submit-details" || Request::segment(1)=="prof-importing-tools-view-details" || Request::segment(1)=="prof-view-alluser-feedback" )class="active"@endif href="{{ route('professional-importing-tools.index') }}">@lang('site.imported_tools')</a></li>       
        <li>
        	<a @if(Request::segment(1)=="professional-360evaluation" || Request::segment(1)=="prof-360asign-view" )class="active"@endif href="{{ route('professional-360evaluation.index') }}">@lang('site.360tools')</a>
        </li>

        <li>
        	<a @if(Request::segment(1)=="prof-smartgoal-tools" || Request::segment(1)=="prof-smartgoal-user-assigne-view" )class="active"@endif href="{{ route('prof-smartgoal-tools.index') }}">@lang('site.smart_goals')</a>
        </li>

        <li>
            <a @if(Request::segment(1)=="user-content-temp")class="active"@endif href="{{ route('user-content-temp.index') }}">@lang('site.content_template')</a>
        </li>

        <li>
            <a @if(Request::segment(1)=="prof-contract-temp")class="active"@endif href="{{ route('prof-contract-temp.index') }}">@lang('site.contract_template')</a>
        </li>

        <li>
            <a @if(Request::segment(1)=="prof-log-book" || Request::segment(1)=="prof-logbook-assigne-list")class="active"@endif href="{{ route('prof-log-book.index') }}">@lang('site.logbook')</a>
        </li>

        <!-- <li>
            <a @if(Request::segment(1)=="prof-evaluation360")class="active"@endif href="{{ route('prof-evaluation360.create') }}">360º evaluation</a>
        </li> -->

        <li>
            <a @if(Request::segment(1)=="user-evaluation360-assign-userlist")class="active"@endif href="{{ route('user.evaluation360.assign.userlist') }}">@lang('site.360evaluation')</a>
        </li>

    </ul>
</div>