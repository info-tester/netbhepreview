<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="Description" content="Enter your description here"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
<link rel="stylesheet" href="assets/css/style.css">
<title>Title</title>
<link href="https://vjs.zencdn.net/7.10.2/video-js.css" rel="stylesheet" />
<style>
  body{
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  #jsplayer_container{
    width: 640px;
    height: auto;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    -webkit-transform: translate(-50%, -50%);
    -moz-transform: translate(-50%, -50%);
    -o-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
  }
</style>

  <!-- If you'd like to support IE8 (for Video.js versions prior to v7) -->
  <script src="https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
</head>
<body>
    <div id="jsplayer_container" style="margin-top: 200px;">
      <form action="{{route('imagick.save')}}" enctype="multipart/form-data" method="post">
        @csrf
        <input type="file" name="pdf_upload" id="pdf_upload">
        <input type="submit" value="Submit">
      </form>
    </div>
    <div id="images row">
      @if(count(@$images)>0)
      @dump($images)
        @foreach($images as $k=>$image)
          <div class="col-md-3">
            <img src="{{$image}}" height="100%">
          </div>
        @endforeach
      @endif
    </div>
  
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>
</script>
<script>
    // $('#pdf_upload').on('change', function(){
        
    // });
</script>
</body>
</html>