<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="Description" content="Enter your description here"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
<title>Title</title>
</head>
<body>
    <audio id="track" src="{{URL::asset('public/frontend/audio.mp3')}}" controls>
        <p>Your browser does not support the audio element</p>
    </audio>
    <br>
    <span id="tracktime">0 / 0</span>
    <!-- <button onclick="document.getElementById('track').play();">Play</button> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.1/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#track')[0].currentTime = localStorage.getItem('audioLastPoint');
        });
        var el_down = 
        document.getElementById("tracktime");
        // gfg_Run();
        // function gfg_Run() {
        //     var iOS = 
        //         /iPad|iPhone|iPod/.test(navigator.userAgent) &&
        //         !window.MSStream;
        //     el_down.innerHTML = iOS;
        // }
        $('#track').bind('timeupdate', function(){
            $('#tracktime').html(Math.floor(this.currentTime) + ' / ' + Math.floor(this.duration));
            localStorage.audioLastPoint = Math.floor(this.currentTime);
        });
        // var timer = 0;
        // var playing = false;
        // $('#track').bind('play', function() {
        //     playing = true;
        //     console.log(playing);
        //     yourCheck();
        // }).bind('pause', function() {
        //     playing = false;
        //     localStorage.audioLastPoint = Math.floor($('#track')[0].currentTime);
        //     clearInterval(timer);
        // }).bind('ended', function() {
        //     playing = false;
        //     localStorage.audioLastPoint = Math.floor($('#track')[0].currentTime);
        //     clearInterval(timer);
        // })

        // function yourCheck() {
        //     if (playing) {
        //         console.log("Updating");
        //         localStorage.audioLastPoint = Math.floor($('#track')[0].currentTime);
        //     } else {
        //         return;
        //     }
        //     timer = setTimeout( yourCheck(), 1000);
        // }
    </script>
</body>
</html>