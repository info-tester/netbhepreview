@if ($paginator->hasPages())
    <div class="pagination">
        @if ($paginator->onFirstPage())
            <a href="javascript:void(0);"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> @lang('client_site.previous')</a>
        @else
            <a href="{{ $paginator->previousPageUrl() }}"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> @lang('client_site.previous')</a>
        @endif
        @foreach ($elements as $element)
            @if (is_string($element))
                <a href="javascript:void(0);">{{ $element }}</a>
            @endif
            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <a href="javascript:void(0);" class="active">{{ $page }}</a>
                    @else
                        <a href="{{ $url }}" >{{ $page }}</a>
                    @endif
                @endforeach
            @endif
        @endforeach
        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <a href="{{ $paginator->nextPageUrl() }}">@lang('client_site.next') <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
        @else
            <a href="javascript:void(0);">@lang('client_site.next') <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
        @endif
    </div>
@endif
