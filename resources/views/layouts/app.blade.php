@if(@Auth::guard('web')->user()->timezone_id)
  @php
    date_default_timezone_set(@Auth::guard('web')->user()->userTimeZone->timezone);
  @endphp
@endif
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="google-site-verification" content="-Ny8r7ZHvubPko1njYw7W3vVHdnJyquGjaMwE_8HhTM">
    <meta name="facebook-domain-verification" content="nze7gqsxzv1egkngvo91ph20fhb1gu" />
    @if(Request::segment(1)!="video-call")
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @endif
        <script type='text/javascript'
            src='https://platform-api.sharethis.com/js/sharethis.js#property=5f86e8ab530ee50014b84819&product=sop'
            async='async'></script>
    @php
        $mTitle = '';
        $mDescription = '';
        $mKeyword = '';
        if(Request::segment(1) == "central-de-ajuda" && Request::segment(2) !== null){

        } else {
            switch (Request::segment(1)) {
                case '':
                    $m = App\Models\MetaTag::find(1);
                    $mTitle = $m->title;
                    $mDescription = $m->description;
                    $mKeyword = $m->keyword;
                    break;
                case 'expert-landing':
                    $m = App\Models\MetaTag::find(2);
                    $mTitle = $m->title;
                    $mDescription = $m->description;
                    $mKeyword = $m->keyword;
                    break;
                case 'consultant':
                    $m = App\Models\MetaTag::find(3);
                    $mTitle = $m->title;
                    $mDescription = $m->description;
                    $mKeyword = $m->keyword;
                    break;
                case 'public-profile':
                    $m = App\Models\MetaTag::find(4);
                    $mTitle = @$user->nick_name ? @$user->nick_name : @$user->name .'-'.@$user->userDetails[0]->categoryName->name;
                    $mDescription = substr( strip_tags(@$user->description),0,100);
                    $mKeyword = $m->keyword;
                    break;
                case 'blog':
                    $m = App\Models\MetaTag::find(5);
                    $mTitle = $m->title;
                    $mDescription = $m->description;
                    $mKeyword = $m->keyword;
                    break;
                case 'sobre-nós':
                    $m = App\Models\MetaTag::find(6);
                    $mTitle = $m->title;
                    $mDescription = $m->description;
                    $mKeyword = $m->keyword;
                    break;
                case 'termos-e-políticas':
                    $m = App\Models\MetaTag::find(7);
                    $mTitle = $m->title;
                    $mDescription = $m->description;
                    $mKeyword = $m->keyword;
                    break;
                case 'privacy-policy':
                    $m = App\Models\MetaTag::find(8);
                    $mTitle = $m->title;
                    $mDescription = $m->description;
                    $mKeyword = $m->keyword;
                    break;
                case 'faq':
                    $m = App\Models\MetaTag::find(9);
                    $mTitle = $m->title;
                    $mDescription = $m->description;
                    $mKeyword = $m->keyword;
                    break;
                case 'central-de-ajuda':
                    $m = App\Models\MetaTag::find(10);
                    $mTitle = $m->title;
                    $mDescription = $m->description;
                    $mKeyword = $m->keyword;
                    break;
                case 'blog-details':
                    $mTitle = $blog->meta_title;
                    $mDescription = $blog->meta_description;
                    $mKeyword = $blog->meta_keyword;
                    break;
                case 'product-details':
                    $mTitle = @$product->title.'-details';
                    $mDescription = substr( strip_tags(@$product->description),0,100);
                    // $mKeyword = $blog->meta_keyword;
                    break;
                case 'categoria-landing-page':
                    $mTitle = @$category->meta_title;
                    $mDescription = strip_tags(@$category->meta_description);
                    break;
                case 'categorias-profissionais':
                    $m = App\Models\MetaTag::find(11);
                    $mTitle = @$m->title;
                    $mDescription = @$m->description;
                    $mKeyword = @$m->keyword;
                    break;
                case 'para-profissionais':
                    $m = App\Models\MetaTag::find(12);
                    $mTitle = @$m->title;
                    $mDescription = @$m->description;
                    $mKeyword = @$m->keyword;
                    break;
                case 'login':
                    $m = App\Models\MetaTag::find(14);
                    $mTitle = @$m->title;
                    $mDescription = @$m->description;
                    $mKeyword = @$m->keyword;
                    break;
                case 'registre-se':
                    $m = App\Models\MetaTag::find(15);
                    $mTitle = @$m->title;
                    $mDescription = @$m->description;
                    $mKeyword = @$m->keyword;
                    break;
                case 'cart':
                    $m = App\Models\MetaTag::find(16);
                    $mTitle = @$m->title;
                    $mDescription = @$m->description;
                    $mKeyword = @$m->keyword;
                    break;
                case 'browse-categories':
                    $m = App\Models\MetaTag::find(17);
                    $mTitle = @$m->title;
                    $mDescription = @$m->description;
                    $mKeyword = @$m->keyword;
                    break;
                case 'seja-um-profissional':
                    $m = App\Models\MetaTag::find(18);
                    $mTitle = @$m->title;
                    $mDescription = @$m->description;
                    $mKeyword = @$m->keyword;
                    break;
                case 'produto-landing-page':
                    $m = App\Models\MetaTag::find(19);
                    $mTitle = @$m->title;
                    $mDescription = @$m->description;
                    $mKeyword = @$m->keyword;
                    break;
                case 'landing-pages':
                    $m = App\Models\MetaTag::find(20);
                    $mTitle = @$m->title;
                    $mDescription = @$m->description;
                    $mKeyword = @$m->keyword;
                    break;
                case 'landing-page-programa-de-afiliados':
                    $m = App\Models\MetaTag::find(21);
                    $mTitle = @$m->title;
                    $mDescription = @$m->description;
                    $mKeyword = @$m->keyword;
                    break;
                case 'forget-password':
                    $m = App\Models\MetaTag::find(22);
                    $mTitle = @$m->title;
                    $mDescription = @$m->description;
                    $mKeyword = @$m->keyword;
                    break;
                case 'todos-os-produtos':
                    $m = App\Models\MetaTag::find(23);
                    $mTitle = @$m->title;
                    $mDescription = @$m->description;
                    $mKeyword = @$m->keyword;
                    break;
            }
        }
    @endphp

    @if ($mTitle == '')
        <title>@yield('title')</title>
    @else
        <title>{{ $mTitle }}</title>
    @endif

    @if(Request::segment(1) == "central-de-ajuda" && Request::segment(2) !== null)
        @yield('help_section_meta')
    @else
        <meta name="description" content="{{ $mDescription }}">
        <meta name="keyword" content="{{ $mKeyword }}">
    @endif
    <!--style-->
    @yield('style')
    @yield('scripts')
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KQCVFDD"
            height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div class="wrapper">
        <div class="parent_loader" style="display: none;">
            <div class="loader">
            </div>
        </div>
        @yield('header')
        @yield('content')
        @yield('footer')
        @if($errors->any())
        <script>
            @foreach($errors->all() as $er)
                toastr.error('{{$er}}');
            @endforeach
        </script>
        @endif
        {{-- @yield('scripts') --}}
        @if(@session()->get('success'))
        <script>
            toastr.success("{{session()->get('success')}}");
        </script>
        @endif

        @if(@session()->get('error'))
        <script>
            toastr.error("{{session()->get('error')}}");
        </script>
        @endif
    </div>
</body>
</html>
