
<!DOCTYPE html>
<html>
	<head>
		<title>Mail</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	@include('mail.account_approval.layouts.header')
	</head>
	<body>
		<div style="width:640px; margin:0 auto;">
			<div style="/*width:620px;*/ min-height:101px; background:#F5F5F3; /*padding: 0px 10px;*/ border:1px solid #dcd7d7;">
                <div style="float: none; text-align: center; margin-top: 0px; background-color: #0f586c;">
                    <img src="{{ url('public/frontnd/images/logo.png') }}" width="240" alt="">
                </div>
            </div>
            <div style="width:620px; margin:0 auto; min-height:20px; padding:0 10px; margin:15px 0; background:#8ccd56; border:1px solid #dcd7d7; color: #000;">
                <p style="font-family:Arial; text-align:center; margin:9px 0; color: #000;">NETBHE LEMBRANDO VOCÊ DE SUA CONSULTA</p>
            </div>
			<div style="width:620px; border:1px solid #dcd7d7; margin: 15px 0; padding:10px; ">
				<h1 style="font-family:Arial; font-size:16px; font-weight:500; /*color:#8ccd56;*/ margin:5px 0 12px 0;">Oi {{@$user->name}},</h1>

				<!-- <div style="display:block;overflow:hidden; width:100%;">
					<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839; float:left;margin: 12px 0 0 0;width:77px;">Name :</p>
					<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839; float:left;margin: 12px 0 0 0;">Proqrx</p>
				</div> -->
				<!-- <div style="display:block;overflow:hidden; width:100%;">
					<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839; float:left;margin: 12px 0 0 0;width:77px;">Username :</p>
					<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839; float:left;margin: 12px 0 0 0;">Username name here Se Will</p>
				</div>
				<div style="display:block;overflow:hidden; width:100%;">
					<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839; float:left;margin: 12px 0 0 0;width:77px;">Password :</p>
					<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839; float:left;margin: 12px 0 0 0;">abcd145235</p>
				</div> -->

				
				<div style="display:block; overflow:hidden; width:100%;">
					<p style="font-family:Arial; font-size:14px; font-weight:500; color:#000;margin: 28px 0px 0px;">
						Sua próxima sessão é {{ @$data->date }} , {{ @$data->start_time }} - {{ @$data->end_time }} , {{ @$data->duration }}
					</p>

					<p style="font-family:Arial; font-size:14px; font-weight:500; color:#000;margin: 28px 0px 0px;">
						Você precisa entrar em sua conta na plataforma e ligar para paciente no horário agendado.
					</p>
					<p style="font-family:Arial; font-size:14px; font-weight:500; color:#000;margin: 28px 0px 0px;">
						Aqui estão os dados da sua sessão:</br>
						<strong>Data</strong> : {{ @$data->date }} </br>
						<strong>Time</strong> : {{ @$data->start_time }} - {{ @$data->end_time }} </br>
						<strong>Duration</strong> : {{ @$data->duration }}</br>

					</p>
				</div>

				<div style="display:block; overflow:hidden; width:100%;">
					<p style="font-family:Arial; font-size:14px; font-weight:500; color:#000;margin: 28px 0px 0px;">
						Antes de seu atendimento começar:
						1.Verifique se você está conectado à internet;
						2.Nossa plataforma funciona somente nos navegadores: Chrome, Firefox e Opera;
						3.Certifique-se que sua internet tem no mínimo 1MB de velocidade;
						4.Na hora da sua sessão, é só entrar na sua conta e clicar em "iniciar sessão"
					</p>
				</div>

				<div style="display:block; overflow:hidden; width:100%;">
					<p style="font-family:Arial; font-size:14px; font-weight:500; color:#000;margin: 28px 0px 0px;">
				Caso tenha alguma dúvida, por favor, entre em contato com nossa equipe de suporte no email: suportepro@netbhe.com.

				</p>
				</div>
				Temos uma equipe pronta para auxiliá-lo(a).
				<div style="display:block;overflow:hidden; width:100%; margin: 5px 0px 10px 0px;">
					<p style="font-family:Arial; font-size:14px; font-weight:500; text-align:center; color:#000;padding: 4px; background:#f5f5f5;">
						Por favor clique no link abaixo :
					</p>
				</div>






				<div style="display:block;overflow:hidden; width:100%; text-align:center; margin: 0px 0px 10px 0px;">
					<a href="{{ route('login') }}" style="font-family:Arial; border-radius:17px;font-size:15px; font-weight:500; color:#FFF; display:inline-block; padding: 7px 12px; background:#8ccd56; text-decoration:none;">Conecte-se</a>
				</div>
				<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839;margin: 0px 0px 10px 0px;">Atenciosamente,</p>
				<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839;margin: 0px 0px 10px 0px;">equipe Netbhe
				https://netbhe.com
				</p>
			</div>
		</div>
	</body>
</html>
