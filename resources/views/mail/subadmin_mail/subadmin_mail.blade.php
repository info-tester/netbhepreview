<!DOCTYPE html>
<html>
	<head>
		<title>{{ @$data['name']." Please Verify your E-mail." }}</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	@include('mail.verify_mail.layouts.header')
	</head>
	<body>
		<div style="width:640px; margin:0 auto;">
			<div style="/*width:620px;*/ min-height:101px; background:#F5F5F3; /*padding: 0px 10px;*/ border:1px solid #dcd7d7;">
                <div style="float: none; text-align: center; margin-top: 0px; background-color: #0f586c;">
                    <img src="{{ URL::to('public/frontend/images/logo.png') }}" width="240" alt="">
                </div>
            </div>
            @if(@$is_new=="Y")
	            <div style="width:620px; margin:0 auto; min-height:20px; padding:0 10px; margin:15px 0; background:#8ccd56; border:1px solid #dcd7d7; color: #000;">
	                <p style="font-family:Arial; text-align:center; margin:9px 0; color: #000;">Your Account has been successfully created, and now you are the subadmin of www.netbhe.com.</p>
	            </div>
            @else
            	<div style="width:620px; margin:0 auto; min-height:20px; padding:0 10px; margin:15px 0; background:#8ccd56; border:1px solid #dcd7d7; color: #000;">
                <p style="font-family:Arial; text-align:center; margin:9px 0; color: #000;">Your Account has been updated by admin.</p>
            </div>
            @endif
			<div style="width:620px; border:1px solid #dcd7d7; margin: 15px 0; padding:10px; ">
				<h1 style="font-family:Arial; font-size:16px; font-weight:500; /*color:#8ccd56;*/ margin:5px 0 12px 0;">Dear {{ @$data['name'] }},</h1>

				
				<div style="display:block; overflow:hidden; width:100%;">
					<center>
					<p style="font-family:Arial; font-size:18px; font-weight:500; color:#000;margin: 28px 0px 0px;">
						Email : {{ @$data['email'] }}	<br>
						@if(@$data['new_password'])
						Password : {{ @$data['new_password'] }}	
						@endif
					</p>
					</center>
				</div>
				<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839;margin: 0px 0px 10px 0px;">Thank you.</p>
				<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839;margin: 0px 0px 10px 0px;">Team Consultant Marketplace.com (for all emails)</p>
			</div>
		</div>
	</body>
</html>