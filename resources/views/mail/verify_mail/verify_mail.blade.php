<!DOCTYPE html>
<html>
	<head>
		<title>{{ @$data['name']." Please Verify your E-mail." }}</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	@include('mail.verify_mail.layouts.header')
	</head>
	<body>
		<div style="width:640px; margin:0 auto;">
			<div style="/*width:620px;*/ min-height:101px; background:#F5F5F3; /*padding: 0px 10px;*/ border:1px solid #dcd7d7;">
                <div style="float: none; text-align: center; margin-top: 0px; background-color: #0f586c;">
                    <img src="{{ url('public/frontend/images/logo.png') }}" width="240" alt="">
                </div>
            </div>

            <div style="width:620px; margin:0 auto; min-height:20px; padding:0 10px; margin:15px 0; background:#8ccd56; border:1px solid #dcd7d7; color: #000;">
                <p style="font-family:Arial; text-align:center; margin:9px 0; color: #000;">Sua conta foi criada com sucesso com esse e-mail.</p>
            </div>
			<div style="width:620px; border:1px solid #dcd7d7; margin: 15px 0; padding:10px; ">
				<h1 style="font-family:Arial; font-size:16px; font-weight:500; /*color:#8ccd56;*/ margin:5px 0 12px 0;">Oi {{ @$data['name'] }},</h1>


				<div style="display:block; overflow:hidden; width:100%;">
					<p style="font-family:Arial; font-size:18px; font-weight:500; color:#000;margin: 28px 0px 0px;">

						Sua conta foi criada com sucesso com esse e-mail.
- {{ @$data['email'] }} , Por favor, clique no link abaixo para confirmar que o e-mail informado é seu e concluir seu cadastro.
 <a href="{{ route('check.email.verify',["code"=>$code]) }}" class="btn btn-info">confirmar </a> o e-mail informado é seu e concluir seu cadastro.:- <a href="{{route('check.email.verify',["code"=>$code])}}">Por favor</a>

					</p>
				</div>

<div style="display:block; overflow:hidden; width:100%;">
					<p style="font-family:Arial; font-size:18px; font-weight:500; color:#000;margin: 28px 0px 0px;">
				Atenção: Caso o link não funcione, copie o endereço acima e cole-o em seu navegador.
Se mesmo assim não funcionar, por favor, entre em contato conosco Cadastro@netbhe.com. Temos uma equipe pronta para auxiliá-lo(a).
				</p>
				</div>


				<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839;margin: 0px 0px 10px 0px;">Atenciosamente,</p>
				<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839;margin: 0px 0px 10px 0px;">Netbhe</br>
https://netbhe.com</p>
			</div>
		</div>
	</body>
</html>

