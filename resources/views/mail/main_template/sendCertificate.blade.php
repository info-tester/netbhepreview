<!DOCTYPE html>
<html>
	<head>
		<title>{{ @$data['name']." Please Verify your E-mail." }}</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	@include('mail.main_template.layouts.header')
	</head>
	<body>
		<div style="width:640px; margin:0 auto;">
			<div style="/*width:620px;*/ min-height:101px; background:#F5F5F3; /*padding: 0px 10px;*/ border:1px solid #dcd7d7;">
                <div style="float: none; text-align: center; margin-top: 0px; background-color: #FFFFFF;">
                    <img src="{{ url('public/frontend/images/logo.png') }}" width="240" alt="">
                </div>
            </div>
            {!! $content !!}
		</div>
	</body>
</html>