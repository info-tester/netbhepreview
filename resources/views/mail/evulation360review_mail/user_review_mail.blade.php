
<!DOCTYPE html>
<html>
	<head>
		<title>Mail</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	@include('mail.user_form_answered.layouts.header')
	</head>
	<body>
		<div style="width:640px; margin:0 auto;">
			<div style="/*width:620px;*/ min-height:101px; background:#F5F5F3; /*padding: 0px 10px;*/ border:1px solid #dcd7d7;">
                <div style="float: none; text-align: center; margin-top: 0px; background-color: #0f586c;">
                    <img src="{{ url('public/frontnd/images/logo.png') }}" width="240" alt="">
                </div>
            </div>
            <div style="width:620px; margin:0 auto; min-height:20px; padding:0 10px; margin:15px 0; background:#8ccd56; border:1px solid #dcd7d7; color: #000;">
                <p style="font-family:Arial; text-align:center; margin:9px 0; color: #000;">Reminder to answer: 360º evaluation</p>
            </div>

			<div style="width:620px; border:1px solid #dcd7d7; margin: 15px 0; padding:10px; ">
				<h1 style="font-family:Arial; font-size:16px; font-weight:500; /*color:#8ccd56;*/ margin:5px 0 12px 0;">Olá  ,</h1>

				
				<div style="display:block; overflow:hidden; width:100%;">

					<p style="overflow-wrap: break-word">
					Este é um lembrete para responder e enviar a Avaliação 360º de Competências.
				</p>

				<p style="overflow-wrap: break-word">
				Para responder a avaliação o tempo será entre 5 e 10 minutos e é composta por avaliações simples e objetivas. Você deverá informar uma nota de 1 a 5 em alguns itens.

				</p>

				<p style="overflow-wrap: break-word">
				Seria muito importante que você respondesse a avaliação assim que puder, pois ela tem uma data limite.
			</p>

				<p style="overflow-wrap: break-word">
					Clique abaixo para responder e enviar:
						RESPONDER ▶</p>
						<p style="overflow-wrap: break-word">Ou copie e cole o link abaixo no seu navegador:</p>

					<p style="font-family:Arial; font-size:14px; font-weight:500; color:#000;margin: 28px 0px 0px;">
						


						<?php
						$usertoolsids = encrypt(@$data);
						//$userto = $data;
						//echo $userto;
						?>
						<p style="overflow-wrap: break-word"><strong>Customer</strong> {{ route('pblc.evaluation',['id'=>$usertoolsids,'type'=>"C"]) }} </p>
						<?php /*
						<p><strong>Manager</strong> {{ route('pblc.evaluation',['id'=>@$data->usertoolsid ,'type'=>"manager"]) }}</p>
						<p><strong>Others</strong>{{ route('pblc.evaluation',['id'=>@$data->usertoolsid ,'type'=>"others"]) }}</p>
						<p><strong>Pairs</strong>{{ route('pblc.evaluation',['id'=>@$data->usertoolsid ,'type'=>"pairs"]) }}</p>
						<p><strong>Subordinates</strong>{{ route('pblc.evaluation',['id'=>@$data->usertoolsid ,'type'=>"subordinates"]) }}</p>
						<p><strong>Users</strong>{{ route('pblc.evaluation',['id'=>@$data->usertoolsid ,'type'=>"users"]) }}</p>
						<?php */ ?>
					</p>
					<p style="overflow-wrap: break-word">
						Caso tenha alguma dúvida, por favor, entre em contato com nossa equipe de suporte no email: suporte@netbhe.com.
					</p>

				</div>
				
				
				<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839;margin: 0px 0px 10px 0px;">Atenciosamente

				</p>

				<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839;margin: 0px 0px 10px 0px;">equipe Netbhe <a href="https://netbhe.com">netbhe.com</a>
					
				</p>
				
			</div>
		</div>
	</body>
</html>
