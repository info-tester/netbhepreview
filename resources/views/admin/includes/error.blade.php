<div class="col-sm-12 col-md-12">
    @if($errors->any())
        @foreach($errors->all() as $err)
            <p class="alert alert-danger">{{ $err }}</p>
        @endforeach
    @endif

    @if(session()->has('success'))
        <p class="alert alert-success">{{ session()->get('success') }}</p>
    @endif
    
    @if(session()->has('error'))
        <p class="alert alert-danger">{{ session()->get('error') }}</p>
    @endif
</div>