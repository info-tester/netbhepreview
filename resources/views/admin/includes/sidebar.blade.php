@php
$menu=adminAccessMenu();
$subMenu=adminAccessSubMenu();
@endphp


<div class="left side-menu">
	<div class="sidebar-inner slimscrollleft">
		<div class="user-details">
			<div class="pull-left">
				<img src="{{ URL::to('public/frontend/images/avatar-1.jpg') }}" alt="" class="thumb-md img-circle">
			</div>
			<div class="user-info">
				<div class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">{{ @Auth::guard('admin')->user()->name }} <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="{{ route('admin.change.password') }}"><i class="md md-settings"></i> Profile</a></li>

						<li>
							<a href="{{ route('admin.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="md md-settings-power"></i> Logout</a>
							<form id="logout-form" action="{{ url('/admin/logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>
						</li>
					</ul>
				</div>
				<p class="text-muted m-0">Administrator</p>
			</div>
		</div>
		<!--- Divider -->
        @if(auth()->guard('admin')->user()->user_type=='S')
		<div id="sidebar-menu">
			<ul>
				<li>
					<a href="{{ route('admin.dashboard') }}" class="<?php if(Request::segment(2)=='home'){echo "active active1";}?>"><i class="fa fa-tachometer" aria-hidden="true"></i>
					<span> Dashboard </span> <span class="pull-right"></span> </a>
				</li>
                @if(in_array(1, $menu))
                    <li class="has_sub">
                        <a href="#" class="waves-effect<?php if(Request::segment(2) == "categories" || Request::segment(2) == "add-category" || Request::segment(2) == "category" ||Request::segment(2) == "manage-category-landing"|| Request::segment(2) == "category-landing-add" || Request::segment(2) == "category-landing-edit"){?>subdrop active active1<?php } ?>"><i class="fa fa-list-alt"></i>
                        <span> Category </span> <span class="pull-right"><i class="md md-add"></i></span></a>
                        <ul class="list-unstyled">
                            @if(in_array(12, $subMenu))
                                <li><a href="{{ route('admin.categories') }}" class="<?php if(Request::segment(2) == "categories" || Request::segment(2) == "add-category" || Request::segment(2) == "category"){?>active2<?php } ?>">Manage Categories</a></li>
                            @endif
                            @if(in_array(13, $subMenu))
                                <li><a href="{{ route('admin.category.landing.index') }}" class="<?php if(Request::segment(2) == "manage-category-landing" || Request::segment(2) == "category-landing-add" || Request::segment(2) == "category-landing-edit"){?>active2<?php } ?>">Manage Categories Landing</a></li>
                            @endif
                        </ul>
                    </li>
                @endif

                @if(in_array(2, $menu))
                    <li class="has_sub">
                        <a href="#" class="waves-effect<?php if((Request::segment(2) == "specialties") || (Request::segment(2) == "manage-language")
                        || (Request::segment(2) == "experience") || (Request::segment(2) == "account") || (Request::segment(2) == "commission")
                        || (Request::segment(2) == "refer-discount") || (Request::segment(2) == "mail-template") || (Request::segment(2) == "coupon" || Request::segment(2)=="coupon-create"|| Request::segment(2)=="coupon-edit")
                        ){?>subdrop active active1<?php } ?>"><i class="fa fa-cog"></i>
                        <span> Settings </span> <span class="pull-right"><i class="md md-add"></i></span></a>
                        <ul class="list-unstyled">

                            @if(in_array(14, $subMenu))
                            <li><a href="{{ route('admin.specialty') }}" class="<?php if(Request::segment(2) == "specialties"){?>active2<?php } ?>"> Professional Specialties</a></li>
                            @endif
                            @if(in_array(15, $subMenu))
                                <li><a href="{{ route('manage.language') }}" class="@if(Request::segment(2) == "manage-language") active2 @endif">Manage Language</a></li>
                            @endif
                            @if(in_array(16, $subMenu))
                                <li><a href="{{ route('admin.experiences') }}" class="<?php if(Request::segment(2) == "experience"){?>active2<?php } ?>"> Experiences</a></li>
                            @endif
                            @if(in_array(17, $subMenu))
                                <li><a href="{{ route('admin.account') }}" class="@if(Request::segment(2) == "account") active2 @endif"> Manage Bank Account</a></li>
                            @endif
                            @if(in_array(18, $subMenu))
                                <li><a href="{{ route('admin.commission') }}" class="@if(Request::segment(2) == "commission") active2 @endif"> Manage commission</a></li>
                            @endif
                            @if(in_array(19, $subMenu))
                                <li><a href="{{ route('admin.refer.discount') }}" class="@if(Request::segment(2) == "refer-discount") active2 @endif"> Manage Refer Discount</a></li>
                            @endif
                            @if(in_array(20, $subMenu))
                                <li><a href="{{ route('admin.mail.template') }}" class="@if(Request::segment(2) == "mail-template") active2 @endif"> Manage Email Templates</a></li>
                            @endif
                            @if(in_array(21, $subMenu))
                                <li><a href="{{ route('admin.coupon') }}" class="@if(Request::segment(2) == "coupon" || Request::segment(2)=="coupon-create"|| Request::segment(2)=="coupon-edit") active2 @endif"> Manage Coupon</a></li>
                            @endif
                        </ul>
                    </li>
                @endif

                @if(in_array(1, $menu))
                    <li class="has_sub">
                        <a href="#" class="waves-effect<?php if(Request::segment(2) == "professional"){?>subdrop active active1<?php } ?>"><i class="md ion-person"></i>
                        <span> Professionals & User </span> <span class="pull-right"><i class="md md-add"></i></span></a>
                        <ul class="list-unstyled">
                            @if(in_array(22, $subMenu))
                                <li><a href="{{ route('professional.index') }}" class="<?php if(Request::segment(2) == "professional" && Request::segment(3) != "inactive"){?>active2<?php } ?>"> Active Professionals & Users</a></li>
                            @endif
                            @if(in_array(22, $subMenu))
                                <li><a href="{{ route('professional.inactive') }}" class="<?php if(Request::segment(2) == "professional" && Request::segment(3) == "inactive"){?>active2<?php } ?>"> Inactive Professionals & Users</a></li>
                            @endif
                        </ul>
                    </li>
                @endif

                @if(in_array(4, $menu))
                    <li class="has_sub">
                        <a href="#" class="waves-effect<?php if((Request::segment(2) == "order")){?>subdrop active active1<?php } ?>"><i class="fa fa-shopping-cart" aria-hidden="true"></i>
                        <span> Bookings </span> <span class="pull-right"><i class="md md-add"></i></span></a>
                        <ul class="list-unstyled">
                            @if(in_array(24, $subMenu))
                                <li><a href="{{ route('admin.order') }}" class="<?php if((Request::segment(2) == "order")){?>active2<?php } ?>">Manage Bookings</a></li>
                            @endif
                        </ul>
                    </li>
                @endif

                @if(in_array(5, $menu))
                <li class="has_sub">
                    <a href="#" class="waves-effect @if (Request::segment(2) == "landing-page-list" ||Request::segment(2) == "landing-page-price" ||Request::segment(2) == "landing-page" ||Request::segment(2) == "landing-page-payment") waves-effectsubdrop active active1 @endif"><i class="fa fa-newspaper-o"></i>
                    <span> Landing Page</span> <span class="pull-right"><i class="md md-add"></i></span></a>
                    <ul class="list-unstyled">
                        @if(in_array(25, $subMenu))
                            <li><a href="{{ route('admin.landing.page.list') }}" class="@if(Request::segment(2) == "landing-page-list" ||Request::segment(2) == "landing-page") active2 @endif">Manage Landing page</a></li>
                        @endif
                        @if(in_array(26, $subMenu))
                            <li><a href="{{ route('admin.landing.page.price') }}" class="@if(Request::segment(2) == "landing-page-price") active2 @endif">Manage Landing page Price</a></li>
                        @endif
                        @if(in_array(27, $subMenu))
                            <li><a href="{{ route('admin.landing.page.payment') }}" class="@if(Request::segment(2) == "landing-page-payment") active2 @endif">Landing Payments</a></li>
                        @endif
                    </ul>
                </li>
                @endif

                @if(in_array(6, $menu))
                    <li class="has_sub">
                        <a href="#" class="waves-effect @if (Request::segment(2) == "product-category" || Request::segment(2) == "product" || Request::segment(2) == "order-product" || Request::segment(2) =="order-cancel-product" ||Request::segment(2) == "reviews") waves-effectsubdrop active active1 @endif"><i class="fa fa-archive"></i>
                        <span> Product</span> <span class="pull-right"><i class="md md-add"></i></span></a>
                        <ul class="list-unstyled">
                            @if(in_array(28, $subMenu))
                                <li><a href="{{ route('admin.product.category') }}" class="@if(Request::segment(2) == "product-category") active2 @endif">Product Category</a></li>
                            @endif
                            @if(in_array(29, $subMenu))
                                <li><a href="{{ route('admin.product.list') }}" class="@if(Request::segment(2) == "product") active2 @endif">Manage Products</a></li>
                            @endif
                            @if(in_array(30, $subMenu))
                                <li><a href="{{ route('admin.order.product') }}" class="@if(Request::segment(2) == "order-product") active2 @endif">Manage Orders</a></li>
                            @endif
                            @if(in_array(30, $subMenu))
                                <li><a href="{{ route('admin.order.cancel.product.search') }}" class="@if(Request::segment(2) == "order-cancel-product") active2 @endif">Manage Cancel Orders</a></li>
                            @endif
                            @if(in_array(31, $subMenu))
                                <li><a href="{{ route('admin.manage.reviews') }}" class="<?php if((Request::segment(2) == "reviews")){?>active2<?php } ?>">Manage Reviews</a></li>
                            @endif
                        </ul>
                    </li>
				@endif

                @if(in_array(7, $menu))
                    <li class="has_sub">
                        <a href="#" class="waves-effect<?php if((Request::segment(2) == "affiliate")){?>subdrop active active1<?php } ?>"><i class="fa fa-retweet" aria-hidden="true"></i>
                        <span> Product Affiliates </span> <span class="pull-right"><i class="md md-add"></i></span></a>
                        <ul class="list-unstyled">
                            @if(in_array(32, $subMenu))
                                <li><a href="{{ route('admin.manage.affiliate') }}" class="<?php if((Request::segment(2) == "affiliate")){?>active2<?php } ?>">View affiliate users</a></li>
                            @endif
                        </ul>
                    </li>
                @endif
                @if(in_array(8, $menu))
                    <li class="has_sub">
                        <a href="#" class="waves-effect<?php if((Request::segment(2) == "video-affiliate" ||Request::segment(2) == "video-affiliate-commission"||Request::segment(2) == "video-affiliate-banner" ||Request::segment(2) == "video-affiliate-banner-list")){?>subdrop active active1<?php } ?>"><i class="fa fa-retweet" aria-hidden="true"></i>
                        <span>Video Affiliates </span> <span class="pull-right"><i class="md md-add"></i></span></a>
                        <ul class="list-unstyled">
                            @if(in_array(33, $subMenu))
                                <li><a href="{{ route('admin.manage.video.affiliate') }}" class="<?php if((Request::segment(2) == "video-affiliate")){?>active2<?php } ?>">Manage Video Affiliates</a></li>
                            @endif
                            @if(in_array(34, $subMenu))
                                <li><a href="{{ route('admin.manage.video.affiliate.banner.list') }}" class="<?php if((Request::segment(2) == "video-affiliate-banner" ||Request::segment(2) == "video-affiliate-banner-list")){?>active2<?php } ?>">Manage Video Affiliates Banner</a></li>
                            @endif
                            @if(in_array(35, $subMenu))
                                <li><a href="{{ route('admin.manage.video.affiliate.commission') }}" class="<?php if((Request::segment(2) == "video-affiliate-commission")){?>active2<?php } ?>">Commission Percentage</a></li>
                            @endif
                        </ul>
                    </li>
                @endif

                @if(in_array(9, $menu))
                    <li class="has_sub">
                        <a href="#" class="waves-effect<?php if((Request::segment(2) == "blog") || (Request::segment(2) == "blog-details") || (Request::segment(2) == "blog-add") || (Request::segment(2) == "blog-edit") || (Request::segment(2) == "blog-category") || (Request::segment(2) == "blog-category-add") || (Request::segment(2) == "blog-category-edit")){?>subdrop active active1<?php } ?>"><i class="md fa fa-rss"></i>
                        <span> Blogs </span> <span class="pull-right"><i class="md md-add"></i></span></a>
                        <ul class="list-unstyled">
                            @if(in_array(36, $subMenu))
                                <li><a href="{{ route('admin.blog.category.index') }}" class="<?php if((Request::segment(2) == "blog-category") || (Request::segment(2) == "blog-category-add") || (Request::segment(2) == "blog-category-edit")){?>active2<?php } ?>">Blog Category</a></li>
                            @endif
                            @if(in_array(37, $subMenu))
                                <li><a href="{{ route('admin.blog.index') }}" class="<?php if((Request::segment(2) == "blog") || (Request::segment(2) == "blog-details") || (Request::segment(2) == "blog-add") || (Request::segment(2) == "blog-edit")){?>active2<?php } ?>">Manage Blog</a></li>
                            @endif
                        </ul>
                    </li>
                @endif

                @if(in_array(10, $menu))
                    <li class="has_sub">
                        <a href="#" class="waves-effect<?php if((Request::segment(2) == "update-about-us" || Request::segment(2) == "how-it-works" ||Request::segment(2) == "affiliate-landing-page"||Request::segment(2) == "help"||Request::segment(2) == "product-section"|| Request::segment(2) == "terms-of-services" || Request::segment(2) == "faq" || Request::segment(2) == "privacy-policy" || Request::segment(2) == "footer-management"|| Request::segment(2) == "testimonials"|| Request::segment(2) == "add-testimonial"|| Request::segment(2) == "edit-testimonial" || Request::segment(2) == "newsletter"  || Request::segment(2) == "home-expert-section" || Request::segment(2) == "other-page-contents" || Request::segment(2) == "meta-tag-management" || Request::segment(2) == "landing-page-management" || Request::segment(2) == "home-banner-management" || Request::segment(2) == "refer-content-management" || Request::segment(2) == "product-landing-page" ||Request::segment(2) == "landing-page-landing")){?>subdrop active active1<?php } ?>"><i class="fa fa-newspaper-o" aria-hidden="true"></i>
                        <span> Contents </span> <span class="pull-right"><i class="md md-add"></i></span></a>

                        <ul class="list-unstyled">
                            @if(in_array(38, $subMenu))
                                <li><a href="{{ route('admin.about_us') }}" class="<?php if((Request::segment(2) == "update-about-us")){?>active2<?php } ?>">Manage About-Us</a></li>
                            @endif
                            @if(in_array(39, $subMenu))
                                <li><a href="{{ route('admin.testimonial') }}" class="<?php if((Request::segment(2) == "testimonials" || Request::segment(2) == "add-testimonial" || Request::segment(2) == "edit-testimonial")){?>active2<?php } ?>">Manage Testimonial</a></li>
                            @endif
                            @if(in_array(40, $subMenu))
                                <li><a href="{{ route('admin.how.it.works.update') }}" class="<?php if((Request::segment(2) == "how-it-works")){?>active2<?php } ?>">How It Works</a></li>
                            @endif
                            @if(in_array(41, $subMenu))
                                <li><a href="{{ route('admin.product.section.update') }}" class="<?php if((Request::segment(2) == "product-section")){?>active2<?php } ?>">Products Section</a></li>
                            @endif
                            @if(in_array(42, $subMenu))
                                <li><a href="{{ route('admin.home.expert.section') }}" class="<?php if((Request::segment(2) == "home-expert-section")){?>active2<?php } ?>">Home Professionals Section</a></li>
                            @endif
                            @if(in_array(43, $subMenu))
                                <li><a href="{{ route('landing.page.edit') }}" class="<?php if((Request::segment(2) == "landing-page-management")){?>active2<?php } ?>">Professional Landing Page</a></li>
                            @endif
                            @if(in_array(44, $subMenu))
                                <li><a href="{{ route('other.page.contents') }}" class="<?php if((Request::segment(2) == "other-page-contents")){?>active2<?php } ?>">Other Page Contents</a></li>
                            @endif
                            @if(in_array(45, $subMenu))
                                <li><a href="{{ route('home.banner.management') }}" class="<?php if((Request::segment(2) == "home-banner-management")){?>active2<?php } ?>">Manage Homepage Banners</a></li>
                            @endif

                            @if(in_array(46, $subMenu))
                                <li><a href="{{ route('meta.tag.manage') }}" class="<?php if((Request::segment(2) == "meta-tag-management")){?>active2<?php } ?>">Meta Tag Manager</a></li>
                            @endif

                            @if(in_array(47, $subMenu))
                                <li><a href="{{ route('admin.newsLetter') }}" class="<?php if((Request::segment(2) == "newsletter")){?>active2<?php } ?>">Newsletter</a></li>
                            @endif

                            @if(in_array(48, $subMenu))
                                <li><a href="{{ route('admin.terms.of.services') }}" class="<?php if((Request::segment(2) == "terms-of-services")){?>active2<?php } ?>">Terms Of Services</a></li>
                            @endif
                            @if(in_array(49, $subMenu))
                                <li><a href="{{ route('admin.privacy.policy.update') }}" class="<?php if((Request::segment(2) == "privacy-policy")){?>active2<?php } ?>">Privacy Policy</a></li>
                            @endif

                            @if(in_array(50, $subMenu))
                                <li><a href="{{ route('admin.faq.update') }}" class="<?php if((Request::segment(2) == "faq")){?>active2<?php } ?>">Faq</a></li>
                            @endif

                            @if(in_array(51, $subMenu))
                                <li><a href="{{ route('admin.help.contents') }}" class="<?php if((Request::segment(2) == "help")){?>active2<?php } ?>">Help Section</a></li>
                            @endif

                            @if(in_array(52, $subMenu))
                                <li><a href="{{ route('admin.footer.management.update') }}" class="<?php if((Request::segment(2) == "footer-management")){?>active2<?php } ?>">Footer Managements</a></li>
                            @endif
                            @if(in_array(53, $subMenu))
                                <li><a href="{{ route('admin.refer.content') }}" class="<?php if((Request::segment(2) == "refer-content-management")){?>active2<?php } ?>">Refer Content Managements</a></li>
                            @endif

                            @if(in_array(54, $subMenu))
                                <li><a href="{{ route('admin.product.landing.page') }}" class="<?php if((Request::segment(2) == "product-landing-page")){?>active2<?php } ?>">Manage Product Landing Page</a></li>
                            @endif

                            @if(in_array(65, $subMenu))
                                <li><a href="{{ route('admin.affiliate.landing.page') }}" class="<?php if((Request::segment(2) == "affiliate-landing-page")){?>active2<?php } ?>">Manage Affiliate Landing Page</a></li>
                            @endif
                            @if(in_array(66, $subMenu))
                                <li><a href="{{ route('admin.professional.landing.page') }}" class="<?php if((Request::segment(2) == "landing-page-landing")){?>active2<?php } ?>">Manage Landing Page</a></li>
                            @endif

                        </ul>
                    </li>
                @endif
                @if(in_array(11, $menu))
                    <li class="has_sub">
                        <a href="#" class="waves-effect<?php if((Request::segment(2) == "form" || Request::segment(2)=='imported' || Request::segment(2)=='tool-content-template' || Request::segment(2)=='tool-logbook-tools' || Request::segment(2)=='tool-contract-template' || Request::segment(2)=='tool-360-evaluation' || Request::segment(2)=='competences-master' || Request::segment(2)=='evaluation-type' || Request::segment(2) == 'evaluation-category') ){?>subdrop active active1<?php } ?>"><i class="md fa fa-rss"></i>
                        <span> Coaching Tools </span> <span class="pull-right"><i class="md md-add"></i></span></a>
                        <ul class="list-unstyled">
                            @if(in_array(55, $subMenu))
                                <li><a href="{{ route('admin.form.category.manage') }}" class="<?php if(Request::segment(3) == "category"){?>active2<?php } ?>">Tools Category</a></li>
                            @endif
                            @if(in_array(56, $subMenu))
                                <li><a href="{{ route('admin.form.manage') }}" class="<?php if(Request::segment(2) == "form" && Request::segment(3) != "category"){?>active2<?php } ?>">Form Tools</a></li>
                            @endif
                            @if(in_array(57, $subMenu))
                                <li><a href="{{ route('admin.imported.manage') }}" class="<?php if(Request::segment(2) == "imported"){?>active2<?php } ?>">Imported Tools</a></li>
                            @endif
                            @if(in_array(58, $subMenu))
                                <li><a href="{{ route('tool-360-evaluation.index') }}" class="<?php if(Request::segment(2) == "tool-360-evaluation"){?>active2<?php } ?>">360 Evaluation</a></li>
                            @endif
                            @if(in_array(59, $subMenu))
                                <li><a href="{{ route('tool-content-template.index') }}" class="<?php if(Request::segment(2) == "tool-content-template"){?>active2<?php } ?>">Content Template</a></li>
                            @endif
                            @if(in_array(60, $subMenu))
                                <li><a href="{{ route('tool-contract-template.index') }}" class="<?php if(Request::segment(2) == "tool-contract-template"){?>active2<?php } ?>">Contract Template</a></li>
                            @endif
                            @if(in_array(61, $subMenu))
                                <li><a href="{{ route('tool-logbook-tools.index') }}" class="<?php if(Request::segment(2) == "tool-logbook-tools"){?>active2<?php } ?>">Logbook</a></li>
                            @endif
                            @if(in_array(62, $subMenu))
                                <li><a href="{{ route('evaluation.category') }}" class="<?php if(Request::segment(2) == "evaluation-category"){?>active2<?php } ?>">Evaluation Category</a></li>
                            @endif
                            @if(in_array(63, $subMenu))
                                <li><a href="{{ route('evaluation-type.index') }}" class="<?php if(Request::segment(2) == "evaluation-type"){?>active2<?php } ?>">Evaluation Type</a></li>
                            @endif
                            @if(in_array(64, $subMenu))
                                <li><a href="{{ route('competences-master.index') }}" class="<?php if(Request::segment(2) == "competences-master"){?>active2<?php } ?>">Competences Master</a></li>
                            @endif

                        </ul>
                    </li>
                @endif

			</ul>
			<div class="clearfix"></div>
		</div>
        @else
        <div id="sidebar-menu">
			<ul>
				<li>
					<a href="{{ route('admin.dashboard') }}" class="<?php if(Request::segment(2)=='home'){echo "active active1";}?>"><i class="fa fa-tachometer" aria-hidden="true"></i>
					<span> Dashboard </span> <span class="pull-right"></span> </a>
				</li>
                <li class="has_sub">
                    <a href="#" class="waves-effect<?php if(Request::segment(2) == "categories" || Request::segment(2) == "add-category" || Request::segment(2) == "category" ||Request::segment(2) == "manage-category-landing"|| Request::segment(2) == "category-landing-add" || Request::segment(2) == "category-landing-edit"){?>subdrop active active1<?php } ?>"><i class="fa fa-list-alt"></i>
                    <span> Category </span> <span class="pull-right"><i class="md md-add"></i></span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('admin.categories') }}" class="<?php if(Request::segment(2) == "categories" || Request::segment(2) == "add-category" || Request::segment(2) == "category"){?>active2<?php } ?>">Manage Categories</a></li>
                        <li><a href="{{ route('admin.category.landing.index') }}" class="<?php if(Request::segment(2) == "manage-category-landing" || Request::segment(2) == "category-landing-add" || Request::segment(2) == "category-landing-edit"){?>active2<?php } ?>">Manage Categories Landing</a></li>
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="#" class="waves-effect<?php if((Request::segment(2) == "specialty") || (Request::segment(2) == "manage-language")
                    || (Request::segment(2) == "experience") || (Request::segment(2) == "account") || (Request::segment(2) == "commission")
                    || (Request::segment(2) == "refer-discount") || (Request::segment(2) == "mail-template") || (Request::segment(2) == "coupon" || Request::segment(2)=="coupon-create"|| Request::segment(2)=="coupon-edit")
                    ){?>subdrop active active1<?php } ?>"><i class="fa fa-cog"></i>
                    <span> Settings </span> <span class="pull-right"><i class="md md-add"></i></span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('admin.specialty') }}" class="<?php if(Request::segment(2) == "specialty"){?>active2<?php } ?>"> Professional Specialties</a></li>

                        <li><a href="{{ route('manage.language') }}" class="@if(Request::segment(2) == "manage-language") active2 @endif">Manage Language</a></li>

                        <li><a href="{{ route('admin.experiences') }}" class="<?php if(Request::segment(2) == "experience"){?>active2<?php } ?>"> Experiences</a></li>

                        <li><a href="{{ route('admin.account') }}" class="@if(Request::segment(2) == "account") active2 @endif"> Manage Bank Account</a></li>

                        <li><a href="{{ route('admin.commission') }}" class="@if(Request::segment(2) == "commission") active2 @endif"> Manage commission</a></li>

                        <li><a href="{{ route('admin.refer.discount') }}" class="@if(Request::segment(2) == "refer-discount") active2 @endif"> Manage Refer Discount</a></li>

                        <li><a href="{{ route('admin.mail.template') }}" class="@if(Request::segment(2) == "mail-template") active2 @endif"> Manage Email Templates</a></li>

                        <li><a href="{{ route('admin.coupon') }}" class="@if(Request::segment(2) == "coupon" || Request::segment(2)=="coupon-create"|| Request::segment(2)=="coupon-edit") active2 @endif"> Manage Coupon</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="#" class="waves-effect<?php if(Request::segment(2) == "professional"){?>subdrop active active1<?php } ?>"><i class="md ion-person"></i>
                    <span> Professionals & User </span> <span class="pull-right"><i class="md md-add"></i></span></a>
                    <ul class="list-unstyled">

                        <li><a href="{{ route('professional.index') }}" class="<?php if(Request::segment(2) == "professional" && Request::segment(3) != "inactive"){?>active2<?php } ?>"> Active Professionals & Users</a></li>

                        <li><a href="{{ route('professional.inactive') }}" class="<?php if(Request::segment(2) == "professional" && Request::segment(3) == "inactive"){?>active2<?php } ?>"> Inactive Professionals & Users</a></li>

                    </ul>
                </li>
                <li class="has_sub">
                    <a href="#" class="waves-effect<?php if((Request::segment(2) == "order")){?>subdrop active active1<?php } ?>"><i class="fa fa-shopping-cart" aria-hidden="true"></i>
                    <span> Bookings </span> <span class="pull-right"><i class="md md-add"></i></span></a>
                    <ul class="list-unstyled">

                        <li><a href="{{ route('admin.order') }}" class="<?php if((Request::segment(2) == "order")){?>active2<?php } ?>">Manage Bookings</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="#" class="waves-effect @if (Request::segment(2) == "landing-page-list" ||Request::segment(2) == "landing-page-price" ||Request::segment(2) == "landing-page" ||Request::segment(2) == "landing-page-payment") waves-effectsubdrop active active1 @endif"><i class="fa fa-newspaper-o"></i>
                    <span> Landing Page</span> <span class="pull-right"><i class="md md-add"></i></span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('admin.landing.page.list') }}" class="@if(Request::segment(2) == "landing-page-list" ||Request::segment(2) == "landing-page") active2 @endif">Manage Landing page</a></li>

                        <li><a href="{{ route('admin.landing.page.price') }}" class="@if(Request::segment(2) == "landing-page-price") active2 @endif">Manage Landing page Price</a></li>

                        <li><a href="{{ route('admin.landing.page.payment') }}" class="@if(Request::segment(2) == "landing-page-payment") active2 @endif">Landing Payments</a></li>
                    </ul>
                </li>


                <li class="has_sub">
                    <a href="#" class="waves-effect @if (Request::segment(2) == "product-category" || Request::segment(2) == "product" || Request::segment(2) == "order-product" || Request::segment(2) =="order-cancel-product" ||Request::segment(2) == "reviews") waves-effectsubdrop active active1 @endif"><i class="fa fa-archive"></i>
                    <span> Product</span> <span class="pull-right"><i class="md md-add"></i></span></a>
                    <ul class="list-unstyled">

                        <li><a href="{{ route('admin.product.category') }}" class="@if(Request::segment(2) == "product-category") active2 @endif">Product Category</a></li>

                        <li><a href="{{ route('admin.product.list') }}" class="@if(Request::segment(2) == "product") active2 @endif">Manage Products</a></li>

                        <li><a href="{{ route('admin.order.product') }}" class="@if(Request::segment(2) == "order-product") active2 @endif">Manage Orders</a></li>

                        <li><a href="{{ route('admin.order.cancel.product.search') }}" class="@if(Request::segment(2) == "order-cancel-product") active2 @endif">Manage Cancel Orders</a></li>

                        <li><a href="{{ route('admin.manage.reviews') }}" class="<?php if((Request::segment(2) == "reviews")){?>active2<?php } ?>">Manage Reviews</a></li>
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="#" class="waves-effect<?php if((Request::segment(2) == "affiliate")){?>subdrop active active1<?php } ?>"><i class="fa fa-retweet" aria-hidden="true"></i>
                    <span> Product Affiliates </span> <span class="pull-right"><i class="md md-add"></i></span></a>
                    <ul class="list-unstyled">

                        <li><a href="{{ route('admin.manage.affiliate') }}" class="<?php if((Request::segment(2) == "affiliate")){?>active2<?php } ?>">View affiliate users</a></li>
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="#" class="waves-effect<?php if((Request::segment(2) == "video-affiliate" ||Request::segment(2) == "video-affiliate-commission"||Request::segment(2) == "video-affiliate-banner" ||Request::segment(2) == "video-affiliate-banner-list")){?>subdrop active active1<?php } ?>"><i class="fa fa-retweet" aria-hidden="true"></i>
                    <span>Video Affiliates </span> <span class="pull-right"><i class="md md-add"></i></span></a>
                    <ul class="list-unstyled">

                        <li><a href="{{ route('admin.manage.video.affiliate') }}" class="<?php if((Request::segment(2) == "video-affiliate")){?>active2<?php } ?>">Manage Video Affiliates</a></li>

                        <li><a href="{{ route('admin.manage.video.affiliate.banner.list') }}" class="<?php if((Request::segment(2) == "video-affiliate-banner" ||Request::segment(2) == "video-affiliate-banner-list")){?>active2<?php } ?>">Manage Video Affiliates Banner</a></li>

                        <li><a href="{{ route('admin.manage.video.affiliate.commission') }}" class="<?php if((Request::segment(2) == "video-affiliate-commission")){?>active2<?php } ?>">Commission Percentage</a></li>
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="#" class="waves-effect<?php if((Request::segment(2) == "blog") || (Request::segment(2) == "blog-details") || (Request::segment(2) == "blog-add") || (Request::segment(2) == "blog-edit") || (Request::segment(2) == "blog-category") || (Request::segment(2) == "blog-category-add") || (Request::segment(2) == "blog-category-edit")){?>subdrop active active1<?php } ?>"><i class="md fa fa-rss"></i>
                    <span> Blogs </span> <span class="pull-right"><i class="md md-add"></i></span></a>
                    <ul class="list-unstyled">

                        <li><a href="{{ route('admin.blog.category.index') }}" class="<?php if((Request::segment(2) == "blog-category") || (Request::segment(2) == "blog-category-add") || (Request::segment(2) == "blog-category-edit")){?>active2<?php } ?>">Blog Category</a></li>

                        <li><a href="{{ route('admin.blog.index') }}" class="<?php if((Request::segment(2) == "blog") || (Request::segment(2) == "blog-details") || (Request::segment(2) == "blog-add") || (Request::segment(2) == "blog-edit")){?>active2<?php } ?>">Manage Blog</a></li>

                    </ul>
                </li>

                <li class="has_sub">
                    <a href="#" class="waves-effect<?php if((Request::segment(2) == "update-about-us" || Request::segment(2) == "how-it-works" ||Request::segment(2) == "affiliate-landing-page"||Request::segment(2) == "help"||Request::segment(2) == "product-section"|| Request::segment(2) == "terms-of-services" || Request::segment(2) == "faq" || Request::segment(2) == "privacy-policy" || Request::segment(2) == "footer-management"|| Request::segment(2) == "testimonials"|| Request::segment(2) == "add-testimonial"|| Request::segment(2) == "edit-testimonial" || Request::segment(2) == "newsletter"  || Request::segment(2) == "home-expert-section" || Request::segment(2) == "other-page-contents" || Request::segment(2) == "meta-tag-management" || Request::segment(2) == "landing-page-management" || Request::segment(2) == "home-banner-management" || Request::segment(2) == "refer-content-management" || Request::segment(2) == "product-landing-page" ||Request::segment(2) == "landing-page-landing")){?>subdrop active active1<?php } ?>"><i class="fa fa-newspaper-o" aria-hidden="true"></i>
                    <span> Contents </span> <span class="pull-right"><i class="md md-add"></i></span></a>

                    <ul class="list-unstyled">
                        <li><a href="{{ route('admin.about_us') }}" class="<?php if((Request::segment(2) == "update-about-us")){?>active2<?php } ?>">Manage About-Us</a></li>
                        <li><a href="{{ route('admin.testimonial') }}" class="<?php if((Request::segment(2) == "testimonials" || Request::segment(2) == "add-testimonial" || Request::segment(2) == "edit-testimonial")){?>active2<?php } ?>">Manage Testimonial</a></li>
                        <li><a href="{{ route('admin.how.it.works.update') }}" class="<?php if((Request::segment(2) == "how-it-works")){?>active2<?php } ?>">How It Works</a></li>
                        <li><a href="{{ route('admin.product.section.update') }}" class="<?php if((Request::segment(2) == "product-section")){?>active2<?php } ?>">Products Section</a></li>
                        <li><a href="{{ route('admin.home.expert.section') }}" class="<?php if((Request::segment(2) == "home-expert-section")){?>active2<?php } ?>">Home Professionals Section</a></li>
                        <li><a href="{{ route('landing.page.edit') }}" class="<?php if((Request::segment(2) == "landing-page-management")){?>active2<?php } ?>">Professional Landing Page</a></li>
                        <li><a href="{{ route('other.page.contents') }}" class="<?php if((Request::segment(2) == "other-page-contents")){?>active2<?php } ?>">Other Page Contents</a></li>
                        <li><a href="{{ route('home.banner.management') }}" class="<?php if((Request::segment(2) == "home-banner-management")){?>active2<?php } ?>">Manage Homepage Banners</a></li>
                        <li><a href="{{ route('meta.tag.manage') }}" class="<?php if((Request::segment(2) == "meta-tag-management")){?>active2<?php } ?>">Meta Tag Manager</a></li>
                        <li><a href="{{ route('admin.newsLetter') }}" class="<?php if((Request::segment(2) == "newsletter")){?>active2<?php } ?>">Newsletter</a></li>
                        <li><a href="{{ route('admin.terms.of.services') }}" class="<?php if((Request::segment(2) == "terms-of-services")){?>active2<?php } ?>">Terms Of Services</a></li>
                        <li><a href="{{ route('admin.privacy.policy.update') }}" class="<?php if((Request::segment(2) == "privacy-policy")){?>active2<?php } ?>">Privacy Policy</a></li>
                        <li><a href="{{ route('admin.faq.update') }}" class="<?php if((Request::segment(2) == "faq")){?>active2<?php } ?>">Faq</a></li>
                        <li><a href="{{ route('admin.help.contents') }}" class="<?php if((Request::segment(2) == "help")){?>active2<?php } ?>">Help Section</a></li>
                        <li><a href="{{ route('admin.footer.management.update') }}" class="<?php if((Request::segment(2) == "footer-management")){?>active2<?php } ?>">Footer Managements</a></li>
                        <li><a href="{{ route('admin.refer.content') }}" class="<?php if((Request::segment(2) == "refer-content-management")){?>active2<?php } ?>">Refer Content Managements</a></li>
                        <li><a href="{{ route('admin.product.landing.page') }}" class="<?php if((Request::segment(2) == "product-landing-page")){?>active2<?php } ?>">Manage Product Landing Page</a></li>
                        <li><a href="{{ route('admin.affiliate.landing.page') }}" class="<?php if((Request::segment(2) == "affiliate-landing-page")){?>active2<?php } ?>">Manage Affiliate Landing Page</a></li>
                        <li><a href="{{ route('admin.professional.landing.page') }}" class="<?php if((Request::segment(2) == "landing-page-landing")){?>active2<?php } ?>">Manage Landing Page</a></li>

                    </ul>
                </li>
                <li class="has_sub">
                    <a href="#" class="waves-effect<?php if(Request::segment(2) == "subadmin" || Request::segment(2) == "add-subadmin" || Request::segment(2) == "edit-subadmin"){?>subdrop active active1<?php } ?>"><i class="fa fa-user"></i>
                    <span> Sub Admin </span> <span class="pull-right"><i class="md md-add"></i></span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('subadmin.index') }}" class="<?php if(Request::segment(2) == "subadmin" || Request::segment(2) == "add-subadmin" || Request::segment(2) == "edit-subadmin"){?>active2<?php } ?>"> Sub Admin</a></li>
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="#" class="waves-effect<?php if((Request::segment(2) == "form" || Request::segment(2)=='imported' || Request::segment(2)=='tool-content-template' || Request::segment(2)=='tool-logbook-tools' || Request::segment(2)=='tool-contract-template' || Request::segment(2)=='tool-360-evaluation' || Request::segment(2)=='competences-master' || Request::segment(2)=='evaluation-type' || Request::segment(2) == 'evaluation-category') ){?>subdrop active active1<?php } ?>"><i class="md fa fa-rss"></i>
                    <span> Coaching Tools </span> <span class="pull-right"><i class="md md-add"></i></span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('admin.form.category.manage') }}" class="<?php if(Request::segment(3) == "category"){?>active2<?php } ?>">Tools Category</a></li>
                        <li><a href="{{ route('admin.form.manage') }}" class="<?php if(Request::segment(2) == "form" && Request::segment(3) != "category"){?>active2<?php } ?>">Form Tools</a></li>
                        <li><a href="{{ route('admin.imported.manage') }}" class="<?php if(Request::segment(2) == "imported"){?>active2<?php } ?>">Imported Tools</a></li>
                        <li><a href="{{ route('tool-360-evaluation.index') }}" class="<?php if(Request::segment(2) == "tool-360-evaluation"){?>active2<?php } ?>">360 Evaluation</a></li>
                        <li><a href="{{ route('tool-content-template.index') }}" class="<?php if(Request::segment(2) == "tool-content-template"){?>active2<?php } ?>">Content Template</a></li>
                        <li><a href="{{ route('tool-contract-template.index') }}" class="<?php if(Request::segment(2) == "tool-contract-template"){?>active2<?php } ?>">Contract Template</a></li>
                        <li><a href="{{ route('tool-logbook-tools.index') }}" class="<?php if(Request::segment(2) == "tool-logbook-tools"){?>active2<?php } ?>">Logbook</a></li>
                        <li><a href="{{ route('evaluation.category') }}" class="<?php if(Request::segment(2) == "evaluation-category"){?>active2<?php } ?>">Evaluation Category</a></li>
                        <li><a href="{{ route('evaluation-type.index') }}" class="<?php if(Request::segment(2) == "evaluation-type"){?>active2<?php } ?>">Evaluation Type</a></li>
                        <li><a href="{{ route('competences-master.index') }}" class="<?php if(Request::segment(2) == "competences-master"){?>active2<?php } ?>">Competences Master</a></li>

                    </ul>
                </li>

			</ul>
			<div class="clearfix"></div>
		</div>
        @endif
		<div class="clearfix"></div>
	</div>
</div>
