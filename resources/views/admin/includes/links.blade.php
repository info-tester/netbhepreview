<!-- Fonts -->

<link rel="dns-prefetch" href="//fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">



<!-- Styles -->
<link href="{{URL::asset('public/admin/css/app.css') }}" rel="stylesheet">
<link href="{{URL::asset('public/admin/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{URL::asset('public/admin/plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{URL::asset('public/admin/plugins/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{URL::asset('public/admin/plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{URL::asset('public/admin/plugins/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />

<link href="{{URL::asset('public/admin/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{URL::asset('public/admin/css/core.css') }}" rel="stylesheet" type="text/css">
<link href="{{URL::asset('public/admin/css/icons.css') }}" rel="stylesheet" type="text/css">
<link href="{{URL::asset('public/admin/css/components.css') }}" rel="stylesheet" type="text/css">
<link href="{{URL::asset('public/admin/css/pages.css') }}" rel="stylesheet" type="text/css">
<link href="{{URL::asset('public/admin/css/menu.css') }}" rel="stylesheet" type="text/css">
<link href="{{URL::asset('public/admin/css/responsive.css') }}" rel="stylesheet" type="text/css">
<link href="{{URL::asset('public/admin/css/jquery.toast.css')}}" rel="stylesheet" type="text/css">
<link href="{{URL::asset('public/admin/css/jquery-ui.css')}}" rel="stylesheet" type="text/css">

<!--scripts -->
<script src="{{URL::asset('public/admin/js/jquery.min.js')}}"></script>
<script src="{{URL::asset('public/admin/js/jquery.toast.js')}}"></script>


<style>
    .error {
        color:red !important;
    }
                
</style>
<style>

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
