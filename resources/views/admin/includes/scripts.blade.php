

<script>
  window.Laravel = <?php echo json_encode([
  	'csrfToken' => csrf_token(),
  ]); ?>
</script>


<script src="{{URL::asset('public/admin/js/bootstrap.min.js') }}"></script>
<script src="{{URL::asset('public/admin/js/detect.js') }}"></script>
<script src="{{URL::asset('public/admin/js/fastclick.js') }}"></script>
<script src="{{URL::asset('public/admin/js/jquery.slimscroll.js') }}"></script>
<script src="{{URL::asset('public/admin/js/jquery.blockUI.js') }}"></script>
<script src="{{URL::asset('public/admin/js/jquery-ui.js') }}"></script>
<script src="{{URL::asset('public/admin/js/waves.js') }}"></script>
<script src="{{URL::asset('public/admin/js/wow.min.js') }}"></script>
<script src="{{URL::asset('public/admin/js/jquery.nicescroll.js') }}"></script>
<script src="{{URL::asset('public/admin/js/jquery.scrollTo.min.js') }}"></script>
<script src="{{URL::asset('public/admin/js/modernizr.min.js') }}"></script>
<script src="{{ URL::asset('public/admin/js/jquery.validate.js') }}"></script>

<script>
  var resizefunc = [];
</script>

<script src="{{URL::asset('public/admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{URL::asset('public/admin/plugins/datatables/dataTables.bootstrap.js') }}"></script>
<script src="{{URL::asset('public/admin/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{URL::asset('public/admin/plugins/datatables/buttons.bootstrap.min.js') }}"></script>

<script src="{{URL::asset('public/admin/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{URL::asset('public/admin/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{URL::asset('public/admin/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{URL::asset('public/admin/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{URL::asset('public/admin/plugins/datatables/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{URL::asset('public/admin/plugins/datatables/dataTables.keyTable.min.js') }}"></script>
<script src="{{URL::asset('public/admin/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{URL::asset('public/admin/plugins/datatables/responsive.bootstrap.min.js') }}"></script>
<script src="{{URL::asset('public/admin/plugins/datatables/dataTables.scroller.min.js') }}"></script>

<script src="{{URL::asset('public/admin/pages/datatables.init.js') }}"></script>
<script src="{{URL::asset('public/admin/js/jquery.app.js') }}"></script>
<script src="{{URL::asset('public/admin/js/jquery-ui.js') }}"></script>
<script src="{{ URL::to('public/frontend/tiny_mce/tinymce.min.js') }}"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#datatable_length').hide();
		$('#datatable').dataTable({
			"ordering": false,
			"destroy": true,
			"info":     false,
			"searching": false,
			stateSave: true,
			"dom": '<"toolbar">frtip'
		});

	});
	TableManageButtons.init();
</script>
<script>
	$(function() {
		$("#frm_date").datepicker({dateFormat: "dd-mm-yy",
			changeMonth: true,
			changeYear: true,
			numberOfMonths: 1,
			defaultDate: new Date(),
			onClose: function( selectedDate ) {
				 $( "#to_date").datepicker( "option", "minDate", selectedDate );
			}
		});
		$("#to_date").datepicker({dateFormat: "dd-mm-yy",
			changeMonth: true,
			changeYear: true,
			numberOfMonths: 1,
			onClose: function( selectedDate ) {
					$( "#frm_date" ).datepicker( "option", "maxDate", selectedDate );
			}
		});
	});
</script>
<script>
	$(function() {
		$( "#datepicker" ).datepicker();
	});
</script>
<script>
	$(function() {
		$( "#datepicker1" ).datepicker();
	});
</script>
