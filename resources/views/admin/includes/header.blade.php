<div class="topbar">
  <!-- LOGO -->
  <div class="topbar-left">
    <div class="text-center">
      <a href="{{ route('admin.dashboard') }}" class="logo">
        <img src="{{ URL::to('public/frontend/images/logo.png') }}" class="img-responsive" alt=""><!--<span>Moltran </span>-->
      </a>
    </div>
  </div>
  <!-- Button mobile view to collapse sidebar menu -->
  <div class="navbar navbar-default" role="navigation">
    <div class="container">
      <div class="">
        <div class="pull-left">
          <button class="button-menu-mobile open-left">
          <i class="fa fa-bars"></i>
          </button>
          <span class="clearfix"></span>
        </div>
        
        <ul class="nav navbar-nav navbar-right pull-right">
          
          <li class="dropdown">
            <a href="#" class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="true"><img src="{{ URL::to('public/frontend/images/avatar-1.jpg') }}" alt="user-img" class="img-circle"> </a>
            <ul class="dropdown-menu">
              <li><a href="{{ route('admin.change.password') }}"><i class="md md-settings"></i> Profile</a></li>
              <!--<li><a href="javascript:void(0)"><i class="md md-lock"></i> Lock screen</a></li>-->
              <li><a href="{{ url('/admin/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="md md-settings-power"></i> Logout</a></li>
            </ul>
          </li>
        </ul>
      </div>
      <!--/.nav-collapse -->
    </div>
  </div>
</div>
