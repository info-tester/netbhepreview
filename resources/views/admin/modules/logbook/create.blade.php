@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Add new logbook')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Add new logbook</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('tool-logbook-tools.index') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
				</div>
			</div>
			@include('admin.includes.error')
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
										
											<form id="myform" method="post" action="{{ route('tool-logbook-tools.store') }}" enctype="multipart/form-data">
											
											{{ csrf_field() }}
											<!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Title *</label>
													<input type="text" name="title" class="form-control required" id="title" placeholder="Title" >
												</div>
											</div> -->


											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
                                                <div id="fields">
                                                    <div class="col-md-12 plus_row" id="fieldRow0">
                                                        <div class="row">
                                                            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="your-mail">
                                                                    <label for="exampleInputEmail1">Question *</label>
                                                                    <input type="text" name="question[]" class="personal-type required" id="field_value0" placeholder="Enter your Question">
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                                                                <button type="button" class="btn btn-success add-button addsingleselect" title="Add"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>    
                                            <input type="hidden" class="countvalues" value="1">  

			                                

											
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="margin-top: 10px;">
												<div class="submit-login add_btnm">
													<input value="submit" type="submit" class="btn btn-default">
												</div>
											</div>
											<!--all_time_sho-->
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>


<style type="text/css">
    .upldd {
    display: block;
    width: auto;
    border-radius: 4px;
    text-align: center;
    background: #9caca9;
    cursor: pointer;
    overflow: hidden;
    padding: 10px 15px;
    font-size: 15px;
    color: #fff;
    cursor: pointer;
    float: left;
    margin-top: 15px;
    font-family: 'Poppins', sans-serif;
    position: relative;
}
.upldd input {
    position: absolute;
    font-size: 50px;
    opacity: 0;
    left: 0;
    top: 0;
    width: 200px;
    cursor: pointer;
}
.upldd:hover{
    background: #1781d2;
}

</style>

  <script>
    $(document).ready(function(){
        $("#myform").validate();
    });
</script>
<script>
    $(document).ready(function(){       
        $('body').on('click','.addsingleselect',function(){
        var count = $('.countvalues').val();
            var singleselect = '<div class="col-md-12" id="fieldRow'+count+'"><div class="row"><div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">\
                                                <div class="your-mail">\
                                                    <label for="exampleInputEmail1">Add Question *</label>\
                                                    <input type="text" name="question[]" class="form-control required" id="field_value'+count+'" placeholder="Enter your Question">\
                                                </div>\
                                            </div>\
                                            <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">\
                                                <button type="button" class="btn btn-danger add-button removesingleselect" title="Remove" data-id="'+count+'"><i class="fa fa-minus" aria-hidden="true"></i></button>\
                                            </div></div></div><div class="clearfix"></div>';
            $('#fieldRow0').before(singleselect);                               
        });
        $('body').on('click','.removesingleselect',function(){
            var id = $(this).data('id');
            $('#fieldRow'+id).remove();
        });
    });
</script>


<style>
    .error{
        color: red !important;
    }
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection