@extends('admin.layouts.app')
@section('title', 'Netbhe | Admin | Professional Update')
@section('header')
@include('admin.includes.header')
<link rel="stylesheet" href="{{URL::to('public/admin/css/chosen.css')}}">
<style>
	.email_error{
		color: red !important;
	}
</style>
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Edit Professional Exerience</h4>
                    <div class="submit-login no_mmg pull-right">
                        <a href="{{ route('professional.show',[$user->id]) }}" title="View"><button type="button" class="btn btn-default">View</button></a>
                        <a href="{{ route('professional.index') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
                    </div>
                    <div class="submit-login no_mmg pull-right" style="padding-right:30px !important">
						<a href="{{ route('professional.edit',[$user->id]) }}" title="Edit Professional Details"><button type="button" class="btn btn-default">Details</button></a>
						@if(@$user->country_id == 30)
							<a href="{{ route('admin.professional.edit.bank.account',['id'=>$user->id]) }}" title="Edit Professional Bank Account"><button type="button" class="btn btn-default">Edit Bank Account</button></a>
						@else
							<a href="{{ route('admin.professional.edit.paypal.address',['id'=>$user->id]) }}" title="Edit Professional Paypal Address"><button type="button" class="btn btn-default">Edit Paypal Address</button></a>
						@endif
                        <a href="{{ route('professional.category.edit',[$user->id]) }}" title="Edit Professional Category"><button type="button" class="btn btn-default">Categories</button></a>
                        <a href="{{ route('professional.experience.edit',[$user->id]) }}" title="Edit Professional Experience"><button type="button" class="btn btn-default">Experience</button></a>
                        <a href="{{ route('professional.qualification.edit',[$user->id]) }}" title="Edit Professional Qualification"><button type="button" class="btn btn-default">Qualification</button></a>
						<a href="{{ route('admin.professional.commission',[$user->id]) }}" title="Edit Professional Commission"><button type="button" class="btn btn-default">Commission</button></a>
					</div>
                </div>
                @include('admin.includes.error')
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body table-rep-plugin">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12 nhp">
                                        <div class="table-responsive" data-pattern="priority-columns">
                                            <form id="myform" method="post" action="{{ route('professional.experience.add',[$user->id])}}" enctype="multipart/form-data">
                                                @csrf
                                                <input type="text" name="user_id" value="{{@$user->id}}" hidden>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">@lang('client_site.experience')</label>
                                                        <select name="experience" id="experience" class="required form-control newdrop cat-select">
                                                            <option value="">@lang('client_site.select_option')</option>
                                                            @foreach($experiences as $exp)
                                                                <option value="{{ $exp->id }}" >{{ $exp->experience }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                    <div class="add_btnm submit-login">
                                                        <input value="Add" type="submit" class="btn btn-default top-gap">
                                                    </div>
                                                </div>
                                            </form>

                                            <div class="clearfix"></div>

                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="table-responsive" data-pattern="priority-columns">
                                                    <table id="datatable" class="table table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>Experience</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($my_experiences as $experience)
                                                            <tr>
                                                                <td>{{@$experience->experience}}</td>
                                                                <td>
                                                                    <a href="javascript:void(0)" onclick="deleteCategory('{{route('professional.experience.delete',['id' => $experience->id, 'uid' => $user->id])}}')" title="Delete"> <i class="fa fa-trash-o delet" aria-hidden="true"></i></a>
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Row -->
            </div>
        </div>
        <!-- container -->
	</div>
    <!-- content -->
</div>
<form method="post" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>
<script type="text/javascript" src="{{ URL::to('public/admin/js/chosen.jquery.min.js') }}"></script>
<script type="text/javascript">

    $(document).ready(function(){
    	jQuery(".chosen").chosen();
    	$('.datepicker').datepicker({
    		'dateFormat': 'yy-mm-dd'
    	});
    	$('#email').click(function() {
    		$('#place_error').removeClass("email_error");
            $('#place_error').html('');
    	});

    	// for mobile error message remove
    	$('#mobile').click(function() {
    		$('#place_error_mobile').removeClass("email_error");
            $('#place_error_mobile').html('');
    	});

    	// for email checking
    	$('#email').blur(function() {
            if($('#email').val() != ""){
                var reqData = {
                  	'jsonrpc' : '2.0',
                  	'_token' : '{{csrf_token()}}',
                  	'params' : {
                        'email' : $('#email').val(),
                        'id' 	: '{{$user->id}}'
                    }
                };
             	$.ajax({
                    url: "{{ route('admin.check.email.change') }}",
                    method: 'post',
                    dataType: 'json',
                    data: reqData,
                    success: function(response){
                        if(response.status == 0){
                            $('#email').val('');
                            $('#place_error').addClass('email_error');
                            $('#place_error').html('Email id already exists.');
                        }
                        else{
                            $('#place_error').removeClass("email_error");
                            $('#place_error').html('');
                        }
                    }
                });
            } else {
                $('#place_error').removeClass("email_error");
                $('#place_error').html('');
            }
        });

    	 $('#myform').submit(function(){
            if($('#language').val()==""){
                $('#lang').text('please select your language');
                return false;
            }
            else{
                $('#lang').text("");
                return true;
            }
        });

        // for mobile checking
        $('#mobile').blur(function(){
            if($('#mobile').val() != ""){
                var reqData = {
                  'jsonrpc' : '2.0',
                  '_token' : '{{csrf_token()}}',
                  'params' : {
                        'mobile' : $('#mobile').val(),
                        'id' 	 : '{{$user->id}}'
                    }
                };
                $.ajax({
                    url: "{{ route('admin.check.mobile.change') }}",
                    method: 'post',
                    dataType: 'json',
                    data: reqData,
                    success: function(response){
                        if(response.status==0){
                            $('#mobile').val("");
                            $('#place_error_mobile').addClass("email_error");
                            $('#place_error_mobile').html("Mobile number already exists.");
                        }
                        else{
                            $('#place_error_mobile').removeClass("email_error");
                            $('#place_error_mobile').html('');
                        }
                    }
                });
            }
            else
            {
                $('#place_error_mobile').removeClass("email_error");
                $('#place_error_mobile').html('');
            }
        });

        // for validation
    	$('#myform').validate({
            rules:{
                "category[]":{
                    required:true
                },
                email:{
                    required:true,
                    email:true
                },
                	zipcode:{
                        // digits:true,
                        required:true,
                        minlength:4,
                        maxlength:10
                    },
                    area_code:{
                        digits:true,
                        minlength:2,
                        maxlength:2
                    },
                    street_number:{
                        digits:true,
                        minlength:3,
                        maxlength:3
                    },
                mobile:{
                    required:true,
                    number:true,
                    maxlength:9,
                    minlength:9
                }
            }
        });

    });

    $('#category').change(function(){
        if($('#category').val()==""){
            $('#subcategory').html("");
        }else{
            var reqData = {
              'jsonrpc' : '2.0',
              '_token' : '{{csrf_token()}}',
              'params' : {
                    'cat' : $('#category').val()
                }
            };
            $.ajax({
                url: "{{ route('fetch.subcat') }}",
                method: 'post',
                dataType: 'json',
                data: reqData,
                success: function(response){
                    if(response.status==1) {
                        var i=0, html="";
                        html = '<option value="">Select Option</option>';
                        for(;i<response.result.length;i++){
                            html+='<option value='+response.result[i].id+'>'+response.result[i].name+'</option>';
                        }
                        $('#subcategory').html(html);
                        }
                }, error: function(error) {
                    console.error(error);
                }
            });
        }
    });

    $('#country').change(function(){
		if($('#country').val()!=""){
			var reqData = {
				'jsonrpc' : '2.0',
				'_token' : '{{csrf_token()}}',
				'params' : {
					'cn' : $('#country').val()
				}
			};
			$.ajax({
				url: "{{ route('admin.get.state') }}",
				method: 'post',
				dataType: 'json',
				data: reqData,
				success: function(response){
					if(response.status==1) {
						var i=0, html="";
						html = '<option value=""> Select state </option>';
						for(;i<response.result.length;i++){
							html+='<option value='+response.result[i].id+'>'+response.result[i].name+'</option>';
						}
						$('#state').html(html);
						$('#state').addClass('valid');
					}
					else{

					}
				}, error: function(error) {
					console.error(error);
				}
			});
		}
	});

	function deleteCategory(val){
        var confirm = window.confirm('Do you want to delete this experience?');
		if(confirm){
			// $("#destroy").attr('action',val);
			// $("#destroy").submit();
			window.location.href = val;
		}
	}

</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
