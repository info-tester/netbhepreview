@extends('admin.layouts.app')
@section('title', 'Vedic Astro World | Admin | Customer Details')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="pull-left page-title">Customer Detail</h4>
          <div class="submit-login no_mmg pull-right">
            <a href="{{ route('subadmin.index')}}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
          </div>
          <!--<ol class="breadcrumb pull-right">
            <li><a href="#">User Dashboard</a></li>
            <li class="active">Order Detail</li>
            </ol>-->
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-default">
            <!--<div class="panel-heading">
              <h3 class="panel-title">Default Example</h3>
              </div>-->
            <div class="panel-body">
              <div class="row">
                <div class="col-lg-12 col-md-12">
                  <h4 class="pull-left page-title">Basic Info</h4>
                  <div class="order-detail">
                    <!--order-detail start-->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <div class="order-id">
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Name :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <span>{{ @$user->name ? $user->name : 'N.A' }}</span>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Mobile :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <strong>{{ @$user->mobile ? $user->mobile : 'N.A' }}</strong>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Email ID :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <strong>{{ @$user->email ? $user->email : 'N.A' }}</strong>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Gender :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <span>{{ @$user->gender=="M" ? "Male" : "Female" }}</span>
                        </div>
                        <div class="clearfix"></div>
{{--                         <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Birth Time :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <strong>{{ @$user->birth_time ? $user->birth_time : 'N.A' }}</strong>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Birth Place :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <strong>{{ @$user->birth_place ? $user->birth_place : 'N.A' }}</strong>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Currency :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <strong>@if(@$user->currency_id == 1) INR @elseif(@$user->currency_id == 2) USD @elseif(@$user->currency_id == 3) EUR @endif</strong>
                        </div>
                        <div class="clearfix"></div> --}}
                        
                      </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <div class="order-id">
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Profile Picture :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <div style="height: 100px; width: 100px;">

                            <img src="{{ @$user->profile_pic ? URL::to('storage/app/public/uploads/profile_pic/'.$user->profile_pic) : URL::to('public/frontend/images/no_img.png') }}" alt="" style="width: auto;height: 100%;">

                          </div>
                        </div>
                        <div class="clearfix"></div>
                        {{-- <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>PAN :</p>
                        </div>
                        <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5">
                          <strong>{{ @$user->pan ? $user->pan : 'N.A' }}</strong>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Landline No :</p>
                        </div>
                        <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5">
                          <strong>{{ @$user->contact_no ? $user->contact_no : 'N.A' }}</strong>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>TAN :</p>
                        </div>
                        <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5">
                          <strong>{{ @$user->tan ? $user->tan : 'N.A' }}</strong>
                        </div>
                        <div class="clearfix"></div> --}}
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Email Verified :</p>
                        </div>
                        <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5">
                          <strong> 
                            @if($user->is_email_verified == 'Y')
                            {{ 'Yes' }}
                            @elseif($user->is_email_verified == 'N')
                            {{ 'No' }}
                            @endif
                          </strong>
                        </div>
                        <div class="clearfix"></div>
                        {{-- <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Mobile Verified :</p>
                        </div>
                        <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5">
                          <strong> 
                            @if($user->is_mobile_verified == 'Y')
                            {{ 'Yes' }}
                            @elseif($user->is_mobile_verified == 'N')
                            {{ 'No' }}
                            @endif
                          </strong>
                        </div>
                        <div class="clearfix"></div> --}}
                         <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Status :</p>
                        </div>
                        <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5">
                          <strong> 
                          @if($user->status == 'A')
                          {{ 'Active' }}
                          @elseif($user->status == 'I')
                          {{ 'Inactive' }}
                          @elseif($user->status == 'AA')
                          {{ 'Awaiting Approval' }}
                          @elseif($user->status == 'U')
                          {{ 'Unverified' }}
                          @endif
                          </strong>
                        </div>
                        <div class="clearfix"></div>
                        {{-- <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Created On :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          @php
                          $created_on = date("jS M Y", strtotime(@$user->created_at));
                          @endphp
                          <strong>{{ @$created_on ? $created_on : 'N.A' }}</strong>
                        </div> --}}
                        <div class="clearfix"></div>
                        {{-- <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Wallet Balance :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <strong>@if(@$user->currency_id == 1) ₹ @elseif(@$user->currency_id == 2) $ @elseif(@$user->currency_id == 3) € @endif {{ @$user->wallet_balance }}</strong>
                        </div>
                        <div class="clearfix"></div> --}}
                      </div>
                    </div>
                  </div>
                  <!--order-detail end-->
                </div>
                <div class="col-md-12">
                  <!--<i class="fa fa-pencil-square-o cncl" aria-hidden="true"> Edit</i> -->
                  <!--<i class="fa fa-pencil cncl" aria-hidden="true"> Review</i>-->
                </div>
                <div class="clearfix"></div>
              </div>
              {{-- <div class="row">
                <div class="col-lg-12 col-md-12">
                <h4 class="pull-left page-title">Description</h4>
                  <div class="order-detail">
                    <!--order-detail start-->
                    <div class="col-lg-12">
                      <div class="order-id">
                        <div class="col-lg-12">
                          <span>{{ @$user->designation ? $user->designation : 'N.A' }}</span>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                  <!--order-detail end-->
                </div>
                <div class="col-md-12">
                  <!--<i class="fa fa-pencil-square-o cncl" aria-hidden="true"> Edit</i> -->
                  <!--<i class="fa fa-pencil cncl" aria-hidden="true"> Review</i>-->
                </div>
                <div class="clearfix"></div>
              </div> --}}
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End Row -->
  </div>
  <!-- container -->
</div>
<!-- content -->
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection