@extends('admin.layouts.app')
@section('title', 'Netbhe | Admin | Professional Update')
@section('header')
@include('admin.includes.header')
<link rel="stylesheet" href="{{URL::to('public/admin/css/chosen.css')}}">
<style>
	.email_error{
		color: red !important;
	}
</style>
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Edit Professional Details</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('professional.show',[$user->id]) }}" title="View"><button type="button" class="btn btn-default">View</button></a>
						<a href="{{ route('professional.index') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
					<div class="submit-login no_mmg pull-right" style="padding-right:30px !important">
						<a href="{{ route('professional.edit',[$user->id]) }}" title="Edit Professional Details"><button type="button" class="btn btn-default">Details</button></a>
						@if(@$user->country_id == 30)
							<a href="{{ route('admin.professional.edit.bank.account',['id'=>$user->id]) }}" title="Edit Professional Bank Account"><button type="button" class="btn btn-default">Edit Bank Account</button></a>
						@else
							<a href="{{ route('admin.professional.edit.paypal.address',['id'=>$user->id]) }}" title="Edit Professional Paypal Address"><button type="button" class="btn btn-default">Edit Paypal Address</button></a>
						@endif
                        <a href="{{ route('professional.category.edit',[$user->id]) }}" title="Edit Professional Category"><button type="button" class="btn btn-default">Categories</button></a>
                        <a href="{{ route('professional.experience.edit',[$user->id]) }}" title="Edit Professional Experience"><button type="button" class="btn btn-default">Experience</button></a>
                        <a href="{{ route('professional.qualification.edit',[$user->id]) }}" title="Edit Professional Qualification"><button type="button" class="btn btn-default">Qualification</button></a>
						<a href="{{ route('admin.professional.commission',[$user->id]) }}" title="Edit Professional Commission"><button type="button" class="btn btn-default">Commission</button></a>
					</div>
				</div>
				@include('admin.includes.error')
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-body table-rep-plugin">
								<div class="row">
									<div class="col-md-12 col-sm-12 col-xs-12 nhp">
										<div class="table-responsive" data-pattern="priority-columns">
											<form id="myform" method="post" action="{{ route('admin.professional.commission',[$user->id])}}" enctype="multipart/form-data">
												@csrf
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Professional Commission For Video (in %)</label>
														<input type="text" name="video_commission" class="form-control required" value="{{@$user->video_commission}}" placeholder="Enter video call commission">
													</div>
												</div>
                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Professional Commission For Chat (in %)</label>
														<input type="text" name="chat_commission" class="form-control required" value="{{@$user->chat_commission}}" placeholder="Enter chat commission">
													</div>
												</div>
                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Professional Commission For Products (in %)</label>
														<input type="text" name="product_commission" class="form-control required" value="{{@$user->product_commission}}" placeholder="Enter product commission">
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="margin-top:30px;">
													<div class="add_btnm submit-login">
														<input value="Update" type="submit" class="btn btn-default">
													</div>
												</div>

											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- End Row -->
			</div>
			<!-- container -->
		</div>
		<!-- content -->
	</div>
</div>
<form method="post" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>
<script type="text/javascript" src="{{ URL::to('public/admin/js/chosen.jquery.min.js') }}"></script>
<script type="text/javascript">

    $(document).ready(function(){

    	//  $('#myform').submit(function(){
        //     if($('#language').val()==""){
        //         $('#lang').text('please select your language');
        //         return false;
        //     }
        //     else{
        //         $('#lang').text("");
        //         return true;
        //     }
        // });

		jQuery.validator.addMethod("mustBePercent", function(value, element) {
			return this.optional(element) || (value > 0 && value < 100);
		}, "Por favor, dê uma porcentagem válidas");
    	$('#myform').validate({
			rules:{
                "video_commission": "mustBePercent",
                "chat_commission": "mustBePercent",
                "product_commission": "mustBePercent",
			}
		});

    });

</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
