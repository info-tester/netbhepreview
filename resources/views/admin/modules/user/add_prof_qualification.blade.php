@extends('admin.layouts.app')
@section('title', 'Netbhe | Admin | Professional Update')
@section('header')
@include('admin.includes.header')
<link rel="stylesheet" href="{{URL::to('public/admin/css/chosen.css')}}">
<style>
	.email_error{
		color: red !important;
	}
</style>
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Add Professional Qualification</h4>
                    <div class="submit-login no_mmg pull-right">
                        <a href="{{ route('professional.show',[$user->id]) }}" title="View"><button type="button" class="btn btn-default">View</button></a>
                        <a href="{{ route('professional.qualification.edit',[$user->id]) }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
                    </div>
                    <div class="submit-login no_mmg pull-right" style="padding-right:30px !important">
                        <a href="{{ route('professional.edit',[$user->id]) }}" title="Edit Professional Details"><button type="button" class="btn btn-default">Details</button></a>
                        <a href="{{ route('professional.category.edit',[$user->id]) }}" title="Edit Professional Category"><button type="button" class="btn btn-default">Categories</button></a>
                        <a href="{{ route('professional.experience.edit',[$user->id]) }}" title="Edit Professional Experience"><button type="button" class="btn btn-default">Experience</button></a>
                        <a href="{{ route('professional.qualification.edit',[$user->id]) }}" title="Edit Professional Qualification"><button type="button" class="btn btn-default">Qualification</button></a>
                    </div>
                </div>
                @include('admin.includes.error')
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body table-rep-plugin">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12 nhp">
                                        <div class="table-responsive" data-pattern="priority-columns">
                                        <form id="myform" method="post" action="{{ route('professional.qualification.add',[$user->id])}}" enctype="multipart/form-data">
												@csrf
                                                <input type="text" name="user_id" value="{{$user->id}}" hidden>
                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Name</label>
                                                        <p class="add_ttrr">{{ @$user->nick_name ? @$user->nick_name :@$user->name }}</p>
													</div>
												</div>
                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Email</label>
                                                        <p class="add_ttrr">{{ @$user->email }}</p>
													</div>
												</div>
                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Phone</label>
                                                        <p class="add_ttrr">{{ @$user->mobile }}</p>
													</div>
												</div>
                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Degree</label>
                                                        <input type="text" name="degree" class="form-control required" id="degree">
													</div>
												</div>
                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Univeristy</label>
                                                        <input type="text" name="university" class="form-control required" id="university">
													</div>
												</div>
                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">From Month</label>
                                                        <select class="form-control newdrop required" name="from_month" id="from_month">
                                                            <option value="">Select Month</option>
                                                            <option value="1">January</option>
                                                            <option value="2">February</option>
                                                            <option value="3">March</option>
                                                            <option value="4">April</option>
                                                            <option value="5">May</option>
                                                            <option value="6">June</option>
                                                            <option value="7">July</option>
                                                            <option value="8">August</option>
                                                            <option value="9">September</option>
                                                            <option value="10">October</option>
                                                            <option value="11">November</option>
                                                            <option value="12">December</option>
                                                        </select>
													</div>
												</div>
                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">From Year</label>
                                                        @php
                                                            $year = date('Y');
                                                        @endphp
                                                        <select class="form-control newdrop required" name="from_year" id="from_year">
                                                            <option value="">Select Year</option>
                                                            @for($i=1970; $i<=$year; $i++)
                                                                <option value="{{ $i }}" @if(@$update->from_year==$i) selected @endif>{{ $i }}</option>
                                                            @endfor
                                                        </select>
													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Certificate</label>
														<input type="file" name="certificate" value="" placeholder="Certificate" class="form-control required" id="certificate" accept="image/png, image/jpeg, image/gif, image/bmp, image/jpg">
													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<div style="height: 100px; width: 100px;">
								                            <img src="{{URL::to('public/frontend/images/no_img.png')}}" alt="" style="width: auto;height: 100%;" id="certificate_img">
                                                        </div>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="add_btnm submit-login">
														<input value="Save" type="submit" class="btn btn-default">
													</div>
												</div>
												
											</form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Row -->
            </div>
        </div>
        <!-- container -->
    </div>
    <!-- content -->
</div>
<form method="post" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>
<script type="text/javascript" src="{{ URL::to('public/admin/js/chosen.jquery.min.js') }}"></script>
<script type="text/javascript">
	
    $(document).ready(function(){
    	jQuery(".chosen").chosen();
    	$('.datepicker').datepicker({
    		'dateFormat': 'yy-mm-dd'
    	});
    	
        // for validation
    	$('#myform').validate();

    });

    function readURL(input) {
        if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function(e) {
            $('.uploaded_ppc').show();
            $('#certificate_img').attr('src', e.target.result);
        }
        
        reader.readAsDataURL(input.files[0]);
        }
    }

    $("#certificate").change(function() {
        readURL(this);
    });

    $('#category').change(function(){
        if($('#category').val()==""){
            $('#subcategory').html("");
        }else{
            var reqData = {
              'jsonrpc' : '2.0',
              '_token' : '{{csrf_token()}}',
              'params' : {
                    'cat' : $('#category').val()
                }
            };
            $.ajax({
                url: "{{ route('fetch.subcat') }}",
                method: 'post',
                dataType: 'json',
                data: reqData,
                success: function(response){
                    if(response.status==1) {
                        var i=0, html="";
                        html = '<option value="">Select Option</option>';
                        for(;i<response.result.length;i++){
                            html+='<option value='+response.result[i].id+'>'+response.result[i].name+'</option>';
                        }
                        $('#subcategory').html(html);
                        }
                }, error: function(error) {
                    console.error(error);
                }
            });
        }
    });

    $('#country').change(function(){
		if($('#country').val()!=""){
			var reqData = {
				'jsonrpc' : '2.0',
				'_token' : '{{csrf_token()}}',
				'params' : {
					'cn' : $('#country').val()
				}
			};
			$.ajax({
				url: "{{ route('admin.get.state') }}",
				method: 'post',
				dataType: 'json',
				data: reqData,
				success: function(response){
					if(response.status==1) {
						var i=0, html="";
						html = '<option value=""> Select state </option>';
						for(;i<response.result.length;i++){
							html+='<option value='+response.result[i].id+'>'+response.result[i].name+'</option>';
						}
						$('#state').html(html);
						$('#state').addClass('valid');
					}
					else{
						
					} 
				}, error: function(error) {
					console.error(error);
				}
			});
		}
	});

	function deleteCategory(val){
		var confirm = window.confirm('Do you want to delete this qualification?');
		if(confirm){
			// $("#destroy").attr('action',val);
			// $("#destroy").submit();
			window.location.href = val;
		}
	}

</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection