@extends('admin.layouts.app')
@section('title', 'Netbhe | Admin | Professional Update')
@section('header')
@include('admin.includes.header')
<link rel="stylesheet" href="{{URL::to('public/admin/css/chosen.css')}}">
<style>
	.email_error{
		color: red !important;
	}
</style>
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Edit Professional Details</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('professional.show',[$user->id]) }}" title="View"><button type="button" class="btn btn-default">View</button></a>
						@if(@$user->status == 'AA' || @$user->status == 'I' ||@$user->status == 'U')
							<a href="{{ route('professional.inactive')}}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
						@else
							<a href="{{ route('professional.index')}}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
						@endif
					</div>
					<div class="submit-login no_mmg pull-right" style="padding-right:30px !important">
						<a href="{{ route('professional.edit',[$user->id]) }}" title="Edit Professional Details"><button type="button" class="btn btn-default">Details</button></a>
						@if(@$user->country_id == 30)
							<a href="{{ route('admin.professional.edit.bank.account',['id'=>$user->id]) }}" title="Edit Professional Bank Account"><button type="button" class="btn btn-default">Edit Bank Account</button></a>
						@else
							<a href="{{ route('admin.professional.edit.paypal.address',['id'=>$user->id]) }}" title="Edit Professional Paypal Address"><button type="button" class="btn btn-default">Edit Paypal Address</button></a>
						@endif
                        <a href="{{ route('professional.category.edit',[$user->id]) }}" title="Edit Professional Category"><button type="button" class="btn btn-default">Categories</button></a>
                        <a href="{{ route('professional.experience.edit',[$user->id]) }}" title="Edit Professional Experience"><button type="button" class="btn btn-default">Experience</button></a>
                        <a href="{{ route('professional.qualification.edit',[$user->id]) }}" title="Edit Professional Qualification"><button type="button" class="btn btn-default">Qualification</button></a>
						<a href="{{ route('admin.professional.commission',[$user->id]) }}" title="Edit Professional Commission"><button type="button" class="btn btn-default">Commission</button></a>
					</div>
				</div>
				@include('admin.includes.error')
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-body table-rep-plugin">
								<div class="row">
									<div class="col-md-12 col-sm-12 col-xs-12 nhp">
										<div class="table-responsive" data-pattern="priority-columns">
											<form id="myform" method="post" action="{{ route('professional.update',[$user->id])}}" enctype="multipart/form-data">
												<input name="_method" type="hidden" value="PUT">
												@csrf
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Status</label>
														<select class="form-control newdrop required" name="status" id="status">
															<option value="U" @if($user->status == 'U') selected @endif>Unverified</option>
															<option value="AA" @if($user->status == 'AA') selected @endif>Awating Approval</option>
															<option value="A" @if($user->status == 'A') selected @endif>Active</option>
															<option value="I" @if($user->status == 'I') selected @endif>Inactive</option>
														</select>
													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Email Verification</label>
														<select class="form-control newdrop required" name="is_email_verified" id="is_email_verified">
															<option value="Y" @if($user->is_email_verified == 'Y') selected @endif>Yes</option>
															<option value="N" @if($user->is_email_verified == 'N') selected @endif>No</option>
														</select>
													</div>
												</div>

												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Nick Name</label>
														@if(@$user->nick_name)
															<label for="exampleInputEmail1">{{@$user->nick_name}}</label>
														@else
															<input type="text" name="nick_name" class="form-control" id="nick_name" placeholder="Nick Name">
															<h5><p id="place_error_nick_name" class="form-group"></p></h5>
														@endif
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Name</label>
														<input type="text" value="{{ $user->name }}" name="name" class="form-control required" id="name" placeholder="Name">
													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Email</label>
														{{-- <label for="exampleInputEmail1">{{ $astrologer->email }}</label> --}}
														<input type="text" value="{{ $user->email }}" name="email" class="form-control required" id="email" readonly placeholder="Email">
														<h5><p id="place_error" class="form-group"></p></h5>
													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Mobile</label>
														{{-- <label for="exampleInputEmail1">{{ $astrologer->mobile }}</label> --}}
														<input type="text" value="{{ $user->mobile }}" name="mobile" class="form-control required" id="mobile">
                                    					<h5><p id="place_error_mobile" class="form-group"></p></h5>
													</div>
												</div>
                                                <div class="clearfix"></div>

												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Date of birth</label>
														<input type="text" value="{{ $user->dob }}" name="dob" class="form-control required datepicker" id="dob" autocomplete="off">
                                    					<h5><p id="place_error_mobile" class="form-group"></p></h5>
													</div>
												</div>



												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Country</label>
														<select class="form-control newdrop required" name="country" id="country">
															<option value="">--Select Country--</option>
															@foreach(@$country as $cn)
			                                                    {{-- @if(@$cn->id==30) --}}
			                                                        <option value="{{@$cn->id}}" @if($user->country_id ==$cn->id) selected @endif>{{@$cn->name}}</option>
			                                                    {{-- @endif --}}
			                                                @endforeach
														</select>
													</div>
												</div>

												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">State</label>
														<select class="form-control newdrop required" name="state" id="state">
															<option value="">--Select State--</option>
															@foreach(@$state as $cn)
		                                                        @if($user->country_id==$cn->country_id )
		                                                            <option value="{{@$cn->id}}"  @if($user->state_id==$cn->id) selected @endif>{{@$cn->name}}</option>
		                                                        @endif
                                                    		@endforeach
														</select>
													</div>
												</div>
                                                <div class="clearfix"></div>

												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">City</label>
														<input type="text" placeholder="Enter city" class="validate required personal-type" value="{{ $user->city }}" name="city">
													</div>
												</div>

												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Name of the street</label>
														<input type="text" placeholder="Enter name of the street" class="required personal-type" value="{{ $user->street_name }}" name="street_name">
													</div>
												</div>

												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Street number</label>
														<input type="text" placeholder="Enter street number" class="required personal-type" value="{{ $user->street_number >0 ? $user->street_number : "" }}" name="street_number">
													</div>
												</div>
                                                <div class="clearfix"></div>

												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Companion Address</label>
														<input type="text" placeholder="Enter companion Address" class="required personal-type" value="{{ $user->complement }}" name="complement">

													</div>
												</div>

												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">District Name</label>
														<input type="text" placeholder="Enter District Name" class="required personal-type" value="{{ $user->district }}" name="district">

													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Zipcode</label>
														<input type="text" placeholder="Enter Zipcode" class="required personal-type" value="{{ $user->zipcode }}" name="zipcode">

													</div>
												</div>
                                                <div class="clearfix"></div>

												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">CRP No.</label>

														<input type="text" value="{{ $user->crp_no }}" name="crp_no" class="form-control required" id="crp_no" placeholder="CRP no">
                                    					<h5><p id="place_error_mobile" class="form-group"></p></h5>
													</div>
												</div>


												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Gender</label>
														<select class="form-control newdrop required" name="gender" id="gender">
															<option value="M" @if($user->gender == 'M') selected @endif>Male</option>
															<option value="F" @if($user->gender == 'F') selected @endif>Female</option>
														</select>
													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				                                    <div class="your-mail">
				                                        <label class="exampleInputEmail1">Rate Per Time</label>
				                                        <select name="rate" class="required form-control newdrop">
				                                            <option value="">Select</option>
				                                            <option value="H" @if(@$user->rate=="H") selected @endif>Hourly</option>
				                                            <option value="M" @if(@$user->rate=="M") selected @endif>Minute</option>
				                                        </select>
				                                    </div>
				                                </div>
                                                <div class="clearfix"></div>

				                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				                                    <div class="your-mail">
				                                        <label class="exampleInputEmail1">Consultant Fees</label>

				                                        <input type="text" name="rate_price" id="rate_price" class="form-control required number" value="{{ @$user->rate_price>0 ? @$user->rate_price: "" }}" placeholder="Fees">
				                                    </div>
				                                </div>
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Area Code</label>
														 <input type="text" placeholder="Enter area code" class="validate required form-control" value="{{ $user->area_code >0 ? $user->area_code: ""}}" name="area_code">
													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Acitvate profile</label>
														<select name="profile_active" class="required newdrop form-control">
															<option value="Y" @if($user->profile_active=="Y") selected @endif>Yes</option>
															<option value="N" @if($user->profile_active=="N") selected @endif>No</option>
														</select>
													</div>
												</div>
                                                <div class="clearfix"></div>
												<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				                                    <div class="your-mail">
				                                        <label class="exampleInputEmail1">Time Zone</label>
				                                        <select class="required form-control" name="time_zone">
				                                            <option value="">select timezone</option>
				                                            @foreach($time as $tm)
				                                                <option value="{{ @$tm->timezone_id }}" @if(@$tm->timezone_id==$user->timezone_id) selected @endif>{{ @$tm->timezone_name }}</option>
				                                            @endforeach
				                                        </select>
				                                    </div>
				                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" @if(@$user->country_id!=30) style="display: none"  @endif id="cpf_no_show" >
													<div class="your-mail">
														<label for="exampleInputEmail1">CPF</label>
														<input type="text" value="{{ $user->cpf_no }}" name="cpf_no" class="form-control required" id="cpf_no">
                                    					<h5><p id="place_error_mobile" class="form-group"></p></h5>
													</div>
												</div>
												<div class="clearfix"></div>
												{{-- @dd($lang_id); --}}
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Language</label>
														<select name="language[]" id="language" class="required form-control chosen" multiple="true">
			                                            @foreach(@$lang as $ln)
			                                                <option value="{{ @$ln->id }}"@if(sizeof($lang_id)>0)  @if(in_array(@$ln->id, $lang_id)) selected @endif @endif>{{ @$ln->name }}</option>
			                                            @endforeach
			                                        </select>
			                                        <p id="lang" class="error"></p>

													</div>
												</div>

												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Profile Picture</label>
														<input type="file" name="profile_pic" value="" placeholder="Profile Picture" class="form-control" id="profile_pic" accept="image/png, image/jpeg, image/gif, image/bmp, image/jpg">
													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<div style="height: 100px; width: 100px;">
								                            <img src="{{ @$user->profile_pic ? URL::to('storage/app/public/uploads/profile_pic/'.$user->profile_pic) : URL::to('public/frontend/images/no_img.png') }}" alt="" style="width: auto;height: 100%;">
								                          </div>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">About Me</label>
														<textarea name="description" id="desc">{{@$user->description}}</textarea>
													</div>
												</div>
												<div class="clearfix"></div>
												@if($user->is_professional=="Y")
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Free Session</label>
														<input type="number" placeholder="Enter Free Session" class="validate required form-control"
															value="{{ $user->free_session_number}}" name="free_session">
													</div>
												</div>
												@endif
												<div class="clearfix"></div>
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="margin-top:30px;">
													<div class="add_btnm submit-login">
														<input value="Update" type="submit" class="btn btn-default">
													</div>
												</div>

											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- End Row -->
			</div>
			<!-- container -->
		</div>
		<!-- content -->
	</div>
</div>
<form method="post" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>
<script type="text/javascript" src="{{ URL::to('public/admin/js/chosen.jquery.min.js') }}"></script>
<script type="text/javascript">

    $(document).ready(function(){
    	jQuery(".chosen").chosen();
    	$('.datepicker').datepicker({
    		'dateFormat': 'yy-mm-dd',
			autoclose:true,
			maxDate: new Date()
    	});
    	$('#email').click(function() {
    		$('#place_error').removeClass("email_error");
            $('#place_error').html('');
    	});

    	// for mobile error message remove
    	$('#mobile').click(function() {
    		$('#place_error_mobile').removeClass("email_error");
            $('#place_error_mobile').html('');
    	});

    	// for email checking
    	$('#email').blur(function() {
            if($('#email').val() != ""){
                var reqData = {
                  	'jsonrpc' : '2.0',
                  	'_token' : '{{csrf_token()}}',
                  	'params' : {
                        'email' : $('#email').val(),
                        'id' 	: '{{$user->id}}'
                    }
                };
             	$.ajax({
                    url: "{{ route('admin.check.email.change') }}",
                    method: 'post',
                    dataType: 'json',
                    data: reqData,
                    success: function(response){
                        if(response.status == 0){
                            $('#email').val('');
                            $('#place_error').addClass('email_error');
                            $('#place_error').html('Email id already exists.');
                        }
                        else{
                            $('#place_error').removeClass("email_error");
                            $('#place_error').html('');
                        }
                    }
                });
            } else {
                $('#place_error').removeClass("email_error");
                $('#place_error').html('');
            }
        });

    	 $('#myform').submit(function(){
            if($('#language').val()==""){
                $('#lang').text('please select your language');
                return false;
            }
            else{
                $('#lang').text("");
                return true;
            }
        });

        // for mobile checking
        $('#mobile').blur(function(){
            if($('#mobile').val() != ""){
                var reqData = {
                  'jsonrpc' : '2.0',
                  '_token' : '{{csrf_token()}}',
                  'params' : {
                        'mobile' : $('#mobile').val(),
                        'id' 	 : '{{$user->id}}'
                    }
                };
                $.ajax({
                    url: "{{ route('admin.check.mobile.change') }}",
                    method: 'post',
                    dataType: 'json',
                    data: reqData,
                    success: function(response){
                        if(response.status==0){
                            $('#mobile').val("");
                            $('#place_error_mobile').addClass("email_error");
                            $('#place_error_mobile').html("Mobile number already exists.");
                        }
                        else{
                            $('#place_error_mobile').removeClass("email_error");
                            $('#place_error_mobile').html('');
                        }
                    }
                });
            }
            else
            {
                $('#place_error_mobile').removeClass("email_error");
                $('#place_error_mobile').html('');
            }
        });

        // for validation
    	$('#myform').validate({
            rules:{
                "category[]":{
                    required:true
                },
                email:{
                    required:true,
                    email:true
                },
                	zipcode:{
                        // digits:true,
                        required:true,
                        minlength:4,
                        maxlength:10
                    },
                    area_code:{
                        digits:true,
                        minlength:2,
                        maxlength:2
                    },
                    street_number:{
                        digits:true,
                    },
                mobile:{
                    required:true,
                    number:true,
                    maxlength:10,
                    minlength:8
                }
            }
        });

    });

    $('#category').change(function(){
        if($('#category').val()==""){
            $('#subcategory').html("");
        }else{
            var reqData = {
              'jsonrpc' : '2.0',
              '_token' : '{{csrf_token()}}',
              'params' : {
                    'cat' : $('#category').val()
                }
            };
            $.ajax({
                url: "{{ route('fetch.subcat') }}",
                method: 'post',
                dataType: 'json',
                data: reqData,
                success: function(response){
                    if(response.status==1) {
                        var i=0, html="";
                        html = '<option value="">Select Option</option>';
                        for(;i<response.result.length;i++){
                            html+='<option value='+response.result[i].id+'>'+response.result[i].name+'</option>';
                        }
                        $('#subcategory').html(html);
                        }
                }, error: function(error) {
                    console.error(error);
                }
            });
        }
    });

    $('#country').change(function(){
		if($('#country').val()!=""){
			var reqData = {
				'jsonrpc' : '2.0',
				'_token' : '{{csrf_token()}}',
				'params' : {
					'cn' : $('#country').val()
				}
			};
			$.ajax({
				url: "{{ route('admin.get.state') }}",
				method: 'post',
				dataType: 'json',
				data: reqData,
				success: function(response){
					if(response.status==1) {
						var i=0, html="";
						html = '<option value=""> Select state </option>';
						for(;i<response.result.length;i++){
							html+='<option value='+response.result[i].id+'>'+response.result[i].name+'</option>';
						}
						$('#state').html(html);
						$('#state').addClass('valid');
					}
					else{

					}
				}, error: function(error) {
					console.error(error);
				}
			});
            if($('#country').val()==30){
                $('#cpf_no_show').css('display','block');
            }else{
                $('#cpf_no_show').css('display','none');
            }
		}
	});

	$('#nick_name').keyup(function(){
		var reqData = {
			'jsonrpc' : '2.0',
			'_token' : '{{csrf_token()}}',
			'params' : {
				'name' : $('#nick_name').val()
			}
		};
		$.ajax({
			url: "{{ route('admin.chk.nick.name') }}",
			method: 'post',
			dataType: 'json',
			data: reqData,
			success: function(response){
				console.log(response);
				if(response.status==1) {
					$('#nick_name').val("");
					$('#place_error_nick_name').text('Nick name already taken.');
				} else if(response.status==0) {
					$('#place_error_nick_name').text('');
					// toastr.error('Nick name already taken.');
				}

			}, error: function(error) {
				toastr.info('Try again after sometime');
			}
		});
	});

	function deleteCategory(val){
		var type = $(this).data('type');
		var question = "Do you want to delete this " + type + "?";
		var confirm = window.confirm(question);
		if(confirm){
			// $("#destroy").attr('action',val);
			// $("#destroy").submit();
			window.location.href = val;
		}
	}

</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
