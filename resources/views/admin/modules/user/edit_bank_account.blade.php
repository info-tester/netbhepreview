@extends('admin.layouts.app')
@section('title', 'Netbhe | Admin | Professional Update')
@section('header')
@include('admin.includes.header')
<link rel="stylesheet" href="{{URL::to('public/admin/css/chosen.css')}}">
<style>
	.email_error{
		color: red !important;
	}
</style>
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Edit Professional Bank Account</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('professional.show',[$user->id]) }}" title="View"><button type="button" class="btn btn-default">View</button></a>
						<a href="{{ route('professional.index') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
					<div class="submit-login no_mmg pull-right" style="padding-right:30px !important">
						<a href="{{ route('professional.edit',[$user->id]) }}" title="Edit Professional Details"><button type="button" class="btn btn-default">Details</button></a>
						<a href="{{ route('admin.professional.edit.bank.account',['id'=>$user->id]) }}" title="Edit Professional Details"><button type="button" class="btn btn-default">Edit Bank Account</button></a>
                        <a href="{{ route('professional.category.edit',[$user->id]) }}" title="Edit Professional Category"><button type="button" class="btn btn-default">Categories</button></a>
                        <a href="{{ route('professional.experience.edit',[$user->id]) }}" title="Edit Professional Experience"><button type="button" class="btn btn-default">Experience</button></a>
                        <a href="{{ route('professional.qualification.edit',[$user->id]) }}" title="Edit Professional Qualification"><button type="button" class="btn btn-default">Qualification</button></a>
                        <a href="{{ route('admin.professional.commission',[$user->id]) }}" title="Edit Professional Commission"><button type="button" class="btn btn-default">Commission</button></a>
					</div>
				</div>
				@include('admin.includes.error')
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-body table-rep-plugin">
								<div class="row">
									<div class="col-md-12 col-sm-12 col-xs-12 nhp">
										<div class="table-responsive" data-pattern="priority-columns">

											<form id="myform" method="post" action="{{ route('admin.professional.edit.bank.account',['id'=>$user->id])}}" enctype="multipart/form-data">
												<input name="_method" type="hidden" value="PUT">
												@csrf
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Bank Name</label>
														<select class="form-control newdrop required" name="bank_name" id="bank_name">
                                                            <option value="">@lang('client_site.select_your_bank')</option>
															@foreach(@$bankList as $bl)
                                                            <option value="{{@$bl->bank_name}}" @if(@$bl->bank_name==@$bank->bankName) selected @endif>{{@$bl->bank_name}}</option>
                                                            @endforeach
														</select>
													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Bank Number</label>
														<select class="form-control newdrop required" name="bank_number" id="bank_number">
                                                            <option value="">@lang('client_site.select_your_bank')</option>
															@foreach(@$bankList as $bl)
                                                            <option value="{{@$bl->bank_number}}" @if(@$bl->bank_number==@$bank->bank_number) selected @endif>{{'('.@$bl->bank_number.') - '.@$bl->bank_name}}</option>
                                                            @endforeach
														</select>
													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Account Type</label>
														<select class="form-control newdrop required" name="account_type">
															<option value="">@lang('site.account_type')</option>
                                                            <option @if (@$bank->account_type == 'SAVING') selected="" @endif value="SAVING">Poupança</option>
                                                            <option @if (@$bank->account_type == 'CHECKING') selected="" @endif value="CHECKING">Conta corrente</option>
														</select>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Account Number</label>

                                                        <input type="text" placeholder="Account Number" id="account_number" name="account_number" class="required form-control" value="{{@$bank->accountNumber}}">
													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Agency Number</label>
                                                         <input type="text" id="agency_number" placeholder="Agency Number')" name="agency_number" class="required form-control" value="{{@$bank->agencyNumber}}">
													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Account Check Number</label>
                                                        <input type="text" id="account_check_number" placeholder="Account Check Number')" name="account_check_number" class="required form-control" value="{{@$bank->accountCheckNumber}}">
													</div>
												</div>
                                                <div class="clearfix"></div>

												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="margin-top:30px;">
													<div class="add_btnm submit-login">
														<input value="Update" type="submit" class="btn btn-default">
													</div>
												</div>

											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- End Row -->
			</div>
			<!-- container -->
		</div>
		<!-- content -->
	</div>
</div>
<form method="post" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>
<script type="text/javascript" src="{{ URL::to('public/admin/js/chosen.jquery.min.js') }}"></script>
<script type="text/javascript">

    $(document).ready(function(){
    	jQuery(".chosen").chosen();
    	$('.datepicker').datepicker({
    		'dateFormat': 'yy-mm-dd'
    	});
    	$('#email').click(function() {
    		$('#place_error').removeClass("email_error");
            $('#place_error').html('');
    	});

    	// for mobile error message remove
    	$('#mobile').click(function() {
    		$('#place_error_mobile').removeClass("email_error");
            $('#place_error_mobile').html('');
    	});



        // for validation
    	$('#myform').validate({
            rules:{
                    account_number:{
                        digits:true,
                        minlength:1,
                        maxlength:12
                    },
                    account_check_number:{
                        digits:true,
                        minlength:1,
                        maxlength:1
                    },
                    agency_number:{
                        digits:true,
                        // minlength:5,
                        // maxlength:5
                    }

                }
        });

    });

	function deleteCategory(val){
		var type = $(this).data('type');
		var question = "Do you want to delete this " + type + "?";
		var confirm = window.confirm(question);
		if(confirm){
			// $("#destroy").attr('action',val);
			// $("#destroy").submit();
			window.location.href = val;
		}
	}

</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
