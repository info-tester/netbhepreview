@extends('admin.layouts.app')
@section('title', 'Netbhe | Admin | Subadmin Details')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Review to {{ @$astrologer_name }}</h4>
					<div class="submit-login no_mmg pull-right">
			            <a href="{{ route('subadmin.index')}}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
			        </div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="card-body" style="font-size: 20px;">
				                    @if (session()->has('success'))
				                        <div class="alert alert-success" role="alert">
				                            {{ session()->get('success') }}
				                        </div>
				                    @endif
				                    @if (session()->has('error'))
				                        <div class="alert alert-danger" role="alert">
				                            {{ session()->get('error') }}
				                        </div>
				                    @endif
					                @if($errors->any())
					                	@foreach($errors->all() as $err)
					                        <div class="alert alert-danger" role="alert">
					                            {{ $err }}
					                        </div>
					                    @endforeach
					                @endif
				                </div>
								<div class="panel panel-default">
									<div class="panel-body table-rep-plugin">
										<form method="post" action="{{ route('admin.astrologer.review.store')}}" id="myform">
											@csrf
											<div class="information_box">
							                    <div class="information_area">
							                        <div class="row">
							                            <div class="col-lg-12 col-md-12">
							                                <div class="form-group">
							                                    <label class="type_label">Rate you experience</label>
																<div class="rviw_str">
																	<li style="list-style: none;">
																		@for($i=1;$i<=5;$i++)
																		<img id="rmg_{{ $i }}" onclick="rate({{ $i }})" src="{{url('public/images/strr2.png')}}" alt="">
																		@endfor
																	</li>
																</div>
						                                        <h5><p style="color:red" id="rt_error"></p></h5>        
							                                </div>
							                            </div>	                        	
							                            <div class="col-lg-12 col-md-12">
							                                <div class="form-group">
							                                    <label class="type_label">Date</label>
							                                    <input type="text" class="login_type required form-control" placeholder="Date" name="date" value="{{ old('date') }}" id="date" readonly>
							                                </div>
							                            </div>	                        	
							                            <div class="col-lg-12 col-md-12">
							                                <div class="form-group">
							                                    <label class="type_label">Customer Name</label>
							                                    <input type="text" class="login_type required form-control" placeholder="Customer Name" name="customer_name" value="{{ old('customer_name') }}" id="customer_name">
							                                </div>
							                            </div>	                        	
							                            <div class="col-lg-12 col-md-12">
							                                <div class="form-group">
							                                    <label class="type_label">Title</label>
							                                    <input type="text" class="login_type required form-control" placeholder="Title" name="title" value="{{ old('title') }}" id="title">
							                                </div>
							                            </div>
						                                
						                                <div class="col-lg-12 col-md-12">
							                                <div class="form-group">
							                                    <label class="type_label">Review Description</label>
							                                    <textarea class="form-control login_type login_message required" placeholder="Description" id="description" name="description">{{ old('description') }}</textarea>
							                                </div>
							                            </div>
							                        </div>
							                        
							                        {{-- <input type="hidden" name="customer_id" value="{{ Auth::id() }}"> --}}

							                        {{-- <input type="hidden" name="order_id" value="{{ $data['id'] }}"> --}}

							                        <input type="hidden" name="astrologer_id" value="{{ $astrologer_id }}">

							                        <input type="hidden" name="rateing" id="rateing" value="">

							                    </div>
						                        <div class="col-lg-12 col-md-12 submit-login add_btnm" style="margin-top: 9px; margin-left: -9px;">
						                           	<button type="submit" class="login_submit btn btn-default">Rate {{ @$astrologer_name }}</button>
						                        </div>
							                </div>
										</form>
									</div>
								</div>
								{{-- <div class="col-sm-12">
									<h4 class="pull-left page-title">Review List</h4>
								</div>
								<div class="col-md-12 dess5">
									<i class="fa fa-trash-o cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">Delete</span></i>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Customer Name</th>
													<th>Date</th>
													<th>Review Point</th>
													<th>title</th>
													<th>description</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@foreach($review as $row)
												
												<tr>
													<td>{{ @$row->customerName->nick_name ? @$row->customerName->nick_name : @$row->customerName->name}}</td>
													<td>{{ date('jS-M-Y', strtotime(@$row->date)) }}</td>
													<td>{{ @$row->review_point }}</td>
													<td>{{ @$row->title }}</td>
													<td>{!! @$row->description !!}</td>
													<td>
														<a href="javascript:void(0)" onclick="if(confirm('Are you want to delete this review?')){ location.href='{{route('admin.astrologer.review.delete',[$row->id])}}'}" title="Delete"> <i class="fa fa-trash-o delet" aria-hidden="true"></i></a>
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div> --}}
								{{-- <div class="col-md-12 dess5">
									<i class="fa fa fa-eye cncl" aria-hidden="true"> <span class="cncl_oopo">View</span></i>
									<i class="fa fa fa-money cncl" aria-hidden="true"> <span class="cncl_oopo">Payout</span></i>
									<i class="fa fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
									<i class="fa fa-trash-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
									<i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active</span></i>
									<i class="fa fa-times cncl" aria-hidden="true"> <span class="cncl_oopo">Inactive</span></i>
									<i class="fa fa-envelope-square cncl" aria-hidden="true"> <span class="cncl_oopo">Email Verify Link</span></i>
									<i class="fa fa-times-circle cncl" aria-hidden="true"> <span class="cncl_oopo">Reject</span></i>
									<i class="fa fa-check-circle cncl" aria-hidden="true"> <span class="cncl_oopo">Approve</span></i>
									<i class="fa fa-square cncl" aria-hidden="true"> <span class="cncl_oopo"> Not Show in Home Page</span></i>
									<i class="fa fa-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Show in Home Page</span></i>
									<i class="fa fa-star cncl" aria-hidden="true"> <span class="cncl_oopo">Mark as Not Recommended</span></i>
									<i class="fa fa-user cncl" aria-hidden="true"> <span class="cncl_oopo">Payments</span></i>
									<i class="fa fa-registered cncl" aria-hidden="true"> <span class="cncl_oopo">Review</span></i>
									<i class="fa fa-star-o cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">Mark as Recommended</span></i>

								</div> --}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>

<script>
	var j="";
	var front=0;
	var rear=5;
    $(document).ready(function(){
        // for validation
        /*$('#checkiz').click(function(){
	    	if($('#checkiz'). prop("checked") == true){
	    		$('.pdtl').show();
	    	}
	    	if($('#checkiz'). prop("checked") == false){
	    		$('.pdtl').hide();
	    	}
        });*/

    	$('#myform').validate({
            /*rules:{
                pan:{
                    required:true,
                    maxlength:10,
                    minlength:10
                },
                "service[]":{
                    required:true
                },
                "skills[]":{
                    required:true
                },
                "category[]":{
                    required:true
                },
                "language[]":{
                    required:true
                },
                email:{
                    required:true,
                    email:true
                },
                mobile:{
                    required:true,
                    number:true,
                    maxlength:10,
                    minlength:10
                }
            },*/
         	errorPlacement:function(error, element){
            }
        });
    });

    $( function() {
        $( "#date" ).datepicker({ dateFormat: 'yy-mm-dd',changeYear: true,yearRange: '1945:'+(new Date).getFullYear() });

        // $( "#partner_dob" ).datepicker({ dateFormat: 'yy-mm-dd',changeYear: true,yearRange: '1945:'+(new Date).getFullYear() });
    });

    /*function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }*/

    /*$("#image").change(function() {
        readURL(this);
        $('#preview').parent('span').show();
    });

    $('#image').change(function(){
	    var fullPath = document.getElementById("image").src;
	    var filename = fullPath.replace(/^.*[\\\/]/, '');
	    document.getElementsByClassName("img_label").value = "Upload ("+filename+")";  
    });*/
    function rate(x){
    	// alert(x);
    	$('#rt_error').text("");
    	if(front<x){
    		for(;front<=x;front++){
				$('#rmg_'+front).attr('src','{{ URL::to('public/images/sstar.png') }}');
			}
    	}
    	else{

    		for(;front>x;front--){
				
				$('#rmg_'+front).attr('src','{{ URL::to('public/images/strr2.png') }}');
			}
			 
    	}
    	$('#rateing').val(x);
    }
    $('#myform').submit(function(event){
    	if($('#rateing').val()<1 || $('#rateing').val()==""){
    		event.preventDefault();
    		$('#rt_error').text("Please select your rating");
    	}
    })
</script>

@endsection
@section('footer')
@include('admin.includes.footer')
@endsection