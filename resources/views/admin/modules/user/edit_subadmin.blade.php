@extends('admin.layouts.app')
@section('title', 'Netbhe | Admin | Edit Sub-Admin')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Edit Subadmin</h4>
                    <div class="submit-login no_mmg pull-right">
                        <a href="{{ route('subadmin.index') }}"><button type="button" class="btn btn-default">Back</button></a>
                    </div>
                </div>
            </div>
            @include('admin.includes.error')
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">

                        <div class="panel-body table-rep-plugin">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 nhp">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        {{-- @if(\Auth::user()->user_type == 1) --}}
                                        <form id="myform" method="post" action="{{ route('admin.subadmin.update',['id'=>@$sub->id]) }}">
                                            @csrf
                                            <input type="hidden" name="id" value="{{ Auth::guard('admin')->user()->id }}">
                                            <div class="all_time_sho">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Enter Name</label>
                                                        <input type="text" class="form-control required" placeholder="Name" value="{{ @$sub->name }}" name="name">
                                                    </div>
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Enter Email</label>
                                                        <input type="text" class="form-control required email" id="email" placeholder="Email" value="{{ @$sub->email }}" name="email" >
                                                    </div>

                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">New Password</label>
                                                        <input type="password" class="form-control" id="new_password" placeholder="New Password" name="new_password">
                                                    </div>
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Confirm New Password</label>
                                                        <input type="password" class="form-control" placeholder="Confirm  Password" name="confirm_password" id="confirm_password">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="all_time_sho">
                                                <div class="col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Specify the SubAdmin access</label>
                                                    </div>
                                                </div>
                                                @php
                                                    $selectedAccess = $sub->access->pluck('access_id')->toArray();
                                                @endphp
                                                @foreach ($accesses as $k => $access)
                                                    <div class="col-lg-3 col-md-3 col-sm-12">
                                                        <div class="your-mail">
                                                            <label for="exampleInputEmail1"><b>{{ $access->name }}</b></label>
                                                        </div>
                                                        @foreach ($access->accesses as $acs)
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="checkbox" value="{{ $acs->id . '-' . $access->id }}" id="access_{{ $acs->id }}" name="access[]" {{ in_array($acs->id, $selectedAccess) ? 'checked' : '' }}>
                                                                <label class="form-check-label" for="access_{{ $acs->id }}">
                                                                    {{ $acs->name }}
                                                                </label>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    @if (($k + 1)%4 == 0)
                                                        <div class="clearfix"></div>
                                                    @endif
                                                @endforeach
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="margin-top: 16px;">
                                                <div class="add_btnm submit-login">
                                                    <input value="Save" type="submit" class="btn btn-default">
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- container -->
    </div> <!-- content -->
    <!--<footer class="footer text-right">
        2015 © Moltran.
    </footer>-->
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $("#myform").validate({
            rules  : {
                name                   : {required: true},
                confirm_password       : {equalTo : "#new_password"},
            }
        });

        $('#new_password').change(function(){
            if($('#new_password').val()!=""){
                $('#confirm_password').addClass('required');
            }
            else{
                $('#confirm_password').removeClass('required');
            }
        });
    });
    $('#email').change(function(){
        var email = "{{@$sub->email}}";
        if(email!=$('#email').val()){
            $('#new_password').addClass('required');
            $('#confirm_password').addClass('required');
        }
        else{
            $('#new_password').removeClass('required');
            $('#confirm_password').removeClass('required');
        }
    });
</script>

@endsection

@section('footer')
@include('admin.includes.footer')
@endsection
