@extends('admin.layouts.app')
@section('title', 'Netbhe| Admin | User Details')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="pull-left page-title">{{ $user->is_professional == "Y" ? "Professional Details": "User Details" }}</h4>
          <div class="submit-login no_mmg pull-right">
            @if(@$user->is_professional=="Y")
              <a href="{{ route('professional.edit',[$user->id]) }}" title="Edit"><button type="button" class="btn btn-default">Edit</button></a>
            @endif
            @if(@$user->status == 'AA' || @$user->status == 'I' ||@$user->status == 'U')
              <a href="{{ route('professional.inactive')}}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
            @else
              <a href="{{ route('professional.index')}}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
            @endif

            <center>
                  @if(@$user->is_professional=="Y" && @$user->is_approved=="N" && $profile==0)
                      {{-- {{dd($profile)}} --}}
                    <a href="javascript:void(0);" title="Approve" onclick="submitMy({{@$user->id}});" class="btn btn-default"> <i class="fa fa-check-circle delet" aria-hidden="true"></i>Approve</a>
                  @endif

                  @if($user->status == 'AA' || $user->status == 'U')
                      <a href="{{ route('admin.professional.reject1',[$user->id]) }}" title="Reject" onclick="return confirm('Do you want to reject this Professional?');" class="btn btn-default"> <i class="fa fa-times-circle" aria-hidden="true"></i> Reject</a>
                      
                  @endif
                </center>
                @if($user->status == 'AA' && $user->is_email_verified == 'N')
                      <a href="{{ route('admin.professional.remainder',[$user->id]) }}" title="Send Remender" class="btn btn-default"> <i class="fa fa-envelope-square delet" aria-hidden="true"></i>Send Remainder</a>     
                @endif  
          </div>
          
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-default">
            <!--<div class="panel-heading">
              <h3 class="panel-title">Default Example</h3>
              </div>-->
            <div class="panel-body">
              <div class="row">
                <div class="col-lg-12 col-md-12">
                  
                </div>
                <form method="post" id="myForm2" name="myForm2" action="{{route('admin.professional.create.account')}}">
                  @csrf
                  <input type="hidden" name="profid" id="profid" value="">
              </form>
                <div class="col-lg-12 col-md-12">
                <h4 class="pull-left page-title">Basic Info</h4>
                  <div class="order-detail">
                    <!--order-detail start-->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <div class="order-id">
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Name :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <span>{{ @$user->name ? ( @$user->nick_name ? @$user->nick_name : @$user->name ) : 'N.A' }}</span>
                        </div>
                        
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Mobile :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <strong>{{ @$user->mobile ? $user->mobile : 'N.A' }}</strong>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Email ID :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <strong>{{ @$user->email ? $user->email : 'N.A' }}</strong>
                        </div>
                        {{-- <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Address :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <strong>{{ @$user->address ? $user->address : 'N.A' }}</strong>
                        </div> --}}
                       
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Gender :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <strong> 
                            @if(@$user->gender == 'M')
                              {{ 'Male' }}
                            @elseif(@$user->gender == 'F')
                              {{ 'Female' }}
                            @endif
                          </strong>
                        </div>
                        <div class="clearfix"></div>
                        {{-- <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Experience :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <strong>{{ @$user->experience ? $user->experience.' years' : 'N.A' }}</strong>
                        </div> --}}
                        
                        {{-- <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <span>{{ @$user->highest_degree ? $user->highest_degree : 'N.A' }}</span>
                        </div>
                        <div class="clearfix"></div> --}}
                        {{-- @if(@$user->is_professional=="Y")
                          <div class="clearfix"></div>
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                            <p>Category :</p>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                            <strong>
                              @php
                              @$printedCat = [];
                              $i=0;
                            @endphp
                            @if(sizeof($user->userDetails)>0)
                              @foreach($user->userDetails as $rw)

                              @if(@$rw->parentCategoryName!=null && !in_array(@$rw->parentCategoryName->id, @$printedCat))
                                  @php
                                                 @$printedCat[$i] = @$rw->parentCategoryName->id;
                                                 
                                                @endphp
                                              {{ @$rw->parentCategoryName->name.'->'.@$rw->categoryName->name }}
                                                
                                            @else
                                              <br>{{  @$rw->categoryName->parent_id>0 ? '->': '•'}}{{ @$rw->categoryName->name }}
                                              @php
                                                 @$printedCat[$i] = @$rw->categoryName->id;
                                                 
                                                @endphp
                                            @endif

                                            @php
                                              $i++;
                                            @endphp
                                            @endforeach
                                        @else
                                          <center>__</center>

                                        @endif
                            </strong>
                          </div>
                        @endif --}}
                        @if(@$user->country_id !== 0)
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                            <p>Country :</p>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                             <strong> {{ $user->countryName->name }} <strong> 
                          </div>
                        @endif
                        @if(@$user->state_id !== 0)
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                            <p>State :</p>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                             <strong> {{ $user->stateName->name }} <strong> 
                          </div>
                        @endif
                        @if(@$user->city)
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                            <p>City :</p>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                             <strong> {{ $user->city }} <strong> 
                          </div>
                        @endif
                        @if(@$user->district)
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                            <p>District :</p>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                             <strong> {{ $user->district }} <strong> 
                          </div>
                        @endif
                        @if(@$user->zipcode)
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                            <p>Zip Code :</p>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                             <strong> {{ $user->zipcode }} <strong> 
                          </div>
                        @endif
                        @if(@$user->rate_price !== 0)
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                            <p>Consultant Fees :</p>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                             <strong> {{ $user->rate_price }} <strong> 
                          </div>
                        @endif
                        @if(@$user->rate)
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                            <p>Rate :</p>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                            <strong> 
                              @if($user->rate == 'H')
                                {{ 'Hourly' }}
                              @elseif($user->rate == 'M')
                                {{ 'Minute' }}
                              @endif
                            <strong> 
                          </div>
                        @endif
                        @if(@$user->crp_no)
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                            <p>CRP No :</p>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                             <strong> {{ $user->crp_no }} <strong> 
                          </div>
                        @endif
                        
                        @if(@$user->found_on)
                        <div class="clearfix"></div>
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                            <p>How did you hear about us :</p>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <p>{{$user->found_on}}</p>
                        </div>
                        @endif

                        <div class="clearfix"></div>
                        
                      </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <div class="order-id">
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Profile Picture :</p>
                        </div>
                        <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5">
                          <div style="height: 100px; width: 100px;">
                            <img src="{{ @$user->profile_pic ? URL::to('storage/app/public/uploads/profile_pic/'.$user->profile_pic) : URL::to('public/frontend/images/no_img.png') }}" alt="" style="width: auto;height: 100%;">
                          </div>
                        </div>
                        <div class="clearfix"></div>
                        
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Email Verified   :</p>
                        </div>
                        <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5">
                          <strong> 
                            @if(@$user->is_email_verified == 'Y')
                              {{ 'Yes' }}
                            @elseif(@$user->is_email_verified == 'N')
                              {{ 'No' }}
                            @endif
                          </strong>
                        </div>
                        
                        <div class="clearfix"></div>
                        
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Created On :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          @php
                          $created_on=date("jS-M-Y", strtotime(@$user->created_at));
                          @endphp
                          <strong>{{ @$created_on ? $created_on : 'N.A' }}</strong>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Status :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <strong> 
                            @if(@$user->status == 'A')
                              {{ 'Active' }}
                            @elseif(@$user->status == 'I')
                              {{ 'Inactive' }}
                            @elseif(@$user->is_approved == 'N')
                              {{ 'Awaiting Approval' }}
                            @elseif(@$user->status == 'U')
                              {{ 'Unverifed' }}
                            @endif
                          </strong>
                        </div>
                        @if(@$user->cpf_no)
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                            <p>CPF No :</p>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                             <strong> {{ $user->cpf_no }} <strong> 
                          </div>
                        @endif
                        @if(@$user->area_code !== 0)
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                            <p>Area Code :</p>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                             <strong> {{ $user->area_code }} <strong> 
                          </div>
                        @endif
                         @if(@$user->street_name)
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                            <p>Street Name :</p>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                             <strong> {{ $user->street_name }} <strong> 
                          </div>
                        @endif
                        @if(@$user->street_number && @$user->street_number !== 0)
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                            <p>Street Number :</p>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                             <strong> {{ $user->street_number }} <strong> 
                          </div>
                        @endif
                        @if(@$user->is_company = 'Y')
                          @if(@$user->company_name)
                            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                              <p>Company Name :</p>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                              <strong> {{ @$user->company_name }} <strong> 
                            </div>
                          @endif
                          @if(@$user->cnpj)
                            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                              <p>CNPJ :</p>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                              <strong> {{ @$user->cnpj }} <strong> 
                            </div>
                          @endif
                        @endif
                        @if(@$user->complement)
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                            <p>Complement :</p>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                             <strong> {{ $user->complement }} <strong> 
                          </div>
                        @endif
                        @if($user->userLang->isNotEmpty())
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                            <p>Languages :</p>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                            @foreach($user->userLang as $row)
                              <strong> 
                                {{ $row->languageName->name }} 
                                  @if($loop->remaining !== 0)
                                    {{ ',' }}
                                  @endif
                                <strong> 
                            @endforeach
                          </div>
                        @endif
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="order-id">
                        <div class="col-md-12 no_padd_lft">
                          <p>Identitfication Documents :</p>
                        </div>
                        @if($user->company_data == null && $user->passport == null && $user->cpf == null && $user->proof_of_residence == null)
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p style="color:grey;">No Documents Uploaded</p>
                        </div>
                        @else
                          @if(@$user->company_data)
                            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                              <p>Company Data :</p>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                <strong> <a target="_blank" href="{{ URL::to('storage/app/public/uploads/identity').'/' . @$user->company_data }}">{{@$user->company_data}}</a> <strong> 
                            </div>
                          @endif
                          @if(@$user->passport)
                            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                              <p>Passport :</p>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                <strong> <a target="_blank" href="{{ URL::to('storage/app/public/uploads/identity').'/' . @$user->passport }}">{{@$user->passport}}</a> <strong> 
                            </div>
                          @endif
                          @if(@$user->cpf)
                            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                              <p>CPF :</p>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                <strong> <a target="_blank" href="{{ URL::to('storage/app/public/uploads/identity').'/' . @$user->cpf }}">{{@$user->cpf}}</a> <strong> 
                            </div>
                          @endif
                          @if(@$user->proof_of_residence)
                            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                              <p>Proof of Residence :</p>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                <strong> <a target="_blank" href="{{ URL::to('storage/app/public/uploads/identity').'/' . @$user->proof_of_residence }}">{{@$user->proof_of_residence}}</a> <strong> 
                            </div>
                          @endif
                        @endif
                      </div>
                    </div>
                  <!--order-detail end-->
                </div>
                <div class="col-md-12">
                 
                </div>
                <div class="clearfix"></div>
              </div>

              @if(sizeof($user->userCategories)>0)
              <div class="row">
                  <div class="col-lg-12 col-md-12">
                  <h4 class="pull-left page-title">Categories</h4>
                    <!--order-detail start-->
                    <div class="order-detail">
                      @foreach(@$user->userCategories as $ua)
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                          <div class="order-id">

                            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                              <p>Catgeory :</p>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                              <span>{{ @$ua->categoryName->parent->name }}</span>
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                          <div class="order-id">
                            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                              <p>Subcategory :</p>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                              <span>{{ @$ua->categoryName->name }}</span>
                            </div>
                            <div class="clearfix"></div>
                          </div>
                        </div>
                      @endforeach
                    </div>
                    <!--order-detail end-->
                  </div>
                  <div class="col-md-12">
                   
                  </div>
                  <div class="clearfix"></div>
                </div>
              @endif

              @if(sizeof($user->userAvailiability)>0)
                <div class="row">
                  <div class="col-lg-12 col-md-12">
                  <h4 class="pull-left page-title">Avaliability</h4>
                    <div class="order-detail">
                      <!--order-detail start-->
                      @php
                        $day = [
                                  '1'     =>  'Sunday',
                                  '2'     =>  'Monday',
                                  '3'     =>  'Tuesday',
                                  '4'     =>  'Wednesday',
                                  '5'     =>  'Thrusday',
                                  '6'     =>  'Friday',
                                  '7'     =>  'Saturday'
                              ];
                      @endphp
                      @foreach(@$user->userAvailiability as $ua)
                      @php $ua->toArray() @endphp
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="order-id">
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 ">
                            <p>Day : {{ date('l', strtotime($ua['date'])) }} </p>
                          </div>
                          
                          
                         
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 ">
                            <p>From : {{ date('H:i:s', strtotime($ua['slot_start'])) }}</p>
                          </div>
                          
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 ">
                            <p>To : {{ date('H:i:s', strtotime($ua['slot_end'])) }}</p>
                          </div>
                          
                          
                          
                          <div class="clearfix"></div>
                        </div>
                      </div>
                      @endforeach
                    </div>
                    <!--order-detail end-->
                  </div>
                  <div class="col-md-12">
                   
                  </div>
                  <div class="clearfix"></div>
                </div>
              @endif

              @if(sizeof($user->userExperience)>0)
                <div class="row">
                  <div class="col-lg-12 col-md-12">
                  <h4 class="pull-left page-title">Experience</h4>
                    <div class="order-detail">
                      <!--order-detail start-->
                      @php
                        $day = [
                                  '1'       =>  'January',
                                  '2'       =>  'February',
                                  '3'       =>  'March',
                                  '4'       =>  'April',
                                  '5'       =>  'May',
                                  '6'       =>  'June',
                                  '7'       =>  'July',
                                  '8'       =>  'Auguust',
                                  '9'       =>  'September',
                                  '10'      =>  'October',
                                  '11'      =>  'November',
                                  '12'      =>  'December'
                              ];
                      @endphp
                      @foreach(@$user->userExperience as $ua)
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="order-id">
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                            <p>Organization : {{ @$ua->organization }} </p>
                          </div>

                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                            <p>Role : {{ @$ua->role }} </p>
                          </div>

                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                            <p>Description : {{ @$ua->description }} </p>
                          </div>
                          
                          
                         
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 ">
                            <p>From : {{@$day[@$ua->from_month].'-'.@$ua->from_year }}</p>
                          </div>
                          
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 ">
                            <p>To : {{@$day[@$ua->to_month].'-'.@$ua->to_year }}</p>
                          </div>
                          
                          
                          
                          <div class="clearfix"></div>
                        </div>
                      </div>
                      @endforeach
                    </div>
                    <!--order-detail end-->
                  </div>
                  <div class="col-md-12">
                   
                  </div>
                  <div class="clearfix"></div>
                </div>
              @endif

              @if(sizeof($user->userQualification)>0)
                <div class="row">
                  <div class="col-lg-12 col-md-12">
                  <h4 class="pull-left page-title">Qualification</h4>
                    <div class="order-detail">
                      <!--order-detail start-->
                      
                      @foreach(@$user->userQualification as $ua)
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="order-id">
                          {{-- <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 ">
                            <p>Degree : {{ @$ua->degree.' From '.@$ua->university }} </p>
                          </div>
                          
                          
                         
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 ">
                            <p>From : {{ @$ua->from_month ? date('M-',strtotime(@$ua->from_year.'-'.@$ua->from_month)): '' }}{{ @$ua->from_year ? @$ua->from_year: 'N.A' }}</p>
                          </div>
                          
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 ">
                            <p>To : {{ @$ua->to_month ? date('M-',strtotime(@$ua->to_year.'-'.@$ua->to_month)): '' }}{{ @$ua->to_year ? $ua->to_year: 'N.A' }}</p>
                          </div>  --}}
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 ">
                            <p>Degree : {{ @$ua->degree.' - '.@$ua->university }} {{ @$ua->from_year ?'-'. @$ua->from_year: '' }}</p>
                          </div> 

                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 ">
                            <p>Download : <a href="{{ URL::to('storage/app/public/uploads/user_qualifiaction').'/'.@$ua->attachment }}" download><i class="fa fa-download"></i></a></p>
                          </div>
                          
                          
                          
                          <div class="clearfix"></div>
                        </div>
                      </div>

                      @endforeach
                    </div>
                    <!--order-detail end-->
                  </div>
                  <div class="col-md-12">
                   
                  </div>
                  <div class="clearfix"></div>
                </div>
              @endif
              
              @if(@$user->paypal_address)
              <div class="row">
                <div class="col-lg-12 col-md-12">
                  <h4 class="pull-left page-title">Paypal Information</h4>
                    <div class="order-detail">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="order-id">
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                            <p>Paypal Email Address : {{ @$user->paypal_address }} </p>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                    </div>
                    <!--order-detail end-->
                  </div>
                  <div class="col-md-12">
                   
                  </div>
                  <div class="clearfix"></div>
                </div>
              @endif

              @if(@$user->bankAccount)
                <div class="row">
                  <div class="col-lg-12 col-md-12">
                  <h4 class="pull-left page-title">Bank Information</h4>
                    <div class="order-detail">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="order-id">
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                            <p>Bank Name : {{ @$user->bankAccount->bankName }} </p>
                          </div>
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                            <p>Account Number : {{ @$user->bankAccount->accountNumber }} </p>
                          </div>
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                            <p>Agency Number : {{ @$user->bankAccount->agencyNumber }} </p>
                          </div>
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 ">
                            <p>Bank Number : {{ @$user->bankAccount->bank_number }}</p>
                          </div>
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 ">
                            <p>Account Check Number : {{ @$user->bankAccount->accountCheckNumber }}</p>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                    </div>
                    <!--order-detail end-->
                  </div>
                  <div class="col-md-12">
                   
                  </div>
                  <div class="clearfix"></div>
                </div>
              @endif

              @if(@$user->userCard)
                <div class="row">
                  <div class="col-lg-12 col-md-12">
                  <h4 class="pull-left page-title">Card Information</h4>
                    <div class="order-detail">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="order-id">
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                            <p>Card Ref No. : {{ @$user->userCard->card_id }} </p>
                          </div>
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                            <p>Card Name: {{ @$user->userCard->card_name }} </p>
                          </div>
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                            <p>First six digits : {{ @$user->userCard->first_six }} </p>
                          </div>
                          <div class="col-lg-6 col-md-4 col-sm-5 col-xs-5 ">
                            <p>Last four digits : {{ @$user->userCard->last_four }}</p>
                          </div>
                          {{-- <div class="col-lg-6 col-md-4 col-sm-5 col-xs-5 ">
                            <p>CVC No : {{ @$user->userCard->cvc_no }}</p>
                          </div> --}}
                          <div class="col-lg-6 col-md-4 col-sm-5 col-xs-5 ">
                            <p>Card Brand : {{ @$user->userCard->card_brand }}</p>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                    </div>
                    <!--order-detail end-->
                  </div>
                  <div class="col-md-12">
                   
                  </div>
                  <div class="clearfix"></div>
                </div>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End Row -->
  </div>
  <!-- container -->
</div>
<!-- content -->

</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
<!-- Right Sidebar -->
<!-- /Right-bar -->
</div>
<script>
  function submitMy(id){
    // alert(id);
    $('#profid').val(id);
    $('#myForm2').submit();
  }
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection