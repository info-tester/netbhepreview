@extends('admin.layouts.app')
@section('title', 'Netbhe | Admin | Subadmin')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Subadmin Management</h4>
					<div class="submit-login no_mmg pull-right">
			            <a href="{{ route('admin.subadmin.add')}}" title="Add subadmin"><button type="button" class="btn btn-default">Add</button></a>
			         </div>
				</div>
			</div>
			@include('admin.includes.error')
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="panel panel-default">
									<div class="panel-body table-rep-plugin">
										<form method="get" action="{{ route('subadmin.index')}}" >
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="your-mail">
													<input class="form-control" name="keyword" placeholder="Keyword" value="{{ @$key['keyword'] }}" type="text">
												</div>
											</div>

											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="your-mail">
													<input class="form-control" name="email" placeholder="email" value="{{ @$key['email'] }}" type="text">
												</div>
											</div>
											
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
												<div class="your-mail">
													<select class="form-control newdrop required" name="keyword_status">
														<option value="">Status</option>
														<option value="A" @if(@$key['keyword_status'] == "A") selected @endif >Active</option>
														<option value="I" @if(@$key['keyword_status'] == "I") selected @endif>Inactive</option>
														
													</select>
												</div>
											</div>

											
											
											<div class="clearfix"></div>
											<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
												<div class="submit-login add_btnm pull-right" >
													<input value="Search" class="btn btn-default" type="submit">
												</div>
											</div>
										</form>
									</div>
								</div>
								<div class="col-md-12 dess5">
									{{-- <i class="fa fa fa-eye cncl" aria-hidden="true"> <span class="cncl_oopo">View</span></i> --}}
									<i class="fa fa-trash-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
									<i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active</span></i>
									<i class="fa fa-times cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo" >Inactive</span></i>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Name</th>
													<th>Email</th>
													<th>Status</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@foreach($subadmin as $row)
												<tr>
													<td>{{ @$row->name }}</td>
													<td>{{ @$row->email ? $row->email : 'N.A' }}</td>
													<td>
														@if($row->status == 'A')
															<label class="label label-success">{{ 'Active' }}</label>
														@elseif($row->status == 'I')
															<label class="label label-danger">{{ 'Inactive' }}</label>
														@elseif($row->status == 'U')
															<label class="label label-danger">{{ 'Unverified' }}</label>
														@endif
													</td>
													<td>
														{{-- <a href="{{ route('subadmin.show',[$row->id]) }}" title="View"> <i class="fa fa-eye delet" aria-hidden="true"></i></a> --}}

														<a href="{{ route('admin.subadmin.edit',[$row->id]) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
														
														@if($row->status == 'I')
															<a href="{{ route('admin.subadmin.status',[$row->id]) }}" title="Active" onclick="return confirm('Do you want to active this subadmin?');"> <i class="fa fa-check delet" aria-hidden="true"></i></a>
															<a href="javascript:void(0)" onclick="deleteCategory('{{route('subadmin.destroy',[$row->id])}}')" title="Delete"> <i class="fa fa-trash-o delet" aria-hidden="true"></i></a>
														@else($row->status == 'A')
															<a href="{{ route('admin.subadmin.status',[$row->id]) }}" title="Inactive" onclick="return confirm('Do you want to inactive this subadmin?');"> <i class="fa fa-times delet" aria-hidden="true"></i></a>
															<a href="javascript:void(0)" onclick="deleteCategory('{{route('subadmin.destroy',[$row->id])}}')" title="Delete"> <i class="fa fa-trash-o delet" aria-hidden="true"></i></a>
														@endif

													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

{{-- Modal For Recharge --}}

<div class="modal fade" id="myModal" role="dialog" style="display: none;">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Customer Reacharge</h4>
        </div>
        <div class="modal-body">
        	{{-- <label class="type_label">Inforfamtion of <span id="holder_name"></span></label> --}}
          <div class="row">
          		<div class="col-md-6">

          			<div class="form-group">
	          			<label>Customer Name: <span id="cust_name"></span></label>
	          		</div>
          		</div>

          		<div class="clearfix"></div>
          		<form method="POST" action="{{ route('cust.reacharge') }}" id="my_form" name="my_form">
          			@csrf
          			<input type="hidden" name="customer_id" id="customer_id">
	          		<div class="col-md-6">
	          			<div class="form-group">
		          			<label class="type_label" for="payout">Rechage Amount
		          			<input type="text" class="login_type required digits" placeholder="Recharge Amount" name="amount" id="amount"></label>
		          		</div>
	          		</div>
	          		<div class="col-md-6">
	          			<div class="form-group">
		          			<label class="type_label" for="notes">Notes
		          			<input type="text" class="login_type required" placeholder="Recharge Notes" name="notes" id="notes"></label>
		          		</div>
	          		</div>
	          		<div class="col-lg-12 col-md-12">
	                   	<button type="submit" class="btn btn-info sbmt_clk">Make Recharge</button>
	          		</div>
          		</form>
          </div>
        </div>
      </div>
    </div>
  </div>
<form method="post" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>

<script>
	function deleteCategory(val){
	var confirm = window.confirm('Are you want to delete this subadmin?');
	if(confirm){
		$("#destroy").attr('action',val);
		$("#destroy").submit();
	}
	}

	$('.pyout').click(function(){
		
		$('#payout').val('');
		$('#payout').removeClass('error');
		$('#notes').val('');
		$('#notes').removeClass('error');
		var x=0;
		$('.parent_loader').show();
		setInterval(function(){ $('.parent_loader').hide(); x=1; }, 1000);
		$('#customer_id').val($(this).data('customer-id'));
		$('#cust_name').text($(this).data('customer-name'));
		$('#myModal').show("fade");
	});
	
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#my_form").validate({
			errorPlacement:function(error, element){

			}
		});	
	});
	
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection