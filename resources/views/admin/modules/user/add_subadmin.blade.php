@extends('admin.layouts.app')
@section('title', 'Netbhe | Admin | Add Sub-Admin')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Add New Subadmin</h4>
                    <div class="submit-login no_mmg pull-right">
                        <a href="{{ route('subadmin.index') }}"><button type="button" class="btn btn-default">Back</button></a>
                    </div>
                </div>
            </div>
            @include('admin.includes.error')
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">

                        <div class="panel-body table-rep-plugin">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 nhp">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        {{-- @if(\Auth::user()->user_type == 1) --}}
                                        <form id="myform" method="post" action="{{ route('admin.subadmin.store') }}">
                                            @csrf
                                            <input type="hidden" name="id" value="{{ Auth::guard('admin')->user()->id }}">
                                            <div class="all_time_sho">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Enter Name</label>
                                                        <input type="text" class="form-control required" placeholder="Name" value="" name="name">
                                                    </div>
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Enter Email</label>
                                                        <input type="text" class="form-control required email" placeholder="Email" value="" name="email" >
                                                    </div>

                                                    {{-- <div class="your-mail">
                                                        <label for="exampleInputEmail1">Choose Admin Type</label>

                                                        <select name="admin_type" id="admin_type" class="form-control required">
                                                            <option value="">Admin Type</option>
                                                            <option value="A">Admin</option>
                                                            <option value="S">Sub-Admin</option>
                                                        </select>
                                                    </div> --}}
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">New Password</label>
                                                        <input type="password" class="form-control required" id="new_password" placeholder="New Password" name="new_password">
                                                    </div>
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Confirm New Password</label>
                                                        <input type="password" class="form-control" placeholder="Confirm  Password" name="confirm_password">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="all_time_sho">
                                                <div class="col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Specify the SubAdmin access</label>
                                                    </div>
                                                </div>
                                                @foreach ($accesses as $k => $access)
                                                    <div class="col-lg-3 col-md-3 col-sm-12">
                                                        <div class="your-mail">
                                                            <label for="exampleInputEmail1"><b>{{ $access->name }}</b></label>
                                                        </div>
                                                        @foreach ($access->accesses as $acs)
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="checkbox" value="{{ $acs->id . '-' . $access->id }}" id="access_{{ $acs->id }}" name="access[]">
                                                                <label class="form-check-label" for="access_{{ $acs->id }}">
                                                                    {{ $acs->name }}
                                                                </label>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    @if (($k + 1)%4 == 0)
                                                        <div class="clearfix"></div>
                                                    @endif
                                                @endforeach
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="add_btnm submit-login">
                                                    <input value="Save" type="submit" class="btn btn-default">
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- container -->
    </div> <!-- content -->
    <!--<footer class="footer text-right">
        2015 © Moltran.
    </footer>-->
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $("#myform").validate({
            rules  : {
                name                   : {required: true},
                email                  :{
                    required: true,
                    email:true,
                    remote:{
                      url:"{{route('admin.chk.email')}}",
                      type:"POST",
                      data:{
                          "_token":function(){return "{{csrf_token()}}";},
                      }
                  }
              },
                new_password           :{required: true,minlength:6},
                confirm_password       : {equalTo : "#new_password"},
            },
            messages : {
                email : {
                    remote        : "This Email already exists",
                    
                },              
            },

        });

    });
</script>

@endsection

@section('footer')
@include('admin.includes.footer')
@endsection
