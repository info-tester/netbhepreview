@extends('admin.layouts.app')
@section('title', 'Netbhe | Admin | Professionals & User')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Professional & User Management</h4>
				</div>
			</div>
			@include('admin.includes.error')
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="panel panel-default">
									<div class="panel-body table-rep-plugin">
										<form method="get" action="" id ="userForm">
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="your-mail">
													<input class="form-control" name="keyword" placeholder="Keyword" value="{{ @$key['keyword'] }}" type="text">
												</div>
											</div>


											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="your-mail">
													<input class="form-control" name="email" placeholder="email" value="{{ @$key['email'] }}" type="text">
												</div>
											</div>

											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
												<div class="your-mail">
													<select class="form-control newdrop required" name="status">
														<option value="">Status</option>
														<option value="A" @if(@$key['status'] == "A") selected @endif >Active</option>
														<option value="AA" @if(@$key['status'] == "AA") selected @endif >Awaiting Approval</option>
														<option value="I" @if(@$key['status'] == "I") selected @endif>Inactive</option>
														<option value="U" @if(@$key['status'] == "U") selected @endif >Unverified</option>
													</select>
												</div>
											</div>

											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="your-mail">
													<select class="form-control newdrop required" name="country">
														<option value="">Country</option>
														@foreach($countries as $country)
														<option value="{{$country->id}}" @if(@$key['country'] == $country->id) selected @endif >{{$country->name}}</option>
														@endforeach
													</select>
												</div>
											</div>

											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="your-mail" style="margin-top: 15px;">
													<input type="checkbox" name="profShow" id="profShow" @if(@$key['category'] || @$key['subcategory']) checked @endif>
													<span >Professional</span>
												</div>
											</div>

											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="your-mail" style="margin-top: 15px;">
													<input type="checkbox" name="is_company" id="is_company" @if(@$key['is_company']) checked @endif>
													<span >Company</span>
												</div>
											</div>

											<div id="profDiv" @if(!@$key['category']) style="display: none; @endif">
												<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
													<div class="your-mail">
														<select class="form-control newdrop " name="category" id="category">
															<option value="">Category</option>
															@foreach($category as $row)
															@if(@$row->parent_id<=0)
															<option value="{{$row->id}}" @if(@$key['category'] == $row->id) selected @endif>{{$row->name}}</option>
															@endif
															@endforeach
														</select>
													</div>
												</div>
												@if(@$key['subcategory'])
													<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
														<div class="your-mail">
															<select class="form-control newdrop " name="subcategory" id="subcategory">
																<option value="">Sub-Category</option>
																@foreach($category as $ct)
																	@if($ct->parent_id!=0)
																	<option value="{{ @$ct->id }}"@if(@$key['subcategory']==@$ct->id) selected @endif>{{ @$ct->name }}</option>
																	@endif
																@endforeach
															</select>
														</div>
													</div>
												@else
													<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
													<div class="your-mail">
														<select class="form-control newdrop " name="subcategory" id="subcategory">
															<option value="">Sub-Category</option>

														</select>
													</div>
												</div>
												@endif
											</div>

											<div class="clearfix"></div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="submit-login add_btnm pull-right" style="margin-top: 5px;">
													<input value="Search" class="btn btn-default" type="submit" id="submit">
                                                    <input value="Download User" class="btn btn-default" type="submit" id="saveUser">
												</div>
											</div>
										</form>
									</div>
								</div>
								<div class="col-md-12 dess5">
									<i class="fa fa fa-eye cncl" aria-hidden="true"> <span class="cncl_oopo">View</span></i>

									<i class="fa fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
									<i class="fa fa-trash-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
									<i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active</span></i>
									<i class="fa fa-times cncl" aria-hidden="true"> <span class="cncl_oopo">Inactive</span></i>
									<i class="fa fa-envelope-square cncl" aria-hidden="true"> <span class="cncl_oopo">Email Verify Link</span></i>
									<i class="fa fa-times-circle cncl" aria-hidden="true"> <span class="cncl_oopo">Reject</span></i>
									<i class="fa fa-check-circle cncl" aria-hidden="true"> <span class="cncl_oopo">Approve</span></i>

									<!-- <i class="fa fa-square cncl" aria-hidden="true"> <span class="cncl_oopo"> Not Show in Home Page</span></i>
									<i class="fa fa-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Show in Home Page</span></i>
									<i class="fa fa-shopping-cart" aria-hidden="true"></i><span class="cncl_oopo"> Orders</span></i> -->




								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Details</th>

													<th>Category</th>

													<th>Professional</th>
													<th>Company</th>
													<th>Date of sign up</th>
													<th>Email Verified</th>
													<th>Status</th>
													<th>Show in<br>Home</th>
													{{-- <th>Reco <br> mended</th> --}}
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@foreach($prof as $row)

												<tr>
												<td>{{ "Name: ". ( @$row->nick_name ? @$row->nick_name : @$row->name ) }}<br>{{@$row->email ? "E-mail: ".$row->email : 'N.A'}}<br>{{ @$row->mobile ? "Mobile: "."+".$row->area_code.$row->mobile : 'N.A' }}</td>


													<td>
														@php
															@$printedCat = [];
															$i=0;
														@endphp
														@if(sizeof($row->userDetails)>0)
															@foreach($row->userDetails as $rw)

															@if(@$rw->parentCategoryName!=null && !in_array(@$rw->parentCategoryName->id, @$printedCat))
																	@php
									                            	 @$printedCat[$i] = @$rw->parentCategoryName->id;

										                            @endphp
									                            {{ @$rw->parentCategoryName->name.'->'.@$rw->categoryName->name }}

										                            <br>
								                            @else
								                            	<br>
								                            	{{  @$rw->categoryName->parent_id>0 ? '->': '•'}}{{ @$rw->categoryName->name }}
								                            	@php
									                            	 @$printedCat[$i] = @$rw->categoryName->id;

										                            @endphp
								                            @endif

								                            @php
								                            	$i++;
								                            @endphp
								                            @endforeach
								                        @else
								                        	<center>__</center>

								                        @endif
							                        </td>
													<td>
														@if(@$row->is_professional=="Y")
														Yes
														@else
														No
														@endif
													</td>
													<td>
														@if(@$row->is_company=="Y")
														Yes
														@else
														No
														@endif
													</td>

													<td>{{date('Y-m-d',strtotime(@$row->created_at))}}</td>
													<td>
														@if($row->is_email_verified == 'N')
															<label class="label label-danger">{{ 'No' }}</label>
														@elseif(@$row->is_email_verified == 'Y')
															<label class="label label-success">{{ 'Yes' }}</label>
														@endif
													</td>
													<td>
														@if(@$row->is_approved == 'N')
														<label class="label label-warning">Awaiting <br> Approval</label>
														@elseif(@$row->status == 'U')
														<label class="label label-danger">{{ 'Unverified' }}</label>
														@elseif(@$row->status == 'A')
														<label class="label label-success">{{ 'Active' }}</label>
														@elseif(@$row->status == 'I')
														<label class="label label-danger">{{ 'Inactive' }}</label>
														@endif
													</td>
													<td>
														@if(@$row->shown_in_home == 'N')
														<label class="label label-danger">{{ 'No' }}</label>
														@elseif(@$row->shown_in_home == 'Y')
														<label class="label label-success">{{ 'Yes' }}</label>
														@endif
													</td>

													<td>
														<a href="{{ route('professional.show',[$row->id]) }}" title="View"> <i class="fa fa-eye delet" aria-hidden="true"></i></a>
														@if(@$row->is_professional=="Y")
														<a href="{{ route('professional.edit',[$row->id]) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
														@endif

														@if(@$row->is_professional=="Y" && @$row->is_approved=="N")
															@if(@$row->country_id == 30 && @$row->bankAccount->id>0)
																<a href="#" title="Approve"  onclick="submitMy({{$row->id}});"> <i class="fa fa-check-circle delet" aria-hidden="true"></i></a>
															@elseif(@$row->country_id != 30 && @$row->paypal_address != null)
																<a href="#" title="Approve"  onclick="submitMy({{$row->id}});"> <i class="fa fa-check-circle delet" aria-hidden="true"></i></a>
															@endif
														@elseif(@$row->is_approved=="N")
															<a href="{{route('admin.user.approve',$row->id)}}" title="Approve"> <i class="fa fa-check-circle delet" aria-hidden="true"></i></a>
														@endif

														@if($row->status == 'AA' || $row->status == 'U')
															<a href="{{ route('admin.professional.reject',[$row->id]) }}" title="Reject" onclick="return confirm('Do you want to reject this {{@$row->is_professional=='Y' ? "Professional": "User"}}');"> <i class="fa fa-times-circle" aria-hidden="true"></i></a>

														@else
															<a href="javascript:void(0)" onclick="deleteCategory('{{route('professional.destroy',[$row->id])}}', {{@$row->is_professional=="Y" ? 1: 0}})" title="Delete"> <i class="fa fa-trash-o delet" aria-hidden="true"></i></a>
														@endif
														@if($row->status == 'I')
															<a href="{{ route('admin.professional.status',[$row->id, 'Active']) }}" title="Active" onclick="return confirm('Do you want to active this {{@$row->is_professional=='Y' ? "Professional": "User"}}');"> <i class="fa fa-check delet" aria-hidden="true"></i></a>
														@elseif($row->status == 'A')
															<a href="{{ route('admin.professional.status',[$row->id, 'Inactive']) }}" title="Inactive" onclick="return confirm('Do you want to inactive this {{@$row->is_professional=='Y' ? "Professional": "User"}}');"> <i class="fa fa-times delet" aria-hidden="true"></i></a>

															@if(@$row->is_professional=="Y")
																@if($row->shown_in_home == 'Y')
																	<a href="{{ route('admin.professional.home',[$row->id]) }}" title="Not Show in Home Page" onclick="return confirm('Do you want to Not Show in Home Page this {{@$row->is_professional=='Y' ? "Professional": "User"}}');"> <i class="fa fa-square delet" aria-hidden="true"></i></a>
																@else
																	<a href="{{ route('admin.professional.home',[$row->id]) }}" title="Show in Home Page " onclick="return confirm('Do you want to Show in Home Page this {{@$row->is_professional=='Y' ? "Professional": "User"}}');"> <i class="fa fa-square-o delet" aria-hidden="true"></i></a>
																@endif
															@endif


														@endif
														@if($row->is_email_verified == 'N')
															<a href="{{ route('admin.professional.status',[$row->id, 'Resend']) }}" title="Resend Email Verification Link" onclick="return confirm('Do you want to resend verification email link to this {{@$row->is_professional=='Y' ? "Professional": "User"}}');"> <i class="fa fa-envelope-square delet" aria-hidden="true"></i></a>
														@endif
														@if(@$row->hasProfessionalBooking->token_no)
															<a href="{{ route('admin.user.orders',[$row->id]) }}" title="Orders"> <i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
														@endif
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
{{-- modal for payout --}}
<div class="modal fade" id="myModal" role="dialog" style="display: none;">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Astrologer Payout </h4>
        </div>
        <div class="modal-body">
        	<label class="type_label">Bank Inforfamtion of <span id="holder_name">Xyz ASd</span></label>
          <div class="row">
          		<div class="col-md-3">

          			<div class="form-group">
	          			<label>Bank Name: <span id="bank_name"></span></label>
	          		</div>
          			<div class="form-group">
	          			<label>Account No:<span id="ac_no"></span></label>

	          		</div>
          		</div>
          		<div class="col-md-3">
          			<div class="form-group">
	          			<label>Account Name: <span id="ac_name"></span></label>

	          		</div>
          		</div>
          		<div class="col-md-3">
          			<div class="form-group">
	          			<label>Account IFSC: <span id="ifsc"></span></label>

	          		</div>
          		</div>
          		<div class="col-md-3">
          			<div class="form-group">
	          			<label>PAN No: <span id="pan"></span></label>
	          		</div>
          		</div>
          		<div class="clearfix"></div>
          		<form method="POST" action="{{ route('professional.payout') }}" id="my_form" name="my_form">
          			@csrf
          			<input type="hidden" name="astro_id" id="astro_id">
	          		<div class="col-md-6">
	          			<div class="form-group">
		          			<label class="type_label" for="payout">Payout
		          			<input type="text" class="login_type required digits" placeholder="Payout" name="payout" id="payout"></label>
		          		</div>
	          		</div>
	          		<div class="col-md-6">
	          			<div class="form-group">
		          			<label class="type_label" for="notes">Notes
		          			<input type="text" class="login_type required" placeholder="Notes" name="notes" id="notes"></label>
		          		</div>
	          		</div>
	          		<div class="col-lg-12 col-md-12">
	                   	<button type="submit" class="btn btn-info sbmt_clk">Make Payment</button>
	          		</div>
          		</form>
          		<form method="post" id="myForm2" name="myForm2" action="{{route('admin.professional.create.account')}}">
					@csrf
					<input type="hidden" name="profid" id="profid">
				</form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{{-- modal for payout --}}
<form method="post" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>
<script>
	function deleteCategory(val, type){
	var i = type==1 ? "Professional": "User";
	var confirm = window.confirm('Do you want to delete this '+i);
	if(confirm){
		$("#destroy").attr('action',val);
		$("#destroy").submit();
	}
	}
</script>

<script type="text/javascript">

	$('.pyout').click(function(){
		//$('#myModal').hide();
		$('#payout').val('');
		$('#payout').removeClass('error');
		$('#notes').val('');
		$('#notes').removeClass('error');
		var x=0;
		$('.parent_loader').show();
		setInterval(function(){ $('.parent_loader').hide(); x=1; }, 1000);
		$('#astro_id').val($(this).data('astro-id'));
		var astro_name = $(this).data('astro-name');
		var bank_name = $(this).data('astro-bank-name');
		var ac_name = $(this).data('astro-ac-name');
		var acc_no = $(this).data('astro-ac-no');
		var ifsc = $(this).data('astro-ifsc');
		var pan = $(this).data('astro-pan');
		$('#holder_name').text(astro_name);
		$('#bank_name').text(bank_name);
		$('#ac_no').text(acc_no);
		$('#ifsc').text(ifsc);
		$('#pan').text(pan);
		$('#ac_name').text(ac_name);
		$('#myModal').show("fade");
	});

	$(document).ready(function(){
		$('#my_form').validate({
			errorPlacement:function(e, e){

			}
		});
	});
	$('#category').change(function(){
        if($('#category').val()==""){
            $('#subcategory').html("");
        }else{
            var reqData = {
              'jsonrpc' : '2.0',
              '_token' : '{{csrf_token()}}',
              'params' : {
                    'cat' : $('#category').val()
                }
            };
            $.ajax({
                url: "{{ route('fetch.subcat') }}",
                method: 'post',
                dataType: 'json',
                data: reqData,
                success: function(response){
                    if(response.status==1) {
                        var i=0, html="";
                        html = '<option value="">Select Option</option>';
                        for(;i<response.result.length;i++){
                            html+='<option value='+response.result[i].id+'>'+response.result[i].name+'</option>';
                        }
                        $('#subcategory').html(html);
                        }
                }, error: function(error) {
                    console.error(error);
                }
            });
        }
    });
</script>
<script>
	$('#profShow').click(function(){
		if($('#profShow').prop('checked')==true){
			$('#profDiv').show();
		}
		else{
			$('#profDiv').hide();
			$('#category').val("");
			$('#subcategory').val("");
		}
	});
	function submitMy(id){
		$('#profid').val(id);
		$('#myForm2').submit();
	}
</script>
<script>
    $('#submit').click(function(){
        $('#userForm').attr('action', "{{ route('professional.inactive')}}");
        $('#userForm').submit();
    });
    $('#saveUser').click(function(){
        $('#userForm').attr('action', "{{ route('inactive.user.export')}}");
        $('#userForm').submit();
    });
</script>
@endsection
@section('footer')
@include('admin.includes.footer')

@endsection
