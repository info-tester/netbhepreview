@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Admin | Professional Landing Page')
@section('header')
@include('admin.includes.header')
<style>
    .your-mail input[type='url'] {
        width: 100%;
        height: 40px;
        /*line-height:50px;*/
        float: left;
        padding: 8px;
        margin-bottom: 8px;
        border: 1px solid #CCC;
        border-radius: 0;
        background: #fff !important;
        color: #000;
        font-family: "Open Sans", sans-serif;
        font-size: 14px;
        font-weight: 400;
    }
</style>
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			@include('admin.includes.error')
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Professional Landing Page</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('exp.landing') }}" target="_blank" title="Preview"><button type="button" class="btn btn-default">Preview</button></a>
						<a href="{{ route('meta.tag.manage') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
										<form id="myform_1" method="post" action="{{ route('landing.page.update', ['section' => 1]) }}" enctype="multipart/form-data">
                                            @csrf
                                            <div class="col-lg-12">
                                                <h4 class="pull-left">Top Banner Section</h4>
                                            </div>
											<div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Title *</label>
													<input type="text" class="form-control required" placeholder="Title" name="banner1_title" value="{{ $banner1->title }}">
												</div>
                                            </div>
											<div class="col-lg-6">
												<div class="your-mail">
													<label for="exampleInputEmail1">Button Text *</label>
													<input type="text" class="form-control required" placeholder="Button Text" name="banner1_heading_4" value="{{ $banner1->heading_4 }}">
												</div>
                                            </div>
											<div class="col-lg-6">
												<div class="your-mail">
													<label for="exampleInputEmail1">Button URL *</label>
													<input type="url" class="form-control required" placeholder="Button URL" name="banner1_description_4" value="{{ $banner1->description_4 }}">
												</div>
                                            </div>
											<div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Description *</label>
                                                    <textarea placeholder="Description" name="banner1_description" class="form-control address required">{!! $banner1->description !!}</textarea>
												</div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-6">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Image (Resolution must be at least 900 * 900px, square image recommened)</label>
                                                    <input type="file" name="banner1_image" class="image_input form-control">
                                                    <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$banner1->image) }}" style="height: 150px; width: 150px; object-fit: contain; display: {{ @$banner1->image ? 'block' : 'none' }};" alt="">
                                                </div>
                                            </div>

											<div class="col-lg-12">
												<div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
													<input value="Save" type="submit" class="btn btn-default">
												</div>
											</div>
										</form>

                                        @foreach ($leftSections as $k => $lsec)
                                            <form id="myform_{{ $k + 2 }}" method="post" action="{{ route('landing.page.update', ['section' => ($k + 2)]) }}">
                                                @csrf
                                                <div class="col-lg-12" style="margin-top: 32px;">
                                                    <h4 class="pull-left">{{ $k + 1 }} of the 2 left sections </h4>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Heading *</label>
                                                        <input type="text" class="form-control required" placeholder="Heading" name="left_sections_title[{{ $k + 2 }}]" value="{{ $lsec->title }}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Title *</label>
                                                        <input type="text" class="form-control required" placeholder="Title" name="left_sections_heading_1[{{ $k + 2 }}]" value="{{ $lsec->heading_1 }}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Button Text *</label>
                                                        <input type="text" class="form-control required" placeholder="Button Text" name="left_sections_heading_4[{{ $k + 2 }}]" value="{{ $lsec->heading_4 }}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Button URL *</label>
                                                        <input type="url" class="form-control required" placeholder="Button URL" name="left_sections_description_4[{{ $k + 2 }}]" value="{{ $lsec->description_4 }}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Description *</label>
                                                        <textarea placeholder="Description" name="left_sections_description_1[{{ $k + 2 }}]" class="form-control address required">{!! $lsec->description_1 !!}</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
                                                        <input value="Save" type="submit" class="btn btn-default">
                                                    </div>
                                                </div>
                                            </form>
                                        @endforeach

                                        <form id="myform_4" method="post" action="{{ route('landing.page.update', ['section' => 4]) }}" enctype="multipart/form-data">
                                            @csrf
                                            <div class="col-lg-12" style="margin-top: 32px;">
                                                <h4 class="pull-left">Video Banner </h4>
                                            </div>
                                            <div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Title *</label>
													<input type="text" class="form-control required" placeholder="Title" name="v_title" value="{{ $videoBanner->title }}">
												</div>
                                            </div>
                                            <div class="col-lg-6">
												<div class="your-mail">
													<label for="exampleInputEmail1">Button Text *</label>
													<input type="text" class="form-control required" placeholder="Button Text" name="v_heading_4" value="{{ $videoBanner->heading_4 }}">
												</div>
                                            </div>
											<div class="col-lg-6">
												<div class="your-mail">
													<label for="exampleInputEmail1">Button URL *</label>
													<input type="url" class="form-control required" placeholder="Button URL" name="v_description_4" value="{{ $videoBanner->description_4 }}">
												</div>
                                            </div>
											<div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Description *</label>
                                                    <textarea placeholder="Description" name="v_description" class="form-control address required">{!! $videoBanner->description !!}</textarea>
												</div>
                                            </div>
                                            <div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">YouTube Video URL *</label>
													<input type="url" class="form-control required" placeholder="Title" name="v_image" value="{{ $videoBanner->image }}">
												</div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
                                                    <input value="Save" type="submit" class="btn btn-default">
                                                </div>
                                            </div>
                                        </form>


                                        <div class="col-lg-12" style="margin-top: 32px;">
                                            <h4 class="pull-left">1st Group of 8 Small Sections </h4>
                                        </div>

                                        @foreach ($smallSections1 as $k => $ssec)
                                            <form id="myform_{{ $k + 5 }}" method="post" action="{{ route('landing.page.update', ['section' => $k + 5]) }}" enctype="multipart/form-data">
                                                @csrf
                                                <div class="col-lg-12" style="margin-top: 8px;">
                                                    <h5 class="pull-left">{{ $k + 1 }} of the 8 small sections </h5>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Title *</label>
                                                        <input type="text" class="form-control required" placeholder="Title" name="small_sections_title[{{ $k + 5 }}]" value="{{ $ssec->title }}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Description *</label>
                                                        <textarea placeholder="Description" name="small_sections_description[{{ $k + 5 }}]" class="form-control address required">{!! $ssec->description !!}</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-6">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Image (Resolution must be at least 128 * 128px, square image recommened)</label>
                                                        <input type="file" name="small_sections_image_{{ $k + 5 }}" class="image_input form-control">
                                                        <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$ssec->image) }}" style="height: 150px; width: 150px; object-fit: contain; display: {{ @$ssec->image ? 'block' : 'none' }};" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
                                                        <input value="Save" type="submit" class="btn btn-default">
                                                    </div>
                                                </div>
                                            </form>
                                        @endforeach

                                        <div class="col-lg-12" style="margin-top: 32px;">
                                            <h4 class="pull-left">2nd Group of 8 Small Sections </h4>
                                        </div>

                                        @foreach ($smallSections2 as $k => $ssec)
                                            <form id="myform_{{ $k + 13 }}" method="post" action="{{ route('landing.page.update', ['section' => $k + 13]) }}" enctype="multipart/form-data">
                                                @csrf
                                                <div class="col-lg-12" style="margin-top: 8px;">
                                                    <h5 class="pull-left">{{ $k + 1 }} of the 8 small sections </h5>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Title *</label>
                                                        <input type="text" class="form-control required" placeholder="Title" name="small_sections_title[{{ $k + 13 }}]" value="{{ $ssec->title }}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Description *</label>
                                                        <textarea placeholder="Description" name="small_sections_description[{{ $k + 13 }}]" class="form-control address required">{!! $ssec->description !!}</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-6">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Image (Resolution must be at least 128 * 128px, square image recommened)</label>
                                                        <input type="file" name="small_sections_image_{{ $k + 13 }}" class="image_input form-control">
                                                        <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$ssec->image) }}" style="height: 150px; width: 150px; object-fit: contain; display: {{ @$ssec->image ? 'block' : 'none' }};" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
                                                        <input value="Save" type="submit" class="btn btn-default">
                                                    </div>
                                                </div>
                                            </form>
                                        @endforeach

                                        <form id="myform_21" method="post" action="{{ route('landing.page.update', ['section' => 21]) }}" enctype="multipart/form-data">
                                            @csrf
                                            <div class="col-lg-12" style="margin-top: 32px;">
                                                <h4 class="pull-left">Middle Banner Section</h4>
                                            </div>
											<div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Title *</label>
													<input type="text" class="form-control required" placeholder="Title" name="banner2_title" value="{{ $banner2->title }}">
												</div>
                                            </div>
                                            <div class="col-lg-6">
												<div class="your-mail">
													<label for="exampleInputEmail1">Button Text *</label>
													<input type="text" class="form-control required" placeholder="Button Text" name="banner2_heading_4" value="{{ $banner2->heading_4 }}">
												</div>
                                            </div>
											<div class="col-lg-6">
												<div class="your-mail">
													<label for="exampleInputEmail1">Button URL *</label>
													<input type="url" class="form-control required" placeholder="Button URL" name="banner2_description_4" value="{{ $banner2->description_4 }}">
												</div>
                                            </div>
											<div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Description *</label>
                                                    <textarea placeholder="Description" name="banner2_description" class="form-control address required">{!! $banner2->description !!}</textarea>
												</div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-6">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Image (Resolution must be at least 900 * 900px, square image recommened)</label>
                                                    <input type="file" name="banner2_image" class="image_input form-control">
                                                    <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$banner2->image) }}" style="height: 150px; width: 150px; object-fit: contain; display: {{ @$banner2->image ? 'block' : 'none' }};" alt="">
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
                                                    <input value="Save" type="submit" class="btn btn-default">
                                                </div>
                                            </div>
                                        </form>

                                        @foreach ($lastAreas as $k => $lastSec)
                                            <form id="myform_{{ $k + 22 }}" method="post" action="{{ route('landing.page.update', ['section' => $k + 22]) }}" enctype="multipart/form-data">
                                                @csrf
                                                <div class="col-lg-12" style="margin-top: 8px;">
                                                    <h4 class="pull-left">Bottom Section {{ $k + 1 }} </h4>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Heading *</label>
                                                        <input type="text" class="form-control required" placeholder="Heading" name="last_sections_title[{{ $k + 22 }}]" value="{{ $lastSec->title }}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Title *</label>
                                                        <input type="text" class="form-control required" placeholder="Title" name="last_sections_heading_1[{{ $k + 22 }}]" value="{{ $lastSec->heading_1 }}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Description *</label>
                                                        <textarea placeholder="Description" name="last_sections_description_1[{{ $k + 22 }}]" class="form-control address required">{!! $lastSec->description_1 !!}</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-6">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Small Image (Resolution must be at least 128 * 128px, square image recommened)</label>
                                                        <input type="file" name="last_sections_image_1_{{ $k + 22 }}" class="image_input form-control">
                                                        <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$lastSec->image_1) }}" style="height: 150px; width: 150px; object-fit: contain; display: {{ @$lastSec->image_1 ? 'block' : 'none' }};" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-6">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Big Image (Resolution recommended 800 * 540px)</label>
                                                        <input type="file" name="last_sections_image_{{ $k + 22 }}" class="image_input form-control">
                                                        <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$lastSec->image) }}" style="height: 150px; width: 150px; object-fit: contain; display: {{ @$lastSec->image ? 'block' : 'none' }};" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
                                                        <input value="Save" type="submit" class="btn btn-default">
                                                    </div>
                                                </div>
                                            </form>
                                        @endforeach
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>
<script>
	$(document).ready(function(){
        const regex = new RegExp("(.*?)\.(png|jpg|jpeg)$");
        $('.image_input').on('change', function() {
            const val = $(this).val().toLowerCase();
            if (!(regex.test(val))) {
                $(this).val('');
                alert('Only png and jpeg files are allowed.');
            }
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                const t = $(this);
                reader.onload = function(e) {
                    t.next('img').attr('src', e.target.result).show();
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
        @foreach (range(1, 23) as $i)
		    $("#myform_{{ $i }}").validate();
        @endforeach
	});
</script>
</script>
<style>
	.error {
		color: red !important;
	}
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
