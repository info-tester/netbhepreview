@extends('admin.layouts.app')
@section('title', 'Vedic Astro World | Admin | Content')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Manage Content</h4>
					{{-- <div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.content.create') }}" title="Create"><button type="button" class="btn btn-default">Add Content</button></a>
					</div> --}}
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 dess5">
									<i class="fa fa-pencil-square-o cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">Edit</span></i>
									{{-- <i class="fa fa-trash-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
									<i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active</span></i>
									<i class="fa fa-times cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">Inactive</span></i> --}}
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Content Title</th>
													{{-- <th>Title 1</th> --}}
													<th>Meta Title</th>
													<th>Meta Description</th>
													<th>Content Description</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@foreach($content as $detail)
												<tr>
													<td>{{ @$detail->title }}</td>
													{{-- <td>{{ @$detail->title_1 }}</td> --}}
													<td>{{ @$detail->meta_title }}</td>
													<td>{{ @$detail->meta_desc }}</td>
													<td>{!! substr($detail->description, 0, 500) !!}</td>
													<td>
														<a href="{{ route('admin.content.edit',[@$detail->id]) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<form method="post" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
	var confirm = window.confirm('Are you want to delete this content ?');
	if(confirm){
		$("#destroy").attr('action',val);
		$("#destroy").submit();
	}
 }
</script>

@endsection
@section('footer')
@include('admin.includes.footer')
@endsection