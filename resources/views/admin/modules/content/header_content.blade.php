@extends('admin.layouts.app')
@section('title', 'Vedic Astro World | Admin | Homepage Header Content')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			{{-- <div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Header Content</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.blog.category.index') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
				</div>
			</div> --}}
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
										<form id="myform" method="post" action="{{ route('admin.content.header.save') }}">
											{{ csrf_field() }}

											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Top Header Heading*</label>
													<input type="text" name="top_heading" class="form-control required" id="top_heading" placeholder="Enter Top Header Heading" value="{{ $content->top_heading }}">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Left Header Heading*</label>
													<input type="text" name="left_heading" class="form-control required" id="left_heading" placeholder="Enter Left Header Heading" value="{{ $content->left_heading }}">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Right Header Heading*</label>
													<input type="text" name="right_heading" class="form-control required" id="right_heading" placeholder="Enter Right Header Heading" value="{{ $content->right_heading }}">
												</div>
											</div>

											<div class="clearfix"></div>

											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Bottom Header Heading*</label>
													<input type="text" name="bottom_heading" class="form-control required" id="bottom_heading" placeholder="Enter Bottom Header Heading" value="{{ $content->bottom_heading }}">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Top Header Description*</label>
													<input type="text" name="top_header" class="form-control required" id="top_header" placeholder="Enter Top Header Description" value="{{ $content->top_header }}">
												</div>
											</div>											
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Left Header Description*</label>
													<input type="text" name="left_header" class="form-control required" id="left_header" placeholder="Enter Left Header Description" value="{{ $content->left_header }}">
												</div>
											</div>

											<div class="clearfix"></div>

											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Right Header Description*</label>
													<input type="text" name="right_header" class="form-control required" id="right_header" placeholder="Enter right Header Description" value="{{ $content->right_header }}">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Bottom Header Description*</label>
													<input type="text" name="bottom_header" class="form-control required" id="bottom_header" placeholder="Enter Bottom Header Description" value="{{ $content->bottom_header }}">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">INR Top Price Text*</label>
													<input type="text" name="inr_top_price_text" class="form-control required" id="inr_top_price_text" placeholder="Enter INR Top Price Text" value="{{ $content->inr_top_price_text }}">
												</div>
											</div>

											<div class="clearfix"></div>
											
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">INR Left Price Text*</label>
													<input type="text" name="inr_left_price_text" class="form-control required" id="inr_left_price_text" placeholder="Enter INR Left Price Text" value="{{ $content->inr_left_price_text }}">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">INR Right Price Text*</label>
													<input type="text" name="inr_right_price_text" class="form-control required" id="inr_right_price_text" placeholder="Enter INR Right Price Text" value="{{ $content->inr_right_price_text }}">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">INR Bottom Price Text*</label>
													<input type="text" name="inr_bottom_price_text" class="form-control required" id="inr_bottom_price_text" placeholder="Enter INR Bottom Price Text" value="{{ $content->inr_bottom_price_text }}">
												</div>
											</div>

											<div class="clearfix"></div>
											
											
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">USD Top Price Text*</label>
													<input type="text" name="usd_top_price_text" class="form-control required" id="usd_top_price_text" placeholder="Enter USD Top Price Text" value="{{ $content->usd_top_price_text }}">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">USD Left Price Text*</label>
													<input type="text" name="usd_left_price_text" class="form-control required" id="usd_left_price_text" placeholder="Enter USD Left Price Text" value="{{ $content->usd_left_price_text }}">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">USD Right Price Text*</label>
													<input type="text" name="usd_right_price_text" class="form-control required" id="usd_right_price_text" placeholder="Enter USD Right Price Text" value="{{ $content->usd_right_price_text }}">
												</div>
											</div>

											<div class="clearfix"></div>
											
											
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">USD Bottom Price Text*</label>
													<input type="text" name="usd_bottom_price_text" class="form-control required" id="usd_bottom_price_text" placeholder="Enter USD Bottom Price Text" value="{{ $content->usd_bottom_price_text }}">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">EUR Top Price Text*</label>
													<input type="text" name="eur_top_price_text" class="form-control required" id="eur_top_price_text" placeholder="Enter EUR Top Price Text" value="{{ $content->eur_top_price_text }}">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">EUR Left Price Text*</label>
													<input type="text" name="eur_left_price_text" class="form-control required" id="eur_left_price_text" placeholder="Enter EUR Left Price Text" value="{{ $content->eur_left_price_text }}">
												</div>
											</div>

											<div class="clearfix"></div>

											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">EUR Right Price Text*</label>
													<input type="text" name="eur_right_price_text" class="form-control required" id="eur_right_price_text" placeholder="Enter EUR Right Price Text" value="{{ $content->eur_right_price_text }}">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">EUR Bottom Price Text*</label>
													<input type="text" name="eur_bottom_price_text" class="form-control required" id="eur_bottom_price_text" placeholder="Enter EUR Bottom Price Text" value="{{ $content->eur_bottom_price_text }}">
												</div>
											</div>
											
											<div class="clearfix"></div>

											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="submit-login add_btnm">
													<input value="Update" type="submit" class="btn btn-default">
												</div>
											</div>
											<!--all_time_sho-->
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>
<script>
	$(document).ready(function(){
		$("#myform").validate({
			errorPlacement: function(errors, content) {
				//
			}
		});
	});
</script>
<style>
	.error{
		color: red !important;
	}
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection