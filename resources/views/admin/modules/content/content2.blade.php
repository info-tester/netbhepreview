@extends('admin.layouts.app')
@section('title', 'Vedic Astro World | Admin | Manage Home Page Slider Content')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Manage Home Page Slider Content</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.content.slider.create') }}" title="Create"><button type="button" class="btn btn-default">Add Slider Content</button></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 dess5">
									<i class="fa fa-pencil-square-o cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">Edit</span></i>
									{{-- <i class="fa fa-trash-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
									<i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active</span></i>
									<i class="fa fa-times cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">Inactive</span></i> --}}
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Position</th>
													<th>Title</th>
													<th>Title 1</th>
													<th>Image</th>
													<th>Status</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@foreach($content as $detail)
												<tr>
													<td>{{ @$detail->position }}</td>
													<td>{{ @$detail->title }}</td>
													<td>{{ @$detail->title_1 }}</td>
													<td><img src="{{ url('storage/app/public/uploads/slider/'.@$detail->image) }}" alt="Image" style="height: 100px; width: 100px;"></td>
													<td>@if(@$detail->status == 'A') <label class="label label-success">Active</label> @elseif(@$detail->status == 'I') <label class="label label-danger">Inactive</label> @endif</td>
													<td>
														<a href="{{ route('admin.content.slider.edit',[@$detail->id]) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
														<a href="javascript:void(0)" onclick="if (confirm('Do you want to delete this slider content?')) { location.href='{{route('admin.content.slider.delete',[@$detail->id])}}' } else { return false; };" title="Delete"> <i class="fa fa-trash-o delet" aria-hidden="true"></i></a>
														@if($detail->status == "I")
															<a href="{{ route('admin.content.slider.status',[@$detail->id]) }}" title="Active" onclick="return confirm('Do you want to active this slider content?');"> <i class="fa fa-check delet" aria-hidden="true"></i></a>
														@elseif($detail->status == "A")
															<a href="{{ route('admin.content.slider.status',[@$detail->id]) }}" title="Inactive" onclick="return confirm('Do you want to inactive this slider content?');"> <i class="fa fa-times delet" aria-hidden="true"></i></a>
														@endif
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<form method="post" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
	var confirm = window.confirm('Are you want to delete this content ?');
	if(confirm){
		$("#destroy").attr('action',val);
		$("#destroy").submit();
	}
 }
</script>

@endsection
@section('footer')
@include('admin.includes.footer')
@endsection