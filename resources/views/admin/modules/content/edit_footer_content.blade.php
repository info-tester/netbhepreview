@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Admin | Edit Footer Content')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			@include('admin.includes.error')
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Footer Content</h4>
					<!-- <div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.dashboard') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div> -->
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
										<form id="myform" method="post" action="{{ route('admin.footer.management.update') }}" enctype="multipart/form-data">
											@csrf
											<div class="all_time_sho">
												<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Address*</label>
														<textarea placeholder="Write Address" name="address" id="address" class="form-control address required">{!! @$footerContent->address !!}</textarea>
													</div>
													<p class="error_1" id="cntnt"></p>
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Phone Number*</label>
													<input type="text" class="form-control required" placeholder="Phone Number" name="ph_no" value="{{@$footerContent->ph_no}}">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Whatsapp*</label>
													<input type="text" class="form-control required" placeholder="Whatsapp" name="whatsapp" value="{{@$footerContent->whatsapp}}">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Email*</label>
													<input type="text" class="form-control required" placeholder="Email" name="email" value="{{@$footerContent->email}}">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Facebook*</label>
													<input type="text" class="form-control" placeholder="Facebook" name="facebook" value="{{@$footerContent->facebook}}">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Twitter*</label>
													<input type="text" class="form-control" placeholder="Twitter" name="twitter" value="{{@$footerContent->twitter}}">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Google Plus*</label>
													<input type="text" class="form-control" placeholder="Google Plus" name="google_plus" value="{{@$footerContent->google_plus}}">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Instagram*</label>
													<input type="text" class="form-control" placeholder="Instagram" name="instagram" value="{{@$footerContent->instagram}}">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Linked In*</label>
													<input type="text" class="form-control " placeholder="Linked In" name="linkedin" value="{{@$footerContent->linkedin}}">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Youtube*</label>
													<input type="text" class="form-control " placeholder="Youtube" name="youtube" value="{{@$footerContent->youtube}}">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
													<input value="Update" type="submit" class="btn btn-default">
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>
<script>
    $(document).ready(function(){
       $('#myform').on('submit', function() {
          var editorContent = tinyMCE.get('address').getContent();
          if(editorContent == '') {
              $('.error_1').html('Please write address');
              $('.error_1').css('color', 'red');
              return false;
          } else {
              $('.error_1').html('');
              return true;
          }
      });
    });
 </script>
<script>
	$(document).ready(function(){
		$("#myform").validate();
		tinyMCE.init({
            mode : "specific_textareas",
            editor_selector : "address",
            plugins: [
              'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
              'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
              'save table contextmenu directionality emoticons template paste textcolor'
            ],
            relative_urls : false,
            remove_script_host : false,
            convert_urls : true,
            toolbar: 'insertfile undo redo | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor emoticons',
            images_upload_url: '{{ URL::to('storage/app/public/uploads/content_image/') }}',
            images_upload_handler: function(blobInfo, success, failure) {
                var formD = new FormData();
                formD.append('file', blobInfo.blob(), blobInfo.filename());
                formD.append( "_token", '{{csrf_token()}}');
                $.ajax({
                    url: '{{ route('artical.img.upload') }}',
                    data: formD,
                    type: 'POST',
                    contentType: false,
                    cache: false,
                    processData:false,
                    dataType: 'JSON',
                    success: function(jsn) {
                        if(jsn.status == 'ERROR') {
                            failure(jsn.error);
                        } else if(jsn.status == 'SUCCESS') {
                            success(jsn.location);
                        }
                    }
                });
            }, 
        });
	});
</script>
</script>
<style>
	.error {
		color: red !important;
	}
	.error_1 {
		color: red !important;
	}
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection