@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Admin | Edit Product Landing Page')
@section('header')
@include('admin.includes.header')
<style>
    .your-mail input[type='url'] {
        width: 100%;
        height: 40px;
        /*line-height:50px;*/
        float: left;
        padding: 8px;
        margin-bottom: 8px;
        border: 1px solid #CCC;
        border-radius: 0;
        background: #fff !important;
        color: #000;
        font-family: "Open Sans", sans-serif;
        font-size: 14px;
        font-weight: 400;
    }
</style>
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			@include('admin.includes.error')
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Edit Product Landing Page</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('product.landing.page') }}" target="_blank" title="Preview"><button type="button" class="btn btn-default">Preview</button></a>
						<a href="{{ route('admin.dashboard') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
										<form id="myform_1" method="post" action="{{ route('admin.product.landing.page.update', ['section' => 1]) }}" enctype="multipart/form-data">
                                            @csrf
                                            <div class="col-lg-12">
                                                <h4 class="pull-left">Top Banner Section</h4>
                                            </div>
                                            <div class="col-lg-12 row">
												<div class="your-mail">
                                                    <div class="col-lg-12">
													    <label for="alignment1">Alignment*</label>
                                                    </div>
													<div class="col-md-4">
														<label for="alignment_left">
															<input type="radio" class="required" id="alignment_left" name="alignment" value="L" @if(@$banner1->description_2 == 'L') checked @endif >
															Left
														</label>
													</div>
													<div class="col-md-4 text-center">
														<label for="alignment_center">
															<input type="radio" class="required" id="alignment_center" name="alignment" value="C" @if(@$banner1->description_2 == 'C') checked @endif >
															Center
														</label>
													</div>
													<div class="col-md-4 text-right">
														<label for="alignment_right">
															<input type="radio" class="required" id="alignment_right" name="alignment" value="R" @if(@$banner1->description_2 == 'R') checked @endif >
															Right
														</label>
													</div>
												</div>
											</div>
											<div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Title *</label>
													<input type="text" class="form-control required" placeholder="Title" name="banner1_title" value="{{@$banner1->title}}">
												</div>
                                            </div>
											<div class="col-lg-6">
												<div class="your-mail">
													<label for="exampleInputEmail1">Button Text *</label>
													<input type="text" class="form-control required" placeholder="Button Text" name="banner1_heading_4" value="{{@$banner1->heading_4}}">
												</div>
                                            </div>
											<div class="col-lg-6">
												<div class="your-mail">
													<label for="exampleInputEmail1">Button URL *</label>
													<input type="url" class="form-control required" placeholder="Button URL" name="banner1_description_4" value="{{@$banner1->description_4}}">
												</div>
                                            </div>
											<div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Description *</label>
                                                    <textarea placeholder="Description" name="banner1_description" class="form-control address required">{{@$banner1->description}}</textarea>
												</div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-6">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Image (Resolution must be at least 1600 * 620px, square image recommened)</label>
                                                    <input type="file" name="banner1_image" class="image_input form-control">
                                                    <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$banner1->image) }}" style="height: 150px; width: 150px; object-fit: contain; display: {{ @$banner1->image ? 'block' : 'none' }};" alt="">
                                                </div>
                                            </div>

											<div class="col-lg-12">
												<div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
													<input value="Save" type="submit" class="btn btn-default">
												</div>
											</div>
										</form>


                                        <form id="myform_2" method="post" action="{{ route('admin.product.landing.page.update', ['section' => 2]) }}" enctype="multipart/form-data">
                                            @csrf
                                            <div class="col-lg-12">
                                                <h4 class="pull-left">Second Banner Section</h4>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Title *</label>
                                                    <input type="text" class="form-control required" placeholder="Title" name="second_banner_title" value="{{@$banner1->heading_1}}">
                                                </div>
                                            </div>
                                            
                                            
                                            <div class="col-lg-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Description *</label>
                                                    <textarea placeholder="Description" name="second_banner_description" class="form-control address required">{{@$banner1->description_1}}</textarea>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Short Description *</label>
                                                    <textarea placeholder="Short Description" name="second_banner_short_description" class="form-control address required">{{@$banner1->short_description_1}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-6">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Image (Resolution must be at least 985 * 623px, square image recommened)</label>
                                                    <input type="file" name="second_banner_image" class="image_input form-control">
                                                    <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$banner1->image_1) }}" style="height: 150px; width: 150px; object-fit: contain; display: {{ @$banner1->image_1 ? 'block' : 'none' }};" alt="">
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
                                                    <input value="Save" type="submit" class="btn btn-default">
                                                </div>
                                            </div>
                                        </form>

                                        

                                        <form id="myform_3" method="post" action="{{ route('admin.product.landing.page.update', ['section' => 3]) }}" enctype="multipart/form-data">
                                            @csrf
                                            <div class="col-lg-12">
                                                <h4 class="pull-left">Course Type</h4>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Title *</label>
                                                    <input type="text" class="form-control required" placeholder="Title" name="course_type_title" value="{{@$banner1->heading_2}}">
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
                                                    <input value="Save" type="submit" class="btn btn-default">
                                                </div>
                                            </div>
                                        </form>


                                        @foreach ($smallSections1 as $k => $ssec)
                                            <form id="myform_{{ $k + 5 }}" method="post" action="{{ route('admin.product.landing.page.update', ['section' => $k + 5]) }}" enctype="multipart/form-data">
                                                @csrf
                                                <div class="col-lg-12" style="margin-top: 8px;">
                                                    <h5 class="pull-left">{{ $k + 1 }} of the 7 small sections </h5>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Lesson Type Name *</label>
                                                        <input type="text" class="form-control required" placeholder="Title" name="small_sections_title[{{ $k + 5 }}]" value="{{ $ssec->title }}">
                                                    </div>
                                                </div>
                                                
                                                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-6">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Image (Resolution must be at least 38 * 38px, square image recommened)</label>
                                                        <input type="file" name="small_sections_image_{{ $k + 5 }}" class="image_input form-control">
                                                        <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$ssec->image) }}" style="height: 150px; width: 150px; object-fit: contain; display: {{ @$ssec->image ? 'block' : 'none' }};" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
                                                        <input value="Save" type="submit" class="btn btn-default">
                                                    </div>
                                                </div>
                                            </form>
                                        @endforeach

                                        

                                        <form id="myform_13" method="post" action="{{ route('admin.product.landing.page.update', ['section' => 13]) }}" enctype="multipart/form-data">
                                            @csrf
                                            <div class="col-lg-12">
                                                <h4 class="pull-left">Third Banner Section</h4>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Title *</label>
                                                    <input type="text" class="form-control required" placeholder="Title" name="third_banner_title" value="{{@$banner1->heading_3}}">
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-6">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Image (Resolution must be at least 540 * 430px, square image recommened)</label>
                                                    <input type="file" name="third_banner_image" class="image_input form-control">
                                                    <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$banner1->image_3) }}" style="height: 150px; width: 150px; object-fit: contain; display: {{ @$banner1->image_3 ? 'block' : 'none' }};" alt="">
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
                                                    <input value="Save" type="submit" class="btn btn-default">
                                                </div>
                                            </div>
                                        </form>
                                        
                                        @foreach ($smallSections2 as $k => $ssec)
                                            <form id="myform_{{ $k + 17 }}" method="post" action="{{ route('admin.product.landing.page.update', ['section' => $k + 17]) }}" enctype="multipart/form-data">
                                                @csrf
                                                <div class="col-lg-12" style="margin-top: 8px;">
                                                    <h5 class="pull-left">{{ $k + 1 }} of the 7 small text </h5>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Text {{ $k + 1 }} *</label>
                                                        <input type="text" class="form-control required" placeholder="Title" name="small_sections_title_2_[{{ $k + 17 }}]" value="{{ $ssec->title }}">
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="col-lg-12">
                                                    <div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
                                                        <input value="Save" type="submit" class="btn btn-default">
                                                    </div>
                                                </div>
                                            </form>
                                        @endforeach

                                        <form id="myform_14" method="post" action="{{ route('admin.product.landing.page.update', ['section' => 14]) }}" enctype="multipart/form-data">
                                            @csrf
                                            <div class="col-lg-12">
                                                <h4 class="pull-left">Fourth Banner Section</h4>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Title *</label>
                                                    <input type="text" class="form-control required" placeholder="Title" name="fourth_banner_title" value="{{@$banner1->heading_5}}">
                                                </div>
                                            </div>
                                            
                                            <div class="col-lg-12">
                                                <div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
                                                    <input value="Save" type="submit" class="btn btn-default">
                                                </div>
                                            </div>
                                        </form>

                                        @foreach ($smallSections3 as $k => $ssec)
                                            <form id="myform_{{ $k + 24 }}" method="post" action="{{ route('admin.product.landing.page.update', ['section' => $k + 24]) }}" enctype="multipart/form-data">
                                                @csrf
                                                <div class="col-lg-12" style="margin-top: 8px;">
                                                    <h5 class="pull-left">{{ $k + 1 }} of the 4 small info section </h5>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Title {{ $k + 1 }} *</label>
                                                        <input type="text" class="form-control required" placeholder="Title" name="small_sections_title_3_[{{ $k + 24 }}]" value="{{ $ssec->title }}">
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Description {{ $k + 1 }} *</label>
                                                    <textarea placeholder="Description" name="small_sections_description_3_[{{ $k + 24 }}]" class="form-control address required">{{ $ssec->description }}</textarea>
                                                </div>
                                            </div>

                                                
                                                
                                                <div class="col-lg-12">
                                                    <div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
                                                        <input value="Save" type="submit" class="btn btn-default">
                                                    </div>
                                                </div>
                                            </form>
                                        @endforeach

                                        <form id="myform_15" method="post" action="{{ route('admin.product.landing.page.update', ['section' => 15]) }}" enctype="multipart/form-data">
                                            @csrf
                                            <div class="col-lg-12">
                                                <h4 class="pull-left">Fifth Banner Section</h4>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Title *</label>
                                                    <input type="text" class="form-control required" placeholder="Title" name="fifth_banner_title" value="{{@$banner1->heading_6}}">
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-6">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Image (Resolution must be at least 540 * 430px, square image recommened)</label>
                                                    <input type="file" name="fifth_banner_image" class="image_input form-control">
                                                    <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$banner1->image_6) }}" style="height: 150px; width: 150px; object-fit: contain; display: {{ @$banner1->image_6 ? 'block' : 'none' }};" alt="">
                                                </div>
                                            </div>
                                            
                                            <div class="col-lg-12">
                                                <div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
                                                    <input value="Save" type="submit" class="btn btn-default">
                                                </div>
                                            </div>
                                        </form>

                                         @foreach ($smallSections4 as $k => $ssec)
                                            <form id="myform_{{ $k + 28 }}" method="post" action="{{ route('admin.product.landing.page.update', ['section' => $k + 28]) }}" enctype="multipart/form-data">
                                                @csrf
                                                <div class="col-lg-12" style="margin-top: 8px;">
                                                    <h5 class="pull-left">{{ $k + 1 }} of the 3 small info section </h5>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Title {{ $k + 1 }} *</label>
                                                        <input type="text" class="form-control required" placeholder="Title" name="small_sections_title_4_[{{ $k + 28 }}]" value="{{ $ssec->title }}">
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Description {{ $k + 1 }} *</label>
                                                    <textarea placeholder="Description" name="small_sections_description_4_[{{ $k + 28 }}]" class="form-control address required">{{ $ssec->description }}</textarea>
                                                </div>
                                            </div>

                                                
                                                
                                                <div class="col-lg-12">
                                                    <div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
                                                        <input value="Save" type="submit" class="btn btn-default">
                                                    </div>
                                                </div>
                                            </form>
                                        @endforeach

                                         <form id="myform_16" method="post" action="{{ route('admin.product.landing.page.update', ['section' => 16]) }}" enctype="multipart/form-data">
                                            @csrf
                                            <div class="col-lg-12">
                                                <h4 class="pull-left">Sixth Banner Section</h4>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Title *</label>
                                                    <input type="text" class="form-control required" placeholder="Title" name="sixth_banner_title" value="{{@$banner1->heading_7}}">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Button Text *</label>
                                                    <input type="text" class="form-control required" placeholder="Button Text" name="sixth_banner_description" value="{{@$banner1->description_7}}">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Button URL *</label>
                                                    <input type="url" class="form-control required" placeholder="Button URL" name="sixth_banner_button_url" value="{{@$banner1->button_link_7}}">
                                                </div>
                                            </div>
                                            
                                            <div class="col-lg-12">
                                                <div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
                                                    <input value="Save" type="submit" class="btn btn-default">
                                                </div>
                                            </div>
                                        </form>

                                       
                                       
                                       

                                        
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>
<script>
	$(document).ready(function(){
        const regex = new RegExp("(.*?)\.(png|jpg|jpeg)$");
        $('.image_input').on('change', function() {
            const val = $(this).val().toLowerCase();
            if (!(regex.test(val))) {
                $(this).val('');
                alert('Only png and jpeg files are allowed.');
            }
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                const t = $(this);
                reader.onload = function(e) {
                    t.next('img').attr('src', e.target.result).show();
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
        @foreach (range(1, 30) as $i)
		    $("#myform_{{ $i }}").validate();
        @endforeach
	});
</script>
</script>
<style>
	.error {
		color: red !important;
	}
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
