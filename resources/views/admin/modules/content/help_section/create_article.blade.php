@extends('admin.layouts.app')
@if(@$article)
@section('title', 'Netbhe.com | Admin | Edit Help Article')
@else
@section('title', 'Netbhe.com | Admin | Add Help Article')
@endif
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			@include('admin.includes.error')
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Help Section</h4>
					<div class="submit-login no_mmg pull-right">
						@if(@$article)
							<a target="_blank" href="{{URL::to('central-de-ajuda').'/'.@$article->slug}}" title="Preview"><button type="button" class="btn btn-default">Preview</button></a>
						@endif
						<a href="{{ route('admin.help.show.articles') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
										<form id="myform" method="post" action="@if(@$article) {{ route('admin.help.articles.store', [@$article->id]) }} @else {{ route('admin.help.articles.store') }} @endif" enctype="multipart/form-data">
											@csrf
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<input type="text" name="ID" value="{{@$article->id}}" hidden>
													<label for="exampleInputEmail1">Article Title*</label>
													<input type="text" class="form-control required" placeholder="Article Title" name="title" value="{{@$article->title}}">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">

													<label for="exampleInputEmail1">Article Category*</label>
													<select class="form-control newdrop required" name="help_cat">
														<option></option>
														@foreach(@$help_cat as $cat)
															<option value="{{@$cat->id}}" @if(@$article->help_category_id == @$cat->id) selected @endif>{{@$cat->category}}</option>
														@endforeach
													</select>
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="all_time_sho">
												<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Description*</label>
														<textarea placeholder="Write description" name="description" id="description" class="form-control description required">{!! @$article->description !!}</textarea>
													</div>
													<p class="error_1" id="cntnt"></p>
												</div>
											</div>
											<div class="clearfix"></div>
                                            <div class="all_time_sho">
												<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Embed youtube video</label>
														<input type="text" name="video" id="video" class="form-control" value="{{ @$article->video ? 'https://www.youtube.com/watch?v=' . @$article->video : '' }}">
													</div>
													<p class="error_1" id="cntnt"></p>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="embed_upd" @if(@$article->video) style="display:block;" @else style="display:none;" @endif>
													<div class="your-mail">
														<iframe width="100%" id="embed_upload" src="https://www.youtube.com/embed/{{@$article->video}}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
													</div>
													<p class="error_1" id="cntnt"></p>
												</div>
                                            </div>
											<div class="clearfix"></div>
                                            <div class="all_time_sho">
												<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Image</label>
														<input type="file" name="image" id="image" accept="image/*" class="form-control">
													</div>
													<p class="error_1" id="cntnt"></p>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
													<div class="your-mail">
														<label for="exampleInputEmail1"> </label>
                                                        <img id="image_upload" src="{{url('/')}}/storage/app/public/uploads/content_images/{{@$article->image}}" alt="" width="200">
													</div>
                                                </div>
											</div>
											<div class="clearfix"></div>

											<div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Meta Title *</label>
													<input type="text" class="form-control required" placeholder="Meta Title" name="meta_title" value="{{ @$article->meta_title }}">
												</div>
                                            </div>
											<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Meta Keywords *</label>
													<input type="text" class="form-control required" placeholder="Meta Keywords" name="meta_keyword" value="{{ @$article->meta_keyword }}">
                                                </div>
                                            </div>
											<div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Meta Description *</label>
                                                    <textarea placeholder="Meta Description" name="meta_description" class="form-control address required">{!! @$article->meta_description !!}</textarea>
												</div>
                                            </div>

											<div class="clearfix"></div>

											<div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
												<div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
													<input value="{{ @$article ? 'Update' : 'Save' }}" type="submit" class="btn btn-default">
												</div>
											</div>
										</form>
									</div>
								</div>

							</div>

						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>
<script>
    $(document).ready(function(){
		$("#myform").validate({
			ignore:[],
		});
		tinyMCE.init({
            mode : "specific_textareas",
            editor_selector : "description",
            height: '420px',
            plugins: [
              'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
              'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
              'save table contextmenu directionality emoticons template textcolor'
            ],
            relative_urls : false,
            remove_script_host : false,
            convert_urls : true,
            toolbar: 'insertfile undo redo | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor emoticons',
            images_upload_url: '{{ URL::to('storage/app/public/uploads/content_image/') }}',
            images_upload_handler: function(blobInfo, success, failure) {
                var formD = new FormData();
                formD.append('file', blobInfo.blob(), blobInfo.filename());
                formD.append( "_token", '{{csrf_token()}}');
                $.ajax({
                    url: '{{ route('admin.artical.img.upload') }}',
                    data: formD,
                    type: 'POST',
                    contentType: false,
                    cache: false,
                    processData:false,
                    dataType: 'JSON',
                    success: function(jsn) {
                        if(jsn.status == 'ERROR') {
                            failure(jsn.error);
                        } else if(jsn.status == 'SUCCESS') {
                            success(jsn.location);
                        }
                    }
                });
            },
        });
        // $('#myform').on('submit', function() {
        //     var editorContent = tinyMCE.get('description').getContent();
        //     if(editorContent == '') {
        //         $('.error_1').html('Please write terms of services description.');
        //         $('.error_1').css('color', 'red');
        //         return false;
        //     } else {
        //         $('.error_1').html('');
        //         return true;
        //     }
        // });

        $('#image').change(function(){
            // console.log($('#image')[0].files[0]);
            $('#image_upload').attr('src', window.URL.createObjectURL($('#image')[0].files[0]));
        });

        $('#video').on('keyup change', function(){
			console.log("Changing");
			console.log(youTubeIdFromLink($(this).val()));
            $('#embed_upload').attr('src', "https://www.youtube.com/embed/"+youTubeIdFromLink($(this).val()));
			$('#embed_upd').show();
        });

        const youTubeIdFromLink = (url) => url.match(/(?:https?:\/\/)?(?:www\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\/?\?v=|\/embed\/|\/)([^\s&]+)/)[1];
    });
 </script>
</script>
<style>
	.error {
		color: red !important;
	}
	.error_1 {
		color: red !important;
	}
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
