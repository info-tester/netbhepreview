@extends('admin.layouts.app')
@section('title', 'NearO | Admin | Coupon')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
                    <h4 class="pull-left page-title">Help Articles</h4>
                    <div class="submit-login no_mmg pull-right">
                        <a href="{{ route('admin.help.articles.add', ['hid' => $helpCategory->id]) }}" title="Add Articles"><button type="button" class="btn btn-default">Add Articles</button></a>
						<a href="{{ route('admin.help.contents') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
                    </div>
				</div>
			</div>
            
			<div class="row">
				
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
                                <div class="row top_from">
                                    <form method="get" action="{{ route('admin.help.articles.manage',$helpCategory->id)}}" >
                                        @csrf
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">	
                                                <label for="exampleInputEmail1">Keyword</label>
                                                <input class="form-control" id="keyword" name="keyword" placeholder="Keyword" value="{{ @$key['keyword'] }}" type="text">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">Status</label>
                                                <select class="form-control newdrop required" name="status" id="status">
                                                <option value="">Status</option>
                                                <option value="A" @if(@$key['status'] == "A") {{ "selected" }} @endif >Active</option>
                                                <option value="I" @if(@$key['status'] == "I") {{ "selected" }} @endif>Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">Help Categories</label>
                                                <select class="form-control newdrop required" name="help_cat" id="help_cat">
                                                <option value="">Help Category</option>
                                                @foreach(@$help_cat as $cat)
                                                <option value="{{$cat->id}}" @if(@$key['help_cat'] == "$cat->id") {{ "selected" }} @endif >{{$cat->category}}</option>
                                                @endforeach
                                                
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="submit-login add_btnm" style="width:auto; margin-top:35px;">
                                                <input value="Search" id="search" class="btn btn-default" type="submit">
                                            </div>
                                        </div>
                                    </form>
                                </div>
								<div class="col-md-12 dess5">
									<i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
									<i class="fa fa-trash-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
									<i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active</span></i>
									<i class="fa fa-times cncl" aria-hidden="true"> <span class="cncl_oopo">Inactive</span></i>
									<i class="fa fa-copy cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">Copy</span></i>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Title</th>
                                                    <!-- <th>Description</th> -->
                                                    <th style="min-width:72px;">Action</th>
                                                </tr>
                                            </thead>
											<tbody>
                                                @if (count(@$articles) > 0)
                                                @foreach($articles as $k => $article)
                                                <tr>
                                                    <td>{{ $k + 1 }}</td>
                                                    <td>{{ $article->title }}</td>
                                                    <td>
                                                        <a href="{{ route('admin.help.articles.add', ['hid' => $helpCategory->id, 'id' => $article->id]) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
                                                        @if($article->status == 'A')
                                                            <a href="{{ route('admin.help.articles.status', ['id' => $article->id]) }}" title="Inactivate" onclick="return confirm('Are you sure you want to inactivate this help article?')"> <i class="fa fa-times delet" aria-hidden="true"></i></a>
                                                        @else
                                                            <a href="{{ route('admin.help.articles.status', ['id' => $article->id]) }}" title="Activate" onclick="return confirm('Are you sure you want to activate this help article?')"> <i class="fa fa-check delet" aria-hidden="true"></i></a>
                                                        @endif
                                                        <a href="{{ route('admin.help.articles.delete', ['id' => $article->id]) }}" title="Delete"> <i class="fa fa-trash delet" aria-hidden="true"></i></a>
                                                        <a href="javascript:;" title="Copy Link" onclick="copyFaqLink(`{{URL::to('help-article-details').'/'.@$article->slug}}`)"> <i class="fa fa-copy delet" aria-hidden="true"></i></a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                @else
                                                <tr>
                                                    <td colspan="4">No data found</td>
                                                </tr>
                                                @endif
                                            </tbody>
										</table>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

{{-- <form method="post" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
	var confirm = window.confirm('Are you want to delete this coupon ?');
	if(confirm){
		$("#destroy").attr('action',val);
		$("#destroy").submit();
	}
 }
</script> --}}
<script>
	$('.copy_coupon').click(function (e) {
        e.preventDefault();
        var copyText = $(this).attr('href');
        document.addEventListener('copy', function(e) {
            e.clipboardData.setData('text/plain', copyText);
            e.preventDefault();
        }, true);
        document.execCommand('copy');
        alert('copied Coupon Code: ' + copyText);
    });
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection