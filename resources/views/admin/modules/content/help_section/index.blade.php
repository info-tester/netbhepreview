@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Admin | Manage Help Section')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			@include('admin.includes.error')
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Help Section</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.help.category.add') }}" title="Add Terms Of Service"><button type="button" class="btn btn-default">Add Help Category</button></a>
						<a href="{{ route('admin.help.show.articles') }}" title="Show Articles"><button type="button" class="btn btn-default">Show Articles</button></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
										<form id="myform" method="get" action="{{ route('admin.help.contents') }}" enctype="multipart/form-data">
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<input type="text" name="ID" value="{{@$helpCategory->id}}" hidden>
													<label for="exampleInputEmail1">Help Section Title*</label>
													<input type="text" class="form-control required" placeholder="Help Section Title" name="main_title" id="main_title" value="{{@$content->title}}">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
													<input value="{{ @$helpCategory ? 'Update' : 'Save' }}" type="submit" class="btn btn-default">
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="row top_from">
                                    <form method="get" action="{{ route('admin.help.contents') }}" >
                                        @csrf
										<input type="text" name="main_title" id="main_title_duplicate" value="{{@$content->title}}" hidden >
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">	
                                                <label for="exampleInputEmail1">Keyword</label>
                                                <input class="form-control" id="keyword" name="keyword" placeholder="Keyword" value="{{ @$key['keyword'] }}" type="text">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">Status</label>
                                                <select class="form-control newdrop required" name="status" id="status">
                                                <option value="">Status</option>
                                                <option value="A" @if(@$key['status'] == "A") {{ "selected" }} @endif >Active</option>
                                                <option value="I" @if(@$key['status'] == "I") {{ "selected" }} @endif>Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="submit-login add_btnm" style="width:auto; margin-top:35px;">
                                                <input value="Search" id="search" class="btn btn-default" type="submit">
                                            </div>
                                        </div>
                                    </form>
                                </div>
								<div class="col-md-12 dess5">
									<!-- <i class="fa fa-folder cncl" aria-hidden="true"> <span class="cncl_oopo">Manage Articles</span></i> -->
									<i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
									<i class="fa fa-trash-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
									<i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active</span></i>
									<i class="fa fa-times cncl" aria-hidden="true"> <span class="cncl_oopo">Inactive</span></i>
									<i class="fa fa-copy cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">Copy</span></i>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>#</th>
													<th>Category Name</th>
													<th style="min-width:72px;">Action</th>
												</tr>
											</thead>
											<tbody>
												@if (count($categories) > 0)
                                                    @foreach($categories as $k => $category)
                                                        <tr>
                                                            <td>{{ $k + 1 }}</td>
                                                            <td>{{ $category->category }}</td>
                                                            <td>
																<!-- <a href="{{ route('admin.help.articles.manage', ['id' => $category->id]) }}" title="Manage Articles"> <i class="fa fa-folder delet" aria-hidden="true"></i></a> -->
																<!-- <a href="{{ route('admin.help.articles.add', ['hid' => $category->id]) }}" title="Add Articles"><i class="fa fa-folder delet" aria-hidden="true"></i></a> -->
                                                                <a href="{{ route('admin.help.category.add', ['id' => $category->id]) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
                                                                @if($category->status == 'A')
                                                                    <a href="{{ route('admin.help.category.status', ['id' => $category->id]) }}" title="Inactivate" onclick="return confirm('Are you sure you want to inactivate this help category?')"> <i class="fa fa-times delet" aria-hidden="true"></i></a>
                                                                @else
                                                                    <a href="{{ route('admin.help.category.status', ['id' => $category->id]) }}" title="Activate" onclick="return confirm('Are you sure you want to activate this help category?')"> <i class="fa fa-check delet" aria-hidden="true"></i></a>
                                                                @endif
                                                                <a href="{{ route('admin.help.category.delete', ['id' => $category->id]) }}" title="Delete" onclick="return confirm('Are you sure you want to delete this category along with related help articles?')"> <i class="fa fa-trash delet" aria-hidden="true"></i></a>
                                                                <a href="javascript:;" title="Copy Link" onclick="copyFaqLink(`{{URL::to('help-categories').'/'.@$category->slug}}`)"> <i class="fa fa-copy delet" aria-hidden="true"></i></a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
												@else
                                                    <tr>
                                                        <td colspan="4">No data found</td>
                                                    </tr>
												@endif
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>
<script>
	$(document).ready(function(){
		$("#myform").validate();
		tinyMCE.init({
            mode : "specific_textareas",
            editor_selector : "description",
            plugins: [
              'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
              'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
              'save table contextmenu directionality emoticons template paste textcolor'
            ],
            relative_urls : false,
            remove_script_host : false,
            convert_urls : true,
            toolbar: 'insertfile undo redo | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor emoticons',
            images_upload_url: '{{ URL::to('storage/app/public/uploads/content_image/') }}',
            images_upload_handler: function(blobInfo, success, failure) {
                var formD = new FormData();
                formD.append('file', blobInfo.blob(), blobInfo.filename());
                formD.append( "_token", '{{csrf_token()}}');
                $.ajax({
                    url: '{{ route('artical.img.upload') }}',
                    data: formD,
                    type: 'POST',
                    contentType: false,
                    cache: false,
                    processData:false,
                    dataType: 'JSON',
                    success: function(jsn) {
                        if(jsn.status == 'ERROR') {
                            failure(jsn.error);
                        } else if(jsn.status == 'SUCCESS') {
                            success(jsn.location);
                        }
                    }
                });
            }, 
        });
	});
	$('#main_title').keyup(function(){
		$('#main_title_duplicate').val($('#main_title').val());
	})
	function copyFaqLink(copyText){
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(copyText).select();
        document.execCommand("copy");
        alert("Help category link copied");
        $temp.remove();
    }
</script>
</script>
<style>
	.error {
		color: red !important;
	}
	.error_1 {
		color: red !important;
	}
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection