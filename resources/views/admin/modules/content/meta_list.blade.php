@extends('admin.layouts.app')
@section('title', 'Netbhe | Admin | Meta Tags')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
<div class="content">
    <div class="container">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="pull-left page-title">Meta Tags For Pages</h4>
            </div>
        </div>

        @include('admin.includes.error')

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body table-rep-plugin">
                        <div class="row">
                            <div class="col-md-12 dess5">
                                <i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" data-pattern="priority-columns">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Page</th>
                                                <th>Meta Title</th>
                                                <th>Meta Description</th>
                                                <th>Meta Keywords</th>
                                                <th style="min-width:72px;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if (count($metas) > 0)
                                                @foreach($metas as $ct)
                                                    <tr>
                                                        <td>{{ $ct->page }}</td>
                                                        <td>{{ $ct->title }}</td>
                                                        <td>{{ $ct->description }}</td>
                                                        <td>{{ $ct->keyword }}</td>
                                                        <td>
                                                            <a href="{{ route('meta.tag.edit', ['id' => $ct->id]) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="5">No data found</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Row -->
    </div>
    <!-- container -->
</div>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
    <script>
        $('.xxx').click(function(){
            $("#myFormSearch").submit();
        });
    </script>
@endsection
