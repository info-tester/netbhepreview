@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Admin | Edit Content')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            @include('admin.includes.error')
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Refer Content Management</h4>
                    <div class="submit-login no_mmg pull-right">
                        <a href="{{ route('admin.dashboard') }}" title="Back"><button type="button"
                                class="btn btn-default">Back</button></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-rep-plugin">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 nhp">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <form id="myform" method="post" action="{{route('admin.refer.content.update')}}"
                                            enctype="multipart/form-data">
                                            @csrf
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Content 1*</label>
                                                    <input type="text" class="form-control required"
                                                        placeholder="content" name="content1"
                                                        value="{{@$data->content_1}}">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Content 2*</label>
                                                    <input type="text" class="form-control required"
                                                        placeholder="content" name="content2"
                                                        value="{{@$data->content_2}}">
                                                </div>
                                            </div>


                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Content 3*</label>
                                                    <input type="text" class="form-control required"
                                                        placeholder="content" name="content3"
                                                        value="{{@$data->content_3}}">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-4">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Image 1 (Recommended size for image 200 * 150)</label>
                                                    <input type="file" name="image_1" class="image_input form-control">
                                                    <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$data->image_1) }}"
                                                        style="height: 150px; width: 150px; object-fit: contain; display: {{ @$data->image_1 ? 'block' : 'none' }};"
                                                        alt="">
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-4">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Image 2 (Recommended size for image 200 * 150)</label>
                                                    <input type="file" name="image_2" class="image_input form-control">
                                                    <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$data->image_2) }}"
                                                        style="height: 150px; width: 150px; object-fit: contain; display: {{ @$data->image_2 ? 'block' : 'none' }};"
                                                        alt="">
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-4">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Image 3 (Recommended size for image 200 * 150)</label>
                                                    <input type="file" name="image_3" class="image_input form-control">
                                                    <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$data->image_3) }}"
                                                        style="height: 150px; width: 150px; object-fit: contain; display: {{ @$data->image_3 ? 'block' : 'none' }};"
                                                        alt="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-4">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Model Image (Recommended size for image 360 * 150)</label>
                                                    <input type="file" name="modal_image" class="image_input form-control">
                                                    <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$data->modal_image) }}"
                                                        style="height: 150px; width: 150px; object-fit: contain; display: {{ @$data->modal_image ? 'block' : 'none' }};"
                                                        alt="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>

                                            
                                            <div class="all_time_sho">
                                                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Invite Process * </label>
                                                        <textarea placeholder="About us short description"
                                                            name="process" id="short_description"
                                                            class="short_description form-control message required">{!! @$data->process_content !!}</textarea>
                                                    </div>
                                                    <p class="error_1" id="cntnt"></p>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
                                                    <input value="Update" type="submit" class="btn btn-default">
                                                </div>
                                            </div>
                                            <!--all_time_sho-->
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Row -->
        </div>
        <!-- container -->
    </div>
    <!-- content -->
</div>
<script>
        $(document).ready(function() {
        const regex = new RegExp("(.*?)\.(png|jpg|jpeg)$");
        $('.image_input').on('change', function() {
            const val = $(this).val().toLowerCase();
            if (!(regex.test(val))) {
                $(this).val('');
                alert('Only png and jpeg files are allowed.');
            }
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                const t = $(this);
                reader.onload = function(e) {
                    t.next('img').attr('src', e.target.result).show();
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
    });
    $(document).ready(function(){
		$("#myform").validate();
		tinyMCE.init({
            mode : "specific_textareas",
            editor_selector : "short_description",
            plugins: [
              'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
              'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
              'save table contextmenu directionality emoticons template paste textcolor'
            ],
            relative_urls : false,
            remove_script_host : false,
            convert_urls : true,
            toolbar: 'insertfile undo redo | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor emoticons',
            images_upload_url: '{{ URL::to('storage/app/public/uploads/content_image/') }}',
            images_upload_handler: function(blobInfo, success, failure) {
                var formD = new FormData();
                formD.append('file', blobInfo.blob(), blobInfo.filename());
                formD.append( "_token", '{{csrf_token()}}');
                $.ajax({
                    url: '{{ route('artical.img.upload') }}',
                    data: formD,
                    type: 'POST',
                    contentType: false,
                    cache: false,
                    processData:false,
                    dataType: 'JSON',
                    success: function(jsn) {
                        if(jsn.status == 'ERROR') {
                            failure(jsn.error);
                        } else if(jsn.status == 'SUCCESS') {
                            success(jsn.location);
                        }
                    }
                });
            }, 
        });
	});
</script>
</script>
<style>
    .error {
        color: red !important;
    }

    .error_1 {
        color: red !important;
    }
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection