@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Admin | Edit Terms Of Services')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			@include('admin.includes.error')
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Terms Of Services</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.terms.of.services.add') }}" title="Add Terms Of Service"><button type="button" class="btn btn-default">Add Terms Of Service</button></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
											<form id="myform" method="post" action="{{ route('admin.terms.of.services') }}" enctype="multipart/form-data">
												@csrf
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 p-0">
													<div class="your-mail">
														<label for="exampleInputEmail1">Title*</label>
														<input type="text" class="form-control required" placeholder="Title" name="title" value="{{@$terms_title->title}}">
													</div>
												</div>
												<div class="clearfix"></div>
												<!-- <div class="all_time_sho">
													<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
														<div class="your-mail">
															<label for="exampleInputEmail1">Description*</label>
															<textarea placeholder="Write description" name="description" id="description" class="form-control description required">{!! @$termsOfServices->description !!}</textarea>
														</div>
														<p class="error_1" id="cntnt"></p>
													</div>
												</div>
												<div class="clearfix"></div> -->
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 p-0">
													<div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
														<input value="Update" type="submit" class="btn btn-default">
													</div>
												</div>
											</form>
										</div>
									</div>
									<div class="col-md-12 col-sm-12 col-xs-12">
										<div class="table-responsive" data-pattern="priority-columns">
											<table id="datatable" class="table table-striped table-bordered">
												<thead>
													<tr>
														<th>#</th>
														<th>Title</th>
														<!-- <th>Description</th> -->
														<th style="min-width:72px;">Action</th>
													</tr>
												</thead>
												<tbody>
													@if (count($terms) > 0)
													@foreach($terms as $k => $term)
													<tr>
														<td>{{ $k + 1 }}</td>
														<td>{{ $term->title }}</td>
														<!-- <td>
															<p style="white-space: pre-wrap;">{{ strip_tags($term->description) }}</p>
														</td> -->
														<td>
															<a href="{{ route('admin.terms.of.services.edit', ['id' => $term->id]) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
															<a href="{{ route('admin.terms.of.services.delete', ['id' => $term->id]) }}" title="Delete"> <i class="fa fa-trash delet" aria-hidden="true"></i></a>
														</td>
													</tr>
													@endforeach
													@else
													<tr>
														<td colspan="4">No data found</td>
													</tr>
													@endif
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>
<script>
	$(document).ready(function(){
		$("#myform").validate();
		tinyMCE.init({
            mode : "specific_textareas",
            editor_selector : "description",
            plugins: [
              'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
              'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
              'save table contextmenu directionality emoticons template paste textcolor'
            ],
            relative_urls : false,
            remove_script_host : false,
            convert_urls : true,
            toolbar: 'insertfile undo redo | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor emoticons',
            images_upload_url: '{{ URL::to('storage/app/public/uploads/content_image/') }}',
            images_upload_handler: function(blobInfo, success, failure) {
                var formD = new FormData();
                formD.append('file', blobInfo.blob(), blobInfo.filename());
                formD.append( "_token", '{{csrf_token()}}');
                $.ajax({
                    url: '{{ route('artical.img.upload') }}',
                    data: formD,
                    type: 'POST',
                    contentType: false,
                    cache: false,
                    processData:false,
                    dataType: 'JSON',
                    success: function(jsn) {
                        if(jsn.status == 'ERROR') {
                            failure(jsn.error);
                        } else if(jsn.status == 'SUCCESS') {
                            success(jsn.location);
                        }
                    }
                });
            }, 
        });
	});
</script>
</script>
<style>
	.error {
		color: red !important;
	}
	.error_1 {
		color: red !important;
	}
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection