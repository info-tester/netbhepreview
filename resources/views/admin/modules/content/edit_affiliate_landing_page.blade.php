@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Admin | Edit Affiliate Landing Page')
@section('header')
@include('admin.includes.header')
<style>
    .your-mail input[type='url'] {
        width: 100%;
        height: 40px;
        /*line-height:50px;*/
        float: left;
        padding: 8px;
        margin-bottom: 8px;
        border: 1px solid #CCC;
        border-radius: 0;
        background: #fff !important;
        color: #000;
        font-family: "Open Sans", sans-serif;
        font-size: 14px;
        font-weight: 400;
    }
    .aff_form .page-title{
        text-align: center !important;
        font-size: 30px !important;
    }
</style>
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			@include('admin.includes.error')
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Edit Affiliate Landing Page</h4>
					<div class="submit-login no_mmg pull-right">
						<a target="_blank" href="{{ route('affiliate.landing.page') }}" target="_blank" title="Preview"><button type="button" class="btn btn-default">Preview</button></a>
						<a href="{{ route('admin.dashboard') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">


										<form id="myform_1" class="aff_form" method="post" action="{{ route('admin.affiliate.landing.page.update', 1) }}" enctype="multipart/form-data">
                                            @csrf
                                            <div class="col-lg-12">
                                                <h4 class="page-title">Top Banner Section</h4>
                                            </div>
                                            <div class="col-lg-12 row">
												<div class="your-mail">
                                                    <div class="col-lg-12">
													    <label for="alignment1">Alignment*</label>
                                                    </div>
													<div class="col-md-4">
														<label for="alignment_left">
															<input type="radio" class="required" id="alignment_left" name="alignment" value="L" @if(@$part1->image_1 == 'L') checked @endif >
															Left
														</label>
													</div>
													<div class="col-md-4 text-center">
														<label for="alignment_center">
															<input type="radio" class="required" id="alignment_center" name="alignment" value="C" @if(@$part1->image_1 == 'C') checked @endif >
															Center
														</label>
													</div>
													<div class="col-md-4 text-right">
														<label for="alignment_right">
															<input type="radio" class="required" id="alignment_right" name="alignment" value="R" @if(@$part1->image_1 == 'R') checked @endif >
															Right
														</label>
													</div>
												</div>
											</div>
											<div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Title *</label>
													<input type="text" class="form-control required" placeholder="Title" name="banner_title" value="{{@$part1->title}}">
												</div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Description *</label>
                                                    <textarea placeholder="Description" name="banner_description" class="form-control address required">{{@$part1->description}}</textarea>
                                                </div>
                                            </div>
											<div class="col-lg-6">
												<div class="your-mail">
													<label for="exampleInputEmail1">Button Text *</label>
													<input type="text" class="form-control required" placeholder="Button Text" name="banner_button_text" value="{{@$part1->short_description_1}}">
												</div>
                                            </div>
											<div class="col-lg-6">
												<div class="your-mail">
													<label for="exampleInputEmail1">Button URL *</label>
													<input type="url" class="form-control required" placeholder="Button URL" name="banner_button_link" value="{{@$part1->button_link_7}}">
												</div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Image (Resolution must be at least 1024 * 768px)</label>
                                                    <input type="file" name="banner_image" class="image_input form-control">
                                                    @if(@$part1->image)
                                                        <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$part1->image) }}" style="width: 100%; object-fit: contain; display: {{ @$part1->image ? 'block' : 'none' }};" alt="">
                                                    @else
		                                                <img src="{{ URL::to('public/frontend/images/how-ban.jpg') }}" alt="" style="width: 100%; object-fit: contain;">
                                                    @endif
                                                </div>
                                            </div>

											<div class="col-lg-12">
												<div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
													<input value="Save" type="submit" class="btn btn-default">
												</div>
											</div>
										</form>



										<form id="myform_2" class="aff_form" method="post" action="{{ route('admin.affiliate.landing.page.update', 2) }}" enctype="multipart/form-data">
                                            @csrf
                                            <div class="col-lg-12">
                                                <h4 class="page-title">For Whom</h4>
                                            </div>
											<div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Title *</label>
													<input type="text" class="form-control required" placeholder="Title" name="for_whom_title" value="{{@$part1->heading_1}}">
												</div>
                                            </div>
                                            <div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Bottom Line *</label>
													<input type="text" class="form-control required" placeholder="Bottom Line" name="for_whom_bottom_line" value="{{@$part1->description_1}}">
												</div>
                                            </div>
                                            <br/>
                                            <div class="col-lg-12">
                                                <h4 class="text-center">For Whom Point 1</h4>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Title *</label>
													<input type="text" class="form-control required" placeholder="Title" name="for_whom_title_1" value="{{@$part1->heading_2}}">
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Description *</label>
                                                    <textarea placeholder="Description" name="for_whom_description_1" class="form-control address required">{{@$part1->description_2}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-6">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Icon (Resolution must be at least 900 * 900px, square image recommened)</label>
                                                    <input type="file" name="for_whom_icon_1" class="for_whom_icon form-control" data-num="1">
                                                    <div id="for_whom_icon_1_div">
                                                        @if(@$part1->image_2)
                                                            <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$part1->image_2) }}" style="height: 56px; width: 56px; object-fit: contain; display: {{ @$part1->image_2 ? 'block' : 'none' }};" alt="">
                                                        @else
                                                            <i class="fa fa-3x fa-briefcase" aria-hidden="true"></i>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <br/>
                                            <div class="col-lg-12">
                                                <h4 class="text-center">For Whom Point 2</h4>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Title *</label>
													<input type="text" class="form-control required" placeholder="Title" name="for_whom_title_2" value="{{@$part1->heading_3}}">
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Description *</label>
                                                    <textarea placeholder="Description" name="for_whom_description_2" class="form-control address required">{{@$part1->description_3}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-6">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Icon (Resolution must be at least 900 * 900px, square image recommened)</label>
                                                    <input type="file" name="for_whom_icon_2" class="for_whom_icon form-control" data-num="2">
                                                    <div id="for_whom_icon_2_div">
                                                        @if(@$part1->image_3)
                                                            <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$part1->image_3) }}" style="height: 56px; width: 56px; object-fit: contain; display: {{ @$part1->image_3 ? 'block' : 'none' }};" alt="">
                                                        @else
                                                            <i class="fa fa-3x fa-usd" aria-hidden="true"></i>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <br/>
                                            <div class="col-lg-12">
                                                <h4 class="text-center">For Whom Point 3</h4>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Title *</label>
													<input type="text" class="form-control required" placeholder="Title" name="for_whom_title_3" value="{{@$part1->heading_4}}">
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Description *</label>
                                                    <textarea placeholder="Description" name="for_whom_description_3" class="form-control address required">{{@$part1->description_4}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-6">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Icon (Resolution must be at least 900 * 900px, square image recommened)</label>
                                                    <input type="file" name="for_whom_icon_3" class="for_whom_icon form-control" data-num="3">
                                                    <div id="for_whom_icon_3_div">
                                                        @if(@$part1->image_4)
                                                            <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$part1->image_4) }}" style="height: 56px; width: 56px; object-fit: contain; display: {{ @$part1->image_4 ? 'block' : 'none' }};" alt="">
                                                        @else
                                                            <i class="fa fa-3x fa-bullhorn" aria-hidden="true"></i>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

											<div class="col-lg-12">
												<div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
													<input value="Save" type="submit" class="btn btn-default">
												</div>
											</div>
                                        </form>


                                        <form id="myform_3" class="aff_form" method="post" action="{{ route('admin.affiliate.landing.page.update', 3) }}" enctype="multipart/form-data">
                                            @csrf
                                            <div class="col-lg-12">
                                                <h4 class="page-title">Middle Banner</h4>
                                            </div>
											<div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Title *</label>
													<input type="text" class="form-control required" placeholder="Title" name="middle_banner_title" value="{{@$part1->heading_5}}">
												</div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Description *</label>
                                                    <textarea placeholder="Description" name="middle_banner_description" class="form-control address required">{{@$part1->description_5}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-6">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Image (Resolution must be at least 900 * 900px, square image recommened)</label>
                                                    <input type="file" name="middle_banner_image" class="image_input form-control">
                                                    @if(@$part1->image_5)
                                                        <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$part1->image_5) }}"  style="width: 445px; height: 445px; object-fit: contain; display: {{ @$part1->image_5 ? 'block' : 'none' }};" alt="">
                                                    @else
		                                                <img src="{{ URL::to('public/frontend/images/how_affiliate1.jpg') }}" alt="" style="width: 445px; height: 445px; object-fit: contain;">
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <h4 class="text-center">Middle Banner Point 1</h4>
                                            </div>
                                            <div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Title *</label>
													<input type="text" class="form-control required" placeholder="Title" name="middle_banner_title_1" value="{{@$part1->heading_6}}">
												</div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Description *</label>
                                                    <textarea placeholder="Description" name="middle_banner_description_1" class="form-control address required">{{@$part1->description_6}}</textarea>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <h4 class="text-center">Middle Banner Point 2</h4>
                                            </div>
                                            <div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Title *</label>
													<input type="text" class="form-control required" placeholder="Title" name="middle_banner_title_2" value="{{@$part1->heading_7}}">
												</div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Description *</label>
                                                    <textarea placeholder="Description" name="middle_banner_description_2" class="form-control address required">{{@$part1->description_7}}</textarea>
                                                </div>
                                            </div>

											<div class="col-lg-12">
												<div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
													<input value="Save" type="submit" class="btn btn-default">
												</div>
											</div>
										</form>


                                        <form id="myform_4" class="aff_form" method="post" action="{{ route('admin.affiliate.landing.page.update', 4) }}" enctype="multipart/form-data">
                                            @csrf
                                            <div class="col-lg-12">
                                                <h4 class="page-title">How It Works</h4>
                                            </div>
											<div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Title *</label>
													<input type="text" class="form-control required" placeholder="Title" name="how_it_works_title" value="{{@$part2->heading_1}}">
												</div>
                                            </div>

                                            <div class="col-lg-12">
                                                <h4 class="text-center">How It Works Point 1</h4>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Title *</label>
													<input type="text" class="form-control required" placeholder="Title" name="how_it_works_title_1" value="{{@$part2->heading_2}}">
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Description *</label>
                                                    <textarea placeholder="Description" name="how_it_works_description_1" class="form-control address required">{{@$part2->description_2}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-6">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Icon (Resolution must be at least 900 * 900px, square image recommened)</label>
                                                    <input type="file" name="how_it_works_icon_1" class="how_it_works_icon form-control" data-num="1">
                                                    <div id="how_it_works_icon_1_div">
                                                        @if(@$part2->image_2)
                                                            <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$part2->image_2) }}" style="height: 56px; width: 56px; object-fit: contain; display: {{ @$part2->image_2 ? 'block' : 'none' }};" alt="">
                                                        @else
                                                            <i class="fa fa-3x fa-video-camera" aria-hidden="true"></i>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <br/>
                                            <div class="col-lg-12">
                                                <h4 class="text-center">How It Works Point 2</h4>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Title *</label>
													<input type="text" class="form-control required" placeholder="Title" name="how_it_works_title_2" value="{{@$part2->heading_3}}">
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Description *</label>
                                                    <textarea placeholder="Description" name="how_it_works_description_2" class="form-control address required">{{@$part2->description_3}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-6">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Icon (Resolution must be at least 900 * 900px, square image recommened)</label>
                                                    <input type="file" name="how_it_works_icon_2" class="how_it_works_icon form-control" data-num="2">
                                                    <div id="how_it_works_icon_2_div">
                                                        @if(@$part2->image_3)
                                                            <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$part2->image_3) }}" style="height: 56px; width: 56px; object-fit: contain; display: {{ @$part2->image_2 ? 'block' : 'none' }};" alt="">
                                                        @else
                                                            <i class="fa fa-3x fa-gift" aria-hidden="true"></i>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <br/>
                                            <div class="col-lg-12">
                                                <h4 class="text-center">How It Works Point 3</h4>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Title *</label>
													<input type="text" class="form-control required" placeholder="Title" name="how_it_works_title_3" value="{{@$part2->heading_4}}">
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Description *</label>
                                                    <textarea placeholder="Description" name="how_it_works_description_3" class="form-control address required">{{@$part2->description_4}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-6">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Icon (Resolution must be at least 900 * 900px, square image recommened)</label>
                                                    <input type="file" name="how_it_works_icon_3" class="how_it_works_icon form-control" data-num="3">
                                                    <div id="how_it_works_icon_3_div">
                                                        @if(@$part2->image_4)
                                                            <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$part2->image_4) }}" style="height: 56px; width: 56px; object-fit: contain; display: {{ @$part2->image_4 ? 'block' : 'none' }};" alt="">
                                                        @else
                                                            <!-- <i class="fa fa-3x fa-gift" aria-hidden="true"></i> -->
                                                            <img src="{{ URL::to('public/frontend/images/percent-icon.png') }}" alt="">
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
												<div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
													<input value="Save" type="submit" class="btn btn-default">
												</div>
											</div>

                                        </form>


                                        <form id="myform_5" class="aff_form" method="post" action="{{ route('admin.affiliate.landing.page.update', 5) }}" enctype="multipart/form-data">
                                            @csrf
                                            <div class="col-lg-12">
                                                <h4 class="page-title">Bottom Banner</h4>
                                            </div>
											<div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Title *</label>
													<input type="text" class="form-control required" placeholder="Title" name="bottom_banner_title" value="{{@$part2->title}}">
												</div>
                                            </div>
                                            <div class="col-lg-6">
												<div class="your-mail">
													<label for="exampleInputEmail1">Button Text *</label>
													<input type="text" class="form-control required" placeholder="Button Text" name="bottom_banner_button_text" value="{{@$part2->short_description_1}}">
												</div>
                                            </div>
											<div class="col-lg-6">
												<div class="your-mail">
													<label for="exampleInputEmail1">Button URL *</label>
													<input type="url" class="form-control required" placeholder="Button URL" name="bottom_banner_button_link" value="{{@$part2->button_link_7}}">
												</div>
                                            </div>

                                            <div class="col-lg-12">
												<div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
													<input value="Save" type="submit" class="btn btn-default">
												</div>
											</div>

                                        </form>


									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>
<script>
	$(document).ready(function(){
        const regex = new RegExp("(.*?)\.(png|jpg|jpeg)$");
        $('.image_input').on('change', function() {
            const val = $(this).val().toLowerCase();
            if (!(regex.test(val))) {
                $(this).val('');
                alert('Only png and jpeg files are allowed.');
            }
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                const t = $(this);
                reader.onload = function(e) {
                    t.next('img').attr('src', e.target.result).show();
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
        @foreach (range(1, 30) as $i)
		    $("#myform_{{ $i }}").validate();
        @endforeach

        $('.for_whom_icon').on('change', function() {
            var num = $(this).data('num');
            console.log(num);
            const val = $(this).val().toLowerCase();
            if (!(regex.test(val))) {
                $(this).val('');
                alert('Only png and jpeg files are allowed.');
            }
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                const t = $(this);
                reader.onload = function(e) {
                    $('#for_whom_icon_'+num+'_div').empty();
                    $('#for_whom_icon_'+num+'_div').append(`<img src="${e.target.result}" style="height: 56px; width: 56px;"/>`);
                }
                reader.readAsDataURL(this.files[0]);
            }
        });

        $('.how_it_works_icon').on('change', function() {
            var num = $(this).data('num');
            console.log(num);
            const val = $(this).val().toLowerCase();
            if (!(regex.test(val))) {
                $(this).val('');
                alert('Only png and jpeg files are allowed.');
            }
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                const t = $(this);
                reader.onload = function(e) {
                    $('#how_it_works_icon_'+num+'_div').empty();
                    $('#how_it_works_icon_'+num+'_div').append(`<img src="${e.target.result}" style="height: 56px; width: 56px;"/>`);
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
	});
</script>
</script>
<style>
	.error {
		color: red !important;
	}
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
