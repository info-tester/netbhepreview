@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Admin | Edit Meta Content')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			@include('admin.includes.error')
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Edit Meta Content</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('meta.tag.manage') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
										<form id="myform" method="post" action="{{ route('meta.tag.edit', ['id' => $meta->id]) }}" enctype="multipart/form-data">
                                            @csrf

											<div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Meta Title *</label>
													<input type="text" class="form-control required" placeholder="Meta Title" name="title" value="{{ $meta->title }}">
												</div>
                                            </div>
											<div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Meta Description *</label>
                                                    <textarea placeholder="Meta Description" name="description" class="form-control address required">{!! $meta->description !!}</textarea>
												</div>
                                            </div>

                                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Meta Keywords *</label>
													<input type="text" class="form-control required" placeholder="Meta Keywords" name="keyword" value="{{ $meta->keyword }}">
                                                </div>
                                            </div>

											<div class="col-lg-12">
												<div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
													<input value="Update" type="submit" class="btn btn-default">
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>
<script>
	$(document).ready(function(){
		$("#myform").validate();
	});
</script>
</script>
<style>
	.error {
		color: red !important;
	}
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
