@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Admin | Edit Faq')
@section('header')
@include('admin.includes.header')
<style>
    .your-mail input[type='url'] {
        width: 100%;
        height: 40px;
        /*line-height:50px;*/
        float: left;
        padding: 8px;
        margin-bottom: 8px;
        border: 1px solid #CCC;
        border-radius: 0;
        background: #fff !important;
        color: #000;
        font-family: "Open Sans", sans-serif;
        font-size: 14px;
        font-weight: 400;
    }
</style>
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            @include('admin.includes.error')
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Faq</h4>
                     <div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.faq.update') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div> 
                </div>
            </div>
            @if (!@$faqEdit)
            <div class="row">
                @if (!@$faqEdit)
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-rep-plugin">
                            <h4>Expert FAQs</h4>
                            <div class="table-responsive" data-pattern="priority-columns">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            {{-- <th>For</th> --}}
                                            <th>Title</th>
                                            <th>Description</th>
                                            <th>Video</th>
                                            <th style="min-width:72px;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($faqItemsE) > 0)
                                        @foreach($faqItemsE as $k => $faqItem)
                                        <tr>
                                            <td>{{ $k + 1 }}</td>
                                            {{-- <td>{{ $faqItem->type }}</td> --}}
                                            <td>{{ $faqItem->title }}</td>
                                            <td>
                                                <p style="white-space: pre-wrap;">{{ strip_tags($faqItem->description) }}</p>
                                            </td>
                                            <td>{{ @$faqItem->video }}</td>
                                            <td>
                                                @if ($k != 0)
                                                <a href="{{ route('admin.faq.move', ['id' => $faqItem, 'dir' => 'UP']) }}"
                                                    title="Move up"> <i class="fa fa-arrow-up delet"
                                                        aria-hidden="true"></i></a>
                                                @endif
                                                @if ($k + 1 != $faqItemsE->count())
                                                <a href="{{ route('admin.faq.move', ['id' => $faqItem, 'dir' => 'DOWN']) }}"
                                                    title="Move down"> <i class="fa fa-arrow-down delet"
                                                        aria-hidden="true"></i></a>
                                                @endif
                                                <a href="{{ route('admin.faq.edit', ['id' => $faqItem]) }}"
                                                    title="Edit"> <i class="fa fa-pencil-square-o delet"
                                                        aria-hidden="true"></i></a>
                                                <a onclick="return confirm('Do you want to delete this faq content?')"
                                                    href="{{ route('admin.faq.delete', ['id' => $faqItem]) }}"
                                                    title="Edit"> <i class="fa fa-trash delet"
                                                        aria-hidden="true"></i></a>
                                                        <a href="javascript:;" title="Copy Link" onclick="copyFaqLink(`{{URL::to('faq/prof').'/'.@$faqItem->id}}`)"> <i class="fa fa-clipboard delet" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td colspan="4">No data found</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            @endif
            <!-- End Row -->
        </div>
        <!-- container -->
    </div>
    <!-- content -->
</div>
<script>
    $(document).ready(function(){
		$("#myform").validate();
		$("#myform1").validate({
            ignore: [],
        });
	    tinyMCE.init({
	        mode : "specific_textareas",
	        editor_selector : "description",
	        height: '320px',

	        forced_root_block : "",
		    force_br_newlines : true,
		    force_p_newlines : false,

	        plugins: [
	          'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
	          'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
	          'save table contextmenu directionality emoticons template paste textcolor'
	        ],
	        relative_urls : false,
	        remove_script_host : false,
	        convert_urls : true,
	        toolbar: 'insertfile undo redo | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor emoticons',
            images_upload_url: '{{ env("PATH_TO_FAQ_CONTENT_IMAGE") }}',
            images_upload_handler: function(blobInfo, success, failure) {
                var formD = new FormData();
                formD.append('file', blobInfo.blob(), blobInfo.filename());
                formD.append( "_token", '{{ csrf_token() }}');
                $.ajax({
                    url: '{{ route("admin.faq.image.upload") }}',
                    data: formD,
                    type: 'POST',
                    contentType: false,
                    cache: false,
                    processData:false,
                    dataType: 'JSON',
                    success: function(jsn) {
                        if(jsn.status == 'ERROR') {
                            failure(jsn.error);
                        } else if(jsn.status == 'SUCCESS') {
                            success(jsn.location);
                        }
                    }
                });
            },
        });
	});
    function copyFaqLink(copyText){
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(copyText).select();
        document.execCommand("copy");
        alert("Faq link copied");
        $temp.remove();
    }
</script>
<style>
    .error {
        color: red !important;
    }

    .error_1 {
        color: red !important;
    }
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection