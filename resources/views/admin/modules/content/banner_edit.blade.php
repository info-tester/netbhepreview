@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Admin | Edit Meta Content')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			@include('admin.includes.error')
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Edit Meta Content</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('home.banner.management') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
										<form id="myform" method="post" action="{{ @$banner ? route('home.banner.edit', ['id' => $banner->id]) : route('home.banner.add') }}" enctype="multipart/form-data">
                                            @csrf

											<div class="col-lg-12 row">
												<div class="your-mail">
													<label for="alignment1">Banner Alignment*</label>
													<div class="col-md-4">
														<label for="alignment_left">
															<input type="radio" class="required" id="alignment_left" name="alignment" value="L" @if(@$banner->alignment == 'L') checked @endif >
															Left
														</label>
													</div>
													<div class="col-md-4 text-center">
														<label for="alignment_center">
															<input type="radio" class="required" id="alignment_center" name="alignment" value="C" @if(@$banner->alignment == 'C') checked @endif >
															Center
														</label>
													</div>
													<div class="col-md-4 text-right">
														<label for="alignment_right">
															<input type="radio" class="required" id="alignment_right" name="alignment" value="R" @if(@$banner->alignment == 'R') checked @endif >
															Right
														</label>
													</div>
												</div>
											</div>
											<div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Banner Text Color *</label>
													<input type="color" class="form-control required" name="text_color" value="{{ old('text_color', @$banner->text_color) }}">
												</div>
                                            </div>
											<div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Banner Heading *</label>
													<input type="text" class="form-control required" placeholder="Banner Heading" name="heading" value="{{ old('heading', @$banner->heading) }}">
												</div>
                                            </div>
											<div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Banner Description *</label>
                                                    <textarea placeholder="Banner Description" name="description" class="form-control address required">{{ old('description', @$banner->description) }}</textarea>
												</div>
                                            </div>

                                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-4">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Image * (Recommended size 1600 * 500)</label>
                                                    <input type="file" name="media" class="image_input form-control">
                                                    <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$banner->media) }}" style="height: 100px; width: 150px; object-fit: contain; display: {{ @$banner->media ? 'block' : 'none' }};" alt="">
                                                </div>
                                            </div>
											<div class="col-lg-12">
												<div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
													<input value="Save" type="submit" class="btn btn-default">
												</div>
											</div>

                                            <div class="col-lg-12" style="margin-top: 10px;">
												<div class="your-mail">
													<label for="exampleInputEmail1">Link Maker</label>
												</div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12 col-lg-4">
                                                <div class="your-mail">
                                                    <input type="text" id="link-text" class="form-control" placeholder="Link Text">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12 col-lg-4">
                                                <div class="your-mail">
                                                    <input type="text" id="link-url" class="form-control" placeholder="Link URL">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12 col-lg-4">
                                                <div class="your-mail">
                                                    <button type="button" id="create-link" class="btn btn-info">Generate</button>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="your-mail">
                                                    <input type="text" id="link" class="form-control" placeholder="Copy and paste">
                                                </div>
                                            </div>

										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>
<script>
	$(document).ready(function() {
        $('#create-link').click(function() {
            $('#link').val('<a target="_blank" href="' + $('#link-url').val() + '">' + $('#link-text').val() + '</a>');
            $('#link-url').val('');
            $('#link-text').val('');
        });
        const regex = new RegExp("(.*?)\.(png|jpg|jpeg)$");
        $('.image_input').on('change', function() {
            const val = $(this).val().toLowerCase();
            if (!(regex.test(val))) {
                $(this).val('');
                alert('Only png and jpeg files are allowed.');
            }
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                const t = $(this);
                reader.onload = function(e) {
                    t.next('img').attr('src', e.target.result).show();
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
		$("#myform").validate();
	});
</script>
</script>
<style>
	.error {
		color: red !important;
	}
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
