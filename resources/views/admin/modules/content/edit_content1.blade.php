@extends('admin.layouts.app')
@section('title', 'Vedic Astro World | Admin | Home Page Content')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Home Page Content</h4>
					{{-- <div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.content.index') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div> --}}
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
										<form id="myform" method="post" action="{{ route('admin.content.update1') }}" enctype="multipart/form-data">
											{{ csrf_field() }}
											{{-- {{method_field('PUT')}} --}}
											{{-- <input type="hidden" name="id" value="{{@$content->id}}"> --}}
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Header Phone No*</label>
													<input type="text" class="form-control required" placeholder="Header Phone No" name="header_phone" value="{{@$content->header_phone}}">
												</div>
											</div>

											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Satisfied Customers*</label>
													<input type="text" class="form-control required" placeholder="Satisfied Customers" name="satisfied_customers" value="{{@$content->satisfied_customers}}">
												</div>
											</div>
											
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Total Astrologer*</label>
													<input type="text" class="form-control required" placeholder="Total Astrologer" name="astrologers" value="{{@$content->astrologers}}">
												</div>
											</div>
											<div class="clearfix"></div>
											
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Footer Address*</label>
													<input type="text" class="form-control required" placeholder="Footer Address" name="footer_address" value="{{@$content->footer_address}}">
												</div>
											</div>
											
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Footer Email*</label>
													<input type="text" class="form-control required" placeholder="Footer Email" name="footer_email" value="{{@$content->footer_email}}">
												</div>
											</div>
											
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Footer Phone*</label>
													<input type="text" class="form-control required" placeholder="Footer Phone" name="footer_phone" value="{{@$content->footer_phone}}">
												</div>
											</div>

											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Facebook link*</label>
													<input type="text" class="form-control required url" placeholder="Facebook link" name="fb_link" value="{{@$content->fb_link}}">
												</div>
											</div>

											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Google link*</label>
													<input type="text" class="form-control required url" placeholder="Google link" name="ggl_link" value="{{@$content->ggl_link}}">
												</div>
											</div>

											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Twitter link*</label>
													<input type="text" class="form-control required url" placeholder="Twitter link" name="twtr_link" value="{{@$content->twtr_link}}">
												</div>
											</div>
											<div class="clearfix"></div>

											<div class="all_time_sho">
												<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Our Strength* </label>
														<textarea placeholder="Our Strength" name="strength" class="form-control message required">{{ strip_tags(@$content->strength) }}</textarea>
													</div>
												</div>
											</div>
											<div class="clearfix"></div>

											<div class="all_time_sho">
												<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Our Vision* </label>
														<textarea placeholder="Our Vision" name="vision" class="form-control message required">{{ strip_tags(@$content->vision) }}</textarea>
													</div>
												</div>
											</div>
											<div class="clearfix"></div>

											<div class="all_time_sho">
												<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Our Mission* </label>
														<textarea placeholder="Our Mission" name="mission" class="form-control message required">{{ strip_tags(@$content->mission) }}</textarea>
													</div>
												</div>
											</div>
											<div class="clearfix"></div>

											<div class="all_time_sho">
												<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Home Page About Us* </label>
														<textarea placeholder="Home Page About Us" name="home_about_us" class="form-control message required">{{ strip_tags(@$content->home_about_us) }}</textarea>
													</div>
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="submit-login add_btnm">
													<input value="Save" type="submit" class="btn btn-default">
												</div>
											</div>
											<!--all_time_sho-->
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>
<script>
	$(document).ready(function(){
		$("#myform").validate({
			rules: {
				header_phone 		: 'number',
				satisfied_customers : 'number',
				astrologers 		: 'number',
				footer_email 		: 'email',
				footer_phone 		: 'number',
			},
			errorPlacement: function() {
				//
			}
		});
	});
</script>

{{-- <script language="JavaScript" type="text/javascript" src="{{url('public/tiny_mce/tiny_mce.js')}}"></script>
<script language="JavaScript" type="text/javascript">
    tinyMCE.init({
        // General options
        mode : "textareas",
		forced_root_block : "",
        height:"550px",
        theme : "advanced",
        editor_deselector : "mceNoEditor",
		relative_urls:false,
        plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
         file_browser_callback : "filebrowser",
        // Theme options
        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,search,replace,|,styleprops",
        theme_advanced_buttons2 : "styleselect,formatselect,fontselect,fontsizeselect,|,help,code,|,forecolor,backcolor",
        theme_advanced_buttons3 : "cut,copy,paste,pastetext,pasteword,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,|,insertdate,inserttime,preview",
       
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        //theme_advanced_resizing : true,

        // Example content CSS (should be your site CSS)
        /*content_css : "csseditor/content.css",*/

        // Drop lists for link/image/media/template dialogs
        template_external_list_url : "lists/template_list.js",
        external_link_list_url : "lists/link_list.js",
        external_image_list_url : "lists/image_list.js",
        media_external_list_url : "lists/media_list.js",

        // Replace values for the template plugin
        template_replace_values : {
            username : "Some User",
            staffid : "991234"
        }
		//height:"500px",
        //width:"600px"
});
 function filebrowser(field_name, url, type, win)
 {
    fileBrowserURL = "{{ url('') }}/public/tiny_mce/plugins/pdw_file_browser/index.php?editor=tinymce&filter=" + type;
    tinyMCE.activeEditor.windowManager.open({
        title: "PDW File Browser",
        url: fileBrowserURL,
        width: 950,
        height: 650,
        inline: 0,
        maximizable: 1,
        close_previous: 0
    },{
        window : win,
        input : field_name
    });

 }
 //End TinyMCE

</script> --}}
<style>
	.error {
		color: red !important;
	}
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection