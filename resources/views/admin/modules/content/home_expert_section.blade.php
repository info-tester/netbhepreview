@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Admin | Home Professionals Section')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			@include('admin.includes.error')
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
                                <div class="col-sm-12">
                                    <h4 class="pull-left page-title">Home Page Professionals Section 1</h4>
                                    <!-- <div class="submit-login no_mmg pull-right">
                                        <a href="{{ route('admin.dashboard') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
                                    </div> -->
                                </div>
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
										<form id="myform" method="post" action="{{ route('admin.home.expert.section') }}" enctype="multipart/form-data">
                                            @csrf


                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Title *</label>
													<input type="text" class="form-control required" placeholder="Write title" name="title_1" value="{{@$exp1->title}}">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="all_time_sho">
												<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Description *</label>
														<textarea placeholder="Write description" name="description_1" id="description_1" rows="6" class="form-control required">{!! @$exp1->description !!}</textarea>
													</div>
													<p class="meet_description" id="cntnt"></p>
												</div>
                                            </div>


                                            <div class="col-lg-12">
                                                <h4 class="pull-left" style="margin-top: 24px;">Home Page Professionals Section 2</h4>
                                            </div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Title*</label>
													<input type="text" class="form-control required" placeholder="Title" name="title_2" value="{{@$exp2->title}}">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="all_time_sho">
												<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Description*</label>
														<textarea placeholder="Write description" name="description_2" id="description_2" class="form-control description required">{!! @$exp2->description !!}</textarea>
													</div>
													<p class="error_1" id="cntnt"></p>
												</div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-4">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Main Image</label>
                                                    <input type="file" name="image_2" class="image_input form-control">
                                                    <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$exp2->image) }}" style="height: 150px; width: 150px; object-fit: contain; display: {{ @$exp2->image ? 'block' : 'none' }};" alt="">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Heading 1*</label>
													<input type="text" class="form-control required" placeholder="Heading 1" name="heading_2_1" value="{{@$exp2->heading_1}}">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="all_time_sho">
												<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Description 1*</label>
														<textarea placeholder="Write description1" name="description_2_1" id="description_2_1" class="form-control description required">{!! @$exp2->description_1 !!}</textarea>
													</div>
													<p class="error_2" id="cntnt"></p>
												</div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-4">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Image 1</label>
                                                    <input type="file" name="image_2_1" class="image_input form-control">
                                                    <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$exp2->image_1) }}" style="height: 150px; width: 150px; object-fit: contain; display: {{ @$exp2->image_1 ? 'block' : 'none' }};" alt="">
                                                </div>
                                            </div>
											<div class="clearfix"></div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Heading 2*</label>
													<input type="text" class="form-control required" placeholder="Heading 2" name="heading_2_2" value="{{@$exp2->heading_2}}">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="all_time_sho">
												<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Description 2*</label>
														<textarea placeholder="Write description 2" name="description_2_2" id="description_2" class="form-control description required">{!! @$exp2->description_2 !!}</textarea>
													</div>
													<p class="error_3" id="cntnt"></p>
												</div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-4">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Image 2</label>
                                                    <input type="file" name="image_2_2" class="image_input form-control">
                                                    <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$exp2->image_2) }}" style="height: 150px; width: 150px; object-fit: contain; display: {{ @$exp2->image_2 ? 'block' : 'none' }};" alt="">
                                                </div>
                                            </div>
											<div class="clearfix"></div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Heading 3*</label>
													<input type="text" class="form-control required" placeholder="Heading 3" name="heading_2_3" value="{{@$exp2->heading_3}}">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="all_time_sho">
												<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Description 3*</label>
														<textarea placeholder="Write description 3" name="description_2_3" id="description_3" class="form-control description required">{!! @$exp2->description_3 !!}</textarea>
													</div>
													<p class="error_4" id="cntnt"></p>
												</div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-4">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Image 3</label>
                                                    <input type="file" name="image_2_3" class="image_input form-control">
                                                    <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$exp2->image_3) }}" style="height: 150px; width: 150px; object-fit: contain; display: {{ @$exp2->image_3 ? 'block' : 'none' }};" alt="">
                                                </div>
                                            </div>
											<div class="clearfix"></div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Heading 4*</label>
													<input type="text" class="form-control required" placeholder="Heading 4" name="heading_2_4" value="{{@$exp2->heading_4}}">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="all_time_sho">
												<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Description 4*</label>
														<textarea placeholder="Write description 4" name="description_2_4" id="description_2_4" class="form-control description required">{!! @$exp2->description_4 !!}</textarea>
													</div>
													<p class="error_5" id="cntnt"></p>
												</div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-4">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Image 4</label>
                                                    <input type="file" name="image_2_4" class="image_input form-control">
                                                    <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$exp2->image_4) }}" style="height: 150px; width: 150px; object-fit: contain; display: {{ @$exp2->image_4 ? 'block' : 'none' }};" alt="">
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <h4 class="pull-left" style="margin-top: 24px;">Home Page Professionals Slider</h4>
                                            </div>

                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Title *</label>
													<input type="text" class="form-control required" placeholder="Write title" name="title_3" value="{{@$exp3->title}}">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="all_time_sho">
												<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Description *</label>
														<textarea placeholder="Write description" name="description_3" id="meet_description" rows="6" class="form-control required">{!! @$exp3->description !!}</textarea>
													</div>
													<p class="meet_description" id="cntnt"></p>
												</div>
                                            </div>

											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
													<input value="Update" type="submit" class="btn btn-default">
												</div>
                                            </div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>
<script>
    $(document).ready(function() {
        const regex = new RegExp("(.*?)\.(png|jpg|jpeg)$");
        $('.image_input').on('change', function() {
            const val = $(this).val().toLowerCase();
            if (!(regex.test(val))) {
                $(this).val('');
                alert('Only png and jpeg files are allowed.');
            }
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                const t = $(this);
                reader.onload = function(e) {
                    t.next('img').attr('src', e.target.result).show();
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
    });
 </script>
<script>
	$(document).ready(function(){
		$("#myform").validate();
		// tinyMCE.init({
        //     mode : "specific_textareas",
        //     editor_selector : "description",
        //     plugins: [
        //       'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
        //       'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
        //       'save table contextmenu directionality emoticons template paste textcolor'
        //     ],
        //     relative_urls : false,
        //     remove_script_host : false,
        //     convert_urls : true,
        //     toolbar: 'insertfile undo redo | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor emoticons',
        //     images_upload_url: '{{ URL::to('storage/app/public/uploads/content_image/') }}',
        //     images_upload_handler: function(blobInfo, success, failure) {
        //         var formD = new FormData();
        //         formD.append('file', blobInfo.blob(), blobInfo.filename());
        //         formD.append( "_token", '{{csrf_token()}}');
        //         $.ajax({
        //             url: '{{ route('artical.img.upload') }}',
        //             data: formD,
        //             type: 'POST',
        //             contentType: false,
        //             cache: false,
        //             processData:false,
        //             dataType: 'JSON',
        //             success: function(jsn) {
        //                 if(jsn.status == 'ERROR') {
        //                     failure(jsn.error);
        //                 } else if(jsn.status == 'SUCCESS') {
        //                     success(jsn.location);
        //                 }
        //             }
        //         });
        //     },
        // });
	});
</script>
</script>
<style>
	.error {
		color: red !important;
	}
	.error_1 {
		color: red !important;
	}
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
