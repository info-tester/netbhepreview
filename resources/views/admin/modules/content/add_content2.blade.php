@extends('admin.layouts.app')
@section('title', 'Vedic Astro World | Admin | Add Home Page Slider Content')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Add Home Page Slider Content</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.content.slider.index') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
										<form id="myform" method="post" action="{{ route('admin.content.slider.store') }}" enctype="multipart/form-data">
											@csrf
											
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Title*</label>
													<input type="text" class="form-control required" placeholder="Content Title" name="title">
												</div>
											</div>

											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Title 1*</label>
													<input type="text" class="form-control required" placeholder="Content Title" name="title_1">
												</div>
											</div>
											
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Position*</label>
													<input type="text" class="form-control required" placeholder="Position" name="position">
												</div>
											</div>
											<div class="clearfix"></div>
											
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Image*</label>
													<input type="file" class="form-control required" id="file" placeholder="Image" name="image" accept="image/*">
												</div>
											</div>
											
											{{-- <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Meta Description*</label>
													<input type="text" class="form-control required" placeholder="Meta Description" name="meta_desc">
												</div>
											</div> --}}
											<div class="clearfix"></div>

											{{-- <div class="all_time_sho">
												<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Content* </label>
														<textarea placeholder="Content Description" name="description" class="form-control message required"></textarea>
													</div>
												</div>
											</div>
											<div class="clearfix"></div> --}}
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="add_btnm submit-login" style="width:auto; margin-top:15px;">
													<input value="Add" type="submit" class="btn btn-default">
												</div>
											</div>
											<!--all_time_sho-->
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>
<script>
	$(document).ready(function(){
		$("#myform").validate({
			rules: {
				position : 'number',
			},
			errorPlacement: function() {
				//
			}
		});
	});
</script>
<style>
	.error {
		color: red !important;
	}
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection