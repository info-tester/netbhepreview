@extends('admin.layouts.app')
@section('title', 'Netbhe | Admin | Homepage Banners')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
<div class="content">
    <div class="container">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="pull-left page-title">Homepage Banners</h4>
            </div>
        </div>

        @include('admin.includes.error')

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body table-rep-plugin">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="link_style">
                                <a href="{{ route('home.banner.add') }}"><i class="fa fa-plus"></i> Add</a>
                                </div>
                            </div>
                            <div class="col-md-12 dess5">
                                <i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
                                <i class="fa fa-trash cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" data-pattern="priority-columns">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Heading</th>
                                                <th>Description</th>
                                                <th style="min-width:72px;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if (count($banners) > 0)
                                                @foreach($banners as $k => $banner)
                                                    <tr>
                                                        <td>{{ $k + 1 }}</td>
                                                        <td>{{ $banner->heading }}</td>
                                                        <td>{{ $banner->description }}</td>
                                                        <td>
                                                            <a href="{{ route('home.banner.edit', ['id' => $banner->id]) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
                                                            <a onclick="return confirm('Do you want to delete this banner content?')" href="{{ route('home.banner.delete', ['id' => $banner->id]) }}" title="Edit"> <i class="fa fa-trash delet" aria-hidden="true"></i></a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="4">No data found</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Row -->
    </div>
    <!-- container -->
</div>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
    <script>
        $('.xxx').click(function(){
            $("#myFormSearch").submit();
        });
    </script>
@endsection
