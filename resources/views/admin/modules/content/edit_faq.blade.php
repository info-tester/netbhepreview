@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Admin | Edit Faq')
@section('header')
@include('admin.includes.header')
<style>
    .your-mail input[type='url'] {
        width: 100%;
        height: 40px;
        /*line-height:50px;*/
        float: left;
        padding: 8px;
        margin-bottom: 8px;
        border: 1px solid #CCC;
        border-radius: 0;
        background: #fff !important;
        color: #000;
        font-family: "Open Sans", sans-serif;
        font-size: 14px;
        font-weight: 400;
    }
</style>
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			@include('admin.includes.error')
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Faq</h4>
					<!-- <div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.dashboard') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div> -->
				</div>
            </div>
            @if (!@$faqEdit)
                <div class="row">
                    @if (!@$faqEdit)
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-body table-rep-plugin">
                                    {{-- <h4>For View User FAQs</h4> --}}
                                    <div class="submit-login no_mmg">
                                        <a href="{{ route('admin.faq.update.user') }}" title="User FAQs"><button type="button" class="btn btn-default">For View User FAQs</button></a>
                                        <a href="{{ route('admin.faq.update.expert') }}" title="Expert FAQs"><button type="button" class="btn btn-default">For View Expert FAQs</button></a>
                                    </div> 
                                    {{-- <h4>User FAQs</h4>
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    
                                                    <th>Title</th>
                                                    <th>Description</th>
                                                    <th style="min-width:72px;">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if (count($faqItemsU) > 0)
                                                    @foreach($faqItemsU as $k => $faqItem)
                                                        <tr>
                                                            <td>{{ $k + 1 }}</td>
                                                            
                                                            <td>{{ $faqItem->title }}</td>
                                                            <td><p style="white-space: pre-wrap;">{{ strip_tags($faqItem->description) }}</p></td>
                                                            <td>
                                                                @if ($k != 0)
                                                                    <a href="{{ route('admin.faq.move', ['id' => $faqItem, 'dir' => 'UP']) }}" title="Move up"> <i class="fa fa-arrow-up delet" aria-hidden="true"></i></a>
                                                                @endif
                                                                @if ($k + 1 != $faqItemsU->count())
                                                                    <a href="{{ route('admin.faq.move', ['id' => $faqItem, 'dir' => 'DOWN']) }}" title="Move down"> <i class="fa fa-arrow-down delet" aria-hidden="true"></i></a>
                                                                @endif
                                                                <a href="{{ route('admin.faq.edit', ['id' => $faqItem]) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
                                                                <a onclick="return confirm('Do you want to delete this faq content?')" href="{{ route('admin.faq.delete', ['id' => $faqItem]) }}" title="Edit"> <i class="fa fa-trash delet" aria-hidden="true"></i></a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="4">No data found</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    <h4>Expert FAQs</h4>
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    
                                                    <th>Title</th>
                                                    <th>Description</th>
                                                    <th style="min-width:72px;">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if (count($faqItemsE) > 0)
                                                    @foreach($faqItemsE as $k => $faqItem)
                                                        <tr>
                                                            <td>{{ $k + 1 }}</td>
                                                            
                                                            <td>{{ $faqItem->title }}</td>
                                                            <td><p style="white-space: pre-wrap;">{{ strip_tags($faqItem->description) }}</p></td>
                                                            <td>
                                                                @if ($k != 0)
                                                                    <a href="{{ route('admin.faq.move', ['id' => $faqItem, 'dir' => 'UP']) }}" title="Move up"> <i class="fa fa-arrow-up delet" aria-hidden="true"></i></a>
                                                                @endif
                                                                @if ($k + 1 != $faqItemsE->count())
                                                                    <a href="{{ route('admin.faq.move', ['id' => $faqItem, 'dir' => 'DOWN']) }}" title="Move down"> <i class="fa fa-arrow-down delet" aria-hidden="true"></i></a>
                                                                @endif
                                                                <a href="{{ route('admin.faq.edit', ['id' => $faqItem]) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
                                                                <a onclick="return confirm('Do you want to delete this faq content?')" href="{{ route('admin.faq.delete', ['id' => $faqItem]) }}" title="Edit"> <i class="fa fa-trash delet" aria-hidden="true"></i></a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="4">No data found</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body table-rep-plugin">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12 nhp">
                                        <div class="table-responsive" data-pattern="priority-columns">
                                            <form id="myform" method="post" action="{{ route('admin.faq.update') }}" enctype="multipart/form-data">
                                                @csrf
                                                <div class="col-lg-12">
                                                    <h4>For Users</h4>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Title*</label>
                                                        <input type="text" class="form-control required" placeholder="Title" name="title" value="{{@$faq->title}}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">YouTube Video URL (Leave empty to disable video)</label>
                                                        <input type="url" class="form-control" placeholder="YouTube Video URL" name="heading_2" value="{{@$faq->heading_2}}">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                {{-- <div class="all_time_sho">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                                        <div class="your-mail">
                                                            <label for="exampleInputEmail1">Description*</label>
                                                            <textarea placeholder="Write description" name="description" id="description" class="form-control description required">{!! @$faq->description !!}</textarea>
                                                        </div>
                                                        <p class="error_1" id="cntnt"></p>
                                                    </div>
                                                </div> --}}
                                                <div class="clearfix"></div>
                                                <div class="col-lg-12" style="margin-top: 16px;">
                                                    <h4>For Experts</h4>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Title*</label>
                                                        <input type="text" class="form-control required" placeholder="Title" name="heading_1" value="{{@$faq->heading_1}}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">YouTube Video URL (Leave empty to disable video)</label>
                                                        <input type="url" class="form-control" placeholder="YouTube Video URL" name="heading_3" value="{{@$faq->heading_3}}">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                {{-- <div class="all_time_sho">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                                        <div class="your-mail">
                                                            <label for="exampleInputEmail1">Description*</label>
                                                            <textarea placeholder="Write description" name="description_1" id="description_1" class="form-control description required">{!! @$faq->description_1 !!}</textarea>
                                                        </div>
                                                        <p class="error_1" id="cntnt"></p>
                                                    </div>
                                                </div> --}}
                                                <div class="clearfix"></div>
                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                    <div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
                                                        <input value="Update" type="submit" class="btn btn-default">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
										<form id="myform1" method="post" action="{{ @$faqEdit ? route('admin.faq.edit', ['id' => $faqEdit]) : route('admin.faq.create') }}">
                                            @csrf
                                            <div class="col-lg-12">
                                                <h4>{{ @$faqEdit ? 'Edit' : 'Add' }} FAQ</h4>
                                            </div>
											<div class="col-lg-4">
												<div class="your-mail">
                                                    <label for="exampleInputEmail1">FAQ For*</label>
                                                    <select name="type" id="type" class="newdrop form-control" required>
                                                        <option value="USER" {{ @$faqEdit->type == 'USER' ? 'selected' : '' }}>Users</option>
                                                        <option value="EXPERT" {{ @$faqEdit->type == 'EXPERT' ? 'selected' : '' }}>Experts</option>
                                                    </select>
												</div>
											</div>
											<div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">FAQ Title*</label>
													<input type="text" class="form-control required" placeholder="Title" name="title" value="{{ @$faqEdit->title }}">
												</div>
											</div>
											<div class="col-lg-12">
												<div class="your-mail">
                                                    <label for="exampleInputEmail1">FAQ Description*</label>
                                                    <textarea name="description" id="description" class="form-control description required" style="white-space: pre-wrap;">{{ @$faqEdit->description }}</textarea>
												</div>
											</div>
                                            <div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Add Youtube Video</label>
													<input type="url" class="form-control" placeholder="Youtube Video" name="video" value="{{ @$faqEdit->video }}">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-12">
												<div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
													<input value="Save" type="submit" class="btn btn-default">
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
                </div>


			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>
<script>
	$(document).ready(function(){
		$("#myform").validate();
		$("#myform1").validate({
            ignore: [],
        });
	    tinyMCE.init({
	        mode : "specific_textareas",
	        editor_selector : "description",
	        height: '320px',

	        forced_root_block : "",
		    force_br_newlines : true,
		    force_p_newlines : false,

	        plugins: [
	          'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
	          'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
	          'save table contextmenu directionality emoticons template paste textcolor'
	        ],
	        relative_urls : false,
	        remove_script_host : false,
	        convert_urls : true,
	        toolbar: 'insertfile undo redo | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor emoticons',
            images_upload_url: '{{ env("PATH_TO_FAQ_CONTENT_IMAGE") }}',
            images_upload_handler: function(blobInfo, success, failure) {
                var formD = new FormData();
                formD.append('file', blobInfo.blob(), blobInfo.filename());
                formD.append( "_token", '{{ csrf_token() }}');
                $.ajax({
                    url: '{{ route("admin.faq.image.upload") }}',
                    data: formD,
                    type: 'POST',
                    contentType: false,
                    cache: false,
                    processData:false,
                    dataType: 'JSON',
                    success: function(jsn) {
                        if(jsn.status == 'ERROR') {
                            failure(jsn.error);
                        } else if(jsn.status == 'SUCCESS') {
                            success(jsn.location);
                        }
                    }
                });
            },
        });
	});
</script>
</script>
<style>
	.error {
		color: red !important;
	}
	.error_1 {
		color: red !important;
	}
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
