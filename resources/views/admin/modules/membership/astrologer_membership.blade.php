@extends('admin.layouts.app')
@section('title', 'Vedic Astro World | Admin | Astrologers Membership')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Astrologer Membership</h4>
				</div>
			</div>
			<div class="row">
				
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="panel panel-default">
									<div class="panel-body table-rep-plugin">
										<form method="get" action="{{ route('admin.users.membership') }}">
											
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<select class="form-control newdrop required" name="astrologer">
														<option value="">Select Astrologer</option>
														@foreach($astrologers as $row)
														<option value="{{ $row->id }}" @if(@$post['astrologer'] == $row->id) selected @endif >{{ $row->name }}</option>
														@endforeach
													</select>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<select class="form-control newdrop required" name="payment_method">
														<option value="">Payment Method</option>
														
														<option value="P" @if(@$post['payment_method']=="P") selected @endif >Paypal</option>
														<option value="C" @if(@$post['payment_method']=="C") selected @endif >CCAvenue</option>
													</select>
												</div>
											</div>
											

											
											<div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                
                                                <input type="text" class="form-control" name="activation_date" id="datepicker" value="{{@$post['activation_date']}}" placeholder="Activation Date" readonly>
                                            </div>
                                        </div>
                                       
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <input type="text" class="form-control" name="expiry_date" id="datepicker1" value="{{@$post['expiry_date']}}" placeholder=" Expiry date" readonly>
                                            </div>
                                        </div>



										
										 <div class="form-action">
												<div class="add_btnm pull-left" style="width:auto; margin-top:15px;">
													<input value="Search" class="btn btn-primary" type="submit">
												</div>
											</div>
										</form>
									</div>

								</div>
								
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Astrologer</th>
													<th>One Time Fee</th>
													<th>Yearly Fee</th>
													<th>Total Membership Fee</th>
													<th>Activation Date</th>
													<th>Expiry Date</th>
													<th>Status</th>
													<th>Payment Ref No.</th>
													<th>Payment Method</th>
												</tr>
											</thead>
											<tbody>
												@foreach($user_membership as $row)
												<tr>
													<td>{{ @$row->astrologerName->nick_name ? @$row->astrologerName->nick_name : @$row->astrologerName->name }}</td>
													@php 
													$currency = '';
													@endphp
													@if($row->currency_id == 1) 
														@php $currency = '&#8377;'; @endphp
													@elseif($row->currency_id == 2)
														@php $currency = '$'; @endphp
													@elseif($row->currency_id == 3)
														@php $currency = '&euro;'; @endphp
													@endif
													<td>{!! $currency . $row->one_time_fees !!}</td>
													<td>{!! $currency . $row->yearly_fees !!}</td>
													<td>{!! $currency . $row->total_subscription_fees !!}</td>
													
													<td>{{ date('d/m/Y', strtotime($row->created_at)) }}</td>
													<td>{{ $futureDate = date('d/m/Y', strtotime('+1 year', strtotime($row->created_at))) }}</td>
													<td>@if($row->status == 'C') Completed @elseif($row->status == 'I') Initiated @elseif($row->status == 'X') Faild @endif</td>
													<td>{{ $row->txn_id?$row->txn_id:'--' }}</td>
													<td>@if($row->payment_method == 'P') Paypal
                                                        @elseif($row->payment_method == 'C') CCAvenue
                                                        @else Free Membership
                                                        @endif
                                                        {{--  {{ $row->payment_method == 'P'?'Paypal': 'CCAvenue' }}  --}}</td>
													
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<form method="post" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
	var confirm = window.confirm('Are you want to delete this news ?');
	if(confirm){
		$("#destroy").attr('action',val);
		$("#destroy").submit();
	}
 }
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection