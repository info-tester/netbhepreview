@extends('admin.layout.auth')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Buyer Membership</h4>
					
				</div>
			</div>
			<div class="row">
				
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="panel panel-default">
									<div class="panel-body table-rep-plugin">
										<form method="get" action="">
											
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<select class="form-control newdrop required" name="plan" id="plan">
														<option value="">Select plan</option>
														@foreach($plan as $row)
														<option value="{{ $row->id }}" @if(@$post['plan'] == $row->id) selected @endif >{{ $row->plan }}</option>
														@endforeach
													</select>
												</div>
											</div>

											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													
													<select class="form-control newdrop required" name="payment_status" id="plan">
														<option value="">Payment status</option>
														<option value="I" @if(@$post['payment_status'] == 'I') selected @endif>Initiated</option>
														<option value="S" @if(@$post['payment_status'] == 'S') selected @endif>Success</option>
														<option value="C" @if(@$post['payment_status'] == 'C') selected @endif>Cancel</option>
														<option value="F" @if(@$post['payment_status'] == 'F') selected @endif>Faild</option>
														
													</select>
												</div>
											</div>

											

											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												<div class="add_btnm pull-right" style="width:auto; margin-top:15px;">
													<input value="Search" class="btn btn-primary" type="submit">
												</div>
											</div>
										</form>
									</div>
								</div>
								{{-- <div class="col-md-12 dess5">
									<i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
									<i class="fa fa-trash-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
									<i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active</span></i>
									<i class="fa fa-times cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">Inactive</span></i>
								</div> --}}
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Plan</th>
													<th>Seller</th>
													<th>Activation date</th>
													<th>Expiry date</th>
													<th>Status</th>
													<th>Payment status</th>
													<th>Payment ref no.</th>
													<th>Payment type</th>
												</tr>
											</thead>
											<tbody>
												@foreach($seller_membership as $row)
												<tr>
													<td>{{ $row->planName->plan }}</td>
													<td>{{ $row->buyerName->first_name }}</td>
													<td>{{ niceDate($row->created_at) }}</td>
													<td>{{ niceDate($row->expiry_date) }}</td>
													<td>@if($row->status == 'A') Active @elseif($row->status == 'I') Inactive @endif</td>
													<td>@if($row->payment_status == 'S') Success @elseif($row->status == 'X') Cancel @elseif($row->status == 'I') Initiated  @elseif($row->status == 'C') Faild  @endif</td>
													<td>{{ $row->payment_reference?$row->payment_reference:'--' }}</td>
													<td>{{ $row->payment_type }}</td>
													
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<form method="post" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
	var confirm = window.confirm('Are you want to delete this news ?');
	if(confirm){
		$("#destroy").attr('action',val);
		$("#destroy").submit();
	}
 }
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection