@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Admin | Update profile')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Update Profile</h4>
                    <div class="submit-login no_mmg pull-right">
                        <a href="{{ route('admin.dashboard') }}"><button type="button" class="btn btn-default">Back</button></a>
                    </div>
                </div>
            </div>
            @include('admin.includes.error')
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        
                        <div class="panel-body table-rep-plugin">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 nhp">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        {{-- @if(\Auth::user()->user_type == 1) --}}
                                        <form id="myform" method="post" action="{{ route('admin.update.password') }}">
                                            @csrf
                                            <input type="hidden" name="id" value="{{ Auth::guard('admin')->user()->id }}">
                                            <div class="all_time_sho">      
                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Name</label>
                                                        <input type="text" class="form-control required" placeholder="" value="{{ Auth::guard('admin')->user()->name }}" name="name">
                                                    </div>
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Email</label>
                                                        <input type="text" class="form-control" placeholder="" value="{{ Auth::guard('admin')->user()->email }}" name="email" readonly="true">
                                                    </div>
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">New Password</label>
                                                        <input type="password" class="form-control" id="password" placeholder="New Password" name="password">
                                                    </div>
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Confirm New Password</label>
                                                        <input type="password" class="form-control" placeholder="Confirm New Password" name="password_confirmation">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="add_btnm submit-login">
                                                    <input value="Save" type="submit" class="btn btn-default">
                                                </div>
                                            </div>
                                        </form>
                                        {{-- @else
                                        <form id="myform2" method="post" action="{{ route('settings.update',[$user->id,'admin' => $user->user_type]) }}">
                                            <input name="_method" type="hidden" value="PUT">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="id" value="{{ $user->id }}">
                                            <input type="hidden" name="type" value="{{ $user->user_type }}">
                                            <div class="all_time_sho">      
                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">First Name</label>
                                                        <input type="text" class="form-control" placeholder="" value="{{ $user->first_name }}" name="first_name">
                                                    </div>
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Last Name</label>
                                                        <input type="text" class="form-control" placeholder="" value="{{ $user->last_name }}" name="last_name">
                                                    </div>
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">New Password</label>
                                                        <input type="password" class="form-control" id="password" placeholder="New Password" name="new_password">
                                                    </div>
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Confirm New Password</label>
                                                        <input type="password" class="form-control" placeholder="Confirm New Password" name="confirm_password">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="add_btnm">
                                                    <input value="Save" type="submit">
                                                </div>
                                            </div>
                                        </form>
                                        @endif --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- container -->
    </div> <!-- content -->
    <!--<footer class="footer text-right">
        2015 © Moltran.
    </footer>-->
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $("#myform").validate({
            rules  : {
                name                   : {required: true},
                // first_name          : {required: true},
                // last_name           : {required: true},
                password_confirmation  : {equalTo : "#password"},
            },
            messages : {              
                name                   : "Please enter name",
                // first_name          : "Please enter first name",
                // last_name           : "Please enter last name",
                password_confirmation  : "Please enter same password again",
            }
        });
    });
</script>
{{-- <style type="text/css">
  .error{
    color:red !important;
  }
</style>
 --}}
@endsection

@section('footer')
@include('admin.includes.footer')
@endsection