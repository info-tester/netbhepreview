@extends('admin.layout.auth')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Edit News</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.news.index') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
										<form id="myform" method="post" action="{{ route('admin.news.update',[@$news->id]) }}" enctype="multipart/form-data">
											{{ csrf_field() }}
											{{method_field('PUT')}}
											
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Title</label>
													<input type="text" class="form-control required" id="meta_title" placeholder="Title" name="title" value="{{@$news->title}}">
												</div>
											</div>
											
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Added By</label>
													<input type="text" class="form-control required" id="added_by" placeholder="Added By" name="added_by" value="{{@$news->added_by}}">
												</div>
											</div>
											
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Image</label>
													<input type="file" class="form-control file" id="file" placeholder="Image" name="image" accept="image/*">
													<span class="file-name"><img src="{{ url(IMG_PATH['news'].@$news->image) }}" alt="{{ @$news->original_name }}" style="width: 100px; height: 100px;"></span>
												</div>
											</div>
											<!--all_time_sho-->
											<div class="all_time_sho">
												<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Description</label>
														<textarea placeholder="Description" id="meta_desc" name="description"rows="3" class="form-control message required">{{ strip_tags(@$news->description) }}</textarea>
													</div>
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="add_btnm">
													<input value="Update" type="submit" class="btn btn-primary">
												</div>
											</div>
											<!--all_time_sho-->
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>
<script>
	$(document).ready(function(){
		$('.file').change(function() {
		    if ($(this).val()) {
		      	$(".file-name").show();
		      	$('.file-name').html('');
		    }
		});
		$("#myform").validate();
	});
</script>
<style>
	.error {
		color: red !important;
	}
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection