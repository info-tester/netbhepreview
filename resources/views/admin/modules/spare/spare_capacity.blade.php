@extends('admin.layout.auth')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div id="wrapper">
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="pull-left page-title">Manage Spare Capacity</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body table-rep-plugin">
                                <div class="row top_from">
                                    <form method="get" action="{{ route('admin.spare.capacity')}}">
                                        {{-- <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">Keyword</label>
                                                <input type="text" class="form-control" name="order_number" id="order_number" value="{{@$key['order_number']}}" placeholder="">
                                            </div>
                                        </div> --}}
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">Type</label>
                                                <select name="type" class="form-control newdrop">
                                                    <option value="">Choose Type</option>
                                                    <option value="M" @if(@$key['type'] == "M") {{ "selected" }} @endif>Listing</option>
                                                    <option value="S" @if(@$key['type'] == "S") {{ "selected" }} @endif>Seeking</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">Product</label>
                                                <select name="product" class="form-control newdrop">
                                                    <option value="">Choose Product</option>
                                                    @if(@$product)
                                                    @foreach($product as $val)
                                                        <option value="{{$val->id}}" @if(@$key['product'] == $val->id) {{ "selected" }} @endif>{{$val->name}}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">Purpose</label>
                                                <select name="purpose" class="form-control newdrop">
                                                    <option value="">Choose Purpose</option>
                                                    @foreach(STATIC_ARRAY['purpose'] as $row => $val)
                                                        <option value="{{$row}}" @if(@$key['purpose'] == $row) {{ "selected" }} @endif>{{$val}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="add_btnm pull-right">
                                                    <input value="Search" id="search" class="btn btn-primary" type="submit">
                                                </div>
                                        </div>
                                    </form>
                                </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 dess5">
                                        <i class="fa fa-eye cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">view</span></i>
                                        {{-- <i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Accept</span></i>
                                        <i class="fa fa-times cncl" aria-hidden="true"> <span class="cncl_oopo">Reject</span></i>
                                        <i class="fa fa-times-circle cncl" aria-hidden="true"> <span class="cncl_oopo">Cancel</span></i>
                                        <i class="fa fa-user cncl" aria-hidden="true"> <span class="cncl_oopo">Assign</span></i>
                                        <i class="fa fa-info cncl" aria-hidden="true"> <span class="cncl_oopo">Raise Issue</span></i>
                                        <i class="fa fa-share-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delivered</span></i> --}}
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" data-pattern="priority-columns">
                                            <table id="datatable" class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Type</th>
                                                        <th>User name</th>
                                                        <th>Product</th>
                                                        <th>Purpose</th>
                                                        <th>Total Capacity</th>
                                                        <th>Spare Capacity</th>
                                                        <th>Unit</th>
                                                        <th>Availability From</th>
                                                        <th>Availability To</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($spare as $row)
                                                    <tr>
                                                        <td>
                                                            @if($row->type == 'M')
                                                                Listing
                                                            @elseif($row->type == 'S')
                                                                Seeking
                                                            @else
                                                                N.A
                                                            @endif
                                                        </td>
                                                        <td>{{-- 
                                                            @if(@$row->UserName->user_type == "B" || @$row->UserName->user_type == "BS")
                                                                <a href="{{ route('admin.buyer.show',[$row->UserName->id]) }}">{{$row->UserName->first_name}} {{$row->UserName->last_name}}</a>
                                                            @elseif(@$row->UserName->user_type == "S" || @$row->UserName->user_type == "SS")
                                                                <a href="{{ route('admin.seller.show',[$row->UserName->id]) }}">{{$row->UserName->first_name}} {{$row->UserName->last_name}}</a>
                                                            @else
                                                                N.A
                                                            @endif --}}
                                                            {{@$row->UserName->first_name ? $row->UserName->first_name : 'N.A'}} {{$row->UserName->last_name}}</td>
                                                        <td>
                                                            @if(@$row->SpareProduct->isNotEmpty())
                                                                @foreach($row->SpareProduct as $val)
                                                                    {{ $val->ProductName->name }}
                                                                    @if($loop->remaining != 0)
                                                                        ,
                                                                    @endif
                                                                @endforeach
                                                            @else
                                                                N.A
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if(@$row->PurposeName->isNotEmpty())
                                                                @foreach($row->PurposeName as $val)
                                                                    {{STATIC_ARRAY['purpose'][$val->purpose_id]}}
                                                                    @if($loop->remaining != 0)
                                                                        ,
                                                                    @endif
                                                                @endforeach
                                                            @else
                                                                N.A
                                                            @endif
                                                        </td>
                                                        <td>{{@$row->total_capacity ? $row->total_capacity : 'N.A'}}</td>
                                                        <td>{{@$row->spare_capacity ? $row->spare_capacity : 'N.A'}}</td>
                                                        <td>
                                                            @if(@$row->unit_id)
                                                                {{STATIC_ARRAY['capacity_unit'][$row->unit_id]}}
                                                            @else
                                                                N.A
                                                            @endif
                                                        </td>
                                                        <td><span class="dateee">
                                                            @if(@$row->availability_from)
                                                                {{niceDate($row->availability_from)}}
                                                            @else
                                                                N.A
                                                            @endif</span>
                                                        </td>
                                                        <td><span class="dateee">
                                                            @if(@$row->availability_to)
                                                                {{niceDate($row->availability_to)}}
                                                            @else
                                                                N.A
                                                            @endif</span>
                                                        </td>
                                                                          
                                                        <td>
                                                            <a href="{{route('admin.spare.capacity.view',[$row->id])}}"  title="View"><i class="fa fa-eye delet" aria-hidden="true"></i></a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Row -->
            </div>
            <!-- container -->
        </div>
        <!-- content -->
    </div>
</div>

    
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection