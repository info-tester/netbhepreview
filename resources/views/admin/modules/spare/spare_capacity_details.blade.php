@extends('admin.layout.auth')
@section('title', 'Buyer Details')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->                      
<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="pull-left page-title">Spare Capacity Detail</h4>
          <div class="submit-login no_mmg pull-right">
            <a href="{{ URL::previous() }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
          </div>
          <!--<ol class="breadcrumb pull-right">
            <li><a href="#">User Dashboard</a></li>
            <li class="active">Order Detail</li>
            </ol>-->
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-default">
            <!--<div class="panel-heading">
              <h3 class="panel-title">Default Example</h3>
              </div>-->
            <div class="panel-body">
              <div class="row">
                <div class="col-lg-12 col-md-12">
                <h4 class="pull-left page-title">Spare Capacity Info</h4>
                  <div class="order-detail">
                    <!--order-detail start-->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <div class="order-id">
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>User Name :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <span>
                            @if(@$spare->UserName->user_type == "B" || @$spare->UserName->user_type == "BS")
                                <a href="{{ route('admin.buyer.show',[$spare->UserName->id]) }}" title="Details">{{$spare->UserName->first_name}} {{$spare->UserName->last_name}}</a>
                            @elseif(@$spare->UserName->user_type == "S" || @$spare->UserName->user_type == "SS")
                                <a href="{{ route('admin.seller.show',[$spare->UserName->id]) }}" title="Details">{{$spare->UserName->first_name}} {{$spare->UserName->last_name}}</a>
                            @else
                                N.A
                            @endif{{-- {{@$spare->UserName->first_name ? $spare->UserName->first_name : 'N.A'}} {{$spare->UserName->last_name}} --}}
                          </span>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Type :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <strong>
                            @if($spare->type == 'M')
                                Listing
                            @elseif($spare->type == 'S')
                                Seeking
                            @else
                                N.A
                            @endif
                          </strong>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Availabilty From:</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <strong>
                            @if(@$spare->availability_from)
                                {{niceDate($spare->availability_from)}}
                            @else
                                N.A
                            @endif
                          </strong>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Availabilty To:</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <strong>
                            @if(@$spare->availability_to)
                                {{niceDate($spare->availability_to)}}
                            @else
                                N.A
                            @endif
                          </strong>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <div class="order-id">
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Total Capacity :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <strong>{{@$spare->total_capacity ? $spare->total_capacity : 'N.A'}}</strong>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Spare Capacity :</p>
                        </div>
                        <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5">
                          <strong>{{@$spare->spare_capacity ? $spare->spare_capacity : 'N.A'}}</strong>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Unit :</p>
                        </div>
                        <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5">
                          <strong>
                            @if(@$spare->unit_id)
                                {{STATIC_ARRAY['capacity_unit'][$spare->unit_id]}}
                            @else
                                N.A
                            @endif
                          </strong>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Created On :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          @php
                          $created_on=date("d-m-Y", strtotime($spare->created_at));
                          @endphp
                          <strong>{{ @$created_on ? niceDate($created_on) : 'N.A' }}</strong>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--order-detail end-->
                </div>
                <div class="col-md-12">
                  <!--<i class="fa fa-pencil-square-o cncl" aria-hidden="true"> Edit</i> -->
                  <!--<i class="fa fa-pencil cncl" aria-hidden="true"> Review</i>-->
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12">
                <h4 class="pull-left page-title">Products</h4>
                  <div class="order-detail">
                    <!--order-detail start-->
                    <div class="col-lg-12">
                      <div class="order-id">
                        <div class="col-lg-12">
                          <span>
                            @if(@$spare->SpareProduct->isNotEmpty())
                                @foreach($spare->SpareProduct as $val)
                                    {{ $val->ProductName->name }}
                                    @if($loop->remaining != 0)
                                        ,
                                    @endif
                                @endforeach
                            @else
                                N.A
                            @endif
                          </span>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                  <!--order-detail end-->
                </div>
                <div class="col-md-12">
                  <!--<i class="fa fa-pencil-square-o cncl" aria-hidden="true"> Edit</i> -->
                  <!--<i class="fa fa-pencil cncl" aria-hidden="true"> Review</i>-->
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12">
                <h4 class="pull-left page-title">Purpose</h4>
                  <div class="order-detail">
                    <!--order-detail start-->
                    <div class="col-lg-12">
                      <div class="order-id">
                        <div class="col-lg-12">
                          <span>
                            @if(@$spare->PurposeName->isNotEmpty())
                                @foreach($spare->PurposeName as $val)
                                    {{STATIC_ARRAY['purpose'][$val->purpose_id]}}
                                    @if($loop->remaining != 0)
                                        ,
                                    @endif
                                @endforeach
                            @else
                                N.A
                            @endif
                          </span>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                  <!--order-detail end-->
                </div>
                <div class="col-md-12">
                  <!--<i class="fa fa-pencil-square-o cncl" aria-hidden="true"> Edit</i> -->
                  <!--<i class="fa fa-pencil cncl" aria-hidden="true"> Review</i>-->
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12">
                <h4 class="pull-left page-title">Process Description</h4>
                  <div class="order-detail">
                    <!--order-detail start-->
                    <div class="col-lg-12">
                      <div class="order-id">
                        <div class="col-lg-12">
                          <span>{!! @$spare->process_description ? $spare->process_description : 'N.A' !!}</span>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                  <!--order-detail end-->
                </div>
                <div class="col-md-12">
                  <!--<i class="fa fa-pencil-square-o cncl" aria-hidden="true"> Edit</i> -->
                  <!--<i class="fa fa-pencil cncl" aria-hidden="true"> Review</i>-->
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12">
                <h4 class="pull-left page-title">Additional Information</h4>
                  <div class="order-detail">
                    <!--order-detail start-->
                    <div class="col-lg-12">
                      <div class="order-id">
                        <div class="col-lg-12">
                          <span>{!! @$spare->additional_information ? $spare->additional_information : 'N.A' !!}</span>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                  <!--order-detail end-->
                </div>
                <div class="col-md-12">
                  <!--<i class="fa fa-pencil-square-o cncl" aria-hidden="true"> Edit</i> -->
                  <!--<i class="fa fa-pencil cncl" aria-hidden="true"> Review</i>-->
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12">
                <h4 class="pull-left page-title">Address</h4>
                  <div class="order-detail">
                    <!--order-detail start-->
                    {{-- @foreach(@$user->company->branch as $branch)
                    <div>
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="order-id">
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                            <p>Branch Name :</p>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                            <span>{{ @$branch->name ? $branch->name : 'N.A' }}</span>
                          </div>
                          <div class="clearfix"></div>
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                            <p>Address :</p>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                            <strong>{{ @$branch->full_address ? $branch->full_address : 'N.A' }}</strong>
                          </div>
                          <div class="clearfix"></div>
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                            <p>City :</p>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                            <strong>{{ @$branch->city ? $branch->city : 'N.A' }}</strong>
                          </div>
                          <div class="clearfix"></div>
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                            <p>State :</p>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                            <strong>{{ @$branch->state ? $branch->state : 'N.A' }}</strong>
                          </div>
                          <div class="clearfix"></div>
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                            <p>Country :</p>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                            <strong>{{ @$branch->country->country_name ? $branch->country->country_name : 'N.A' }}</strong>
                          </div>
                          <div class="clearfix"></div>
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                            <p>Status :</p>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                            <strong> 
                              @if($branch->status == 'A')
                              {{ 'Active' }}
                              @elseif($branch->status == 'B')
                              {{ 'Inactive' }}
                              @elseif($branch->status == 'AA')
                              {{ 'Awaiting Approval' }}
                              @endif
                            </strong>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="order-id">
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                            <p>Bank Account No :</p>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                            <strong>{{ @$branch->bank_account ? $branch->bank_account : 'N.A' }}</strong>
                          </div>
                          <div class="clearfix"></div>
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                            <p>IFSC :</p>
                          </div>
                          <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5">
                            <strong>{{ @$branch->ifsc ? $branch->ifsc : 'N.A' }}</strong>
                          </div>
                          <div class="clearfix"></div>
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                            <p>GST :</p>
                          </div>
                          <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5">
                            <strong>{{ @$branch->gst ? $branch->gst : 'N.A' }}</strong>
                          </div>
                          <div class="clearfix"></div>
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                            <p>Created By :</p>
                          </div>
                          <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5">
                            <strong>{{ @$branch->user_name->first_name ? $branch->user_name->first_name : 'N.A' }} {{ @$branch->user_name->last_name ? $branch->user_name->last_name : 'N.A' }}</strong>
                          </div>
                          <div class="clearfix"></div>
                          <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                            <p>Created On :</p>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                            @php
                            $created_on=date("d-m-Y", strtotime($branch->created_at));
                            @endphp
                            <strong>{{ @$created_on ? $created_on : 'N.A' }}</strong>
                          </div>
                        </div>
                      </div>
                        <div class="clearfix"></div>
                        <hr>
                    </div>
                    @endforeach --}}

                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="table-responsive" data-pattern="priority-columns">
                        <table id="{{-- datatable --}}" class="table table-striped table-bordered">
                          <thead>
                            <tr>
                             
                              {{-- <th>Address</th> --}}
                              <th>City</th>
                              <th>State</th>
                              <th>Country</th>
                             {{--  <th>Pincode</th> --}}
                              {{-- <th>Action</th> --}}
                            </tr>
                          </thead>
                          <tbody>
                          @if(count($spare->SpareAddress) > 0)
                            @foreach(@$spare->SpareAddress as $row)
                              <tr>
                                 {{--  <td>{{ @$row->address ? $row->address : 'N.A' }}</td> --}}
                                  <td>{{ @$row->city ? $row->city : 'N.A' }}</td>
                                  <td>{{ @$row->state ? $row->state : 'N.A' }}</td>
                                  <td>{{ @$row->CountryName->country_name ? $row->CountryName->country_name : 'N.A' }}</td>
                                 {{--  <td>{{ @$row->pincode ? $row->pincode : 'N.A' }}</td> --}}
                                  
                                  
                                  
                                {{-- <td>
                                  <a href="{{ route('admin.buyer.show',[$buyer->id]) }}" title="View"> <i class="fa fa-eye delet" aria-hidden="true"></i></a>
                                  @if($buyer->status == 'AA' || $buyer->status == 'U')
                                  <a href="{{ route('admin.buyer-reject',[$buyer->id]) }}" title="Reject" onclick="return confirm('Do you want to Reject this Buyer?');"> <i class="fa fa-times-circle" aria-hidden="true"></i></a>
                                  @else
                                  <a href="javascript:void(0)" onclick="deleteCategory('{{route('admin.buyer.destroy',[$buyer->id])}}')" title="Delete"> <i class="fa fa-trash-o delet" aria-hidden="true"></i></a>
                                  @endif
                                  @if($buyer->status == 'I')
                                  <a href="{{ route('admin.buyer-status',[$buyer->id]) }}" title="Active" onclick="return confirm('Do you want to Active this buyer?');"> <i class="fa fa-check delet" aria-hidden="true"></i></a>
                                  @elseif($buyer->status == 'A')
                                  <a href="{{ route('admin.buyer-status',[$buyer->id]) }}" title="Inactive" onclick="return confirm('Do you want to Inactive this buyer?');"> <i class="fa fa-times delet" aria-hidden="true"></i></a>
                                  @elseif($buyer->status == 'AA')
                                  <a href="{{ route('admin.buyer-status',[$buyer->id]) }}" title="Approve" onclick="return confirm('Do you want to Approve this buyer?');"> <i class="fa fa-check-circle delet" aria-hidden="true"></i></a>
                                  @elseif($buyer->status == 'U')
                                  <a href="{{ route('admin.buyer-status',[$buyer->id]) }}" title="Verify Email Send" onclick="return confirm('Do you want to Verify Email Send this buyer?');"> <i class="fa fa-envelope-square delet" aria-hidden="true"></i></a>
                                  @endif
                                </td> --}}
                              </tr>
                            @endforeach
                          @else 
                            <tr>
                              <td colspan="8" align="center">N.A</td>
                            </tr>
                          @endif
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <!--order-detail end-->
                </div>
                <div class="col-md-12">
                  <!--<i class="fa fa-pencil-square-o cncl" aria-hidden="true"> Edit</i> -->
                  <!--<i class="fa fa-pencil cncl" aria-hidden="true"> Review</i>-->
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End Row -->
  </div>
  <!-- container -->
</div>
<!-- content -->
{{-- <footer class="footer text-right">
  2018 &copy; ProQRX.
</footer> --}}
</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
<!-- Right Sidebar -->
<!-- /Right-bar -->
</div>
<!-- END wrapper -->
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection