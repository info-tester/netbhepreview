@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Admin | Product Orders')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- Begin page -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="pull-left page-title">Manage Cancel Product Orders</h4>

                        <!--<ol class="breadcrumb pull-right">
                            <li><a href="#">User Dashboard</a></li>
                            <li class="active">My Order</li>
                            </ol>-->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body table-rep-plugin">
                                <div class="row top_from">
                                    <form method="post" action="{{ route('admin.order.cancel.product.search')}}">
                                        @csrf
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">Order No</label>
                                                <input type="text" class="form-control" name="token_no" id="token_no"
                                                    value="{{@$key['token_no']}}" placeholder="Token number">
                                            </div>
                                        </div>

                                        {{-- <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">Customer Name</label>
                                                <input type="text" class="form-control" name="user_name" id="user_name"
                                                    value="{{@$key['user_name']}}" placeholder="User name">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">Professional Name</label>
                                                <input type="text" class="form-control" name="professional_name" id="professional_name"
                                                    value="{{@$key['professional_name']}}" placeholder="User name">
                                            </div>
                                        </div> --}}

                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="submit-login add_btnm" style="width:auto; margin-top:35px;">
                                                <input value="Search" id="search" class="btn btn-default" type="submit">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 dess5">
                                    {{-- <i class="fa fa-eye cncl" aria-hidden="true"> <span class="cncl_oopo">View</span></i> --}}
                                    <i class="fa fa-check-circle cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">Cancel Request Approve</span></i>

                                </div>
                                <div class="clearfix"></div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <table id="datatable" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Order No.</th>
                                                    <th>Professional </th>
                                                    <th>Customer</th>
                                                    <th>Product</th>
                                                    {{-- <th>Date </th> --}}
                                                    <th>Amount </th>
                                                    <th>Payment Type</th>
                                                    <th>Cancellation Date</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($allOrderList as $row)
                                                <tr>
                                                    <td><span class="dateee">
                                                        {{ $row->orderMaster->token_no }}
                                                    </td>
                                                    <td>{{@$row->profDetails->nick_name ? @$row->profDetails->nick_name : @$row->profDetails->name}}</td>
                                                    <td>{{@$row->orderMaster->userDetails->nick_name ? @$row->orderMaster->userDetails->nick_name : @$row->orderMaster->userDetails->name}}</td>
                                                    <td>{{@$row->product->title}}</td>
                                                    {{-- <td>{{ @$row->orderMaster->order_date ? date('jS-M-Y', strtotime($row->orderMaster->order_date)) : '' }}</td> --}}

                                                    <td>
                                                        ${{(@$row->amount)}}
                                                    </td>
                                                    <td>
                                                        @if(@$row->orderMaster->payment_type=='C' && @$row->orderMaster->no_of_installment==1)
                                                        wirecard
                                                        @elseif(@$row->orderMaster->payment_type=='C' && @$row->orderMaster->no_of_installment>1)
                                                        Installment
                                                        @elseif(@$row->orderMaster->payment_type=='BA')
                                                        Bank Account
                                                        @elseif(@$row->orderMaster->payment_type=='S')
                                                        Stripe
                                                        @elseif(@$row->orderMaster->payment_type=='P')
                                                        Paypal
                                                        @elseif(@$row->orderMaster->wallet==@$row->orderMaster->sub_total)
                                                        Wallet
                                                        @elseif(@$row->orderMaster->amount==0)
                                                        Free Session
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if(@$row->cancelled_on)
                                                            {{ date('jS F Y', strtotime(@$row->cancelled_on)) }}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if(@$row->order_status == 'A' && @$row->cancel_request =='N' && $row->orderMaster->payment_status == 'P') {{__('site.product_active')}}
                                                        @elseif(@$row->order_status == 'C' && $row->orderMaster->payment_status == 'P' && @$row->is_cancel_responce=='N'){{__('site.product_cancel')}}
                                                        @elseif(@$row->order_status == 'C' && $row->orderMaster->payment_status == 'P' && @$row->is_cancel_responce=='A'){{__('site.product_order_cancelation_accept')}}
                                                        @elseif($row->orderMaster->payment_status == 'P' && @$row->is_cancel_responce=='R'){{__('site.product_order_cancelation_reject')}}
                                                        @elseif(@$row->order_status == 'A' && @$row->cancel_request =='Y' && $row->orderMaster->payment_status == 'P') {{__('site.product_request')}}
                                                        @else
                                                        {{__('site.product_inprogress')}}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if(@$row->order_status == 'A' && @$row->cancel_request =='Y' && $row->orderMaster->payment_status == 'P' && toUTCTime(date('Y-m-d H:i:s'))< toUTCTime(date('Y-m-d H:i:s', strtotime($row->product->purchase_end_date."+".@$row->product->cancel_day." days"))))
                                                       <a href="{{route('product.order.cancel',['id'=> $row,'token'=>$row->orderMaster->token_no])}}" title="Cancel Request Approve" onclick="return confirm('Do you want to Cancel Request Accept?');"> <i class="fa fa-check-circle delet" aria-hidden="true"></i></a>
                                                       @endif
                                                       <a href="javascript:;" class="show_msg" title="Show Messages" data-cancelreason="{{@$row->cancellation_reason}}" data-rejectreason="{{@$row->cancel_reject_reason}}"> <i class="fa fa-eye delet" aria-hidden="true"></i></a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Row -->
        </div>
        <!-- container -->
    </div>
    <!-- content -->
    <!--<footer class="footer text-right">
            2015 © Moltran.
            </footer>-->
</div>
<div class="modal fade" id="showmessage">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Cancellation Messages</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <strong>Reason for cancellation:</strong>
                <p id="cancel_reason"></p>
                <div id="reject_reason_div">
                    <strong>Reason for rejection:</strong>
                    <p id="reject_reason"></p>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    var resizefunc = [];
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.show_msg').click(function(){
            $('#cancel_reason').text($(this).data('cancelreason'));
            $('#reject_reason').text($(this).data('rejectreason'));
            if($(this).data('rejectreason') == '') $('#reject_reason_div').hide();
            else $('#reject_reason_div').show();
            $('#showmessage').modal('show');
        });
        $('#datatable_length').hide();
        $('#datatable').dataTable({
            "ordering": false,
            "info":     false,
            "searching": false,
            "dom": '<"toolbar">frtip'
        });
        $(".check_check").click(function(){
            if($(this).is(":checked")) {
                $('.ch_1').show();
            } else {
                $('.ch_1').hide();
            }
        });
        //$("div.toolbar").html('<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 status">Status: <select name="" class="newdrop"><option value="1">Active</option><option value="1">Inactive</option></select></div><div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 clndr">From date : <input type="text" name="from" id="frm_date"> </div><div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 clndr"> To date : <input type="text" name="to" id="to_date"> </div><div class="col-md-4 col-lg-4 clndr">Keyword: <input type="text" name="abc"></div><div class="col-md-4 col-lg-4 col-sm-4 colxs-12 srch"><input type="submit" name="submit" value="Search"> </div>');
        // $('#datatable-keytable').DataTable( { keys: true } );
        //$('#datatable-responsive').DataTable();
        //$('#datatable-scroller').DataTable( { ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
        //var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } );
    });
    // TableManageButtons.init();
</script>
<script>
    $(function() {
        $("#datepicker").datepicker({dateFormat: "dd-mm-yy",
           defaultDate: new Date(),
           onClose: function( selectedDate ) {
           $( "#datepicker1").datepicker( "option", "minDate", selectedDate );
          }
        });

        $("#datepicker1").datepicker({dateFormat: "dd-mm-yy",
         defaultDate: new Date(),
              onClose: function( selectedDate ) {
              $( "#datepicker" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
<script>
    function submitForm(amount, name, token){
        if(confirm('The amount '+amount+' will be transferred on '+name+'\'s account')){

            $('#no').val(token);
            $('#myForm').submit();
        }
        else{
            return false;
        }
    }
    function prof_account_info(amount, name, token, prof_id, bankName ,accountNumber ,agencyNumber,agencyCheckNumber,bank_number){
        console.log(token)
            // $('#amount').val(amount);
            $('#payment_token_no').val(token);
            $('#professional_name').html("Professional Name : "+name);
            $('#payment_amount').html("Payment Amount : "+amount);
            $('#bankName').html("Bank Name : "+bankName);
            $('#accountNumber').html("Account Number : "+accountNumber);
            $('#agencyNumber').html("Agency Number : "+agencyNumber);
            $('#agencyCheckNumber').html("Agency Check Number : "+agencyCheckNumber);
            $('#bank_number').html("Bank Number : "+bank_number);
            $('#myModal').modal('show');
    }
</script>
<script>
    $(document).ready(function(){
        $("#updateeventsform2").validate({});
    });
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
