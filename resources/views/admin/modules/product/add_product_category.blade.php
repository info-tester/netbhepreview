@extends('admin.layouts.app')
@section('title', @$productCategory ? 'Netbhe | Edit Product Category':'Netbhe | Add Product Category')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Add Product Category</h4>
                    <div class="submit-login no_mmg pull-right">
                        <a href="{{ route('admin.product.category') }}" title="Back"><button type="button"
                                class="btn btn-default">Back</button></a>
                    </div>
                </div>
            </div>

            {{-- @include('admin.includes.error') --}}
            {{-- {{ dd(old()) }} --}}
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-rep-plugin">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 nhp">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <form id="myform" method="post" action="{{ @$productCategory? route('admin.product.category.edit',['id'=>@$productCategory->id]): route('admin.product.category.add')}}"
                                            enctype="multipart/form-data">
                                            @csrf

                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Product Category Name*</label>
                                                    <input type="text" name="product_category_name" class="form-control required"
                                                        id="cat_name" value="{{ old('product_category_name',@$productCategory->category_name) }}">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <!-- <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 image_section" id="img">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Image</label>
                                                    <input type="file" class="form-control required" id="file" placeholder="Image" name="image" accept="image/*">
                                                    <span>
                                                    (Image should be 276x183)
                                                    </span><div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div> -->

                                            <!-- <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Commission Percentage</label>
                                                    <input type="text" name="commission" class="form-control required"
                                                        id="commission" value="{{ old('commission',@$productCategory->commission) }}">
                                                </div>
                                            </div> -->

                                            <div class="clearfix"></div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="meta_tit">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Meta Title*</label>
                                                    <input type="text" class="form-control required" id="meta_title"
                                                        placeholder="" name="meta_title"
                                                        value="{{ old('meta_title',@$productCategory->meta_title) }}">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <!--all_time_sho-->
                                            <div class="all_time_sho">
                                                <div class="col-md-6 col-sm-12 col-xs-12 col-lg-6" id="meta_desc">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Meta Description*</label>
                                                        <textarea placeholder="" id="meta_description"
                                                            name="meta_description" rows="3"
                                                            class="form-control message required"
                                                            >{{old('meta_description', @$productCategory->meta_description)}}</textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            {{-- <div class="clearfix"></div>

                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 image_section" id="img">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Image</label>
                                                    <input type="file" class="form-control required" id="file"
                                                        placeholder="Image" name="image" accept="image/*">
                                                    <span>
                                                        (Image should be 276x183)
                                                    </span>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div> --}}

                                            <div class="clearfix"></div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="add_btnm submit-login">
                                                    <input value="{{@$productCategory?'Update':'Add'}}" type="submit" class="btn btn-default">
                                                </div>
                                            </div>
                                            <!--all_time_sho-->
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Row -->
            @if(@$productCategory)
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Subcategory List</h4>

                    <div class="submit-login no_mmg pull-right">
                    {{-- @$subcategory? route('admin.product.subcategory.store',['pid'=>@$productCategory->id, 'id'=>@$subcategory->id]): route('admin.product.subcategory.store', ['pid'=>@$productCategory->id]) --}}
                        <a href="{{ route('admin.product.subcategory.store.view', ['pid'=>@$productCategory->id]) }}" title="Create"><button type="button"
                                class="btn btn-default">Add</button></a>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-rep-plugin">
                            <div class="row">

                        <div class="col-md-12 dess5">
                            <i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span
                                    class="cncl_oopo">Edit</span></i>
                            <i class="fa fa-trash-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
                            <i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active</span></i>
                            <i class="fa fa-times cncl" aria-hidden="true" style="border: none;"> <span
                                    class="cncl_oopo">Inactive</span></i>
                            {{-- <i class="fa fa-copy cncl" aria-hidden="true" style="border: none;"> <span
                                    class="cncl_oopo">Copy</span></i> --}}
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="table-responsive" data-pattern="priority-columns">
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <!-- <th>Image</th> -->
                                            <th>Name of Subcategory</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(@$subcategories)
                                            @foreach(@$subcategories as $row)
                                            <tr>
                                                <td>{{ @$row->name}}</td>
                                                <td>
                                                    @if(@$row->status == 'I')
                                                    <label class="label label-danger">Inactive</label>
                                                    @elseif(@$row->status == 'A')
                                                    <label class="label label-success">Active</label>
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="{{ route('admin.product.subcategory.store.view',[@$productCategory->id, @$row->id]) }}" title="Edit">
                                                        <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
                                                    <a href="{{route('admin.product.subcategory.delete',[@$row->id])}}"
                                                        onclick="return confirm('Are you want to delete this Subcategory ?');"
                                                        title="Delete"> <i class="fa fa-trash-o delet"
                                                            aria-hidden="true"></i></a>
                                                    @if(@$row->status == 'I')
                                                    <a href="{{route('admin.product.subcategory.status.change',[@$row->id])}}"
                                                        title="Active"
                                                        onclick="return confirm('Do you want to active this Product Subcategory ?');"> <i
                                                            class="fa fa-check delet" aria-hidden="true"></i></a>
                                                    @elseif(@$row->status == 'A')
                                                    <a href="{{route('admin.product.subcategory.status.change',[@$row->id])}}"
                                                        title="Inactive"
                                                        onclick="return confirm('Do you want to inactive this Product Subcategory ?');">
                                                        <i class="fa fa-times delet" aria-hidden="true"></i></a>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif

        </div>
        <!-- container -->
    </div>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
<script>
    $(document).ready(function(){
        $("#myform").validate({
            rules: {
				// commission : 'number',
			},
            messages :
            {
                product_category_name   : "Please enter Product category name",
                // commission              : {
				// 	required 		: "Please enter commission percentage",
				// 	number 			: "Please enter number."
				// },
                meta_title              : "Please enter meta title",
                meta_description        : "Please enter meta description",
                // description         : "Please enter description",
                // image               : "Please enter category image",
            }
        });
    });

    // $('#parent_id').change(function(){
    //     if($('#parent_id').val()!=""){
    //         $('#img').hide();
    //         $('#meta_desc').hide();
    //         $('#meta_tit').hide();
    //     }
    //     else{
    //         $('#img').show();
    //         $('#meta_desc').show();
    //         $('#meta_tit').show();
    //     }

    // });
</script>
@endsection
