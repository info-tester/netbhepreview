@extends('admin.layouts.app')
@section('title', 'Netbhe | Product Category')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Manage Product Category</h4>
                    <div class="submit-login no_mmg pull-right">
                        <a href="{{ route('admin.product.category.add.view') }}" title="Create"><button type="button"
                                class="btn btn-default">Add</button></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="panel panel-default">
                                    <div class="panel-body table-rep-plugin">
                                        <form method="post" action="{{route('admin.product.category.search')}}">
                                            @csrf
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Keyword</label>
                                                    <input class="form-control" name="keyword" placeholder="Keyword" value="{{ @$key['keyword'] }}"
                                                        type="text">
                                                </div>
                                            </div>
                                            <div id="profDiv">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Product Category</label>
                                                        <select class="form-control newdrop " name="category" id="category">
                                                            <option value="0">Select category</option>
                                                            @foreach(@$category_list as $row)
                                                            <option value="{{$row->id}}" @if(@$key['category']==$row->id) selected @endif>{{$row->category_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div id="profDiv">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Product Subcategory</label>
                                                        <select class="form-control newdrop " name="sub_cat" id="sub_cat">
                                                            <option value="">Select Product Subcategory</option>
                                                            @foreach(@$sub_cat_list as $row)
                                                            <option value="{{$row->id}}" @if(@$key['sub_cat']==$row->id) selected @endif>{{$row->name}}</option>
                                                            @endforeach
                                                            
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
 -->
                                            <div class="clearfix"></div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="submit-login add_btnm pull-right" style="margin-top: 5px;">
                                                    <input value="Search" class="btn btn-default" type="submit">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-rep-plugin">
                            <div class="row">

                        <div class="col-md-12 dess5">
                            <i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span
                                    class="cncl_oopo">Edit</span></i>
                            <i class="fa fa-trash-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
                            
                            <i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active</span></i>
                            
                            

                            <i class="fa fa-times cncl" aria-hidden="true" style="border: none;"><span class="cncl_oopo">Inactive</span></i>

                            {{-- <i class="fa fa-copy cncl" aria-hidden="true" style="border: none;"> <span
                                    class="cncl_oopo">Copy</span></i> --}}
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="table-responsive" data-pattern="priority-columns">
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <!-- <th>Image</th> -->
                                            <th>Name of Category</th>
                                            <th>Subcategory</th>
                                            {{-- <th>Created By</th> --}}
                                            <!-- <th>Professional Commission Percentage</th> -->
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>   
                                    <tbody>
                                    @foreach(@$productCategoryList as $row)
                                            @if(count(@$row->getSubCategories)>0)
                                                @foreach(@$row->getSubCategories as $k=>$sub_cat)
                                            
                                                    @if($k == 0)
                                                        @if(@$key['category'] == 0)
                                                        <tr>
                                                                <td><b>{{ @$row->category_name}}</b></td>
                                                                <td></td>
                                                                <!-- <td><b>{{ @$row->commission}}%</b></td> -->
                                                                <td> 
                                                                    @if(@$row->status == 'I')
                                                                    <label class="label label-danger">Inactive</label>
                                                                    @elseif(@$row->status == 'A')
                                                                    <label class="label label-success">Active</label>
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                <a href="{{ route('admin.product.category.edit.view',[@$row->id]) }}" title="Edit">
                                                                    <i class="fa fa-pencil-square-o delet" aria-hidden="true">
                                                                        
                                                                    </i>
                                                                </a>
                                                                <a href="{{route('admin.product.category.delete',[@$row->id])}}"
                                                                    onclick="return confirm('Are you want to delete this Product ?');"
                                                                    title="Delete"> <i class="fa fa-trash-o delet"
                                                                        aria-hidden="true"></i></a>
                                                                @if(@$row->status == 'I')
                                                                <a href="{{route('admin.product.category.status.change',[@$row->id])}}"
                                                                    title="Active"
                                                                    onclick="return confirm('Do you want to active this Product Category ?');"> <i
                                                                        class="fa fa-check delet" aria-hidden="true"></i></a>
                                                                @elseif(@$row->status == 'A')
                                                                <a href="{{route('admin.product.category.status.change',[@$row->id])}}"
                                                                    title="Inactive"
                                                                    onclick="return confirm('Do you want to inactive this Product Category ?');">
                                                                    <i class="fa fa-times delet" aria-hidden="true"></i></a>
                                                                @endif
                                                                <!-- <a href="{{route('admin.product.category.view',[@$row->id])}}"
                                                                    title="View">
                                                                    <i class="fa fa-eye delet" aria-hidden="true"></i></a> -->
                                                            </td>
                                                            </tr> 
                                                        @endif
                                                    @endif
                                               
                                                    <tr>
                                                        
                                                        <td>{{ @$row->category_name}}</td>
                                                    
                                                        <td>
                                                            {{ @$sub_cat->name }}
                                                        </td>
                                                       
                                                        <!-- <td>{{ @$row->commission}}%</td> -->

                                                        <td>
                                                            @if(@$row->status == 'I')
                                                            <label class="label label-danger">Inactive</label>
                                                            @elseif(@$row->status == 'A')
                                                            <label class="label label-success">Active</label>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('admin.product.subcategory.store.view',[@$row->id, @$sub_cat->id]) }}" title="Edit">
                                                                <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i>
                                                            </a>
                                                            <a href="{{route('admin.product.subcategory.delete',[@$sub_cat->id])}}"
                                                                onclick="return confirm('Are you want to delete this Product Subcategory?');"
                                                                title="Delete"> <i class="fa fa-trash-o delet"
                                                                    aria-hidden="true"></i></a>
                                                            @if(@$row->status == 'I')
                                                            <a href="{{route('admin.product.category.status.change',[@$sub_cat->id])}}"
                                                                title="Active"
                                                                onclick="return confirm('Do you want to active this Product Category ?');"> <i
                                                                    class="fa fa-check delet" aria-hidden="true"></i></a>
                                                            @elseif(@$row->status == 'A')
                                                            <a href="{{route('admin.product.category.status.change',[@$sub_cat->id])}}"
                                                                title="Inactive"
                                                                onclick="return confirm('Do you want to inactive this Product Category ?');">
                                                                <i class="fa fa-times delet" aria-hidden="true"></i></a>
                                                            @endif
                                                            <!-- <a href="{{route('admin.product.category.view',[@$row->id])}}"
                                                                title="View">
                                                                <i class="fa fa-eye delet" aria-hidden="true"></i></a> -->
                                                        </td>
                                                    </tr>
                            @endforeach
                                            @else
                                            <tr>
                                                
                                                <td><b>{{ @$row->category_name}}</b></td>
                                                <td></td>
                                                <!-- <td><b>{{ @$row->commission }}%</b></td> -->
                                                <td> 
                                                    @if(@$row->status == 'I')
                                                        <label class="label label-danger">Inactive</label>
                                                    @elseif(@$row->status == 'A')
                                                        <label class="label label-success">Active</label>
                                                    @endif
                                                </td>
                                                <td>
                                                            <a href="{{ route('admin.product.category.edit.view',[@$row->id]) }}" title="Edit">
                                                                <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
                                                            <a href="{{route('admin.product.category.delete',[@$row->id])}}"
                                                                onclick="return confirm('Are you want to delete this Product ?');"
                                                                title="Delete"> <i class="fa fa-trash-o delet"
                                                                    aria-hidden="true"></i></a>
                                                            @if(@$row->status == 'I')
                                                            <a href="{{route('admin.product.category.status.change',[@$row->id])}}"
                                                                title="Active"
                                                                onclick="return confirm('Do you want to active this Product Category ?');"> <i
                                                                    class="fa fa-check delet" aria-hidden="true"></i></a>
                                                            @elseif(@$row->status == 'A')
                                                            <a href="{{route('admin.product.category.status.change',[@$row->id])}}"
                                                                title="Inactive"
                                                                onclick="return confirm('Do you want to inactive this Product Category ?');">
                                                                <i class="fa fa-times delet" aria-hidden="true"></i></a>
                                                            @endif
                                                            <!-- <a href="{{route('admin.product.category.view',[@$row->id])}}"
                                                                title="View">
                                                                <i class="fa fa-eye delet" aria-hidden="true"></i></a> -->
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>

@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
