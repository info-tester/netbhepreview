@extends('admin.layouts.app')
@section('title', 'Netbhe | Product List')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Curriculum List</h4>
                    <div class="submit-login no_mmg pull-right">
                        <a href="{{ route('admin.product.list')}}" title="Back"><button type="button"
                                class="btn btn-default">Back</button></a>
                    </div>
                    <div class="submit-login no_mmg pull-right">
                        {{-- <a href="{{ route('admin.product.category.add.view') }}" title="Create"><button type="button"
                                class="btn btn-default">Add</button></a> --}}
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-rep-plugin">
                            <div class="row">
                                <!-- <div class="panel panel-default">
                                    <div class="panel-body table-rep-plugin">
                                       
                                    </div>
                                </div> -->

                                <div class="col-md-12 dess5">
                                    <span><b>Professional Name : {{ @$professional->nick_name ? @$professional->nick_name : @$professional->name}}</b></span>

                                    &nbsp;
                                    >
                                    &nbsp;

                                    <span><b><a href="{{route('admin.product.list')}}">Course :{{@$product_detail->title}}</a></b></span>
                                    
                                    

                                    
                                   {{-- <i class="fa fa fa-eye cncl" aria-hidden="true"> <span class="cncl_oopo">View</span></i>
                                   <i class="fa fa-times-circle cncl" aria-hidden="true"> <span class="cncl_oopo">Reject</span></i>
                                   <i class="fa fa-check-circle cncl" aria-hidden="true"> <span class="cncl_oopo">Approve</span></i>
                                   <i class="fa fa-square cncl" aria-hidden="true"> <span class="cncl_oopo">Show in Home Page</span></i>
                                   <i class="fa fa-square-o cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">Not Show in Home Page</span></i> --}}

                                {{-- <i class="fa fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
                                <i class="fa fa-trash-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
                                <i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active</span></i>
                                <i class="fa fa-times cncl" aria-hidden="true"> <span class="cncl_oopo">Inactive</span></i>
                                <i class="fa fa-envelope-square cncl" aria-hidden="true"> <span class="cncl_oopo">Email Verify Link</span></i> --}}



                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <table id="datatable" class="table table-striped table-bordered">
                                            <thead>


                                                <tr>
                                                    <th>Name of Curriculum</th>
                                                    <th>Lesson</th>
                                                    <!-- <th>Created By</th> -->
                                                    <th>User Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                @foreach($product as $row)

                                                @if(count(@$row->getLessons)>0)
                                                    @foreach(@$row->getLessons as $k=>$lesson)
                                                    
                                                    @if($k == 0)
                                                        <tr>
                                                            <td><b>{{ @$row->chapter_title}}</b></td>
                                                            <td></td>
                                                            <!-- <td>{{ @$professional->nick_name ? @$professional->nick_name : @$professional->name}}</td> -->

                                                            <td>
                                                                @if(@$row->status == 'D')
                                                                <label class="label label-danger">Draft</label>
                                                                @elseif(@$row->status == 'P')
                                                                <label class="label label-success">Published</label>
                                                                @endif
                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                    @endif
                                                        <tr>
                                                            <td>{{ @$row->chapter_title}}</td>
                                                            <td>{{$lesson->lesson_title}}</td>
                                                            <!-- <td>{{ @$professional->nick_name ? @$professional->nick_name : @$professional->name}}</td> -->

                                                            <td>
                                                                @if(@$lesson->status == 'D')
                                                                <label class="label label-danger">Draft</label>
                                                                @elseif(@$lesson->status == 'A')
                                                                <label class="label label-success">Published</label>
                                                                @endif
                                                            </td>
                                                            
                                                           
                                                            <td>

                                                                <a href="{{ route('admin.lesson.view',[@$lesson->id]) }}"
                                                                    title="lesson">
                                                                    <i class="fa fa-folder delet" aria-hidden="true"></i>
                                                                </a>

                                                                <!-- <a href="{{ route('admin.product.view',[@$row->id]) }}"
                                                                    title="View this product">
                                                                    <i class="fa fa-eye delet" aria-hidden="true"></i>
                                                                </a>
                                                                <a href="{{route('admin.product.delete',[@$row->id])}}"
                                                                    onclick="return confirm('Are you want to delete this Product ?');"
                                                                    title="Delete">
                                                                    <i class="fa fa-trash-o delet" aria-hidden="true"></i>
                                                                </a> -->
                                                                
                                                               
                                                            </td>
                                                        </tr>
                                                    
                                                    @endforeach
                                                @else
                                                <tr>
                                                    <td>{{ @$row->chapter_title}}</td>
                                                    <td>Lesson</td>
                                                    <!-- <td>{{ @$professional->nick_name ? @$professional->nick_name : @$professional->name}}</td> -->

                                                    <td>
                                                        @if(@$row->status == 'D')
                                                        <label class="label label-danger">Draft</label>
                                                        @elseif(@$row->status == 'P')
                                                        <label class="label label-success">Published</label>
                                                        @endif
                                                    </td>
                                                    
                                                   
                                                    <td>

                                                        <a href="{{ route('admin.product.lesson.view',[@$row->id]) }}"
                                                            title="lesson">
                                                            <i class="fa fa-folder delet" aria-hidden="true"></i>
                                                        </a>

                                                        <!-- <a href="{{ route('admin.product.view',[@$row->id]) }}"
                                                            title="View this product">
                                                            <i class="fa fa-eye delet" aria-hidden="true"></i>
                                                        </a>
                                                        <a href="{{route('admin.product.delete',[@$row->id])}}"
                                                            onclick="return confirm('Are you want to delete this Product ?');"
                                                            title="Delete">
                                                            <i class="fa fa-trash-o delet" aria-hidden="true"></i>
                                                        </a> -->
                                                        
                                                       
                                                    </td>
                                                </tr>
                                                @endif
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
