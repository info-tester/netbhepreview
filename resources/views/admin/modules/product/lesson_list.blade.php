@extends('admin.layouts.app')
@section('title', 'Netbhe | Product List')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Lesson List</h4>
                    
                    <div class="submit-login no_mmg pull-right">
                        {{-- <a href="{{ route('admin.product.category.add.view') }}" title="Create"><button type="button"
                                class="btn btn-default">Add</button></a> --}}
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-rep-plugin">
                            <div class="row">
                                <!-- <div class="panel panel-default">
                                    <div class="panel-body table-rep-plugin">
                                       
                                    </div>
                                </div> -->

                                <div class="col-md-12 dess5">
                                   {{-- <i class="fa fa fa-eye cncl" aria-hidden="true"> <span class="cncl_oopo">View</span></i>
                                   <i class="fa fa-times-circle cncl" aria-hidden="true"> <span class="cncl_oopo">Reject</span></i>
                                   <i class="fa fa-check-circle cncl" aria-hidden="true"> <span class="cncl_oopo">Approve</span></i>
                                   <i class="fa fa-square cncl" aria-hidden="true"> <span class="cncl_oopo">Show in Home Page</span></i>
                                   <i class="fa fa-square-o cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">Not Show in Home Page</span></i> --}}

                                {{-- <i class="fa fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
                                <i class="fa fa-trash-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
                                <i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active</span></i>
                                <i class="fa fa-times cncl" aria-hidden="true"> <span class="cncl_oopo">Inactive</span></i>
                                <i class="fa fa-envelope-square cncl" aria-hidden="true"> <span class="cncl_oopo">Email Verify Link</span></i> --}}



                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <table id="datatable" class="table table-striped table-bordered">
                                            <thead>


                                                <tr>
                                                    <th>Name of Lesson</th>
                                                    <th>Created By</th>
                                                    <th>Lesson type</th>
                                                    <!-- <th>User Status</th> -->
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                @foreach($product as $row)
                                                <tr>
                                                    <td>{{ @$row->lesson_title}}</td>
                                                    <td>{{ @$professional->nick_name ? @$professional->nick_name : @$professional->name}}</td>
                                                    
                                                    <td>
                                                        @if(@$row->lesson_type == 'V')
                                                        Video
                                                        @elseif(@$row->lesson_type == 'Q')
                                                        Quiz
                                                        @elseif(@$row->lesson_type == 'M')
                                                        Multimedia
                                                        @elseif(@$row->lesson_type == 'T')
                                                        Text
                                                        @elseif(@$row->lesson_type == 'A')
                                                        Audio
                                                        @elseif(@$row->lesson_type == 'D')
                                                        Download
                                                        @elseif(@$row->lesson_type == 'P')
                                                        Presentation
                                                        @endif
                                                        
                                                    </td>

                                                    <!-- <td>
                                                        @if(@$row->status == 'D')
                                                        <label class="label label-danger">Draft</label>
                                                        @elseif(@$row->status == 'P')
                                                        <label class="label label-success">Published</label>
                                                        @endif
                                                    </td> -->
                                                    
                                                   
                                                    <td>
                                                         <a href="{{ route('admin.lesson.view',[@$row->id]) }}"
                                                            title="View this lesson">
                                                            <i class="fa fa-eye delet" aria-hidden="true"></i>
                                                        </a>

                                                        

                                                        <!-- <a href="{{ route('admin.product.view',[@$row->id]) }}"
                                                            title="View this product">
                                                            <i class="fa fa-eye delet" aria-hidden="true"></i>
                                                        </a>
                                                        <a href="{{route('admin.product.delete',[@$row->id])}}"
                                                            onclick="return confirm('Are you want to delete this Product ?');"
                                                            title="Delete">
                                                            <i class="fa fa-trash-o delet" aria-hidden="true"></i>
                                                        </a> -->
                                                        
                                                       
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
