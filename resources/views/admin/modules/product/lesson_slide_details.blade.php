@extends('admin.layouts.app')
@section('title', 'Netbhe| Admin | Lesson File Details')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">
                        Lesson Slide Details
                    </h4>
                    <div class="submit-login no_mmg pull-right">
                        <!-- <a href="{{ route('admin.product.list')}}" title="Back"><button type="button"
                                class="btn btn-default">Back</button></a> -->
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12">

                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <h4 class="pull-left page-title">Lesson Slide Info</h4>
                                    <div class="order-detail">
                                        <!--order-detail start-->
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="order-id">
                                                <div class="clearfix"></div>
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Slide Image :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong><img src="{{ URL::to('storage/app/public/lessons/type_presentation/'.@$product->filename)}}"></strong>
                                                </div>
                                                @if(@$product->audio != null)
                                                <div class="clearfix"></div>
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Uploaded Audio :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                   
                                                        <audio src="{{ URL::to('storage/app/public/lessons/type_presentation/audio/'.@$product->audio)}}" controls>
                                                            
                                                        </audio>
                                                </div>
                                                @endif
                                                @if(@$product->description != null)
                                                <div class="clearfix"></div>
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Description :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong>{!! @$product->description !!}</strong>
                                                </div>
                                                @endif
                                                
                                                

                                            </div>
                                        </div>
                                    </div>
                                   
                                    <!--order-detail end-->
                                </div>
                                <div class="col-md-12">

                                </div>
                                <div class="clearfix"></div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Row -->
    </div>
    <!-- container -->
</div>
<!-- content -->

</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
<!-- Right Sidebar -->
<!-- /Right-bar -->
</div> 
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
