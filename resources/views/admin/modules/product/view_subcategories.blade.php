@extends('admin.layouts.app')
@section('title', @$productCategory ? 'Netbhe | Edit Product Category':'Netbhe | Add Product Category')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            

            {{-- @include('admin.includes.error') --}}
            {{-- {{ dd(old()) }} --}}
            
            <!-- End Row -->

            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Subcategory List</h4>
                    <div class="submit-login no_mmg pull-right">
                    {{-- @$subcategory? route('admin.product.subcategory.store',['pid'=>@$productCategory->id, 'id'=>@$subcategory->id]): route('admin.product.subcategory.store', ['pid'=>@$productCategory->id]) --}}
                        <!--  -->
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-rep-plugin">
                            <div class="row">

                        <div class="col-md-12 dess5">
                            <i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span
                                    class="cncl_oopo">Edit</span></i>
                            <i class="fa fa-trash-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
                            <i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active</span></i>
                            <i class="fa fa-times cncl" aria-hidden="true" style="border: none;"> <span
                                    class="cncl_oopo">Inactive</span></i>
                            {{-- <i class="fa fa-copy cncl" aria-hidden="true" style="border: none;"> <span
                                    class="cncl_oopo">Copy</span></i> --}}
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="table-responsive" data-pattern="priority-columns">
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <!-- <th>Image</th> -->
                                            <th>Name of Subcategory</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(@$product)
                                            @foreach(@$product as $row)
                                            <tr>
                                                <td>{{ @$row->name}}</td>
                                                <td>
                                                    @if(@$row->status == 'I')
                                                    <label class="label label-danger">Inactive</label>
                                                    @elseif(@$row->status == 'A')
                                                    <label class="label label-success">Active</label>
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="{{ route('admin.product.subcategory.store.view',[@$category->id, @$row->id]) }}" title="Edit">
                                                        <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
                                                    <a href="{{route('admin.product.subcategory.delete',[@$row->id])}}"
                                                        onclick="return confirm('Are you want to delete this Subcategory ?');"
                                                        title="Delete"> <i class="fa fa-trash-o delet"
                                                            aria-hidden="true"></i></a>
                                                    @if(@$row->status == 'I')
                                                    <a href="{{route('admin.product.subcategory.status.change',[@$row->id])}}"
                                                        title="Active"
                                                        onclick="return confirm('Do you want to active this Product Subcategory ?');"> <i
                                                            class="fa fa-check delet" aria-hidden="true"></i></a>
                                                    @elseif(@$row->status == 'A')
                                                    <a href="{{route('admin.product.subcategory.status.change',[@$row->id])}}"
                                                        title="Inactive"
                                                        onclick="return confirm('Do you want to inactive this Product Subcategory ?');">
                                                        <i class="fa fa-times delet" aria-hidden="true"></i></a>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <!-- container -->
    </div>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
<script>
    $(document).ready(function(){
        $("#myform").validate({
            rules: {
				commission : 'number',
			},
            messages :
            {
                product_category_name   : "Please enter Product category name",
                commission              : {
					required 		: "Please enter commission percentage",
					number 			: "Please enter number."
				},
                meta_title              : "Please enter meta title",
                meta_description        : "Please enter meta description",
                // description         : "Please enter description",
                // image               : "Please enter category image",
            }
        });
    });

    // $('#parent_id').change(function(){
    //     if($('#parent_id').val()!=""){
    //         $('#img').hide();
    //         $('#meta_desc').hide();
    //         $('#meta_tit').hide();
    //     }
    //     else{
    //         $('#img').show();
    //         $('#meta_desc').show();
    //         $('#meta_tit').show();
    //     }

    // });
</script>
@endsection
