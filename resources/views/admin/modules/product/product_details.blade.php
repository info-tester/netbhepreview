@extends('admin.layouts.app')
@section('title', 'Netbhe| Admin | User Details')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">
                        Product Details
                    </h4>
                    <div class="submit-login no_mmg pull-right">
                        <a href="{{ route('admin.product.list')}}" title="Back"><button type="button"
                                class="btn btn-default">Back</button></a>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12">

                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <h4 class="pull-left page-title">Product Info</h4>
                                    <div class="order-detail">
                                        <!--order-detail start-->
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="order-id">
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Product Title :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <span>{{ $product->title }}</span>
                                                </div>

                                                <div class="clearfix"></div>
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Professional Name :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong>{{ @$product->professional->nick_name ? @$product->professional->nick_name :@$product->professional->name }}</strong>
                                                </div>


                                                <div class="clearfix"></div>
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Category :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong>{{ $product->category->category_name }}</strong>
                                                </div>

                                                <div class="clearfix"></div>
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Subcategory :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong>{{ $product->subCategory->name }}</strong>
                                                </div>

                                                <div class="clearfix"></div>
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>No of Installments for the section :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong>{{ $product->no_of_installment }}</strong>
                                                </div>

                                                <div class="clearfix"></div>
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Installment given :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong>@if(@$product->installment_charge == 'N')
                                                        No
                                                        @else
                                                        Yes
                                                        @endif
                                                    </strong>
                                                </div>




                                                <div class="clearfix"></div>
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Price :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong>{{ $product->price}}</strong>
                                                </div>

                                                <div class="clearfix"></div>
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Product Type :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong>{{ $product->product_type=='N'?'Normal':'Customizable'}}</strong>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Product Show In Home :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong>{{ $product->show_in_home_page =='Y'? 'Yes':'No'}}</strong>
                                                </div>
                                                <div class="clearfix"></div>

                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Product is a part of affiliation program :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong>{{ $product->affiliate_program =='Y'? 'Yes':'No'}}</strong>
                                                </div>
                                                <div class="clearfix"></div>

                                                @if($product->affiliate_program == 'Y')
                                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                        <p>Product affiliation commission :</p>
                                                    </div>
                                                    <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                        <strong>{{ $product->affiliate_percentage }}</strong>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="order-id">
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Profile Picture :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5">
                                                    <div style="height: 100px; width: 100px;">
                                                        <img src="{{ @$product->cover_image ? URL::to('storage/app/public/uploads/product_cover_images/'.@$product->cover_image) : URL::to('public/frontend/images/no_img.png') }}"
                                                            alt="" style="width: auto;height: 100%;">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="order-id">
                                                <div class="col-lg-2 col-md-2 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Product File :</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-sm-7 col-xs-7">
                                                {{-- @if(sizeof($product->productFile)>0)
                                                @foreach($product->productFile as $file)

                                                <p><strong>{{$file->caption}}</strong><a href="{{ URL::to('storage/app/public/uploads/product_files/'. $file->product_file) }}" target="_blank">(file view link)</a></p>
                                            @endforeach
                                            @else
                                            <p><strong>File not Uploaded</strong></p>
                                            @endif --}}
                                            </div>

                                        </div> -->
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="order-id">
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Product Description :</p>
                                                </div>
                                            </div>
                                            <div class="blog_area_nn01 col-12" style="text-align: left !important; box-shadow:none !important;">
                                                {!! $product->description !!}
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="order-id">
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>How does it work :</p>
                                                </div>
                                            </div>
                                            <div class="blog_area_nn01 col-12" style="text-align: left !important; box-shadow:none !important;">
                                                {!! $product->how_it_works !!}
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="order-id">
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>How can this help:</p>
                                                </div>
                                            </div>
                                            <div class="blog_area_nn01 col-12" style="text-align: left !important; box-shadow:none !important;">
                                                {!! $product->can_help !!}
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="order-id">
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>For whom :</p>
                                                </div>
                                            </div>
                                            <div class="blog_area_nn01 col-12" style="text-align: left !important; box-shadow:none !important;">
                                                {!! $product->for_whom !!}
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="order-id">
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Benefits :</p>
                                                </div>
                                            </div>
                                            <div class="blog_area_nn01 col-12" style="text-align: left !important; box-shadow:none !important;">
                                                {!! $product->benefits !!}
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="order-id">
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>When to do :</p>
                                                </div>
                                            </div>
                                            <div class="blog_area_nn01 col-12" style="text-align: left !important; box-shadow:none !important;">
                                                {!! $product->when_to_do !!}
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="order-id">
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Previous Preparation :</p>
                                                </div>
                                            </div>
                                            <div class="blog_area_nn01 col-12" style="text-align: left !important; box-shadow:none !important;">
                                                {!! $product->previous_preparation !!}
                                            </div>
                                        </div>
                                    </div>


                                    <!--order-detail end-->
                                </div>
                                <div class="col-md-12">

                                </div>
                                <div class="clearfix"></div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Row -->
    </div>
    <!-- container -->
</div>
<!-- content -->

</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
<!-- Right Sidebar -->
<!-- /Right-bar -->
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
