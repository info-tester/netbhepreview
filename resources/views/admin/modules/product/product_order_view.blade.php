@extends('admin.layouts.app')
@section('title', 'Netbhe.com| Admin | Booking Details')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">

    <div class="content">

        <div class="container">
            <div class="row">
                @include('admin.includes.error')
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Product Order details of: {{ @$orderProduct->userDetails->nick_name ? @$orderProduct->userDetails->nick_name : @$orderProduct->userDetails->name }}
                        <!-- with
                        {{ @$orderProduct->profDetails->nick_name ? @$orderProduct->profDetails->nick_name : @$orderProduct->profDetails->name }} --> </h4>
                    <div class="submit-login no_mmg pull-right">
                        <a href="{{ route('admin.order.product') }}" title="Back"><button type="button"
                                class="btn btn-default">Back</button></a>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="right_dash ml-auto">
                        <div class="information_box" style="    border: 1px solid #8f8f8f; padding: 5px 12px;">
                            <h4><span> <strong>Product Order Information </strong></span></h4>
                            <div class="information_area">
                                <div class="row">
                                    <div class="col-lg-6 col-md-12">
                                        <div class="form-group">
                                            <label class="type_label1 ">User Name: </label>
                                            <label class="type_label2">{{ @$orderProduct->userDetails->nick_name ? @$orderProduct->userDetails->nick_name : @$orderProduct->userDetails->name }}</label>
                                        </div>
                                    </div>
                                    <!-- <div class="col-lg-6 col-md-12">
                                        <div class="form-group">
                                            <label class="type_label1 ">Professional Name: </label>
                                            <label class="type_label2">{{ @$orderProduct->profDetails->nick_name ? @$orderProduct->profDetails->nick_name : @$orderProduct->profDetails->name }}</label>
                                        </div>
                                    </div> -->
                                    <!-- <div class="col-lg-6 col-md-12">
                                        <div class="form-group">
                                            <label class="type_label1">Product Name: </label>
                                            <label
                                                class="type_label2"></label>
                                        </div>
                                    </div> -->
                                    <!-- <div class="col-lg-6 col-md-12">
                                        <div class="form-group">
                                            <label class="type_label1">Category: </label>
                                            <label
                                                class="type_label2">{{@$orderProduct->productCategoryDetails->category_name}}</label>
                                        </div>
                                    </div> -->
                                    {{-- <div class="col-lg-6 col-md-12">
                                        <div class="form-group">
                                            <label class="type_label1">Sub Category: </label>
                                            <label
                                                class="type_label2">{{ @$order->childCatDetails ? @$order->childCatDetails->name: "N.A"}}</label>
                                        </div>
                                    </div> --}}
                                    <div class="col-lg-6 col-md-12">
                                        <div class="form-group">
                                            <label class="type_label1">Token Number: </label>
                                            <label class="type_label2">{{@$orderProduct->token_no}}</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12">
                                        <div class="form-group">
                                            <label class="type_label1">Date of Sold: </label>
                                            <label class="type_label2">{{date('d-M-Y',strtotime(@$orderProduct->order_date))}}</label>
                                        </div>
                                    </div>

                                    {{-- <div class="col-lg-6 col-md-12">
                                        <div class="form-group">
                                            <label class="type_label1">Start time: </label>
                                            <label
                                                class="type_label2">{{date('h:i A',strtotime(@$order->start_time))}}</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12">
                                        <div class="form-group">
                                            <label class="type_label1">End time: </label>
                                            <label
                                                class="type_label2">{{date('h:i A',strtotime(@$order->end_time))}}</label>
                                        </div>
                                    </div> --}}

                                    <!--<div class="col-lg-6 col-md-12">-->
                                    <!--    <div class="form-group">-->
                                    <!--        <label class="type_label1">Product Price: </label>-->
                                    <!--        <label class="type_label2"> ${{@$orderProduct->amount}}</label>-->
                                    <!--    </div>-->
                                    <!--</div>-->
                                    @if(@$orderProduct->extra_installment_charge > 1)
                                    <div class="col-lg-6 col-md-12">
                                        <div class="form-group">
                                            <label class="type_label1">Installment Charge: </label>
                                            <label class="type_label2"> ${{number_format((float)@$orderProduct->extra_installment_charge,2,'.','')}}</label>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="col-lg-6 col-md-12">
                                        <div class="form-group">
                                            <label class="type_label1">Total Amount: </label>
                                            <label class="type_label2"> ${{number_format((float)@$orderProduct->sub_total+@$orderProduct->extra_installment_charge, 2, '.', '')}}</label>
                                        </div>
                                    </div>

                                    <!--<div class="col-lg-6 col-md-12">-->
                                    <!--    <div class="form-group">-->
                                    <!--        <label class="type_label1">Commision: </label>-->
                                    <!--        <label class="type_label2">-->
                                    <!--            ${{@$orderProduct->PaymentDetails ? number_format((float)@$orderProduct->PaymentDetails->admin_amount+@$orderProduct->extra_installment_charge, 2, '.', '') : '0.00' }}</label>-->
                                    <!--    </div>-->
                                    <!--</div>-->

                                    <!--<div class="col-lg-6 col-md-12">-->
                                    <!--    <div class="form-group">-->
                                    <!--        <label class="type_label1">Professional earning: </label>-->
                                    <!--        <label class="type_label2">-->
                                    <!--            {{@$orderProduct->PaymentDetails->professional_amount}}-->
                                    <!--            ${{@$orderProduct->PaymentDetails ? @$orderProduct->PaymentDetails->professional_amount : '0.00' }}</label>-->

                                    <!--    </div>-->
                                    <!--</div>-->
                                    {{-- <div class="col-lg-6 col-md-12">
                                        <div class="form-group">
                                            <label class="type_label1">Order Status: </label>
                                            <label class="type_label2">@if(@$order->video_status == 'I') Order Initiated
                                                @elseif(@$order->video_status == 'C') Order Completed @endif</label>
                                        </div>
                                    </div> --}}

                                    <div class="col-lg-6 col-md-12">
                                        <div class="form-group">
                                            <label class="type_label1">Payment Status: </label>
                                            <label class="type_label2">@if(@$orderProduct->payment_status == 'I') Payment
                                                Initiated @elseif(@$orderProduct->payment_status == 'P') Paid
                                                @elseif(@$orderProductr->payment_status == 'F') Payment Failed
                                                @elseif(@$orderProduct->payment_status == 'PR') Payment Processing @endif
                                            </label>
                                            @if(@$orderProduct->payment_document && @$orderProduct->payment_status == 'PR')
                                            <a href="{{ URL::to('storage/app/public/Bank_Document/'. @$orderProduct->payment_document) }}"
                                                target="_blank">(document link)</a>
                                            <a href="{{route('admin.payment.approve.product',['slug'=>@$orderProduct->token_no]) }}"><i
                                                    class="fa fa-check-circle cncl" aria-hidden="true"></i></a>
                                            <a href="{{route('admin.payment.reject.product',['slug'=>@$orderProduct->token_no]) }}"><i
                                                    class="fa fa-times-circle" aria-hidden="true"></i></a>
                                            @elseif(@$orderProduct->payment_status == 'PR')
                                            (No Document Upload)
                                            <a href="{{route('admin.payment.reject.product',['slug'=>@$orderProduct->token_no]) }}"><i
                                                    class="fa fa-times-circle cncl" aria-hidden="true"></i></a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12">
                                        <div class="form-group">
                                            <label class="type_label1">Payment Type: </label>
                                            <label class="type_label2">
                                                @if(@$orderProduct->payment_type == 'C' && $orderProduct->no_of_installment==1)wirecard Payment
                                                @elseif(@$orderProduct->payment_type == 'C' && $orderProduct->no_of_installment>1) wirecard Payment - installment({{@$orderProduct->no_of_installment}})
                                                @elseif(@$orderProduct->payment_type == 'BA') Bank Account
                                                @elseif(@$orderProduct->payment_type == 'S') Stripe
                                                @elseif(@$orderProduct->payment_type == 'P') Paypal
                                                @elseif($orderProduct->sub_total==0)Free purchess
                                                @elseif(@$orderProduct->wallet==@$orderProduct->sub_total)Wallet
                                                @endif
                                            </label>
                                        </div>
                                    </div>
                                    {{-- <div class="col-lg-6 col-md-12">
                                        <div class="form-group">
                                            <label class="type_label1">Total Payable Amount: </label>
                                            <label class="type_label2"> ${{ @$order->sub_total }}</label>
                                        </div>
                                    </div> --}}
                                    @if(@$orderProduct->PaymentDetails->pg_payment_id)
                                    <div class="col-lg-6 col-md-12">
                                        <div class="form-group">
                                            <label class="type_label1">Payment TXN ID <i class="fa fa-info-circle"
                                                    title="This is Wirecard Payment ID" data-toggle="tooltip"></i> :
                                            </label>
                                            <label class="type_label2">{{ @$orderProduct->PaymentDetails->pg_payment_id}}</label>
                                        </div>
                                    </div>
                                    @endif

                                    <!--<div class="col-lg-6 col-md-12">-->
                                    <!--    <div class="form-group">-->
                                    <!--        <label class="type_label1">Transfer ID <i class="fa fa-info-circle"-->
                                    <!--                title="This is Wirecard Transfer ID" data-toggle="tooltip"></i>:-->
                                    <!--        </label>-->
                                    <!--        <label-->
                                    <!--            class="type_label2">{{ @$orderProduct->pg_payment_id}}</label>-->
                                    <!--    </div>-->
                                    <!--</div>-->
                            @if(count(@$orderProduct->orderDetails) >0)
                            {{-- <form method="post" action="{{route('admin.make.multiple.transfer')}}">
                                @csrf --}}
                                <div class="col-md-12 dess5">
                                <i class="fa fa-money cncl" aria-hidden="true"> <span class="cncl_oopo">Transfer Money Professional</span></i>
                                <i class="fa fa-credit-card cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">Transfer Money Affiliate</span></i>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" data-pattern="priority-columns">
                                    <table id="datatable" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    {{-- <th><i class="fa fa-check"></i></th> --}}
                                                    <th>Product Image</th>
                                                    <th>Product Title</th>
                                                    <th>Professional</th>
                                                    <th>Affiliate</th>
                                                    {{-- <th>Date of Sold</th> --}}
                                                    <th>Total </th>
                                                    <th>Professional Commission</th>
                                                    <th>Admin Commission</th>
                                                    <th>Affiliate Commission</th>
                                                    <th>Balance Transfer Professional</th>
                                                    <th>Balance Transfer Affiliate</th>
                                                    <th>Order Status</th>
                                                    <th>Action</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach(@$orderProduct->orderDetails as $row)
                                                <tr>
                                                    {{-- <td><input type="checkbox" class="checkhour" name="checked_val[]" value="{{$row->id}}"></td> --}}
                                                    <td><img src="{{URL::to('storage/app/public/uploads/product_cover_images/') . '/' . @$row->product->cover_image}}" height="50" width="60"></td>
                                                    <td><span class="dateee">
                                                    {{ @$row->product->title }}
                                            </td>
                                            <td>{{@$row->profDetails->nick_name ? @$row->profDetails->nick_name : @$row->profDetails->name}}
                                            </td>
                                            <td>@if(@$row->affiliateDetails){{@$row->affiliateDetails->nick_name ? @$row->affiliateDetails->nick_name : @$row->affiliateDetails->name}}
                                                @else
                                                No Affiliate
                                                @endif
                                            </td>
                                            {{-- <td>{{date('Y-m-d', strtotime(@$row->created_at))}}</td> --}}
                                            <td>
                                                {{ number_format((float)(@$row->professional_amount+@$row->admin_commission+@$row->affiliate_commission), 2, '.', '') }}
                                            </td>
                                            <td>{{@$row->professional_amount}}</td>
                                            <td>{{@$row->admin_commission}}</td>
                                            <td>{{@$row->affiliate_commission}}</td>
                                            <td>
                                                @if(@$row->paymentProductDetails->balance_status=="R")
                                                Under liquidation
                                                @elseif(@$row->paymentProductDetails->balance_status=="W")
                                                Already configured
                                                @endif
                                            </td>
                                            <td>
                                                @if(@$row->paymentProductDetails->affiliate_balance_status=="R"&& @$row->affiliateDetails)
                                                Under liquidation
                                                @elseif(@$row->paymentProductDetails->affiliate_balance_status=="W" && @$row->affiliateDetails)
                                                Already configured
                                                @endif
                                            </td>
                                            <td>
                                                @if(@$row->order_status == 'A' && @$row->cancel_request =='N' && @$orderProduct->payment_status == 'P')
                                                {{__('site.product_active')}}
                                                @elseif(@$row->order_status == 'C' && @$orderProduct->payment_status == 'P' && @$row->is_cancel_responce=='N')
                                                {{__('site.product_cancel')}}
                                                @elseif(@$row->order_status == 'C' && @$orderProduct->payment_status == 'P' && @$row->is_cancel_responce=='A')
                                                {{__('site.product_order_cancelation_accept')}}
                                                @elseif(@$orderProduct->payment_status == 'P' && @$row->is_cancel_responce=='R')
                                                {{__('site.product_order_cancelation_reject')}}
                                                @elseif(@$row->order_status == 'A' && @$row->cancel_request =='Y' && @$orderProduct->payment_status == 'P')
                                                {{__('site.product_request')}}
                                                @else
                                                {{__('site.product_inprogress')}}
                                                @endif
                                            </td>
                                            <td>

                                                @if(@$orderProduct->sub_total!=0 && @$orderProduct->payment_status == 'P' && @$row->order_status == 'A' && toUTCTime(date('Y-m-d H:i:s'))>toUTCTime(date('Y-m-d H:i:s', strtotime($row->product->purchase_end_date."+".@$row->product->cancel_day." days"))))

                                                    @if(@$row->paymentProductDetails->balance_status=="R" && @$row->paymentProductDetails->withdraw_by_wirecard=='Y' && @$row->profDetails->professional_access_token)
                                                    <a href="javascript:void(0);" onclick="submitForm({{@$row->paymentProductDetails->professional_amount}},'{{@$row->profDetails->nick_name ? @$row->profDetails->nick_name : @$row->profDetails->name}}', '{{@$orderProduct->token_no}}','{{@$row->paymentProductDetails->id}}');"  title="Transfer"><i style="cursor: pointer;" class="fa fa-money delet" aria-hidden="true"></i></a>
                                                    {{-- @endif --}}
                                                     @elseif(@$row->paymentProductDetails->balance_status == "R" && @$row->paymentProductDetails->withdraw_by_wirecard =='N' && @$row->profDetails->professional_access_token)
                                                    <a href="javascript:void(0);" onclick="prof_account_info({{@$row->paymentProductDetails->professional_amount}},'{{@$row->profDetails->nick_name ? @$row->profDetails->nick_name : @$row->profDetails->name}}', '{{@$orderProduct->token_no}}', '{{@$row->profDetails->bankAccount}}', '{{@$row->profDetails->bankAccount->bankName}}', '{{@$row->profDetails->bankAccount->accountNumber}}', '{{@$row->profDetails->bankAccount->agencyNumber}}', '{{@$row->profDetails->bankAccount->agencyCheckNumber}}', '{{@$row->profDetails->bankAccount->bank_number}}','{{@$row->paymentProductDetails->id}}');" title="Transfer">
                                                        <i style="cursor: pointer;" class="fa fa-money delet" aria-hidden="true"></i>
                                                    </a>
                                                    {{-- @endif --}}
                                                    @elseif(@$row->paymentProductDetails->balance_status == "R" && @$row->paymentProductDetails->withdraw_by_wirecard =='N' && @$row->profDetails->paypal_address)
                                                    <a href="javascript:void(0);" onclick="prof_account_info1({{@$row->paymentProductDetails->professional_amount}},'{{@$row->profDetails->nick_name ? @$row->profDetails->nick_name : @$row->profDetails->name}}', '{{@$orderProduct->token_no}}', '{{@$row->profDetails->paypal_address}}','{{@$row->paymentProductDetails->id}}');" title="Transfer">
                                                        <i style="cursor: pointer;" class="fa fa-money delet" aria-hidden="true"></i>
                                                    </a>
                                                    @endif




                                                    @if(@$row->paymentProductDetails->affiliate_balance_status=="R" && @$row->paymentProductDetails->affiliate_withdraw_by_wirecard=='Y' && @$row->affiliateDetails->professional_access_token)
                                                    <a href="javascript:void(0);" onclick="submitFormAffiliate({{@$row->paymentProductDetails->affiliate_commission}},'{{@$row->affiliateDetails->nick_name ? @$row->affiliateDetails->nick_name : @$row->affiliateDetails->name}}', '{{@$orderProduct->token_no}}','{{@$row->paymentProductDetails->id}}');"  title="Transfer"><i style="cursor: pointer;" class="fa fa-credit-card delet" aria-hidden="true"></i></a>
                                                    {{-- @endif --}}
                                                     @elseif(@$row->paymentProductDetails->affiliate_balance_status == "R" && @$row->paymentProductDetails->affiliate_withdraw_by_wirecard =='N' && @$row->affiliateDetails->professional_access_token)
                                                    <a href="javascript:void(0);" onclick="affiliate_account_info({{@$row->paymentProductDetails->affiliate_commission}},'{{@$row->affiliateDetails->nick_name ? @$row->affiliateDetails->nick_name : @$row->affiliateDetails->name}}', '{{@$orderProduct->token_no}}', '{{@$row->affiliateDetails->bankAccount}}', '{{@$row->affiliateDetails->bankAccount->bankName}}', '{{@$row->affiliateDetails->bankAccount->accountNumber}}', '{{@$row->affiliateDetails->bankAccount->agencyNumber}}', '{{@$row->affiliateDetails->bankAccount->agencyCheckNumber}}', '{{@$row->affiliateDetails->bankAccount->bank_number}}','{{@$row->paymentProductDetails->id}}');" title="Transfer">
                                                        <i style="cursor: pointer;" class="fa fa-credit-card delet" aria-hidden="true"></i>
                                                    </a>
                                                    {{-- @endif --}}
                                                    @elseif(@$row->paymentProductDetails->affiliate_balance_status == "R" && @$row->paymentProductDetails->affiliate_withdraw_by_wirecard =='N' && @$row->affiliateDetails->paypal_address)
                                                    <a href="javascript:void(0);" onclick="affiliate_account_info1({{@$row->paymentProductDetails->affiliate_commission}},'{{@$row->affiliateDetails->nick_name ? @$row->affiliateDetails->nick_name : @$row->affiliateDetails->name}}', '{{@$orderProduct->token_no}}', '{{@$row->affiliateDetails->paypal_address}}','{{@$row->paymentProductDetails->id}}');" title="Transfer">
                                                        <i style="cursor: pointer;" class="fa fa-credit-card delet" aria-hidden="true"></i>
                                                    </a>
                                                    @endif
                                                @else

                                                No action
                                                @endif
                                            </td>


                                        </tr>
                                        @endforeach
                                            </tbody>
                                            {{-- <input type="hidden" name="token" value="{{@$orderProduct->token_no}}">
                                            <button type="submit" class="btn btn-primary delete_btn" >Trsnsfer Money</button> --}}

                                        </table>

                                    </div>
                                </div>



                            {{-- </form> --}}
                                <form method="post" action="{{route('admin.make.transfer.product')}}" id="myForm" name="myForm">
                                    <input type="hidden" name="no" id="no" value="">
                                    <input type="hidden" name="id" id="id" value="">
                                    @csrf
                                </form>
                                <form method="post" action="{{route('admin.make.transfer.affiliate')}}" id="myFormAffiliate" name="myFormAffiliate">
                                    <input type="hidden" name="affiliate_no" id="affiliate_no" value="">
                                    <input type="hidden" name="affiliate_id" id="affiliate_id" value="">
                                    @csrf
                                </form>
                            @endif

                                    {{-- <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <label class="type_label1">Message: </label>
                                            <label class="type_label2">{!!strip_tags(@$order->msg, '<br>')!!}</label>
                                        </div>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Transfer the amount to the following bank account and then enter a note for the professional</h4>
                    {{-- Transfer the amount to the following bank account and then enter a note for the professional --}}
                </div>
                <div class="modal-body" style="padding: 1rem;">
                    <p id="professional_name"></p>
                    <p id="payment_amount"></p>
                    <p id="bankName"></p>
                    <p id="accountNumber"></p>
                    <p id="agencyNumber"></p>
                    <p id="agencyCheckNumber"></p>
                    <p id="bank_number"></p>
                    <form action="{{route('admin.make.transfer.bank.product')}}" method="post" id="updateeventsform2"
                        class="form-horizontal">
                        <input type="hidden" name="payment_token_no" id="payment_token_no" value="">
                        <input type="hidden" name="paymentId" id="paymentId" value="">
                        @csrf
                        <label for='areaforinfo'>Note </label>

                        <textarea class="form-control required" id='areaforinfo' rows="4"
                            style="min-width: 100%; border: 1px solid #CCC;" name="note"></textarea>
                        <div class="submit-login add_btnm " style="width:auto;margin-top:10px;">
                            <button type="submit" class="btn btn-default">Payment Confirmation </button>
                        </div>
                    </form>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Transfer the amount to the following paypal address and then enter a note for the professional</h4>
                    {{-- Transfer the amount to the following bank account and then enter a note for the professional --}}
                </div>
                <div class="modal-body" style="padding: 1rem;">
                    <p id="professional_name1"></p>
                    <p id="payment_amount1"></p>
                    <p id="paypal"></p>
                    <form action="{{route('admin.make.transfer.paypal')}}" method="post" id="updateeventsform3"
                        class="form-horizontal">
                        <input type="hidden" name="payment_token_no" id="payment_token_no1" value="">
                        <input type="hidden" name="paymentId" id="paymentId1" value="">
                        @csrf
                        <label for='areaforinfo'>Note </label>

                        <textarea class="form-control required" id='areaforinfo' rows="4"
                            style="min-width: 100%; border: 1px solid #CCC;" name="note"></textarea>
                        <div class="submit-login add_btnm " style="width:auto;margin-top:10px;">
                            <button type="submit" class="btn btn-default">Payment Confirmation </button>
                        </div>
                    </form>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    <div class="modal fade" id="affiliate_myModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Transfer the amount to the following bank account and then enter a note for the affiliate</h4>
                    {{-- Transfer the amount to the following bank account and then enter a note for the professional --}}
                </div>
                <div class="modal-body" style="padding: 1rem;">
                    <p id="affiliate_professional_name"></p>
                    <p id="affiliate_payment_amount"></p>
                    <p id="affiliate_bankName"></p>
                    <p id="affiliate_accountNumber"></p>
                    <p id="affiliate_agencyNumber"></p>
                    <p id="affiliate_agencyCheckNumber"></p>
                    <p id="affiliate_bank_number"></p>
                    <form action="{{route('admin.make.transfer.bank.product.affiliate')}}" method="post" id="affiliate_updateeventsform2"
                        class="form-horizontal">
                        <input type="hidden" name="affiliate_payment_token_no" id="affiliate_payment_token_no" value="">
                        <input type="hidden" name="affiliate_paymentId" id="affiliate_paymentId" value="">
                        @csrf
                        <label for='areaforinfo'>Note </label>

                        <textarea class="form-control required" id='affiliate_areaforinfo' rows="4"
                            style="min-width: 100%; border: 1px solid #CCC;" name="affiliate_note"></textarea>
                        <div class="submit-login add_btnm " style="width:auto;margin-top:10px;">
                            <button type="submit" class="btn btn-default">Payment Confirmation </button>
                        </div>
                    </form>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    <div class="modal fade" id="affiliate_myModal1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Transfer the amount to the following paypal address and then enter a note for the affiliate</h4>
                    {{-- Transfer the amount to the following bank account and then enter a note for the professional --}}
                </div>
                <div class="modal-body" style="padding: 1rem;">
                    <p id="affiliate_professional_name1"></p>
                    <p id="affiliate_payment_amount1"></p>
                    <p id="affiliate_paypal"></p>
                    <form action="{{route('admin.make.transfer.paypal.affiliate')}}" method="post" id="affiliate_updateeventsform3"
                        class="form-horizontal">
                        <input type="hidden" name="affiliate_payment_token_no" id="affiliate_payment_token_no1" value="">
                        <input type="hidden" name="affiliate_paymentId" id="affiliate_paymentId1" value="">
                        @csrf
                        <label for='areaforinfo'>Note </label>

                        <textarea class="form-control required" id='affiliate_areaforinfo' rows="4"
                            style="min-width: 100%; border: 1px solid #CCC;" name="affiliate_note"></textarea>
                        <div class="submit-login add_btnm " style="width:auto;margin-top:10px;">
                            <button type="submit" class="btn btn-default">Payment Confirmation </button>
                        </div>
                    </form>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    <style type="text/css">
        .converse_me p {
            float: right;
            padding: 6px 7px;
            color: #fff;
            max-width: 84%;
            margin: 0;
            background: #26c6da;
            overflow: hidden;
            border-radius: 3px;
        }

        .chattt {
            width: 100%;
            float: left;
            border: 1px solid #8f8f8f;
            padding: 12px;
            margin: 15px 0;
        }

        .converse_you {
            float: right;
            width: 100%;
            padding: 5px 0 4px;
            margin-top: 12px;
        }

        .converse_you img {
            float: left;
            width: 30px;
            height: 30px;
            margin-right: 5px;
            border-radius: 50%;
        }

        .converse_you p {
            padding: 6px 7px;
            color: #353535;
            text-align: left;
            float: left;
            background: #eaeaea;
            max-width: 84%;
            border-radius: 3px;
            margin: 0;
        }

        .converse_you span {
            font-weight: 400;
            color: #9b939e;
            text-align: left;
            width: 100%;
            padding: 4px 0 0 36px;
            float: left;
            font-size: 11px;
        }

        .converse_me {
            float: left;
            width: 100%;
            padding: 5px 0 4px;
        }

        .converse_me img {
            float: right;
            width: 30px;
            height: 30px;
            margin-left: 5px;
            border-radius: 50%;
        }

        .converse_me span {
            float: right;
            font-size: 11px;
            font-weight: 400;
            color: #9b939e;
            width: 100%;
            text-align: right;
            padding: 4px 36px 0 0;
        }
    </style>
    <script>
        function submitForm(amount, name, token,id){

            if(confirm('The amount '+amount+' will be transferred on '+name+'\'s account')){

                $('#id').val(id);
                $('#no').val(token);
                $('#myForm').submit();
            }
            else{
                return false;
            }
        }
        function submitFormAffiliate(amount, name, token,id){

            if(confirm('The amount '+amount+' will be transferred on '+name+'\'s account')){

                $('#affiliate_id').val(id);
                $('#affiliate_no').val(token);
                $('#myFormAffiliate').submit();
            }
            else{
                return false;
            }
        }
        function prof_account_info(amount, name, token, prof_id, bankName ,accountNumber ,agencyNumber,agencyCheckNumber,bank_number,id){
            console.log(token)
                // $('#amount').val(amount);
                $('#payment_token_no').val(token);
                $('#paymentId').val(id);
                $('#professional_name').html("Professional Name : "+name);
                $('#payment_amount').html("Payment Amount : "+amount);
                $('#bankName').html("Bank Name : "+bankName);
                $('#accountNumber').html("Account Number : "+accountNumber);
                $('#agencyNumber').html("Agency Number : "+agencyNumber);
                $('#agencyCheckNumber').html("Agency Check Number : "+agencyCheckNumber);
                $('#bank_number').html("Bank Number : "+bank_number);
                $('#myModal').modal('show');
        }
        function prof_account_info1(amount, name, token, paypal,id){
            console.log(token)
                // $('#amount').val(amount);
                $('#payment_token_no1').val(token);
                $('#paymentId1').val(id);
                $('#professional_name1').html("Professional Name : "+name);
                $('#payment_amount1').html("Payment Amount : "+amount);
                $('#paypal').html("Paypal Address : "+paypal);
                $('#myModal1').modal('show');
        }
        function affiliate_account_info(amount, name, token, prof_id, bankName ,accountNumber ,agencyNumber,agencyCheckNumber,bank_number,id){
            console.log(token)
                // $('#amount').val(amount);
                $('#affiliate_payment_token_no').val(token);
                $('#affiliate_paymentId').val(id);
                $('#affiliate_professional_name').html("Affiliate Name : "+name);
                $('#affiliate_payment_amount').html("Payment Amount : "+amount);
                $('#affiliate_bankName').html("Bank Name : "+bankName);
                $('#affiliate_accountNumber').html("Account Number : "+accountNumber);
                $('#affiliate_agencyNumber').html("Agency Number : "+agencyNumber);
                $('#affiliate_agencyCheckNumber').html("Agency Check Number : "+agencyCheckNumber);
                $('#affiliate_bank_number').html("Bank Number : "+bank_number);
                $('#affiliate_myModal').modal('show');
        }
        function affiliate_account_info1(amount, name, token, paypal,id){
            console.log(token)
                // $('#amount').val(amount);
                $('#affiliate_payment_token_no1').val(token);
                $('#affiliate_paymentId1').val(id);
                $('#affiliate_professional_name1').html("Affiliate Name : "+name);
                $('#affiliate_payment_amount1').html("Payment Amount : "+amount);
                $('#affiliate_paypal').html("Paypal Address : "+paypal);
                $('#affiliate_myModal1').modal('show');
        }
    </script>
    <script>
        $(document).ready(function(){
            $("#updateeventsform2").validate({});
            $("#updateeventsform3").validate({});
            $("#affiliate_updateeventsform2").validate({});
            $("#affiliate_updateeventsform3").validate({});
        });
    </script>
    @endsection
    @section('footer')
    @include('admin.includes.footer')
    @endsection
