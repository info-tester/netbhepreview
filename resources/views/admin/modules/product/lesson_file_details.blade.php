@extends('admin.layouts.app')
@section('title', 'Netbhe| Admin | Lesson File Details')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')


<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">
                        Lesson File Details
                    </h4>
                    <div class="submit-login no_mmg pull-right">
                        <!-- <a href="{{ route('admin.product.list')}}" title="Back"><button type="button"
                                class="btn btn-default">Back</button></a> -->
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12">

                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <h4 class="pull-left page-title">Lesson File Info</h4>
                                    <div class="order-detail">
                                        <!--order-detail start-->
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="order-id">
                                                <div class="clearfix"></div>
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>File Name :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    @if(@$product->filetype =='Y')
                                                    <strong>YouTube Embed Link</strong>
                                                    @else
                                                    <strong>{{ @$product->name ? @$product->name : @$product->filename }}</strong>
                                                    @endif
                                                </div>
                                                <div class="clearfix"></div>
                                                 <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>File :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                   
                                                    @if(@$product->filetype == 'V')
                                                    
                                                    <video width="100%" height="320" controls src="{{ URL::to('storage/app/public/lessons/type_video/'.@$product->filename) }}" >
                                                    </video>
                                                    
                                                    @elseif(@$product->filetype == 'Y')
                                                    
                                                    
                                                    <iframe width="100%" height="320" src="{{'https://www.youtube.com/embed/'.@$product->filename}}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                    
                                                    @elseif(@$product->filetype == 'P')
                                                    
                                                    <strong>Pdf</strong>

                                                    @elseif(@$product->filetype == 'T')

                                                    <img src="{{ URL::to('storage/app/public/lessons/type_video/attachments/'.@$product->filename)}}">

                                                    @elseif(@$product->filetype == 'A')
                                                    
                                                    <audio src="{{URL::to('storage/app/public/lessons/type_audio/'.@$product->filename)}}" controls ></audio> 

                                                    @elseif(@$product->filetype == 'D')
                                                    
                                                        @if(@$product->mimetype == 'image/jpeg' || @$product->mimetype == 'image/png')
                                                            <img src="{{URL::to('storage/app/public/lessons/type_video/attachments/'.@$product->filename)}}">
                                                        @endif
                                                        @if(@$product->mimetype == 'video/webm' || @$product->mimetype == 'video/mp4')

                                                        @if(@$product->parent_id == 0)
                                                         <video width="100%" height="320" controls src="{{ URL::to('storage/app/public/lessons/type_downloads/'.@$product->filename) }}" >
                                                            </video> 

                                                        @else
                                                         <video width="100%" height="320" controls src="{{ URL::to('storage/app/public/lessons/type_video/attachments/'.@$product->filename) }}" >
                                                        </video>

                                                        @endif
                                                           
                                                        @endif

                                                        @if(@$product->mimetype == 'audio/mpeg')
                                                            <audio src="{{URL::to('storage/app/public/lessons/type_video/attachments/'.@$product->filename)}}" controls ></audio> 
                                                        @endif

                                                        @if(@$product->mimetype == 'application/pdf' || @$product->mimetype == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document')
                                                      
                                                        @if(@$product->parent_id == 0)
                                                        <a href="{{URL::to('storage/app/public/lessons/type_downloads/'.@$product->filename)}}" controls target="blank" ><strong>{{@$product->filename}}</strong></a> 

                                                        @else
                                                        <a href="{{URL::to('storage/app/public/lessons/type_video/attachments/'.@$product->filename)}}" controls target="blank" ><strong>{{@$product->filename}}</strong></a> 

                                                        @endif
                                                            
                                                        @endif


                                                   
                                                    <!-- <strong>Download</strong> -->
                                                    @endif
                                                </div>
                                                <div class="clearfix"></div>

                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>File Type :</p>
                                                </div>
                                                
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    @if(@$product->filetype == 'V')
                                                        <strong>Video</strong>
                                                        @elseif(@$product->filetype == 'Y')
                                                        <strong>YouTube Embed Link</strong>
                                                        @elseif(@$product->filetype == 'P')
                                                        <strong>Pdf</strong>
                                                        @elseif(@$product->filetype == 'T')
                                                        <strong>Thumbnail</strong>
                                                        @elseif(@$product->filetype == 'A')
                                                        <strong>Audio</strong>
                                                        @elseif(@$product->filetype == 'D')
                                                        <strong>Download</strong>
                                                    @endif
                                                    
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                   
                                    <!--order-detail end-->
                                </div>
                                <div class="col-md-12">

                                </div>
                                <div class="clearfix"></div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Row -->
    </div>
    <!-- container -->
</div>
<!-- content -->

</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
<!-- Right Sidebar -->
<!-- /Right-bar -->
</div> 
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
