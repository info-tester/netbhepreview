@extends('admin.layouts.app')
@section('title', 'Netbhe | Product List')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Manage Product List</h4>
                    <div class="submit-login no_mmg pull-right">
                        {{-- <a href="{{ route('admin.product.category.add.view') }}" title="Create"><button type="button"
                                class="btn btn-default">Add</button></a> --}}
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-rep-plugin">
                            <div class="row">
                                <div class="panel panel-default">
                                    <div class="panel-body table-rep-plugin">
                                        <form method="post" action="{{route('admin.product.list.search')}}">
                                            @csrf
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Product Name</label>
                                                    <input class="form-control" name="keyword" placeholder="Product Name" value="{{ @$key['keyword'] }}"
                                                        type="text">
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Status</label>
                                                    <select class="form-control newdrop required" name="status">
                                                        <option value="">Select Status</option>
                                                        <option value="A" @if(@$key['status']=="A" ) selected @endif>Active</option>
                                                        <option value="I" @if(@$key['status']=="I" ) selected @endif>Inactive</option>
                                                        <option value="AA" @if(@$key['status']=="AA" ) selected @endif>Awaiting Approval</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div id="profDiv">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Product Category</label>
                                                        <select class="form-control newdrop " name="category" id="category">
                                                            <option value="">Select category</option>
                                                            @foreach($productCategoryList as $row)
                                                            <option value="{{$row->id}}" @if(@$key['category']==$row->id) selected @endif>{{$row->category_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="profDiv">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Professional Name</label>
                                                        <select class="form-control newdrop " name="professional" id="professional">
                                                            <option value="">Select Professional</option>
                                                            @foreach($professional as $row)
                                                            <option value="{{$row->id}}" @if(@$key['professional']==$row->id) selected @endif>{{ @$row->nick_name ? $row->nick_name : @$row->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="profDiv">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Product Subcategory</label>
                                                        <select class="form-control newdrop " name="product_sub_cat" id="product_sub_cat">
                                                            <option value="0">Select Product Subcategory</option>
                                                            @foreach($product_sub_category as $row)
                                                            <option value="{{$row->id}}"  @if(@$key['product_sub_cat']==$row->id) selected @endif>{{$row->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="clearfix"></div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="submit-login add_btnm pull-right" style="margin-top: 5px;">
                                                    <input value="Search" class="btn btn-default" type="submit">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="col-md-12 dess5">
                                   <i class="fa fa fa-eye cncl" aria-hidden="true"> <span class="cncl_oopo">View</span></i>
                                   <i class="fa fa-times-circle cncl" aria-hidden="true"> <span class="cncl_oopo">Reject</span></i>
                                   <i class="fa fa-check-circle cncl" aria-hidden="true"> <span class="cncl_oopo">Approve</span></i>
                                   <i class="fa fa-square cncl" aria-hidden="true"> <span class="cncl_oopo">Show in Home Page</span></i>
                                   <i class="fa fa-square-o cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">Not Show in Home Page</span></i>

                                {{-- <i class="fa fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
                                <i class="fa fa-trash-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
                                <i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active</span></i>
                                <i class="fa fa-times cncl" aria-hidden="true"> <span class="cncl_oopo">Inactive</span></i>
                                <i class="fa fa-envelope-square cncl" aria-hidden="true"> <span class="cncl_oopo">Email Verify Link</span></i> --}}



                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <table id="datatable" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Name of Product</th>
                                                    <th>Price</th>
                                                    <th>Category</th>
                                                    <th>Created By</th>
                                                    <th>Added on</th>
                                                    <th>Updated on</th>
                                                    <th>User Status</th>
                                                    <th>Admin Status</th>
                                                    <th>Show in Home</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($products as $row)
                                                <tr>
                                                    <td>{{ @$row->title}}</td>
                                                    <td>{{ @$row->price}}</td>
                                                    <td>{{ @$row->category->category_name}}</td>
                                                    <td>{{ @$row->professional->nick_name ? @$row->professional->nick_name : @$row->professional->name}}</td>   
                                                    <td>{{ date('Y-m-d',strtotime(@$row->created_at) )}}</td>
                                                    <td>{{ date('Y-m-d',strtotime(@$row->updated_at) )}}</td>
                                                    <td>
                                                        @if(@$row->status == 'I')
                                                        <label class="label label-danger">Inactive</label>
                                                        @elseif(@$row->status == 'A')
                                                        <label class="label label-success">Active</label>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if(@$row->admin_status == 'I')
                                                        <label class="label label-danger">Inactive</label>
                                                        @elseif(@$row->admin_status == 'A')
                                                        <label class="label label-success">Active</label>
                                                        @elseif(@$row->admin_status == 'AA')
                                                        <label class="label label-warning">Awaiting Approval</label>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if(@$row->show_in_home_page == 'N')
                                                        <label class="label label-danger">No</label>
                                                        @elseif(@$row->show_in_home_page == 'Y')
                                                        <label class="label label-success">Yes</label>
                                                        @endif
                                                    </td>
                                                    <td>

                                                        <a href="{{ route('admin.product.chapter.view',[@$row->id]) }}"
                                                            title="Curriculum">
                                                            <i class="fa fa-folder delet" aria-hidden="true"></i>
                                                            
                                                        </a>

                                                        <a href="{{ route('admin.product.view',[@$row->id]) }}"
                                                            title="View this product">
                                                            <i class="fa fa-eye delet" aria-hidden="true"></i>
                                                        </a>
                                                        <a href="{{route('admin.product.delete',[@$row->id])}}"
                                                            onclick="return confirm('Are you want to delete this Product ?');"
                                                            title="Delete">
                                                            <i class="fa fa-trash-o delet" aria-hidden="true"></i>
                                                        </a>
                                                        @if(@$row->show_in_home_page == 'Y')
                                                        <a href="{{route('admin.product.showinhomepage',[@$row->id])}}"
                                                            title="Remove this product from home page"
                                                            onclick="return confirm('Do you want to hide this product from home page?');">
                                                            <i class="fas fa-square" aria-hidden="true"></i>
                                                        </a>
                                                        @elseif(@$row->show_in_home_page == 'N')
                                                        <a href="{{route('admin.product.showinhomepage',[@$row->id])}}"
                                                            title="Show this product in home page"
                                                            onclick="return confirm('Do you want to show this product in home page?');">
                                                            <i class="fa fa-square-o" aria-hidden="true" style="border: none;"></i>
                                                        </a>
                                                        @endif
                                                        @if(@$row->admin_status == 'I')
                                                        <a href="{{route('admin.product.status.change',[@$row->id])}}"
                                                            title="Activate this product"
                                                            onclick="return confirm('Do you want to activate this Product ?');">
                                                            <i class="fa fa-check delet" aria-hidden="true"></i>
                                                        </a>
                                                        @elseif(@$row->admin_status == 'A')
                                                        <a href="{{route('admin.product.status.change',[@$row->id])}}"
                                                            title="Inactivate this product"
                                                            onclick="return confirm('Do you want to inactivate this Product ?');">
                                                            <i class="fa fa-times delet" aria-hidden="true"></i>
                                                        </a>
                                                        @elseif(@$row->admin_status == 'AA')
                                                        <a href="{{route('admin.product.status.change',[@$row->id])}}"
                                                            title="Approve this product" data-id="{{@$row->id}}"
                                                            onclick="return confirm('Do you want to approve this Product ?');"
                                                            >
                                                            <i class="fa fa-check delet" aria-hidden="true"></i>
                                                        </a>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalapprove">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Admin Description </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form name="myForm3" id="myForm3" method="post" action="{{route('admin.product.approve')}}">
                @csrf
                <div class="modal-body">
                    <input type="hidden" name="product_id" id="product_id">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group" style="width: 100%;">
                                    <label>Description</label>
                                    <textarea name="admin_description" id="admin_description" class="form-control required"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" value="Approve" class="btn btn-primary">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('site.close')</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        // $('#myForm3').validate({
        //     rules:{
        //     'admin_description' : 'required'
        //     },
        //     submitHandler: function(form) {
        //         // console.log("ABC");
        //         // return false;
        //         form.submit();
        //     }
        // });
        // $('#myForm3').submit(function(e){
        //     e.preventDefault();
        //     console.log("ABC");
        // })
        // $('.approveProduct').click(function(){
        //     if(confirm('Do you want to approve this Product ?')){
        //         var id = $(this).data('id');
        //         $('#product_id').val(id);
        //         $('#admin_description').val('');
        //         $('#modalapprove').modal('show')
        //     }
        // });
    });
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
