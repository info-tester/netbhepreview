@extends('admin.layouts.app')
@section('title', 'Netbhe| Admin | Lesson Details')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">
                        Lesson Details
                    </h4>
                    <!-- <div class="submit-login no_mmg pull-right">
                        <a href="{{ route('admin.product.list')}}" title="Back"><button type="button"
                                class="btn btn-default">Back</button></a>
                    </div> -->

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <span><b>Professional Name : {{ @$professional->nick_name ? @$professional->nick_name :@$professional->name }}</b></span>
                                    &nbsp;
                                    >
                                    &nbsp;
                                    <span><b><a href="{{route('admin.product.list')}}">Course : {{@$product_detail->title}}</a></b></span>
                                    &nbsp;
                                    >
                                    &nbsp;
                                    <span><b><a href="{{route('admin.product.chapter.view',[@$product_detail->id])}}">Chapter : {{ @$chapter->chapter_title}}</a></b></span>
                                    &nbsp;
                                    >
                                    &nbsp;
                                    <span><b>Lesson : {{ @$product->lesson_title }}</b></span>
                                    <span></span>
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <h4 class="pull-left page-title">Lesson Info</h4>

                                    <div class="order-detail">
                                        <!--order-detail start-->
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="order-id">
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Lesson Title :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <span>{{ @$product->lesson_title }}</span>
                                                </div>

                                                <div class="clearfix"></div>
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Lesson Type :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    @if(@$product->lesson_type == 'V')
                                                    <strong>Video</strong>
                                                    @elseif(@$product->lesson_type == 'Q')
                                                    <strong>Quiz</strong>
                                                    @elseif(@$product->lesson_type == 'M')
                                                    <strong>Multimedia</strong>
                                                    @elseif(@$product->lesson_type == 'T')
                                                    <strong>Text</strong>
                                                    @elseif(@$product->lesson_type == 'A')
                                                    <strong>Audio</strong>
                                                    @elseif(@$product->lesson_type == 'D')
                                                    <strong>Download</strong>
                                                    @elseif(@$product->lesson_type == 'P')
                                                    <strong>Presentation</strong>
                                                    @endif
                                                </div>
                                                <div class="clearfix"></div>
                                                <!-- <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Professional Name :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong>{{ @$professional->nick_name ? @$professional->nick_name :@$professional->name }}</strong>
                                                </div> -->

                                                <div class="clearfix"></div>
                                               <!--  <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Price :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong>{{ $product->price}}</strong>
                                                </div> -->

                                                <div class="clearfix"></div>
                                                <!-- <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Product Type :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong>{{ $product->product_type=='N'?'Normal':'Customizable'}}</strong>
                                                </div> -->
                                                <div class="clearfix"></div>
                                                <!-- <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Product Show In Home :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong>{{ $product->show_in_home_page =='Y'? 'Yes':'No'}}</strong>
                                                </div> -->
                                                <div class="clearfix"></div>
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Is Allowed Discussion:</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong>{{ $product->is_allow_discussion =='Y'? 'Yes':'No'}}</strong>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Is Prerequisite:</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong>{{ $product->is_prerequisite =='Y'? 'Yes':'No'}}</strong>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Is Free Preview:</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong>{{ $product->is_free_preview =='Y'? 'Yes':'No'}}</strong>
                                                </div>
                                                @if(count(@$lesson_files) > 0)
                                                <div class="clearfix"></div>

                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>No of files:</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong>{{ count(@$lesson_files) }}</strong>
                                                </div>
                                                @endif

                                                @if(@$product->lesson_type == 'M')
                                                <div class="clearfix"></div>

                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Multimedia URL:</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong><a href="{{@$product->multimedia_url}}">{{ @$product->multimedia_url }}</a></strong>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                      
                                        @if(@$product->lesson_type != 'Q')
                                            @if(@$product->lesson_type == 'A' || @$product->lesson_type == 'V' || @$product->lesson_type == 'T' )
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="order-id">
                                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                        <p>Lesson Description :</p>
                                                    </div>
                                                </div>
                                                <div class="blog_area_nn01 col-12" style="text-align: left !important; box-shadow:none !important;">
                                                    {!! $product->description !!}
                                                </div>
                                            </div>
                                            @endif
                                        @else

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="table-responsive" data-pattern="priority-columns">
                                                <table id="datatable1" class="table table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>Question</th>
                                                                <th>Option 1</th>
                                                                <th>Option 2</th>
                                                                <th>Option 3</th>
                                                                <th>Option 4</th>
                                                                <th>Option 5</th>
                                                                <th>Option 6</th>
                                                                <th>Correct Answer</th>
                                                                <th>Explanation</th>
                                                            </tr>
                                                        </thead>  
                                                        <tbody>

                                                            @foreach(@$question as $row)
                                                            <tr>
                                                                <td>{!! @$row->question !!}</td>
                                                                <td>{!! @$row->answer_1 !!}</td>
                                                                <td>{!! @$row->answer_2 !!}</td>
                                                                <td>{!! @$row->answer_3 !!}</td>
                                                                <td>{!! @$row->answer_4 !!}</td>
                                                                <td>{!! @$row->answer_5  !!}</td>
                                                                <td>{!! @$row->answer_6 !!}</td>
                                                                <td>Option {{@$row->correct_answer}}</td>
                                                                <td>{!! @$row->explanation !!}</td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>  
                                                    </table>
                                                    
                                                </div>
                                                        
                                        </div>

                                        @endif
                                        @if(count(@$lesson_files)>0)
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="table-responsive" data-pattern="priority-columns">
                                                    <table id="datatable2" class="table table-striped table-bordered">
                                                <thead>


                                                    <tr>
                                                        <th>Filename</th>
                                                        <!-- <th>Created By</th> -->
                                                        <th>File type</th>
                                                        <!-- <th>User Status</th> -->
                                                        <th>File</th>
                                                        <!-- <th>Action</th> -->

                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    @foreach($lesson_files as $row)
                                                    <tr>
                                                        @if(@$row->filetype == 'D' && (@$row->mimetype == 'image/jpeg' || @$row->mimetype == 'image/png')  )
                                                           
                                                            
                                                                
                                                                @if(@$row->parent_id == 0)
        
                                                                    <td><img src="{{URL::to('storage/app/public/lessons/type_downloads/'.@$row->filename)}}" width="100" height="100"></td>
        
                                                                @else
        
                                                                    <td><img src="{{URL::to('storage/app/public/lessons/type_video/attachments/'.@$row->filename)}}" width="100" height="100"></td>
        
                                                                @endif
                                                            
                                                                
                                                            

                                                                
                                                            
                                                        @else
                                                        <td>{{ @$row->name ? @$row->name : @$row->filename }}</td>
                                                        @endif
                                                        <!-- <td>{{ @$professional->nick_name ? @$professional->nick_name : @$professional->name}}</td> -->
                                                        
                                                        <td>
                                                            @if(@$row->filetype == 'V')
                                                            Video
                                                            @elseif(@$row->filetype == 'Y')
                                                            Youtube Embed
                                                            @elseif(@$row->filetype == 'P')
                                                            Pdf
                                                            @elseif(@$row->filetype == 'T')
                                                            Thumbnail
                                                            @elseif(@$row->filetype == 'A')
                                                            Audio
                                                            @elseif(@$row->filetype == 'D')
                                                            Download
                                                            @endif
                                                            
                                                        </td>

                                                        <td>

                                                        @if(@$row->filetype == 'V')
                                                        
                                                        <video width="100%" height="320" controls src="{{ URL::to('storage/app/public/lessons/type_video/'.@$row->filename) }}" >
                                                        </video>

                                                        @elseif(@$row->filetype == 'Y')

                                                        <iframe width="100%" height="320" src="{{'https://www.youtube.com/embed/'.@$row->filename}}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                                                        @elseif(@$row->filetype == 'P')
                                                            
                                                            <strong>Pdf</strong>
                                                        @elseif(@$row->filetype == 'T')

                                                        <img src="{{ URL::to('storage/app/public/lessons/type_video/attachments/'.@$row->filename)}}">

                                                        @elseif(@$row->filetype == 'A')

                                                        <audio src="{{URL::to('storage/app/public/lessons/type_audio/'.@$row->filename)}}" controls ></audio> 

                                                        @elseif(@$row->filetype == 'D')
                                                        
                                                            @if(@$row->mimetype == 'image/jpeg' || @$row->mimetype == 'image/png')
                                                            
                                                            @if(@$row->parent_id == 0)

                                                                <a href="{{URL::to('storage/app/public/lessons/type_downloads/'.@$row->filename)}}" target="blank"><button type="button" class="btn btn-success" >Download</button></a>

                                                            @else

                                                                <a href="{{URL::to('storage/app/public/lessons/type_video/attachments/'.@$row->filename)}}" target="blank"><button type="button" class="btn btn-success">Download</button></a>

                                                            @endif
                                                                
                                                            @endif
                                                            @if(@$row->mimetype == 'video/webm' || @$row->mimetype == 'video/mp4')

                                                            @if(@$row->parent_id == 0)
                                                             <video width="100%" height="320" controls src="{{ URL::to('storage/app/public/lessons/type_downloads/'.@$row->filename) }}" >
                                                                </video> 

                                                            @else
                                                             <video width="100%" height="320" controls src="{{ URL::to('storage/app/public/lessons/type_video/attachments/'.@$row->filename) }}" >
                                                            </video>

                                                            @endif
                                                               
                                                            @endif

                                                            @if(@$row->mimetype == 'audio/mpeg')
                                                                <audio src="{{URL::to('storage/app/public/lessons/type_video/attachments/'.@$row->filename)}}" controls ></audio> 
                                                            @endif

                                                            @if(@$row->mimetype == 'text/plain')

                                                             @if(@$row->parent_id == 0)
                                                            <a href="{{URL::to('storage/app/public/lessons/type_downloads/'.@$row->filename)}}" controls download ><button type="button" class="btn btn-success">Download</button></a> 

                                                            @else
                                                            <a href="{{URL::to('storage/app/public/lessons/type_video/attachments/'.@$row->filename)}}" controls download ><button type="button" class="btn btn-success">Download</button></a> 

                                                            @endif
                                                            
                                                            @endif


                                                            @if(@$row->mimetype == 'application/pdf' || @$row->mimetype == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document')
                                                          
                                                            @if(@$row->parent_id == 0)
                                                            <a href="{{URL::to('storage/app/public/lessons/type_downloads/'.@$row->filename)}}" controls download ><button type="button" class="btn btn-success">Download</button></a> 

                                                            @else
                                                            <a href="{{URL::to('storage/app/public/lessons/type_video/attachments/'.@$row->filename)}}" controls download ><button type="button" class="btn btn-success">Download</button></a> 

                                                            @endif
                                                                
                                                            @endif


                                                       
                                                        <!-- <strong>Download</strong> -->
                                                        @endif

                                                        </td>

                                                        <!-- <td>
                                                            @if(@$row->status == 'D')
                                                            <label class="label label-danger">Draft</label>
                                                            @elseif(@$row->status == 'P')
                                                            <label class="label label-success">Published</label>
                                                            @endif
                                                        </td> -->
                                                        
                                                       
                                                        
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        @endif
                                        @if(@$slides)
                                            @if(count(@$slides)>0)
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="table-responsive" data-pattern="priority-columns">
                                                    <table id="datatable3" class="table table-striped table-bordered">
                                                        <thead>


                                                            <tr>
                                                                <th>Slide</th>
                                                                <th>Slide Image</th>
                                                                <th>Description</th>
                                                                <th>Audio present(Y/N)</th>
                                                                <!-- <th>Action</th> -->
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                            @foreach(@$slides as $k => $row)
                                                            <tr>
                                                                <td>Slide {{@$k+1}} </td>
                                                                <td><img src="{{ URL::to('storage/app/public/lessons/type_presentation/'.@$row->filename)}}" height="300" width="100%"></td>
                                                               
                                                                @if($row->description != null)
                                                                <td>
                                                                    @if(strlen($row->description) > 50)
                                                                    {{ str_limit(strip_tags($row->description),100) }}
                                                                    @else
                                                                    {!!$row->description!!}
                                                                    @endif
                                                                    
                                                                </td>
                                                                @else
                                                                <td>
                                                                    
                                                                </td>
                                                            
                                                                @endif
                                                        
                                                                @if(@$row->audio != null)
                                                                <td>
                                                                
                                                                    <audio src="{{URL::to('storage/app/public/lessons/type_presentation/audio/'.@$row->audio)}}" controls></audio> 
                                                                
                                                                </td>
                                                                @else
                                                                <td>No Audio Present</td>
                                                                @endif
                                                                <!-- <td>
                                                                   <a href="{{ route('admin.lesson.slides.view',[@$row->id]) }}"
                                                                        title="View this Slide">
                                                                        <i class="fa fa-eye delet" aria-hidden="true"></i>
                                                                    </a> 
                                                                </td> -->
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                    </div>
                                                </div>
                                            @endif
                                        @endif


                                        
                                    </div>

                                    <div class="order-detail">
                                        <h4 align="center">Questions & Answers</h4>
                            
                                         <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="table-responsive" data-pattern="priority-columns">
                                                <table id="datatable4" class="table table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Question</th>
                                                                <th>Asked by</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @if(@$ques)
                                                            @foreach(@$ques as $k=>$row)
                                                            <tr>
                                                                <td>{{@$k+1}}</td>
                                                                <td>{{@$row->question}}</td>
                                                                <td>{{@$row->getQuestioner->name}}</td>
                                                                <td>
                                                                   <a href="{{route('admin.lesson.qa',@$row->id)}}"
                                                                        title="View Question Answer">
                                                                        <i class="fa fa-eye delet" aria-hidden="true"></i>
                                                                    </a> 
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                            @endif
                                                            
                                                        </tbody>
                                                        
                                                    </table>
                                                    </div>
                                                </div>

                                    </div>
                                   
                                    <!--order-detail end-->


                                </div>
                                <div class="col-md-12">



                                </div>
                                <div class="clearfix"></div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Row -->
    </div>
    <!-- container -->
</div>
<!-- content -->

</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
<!-- Right Sidebar -->
<!-- /Right-bar -->
</div>
<script>

    $(document).ready( function () {
    $('#datatable1').DataTable(
      {
        "bProcessing": true,
        "sAutoWidth": false,
        "bDestroy":true,
        "sPaginationType": "bootstrap", // full_numbers
        "iDisplayStart ": 10,
        "iDisplayLength": 10,
        "bPaginate": false, //hide pagination
        "bFilter": false, //hide Search bar
        "bInfo": false, // hide showing entries
      }
    );

    $('#datatable2').DataTable(
      {
        "bProcessing": true,
        "sAutoWidth": false,
        "bDestroy":true,
        "sPaginationType": "bootstrap", // full_numbers
        "iDisplayStart ": 10,
        "iDisplayLength": 10,
        "bPaginate": false, //hide pagination
        "bFilter": false, //hide Search bar
        "bInfo": false, // hide showing entries
      }
    );


    $('#datatable3').DataTable(
      {
        "bProcessing": true,
        "sAutoWidth": false,
        "bDestroy":true,
        "sPaginationType": "bootstrap", // full_numbers
        "iDisplayStart ": 10,
        "iDisplayLength": 10,
        "bPaginate": false, //hide pagination
        "bFilter": false, //hide Search bar
        "bInfo": false, // hide showing entries
      }
    );


    $('#datatable4').DataTable(
      {
        "bProcessing": true,
        "sAutoWidth": false,
        "bDestroy":true,
        "sPaginationType": "bootstrap", // full_numbers
        "iDisplayStart ": 10,
        "iDisplayLength": 10,
        "bPaginate": false, //hide pagination
        "bFilter": false, //hide Search bar
        "bInfo": false, // hide showing entries
      }
    );



} );
    
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
