@extends('admin.layouts.app')
@section('title', 'Netbhe| Admin | Lesson QA Details')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">
                        Lesson QA Details
                    </h4>
                    <!-- <div class="submit-login no_mmg pull-right">
                        <a href="{{ route('admin.product.list')}}" title="Back"><button type="button"
                                class="btn btn-default">Back</button></a>
                    </div> -->

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    @if(@$answers)
                                    <div class="order-detail">
                                        <h4 align="center">Questions & Answers</h4>
                                        <span><b>Question :</b><span style="font-size:20px">{{@$answers->question}}</span></span>
                                        
                                        
                                        @foreach(@$answers->getAnswers as $ans)
                                        
                                        <div class="question_box ansqq" id="answer_box_2" style="">
                                            <span class="question_user"><img src=""></span>
                                            <div class="question_details">
                                                
                                                
                                                <span><b>Answer :</b>{{$ans->answers}}</span>
                                            </div>
                                        </div>
                                        
                                        @endforeach

                                         

                                    </div>
                                    @endif
                                    
                                </div>
                                <div class="col-md-12">



                                </div>
                                <div class="clearfix"></div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Row -->
    </div>
    <!-- container -->
</div>
<!-- content -->

</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
<!-- Right Sidebar -->
<!-- /Right-bar -->
</div>
<script>

    $(document).ready( function () {
    $('#datatable1').DataTable(
      {
        "bProcessing": true,
        "sAutoWidth": false,
        "bDestroy":true,
        "sPaginationType": "bootstrap", // full_numbers
        "iDisplayStart ": 10,
        "iDisplayLength": 10,
        "bPaginate": false, //hide pagination
        "bFilter": false, //hide Search bar
        "bInfo": false, // hide showing entries
      }
    );

    $('#datatable2').DataTable(
      {
        "bProcessing": true,
        "sAutoWidth": false,
        "bDestroy":true,
        "sPaginationType": "bootstrap", // full_numbers
        "iDisplayStart ": 10,
        "iDisplayLength": 10,
        "bPaginate": false, //hide pagination
        "bFilter": false, //hide Search bar
        "bInfo": false, // hide showing entries
      }
    );


    $('#datatable3').DataTable(
      {
        "bProcessing": true,
        "sAutoWidth": false,
        "bDestroy":true,
        "sPaginationType": "bootstrap", // full_numbers
        "iDisplayStart ": 10,
        "iDisplayLength": 10,
        "bPaginate": false, //hide pagination
        "bFilter": false, //hide Search bar
        "bInfo": false, // hide showing entries
      }
    );


} );
    
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
