@extends('admin.layouts.app')
@section('title', @$subcategory ? 'Netbhe | Edit Product Category':'Netbhe | Add Product Category')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">{{@$productSubcategory?'Update':'Add'}} Product Subcategory</h4>
                    <div class="submit-login no_mmg pull-right">
                       <!--  <a href="{{ route('admin.product.category.edit.view',[@$productCategory->id]) }}" title="Back"><button type="button"
                                class="btn btn-default">Back</button></a> -->

                        <a href="{{ route('admin.product.category') }}" title="Back"><button type="button"
                                class="btn btn-default">Back</button></a>
                    </div>
                </div>
            </div>

            {{-- @include('admin.includes.error') --}}
            {{-- {{ dd(old()) }} --}}
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-rep-plugin">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 nhp">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <form id="myform" method="post" action="{{route('admin.product.subcategory.store')}}"
                                            enctype="multipart/form-data">
                                            @csrf
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Product Subcategory Name</label>
                                                    <input type="text" name="category_id" class="form-control required" value="{{@$productCategory->id}}" style="display:none;">
                                                    <input type="text" name="subcategory_id" class="form-control" value="{{@$productSubcategory->id}}" style="display:none;">
                                                    <input type="text" name="subcategory_name" class="form-control required" value="{{ old('subcategory_name',@$productSubcategory->name) }}">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>

                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="add_btnm submit-login">
                                                    <input value="{{@$productSubcategory?'Update':'Add'}}" type="submit" class="btn btn-default">
                                                </div>
                                            </div>
                                            <!--all_time_sho-->
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Row -->

        </div>
        <!-- container -->
    </div>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
<script>
    $(document).ready(function(){
        $("#myform").validate();
    });

    // $('#parent_id').change(function(){
    //     if($('#parent_id').val()!=""){
    //         $('#img').hide();
    //         $('#meta_desc').hide();
    //         $('#meta_tit').hide();
    //     }
    //     else{
    //         $('#img').show();
    //         $('#meta_desc').show();
    //         $('#meta_tit').show();
    //     }

    // });
</script>
@endsection
