@extends('admin.layouts.app')
@section('title', 'Netbhe | Admin | Other Page Content')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
<div class="content">
    <div class="container">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="pull-left page-title">Newsletter Subscription</h4>
            </div>
        </div>

        @include('admin.includes.error')

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body table-rep-plugin">
                        <div class="row">
                            <form method="POST" action="{{ route('other.page.contents') }}" id="myForm">
                                @csrf
                                <div class="col-lg-12">
                                    <h4 class="pull-left" style="margin-top: 8px;">Contents in Professional Public Profile Page</h4>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                    <div class="your-mail">
                                        <label for="exampleInputEmail1">Heading *</label>
                                        <input type="text" name="pp_title" id="pp_title" value="{{ $profPage->title }}" placeholder="Write heading">
                                    </div>
                                </div>
                                <div class="all_time_sho">
                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                        <div class="your-mail">
                                            <label for="exampleInputEmail1">Description *</label>
                                            <textarea placeholder="Write description" name="pp_description" id="pp_description" rows="6" class="form-control required">{{ $profPage->description }}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12" style="margin-top: 16px;">
                                    <h4 class="pull-left" style="margin-top: 24px;">Contents in Professional Booking Page</h4>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                    <div class="your-mail">
                                        <label for="exampleInputEmail1">Heading *</label>
                                        <input type="text" name="pb_title" id="pb_title" value="{{ $bookingPage->title }}" placeholder="Write heading">
                                    </div>
                                </div>
                                <div class="all_time_sho">
                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                        <div class="your-mail">
                                            <label for="exampleInputEmail1">Description *</label>
                                            <textarea placeholder="Write description" name="pb_description" id="pb_description" rows="6" class="form-control required">{{ $bookingPage->description }}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12" style="margin-top: 16px;">
                                    <h4 class="pull-left" style="margin-top: 24px;">Contents in Professional Dashboard Page</h4>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                    <div class="your-mail">
                                        <label for="exampleInputEmail1">Heading *</label>
                                        <input type="text" name="pd_title" id="pd_title" value="{{ $dashBoardPage->title }}" placeholder="Write heading">
                                    </div>
                                </div>
                                <div class="all_time_sho">
                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                        <div class="your-mail">
                                            <label for="exampleInputEmail1">Description *</label>
                                            <textarea placeholder="Write description" name="pd_description" id="pd_description" rows="6" class="form-control required">{{ $dashBoardPage->description }}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
                                        <input value="Update" type="submit" class="btn btn-default">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Row -->
    </div>
    <!-- container -->
</div>
</div>
<script>
    $(document).ready(function() {
        $("#myForm").validate();
    });
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
