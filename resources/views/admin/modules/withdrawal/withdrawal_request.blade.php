@extends('admin.layout.auth')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- Begin page -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->                      
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="pull-left page-title">Manage withdrawal request</h4>
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <!--<div class="panel-heading">
                                <h3 class="panel-title">Default Example</h3>
                                </div>-->
                            <div class="panel-body table-rep-plugin">
                                <div class="row top_from">
                                    <form method="post" action="{{ route('admin.withdrawal.request')}}">
                                        {{ csrf_field() }}
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">Status</label>
                                                <select name="status" class="form-control newdrop">
                                                    <option value="">Select Status</option>
                                                    <option value="I" @if(@$post_data['status'] == 'I') {{ 'selected' }} @endif>New Request</option>
                                                    <option value="C" @if(@$post_data['status'] == 'C') {{ 'selected' }} @endif>Completed</option>
                                                    <option value="R" @if(@$post_data['status'] == 'R') {{ 'selected' }} @endif>Rejected</option>
                                                </select>
                                            </div>
                                        </div>
                                       
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">Seller</label>
                                                <select name="seller" class="form-control newdrop">
                                                    <option value="">Select Seller</option>
                                                    @foreach($sellers as $row)
                                                    <option @if(@$post_data['seller'] == $row->id) {{ 'selected' }} @endif value="{{ $row->id }}">{{ $row->first_name.' '.$row->last_name }} [{{ $row->company->name }}]</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="add_btnm pull-right">
                                                    <input value="Search" id="search" class="btn btn-primary" type="submit">
                                                </div>
                                        </div>
                                    </div>
                                    </form>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" data-pattern="priority-columns">
                                            <table id="datatable" class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Seller</th>
                                                        <th>Branch & Account</th>
                                                        <th>Request Amount</th>
                                                        <th>Request Date</th>
                                                        <th>Process Date</th>
                                                        <th>Process Note</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    @foreach($withdrawal_request as $row)
                                                    <tr>
                                                        <td>
                                                            Name: {{ $row->sellerName->first_name.' '.$row->sellerName->last_name }}
                                                            <br> 
                                                            Email: {{ $row->sellerName->email}}<br>
                                                            Company: {{ $row->sellerName->company->name}}
                                                        </td>
                                                        <td>
                                                            Branch Name: {{ $row->branchName->name }}
                                                            <br>
                                                            Full Address: {{ $row->branchName->full_address }}
                                                            <br>
                                                            Account: <strong>{{ $row->branchName->bank_account }}</strong>
                                                            <br>
                                                            IFSC: {{ $row->branchName->ifsc }}
                                                            <br>

                                                        </td>
                                                        <td>{{ env('RUPPY').' '.$row->amount }}</td>
                                                        <td>{{ niceDate($row->created_at) }}</td>
                                                        <td>{{ niceDate($row->process_date) }}</td>
                                                        <td>{{ $row->process_note?$row->process_note:'--' }}</td>
                                                        <td>
                                                            @if($row->status == 'I')
                                                            New Request
                                                            @elseif($row->status == 'C')
                                                            Completed
                                                            @elseif($row->status == 'R')
                                                            Rejected
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <a class="view" data-id="{{ $row->id }}" href="#" title="View"><i class="fa fa-eye delet" aria-hidden="true"></i></a>
                                                            @if($row->status == 'I')
                                                            <a data-id="{{ $row->id }}" class="accept" href="#" title="Accept payment"><i class="fa fa-check delet" aria-hidden="true"></i></a>

                                                            <a class="reject" data-id="{{ $row->id }}" href="#" data-toggle="modal" data-target="#process_note" title="Reject payment"><i class="fa fa-times delet" aria-hidden="true"></i></a>
                                                            
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Row -->
            </div>
            <!-- container -->
        </div>
        <!-- content -->
        <!--<footer class="footer text-right">
            2015 © Moltran.
            </footer>-->
    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->
    
</div>
<!-- END wrapper -->
<script type="text/javascript">
    $(document).ready(function() {
        $('.reject').click(function() {
            var id = $(this).data('id')

            $('#request_id1').val(id)
        });
        $('.accept').click(function(event) {
            var id = $(this).data('id')
            $('#request_id').val(id)
            var reqData = {
                'jsonrpc': '2.0',
                '_token': '{{ csrf_token() }}',
                'params': {
                    'id': id
                }
            };
            $.ajax({
                url: '{{ route('admin.get.seller.info') }}',
                type: 'post',
                dataType: 'json',
                data: reqData,
            })
            .done(function(response) {
                console.log("success", response);
                $('#branch_name').html(response.result.seller_info.branch_name.name)
                $('#address').html(response.result.seller_info.branch_name.full_address)
                $('#account_no').html(response.result.seller_info.branch_name.bank_account)
                $('#ifsc_code').html(response.result.seller_info.branch_name.ifsc)

                // seller info
                $('#seller_name').html(response.result.seller_info.seller_name.first_name+' '+response.result.seller_info.seller_name.last_name)
                $('#seller_email').html(response.result.seller_info.seller_name.email)
                $('#total_earning').html('{{ env('RUPPY') }}'+response.result.seller_info.seller_name.total_earning)
                $('#total_commission').html('{{ env('RUPPY') }}'+response.result.seller_info.seller_name.total_commission)
                $('#total_payout').html('{{ env('RUPPY') }}'+response.result.seller_info.seller_name.total_payout)
                $('#total_due').html('{{ env('RUPPY') }}'+response.result.seller_info.seller_name.total_due)
                $('#accept_form').show();
                $('#withdrawal_accept').modal('show')
            })
            .fail(function(error) {
                console.log("error");
            });
            
        });

        $('.view').click(function(event) {
            var id = $(this).data('id')
            $('#request_id').val(id)
            var reqData = {
                'jsonrpc': '2.0',
                '_token': '{{ csrf_token() }}',
                'params': {
                    'id': id
                }
            };
            $.ajax({
                url: '{{ route('admin.get.seller.info') }}',
                type: 'post',
                dataType: 'json',
                data: reqData,
            })
            .done(function(response) {
                console.log("success", response);
                $('#branch_name').html(response.result.seller_info.branch_name.name)
                $('#address').html(response.result.seller_info.branch_name.full_address)
                $('#account_no').html(response.result.seller_info.branch_name.bank_account)
                $('#ifsc_code').html(response.result.seller_info.branch_name.ifsc)

                // seller info
                $('#seller_name').html(response.result.seller_info.seller_name.first_name+' '+response.result.seller_info.seller_name.last_name)
                $('#seller_email').html(response.result.seller_info.seller_name.email)
                $('#total_earning').html('{{ env('RUPPY') }}'+response.result.seller_info.seller_name.total_earning)
                $('#total_commission').html('{{ env('RUPPY') }}'+response.result.seller_info.seller_name.total_commission)
                $('#total_payout').html('{{ env('RUPPY') }}'+response.result.seller_info.seller_name.total_payout)
                $('#total_due').html('{{ env('RUPPY') }}'+response.result.seller_info.seller_name.total_due)
                // $('#accept_form').show();
                $('#withdrawal_accept').modal('show')
            })
            .fail(function(error) {
                console.log("error");
            });
            
        });
    });
</script>
<div class="modal fade" id="withdrawal_accept" role="dialog">
    <div class="modal-dialog" style="margin: 100px auto;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" style="top: 0px; right: 0px;" type="button">
                    ×
                </button>
                <h4 class="modal-title">
                    Withdrawal Request
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-lg-12 col-md-12">
                        <div class="col-sm-12 col-lg-12 col-md-12 col-xs-12" style="padding: 0;">
                            <div class="panel panel-primary bank_info">
                                <div class="panel-body bg-ash-1">
                                    <div class="row">
                                        <div class="col-sm-6 col-lg-6 col-md-6">
                                            <div class="col-sm-5 col-lg-5 col-md-5">
                                                <strong>Branch Name</strong>
                                            </div>
                                            <div class="col-sm-7 col-lg-7 col-md-7">
                                                <p id="branch_name"></p>
                                            </div>
                                            <div class="col-sm-5 col-lg-5 col-md-5">
                                                <strong>A/C No.</strong>
                                            </div>
                                            <div class="col-sm-7 col-lg-7 col-md-7">
                                                <p id="account_no"></p>
                                            </div>
                                            
                                        </div>
                                        <div class="col-sm-6 col-lg-6 col-md-6">
                                            <div class="col-sm-5 col-lg-5 col-md-5">
                                                <strong>Address</strong>
                                            </div>
                                            <div class="col-sm-7 col-lg-7 col-md-7">
                                                <p id="address"></p>
                                            </div>
                                            <div class="col-sm-5 col-lg-5 col-md-5">
                                                <strong>IFSC</strong>
                                            </div>
                                            <div class="col-sm-7 col-lg-7 col-md-7">
                                                <p id="ifsc_code"></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 " style="padding: 0;">
                            <div class="panel panel-primary bank_info">
                                <div class="panel-body bg-ash">
                                    <div class="row">
                                        <div class="col-sm-6 col-lg-6 col-md-6">
                                            <div class="col-sm-7 col-lg-7 col-md-7">
                                                <strong>Saller Name</strong>
                                            </div>
                                            <div class="col-sm-5 col-lg-5 col-md-5">
                                                <p id="seller_name"></p>
                                            </div>
                                            
                                            <div class="col-sm-7 col-lg-7 col-md-7">
                                                <strong>Total Earning</strong>
                                            </div>
                                            <div class="col-sm-5 col-lg-5 col-md-5">
                                                <p id="total_earning"></p>
                                            </div>
                                            <div class="col-sm-7 col-lg-7 col-md-7">
                                                <strong>Total Commission</strong>
                                            </div>
                                            <div class="col-sm-5 col-lg-5 col-md-5">
                                                <p id="total_commission"></p>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-lg-6 col-md-6">
                                            <div class="col-sm-7 col-lg-7 col-md-7">
                                                <strong>Email</strong>
                                            </div>
                                            <div class="col-sm-5 col-lg-5 col-md-5">
                                                <p style="word-break: break-all;" id="seller_email"></p>
                                            </div>
                                            <div class="col-sm-7 col-lg-7 col-md-7">
                                                <strong>Total Payout</strong>
                                            </div>
                                            <div class="col-sm-5 col-lg-5 col-md-5">
                                                <p id="total_payout"></p>
                                            </div>
                                            <div class="col-sm-7 col-lg-7 col-md-7">
                                                <strong>Total Due</strong>
                                            </div>
                                            <div class="col-sm-5 col-lg-5 col-md-5">
                                                <p id="total_due"></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <form id="accept_form" style="display: none;" accept-charset="utf-8" action="{{ route('admin.accept.withdrawal.request') }}" method="post">
                    {{ csrf_field() }}
                    <input id="request_id" name="request_id" type="hidden" value="">
                        <textarea class="form-control" name="process_note" placeholder="Enter notes here.." rows="6"></textarea>
                        <br>
                            <button class="btn btn-success" type="submit">
                                Confirm Accept
                            </button>
                        </br>
                    </input>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="process_note" role="dialog">
    <div class="modal-dialog" style="margin: 100px auto;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" style="top: 0px; right: 0px;" type="button">
                    ×
                </button>
                <h4 class="modal-title">
                    Withdrawal rejection
                </h4>
            </div>
            <div class="modal-body">
                <form accept-charset="utf-8" action="{{ route('admin.reject.withdrawal.request') }}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="request_id" value="" id="request_id1">
                    <textarea class="form-control" name="process_note" placeholder="Enter rejection note" rows="6"></textarea>
                    <br>
                        <button class="btn btn-danger" type="submit">
                            Confirm Reject
                        </button>
                    </br>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" type="button">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>


<script>
    var resizefunc = [];
</script>

<script type="text/javascript">
    $(document).ready(function() {
    $('#datatable_length').hide();
        $('#datatable').dataTable({
            "ordering": false,
            "info":     false,
            "searching": false,
            "dom": '<"toolbar">frtip'
        });
    $(".check_check").click(function(){
    if($(this).is(":checked"))
    {
    $('.ch_1').show();
    }
    else
    {
    $('.ch_1').hide();
    }
    });
    
    } );
    
</script>
<script>
    $(function() {  
            $("#datepicker").datepicker({dateFormat: "dd-mm-yy",
               defaultDate: new Date(),
               onClose: function( selectedDate ) {
               $( "#datepicker1").datepicker( "option", "minDate", selectedDate );
              }
            }); 
        $("#datepicker1").datepicker({dateFormat: "dd-mm-yy",
         defaultDate: new Date(),
              onClose: function( selectedDate ) {
              $( "#datepicker" ).datepicker( "option", "maxDate", selectedDate );
            }
         }); 
            
        });
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection