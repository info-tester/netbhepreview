@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Add Blog Category')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Add 360º evaluation</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('tool-360-evaluation.index') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
				</div>
			</div>
			@include('admin.includes.error')
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
										
											<form id="myform" method="post" action="{{ route('tool-360-evaluation.store') }}">
											
											{{ csrf_field() }}
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Title *</label>
												<input type="text" name="title" class="form-control required" id="title" placeholder="Title" value="">
												</div>
											</div>

											<div id="fields">
												<div class="col-md-12 plus_row" id="fieldRow0">
													<div class="row">
														<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
															<div class="your-mail">
																<label for="exampleInputEmail1">Answer *</label>
																<input type="text" name="answer[]" class="form-control required" id="field_value0" placeholder="Enter your answer">
																
																<label for="exampleInputEmail1">Choose your answer color *</label>
																<input type="color" name="color_box[]" value="#ffffff" class="form-control required" id="field_value0" placeholder="Enter your answer">
															</div>
														</div>
														<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
															<button type="button" class="btn btn-success add-button addsingleselect" title="Add"><i class="fa fa-plus" aria-hidden="true"></i></button>
														</div>
													</div>
												</div>
												<div class="clearfix"></div>
											</div>
											<input type="hidden" class="countvalues" value="1">
											<div class="clearfix"></div>
											<br>
											
											
											<div class="clearfix"></div>
											<br>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="submit-login add_btnm">
													<input value="submit" type="submit" class="btn btn-default">
												</div>
											</div>
											<!--all_time_sho-->
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>


<script>
	$(document).ready(function(){		
		$('body').on('click','.addsingleselect',function(){
		var count = $('.countvalues').val();
			var singleselect = '<div class="col-md-12" id="fieldRow'+count+'"><div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">\
												<div class="your-mail">\
													<label for="exampleInputEmail1">Option *</label>\
													<input type="text" name="answer[]" class="form-control required" id="field_value'+count+'" placeholder="Enter your answer">\
													<label for="exampleInputEmail1">Choose your answer color *</label>\
													<input type="color" name="color_box[]" value="#ffffff" class="form-control required" id="field_value0" placeholder="Enter your answer">\
												</div>\
											</div>\
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">\
												<button type="button" class="btn btn-danger add-button removesingleselect" title="Remove" data-id="'+count+'"><i class="fa fa-minus" aria-hidden="true"></i></button>\
											</div></div></div><div class="clearfix"></div>';
			$('#fieldRow0').before(singleselect);								
		});
		$('body').on('click','.removesingleselect',function(){
			var id = $(this).data('id');
			$('#fieldRow'+id).remove();
		});
	});
</script>


<style>
	.error{
		color: red !important;
	}
</style>
<script type="text/javascript">
    $(document).ready(function(){
        $("#myform").validate({
        	ignore:[],
            rules  : {
                title                   : {required: true},
                answer                  :{
                    required: true,
                    
                  }
              }
            

        });

    });
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection