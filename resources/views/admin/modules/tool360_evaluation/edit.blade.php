@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Edit')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Edit 360º evaluation</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('tool-360-evaluation.index') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
				</div>
			</div>
			@include('admin.includes.error')
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
										
											<form id="myform" method="post" action="{{ route('tool-360-evaluation.update',['id'=>@$details->id]) }}">
											<input name="_method" type="hidden" value="PUT">
											{{ csrf_field() }}
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Title *</label>
												<input type="text" name="title" class="form-control required" id="title" placeholder="Title" value="{{ $details->title }}">
												</div>
											</div>

											<div id="fields">
												@php
													$totalanswer = count($details->get360Details);
												@endphp
												@foreach($details->get360Details as $key=>$val)
												@if(($key+1)==($totalanswer))
												<div class="col-md-12 plus_row" id="fieldRow0">
													<div class="row">
														<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
															<div class="your-mail">
																<label for="exampleInputEmail1">Answer *</label>
																<input type="text" name="answer[]" class="form-control required" id="field_value0" placeholder="Enter your answer" value="{{ $val['answer'] }}">
																<input type="color" name="color_box[]" class="form-control required" id="field_value0" placeholder="Enter your answer" value="{{ $val['color_box'] }}">
															</div>
														</div>
														<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
															<button type="button" class="btn btn-success add-button addsingleselect" title="Add"><i class="fa fa-plus" aria-hidden="true"></i></button>
														</div>
													</div>
												</div>
												<div class="clearfix"></div>
												@else
												<div class="col-md-12 plus_row" id="fieldRow{{ $key+1 }}">
													<div class="row">
														<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
															<div class="your-mail">
																<label for="exampleInputEmail1">Answer *</label>
																<input type="text" name="answer[]" class="form-control required" id="field_value0" placeholder="Enter your answer" value="{{ $val['answer'] }}">
																<input type="color" name="color_box[]" class="form-control required" id="field_value0" placeholder="Enter your answer" value="{{ $val['color_box'] }}">
															</div>
														</div>
														<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
															<button type="button" class="btn btn-danger add-button addsingleselect" title="Delete"><i class="fa fa-minus" aria-hidden="true"></i></button>
														</div>
													</div>
												</div>
												<div class="clearfix"></div>
												@endif
												@endforeach
											</div>
											<input type="hidden" class="countvalues" value="1">
											<div class="clearfix"></div>
											<br>
											
											
											<div class="clearfix"></div>
											<br>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="submit-login add_btnm">
													<input value="submit" type="submit" class="btn btn-default">
												</div>
											</div>
											<!--all_time_sho-->
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>


<script>
	$(document).ready(function(){		
		$('body').on('click','.addsingleselect',function(){
		var count = $('.countvalues').val();
			var singleselect = '<div class="col-md-12" id="fieldRow'+count+'"><div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">\
												<div class="your-mail">\
													<label for="exampleInputEmail1">Answer *</label>\
													<input type="text" name="answer[]" class="form-control required" id="field_value'+count+'" placeholder="Enter your answer">\
													<input type="color" name="color_box[]" class="form-control required" id="field_value0" placeholder="Enter your answer">\
												</div>\
											</div>\
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">\
												<button type="button" class="btn btn-danger add-button removesingleselect" title="Remove" data-id="'+count+'"><i class="fa fa-minus" aria-hidden="true"></i></button>\
											</div></div></div><div class="clearfix"></div>';
			$('#fieldRow0').before(singleselect);								
		});
		$('body').on('click','.removesingleselect',function(){
			var id = $(this).data('id');
			$('#fieldRow'+id).remove();
		});
	});
</script>


<style>
	.error{
		color: red !important;
	}
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection