@extends('admin.layouts.app')
@section('title', 'netbhe.com | Admin | 360Evaluation')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">{{ $title }}</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('tool-360-evaluation.create') }}" title="Create"><button type="button" class="btn btn-default">Add 360 evaluation</button></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 dess5">
									<i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
									<i class="fa fa-trash-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
									<i class="fa fa-eye cncl" style="border: none;" aria-hidden="true"> <span class="cncl_oopo">View Fields</span></i>
									<i class="fa fa-square cncl" aria-hidden="true"> <span class="cncl_oopo"> Not Show in All Professional</span></i>
                                    <i class="fa fa-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Show in All Professional</span></i>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Title</th>		
													<th>Added By</th>
													<th>Status</th>
													<th>Show in<br>Professional</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@if($all360data->isNotEmpty())
												@foreach(@$all360data as $detail)
												<tr>
													<td>{{ $detail['title'] }}</td>												
													<td>{{ $detail['added_by']=='A'?'Admin': ( @$detail->getUserData->nick_name ? @$detail->getUserData->nick_name : @$detail->getUserData->name ) }}</td>
													<td>{{ $detail['status'] }}</td>
													<td>
	                                                    @if(@$detail->shown_in_proff == 'N')
	                                                    <label class="label label-danger">{{ 'No' }}</label>
	                                                    @elseif(@$detail->shown_in_proff == 'Y')
	                                                    <label class="label label-success">{{ 'Yes' }}</label>
	                                                    @endif
	                                                </td>
													<td>
														<a href="{{ route('tool-360-evaluation.edit',['form_id'=>@$detail->id]) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
														
														<a href="{{ route('tool-360-evaluation.show',[@$detail->id]) }}" title="View Fields"> <i class="fa fa-eye delet" aria-hidden="true"></i></a>
														<a href="javascript:void(0)" onclick="deleteCategory('{{route('tool-360-evaluation.destroy',[$detail->id])}}')" title="Delete" class="rjct delbtn"><i class="fa fa-trash-o delet" aria-hidden="true"></i></a>

														
														@if($detail->shown_in_proff == 'Y')
	                                                        <a href="{{ route('admin.tools360.forproff',[$detail->id]) }}" title="Not Show in Professional Page" onclick="return confirm('Do you want to Not Show in Professional Page this ');"> <i class="fa fa-square delet" aria-hidden="true"></i></a>
	                                                    @else
	                                                        <a href="{{ route('admin.tools360.forproff',[$detail->id]) }}" title="Show in Professional Page " onclick="return confirm('Do you want to Show in Professional Page this ');"> <i class="fa fa-square-o delet" aria-hidden="true"></i></a>
	                                                    @endif
													</td>
												</tr>
												@endforeach
												
												@endif
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<form method="post" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
	var confirm = window.confirm('Are you want to delete this content ?');
	if(confirm){
		$("#destroy").attr('action',val);
		$("#destroy").submit();
	}
 }
</script>

@endsection
@section('footer')
@include('admin.includes.footer')
@endsection