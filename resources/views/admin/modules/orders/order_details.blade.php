@extends('admin.layouts.app')
@section('title', 'Netbhe.com| Admin | Booking Details')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">

    <div class="content">

        <div class="container">
            <div class="row">
                @include('admin.includes.error')
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Booking details of: {{ @$order->userDetails->nick_name ? @$order->userDetails->nick_name : @$order->userDetails->name }} with {{ @$order->profDetails->nick_name ? @$order->profDetails->nick_name : @$order->profDetails->name }} </h4>
                    <div class="submit-login no_mmg pull-right">
                        <a href="{{ route('admin.order') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="right_dash ml-auto">
                        <div class="information_box" style="    border: 1px solid #8f8f8f; padding: 5px 12px;">
                        <h4><span>  <strong>Booking Information </strong></span></h4>
                        <div class="information_area">
                            <div class="row">
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <label class="type_label1 ">User Name: </label>
                                        <label class="type_label2">{{ @$order->userDetails->nick_name ? @$order->userDetails->nick_name : @$order->userDetails->name }}</label>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <label class="type_label1 ">Professional Name: </label>
                                        <label class="type_label2">{{ @$order->profDetails->nick_name ? @$order->profDetails->nick_name : @$order->profDetails->name }}</label>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <label class="type_label1">Category: </label>
                                        <label class="type_label2">{{$order->parentCatDetails ? @$order->parentCatDetails->name: "N.A"}}</label>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <label class="type_label1">Sub Category: </label>
                                        <label class="type_label2">{{ @$order->childCatDetails ? @$order->childCatDetails->name: "N.A"}}</label>
                                    </div>
                                </div>
                                 <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <label class="type_label1">Token Number: </label>
                                        <label class="type_label2">{{@$order->token_no}}</label>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <label class="type_label1">Date: </label>
                                        <label class="type_label2">{{@$order->date}}</label>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <label class="type_label1">Duration: </label>
                                        <label class="type_label2">{{@$order->duration}} Minutes</label>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <label class="type_label1">Start time: </label>
                                        <label class="type_label2">{{date('h:i A',strtotime(@$order->start_time))}}</label>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <label class="type_label1">End time: </label>
                                        <label class="type_label2">{{date('h:i A',strtotime(@$order->end_time))}}</label>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <label class="type_label1">Total Amount: </label>
                                        <label class="type_label2"> ${{number_format((float)@$order->sub_total+@$order->extra_installment_charge, 2, '.', '')}}</label>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <label class="type_label1">Commision: </label>
                                        <label class="type_label2"> ${{@$order->payment->paymentDetails1->admin_commission>0 ? number_format((float)@@$order->payment->paymentDetails1->admin_commission+@$order->extra_installment_charge, 2, '.', ''): @$order->amount/2 }}</label>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <label class="type_label1">Professional earning: </label>
                                        <label class="type_label2"> ${{@$order->payment->paymentDetails1->professional_amount>0 ? @$order->payment->paymentDetails1->professional_amount : $order->amount/2 }}</label>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <label class="type_label1">Order Status: </label>
                                        <label class="type_label2">@if(@$order->video_status == 'I') Order Initiated @elseif(@$order->video_status == 'C') Order Completed @endif</label>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <label class="type_label1">Payment Status: </label>
                                        <label class="type_label2">@if(@$order->payment_status == 'I') Payment Initiated @elseif(@$order->payment_status == 'P') Paid @elseif(@$order->payment_status == 'F') Payment Failed @elseif(@$order->payment_status == 'PR') Payment Processing @endif </label>
                                        @if(@$order->payment_document && @$order->payment_status == 'PR')
                                        <a href="{{ URL::to('storage/app/public/Bank_Document/'. @$order->payment_document) }}" target="_blank">(document link)</a>
                                        <a href="{{route('admin.payment.approve',['slug'=>@$order->token_no]) }}"><i class="fa fa-check-circle cncl" aria-hidden="true"></i></a>
                                        <a href="{{route('admin.payment.reject',['slug'=>@$order->token_no]) }}"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
                                        @elseif(@$order->payment_status == 'PR')
                                        (No Document Upload)
                                        <a href="{{route('admin.payment.reject',['slug'=>@$order->token_no]) }}"><i class="fa fa-times-circle cncl" aria-hidden="true"></i></a>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <label class="type_label1">Payment Type: </label>
                                        <label class="type_label2">
                                            @if(@$order->payment_type == 'C' && $order->no_of_installment==1) Card Pament
                                            @elseif(@$order->payment_type == 'C' && $order->no_of_installment>1) Card Pament - installment({{$order->no_of_installment}})
                                            @elseif(@$order->payment_type == 'BA') Bank Account
                                            @elseif(@$order->payment_type == 'S') Stripe
                                            @elseif(@$order->payment_type == 'P') Paypal
                                            @elseif(@$order->wallet==@$order->sub_total) Wallet
                                            @endif
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <label class="type_label1">Total Payable Amount: </label>
                                        <label class="type_label2"> ${{ @$order->sub_total }}</label>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <label class="type_label1">Booking Type: </label>
                                        <label class="type_label2">@if(@$order->booking_type == 'C') Chat @elseif(@$order->booking_type == 'V') Video call @endif</label>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <label class="type_label1">Payment TXN ID <i class="fa fa-info-circle" title="This is Wirecard Payment ID" data-toggle="tooltip"></i> : </label>
                                        <label class="type_label2">{{ @$order->payment->payment_id }}</label>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <label class="type_label1">Transfer ID <i class="fa fa-info-circle" title="This is Wirecard Transfer ID" data-toggle="tooltip"></i>: </label>
                                        <label class="type_label2">{{ @$order->payment->wirecard_transfer_id }}</label>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="type_label1">Message: </label>
                                        <label class="type_label2">{!!strip_tags(@$order->msg, '<br>')!!}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="information_box">
                        {{-- <h4><span> Booking Details</span></h4> --}}
                        <div class="information_area">
                            @if($message->isNotEmpty())
                                <div class="chattt">
                                  @foreach($message as $row1)
                                    @if($row1->sender_id == $isMaster)
                                      <div class="converse_me">
                                        <img src="http://crm.spio.it/public/assets/dist/img/avatar4.png" alt="">
                                        <p id="me_text" style="white-space: pre-wrap;">{!!strip_tags($row1->msg, '<br>') !!}</p>
                                        <div class="clearfix"></div>
                                        @if(@$row1->attachment)
                                            <strong class="show_attachment">
                                              <a href="{{ URL::to('public/ticket/'.@$row1->getAttachment->attachment) }}" download="_new" title="Download">{{@$row1->attachment}}</a> <i class="fa fa-download" aria-hidden="true" title="Download" ></i>
                                            </strong>
                                        @endif
                                        <span class="show_name_date"> {{ date('jS M Y', strtotime($row1->created_at)) }}</span>
                                      </div>
                                    @elseif($row1->sender_id !== $isMaster)
                                      <div class="converse_you">
                                        <img src="http://crm.spio.it/public/assets/dist/img/avatar5.png" alt="">
                                          <p id="" style="white-space: pre-wrap;">{!! strip_tags($row1->msg, '<br>') !!}</p>
                                          <div class="clearfix"></div>
                                          @if(@$row1->attachment)
                                              <strong class="show_attachment">
                                                <img src="{{URL::to('public/ticket').'/'.@$row1->attachment}}"> <i class="fa fa-download" aria-hidden="true" title="Download"></i>
                                            </strong>
                                          @endif
                                        <span class="show_name_date"> {{ date('jS M Y', strtotime($row1->created_at)) }}</span>
                                      </div>
                                    @endif
                                  @endforeach
                                </div>
                            @endif
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .converse_me p {
    float: right;
    padding: 6px 7px;
    color: #fff;
    max-width: 84%;
    margin: 0;
    background: #26c6da;
    overflow: hidden;
    border-radius: 3px;
}
.chattt {
    width: 100%;
    float: left;
    border: 1px solid #8f8f8f;
    padding: 12px;
    margin: 15px 0;
}
.converse_you {
    float: right;
    width: 100%;
    padding: 5px 0 4px;
    margin-top: 12px;
}
.converse_you img {
    float: left;
    width: 30px;
    height: 30px;
    margin-right: 5px;
    border-radius: 50%;
}
.converse_you p {
    padding: 6px 7px;
    color: #353535;
    text-align: left;
    float: left;
    background: #eaeaea;
    max-width: 84%;
    border-radius: 3px;
    margin: 0;
}
.converse_you span {
    font-weight: 400;
    color: #9b939e;
    text-align: left;
    width: 100%;
    padding: 4px 0 0 36px;
    float: left;
    font-size: 11px;
}
.converse_me {
    float: left;
    width: 100%;
    padding: 5px 0 4px;
}
.converse_me img {
    float: right;
    width: 30px;
    height: 30px;
    margin-left: 5px;
    border-radius: 50%;
}
.converse_me span {
    float: right;
    font-size: 11px;
    font-weight: 400;
    color: #9b939e;
    width: 100%;
    text-align: right;
    padding: 4px 36px 0 0;
}
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
