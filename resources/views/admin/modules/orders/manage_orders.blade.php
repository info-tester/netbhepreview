@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Admin | Bookings')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- Begin page -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="pull-left page-title">Manage Bookings</h4>

                        <!--<ol class="breadcrumb pull-right">
                            <li><a href="#">User Dashboard</a></li>
                            <li class="active">My Order</li>
                            </ol>-->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <!--<div class="panel-heading">
                                <h3 class="panel-title">Default Example</h3>
                                </div>-->
                            <div class="panel-body table-rep-plugin">
                                <div class="row top_from">
                                    <form method="get" action="{{ route('admin.order')}}">
                                        {{-- <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">Service</label>
                                                <select name="service" class="form-control newdrop">
                                                    <option value="">Choose Service</option>
                                                    <option value="2" @if(@$key['service'] == 2) {{ "selected" }} @endif>Call</option>
                                                    <option value="1" @if(@$key['service'] == 1) {{ "selected" }} @endif>Chat</option>
                                                    <option value="5" @if(@$key['service'] == 5) {{ "selected" }} @endif>Pooja</option>
                                                    <option value="4" @if(@$key['service'] == 4) {{ "selected" }} @endif>Question</option>
                                                    <option value="3" @if(@$key['service'] == 3) {{ "selected" }} @endif>Report</option>

                                                </select>
                                            </div>
                                        </div> --}}



                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">Token Number</label>
                                                <input type="text" class="form-control" name="token_no" id="token_no" value="{{@$key['token_no']}}" placeholder="Token number">
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">Professional Name</label>
                                                <select name="professional" id="professional" class="form-control newdrop">
                                                    <option value="">Choose Professional</option>
                                                    @foreach($expert as $astr)
                                                    <option value="{{ @$astr->id }}" @if(@$key['professional'] == $astr->id) {{ "selected" }} @endif>{{ @$astr->nick_name ? @$astr->nick_name : @$astr->name}}</option>

                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">Customer Name</label>
                                                <input type="text" class="form-control" name="user_name" id="user_name" value="{{@$key['user_name']}}" placeholder="User name">
                                            </div>
                                        </div>


                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">From Date</label>
                                                <input type="text" class="form-control" name="from_date" id="datepicker" value="{{@$key['from_date']}}" placeholder="From Date">
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">To Date</label>
                                                <input type="text" class="form-control" name="to_date" id="datepicker1" value="{{@$key['to_date']}}" placeholder="To Date">
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">Status</label>
                                                <select name="order_status" class="form-control newdrop">
                                                    <option value="">Choose Status</option>
                                                    <option value="AA" @if(@$key['order_status'] == "AA") {{ "selected" }} @endif>Awaiting Approval</option>
                                                    <option value="A" @if(@$key['order_status'] == "A") {{ "selected" }} @endif>Approved</option>
                                                    <option value="R" @if(@$key['order_status'] == "R") {{ "selected" }} @endif>Rejected</option>

                                                    <option value="P" @if(@$key['order_status'] == "P") {{ "selected" }} @endif>Complete</option>
                                                    <option value="C" @if(@$key['order_status'] == "C") {{ "selected" }} @endif>Cancel</option>

                                                </select>
                                            </div>
                                        </div>

                                        {{-- <div class="clearfix"></div> --}}
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="submit-login add_btnm {{-- pull-right --}}" style="width:auto; margin-top:35px;">
                                                <input value="Search" id="search" class="btn btn-default" type="submit">
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 dess5">
                                        <i class="fa fa-eye cncl" aria-hidden="true"> <span class="cncl_oopo">View</span></i>
                                        <i class="fa fa-money cncl" aria-hidden="true"> <span class="cncl_oopo">Transfer Money </span></i>
                                        <i class="fa fa-check-circle-o cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">Video call status change</span></i>

                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" data-pattern="priority-columns">
                                            <table id="datatable" class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Order No.</th>
                                                        <!-- <th>Wirecard Order ID</th> -->
                                                        <th>Date </th>
                                                        <th>Professional</th>
                                                        <th>Customer</th>
                                                        <th>Total </th>
                                                        <th>Payment Type </th>
                                                        <th>Payment Status </th>
                                                        <th>Booking Type </th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($order as $row)
                                                    <tr>
                                                        <td><span class="dateee">
                                                            {{ $row->token_no }}
                                                        </td>
                                                        <!-- <td>{{ @$row->moip_order_id }}</td> -->
                                                        <td>{{ @$row->date ? date('jS-M-Y', strtotime($row->date)) : '' }}</td>
                                                        <td>{{ @$row->profDetails->nick_name ? @$row->profDetails->nick_name : @$row->profDetails->name }}</td>
                                                        <td>{{ @$row->userDetails->nick_name ? @$row->userDetails->nick_name : @$row->userDetails->name }}</td>

                                                        <td>
                                                            ${{@$row->amount}}
                                                        </td>
                                                        <td>
                                                            @if(@$row->payment_type=='C' && @$row->no_of_installment==1)
                                                            Card
                                                            @elseif(@$row->payment_type=='C' && @$row->no_of_installment>1)
                                                            Installment
                                                            @elseif(@$row->payment_type=='BA')
                                                            Bank Account
                                                            @elseif(@$row->payment_type=='S')
                                                            Stripe
                                                            @elseif(@$row->payment_type=='P')
                                                            Paypal
                                                            @elseif(@$row->wallet==@$row->sub_total)
                                                            Wallet
                                                            @elseif(@$row->amount==0)
                                                            Free Session
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if(@$row->payment_status == 'I') Payment Initiated @elseif(@$row->payment_status == 'P') Paid
                                                            @elseif(@$row->payment_status == 'F') Payment Failed @elseif(@$row->payment_status == 'PR') Payment Processing
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if(@$row->booking_type=='C')
                                                            Chat
                                                            @elseif(@$row->booking_type=='V')
                                                            Video call
                                                            @endif
                                                        </td>

                                                        <td>
                                                    @php
                                                        @$status = [
                                                            'AA' =>  'Awaitnig Approval',
                                                            'A'  =>  'Approved',
                                                            'R'  =>  'Reject',
                                                            'P'  =>  'Completed',
                                                            'C'  =>'Cancel'
                                                        ];
                                                    @endphp
                                                        {{@$row->video_status=="C" ? "Completed": @$status[@$row->order_status]}}
                                                        </td>
                                                        <td>
                                                            <a href="{{route('admin.order.details',[$row->token_no])}}"  title="View"><i class="fa fa-eye delet" style="cursor: pointer;" aria-hidden="true"></i></a>
                                                            @if(@$row->order_status != 'C')
                                                            @if(@$row->payment->paymentDetails1->balance_status == "R" && @$row->payment_status == 'P' && @$row->profDetails->professional_access_token && @$row->payment->paymentDetails1->withdraw_by_wirecard =='Y' )

                                                                <a href="javascript:void(0);" onclick="submitForm({{@$row->payment->paymentDetails1->professional_amount}},'{{@$row->profDetails->nick_name ? @$row->profDetails->nick_name : @$row->profDetails->name}}', '{{@$row->payment->token_no}}','{{@$row->payment->paymentDetails1->id}}');"  title="Transfer Money "><i style="cursor: pointer;" class="fa fa-money delet" aria-hidden="true"></i></a>

                                                            {{-- @endif --}}
                                                            @elseif(@$row->payment->paymentDetails1->balance_status == "R" && @$row->payment_status == 'P' && @$row->profDetails->professional_access_token && @$row->payment->paymentDetails1->withdraw_by_wirecard =='N')
                                                            <a href="javascript:void(0);" onclick="prof_account_info({{@$row->payment->paymentDetails1->professional_amount}},'{{@$row->profDetails->nick_name ?@$row->profDetails->nick_name : @$row->profDetails->name}}', '{{@$row->payment->token_no}}', '{{@$row->profDetails->bankAccount}}', '{{@$row->profDetails->bankAccount->bankName}}', '{{@$row->profDetails->bankAccount->accountNumber}}', '{{@$row->profDetails->bankAccount->agencyNumber}}', '{{@$row->profDetails->bankAccount->agencyCheckNumber}}', '{{@$row->profDetails->bankAccount->bank_number}}','{{@$row->payment->paymentDetails1->id}}');"
                                                                    title="Transfer Money "><i style="cursor: pointer;" class="fa fa-money delet" aria-hidden="true"></i></a>

                                                            {{-- @endif --}}

                                                            @elseif(@$row->payment->paymentDetails1->balance_status == "R" && @$row->payment_status == 'P' && @$row->profDetails->paypal_address && @$row->payment->paymentDetails1->withdraw_by_wirecard =='N')
                                                            <a href="javascript:void(0);" onclick="prof_account_info1({{@$row->payment->paymentDetails1->professional_amount}},'{{@$row->profDetails->nick_name ?@$row->profDetails->nick_name : @$row->profDetails->name}}', '{{@$row->payment->token_no}}', '{{@$row->profDetails->paypal_address}}', '{{@$row->payment->paymentDetails1->id}}');"
                                                                    title="Transfer Money "><i style="cursor: pointer;" class="fa fa-money delet" aria-hidden="true"></i></a>

                                                            @endif

                                                            @if(@$row->is_video_started == "Y" && $row->video_status == 'I')
                                                                <a href="{{ route('admin.order.video.callcharge', ['token_no'=>@$row->token_no]) }}" onclick="return confirm('Are you really want to change the status?')" title="Video call status change">
                                                                    <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                                                </a>
                                                            @endif
                                                            @endif

                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                                 <form method="post" action="{{route('admin.make.transfer')}}" id="myForm" name="myForm">
                                                        <input type="hidden" name="no" id="no" value="">
                                                        <input type="hidden" name="id" id="id" value="">
                                                        @csrf
                                                </form>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Row -->
            </div>
            <!-- container -->
        </div>
        <!-- content -->
        <!--<footer class="footer text-right">
            2015 © Moltran.
            </footer>-->
    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->
    <!-- Right Sidebar -->
    {{-- <div class="side-bar right-bar nicescroll">
        <h4 class="text-center">Chat</h4>
        <div class="contact-list nicescroll">
            <ul class="list-group contacts-list">
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-1.jpg" alt="">
                        </div>
                        <span class="name">Chadengle</span>
                        <i class="fa fa-circle online"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-2.jpg" alt="">
                        </div>
                        <span class="name">Tomaslau</span>
                        <i class="fa fa-circle online"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-3.jpg" alt="">
                        </div>
                        <span class="name">Stillnotdavid</span>
                        <i class="fa fa-circle online"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-4.jpg" alt="">
                        </div>
                        <span class="name">Kurafire</span>
                        <i class="fa fa-circle online"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-5.jpg" alt="">
                        </div>
                        <span class="name">Shahedk</span>
                        <i class="fa fa-circle away"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-6.jpg" alt="">
                        </div>
                        <span class="name">Adhamdannaway</span>
                        <i class="fa fa-circle away"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-7.jpg" alt="">
                        </div>
                        <span class="name">Ok</span>
                        <i class="fa fa-circle away"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-8.jpg" alt="">
                        </div>
                        <span class="name">Arashasghari</span>
                        <i class="fa fa-circle offline"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-9.jpg" alt="">
                        </div>
                        <span class="name">Joshaustin</span>
                        <i class="fa fa-circle offline"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-10.jpg" alt="">
                        </div>
                        <span class="name">Sortino</span>
                        <i class="fa fa-circle offline"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
            </ul>
        </div>
    </div> --}}
    <!-- /Right-bar -->
</div>
<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Transfer the amount to the following bank account and then enter a note for the professional</h4>
                {{-- Transfer the amount to the following bank account and then enter a note for the professional --}}
            </div>
            <div class="modal-body" style="padding: 1rem;">
                <p id="professional_name"></p>
                <p id="payment_amount"></p>
                <p id="bankName"></p>
                <p id="accountNumber"></p>
                <p id="agencyNumber"></p>
                <p id="agencyCheckNumber"></p>
                <p id="bank_number"></p>
                <form action="{{route('admin.make.transfer.bank')}}" method="post" id="updateeventsform2" class="form-horizontal">
                    <input type="hidden" name="payment_token_no" id="payment_token_no" value="">
                    <input type="hidden" name="paymentId" id="paymentId" value="">
                    @csrf
                        <label for='areaforinfo'>Note </label>

                            <textarea class="form-control required" id='areaforinfo' rows="4" style="min-width: 100%; border: 1px solid #CCC;"  name="note"></textarea>
                    <div class="submit-login add_btnm " style="width:auto;margin-top:10px;">
                    <button type="submit" class="btn btn-default">Payment Confirmation </button>
                    </div>
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="myModal1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Transfer the amount to the following paypal address and then enter a note for the professional</h4>
                {{-- Transfer the amount to the following bank account and then enter a note for the professional --}}
            </div>
            <div class="modal-body" style="padding: 1rem;">
                <p id="professional_name1"></p>
                <p id="payment_amount1"></p>
                <p id="paypal"></p>
                <form action="{{route('admin.make.transfer.paypal')}}" method="post" id="updateeventsform3"
                    class="form-horizontal">
                    <input type="hidden" name="payment_token_no" id="payment_token_no1" value="">
                    <input type="hidden" name="paymentId" id="paymentId1" value="">
                    @csrf
                    <label for='areaforinfo'>Note </label>

                    <textarea class="form-control required" id='areaforinfo' rows="4"
                        style="min-width: 100%; border: 1px solid #CCC;" name="note"></textarea>
                    <div class="submit-login add_btnm " style="width:auto;margin-top:10px;">
                        <button type="submit" class="btn btn-default">Payment Confirmation </button>
                    </div>
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
<!-- END wrapper -->
<!--Raise  popup-->
{{-- <div class="container">
    <div class="custom_popup pop1" style="display:none;">
        <button type="button" class="close lg_csl" data-dismiss="modal" aria-hidden="true">
        <img src="image1/close_btn.png" alt=""></button>
        <div class="pop_from_area">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="your-mail">
                    <label for="exampleInputEmail1">Vendor</label>
                    <select class="form-control newdrop">
                        <option>Choose</option>
                        <option value="volvo">Vendor 1</option>
                        <option value="saab">Vendor 2</option>
                        <option value="volvo">Vendor 3</option>
                        <option value="saab">Vendor 4</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="your-mail">
                    <label for="exampleInputEmail1">Issue Category</label>
                    <select class="form-control newdrop">
                        <option value="volvo">Choose</option>
                        <option value="saab">Category 1</option>
                        <option value="mercedes">Category 2</option>
                        <option value="saab">Category 3</option>
                        <option value="mercedes">Category 4</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="your-mail">
                    <label for="exampleInputEmail1">Description</label>
                    <textarea class="form-control des" rows="3"></textarea>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="submit-login no_mmg">
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div> --}}
<!--Raise  popup-->
<script>
    var resizefunc = [];
</script>

<script type="text/javascript">
    $(document).ready(function() {
    $('#datatable_length').hide();
        $('#datatable').dataTable({
    "ordering": false,
    "info":     false,
    "searching": false,
    "dom": '<"toolbar">frtip'
    });
    $(".check_check").click(function(){
    if($(this).is(":checked"))
    {
    $('.ch_1').show();
    }
    else
    {
    $('.ch_1').hide();
    }
    });
    //$("div.toolbar").html('<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 status">Status: <select name="" class="newdrop"><option value="1">Active</option><option value="1">Inactive</option></select></div><div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 clndr">From date : <input type="text" name="from" id="frm_date"> </div><div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 clndr"> To date : <input type="text" name="to" id="to_date"> </div><div class="col-md-4 col-lg-4 clndr">Keyword: <input type="text" name="abc"></div><div class="col-md-4 col-lg-4 col-sm-4 colxs-12 srch"><input type="submit" name="submit" value="Search"> </div>');
       // $('#datatable-keytable').DataTable( { keys: true } );
       //$('#datatable-responsive').DataTable();
        //$('#datatable-scroller').DataTable( { ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
        //var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } );
    } );
    // TableManageButtons.init();
</script>
<script>
    $(function() {
        $("#datepicker").datepicker({dateFormat: "dd-mm-yy",
           defaultDate: new Date(),
           onClose: function( selectedDate ) {
           $( "#datepicker1").datepicker( "option", "minDate", selectedDate );
          }
        });

        $("#datepicker1").datepicker({dateFormat: "dd-mm-yy",
         defaultDate: new Date(),
              onClose: function( selectedDate ) {
              $( "#datepicker" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
<script>
    function submitForm(amount, name, token,id){
        if(confirm('The amount '+amount+' will be transferred on '+name+'\'s account')){
            $('#id').val(id);
            $('#no').val(token);
            $('#myForm').submit();
        }
        else{
            return false;
        }
    }
    function prof_account_info(amount, name, token, prof_id, bankName ,accountNumber ,agencyNumber,agencyCheckNumber,bank_number,id){
        console.log(token)
            // $('#amount').val(amount);
            $('#payment_token_no').val(token);
            $('#paymentId').val(id);
            $('#professional_name').html("Professional Name : "+name);
            $('#payment_amount').html("Payment Amount : "+amount);
            $('#bankName').html("Bank Name : "+bankName);
            $('#accountNumber').html("Account Number : "+accountNumber);
            $('#agencyNumber').html("Agency Number : "+agencyNumber);
            $('#agencyCheckNumber').html("Agency Check Number : "+agencyCheckNumber);
            $('#bank_number').html("Bank Number : "+bank_number);
            $('#myModal').modal('show');
    }
    function prof_account_info1(amount, name, token, paypal,id){
            console.log(token)
                // $('#amount').val(amount);
                $('#payment_token_no1').val(token);
                $('#paymentId1').val(id);
                $('#professional_name1').html("Professional Name : "+name);
                $('#payment_amount1').html("Payment Amount : "+amount);
                $('#paypal').html("Paypal Address : "+paypal);
                $('#myModal1').modal('show');
        }
</script>
<script>
    $(document).ready(function(){
		$("#updateeventsform2").validate({});
        $("#updateeventsform3").validate({});
	});
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
