@extends('admin.layout.auth')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- Begin page -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->                      
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="pull-left page-title">Messages</h4>
                        
                        <!--<ol class="breadcrumb pull-right">
                            <li><a href="#">User Dashboard</a></li>
                            <li class="active">My Order</li>
                            </ol>-->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <!--<div class="panel-heading">
                                <h3 class="panel-title">Default Example</h3>
                                </div>-->
                            <div class="panel-body table-rep-plugin">
                               
                                    
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="msg_board">
                                        <div class="msg_contain">
                                            
                                            @foreach($messages as $row)
                                                {{-- {{dump(@Auth::user()->id)}} --}}
                                                @if(@$row->from_user_id)
                                                <div class="msg_row">
                                                    <div class="floatL">
                                                        <div class="msg_img">
                                                            {{$row->messagingPerson->first_name}} :
                                                         </div>
                                                           
                                                        <div class="msg_ccontains">{{ $row->message }}</div>
                                                    </div>
                                                </div>
                                               {{--  @elseif(@$row->from_user_id)
                                                <div class="msg_row">
                                                    <div class="floatR">
                                                        <div class="msg_ccontains_1">{{ $row->message }}</div>
                                                        <div class="msg_img"> <img src="public/images/msg_img.jpg" /></div>
                                                    </div>
                                                </div> --}}
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Row -->
            </div>
            <!-- container -->
        </div>
        <!-- content -->
        <!--<footer class="footer text-right">
            2015 © Moltran.
            </footer>-->
    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->
    <!-- Right Sidebar -->
   
    <!-- /Right-bar -->
</div>
<!-- END wrapper -->
<!--Raise  popup-->

<!--Raise  popup-->
<script>
    var resizefunc = [];
</script>

@endsection
@section('footer')
@include('admin.includes.footer')
@endsection