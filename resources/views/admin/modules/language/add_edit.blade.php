@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Add new logbook')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">{{ @$details ? 'Update Language' : 'Add new Language'}}</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('manage.language') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
				</div>
			</div>
			@include('admin.includes.error')
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">

											<form id="myform" method="post" action="{{ @$details ? route('manage.languages.edit.update',['id'=>@$details->id]) :route('manage.language.add.store') }}" enctype="multipart/form-data">
                                                @csrf

											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
                                                <div id="fields">
                                                    <div class="col-md-12 plus_row" id="fieldRow0">
                                                        <div class="row">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="your-mail">
                                                                    <label for="exampleInputEmail1">Language name *</label>
                                                                    <input type="text" name="language_name" class="personal-type required" id="field_value0" placeholder="Enter Language name" value="{{@$details->name}}">
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="margin-top: 10px;">
												<div class="submit-login add_btnm">
													<input value="submit" type="submit" class="btn btn-default">
												</div>
											</div>
											<!--all_time_sho-->
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>
<script>
	$(document).ready(function(){
		$("#myform").validate({});
	});
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
