@extends('admin.layouts.app')
@section('title', 'netbhe.com | Admin | Logbook')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h3 class="pull-left page-title">Manage language</h3>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('manage.language.add') }}" title="Create"><button type="button" class="btn btn-default">Add language</button></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 dess5">
									<i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>language Name</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@if(@$languages->isNotEmpty())
												@foreach(@$languages as $detail)
												<tr>
													<td>{{ $detail['name'] }}</td>
													<td>
														<a href="{{ route('manage.languages.edit',['id'=>@$detail->id]) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
													</td>
												</tr>
												@endforeach

												@endif
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<form method="post" id="destroy">
    {{ csrf_field() }}
    {{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
    var confirm = window.confirm('Do you want to delete this logbook tools?');
    if(confirm){
        $("#destroy").attr('action',val);
        $("#destroy").submit();
    }
 }
</script>

@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
