@extends('admin.layouts.app')
@section('title', 'NearO | Admin | Product Details')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->                      
<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="pull-left page-title">Product Detail</h4>
          <div class="submit-login no_mmg pull-right">
            <a href="{{ URL::previous() }}" title="Create"><button type="button" class="btn btn-default">Back</button></a>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-body">
              <div class="row">
                <div class="col-lg-12 col-md-12">
                  <h4 class="pull-left page-title">Basic Info</h4>
                  <div class="order-detail">
                    <!--order-detail start-->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <div class="order-id">
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Product Name :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7"><span>{{ @$product->name ? $product->name : 'N.A' }}</span></div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Price Start :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7"><strong> ${{ @$product->starting_price }} / {{ @$product->starting_quantity }} {{ @$product->units->name }}</strong></div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Parent Category :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7"><strong>
                          {{ @$product->category->name ? $product->category->name : 'N.A' }} {{-- {{ @$product->parent_cat ? $product->parent_cat : 'N.A' }} --}}</strong></div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Sub Category :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7"><strong>
                          {{ @$product->subCategory->name ? $product->subCategory->name : 'N.A' }}</strong></div>
                          
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Created On :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7"><strong>
                          @php
                          $created_on = date("dS F Y", strtotime(@$product->created_at));
                          @endphp
                          {{ @$created_on ? $created_on : 'N.A' }}</strong></div>
                      </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <div class="order-id">
                         <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Status:</p>
                        </div>
                        <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5"><strong> 
                          @if(@$product->status == 'A')
                          {{ 'Active' }}
                          @elseif(@$product->status == 'IA')
                          {{ 'Inactive by admin' }}
                          @elseif(@$product->status == 'IS')
                          {{ 'Inactive by seller' }}
                          @elseif(@$product->status == 'AA')
                          {{ 'Awaiting Approval' }}
                          @endif
                          </strong></div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                  <!--order-detail end-->
                </div>
                <div class="col-md-12">
                  <!--<i class="fa fa-pencil-square-o cncl" aria-hidden="true"> Edit</i> -->
                  <!--<i class="fa fa-pencil cncl" aria-hidden="true"> Review</i>-->
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12">
                  <h4 class="pull-left page-title">Price List</h4>
                  <div class="order-detail">
                    <!--order-detail start-->
                    <div class="col-lg-12">
                      <div class="order-id">
                        <div class="col-lg-12">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="table-responsive" data-pattern="priority-columns">
                              <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                  <tr>
                                    <th>Original Price</th>
                                    <th>Discounted Price</th>
                                    <th>Quentity</th>
                                    <th>Unit</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  @foreach($product->unitPrice as $detail)
                                  <tr>
                                    <td>{{ @$detail->price ? $detail->price : 'N.A' }}</td>
                                    <td>{{ @$detail->discount_price ? $detail->discount_price : 'N.A' }}</td>
                                    <td>{{ @$detail->quantity ? $detail->quantity : 'N.A' }}</td>
                                    <td>{{ @$detail->unitName->name ? $detail->unitName->name : 'N.A' }}</td>
                                  </tr>
                                  @endforeach
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                  <!--order-detail end-->
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12">
                  <h4 class="pull-left page-title">Description</h4>
                  <div class="order-detail">
                    <!--order-detail start-->
                    <div class="col-lg-12">
                      <div class="order-id">
                        <div class="col-lg-12">
                          <span>{!! @$product->description ? $product->description : 'N.A' !!}</span>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                  <!--order-detail end-->
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12">
                <h4 class="pull-left page-title">Images</h4>
                  <div class="order-detail">
                    <!--order-detail start-->
                    <div class="col-lg-12">
                      <div class="order-id">
                          <div class="img-list">
                            <ul>
                              @foreach(@$product->imgs as $row)
                              <li><img src="{{ url('public/uploads/products/thumbs/'.@$row->image) }}"></li>
                              @endforeach
                            </ul>
                          </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                  <!--order-detail end-->
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End Row -->
  </div>
  <!-- container -->
</div>
<!-- content -->
<footer class="footer text-right">
  2018 &copy; ProQRX.
</footer>
</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
<!-- Right Sidebar -->
<!-- /Right-bar -->
</div>
<!-- END wrapper -->
{{-- <script>
  var resizefunc = [];
</script> --}}
<!-- jQuery  -->
<!-- Datatables-->
<!--<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>-->
<!-- Datatable init js -->
{{-- <script type="text/javascript">
  $(document).ready(function() {
      $('#datatable').dataTable({
           "ordering": false,
              "info":     false,
              "searching": false,
               "dom": '<"toolbar">frtip'
          });
      $('#datatable-keytable').DataTable( { keys: true } );
      $('#datatable-responsive').DataTable();
      $('#datatable-scroller').DataTable( { ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
      var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } );
  } );
  TableManageButtons.init();
</script> --}}
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection