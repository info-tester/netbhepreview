@extends('admin.layout.auth')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="pull-left page-title">Seller Product Detail</h4>
          <div class="submit-login no_mmg pull-right">
            <a href="{{ URL::previous() }}" title="Create"><button type="button" class="btn btn-default">Back</button></a>
          </div>
          <!--<ol class="breadcrumb pull-right">
            <li><a href="#">User Dashboard</a></li>
            <li class="active">Order Detail</li>
            </ol>-->
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-default">
            <!--<div class="panel-heading">
              <h3 class="panel-title">Default Example</h3>
              </div>-->
            <div class="panel-body">
              <div class="row">
                <div class="col-lg-12 col-md-12">
                  <h4 class="pull-left page-title">Basic Info</h4>
                  <div class="order-detail">
                    <!--order-detail start-->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <div class="order-id">
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Product Name :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <span>{{ @$showproducts->ProductName->name ? $showproducts->ProductName->name : 'N.A' }}</span>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Seller Name :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <span>{{ @$showproducts->SellerName->first_name ? $showproducts->SellerName->first_name : 'N.A' }} {{ @$showproducts->SellerName->last_name ? $showproducts->SellerName->last_name : 'N.A' }}</span>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>CAS No :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <strong>{{ @$showproducts->ProductName->cas_no ? $showproducts->ProductName->cas_no : 'N.A' }}</strong>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>HSN No :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <strong>{{ @$showproducts->ProductName->hsn_no ? $showproducts->ProductName->hsn_no : 'N.A' }}</strong>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Price INR :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <strong>{{ env('RUPPY') }}{{ @$showproducts->price_inr }} / @if(@$showproducts->unit_id == 1) KG @elseif(@$showproducts->unit_id == 2) Metric Ton @endif</strong>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Price USD :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <strong>{{ env('CURRENCY') }}{{ @$showproducts->price_usd }} / @if(@$showproducts->unit_id == 1) KG @elseif(@$showproducts->unit_id == 2) Metric Ton @endif</strong>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Concentration :</p>
                        </div>
                        <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5">
                          <strong>{{ @$showproducts->concentration ? $showproducts->concentration : 'N.A'}}</strong>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Chemical Grade :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <strong>{{ @$showproducts->chemical_grade_id ? STATIC_ARRAY['chemical_grade'][$showproducts->chemical_grade_id] : 'N.A' }}</strong>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Certification :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <strong>
                            @if(count($showproducts->sellerProductCertification) > 0)
                              @php 
                                $certification = [];
                                foreach(@$showproducts->sellerProductCertification as $row) {
                                  $certification[] = $row->certificate_id;
                                }
                                echo removeLastComma($certification, STATIC_ARRAY['certification']);
                              @endphp
                            @else
                              {{ 'N.A' }}
                            @endif
                          </strong>
                        </div>
                          
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Created On :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7"><strong>
                          @php
                          $created_on=date("d/m/Y", strtotime(@$showproducts->created_at));
                          @endphp
                          {{ @$created_on ? $created_on : 'N.A' }}</strong></div>
                      </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <div class="order-id">
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Parent Category :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <strong>{{ @$showproducts->ProductName->parent_category->category_name->cat_name ? $showproducts->ProductName->parent_category->category_name->cat_name : 'N.A' }}</strong>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Sub Category :</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                          <strong>{{ @$showproducts->ProductName->category->category_name->cat_name ? $showproducts->ProductName->category->category_name->cat_name : 'N.A' }}</strong>
                        </div>
                        <div class="clearfix"></div>
                        {{-- <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Concentration :</p>
                        </div>
                        <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5">
                          <strong>{{ @$showproducts->concentration ? $showproducts->concentration : 'N.A'}}</strong>
                        </div>
                        <div class="clearfix"></div> --}}
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Selling Type :</p>
                        </div>
                        <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5">
                          <strong>
                            @php 
                              $selling_type = json_decode(@$showproducts->selling_type, true);
                              echo removeLastComma($selling_type, STATIC_ARRAY['selling_type']);
                              /*$length = count($selling_type);
                              $i = 1;
                              foreach(@$selling_type as $row) {
                                echo STATIC_ARRAY['selling_type'][$row];
                                if($i < $length) {
                                  echo ", ";
                                  $i++;
                                }
                              }*/
                            @endphp
                         </strong>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Credit Period :</p>
                        </div>
                        <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5">
                          <strong>{{ @$showproducts->credit_period ? STATIC_ARRAY['credit_period'][$showproducts->credit_period] : 'N.A' }}</strong>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Margin Offered :</p>
                        </div>
                        <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5">
                          <strong>{{ @$showproducts->margin_offered ? $showproducts->margin_offered.' %' : 'N.A' }}</strong>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>CGST :</p>
                        </div>
                        <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5">
                          <strong>{{ @$showproducts->cgst ? $showproducts->cgst.' %' : 'N.A' }}</strong>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>IGST :</p>
                        </div>
                        <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5">
                          <strong>{{ @$showproducts->igst ? $showproducts->igst.' %' : 'N.A' }}</strong>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>SGST :</p>
                        </div>
                        <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5">
                          <strong>{{ @$showproducts->sgst ? $showproducts->sgst.' %' : 'N.A' }}</strong>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Total GST :</p>
                        </div>
                        <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5">
                          <strong>{{ @$showproducts->gst ? $showproducts->gst.' %' : 'N.A' }}</strong>
                        </div>
                        <div class="clearfix"></div>
                         <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                          <p>Status :</p>
                        </div>
                        <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5">
                          <strong> 
                            @if($showproducts->status == 'A')
                              {{ 'Active' }}
                            @elseif($showproducts->status == 'I')
                              {{ 'Inactive' }}
                            @elseif($showproducts->status == 'AA')
                              {{ 'Awaiting Approval' }}
                            @endif
                          </strong>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                  <!--order-detail end-->
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12">
                  <h4 class="pull-left page-title">Description</h4>
                  <div class="order-detail">
                    <!--order-detail start-->
                    <div class="col-lg-12">
                      <div class="order-id">
                        <div class="col-lg-12">
                          <span>{{ @$showproducts->ProductName->description ? $showproducts->ProductName->description : 'N.A' }}</span>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                  <!--order-detail end-->
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12">
                <h4 class="pull-left page-title">Images</h4>
                  <div class="order-detail">
                    <!--order-detail start-->
                    <div class="col-lg-12">
                      <div class="order-id">
                          <div class="img-list">
                            <ul>
                              @foreach(@$showproducts->sellerProductImages as $row)
                              <li><img src="{{ url('public/uploads/products_images/thumbs/'.$row->image) }}"></li>
                              @endforeach
                            </ul>
                          </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                  <!--order-detail end-->
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12">
                <h4 class="pull-left page-title">Documents</h4>
                  <div class="order-detail">
                    <!--order-detail start-->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <div class="order-id">
                        <div class="col-lg-6">
                          <p>Qyality Certificate :</p>
                        </div>
                        <div class="col-lg-6">
                          <span>
                            @if(@$showproducts->qyality_certificate)
                              <div class="ccir">
                                <a href="{{ url('public/uploads/quality_certificate/'.$showproducts->qyality_certificate) }}" download="_new" title="Download">{{ $showproducts->qyality_certificate }}</a><i class="fa fa-download" aria-hidden="true" title="Download"></i>
                              </div> 
                            @else 
                              {{ 'N.A' }} 
                            @endif
                          </span>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-6">
                          <p>License :</p>
                        </div>
                        <div class="col-lg-6">
                          <span>
                            @if(@$showproducts->license) 
                              <div class="ccir">
                                <a href="{{ url('public/uploads/license/'.$showproducts->license) }}" download="_new" title="Download">{{ $showproducts->license }}</a><i class="fa fa-download" aria-hidden="true" title="Download"></i>
                              </div> 
                            @else 
                              {{ 'N.A' }} 
                            @endif
                          </span>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-6">
                          <p>MSDS File :</p>
                        </div>
                        <div class="col-lg-6">
                          <span>
                            @if(@$showproducts->msdc_file)
                              <div class="ccir">
                                <a href="{{ url('public/uploads/msds_files/'.$showproducts->msdc_file) }}" download="_new" title="Download">{{ $showproducts->msdc_file }}</a><i class="fa fa-download" aria-hidden="true" title="Download"></i>
                              </div> 
                            @else
                              {{ 'N.A' }} 
                            @endif
                          </span>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-6">
                          <p>Specification File :</p>
                        </div>
                        <div class="col-lg-6">
                          <span>
                            @if(@$showproducts->spec)
                              <div class="ccir">
                                <a href="{{ url('public/uploads/spec/'.$showproducts->spec) }}" download="_new" title="Download">{{ $showproducts->spec }}</a><i class="fa fa-download" aria-hidden="true" title="Download"></i>
                              </div> 
                            @else
                              {{ 'N.A' }} 
                            @endif
                          </span>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                  <!--order-detail end-->
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End Row -->
  </div>
  <!-- container -->
</div>
<!-- content -->
<footer class="footer text-right">
  2018 &copy; WishOnWheels.
</footer>
</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
<!-- Right Sidebar -->
<!-- /Right-bar -->
</div>
<!-- END wrapper -->
{{-- <script>
  var resizefunc = [];
</script> --}}
<!-- jQuery  -->
<!-- Datatables-->
<!--<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>-->
<!-- Datatable init js -->
{{-- <script type="text/javascript">
  $(document).ready(function() {
      $('#datatable').dataTable({
           "ordering": false,
              "info":     false,
              "searching": false,
               "dom": '<"toolbar">frtip'
          });
      $('#datatable-keytable').DataTable( { keys: true } );
      $('#datatable-responsive').DataTable();
      $('#datatable-scroller').DataTable( { ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
      var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } );
  } );
  TableManageButtons.init();
</script> --}}
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection