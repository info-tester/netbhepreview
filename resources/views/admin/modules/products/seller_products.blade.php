@extends('admin.layout.auth')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">View Seller Products</h4>
					<!--<ol class="breadcrumb pull-right">
						<li><a href="#">User Dashboard</a></li>
						<li class="active">View Item</a></li>
						</ol>-->
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="panel panel-default">
									<div class="panel-body table-rep-plugin">
										<form method="get" action="{{ route('admin.seller-product.index')}}" >
											<input type="hidden" id="min_price" name="min_price" value="@if(@$min_price1){{$min_price1}} @else {{$lowest_price}} @endif"/>
											<input type="hidden" id="max_price" name="max_price" value="@if(@$max_price1){{$max_price1}} @else {{$highest_price}} @endif"/>
		                                 	<input type="hidden" id="minimum_price" placeholder="minimum_price" name="minimum_price" value="{{$lowest_price}}"/>
		                                 	<input type="hidden" id="maximum_price" placeholder="maximum_price" name="maximum_price" value="{{$highest_price}}"/>
                                 			<input type="hidden" id="currency" value="{{env('RUPPY')}}"/>
											{{-- <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<input class="form-control" placeholder="keyword"
													id="keyword" name="keyword" value="{{@$key['keyword']}}" type="text">
												</div>
											</div> --}}
											 
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="your-mail">
													<select class="form-control newdrop " name="parent_id" id="category">
														<option value="">Parent Category</option>
														@foreach($parent as $row)
														<option value="{{$row->id}}" @if(@$key['parent_id'] == $row->id) selected @endif>{{$row->cat_name}}</option>
														@endforeach
													</select>
												</div>
											</div>
											
											 
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="your-mail">
													<select class="form-control newdrop " name="child_id" id="sub_category">
														<option value="">Sub Category</option>
														@if(@$key['child_id'])
															@foreach($sub_category as $child)
															<option value="{{$child->id}}"@if(@$key['child_id'] == $child->id) selected @endif>{{$child->cat_name}}</option>
															@endforeach
														@endif
													</select>
												</div>
											</div>
											
											 
											{{-- <div class="clearfix"></div> --}}
											
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="your-mail">
													<select class="form-control newdrop required" name="status" id="status">
														<option value="">Status</option>
														<option value="A" @if(@$key['status'] == "A") {{ "selected" }} @endif >Active</option>
														<option value="AA" @if(@$key['status'] == "AA") {{ "selected" }} @endif>Awaiting Approval</option>
														<option value="IA" @if(@$key['status'] == "IA") {{ "selected" }} @endif>Inactive By Admin</option>
														<option value="IS" @if(@$key['status'] == "IS") {{ "selected" }} @endif>Inactive By Seller</option>
														<option value="R" @if(@$key['status'] == "R") {{ "selected" }} @endif>Reject</option>
													</select>
												</div>
											</div>
											 
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<input class="form-control" placeholder="Product Name"
														id="product_name" name="product_name" value="{{@$key['product_name']}}" type="text">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<input class="form-control" placeholder="Seller Name"
														id="seller_name" name="seller_name" value="{{@$key['seller_name']}}" type="text">
												</div>
											</div>
											
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<input class="form-control" placeholder="CAS No."
														id="cas_no" name="cas_no" value="{{@$key['cas_no']}}" type="text">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<input class="form-control" placeholder="HSN No."
														id="hsn_no" name="hsn_no" value="{{@$key['hsn_no']}}" type="text">
												</div>
											</div>
											 
											{{-- <div class="clearfix"></div> --}}
											
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<p>
														<label for="amount">Price Range (INR):</label>
														<input type="text" id="amount" name="range" readonly style="border:0; color:#c3433f; font-weight:bold;">
													</p>
													<div id="slider-range"></div>
												</div>
											</div>
											<div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
												<div class="add_btnm pull-right" style="width:auto; margin-top:15px;">
													<input value="Search" id="search" class="btn btn-primary" type="submit">
												</div>
											</div>
										</form>
									</div>
								</div>
								<div class="col-md-12 dess5">
									<i class="fa fa-eye cncl" aria-hidden="true"> <span class="cncl_oopo">View</span></i>
									<i class="fa fa-trash-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
									<i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active</span></i>
									<i class="fa fa-times cncl" aria-hidden="true"> <span class="cncl_oopo">Inactive</span></i>
									<i class="fa fa-times-circle cncl" aria-hidden="true"> <span class="cncl_oopo">Reject</span></i>
									<i class="fa fa-check-circle cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">Approve</span></i>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Product</th>
													<th>Product Name</th>
													<th>Seller Name</th>
													<th>Category</th>
													<th>Concentration</th>
													<th>Price</th>
													<th>Status</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@foreach($products as $detail)
												<tr>
													<td><img src="{{ @$detail->defaultProductImage->image ? url('public/uploads/products_images/thumbs/'.$detail->defaultProductImage->image) : url('public/images/no_img.png') }}" style="height: 50px; width: 50px;" alt="Image"></td>
													<td>{{ @$detail->ProductName->name }}</td>
													<td>{{ @$detail->SellerName->first_name}} {{ @$detail->SellerName->last_name}}</td>
													<td>{{ @$detail->ProductName->parent_category->category_name->cat_name ? $detail->ProductName->parent_category->category_name->cat_name : 'N.A' }} <i class="fa fa-chevron-right" aria-hidden="true"></i> {{ @$detail->ProductName->category->category_name->cat_name ? $detail->ProductName->category->category_name->cat_name : 'N.A' }}</td>
													<td>{{ @$detail->concentration ? $detail->concentration : 0 }}</td>
													<td><b>{{ env('RUPPY') }}</b> {{ @$detail->price_inr }} <br> <b>{{ env('CURRENCY') }}</b> {{ @$detail->price_usd }} <br> {{ @$detail->gst }} % (GST)</td>
													<td>
														@if(@$detail->status == 'IA')
														<label class="label label-danger">{{ 'Inactive By Admin' }}</label>
														@elseif(@$detail->status == 'IS')
														<label class="label label-danger">{{ 'Inactive By Seller' }}</label>
														@elseif(@$detail->status == 'A')
														<label class="label label-success">{{ 'Active' }}</label>
														@elseif(@$detail->status == 'AA')
														<label class="label label-warning">{{ 'Awaiting Approval' }}</label>
														@elseif(@$detail->status == 'R')
														<label class="label label-danger">{{ 'Reject' }}</label>
														@endif
													</td>
													<td>
														<a href="{{ route('admin.seller-product.show',[$detail->id]) }}" title="View"><i class="fa fa-eye delet" aria-hidden="true"></i></a>
														<a href="javascript:void(0)" onclick="deleteCategory('{{route('admin.seller-product.destroy',[$detail->id])}}')" title="Delete"> <i class="fa fa-trash-o delet" aria-hidden="true"></i></a>
														@if(@$detail->status == 'IA')
														<a href="{{ route('admin.seller-product-status',[$detail->id]) }}" title="Active" onclick="return confirm('Do you want to active this product?');"> <i class="fa fa-check delet" aria-hidden="true"></i></a>
														@elseif(@$detail->status == 'A' || @$detail->status == 'IS')
														<a href="{{ route('admin.seller-product-status',[$detail->id]) }}" title="Inactive" onclick="return confirm('Do you want to inactive this product?');"> <i class="fa fa-times delet" aria-hidden="true"></i></a>
														@elseif(@$detail->status == 'AA')
														<a href="{{ route('admin.seller-product-status',[$detail->id]) }}" title="Approve" onclick="return confirm('Do you want to approve this product?');"> <i class="fa fa-check-circle delet" aria-hidden="true"></i></a>
														@endif
														@if($detail->status == 'AA')
														<a href="{{ route('admin.seller-product-reject',[$detail->id]) }}" title="Reject" onclick="return confirm('Do you want to reject this product?');"> <i class="fa fa-times-circle" aria-hidden="true"></i></a>
														@endif
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
</div>
<form method="post" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>
<script>
	function deleteCategory(val){
	 var confirm = window.confirm('Are you want to delete this product ?');
	 if(confirm){
	   $("#destroy").attr('action',val);
	   $("#destroy").submit();
	 }
	}
</script>
<script type="text/javascript">
  	$(document).ready(function(){
    	$('#category').change(function(){
      		var reqData = {
        		'jsonrpc':'2.0',
        		'method': 'get_sub_category',
        		'data': {
          			'parent_id' : $(this).val()
        		}
      		};
      		$.ajax({
        		url: '{{ url('admin/get-sub-category') }}',
        		type: 'GET',
        		dataType: 'json',
        		data: reqData,
      		})
      		.done(function(response) {
        		if(response.result) {
          			var parseHtml = '<option value="">Sub Category</option>';
          			for(var i=0; i<response.result.subCategory.length; i++) {
            			parseHtml += '<option value="'+ response.result.subCategory[i]['id'] +'">' + response.result.subCategory[i]['cat_name'] +'</option>';
          			}
          			$('#sub_category').html(parseHtml);
        		}
      		})
      		.fail(function() {
        		console.log("error");
      		})
   		});
  	});
</script>
<script>
	$( function() {
		var v1=$('#min_price').val();
		var v2=$('#max_price').val();
		var currency = $('#currency').val();
        var minimum = parseInt($('#minimum_price').val());
        var maximum = $('#maximum_price').val();
	  $( "#slider-range" ).slider({
	    range: true,
	    min: minimum,
	    max: maximum,
	    values: [ v1, v2 ],
	    slide: function( event, ui ) {
	      $( "#amount" ).val( currency + ui.values[ 0 ] + " - " + currency + ui.values[ 1 ] );
	    }
	  });
	  $( "#amount" ).val( currency + $( "#slider-range" ).slider( "values", 0 ) +
	    " - " + currency + $( "#slider-range" ).slider( "values", 1 ) );
	
	
	  
	 
	$('.ui-slider-handle').on('click',function()
	{	  
		var min_price=$( "#slider-range" ).slider( "values", 0 );
	var max_price=$( "#slider-range" ).slider( "values", 1 );
	 
	//alert(min_price);
	
	//var qs="min_price="+min_price+"&max_price="+max_price;
	$('#min_price').val(min_price);
	$('#max_price').val(max_price);
	
	
	
	
	});
	
	
	
	
	});
</script>

<script>
	$(document).ready(function(){
	  $( "#product_name" ).autocomplete({
	    source: function( request, response ) {
	      $.ajax({
	        url: "{{ url('/admin/search-product') }}",
	        dataType: "json",
	        data: {
	          product_name: request.term
	        },
	        success: function( data ) {
	          response( data );
	        }
	      });
	    },
	    minLength: 1,
	    select: function( event, ui ) {
	      // $("#company_id").val(ui.item.id);
	    }
	  });
	  $( "#seller_name" ).autocomplete({
	    source: function( request, response ) {
	      $.ajax({
	        url: "{{ url('/admin/search-seller-name') }}",
	        dataType: "json",
	        data: {
	          seller_name: request.term
	        },
	        success: function( data ) {
	          response( data );
	        }
	      });
	    },
	    minLength: 1,
	    select: function( event, ui ) {
	      // $("#company_id").val(ui.item.id);
	    }
	  });
	});
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection