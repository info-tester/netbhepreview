@extends('admin.layouts.app')
@section('title', 'NearO | Admin | Product')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Manage Products</h4>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
								<div class="row top_from">
									<form method="get" action="{{ route('admin.product')}}" >
											<input type="hidden" id="min_price" name="min_price" value="@if(@$min_price1){{$min_price1}} @else {{$lowest_price}} @endif"/>

											<input type="hidden" id="max_price" name="max_price" value="@if(@$max_price1){{$max_price1}} @else {{$highest_price}} @endif"/>

		                                 	<input type="hidden" id="minimum_price" placeholder="minimum_price" name="minimum_price" value="{{$lowest_price}}"/>

		                                 	<input type="hidden" id="maximum_price" placeholder="maximum_price" name="maximum_price" value="{{$highest_price}}"/>

                                 			<input type="hidden" id="currency" value="{{env('CURRENCY')}}"/>

											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<input class="form-control" placeholder="Keyword" name="product_name" id="product_name" value="{{@$key['product_name']}}" type="text">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="your-mail">
													<select class="form-control newdrop " name="parent_id" id="category">
														<option value="">Parent Category</option>
														@foreach($category as $row)
														<option value="{{$row->id}}" @if(@$key['parent_id'] == $row->id) selected @endif>{{$row->name}}</option>
														@endforeach
													</select>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="your-mail">
													<select class="form-control newdrop " name="child_id" id="sub_category">
														<option value="">Sub Category</option>
														@if(@$key['child_id'] || @$key['parent_id'])
															@foreach($sub_category as $child)
															<option value="{{$child->id}}"@if(@$key['child_id'] == $child->id) selected @endif>{{$child->name}}</option>
															@endforeach
														@endif
													</select>
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="your-mail">
													<select class="form-control newdrop required" name="status" id="status">
														<option value="">Status</option>
														<option value="A" @if(@$key['status'] == "A") {{ "selected" }} @endif >Active</option>
														<option value="AA" @if(@$key['status'] == "AA") {{ "selected" }} @endif >Awaiting Approval</option>
														<option value="I" @if(@$key['status'] == "I") {{ "selected" }} @endif>Inactive</option>
													</select>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<input class="form-control" placeholder="Seller Name"
														id="seller_name" name="seller_name" value="{{@$key['seller_name']}}" type="text">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<p>
														<label for="amount">Price range:</label>
														<input type="text" id="amount" name="range" readonly style="border:0; color:#c3433f; font-weight:bold;">
													</p>
													<div id="slider-range"></div>
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
												<div class="add_btnm pull-right" {{-- style="width:auto; margin-top:15px;" --}}>
													<input value="Search" id="search" class="btn btn-primary" type="submit">
												</div>
											</div>
										</form>
                                 </div>       
                                    
								<div class="col-md-12 dess5">
									<i class="fa fa-eye cncl" aria-hidden="true"> <span class="cncl_oopo">View</span></i>
									<i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active</span></i>
									<i class="fa fa-times cncl" aria-hidden="true"> <span class="cncl_oopo">Inactive</span></i>
									{{-- <i class="fa fa-star cncl" aria-hidden="true"> <span class="cncl_oopo">Featured</span></i>
									<i class="fa fa-star-o cncl" aria-hidden="true"> <span class="cncl_oopo">Not Featured</span></i> --}}
									<i class="fa fa-check-circle cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">Approve</span></i>
								</div>
                                
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Product</th>
													<th>Product Name</th>
													<th>Category</th>
													<th>Seller Name</th>
													<th>Base Price / Unit</th>
													<th>Status</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@foreach($products as $detail)
												<tr>
													<td><img src="{{ @$detail->img->image ? url('public/uploads/products/thumbs/'.$detail->img->image) : url('public/images/no_img.png') }}" style="height: 50px; width: 50px;" alt="Image"></td>
													<td>{{ @$detail->name ? $detail->name : 'N.A' }}</td>
													<td>{{ @$detail->category->name ? $detail->category->name : 'N.A' }} <i class="fa fa-chevron-right" aria-hidden="true"></i> {{ @$detail->subCategory->name ? $detail->subCategory->name : 'N.A' }}</td>
													<td>{{ @$detail->sellerName->fname ? $detail->sellerName->fname : 'N.A' }} {{ @$detail->sellerName->lname }}</td>
													<td>{{ @$detail->starting_price }} / {{ @$detail->starting_quantity }} {{ @$detail->units->name }}</td>
													<td>
														@if(@$detail->status == 'IA')
														<label class="label label-danger">{{ 'Inactive by admin' }}</label>
														@elseif(@$detail->status == 'IS')
														<label class="label label-danger">{{ 'Inactive by seller' }}</label>
														@elseif(@$detail->status == 'A')
														<label class="label label-success">{{ 'Active' }}</label>
														@elseif(@$detail->status == 'AA')
														<label class="label label-warning">{{ 'Awaiting Approval' }}</label>
														@endif
														<br><br>
														{{-- @if(@$detail->is_featured == 1)
														<label class="label label-success">{{ 'Featured' }}</label>
														@elseif(@$detail->is_featured == 0)
														<label class="label label-danger">{{ 'Not Featured' }}</label>
														@endif --}}
													</td>
													<td>
														{{-- @if(@$detail->is_featured == 1)
															<a href="{{ route('admin.product-featured',[$detail->id]) }}" title="Not Featured" onclick="return confirm('Do you want to Not featured this product?');"><i class="fa fa-star delet" aria-hidden="true"></i></a>
														@else
															<a href="{{ route('admin.product-featured',[$detail->id]) }}" title="Featured" onclick="return confirm('Do you want to featured this product?');"><i class="fa fa-star-o delet" aria-hidden="true"></i></a>
														@endif --}}
														<a href="{{ route('admin.product.details',[$detail->id]) }}" title="View"><i class="fa fa-eye delet" aria-hidden="true"></i></a>
														@if(@$detail->status == 'IA' || @$detail->status == 'IS')
														<a href="{{ route('admin.product.status',[$detail->id]) }}" title="Active" onclick="return confirm('Do you want to active this product?');"> <i class="fa fa-check delet" aria-hidden="true"></i></a>
														@elseif(@$detail->status == 'A')
														<a href="{{ route('admin.product.status',[$detail->id]) }}" title="Inactive" onclick="return confirm('Do you want to inactive this product?');"> <i class="fa fa-times delet" aria-hidden="true"></i></a>
														@elseif(@$detail->status == 'AA')
														<a href="{{ route('admin.product.status',[$detail->id]) }}" title="Approve" onclick="return confirm('Do you want to approve this product?');"> <i class="fa fa-check-circle delet" aria-hidden="true"></i></a>
														@endif
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
</div>
<form method="post" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>
<script>
	function deleteCategory(val){
	 var confirm = window.confirm('Are you want to delete this Category ?');
	 if(confirm){
	   $("#destroy").attr('action',val);
	   $("#destroy").submit();
	 }
	}
</script>
<script type="text/javascript">
  	$(document).ready(function(){
    	$('#category').change(function(){
      		var reqData = {
        		'jsonrpc':'2.0',
        		'method': 'get_sub_category',
        		'data': {
          			'parent_id' : $(this).val()
        		}
      		};
      		$.ajax({
        		url: '{{ url('get-sub-category') }}',
        		type: 'GET',
        		dataType: 'json',
        		data: reqData,
      		})
      		.done(function(response) {
        		if(response.result) {
          			var parseHtml = '<option value="">Sub Category</option>';
          			for(var i=0; i<response.result.subCategory.length; i++) {
            			parseHtml += '<option value="'+ response.result.subCategory[i]['id'] +'">' + response.result.subCategory[i]['name'] +'</option>';
          			}
          			$('#sub_category').html(parseHtml);
        		}
      		})
      		.fail(function() {
        		console.log("error");
      		})
   		});
  	});
</script>
<script>
	$( function() {
		var v1 = $('#min_price').val();
		var v2 = $('#max_price').val();
		var currency = $('#currency').val();
        var minimum = parseInt($('#minimum_price').val());
        var maximum = parseInt($('#maximum_price').val());
		console.log(minimum, maximum, v1, v2);
	  	$( "#slider-range" ).slider({
	    	range: true,
	    	min: minimum,
	    	max: maximum,
	    	values: [ v1, v2 ],
	    	slide: function( event, ui ) {
	      		$( "#amount" ).val( currency + ui.values[ 0 ] + " - " + currency + ui.values[ 1 ] );
	    	}
	  	});
	  	$( "#amount" ).val( currency + $( "#slider-range" ).slider( "values", 0 ) + " - " + currency + $( "#slider-range" ).slider( "values", 1 ) );

		$('.ui-slider-handle').on('click',function(){	  
			var min_price = $( "#slider-range" ).slider( "values", 0 );
			var max_price = $( "#slider-range" ).slider( "values", 1 );
		 
			//alert(min_price);
		
			//var qs="min_price="+min_price+"&max_price="+max_price;
			$('#min_price').val(min_price);
			$('#max_price').val(max_price);
		});
			console.log(min_price, max_price);
	});
</script>

{{-- <script>
	$(document).ready(function(){
	  $( "#product_name" ).autocomplete({
	    source: function( request, response ) {
	      $.ajax({
	        url: "{{ url('/admin/search-product') }}",
	        dataType: "json",
	        data: {
	          product_name: request.term
	        },
	        success: function( data ) {
	        	//console.log(data);
	          response( data );
	        }
	      });
	    },
	    minLength: 1,
	    select: function( event, ui ) {
	      $("#company_id").val(ui.item.id);
	    }
	  });
	});
</script> --}}
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection