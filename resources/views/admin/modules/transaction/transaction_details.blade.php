@extends('admin.layouts.app')
@section('title', 'Vedic Astro World | Admin | All Transactions')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">All Transaction Details</h4>
				</div>
			</div>
			<div class="row">
				
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="panel panel-default">
									<div class="panel-body table-rep-plugin">
										<form method="get" action="{{ route('admin.transaction.filter') }}">
										
											{{-- <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
	                                            <div class="your-mail">
	                                                
	                                                <input type="text" class="form-control" name="txn_id" id="txn_id" value="{{@$post['txn_id']}}" placeholder="TXN ID">
	                                            </div>
                                        	</div> --}}
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<select class="form-control newdrop required" name="status">
														<option value="">Select Transaction Status</option>
														<option value="I" @if(@$post['status']=="I") selected @endif >Initiated</option>
														<option value="S" @if(@$post['status']=="S") selected @endif >Success</option>
														<option value="C" @if(@$post['status']=="C") selected @endif >Cancelled</option>
														{{-- <option value="F" @if(@$post['status']=="F") selected @endif >Failed</option> --}}
														
													</select>
												</div>
											</div>
											{{-- <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<select class="form-control newdrop required" name="payment_type">
														<option value="">Payment type</option>
														
														<option value="W" @if(@$post['payment_type']=="W") selected @endif >Wallet</option>
														<option value="M" @if(@$post['payment_type']=="M") selected @endif >Membership</option>
													</select>
												</div>
											</div> --}}
										
										 <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="add_btnm pull-left" style="width:auto; margin-top:10px;">
													<input value="Search" class="btn btn-primary" type="submit">
												</div>
											</div>
										</form>
									</div>
									<div class="clearfix"></div>
									
                                    <div class="col-md-12 dess5">
										<i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Mark as Paid</span></i>
										<i class="fa fa-times cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">Cancel</span></i>
										
									</div>
								</div>
								
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Id</th>
													<th>Txn Id</th>
													<th>Name</th>
													<th>Amount</th>
													<th>Type</th>
													<th>Date</th>
													<th>Payment Method</th>
													<th>Status</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@foreach($payments as $row)
												<tr>
													<td>{{ $row->id }}</td>
													<td>{{ $row->txn_id }}</td>
													<td>{{ @$row->userName->nick_name ? @$row->userName->nick_name : @$row->userName->name }}</td>
													@php 
													$currency = '';
													@endphp
													@if($row->currency == "INR") 
														
														@php $currency = '&#8377;'; @endphp
													@elseif($row->currency == "USD")
														@php $currency = '$'; @endphp
													@elseif($row->currency == "EUR")
														@php $currency = '&euro;'; @endphp
													@endif
													<td>{!! $currency . $row->amount !!}</td>
													<td>{{ $row->type=="W" ? "Wallet": "Membership" }}</td>
													
													<td>{{ date('d/m/Y', strtotime($row->created_at)) }}</td>
													<td>{{ $row->payment_mode=="C" ? "CCAvenue": "Paypal"}}</td>
													
													<td>@if($row->status == 'C' || $row->status=="F") Cancelled @elseif($row->status == 'I') Initiated @else Success @endif</td>
													<td>@if($row->status == 'I') <a href="{{ route('transaction.status.cancel',["id"=>$row->id]) }}" onclick="return confirm('Are you sure to mark this transaction as cancelled transaction?')" title="Cancel"><i class="fa fa-times"></i>
														<a href="{{ route('transaction.status.success',["id"=>$row->id]) }}" onclick="return confirm('Are you sure to mark this transaction as paid transaction?')" title="Mark as Paid"><i class="fa fa-check"></i>
													</a>  @endif</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<form method="post" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
	var confirm = window.confirm('Are you want to delete this news ?');
	if(confirm){
		$("#destroy").attr('action',val);
		$("#destroy").submit();
	}
 }
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection