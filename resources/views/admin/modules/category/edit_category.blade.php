@extends('admin.layouts.app')
@section('title', 'Netbhe|Edit Category')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
<div class="content">
    <div class="container">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="pull-left page-title">Edit Category</h4>
                <div class="submit-login no_mmg pull-right">
                    <a href="{{ route('admin.categories') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
                </div>
            </div>

            @include('admin.includes.error')

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-rep-plugin">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 nhp">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <form id="myform" method="post" action="{{ route('admin.category.update',[$category->id])}}" enctype="multipart/form-data">
                                            
                                            @csrf
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Category Name</label>
                                                    <input type="text" value="{{ $category->name }}" name="name" class="form-control required" id="name">
                                                </div>
                                            </div>
                                            @if(@$category->parent_id!=0)
                                            <div class="clearfix"></div>
                                            <div class="col-lg-6 col-md6 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Parent Category </label>
                                                    <select class="form-control newdrop" name="parent_id" id="parent_id">
                                                        <option value="">Choose Category</option>
                                                        @foreach($parent as $row)
                                                        <option value="{{$row->id}}" @if($category->parent_id == $row->id) selected @endif>{{$row->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    <span>
                                                    (Leave this field for parent category)
                                                    </span>
                                                </div>
                                            </div>
                                            @endif
                                            <div class="clearfix"></div>


                                            <div class="col-lg-6 col-md6 col-sm-6 col-xs-12" id="meta_tit" @if($category->parent_id>0) style="display: none;" @endif>
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Meta Title</label>
                                                    <input type="text" class="form-control required" id="meta_title" placeholder="" value="{{ $category->meta_title }}" name="meta_title">
                                                </div>
                                            </div>
                                            <!--all_time_sho-->
                                            <div class="clearfix"></div>
                                            <div class="all_time_sho">
                                                <div class="col-md-6 col-sm-6 col-xs-12 col-lg-6" id="meta_desc" @if($category->parent_id>0) style="display: none;" @endif>
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Meta Description</label>
                                                        <textarea placeholder="" id="meta_description" name="meta_description" rows="3" class="form-control message required">{{ $category->meta_description }}</textarea>
                                                    </div>
                                                </div>
                                                {{-- <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Description</label>
                                                        <textarea placeholder="" id="description" name="description" rows="3" class="form-control message required">{{ $category->description }}</textarea>
                                                    </div>
                                                </div> --}}
                                                <div class="clearfix"></div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 image_section" id="img" @if($category->parent_id>0) style="display: none;" @endif>
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Image</label>
                                                        <input type="file" class="form-control" id="file" placeholder="Image" name="image" accept="image/*">
                                                        <span>
                                                        (Image size must be 276 X 183.)
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 image_section" id="prdimg">
                                                    <div class="your-mail">
                                                        <span><img src="{{URL::to('storage/app/public/category_images').'/'.$category->image }}" alt="" style="height: 100px;"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="add_btnm submit-login">
                                                    <input value="Update" type="submit" class="btn btn-default">
                                                </div>
                                            </div>
                                            <!--all_time_sho-->
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Row -->
        </div>
        <!-- container -->
    </div>
    <!-- content -->
</div>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')

        <script>
        $(document).ready(function(){
            $("#myform").validate({
                messages : 
                {
                    name                : "Please enter category name",
                    meta_title          : "Please enter meta title",
                    meta_description    : "Please enter meta description",
                    description         : "Please enter description",
                    image               : "Please enter category image",
                }
            });
        });
            $('#parent_id').change(function(){
                if($('#parent_id').val()!=""){
                    $('#img').hide();
                    $('#meta_desc').hide();
                    $('#meta_tit').hide();
                    $('#prdimg').hide();
                }
                else{
                    $('#img').show();
                    $('#meta_desc').show();
                    $('#prdimg').show();
                    $('#meta_tit').show();
                }
                
            });
    </script>
@endsection
