@extends('admin.layouts.app')
@section('title', 'Netbhe|All Category')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
<div class="content">
    <div class="container">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h3 class="pull-left page-title">Manage Category</h3>
                <div class="submit-login no_mmg pull-right">
                    <a href="{{ route('admin.add.categories') }}" title="Add Category"><button type="button" class="btn btn-default">Add Category</button></a>
                </div>
            </div>
        </div>

        @include('admin.includes.error')
        
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body table-rep-plugin">
                        <div class="row">
                            <div class="exmmt_rrn2">
                                <form action="{{ route('admin.filter.categories') }}" method="post" id="myFormSearch" name="myFormSearch">
                                    @csrf
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                        <div class="your-mail">
                                            <label for="exampleInputEmail1">Keyword</label>
                                            <input class="form-control" id="keyword" type="text" name="keyword" value="{{ @$key['keyword'] }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                        <div class="your-mail">
                                            <label for="exampleInputEmail1">Parent Category</label>
                                            <select class="form-control newdrop" name="parent_id" id="parent_id">
                                                <option value="">Select</option>
                                                @foreach($p_cat as $pt)
                                                <option value="{{ $pt->id }}" @if(@$key['parent_id']==$pt->id) selected @endif>{{ $pt->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                        <div class="your-mail">
                                            <label for="exampleInputEmail1">Status</label>
                                            <select class="form-control newdrop" name="status" id="status">
                                                <option value="">Select</option>
                                                <option value="A" @if(@$key['status']=="A") selected @endif>Active</option>
                                                <option value="I" @if(@$key['status']=="I") selected @endif>Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                        <div class="add_btnm exrt">
                                            <input class="nm9 xxx" value="Search" type="button">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12 dess5">
                                <i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
                                <i class="fa fa-trash cncl"> <span class="cncl_oopo">Delete</span></i>
                                <i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active</span></i>
                                <i class="fa fa-times cncl" aria-hidden="true"> <span class="cncl_oopo">Inactive</span></i>
                                <i class="fa fa-square cncl" aria-hidden="true"> <span class="cncl_oopo">Show in Home Page</span></i>
                                <i class="fa fa-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Not Show in Home Page</span></i>
                                <i class="fa fa-circle cncl" aria-hidden="true"> <span class="cncl_oopo">Show in Category Search Page</span></i>
                                <i class="fa fa-circle-o cncl" aria-hidden="true"> <span class="cncl_oopo">Not Show in Category Search Page</span></i>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" data-pattern="priority-columns">
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Image</th>
                                                <th>Category</th>
                                                <th>Parent Category</th>
                                                {{-- <th>Description</th> --}}
                                                <th>Meta Title</th>
                                                <th>Meta Description</th>
                                                <th>Status</th>
                                                <th>Show in<br>Home</th>
                                                <th>Show in<br>Category Page</th>
                                                <th style="min-width:72px;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($cat as $ct)
                                                <tr>
                                                    <td>
                                                        @if(@$ct->image!=null)
                                                        <img src="{{ URL::to('storage/app/public/category_images').'/'.$ct->image }}" class="img-responsive" height="50" alt="">
                                                        @else
                                                            <center>_</center>
                                                        @endif

                                                    </td>
                                                    <td>{{ $ct->name }}</td>
                                                    <td>
                                                        @if($ct->parent_id==0)
                                                            <center>_</center>
                                                        @else
                                                            {{ $ct->parent->name }}
                                                        @endif
                                                     </td>
                                                    
                                                    <td>
                                                        @if(@$ct->meta_title!=null)
                                                        {{$ct->meta_title }}
                                                        @else
                                                            {{ $ct->parent->meta_title }}
                                                        @endif
                                                        
                                                    </td>
                                                    <td>
                                                        @if(@$ct->meta_description!=null)
                                                        {{$ct->meta_description }}
                                                        @else
                                                            {{ substr(@$ct->parent->meta_description, 0,20) }}...
                                                        @endif

                                                    </td>

                                                    <td>
                                                        <label @if($ct->status=="A")class="label label-success" @else class="label label-danger" @endif>{{ $ct->status=="A" ? "Active": "Inactive"}}</label>
                                                    </td>
                                                    <td>
                                                        @if(@$ct->shown_in_home == 'N')
                                                        <label class="label label-danger">{{ 'No' }}</label>
                                                        @elseif(@$ct->shown_in_home == 'Y')
                                                        <label class="label label-success">{{ 'Yes' }}</label>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if(@$ct->shown_in_cat_search == 'N')
                                                        <label class="label label-danger">{{ 'No' }}</label>
                                                        @elseif(@$ct->shown_in_cat_search == 'Y')
                                                        <label class="label label-success">{{ 'Yes' }}</label>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a href="{{ route('admin.category.edit',['id'=>$ct->id]) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
                                                        <a href="{{ route('admin.category.delete', ['id'=>$ct->id]) }}" onclick="return confirm('Are you really want to delete this category?')" title="Delete"> <i class="fa fa-trash delet"></i></a>
                                                        @if($ct->status=="A")
                                                            <a href="{{ route('admin.category.status', ['id'=>$ct->id]) }}" onclick="return confirm('Are you really want to Inactive this category?')" title="Inactive"><i class="fa fa-times delet" aria-hidden="true"></i></a>
                                                        @else
                                                            <a href="{{ route('admin.category.status', ['id'=>$ct->id]) }}" title="Active" onclick="return confirm('Are you really want to Active this category?')"><i class="fa fa-check delet" aria-hidden="true"></i></a>
                                                        @endif

                                                        @if($ct->shown_in_home == 'Y')
                                                            <a href="{{ route('admin.category.homepage',[$ct->id]) }}" title="Not Show in Home Page" onclick="return confirm('Do you want to Not Show in Home Page this ');"> <i class="fa fa-square delet" aria-hidden="true"></i></a>
                                                        @else
                                                            <a href="{{ route('admin.category.homepage',[$ct->id]) }}" title="Show in Home Page " onclick="return confirm('Do you want to Show in Home Page this ');"> <i class="fa fa-square-o delet" aria-hidden="true"></i></a>
                                                        @endif

                                                        @if($ct->shown_in_cat_search == 'Y')
                                                            <a href="{{ route('admin.category.search.show',[$ct->id]) }}" title="Not Show in Category Search Page" onclick="return confirm('Do you want to Not Show in Categories Search Page this ');"> <i class="fa fa-circle delet" aria-hidden="true"></i></a>
                                                        @else
                                                            <a href="{{ route('admin.category.search.show',[$ct->id]) }}" title="Show in Category Search Page " onclick="return confirm('Do you want to Show in Categories Search Page this ');"> <i class="fa fa-circle-o delet" aria-hidden="true"></i></a>
                                                        @endif

                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Row -->
    </div>
    <!-- container -->
</div>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
    <script>
        $('.xxx').click(function(){
            $("#myFormSearch").submit();
        });
    </script>
@endsection