@extends('admin.layouts.app')
@section('title', @$content->id?'Netbhe| Edit Category Landing Page':'Netbhe| Add Category Landing Page')
@section('header')
@include('admin.includes.header')
<style>
    .your-mail input[type='url'] {
        width: 100%;
        height: 40px;
        /*line-height:50px;*/
        float: left;
        padding: 8px;
        margin-bottom: 8px;
        border: 1px solid #CCC;
        border-radius: 0;
        background: #fff !important;
        color: #000;
        font-family: "Open Sans", sans-serif;
        font-size: 14px;
        font-weight: 400;
    }
    .landingform h1{
        text-align: center !important;
        font-size: 30px;
    }

</style>
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="pull-left page-title">{{@$content->id?'Edit' :'Add'}} Category Landing Page</h4>
                        <div class="submit-login no_mmg pull-right">
                            <a target="_blank" href="{{url('/')}}/categoria-landing-page/{{@$content->categoryDetails->slug}}" title="Preview"><button type="button" class="btn btn-default">Preview</button></a>
                            <a href="{{ route('admin.category.landing.index') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
                        </div>
                    </div>
                </div>

                @include('admin.includes.error')
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body table-rep-plugin">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12 nhp">
                                        <div class="table-responsive" data-pattern="priority-columns">
                                            <form id="myform" class="landingform" method="post" action="{{@$content->id?route('admin.category.landing.add.update',['id'=>$content->id]):route('admin.category.landing.add.save')}}" enctype="multipart/form-data">
                                                @csrf

                                                @if(@$content->id==null)
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Category </label>
                                                        <select class="form-control newdrop " name="category" id="category" required>
                                                            <option value="">Select Category</option>
                                                            @foreach($cat as $row)
                                                            <option value="{{$row->id}}" @if(old('category')==$row->id)selected @endif>{{$row->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                @endif




                                                {{-- appearance  --}}
                                                <h1> Appearance</h1>
                                                <div class="col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="page_color">Page Color *</label>
                                                        <input type="color" class="form-control required" id="page_color" name="page_color" value="{{@$content->page_color ?? '#1781d2'}}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="page_color">Button Color *</label>
                                                        <input type="color" class="form-control required" id="button_color" name="button_color" value="{{@$content->button_color ?? '#1781d2'}}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="page_color">Button Text Color *</label>
                                                        <input type="color" class="form-control required" id="button_text_color" name="button_text_color" value="{{@$content->button_text_color ?? '#ffffff'}}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="banner_heading">In footer where do you want it to appear? *</label>
                                                        <label for="for_professionals">
                                                            <input type="radio" class="required" id="for_professionals" name="appearance" value="P" @if(@$content->appearance != 'U') checked @endif >
                                                            For professionals
                                                        </label>
                                                        <label for="for_users">
                                                            <input type="radio" class="required" id="for_users" name="appearance" value="U" @if(@$content->appearance == 'U') checked @endif >
                                                            For Users
                                                        </label>
                                                    </div>
                                                </div>



                                                {{-- banner  --}}
                                                <h1> Banner</h1>
                                                <div class="col-lg-12 row">
                                                    <div class="your-mail">
                                                        <label for="alignment1">Alignment*</label>
                                                        <div class="col-md-4">
                                                            <label for="alignment_left">
                                                                <input type="radio" class="required" id="alignment_left" name="alignment" value="L" @if(@$content->alignment == 'L') checked @endif >
                                                                Left
                                                            </label>
                                                        </div>
                                                        <div class="col-md-4 text-center">
                                                            <label for="alignment_center">
                                                                <input type="radio" class="required" id="alignment_center" name="alignment" value="C" @if(@$content->alignment == 'C') checked @endif >
                                                                Center
                                                            </label>
                                                        </div>
                                                        <div class="col-md-4 text-right">
                                                            <label for="alignment_right">
                                                                <input type="radio" class="required" id="alignment_right" name="alignment" value="R" @if(@$content->alignment == 'R') checked @endif >
                                                                Right
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="banner_heading">Banner Heading*</label>
                                                        <input type="text" class="form-control required" placeholder="Banner heading" id="banner_heading" name="banner_heading" value="{{old('banner_heading',@$content->banner_heading)}}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="banner_heading_color">Banner Heading Color*</label>
                                                        <input type="color" class="form-control required" id="banner_heading_color" name="banner_heading_color" value="{{old('banner_heading_color',@$content->banner_heading_color)}}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="your-mail">
                                                        <label for="banner_button_text">Banner Button Text *</label>
                                                        <input type="text" class="form-control required" placeholder="Banner Button Text" name="banner_button_text" value="{{old('banner_button_text',@$content->banner_button_text)}}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="your-mail">
                                                        <label for="banner_button_link">Banner Button URL *</label>
                                                        <input type="url" class="form-control required" placeholder="Banner Button URL" name="banner_button_link" value="{{old('banner_button_link',@$content->banner_button_link)}}">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="all_time_sho">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                                        <div class="your-mail">
                                                            <label for="banner_desc">Banner Description*</label>
                                                            <textarea placeholder="Write Banner description" name="banner_desc" id="banner_desc" class="form-control required">{{old('banner_desc',@$content->banner_desc)}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="banner_desc_color">Banner Description Color*</label>
                                                        <input type="color" class="form-control required" id="banner_desc_color" name="banner_desc_color" value="{{old('banner_desc_color',@$content->banner_desc_color)}}">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-4">
                                                    <div class="your-mail">
                                                        <label for="banner_image">Banner Image (Recommended size 1600 x 500)</label>
                                                        <input type="file" name="banner_image" class="image_input form-control">
                                                        <img src="{{ URL::to('storage/app/public/uploads/category_landing_images/' . @$content->banner_image) }}" style="height: 150px; width: 150px; object-fit: contain; display: {{@$content->banner_image ? 'block' : 'none' }};" alt="">
                                                    </div>
                                                </div>
                                                {{-- end banner  --}}
                                                <div class="clearfix"></div>




                                                {{-- whyus section  --}}
                                                <h1> Why Us</h1>

                                                <div class="col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="whyus_section_heading">Why us Heading*</label>
                                                        <input type="text" class="form-control required" placeholder="Why us heading" id="whyus_section_heading" name="whyus_section_heading" value="{{old('whyus_section_heading',@$content->whyus_section_heading)}}">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="all_time_sho">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                                        <div class="your-mail">
                                                            <label for="whyus_section_desc">Why us Description*</label>
                                                            <textarea placeholder="Write why us description" name="whyus_section_desc" id="whyus_section_desc" class="form-control required">{{old('whyus_section_desc',@$content->whyus_section_desc)}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{-- end whyus section  --}}
                                                <div class="clearfix"></div>


                                                {{-- whyus section 1  --}}
                                                <div class="col-lg-6">
                                                    <h1> Why Us Section-1</h1>
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="your-mail">
                                                                <label for="whyus_heading_1">Why us section 1 Heading*</label>
                                                                <input type="text" class="form-control required" placeholder="Why us section heading" id="whyus_heading_1" name="whyus_heading_1" value="{{old('whyus_heading_1',@$content->whyus_heading_1)}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="your-mail">
                                                                <label for="whyus_btn_text_1">Why us section 1 Button Text *</label>
                                                                <input type="text" class="form-control required" placeholder="Why us section Button Text" name="whyus_btn_text_1" value="{{old('whyus_btn_text_1',@$content->whyus_btn_text_1)}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="your-mail">
                                                                <label for="whyus_btn_link_1">Why us section 1 link*</label>
                                                                <input type="url" class="form-control required" placeholder="Why us section URL" name="whyus_btn_link_1" value="{{old('whyus_btn_link_1',@$content->whyus_btn_link_1)}}">
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="all_time_sho">
                                                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                                                <div class="your-mail">
                                                                    <label for="whyus_desc_1">Why us section 1 Description*</label>
                                                                    <textarea placeholder="Write Banner description" name="whyus_desc_1" id="whyus_desc_1" class="form-control required">{{old('whyus_desc_1',@$content->whyus_desc_1)}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                                            <div class="your-mail">
                                                                <label for="whyus_image_1">Why us section 1 Image (Recommended size 64 x 64)</label>
                                                                <input type="file" name="whyus_image_1" class="image_input form-control">
                                                                <img src="{{ URL::to('storage/app/public/uploads/category_landing_images/' . @$content->whyus_image_1) }}" style="height: 64px; width: 64px; object-fit: contain; display: {{@$content->whyus_image_1 ? 'block' : 'none' }};" alt="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                {{-- whyus section 1  --}}
                                                {{-- whyus section 2  --}}

                                                <div class="col-lg-6">
                                                    <h1> Why Us Section-2</h1>
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="your-mail">
                                                                <label for="whyus_heading_2">Why us section 2 Heading*</label>
                                                                <input type="text" class="form-control required" placeholder="Why us section heading" id="whyus_heading_2" name="whyus_heading_2" value="{{old('whyus_heading_2',@$content->whyus_heading_2)}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="your-mail">
                                                                <label for="whyus_btn_text_2">Why us section 2 Button Text *</label>
                                                                <input type="text" class="form-control required" placeholder="Why us section Button Text" name="whyus_btn_text_2" value="{{old('whyus_btn_text_2',@$content->whyus_btn_text_2)}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="your-mail">
                                                                <label for="whyus_btn_link_2">Why us section 2 link*</label>
                                                                <input type="url" class="form-control required" placeholder="Why us section URL" name="whyus_btn_link_2" value="{{old('whyus_btn_link_2',@$content->whyus_btn_link_2)}}">
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="all_time_sho">
                                                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                                                <div class="your-mail">
                                                                    <label for="whyus_desc_2">Why us section 2 Description*</label>
                                                                    <textarea placeholder="Write Banner description" name="whyus_desc_2" id="whyus_desc_2" class="form-control required">{{old('whyus_desc_2',@$content->whyus_desc_2)}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                                            <div class="your-mail">
                                                                <label for="whyus_image_2">Why us section 2 Image  (Recommended size 64 x 64)</label>
                                                                <input type="file" name="whyus_image_2" class="image_input form-control">
                                                                <img src="{{ URL::to('storage/app/public/uploads/category_landing_images/' . @$content->whyus_image_2) }}" style="height: 64px; width: 64px; object-fit: contain; display: {{@$content->whyus_image_2 ? 'block' : 'none' }};" alt="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                {{-- whyus section 2  --}}
                                                <div class="clearfix"></div>

                                                {{-- whyus section 1  --}}
                                                <div class="col-lg-6">
                                                    <h1> Why Us Section-3</h1>
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="your-mail">
                                                                <label for="whyus_heading_3">Why us section 3 Heading*</label>
                                                                <input type="text" class="form-control required" placeholder="Why us section heading" id="whyus_heading_3" name="whyus_heading_3" value="{{old('whyus_heading_3',@$content->whyus_heading_3)}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="your-mail">
                                                                <label for="whyus_btn_text_3">Why us section 3 Button Text *</label>
                                                                <input type="text" class="form-control required" placeholder="Why us section Button Text" name="whyus_btn_text_3" value="{{old('whyus_btn_text_3',@$content->whyus_btn_text_3)}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="your-mail">
                                                                <label for="whyus_btn_link_3">Why us section 3 link*</label>
                                                                <input type="url" class="form-control required" placeholder="Why us section URL" name="whyus_btn_link_3" value="{{old('whyus_btn_link_3',@$content->whyus_btn_link_3)}}">
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="all_time_sho">
                                                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                                                <div class="your-mail">
                                                                    <label for="whyus_desc_3">Why us section 3 Description*</label>
                                                                    <textarea placeholder="Write Banner description" name="whyus_desc_3" id="whyus_desc_3" class="form-control required">{{old('whyus_desc_3',@$content->whyus_desc_3)}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                                            <div class="your-mail">
                                                                <label for="whyus_image_3">Why us section 3 Image  (Recommended size 64 x 64)</label>
                                                                <input type="file" name="whyus_image_3" class="image_input form-control">
                                                                <img src="{{ URL::to('storage/app/public/uploads/category_landing_images/' . @$content->whyus_image_3) }}" style="height: 64px; width: 64px; object-fit: contain; display: {{@$content->whyus_image_3 ? 'block' : 'none' }};" alt="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                {{-- whyus section 3  --}}
                                                {{-- whyus section 4  --}}
                                                <div class="col-lg-6">
                                                    <h1> Why Us Section-4</h1>
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="your-mail">
                                                                <label for="whyus_heading_4">Why us section 4 Heading*</label>
                                                                <input type="text" class="form-control required" placeholder="Why us section heading" id="whyus_heading_4" name="whyus_heading_4" value="{{old('whyus_heading_4',@$content->whyus_heading_4)}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="your-mail">
                                                                <label for="whyus_btn_text_4">Why us section 4 Button Text *</label>
                                                                <input type="text" class="form-control required" placeholder="Why us section Button Text" name="whyus_btn_text_4" value="{{old('whyus_btn_text_4',@$content->whyus_btn_text_4)}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="your-mail">
                                                                <label for="whyus_btn_link_4">Why us section 4 link*</label>
                                                                <input type="url" class="form-control required" placeholder="Why us section URL" name="whyus_btn_link_4" value="{{old('whyus_btn_link_4',@$content->whyus_btn_link_4)}}">
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="all_time_sho">
                                                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                                                <div class="your-mail">
                                                                    <label for="whyus_desc_4">Why us section 4 Description*</label>
                                                                    <textarea placeholder="Write Banner description" name="whyus_desc_4" id="whyus_desc_4" class="form-control required">{{old('whyus_desc_4',@$content->whyus_desc_4)}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                                            <div class="your-mail">
                                                                <label for="whyus_image_4">Why us section 4 Image (Recommended size 64 x 64)</label>
                                                                <input type="file" name="whyus_image_4" class="image_input form-control">
                                                                <img src="{{ URL::to('storage/app/public/uploads/category_landing_images/' . @$content->whyus_image_4) }}" style="height: 64px; width: 64px; object-fit: contain; display: {{@$content->whyus_image_4 ? 'block' : 'none' }};" alt="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                {{-- whyus section 4  --}}
                                                <div class="clearfix"></div>



                                                {{-- What We Do --}}
                                                <h1> What We Do</h1>
                                                <div class="col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="whatwedo_heading">What We Do Heading*</label>
                                                        <input type="text" class="form-control required" placeholder="What We Do heading" id="whatwedo_heading" name="whatwedo_heading" value="{{old('whatwedo_heading',@$content->whatwedo_heading)}}">
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="your-mail">
                                                        <label for="whatwedo_point_1">What We Do Point 1 *</label>
                                                        <input type="text" class="form-control required" placeholder="What We Do Point 1" name="whatwedo_point_1" value="{{old('whatwedo_point_1',@$content->whatwedo_point_1)}}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="your-mail">
                                                        <label for="whatwedo_point_2">What We Do Point 2 *</label>
                                                        <input type="text" class="form-control required" placeholder="What We Do Point 2" name="whatwedo_point_2" value="{{old('whatwedo_point_2',@$content->whatwedo_point_2)}}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="your-mail">
                                                        <label for="whatwedo_point_3">What We Do Point 3 *</label>
                                                        <input type="text" class="form-control required" placeholder="What We Do Point 3" name="whatwedo_point_3" value="{{old('whatwedo_point_3',@$content->whatwedo_point_3)}}">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="all_time_sho">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                                        <div class="your-mail">
                                                            <label for="whatwedo_desc">What We Do Description*</label>
                                                            <textarea placeholder="Write What We Do description" name="whatwedo_desc" id="whatwedo_desc" class="form-control required">{{old('whatwedo_desc',@$content->whatwedo_desc)}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>

                                                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-4">
                                                    <div class="your-mail">
                                                        <label for="whatwedo_image">What We Do Image (Recommended size 650 x 650)</label>
                                                        <input type="file" name="whatwedo_image" class="image_input form-control">
                                                        <img src="{{ URL::to('storage/app/public/uploads/category_landing_images/' . @$content->whatwedo_image) }}" style="height: 150px; width: 150px; object-fit: contain; display: {{@$content->whatwedo_image ? 'block' : 'none' }};" alt="">
                                                    </div>
                                                </div>
                                                {{-- end What We Do --}}
                                                <div class="clearfix"></div>



                                                {{-- testimonial  --}}
                                                <h1> Testimonial </h1>
                                                <div class="col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="hide_testimonal">
                                                            <input type="checkbox" id="hide_testimonal" name="hide_testimonal" @if(@$content->hide_testimonal == 'Y') checked @endif >
                                                            Hide Testimonial
                                                        </label>
                                                    </div>
                                                </div>
                                                <div id="show_hide_testimonial_div" @if(@$content->hide_testimonal == 'Y') style="display:none;" @endif >
                                                    <div class="col-lg-12">
                                                        <div class="your-mail">
                                                            <label for="testimonial_section_heading">Testimonial Section Heading*</label>
                                                            <input type="text" class="form-control required" placeholder="Testimonial Section heading" id="testimonial_section_heading" name="testimonial_section_heading" value="{{old('testimonial_section_heading',@$content->testimonial_section_heading)}}">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="all_time_sho">
                                                        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                                            <div class="your-mail">
                                                                <label for="testimonial_section_desc">Testimonial Section Description*</label>
                                                                <textarea placeholder="Testimonial Section description" name="testimonial_section_desc" id="testimonial_section_desc" class="form-control required">{{old('testimonial_section_desc',@$content->testimonial_section_desc)}}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    {{-- end testimonial   --}}
                                                    <div class="clearfix"></div>
                                                    <h1> Testimonial - 1</h1>

                                                    {{-- testimonial 1  --}}
                                                    <div class="col-lg-6">
                                                        <div class="your-mail">
                                                            <label for="testimonial_name_1">Testimonial Section 1 name *</label>
                                                            <input type="text" class="form-control required" placeholder="Testimonial Name" name="testimonial_name_1" value="{{old('testimonial_name_1',@$content->testimonial_name_1)}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="your-mail">
                                                            <label for="testimonial_profession_1">Testimonial Section 1 Profession *</label>
                                                            <input type="text" class="form-control required" placeholder="Testimonial Profession" name="testimonial_profession_1" value="{{old('testimonial_profession_1',@$content->testimonial_profession_1)}}">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="all_time_sho">
                                                        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                                            <div class="your-mail">
                                                                <label for="testimonial_desc_1">Testimonial Section 1 Description*</label>
                                                                <textarea placeholder="Testimonial Section description" name="testimonial_desc_1" id="testimonial_desc_1" class="form-control required">{{old('testimonial_desc_1',@$content->testimonial_desc_1)}}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-6">
                                                        <div class="your-mail">
                                                            <label for="testimonial_file_1">Testimonial Section 1 Image/Video (Recommended size 500 x 300)</label>
                                                            <input type="file" name="testimonial_file_1" class="file_input form-control">
                                                            <img src="{{ URL::to('storage/app/public/uploads/category_landing_images/' . @$content->testimonial_file_1) }}" style="height: 150px; width: 150px; object-fit: contain; display: {{@$content->testimonial_file_1 &&  @$content->testimonial_filetype_1=='I' ? 'block' : 'none' }};" alt="">
                                                            <video controls="" style="height: 200px; width: 300px; object-fit: contain; display: {{@$content->testimonial_file_1 && @$content->testimonial_filetype_1=='V' ? 'block' : 'none' }};">

                                                                <source src="{{ URL::to('storage/app/public/uploads/category_landing_images/' . @$content->testimonial_file_1) }}" type="video/mp4">
                                                            </video>
                                                        </div>
                                                    </div>
                                                    {{-- end testimonial 1  --}}

                                                    <div class="clearfix"></div>

                                                    {{-- testimonial 2  --}}
                                                    <h1> Testimonial - 2</h1>
                                                    <div class="col-lg-6">
                                                        <div class="your-mail">
                                                            <label for="testimonial_name_2">Testimonial Section 2 name *</label>
                                                            <input type="text" class="form-control " placeholder="Testimonial Name" name="testimonial_name_2" value="{{old('testimonial_name_2',@$content->testimonial_name_2)}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="your-mail">
                                                            <label for="testimonial_profession_2">Testimonial Section 2 Profession *</label>
                                                            <input type="text" class="form-control " placeholder="Testimonial Profession" name="testimonial_profession_2" value="{{old('testimonial_profession_2',@$content->testimonial_profession_2)}}">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="all_time_sho">
                                                        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                                            <div class="your-mail">
                                                                <label for="testimonial_desc_2">Testimonial Section 2 Description*</label>
                                                                <textarea placeholder="Testimonial Section description" name="testimonial_desc_2" id="testimonial_desc_2" class="form-control ">{{old('testimonial_desc_2',@$content->testimonial_desc_2)}}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-6">
                                                        <div class="your-mail">
                                                            <label for="testimonial_file_2">Testimonial Section 2 Image/Video (Recommended size 500 x 300)</label>
                                                            <input type="file" name="testimonial_file_2" class="file_input form-control">

                                                            <img src="{{ URL::to('storage/app/public/uploads/category_landing_images/' . @$content->testimonial_file_2) }}" style="height: 150px; width: 150px; object-fit: contain; display: {{@$content->testimonial_file_2 && @$content->testimonial_filetype_2=='I'? 'block' : 'none' }};" alt="">
                                                            <video controls="" style="height: 200px; width: 300px; object-fit: contain; display: {{@$content->testimonial_file_2 && @$content->testimonial_filetype_2=='V' ? 'block' : 'none' }};">

                                                                <source src="{{ URL::to('storage/app/public/uploads/category_landing_images/' . @$content->testimonial_file_2) }}" type="video/mp4">
                                                            </video>
                                                        </div>
                                                    </div>
                                                    {{-- end testimonial 2  --}}
                                                    <div class="clearfix"></div>
                                                    <h1> Testimonial - 3 </h1>
                                                    {{-- testimonial 3  --}}
                                                    <div class="col-lg-6">
                                                        <div class="your-mail">
                                                            <label for="testimonial_name_3">Testimonial Section 3 name *</label>
                                                            <input type="text" class="form-control " placeholder="Testimonial Name" name="testimonial_name_3" value="{{old('testimonial_name_3',@$content->testimonial_name_3)}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="your-mail">
                                                            <label for="testimonial_profession_3">Testimonial Section 3 Profession *</label>
                                                            <input type="text" class="form-control " placeholder="Testimonial Profession" name="testimonial_profession_3" value="{{old('testimonial_profession_3',@$content->testimonial_profession_3)}}">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="all_time_sho">
                                                        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                                            <div class="your-mail">
                                                                <label for="testimonial_desc_3">Testimonial Section 3 Description*</label>
                                                                <textarea placeholder="Testimonial Section description" name="testimonial_desc_3" id="testimonial_desc_3" class="form-control ">{{old('testimonial_desc_3',@$content->testimonial_desc_3)}}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-6">
                                                        <div class="your-mail">
                                                            <label for="testimonial_file_3">Testimonial Section 3 Image/Video (Recommended size 500 x 300)</label>
                                                            <input type="file" name="testimonial_file_3" class="file_input form-control">
                                                            <img src="{{ URL::to('storage/app/public/uploads/category_landing_images/' . @$content->testimonial_file_3) }}" style="height: 150px; width: 150px; object-fit: contain; display: {{@$content->testimonial_file_3 && @$content->testimonial_filetype_3=='I'? 'block' : 'none' }};" alt="">

                                                            <video controls="" style="height: 200px; width: 300px; object-fit: contain; display: {{@$content->testimonial_file_3 && @$content->testimonial_filetype_3=='V'? 'block' : 'none' }};">

                                                                <source src="{{ URL::to('storage/app/public/uploads/category_landing_images/' . @$content->testimonial_file_3) }}" type="video/mp4">
                                                            </video>
                                                        </div>
                                                    </div>
                                                    {{-- end testimonial 3  --}}




                                                    <div class="clearfix"></div>
                                                </div>


                                                {{-- content 1  --}}
                                                <h1> Content 1</h1>
                                                <div class="col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="content_heading_1">Content 1 Heading*</label>
                                                        <input type="text" class="form-control required" placeholder="Content heading" id="content_heading_1" name="content_heading_1" value="{{old('content_heading_1',@$content->content_heading_1)}}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="your-mail">
                                                        <label for="content_btn_text_1">Content 1 Button Text *</label>
                                                        <input type="text" class="form-control required" placeholder="Content Button Text" name="content_btn_text_1" value="{{old('content_btn_text_1',@$content->content_btn_text_1)}}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="your-mail">
                                                        <label for="content_btn_link_1">Content 1 Button URL *</label>
                                                        <input type="url" class="form-control required" placeholder="Content Button URL" name="content_btn_link_1" value="{{old('content_btn_link_1',@$content->content_btn_link_1)}}">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="all_time_sho">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                                        <div class="your-mail">
                                                            <label for="content_desc_1">Content 1 Description*</label>
                                                            <textarea placeholder="Write Content description" name="content_desc_1" id="content_desc_1" class="form-control required">{{old('content_desc_1',@$content->content_desc_1)}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-4">
                                                    <div class="your-mail">
                                                        <label for="content_image_1">Content 1 Image (Recommended size 1200 x 600)</label>
                                                        <input type="file" name="content_image_1" class="image_input form-control">
                                                        <img src="{{ URL::to('storage/app/public/uploads/category_landing_images/' . @$content->content_image_1) }}" style="height: 150px; width: 150px; object-fit: contain; display: {{@$content->content_image_1 ? 'block' : 'none' }};" alt="">
                                                    </div>
                                                </div>
                                                {{-- end content 1  --}}

                                                <div class="clearfix"></div>

                                                {{-- content 2  --}}
                                                <h1> Content 2</h1>
                                                <div class="col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="content_heading_2">Content 2 Heading*</label>
                                                        <input type="text" class="form-control required" placeholder="Content heading" id="content_heading_2" name="content_heading_2" value="{{old('content_heading_2',@$content->content_heading_2)}}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="your-mail">
                                                        <label for="content_btn_text_2">Content 2 Button Text *</label>
                                                        <input type="text" class="form-control required" placeholder="Content Button Text" name="content_btn_text_2" value="{{old('content_btn_text_2',@$content->content_btn_text_2)}}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="your-mail">
                                                        <label for="content_btn_link_2">Content 2 Button URL *</label>
                                                        <input type="url" class="form-control required" placeholder="Content Button URL" name="content_btn_link_2" value="{{old('content_btn_link_2',@$content->content_btn_link_2)}}">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="all_time_sho">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                                        <div class="your-mail">
                                                            <label for="content_desc_2">Content 2 Description*</label>
                                                            <textarea placeholder="Write Content description" name="content_desc_2" id="content_desc_2" class="form-control required">{{old('content_desc_2',@$content->content_desc_2)}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-4">
                                                    <div class="your-mail">
                                                        <label for="content_image_2">Content 2 Image (Recommended size 1200 x 600)</label>
                                                        <input type="file" name="content_image_2" class="image_input form-control">
                                                        <img src="{{ URL::to('storage/app/public/uploads/category_landing_images/' . @$content->content_image_2) }}" style="height: 150px; width: 150px; object-fit: contain; display: {{@$content->content_image_2 ? 'block' : 'none' }};" alt="">
                                                    </div>
                                                </div>
                                                {{-- end content 2  --}}

                                                <div class="clearfix"></div>

                                                {{-- content 3  --}}
                                                <h1> Content 3 </h1>
                                                <div class="col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="content_heading_3">Content 3 Heading*</label>
                                                        <input type="text" class="form-control required" placeholder="Content heading" id="content_heading_3" name="content_heading_3" value="{{old('content_heading_3',@$content->content_heading_3)}}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="your-mail">
                                                        <label for="content_btn_text_3">Content 3 Button Text *</label>
                                                        <input type="text" class="form-control required" placeholder="Content Button Text" name="content_btn_text_3" value="{{old('content_btn_text_3',@$content->content_btn_text_3)}}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="your-mail">
                                                        <label for="content_btn_link_3">Content 3 Button URL *</label>
                                                        <input type="url" class="form-control required" placeholder="Content Button URL" name="content_btn_link_3" value="{{old('content_btn_link_3',@$content->content_btn_link_3)}}">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="all_time_sho">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                                        <div class="your-mail">
                                                            <label for="content_desc_3">Content 3 Description*</label>
                                                            <textarea placeholder="Write Content description" name="content_desc_3" id="content_desc_3" class="form-control required">{{old('content_desc_3',@$content->content_desc_3)}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-4">
                                                    <div class="your-mail">
                                                        <label for="content_image_3">Content 3 Image (Recommended size 1200 x 600)</label>
                                                        <input type="file" name="content_image_3" class="image_input form-control">
                                                        <img src="{{ URL::to('storage/app/public/uploads/category_landing_images/' . @$content->content_image_3) }}" style="height: 150px; width: 150px; object-fit: contain; display: {{@$content->content_image_3 ? 'block' : 'none' }};" alt="">
                                                    </div>
                                                </div>
                                                {{-- end content 3  --}}
                                                <div class="clearfix"></div>

                                                {{-- content 4  --}}
                                                <h1> Content 4</h1>
                                                <div class="col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="content_heading_4">Content 4 Heading*</label>
                                                        <input type="text" class="form-control required" placeholder="Content heading" id="content_heading_4" name="content_heading_4" value="{{old('content_heading_4',@$content->content_heading_4)}}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="your-mail">
                                                        <label for="content_btn_text_4">Content 4 Button Text *</label>
                                                        <input type="text" class="form-control required" placeholder="Content Button Text" name="content_btn_text_4" value="{{old('content_btn_text_4',@$content->content_btn_text_4)}}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="your-mail">
                                                        <label for="content_btn_link_4">Content 4 Button URL *</label>
                                                        <input type="url" class="form-control required" placeholder="Content Button URL" name="content_btn_link_4" value="{{old('content_btn_link_4',@$content->content_btn_link_4)}}">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="all_time_sho">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                                        <div class="your-mail">
                                                            <label for="content_desc_4">Content 4 Description*</label>
                                                            <textarea placeholder="Write Content description" name="content_desc_4" id="content_desc_4" class="form-control required">{{old('content_desc_4',@$content->content_desc_4)}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-4">
                                                    <div class="your-mail">
                                                        <label for="content_image_4">Content 4 Image (Recommended size 1200 x 600)</label>
                                                        <input type="file" name="content_image_4" class="image_input form-control">
                                                        <img src="{{ URL::to('storage/app/public/uploads/category_landing_images/' . @$content->content_image_4) }}" style="height: 150px; width: 150px; object-fit: contain; display: {{@$content->content_image_4 ? 'block' : 'none' }};" alt="">
                                                    </div>
                                                </div>
                                                {{-- end content 4  --}}






                                                <div class="col-lg-12">
                                                    <div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
                                                        <input value="{{@$content->id?'Update':'Save'}}" type="submit" class="btn btn-default">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Row -->
            </div>
            <!-- container -->
        </div>
    </div>
@endsection
@section('footer')
@include('admin.includes.footer')
<script>
    $(document).ready(function(){
        $("#myform").validate({
        });
    });

    $('#parent_id').change(function(){
        if($('#parent_id').val()!=""){
            $('#img').hide();
            $('#meta_desc').hide();
            $('#meta_tit').hide();
        }
        else{
            $('#img').show();
            $('#meta_desc').show();
            $('#meta_tit').show();
        }

    });

</script>
<script>
	$(document).ready(function(){
		$("#myform").validate();
		tinyMCE.init({
            mode : "specific_textareas",
            editor_selector : "description",
            plugins: [
              'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
              'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
              'save table contextmenu directionality emoticons template paste textcolor'
            ],
            relative_urls : false,
            remove_script_host : false,
            convert_urls : true,
            toolbar: 'insertfile undo redo | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor emoticons',
            images_upload_url: '{{ URL::to('storage/app/public/uploads/content_image/') }}',
            images_upload_handler: function(blobInfo, success, failure) {
                var formD = new FormData();
                formD.append('file', blobInfo.blob(), blobInfo.filename());
                formD.append( "_token", '{{csrf_token()}}');
                $.ajax({
                    url: '{{ route('artical.img.upload') }}',
                    data: formD,
                    type: 'POST',
                    contentType: false,
                    cache: false,
                    processData:false,
                    dataType: 'JSON',
                    success: function(jsn) {
                        if(jsn.status == 'ERROR') {
                            failure(jsn.error);
                        } else if(jsn.status == 'SUCCESS') {
                            success(jsn.location);
                        }
                    }
                });
            },
        });
	});
</script>
<script>
	$(document).ready(function(){
        const regex = new RegExp("(.*?)\.(png|jpg|jpeg)$");
        const regex1 = new RegExp("(.*?)\.(png|jpg|jpeg|mp4)$");
        $('.file_input').on('change', function() {
            const val = $(this).val().toLowerCase();
            if (!(regex1.test(val))) {
                $(this).val('');
                alert('Only png ,jpeg and mp4 files are allowed.');
            }
            var extension = val.substr( (val.lastIndexOf('.') +1) );
            console.log(extension)
            if(this.files && this.files[0]&& extension=='mp4'){
                var reader = new FileReader();
                const t = $(this);
                reader.onload = function(e) {
                    // console.log(e.target.result);
                    t.nextAll('video').html('');

                    const video =t.nextAll('video');
                    const videoSource = document.createElement('source');
                    videoSource.setAttribute('src', e.target.result);
                    video.append(videoSource);
                    video.load();
                    // video.play();
                    // var htmlVideo= `<source src="`+e.target.result+`" type="video/mp4">`


                    // t.nextAll('video').html(htmlVideo);
                    t.nextAll('video').show();
                    // t.nextAll('sourse').attr('src', e.target.result).show();
                    t.nextAll('img').hide();
                    // video.appendChild(videoSource);
                    // videoSource.setAttribute('src', e.target.result);
                    // video.appendChild(videoSource);
                    // video.load();
                    // video.play();
                }
                reader.readAsDataURL(this.files[0]);
            }
            if (this.files && this.files[0] && extension!='mp4') {
                var reader = new FileReader();
                const t = $(this);
                reader.onload = function(e) {
                    t.nextAll('video').hide();
                    t.nextAll('img').attr('src', e.target.result).show();
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
        $('.image_input').on('change', function() {
            const val = $(this).val().toLowerCase();
            if (!(regex.test(val))) {
                $(this).val('');
                alert('Only png and jpeg files are allowed.');
            }
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                const t = $(this);
                reader.onload = function(e) {
                    t.next('img').attr('src', e.target.result).show();
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
        $('#hide_testimonal').click(function(){
            if($(this).prop("checked") == true){
                // console.log("Checkbox is checked.");
                $('#show_hide_testimonial_div').hide();
            }
            else if($(this).prop("checked") == false){
                // console.log("Checkbox is unchecked.");
                $('#show_hide_testimonial_div').show();
            }
        });
	});
</script>
@endsection
