@extends('admin.layouts.app')
@section('title', 'Netbhe|Add Category')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="pull-left page-title">Add Category</h4>
                        <div class="submit-login no_mmg pull-right">
                            <a href="{{ route('admin.categories') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
                        </div>
                    </div>
                </div>

                @include('admin.includes.error')
                {{-- {{ dd(old()) }} --}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body table-rep-plugin">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12 nhp">
                                        <div class="table-responsive" data-pattern="priority-columns">
                                            <form id="myform" method="post" action="{{ route('admin.store.categories')}}" enctype="multipart/form-data">
                                                @csrf

                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Category Name</label>
                                                        <input type="text" name="name" class="form-control required" id="cat_name" value="{{ old('name') }}">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Parent Category </label>
                                                        <select class="form-control newdrop " name="parent_id" id="parent_id">
                                                            <option value="">Choose Category</option>
                                                            @foreach($cat as $row)
                                                            <option value="{{$row->id}}" @if(old('parent_id')==$row->id)selected @endif>{{$row->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <span>
                                                        (Leave this field for parent category)
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="meta_tit">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Meta Title</label>
                                                        <input type="text" class="form-control required" id="meta_title" placeholder="" name="meta_title" value="{{ old('meta_title') }}">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <!--all_time_sho-->
                                                <div class="all_time_sho">
                                                    <div class="col-md-6 col-sm-12 col-xs-12 col-lg-6" id="meta_desc">
                                                        <div class="your-mail">
                                                            <label for="exampleInputEmail1">Meta Description</label>
                                                            <textarea placeholder="" id="meta_description" name="meta_description"rows="3" class="form-control message required" value="{{old('meta_description') }}"></textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>

                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 image_section" id="img">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Image</label>
                                                        <input type="file" class="form-control required" id="file" placeholder="Image" name="image" accept="image/*">
                                                        <span>
                                                        (Image  should be 276x183)
                                                        </span><div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                
                                                <div class="clearfix"></div>
                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                    <div class="add_btnm submit-login">
                                                        <input value="Add" type="submit" class="btn btn-default">
                                                    </div>
                                                </div>
                                                <!--all_time_sho-->
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Row -->
            </div>
            <!-- container -->
        </div>
    </div>
@endsection
@section('footer')
@include('admin.includes.footer')
<script>
    $(document).ready(function(){
        $("#myform").validate({
            messages : 
            {
                name                : "Please enter category name",
                meta_title          : "Please enter meta title",
                meta_description    : "Please enter meta description",
                description         : "Please enter description",
                image               : "Please enter category image",
            }
        });
    });
    
    $('#parent_id').change(function(){
        if($('#parent_id').val()!=""){
            $('#img').hide();
            $('#meta_desc').hide();
            $('#meta_tit').hide();
        }
        else{
            $('#img').show();
            $('#meta_desc').show();
            $('#meta_tit').show();
        }
        
    });
</script>
@endsection
