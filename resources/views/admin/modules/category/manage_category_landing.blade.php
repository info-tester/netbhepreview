@extends('admin.layouts.app')
@section('title', 'Netbhe| Manage Category Landing Page')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')







<!-- Begin page -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="pull-left page-title">Manage Category Landing Page</h4>
                        <div class="submit-login no_mmg pull-right">
                            <a href="{{ route('admin.category.landing.add') }}" title="Back"><button type="button" class="btn btn-default">Add Category Landing Page</button></a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <!--<div class="panel-heading">
                                <h3 class="panel-title">Default Example</h3>
                                </div>-->
                            <div class="panel-body table-rep-plugin">
                                <div class="row top_from">
                                    <form action="{{ route('admin.category.landing.index') }}" method="post" id="myFormSearch" name="myFormSearch">
                                        @csrf
                                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">Category</label>
                                                <select class="form-control newdrop" name="category_id" id="category_id">
                                                    <option value="">Select Category</option>
                                                    @foreach($p_cat as $pt)
                                                    <option value="{{ $pt->id }}" @if(@$key['category_id']==$pt->id) selected @endif>{{ $pt->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="add_btnm exrt">
                                                <input class="nm9 xxx" value="Search" type="button">
                                            </div>
                                        </div>
                                    </form>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 dess5">
                                        <i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
                                        <i class="fa fa-clipboard cncl" aria-hidden="true"> <span class="cncl_oopo">Copy</span></i>
                                        <i class="fa fa-eye cncl" aria-hidden="true"> <span class="cncl_oopo">View</span></i>
                                        <i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active</span></i>
                                        <i class="fa fa-times cncl" aria-hidden="true"> <span class="cncl_oopo">Inactive</span></i>
                                        <i class="fa fa-square cncl" aria-hidden="true"> <span class="cncl_oopo">Show in Footer</span></i>
                                        <i class="fa fa-square-o cncl" aria-hidden="true"> <span class="cncl_oopo"> Not Show in Footer</span></i>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="clearfix"></div>

                                    <div class="clearfix"></div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" data-pattern="priority-columns">
                                            <table id="datatable" class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th> Category Name</th>
                                                        <th> Status</th>
                                                        <th> Show in Footer</th>
                                                        <th style="min-width:72px;">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($categoryLanding as $ct)
                                                    <tr>
                                                        <td>
                                                            {{@$ct->categoryDetails->name}}
                                                        </td>
                                                        <td>
                                                            @if(@$ct->status == 'A')
                                                            Active
                                                            @else
                                                            Inactive
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if(@$ct->show_in_footer == 'Y')
                                                            Yes
                                                            @elseif(@$ct->show_in_footer == 'N')
                                                            No
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('admin.category.landing.edit',['id'=>$ct->id]) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
                                                            <a href="javascript:;" title="Copy Link" onclick="copyText(`{{url('/')}}/categoria-landing-page/{{@$ct->categoryDetails->slug}}`)"> <i class="fa fa-clipboard delet" aria-hidden="true"></i></a>
                                                            <a href="{{route('category.landing.page.f',['slug'=>@$ct->categoryDetails->slug])}}" title="View" target="_blank"> <i class="fa fa-eye delet" aria-hidden="true"></i></a>
                                                            @if(@$ct->status == 'A')
                                                                <a href="{{ route('admin.category.landing.status',['id'=>$ct->id]) }}" onclick="return confirm('Do you really want to Inactivate this category landing page?')" title="Inactive"> <i class="fa fa-times delet" aria-hidden="true"></i></a>
                                                            @else
                                                                <a href="{{ route('admin.category.landing.status',['id'=>$ct->id]) }}" onclick="return confirm( 'Do you really want to Activate this category landing page?')" title="Active"> <i class="fa fa-check delet" aria-hidden="true"></i></a>
                                                            @endif
                                                            <a href="{{route('admin.category.landing.delete',['id'=>@$ct->id])}}" onclick="return confirm( 'Do you really want to delete this category landing page?')" title="Delete"> <i class="fa fa-trash delet" aria-hidden="true"></i></a>
                                                            @if(@$ct->show_in_footer == 'Y')
                                                            <a href="{{route('admin.category.landing.show.footer',['id'=>@$ct->id])}}" onclick="return confirm( 'Do you this category landing page not show in footer ?')" title="Not Show in Footer"> <i class="fa fa-square" aria-hidden="true"></i></a>
                                                            @elseif(@$ct->show_in_footer == 'N')
                                                            <a href="{{route('admin.category.landing.show.footer',['id'=>@$ct->id])}}" onclick="return confirm( 'Do you this category landing page show in footer?')" title="Show in Footer"> <i class="fa fa-square-o" aria-hidden="true"></i></a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Row -->
            </div>
            <!-- container -->
        </div>
        <!-- content -->
        <!--<footer class="footer text-right">
            2015 © Moltran.
            </footer>-->
    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->
    <!-- Right Sidebar -->

</div>



<!-- END wrapper -->
<!--Raise  popup-->
@endsection
@section('footer')
@include('admin.includes.footer')
    <script>
        $('.xxx').click(function(){
            $("#myFormSearch").submit();
        });
        function copyText(text){
            var input = document.createElement('input');
            input.setAttribute('value', text);
            document.body.appendChild(input);
            input.select();
            var result = document.execCommand('copy');
            document.body.removeChild(input);
            toastr.success("Link Copied");
            return result;
        }
    </script>
@endsection
