@extends('admin.layouts.app')
@section('title', 'Netbhe | Add Specialty')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="pull-left page-title">Add Specialty</h4>
                        <div class="submit-login no_mmg pull-right">
                            <a href="{{ route('admin.specialty') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
                        </div>
                    </div>
                </div>

                @include('admin.includes.error')

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body table-rep-plugin">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12 nhp">
                                        <div class="table-responsive" data-pattern="priority-columns">
                                            <form id="myform" method="post" action="{{ @$specialty ? route('admin.specialty.edit', ['id' => $specialty->id]) : route('admin.specialty.add') }}">
                                                @csrf
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Specialty Name</label>
                                                        <input type="text" name="name" class="form-control required" id="name" value="{{ old('name', (@$specialty->name)) }}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">Entry Info Message</label>
                                                        <input type="text" name="entry_info_message" class="form-control required" id="entry_info_message" value="{{ old('entry_info_message', (@$specialty->entry_info_message)) }}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="add_btnm submit-login">
                                                        <input value="Save" type="submit" class="btn btn-default">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Row -->
            </div>
            <!-- container -->
        </div>
    </div>
@endsection
@section('footer')
@include('admin.includes.footer')
<script>
    $(document).ready(function(){
        $("#myform").validate({
            messages :
            {
                name                : "Please enter specialty name",
                entry_info_message  : "Please enter info message",
                meta_description    : "Please enter meta description",
                description         : "Please enter description",
                image               : "Please enter category image",
            }
        });
    });

    $('#parent_id').change(function(){
        if($('#parent_id').val()!=""){
            $('#img').hide();
            $('#meta_desc').hide();
            $('#meta_tit').hide();
        }
        else{
            $('#img').show();
            $('#meta_desc').show();
            $('#meta_tit').show();
        }

    });
</script>
@endsection
