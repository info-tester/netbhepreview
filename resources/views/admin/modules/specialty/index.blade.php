@extends('admin.layouts.app')
@section('title', 'Netbhe | Professional Specialties')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
<div class="content">
    <div class="container">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h3 class="pull-left page-title">Professional Specialties</h3>
                <div class="submit-login no_mmg pull-right">
                    <a href="{{ route('admin.specialty.add') }}" title="Add Category"><button type="button" class="btn btn-default">Add Specialties</button></a>
                </div>
            </div>
        </div>

        @include('admin.includes.error')

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-body table-rep-plugin">
                        <div class="row">
                            <div class="col-md-12 dess5">
                                <i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
                                <i class="fa fa-trash cncl"> <span class="cncl_oopo">Delete</span></i>
                                {{-- <i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active</span></i>
                                <i class="fa fa-times cncl" aria-hidden="true"> <span class="cncl_oopo">Inactive</span></i>
                                <i class="fa fa-square cncl" aria-hidden="true"> <span class="cncl_oopo"> Not Show in Home Page</span></i>
                                <i class="fa fa-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Show in Home Page</span></i> --}}
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" data-pattern="priority-columns">
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Specialty</th>
                                                <th>Entry Info Message</th>
                                                <th style="min-width:72px;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($specialties as $ct)
                                                <tr>
                                                    <td>{{ $ct->name }}</td>
                                                    <td>{{ $ct->entry_info_message }}</td>

                                                    <td>
                                                        <a href="{{ route('admin.specialty.edit', ['id' => $ct->id]) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
                                                        <a href="{{ route('admin.specialty.delete', ['id' => $ct->id]) }}" onclick="return confirm('Are you really want to delete this specialty?')" title="Delete"> <i class="fa fa-trash delet"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Row -->
    </div>
    <!-- container -->
</div>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
    <script>
        $('.xxx').click(function(){
            $("#myFormSearch").submit();
        });
    </script>
@endsection
