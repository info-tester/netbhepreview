@extends('admin.layouts.app')
@section('title', @$title)
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="all_body">
	<div class="container"> 
		<div class="row">
		  <div class="sucstxt" style="display: block; width: 100%; text-align: center; margin-top: 60px; margin-bottom: 60px; height: 240px;"> 
		    <img src="{{ url('public/images/'.@$img)}}" alt="">
		    <h2>{{@$heading}}</h2>
		    <p>{{@$message}}</p>
		  </div>
		</div>
	</div>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection