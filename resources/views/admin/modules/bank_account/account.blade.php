@extends('admin.layouts.app')
@section('title', 'Netbhe | Admin | Bank Account')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Manage Account</h4>
                    {{-- <div class="submit-login no_mmg pull-right">
                        <a href="{{ route('admin.coupon.create') }}" title="Create"><button type="button"
                                class="btn btn-default">Add</button></a>
                    </div> --}}
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-rep-plugin">
                            <div class="row">

                        <div class="col-md-12 dess5">
                            <i class="fa fa-pencil-square-o cncl" aria-hidden="true" style="border: none;"> <span
                                    class="cncl_oopo">Edit</span></i>

                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="table-responsive" data-pattern="priority-columns">
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Bank Information</th>
                                            <th>Beneficiary</th>
                                            <th>Agency</th>
                                            <th>Current Account</th>
                                            <th>CNPJ</th>
                                            <th>Instruction</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach(@$bankAccount as $row)
                                        <tr>
                                            <td>{{ @$row->bank_information }} </td>
                                            <td>{{ @$row->beneficiary }} </td>
                                            <td>{{ @$row->agency }} </td>
                                            <td>{{ @$row->current_account }} </td>
                                            <td>{{ @$row->cnpj }} </td>
                                            <td>{{ @$row->instruction }} </td>
                                            <td>
                                                <a href="{{ route('admin.account.edit',[@$row->id]) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>

{{-- <form method="post" id="destroy">
	{{ csrf_field() }}
{{method_field('delete')}}
</form>
<script>
    function deleteCategory(val){
	var confirm = window.confirm('Are you want to delete this coupon ?');
	if(confirm){
		$("#destroy").attr('action',val);
		$("#destroy").submit();
	}
 }
</script> --}}
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
