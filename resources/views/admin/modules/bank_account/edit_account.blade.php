@extends('admin.layouts.app')
@section('title', 'Netbhe | Admin | Bank Account')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Edit Information</h4>
                    <div class="submit-login no_mmg pull-right">
                        <a href="{{ route('admin.account') }}" title="Back"><button type="button"
                                class="btn btn-default">Back</button></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-rep-plugin">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 nhp">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <form id="myform" method="post"
                                            action="{{route('admin.account.update',['id'=>@$bankAccount->id])}}"
                                            enctype="multipart/form-data">
                                            @csrf

                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="bank_information">Bank : </label>
                                                    <input type="text" name="bank_information" id="bank_information" class="form-control required" value="{{old('bank_information',@$bankAccount->bank_information)}}">
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="beneficiary">Beneficiary : </label>
                                                    <input type="text" name="beneficiary" id="beneficiary" class="form-control required" value="{{old('beneficiary',@$bankAccount->beneficiary)}}">
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="agency">Agency : </label>
                                                    <input type="text" name="agency" id="agency" class="form-control required" value="{{old('agency',@$bankAccount->agency)}}">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="cnpj">CNPJ : </label>
                                                    <input type="text" name="cnpj" id="cnpj" class="form-control required" value="{{old('cnpj',@$bankAccount->cnpj)}}">
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="current_account">Current Account : </label>
                                                    <input type="text" name="current_account" id="current_account" class="form-control required" value="{{old('current_account',@$bankAccount->current_account)}}">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="instruction">Instruction : </label>
                                                    <textarea placeholder="About us long description" name="instruction" id="instruction" class="short_description form-control message required">{!! @$bankAccount->instruction !!}</textarea>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>

                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="add_btnm submit-login">
                                                    <input value="Update" type="submit"
                                                        class="btn btn-default">
                                                </div>
                                            </div>
                                            <!--all_time_sho-->
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Row -->
        </div>
        <!-- container -->
    </div>
    <!-- content -->
</div>
<script>
    $(document).ready(function(){
		$("#myform").validate({
			messages : {
				bank_information 	: {
					required 		: "Please enter Bank Information.",
				},
				instruction : {
					required 		: "Please enter Instruction.",
				}
	        },
	        errorPlacement: function (error, element)
	        {
	            // toastr.error(error.text());
                error.insertAfter(element);
	        }
		});
	});
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
