@extends('admin.layouts.app')
@section('title', 'Netbhe| Admin |Details')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->



            <div class="col-sm-12">
                <h4 class="pull-left page-title">Tool certificate template details</h4>
                <div class="submit-login no_mmg pull-right">
                    <a href="{{ route('tool-certificate-template.edit',[$details->id]) }}" title="Edit">
                        <button type="button" class="btn btn-default">Edit</button>
                    </a>
                    <a href="{{ route('tool-certificate-template.index') }}" title="Back">
                        <button type="button" class="btn btn-default">Back</button>
                    </a>
                </div>              
            </div>




          
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        
                        <div class="panel-body">
                            <div class="row">
                                
                                <div class="col-lg-12 col-md-12">
                                    <h4 class="pull-left page-title">Details</h4>
                                    <div class="order-detail">
                                        <!--order-detail start-->
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="order-id">
                                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 no_padd_lft">
                                                    <p>Title :</p>
                                                </div>
                                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-8">
                                                    <span>{{ $details->title }}</span>
                                                </div>
                                                <div class="clearfix"></div>

                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padd_lft">
                                                    <p>Description :</p>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div>
                                                    {!! $details->certificate_description !!}
                                                </div>
                                                <div class="clearfix"></div>

                                                

                                            </div>
                                        </div>
                                        

                                    </div>
                                    <!--order-detail end-->
                                </div>
                                <div class="col-md-12">
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            
                            
                            
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Row -->
    </div>
    <!-- container -->
</div>
<!-- content -->
</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
<!-- Right Sidebar -->
<!-- /Right-bar -->
</div>
<script>
    function submitMy(id){
      // alert(id);
      $('#profid').val(id);
      $('#myForm2').submit();
    }
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection