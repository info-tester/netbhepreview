@extends('admin.layouts.app')
@section('title', 'Netbhe | Admin | Dashboard')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Dashboard</h4>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        
                        <div class="content">
                            <div class="container">
                                
                                <div class="row" style="margin-top: 19px;">
                                    <h1 align="center" style="line-height: 400px; font-size: 25px; color: #7f7f7f;">Welcome to Marketplace Admin</h1>
                                    
                                </div>
                            </div>
                            <!-- container -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- container -->
    </div>
    <!-- content -->
    {{-- <footer class="footer text-right">
        {{ date('Y') }} &copy; NearO.in
    </footer> --}}
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection