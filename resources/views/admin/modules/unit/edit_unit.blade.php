@extends('admin.layouts.app')
@section('title', 'NearO | Admin | Unit Update')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
<!-- Start content -->
<div class="content">
	<div class="container">
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 class="pull-left page-title">Edit Unit</h4>
				<div class="submit-login no_mmg pull-right">
					<a href="{{ route('unit.index') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
										<form id="myform" method="post" action="{{ route('unit.update',[$unit->id])}}">
											<input name="_method" type="hidden" value="PUT">
											{{ csrf_field() }}
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Unit Name</label>
													<input type="text" value="{{ $unit->name }}" name="name" class="form-control required" id="name">
												</div>
											</div>
											{{-- 
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Parent Category </label>
													<select class="form-control newdrop required" name="parent_id" id="parent_id">
														<option value="">Choose Category</option>
														@foreach($parent as $row)
														<option value="{{$row->id}}" @if($category->parent_id == $row->id) selected @endif>{{$row->name}}</option>
														@endforeach
													</select>
													<span>
													(Leave this field for parent category)
													</span>
												</div>
											</div>
											--}}
											{{-- 
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Meta Title</label>
													<input type="text" class="form-control required" id="meta_title" placeholder="" value="{{ $category->meta_title }}" name="meta_title">
												</div>
											</div>
											<!--all_time_sho-->
											<div class="all_time_sho">
												<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Meta Description</label>
														<textarea placeholder="" id="meta_description" name="meta_description" rows="3" class="form-control message required">{{ $category->meta_description }}</textarea>
													</div>
												</div>
												<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Description</label>
														<textarea placeholder="" id="description" name="description" rows="3" class="form-control message required">{{ $category->description }}</textarea>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 image_section">
													<div class="your-mail">
														<label for="exampleInputEmail1">Image</label>
														<input type="file" class="form-control" id="file" placeholder="Image" name="image" accept="image/*">
														<span>
														(Image dieantion should be 100*100 to 200*200)
														</span>
													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 image_section">
													<div class="your-mail">
														<span><img src="{{ url('public/uploads/category/'.$category->image) }}" alt="" style="height: 100px; width: 100px;"></span>
													</div>
												</div>
											</div>
											--}}
											<div class="clearfix"></div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="add_btnm submit-login">
													<input value="Update" type="submit" class="btn btn-default">
												</div>
											</div>
											<!--all_time_sho-->
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>


<script>
	$(document).ready(function(){
		$("#myform").validate({
			messages : 
			{
				name : "Please enter unit name",
	        },
	        errorPlacement: function (error, element) 
	        {
	            toastr.error(error.text());
	        }
		});
	});
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection