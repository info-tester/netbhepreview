@extends('admin.layouts.app')
@section('title', 'netbhe.com | Admin | Edit Testimonial')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Edit Testimonial</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.testimonial') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
										<form id="myform" method="post" action="{{ route('admin.update.testimonial',[@$testi->id]) }}" enctype="multipart/form-data">
											@csrf
											<input type="hidden" name="id" value="{{@$testi->id}}">
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Customer name*</label>
													<input type="text" class="form-control required" placeholder="Added By" name="name" value="{{@$testi->name}}">
												</div>
											</div>

											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Location*</label>
													<input type="text" class="form-control required" placeholder="Location" name="location" value="{{@$testi->location}}">
												</div>
											</div>
											{{-- <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Title 1*</label>
													<input type="text" class="form-control required" placeholder="Content Title" name="title_1" value="{{@$testi->title_1}}">
												</div>
											</div> --}}
											
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Image</label>
													<input type="file" class="form-control" id="file" placeholder="Image" name="image" accept="image/*">
												</div>
											</div>
											
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Image View</label>
													<img src="{{ url('storage/app/public/testimonial/'.@$testi->image) }}" alt="" style="width: 200px; height: 100px;">
												</div>
											</div>
											{{-- <div class="clearfix"></div> --}}
											
											{{-- <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Meta Description*</label>
													<input type="text" class="form-control required" placeholder="Meta Description" name="meta_desc" value="{{@$testi->meta_desc}}">
												</div>
											</div> --}}
											<div class="clearfix"></div>
											<div class="all_time_sho">
												<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Comment* </label>
														<textarea placeholder="Comment" name="comment" class="form-control message required">{!!strip_tags(nl2br(@$testi->comment), '<br>') !!}</textarea>
													</div>
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="add_btnm submit-login">
													<input value="Update" type="submit" class="btn btn-default">
												</div>
											</div>
											<!--all_time_sho-->
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>
<script>
	$(document).ready(function(){
		$("#myform").validate({
			errorPlacement: function (error, element)
	        {
	            // toastr.error(error.text());
                error.insertAfter(element);
	        }
		});
	});
</script>

{{-- <script language="JavaScript" type="text/javascript" src="{{url('public/tiny_mce/tiny_mce.js')}}"></script>
<script language="JavaScript" type="text/javascript">
    tinyMCE.init({
        // General options
        mode : "textareas",
		forced_root_block : "",
        height:"550px",
        theme : "advanced",
        editor_deselector : "mceNoEditor",
		relative_urls:false,
        plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
         file_browser_callback : "filebrowser",
        // Theme options
        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,search,replace,|,styleprops",
        theme_advanced_buttons2 : "styleselect,formatselect,fontselect,fontsizeselect,|,help,code,|,forecolor,backcolor",
        theme_advanced_buttons3 : "cut,copy,paste,pastetext,pasteword,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,|,insertdate,inserttime,preview",
       
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        //theme_advanced_resizing : true,

        // Example content CSS (should be your site CSS)
        /*content_css : "csseditor/content.css",*/

        // Drop lists for link/image/media/template dialogs
        template_external_list_url : "lists/template_list.js",
        external_link_list_url : "lists/link_list.js",
        external_image_list_url : "lists/image_list.js",
        media_external_list_url : "lists/media_list.js",

        // Replace values for the template plugin
        template_replace_values : {
            username : "Some User",
            staffid : "991234"
        }
		//height:"500px",
        //width:"600px"
});
 function filebrowser(field_name, url, type, win)
 {
    fileBrowserURL = "{{ url('') }}/public/tiny_mce/plugins/pdw_file_browser/index.php?editor=tinymce&filter=" + type;
    tinyMCE.activeEditor.windowManager.open({
        title: "PDW File Browser",
        url: fileBrowserURL,
        width: 950,
        height: 650,
        inline: 0,
        maximizable: 1,
        close_previous: 0
    },{
        window : win,
        input : field_name
    });

 }
 //End TinyMCE

</script> --}}
<style>
	.error {
		color: red !important;
	}
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection