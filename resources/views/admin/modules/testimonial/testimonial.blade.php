@extends('admin.layouts.app')
@section('title', 'netbhe.com | Admin | Testimonial')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Manage Testimonial</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.add.testimonial') }}" title="Create"><button type="button" class="btn btn-default">Add Testimonial</button></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
                            <div class="row">
                                <form action="{{ route('admin.testimonial') }}" method="POST">
                                    @csrf
                                    <div class="col-sm-12">
                                        <h4 class="pull-left" style="margin-top: 24px;">Home page Testimonial Content</h4>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                        <div class="your-mail">
                                            <label for="exampleInputEmail1">Title *</label>
                                            <input type="text" class="form-control required" placeholder="Write title" name="title" value="{{@$testiContent->title}}">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="all_time_sho">
                                        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">Description *</label>
                                                <textarea placeholder="Write description" name="description" id="meet_description" rows="6" class="form-control required">{!! @$testiContent->description !!}</textarea>
                                            </div>
                                            <p class="meet_description" id="cntnt"></p>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                        <div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
                                            <input value="Update" type="submit" class="btn btn-default">
                                        </div>
                                    </div>
                                </form>
                            </div>
							<div class="row">
								<div class="col-md-12 dess5">
									<i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
									<i class="fa fa-trash-o cncl" style="border: none;" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>

								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Image</th>
													<th>Location</th>
													<th>Description</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@foreach(@$testi as $detail)
												<tr>
													<td width="100px;"><img src="{{ url('storage/app/public/testimonial/'.@$detail->image) }}" style="height: 100px;"></td>
													<td>{{ @$detail->location }}</td>
													<td>{!! strip_tags(nl2br(@$detail->comment), '<br>') !!}</td>

													<td>
														<a href="{{ route('admin.edit.testimonial',[@$detail->id]) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
														<a href="javascript:void(0)" onclick="if (confirm('Do you want to delete this testimonial?')) { location.href='{{route('admin.delete.testimonial',[@$detail->id])}}' } else { return false; };" title="Delete"> <i class="fa fa-trash-o delet" aria-hidden="true"></i></a>

													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<form method="post" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
	var confirm = window.confirm('Are you want to delete this content ?');
	if(confirm){
		$("#destroy").attr('action',val);
		$("#destroy").submit();
	}
 }
</script>

@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
