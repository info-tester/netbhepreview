@extends('admin.layouts.app')
@section('title', 'Vedic Astro World | Admin | Currency Conversion')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<style type="text/css" media="screen">
.error{
	color:#f00 !important;
}	
</style>
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Currency Conversion</h4>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								{{-- <div class="panel panel-default show_edit_form" style="display: none;">
									<div class="panel-body table-rep-plugin">
										<form method="post" id="myform" action="{{ route('admin.membership.update') }}">
											@csrf

											<h3>Edit plan price</h3>
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="your-mail">
													<label for="exampleInputEmail1">Currency</label>
													<select class="form-control newdrop required" id="currency_id" name="currency_id" disabled>
														<option value="1">INR</option>
														<option value="2">USD</option>
														<option value="3">EURO</option>
													</select>
													<input type="hidden" name="id" id="plan_id">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="your-mail">
													<label for="exampleInputEmail1">Yearly Fees</label>
													<input class="form-control required" id="yearly_fees" name="yearly_fees" placeholder="Yearly Fees" value="" type="text">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="your-mail">
													<label for="exampleInputEmail1">One Time Fees</label>
													<input class="form-control required" id="one_time_fees" name="one_time_fees" placeholder="One Time Fees" value="" type="text">
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												<div class="submit-login add_btnm" style="width:auto; margin-top:0px;">
													<input value="Update" class="btn btn-default" type="submit">
												</div>
											</div>
										</form>
									</div>
								</div> --}}
								{{-- <div class="col-md-12 dess5">
									<i class="fa fa-pencil-square-o cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">Edit</span></i>
								</div> --}}
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>From Currency</th>
													<th>To Currency</th>
													<th>Converted Amount</th>
													<th>Last Update</th>
												</tr>
											</thead>
											<tbody>
												@foreach($conversion as $row)
												<tr>
													<td>@if($row->from_currency_id == 1) 1 INR @elseif($row->from_currency_id == 2) 1 USD @elseif($row->from_currency_id == 3) 1 EUR @endif</td>
													<td>@if($row->to_currency_id == 1) INR @elseif($row->to_currency_id == 2) USD @elseif($row->to_currency_id == 3) EUR @endif</td>
													<td>{{ $row->conv_factor }} @if($row->to_currency_id == 1) INR @elseif($row->to_currency_id == 2) USD @elseif($row->to_currency_id == 3) EUR @endif</td>
													<td>{{ date('jS-M-Y h:m a' ,strtotime($row->updated_at)) }}</td>
													
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
{{-- <script type="text/javascript">
	$(document).ready(function() {
		$('.edit_plan').click(function(event) {
			var id = $(this).data('id');
			var currency_id = $(this).data('currency_id');
			var yearly_fees = $(this).data('yearly_fees');
			var one_time_fees = $(this).data('one_time_fees');
			$('#currency_id').val(currency_id);
			$('#yearly_fees').val(yearly_fees);
			$('#one_time_fees').val(one_time_fees);
			$('#plan_id').val(id);
			$('.show_edit_form').show();
		});
		$('#myform').validate({
			rules:{
				yearly_fees:{
                    required:true,
                    number:true,
                    min:1
                },
				one_time_fees:{
                    required:true,
                    number:true,
                    min:1
                }
			},
			errorPlacement:function(error, element){

			}
		});
	});
</script> --}}
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection