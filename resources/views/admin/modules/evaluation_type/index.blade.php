@extends('admin.layouts.app')
@section('title', 'netbhe.com | Admin | Evalution Type')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Evalution Type</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('evaluation-type.create') }}" title="Create"><button type="button" class="btn btn-default">Add evalution type</button></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 dess5">
									<i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Evalution Type</th>
													<th>Evalution Category</th>
													<th>Associated Specialities</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@if($all_evalution_type->isNotEmpty())
                                                    @foreach(@$all_evalution_type as $detail)
                                                        <tr>
                                                            <td>{{ $detail->title }}</td>
                                                            <td>{{ $detail->category->name }}</td>
															<td>
																@if(count(@$detail->specialities)>0)
																	@php
																		$specialities = "";
																		foreach($detail->specialities as $spec){
																			$specialities .= $spec->name . ', ';
																		}
																	@endphp
																	{{substr($specialities,0,-2)}}
																@endif
															</td>
                                                            <td>
                                                                <a href="{{ route('evaluation-type.edit', ['id' => $detail]) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
                                                                <a onclick="return confirm('Do you want to delete this evaluation type?')" href="{{ route('evaluation-type.delete', ['id' => $detail]) }}" title="Delete"> <i class="fa fa-trash-o delet" aria-hidden="true"></i></a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
												@endif
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
