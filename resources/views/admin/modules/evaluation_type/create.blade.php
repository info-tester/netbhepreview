@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Add new evulation type')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<script src="{{URL::to('public/js/chosen.jquery.min.js')}}"></script>
<link rel="stylesheet" href="{{URL::to('public/css/chosen.css')}}">

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Add new evulation type</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('evaluation-type.index') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
				</div>
			</div>
			@include('admin.includes.error')
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
                                        <form id="myform" method="post" action="{{ route('evaluation-type.store') }}" enctype="multipart/form-data">
                                            @csrf
											<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Title *</label>
													<input type="text" name="title" class="form-control required" id="title" placeholder="Title">
												</div>
											</div>

											<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
												<div class="your-mail">
                                                    <label for="exampleInputEmail1">Evaluation Category *</label>
                                                    <select name="evaluation_category_id" id="evaluation_category_id" class="newdrop form-control" required>
                                                        <option value="">Select Evaluation Category</option>
                                                        @foreach ($categories as $cat)
                                                            <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                                                        @endforeach
                                                    </select>
												</div>
											</div>
                                            
                                            <div class="clearfix"></div>
                                            <br>

                                            <div class="col-lg-6 col-md-12 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Professional speciality  *</label>
                                                    <select name="speciality_ids[]" id="speciality_ids" data-placeholder ="Select professional speciality" class="required form-control newdrop required chosen-select" multiple="true" required="">
													@foreach (@$specialities as $spec)
														<option value="{{ $spec->id }}"
														@if(@$related_spec_ids && in_array($spec->id, $related_spec_ids)) selected @endif
														>{{ $spec->name }}</option>
													@endforeach
                                                    </select>
													<label class="error" id="chosen_select_error" style="display:none;">This field is required</label>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                            <br>

											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="margin-top: 10px;">
												<div class="submit-login add_btnm">
													<input value="submit" type="submit" class="btn btn-default">
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>


<style type="text/css">
    .upldd {
    display: block;
    width: auto;
    border-radius: 4px;
    text-align: center;
    background: #9caca9;
    cursor: pointer;
    overflow: hidden;
    padding: 10px 15px;
    font-size: 15px;
    color: #fff;
    cursor: pointer;
    float: left;
    margin-top: 15px;
    font-family: 'Poppins', sans-serif;
    position: relative;
}
.upldd input {
    position: absolute;
    font-size: 50px;
    opacity: 0;
    left: 0;
    top: 0;
    width: 200px;
    cursor: pointer;
}
.upldd:hover{
    background: #1781d2;
}

</style>

    <script>
    window.setInterval(function() {
    $('.chosen-select').trigger('chosen:updated');
    }, 1000);
    $(document).ready(function(){
        $(".chosen-select").chosen();
        // $("body").delegate("#evaluation_category_id", "change", function() {
        $('#evaluation_category_id').on('change', function(){
            if($(this).val() != ''){
                var arr = [$(this).val()];
                console.log(arr);
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('load.specialities') }}",
                    method:"POST",
                    data:{arr:arr, _token:_token},
                    success:function(data){
                        console.log(data);
                        $('#speciality_ids > option').removeAttr("selected");
                        $('#speciality_ids > option').each(function(){
                            if($.inArray( parseInt($(this).val()), data ) !== -1){
                                // console.log("VAL: "+$(this).val())
                                // console.log($(this).text());
                                // $(this).attr('selected',true);
                                $(this).prop('selected',true);
		                        $('.chosen-select').trigger('chosen:updated');
                            }
                        });
                        console.log("Specialities loaded");
                        $('.chosen-select').trigger('chosen:updated');
                    }, error:function(error){
                        console.log(error);
                    }
                });
            }
        });

        $("#myform").validate({
			submitHandler: function(form) {
				var flag = 0;
				if($('#speciality_ids').val() == null){
					$('#chosen_select_error').css('display', 'block');
					flag = 1;
				}
				if(flag == 1){
					return false;
				} else {
					$('#chosen_select_error').css('display', 'none');
					// console.log("Submitted");
					// return false;
					form.submit();
				}
			}
        });
        
        tinyMCE.init({
            mode : "specific_textareas",
            editor_selector : "desc",
            height: '320px',
            plugins: [
              'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
              'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
              'save table contextmenu directionality emoticons template paste textcolor'
            ],
            relative_urls : false,
            remove_script_host : false,
            convert_urls : true,
            toolbar: 'insertfile undo redo | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor emoticons',
                images_upload_url: '{{ URL::to('storage/app/public/uploads/content_image/') }}',
                images_upload_handler: function(blobInfo, success, failure) {
                    var formD = new FormData();
                    formD.append('file', blobInfo.blob(), blobInfo.filename());
                    formD.append( "_token", '{{csrf_token()}}');
                    $.ajax({
                        url: '{{route("admin.artical.img.upload")}}',
                        data: formD,
                        type: 'POST',
                        contentType: false,
                        cache: false,
                        processData:false,
                        dataType: 'JSON',
                        success: function(jsn) {
                            if(jsn.status == 'ERROR') {
                                failure(jsn.error);
                            } else if(jsn.status == 'SUCCESS') {
                                success(jsn.location);
                            }
                        }
                    });
                },
            });
            $("#myform").submit(function (event) {
        if(tinyMCE.get('desc').getContent()==""){
            event.preventDefault();
            $("#cntnt").html("Description field is required").css('color','red');
        }
        else{
            $("#cntnt").html("");
        }
    });
    });


</script>


<style>
    .error{
        color: red !important;
    }
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
