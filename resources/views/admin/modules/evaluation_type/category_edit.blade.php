@extends('admin.layouts.app')
@if (@$category)
    @section('title', 'Netbhe.com | Admin | Edit Evaluation Category')
@else
    @section('title', 'Netbhe.com | Admin | Add Evaluation Category')
@endif
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<script src="{{URL::to('public/js/chosen.jquery.min.js')}}"></script>
<link rel="stylesheet" href="{{URL::to('public/css/chosen.css')}}">

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">{{ @$category ? 'Edit' : 'Add' }} Evaluation Category</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('evaluation.category') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
				</div>
			</div>
			@include('admin.includes.error')
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div data-pattern="priority-columns">
                                        <form id="myform" method="post" action="{{ @$category ? route('evaluation.category.edit', ['id' => $category->id]) : route('evaluation.category.add') }}" enctype="multipart/form-data">
                                            @csrf
											<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Name *</label>
													<input type="text" name="name" class="form-control required" id="name" placeholder="Evaluation Category Name" value="{{ old('name', @$category->name) }}">
												</div>
											</div>

                                            <div class="col-lg-6 col-md-12 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Professional speciality  *</label>
                                                    <select name="speciality_ids[]" id="speciality_ids" data-placeholder ="Select professional speciality" class="required form-control newdrop required chosen-select" multiple="true" required="">
													@foreach (@$specialities as $spec)
														<option value="{{ $spec->id }}"
														@if(@$related_spec_ids && in_array($spec->id, $related_spec_ids)) selected @endif
														>{{ $spec->name }}</option>
													@endforeach
                                                    </select>
													<label class="error" id="chosen_select_error" style="display:none;">This field is required</label>
                                                </div>
                                            </div>

											<div class="col-lg-12" style="margin-top: 10px;">
												<div class="submit-login add_btnm">
													<input value="Save" type="submit" class="btn btn-default">
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>


<style type="text/css">
    .upldd {
    display: block;
    width: auto;
    border-radius: 4px;
    text-align: center;
    background: #9caca9;
    cursor: pointer;
    overflow: hidden;
    padding: 10px 15px;
    font-size: 15px;
    color: #fff;
    cursor: pointer;
    float: left;
    margin-top: 15px;
    font-family: 'Poppins', sans-serif;
    position: relative;
}
.upldd input {
    position: absolute;
    font-size: 50px;
    opacity: 0;
    left: 0;
    top: 0;
    width: 200px;
    cursor: pointer;
}
.upldd:hover{
    background: #1781d2;
}

</style>

    <script>
    $(document).ready(function(){
        $(".chosen-select").chosen();
		$('.chosen-select').trigger('chosen:updated');
		$("#myform").validate({
			submitHandler: function(form) {
				var flag = 0;
				if($('#speciality_ids').val() == null){
					$('#chosen_select_error').css('display', 'block');
					flag = 1;
				}
				if(flag == 1){
					return false;
				} else {
					$('#chosen_select_error').css('display', 'none');
					// console.log("Submitted");
					// return false;
					form.submit();
				}
			}
		});
    });


</script>


<style>
    .error{
        color: red !important;
    }
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
