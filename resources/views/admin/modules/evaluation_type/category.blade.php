@extends('admin.layouts.app')
@section('title', 'netbhe.com | Admin | Evaluation Categories')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Evaluation Categories</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('evaluation.category.add') }}" title="Create"><button type="button" class="btn btn-default">Add Evaluation category</button></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 dess5">
									<i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Evaluation Category</th>
													<th>Associated Specialities</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@if($categories->isNotEmpty())
                                                    @foreach(@$categories as $category)
                                                        <tr>
                                                            <td>{{ $category->name }}</td>
															<td>
																@if(count(@$category->specialities)>0)
																	@php
																		$specialities = "";
																		foreach($category->specialities as $spec){
																			$specialities .= $spec->name . ', ';
																		}
																	@endphp
																	{{substr($specialities,0,-2)}}
																@endif
															</td>
                                                            <td>
                                                                <a href="{{ route('evaluation.category.edit', ['id' => @$category->id]) }}" title="Edit"><i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
                                                                <a onclick="return confirm('Do you want to delete this category?')" href="{{ route('evaluation.category.delete', ['id' => @$category->id]) }}" title="Delete"><i class="fa fa-trash-o delet" aria-hidden="true"></i></a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
												@endif
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- <form method="DELETE" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
	var confirm = window.confirm('Do you want to delete this blog?');
	if(confirm){
		$("#destroy").attr('action',val);
		$("#destroy").submit();
	}
 }
</script> -->

<form method="post" id="destroy">
    {{ csrf_field() }}
    {{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
    var confirm = window.confirm('@lang('site.confrm_del_import_tool')');
    if(confirm){
        $("#destroy").attr('action',val);
        $("#destroy").submit();
    }
 }
</script>


@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
