@extends('admin.layouts.app')
@section('title', 'Vedic Astro World | Admin | Payouts')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Payment Management</h4>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="panel panel-default">
									<div class="panel-body table-rep-plugin">
										<form method="get" action="{{ route('payouts.filter')}}" >
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="your-mail">
													<label for="name">Astrologer Name</label>
													<select class="form-control newdrop " name="name">
														<option value="">Astrologer name</option>
														@foreach($astro_name as $row)
														<option value="{{$row->name}}" @if(@$key['name'] == $row->name) selected @endif>{{@$row->nick_name ? @$row->nick_name : @$row->name}}</option>
														@endforeach
													</select>
												</div>
											</div>
											<div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">From Date</label>
                                                <input type="text" class="form-control" name="from_date" id="datepicker" value="{{@$key['from_date']}}" readonly placeholder=" Form Date">
                                            </div>
                                        </div>
                                       
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">To Date</label>
                                                <input type="text" class="form-control" name="to_date" id="datepicker1" value="{{@$key['to_date']}}" readonly placeholder=" To date">
                                            </div>
                                        </div>
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="your-mail">
													<label for="amount">Amount</label>
													<input class="form-control" name="amount" placeholder="Amount" value="{{ @$key['amount'] }}" type="text">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="submit-login add_btnm {{-- pull-right --}}" style="width:auto; margin-top:35px;">
													<input value="Search" class="btn btn-default" type="submit">
												</div>
											</div>
										</form>
									</div>
								</div>
								{{-- <div class="col-md-12 dess5">
									<i class="fa fa fa-eye cncl" aria-hidden="true"> <span class="cncl_oopo">View</span></i>
									<i class="fa fa fa-money cncl" aria-hidden="true"> <span class="cncl_oopo">Payout</span></i>
									<i class="fa fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
									<i class="fa fa-trash-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
									<i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active</span></i>
									<i class="fa fa-times cncl" aria-hidden="true"> <span class="cncl_oopo">Inactive</span></i>
									<i class="fa fa-envelope-square cncl" aria-hidden="true"> <span class="cncl_oopo">Email Verify Link</span></i>
									<i class="fa fa-times-circle cncl" aria-hidden="true"> <span class="cncl_oopo">Reject</span></i>
									<i class="fa fa-check-circle cncl" aria-hidden="true"> <span class="cncl_oopo">Approve</span></i>
									<i class="fa fa-square cncl" aria-hidden="true"> <span class="cncl_oopo">Show in Home Page</span></i>
									<i class="fa fa-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Not Show in Home Page</span></i>
									<i class="fa fa-star cncl" aria-hidden="true"> <span class="cncl_oopo">Recommended</span></i>
									<i class="fa fa-star-o cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">Not Recommended</span></i>
								</div> --}}
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Details</th>
													{{-- <th>Email ID</th>
													<th>Mobile No</th> --}}
													<th>Description</th>
													<th>Date</th>
													<th>Paid</th>
													<th>Wallet Balance</th>
													
													{{-- <th>Action</th> --}}
												</tr>
											</thead>
											<tbody>
												@foreach($user as $row)
												
												<tr>
													<td>{{ "Name: ". (@$row->userDetails->nick_name ? @$row->userDetails->nick_name : @$row->userDetails->name)}}<br>{{@$row->userDetails->email ? "E-mail: ".$row->userDetails->email : 'N.A'}}<br>{{ @$row->userDetails->mobile ? "Mobile: ".$row->userDetails->mobile : 'N.A' }}</td>
													
													<td>
							                            {{ @$row->description }}
													</td>
												
													<td>{{ @$row->date }}</td>
													
													<td><span>@if(@$row->userDetails->currency_id == 1) ₹ @elseif(@$row->userDetails->currency_id == 2) $ @elseif(@$row->userDetails->currency_id == 3) € @endif</span>{{ @$row->amount }}</td>

													<td><span>@if(@$row->userDetails->currency_id == 1) ₹ @elseif(@$row->userDetails->currency_id == 2) $ @elseif(@$row->userDetails->currency_id == 3) € @endif</span>{{ @$row->balance }}</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>
{{-- modal for payout --}}
<div class="modal fade" id="myModal" role="dialog" style="display: none;">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Astrologer Payout </h4>
        </div>
        <div class="modal-body">
        	<label class="type_label">Bank Inforfamtion of <span id="holder_name">Xyz ASd</span></label>
          <div class="row">
          		<div class="col-md-3">

          			<div class="form-group">
	          			<label>Account No:<span id="ac_no"></span></label>
	          			 
	          		</div>
          		</div>
          		<div class="col-md-3">
          			<div class="form-group">
	          			<label>Account Name: <span id="ac_name"></span></label>
	          			 
	          		</div>
          		</div>
          		<div class="col-md-3">
          			<div class="form-group">
	          			<label>Account IFSC: <span id="ifsc"></span></label>
	          			 
	          		</div>
          		</div>
          		<div class="col-md-3">
          			<div class="form-group">
	          			<label>PAN No: <span id="pan"></span></label>
	          		</div>
          		</div>
          		<div class="clearfix"></div>
          		<form method="POST" action="{{ route('astro.payout') }}" id="my_form" name="my_form">
          			@csrf
          			<input type="hidden" name="astro_id" id="astro_id">
	          		<div class="col-md-6">
	          			<div class="form-group">
		          			<label class="type_label" for="payout">Payout
		          			<input type="text" class="login_type required digits" placeholder="Payout" name="payout" id="payout"></label>
		          		</div>
	          		</div>
	          		<div class="col-md-6">
	          			<div class="form-group">
		          			<label class="type_label" for="notes">Notes
		          			<input type="text" class="login_type required" placeholder="Notes" name="notes" id="notes"></label>
		          		</div>
	          		</div>
	          		<div class="col-lg-12 col-md-12">
	                   	<button type="submit" class="btn btn-info sbmt_clk">Make Payment</button>
	          		</div>
          		</form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{{-- modal for payout --}}
<form method="post" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>
<script>
	function deleteCategory(val){
	var confirm = window.confirm('Are you want to delete this astrologer?');
	if(confirm){
		$("#destroy").attr('action',val);
		$("#destroy").submit();
	}
	}
</script>

<script type="text/javascript">

	$('.pyout').click(function(){
		//$('#myModal').hide();
		$('#payout').val('');
		$('#payout').removeClass('error');
		$('#notes').val('');
		$('#notes').removeClass('error');
		var x=0;
		$('.parent_loader').show();
		setInterval(function(){ $('.parent_loader').hide(); x=1; }, 1000);
		$('#astro_id').val($(this).data('astro-id'));
		var astro_name = $(this).data('astro-name');
		var bank_name = $(this).data('astro-bank-name');
		var ac_name = $(this).data('astro-ac-name');
		var acc_no = $(this).data('astro-ac-no');
		var ifsc = $(this).data('astro-ifsc');
		var pan = $(this).data('astro-pan');
		$('#holder_name').text(astro_name);
		$('#bank_name').text(bank_name);
		$('#bank_name').text(bank_name);
		$('#ac_no').text(acc_no);
		$('#ifsc').text(ifsc);
		$('#pan').text(pan);
		$('#ac_name').text(ac_name);
		$('#myModal').show("fade");
	});
	
	$(document).ready(function(){
		$('#my_form').validate({
			errorPlacement:function(e, e){

			}
		});
	});
</script>
<script>
    $(function() {  
        $("#datepicker").datepicker({dateFormat: "yy-mm-dd",
           defaultDate: new Date(),
           onClose: function( selectedDate ) {
           $( "#datepicker1").datepicker( "option", "minDate", selectedDate );
          }
        });

        $("#datepicker1").datepicker({dateFormat: "yy-mm-dd",
         defaultDate: new Date(),
              onClose: function( selectedDate ) {
              $( "#datepicker" ).datepicker( "option", "maxDate", selectedDate );
            }
        });        
    });
</script>
@endsection
@section('footer')
@include('admin.includes.footer')

@endsection