@extends('admin.layouts.app')
@section('title', 'Vedic Astro World | Admin | Review')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- Begin page -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->                      
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="pull-left page-title">Manage Review</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body table-rep-plugin">
                                <div class="row top_from">
                                    <form method="get" action="{{ route('admin.review')}}">
                                        {{-- <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">Service</label>
                                                <select name="service" class="form-control newdrop">
                                                    <option value="">Choose Service</option>
                                                    <option value="2" @if(@$key['service'] == 2) {{ "selected" }} @endif>Call</option>
                                                    <option value="1" @if(@$key['service'] == 1) {{ "selected" }} @endif>Chat</option>
                                                    <option value="5" @if(@$key['service'] == 5) {{ "selected" }} @endif>Pooja</option>
                                                    <option value="4" @if(@$key['service'] == 4) {{ "selected" }} @endif>Question</option>
                                                    <option value="3" @if(@$key['service'] == 3) {{ "selected" }} @endif>Report</option>
                                                    
                                                </select>
                                            </div>
                                        </div>

                                        
                                       
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">Order Number</label>
                                                <input type="text" class="form-control" name="order_no" id="order_no" value="{{@$key['order_no']}}" placeholder="Order number">
                                            </div>
                                        </div> --}}
                                       
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">Astrologer Name</label>
                                                <select name="astrolger_name" id="astrolger_name" class="form-control newdrop">
                                                    <option value="">Choose Astrolger Name</option>
                                                    @foreach($astrologer as $astr)
                                                    <option value="{{ @$astr->id }}" @if(@$key['astrolger_name'] == $astr->id) {{ "selected" }} @endif>{{ @$astr->nick_name ? @$astr->nick_name : @$astr->name }}</option>
                                                    
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        {{-- <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">Customer Name</label>
                                                <input type="text" class="form-control" name="customer_name" id="customer_name" value="{{@$key['customer_name']}}" placeholder="Customer name">
                                            </div>
                                        </div>
                                        
                                       
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">From Date</label>
                                                <input type="text" class="form-control" name="from_date" id="datepicker" value="{{@$key['from_date']}}" placeholder="From Date">
                                            </div>
                                        </div>
                                       
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">To Date</label>
                                                <input type="text" class="form-control" name="to_date" id="datepicker1" value="{{@$key['to_date']}}" placeholder="To Date">
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">Status</label>
                                                <select name="order_status" class="form-control newdrop">
                                                    <option value="">Choose Status</option>
                                                    <option value="N" @if(@$key['order_status'] == "N") {{ "selected" }} @endif>New Order</option>
                                                    <option value="C" @if(@$key['order_status'] == "C") {{ "selected" }} @endif>Complete</option>
                                                    
                                                </select>
                                            </div>
                                        </div> --}}

                                        {{-- <div class="clearfix"></div> --}}
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="submit-login add_btnm pull-right" style="/*width:auto;*/ margin-top:35px;">
                                                <input value="Search" id="search" class="btn btn-default" type="submit">
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 dess5">
                                        <i class="fa fa-trash-o cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">Delete</span></i>
                                        
                                    </div>
                                    <div class="clearfix"></div>
                                    
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" data-pattern="priority-columns">
                                            <table id="datatable" class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        {{-- <th>Order No.</th> --}}
                                                        <th>Date </th>
                                                        <th>Astrologer</th>
                                                        <th>Customer</th>
                                                        {{-- <th>Service</th> --}}
                                                        <th>Review Point</th>
                                                        <th>Title</th>
                                                        <th>Description</th>
                                                        {{-- <th>Status</th> --}}
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($review as $row)
                                                    <tr>
                                                        {{-- <td><span class="dateee">
                                                            {{ $row->order_no }}
                                                        </td> --}}
                                                        <td>{{ @$row->created_at ? date('jS-M-Y', strtotime($row->created_at)) : '' }}</td>
                                                        <td>{{@$row->astrologerName->nick_name ? @$row->astrologerName->nick_name : @$row->astrologerName->name}}</td>
                                                        <td>{{@$row->customerName->nick_name ? @$row->customerName->nick_name : @$row->customerName->name}}</td>
                                                        {{-- <td>{{ @$row->serviceName->name }}</td> --}}
                                                        <td>{{ @$row->review_point }}</td>
                                                        <td>{{ @$row->title }}</td>
                                                        <td>{!! @$row->description !!}</td>
                                                        {{-- <td>@if(@$row->service_id == 2)
                                                                {{ @$row->call_status ? $row->call_status : "" }}
                                                            @else
                                                                {{ @$row->status == 'N' ? "New": "Completed" }}
                                                            @endif
                                                        </td> --}}
                                                        <td>
                                                            <a href="javascript:void(0)" onclick="if(confirm('Are you want to delete this review?')){ location.href='{{route('admin.review.destroy',[$row->id])}}'}" title="Delete"> <i class="fa fa-trash-o delet" aria-hidden="true"></i></a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Row -->
            </div>
            <!-- container -->
        </div>
        <!-- content -->
        <!--<footer class="footer text-right">
            2015 © Moltran.
            </footer>-->
    </div>
</div>
<!--Raise  popup-->
<script>
    var resizefunc = [];
</script>

<script type="text/javascript">
    $(document).ready(function() {
    $('#datatable_length').hide();
        $('#datatable').dataTable({
    "ordering": false,
    "info":     false,
    "searching": false,
    "dom": '<"toolbar">frtip'
    });
    $(".check_check").click(function(){
    if($(this).is(":checked"))
    {
    $('.ch_1').show();
    }
    else
    {
    $('.ch_1').hide();
    }
    });
    //$("div.toolbar").html('<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 status">Status: <select name="" class="newdrop"><option value="1">Active</option><option value="1">Inactive</option></select></div><div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 clndr">From date : <input type="text" name="from" id="frm_date"> </div><div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 clndr"> To date : <input type="text" name="to" id="to_date"> </div><div class="col-md-4 col-lg-4 clndr">Keyword: <input type="text" name="abc"></div><div class="col-md-4 col-lg-4 col-sm-4 colxs-12 srch"><input type="submit" name="submit" value="Search"> </div>');   
       // $('#datatable-keytable').DataTable( { keys: true } );
       //$('#datatable-responsive').DataTable();
        //$('#datatable-scroller').DataTable( { ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
        //var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } );
    } );
    // TableManageButtons.init();
</script>
<script>
    $(function() {  
        $("#datepicker").datepicker({dateFormat: "dd-mm-yy",
           defaultDate: new Date(),
           onClose: function( selectedDate ) {
           $( "#datepicker1").datepicker( "option", "minDate", selectedDate );
          }
        });

        $("#datepicker1").datepicker({dateFormat: "dd-mm-yy",
         defaultDate: new Date(),
              onClose: function( selectedDate ) {
              $( "#datepicker" ).datepicker( "option", "maxDate", selectedDate );
            }
        });        
    });
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection