@extends('admin.layouts.app')
@section('title', 'netbhe.com | Admin | ContractTemplat')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">ContractTemplate</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('tool-contract-template.create') }}" title="Create"><button type="button" class="btn btn-default">Add contract template</button></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 dess5">
									<i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
									<i class="fa fa-trash-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
									<i class="fa fa-eye cncl"  aria-hidden="true"> <span class="cncl_oopo">View Fields</span></i>

									<i class="fa fa-square cncl" aria-hidden="true"> <span class="cncl_oopo"> Not Show in All Professional</span></i>
                                    <i class="fa fa-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Show in All Professional</span></i>
									<!-- <i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active</span></i>
                                	<i class="fa fa-times cncl" aria-hidden="true"> <span class="cncl_oopo">Inactive</span></i> -->
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Title</th>
													<th>Added By</th>
													<th>Status</th>
													<th>Show in<br>Professional</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@if($contract_templats->isNotEmpty())
												@foreach(@$contract_templats as $detail)
												<tr>
													<td>{{ $detail['title'] }}</td>
													<td>{{ $detail['added_by']=='A'?'Admin': ( @$detail->user->nick_name ? @$detail->user->nick_name : @$detail->user ) }}</td>
													<td>{{ $detail['status'] }}</td>

													<td>
	                                                    @if(@$detail->shown_in_proff == 'N')
	                                                    <label class="label label-danger">{{ 'No' }}</label>
	                                                    @elseif(@$detail->shown_in_proff == 'Y')
	                                                    <label class="label label-success">{{ 'Yes' }}</label>
	                                                    @endif
	                                                </td>

													<td>
														<a href="{{ route('tool-contract-template.edit',['id'=>@$detail->id]) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>

														<a href="{{ route('tool-contract-template.show',[@$detail->id]) }}" title="View Fields"> <i class="fa fa-eye delet" aria-hidden="true"></i></a>


														<a href="javascript:void(0)" onclick="deleteCategory('{{route('tool-contract-template.destroy',[$detail->id])}}')" title="Delete" class="rjct delbtn"><i class="fa fa-trash-o delet" aria-hidden="true"></i></a>

														@if($detail->shown_in_proff == 'Y')
	                                                        <a href="{{ route('admin.contracttools.forproff',[$detail->id]) }}" title="Not Show in Professional Page" onclick="return confirm('Do you want to Not Show in Professional Page this ');"> <i class="fa fa-square delet" aria-hidden="true"></i></a>
	                                                    @else
	                                                        <a href="{{ route('admin.contracttools.forproff',[$detail->id]) }}" title="Show in Professional Page " onclick="return confirm('Do you want to Show in Professional Page this ');"> <i class="fa fa-square-o delet" aria-hidden="true"></i></a>
	                                                    @endif

														<!-- @if($detail->status=="A")
                                                            <a href="{{ route('admin.category.status', ['id'=>$detail->id]) }}" onclick="return confirm('Are you really want to Inactive this category?')" title="Inactive"><i class="fa fa-times delet" aria-hidden="true"></i></a>
                                                        @else
                                                            <a href="{{ route('admin.category.status', ['id'=>$detail->id]) }}" title="Active" onclick="return confirm('Are you really want to Active this category?')"><i class="fa fa-check delet" aria-hidden="true"></i></a>
                                                        @endif -->
													</td>
												</tr>
												@endforeach

												@endif
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- <form method="DELETE" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
	var confirm = window.confirm('Do you want to delete this blog?');
	if(confirm){
		$("#destroy").attr('action',val);
		$("#destroy").submit();
	}
 }
</script> -->

<form method="post" id="destroy">
    {{ csrf_field() }}
    {{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
    var confirm = window.confirm('@lang('site.confrm_del_import_tool')');
    if(confirm){
        $("#destroy").attr('action',val);
        $("#destroy").submit();
    }
 }
</script>


@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
