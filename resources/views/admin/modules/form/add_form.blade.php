@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Add Blog Category')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<script src="{{URL::to('public/js/chosen.jquery.min.js')}}"></script>
<link rel="stylesheet" href="{{URL::to('public/css/chosen.css')}}">

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">{{ $title }}</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.form.manage') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
				</div>
			</div>
			@include('admin.includes.error')
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
										@if(@$details->id)
										<form id="myform" method="post" action="{{ route('admin.form.update',['form_id'=>@$details->id]) }}">
												@else
												<form id="myform" method="post" action="{{ route('admin.form.create') }}">
											@endif
											{{ csrf_field() }}
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Title *</label>
												<input type="text" name="title" class="form-control required" id="title" placeholder="Title" value="{{ @$details->form_title }}">
												</div>
											</div>

											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Category *</label>
													<select name="cat_id" class="form-control newdrop required">
														<option value="">Select</option>
														@foreach($category as $row)
														<option value="{{ $row['id'] }}" @if(@$details->form_cat_id==$row['id']) selected @endif>{{ $row['form_category'] }}</option>
														@endforeach
													</select>
												</div>
											</div>

											@if(@$details->added_by != 'P')
                                            <div class="col-lg-6 col-md-12 col-sm-6 col-xs-12">
                                                <div class="your-mail prof_spec">
                                                    <label for="exampleInputEmail1">Professional speciality  *</label>
                                                    <select name="speciality_ids[]" id="speciality_ids" data-placeholder ="Select professional speciality" class="required form-control newdrop required chosen-select" multiple="true" required="">
													@foreach (@$specialities as $spec)
														<option value="{{ $spec->id }}"
														@if(@$related_spec_ids && in_array($spec->id, $related_spec_ids)) selected @endif
														>{{ $spec->name }}</option>
													@endforeach
                                                    </select>
													<label class="error" id="chosen_select_error" style="display:none;">This field is required</label>
                                                </div>
                                            </div>
											@endif

											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Description *</label>
													<textarea name="desc" id="desc" rows="10" placeholder="Description" style="width:100%" class="required form-control"><?= strip_tags(@$details->form_dsc) ?></textarea>
												</div>
											</div>

											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Help tools view *</label>
													<textarea name="help_tools_view" id="help_tools_view" rows="10" placeholder="Help tools view" style="width:100%" class="required form-control"><?= strip_tags(@$details->help_tools_view) ?></textarea>
												</div>
											</div>

											<div class="clearfix"></div>
											<br>

											
											
											
											<div class="clearfix"></div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="submit-login add_btnm">
													<input value="{{ $button }}" type="submit" class="btn btn-default">
												</div>
											</div>
											<!--all_time_sho-->
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>
<script>
	$(document).ready(function(){
		var added_by = "{{@$details->added_by}}";
        console.log(added_by);
        $(".chosen-select").chosen();
		$('.chosen-select').trigger('chosen:updated');
		$("#myform").validate({
			ignore:[],
			rules:{
				speciality_ids:{
					required:true
				}
			},
			submitHandler: function(form) {
				var flag = 0;
				if($('#speciality_ids').val() == null && added_by != 'P'){
					$('#chosen_select_error').css('display', 'block');
					flag = 1;
				}
				if(flag == 1){
					return false;
				} else {
					$('#chosen_select_error').css('display', 'none');
					// console.log("Submitted");
					// return false;
					form.submit();
				}
			}
		});
	});
</script>
<style>
	.error{
		color: red !important;
	}
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection