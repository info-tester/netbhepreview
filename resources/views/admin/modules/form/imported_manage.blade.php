@extends('admin.layouts.app')
@section('title', 'netbhe.com | Admin | Form')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">{{ $title }}</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.form.add') }}" title="Create"><button type="button" class="btn btn-default">Add Form</button></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 dess5">
									<i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
									<i class="fa fa-trash-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
									<i class="fa fa-eye cncl" style="border: none;" aria-hidden="true"> <span class="cncl_oopo">View Fields</span></i>
									
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Title</th>
													<th>Description</th>
													<th>Category</th>
													<th>Added By</th>
													<th>Status</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@if($form->isNotEmpty())
												@foreach(@$form as $detail)
												<tr>
												<td>{{ $detail['form_title'] }}</td>
												<td>{{ strlen($detail['form_dsc'])>20?substr($detail['form_dsc'],0,20).'..':$detail['form_dsc'] }}</td>
												<td>{{ $detail->category->form_category }}</td>
												<td>{{ $detail['added_by']=='A'?'Admin': ( @$detail->user->nick_name ? @$detail->user->nick_name : @$detail->user->name ) }}</td>
												<td>{{ $detail['status'] }}</td>
													
													<td>
														<a href="{{ route('admin.form.edit',['form_id'=>@$detail->id]) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
														<a href="{{route('admin.form.delete',['form_id'=>@$detail->id])}}" onclick="return confirm('Do you want to delete this form?')" title="Delete"> <i class="fa fa-trash-o delet" aria-hidden="true"></i></a>
														<a href="{{ route('admin.form.details.manage',[@$detail->id]) }}" title="View Fields"> <i class="fa fa-eye delet" aria-hidden="true"></i></a>
													</td>
												</tr>
												@endforeach
												
												@endif
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<form method="post" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
	var confirm = window.confirm('Are you want to delete this content ?');
	if(confirm){
		$("#destroy").attr('action',val);
		$("#destroy").submit();
	}
 }
</script>

@endsection
@section('footer')
@include('admin.includes.footer')
@endsection