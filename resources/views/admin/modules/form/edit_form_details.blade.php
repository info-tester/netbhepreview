@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Add Form')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">{{ $title }}</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.form.details.manage',['form_id' => $form->id]) }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
				</div>
			</div>
			@include('admin.includes.error')
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12">
									<strong>Form Name -</strong> {{ $form->form_title }}
								</div>
								<div class="clearfix"></div>
								<br>
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
										<form id="myform" method="post" action="{{ route('admin.form.details.update',['form_id'=>@$form->id]) }}">
											{{ csrf_field() }}
											

											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Type *</label>
													<select name="formtype" class="form-control newdrop required formType">
														<option value="">Select</option>
														<option value="TEXTBOX" {{ @$details->field_type=='TEXTBOX'?'selected':'' }}>Parágrafo - Resposta aberta{{-- Textbox --}}</option>
														<option value="TEXTAREA" {{ @$details->field_type=='TEXTAREA'?'selected':'' }}>Selecionar- Único valor{{-- Textarea --}}</option>
														<option value="SINGLESELECT" {{ @$details->field_type=='SINGLESELECT'?'selected':'' }}>Selecionar - Múltiplos valores{{-- Singleselect --}}</option>
														<option value="MULTISELECT" {{ @$details->field_type=='MULTISELECT'?'selected':'' }}>Pergunta múltipla de texto{{-- Multiselect --}}</option>
													</select>
												</div>
											</div>

											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Question Name *</label>
												<input type="text" name="field_name" class="form-control required" id="field_name" placeholder="Question Name" value="{{ @$details->field_name }}">
												</div>
											</div>
											@php
											$is_add = 'YES';
											$total_data = 1;
											@endphp
											<div id="fields">
												@if(@$details->field_type=="SINGLESELECT" || @$details->field_type=="MULTISELECT")
													@php
														$is_add = 'NO';
														$data = json_decode(@$details->field_values);
														$total_data = count($data);
														// $data = array_reverse($data);
														// dd($data)
													@endphp
													@foreach($data as $key=>$row)
														@if($key==$total_data-1)
											<div class="col-md-12" id="fieldRow{{ $key }}"><div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
														<div class="your-mail">
															<label for="exampleInputEmail1">Option *</label>
														<input type="text" name="field_value[]" class="form-control required" id="field_value{{ $key }}" placeholder="Eneter Select Value" value="{{ $row }}">
														</div>
													</div>
													<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
														<button type="button" class="btn btn-success add-button addsingleselect" title="Add"><i class="fa fa-plus" aria-hidden="true"></i></button>
													</div></div></div><div class="clearfix"></div>
														@else
														<div class="col-md-12" id="fieldRow{{ $key }}"><div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Option *</label>
													<input type="text" name="field_value[]" class="form-control required" id="field_value{{ $key }}" placeholder="Eneter Select Value" value="{{ $row }}">
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<button type="button" class="btn btn-danger add-button removesingleselect" title="Remove" data-id="{{ $key }}"><i class="fa fa-minus" aria-hidden="true"></i></button>
											</div></div></div><div class="clearfix"></div>
														@endif
													@endforeach
												@endif
											</div>
											<input type="hidden" class="countvalues" value="{{ $total_data }}">
										<input type="hidden" class="total_value" value="{{ $total_data }}">
										{{-- <input type="hidden" class="totalcount" value="{{ $total_data }}"> --}}
											<div class="clearfix"></div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="submit-login add_btnm">
													<input value="Edit" type="submit" class="btn btn-default">
												</div>
											</div>
											<!--all_time_sho-->
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>
<script>
	$(document).ready(function(){
		$("#myform").validate();
	});
</script>
<style>
	.error{
		color: red !important;
	}
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
<script>
	$(document).ready(function(){
		$('.formType').change(function(){
			var is_add = '{{ $is_add }}';
			$('#fields').show();
			// if(is_add=='NO'){
			// 	return false;
			// }else{
			// 	return true;
			// }
			var type = $(this).val();
			var count = $('.countvalues').val();
			var singleselect = '<div class="col-md-12" id="fieldRow'+count+'"><div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">\
												<div class="your-mail">\
													<label for="exampleInputEmail1">Option *</label>\
													<input type="text" name="field_value[]" class="form-control required" id="field_value'+count+'" placeholder="Eneter Select Value">\
												</div>\
											</div>\
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">\
												<button type="button" class="btn btn-success add-button addsingleselect" title="Add"><i class="fa fa-plus" aria-hidden="true"></i></button>\
											</div></div></div><div class="clearfix"></div>';
			if(type=='SINGLESELECT' || type=='MULTISELECT'){
				if(is_add=='YES'){
					$('#fields').html('');
					var newcount = parseInt(count)+1;
					$('.countvalues').val(newcount);
					$('#fields').html(singleselect);
				}
			}else{
				$('#fields').hide();
			}
		});
		$('body').on('click','.addsingleselect',function(){
			// alert();
		var count = $('.countvalues').val();
		var total_value = $('.total_value').val();
		total_value = parseInt(total_value)-1;
		var singleselect = '<div class="col-md-12" id="fieldRow'+count+'"><div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">\
												<div class="your-mail">\
													<label for="exampleInputEmail1">Option *</label>\
													<input type="text" name="field_value[]" class="form-control required" id="field_value'+count+'" placeholder="Eneter Select Value">\
												</div>\
											</div>\
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">\
												<button type="button" class="btn btn-danger add-button removesingleselect" title="Remove" data-id="'+count+'"><i class="fa fa-minus" aria-hidden="true"></i></button>\
											</div></div></div><div class="clearfix"></div>';
		$('#fieldRow'+total_value).before(singleselect);	
		// alert();							
		});
		$('body').on('click','.removesingleselect',function(){
			var id = $(this).data('id');
			$('#fieldRow'+id).remove();
		});
	});
</script>
@endsection