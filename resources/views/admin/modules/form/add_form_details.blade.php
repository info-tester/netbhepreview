@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Add Form')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">{{ $title }}</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.form.details.manage',['form_id' => $details->id]) }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
				</div>
			</div>
			@include('admin.includes.error')
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12">
									<strong>Form Name - </strong> {{ $details->form_title }}
								</div>
								<div class="clearfix"></div>
								<br>
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
										<form id="myform" method="post" action="{{ route('admin.form.details.create',['form_id'=>@$details->id]) }}">
											{{ csrf_field() }}
											

											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Type *</label>
													<select name="formtype" class="form-control newdrop required formType">
														<option value="">Select</option>
														<option value="TEXTBOX">Parágrafo - Resposta aberta{{-- Textbox --}}</option>
														<option value="TEXTAREA">Selecionar- Único valor{{-- Textarea --}}</option>
														<option value="SINGLESELECT">Selecionar - Múltiplos valores{{-- Singleselect --}}</option>
														<option value="MULTISELECT">Pergunta múltipla de texto{{-- Multiselect --}}</option>
													</select>
												</div>
											</div>

											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Question Name *</label>
													<input type="text" name="field_name" class="form-control required" id="field_name" placeholder="Field Name">
												</div>
											</div>
											<div id="fields">

											</div>
											<input type="hidden" class="countvalues" value="1">
											<div class="clearfix"></div>
											<br>
											<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
												<div class="submit-login add_btnm">
													<input value="Save" type="submit" class="btn btn-default">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="submit-login add_btnm">
													<input value="Finised" type="submit" class="btn btn-default" name="finised">
												</div>
											</div>
											<!--all_time_sho-->
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>
<script>
	$(document).ready(function(){
		$("#myform").validate();
	});
</script>
<style>
	.error{
		color: red !important;
	}
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
<script>
	$(document).ready(function(){
		$('.formType').change(function(){
			var type = $(this).val();
			var count = $('.countvalues').val();
			var singleselect = '<div class="col-md-12 plus_row" id="fieldRow'+count+'"><div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">\
												<div class="your-mail">\
													<label for="exampleInputEmail1">Option *</label>\
													<input type="text" name="field_value[]" class="form-control required" id="field_value'+count+'" placeholder="Eneter Select Value">\
												</div>\
											</div>\
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">\
												<button type="button" class="btn btn-success add-button addsingleselect" title="Add"><i class="fa fa-plus" aria-hidden="true"></i></button>\
											</div></div></div><div class="clearfix"></div>';
			if(type=='SINGLESELECT' || type=='MULTISELECT'){
				$('#fields').html('');
				var newcount = parseInt(count)+1;
				$('.countvalues').val(newcount);
				$('#fields').html(singleselect);
			}
		});
		$('body').on('click','.addsingleselect',function(){
		var count = $('.countvalues').val();
			var singleselect = '<div class="col-md-12" id="fieldRow'+count+'"><div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">\
												<div class="your-mail">\
													<label for="exampleInputEmail1">Option *</label>\
													<input type="text" name="field_value[]" class="form-control required" id="field_value'+count+'" placeholder="Eneter Select Value">\
												</div>\
											</div>\
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">\
												<button type="button" class="btn btn-danger add-button removesingleselect" title="Remove" data-id="'+count+'"><i class="fa fa-minus" aria-hidden="true"></i></button>\
											</div></div></div><div class="clearfix"></div>';
			$('#fieldRow1').before(singleselect);								
		});
		$('body').on('click','.removesingleselect',function(){
			var id = $(this).data('id');
			$('#fieldRow'+id).remove();
		});
	});
</script>
@endsection