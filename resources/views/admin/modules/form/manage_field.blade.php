@extends('admin.layouts.app')
@section('title', 'netbhe.com | Admin | Form')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">{{ $title }}</h4>
					<div class="submit-login no_mmg pull-right">
						@if($form->status=='INCOMPLETE')
							<a href="{{ route('admin.form.details.add',['form_id' => $form->id]) }}" title="Create"><button type="button" class="btn btn-default">Add Question</button></a>
						@endif
						<a href="{{ route('admin.form.manage') }}" title="Back"><button type="button" class="btn btn-default">back</button></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12">
									<strong>Form Name -</strong> {{ $form->form_title }}
								</div>
								<div class="col-md-12 dess5">
									<i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
									<i class="fa fa-trash-o cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">Delete</span></i>
									{{-- <i class="fa fa-eye cncl" style="border: none;" aria-hidden="true"> <span class="cncl_oopo">View Field</span></i> --}}
									
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>#</th>
													<th>Question Type</th>
													<th>Question Name</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@if($form_fields->isNotEmpty())
												@foreach(@$form_fields as $key=>$detail)
												<tr>
												<td>{{ $key+1 }}</td>
												<td>{{ ucfirst($detail['field_type']) }}</td>
												
												<td>{{ $detail->field_name }}</td>
													<td>
														<a href="{{ route('admin.form.details.edit',['form_id'=>$form->id,'fileld_id'=>@$detail->id]) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
														<a href="{{route('admin.form.details.delete',['field_id'=>@$detail->id])}}" onclick="return confirm('Do you want to delete this field?')" title="Delete"> <i class="fa fa-trash-o delet" aria-hidden="true"></i></a>
														{{-- <a href="{{ route('admin.form.details.manage',[@$detail->id]) }}" title="View Fields"> <i class="fa fa-eye delet" aria-hidden="true"></i></a> --}}
													</td>
												</tr>
												@endforeach
												
												@endif
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<form method="post" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
	var confirm = window.confirm('Are you want to delete this content ?');
	if(confirm){
		$("#destroy").attr('action',val);
		$("#destroy").submit();
	}
 }
</script>

@endsection
@section('footer')
@include('admin.includes.footer')
@endsection