@extends('admin.layouts.app')
@section('title', 'netbhe.com | Admin | Form')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">{{ $title }}</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.form.add') }}" title="Create"><button type="button" class="btn btn-default">Add Form</button></a>
					</div>
				</div>
			</div>
			<div class="row">

				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">



								<div class="panel panel-default">
									<div class="panel-body table-rep-plugin">
										<form method="get" action="{{ route('admin.form.manage') }}" >
											
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="your-mail">											
													<input class="form-control" id="keyword" name="keyword" placeholder="Keyword" value="{{@$key['keyword']}}" type="text">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="your-mail">
													<select class="form-control newdrop " name="cat_id" id="cat_id">
													<option value="">Select Category</option>
													@foreach($category as $row)
													<option value="{{$row->id}}" @if($row->id == @$key['form_cat_id']) selected @endif>{{$row->form_category}}</option>
													@endforeach
													</select>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<select class="form-control newdrop required" name="status" id="status">
													<option value="">Status</option>
													<option value="A" @if(@$key['status'] == 'A') selected @endif>Active</option>
													<option value="I" @if(@$key['status'] == 'I') selected @endif>Inactive</option>
													</select>
												</div>
											</div>
											{{-- <div class="clearfix"></div> --}}
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="submit-login add_btnm {{-- pull-right --}}" style="width:auto; margin-top:0px;">
													<input value="Search" class="btn btn-default" type="submit">
												</div>
											</div>
										</form>
									</div>
								</div>



								
								<div class="col-md-12 dess5">
									<i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
									<i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active</span></i>
									<i class="fa fa-trash-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
									<i class="fa fa-eye cncl" style="border: none;" aria-hidden="true"> <span class="cncl_oopo">View Questions</span></i>
									<i class="fa fa-square cncl" aria-hidden="true"> <span class="cncl_oopo"> Not Show in All Professional</span></i>
                                    <i class="fa fa-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Show in All Professional</span></i>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Title</th>
													<th>Description</th>
													<th>Category</th>
													<th>Added By</th>
													<th>Status</th>
													<th>Show in<br>Professional</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@if($form->isNotEmpty())
												@foreach(@$form as $detail)
												<tr>
												<td>{{ @$detail['form_title'] }}</td>
												<td><?= strlen($detail['form_dsc'])>20?substr($detail['form_dsc'],0,20).'..':$detail['form_dsc'] ?></td>
												<td>{{ @$detail->category->form_category }}</td>
												<td>{{ @$detail['added_by']=='A'?'Admin': ( @$detail->user->nick_name ? @$detail->user->nick_name : @$detail->user->name ) }}</td>
												<td>{{ @$detail['status'] }}</td>

												<td>
                                                    @if(@$detail->shown_in_proff == 'N')
                                                    <label class="label label-danger">{{ 'No' }}</label>
                                                    @elseif(@$detail->shown_in_proff == 'Y')
                                                    <label class="label label-success">{{ 'Yes' }}</label>
                                                    @endif
                                                </td>

												<td>
													<a href="{{ route('admin.form.details.manage',[@$detail->id]) }}" title="View Questions"> <i class="fa fa-eye delet" aria-hidden="true"></i></a>
													<a href="{{ route('admin.form.edit',['form_id'=>@$detail->id]) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
													@if(@$detail->status=='INCOMPLETE')
													<a href="{{ route('admin.form.active',['form_id'=>@$detail->id]) }}" title="Active" onclick="return confirm('Do you want to Active this form?');"> <i class="fa fa-check delet" aria-hidden="true"></i></a>
													@endif
													<a href="{{route('admin.form.delete',['form_id'=>@$detail->id])}}" onclick="return confirm('Do you want to delete this form?')" title="Delete"> <i class="fa fa-trash-o delet" aria-hidden="true"></i></a>
													@if($detail->shown_in_proff == 'Y')
                                                        <a href="{{ route('admin.formtools.forproff',[$detail->id]) }}" title="Not Show in Professional Page" onclick="return confirm('Do you want to Not Show in Professional Page this ');"> <i class="fa fa-square delet" aria-hidden="true"></i></a>
                                                    @else
                                                        <a href="{{ route('admin.formtools.forproff',[$detail->id]) }}" title="Show in Professional Page " onclick="return confirm('Do you want to Show in Professional Page this ');"> <i class="fa fa-square-o delet" aria-hidden="true"></i></a>
                                                    @endif
												</td>
												</tr>
												@endforeach
												
												@endif
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


			</div>
		</div>
	</div>
</div>

<form method="post" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
	var confirm = window.confirm('Are you want to delete this content ?');
	if(confirm){
		$("#destroy").attr('action',val);
		$("#destroy").submit();
	}
 }
</script>

@endsection
@section('footer')
@include('admin.includes.footer')
@endsection