@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Add Form')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">{{ $title }}</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.form.category.manage') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
				</div>
			</div>
			@include('admin.includes.error')
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
										@if(@$details->id)
										<form id="myform" method="post" action="{{ route('admin.form.category.update',['cat_id'=>@$details->id]) }}">
											@else
											<form id="myform" method="post" action="{{ route('admin.form.category.create') }}">
											@endif
											{{ csrf_field() }}
											<!-- <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Type *</label>
													<select name="cat_type" class="form-control newdrop required formType">
														<option value="">Select</option>
														<option value="FORM" @if(@$details->cat_type=='FORM') selected @endif>Form</option>
														<option value="IMPORTED" @if(@$details->cat_type=='IMPORTED') selected @endif>Imported</option>
													</select>
												</div>
											</div> -->
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Category Name *</label>
												<input type="text" name="cat_name" class="form-control required" id="cat_name" placeholder="Category Name" value="{{ @$details->form_category }}">
												</div>
											</div>
																						
											<div class="clearfix"></div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="submit-login add_btnm">
													<input value="{{ $button }}" type="submit" class="btn btn-default">
												</div>
											</div>
											<!--all_time_sho-->
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>
<script>
	$(document).ready(function(){
		$("#myform").validate();
	});
</script>
<style>
	.error{
		color: red !important;
	}
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
<script>
	$(document).ready(function(){
		$('.formType').change(function(){
			var type = $(this).val();
			var count = $('.countvalues').val();
			var singleselect = '<div class="col-md-12" id="fieldRow'+count+'"><div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">\
												<div class="your-mail">\
													<label for="exampleInputEmail1">Values *</label>\
													<input type="text" name="field_value[]" class="form-control required" id="field_value'+count+'" placeholder="Eneter Select Value">\
												</div>\
											</div>\
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">\
												<button type="button" class="btn btn-success add-button addsingleselect" title="Add"><i class="fa fa-plus" aria-hidden="true"></i></button>\
											</div></div></div><div class="clearfix"></div>';
			if(type=='SINGLESELECT' || type=='MULTISELECT'){
				var newcount = parseInt(count)+1;
				$('.countvalues').val(newcount);
				$('#fields').append(singleselect);
			}
		});
		$('body').on('click','.addsingleselect',function(){
		var count = $('.countvalues').val();
			var singleselect = '<div class="col-md-12" id="fieldRow'+count+'"><div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">\
												<div class="your-mail">\
													<label for="exampleInputEmail1">Values *</label>\
													<input type="text" name="field_value[]" class="form-control required" id="field_value'+count+'" placeholder="Eneter Select Value">\
												</div>\
											</div>\
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">\
												<button type="button" class="btn btn-danger add-button removesingleselect" title="Remove" data-id="'+count+'"><i class="fa fa-minus" aria-hidden="true"></i></button>\
											</div></div></div><div class="clearfix"></div>';
			$('#fields').append(singleselect);								
		});
		$('body').on('click','.removesingleselect',function(){
			var id = $(this).data('id');
			$('#fieldRow'+id).remove();
		});
	});
</script>
@endsection