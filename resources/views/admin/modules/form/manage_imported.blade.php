@extends('admin.layouts.app')
@section('title', 'netbhe.com | Admin | Form')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">{{ $title }}</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.imported.add') }}" title="Create"><button type="button" class="btn btn-default">Add Imported</button></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 dess5">
									<i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
									<i class="fa fa-trash-o cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">Delete</span></i>
									<i class="fa fa-info cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">View help tools </span></i>
									<i class="fa fa-square cncl" aria-hidden="true"> <span class="cncl_oopo"> Not Show in All Professional</span></i>
                                    <i class="fa fa-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Show in All Professional</span></i>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Title</th>
													<th>Category</th>
													<th>Added By</th>
													<th>Status</th>
													<th>Show in<br>Professional</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@if($importeds->isNotEmpty())
												@foreach(@$importeds as $detail)
												<tr>
												<td>{{ $detail['title'] }}</td>												
												<td>{{ $detail->category['form_category'] }}</td>												
												<td>{{ $detail['added_by']=='A'?'Admin':$detail->user['name'] }}</td>												
												<td>{{ $detail['status'] }}</td>
												<td>
                                                    @if(@$detail->shown_in_proff == 'N')
                                                    <label class="label label-danger">{{ 'No' }}</label>
                                                    @elseif(@$detail->shown_in_proff == 'Y')
                                                    <label class="label label-success">{{ 'Yes' }}</label>
                                                    @endif
                                                </td>												
												<td>
													<a href="{{ route('admin.imported.edit',['imp_id'=>@$detail->id]) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
													<a href="javascript:;" class="view_tools_help acpt assginbtn" data-title="{{$detail->title}}" data-helpdesc="{{ $detail->help_tools_view }}" style="margin: 2px;" title=" view help tools"><i class="fa fa-info delet" aria-hidden="true"></i></a>
													<a href="{{route('admin.imported.delete',['imp_id'=>@$detail->id])}}" onclick="return confirm('Do you want to delete this imported?')" title="Delete"> <i class="fa fa-trash-o delet" aria-hidden="true"></i></a>
													@if($detail->shown_in_proff == 'Y')
                                                        <a href="{{ route('admin.importedtools.forproff',[$detail->id]) }}" title="Not Show in Professional Page" onclick="return confirm('Do you want to Not Show in Professional Page this ');"> <i class="fa fa-square delet" aria-hidden="true"></i></a>
                                                    @else
                                                        <a href="{{ route('admin.importedtools.forproff',[$detail->id]) }}" title="Show in Professional Page " onclick="return confirm('Do you want to Show in Professional Page this ');"> <i class="fa fa-square-o delet" aria-hidden="true"></i></a>
                                                    @endif
												</td>
												</tr>
												@endforeach
												
												@endif
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--View tool help Modal start -->
<div class="modal fade" id="view_tool_help_Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Tool Title: <span class="tool-title"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
            <div class="modal-body">
                <span class="tool-help-desc-view"></span>
            </div>           
        </div>
    </div>
</div>
<!--View tool help Modal end -->


<form method="post" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
	var confirm = window.confirm('Are you want to delete this content ?');
	if(confirm){
		$("#destroy").attr('action',val);
		$("#destroy").submit();
	}
 }
</script>


<script>

$(document).ready(function(){ 

    $('.view_tools_help').click(function(){
        var title = $(this).data('title'); 
        var help_desc = $(this).data('helpdesc'); 
        $('.tool-title').html(title);
        $('.tool-help-desc-view').html(help_desc);       
        $('#view_tool_help_Modal').modal('show');
    });

});

$(".chosen-select").chosen();

$(document).ready(function(){
        $("#myform").validate();
    });
</script>


@endsection
@section('footer')
@include('admin.includes.footer')
@endsection