@extends('admin.layouts.app')
@section('title', 'NearO | Admin | Coupon')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Manage Document</h4>
                    <div class="submit-login no_mmg pull-right">
                        <a href="{{ route('admin.document.add') }}" title="Create"><button type="button"
                                class="btn btn-default">Add</button></a>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-rep-plugin">
                            <div class="row">
                                {{-- <div class="panel panel-default">
									<div class="panel-body table-rep-plugin">
										<form method="get" action="{{ route('admin.coupon')}}" >

                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <div class="your-mail">
                                        <input class="form-control" id="name" name="name" placeholder="Keyword"
                                            value="{{ @$keys['name'] }}" type="text">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="your-mail">
                                        <select class="form-control newdrop required" name="status" id="status">
                                            <option value="">Status</option>
                                            <option value="A" @if(@$keys['status']=="A" ) {{ "selected" }} @endif>Active
                                            </option>
                                            <option value="I" @if(@$keys['status']=="I" ) {{ "selected" }} @endif>
                                                Inactive</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="add_btnm pull-right" style="width:auto; margin-top:15px;">
                                        <input value="Search" class="btn btn-primary" type="submit">
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div> --}}
                        <div class="col-md-12 dess5">
                            <i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span
                                    class="cncl_oopo">Edit</span></i>
                            <i class="fa fa-trash-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
                            <i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active</span></i>
                            <i class="fa fa-times cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">Inactive</span></i>
                            {{-- <i class="fa fa-copy cncl" aria-hidden="true" style="border: none;"> <span
                                    class="cncl_oopo">Copy</span></i> --}}
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="table-responsive" data-pattern="priority-columns">
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Created By</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach(@$template as $row)
                                        <tr>
                                            <td>{{ @$row->title}}</td>
                                            <td>@if(@$row->created_by=='A') Admin @endif @if(@$row->created_by=='P') Professional @endif</td>
                                            <td>
                                                @if(@$row->status == 'I')
                                                <label class="label label-danger">Inactive</label>
                                                @elseif(@$row->status == 'A')
                                                <label class="label label-success">Active</label>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('admin.document.edit',[@$row->id]) }}" title="Edit"> <i
                                                        class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
                                                {{-- <a href="{{ @$row->coupon_code }}" title="Copy" class="copy_coupon"> <i
                                                        class="fa fa-copy delet" aria-hidden="true"></i></a> --}}
                                                {{-- @if(@$row->is_used == 'N') --}}
                                                <a href="{{route('admin.document.delete',[@$row->id])}}"
                                                    onclick="return confirm('Are you want to delete this coupon ?');"
                                                    title="Delete"> <i class="fa fa-trash-o delet"
                                                        aria-hidden="true"></i></a>
                                                {{-- @endif --}}
                                                @if(@$row->status == 'I')
                                                <a href="{{ route('admin.document.status',[@$row->id]) }}" title="Active"
                                                    onclick="return confirm('Do you want to active this coupon ?');"> <i
                                                        class="fa fa-check delet" aria-hidden="true"></i></a>
                                                @elseif(@$row->status == 'A')
                                                <a href="{{ route('admin.document.status',[@$row->id]) }}"
                                                    title="Inactive"
                                                    onclick="return confirm('Do you want to inactive this coupon ?');">
                                                    <i class="fa fa-times delet" aria-hidden="true"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>

@endsection
@section('footer')
@include('admin.includes.footer')
@endsection