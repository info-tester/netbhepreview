@extends('admin.layouts.app')
@section('title', 'Netbhe | Admin | Landing Page Leads')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h3 class="pull-left page-title">Landing Page Leads</h3>
					{{-- <div class="submit-login no_mmg pull-right">
						<a href="{{ route('manage.language.add') }}" title="Create"><button type="button" class="btn btn-default">Add language</button></a>
					</div> --}}
                    <div class="submit-login no_mmg pull-right">
                        <a href="{{route('admin.landing.page.list')}}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
                    </div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">

                        <div class="panel-body">
                            <div class="right_dash ml-auto">
                                <div class="information_box" style="    border: 1px solid #8f8f8f; padding: 5px 12px;">
                                <h4><span>  <strong>Template Information </strong></span></h4>
                                <div class="information_area">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-12">
                                            <div class="form-group">
                                                <label class="type_label1 extra">Landing Page Title: </label>
                                                <label class="type_label2 ">{{@$landing->landing_title}}</label>
                                            </div>
                                            <div class="form-group">
                                                <label class="type_label1 extra">Template: </label>
                                                <label class="type_label2 ">Template - {{@$landing->landing_template_id}}</label>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>

						<div class="panel-body table-rep-plugin">
							<div class="row">
                                <div class="row top_from">
                                </div>
                                <div class="clearfix"></div>
								{{-- <div class="col-md-12 dess5">
									<i class="fa fa fa-eye cncl" aria-hidden="true"> <span class="cncl_oopo">Preview</span></i>
									<i class="fa fa fa-eye cncl" aria-hidden="true"> <span class="cncl_oopo">View Leads</span></i>
									<i class="fa fa-copy" aria-hidden="true"> <span class="cncl_oopo">Copy Link</span></i>
								</div> --}}
                                <div class="clearfix"></div>
                                <div class="clearfix"></div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
                                                    @if(@$landing->lead_fname=='Y')
                                                    <th>First Name</th>
                                                    @endif
                                                    @if(@$landing->lead_lname=='Y')
                                                    <th>Last Name</th>
                                                    @endif
                                                    @if(@$landing->lead_email=='Y')
                                                    <th>Email</th>
                                                    @endif
                                                    @if(@$landing->lead_mobile=='Y')
                                                    <th>Mobile</th>
                                                    @endif
                                                    @if(@$landing->lead_message=='Y')
                                                    <th>Message</th>
                                                    @endif
                                                    @if(@$landing->lead_address=='Y')
                                                    <th>Address</th>
                                                    @endif
                                                    @if(@$landing->lead_country_code=='Y')
                                                    <th>Country Code</th>
                                                    @endif
                                                    @if(@$landing->lead_city=='Y')
                                                    <th>City</th>
                                                    @endif
                                                    @if(@$landing->lead_state=='Y')
                                                    <th>State</th>
                                                    @endif
                                                    @if(@$landing->lead_postal_code=='Y')
                                                    <th>Postal Code</th>
                                                    @endif
                                                    @if(@$landing->lead_website=='Y')
                                                    <th>Website</th>
                                                    @endif
                                                    @if(@$landing->lead_facebook=='Y')
                                                    <th>Facebook Link</th>
                                                    @endif
                                                    @if(@$landing->lead_instagram=='Y')
                                                    <th>Instagram Link</th>
                                                    @endif
                                                    @if(@$landing->lead_linkedin=='Y')
                                                    <th>Linkedin Link</th>
                                                    @endif
                                                </tr>
											</thead>
											<tbody>
												@if(@$leads->isNotEmpty())
												@foreach(@$leads as $detail)
												<tr>
                                                    @if(@$landing->lead_fname=='Y')
                                                    <td>{{@$detail->fname??'-'}}</td>
                                                    @endif
                                                    @if(@$landing->lead_lname=='Y')
                                                    <td>{{@$detail->lname??'-'}}</td>
                                                    @endif
                                                    @if(@$landing->lead_email=='Y')
                                                    <td>{{@$detail->email??'-'}}</td>
                                                    @endif
                                                    @if(@$landing->lead_mobile=='Y')
                                                    <td>{{@$detail->mobile??'-'}}</td>
                                                    @endif
                                                    @if(@$landing->lead_message=='Y')
                                                    <td>{{@$detail->message??'-'}}</td>
                                                    @endif
                                                    @if(@$landing->lead_address=='Y')
                                                    <td>{{@$detail->address??'-'}}</td>
                                                    @endif
                                                    @if(@$landing->lead_country_code=='Y')
                                                    <td>{{@$detail->country_code??'-'}}</td>
                                                    @endif
                                                    @if(@$landing->lead_city=='Y')
                                                    <td>{{@$detail->city??'-'}}</td>
                                                    @endif
                                                    @if(@$landing->lead_state=='Y')
                                                    <td>{{@$detail->state??'-'}}</td>
                                                    @endif
                                                    @if(@$landing->lead_postal_code=='Y')
                                                    <td>{{@$detail->postal_code??'-'}}</td>
                                                    @endif
                                                    @if(@$landing->lead_website=='Y')
                                                    <td>{{@$detail->website??'-'}}</td>
                                                    @endif
                                                    @if(@$landing->lead_facebook=='Y')
                                                    <td>{{@$detail->facebook_link??'-'}}</td>
                                                    @endif
                                                    @if(@$landing->lead_instagram=='Y')
                                                    <td>{{@$detail->instagram_link??'-'}}</td>
                                                    @endif
                                                    @if(@$landing->lead_linkedin=='Y')
                                                    <td>{{@$detail->linkedin_link??'-'}}</td>
                                                    @endif

												</tr>
												@endforeach

												@endif
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
 function copyText(text){
    var input = document.createElement('input');
    input.setAttribute('value', text);
    document.body.appendChild(input);
    input.select();
    var result = document.execCommand('copy');
    document.body.removeChild(input);
    toastr.success("Link Copied");
    return result;
}
</script>
<style>
    .extra{
        width: 204px !important;
    }
</style>

@endsection
@section('footer')
@include('admin.includes.footer')

@endsection
