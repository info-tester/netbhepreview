@extends('admin.layouts.app')
@section('title', 'Netbhe | Admin | Landing Page Payment')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h3 class="pull-left page-title">Landing Payments</h3>
					{{-- <div class="submit-login no_mmg pull-right">
						<a href="{{ route('manage.language.add') }}" title="Create"><button type="button" class="btn btn-default">Add language</button></a>
					</div> --}}
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
                                <div class="row top_from">
                                    <form method="post" action="{{ route('admin.landing.page.payment')}}">
                                        @csrf
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="token">Order Id</label>
                                                <input type="text" name="token" value="{{@$key['token']}}" id="token">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="professional">Professional Name</label>
                                                <select name="professional" id="professional" class="form-control newdrop">
                                                    <option value="">Choose Professional</option>
                                                    @foreach($experts as $astr)
                                                    <option value="{{ @$astr->id }}" @if(@$key['professional'] == $astr->id) {{ "selected" }} @endif>{{ @$astr->nick_name ? @$astr->nick_name : @$astr->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="payment_for">Payment For</label>
                                                <select name="payment_for" id="payment_for" class="form-control newdrop">
                                                    <option value="">Select payment for</option>
                                                    <option value="T"  @if(@$key['payment_for'] == 'T') {{ "selected" }} @endif>Template</option>
                                                    <option value="B" @if(@$key['payment_for'] == 'B') {{ "selected" }} @endif>Banding</option>
                                                    <option value="BT" @if(@$key['payment_for'] == 'BT') {{ "selected" }} @endif>Template & Banding</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="is_used">Used </label>
                                                <select name="is_used" id="is_used" class="form-control newdrop">
                                                    <option value="">Select Used</option>
                                                    <option value="Y" @if(@$key['is_used'] == 'Y') {{ "selected" }} @endif>Yes</option>
                                                    <option value="N" @if(@$key['is_used'] == 'N') {{ "selected" }} @endif>No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="payment_type">Payment Type </label>
                                                <select name="payment_type" id="payment_type" class="form-control newdrop">
                                                    <option value="">Select Payment Type</option>
                                                    <option value="C" @if(@$key['payment_type'] == 'C') {{ "selected" }} @endif>WireCard</option>
                                                    <option value="S" @if(@$key['payment_type'] == 'S') {{ "selected" }} @endif>Stripe</option>
                                                    <option value="P" @if(@$key['payment_type'] == 'P') {{ "selected" }} @endif>Paypal</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="submit-login add_btnm" style="width:auto; margin-top:35px;">
                                                <input value="Search" id="search" class="btn btn-default" type="submit">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="clearfix"></div>
								<div class="col-md-12 dess5">
									{{-- <i class="fa fa-eye-slash cncl" aria-hidden="true"> <span class="cncl_oopo">Preview</span></i>
									<i class="fa fa fa-eye cncl" aria-hidden="true"> <span class="cncl_oopo">View Leads</span></i>
									<i class="fa fa-copy" aria-hidden="true"> <span class="cncl_oopo">Copy Link</span></i> --}}
								</div>
                                <div class="clearfix"></div>
                                <div class="clearfix"></div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Order Id</th>
													<th>Landing Page Title</th>
													<th>Professional Name</th>
													<th>Payment For</th>
													<th>Used</th>
													<th>Used</th>
													<th>Amount</th>
												</tr>
											</thead>
											<tbody>
												@if(@$payments->isNotEmpty())
												@foreach(@$payments as $detail)
												<tr>
													<td>{{@$detail->token_no}}</td>
													<td>{{@$detail->landingDetails->landing_title??'--'}}</td>
													<td>{{ @$detail->userDetails->nick_name ? @$detail->userDetails->nick_name : @$detail->userDetails->name}}</td>

													<td>
                                                        @if(@$detail->payment_for=='T') Template
                                                        @elseif(@$detail->payment_for=='B')Banding
                                                        @elseif(@$detail->payment_for=='BT') Template & Banding
                                                        @endif
                                                    </td>
													<td>
                                                        @if(@$detail->is_use=='Y') Yes
                                                        @elseif(@$detail->is_use=='N')No
                                                        @endif
                                                    </td>
													<td>
                                                        @if(@$detail->payment_type=='C') WireCard
                                                        @elseif(@$detail->payment_type=='S') Stripe
                                                        @elseif(@$detail->payment_type=='P') Paypal
                                                        @endif
                                                    </td>
                                                    <td>$ {{(int)@$detail->total_amount}}</td>

												</tr>
												@endforeach

												@endif
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<form method="post" id="destroy">
    {{ csrf_field() }}
    {{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
    var confirm = window.confirm('Do you want to delete this logbook tools?');
    if(confirm){
        $("#destroy").attr('action',val);
        $("#destroy").submit();
    }
 }
 function copyText(text){
    var input = document.createElement('input');
    input.setAttribute('value', text);
    document.body.appendChild(input);
    input.select();
    var result = document.execCommand('copy');
    document.body.removeChild(input);
    toastr.success("Link Copied");
    return result;
}
</script>

@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
