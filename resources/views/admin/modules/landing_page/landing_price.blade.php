@extends('admin.layouts.app')
@section('title', 'Netbhe | Admin | Landing Page Price')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Landing Page Price</h4>
                    {{-- <div class="submit-login no_mmg pull-right">
                        <a href="{{ route('admin.refer.discount') }}" title="Back"><button type="button"
                                class="btn btn-default">Back</button></a>
                    </div> --}}
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-rep-plugin">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 nhp">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <form id="myform" method="post"
                                            action="{{route('admin.landing.page.price.update')}}"
                                            enctype="multipart/form-data">
                                            @csrf
                                            <div class="clearfix"></div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="free_landing_pages">Free Landing Page number</label>
                                                    <input type="text" name="free_landing_pages"
                                                        class="form-control required" id="free_landing_pages"
                                                        value="{{old('free_landing_pages',@$price->free_landing_pages)}}">
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="price_per_page">Landing Page Price ($)</label>
                                                    <input type="text" name="price_per_page"
                                                        class="form-control required" id="price_per_page"
                                                        value="{{old('price_per_page',(int)@$price->price_per_page)}}">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="price_branding_free">Branding Free Price ($)</label>
                                                    <input type="text" name="price_branding_free"
                                                        class="form-control required" id="price_branding_free"
                                                        value="{{old('aprice_branding_free',(int)@$price->price_branding_free)}}">
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>

                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="add_btnm submit-login">
                                                    <input value="Update" type="submit" class="btn btn-default">
                                                </div>
                                            </div>
                                            <!--all_time_sho-->
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- container -->
    </div>
    <!-- content -->
</div>
<script>
    $(document).ready(function(){
		$("#myform").validate({
			rules: {
                free_landing_pages : 'digits',
                price_per_page : 'digits',
                price_branding_free : 'digits',
			},
			messages : {
                free_landing_pages : {
					required 		: "Please enter free landing page.",
					number 			: "Please enter number."
				},
                price_per_page : {
					required 		: "Please enter price.",
					number 			: "Please enter number."
				},
                price_branding_free : {
					required 		: "Please enter price.",
					number 			: "Please enter number."
				},
	        },
	        errorPlacement: function (error, element)
	        {
	            // toastr.error(error.text());
                error.insertAfter(element);
	        }
		});
	});
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
