@extends('admin.layouts.app')
@section('title', 'Netbhe | Admin | Landing Page')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h3 class="pull-left page-title">Manage Landing Page</h3>
					{{-- <div class="submit-login no_mmg pull-right">
						<a href="{{ route('manage.language.add') }}" title="Create"><button type="button" class="btn btn-default">Add language</button></a>
					</div> --}}
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
                                <div class="row top_from">
                                    <form method="post" action="{{ route('admin.landing.page.list')}}">
                                        @csrf
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="professional">Professional Name</label>
                                                <select name="professional" id="professional" class="form-control newdrop">
                                                    <option value="">Choose Professional</option>
                                                    @foreach($experts as $astr)
                                                    <option value="{{ @$astr->id }}" @if(@$key['professional'] == $astr->id) {{ "selected" }} @endif>{{ @$astr->nick_name ? @$astr->nick_name : @$astr->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="template_id">Template</label>
                                                <select name="template_id" id="template_id" class="form-control newdrop">
                                                    <option value="">Select Template</option>
                                                    <option value="1" @if(@$key['template_id'] == 1) {{ "selected" }} @endif >Template - 1</option>
                                                    <option value="2" @if(@$key['template_id'] == 2) {{ "selected" }} @endif>Template - 2</option>
                                                    <option value="3" @if(@$key['template_id'] == 3) {{ "selected" }} @endif>Template - 3</option>
                                                    <option value="4" @if(@$key['template_id'] == 4) {{ "selected" }} @endif>Template - 4</option>

                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="page_type">Page Type</label>
                                                <select name="page_type" id="page_type" class="form-control newdrop">
                                                    <option value="">Select Template</option>
                                                    <option value="F"  @if(@$key['page_type'] == 'F') {{ "selected" }} @endif>Free</option>
                                                    <option value="P" @if(@$key['page_type'] == 'P') {{ "selected" }} @endif>Paid</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="branding_free">Branding Free</label>
                                                <select name="branding_free" id="branding_free" class="form-control newdrop">
                                                    <option value="">Select Template</option>
                                                    <option value="Y" @if(@$key['branding_free'] == 'Y') {{ "selected" }} @endif>Yes</option>
                                                    <option value="N" @if(@$key['branding_free'] == 'N') {{ "selected" }} @endif>No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="page_status">Status</label>
                                                <select name="page_status" id="page_status" class="form-control newdrop">
                                                    <option value="">Select Status</option>
                                                    <option value="B" @if(@$key['page_status'] == 'B') {{ "selected" }} @endif>Block</option>
                                                    <option value="P" @if(@$key['page_status'] == 'P') {{ "selected" }} @endif>Publish</option>
                                                    <option value="UP" @if(@$key['page_status'] == 'UP') {{ "selected" }} @endif>Unpublish</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="submit-login add_btnm" style="width:auto; margin-top:35px;">
                                                <input value="Search" id="search" class="btn btn-default" type="submit">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="clearfix"></div>
								<div class="col-md-12 dess5">
									<i class="fa fa-eye-slash cncl" aria-hidden="true"> <span class="cncl_oopo">Preview</span></i>
									<i class="fa fa fa-eye cncl" aria-hidden="true"> <span class="cncl_oopo">View Leads</span></i>
									<i class="fa fa-times cncl" aria-hidden="true"> <span class="cncl_oopo">Block</span></i>
									<i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Unblock</span></i>
									<i class="fa fa-copy" aria-hidden="true"> <span class="cncl_oopo">Copy Link</span></i>
								</div>
                                <div class="clearfix"></div>
                                <div class="clearfix"></div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Landing Page Title</th>
													<th>Template</th>
													<th>Professional Name</th>
													<th>Page Type</th>
													<th>Page price</th>
													<th>Branding Free</th>
													<th>Branding price</th>
													<th>Status</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@if(@$templates->isNotEmpty())
												@foreach(@$templates as $detail)
												<tr>
													<td>{{ @$detail->landing_title }}</td>
													<td>Template - {{ @$detail->landing_template_id }}</td>
													<td>{{ @$detail->userDetails->nick_name ? @$detail->userDetails->nick_name : @$detail->userDetails->name}}</td>

													<td>
                                                        @if(@$detail->page_type=='P')
                                                        Paid
                                                        @elseif(@$detail->page_type=='F')
                                                        Free
                                                        @endif
                                                    </td>
													<td>
                                                        @if(@$detail->page_type=='P')
                                                        {{(int)@$detail->page_fees}}
                                                        @elseif(@$detail->page_type=='F')
                                                        --
                                                        @endif
                                                    </td>
													<td>
                                                        @if(@$detail->is_branding_free=='Y')
                                                        Yes
                                                        @elseif(@$detail->is_branding_free=='N')
                                                        No
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if(@$detail->is_branding_free=='Y')
                                                        ${{(int)@$detail->branding_fees}}
                                                        @elseif(@$detail->is_branding_free=='N')
                                                        --
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if(@$detail->page_status=='B')
                                                        Block
                                                        @elseif(@$detail->page_status=='P')
                                                        Publish
                                                        @elseif(@$detail->page_status=='UP')
                                                        Unpublish
                                                        @endif
                                                    </td>
													<td>
                                                        @if(@$detail->page_status=='UP'|| @$detail->page_status=='P')
														<a href="{{ route('admin.landing.page.status',['status'=>'B','id'=>@$detail->id]) }}" title="Block" > <i class="fa fa-times" aria-hidden="true"></i></a>
                                                        @elseif(@$detail->page_status=='B')
														<a href="{{ route('admin.landing.page.status',['status'=>'UP','id'=>@$detail->id]) }}" title="Unblock" > <i class="fa fa-check" aria-hidden="true"></i></a>
                                                        @endif
														<a href="{{ route('landing.page',['prof_slug'=>@$detail->userDetails->slug,'slug'=>@$detail->slug]) }}" title="Preview" target="_blank"> <i class="fa fa-eye-slash" aria-hidden="true"></i></a>
														<a href="{{ route('admin.landing.page.leads',['id'=>@$detail->id]) }}" title="View Leads"> <i class="fa fa fa-eye" aria-hidden="true"></i></a>
														<a href="javascript:;" title="Copy Links" onclick=" copyText('{{ route('landing.page',['prof_slug'=>@$detail->userDetails->slug,'slug'=>@$detail->slug]) }}')"> <i class="fa fa-copy" aria-hidden="true"></i></a>
													</td>
												</tr>
												@endforeach

												@endif
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<form method="post" id="destroy">
    {{ csrf_field() }}
    {{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
    var confirm = window.confirm('Do you want to delete this logbook tools?');
    if(confirm){
        $("#destroy").attr('action',val);
        $("#destroy").submit();
    }
 }
 function copyText(text){
    var input = document.createElement('input');
    input.setAttribute('value', text);
    document.body.appendChild(input);
    input.select();
    var result = document.execCommand('copy');
    document.body.removeChild(input);
    toastr.success("Link Copied");
    return result;
}
</script>

@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
