@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Edit')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<script src="{{URL::to('public/js/chosen.jquery.min.js')}}"></script>
<link rel="stylesheet" href="{{URL::to('public/css/chosen.css')}}">

<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Edit content template</h4>
                    <div class="submit-login no_mmg pull-right">
                        <a href="{{ route('tool-content-template.index') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
                    </div>
                </div>
            </div>
            @include('admin.includes.error')
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-rep-plugin">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 nhp">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        
                                            <form id="myform" method="post" action="{{ route('tool-content-template.update',['id'=>@$details->id]) }}">
                                            <input name="_method" type="hidden" value="PUT">
                                            {{ csrf_field() }}
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
                                    <div class="your-mail">
                                        <label for="exampleInputEmail1" class="personal-label">Subject *</label>
                                        <input type="text" name="cont_subject" value="{{ $details->cont_subject }}" class="form-control required" id="cont_subject" placeholder="Subject" >
                                    </div>
                                </div>

                                @if($details->added_by != 'P')
                                <div class="col-lg-6 col-md-12 col-sm-6 col-xs-12">
                                    <div class="your-mail">
                                        <label for="exampleInputEmail1">Professional speciality  *</label>
                                        <select name="speciality_ids[]" id="speciality_ids" data-placeholder ="Select professional speciality" class="required form-control newdrop required chosen-select" multiple="true" required="">
                                        @foreach ($specialities as $spec)
                                            <option value="{{ $spec->id }}"
                                            @if(@$related_spec_ids && in_array($spec->id, $related_spec_ids)) selected @endif
                                            >{{ $spec->name }}</option>
                                        @endforeach
                                        </select>
                                        <label class="error" id="chosen_select_error" style="display:none;">This field is required</label>
                                    </div>
                                </div>
                                @endif
                               
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="personal-label" for="exampleInputEmail1">@lang('site.description') *</label>
                                        <div class="clearfix"></div>
                                        <textarea name="cont_description" id="desc" rows="10" placeholder="Description" style="width:100%" class="form-control required desc"><?= $details->cont_description ?></textarea>
                                    </div>
                                    <div class="clearfix"></div>
                                    <p class="error_1" id="cntnt"></p>
                                    <div class="clearfix"></div>
                                </div>    

                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="your-mail">
                                        <label class="personal-label">Select category</label>
                                        <select class="form-control personal-select" name="cont_category" id="filetype">
                                            <option value="">Select</option>                                            
                                            <option value="video" @if(@$details->cont_category=="video") selected @endif>Video</option> 
                                            <option value="audio"@if(@$details->cont_category=="audio") selected @endif>Audio</option> 
                                            <option value="image"@if(@$details->cont_category=="image") selected @endif>Image</option>
                                        </select>
                                        
                                    </div>
                                </div>

                                {{-- for video --}}
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px" id="videofile">
                                   
                                    <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                                        <div class="your-mail">
                                            <label for="exampleInputEmail1">Youtube Video</label>
                                            <input type="text" name="cont_cat_video_img_audio" placeholder="Youtube Video" class="personal-type" id="youTubeUrl">
                                            {{-- <textarea name="youtube_video" rows="4" cols="50" class="form-control required"></textarea> --}}
                                            Example: https://www.youtube.com/watch?v=Kb8CW3axqRE 

                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12" id="iframeBox" style="display: none">
                                        <iframe id="videoObject" type="text/html" width="300" height="200" frameborder="0" allowfullscreen></iframe>
                                    </div>

                                </div>

                                {{-- for audio --}}
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px" id="audiofile">
                                    <div class="your-mail">
                                        <label for="exampleInputEmail1" class="personal-label">Upload audio *</label>
                                        <input class="personal-type" type="file" name="cont_cat_video_img_audio" id="cont_audio" style="margin-top: 5px;">                                    </div>
                                </div>
                                {{-- for image --}}
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px" id="imagefile">
                                    <div class="your-mail">
                                        <label for="exampleInputEmail1" class="personal-label">Upload Image *</label>
                                        <input class="personal-type" type="file" name="cont_cat_video_img_audio" id="cont_image" style="margin-top: 5px;">
                                    </div>
                                </div>
                                            
                                            
                                            <div class="clearfix"></div>
                                            <br>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="submit-login add_btnm">
                                                    <input value="submit" type="submit" class="btn btn-default">
                                                </div>
                                            </div>
                                            <!--all_time_sho-->
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Row -->
        </div>
        <!-- container -->
    </div>
    <!-- content -->
</div>


<style type="text/css">
    .upldd {
    display: block;
    width: auto;
    border-radius: 4px;
    text-align: center;
    background: #9caca9;
    cursor: pointer;
    overflow: hidden;
    padding: 10px 15px;
    font-size: 15px;
    color: #fff;
    cursor: pointer;
    float: left;
    margin-top: 15px;
    font-family: 'Poppins', sans-serif;
    position: relative;
}
.upldd input {
    position: absolute;
    font-size: 50px;
    opacity: 0;
    left: 0;
    top: 0;
    width: 200px;
    cursor: pointer;
}
.upldd:hover{
    background: #1781d2;
}

</style>

    <script>
    $(document).ready(function(){
        var added_by = "{{$details->added_by}}";
        console.log(added_by);
        $(".chosen-select").chosen();
        $('.chosen-select').trigger('chosen:updated');
        $("#myform").validate({
            submitHandler: function(form) {
                var flag = 0;
                if($('#speciality_ids').val() == null && added_by != 'P'){
                    $('#chosen_select_error').css('display', 'block');
                    flag = 1;
                }
                if(flag == 1){
                    return false;
                } else {
                    $('#chosen_select_error').css('display', 'none');
                    // console.log("Submitted");
                    // return false;
                    
                }

                if($('#youTubeUrl').val() != ' ' ){
                    var url = $('#youTubeUrl').val();
                    if(ytVidId(url) !== false){
                        alert('not valid');
                        $('#iframeBox').hide()
                        return false;
                    }else{
                        return true;
                    }
                }
                form.submit();
            }
        });
        tinyMCE.init({
            mode : "specific_textareas",
            editor_selector : "desc",
            height: '320px',
            plugins: [
              'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
              'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
              'save table contextmenu directionality emoticons template paste textcolor'
            ],
            relative_urls : false,
            remove_script_host : false,
            convert_urls : true,
            toolbar: 'insertfile undo redo | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor emoticons',
                images_upload_url: '{{ URL::to('storage/app/public/uploads/content_image/') }}',
                images_upload_handler: function(blobInfo, success, failure) {
                    var formD = new FormData();
                    formD.append('file', blobInfo.blob(), blobInfo.filename());
                    formD.append( "_token", '{{csrf_token()}}');
                    $.ajax({
                        url: '{{route("admin.artical.img.upload")}}',
                        data: formD,
                        type: 'POST',
                        contentType: false,
                        cache: false,
                        processData:false,
                        dataType: 'JSON',
                        success: function(jsn) {
                            if(jsn.status == 'ERROR') {
                                failure(jsn.error);
                            } else if(jsn.status == 'SUCCESS') {
                                success(jsn.location);
                            }
                        }
                    });
                }, 
            });
         
    });



    $(function() {
        $('#videofile').hide(); 
        $('#audiofile').hide();
        $('#imagefile').hide();
        $('#filetype').change(function(){
            if($('#filetype').val() == 'video') {
                $('#videofile').show(); 
                $('#audiofile').hide();
                $('#imagefile').hide();
            }else if($('#filetype').val() == 'audio') {
                $('#audiofile').show();
                $('#videofile').hide(); 
                $('#imagefile').hide(); 
            }else if($('#filetype').val() == 'image') {
                $('#imagefile').show();
                $('#audiofile').hide();
                $('#videofile').hide(); 
                 
            }else {
                $('#reminderdate').hide(); 
                $('#dayofweek').hide();
                $('#imagefile').hide();
            } 
        });
    });


</script>

<script>
$(document).ready(function(){
    $('#youTubeUrl').blur(function(){


    var url = $('#youTubeUrl').val();
        if(url != ''){
        $('#iframeBox').show();
    } else {
    $('#iframeBox').hide()
    };


    validateYouTubeUrl();
    }); 
});

// $(document).ready(function(){
//  var url = $('#youTubeUrl').val();   
//  if(url != ''){
//      $('#iframeBox').show();
//  } else {
//  $('#iframeBox').hide()
//  }
// });

function validateYouTubeUrl() {    
    var url = $('#youTubeUrl').val();
    if (url != undefined || url != '') {        
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
        var match = url.match(regExp);
        if (match && match[2].length == 11) {
            '<div class="munmundey">';
            // Do anything for being valid
            // if need to change the url to embed url then use below line            
            $('#videoObject').attr('src', 'https://www.youtube.com/embed/' + match[2] + '?autoplay=1&enablejsapi=1');
        '</div>';
    

        } else {
            alert('not valid');
            
            // Do anything for not being valid
        }
    }
}
</script>
<script>
    function ytVidId(url) {

        var p = /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;

        return (url.match(p)) ? RegExp.$1 : false;

    }



</script>


<style>
    .error{
        color: red !important;
    }
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection