@extends('admin.layouts.app')
@section('title', 'Netbhe| Admin |Details')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->



            <div class="col-sm-12">
                <h4 class="pull-left page-title">Tool content template details</h4>
                <div class="submit-login no_mmg pull-right">
                    <a href="{{ route('tool-content-template.edit',[$details->id]) }}" title="Edit">
                        <button type="button" class="btn btn-default">Edit</button>
                    </a>
                    <a href="{{ route('tool-content-template.index') }}" title="Back">
                        <button type="button" class="btn btn-default">Back</button>
                    </a>
                </div>              
            </div>




          
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        
                        <div class="panel-body">
                            <div class="row">
                                
                                <div class="col-lg-12 col-md-12">
                                    <h4 class="pull-left page-title">Details</h4>
                                    <div class="order-detail">
                                        <!--order-detail start-->
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="order-id">
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Subject :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <span>{{ $details->cont_subject }}</span>
                                                </div>
                                                <div class="clearfix"></div>

                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Category :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <span>{{ $details->cont_category }}</span>
                                                </div>
                                                <div class="clearfix"></div>

                                                @if($details->cont_category == "video")
                                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                        <span>Video:</span>
                                                    </div>    
                                                    {{-- {{ date('Y-m-d', strtotime($request->reminder_date)) }}; --}}
                                                    <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                        <div class="order-id">
                                                            <div class="col-lg-12">
                                                              <span>
                                                                <iframe width="350" height="220" src="https://www.youtube.com/embed/{{ $details->cont_cat_video_img_audio }}"></iframe>
                                                              </span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                    
                                                @elseif($details->cont_category == "audio")
                                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                        <span>Audio:</span>
                                                    </div>    
                                                        {{ $details->cont_cat_video_img_audio }}
                                                    
                                                @elseif($details->cont_category == "image")
                                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                        <span>Image:</span>
                                                    </div> 
                                                    <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">   
                                                        <img src="{{ url('storage/app/public/uploads/user_template_content/'.$details->cont_cat_video_img_audio) }}" alt="" style="width: 200px;">
                                                    </div>
                                                @endif 

                                            </div>
                                        </div>
                                        

                                    </div>
                                    <!--order-detail end-->
                                </div>
                                <div class="col-md-12">
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            
                            
                            
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Row -->
    </div>
    <!-- container -->
</div>
<!-- content -->
</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
<!-- Right Sidebar -->
<!-- /Right-bar -->
</div>
<script>
    function submitMy(id){
      // alert(id);
      $('#profid').val(id);
      $('#myForm2').submit();
    }
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection