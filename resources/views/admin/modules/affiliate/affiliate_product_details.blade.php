@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Admin | Bookings')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- Begin page -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="pull-left page-title">Affiliated Product List</h4>

                        <!--<ol class="breadcrumb pull-right">
                            <li><a href="#">User Dashboard</a></li>
                            <li class="active">My Order</li>
                            </ol>-->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <!--<div class="panel-heading">
                                <h3 class="panel-title">Default Example</h3>
                                </div>-->
                            <div class="panel-body table-rep-plugin">
                                <div class="row top_from">
                                    <!-- <form method="get" action="{{ route('admin.manage.reviews')}}">
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">Professional Name</label>
                                                <select name="professional" id="professional" class="form-control newdrop">
                                                    <option value="">Choose Professional</option>
                                                    
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">Customer Name</label>
                                                <input type="text" class="form-control" name="user_name" id="user_name" value="{{@$key['user_name']}}" placeholder="User name">
                                            </div>
                                        </div>


                                        


                                        {{-- <div class="clearfix"></div> --}}
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="submit-login add_btnm {{-- pull-right --}}" style="width:auto; margin-top:35px;">
                                                <input value="Search" id="search" class="btn btn-default" type="submit">
                                            </div>
                                        </div>
                                    </div>
                                    </form> -->
                                    <div class="clearfix"></div>
                                    
                                    <div class="clearfix"></div>




                                    <!-- Product Details -->
                                    <div class="col-lg-12 col-md-12">
                                        <h4 class="pull-left page-title">Affiliation Info</h4>
                                        <div class="order-detail">
                                            <!--order-detail start-->
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div class="order-id">

                                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                        <p>Name of Affiliate :</p>
                                                    </div>
                                                    <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                        <span>{{ @$user->nick_name ?? @$user->name }}</span>
                                                    </div>
                                                    <div class="clearfix"></div>

                                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                        <p>Affiliate Email :</p>
                                                    </div>
                                                    <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                        <span>{{ @$user->email }}</span>
                                                    </div>
                                                    <div class="clearfix"></div>

                                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                        <p>Product Title :</p>
                                                    </div>
                                                    <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                        <span>{{ $product->title }}</span>
                                                    </div>
                                                    <div class="clearfix"></div>

                                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                        <p>Professional Name :</p>
                                                    </div>
                                                    <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                        <strong>{{ @$product->professional->nick_name ? @$product->professional->nick_name :@$product->professional->name }}</strong>
                                                    </div>
                                                    <div class="clearfix"></div>

                                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                        <p>Professional Email :</p>
                                                    </div>
                                                    <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                        <strong>{{ @$product->professional->email }}</strong>
                                                    </div>
                                                    <div class="clearfix"></div>

                                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                        <p>Category :</p>
                                                    </div>
                                                    <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                        <strong>{{ $product->category->category_name }}</strong>
                                                    </div>
                                                    <div class="clearfix"></div>

                                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                        <p>Subcategory :</p>
                                                    </div>
                                                    <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                        <strong>{{ @$product->subCategory->name }}</strong>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                
                                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                        <p>Price :</p>
                                                    </div>
                                                    <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                        <strong>{{ $product->price}}</strong>
                                                    </div>

                                                    <div class="clearfix"></div>
                                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                        <p>Product Type :</p>
                                                    </div>
                                                    <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                        <strong>{{ $product->product_type=='N'?'Normal':'Customizable'}}</strong>
                                                    </div>
                                                    <div class="clearfix"></div>

                                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                        <p>Affiliation Commission :</p>
                                                    </div>
                                                    <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                        <span>{{ @$product->affiliate_percentage }}%</span>
                                                    </div>
                                                    <div class="clearfix"></div>

                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div class="order-id">
                                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                        <p>Cover Image :</p>
                                                    </div>
                                                    <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5">
                                                        <div style="height: 100px; width: 100px;">
                                                            <img src="{{ @$product->cover_image ? URL::to('storage/app/public/uploads/product_cover_images/'.@$product->cover_image) : URL::to('public/frontend/images/no_img.png') }}"
                                                                alt="" style="width: auto;height: 100%;">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="order-id">
                                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                        <p>Product Description :</p>
                                                    </div>
                                                </div>
                                                <div class="blog_area_nn01 col-12" style="text-align: left !important; box-shadow:none !important;">
                                                    {!! $product->description !!}
                                                </div>
                                            </div>
                                            

                                        </div>


                                        <!--order-detail end-->
                                    </div>


                                </div>
                            </div>
                        </div>

                        
                    </div>
                </div>
                <!-- End Row -->
            </div>
            <!-- container -->
        </div>
        <!-- content -->
        <!--<footer class="footer text-right">
            2015 © Moltran.
            </footer>-->
    </div>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
