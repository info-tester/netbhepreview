@extends('admin.layouts.app')
@section('title', 'NearO | Admin | Video Affiliate commision')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Video Affiliate commision</h4>
                    {{-- <div class="submit-login no_mmg pull-right">
                        <a href="{{ route('admin.refer.discount') }}" title="Back"><button type="button"
                                class="btn btn-default">Back</button></a>
                    </div> --}}
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-rep-plugin">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 nhp">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <form id="myform" method="post"
                                            action="{{ route('admin.manage.video.affiliate.commission.edit',['id'=>@$commission->id])}}"
                                            enctype="multipart/form-data">
                                            @csrf
                                            <div class="clearfix"></div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="affiliate_commission_user">Affiliate Commission - Student Books
                                                        (%)</label>
                                                    <input type="text" name="affiliate_commission_user"
                                                        class="form-control required" id="affiliate_commission_user"
                                                        value="{{old('affiliate_commission_user',@$commission->affiliate_commission_user)}}">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="affiliate_commission_professional">Affiliate Commission - Professional Hired
                                                        (%)</label>
                                                    <input type="text" name="affiliate_commission_professional"
                                                        class="form-control required" id="affiliate_commission_professional"
                                                        value="{{old('affiliate_commission_professional',@$commission->affiliate_commission_professional)}}">
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>

                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="add_btnm submit-login">
                                                    <input value="Update" type="submit" class="btn btn-default">
                                                </div>
                                            </div>
                                            <!--all_time_sho-->
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- container -->
    </div>
    <!-- content -->
</div>
<script>
    $(document).ready(function(){
		$("#myform").validate({
			rules: {
                // affiliate_commission_user : 'number',
                // affiliate_commission_professional : 'number',
                affiliate_commission_user : {
                    required:true,digits:true,affiliate_commission_user_can_not_be_greater:true},
                affiliate_commission_professional : {
                    required:true,digits:true,affiliate_commission_professional_can_not_be_greater:true},
			},
			messages : {
                affiliate_commission_user : {
					required 		: "Please enter commission percentage.",
					number 			: "Please enter number.",
                    digits          : "Please enter positive number."
				},
                affiliate_commission_professional : {
					required 		: "Please enter commission percentage.",
					number 			: "Please enter number.",
                     digits          : "Please enter positive number."
				},
	        },
	        // errorPlacement: function (error, element)
	        // {
	        //     toastr.error(error.text());
	        // }
		});
        jQuery.validator.addMethod("affiliate_commission_user_can_not_be_greater", function(value, element) {
        var comm = $('#affiliate_commission_user').val(); 
         if (comm  <= 100)
            return true;
            return false; 
        }, "Commission cannot be greater than 100");

        jQuery.validator.addMethod("affiliate_commission_professional_can_not_be_greater", function(value, element) {
        var comm = $('#affiliate_commission_professional').val(); 
         if (comm  <= 100)
            return true;
            return false; 
        }, "Commission cannot be greater than 100");
	});
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
