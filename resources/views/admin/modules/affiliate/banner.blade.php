@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Admin | Video Affiliaton Banner')

@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- Begin page -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="pull-left page-title">Video Affiliaton Banner</h4>
                        <div class="submit-login no_mmg pull-right">
                            {{-- <a href="{{route('admin.manage.video.affiliate')}}" title="Back"><button type="button" class="btn btn-default">Back</button></a> --}}
                        </div>

                        <!--<ol class="breadcrumb pull-right">
                            <li><a href="#">User Dashboard</a></li>
                            <li class="active">My Order</li>
                            </ol>-->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body table-rep-plugin">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12 nhp">
                                        <div class="table-responsive" data-pattern="priority-columns">
                                            <div class="col-md-12 dess5">
                                                <i class="fa fa-pencil-square-o cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo" >Edit Banner </span></i>
                                            </div>


                                            <div class="clearfix"></div>

                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="table-responsive" data-pattern="priority-columns">
                                                    <table id="datatable" class="table table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>Image</th>
                                                                <th>Banner </th>
                                                                <th>Size </th>
                                                                <th>Action </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <img src="{{ URL::to('storage/app/public/uploads/banner/banner1.png')}}" alt="" height="80" width="120">
                                                                    </td>
                                                                    <td>
                                                                        Banner - 1
                                                                    </td>
                                                                    <td>
                                                                        300×250
                                                                    </td>
                                                                    <td>
                                                                        <a href="{{route('admin.manage.video.affiliate.banner',['id'=>1])}}"> <i class="fa fa-pencil-square-o delet" aria-hidden="true" title="Edit"></i></a>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <img src="{{ URL::to('storage/app/public/uploads/banner/banner2.png')}}" alt="" height="80" width="120">
                                                                    </td>
                                                                    <td>
                                                                        Banner - 2
                                                                    </td>
                                                                    <td>
                                                                        728×90
                                                                    </td>
                                                                    <td>
                                                                        <a href="{{route('admin.manage.video.affiliate.banner',['id'=>2])}}"> <i class="fa fa-pencil-square-o delet" aria-hidden="true" title="Edit"></i></a>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <img src="{{ URL::to('storage/app/public/uploads/banner/banner3.png')}}" alt="" height="80" width="120">
                                                                    </td>
                                                                    <td>
                                                                        Banner - 3
                                                                    </td>
                                                                    <td>
                                                                        970×250
                                                                    </td>
                                                                    <td>
                                                                        <a href="{{route('admin.manage.video.affiliate.banner',['id'=>3])}}"> <i class="fa fa-pencil-square-o delet" aria-hidden="true" title="Edit"></i></a>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <img src="{{ URL::to('storage/app/public/uploads/banner/banner4.png')}}" alt="" height="80" width="120">
                                                                    </td>
                                                                    <td>
                                                                        Banner - 4
                                                                    </td>
                                                                    <td>
                                                                        300×600
                                                                    </td>
                                                                    <td>
                                                                        <a href="{{route('admin.manage.video.affiliate.banner',['id'=>4])}}"> <i class="fa fa-pencil-square-o delet" aria-hidden="true" title="Edit"></i></a>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <img src="{{ URL::to('storage/app/public/uploads/banner/banner5.png')}}" alt="" height="80" width="120">
                                                                    </td>
                                                                    <td>
                                                                        Banner - 5
                                                                    </td>
                                                                    <td>
                                                                        200×200
                                                                    </td>
                                                                    <td>
                                                                        <a href="{{route('admin.manage.video.affiliate.banner',['id'=>5])}}"> <i class="fa fa-pencil-square-o delet" aria-hidden="true" title="Edit"></i></a>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <img src="{{ URL::to('storage/app/public/uploads/banner/banner6.png')}}" alt="" height="80" width="120">
                                                                    </td>
                                                                    <td>
                                                                        Banner - 6
                                                                    </td>
                                                                    <td>
                                                                        970×90
                                                                    </td>
                                                                    <td>
                                                                        <a href="{{route('admin.manage.video.affiliate.banner',['id'=>6])}}"> <i class="fa fa-pencil-square-o delet" aria-hidden="true" title="Edit"></i></a>
                                                                    </td>
                                                                </tr>

                                                        </tbody>


                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Row -->
            </div>
            <!-- container -->
        </div>
        <!-- content -->
        <!--<footer class="footer text-right">
            2015 © Moltran.
            </footer>-->
    </div>
</div>

<script>
    var resizefunc = [];
</script>

<script type="text/javascript">
    $(document).ready(function() {
    $('#datatable_length').hide();
        $('#datatable').dataTable({
    "ordering": false,
    "info":     false,
    "searching": false,
    "dom": '<"toolbar">frtip'
    });
        $('#datatable1').dataTable({
    "ordering": false,
    "info":     false,
    "searching": false,
    "dom": '<"toolbar">frtip'
    });
    $(".check_check").click(function(){
    if($(this).is(":checked"))
    {
    $('.ch_1').show();
    }
    else
    {
    $('.ch_1').hide();
    }
    });
    //$("div.toolbar").html('<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 status">Status: <select name="" class="newdrop"><option value="1">Active</option><option value="1">Inactive</option></select></div><div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 clndr">From date : <input type="text" name="from" id="frm_date"> </div><div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 clndr"> To date : <input type="text" name="to" id="to_date"> </div><div class="col-md-4 col-lg-4 clndr">Keyword: <input type="text" name="abc"></div><div class="col-md-4 col-lg-4 col-sm-4 colxs-12 srch"><input type="submit" name="submit" value="Search"> </div>');
       // $('#datatable-keytable').DataTable( { keys: true } );
       //$('#datatable-responsive').DataTable();
        //$('#datatable-scroller').DataTable( { ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
        //var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } );
    } );
    // TableManageButtons.init();
    $(document).ready(function() {
        $('#create-link').click(function() {
            $('#link').val('<a target="_blank" href="' + $('#link-url').val() + '">' + $('#link-text').val() + '</a>');
            $('#link-url').val('');
            $('#link-text').val('');
        });
        const regex = new RegExp("(.*?)\.(png|jpg|jpeg)$");
        $('.image_input').on('change', function() {
            const val = $(this).val().toLowerCase();
            if (!(regex.test(val))) {
                $(this).val('');
                alert('Only png and jpeg files are allowed.');
            }
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                const t = $(this);
                reader.onload = function(e) {
                    t.next('img').attr('src', e.target.result).show();
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
		$("#myform").validate({});
        jQuery.validator.addMethod("dimention1", function (value, element, param) {
            var width = $(element).data('imageWidth');
            var height = $(element).data('imageHeight');
            var a=param[0];
            if(width <= param[0] && height <= param[1]){
                return true;
            }else if(width==undefined){
                return true;
            }else{
                return false;
            }
        }, 'Image size 300 x 200');
        jQuery.validator.addMethod("dimention2", function (value, element, param) {
            var width = $(element).data('imageWidth');
            var height = $(element).data('imageHeight');
            var a=param[0];
            if(width <= param[0] && height <= param[1]){
                return true;
            }else if(width==undefined){
                return true;
            }else{
                return false;
            }
        }, 'Image size 600 x 480');
        jQuery.validator.addMethod("dimention3", function (value, element, param) {
            var width = $(element).data('imageWidth');
            var height = $(element).data('imageHeight');
            var a=param[0];
            if(width <= param[0] && height <= param[1]){
                return true;
            }else if(width==undefined){
                return true;
            }else{
                return false;
            }
        }, 'Image size 900 x 720');
        jQuery.validator.addClassRules("nameExistErrClass1", {
            dimention1:[300, 200]
        });
        jQuery.validator.addClassRules("nameExistErrClass2", {
            dimention2:[600, 480]
        });
        jQuery.validator.addClassRules("nameExistErrClass3", {
            dimention3:[900, 720]
        });


            $('.image_input').change(function() {
                var nameId = $(this).data('id');
                console.log(nameId);
                $('#'+nameId ).removeData('imageWidth');
                $('#'+nameId).removeData('imageHeight');
                var file = this.files[0];
                var tmpImg = new Image();
                tmpImg.src=window.URL.createObjectURL( file );
                tmpImg.onload = function() {
                    width = tmpImg.naturalWidth,
                    height = tmpImg.naturalHeight;
                    $('#'+nameId).data('imageWidth', width);
                    $('#'+nameId).data('imageHeight', height);
                }
            });
	});
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
