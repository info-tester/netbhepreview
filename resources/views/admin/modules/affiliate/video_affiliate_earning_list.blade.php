@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Admin | Video Affiliaton Earning')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- Begin page -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="pull-left page-title">Video Affiliaton Earning</h4>
                        <div class="submit-login no_mmg pull-right">
                            <a href="{{route('admin.manage.video.affiliate')}}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
                        </div>

                        <!--<ol class="breadcrumb pull-right">
                            <li><a href="#">User Dashboard</a></li>
                            <li class="active">My Order</li>
                            </ol>-->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="right_dash ml-auto">
                                    <div class="information_box" style="    border: 1px solid #8f8f8f; padding: 5px 12px;">
                                    <h4><span>  <strong>User Information </strong></span></h4>
                                    <div class="information_area">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                    <label class="type_label1 ">Name: </label>
                                                    <label class="type_label2">{{@$users->name}}</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                    <label class="type_label1 ">Email: </label>
                                                    <label class="type_label2">{{@$users->email}}</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                    <label class="type_label1 ">Country: </label>
                                                    <label class="type_label2">{{@$users->countryName->name}}</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                    <label class="type_label1 ">Total Earning: </label>
                                                    <label class="type_label2">{{@$users->video_aff_earning}}</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                    <label class="type_label1 ">Total Paid: </label>
                                                    <label class="type_label2">{{@$users->video_aff_paid}}</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                    <label class="type_label1 ">Due amunt: </label>
                                                    <label class="type_label2">{{@$users->video_aff_earning-@$users->video_aff_paid}}</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                    <label class="type_label1 ">Total Sign Up: </label>
                                                    <label class="type_label2">{{@$users->affiliated_user}}</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                    <label class="type_label1 ">Total Link Clicks: </label>
                                                    <label class="type_label2">{{@$users->aff_link_click}}</label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12 dess5">
                                                Earning Details:
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="table-responsive" data-pattern="priority-columns">
                                                    <table id="datatable" class="table table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>Order no</th>
                                                                <th>Username</th>
                                                                <th>Type</th>
                                                                <th>Booking Total</th>
                                                                <th>Amount</th>
                                                                <th>Earning Date</th>


                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @if(sizeof(@$earnings)>0)
                                                            @foreach(@$earnings as $k=>$row)
                                                                <tr>
                                                                    <td>
                                                                        {{@$row->bookingDetails->token_no}}
                                                                    </td>
                                                                    <td>
                                                                        {{ @$row->userDetails->nick_name ?? @$row->userDetails->name }}
                                                                    </td>
                                                                    <td>
                                                                        @if(@$row->type=='P')
                                                                        Professional
                                                                        @elseif(@$row->type=='S')
                                                                        Student
                                                                        @else
                                                                        --
                                                                        @endif
                                                                    </td>
                                                                    <td>
                                                                        {{ @$row->bookingDetails->sub_total }}
                                                                    </td>
                                                                    <td>
                                                                        {{ @$row->amount }}
                                                                    </td>
                                                                    <td>
                                                                        {{date('d-m-Y',strtotime($row->created_at)) }}
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            @endif

                                                        </tbody>


                                                    </table>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12 dess5">
                                                Payment Details:
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="table-responsive" data-pattern="priority-columns">
                                                    <table id="datatable1" class="table table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Date</th>
                                                                <th>Name</th>
                                                                <th>Amount</th>
                                                                <th>Note</th>


                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @if(sizeof(@$payment)>0)
                                                            @foreach(@$payment as $k=>$row)
                                                                <tr>
                                                                    <td>
                                                                        {{(@$k+1)}}
                                                                    </td>
                                                                    <td>
                                                                        {{date('d-m-Y',strtotime(@$row->created_at))}}
                                                                    </td>
                                                                    <td>
                                                                        {{ @$row->userDetails->nick_name ?? @$row->userDetails->name }}
                                                                    </td>
                                                                    <td>
                                                                        {{ @$row->amount }}
                                                                    </td>
                                                                    <td>
                                                                        {{ @$row->note }}
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            @endif

                                                        </tbody>


                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="panel-body table-rep-plugin">
                                <div class="row top_from">

                                    <div class="clearfix"></div>
                                    <div class="col-md-12 dess5">
                                        Earning Details:
                                    </div>

                                    <div class="clearfix"></div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" data-pattern="priority-columns">
                                            <table id="datatable" class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Order no</th>
                                                        <th>User name</th>
                                                        <th>Amount</th>


                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if(sizeof(@$earnings)>0)
                                                    @foreach(@$earnings as $k=>$row)
                                                        <tr>
                                                            <td>
                                                                {{(@$k+1)}}
                                                            </td>
                                                            <td>
                                                                {{@$row->bookingDetails->token_no}}
                                                            </td>
                                                            <td>
                                                                {{ @$row->userDetails->nick_name ?? @$row->userDetails->name }}
                                                            </td>
                                                            <td>
                                                                {{ @$row->amount }}
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    @endif

                                                </tbody>


                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" data-pattern="priority-columns">
                                            <table id="datatable1" class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Order no</th>
                                                        <th>User name</th>
                                                        <th>Amount</th>


                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if(sizeof(@$earnings1)>0)
                                                    @foreach(@$earnings1 as $k=>$row)
                                                        <tr>
                                                            <td>
                                                                {{(@$k+1)}}
                                                            </td>
                                                            <td>
                                                                {{@$row->bookingDetails->token_no}}
                                                            </td>
                                                            <td>
                                                                {{ @$row->userDetails->nick_name ?? @$row->userDetails->name }}
                                                            </td>
                                                            <td>
                                                                {{ @$row->amount }}
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    @endif

                                                </tbody>


                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
                <!-- End Row -->
            </div>
            <!-- container -->
        </div>
        <!-- content -->
        <!--<footer class="footer text-right">
            2015 © Moltran.
            </footer>-->
    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->
    <!-- Right Sidebar -->
    {{-- <div class="side-bar right-bar nicescroll">
        <h4 class="text-center">Chat</h4>
        <div class="contact-list nicescroll">
            <ul class="list-group contacts-list">
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-1.jpg" alt="">
                        </div>
                        <span class="name">Chadengle</span>
                        <i class="fa fa-circle online"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-2.jpg" alt="">
                        </div>
                        <span class="name">Tomaslau</span>
                        <i class="fa fa-circle online"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-3.jpg" alt="">
                        </div>
                        <span class="name">Stillnotdavid</span>
                        <i class="fa fa-circle online"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-4.jpg" alt="">
                        </div>
                        <span class="name">Kurafire</span>
                        <i class="fa fa-circle online"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-5.jpg" alt="">
                        </div>
                        <span class="name">Shahedk</span>
                        <i class="fa fa-circle away"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-6.jpg" alt="">
                        </div>
                        <span class="name">Adhamdannaway</span>
                        <i class="fa fa-circle away"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-7.jpg" alt="">
                        </div>
                        <span class="name">Ok</span>
                        <i class="fa fa-circle away"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-8.jpg" alt="">
                        </div>
                        <span class="name">Arashasghari</span>
                        <i class="fa fa-circle offline"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-9.jpg" alt="">
                        </div>
                        <span class="name">Joshaustin</span>
                        <i class="fa fa-circle offline"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-10.jpg" alt="">
                        </div>
                        <span class="name">Sortino</span>
                        <i class="fa fa-circle offline"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
            </ul>
        </div>
    </div> --}}
    <!-- /Right-bar -->
</div>
<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Transfer the amount to the following bank account and then enter a note for the professional</h4>
                {{-- Transfer the amount to the following bank account and then enter a note for the professional --}}
            </div>
            <div class="modal-body" style="padding: 1rem;">
                <p id="professional_name"></p>
                <p id="payment_amount"></p>
                <p id="bankName"></p>
                <p id="accountNumber"></p>
                <p id="agencyNumber"></p>
                <p id="agencyCheckNumber"></p>
                <p id="bank_number"></p>
                <form action="{{route('admin.make.transfer.bank')}}" method="post" id="updateeventsform2" class="form-horizontal">
                    <input type="hidden" name="payment_token_no" id="payment_token_no" value="">
                    <input type="hidden" name="paymentId" id="paymentId" value="">
                    @csrf
                        <label for='areaforinfo'>Note </label>

                            <textarea class="form-control required" id='areaforinfo' rows="4" style="min-width: 100%; border: 1px solid #CCC;"  name="note"></textarea>
                    <div class="submit-login add_btnm " style="width:auto;margin-top:10px;">
                    <button type="submit" class="btn btn-default">Payment Confirmation </button>
                    </div>
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="myModal1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Transfer the amount to the following paypal address and then enter a note for the professional</h4>
                {{-- Transfer the amount to the following bank account and then enter a note for the professional --}}
            </div>
            <div class="modal-body" style="padding: 1rem;">
                <p id="professional_name1"></p>
                <p id="payment_amount1"></p>
                <p id="paypal"></p>
                <form action="{{route('admin.make.transfer.paypal')}}" method="post" id="updateeventsform3"
                    class="form-horizontal">
                    <input type="hidden" name="payment_token_no" id="payment_token_no1" value="">
                    <input type="hidden" name="paymentId" id="paymentId1" value="">
                    @csrf
                    <label for='areaforinfo'>Note </label>

                    <textarea class="form-control required" id='areaforinfo' rows="4"
                        style="min-width: 100%; border: 1px solid #CCC;" name="note"></textarea>
                    <div class="submit-login add_btnm " style="width:auto;margin-top:10px;">
                        <button type="submit" class="btn btn-default">Payment Confirmation </button>
                    </div>
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
<!-- END wrapper -->
<!--Raise  popup-->
{{-- <div class="container">
    <div class="custom_popup pop1" style="display:none;">
        <button type="button" class="close lg_csl" data-dismiss="modal" aria-hidden="true">
        <img src="image1/close_btn.png" alt=""></button>
        <div class="pop_from_area">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="your-mail">
                    <label for="exampleInputEmail1">Vendor</label>
                    <select class="form-control newdrop">
                        <option>Choose</option>
                        <option value="volvo">Vendor 1</option>
                        <option value="saab">Vendor 2</option>
                        <option value="volvo">Vendor 3</option>
                        <option value="saab">Vendor 4</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="your-mail">
                    <label for="exampleInputEmail1">Issue Category</label>
                    <select class="form-control newdrop">
                        <option value="volvo">Choose</option>
                        <option value="saab">Category 1</option>
                        <option value="mercedes">Category 2</option>
                        <option value="saab">Category 3</option>
                        <option value="mercedes">Category 4</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="your-mail">
                    <label for="exampleInputEmail1">Description</label>
                    <textarea class="form-control des" rows="3"></textarea>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="submit-login no_mmg">
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div> --}}
<!--Raise  popup-->
<script>
    var resizefunc = [];
</script>

<script type="text/javascript">
    $(document).ready(function() {
    $('#datatable_length').hide();
        $('#datatable').dataTable({
    "ordering": false,
    "info":     false,
    "searching": false,
    "dom": '<"toolbar">frtip'
    });
        $('#datatable1').dataTable({
    "ordering": false,
    "info":     false,
    "searching": false,
    "dom": '<"toolbar">frtip'
    });
    $(".check_check").click(function(){
    if($(this).is(":checked"))
    {
    $('.ch_1').show();
    }
    else
    {
    $('.ch_1').hide();
    }
    });
    //$("div.toolbar").html('<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 status">Status: <select name="" class="newdrop"><option value="1">Active</option><option value="1">Inactive</option></select></div><div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 clndr">From date : <input type="text" name="from" id="frm_date"> </div><div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 clndr"> To date : <input type="text" name="to" id="to_date"> </div><div class="col-md-4 col-lg-4 clndr">Keyword: <input type="text" name="abc"></div><div class="col-md-4 col-lg-4 col-sm-4 colxs-12 srch"><input type="submit" name="submit" value="Search"> </div>');
       // $('#datatable-keytable').DataTable( { keys: true } );
       //$('#datatable-responsive').DataTable();
        //$('#datatable-scroller').DataTable( { ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
        //var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } );
    } );
    // TableManageButtons.init();
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
