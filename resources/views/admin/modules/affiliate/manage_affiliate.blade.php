@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Admin | Manage Affiliation')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- Begin page -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="pull-left page-title">Manage Affiliaton</h4>

                        <!--<ol class="breadcrumb pull-right">
                            <li><a href="#">User Dashboard</a></li>
                            <li class="active">My Order</li>
                            </ol>-->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <!--<div class="panel-heading">
                                <h3 class="panel-title">Default Example</h3>
                                </div>-->
                            <div class="panel-body table-rep-plugin">
                                <div class="row top_from">
                                    <form id="myForm" method="get" action="{{ route('admin.manage.affiliate')}}">
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">Professional Name</label>
                                                <select name="professional" id="professional" class="form-control newdrop">
                                                    <option value="">Choose Professional</option>
                                                    @if(count(@$user_list)>0)
                                                        @foreach($user_list as $k=>$row)
                                                            <option value="{{@$row->id}}" @if(@$key['professional'] == @$row->id) selected @endif>{{ @$row->nick_name ?? @$row->name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">Total Earning Less Than</label>
                                                <input type="text" class="form-control" name="tot_earning" id="tot_earning" value="{{@$key['tot_earning']}}" placeholder="Total Earning">
                                            </div>
                                        </div>

                                        {{-- <div class="clearfix"></div> --}}
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="submit-login add_btnm {{-- pull-right --}}" style="width:auto; margin-top:35px;">
                                                <input value="Search" id="search" class="btn btn-default" type="submit">
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div class="clearfix"></div>

                                <div class="row">
                                    <div class="col-md-12 dess5">
                                        <i class="fa fa-folder cncl" aria-hidden="true"> <span class="cncl_oopo">Show Details</span></i>
                                        <i class="fa fa-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Block</span></i>
                                        <i class="fa fa-square cncl" aria-hidden="true"> <span class="cncl_oopo">Unblock</span></i>
                                        <i class="fa fa-usd cncl" aria-hidden="true"> <span class="cncl_oopo">Show Affiliate Earnings</span></i>
                                    </div>
                                    
                                    <div class="clearfix"></div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" data-pattern="priority-columns">
                                            <table id="datatable" class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        
                                                        <!-- <th>Wirecard Order ID</th> -->
                                                        <th>Affiliate Name</th>
                                                        <th>Affiliate Type</th>
                                                        <th>Affiliate Email</th>
                                                        <th>No. of products</th>
                                                        <th>Total Earning</th>
                                                        <th>Earning Due</th>
                                                        <th>Earning Paid</th>
                                                        <th>Action</th>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if(count(@$users)>0)
                                                        @foreach($users as $k=>$row)
                                                        <tr>
                                                            
                                                            
                                                            <td>
                                                                {{ @$row->nick_name ?? @$row->name }}
                                                            </td>
                                                            <td>
                                                                {{ (@$row->is_professional == 'Y') ? 'Professional' : 'User' }}
                                                            </td>
                                                            <td>
                                                                {{ @$row->email }}
                                                            </td>
                                                            <td>
                                                                {{ @$row->affiliated_products }}
                                                            </td>
                                                            <td>
                                                                {{ @$row->total_earning }}
                                                            </td>
                                                            <td>
                                                                {{ @$row->due_earning }}
                                                            </td>
                                                            <td>
                                                                {{ @$row->paid_earning }}
                                                            </td>
                                                            <td>
                                                               <a href="{{route('admin.manage.affiliated.product',@$row->id)}}"> <i class="fa fa-folder delet" aria-hidden="true"></i></a>
                                                               @if(@$row->is_join_affiliate == 'Y')
                                                               <a href="{{route('admin.affiliate.blockuser',@$row->id)}}" title="Block"  onclick="return confirm('Do you want block {{@$row->nick_name ? @$row->nick_name :@$row->name}} from affiliation program?');"><i class="fa fa-square-o" aria-hidden="true" style="border: none;"></i>
                                                               </a> 
                                                               @else
                                                               <a href="{{route('admin.affiliate.blockuser',@$row->id)}}" title="Unblock"  onclick="return confirm('Do you want unblock {{@$row->nick_name ? @$row->nick_name :@$row->name}} from affiliation program?');"><i class="fa fa-square" aria-hidden="true"></i>
                                                               </a>
                                                               @endif
                                                               <a href="{{route('admin.affiliate.earnings',@$row->id)}}" title="Show Affiliate Earnings"> <i class="fa fa-usd" aria-hidden="true"></i></a>

                                                               

                                                            </td>
                                                            

                                                            
                                                            
                                                        </tr>
                                                        @endforeach
                                                    @endif
                                                    
                                                </tbody>
                                                
                                                 
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Row -->
            </div>
            <!-- container -->
        </div>
        <!-- content -->
        <!--<footer class="footer text-right">
            2015 © Moltran.
            </footer>-->
    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->
    <!-- Right Sidebar -->
    <!-- /Right-bar -->
</div>
<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Transfer the amount to the following bank account and then enter a note for the professional</h4>
                {{-- Transfer the amount to the following bank account and then enter a note for the professional --}}
            </div>
            <div class="modal-body" style="padding: 1rem;">
                <p id="professional_name"></p>
                <p id="payment_amount"></p>
                <p id="bankName"></p>
                <p id="accountNumber"></p>
                <p id="agencyNumber"></p>
                <p id="agencyCheckNumber"></p>
                <p id="bank_number"></p>
                <form action="{{route('admin.make.transfer.bank')}}" method="post" id="updateeventsform2" class="form-horizontal">
                    <input type="hidden" name="payment_token_no" id="payment_token_no" value="">
                    <input type="hidden" name="paymentId" id="paymentId" value="">
                    @csrf
                        <label for='areaforinfo'>Note </label>

                            <textarea class="form-control required" id='areaforinfo' rows="4" style="min-width: 100%; border: 1px solid #CCC;"  name="note"></textarea>
                    <div class="submit-login add_btnm " style="width:auto;margin-top:10px;">
                    <button type="submit" class="btn btn-default">Payment Confirmation </button>
                    </div>
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="myModal1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Transfer the amount to the following paypal address and then enter a note for the professional</h4>
                {{-- Transfer the amount to the following bank account and then enter a note for the professional --}}
            </div>
            <div class="modal-body" style="padding: 1rem;">
                <p id="professional_name1"></p>
                <p id="payment_amount1"></p>
                <p id="paypal"></p>
                <form action="{{route('admin.make.transfer.paypal')}}" method="post" id="updateeventsform3"
                    class="form-horizontal">
                    <input type="hidden" name="payment_token_no" id="payment_token_no1" value="">
                    <input type="hidden" name="paymentId" id="paymentId1" value="">
                    @csrf
                    <label for='areaforinfo'>Note </label>

                    <textarea class="form-control required" id='areaforinfo' rows="4"
                        style="min-width: 100%; border: 1px solid #CCC;" name="note"></textarea>
                    <div class="submit-login add_btnm " style="width:auto;margin-top:10px;">
                        <button type="submit" class="btn btn-default">Payment Confirmation </button>
                    </div>
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
<!-- END wrapper -->
<!--Raise  popup-->
{{-- <div class="container">
    <div class="custom_popup pop1" style="display:none;">
        <button type="button" class="close lg_csl" data-dismiss="modal" aria-hidden="true">
        <img src="image1/close_btn.png" alt=""></button>
        <div class="pop_from_area">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="your-mail">
                    <label for="exampleInputEmail1">Vendor</label>
                    <select class="form-control newdrop">
                        <option>Choose</option>
                        <option value="volvo">Vendor 1</option>
                        <option value="saab">Vendor 2</option>
                        <option value="volvo">Vendor 3</option>
                        <option value="saab">Vendor 4</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="your-mail">
                    <label for="exampleInputEmail1">Issue Category</label>
                    <select class="form-control newdrop">
                        <option value="volvo">Choose</option>
                        <option value="saab">Category 1</option>
                        <option value="mercedes">Category 2</option>
                        <option value="saab">Category 3</option>
                        <option value="mercedes">Category 4</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="your-mail">
                    <label for="exampleInputEmail1">Description</label>
                    <textarea class="form-control des" rows="3"></textarea>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="submit-login no_mmg">
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div> --}}
<!--Raise  popup-->
<script>
    var resizefunc = [];
</script>

<script type="text/javascript">
    $(document).ready(function() {
    $('#datatable_length').hide();
        $('#datatable').dataTable({
    "ordering": false,
    "info":     false,
    "searching": false,
    "dom": '<"toolbar">frtip'
    });
    $(".check_check").click(function(){
    if($(this).is(":checked"))
    {
    $('.ch_1').show();
    }
    else
    {
    $('.ch_1').hide();
    }
    });
    //$("div.toolbar").html('<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 status">Status: <select name="" class="newdrop"><option value="1">Active</option><option value="1">Inactive</option></select></div><div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 clndr">From date : <input type="text" name="from" id="frm_date"> </div><div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 clndr"> To date : <input type="text" name="to" id="to_date"> </div><div class="col-md-4 col-lg-4 clndr">Keyword: <input type="text" name="abc"></div><div class="col-md-4 col-lg-4 col-sm-4 colxs-12 srch"><input type="submit" name="submit" value="Search"> </div>');
       // $('#datatable-keytable').DataTable( { keys: true } );
       //$('#datatable-responsive').DataTable();
        //$('#datatable-scroller').DataTable( { ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
        //var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } );
    } );
    // TableManageButtons.init();
</script>
<script>
    $(function() {
        $("#datepicker").datepicker({dateFormat: "dd-mm-yy",
           defaultDate: new Date(),
           onClose: function( selectedDate ) {
           $( "#datepicker1").datepicker( "option", "minDate", selectedDate );
          }
        });

        $("#datepicker1").datepicker({dateFormat: "dd-mm-yy",
         defaultDate: new Date(),
              onClose: function( selectedDate ) {
              $( "#datepicker" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
<script>
    function submitForm(amount, name, token,id){
        if(confirm('The amount '+amount+' will be transferred on '+name+'\'s account')){
            $('#id').val(id);
            $('#no').val(token);
            $('#myForm').submit();
        }
        else{
            return false;
        }
    }
    function prof_account_info(amount, name, token, prof_id, bankName ,accountNumber ,agencyNumber,agencyCheckNumber,bank_number,id){
        console.log(token)
            // $('#amount').val(amount);
            $('#payment_token_no').val(token);
            $('#paymentId').val(id);
            $('#professional_name').html("Professional Name : "+name);
            $('#payment_amount').html("Payment Amount : "+amount);
            $('#bankName').html("Bank Name : "+bankName);
            $('#accountNumber').html("Account Number : "+accountNumber);
            $('#agencyNumber').html("Agency Number : "+agencyNumber);
            $('#agencyCheckNumber').html("Agency Check Number : "+agencyCheckNumber);
            $('#bank_number').html("Bank Number : "+bank_number);
            $('#myModal').modal('show');
    }
    function prof_account_info1(amount, name, token, paypal,id){
            console.log(token)
                // $('#amount').val(amount);
                $('#payment_token_no1').val(token);
                $('#paymentId1').val(id);
                $('#professional_name1').html("Professional Name : "+name);
                $('#payment_amount1').html("Payment Amount : "+amount);
                $('#paypal').html("Paypal Address : "+paypal);
                $('#myModal1').modal('show');
        }
</script>
<script>
    $(document).ready(function(){
		$("#updateeventsform2").validate({});
        $("#updateeventsform3").validate({});
        $("#myForm").validate({
            rules : {
                tot_earning:{
                    digits: true,
                }
            }
        });

	});
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
