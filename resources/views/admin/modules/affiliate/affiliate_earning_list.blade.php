@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Admin | Affiliation Earnings')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- Begin page -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="pull-left page-title">Affiliate Earnings List</h4>
                        <div class="submit-login no_mmg pull-right">
                            <a href="{{route('admin.manage.affiliated.product', @$user->id)}}" title="Show Affiliate Products List"><button type="button" class="btn btn-default">Show Affiliate Products List</button></a>
                            <a href="{{route('admin.manage.affiliate')}}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
                        </div>
                        <!--<ol class="breadcrumb pull-right">
                            <li><a href="#">User Dashboard</a></li>
                            <li class="active">My Order</li>
                            </ol>-->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <!--<div class="panel-heading">
                                <h3 class="panel-title">Default Example</h3>
                                </div>-->
                            <div class="panel-body table-rep-plugin">
                                <div class="row top_from">
                                    <!-- <form method="get" action="{{ route('admin.manage.reviews')}}">
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">Professional Name</label>
                                                <select name="professional" id="professional" class="form-control newdrop">
                                                    <option value="">Choose Professional</option>

                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="your-mail">
                                                <label for="exampleInputEmail1">Customer Name</label>
                                                <input type="text" class="form-control" name="user_name" id="user_name" value="{{@$key['user_name']}}" placeholder="User name">
                                            </div>
                                        </div>





                                        {{-- <div class="clearfix"></div> --}}
                                        <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                            <div class="submit-login add_btnm {{-- pull-right --}}" style="width:auto; margin-top:35px;">
                                                <input value="Search" id="search" class="btn btn-default" type="submit">
                                            </div>
                                        </div>
                                    </div>
                                    </form> -->
                                    <div class="clearfix"></div>



                                    <div class="col-md-12">
                                        <h4 class="pull-left page-title">User Basic Info</h4>
                                        <div class="order-detail">
                                            <!--order-detail start-->
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="order-id">
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                <p>Name :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                <span>{{ @$user->name ? ( @$user->nick_name ? @$user->nick_name : @$user->name ) : 'N.A' }}</span>
                                                </div>

                                                <div class="clearfix"></div>
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                <p>Mobile :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                <strong>{{ @$user->mobile ? $user->mobile : 'N.A' }}</strong>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                <p>Email ID :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                <strong>{{ @$user->email ? $user->email : 'N.A' }}</strong>
                                                </div>
                                                {{-- <div class="clearfix"></div>
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                <p>Address :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                <strong>{{ @$user->address ? $user->address : 'N.A' }}</strong>
                                                </div> --}}

                                                <div class="clearfix"></div>
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                <p>Gender :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                <strong>
                                                    @if(@$user->gender == 'M')
                                                    {{ 'Male' }}
                                                    @elseif(@$user->gender == 'F')
                                                    {{ 'Female' }}
                                                    @endif
                                                </strong>
                                                </div>
                                                <div class="clearfix"></div>
                                                {{-- <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                <p>Experience :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                <strong>{{ @$user->experience ? $user->experience.' years' : 'N.A' }}</strong>
                                                </div> --}}

                                                {{-- <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                <span>{{ @$user->highest_degree ? $user->highest_degree : 'N.A' }}</span>
                                                </div>
                                                <div class="clearfix"></div> --}}
                                                {{-- @if(@$user->is_professional=="Y")
                                                <div class="clearfix"></div>
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Category :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong>
                                                    @php
                                                    @$printedCat = [];
                                                    $i=0;
                                                    @endphp
                                                    @if(sizeof($user->userDetails)>0)
                                                    @foreach($user->userDetails as $rw)

                                                    @if(@$rw->parentCategoryName!=null && !in_array(@$rw->parentCategoryName->id, @$printedCat))
                                                        @php
                                                                        @$printedCat[$i] = @$rw->parentCategoryName->id;

                                                                        @endphp
                                                                    {{ @$rw->parentCategoryName->name.'->'.@$rw->categoryName->name }}

                                                                    @else
                                                                    <br>{{  @$rw->categoryName->parent_id>0 ? '->': '•'}}{{ @$rw->categoryName->name }}
                                                                    @php
                                                                        @$printedCat[$i] = @$rw->categoryName->id;

                                                                        @endphp
                                                                    @endif

                                                                    @php
                                                                    $i++;
                                                                    @endphp
                                                                    @endforeach
                                                                @else
                                                                <center>__</center>

                                                                @endif
                                                    </strong>
                                                </div>
                                                @endif --}}
                                                @if(@$user->country_id !== 0)
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Country :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong> {{ $user->countryName->name }} <strong>
                                                </div>
                                                @endif
                                                @if(@$user->state_id !== 0)
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>State :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong> {{ $user->stateName->name }} <strong>
                                                </div>
                                                @endif
                                                @if(@$user->city)
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>City :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong> {{ $user->city }} <strong>
                                                </div>
                                                @endif
                                                @if(@$user->district)
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>District :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong> {{ $user->district }} <strong>
                                                </div>
                                                @endif
                                                @if(@$user->zipcode)
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Zip Code :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong> {{ $user->zipcode }} <strong>
                                                </div>
                                                @endif
                                                @if(@$user->rate_price !== 0)
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Consultant Fees :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong> {{ $user->rate_price }} <strong>
                                                </div>
                                                @endif
                                                @if(@$user->rate)
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Rate :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong>
                                                    @if($user->rate == 'H')
                                                        {{ 'Hourly' }}
                                                    @elseif($user->rate == 'M')
                                                        {{ 'Minute' }}
                                                    @endif
                                                    <strong>
                                                </div>
                                                @endif
                                                @if(@$user->crp_no)
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>CRP No :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong> {{ $user->crp_no }} <strong>
                                                </div>
                                                @endif

                                                @if(@$user->found_on)
                                                <div class="clearfix"></div>
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>How did you hear about us :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                <p>{{$user->found_on}}</p>
                                                </div>
                                                @endif

                                                <div class="clearfix"></div>
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                <p>Due Affiliation Earnings:</p>
                                                </div>
                                                <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5">
                                                <strong>
                                                    {{@$due}}
                                                </strong>
                                                </div>

                                            </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div class="order-id">
                                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Profile Picture :</p>
                                                    </div>
                                                    <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5">
                                                    <div style="height: 100px; width: 100px;">
                                                        <img src="{{ @$user->profile_pic ? URL::to('storage/app/public/uploads/profile_pic/'.$user->profile_pic) : URL::to('public/frontend/images/no_img.png') }}" alt="" style="width: auto;height: 100%;">
                                                    </div>
                                                    </div>
                                                    <div class="clearfix"></div>

                                                    <div class="clearfix"></div>
                                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Email Verified   :</p>
                                                    </div>
                                                    <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5">
                                                    <strong>
                                                        @if(@$user->is_email_verified == 'Y')
                                                        {{ 'Yes' }}
                                                        @elseif(@$user->is_email_verified == 'N')
                                                        {{ 'No' }}
                                                        @endif
                                                    </strong>
                                                    </div>

                                                    <div class="clearfix"></div>

                                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Created On :</p>
                                                    </div>
                                                    <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    @php
                                                    $created_on=date("jS-M-Y", strtotime(@$user->created_at));
                                                    @endphp
                                                    <strong>{{ @$created_on ? $created_on : 'N.A' }}</strong>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Status :</p>
                                                    </div>
                                                    <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong>
                                                        @if(@$user->status == 'A')
                                                        {{ 'Active' }}
                                                        @elseif(@$user->status == 'I')
                                                        {{ 'Inactive' }}
                                                        @elseif(@$user->is_approved == 'N')
                                                        {{ 'Awaiting Approval' }}
                                                        @elseif(@$user->status == 'U')
                                                        {{ 'Unverifed' }}
                                                        @endif
                                                    </strong>
                                                    </div>
                                                    @if(@$user->cpf_no)
                                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                        <p>CPF No :</p>
                                                    </div>
                                                    <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                        <strong> {{ $user->cpf_no }} <strong>
                                                    </div>
                                                    @endif
                                                    @if(@$user->area_code !== 0)
                                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                        <p>Area Code :</p>
                                                    </div>
                                                    <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                        <strong> {{ $user->area_code }} <strong>
                                                    </div>
                                                    @endif
                                                    @if(@$user->street_name)
                                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                        <p>Street Name :</p>
                                                    </div>
                                                    <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                        <strong> {{ $user->street_name }} <strong>
                                                    </div>
                                                    @endif
                                                    @if(@$user->street_number !== 0)
                                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                        <p>Street Number :</p>
                                                    </div>
                                                    <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                        <strong> {{ $user->street_number }} <strong>
                                                    </div>
                                                    @endif
                                                    @if(@$user->complement)
                                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                        <p>Complement :</p>
                                                    </div>
                                                    <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                        <strong> {{ $user->complement }} <strong>
                                                    </div>
                                                    @endif
                                                    @if($user->userLang->isNotEmpty())
                                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                        <p>Languages :</p>
                                                    </div>
                                                    <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                        @foreach($user->userLang as $row)
                                                        <strong>
                                                            {{ $row->languageName->name }}
                                                            @if($loop->remaining !== 0)
                                                                {{ ',' }}
                                                            @endif
                                                            <strong>
                                                        @endforeach
                                                    </div>
                                                    @endif
                                                    <div class="clearfix"></div>
                                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Total Affiliation Earnings:</p>
                                                    </div>
                                                    <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5">
                                                    <strong>
                                                        {{@$total}}
                                                    </strong>
                                                    </div>

                                                    <div class="clearfix"></div>
                                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Paid Affiliation Earnings:</p>
                                                    </div>
                                                    <div class="col-lg-8 col-md-5 col-sm-5 col-xs-5">
                                                    <strong>
                                                        {{@$paid}}
                                                    </strong>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <!--order-detail end-->
                                    </div>


                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" data-pattern="priority-columns">
                                            <table id="datatable" class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>


                                                        <th>Product Image</th>
                                                        <th>Name of Product</th>
                                                        <th>Price</th>
                                                        <th>Category</th>
                                                        <th>Ordered By</th>
                                                        <th>Affiliate Percentage</th>
                                                        <th>Affiliate Commission</th>
                                                        <th>Balance Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    @if(count(@$orders)>0)
                                                        @foreach(@$orders as $order)
                                                        <tr>
                                                            <td><img src="{{URL::to('storage/app/public/uploads/product_cover_images/') . '/' . @$order->product->cover_image}}" height="50" width="60"></td>
                                                            <td>{{@$order->product->title}}</td>
                                                            <td>{{@$order->product->price}}</td>
                                                            <td>{{@$order->product->category->category_name}}</td>
                                                            <td>{{@$order->orderMaster->userDetails->nick_name ? @$order->orderMaster->userDetails->nick_name :@$order->orderMaster->userDetails->name }}</td>
                                                            <td>{{@$order->product->affiliate_percentage}}</td>
                                                            <td>{{@$order->affiliate_commission}}</td>
                                                            <td>
                                                                @if(@$order->paymentProductDetails->balance_status == 'R')
                                                                    Due
                                                                @else
                                                                    Paid
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        @endforeach

                                                    @endif

                                                </tbody>


                                            </table>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <!-- End Row -->
            </div>
            <!-- container -->
        </div>
        <!-- content -->
        <!--<footer class="footer text-right">
            2015 © Moltran.
            </footer>-->
    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->
    <!-- Right Sidebar -->
    <!-- /Right-bar -->
</div>
<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Transfer the amount to the following bank account and then enter a note for the professional</h4>
                {{-- Transfer the amount to the following bank account and then enter a note for the professional --}}
            </div>
            <div class="modal-body" style="padding: 1rem;">
                <p id="professional_name"></p>
                <p id="payment_amount"></p>
                <p id="bankName"></p>
                <p id="accountNumber"></p>
                <p id="agencyNumber"></p>
                <p id="agencyCheckNumber"></p>
                <p id="bank_number"></p>
                <form action="{{route('admin.make.transfer.bank')}}" method="post" id="updateeventsform2" class="form-horizontal">
                    <input type="hidden" name="payment_token_no" id="payment_token_no" value="">
                    <input type="hidden" name="paymentId" id="paymentId" value="">
                    @csrf
                        <label for='areaforinfo'>Note </label>

                            <textarea class="form-control required" id='areaforinfo' rows="4" style="min-width: 100%; border: 1px solid #CCC;"  name="note"></textarea>
                    <div class="submit-login add_btnm " style="width:auto;margin-top:10px;">
                    <button type="submit" class="btn btn-default">Payment Confirmation </button>
                    </div>
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="myModal1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Transfer the amount to the following paypal address and then enter a note for the professional</h4>
                {{-- Transfer the amount to the following bank account and then enter a note for the professional --}}
            </div>
            <div class="modal-body" style="padding: 1rem;">
                <p id="professional_name1"></p>
                <p id="payment_amount1"></p>
                <p id="paypal"></p>
                <form action="{{route('admin.make.transfer.paypal')}}" method="post" id="updateeventsform3"
                    class="form-horizontal">
                    <input type="hidden" name="payment_token_no" id="payment_token_no1" value="">
                    <input type="hidden" name="paymentId" id="paymentId1" value="">
                    @csrf
                    <label for='areaforinfo'>Note </label>

                    <textarea class="form-control required" id='areaforinfo' rows="4"
                        style="min-width: 100%; border: 1px solid #CCC;" name="note"></textarea>
                    <div class="submit-login add_btnm " style="width:auto;margin-top:10px;">
                        <button type="submit" class="btn btn-default">Payment Confirmation </button>
                    </div>
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
<!-- END wrapper -->
<!--Raise  popup-->
{{-- <div class="container">
    <div class="custom_popup pop1" style="display:none;">
        <button type="button" class="close lg_csl" data-dismiss="modal" aria-hidden="true">
        <img src="image1/close_btn.png" alt=""></button>
        <div class="pop_from_area">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="your-mail">
                    <label for="exampleInputEmail1">Vendor</label>
                    <select class="form-control newdrop">
                        <option>Choose</option>
                        <option value="volvo">Vendor 1</option>
                        <option value="saab">Vendor 2</option>
                        <option value="volvo">Vendor 3</option>
                        <option value="saab">Vendor 4</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="your-mail">
                    <label for="exampleInputEmail1">Issue Category</label>
                    <select class="form-control newdrop">
                        <option value="volvo">Choose</option>
                        <option value="saab">Category 1</option>
                        <option value="mercedes">Category 2</option>
                        <option value="saab">Category 3</option>
                        <option value="mercedes">Category 4</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="your-mail">
                    <label for="exampleInputEmail1">Description</label>
                    <textarea class="form-control des" rows="3"></textarea>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="submit-login no_mmg">
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div> --}}
<!--Raise  popup-->
<script>
    var resizefunc = [];
</script>

<script type="text/javascript">
    $(document).ready(function() {
    $('#datatable_length').hide();
        $('#datatable').dataTable({
    "ordering": false,
    "info":     false,
    "searching": false,
    "dom": '<"toolbar">frtip'
    });
    $(".check_check").click(function(){
    if($(this).is(":checked"))
    {
    $('.ch_1').show();
    }
    else
    {
    $('.ch_1').hide();
    }
    });
    //$("div.toolbar").html('<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 status">Status: <select name="" class="newdrop"><option value="1">Active</option><option value="1">Inactive</option></select></div><div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 clndr">From date : <input type="text" name="from" id="frm_date"> </div><div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 clndr"> To date : <input type="text" name="to" id="to_date"> </div><div class="col-md-4 col-lg-4 clndr">Keyword: <input type="text" name="abc"></div><div class="col-md-4 col-lg-4 col-sm-4 colxs-12 srch"><input type="submit" name="submit" value="Search"> </div>');
       // $('#datatable-keytable').DataTable( { keys: true } );
       //$('#datatable-responsive').DataTable();
        //$('#datatable-scroller').DataTable( { ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
        //var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } );
    } );
    // TableManageButtons.init();
</script>
<script>
    function submitForm(amount, name, token,id){
        if(confirm('The amount '+amount+' will be transferred on '+name+'\'s account')){
            $('#id').val(id);
            $('#no').val(token);
            $('#myForm').submit();
        }
        else{
            return false;
        }
    }
</script>
<script>
    $(document).ready(function(){
		$("#updateeventsform2").validate({});
        $("#updateeventsform3").validate({});
	});
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
