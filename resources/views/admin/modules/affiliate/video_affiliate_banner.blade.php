@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Admin | Banner Edit')

@section('header')
@include('admin.includes.header')
<style>
    .img_div{
        width: 50%;
    }
</style>
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- Begin page -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="pull-left page-title">Banner Edit</h4>
                        <div class="submit-login no_mmg pull-right">
                            <a href="{{route('admin.manage.video.affiliate.banner.list')}}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
                        </div>

                        <!--<ol class="breadcrumb pull-right">
                            <li><a href="#">User Dashboard</a></li>
                            <li class="active">My Order</li>
                            </ol>-->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body table-rep-plugin">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12 nhp">
                                        <div class="table-responsive" data-pattern="priority-columns">
                                            <form id="myform" method="post" action="{{route('admin.manage.video.affiliate.banner',['id'=>Request::segment(3)])}}" enctype="multipart/form-data">
                                                @csrf
                                                {{-- @if(Request::segment(3)==1) --}}
                                                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                                    <div class="your-mail">
                                                        <label for="media">Banner {{Request::segment(3)}}
                                                            @if(Request::segment(3)==1)
                                                            (Recommended size 300×250 )
                                                            @elseif(Request::segment(3)==2)
                                                            (Recommended size 728×90 )
                                                            @elseif(Request::segment(3)==3)
                                                            (Recommended size 970×250 )
                                                            @elseif(Request::segment(3)==4)
                                                            (Recommended size 300×600 )
                                                            @elseif(Request::segment(3)==5)
                                                            (Recommended size 200×200 )
                                                            @elseif(Request::segment(3)==6)
                                                            (Recommended size 970×90 )
                                                            @endif
                                                        </label>
                                                        <input type="file" accept="image/*" name="media" class="image_input form-control nameExistErrClass1" id="media" >
                                                        <img src="{{ URL::to('storage/app/public/uploads/banner/banner'.Request::segment(3).'.png')}}" style=" object-fit: contain; display: block ; max-width:100%" alt="">
                                                    </div>
                                                </div>
                                                {{-- @endif --}}
                                                {{-- @if(Request::segment(3)==2)
                                                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-4">
                                                    <div class="your-mail">
                                                        <label for="media2">Banner 2 * (Recommended size 600 * 480)</label>
                                                        <input type="file" accept="image/*" name="media2" class="image_input form-control nameExistErrClass2" data-id="media2" id="media2">
                                                        <img src="{{ URL::to('storage/app/public/uploads/banner/banner2.png')}}" style="height: 100px; width: 150px; object-fit: contain; display: block;" alt="">
                                                    </div>
                                                </div>
                                                @endif
                                                @if(Request::segment(3)==3)
                                                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-4">
                                                    <div class="your-mail">
                                                        <label for="media3">Banner 3 * (Recommended size 900 * 720)</label>
                                                        <input type="file" accept="image/*" name="media3" class="image_input form-control nameExistErrClass3" data-id="media3" id="media3">
                                                        <img src="{{ URL::to('storage/app/public/uploads/banner/banner3.png')}}" style="height: 100px; width: 150px; object-fit: contain; display: block;" alt="">
                                                    </div>
                                                </div>
                                                @endif --}}
                                                <div class="col-lg-12">
                                                    <div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
                                                        <input value="Save" type="submit" class="btn btn-default">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Row -->
            </div>
            <!-- container -->
        </div>
        <!-- content -->
        <!--<footer class="footer text-right">
            2015 © Moltran.
            </footer>-->
    </div>
</div>

<script>
    var resizefunc = [];
</script>

<script type="text/javascript">
    $(document).ready(function() {
    $('#datatable_length').hide();
        $('#datatable').dataTable({
    "ordering": false,
    "info":     false,
    "searching": false,
    "dom": '<"toolbar">frtip'
    });
        $('#datatable1').dataTable({
    "ordering": false,
    "info":     false,
    "searching": false,
    "dom": '<"toolbar">frtip'
    });
    $(".check_check").click(function(){
    if($(this).is(":checked"))
    {
    $('.ch_1').show();
    }
    else
    {
    $('.ch_1').hide();
    }
    });
    //$("div.toolbar").html('<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 status">Status: <select name="" class="newdrop"><option value="1">Active</option><option value="1">Inactive</option></select></div><div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 clndr">From date : <input type="text" name="from" id="frm_date"> </div><div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 clndr"> To date : <input type="text" name="to" id="to_date"> </div><div class="col-md-4 col-lg-4 clndr">Keyword: <input type="text" name="abc"></div><div class="col-md-4 col-lg-4 col-sm-4 colxs-12 srch"><input type="submit" name="submit" value="Search"> </div>');
       // $('#datatable-keytable').DataTable( { keys: true } );
       //$('#datatable-responsive').DataTable();
        //$('#datatable-scroller').DataTable( { ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
        //var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } );
    } );
    // TableManageButtons.init();
    $(document).ready(function() {
        $('#create-link').click(function() {
            $('#link').val('<a target="_blank" href="' + $('#link-url').val() + '">' + $('#link-text').val() + '</a>');
            $('#link-url').val('');
            $('#link-text').val('');
        });
        const regex = new RegExp("(.*?)\.(png|jpg|jpeg)$");
        $('.image_input').on('change', function() {
            const val = $(this).val().toLowerCase();
            if (!(regex.test(val))) {
                $(this).val('');
                alert('Only png and jpeg files are allowed.');
            }
            if (this.files && this.files[0]) {
                // $('#media' ).removeData('imageWidth');
                // $('#media').removeData('imageHeight');
                // var file = this.files[0];
                // var tmpImg = new Image();
                // tmpImg.src=window.URL.createObjectURL( file );
                // tmpImg.onload = function() {
                //     width = tmpImg.naturalWidth,
                //     height = tmpImg.naturalHeight;
                //     $('#media').data('imageWidth', width);
                //     $('#media').data('imageHeight', height);
                // }
                var reader = new FileReader();
                const t = $(this);
                reader.onload = function(e) {
                    t.next('img').attr('src', e.target.result).show();
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
        var data =[[300, 250],[728,90],[970,250],[300,600],[200,200],[97090]] ;
        console.log(data[0]);
        var index=parseInt('{{Request::segment(3)}}')-1;
		$("#myform").validate({});
        // jQuery.validator.addMethod("dimention1", function (value, element, param) {
        //     var width = $(element).data('imageWidth');
        //     var height = $(element).data('imageHeight');
        //     var a=param[0];
        //     if(width <= param[0] && height <= param[1]){
        //         return true;
        //     }else if(width==undefined){
        //         return true;
        //     }else{
        //         return false;
        //     }
        // }, 'Image size not geter than recommended size');
        // jQuery.validator.addClassRules("nameExistErrClass1", {
        //     dimention1:data[index]
        // });
	});
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
