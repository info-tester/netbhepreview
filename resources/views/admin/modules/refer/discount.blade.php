@extends('admin.layouts.app')
@section('title', 'NearO | Admin | Refer Discount')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Manage Discount</h4>
                    {{-- <div class="submit-login no_mmg pull-right">
                        <a href="{{ route('admin.coupon.create') }}" title="Create"><button type="button"
                                class="btn btn-default">Add</button></a>
                    </div> --}}
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-rep-plugin">
                            <div class="row">
                                
                        <div class="col-md-12 dess5">
                            <i class="fa fa-pencil-square-o cncl" aria-hidden="true" style="border: none;"> <span
                                    class="cncl_oopo">Edit</span></i>
                            
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="table-responsive" data-pattern="priority-columns">
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Referrer User Discount</th>
                                            <th>Referrer professionals discount</th>
                                            <th>Referred User Discount</th>
                                            <th>Referred professionals discount</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach(@$referDiscount as $row)
                                        <tr>
                                            <td>{{ @$row->referrer_student_discount }} %</td>
                                            <td>{{ @$row->referrer_teacher_discount }} %</td>
                                            <td>{{ @$row->referred_student_discount }} %</td>
                                            <td>{{ @$row->referred_teacher_discount }} %</td>
                                            
                                            <td>
                                                <a href="{{ route('admin.refer.discount.edit',[@$row->id]) }}" title="Edit"> <i
                                                        class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>

{{-- <form method="post" id="destroy">
	{{ csrf_field() }}
{{method_field('delete')}}
</form>
<script>
    function deleteCategory(val){
	var confirm = window.confirm('Are you want to delete this coupon ?');
	if(confirm){
		$("#destroy").attr('action',val);
		$("#destroy").submit();
	}
 }
</script> --}}
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection