@extends('admin.layouts.app')
@section('title', 'NearO | Admin | Refer Discount Edit')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Edit Discount</h4>
                    <div class="submit-login no_mmg pull-right">
                        <a href="{{ route('admin.refer.discount') }}" title="Back"><button type="button"
                                class="btn btn-default">Back</button></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-rep-plugin">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 nhp">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <form id="myform" method="post"
                                            action="{{ route('admin.refer.discount.update',['id'=>@$referDiscount->id])}}"
                                            enctype="multipart/form-data">
                                            @csrf
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Referrer Benefit:</label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">User Discount on Booking (%)</label>
                                                    <input type="text" name="referrer_student_discount" class="form-control required"
                                                        id="referrer_student_discount"
                                                        value="{{old('referrer_student_discount',@$referDiscount->referrer_student_discount)}}">
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Professionals discount on Commission (%)</label>
                                                    <input type="text" name="referrer_teacher_discount"
                                                        class="form-control required" id="referrer_teacher_discount"
                                                        value="{{old('referrer_teacher_discount',@$referDiscount->referrer_teacher_discount)}}">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Referred User Benefit:</label>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">User Discount on Booking (%)</label>
                                                    <input type="text" name="referred_student_discount" class="form-control required" id="referred_student_discount"
                                                        value="{{old('referred_student_discountt',@$referDiscount->referred_student_discount)}}">
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Professionals discount on Commission (%)</label>
                                                    <input type="text" name="referred_teacher_discount" class="form-control required" id="referred_teacher_discount"
                                                        value="{{old('referred_teacher_discount',@$referDiscount->referred_teacher_discount)}}">
                                                </div>
                                            </div>
                                            
                                            <div class="clearfix"></div>

                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="add_btnm submit-login">
                                                    <input value="Update" type="submit"
                                                        class="btn btn-default">
                                                </div>
                                            </div>
                                            <!--all_time_sho-->
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Row -->
        </div>
        <!-- container -->
    </div>
    <!-- content -->
</div>
<script>
    $(document).ready(function(){
		$("#myform").validate({
			rules: {
				// referrer_student_discount : 'number',
				// referrer_teacher_discount : 'number',
				// referred_student_discount : 'number',
				// referred_teacher_discount : 'number',
                referrer_student_discount : {
                    digits: true,
                    referrer_student_discount_can_not_be_greater:true
                },
                referrer_teacher_discount : {
                    digits: true,
                    referrer_teacher_discount_can_not_be_greater:true
                },
                referred_student_discount : {
                     digits: true,
                     referred_student_discount_can_not_be_greater:true
                },
                referred_teacher_discount : {
                     digits: true,
                     referred_teacher_discount_can_not_be_greater:true
                },
			},
			messages : {
				referrer_student_discount : {
					required 		: "Please enter discount amount.",
					number 			: "Please enter number.",
                    digits          : "Please enter positive number."
				},
				referrer_teacher_discount : {
					required 		: "Please enter discount amount.",
					number 			: "Please enter number.",
                    digits          : "Please enter positive number."
				},
				referred_student_discount : {
					required 		: "Please enter discount amount.",
					number 			: "Please enter number.",
                    digits          : "Please enter positive number."
				},
				referred_teacher_discount : {
					required 		: "Please enter discount amount.",
					number 			: "Please enter number.",
                    digits          : "Please enter positive number."
				},
				
	        },
	        // errorPlacement: function (error, element) 
	        // {
	        //     toastr.error(error.text());
	        // }
		});
        
    jQuery.validator.addMethod("referrer_student_discount_can_not_be_greater", function(value, element) {
        var comm = $('#referrer_student_discount').val(); 
         if (comm  <= 100)
            return true;
            return false; 
        }, "Referrer Student Discount cannot be greater than 100");

    jQuery.validator.addMethod("referrer_teacher_discount_can_not_be_greater", function(value, element) {
        var comm = $('#referrer_teacher_discount').val(); 
         if (comm  <= 100)
            return true;
            return false; 
        }, "Referrer Teacher Discount cannot be greater than 100");

    jQuery.validator.addMethod("referred_student_discount_can_not_be_greater", function(value, element) {
        var comm = $('#referred_student_discount').val(); 
         if (comm  <= 100)
            return true;
            return false; 
        }, "Referred Student Discount cannot be greater than 100");

    jQuery.validator.addMethod("referred_teacher_discount_can_not_be_greater", function(value, element) {
        var comm = $('#referred_teacher_discount').val(); 
         if (comm  <= 100)
            return true;
            return false; 
        }, "Referred Teacher Discount cannot be greater than 100");
	});
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection