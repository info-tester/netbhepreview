@extends('admin.layouts.app')
@section('title', 'Netbhe|Newsletter Subscriptions')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
<div class="content">
    <div class="container">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="pull-left page-title">Newsletter Subscription</h4>
            </div>
        </div>

        @include('admin.includes.error')

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body table-rep-plugin">
                        <div class="row">
                            <form method="POST" action="{{ route('admin.newsLetter') }}" id="myForm">
                                @csrf
                                <div class="col-lg-12">
                                    <h4 class="pull-left" style="margin-top: 24px;">Newsletter Text Content</h4>
                                </div>
                                <div class="all_time_sho">
                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                        <div class="your-mail">
                                            <label for="exampleInputEmail1">Description *</label>
                                            <textarea placeholder="Write description" name="description" id="description" rows="6" class="form-control required">{!! @$nlContent->description !!}</textarea>
                                        </div>
                                        <p class="meet_description" id="cntnt"></p>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="submit-login add_btnm" style="width:auto; margin-top:15px;">
                                        <input value="Update" type="submit" class="btn btn-default">
                                    </div>
                                </div>
                            </form>


                            {{-- <div class="exmmt_rrn2">
                                <form action="{{ route('admin.filter.categories') }}" method="post" id="myFormSearch" name="myFormSearch">
                                    @csrf
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                        <div class="your-mail">
                                            <label for="exampleInputEmail1">Keyword</label>
                                            <input class="form-control" id="keyword" type="text" name="keyword" value="{{ @$key['keyword'] }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                        <div class="your-mail">
                                            <label for="exampleInputEmail1">Parent Category</label>
                                            <select class="form-control newdrop" name="parent_id" id="parent_id">
                                                <option value="">Select</option>
                                                @foreach($p_cat as $pt)
                                                <option value="{{ $pt->id }}" @if(@$key['parent_id']==$pt->id) selected @endif>{{ $pt->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                        <div class="your-mail">
                                            <label for="exampleInputEmail1">Status</label>
                                            <select class="form-control newdrop" name="status" id="status">
                                                <option value="">Select</option>
                                                <option value="A" @if(@$key['status']=="A") selected @endif>Active</option>
                                                <option value="I" @if(@$key['status']=="I") selected @endif>Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                        <div class="add_btnm exrt">
                                            <input class="nm9 xxx" value="Search" type="button">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12 dess5">
                                <i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
                                <i class="fa fa-trash cncl"> <span class="cncl_oopo">Delete</span></i>
                                <i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active</span></i>
                                <i class="fa fa-times cncl" aria-hidden="true"> <span class="cncl_oopo">Inactive</span></i>
                            </div> --}}
                            <div class="col-md-12">
                                <div class="link_style">
                                <a href="{{ route('admin.newsLetter.download') }}"><i class="fa fa-download"></i>Download</a>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <h4 class="pull-left" style="margin-top: 24px;">Newsletter Entries</h4>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" data-pattern="priority-columns">
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Email</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $i=1;
                                            @endphp
                                            @foreach(@$nl as $n)
                                                <tr>
                                                    <td>{{ @$i }}</td>
                                                    <td>{{ @$n->email }}</td>
                                                </tr>
                                                @php
                                                    $i++;
                                                @endphp
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Row -->
    </div>
    <!-- container -->
</div>
</div>
<script>
    $(document).ready(function() {
        $("#myForm").validate();
    });
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
    <script>
        $('.xxx').click(function(){
            $("#myFormSearch").submit();
        });
    </script>
@endsection
