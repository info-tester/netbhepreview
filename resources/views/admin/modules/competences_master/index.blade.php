@extends('admin.layouts.app')
@section('title', 'netbhe.com | Admin | ContractTemplat')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Competences Master</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('competences-master.create') }}" title="Create"><button type="button" class="btn btn-default">Add Competences</button></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
                            <div class="row">
                                <form method="GET" action="{{ route('competences-master.index')}}">
                                    <div class="col-md-6 col-sm-12 col-xs-12 col-lg-4">
                                        <div class="your-mail">
                                            <label for="exampleInputEmail1">Search by Evaluation Category</label>
                                            <select name="category" id="category" class="form-control newdrop">
                                                <option value="">Choose Category</option>
                                                @foreach($categories as $cat)
                                                    <option value="{{ @$cat->id }}" @if(@$keys['category'] == $cat->id) {{ "selected" }} @endif>{{$cat->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 submit-login">
                                        <button type="submit" class="btn btn-default">Search</button>
                                    </div>
                                </form>
                            </div>
							<div class="row">
								<div class="col-md-12 dess5">
									<i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
									<i class="fa fa-trash-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Competences</th>
													<th>Evaluation Category</th>
													<th>Associated Specialities</th>
													<!-- <th>Question</th> -->
													<!-- <th>Added By</th>
													<th>Status</th>
													<th>Show in<br>Professional</th> -->
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@if($all_competences_master->isNotEmpty())
												@foreach(@$all_competences_master as $detail)
												<tr>
													<td>{{ $detail['title'] }}</td>
													<td>{{ $detail->evalCategory->name }}</td>
													<td>
														@if(count(@$detail->specialities)>0)
															@php
																$specialities = "";
																foreach($detail->specialities as $spec){
																	$specialities .= $spec->name . ', ';
																}
															@endphp
															{{substr($specialities,0,-2)}}
														@endif
													</td>

													<!-- <td>{{ $detail['question'] }}</td> -->



													<td>
														<a href="{{ route('competences-master.edit',['id'=>@$detail->id]) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
														<a href="javascript:void(0)" onclick="deleteCategory('{{route('competences-master.destroy',[$detail->id])}}')" title="Delete" class="rjct delbtn"><i class="fa fa-trash-o delet" aria-hidden="true"></i></a>
													</td>
												</tr>
												@endforeach

												@endif
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- <form method="DELETE" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
	var confirm = window.confirm('Do you want to delete this blog?');
	if(confirm){
		$("#destroy").attr('action',val);
		$("#destroy").submit();
	}
 }
</script> -->

<form method="post" id="destroy">
    {{ csrf_field() }}
    {{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
    var confirm = window.confirm('Do you want to delete this ?');
    if(confirm){
        $("#destroy").attr('action',val);
        $("#destroy").submit();
    }
 }
</script>


@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
