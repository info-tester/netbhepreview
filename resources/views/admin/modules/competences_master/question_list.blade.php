@extends('admin.layouts.app')
@section('title', 'netbhe.com | Admin | Competences')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<script src="{{URL::to('public/js/chosen.jquery.min.js')}}"></script>
<link rel="stylesheet" href="{{URL::to('public/css/chosen.css')}}">

<div class="content-page">
	<!-- Start content -->






	<div class="content">



		<div class="container">


			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Competences Edit</h4>

					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('competences-master.index') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>


				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">

                                        <form id="myform" method="post" action="{{ route('competences-master.update',['id'=>@$details->id]) }}">
											<input name="_method" type="hidden" value="PUT">
											{{ csrf_field() }}

											<div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Selected evaluation category</label>
                                                    <label for="exampleInputEmail1"><b>{{ $details->evalCategory->name }}</b></label>
                                                </div>
                                            </div>
											<div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Selected evaluation types</label>
                                                    <label for="exampleInputEmail1"><b>{{ implode(', ', $types) }}</b></label>
                                                </div>
                                            </div>

											<div class="col-lg-6 col-md-12 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Professional speciality  *</label>
                                                    <select name="speciality_ids[]" id="speciality_ids" data-placeholder ="Select professional speciality" class="required form-control newdrop required chosen-select1" multiple="true" required="">
													@foreach (@$specialities as $spec)
														<option value="{{ $spec->id }}"
														@if(@$related_spec_ids && in_array($spec->id, $related_spec_ids)) selected @endif
														>{{ $spec->name }}</option>
													@endforeach
                                                    </select>
													<label class="error" id="chosen_select_error" style="display:none;">This field is required</label>
                                                </div>
                                            </div>

											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1" class="personal-label">Competences *</label>
                                                    <input type="text" name="title" value="{{ $details->title }}" class="form-control required" id="title" placeholder="Subject" >
                                                </div>
                                            </div>

                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Description *</label>
                                                    <textarea  name="description" class="form-control required" id="title" placeholder="Description" >{{ @$details->description }} </textarea>
                                                </div>
                                            </div>


											<div class="clearfix"></div>
											<br>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="submit-login add_btnm">
													<input value="Update" type="submit" class="btn btn-default">
												</div>
											</div>
											<!--all_time_sho-->
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>




			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Competences Master : {{@$details->title}} All Question lists</h4>

					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.competences.question.add',['id'=>@$details->id]) }}" title="Add Questions"><button type="button" class="btn btn-default">Add Question</button></a>
					</div>
				</div>
			</div>



			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 dess5">
									<i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
									<i class="fa fa-trash-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
									<!-- <i class="fa fa-trash-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
									<i class="fa fa-eye cncl"  aria-hidden="true"> <span class="cncl_oopo">View Fields</span></i>

									<i class="fa fa-square cncl" aria-hidden="true"> <span class="cncl_oopo"> Not Show in All Professional</span></i>
                                    <i class="fa fa-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Show in All Professional</span></i> -->
									<!-- <i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active</span></i>
                                	<i class="fa fa-times cncl" aria-hidden="true"> <span class="cncl_oopo">Inactive</span></i> -->
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Competences</th>
													<th>Question</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@if($all_question->isNotEmpty())
												@foreach(@$all_question as $detail)
												<tr>
													<td>{{@$details->title}}</td>
													<td>{{ $detail['question'] }}</td>

													<td>
														<a href="{{ route('admin.competences.question',['id'=>@$detail->id]) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
														<a href="javascript:void(0)" onclick="deleteCategory('{{route('admin.competences.questiondel',[$detail->id])}}')" title="Delete" class="rjct delbtn"><i class="fa fa-trash-o delet" aria-hidden="true"></i></a>
													</td>
												</tr>
												@endforeach

												@endif
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>










		</div>



	</div>
</div>

<!-- <form method="DELETE" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
	var confirm = window.confirm('Do you want to delete this blog?');
	if(confirm){
		$("#destroy").attr('action',val);
		$("#destroy").submit();
	}
 }
</script> -->

<form method="post" id="destroy">
    {{ csrf_field() }}
    {{method_field('POST')}}
</form>
<script>
	function deleteCategory(val){
		var confirm = window.confirm('Do you want to delete this ?');
		if(confirm){
			$("#destroy").attr('action',val);
			$("#destroy").submit();
		}
	}

	$(document).ready(function(){
        $(".chosen-select").chosen();
        $(".chosen-select1").chosen();

        $('#evaluation_category_id').change(function() {
            var val = $(this).val();

            var chosenOpts = '';
            types.forEach(v => {
                if (v.evaluation_category_id == val) {
                    chosenOpts += '<option value="' + v.id + '">' + v.title + '</option>'
                }
            });
            $(".chosen-select").html(chosenOpts);
            $(".chosen-select").trigger("chosen:updated");

            if(val != ''){
                // console.log(val);
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('load.specialities') }}",
                    method:"POST",
                    data:{val:val, _token:_token},
                    success:function(data){
                        // console.log(data);
                        $('#speciality_ids > option').each(function(){
                            if($.inArray( parseInt($(this).val()), data ) !== -1){
                                // console.log("VAL: "+$(this).val())
                                // console.log($(this).text());
                                $(this).attr('selected','selected');
                            }
                        });
                        $(".chosen-select1").trigger("chosen:updated");
                    }
                });
            }
        });

        $('#evaluation_type_id').change(function() {
            var arr = $(this).val();
            if(arr != ''){
                console.log(arr);
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('load.type.specialities') }}",
                    method:"POST",
                    data:{arr:arr, _token:_token},
                    success:function(data){
                        // console.log(data);
                        $('#speciality_ids > option').each(function(){
                            if($.inArray( parseInt($(this).val()), data ) !== -1){
                                // console.log("VAL: "+$(this).val())
                                // console.log($(this).text());
                                $(this).attr('selected','selected');
                            }
                        });
                        $(".chosen-select1").trigger("chosen:updated");
                    },
                    error:function(error){
                        console.log(error);
                    }
                });
            }
        });

        $("#myform").validate({
			submitHandler: function(form) {
				var flag = 0;
				if($('#speciality_ids').val() == null){
					$('#chosen_select_error').css('display', 'block');
					flag = 1;
				}
				if(flag == 1){
					return false;
				} else {
					$('#chosen_select_error').css('display', 'none');
					// console.log("Submitted");
					// return false;
					form.submit();
				}
			}
        });
	});
</script>


@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
