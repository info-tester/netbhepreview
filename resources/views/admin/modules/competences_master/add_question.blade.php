@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Add')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Add Competences Master Question ({{ $details->title }})</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('competences-master.index') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
				</div>
			</div>
			@include('admin.includes.error')
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
										
											<form id="myform" method="post" action="{{ route('admin.competences.question.added') }}">
											<!-- <input name="_method" type="hidden" value="PUT"> -->
											{{ csrf_field() }}
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1" class="personal-label">Question *</label>
                                                    <input type="text" name="question"  class="form-control required" id="title" placeholder="Add Question" >
                                                    <input type="hidden" name="competences_id" value="{{ $details->id }}" class="form-control required" id="title" >
                                                    <input type="hidden" name="title" value="{{ $details->title }}" class="form-control required" id="title" >
                                                </div>
                                            </div>

											
											<div class="clearfix"></div>
											<br>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="submit-login add_btnm">
													<input value="submit" type="submit" class="btn btn-default">
												</div>
											</div>
											<!--all_time_sho-->
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>


<style type="text/css">
    .upldd {
    display: block;
    width: auto;
    border-radius: 4px;
    text-align: center;
    background: #9caca9;
    cursor: pointer;
    overflow: hidden;
    padding: 10px 15px;
    font-size: 15px;
    color: #fff;
    cursor: pointer;
    float: left;
    margin-top: 15px;
    font-family: 'Poppins', sans-serif;
    position: relative;
}
.upldd input {
    position: absolute;
    font-size: 50px;
    opacity: 0;
    left: 0;
    top: 0;
    width: 200px;
    cursor: pointer;
}
.upldd:hover{
    background: #1781d2;
}

</style>




<style>
    .error{
        color: red !important;
    }
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection