@extends('admin.layouts.app')
@section('title', 'Netbhe | Admin | Coupon Add')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">{{ @$coupon?'Edit Coupon':'Add Coupon'}}</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.coupon') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
										<form id="myform" method="post" action="{{ @$coupon ? route('admin.coupon.update',['id'=>$coupon->id]) :route('admin.coupon.store')}}" enctype="multipart/form-data">
											@csrf

											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Coupon Code</label>
													<input type="text" name="coupon_code" class="form-control required" id="coupon_code" value="{{old('coupon_code',@$coupon->coupon_code)}}">
												</div>
											</div>

											{{-- <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Coupon Type </label>
													<select class="form-control newdrop required" name="coupon_type" id="coupon_type">
														<option value="">Choose Type</option>
														<option value="F">Flat</option>
														<option value="P">Percentage</option>
													</select>
												</div>
											</div> --}}

											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Discount</label>
													<input type="text" name="discount_amount" class="form-control required" id="discount_amount" value="{{old('discount_amount',@$coupon->discount)}}">
												</div>
											</div>
											<div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
												<div class="your-mail">
													<label for="exampleInputEmail1">Start Date</label>@if(@$coupon->start_date) @php $start_date=date('d-m-Y', strtotime(@$coupon->start_date)) @endphp @endif
													<input type="text" class="form-control required" name="start_date" id="datepicker" placeholder="" value="{{old('start_date',@$start_date)}}">
												</div>
											</div>

											<div class="clearfix"></div>

											<div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
	                                            <div class="your-mail">
	                                                <label for="exampleInputEmail1">Expiry Date</label>@if(@$coupon->exp_date) @php $exp_date=date('d-m-Y', strtotime(@$coupon->exp_date)) @endphp @endif
	                                                <input type="text" class="form-control required" name="exp_date" id="datepicker1" placeholder="" value="{{old('exp_date', @$exp_date)}}">
	                                            </div>
	                                        </div>

											<div class="clearfix"></div>

											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="add_btnm submit-login">
													<input value="{{ @$coupon ? 'Update' : 'Add'}}" type="submit" class="btn btn-default">
												</div>
											</div>
											<!--all_time_sho-->
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>
<script>
	$(document).ready(function(){
		jQuery.validator.addMethod("lessthan100", function(value, element) {
			return this.optional(element) || value < 100;
		});
		jQuery.validator.addMethod("geterthan0", function(value, element) {
			return this.optional(element) || value > 0;
		});
		jQuery.validator.addMethod("greaterThanStartDate", function(value, element) {
            if($('#datepicker').val() == ""){
                return true;
            } else {
                var date = $('#datepicker').val();
                return this.optional(element) || new Date(value) > new Date(date);
            }
		}, "This has to be greater than start date");
		
		$("#myform").validate({
			rules: {
				discount_amount : {number: true, lessthan100: true,geterthan0: true},
				coupon_code 	: 'alphanumeric',
			},
			messages : {
				coupon_code 	: {
					required 		: "Please enter coupon code.",
					alphanumeric 	: "Please enter alphanumeric."
				},
				exp_date 		: {
					required		: "Please enter expiry date.",
				},
				discount_amount : {
					required 		: "Please enter discount amount.",
					number 			: "Please enter number.",
					lessthan100		: "Please enter a number less than 100",
					geterthan0		: "Please enter a number geter than 0"
				},
				coupon_type 	: "Please select coupon type.",
	        },
	        errorPlacement: function (error, element)
	        {
	            // toastr.error(error.text());
                error.insertAfter(element);
	        }
		});
	});
</script>
<script>
    $(function() {
        // $("#datepicker,#datepicker1").datepicker({
        // 	dateFormat: "dd-mm-yy",
        //  	defaultDate: new Date(),
        //  	minDate: 0
        // });
        $("#datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd-mm-yy",
            minDate: new Date(),
            onClose: function (selectedDate, inst) {
                let minDate = new Date(selectedDate);
                minDate.setDate(minDate.getDate());
                var selectedDate = $('#datepicker').datepicker('getDate');
                selectedDate.setDate(selectedDate.getDate()+1);
                $("#datepicker1").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#datepicker1").datepicker({
            minDate: '+1',
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd-mm-yy",
            onClose: function (selectedDate, inst) {
                // var selectedDate = $('#datepicker1').datepicker('getDate');
                // if(selectedDate==''|| selectedDate==null || selectedDate==undefined){

                // }else{
                //     selectedDate.setDate(selectedDate.getDate()-1);
                //     $("#datepicker").datepicker("option", "maxDate", selectedDate);
                // }
            }
        });
        // var date1 = $("#datepicker").val();
        // var date2 = $("#datepicker1").val();
        // $("#datepicker").datepicker().datepicker("setDate", date1);
        // $("#datepicker1").datepicker().datepicker("setDate", date2);
    });
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
