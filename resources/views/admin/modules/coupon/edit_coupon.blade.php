@extends('admin.layouts.app')
@section('title', 'NearO | Admin | Coupon Update')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
<!-- Start content -->
<div class="content">
	<div class="container">
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 class="pull-left page-title">Edit Coupon</h4>
				<div class="submit-login no_mmg pull-right">
					<a href="{{ route('admin.coupon') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
										<form id="myform" method="post" action="{{ route('admin.coupon.update')}}">
											@csrf
											<input type="hidden" name="id" value="{{ $coupon->id }}">
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Coupon Code</label>
													<input type="text" name="coupon_code" class="form-control required" id="coupon_code" value="{{ $coupon->coupon_code }}">
												</div>
											</div>

											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Coupon Type </label>
													<select class="form-control newdrop required" name="coupon_type" id="coupon_type">
														<option value="">Choose Type</option>
														<option value="F" @if($coupon->coupon_type == 'F') selected @endif>Flat</option>
														<option value="P" @if($coupon->coupon_type == 'P') selected @endif>Percentage</option>
													</select>
												</div>
											</div>

											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Discount Amount</label>
													<input type="text" name="discount_amount" class="form-control required" id="discount_amount" value="{{ $coupon->discount_amount }}">
												</div>
											</div>

											<div class="clearfix"></div>
											<input type="hidden" name="old_date" id="old_date" value="{{ date('d-m-Y', strtotime($coupon->exp_date)) }}">
											<div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
	                                            <div class="your-mail">
	                                                <label for="exampleInputEmail1">Expairy Date</label>
	                                                <input type="text" class="form-control required" name="exp_date" id="datepicker" placeholder=""  value="{{ date('d-m-Y', strtotime($coupon->exp_date)) }}">
	                                            </div>
	                                        </div>

											<div class="clearfix"></div>

											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="add_btnm submit-login">
													<input value="Update" type="submit" class="btn btn-default">
												</div>
											</div>
											<!--all_time_sho-->
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>


<script>
	$(document).ready(function(){
		jQuery.validator.addMethod("greaterThanStartDate", function(value, element) {
            if($('#datepicker').val() == ""){
                return true;
            } else {
                var date = $('#datepicker').val();
                return this.optional(element) || new Date(value) > new Date(date);
            }
		}, "This has to be greater than start date");
		$("#myform").validate({
			rules: {
				discount_amount : 'number',
				coupon_code 	: 'alphanumeric',
			},
			messages : {
				coupon_code 	: {
					required 		: "Please enter coupon code.",
					alphanumeric 	: "Please enter alphanumeric."
				},
				exp_date 		: {
					required		: "Please enter expiry date.",
				},
				discount_amount : {
					required 		: "Please enter discount amount.",
					number 			: "Please enter number."
				},
				coupon_type 	: "Please select coupon type.",
	        },
	        errorPlacement: function (error, element)
	        {
	            // toastr.error(error.text());
                error.insertAfter(element);
	        }
		});
	});
</script>
<script>
    $(function() {
        // $("#datepicker").datepicker({
        // 	dateFormat: "dd-mm-yy",
        //  	defaultDate: new Date(),
        //  	minDate: 0
        // });
        // var date = $("#old_date").val();
        // $("#datepicker").datepicker().datepicker("setDate", date);
		$("#datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd-mm-yy",
            minDate: new Date(),
            onClose: function (selectedDate, inst) {
                let minDate = new Date(selectedDate);
                minDate.setDate(minDate.getDate());
                var selectedDate = $('#datepicker').datepicker('getDate');
                selectedDate.setDate(selectedDate.getDate()+1);
                $("#datepicker1").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#datepicker1").datepicker({
            minDate: '+1',
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd-mm-yy",
            onClose: function (selectedDate, inst) {
                // var selectedDate = $('#datepicker1').datepicker('getDate');
                // if(selectedDate==''|| selectedDate==null || selectedDate==undefined){

                // }else{
                //     selectedDate.setDate(selectedDate.getDate()-1);
                //     $("#datepicker").datepicker("option", "maxDate", selectedDate);
                // }
            }
        });
    });
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
