@extends('admin.layout.auth')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<style type="text/css" media="screen">
.error{
	color:#f00 !important;
}	
</style>
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Buyer Membership Plan</h4>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="panel panel-default show_edit_form" style="display: none;">
									<div class="panel-body table-rep-plugin">

										<form method="post" action="{{ route('admin.buyer.membership.update.plan') }}" id="myform">
											{{csrf_field()}}
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<h3>Edit plan price</h3>
												<div class="your-mail">
													<label for="exampleInputEmail1">Price</label>
													<input class="form-control required" id="price" name="price" placeholder="Price" value="" type="text">
													<input type="hidden" name="plan_id" id="plan_id">
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												<div class="add_btnm" style="width:auto; margin-top:15px;">
													<input value="Update" class="btn btn-primary" type="submit">
												</div>
											</div>
										</form>
									</div>
								</div>
								<div class="col-md-12 dess5">
									<i class="fa fa-pencil-square-o cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">Edit</span></i>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Plan</th>
													<th>Duration</th>
													<th>Price</th>
													{{-- <th>Status</th> --}}
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@foreach($plan as $row)
												<tr>
													<td>{{ $row->plan }}</td>
													<td>{{ $row->duration }} months</td>
													<td>{{ $row->price }}</td>
													{{-- <td>@if($row->status == 'A') Active @elseif($row->status == 'I') Inactive @endif</td> --}}
													<td><a href="javascript:void(0)" data-price="{{ $row->price }}" data-id="{{ $row->id }}" class="edit_plan"><i class="fa fa-pencil-square-o " aria-hidden="true"></i></a></td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<form method="post" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
	var confirm = window.confirm('Are you want to delete this plan ?');
	if(confirm){
		$("#destroy").attr('action',val);
		$("#destroy").submit();
	}
 }
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.edit_plan').click(function(event) {
			var id = $(this).data('id');
			var price = $(this).data('price')
			$('#price').val(price)
			$('#plan_id').val(id)
			$('.show_edit_form').show();
		});
		$('#myform').validate();
	});
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection