@extends('admin.layouts.app')
@section('title', 'Netbhe| Admin |Details')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->



            <div class="col-sm-12">
                <h4 class="pull-left page-title">Tool 360 Evaluation Details</h4>
                <div class="submit-login no_mmg pull-right">
                    <a href="{{ route('tool-360-evaluation.edit',[$details->id]) }}" title="Edit">
                        <button type="button" class="btn btn-default">Edit</button>
                    </a>
                    <a href="{{ route('tool-360-evaluation.index') }}" title="Back">
                        <button type="button" class="btn btn-default">Back</button>
                    </a>
                </div>              
            </div>




          
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        
                        <div class="panel-body">
                            <div class="row">
                                
                                <div class="col-lg-12 col-md-12">
                                    <h4 class="pull-left page-title">Details</h4>
                                    <div class="order-detail">
                                        <!--order-detail start-->
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="order-id">
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Question :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <span>{{ $details->title }}</span>
                                                </div>
                                                

                                                <div class="clearfix"></div>
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 no_padd_lft">
                                                    <p>Answer :</p>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
                                                    <strong>                                                       
                                                        
                                                        @foreach($details->get360Details as $key => $rw)
                                                           <b>{{ $key+1 }} .</b> {{ $rw->answer }} </br>
                                                        @endforeach

                                                                                                             
                                                    </strong>
                                                </div>

                                            </div>
                                        </div>
                                        

                                    </div>
                                    <!--order-detail end-->
                                </div>
                                <div class="col-md-12">
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            
                            
                            
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Row -->
    </div>
    <!-- container -->
</div>
<!-- content -->
</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
<!-- Right Sidebar -->
<!-- /Right-bar -->
</div>
<script>
    function submitMy(id){
      // alert(id);
      $('#profid').val(id);
      $('#myForm2').submit();
    }
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection