@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Add post')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Add Blog</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.blog.index') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
				</div>
			</div>
			@include('admin.includes.error')
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
										<form id="myform" method="post" action="{{ route('admin.blog.store') }}" enctype="multipart/form-data">
											{{ csrf_field() }}


											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Post Title *</label>
													<input type="text" class="form-control required" id="post_title" placeholder="Post Title" name="post_title">
												</div>
											</div>

											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Category Name *</label>
													<select name="cat_id" class="form-control newdrop required">
														<option value="">Select</option>
														@foreach($category as $row)
															<option value="{{$row->id}}">{{$row->name}}</option>
														@endforeach
													</select>
												</div>
											</div>



											<div class="clearfix"></div>

											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">You tube video link </label>
													<input type="url" class="form-control" placeholder="You tube video link" id="youtube" name="you_tube_video">
													<span id="ytlInfo" class="chck_eml_rd"></span>

												</div>
											</div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Show form in Blog *</label>
													<select name="form_show" class="form-control newdrop required">
														<option value="Y">Yes</option>
														<option value="N" selected>No</option>
													</select>
												</div>
											</div>

											<div class="clearfix"></div>

											<div class="col-lg-6 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Image * (Recommended size for image 1280 * 720px)</label>
													<input type="file" class="form-control required" id="file" placeholder="Image" name="image" accept="image/*">
												</div>
											</div>

											<div class="clearfix"></div>
											<!--all_time_sho-->
											<div class="all_time_sho">
												<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Description *</label>
														{{-- <textarea placeholder="Description" id="ckEditor" name="description"rows="3" class="short_description form-control message required"></textarea> --}}
														<textarea placeholder="Description" id="description" name="description"rows="3" class="short_description form-control message required"></textarea>
													</div>
													<div class="clearfix"></div>
													<p class="error_1" id="cntnt"></p>
													<div class="clearfix"></div>
												</div>
                                            </div>

                                            <div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Meta Title *</label>
													<input type="text" class="form-control required" placeholder="Post Title" name="meta_title">
												</div>
											</div>
                                            <div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Meta Description *</label>
													<input type="text" class="form-control required" placeholder="Post Description" name="meta_description">
												</div>
											</div>
                                            <div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Meta Keywords *</label>
													<input type="text" class="form-control required" placeholder="Post Keywords" name="meta_keyword">
												</div>
											</div>

											<div class="clearfix"></div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="submit-login add_btnm">
													<input value="Add" type="submit" class="btn btn-default">
												</div>
											</div>
											<!--all_time_sho-->
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>

{{-- <style>
    .cke_editable .span[lang] {
        font-style: unset !important;
    }
</style>
<script src="{{ asset('public/admin/plugins/ckeditor/ckeditor.js') }}"></script> --}}
<script>
	$(document).ready(function(){
        $('#myform').validate();

        // var ckEditor = document.getElementById('ckEditor');
        // if (ckEditor != undefined && ckEditor != null) {
        //     CKEDITOR.replace('ckEditor', {
        //         language: 'en',
        //         filebrowserBrowseUrl: 'path',
        //         height: 500,
        //         removeButtons: 'Save',
        //         removePlugins: 'image',
        //     });
        //     CKEDITOR.config.pasteFromWordRemoveFontStyles = false;
        //     CKEDITOR.config.allowedContent = true;
        //     CKEDITOR.config.extraAllowedContent = '*(*);*{*}';
        // }
        // function selectFile(fileUrl) {
        //     window.opener.CKEDITOR.tools.callFunction(1, fileUrl);
        // }
        // CKEDITOR.on('dialogDefinition', function (ev) {
        //     var editor = ev.editor;
        //     var dialogDefinition = ev.data.definition;

        //     // This function will be called when the user will pick a file in file manager
        //     var cleanUpFuncRef = CKEDITOR.tools.addFunction(function (a) {
        //         $('#ck_file_manager').modal('hide');
        //         CKEDITOR.tools.callFunction(1, a, "");
        //     });
        //     var tabCount = dialogDefinition.contents.length;
        //     for (var i = 0; i < tabCount; i++) {
        //         var browseButton = dialogDefinition.contents[i].get('browse');
        //         if (browseButton !== null) {
        //             browseButton.onClick = function (dialog, i) {
        //                 editor._.filebrowserSe = this;
        //                 var iframe = $('#ck_file_manager').find('iframe').attr({
        //                     src: editor.config.filebrowserBrowseUrl + '&CKEditor=body&CKEditorFuncNum=' + cleanUpFuncRef + '&langCode=en'
        //                 });
        //                 $('#ck_file_manager').appendTo('body').modal('show');
        //             }
        //         }
        //     }

        // });
	    tinyMCE.init({
        mode : "specific_textareas",
        editor_selector : "short_description",
        height: '420px',
        plugins: [
          'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
            'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
            'save table contextmenu directionality emoticons template textcolor'
        ],
        content_style:
        "body {font-family: Roboto; }",
        relative_urls : false,
        remove_script_host : false,
        convert_urls : true,
        toolbar: 'insertfile undo redo | styleselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor emoticons',
        images_upload_url: '{{ URL::to('storage/app/public/uploads/content_image/') }}',
        images_upload_handler: function(blobInfo, success, failure) {
            var formD = new FormData();
            formD.append('file', blobInfo.blob(), blobInfo.filename());
            formD.append( "_token", '{{csrf_token()}}');
            $.ajax({
                url: '{{ route('admin.artical.img.upload') }}',
                data: formD,
                type: 'POST',
                contentType: false,
                cache: false,
                processData:false,
                dataType: 'JSON',
                success: function(jsn) {
                    if(jsn.status == 'ERROR') {
                        failure(jsn.error);
                    } else if(jsn.status == 'SUCCESS') {
                        success(jsn.location);
                    }
                }
            });
        },
    });
    });

	$("#myform").submit(function (event) {
	    if(tinyMCE.get('description').getContent()==""){
	    	event.preventDefault();
	    	$("#cntnt").html("Description field is required");
	    }
	    else{
	    	$("#cntnt").html("");
	    }
	});
	function ytVidId(url) {
            var p = /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
            return (url.match(p)) ? RegExp.$1 : false;
        }

        $('#youtube').bind("change", function() {
            var url = $(this).val();
            if (ytVidId(url) !== false) {
                $('#ytlInfo').html('');
            } else {
                $("#ytlInfo").html('Invalid youtube link.');
                $('#youtube').val('');
            }
        });
</script>
<style>
	.error {
		color: red !important;
	}
	.error_1 {
		color: red !important;
	}
	.chck_eml_rd{
        color: red;
        font-weight: bold;
        font-size: 14px;
    }
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
