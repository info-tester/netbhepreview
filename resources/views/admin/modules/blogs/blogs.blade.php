@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Blogs')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">All Blogs </h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.blog.create') }}" title="Back"><button type="button" class="btn btn-default">Add</button></a>
					</div>
				</div>
			</div>
			<div class="row">

				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">

								<div class="panel panel-default">
									<div class="panel-body table-rep-plugin">
										<form method="get" action="{{ route('admin.blog.index')}}" >

											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="your-mail">
													<input class="form-control" id="keyword" name="keyword" placeholder="Keyword" value="{{@$key['keyword']}}" type="text">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="your-mail">
													<select class="form-control newdrop " name="cat_id" id="cat_id">
													<option value="">Select Category</option>
													@foreach($category as $row)
													<option value="{{$row->id}}" @if($row->id == @$key['cat_id']) selected @endif>{{$row->name}}</option>
													@endforeach
													</select>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<select class="form-control newdrop required" name="status" id="status">
													<option value="">Status</option>
													<option value="A" @if(@$key['status'] == 'A') selected @endif>Active</option>
													<option value="I" @if(@$key['status'] == 'I') selected @endif>Inactive</option>
													</select>
												</div>
											</div>
											{{-- <div class="clearfix"></div> --}}
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="submit-login add_btnm {{-- pull-right --}}" style="width:auto; margin-top:0px;">
													<input value="Search" class="btn btn-default" type="submit">
												</div>
											</div>
										</form>
									</div>
								</div>


								<div class="col-md-12 dess5">
									<i class="fa fa fa-eye cncl"  aria-hidden="true"> <span class="cncl_oopo">View</span></i>
									<i class="fa fa-trash-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
									<i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active</span></i>
									<i class="fa fa-square cncl" aria-hidden="true"> <span class="cncl_oopo">Not show contact from</span></i>
									<i class="fa fa-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Show contact from</span></i>
									<i class="fa fa-envelope-o cncl" aria-hidden="true"> <span class="cncl_oopo">View contact List</span></i>
									<i class="fa fa-times cncl" style="border: none;" aria-hidden="true" > <span class="cncl_oopo">Inactive</span></i>


								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Image</th>
													<th>Title</th>
													<th>Category</th>
													<th>Description</th>
													<th>Posted By</th>
													<th>Post Date</th>
													<th>Status</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@foreach($blogs as $detail)
												<tr>
													<td>
														@if(@$detail->image)
															<img src="{{ url('storage/app/public/uploads/blog_image/'.@$detail->image) }}" alt="Image" style="height: 50px; width: 50px;">
														@else
															<img src="{{ url('public/images/no_img.png') }}" alt="Image" style="height: 50px; width: 50px;">
														@endif
													</td>
													<td>{{ @$detail->title }}</td>
													<td>{{ $detail->blogCategoryName->name }}</td>
													<td><?= substr(strip_tags(@$detail->desc),0,100) ?>...</td>
													<td>{{ @$detail->posted_by !=0 ? ( @$detail->postedBy->nick_name ? @$detail->postedBy->nick_name : @$detail->postedBy->name) : @$detail->adminPostedBy->name   }}</td>
													<td>{{date('d-M-Y', strtotime(@$detail->post_date))}}</td>
													<td>
														@if(@$detail->status == 'A') <label class="label label-success">Active</label> @elseif(@$detail->status == 'I') <label class="label label-danger">Inactive</label> @endif
													</td>
													<td>
														{{-- @if(@$detail->admin_post_id !=0 && @$detail->admin_post_id==Auth::guard('admin')->user()->id) --}}
                                                        {{-- @if(Auth::guard('admin')->user()->user_type == 'A')
														<a href="{{ route('admin.blog.edit',['id'=>@$detail->id]) }}" title="edit" > <i class="fa fa fa-pencil" aria-hidden="true"></i></a>
                                                        @elseif(@$detail->admin_post_id !=0 && @$detail->admin_post_id==Auth::guard('admin')->user()->id) --}}
                                                        <a href="{{ route('admin.blog.edit',['id'=>@$detail->id]) }}" title="edit"> <i class="fa fa fa-pencil" aria-hidden="true"></i></a>
                                                        {{-- @endif --}}
														{{-- @endif --}}
														<a href="{{ url('admin/blog-details',[@$detail->id]) }}" title="View" > <i class="fa fa fa-eye" aria-hidden="true"></i></a>

                                                        @if(Auth::guard('admin')->user()->user_type == 'A')
														<a href="javascript:void(0)" onclick="if (confirm('Do you really want to delete this blog post?')) { location.href='{{route('admin.blog.destroy',[@$detail->id])}}' } else { return false; };" title="Delete"> <i class="fa fa-trash-o delet" aria-hidden="true"></i></a>
														@elseif(@$detail->admin_post_id !=0 && @$detail->admin_post_id==Auth::guard('admin')->user()->id)
                                                        <a href="javascript:void(0)" onclick="if (confirm('Do you really want to delete this blog post?')) { location.href='{{route('admin.blog.destroy',[@$detail->id])}}' } else { return false; };" title="Delete"> <i class="fa fa-trash-o delet" aria-hidden="true"></i></a>
                                                        @endif

														@if($detail->status == "I")
															<a href="{{ url('admin/blog-status',[@$detail->id]) }}" title="Active" onclick="return confirm('Do you really want to active this blog post?');"> <i class="fa fa-check delet" aria-hidden="true"></i></a>
														@elseif($detail->status == "A")
															<a href="{{ url('admin/blog-status',[@$detail->id]) }}" title="Inactive" onclick="return confirm('Do you really want to inactive this blog post?');"> <i class="fa fa-times delet" aria-hidden="true"></i></a>
														@endif
                                                        @if($detail->is_show_contact_from == "Y")
                                                        <a href="{{route('admin.blog.contact.show',[@$detail->id])}}" title="Not show contact from" > <i class="fa fa-square" aria-hidden="true"></i></a>
                                                        @else
														<a href="{{route('admin.blog.contact.show',[@$detail->id])}}" title="Show contact from" > <i class="fa fa-square-o" aria-hidden="true"></i></a>
                                                        @endif
														<a href="{{route('admin.blog.contact.list',[@$detail->id])}}" title="View contact List" > <i class="fa fa-envelope-o" aria-hidden="true"></i></a>
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<form method="post" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
	var confirm = window.confirm('Do you want to delete this blog?');
	if(confirm){
		$("#destroy").attr('action',val);
		$("#destroy").submit();
	}
 }
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
