@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Edit Blog Category')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Edit Blog Category</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.blog.category.index') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
				</div>
			</div>
			@include('admin.includes.error')
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
										<form id="myform" method="post" action="{{ route('admin.blog.category.update') }}">
											@csrf
											<input type="hidden" name="id" value="{{@$category->id}}">
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Category Name *</label>
													<input type="text" name="name" class="form-control required" id="name" placeholder="Category Name" value="{{ @$category->name }}">
												</div>
											</div>

											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Meta title *</label>
													<input type="text" name="meta_title" class="form-control required" id="meta_title" placeholder="Meta title" value="{{ @$category->meta_title }}">
												</div>
											</div>

											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Meta keyword *</label>
													<input type="text" name="meta_keyword" class="form-control required" id="meta_keyword" placeholder="Meta keyword" value="{{ @$category->meta_keyword }}">
												</div>
											</div>

											<div class="clearfix"></div>

											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Meta description *</label>
													<input type="text" name="meta_description" class="form-control required" id="meta_description" placeholder="Meta description" value="{{ @$category->meta_description }}">
												</div>
											</div>
											
											
											<div class="clearfix"></div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="submit-login add_btnm">
													<input value="Update" type="submit" class="btn btn-default">
												</div>
											</div>
											
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>
<script>
	$(document).ready(function(){
		$("#myform").validate();
	});
</script>
<style>
	.error{
		color: red !important;
	}
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection