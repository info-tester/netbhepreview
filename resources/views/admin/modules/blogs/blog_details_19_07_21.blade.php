@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Blog Details')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<div class="content">
		<div class="container">


			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">{{$blog_details->title}} Details </h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.blog.index') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
				</div>
			</div>
			<div class="row">

				<div class="col-md-12">
					<div class="panel panel-default for_editor_text">
						<div class="panel-body table-rep-plugin">
							<div class="row">


                           <div class="blog_area_nn01" style="padding-top: 70px !important;">
                            	{{-- <img src="{{ url('storage/app/public/uploads/blog/'.@$blog_details->image) }}" alt="{{ @$blog_details->original_name }}" class="img-responsive" width="100"> --}}
                                <div class="blog_image_prbbon">{{$blog_details->blogCategoryName->name}}</div>
                            </div>

                            @if(@$blog_details->you_tube_video)
	                            <div class="blog_area_nn01">
	                            	<iframe id="videoObject" width="600" height="300" src="https://www.youtube.com/embed/{{@$blog_details->you_tube_video}}" frameborder="2" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	                            </div>
                            @else
                            <div class="blog_area_nn01">
                                <img src="{{ URL::to('storage/app/public/uploads/blog_image').'/'.@$blog_details->image }}" alt="">
                            </div>
                            @endif
                            <div class="blog_area_nn01" style="text-align: left !important;">
                            	<h4>{{$blog_details->title}}</h4>
                                <h5>
                                <strong><img src="{{url('public/images/rusr.png')}}" alt=""> By: {{-- {{$blog_details->postedBy->name}} --}}
                                	{{ @$blog_details->posted_by !=0 ? ( @$blog_details->postedBy->nick_name ? @$blog_details->postedBy->nick_name : @$blog_details->postedBy->name ) : @$blog_details->adminPostedBy->name   }}
                                </strong>
                                <strong><img src="{{url('public/images/calender.png')}}" alt=""> {{ date('d-M-Y', strtotime(@$blog_details->created_at)) }} </strong>
                                </h5>
                            	<p>{!!@$blog_details->desc!!}</p>
                            </div>


                            <div class="clearfix"></div>



							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<form method="post" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
