@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Blog Categories')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Blog categories</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.blog.category.create') }}" title="Create"><button type="button" class="btn btn-default">Add</button></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								@if(Session::has('error'))
								    <div class="alert alert-danger"><span class="glyphicon glyphicon-ok"></span><em> {{ Session::get('error') }}</em></div>
								@endif
								@if(Session::has('success'))
								    <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {{ Session::get('success') }}</em></div>
								@endif
								<div class="panel panel-default">
									<div class="panel-body table-rep-plugin">
										<form method="get" action="{{ route('admin.blog.category.index')}}" >
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="your-mail">											
													<input class="form-control" id="category_name" name="keyword" placeholder="Keyword" value="{{@$key['keyword']}}" type="text">
												</div>
											</div>
											
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<select class="form-control newdrop required" name="status" id="status">
													<option value="">Status</option>
													<option value="I" @if(@$key['status'] == "I") selected @endif>Inactive</option>
													<option value="A" @if(@$key['status'] == "A") selected @endif >Active</option>
													</select>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="submit-login add_btnm pull-right" style="width:auto; margin-top:0px;">
													<input value="Search" class="btn btn-default" type="submit">
												</div>
											</div>
										</form>
									</div>
								</div>
								<div class="col-md-12 dess5">
									<i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
									<i class="fa fa-trash-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
									<i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active</span></i>
									<i class="fa fa-times cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">Inactive</span></i>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Category</th>
													
													<th>Status</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@foreach($category as $detail)
												<tr>
													<td>{{ $detail->name }}</td>
													
													<td>@if(@$detail->status == 'A') <label class="label label-success">Active</label> @elseif(@$detail->status == 'I') <label class="label label-danger">Inactive</label> @endif
													</td>
													<td>
														<a href="{{ route('admin.blog.category.edit',[$detail->id]) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
														<a href="javascript:void(0)" onclick="if (confirm('Do you really want to delete this category ?')) { location.href='{{route('admin.blog.category.destroy',[$detail->id])}}' } else { return false; };" title="Delete"> <i class="fa fa-trash-o delet" aria-hidden="true"></i></a>
														@if($detail->status == "I")
															<a href="{{ route('admin.blog.category.status',[$detail->id]) }}" title="Active" onclick="return confirm('Do you really want to active this category?');"> <i class="fa fa-check delet" aria-hidden="true"></i></a>
														@elseif($detail->status == "A")
															<a href="{{ route('admin.blog.category.status',[$detail->id]) }}" title="Inactive" onclick="return confirm('Do you really want to inactive this category?');"> <i class="fa fa-times delet" aria-hidden="true"></i></a>
														@endif
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection