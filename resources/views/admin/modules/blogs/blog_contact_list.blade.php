@extends('admin.layouts.app')
@section('title', 'Netbhe.com | Blogs')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">All Blog Contact List</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.blog.index') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
				</div>
			</div>
			<div class="row">

				<div class="col-md-12">
				<div class="panel panel-default">
						<div class="panel-body">
							<p><strong>Blog Title: </strong>{{@$blog->title}}</p>
							<p><strong>Blog Category: </strong>{{@$blog->blogCategoryName->name}}</p>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Name</th>
													<th>Email</th>
												</tr>
											</thead>
											<tbody>
												@foreach(@$contactList as $detail)
												<tr>
													<td>{{ @$detail->name }}</td>
													<td>{{ $detail->email }}</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
