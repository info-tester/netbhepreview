@extends('admin.layout.auth')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">View Blog Comments </h4>
					<div class="submit-login no_mmg pull-right">
						
					</div>
				</div>
			</div>
			<div class="row">
				
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
                            
                            
                            <!--==================blog=================-->
                            <div class="blog_area_nn01">
                            	<img src="http://computer24-pc/proqrx/public/images/6.png" alt="">
                                <div class="blog_image_prbbon">Category 1</div>
                            </div>
                            
                            <div class="blog_area_nn01">
                            	<h4>New blog</h4>
                                <h5>
                                <strong><img src="http://computer24-pc/proqrx/public/images/blog_man.png" alt=""> By aniket</strong>
                                <strong><img src="http://computer24-pc/proqrx/public/images/blog_calender.png" alt=""> 19th Sep 2018</strong>
                                </h5>
                            	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum been standard dummy text ever since the 1500s, when an Lorem Ipsum is simply dummy text the printing and typesettingindustry lorem Ipsum has been the industry dummy text ever since the, when an Lorem Ipsum is the simply dummy text the printing and typesettingindustry lorem Ipsum has been the industry dummy text ever since the,</p>
                            </div>
                            
                            <div class="com_pro_box u01">
                                <div class="com_img">
                                    <img src="http://computer24-pc/proqrx/public/images/logo001.png" alt="">
                                </div>
                                
                                <div class="com_detls">
                                    <h4>Ankan Das</h4>
                                    <a href="javascript:void();" title="Active"><i class="fa fa-check-circle" aria-hidden="true"></i></a>
                                    <a href="javascript:void();" title="Delete Comment"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    <h5>21/09/2018, 06:59:47 PM</h5>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum been standard dummy text ever since the 1500s, when an Lorem Ipsum is simply dummy</p>
                               </div>
                            </div>
                            
                            <div class="com_pro_box u02">
                                <div class="com_img">
                                    <img src="http://computer24-pc/proqrx/public/images/logo001.png" alt="">
                                </div>
                                
                                <div class="com_detls">
                                    <h4>Ankan Das</h4>
                                    <a href="javascript:void();" title="Active"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="javascript:void();" title="Delete Comment"><i class="fa fa-check-circle" aria-hidden="true"></i></a>
                                    <h5>21/09/2018, 06:59:47 PM</h5>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum been standard dummy text ever since the 1500s, when an Lorem Ipsum is simply dummy</p>
                               </div>
                            </div>
                            <!--==================blog=================-->
                            
                            
                            <div class="clearfix"></div>
                            
                            
								{{-- <div class="panel panel-default">
									<div class="panel-body table-rep-plugin">
										<form method="post" action="{{ url('admin/blog-comments',[Session::get('blog_id')]) }}" >
											{{csrf_field()}}
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="your-mail">											
													<input class="form-control" id="keyword" name="keyword" placeholder="Keyword" value="{{@$key['keyword']}}" type="text">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<select class="form-control newdrop required" name="status" id="status">
													<option value="">Status</option>
													<option value="AA" @if(@$key['status'] == "AA") {{ "selected" }} @endif>Awaiting Approval</option>
													<option value="A" @if(@$key['status'] == 'A') selected @endif>Active</option>
													<option value="I" @if(@$key['status'] == 'I') selected @endif>Inactive</option>
													</select>
												</div>
											</div>
											{{-- <div class="clearfix"></div> --}}
{{-- 											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												<div class="add_btnm pull-right" style="width:auto; margin-top:15px;">
													<input value="Search" class="btn btn-primary" type="submit">
												</div>
											</div> --}}
										{{-- </form>
									</div> --}}
								{{-- </div>
								<div class="col-md-12 dess5">
									
									
									<i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active / Approve</span></i>
									<i class="fa fa-times cncl" aria-hidden="true" > <span class="cncl_oopo">Inactive</span></i> --}}
									
								{{-- </div> --}} 
								{{-- <div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													
													<th>Comment</th>
													<th>Commented By</th>
													<th>Status</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@foreach($blog_comments as $detail)
												<tr>
													<td>{!! @$detail->comment  !!}</td>
													<td>{{ @$detail->commentedby->first_name }} {{ @$detail->commentedby->last_name }}</td>
													
													<td>
														@if(@$detail->status == 'A')
														<label class="label label-success">{{ 'Active' }}</label>
														@elseif(@$detail->status == 'I')
														<label class="label label-danger">{{ 'Inactive' }}</label>
														@elseif(@$detail->status == 'AA')
														<label class="label label-warning">{{ 'Awaiting Approval' }}</label>
														@endif
													</td>
													<td>
														@if(@$detail->status == "I")
															<a href="{{ url('admin/blogs-comment-status',[@$detail->id]) }}" title="Active" onclick="return confirm('Do you want to active this blog?');"> <i class="fa fa-check-circle delet" aria-hidden="true"></i></a>
														@elseif($detail->status == "A")
															<a href="{{ url('admin/blogs-comment-status',[@$detail->id]) }}" title="Inactive" onclick="return confirm('Do you want to inactive this blog?');"> <i class="fa fa-times delet" aria-hidden="true"></i></a>
														@elseif(@$detail->status == 'AA')
														<a href="{{ url('admin/blogs-comment-status',[$detail->id]) }}" title="Approve" onclick="return confirm('Do you want to approve this comment?');"> <i class="fa fa-check-circle delet" aria-hidden="true"></i></a>	
														@endif
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div> --}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<form method="post" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
	var confirm = window.confirm('Do you want to delete this blog?');
	if(confirm){
		$("#destroy").attr('action',val);
		$("#destroy").submit();
	}
 }
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection