@extends('admin.layouts.app')
@section('title', 'netbhe.com | Admin | Edit Blog')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Edit Blog</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.blog.index') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
				</div>
			</div>
			@include('admin.includes.error')
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
										<form id="myform" method="post" action="{{ route('admin.mail.template.edit', ['id' => $template->id]) }}" enctype="multipart/form-data">
											@csrf
											<div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Template name</label>
													<label class="form-control required" >{{ @$template->name }}</label>
												</div>
											</div>
											<div class="col-lg-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Template Subject *</label>
													<input type="text" class="form-control required" value="{{ @$template->subject }}" name="subject">
												</div>
											</div>

                                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Mail Content *</label>
                                                    <textarea placeholder="Description" id="description" name="content"rows="3" class="short_description form-control message required">{!! @$template->content !!}</textarea>
                                                </div>
                                                <div class="clearfix"></div>
                                                <p class="error_1" id="cntnt"></p>
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="your-mail">
                                                    <label>You can use the below parameters in Subject and Content field.</label>
                                                    <p>
                                                        <code style="white-space: pre-wrap;">{!! @$template->param_helper !!}</code>
                                                    </p>
                                                </div>
                                            </div>

											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="add_btnm">
													<input value="Update" type="submit" class="btn btn-primary" style="margin-top: 20px;">
												</div>
											</div>
											<!--all_time_sho-->
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>

<script>


	$(document).ready(function(){
		$('#myform').validate();
		tinyMCE.init({
			//selector: "textarea",
	        mode : "specific_textareas",
	        editor_selector : "short_description",
	        height: '500px',
	        plugins: [
	          'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
	          'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
	          'save table contextmenu directionality emoticons template paste textcolor'
	        ],
	        //entity_encoding: 'raw',

	        forced_root_block : "",
		    force_br_newlines : true,
		    force_p_newlines : false,

		    remove_trailing_brs: false,


	        relative_urls : false,
	        remove_script_host : false,
	        convert_urls : false,
	        toolbar: 'insertfile undo redo | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | preview | forecolor backcolor emoticons',
            images_upload_url: '{{ URL::to('storage/app/public/uploads/content_image/') }}',
            images_upload_handler: function(blobInfo, success, failure) {
                var formD = new FormData();
                formD.append('file', blobInfo.blob(), blobInfo.filename());
                formD.append( "_token", '{{csrf_token()}}');
                $.ajax({
                    url: '{{route("admin.artical.img.upload")}}',
                    data: formD,
                    type: 'POST',
                    contentType: false,
                    cache: false,
                    processData:false,
                    dataType: 'JSON',
                    success: function(jsn) {
                        if(jsn.status == 'ERROR') {
                            failure(jsn.error);
                        } else if(jsn.status == 'SUCCESS') {
                            success(jsn.location);
                        }
                    }
                });
            },

        });
	    $("#myform").submit(function (event) {
		    if(tinyMCE.get('description').getContent()==""){
		    	event.preventDefault();
		    	$("#cntnt").html("Description field is required");
		    }
		    else{
		    	$("#cntnt").html("");
		    }
		});
	});
</script>
<style>
	.error {
		color: red !important;
	}
	.error_1 {
		color: red !important;
	}
	.chck_eml_rd{
        color: red;
        font-weight: bold;
        font-size: 14px;
    }
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
