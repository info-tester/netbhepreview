@extends('admin.layouts.app')
@section('title', 'Netbhe | Admin | Email Templates')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Email Templates Management</h4>
				</div>
			</div>
			@include('admin.includes.error')
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 dess5">
									<i class="fa fa-pencil-square-o cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo" >Edit</span></i>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Template Name</th>
													<th>Subject</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@foreach($templates as $row)
												<tr>
													<td>{{ @$row->name }}</td>
													<td>{{ @$row->subject }}</td>
													<td>
														<a href="{{ route('admin.mail.template.edit', ['id' => $row->id]) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

{{-- Modal For Recharge --}}

<div class="modal fade" id="myModal" role="dialog" style="display: none;">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Customer Reacharge</h4>
        </div>
        <div class="modal-body">
        	{{-- <label class="type_label">Inforfamtion of <span id="holder_name"></span></label> --}}
          <div class="row">
          		<div class="col-md-6">

          			<div class="form-group">
	          			<label>Customer Name: <span id="cust_name"></span></label>
	          		</div>
          		</div>

          		<div class="clearfix"></div>
          		<form method="POST" action="{{ route('cust.reacharge') }}" id="my_form" name="my_form">
          			@csrf
          			<input type="hidden" name="customer_id" id="customer_id">
	          		<div class="col-md-6">
	          			<div class="form-group">
		          			<label class="type_label" for="payout">Rechage Amount
		          			<input type="text" class="login_type required digits" placeholder="Recharge Amount" name="amount" id="amount"></label>
		          		</div>
	          		</div>
	          		<div class="col-md-6">
	          			<div class="form-group">
		          			<label class="type_label" for="notes">Notes
		          			<input type="text" class="login_type required" placeholder="Recharge Notes" name="notes" id="notes"></label>
		          		</div>
	          		</div>
	          		<div class="col-lg-12 col-md-12">
	                   	<button type="submit" class="btn btn-info sbmt_clk">Make Recharge</button>
	          		</div>
          		</form>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
