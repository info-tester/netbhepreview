@extends('admin.layout.auth')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Edit Advertisement</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.advertisement.index') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
										<form id="myform" method="post" action="{{ route('admin.advertisement.update',[@$advertisement->id]) }}" enctype="multipart/form-data">
											{{ csrf_field() }}
											{{method_field('PUT')}}
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Ad Type</label>
													Text Ad <input type="radio" name="ad_type" value="T" @if(@$advertisement->ad_type == "T") checked="checked" @endif />
    
    												Image Ad <input type="radio" name="ad_type" value="I" @if(@$advertisement->ad_type == "I") checked="checked" @endif  />
												</div>
											</div>
											<input type="hidden" name="type" id="type" value="{{ @$advertisement->ad_type }}">
											<div class="clearfix"></div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Location</label>
													<select class="form-control newdrop required" name="location_id" id="location_id" disabled>
														<option value="">Select</option>
														@foreach($location as $row)
															<option value="{{ $row->id }}" data-w="{{ $row->width }}" data-h="{{ $row->height }}" @if(@$advertisement->location_id == $row->id) selected @endif >{{ $row->name }}</option>
														@endforeach
													</select>
													<span id="image_size" style="color: red;"><b>Image size must be {{@$img->width}} * {{@$img->height}} pixels</b></span>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 url_section" style = "display: none;">
												<div class="your-mail">
													<label for="exampleInputEmail1">URL</label>
													<input type="text" class="form-control required" id="url" placeholder="https://example.com" name="url" value="{{@$advertisement->url}}">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 ad_title" style="display: none;">
												<div class="your-mail">
													<label for="exampleInputEmail1">Title</label>
													<input type="text" class="form-control required" id="meta_title" placeholder="Title" name="title" value="{{@$advertisement->title}}">
												</div>
											</div>
											
											<div class="clearfix"></div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Expiry Date</label>
													<input type="text" class="form-control sc_date required" placeholder="Select Date" name="exp_date" id="datepicker" readonly="" value="{{date('d/m/Y', strtotime(@$advertisement->exp_date))}}">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 image_alt" style = "display: none;">
												<div class="your-mail">
													<label for="exampleInputEmail1">Image Alt</label>
													<input type="text" class="form-control required" id="image_alt" placeholder="Image Alt" name="image_alt" value="{{@$advertisement->image_alt}}">
												</div>
											</div>
											<input type="hidden" name="height" value="{{@$img->height}}" class="img_height">
											<input type="hidden" name="width" value="{{@$img->width}}" class="img_width">
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 image_section" style = "display: none;">
												<div class="your-mail">
													<label for="exampleInputEmail1">Image</label>
													<input type="file" class="form-control" id="file" placeholder="Image" name="image" accept="image/*">
													<span class="file-name"><img src="{{ url(IMG_PATH['advertisement'].@$advertisement->image) }}" alt="{{ @$advertisement->original_name }}" style="width: 100px; height: 100px;"></span>
												</div>
											</div>
											
											{{-- <input type="text" placeholder="Select Date" class="required sc_date" id="datepicker" readonly="" required="true"> --}}

											<!--all_time_sho-->
											<div class="clearfix"></div>
											<div class="all_time_sho">
												<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 ad_content">
													<div class="your-mail">
														<label for="exampleInputEmail1">Ad Content</label>
														<textarea placeholder="Ad Content" name="google_ads" rows="3" class="form-control message required">{{ @$advertisement->google_ads }}</textarea>
													</div>
												</div>
											</div>
											<div class="clearfix"></div>
											
											{{-- <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Title</label>
													<input type="text" class="form-control required" id="meta_title" placeholder="Title" name="title" value="{{@$advertisement->title}}">
												</div>
											</div>
											
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">URL</label>
													<input type="url" class="form-control required" id="url" placeholder="https://example.com" name="url" value="{{@$advertisement->url}}">
												</div>
											</div>
											
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Location</label>
													<select class="form-control newdrop required" name="location_id" id="location_id">
														<option value="">Select</option>
														@foreach($location as $row)
															<option value="{{ $row->id }}" data-w="{{ $row->width }}" data-h="{{ $row->height }}" @if(@$advertisement->location_id == $row->id) selected @endif >{{ $row->name }}</option>
														@endforeach
													</select>
													<span id="image_size" style="color: red;"></span>
												</div>
											</div>
											<input type="hidden" name="height" value="{{@$img->height}}" class="img_height">
											<input type="hidden" name="width" value="{{@$img->width}}" class="img_width">
											<div class="clearfix"></div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Image</label>
													<input type="file" class="form-control file" id="file" placeholder="Image" name="image" accept="image/*">
													<span class="file-name"><img src="{{ url(IMG_PATH['advertisement'].@$advertisement->image) }}" alt="{{ @$advertisement->original_name }}" style="width: 100px; height: 100px;"></span>
												</div>
											</div>
											
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Expiry Date</label>
													<input type="text" class="form-control sc_date required" placeholder="Select Date" name="exp_date" id="datepicker" readonly=""  value="{{date('d/m/Y', strtotime(@$advertisement->exp_date))}}">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="all_time_sho">
												<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
													<div class="your-mail">
														<label for="exampleInputEmail1">Google Ads</label>
														<textarea placeholder="Google Ads" name="google_ads" rows="3" class="form-control message">{{ @$advertisement->google_ads }}</textarea>
													</div>
												</div>
											</div>
											<div class="clearfix"></div> --}}
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="add_btnm">
													<input value="Update" type="submit" class="btn btn-primary">
												</div>
											</div>
											<!--all_time_sho-->
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>
<script>
	$(document).ready(function(){
		var type = $('#type').val();
        if(type == 'T')
        {
        	$('.ad_content').show();
        	$('.image_alt').hide();
        	$('.image_section').hide();
        	$('.url_section').hide();
        	$('.ad_title').hide();
        	$('#image_size').hide();
        }
        else
        {
        	$('.ad_content').hide();
        	$('.image_alt').show();
        	$('.image_section').show();
        	$('.url_section').show();
        	$('.ad_title').show();
        	$('#location_id').change(function(event) {
        		$('.img_height').val('');
				$('.img_width').val('');
				// find(':selected').attr('data-id')
				// console.log($('#location_id').children('option:selected').data('w'));
				var width = $('#location_id').children('option:selected').data('w');
				var height = $('#location_id').children('option:selected').data('h');
				$('.img_height').val(height);
				$('.img_width').val(width);
				$('#image_size').html('');
				if(width != null) {
					$('#image_size').append("<b>Image size must be "+width+" * "+height+" pixels</b>");
				}
				
			});

        }
		$('input[type="radio"]').click(function(){
	        var inputValue = $(this).attr("value");
	        if(inputValue == 'T')
	        {
	        	$('.ad_content').show();
	        	$('.image_alt').hide();
	        	$('.image_section').hide();
	        	$('.url_section').hide();
	        	$('.ad_title').hide();
	        	$('#image_size').hide();
	        }
	        else
	        {
	        	$('.ad_content').hide();
	        	$('.image_alt').show();
	        	$('.image_section').show();
	        	$('.url_section').show();
	        	$('.ad_title').show();
	        	$('#location_id').change(function(event) {
					// find(':selected').attr('data-id')
					// console.log($('#location_id').children('option:selected').data('w'));
					var width = $('#location_id').children('option:selected').data('w');
					var height = $('#location_id').children('option:selected').data('h');
					$('.img_height').val(height);
					$('.img_width').val(width);
					$('#image_size').html('');
					if(width != null) {
						$('#image_size').append("<b>Image size must be "+width+" * "+height+" pixels</b>");
					}
					
				});

	        }
	    });

		/*$('#location_id').change(function(event) {
			// find(':selected').attr('data-id')
			// console.log($('#location_id').children('option:selected').data('w'));
			$('.img_height').val('');
			$('.img_width').val('');
			var width = $('#location_id').children('option:selected').data('w');
			var height = $('#location_id').children('option:selected').data('h');
			$('.img_height').val(height);
			$('.img_width').val(width);
			$('#image_size').html('');
			if(width != null) {
				$('#image_size').append("<b>Image size must be "+width+" * "+height+" pixels</b>");
			}
			
		});*/
		$( "#datepicker" ).datepicker({
			dateFormat: "dd/mm/yy",
            minDate: 0,
			changeMonth: true,
			changeYear: true
		});
		// $("#datepicker").datepicker().datepicker("setDate", new Date());

		$('.file').change(function() {
		    if ($(this).val()) {
		      	$(".file-name").show();
		      	$('.file-name').html('');
		    }
		});

		$("#myform").validate();
	});
</script>
<style>
	.error {
		color: red !important;
	}
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection