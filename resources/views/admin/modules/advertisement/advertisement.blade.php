@extends('admin.layout.auth')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">View Advertisement</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.advertisement.create') }}" title="Create"><button type="button" class="btn btn-default">Add</button></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="panel panel-default">
									<div class="panel-body table-rep-plugin">
										<form method="get" action="{{ route('admin.advertisement.index')}}" >
											
											<!--<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
														<label for="exampleInputEmail1">Type</label>
												<select class="form-control newdrop">
																		 <option>Choose</option>
																		 <option value="volvo">Sales</option>
																		 <option value="volvo">Sub–admin</option>
																		 <option value="volvo">Support</option>
															 </select>
												</div>
												</div>-->
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="your-mail">											
													<input class="form-control" id="keyword" name="keyword" placeholder="Keyword" value="{{@$key['keyword']}}" type="text">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="your-mail">
													{{-- {{dump($location)}} --}}
													<select class="form-control newdrop " name="location" id="location">
													<option value="">Advertisement Location</option>
													@foreach($location as $row)
													<option value="{{$row->id}}" @if(@$key['location'] == $row->id) selected @endif >{{$row->name}}</option>
													@endforeach
													</select>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="your-mail">
													<select class="form-control newdrop required" name="status" id="status">
													<option value="">Status</option>
													<option value="A" @if(@$key['status'] == "A") selected @endif >Active</option>
													<option value="I" @if(@$key['status'] == "I") selected @endif>Inactive</option>
													</select>
												</div>
											</div>
											{{-- <div class="clearfix"></div> --}}
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												<div class="add_btnm pull-right" style="width:auto; margin-top:15px;">
													<input value="Search" class="btn btn-primary" type="submit">
												</div>
											</div>
										</form>
									</div>
								</div>
								<div class="col-md-12 dess5">
									<i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
									<i class="fa fa-trash-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
									<i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active</span></i>
									<i class="fa fa-times cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">Inactive</span></i>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Type</th>
													<th>Location</th>
													<th>Image</th>
													<th>Ad Content</th>
													{{-- <th>URL</th> --}}
													<th>Expiry Date</th>
													<th>Status</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@foreach($advertisement as $detail)
												<tr>
													<td>
														@if(@$detail->ad_type == "I")
															{{ 'Image' }}
														@elseif(@$detail->ad_type == 'T')
															{{ 'Text' }}
														@endif
													</td>
													<td>{{ $detail->location->name }}</td>
													<td>
														@if(@$detail->image)
															<img src="{{ url(IMG_PATH['advertisement'].@$detail->image) }}" alt="Image" style="height: 50px; width: 50px;">
														{{-- @else
															<img src="{{ url('public/images/no_img.png') }}" alt="Image" style="height: 50px; width: 50px;"> --}}
														@endif
													</td>
													<td>{{ @$detail->google_ads }}</td>
													{{-- <td>{{ $detail->url }}</td> --}}
													<td>{{ niceDate(@$detail->exp_date) }}</td>
													<td>
														@if(@$detail->status == "I")
															{{ 'Inactive' }}
														@elseif(@$detail->status == 'A')
															{{ 'Active' }}
														@endif
													</td>
													<td>
														<a href="{{ route('admin.advertisement.edit',[@$detail->id]) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
														<a href="javascript:void(0)" onclick="deleteCategory('{{route('admin.advertisement.destroy',[@$detail->id])}}')" title="Delete"> <i class="fa fa-trash-o delet" aria-hidden="true"></i></a>
														@if($detail->status == "I")
															<a href="{{ url('admin/advertisement-status',[@$detail->id]) }}" title="Active" onclick="return confirm('Do you want to active this advertisement?');"> <i class="fa fa-check delet" aria-hidden="true"></i></a>
														@elseif($detail->status == "A")
															<a href="{{ url('admin/advertisement-status',[@$detail->id]) }}" title="Inactive" onclick="return confirm('Do you want to inactive this advertisement');"> <i class="fa fa-times delet" aria-hidden="true"></i></a>
														@endif
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<form method="post" id="destroy">
	{{ csrf_field() }}
	{{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
	var confirm = window.confirm('Are you want to delete this advertisement ?');
	if(confirm){
		$("#destroy").attr('action',val);
		$("#destroy").submit();
	}
 }
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection