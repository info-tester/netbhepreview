@extends('admin.layouts.app')
@if(@$experience)
@section('title', 'netbhe.com | Admin | Update Experience')
@else
@section('title', 'netbhe.com | Admin | Add Experience')
@endif
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">
						@if(@$experience)
							Update experience
						@else
							Add experience
						@endif
					</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.experiences') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
					</div>
				</div>
			</div>
			@include('admin.includes.error')
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 nhp">
									<div class="table-responsive" data-pattern="priority-columns">
										
											<form id="myform" method="post" action="{{ route('admin.experience.add') }}">
											{{ csrf_field() }}
											<input type="text" name="ID" id="" value="{{@$experience->id}}" hidden>
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1">Experience *</label>
												<input type="text" name="experience" class="form-control required" id="experience" placeholder="Experience" value="{{@$experience->experience}}">
												</div>
											</div>									
											<div class="clearfix"></div>
											<br>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="submit-login add_btnm">
													<input value="submit" type="submit" class="btn btn-default">
												</div>
											</div>
											<!--all_time_sho-->
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
</div>

@endsection
@section('footer')
@include('admin.includes.footer')
@endsection