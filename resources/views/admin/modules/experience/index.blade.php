@extends('admin.layouts.app')
@section('title', 'netbhe.com | Admin | Experiences')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">Manage Experiences</h4>
					<div class="submit-login no_mmg pull-right">
						<a href="{{ route('admin.experience.add') }}" title="Add"><button type="button" class="btn btn-default">Add Experiences</button></a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body table-rep-plugin">
                            <div class="row">
                                <form method="post" action="{{ route('admin.experiences')}}">
									@csrf
                                    <div class="col-md-6 col-sm-12 col-xs-12 col-lg-4">
                                        <div class="your-mail">
                                            <label for="exampleInputEmail1">Search by Experience Name</label>
											<input class="form-control" id="keyword" name="keyword" placeholder="Keyword" value="{{@$key['keyword']}}" type="text">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 submit-login">
                                        <button type="submit" class="btn btn-default">Search</button>
                                    </div>
                                </form>
                            </div>
							<div class="row">
								<div class="col-md-12 dess5">
									<i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
									<i class="fa fa-trash-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive" data-pattern="priority-columns">
										<table id="datatable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Experience</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@foreach(@$experiences as $exp)
												<tr>
													<td>{{ $exp->experience }}</td>
													<td>
														<a href="{{ route('admin.experience.edit',['id'=>@$exp->id]) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
														<a href="{{route('admin.experience.delete',$exp->id)}}" title="Delete" class="rjct delbtn" onclick="return confirm('Are you want to delete this experience ?');><i class="fa fa-trash-o delet" aria-hidden="true"></i></a>
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection