@extends('admin.layouts.app')
@section('title', 'NearO | Admin | Product Category Commission Edit')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Edit Product Category Commission</h4>
                    <div class="submit-login no_mmg pull-right">
                        <a href="{{ route('admin.commission') }}" title="Back"><button type="button"
                                class="btn btn-default">Back</button></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-rep-plugin">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 nhp">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <form id="myform" method="post"
                                            action="{{ route('category.commission.edit',['id'=>@$category->id])}}"
                                            enctype="multipart/form-data">
                                            @csrf
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="commission">Category Name</label>
                                                    <label>{{$category->category_name}}</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="commission">Commission
                                                        (%)</label>
                                                    <input type="text" name="commission"
                                                        class="form-control required" id="category_commission"
                                                        value="{{old('commission',@$category->commission)}}">
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>

                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="add_btnm submit-login">
                                                    <input value="Update" type="submit" class="btn btn-default">
                                                </div>
                                            </div>
                                            <!--all_time_sho-->
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Row -->
        </div>
        <!-- container -->
    </div>
    <!-- content -->
</div>
<script>
    $(document).ready(function(){
		$("#myform").validate({
			rules: {
				commission : 'number',
			},
			messages : {
				commission : {
					required 		: "Please enter commission percentage",
					number 			: "Please enter number."
				},				
	        },
	        errorPlacement: function (error, element) 
	        {
	            toastr.error(error.text());
	        }
		});
	});
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection