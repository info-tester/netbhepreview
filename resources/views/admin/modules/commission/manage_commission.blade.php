@extends('admin.layouts.app')
@section('title', 'NearO | Admin | Commission Edit')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Edit Commission</h4>
                    {{-- <div class="submit-login no_mmg pull-right">
                        <a href="{{ route('admin.refer.discount') }}" title="Back"><button type="button"
                                class="btn btn-default">Back</button></a>
                    </div> --}}
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-rep-plugin">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 nhp">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <form id="myform" method="post"
                                            action="{{ route('admin.commission.edit',['id'=>@$commission->id])}}"
                                            enctype="multipart/form-data">
                                            @csrf
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="commission">Professional Commission - Video Call
                                                        (%)</label>
                                                    <input type="text" name="commission"
                                                        class="form-control required" id="commission"
                                                        value="{{old('commission',@$commission->commission)}}">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="commission">Professional Commission - Chat
                                                        (%)</label>
                                                    <input type="text" name="chat_commission"
                                                        class="form-control required" id="chat_commission"
                                                        value="{{old('commission',@$commission->chat_commission)}}">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            {{-- <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="affiliate_commission_user">Affiliate Commission - Student Books
                                                        (%)</label>
                                                    <input type="text" name="affiliate_commission_user"
                                                        class="form-control required" id="affiliate_commission_user"
                                                        value="{{old('affiliate_commission_user',@$commission->affiliate_commission_user)}}">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="affiliate_commission_professional">affiliate Commission - Professional Hired
                                                        (%)</label>
                                                    <input type="text" name="affiliate_commission_professional"
                                                        class="form-control required" id="affiliate_commission_professional"
                                                        value="{{old('affiliate_commission_professional',@$commission->affiliate_commission_professional)}}">
                                                </div>
                                            </div> --}}

                                            <div class="clearfix"></div>

                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="add_btnm submit-login">
                                                    <input value="Update" type="submit" class="btn btn-default">
                                                </div>
                                            </div>
                                            <!--all_time_sho-->
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Edit Product Category Commission</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-rep-plugin">
                        <div class="row">
                            <div class="col-md-12 dess5">
                                <i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" data-pattern="priority-columns">
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Category Name</th>
                                                <th>Professional Commission Percentage</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if($categories->isNotEmpty())
                                            @foreach(@$categories as $category)
                                            <tr>
                                                <td>{{ $category->category_name }}</td>
                                                <td>{{ $category->commission }}%</td>
                                                <td>
                                                    <a href="{{ route('category.commission.edit',$category->id) }}" title="Edit"> <i class="fa fa-pencil-square-o delet" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach

                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- End Row -->
        </div>
        <!-- container -->
    </div>
    <!-- content -->
</div>
<script>
    $(document).ready(function(){
		$("#myform").validate({
			rules: {
				// commission : 'number',
                commission : {
                    required: true,
                    digits: true,
                    commission_can_not_be_greater:true
                },
                // chat_commission : 'number',
                chat_commission : {
                    required: true,
                    digits: true,
                    chat_commission_can_not_be_greater:true
                },
                affiliate_commission_user : 'number',
                affiliate_commission_professional : 'number',
			},
			messages : {
				commission : {
					required 		: "Please enter commission percentage.",
					number 			: "Please enter number.",
                    digits          : "Please enter positive number."
				},
                chat_commission : {
					required 		: "Please enter commission percentage.",
					number 			: "Please enter number.",
                    digits          : "Please enter positive number."
				},
                affiliate_commission_user : {
					required 		: "Please enter commission percentage.",
					number 			: "Please enter number."
				},
                affiliate_commission_professional : {
					required 		: "Please enter commission percentage.",
					number 			: "Please enter number."
				},
	        },
	        // errorPlacement: function (error, element)
	        // {
	        //     toastr.error(error.text());
	        // }
		});

        jQuery.validator.addMethod("commission_can_not_be_greater", function(value, element) {
        var comm = $('#commission').val();
        if (comm <= 100)
            return true;
            return false;
    }, "Commission cannot be greater than 100");
        jQuery.validator.addMethod("chat_commission_can_not_be_greater", function(value, element) {
        var comm = $('#chat_commission').val();
        if (comm <= 100)
            return true;
            return false;
    }, "Commission cannot be greater than 100");

	});
</script>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
