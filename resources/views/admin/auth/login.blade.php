@extends('admin.layouts.app')
@section('title', 'Netbhe | Admin | Login')
@section('content')
<div class="wrapper-page">
    <div class="panel panel-color panel-primary panel-pages nner">
        <div class="panel-heading bg-img">
            <div class="bg-overlay"></div>
            <h3 class="text-center m-t-10 text-white"><img src="{{ URL::to('public/frontend/images/logo.png') }}"></h3>
        </div>
        <div class="panel-body">
            <form class="form-horizontal m-t-20" role="form" method="POST" action="{{ route('admin.post.login') }}" id="login">
                @csrf
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <div class="col-xs-12">
                         @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif

                        @if (session()->has('error'))
                            <span class="help-block">
                                <strong class="error">{{ session()->get('error') }}</strong>
                            </span>
                        @endif
                        <input class="form-control input-lg required" id="email" name="email" type="text" placeholder="Email" value="{{ @Cookie::get('admin_user_email') == null ? old('email'): @Cookie::get('admin_user_email')}}" autofocus>
                       
                    </div>
                </div>
                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                    <div class="col-xs-12">
                         @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                        <input class="form-control input-lg required"id="password"type="password" placeholder="Password" name="password" value="{{ @Cookie::get('admin_user_password') }}">
                       
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <div class="checkbox checkbox-primary">
                            <input id="checkbox-signup" type="checkbox" name="remember" id="remember" @if(@Cookie::get('admin_user_email') != null) checked @endif>
                            <label for="checkbox-signup">Remember me</label>
                        </div>
                    </div>
                </div>
                <div class="form-group text-center m-t-40">
                    <div class="col-xs-12">
                        <button class="btn btn-primary btn-lg w-lg waves-effect waves-light" type="submit">Log In</button>
                    </div>
                </div>
                <div class="form-group m-t-30">
                    <div class="col-sm-7">
                        <a href="{{ url('/admin/password/reset') }}"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>
                    </div>
                    <!--<div class="col-sm-5 text-right">
                        <a href="register.html">Create an account</a>
                        </div>-->
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#login').validate();
    });
</script>
@endsection
