<!DOCTYPE html>
<html>
	<head>
		<title>{{ @$user['name']." Click on this link to reset your password." }}</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	@include('mail.forget_password.layouts.header')
	</head>
	<body>
		<div style="width:640px; margin:0 auto;">
			<div style="/*width:620px;*/ min-height:101px; background:#F5F5F3; /*padding: 0px 10px;*/ border:1px solid #dcd7d7;">
                <div style="float: none; text-align: center; margin-top: 0px; background-color: #0f586c;">
                    <img src="{{ url('public/frontend/images/logo.png') }}" width="240" alt="">
                </div>
            </div>
            <div style="width:620px; margin:0 auto; min-height:20px; padding:0 10px; margin:15px 0; background:#8ccd56; border:1px solid #dcd7d7; color: #000;">
                <p style="font-family:Arial; text-align:center; margin:9px 0; color: #000;">Reset your password by clicking on this link.</p>
            </div>
			<div style="width:620px; border:1px solid #dcd7d7; margin: 15px 0; padding:10px; ">
				<h1 style="font-family:Arial; font-size:16px; font-weight:500; /*color:#8ccd56;*/ margin:5px 0 12px 0;">Dear {{ @$user['name'] }},</h1>

				
				<div style="display:block; overflow:hidden; width:100%;">
					<p style="font-family:Arial; font-size:18px; font-weight:500; color:#000;margin: 28px 0px 0px;">
						Click this link to reset your password,<a href="{{ route('resetPass',["code"=>$code]) }}" class="btn btn-info">To Reset Your Password:- <a href="{{route('resetPass',["code"=>$code])}}">Click Here</a>
					</p>
				</div>
				
				<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839;margin: 0px 0px 10px 0px;">Thank you.</p>
				<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839;margin: 0px 0px 10px 0px;">Team Consultant Marketplace.com (for all emails)</p>
			</div>
		</div>
	</body>
</html>