<head>
    <style type="text/css">
        .form_bbg {
            width: 84%;
            display: block;
            margin:0 auto;
            -webkit-box-shadow: 0px 2px 12px -1px rgba(0,0,0,0.12);
            -moz-box-shadow: 0px 2px 12px -1px rgba(0,0,0,0.12);
            box-shadow: 0px 2px 11px -1px rgba(0,0,0,0.12);
            background: #fff;
            padding: 15px;
            padding-bottom: 22px;
            margin-bottom: 10px;
        }
        .heading_info {
            width: 100%;
            display: block;
            overflow: hidden;
        }
        .heading_info h2 {
            font-family: 'Roboto', sans-serif;
            font-size: 22px;
            color: #363636;
            float: left;
            width: auto;
            margin: 0 0 10px;
        }
        .nnew_add_1 {
            display: block;
            overflow: hidden;
            width: 100%;
            border: 1px solid #e9e4e4;
        }
        
        .nnew_add_1 p {
            color: #444141;
            font-family: 'Open Sans', sans-serif;
            font-size: 15px;
            font-weight: 400;
            text-align: left;
            margin: 0 0 8px 0 !important;
            float: left;
            width: 100%;
        }
        .nnew_add_1 p strong {
            float: left;
            width: 30%;
            color: #000;
        }
        .nnew_add_1 span {
            padding: 0 0 0 9px;
            width: 70%;
            /*float: right;*/
        }
        .ordrdtls{
            width:100%;
            float:left;
            display:block;
            overflow:hidden;
            padding:10px;
        }

        table.table-style-one {
            font-family: verdana,arial,sans-serif;
            font-size:13px;
            color:#333333;
            border-width: 1px;
            border-color: #ccc;
            border-collapse: collapse;
            width:100%;
            margin-top:20px;
        }
        table.table-style-one th {
            border-width: 1px;
            padding: 8px;
            border-style: solid;
            border-color: #ccc;
            background-color: #E4E4E4;
        }
        table.table-style-one td {
            border-width: 1px;
            padding: 8px;
            border-style: solid;
            border-color: #ccc;
            background-color: #ffffff;
        }
        
        @media(max-width:1199px) {
            }   
        @media(max-width:991px) {
            }
        @media(max-width:575px) {   
        .nnew_add_1 p strong {
            width: 128px;
            color: #000;
        }   
        .nnew_add_1 span {
            width: auto;
        }
        }
        .main_div_
        {
            width: 50%;
            float: left;
            font-weight: 600 !important;
        }
        .main_div_ p
        {
            font-weight: 600 !important;
        }
    </style>
</head>