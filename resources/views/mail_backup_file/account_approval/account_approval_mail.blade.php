
<!DOCTYPE html>
<html>
	<head>
		<title>Mail</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	@include('mail.account_approval.layouts.header')
	</head>
	<body>
		<div style="width:640px; margin:0 auto;">
			<div style="/*width:620px;*/ min-height:101px; background:#F5F5F3; /*padding: 0px 10px;*/ border:1px solid #dcd7d7;">
                <div style="float: none; text-align: center; margin-top: 0px; background-color: #0f586c;">
                    <img src="{{ url('public/frontnd/images/logo.png') }}" width="240" alt="">
                </div>
            </div>
            <div style="width:620px; margin:0 auto; min-height:20px; padding:0 10px; margin:15px 0; background:#8ccd56; border:1px solid #dcd7d7; color: #000;">
                <p style="font-family:Arial; text-align:center; margin:9px 0; color: #000;">Thank You for connecting with Netbhe</p>
            </div>
			<div style="width:620px; border:1px solid #dcd7d7; margin: 15px 0; padding:10px; ">
				<h1 style="font-family:Arial; font-size:16px; font-weight:500; /*color:#8ccd56;*/ margin:5px 0 12px 0;">Dear {{@$user->name}},</h1>

				<!-- <div style="display:block;overflow:hidden; width:100%;">
					<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839; float:left;margin: 12px 0 0 0;width:77px;">Name :</p>
					<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839; float:left;margin: 12px 0 0 0;">Proqrx</p>
				</div> -->
				<!-- <div style="display:block;overflow:hidden; width:100%;">
					<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839; float:left;margin: 12px 0 0 0;width:77px;">Username :</p>
					<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839; float:left;margin: 12px 0 0 0;">Username name here Se Will</p>
				</div>
				<div style="display:block;overflow:hidden; width:100%;">
					<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839; float:left;margin: 12px 0 0 0;width:77px;">Password :</p>
					<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839; float:left;margin: 12px 0 0 0;">abcd145235</p>
				</div> -->
				<div style="display:block; overflow:hidden; width:100%;">
					<p style="font-family:Arial; font-size:14px; font-weight:500; color:#000;margin: 28px 0px 0px;">
						Your account has been activated.
					</p>
				</div>
				<div style="display:block;overflow:hidden; width:100%; margin: 5px 0px 10px 0px;">
					<p style="font-family:Arial; font-size:14px; font-weight:500; text-align:center; color:#000;padding: 4px; background:#f5f5f5;">
						please click the link below :
					</p>
				</div>
				<div style="display:block;overflow:hidden; width:100%; text-align:center; margin: 0px 0px 10px 0px;">
					<a href="{{ route('login') }}" style="font-family:Arial; border-radius:17px;font-size:15px; font-weight:500; color:#FFF; display:inline-block; padding: 7px 12px; background:#8ccd56; text-decoration:none;">Login</a>
				</div>
				<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839;margin: 0px 0px 10px 0px;">Thank you.</p>
				<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839;margin: 0px 0px 10px 0px;">Team Netbhe.com (for all emails)</p>
			</div>
		</div>
	</body>
</html>
