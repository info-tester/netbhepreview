@extends('layouts.app')
@section('title', session()->has('success') || session()->has('warning')  ? "Success": "Error")

@section('style')
    @include('includes.style')
@endsection

@section('scripts')
    @include('includes.scripts')
@endsection

@section('header')
    @include('includes.header')
@endsection


@section('content')
    <div class="login-body">
    <div class="container">
        <div class="row">
            <div class="main-login">
                <h2>
                    @if(session()->has('success'))
                    {{ "Sucesso" }}
                    @elseif(session()->has('error'))
                    {{ "Error" }}
                    @endif
                </h2>
                <div class="white-div">
                    {{-- <form id="myform" name="myform" method="post" action="{{ route('login') }}">
                        @csrf --}}

                        <div class="from-group">
                            @if($errors->any())
                                @foreach($errors->all() as $err)

                                    <div class="alert alert-danger alert-dismissible">
                                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                      <strong>{{ $err }}</strong>
                                    </div>
                                @endforeach
                            @endif

                            @if(session()->has('success'))
                                @if(session()->get('success') == "Registration succesful!")
                                    <div class="alert alert-success">
                                        <h4 class="mb-4">Bem-vindo a Plataforma da Netbhe!</h4>
                                        <p>Estamos felizes que você se cadastrou!</p>
                                        <p>Por favor, complete seu perfil pessoal e profissional para que nossa equipe revise seus dados e ative sua conta para você aparecer online, isso pode levar até 48 horas. </p>
                                    </div>
                                @else
                                    <div class="alert alert-success alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <h2><p>{{ session()->get('success') }}</p></21>
                                    </div>
                                @endif
                            @endif

                            @if(session()->has('error'))
                                <div class="alert alert-danger alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                  <strong>{{ session()->get('error') }}</strong>
                                </div>
                            @endif

                            @if(session()->has('warning'))
                                <div class="alert alert-info alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                  <strong>{{ session()->get('warning') }}</strong>
                                </div>
                            @endif
                        </div>


                       <div class="form-group col-md-12">
                        @if(session()->get('success') == "Registration succesful!")
                          <center><a href="javascript:;" id="logmein" class="login_submit1">@lang('client_site.back_to_log_in')</a></center>
                        @else
                          <center><a href="{{ route('login') }}" class="login_submit1">@lang('client_site.back_to_log_in')</a></center>
                        @endif
                       </div>

                </div>
                {{-- <div class="white-div below-white">
                    <p>Dont Have an account ?</p>
                    <div class="user-div">
                        <a href="{{ route('exp.register') }}">Sign Up as Expert</a>
                        <a href="{{ route('register') }}">Sign Up as User</a>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
</div>
@endsection



@section('footer')
    @include('includes.footer')
    <script>
        $('#myform').validate();
        $("#logmein").click(function(){
            window.location.href = "{{ route('log-me-in',['id' => session()->get('my_id'), 'val' => 'registrustdyyui']) }}";
        });
    </script>
@endsection
