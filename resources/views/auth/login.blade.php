@extends('layouts.app')
@section('title')
    @lang('client_site.login')
@endsection

@section('style')
    @include('includes.style')
@endsection

@section('scripts')
    @include('includes.scripts')
@endsection

@section('header')
    @include('includes.header')
@endsection


@section('content')
    <div class="login-body">
    <div class="container">
        <div class="row">
            <div class="main-login">
                <h2>@lang('client_site.login')</h2>
                <div class="white-div">
                    <form id="myform" name="myform" method="post" action="{{ route('login') }}">
                        @csrf

                        <div class="from-group">
                            @if($errors->any())
                                @foreach($errors->all() as $err)

                                    <div class="alert alert-danger alert-dismissible">
                                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                      <strong>{{ $err }}</strong>
                                    </div>
                                @endforeach
                            @endif

                            @if(session()->has('success'))
                                <div class="alert alert-success alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                  <strong>{{ session()->get('success') }}</strong>
                                </div>
                            @endif

                            @if(session()->has('error'))
                                <div class="alert alert-danger alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                  <strong>{{ session()->get('error') }}</strong>
                                </div>
                            @endif

                            @if(session()->has('warning'))
                                <div class="alert alert-info alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                  <strong>{{ session()->get('warning') }}</strong>
                                </div>
                            @endif
                        </div>

                        <div class="from-group">
                            <input type="text" name="email" id="email" placeholder="@lang('client_site.email_placeholder')" class="required email login-type" autocomplete="off" value="{{ Cookie::get('front_user_email')?Cookie::get('front_user_email'):'' }}">
                        </div>
                        <div class="clearfix"></div>
                        <div class="from-group">
                            <input type="password" name="password" id="password" placeholder="@lang('client_site.password')" class="required login-type" autocomplete="off" value="{{ Cookie::get('front_user_password')? Cookie::get('front_user_password'):'' }}" >
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group check_group">
                            <div class="checkbox-group">
                                <input id="checkiz"  type="checkbox" name="remember_me" @if(Cookie::get('front_user_password')) checked @endif>
                                <label for="checkiz" class="Fs16">
                                    <span class="check find_chek"></span>
                                    <span class="box W25 boxx"></span>
                                    @lang('client_site.remember_me')
                                </label>
                            </div>
                            <a class="login_forgot" href="{{ route('forget.pass') }}">@lang('client_site.forgot_password')</a>
                       </div>
                       <div class="form-group text-center">
                          <button type="submit" class="login_submit1">@lang('client_site.login')</button>
                       </div>
                       <div class="or-main">
                            <span>@lang('client_site.or')</span>
                       </div>
                       <div class="login-social">
                        <a href="{{ route('social.login',['prvdr'=>'facebook']) }}"><img src="{{ URL::to('public/frontend/images/facebook.png') }}" alt=""></a>
                        <a href="{{ route('social.login',['prvdr'=>'google']) }}"><img src="{{ URL::to('public/frontend/images/google.png') }}" alt=""></a>
                       </div>
                    </form>
                </div>
                <div class="white-div below-white">
                    <p>@lang('client_site.dont_have_an_account')</p>
                    <div class="user-div">
                        <a href="{{ route('exp.register') }}">@lang('client_site.sign_up_as_expert')</a>
                        <a href="{{ route('register') }}">@lang('client_site.sign_up_as_user')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



@section('footer')
    @include('includes.footer')
    <script>
        $('#myform').validate();
    </script>
@endsection
