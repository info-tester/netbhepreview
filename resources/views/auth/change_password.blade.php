@extends('layouts.app')
@section('title', 'Change your password')

@section('style')
    @include('includes.style')
@endsection

@section('scripts')
    @include('includes.scripts')
@endsection

@section('header')
    @include('includes.header')
@endsection


@section('content')
    <div class="login-body">
    <div class="container">
        <div class="row">
            <div class="main-login">
                <h2>Change Password</h2>
                <div class="white-div">
                    <form id="myform" name="myform" method="post" action="{{ route('updatePass') }}">
                        @csrf
                        
                        <div class="from-group">
                            @if($errors->any())
                                @foreach($errors->all() as $err)

                                    <div class="alert alert-danger alert-dismissible">
                                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                      <strong>{{ $err }}</strong>
                                    </div>
                                @endforeach
                            @endif

                            @if(session()->has('success'))
                                <div class="alert alert-success alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                  <strong>{{ session()->get('success') }}</strong>
                                </div>
                            @endif

                            @if(session()->has('error'))
                                <div class="alert alert-danger alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                  <strong>{{ session()->get('error') }}</strong>
                                </div>
                            @endif
                            
                            @if(session()->has('warning'))
                                <div class="alert alert-info alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                  <strong>{{ session()->get('warning') }}</strong>
                                </div>
                            @endif
                        </div>
                        <input type="hidden" name="vcode" value="{{ @$code }}">
                        <div class="from-group">
                            <input type="password" name="new_password" id="new_password" placeholder="New password" class="required login-type">
                        </div>
                        <div class="clearfix"></div>
                        <div class="from-group">
                            <input type="password" name="confirm_password" id="confirm_password" placeholder="Confirm Password" class="required login-type" >
                        </div>
                        <div class="clearfix"></div>
                        
                       <div class="form-group text-center">
                          <button type="submit" class="login_submit1">Update Password</button>
                       </div>
                       
                    </form>
                </div>
                <div class="white-div below-white">
                    
                </div>
            </div>
        </div>
    </div>
</div> 
@endsection



@section('footer')
    @include('includes.footer')
    <script>
        $('#myform').validate({
            rules:{
                new_password:{
                    required:true,
                    minlength:8,
                    // maxlength:8
                },
                confirm_password:{
                    required:true,
                    equalTo:"#new_password"
                }
            }
        });
    </script>
@endsection