@extends('layouts.app')
@php
// if(session()->get('expert')==1){ $x= 'Be An Expert By Creating Your Account'; }else { $x= "Register"; }
if(session()->get('expert')==1){ $x= 'Seja um especialista criando sua conta'; }else { $x= "Inscrever-se
"; }
@endphp
@section('title', @$x)

@section('style')
    @include('includes.style')
@endsection

@section('scripts')
    @include('includes.scripts')
@endsection

@section('header')
    @include('includes.header')
@endsection


@section('content')
    <div class="login-body">
    <div class="container">
        <div class="row">
            <div class="main-login expart-signup">
                <h2>@lang('client_site.sign_up')</h2>
                <p>@lang('client_site.sign_up_head')</p>
                <div class="white-div2">
                    <form id="myform" name="myform" method="post" action="{{ route('register') }}">
                        @csrf
                        <input type="hidden" name="total_dyn" id="total_dyn">
                        <div class="top-white">
                            <div class="from-group">
                                @if($errors->any())
                                    @foreach($errors->all() as $err)

                                        <div class="alert alert-danger alert-dismissible">
                                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                          <strong>{{ $err }}</strong>
                                        </div>
                                    @endforeach
                                @endif
                                <div style="display: none;" id="aldiv" class="alert alert-danger alert-dismissible">
                                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                          <strong id="emlAlr"></strong>
                                        </div>
                            </div>

                            @php
                                @$name = explode(' ', Cookie::get('name'));
                            @endphp
                            <div class="form-group">
                                <label class="expart-label">@lang('client_site.first_name')*</label>
                                <input type="text" name="first_name" id="first_name" placeholder="@lang('client_site.first_name_placeholder')" class="required login-type" value="{{ @$name[0] }}">
                            </div>
                            <div class="form-group">
                                <label class="expart-label">@lang('client_site.last_name')*</label>
                                <input type="text" id="last_name" name="last_name" placeholder="@lang('client_site.last_name_placeholder')" class="required login-type" value="{{ @$name[1] }}">
                            </div>
                            <div class="form-group">
                                <label class="expart-label">@lang('client_site.gender')*</label>
                                <!--<input type="text" placeholder="Ex : John" class="login-type">-->
                                <select name="gender" class="required login-type login-select">
                                    <option value="">@lang('client_site.gender')</option>
                                    <option value="M">@lang('client_site.male')</option>
                                    <option value="F">@lang('client_site.female')</option>
                                    <option value="B">@lang('client_site.both')</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="expart-label">@lang('client_site.email')*</label>
                                <input type="text" name="email" id="email" placeholder="@lang('client_site.email_placeholder')" class="required email login-type" value="{{ Cookie::get('email') }}">
                            </div>

                            <div class="form-group">
                                <label class="expart-label">@lang('client_site.password')*</label>
                                <input type="password" name="password" id="password" placeholder="@lang('client_site.password_placeholder')" class="required login-type">
                            </div>
                            <div class="form-group">
                                <label class="expart-label">@lang('client_site.confirm_password')*</label>
                                <input type="password" name="confirm_password" id="confirm_password" placeholder="@lang('client_site.confirm_password_placeholder')" class="login-type">
                            </div>
                            <div class="form-group">
                                <label class="expart-label">@lang('client_site.found_on')</label>
                                <input type="text" name="found_on" id="found_on" hidden>
                                <select id="found_on_select" class="login-type">
                                    <option value="">@lang('client_site.select')</option>
                                    <option value="Google">Google</option>
                                    <option value="Facebook">Facebook</option>
                                    <option value="Instagram">Instagram</option>
                                    <option value="Whatsapp">Whatsapp</option>
                                    <option value="Friends">Amigos</option>
                                    <option value="Youtube">Youtube</option>
                                    <option value="Sites">Sites</option>
                                    <option value="Coupon">Cupom</option>
                                    <option value="Others">Outros</option>
                                <select>
                            </div>
                            <div class="form-group" id="found_on_inp_parent">
                                <label class="expart-label">@lang('client_site.mention')*</label>
                                <input type="text" id="found_on_inp" class="login-type">
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 ">
                                <div class="capca_box form-group">
                                    <div class="g-recaptcha" data-sitekey="6LdC4iMaAAAAAPG7KS9WM4jQ89xMDaYPLJQMnLV6" data-callback="moCaptcha">
                                    </div>
                                </div>
                                <span class="moCaptchaError" style="color:red;display: none;">Please complete the recaptcha</span>
                            </div>
                        </div>


                        <div id="maindiv">
                            <div class="form-group">
                                <input type="checkbox" id="prof" name="prof">
                                <label for="prof">@lang('client_site.i_want_to_became_a_professional')</label>

                            </div>

                            <div class="mid-light" style="display:none;">
                                <div class="form-group" style="width:79%">
                                    <label class="expart-label">@lang('client_site.select_country')*</label>
                                    <select class="login-type" name="country" id="country_1">
                                        <option value="" >@lang('client_site.select_option')</option>
                                        @foreach($countries as $country)
                                            <option value="{{ @$country->id }}">{{ @$country->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group" style="width:79%">
                                    <label class="expart-label">@lang('client_site.registering_as')*</label>
                                    <label for="individual">
                                        <input type="radio" name="is_company" value="N" id="individual" checked >
                                        @lang('client_site.individual')
                                    </label>
                                    <label for="company">
                                        <input type="radio" name="is_company" value="Y" id="company">
                                        @lang('client_site.company')
                                    </label>
                                </div>
                                <div class="form-group" style="width:79%; display: none;" id="cnpj_field">
                                    <label class="expart-label">@lang('site.cnpj')*</label>
                                    <input type="text" name="cnpj" id="cnpj" class="login-type">
                                    <label class="expart-label">@lang('client_site.company_name')*</label>
                                    <input type="text" name="company_name" id="company_name" class="login-type">
                                </div>
                                <div class="form-group" style="width:79%">
                                    <label class="expart-label">@lang('site.sell_what')*</label>
                                    <select class="login-type" name="sell_what" id="sell_what">
                                        <option value="">@lang('client_site.select')</option>
                                        <option value="VS">@lang('client_site.video_only')</option>
                                        <option value="PS">@lang('client_site.product_sell_only')</option>
                                        <option value="BOTH" selected>@lang('client_site.both_of_above')</option>
                                    </select>
                                </div>
                                <div class="form-group category_hide">
                                    <label class="expart-label">@lang('client_site.category')*</label>
                                    <select class="login-type" name="category[]" id="category_1" onchange="call(1);" disabled>
                                        <option value="" >@lang('client_site.select_option')</option>
                                        @foreach($cat as $ct)
                                            <option value="{{ @$ct->id }}">{{ @$ct->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group category_hide">
                                    <label class="expart-label">@lang('client_site.subcategory')</label>
                                    <select class="login-type" name="subcategory[]" id="subcategory_1" disabled>
                                        <option value="">@lang('client_site.select_option')</option>
                                    </select>
                                </div>
                                <a href="javascript:void(0);" class="add_new_btn category_hide">+@lang('client_site.add')</a>
                            </div>


                        </div>


                        <div class="top-white">
                            <div class="form-group text-center">
                              <button type="button" class="login_submit1">@lang('client_site.create_account')</button>
                           </div>
                           <!-- Li e aceito as Condições legais, Termos de uso e Política de Proteção de Dados (receberá ofertas por e-mail e poderá pedir o cancelamento das comunicações a qualquer momento). -->

                           <!-- <span class="sign-des">@lang('client_site.i_agree_to_the')  <a href="{{ route('terms.of.services') }}">@lang('client_site.terms_of_use') </a> @lang('client_site.and') <a href="{{ route('privacy.policy') }}">@lang('client_site.privacy_policy').</a></span> -->
                           <span class="sign-des">Li e aceito as Condições legais,  <a href="{{ route('terms.of.services') }}">Termos de uso </a> e <a href="{{ route('privacy.policy') }}">Política de Proteção de Dados </a>(receberá ofertas por e-mail e poderá pedir o cancelamento das comunicações a qualquer momento).</span>

                           <div class="or-main">
                                <span>@lang('client_site.or')</span>
                           </div>
                           <div class="login-social">
                            <a href="{{ route('social.login',['prvdr'=>'facebook']) }}"><img src="{{ URL::to('public/frontend/images/ex-fab.png') }}" alt=""></a>
                            <a href="{{ route('social.login',['prvdr'=>'google']) }}"><img src="{{ URL::to('public/frontend/images/ex-goo.png') }}" alt=""></a>
                           </div>

                        </div>
                        <input type="hidden" name="user_type" id="user_type" value="U">
                    </form>
                </div>
                <span class="already">@lang('client_site.already_have_an_account') <a href="{{ route('login') }}">@lang('client_site.login')</a></span>
            </div>
        </div>

    </div>
</div>
@endsection



@section('footer')
    @include('includes.footer')

    @if(session()->get('expert')==1)

        <script>
            $('#prof').prop('checked', 'checked');
            $('#category_1').removeAttr("disabled");
            $('#subcategory_1').removeAttr("disabled");
            $('#user_type').val('P');
            $('#category_1').removeAttr("disabled");
            $('#subcategory_1').removeAttr("disabled");
            $('#user_type').val('P');
            $(".mid-light").show();
            if(!$('#sell_what').hasClass('required')) $('#sell_what').addClass('required');
            if(!$('#country_1').hasClass('required')) $('#country_1').addClass('required');
            if(!$('#category_1').hasClass('required')) $('#category_1').addClass('required');
            if(!$('#subcategory_1').hasClass('required')) $('#subcategory_1').addClass('required');
            $('#cnpj_field').hide();
        </script>
    @endif
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
        <script>
            let moCapt = null,
            vmtCapt = null;
            function moCaptcha(response) {
                moCapt = response;
                $('.moCaptchaError').hide();
            }
            function vmtCaptcha(response) {
                vmtCapt = response;
                $('.vmtCaptchaError').hide();
            }
            // $("#myform").submit(function() {
            //     if (moCapt == null || moCapt == '') {
            //         $('.moCaptchaError').show();
            //         return false;
            //     } else {
            //         $('.moCaptchaError').hide();
            //     }
            // });
        </script>
    <script>
        var category_is_reqd = true;
        var validator = $('#myform').validate({
            ignore: [],
            rules: {
                'confirm_password': {
                    required: true,
                    equalTo: "#password"
                },
                'password': {
                    required: true,
                    minlength: 8
                },
                'cnpj': {
                    remote: {
                        url: '{{ route("verify.cnpj") }}',
                        type: "post",
                        data: {
                            cnpj: function() {
                                return $( "#cnpj" ).val();
                            },
                            _token: '{{ csrf_token() }}'
                        }
                    }
                }
            },
            messages: {
                'cnpj': {
                    remote: "@lang('client_site.invalid_cnpj_number')",
                },
            },
            submitHandler: function(form) {
                var flag = 0;
                if($('#prof:checked').length > 0){
                    $('.category').each(function() {
                        if($(this).val()==""){
                            $(this).addClass('error');
                            $(this).parent().append('<label class="error">Este campo é obrigatório.</label>');
                            flag = 1;
                        }
                    });
                    $('.subcategory').each(function() {
                        if($(this).val()==""){
                            $(this).addClass('error');
                            $(this).parent().append('<label class="error">Este campo é obrigatório.</label>');
                            flag = 1;
                        }
                    });
                }
                if (moCapt == null || moCapt == '') {
                    $('.moCaptchaError').show();
                    flag == 1;
                    return false;
                } else {
                    $('.moCaptchaError').hide();
                }
                if(flag == 1){
                    return false;
                } else {
                    $('#total_dyn').val(total);
                    $('.login_submit1').addClass('disable_btn');
                    $('.login_submit1').attr('disabled', true);
                    $('.login_submit1').text('Em andamento...');
                    form.submit();
                }
            }
        });

        $('.login_submit1').click(function(){
            console.log(validator.errorList);
            validator.resetForm();
            if($("#myform").valid()){
                $('#myform').submit();
            }
        });

        $("#email").change(function(){
            if($("#email").val() !=""){
                var reqData = {
                  'jsonrpc' : '2.0',
                  '_token' : '{{csrf_token()}}',
                  'params' : {
                        'email' : $('#email').val()
                    }
                };
                $.ajax({
                    url: "{{ route('chk.email') }}",
                    method: 'post',
                    dataType: 'json',
                    data: reqData,
                    success: function(response){
                        if(response.status==1) {

                            $('#aldiv').show();
                            $("#emlAlr").html("E-mail já em uso.");
                            $('#email').val("");
                        }
                        else{
                            $('#aldiv').hide();
                            $("#emlAlr").html("");
                        }
                    }, error: function(error) {
                        console.error(error);
                    }
                });
            }

        });

        $('#sell_what').change(function(){
            if($(this).val() == 'PS'){
                $('.category_hide').hide();
                $('#category_1').removeClass('required');
                $('#subcategory_1').removeClass('required');
            } else {
                $('.category_hide').show();
                if(!$('#category_1').hasClass('required')) $('#category_1').addClass('required');
                if(!$('#subcategory_1').hasClass('required')) $('#subcategory_1').addClass('required');
            }
        });

        $(document).delegate('input[type=radio][name=is_company]', 'change', function(){
            if(this.value == "Y") {
                $('#cnpj_field').show();
                if(!$('#cnpj').hasClass('required')) $('#cnpj').addClass('required');
                if(!$('#company_name').hasClass('required')) $('#company_name').addClass('required');
            } else {
                $('#cnpj_field').hide();
                $('#cnpj').val('');
                $('#company_name').val('');
                $('#cnpj').removeClass('required');
                $('#cnpj').removeClass('error');
                $('#company_name').removeClass("required");
                $('#company_name').removeClass('error');
            }
        });
    </script>
    <script>
        var total = 1;
        // $(document).ready(function(){
            $('.add_new_btn').click(function(){
                if($('#category_1').prop('disabled')==true){
                    return false;
                }
                total++;
                var html = '<div class="mid-light category_hide" id="dyn_'+total+'"><div class="form-group"><label class="expart-label">Category*</label><select class="login-type category" name="category[]" id="category_'+total+'" onchange="call('+total+');" ><option value="" >@lang('client_site.select')</option>@foreach(@$cat as $ct)<option value="{{ @$ct->id }}">{{ @$ct->name }}</option>@endforeach</select></div>\
                                    <div class="form-group">\
                                        <label class="expart-label">Subcategory</label>\
                                        <select class= login-type subcategory" name="subcategory[]" id="subcategory_'+total+'">\
                                            <option value="">Selecione a subcategoria</option>\
                                        </select>\
                                    </div>\
                                    <a href="javascript:void(0);" class="rmv_btn" onclick="minusDiv('+total+');">-</a></div>';
                $('#maindiv').append(html);
                return false;
            });
            function minusDiv(id){
                total--;
                $('#dyn_'+id).remove();
                return false;
            }

            function call(id){
                if($('#category_'+id).val()!=""){
                    var reqData = {
                      'jsonrpc' : '2.0',
                      '_token' : '{{csrf_token()}}',
                      'params' : {
                            'cat' : $('#category_'+id).val()
                        }
                    };
                    $.ajax({
                        url: "{{ route('get.subcat') }}",
                        method: 'post',
                        dataType: 'json',
                        data: reqData,
                        success: function(response){
                            if(response.status==1) {
                                var i=0, html="";
                                html = '<option value="">Selecione a subcategoria</option>';
                                for(;i<response.result.length;i++){
                                    html+='<option value='+response.result[i].id+'>'+response.result[i].name+'</option>';
                                }
                                $('#subcategory_'+id).html(html);
                                $('#subcategory_'+id).addClass('valid');
                                // $('#subcategory_'+id).addClass('required');
                            }
                            else{

                            }
                        }, error: function(error) {
                            console.error(error);
                        }
                    });
                }
            }

            $('#prof').click(function(){
                if($('#prof').prop('checked')==true){
                    $('#category_1').removeAttr("disabled");
                    $('#subcategory_1').removeAttr("disabled");
                    $('#user_type').val('P');
                    $(".mid-light").show();
                    if(!$('#sell_what').hasClass('required')) $('#sell_what').addClass('required');
                    if(!$('#country_1').hasClass('required')) $('#country_1').addClass('required');
                    if(!$('#category_1').hasClass('required')) $('#category_1').addClass('required');
                    if(!$('#subcategory_1').hasClass('required')) $('#subcategory_1').addClass('required');
                }
                else{
                    $(".mid-light").hide();
                    $('#category_1').prop('disabled', 'disabled');
                    $('#subcategory_1').prop('disabled', 'disabled');
                    $('#user_type').val('U');
                    $('#sell_what').removeClass('required');
                    $('#country_1').removeClass('required');
                    $('#category_1').removeClass('required');
                    $('#subcategory_1').removeClass('required');
                    $('#cnpj').removeClass("required");
                    $('#company_name').removeClass("required");
                    for(i=1;i<=total;i++){
                        $('#dyn_'+i).remove();
                    }
                    total = 1;
                }
            });
        // });
        // $("#myform").submit(function(){
        //     $('#total_dyn').val(total);
        //     if($("#myform").valid()){
        //         $('.login_submit1').addClass('disable_btn');
        //         $('.login_submit1').attr('disabled', true);
        //         $('.login_submit1').text('Em andamento...');
        //     }
        // });


        $("#found_on_select").change(function(){
            if($(this).val() == "Others"){
                $("#found_on_inp_parent").css("display", "block");
                $("#found_on_inp").addClass("required");
            } else {
                $('#found_on').val($(this).val());
                $("#found_on_inp_parent").css("display", "none");
                $("#found_on_inp").removeClass("required");
            }
        });

        $('#found_on_inp').keyup(function(){
            $('#found_on').val($(this).val());
        });

    </script>
@endsection
