@extends('layouts.app')
@section('title', 'Register your account')

@section('style')
    @include('includes.style')
@endsection

@section('scripts')
    @include('includes.scripts')
@endsection

@section('header')
    @include('includes.header')
@endsection


@section('content')
  <div class="login-body">
    <div class="container">
        <div class="row">
            <div class="main-login">
                <h2>@lang('client_site.sign_up')</h2>
                <div class="white-div signup-page">
                    <form id="myform" name="myform" action="{{ route('register') }}" method="post">
                        @csrf
                        <div class="from-group">
                            @if($errors->any())
                                @foreach($errors->all() as $err)

                                    <div class="alert alert-danger alert-dismissible">
                                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                      <strong>{{ $err }}</strong>
                                    </div>
                                @endforeach

                            @endif
                            <div style="display: none;" id="aldiv" class="alert alert-danger alert-dismissible">
                                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                          <strong id="emlAlr"></strong>
                                        </div>
                        </div>

                        <div class="one-row">
                            <div class="from-group">
                                <input type="text" name="first_name" id="first_name" placeholder="First Name" class=" required login-type">
                            </div>
                            <div class="from-group" style="margin-right:0px;">
                                <input type="text" name="last_name" id="last_name" placeholder="Last Name" class="required login-type">
                            </div>
                        </div>
                        <input type="hidden" name="user_type" id="user_type" value="U">
                        <div class="from-group">

                            <select name="gender" class="required login-type login-select">
                                <option value="">@lang('client_site.gender')</option>
                                <option value="M">@lang('client_site.male')</option>
                                <option value="F">@lang('client_site.female')</option>
                            </select>
                        </div>
                        <div class="from-group">
                            <input type="text" name="email" id="email" placeholder="Enter Email" class="login-type required email">
                        </div>
                        <div class="from-group">
                            <input type="password" name="password" id="password" placeholder="Password" class="login-type">
                        </div>
                        <div class="from-group">
                            <input type="password" name="confirmed" id="confirmed" placeholder="Confirm Password" class="login-type">
                        </div>
                       <div class="form-group text-center">
                          <button type="submit" class="login_submit1">@lang('client_site.create_account')</button>
                       </div>
                       <p class="sign-des">@lang('client_site.by_signing_up') <a href="#">@lang('client_site.Privacy Policy') </a> @lang('client_site.and') <a href="#">@lang('client_site.terms_of_service')</a></p>
                       <div class="login-social">
                        <p>@lang('client_site.or_sign_up_with_your_social_network')</p>
                        <a href="{{ route('social.login',['prvdr'=>'facebook']) }}"><img src="{{ URL::to('public/frontend/images/facebook.png') }}" alt=""></a>
                        <a href="{{ route('social.login',['prvdr'=>'google']) }}"><img src="{{ URL::to('public/frontend/images/google.png') }}" alt=""></a>
                       </div>
                    </form>
                </div>
                <p class="already">@lang('client_site.already_have_an_account') <a href="login.html">@lang('client_site.login')</a></p>
            </div>
        </div>
    </div>
</div>
@endsection



@section('footer')
    @include('includes.footer')
    <script>
        $('#myform').validate({
            rules:{
                confirmed:{
                    required:true,
                    equalTo:"#password"
                },
                password:{
                    required:true,
                    minlength:8,
                    maxlength:8
                }
            }
        });

        $("#email").change(function(){
            if($("#email").val() !=""){
                var reqData = {
                    jsonrpc: '2.0',
                    _token: '{{ csrf_token() }}',
                    params: {
                        'email': $('#email').val()
                    }
                };
                $.ajax({
                    url: "{{ route('chk.email') }}",
                    method: 'post',
                    dataType: 'json',
                    data: reqData,
                    success: function(response){
                        if(response.status==1) {

                            $('#aldiv').show();
                            $("#emlAlr").html('@client_site('e_mail_already')');
                            $('#email').val("");
                        }
                        else{
                            $('#aldiv').hide();
                            $("#emlAlr").html("");
                        }
                    }, error: function(error) {
                        console.error(error);
                    }
                });
            }
        });
    </script>
@endsection
