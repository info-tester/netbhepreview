@extends('layouts.app')
@section('title')
    @lang('client_site.forget_password')
@endsection
@section('style')
    @include('includes.style')
    <style type="text/css">
        .login-type {
            max-width: 80% !important
        }
        
    </style>
@endsection

@section('scripts')
    @include('includes.scripts')
@endsection

@section('header')
    @include('includes.header')
@endsection


@section('content')
    <div class="login-body">
    <div class="container">
        <div class="row">
            <div class="main-login">
                <h2>@lang('client_site.forget_password')</h2>
                <div class="white-div">
                    <form id="myform" name="myform" method="post" action="{{ route('send.pass.link') }}">
                        @csrf
                        
                        <div class="from-group">
                            @if($errors->any())
                                @foreach($errors->all() as $err)

                                    <div class="alert alert-danger alert-dismissible">
                                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                      <strong>{{ $err }}</strong>
                                    </div>
                                @endforeach
                            @endif

                            @if(session()->has('success'))
                                <div class="alert alert-success alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                  <strong>{{ session()->get('success') }}</strong>
                                </div>
                            @endif

                            @if(session()->has('error'))
                                <div class="alert alert-danger alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                  <strong>{{ session()->get('error') }}</strong>
                                </div>
                            @endif
                            
                            @if(session()->has('warning'))
                                <div class="alert alert-info alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                  <strong>{{ session()->get('warning') }}</strong>
                                </div>
                            @endif
                        </div>

                        <div class="from-group">
                            <input type="text" name="email" id="email" placeholder="@lang('client_site.enter_email')" class="required email login-type" value="{{ Cookie::get('front_user_email') }}">
                        </div>
                        <div class="clearfix"></div>

                        <div class="clearfix"></div>
                        
                       <div class="form-group text-center wrapper1" style="width: 100%;">
                          <button type="submit"  class="login_submit1 button123">@lang('client_site.submit')</button>
                       </div>
                       
                    </form>
                </div>
                
            </div>
        </div>
    </div>
</div> 
@endsection



@section('footer')
    @include('includes.footer')
    <script>
        $('#myform').validate();
    </script>
@endsection