@extends('layouts.app')
@php
// if(session()->get('expert')==1){ $x= 'Be An Expert By Creating Your Account'; }else { $x= "Register"; }
if(session()->get('expert')==1){ $x= 'Seja um especialista criando sua conta'; }else { $x= "Inscrever-se
"; }
@endphp
@section('title', @$x)

@section('style')
    @include('includes.style')
    <link rel="stylesheet" href="{{ URL::to('public/frontend/css/landing-style.css') }}">
    <link rel="stylesheet" href="{{ URL::to('public/frontend/css/landing-style-1.css') }}">
@endsection

@section('scripts')
    @include('includes.scripts')
@endsection

@section('header')
    @include('includes.header')
@endsection


@section('content')
<div id="page-container" style="float: left; width: 100%;" class="et-animated-content">

    <!-- #main-header -->
    <div id="et-main-area">
        <div id="main-content">
            <article id="post-160" class="post-160 page type-page status-publish hentry">
                <div class="entry-content">
                    <div id="et-boc" class="et-boc">
                        <div class="et_builder_inner_content et_pb_gutters3">
                            <div class="et_pb_section et_pb_section_0 et_pb_with_background et_section_regular et_had_animation"
                                style="">
                                <div class="et_pb_row et_pb_row_0">
                                    <div
                                        class="et_pb_column et_pb_column_1_2 et_pb_column_0  et_pb_css_mix_blend_mode_passthrough">
                                        <div
                                            class="et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_dark  et_pb_text_align_left">
                                            <div class="et_pb_text_inner">
                                                <h1>{{ $banner1->title }}</h1>
                                            </div>
                                        </div>
                                        <!-- .et_pb_text -->
                                        <div
                                            class="et_pb_module et_pb_text et_pb_text_1 et_pb_bg_layout_light  et_pb_text_align_left">
                                            <div class="et_pb_text_inner">
                                                <p><span style="font-size: 16px;color: #d4ccff!important;">{{ $banner1->description }}</span></p>
                                            </div>
                                        </div>
                                        <!-- .et_pb_text -->
                                        <div class="et_pb_button_module_wrapper et_pb_button_0_wrapper  et_pb_module et_had_animation"
                                            style=""> <a
                                                class="et_pb_button et_pb_custom_button_icon et_pb_button_0 et_hover_enabled et_pb_bg_layout_light"
                                                href="{{ $banner1->description_4 }}" data-icon="E">{{ $banner1->heading_4 }}</a> </div>
                                    </div>
                                    <!-- .et_pb_column -->
                                    <div
                                        class="et_pb_column et_pb_column_1_2 et_pb_column_1  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                        <div class="et_pb_module et_pb_image et_pb_image_0 et-animated et_had_animation"
                                            style=""> <span class="et_pb_image_wrap "><img src="{{ URL::to('storage/app/public/uploads/content_images/' . $banner1->image) }}" srcset="" sizes="((min-width: 0px) and (max-width: 480px)) 480px, (min-width: 481px) 862px, 100vw"></span>
                                        </div>
                                    </div>
                                    <!-- .et_pb_column -->

                                </div>
                                <!-- .et_pb_row -->

                            </div>
                            <!-- .et_pb_section -->
                            <div class="et_pb_section et_pb_section_1 et_section_specialty">
                                <div class="et_pb_row">
                                    <div
                                        class="et_pb_column et_pb_column_1_3 et_pb_column_2 et_pb_css_mix_blend_mode_passthrough et_pb_column_single">
                                        <div class="et_pb_module et_pb_text et_pb_text_2 et_pb_bg_layout_light  et_pb_text_align_left et_had_animation"
                                            style="">
                                            <div class="et_pb_text_inner">
                                                <h2>{{ $leftSections[0]->title }}</h2>
                                            </div>
                                        </div>
                                        <!-- .et_pb_text -->
                                        <div class="et_pb_module et_pb_divider et_pb_divider_0 et_pb_divider_position_ et_pb_space et_had_animation"
                                            style="">
                                            <div class="et_pb_divider_internal"></div>
                                        </div>
                                        <div class="et_pb_module et_pb_text et_pb_text_3 et_pb_bg_layout_light  et_pb_text_align_left et_had_animation"
                                            style="">
                                            <div class="et_pb_text_inner">
                                                <h4>{{ $leftSections[0]->heading_1 }}.&nbsp;</h4>
                                                <p>{{ $leftSections[0]->description_1 }}</p>
                                            </div>
                                        </div>
                                        <!-- .et_pb_text -->
                                        <div class="et_pb_button_module_wrapper et_pb_button_1_wrapper et_pb_button_alignment_left et_pb_module et_had_animation"
                                            style=""> <a class="et_pb_button et_pb_custom_button_icon et_pb_button_1 et_hover_enabled et_pb_bg_layout_light" href="{{ $leftSections[0]->description_4 }}" data-icon="E">{{ $leftSections[0]->heading_4 }}</a> </div>
                                    </div>
                                    <!-- .et_pb_column -->
                                    <div class="et_pb_column et_pb_column_2_3 et_pb_column_3   et_pb_specialty_column  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                        <div class="et_pb_row_inner et_pb_row_inner_0 et_pb_gutters1 et_had_animation">
                                            @foreach ($sm1 as $k => $ssecs)
                                                <div class="et_pb_column et_pb_column_1_3 et_pb_column_inner et_pb_column_inner_{{ $k }} et_pb_column_1_2">
                                                    @foreach ($ssecs as $kk => $ssec)
                                                        <div class="et_pb_module et_pb_blurb et_pb_blurb_0 et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_left et_had_animation"
                                                            style="">
                                                            <div class="et_pb_blurb_content">
                                                                <div class="et_pb_main_blurb_image"><a href="javascript:void(0);"><span class="et_pb_image_wrap"><img src="{{ URL::to('storage/app/public/uploads/content_images/' . $ssec->image) }}" sizes="(max-width: 128px) 100vw, 128px" class="et-waypoint et_pb_animation_top et-animated"></span></a>
                                                                </div>
                                                                <div class="et_pb_blurb_container">
                                                                    <h4 class="et_pb_module_header"><a href="javascript:void(0);">{{ $ssec->title }}</a></h4>
                                                                    <div class="et_pb_blurb_description">
                                                                        <p>{{ $ssec->description }}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- .et_pb_blurb_content -->
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @endforeach
                                            <!-- .et_pb_column -->

                                        </div>
                                        <!-- .et_pb_row_inner -->
                                    </div>
                                    <!-- .et_pb_column -->
                                </div>
                                <!-- .et_pb_row -->

                            </div>
                            <!-- .et_pb_section -->
                            <div class="et_pb_section et_pb_section_3 et_pb_with_background et_section_regular">
                                <div class="et_pb_row et_pb_row_1 et_had_animation" style="">
                                    <div class="et_pb_column et_pb_column_1_2 et_pb_column_6  et_pb_css_mix_blend_mode_passthrough" style="padding-top: 32px;">
                                        @php
                                            preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $videoBanner->image, $match);
                                            $youtube_id = @$match[1];
                                        @endphp
                                        <iframe style="width: 100%; height: 400px;" src="https://www.youtube.com/embed/{{ $youtube_id }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                        <div class="et_pb_module et_pb_image et_pb_image_1 et-animated et_had_animation" style="">
                                            {{-- <span class="et_pb_image_wrap "><img src="{{ URL::to('storage/app/public/uploads/content_images/' . $banner2->image) }}" srcset="" sizes="((min-width: 0px) and (max-width: 480px)) 480px, (min-width: 481px) 800px, 100vw"></span> --}}
                                        </div>
                                    </div>
                                    <!-- .et_pb_column -->
                                    <div
                                        class="et_pb_column et_pb_column_1_2 et_pb_column_7  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                        <div class="et_pb_module et_pb_text et_pb_text_6 et_pb_bg_layout_dark  et_pb_text_align_left et_had_animation"
                                            style="">
                                            <div class="et_pb_text_inner">
                                                <h2>{{ $videoBanner->title }}</h2>
                                            </div>
                                        </div>
                                        <!-- .et_pb_text -->
                                        <div class="et_pb_module et_pb_divider et_pb_divider_2 et_pb_divider_position_ et_pb_space et_had_animation"
                                            style="">
                                            <div class="et_pb_divider_internal"></div>
                                        </div>
                                        <div class="et_pb_module et_pb_text et_pb_text_7 et_pb_bg_layout_light  et_pb_text_align_left et_had_animation"
                                            style="">
                                            <div class="et_pb_text_inner">
                                                <p style="color: #d4ccff!important;">{{ $videoBanner->description }}</p>
                                            </div>
                                        </div>
                                        <!-- .et_pb_text -->
                                        <div class="et_pb_button_module_wrapper et_pb_button_3_wrapper  et_pb_module et_had_animation"
                                            style=""> <a class="et_pb_button et_pb_custom_button_icon et_pb_button_3 et_hover_enabled et_pb_bg_layout_light" href="{{ $videoBanner->description_4 }}" data-icon="E">{{ $videoBanner->heading_4 }}</a> </div>
                                    </div>
                                    <!-- .et_pb_column -->
                                </div>
                                <!-- .et_pb_row -->
                            </div>
                            <div class="et_pb_section et_pb_section_2 et_section_specialty">
                                <div class="et_pb_row">
                                    <div
                                        class="et_pb_column et_pb_column_1_3 et_pb_column_4    et_pb_css_mix_blend_mode_passthrough et_pb_column_single">
                                        <div class="et_pb_module et_pb_text et_pb_text_4 et_pb_bg_layout_light  et_pb_text_align_left et_had_animation"
                                            style="">
                                            <div class="et_pb_text_inner">
                                                <h2>{{ $leftSections[1]->title }}</h2>
                                            </div>
                                        </div>
                                        <!-- .et_pb_text -->
                                        <div class="et_pb_module et_pb_divider et_pb_divider_1 et_pb_divider_position_ et_pb_space et_had_animation"
                                            style="">
                                            <div class="et_pb_divider_internal"></div>
                                        </div>
                                        <div class="et_pb_module et_pb_text et_pb_text_5 et_pb_bg_layout_light  et_pb_text_align_left et_had_animation"
                                            style="">
                                            <div class="et_pb_text_inner">
                                                <h4>{{ $leftSections[1]->heading_1 }}.&nbsp;</h4>
                                                <p>{{ $leftSections[1]->description_1 }}</p>
                                            </div>
                                        </div>
                                        <!-- .et_pb_text -->
                                        <div class="et_pb_button_module_wrapper et_pb_button_2_wrapper et_pb_button_alignment_left et_pb_module et_had_animation"
                                            style=""> <a
                                                class="et_pb_button et_pb_custom_button_icon et_pb_button_2 et_hover_enabled et_pb_bg_layout_light"
                                                href="{{ $leftSections[1]->description_4 }}" data-icon="E">{{ $leftSections[1]->heading_4 }}</a> </div>
                                    </div>
                                    <!-- .et_pb_column -->
                                    <div
                                        class="et_pb_column et_pb_column_2_3 et_pb_column_5   et_pb_specialty_column  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                        <div class="et_pb_row_inner et_pb_row_inner_1 et_pb_gutters1 et_had_animation" style="">
                                            @foreach ($sm2 as $k => $ssecs)
                                                <div class="et_pb_column et_pb_column_1_3 et_pb_column_inner et_pb_column_inner_{{ $k + 2 }} et_pb_column_1_2">
                                                    @foreach ($ssecs as $kk => $ssec)
                                                        <div class="et_pb_module et_pb_blurb et_pb_blurb_{{ $kk + 8 }} et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_left et_had_animation"
                                                            style="">
                                                            <div class="et_pb_blurb_content">
                                                                <div class="et_pb_main_blurb_image"><a href="javascript:void(0);"><span class="et_pb_image_wrap"><img src="{{ URL::to('storage/app/public/uploads/content_images/' . $ssec->image) }}" sizes="(max-width: 128px) 100vw, 128px" class="et-waypoint et_pb_animation_top et-animated"></span></a>
                                                                </div>
                                                                <div class="et_pb_blurb_container">
                                                                    <h4 class="et_pb_module_header"><a href="javascript:void(0);">{{ $ssec->title }}</a></h4>
                                                                    <div class="et_pb_blurb_description">
                                                                        <p>{{ $ssec->description }}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- .et_pb_blurb_content -->
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @endforeach
                                            <!-- .et_pb_column -->
                                        </div>
                                        <!-- .et_pb_row_inner -->
                                    </div>
                                    <!-- .et_pb_column -->
                                </div>
                                <!-- .et_pb_row -->
                            </div>
                            <!-- .et_pb_section -->
                            <div class="et_pb_section et_pb_section_3 et_pb_with_background et_section_regular">
                                <div class="et_pb_row et_pb_row_1 et_had_animation" style="">
                                    <div
                                        class="et_pb_column et_pb_column_1_2 et_pb_column_6  et_pb_css_mix_blend_mode_passthrough">
                                        <div class="et_pb_module et_pb_image et_pb_image_1 et-animated et_had_animation"
                                            style=""> <span class="et_pb_image_wrap "><img src="{{ URL::to('storage/app/public/uploads/content_images/' . $banner2->image) }}" srcset="" sizes="((min-width: 0px) and (max-width: 480px)) 480px, (min-width: 481px) 800px, 100vw"></span>
                                        </div>
                                    </div>
                                    <!-- .et_pb_column -->
                                    <div
                                        class="et_pb_column et_pb_column_1_2 et_pb_column_7  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                        <div class="et_pb_module et_pb_text et_pb_text_6 et_pb_bg_layout_dark  et_pb_text_align_left et_had_animation"
                                            style="">
                                            <div class="et_pb_text_inner">
                                                <h2>{{ $banner2->title }}</h2>
                                            </div>
                                        </div>
                                        <!-- .et_pb_text -->
                                        <div class="et_pb_module et_pb_divider et_pb_divider_2 et_pb_divider_position_ et_pb_space et_had_animation"
                                            style="">
                                            <div class="et_pb_divider_internal"></div>
                                        </div>
                                        <div class="et_pb_module et_pb_text et_pb_text_7 et_pb_bg_layout_light  et_pb_text_align_left et_had_animation"
                                            style="">
                                            <div class="et_pb_text_inner">
                                                <p style="color: #d4ccff!important;">{{ $banner2->description }}</p>
                                            </div>
                                        </div>
                                        <!-- .et_pb_text -->
                                        <div class="et_pb_button_module_wrapper et_pb_button_3_wrapper  et_pb_module et_had_animation"
                                            style=""> <a class="et_pb_button et_pb_custom_button_icon et_pb_button_3 et_hover_enabled et_pb_bg_layout_light"
                                                href="{{ $banner2->description_4 }}" data-icon="E">{{ $banner2->heading_4 }}</a> </div>
                                    </div>
                                    <!-- .et_pb_column -->
                                </div>
                                <!-- .et_pb_row -->
                            </div>
                            <!-- .et_pb_section -->
                            <div class="et_pb_section et_pb_section_4 et_section_regular">
                                <div class="et_pb_row et_pb_row_2">
                                    <div
                                        class="et_pb_column et_pb_column_1_2 et_pb_column_8  et_pb_css_mix_blend_mode_passthrough">
                                        <div class="et_pb_module et_pb_image et_pb_image_2 et-animated et_had_animation"
                                            style=""> <span class="et_pb_image_wrap "><img
                                                    src="{{ URL::to('storage/app/public/uploads/content_images/' . $lastAreas[0]->image_1) }}"
                                                    sizes="(max-width: 128px) 100vw, 128px"></span> </div>
                                        <div class="et_pb_module et_pb_text et_pb_text_8 et_pb_bg_layout_light  et_pb_text_align_left et_had_animation"
                                            style="">
                                            <div class="et_pb_text_inner">
                                                <h2>{{ $lastAreas[0]->title }}&nbsp;</h2>
                                            </div>
                                        </div>
                                        <!-- .et_pb_text -->
                                        <div class="et_pb_module et_pb_divider et_pb_divider_3 et_pb_divider_position_ et_pb_space et_had_animation"
                                            style="">
                                            <div class="et_pb_divider_internal"></div>
                                        </div>
                                        <div class="et_pb_module et_pb_text et_pb_text_9 et_pb_bg_layout_light  et_pb_text_align_left et_had_animation"
                                            style="">
                                            <div class="et_pb_text_inner">
                                                <h4>{{ $lastAreas[0]->heading_1 }}</h4>
                                                <p>{{ $lastAreas[0]->description_1 }}</p>
                                            </div>
                                        </div>
                                        <!-- .et_pb_text -->
                                    </div>
                                    <!-- .et_pb_column -->
                                    <div
                                        class="et_pb_column et_pb_column_1_2 et_pb_column_9  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                        <div class="et_pb_module et_pb_image et_pb_image_3 et-animated et_had_animation"
                                            style=""> <span class="et_pb_image_wrap "><img src="{{ URL::to('storage/app/public/uploads/content_images/' . $lastAreas[0]->image) }}" srcset="" sizes="((min-width: 0px) and (max-width: 480px)) 480px, (min-width: 481px) 800px, 100vw"></span>
                                        </div>
                                    </div>
                                    <!-- .et_pb_column -->

                                </div>
                                <!-- .et_pb_row -->
                                <div class="et_pb_row et_pb_row_3">
                                    <div
                                        class="et_pb_column et_pb_column_1_2 et_pb_column_10  et_pb_css_mix_blend_mode_passthrough">
                                        <div class="et_pb_module et_pb_image et_pb_image_4 et-animated et_had_animation"
                                            style=""> <span class="et_pb_image_wrap "><img src="{{ URL::to('storage/app/public/uploads/content_images/' . $lastAreas[1]->image) }}" srcset="" sizes="((min-width: 0px) and (max-width: 480px)) 480px, (min-width: 481px) 800px, 100vw"></span>
                                        </div>
                                    </div>
                                    <!-- .et_pb_column -->
                                    <div
                                        class="et_pb_column et_pb_column_1_2 et_pb_column_11  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                        <div class="et_pb_module et_pb_image et_pb_image_5 et-animated et_had_animation"
                                            style=""> <span class="et_pb_image_wrap "><img src="{{ URL::to('storage/app/public/uploads/content_images/' . $lastAreas[1]->image_1) }}" sizes="(max-width: 128px) 100vw, 128px"></span> </div>
                                        <div class="et_pb_module et_pb_text et_pb_text_10 et_pb_bg_layout_light  et_pb_text_align_left et_had_animation"
                                            style="">
                                            <div class="et_pb_text_inner">
                                                <h2>{{ $lastAreas[1]->title }}</h2>
                                            </div>
                                        </div>
                                        <!-- .et_pb_text -->
                                        <div class="et_pb_module et_pb_divider et_pb_divider_4 et_pb_divider_position_ et_pb_space et_had_animation"
                                            style="">
                                            <div class="et_pb_divider_internal"></div>
                                        </div>
                                        <div class="et_pb_module et_pb_text et_pb_text_11 et_pb_bg_layout_light  et_pb_text_align_left et_had_animation"
                                            style="">
                                            <div class="et_pb_text_inner">
                                                <h4>{{ $lastAreas[1]->heading_1 }}</h4>
                                                <p>{{ $lastAreas[1]->description_1 }}</p>
                                            </div>
                                        </div>
                                        <!-- .et_pb_text -->
                                    </div>
                                    <!-- .et_pb_column -->

                                </div>
                                <!-- .et_pb_row -->

                            </div>
                            <!-- .et_pb_section -->
                        </div>
                    </div>
                </div>
                <!-- .entry-content -->

            </article>
            <!-- .et_pb_post -->
            <div class="et_pb_section">
                <div class="et_pb_button_module_wrapper et_pb_button_3_wrapper  et_pb_module et_had_animation" style="text-align: center;"> <a class="et_pb_button et_pb_custom_button_icon et_pb_button_3 et_hover_enabled et_pb_bg_layout_light" href="{{ route('exp.register') }}" data-icon="E">Cadastre-se</a> </div>
            </div>
        </div>

    </div>
    <!-- #et-main-area -->

</div>
@endsection



@section('footer')
    @include('includes.footer')

@endsection
