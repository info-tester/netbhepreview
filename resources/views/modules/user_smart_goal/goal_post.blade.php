@extends('layouts.app')
@section('title')
    {{-- @lang('site.edit') @lang('site.blog')-@lang('site.post') --}}
    Smart Goal
@endsection
@section('style')
@include('includes.style')
<style type="text/css">
    .chck_eml_rd{
        color: red;
        font-weight: bold;
        font-size: 14px;
    }
</style>
@endsection
@section('scripts')
@include('includes.scripts')

@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>
            @lang('site.smart_goals')
            {{-- Smart Goals --}}
            {{-- @lang('site.edit_form') --}}
            {{-- @lang('site.edit') @lang('site.blog')-@lang('site.post') --}}
        </h2>
        {{-- @include('includes.professional_tab_menu') --}}
        <div class="bokcntnt-bdy no-margin-top">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.user_sidebar')


            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">


                    <div class="mainDiv">
                        @if(sizeof(@$all_smart_goal_details)>0)
                        {{-- <strong>Goal Title : {{ @$details->getSmartGoalsData->goal_title }} ( {{ date('m/d/Y (D)', strtotime(@$details->getSmartGoalsData->start_date)) }} - {{ date('m/d/Y (D)', strtotime(@$details->getSmartGoalsData->end_date)) }} )</strong> --}}
                            <div class="buyer_table">
                                <div class="table-responsive">
                                    <div class="table">
                                        <div class="one_row1 hidden-sm-down only_shawo">
                                            <div class="cell1 tab_head_sheet">@lang('site.Goals_Point')</div>
                                            <div class="cell1 tab_head_sheet">@lang('site.date')</div>
                                        </div>
                                        <!--row 1-->
                                        @foreach(@$all_smart_goal_details as $goal_details)
                                        <div class="one_row1 small_screen31">

                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">goal_point</span>
                                                <p class="add_ttrr">{{ @$goal_details->goal_point }}</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">assign_date</span>
                                                <p class="add_ttrr">{{ date('m/d/Y (D)', strtotime(@$goal_details->created_at)) }}</p>
                                            </div>

                                        </div>
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <?php
                    $nowtime = date('Y-m-d');
                    $smar_enddate = date('Y-m-d', strtotime(@$details->getSmartGoalsData->end_date));

                    ?>

                    @if($nowtime < $smar_enddate)
                        <form id="myform" method="post" action="{{ route('user.smart.goal.post',['user_tools_is'=>@$details->id]) }}">
                            @csrf
                            <div class="form_body">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
                                        <div class="your-mail">
                                            <label for="exampleInputEmail1" class="personal-label"></label>

                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
                                        <div class="your-mail">
                                            <label for="exampleInputEmail1" class="personal-label">{{ @$details->getSmartGoalsData->goal_title }} {{-- Today Your Goal Point --}} *</label>
                                            <input type="text" name="goal_point" value="" class="personal-type required" id="goal_point" placeholder="@lang('site.enter_your_Goals_Point')" >
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                        <div class="submit-login add_btnm">
                                            <input value="@lang('site.submit')" type="submit" class="login_submitt">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
   <script>
    $(document).ready(function(){
        var finalgoal = parseInt("{{ @$details->getSmartGoalsData->goal_point ? @$details->getSmartGoalsData->goal_point : 0}}");
        $('.datepicker').datepicker();

        jQuery.validator.addMethod("lessthanfinalgoal", function(value, element) {
			return this.optional(element) || value <= finalgoal;
		}, "Please enter value less than or equal to "+ finalgoal);
        $("#myform").validate({
            rules:{
                goal_point: {lessthanfinalgoal: true},
            },
        });

        $('#goal_point').keyup(function(){
            $("#goal_point").inputFilter(function(value) {
            return /^\d*$/.test(value);
            });
        });
    });


    $(function() {
        $('#reminderdate').hide();
        $('#dayofweek').hide();
        $('#remindertype').change(function(){
            if($('#remindertype').val() == 'one time') {
                $('#reminderdate').show();
                $('#dayofweek').hide();
            }else if($('#remindertype').val() == 'weekly') {
                $('#dayofweek').show();
                $('#reminderdate').hide();
            }else {
                $('#reminderdate').hide();
                $('#dayofweek').hide();
            }
        });
    });

</script>


<style>
    .error{
        color: red !important;
    }
</style>

@endsection
