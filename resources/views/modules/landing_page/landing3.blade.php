<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Netbhe</title>
    <link href="{{URL::asset('public/frontend/landing_page3/css/bootstrap.css')}}" rel="stylesheet">
	<link href="{{URL::asset('public/frontend/landing_page3/css/style.css')}}" rel="stylesheet">
	<link href="{{URL::asset('public/frontend/landing_page3/css/responsive.css')}}" rel="stylesheet">
	<link href="{{URL::asset('public/frontend/landing_page3/css/font-awesome/all.min.css')}}" rel="stylesheet">
	<link href="{{URL::asset('public/frontend/landing_page3/css/jquery.fancybox.css')}}" rel="stylesheet">
	<link href="{{URL::asset('public/frontend/landing_page3/css/owl.carousel.css')}}" rel="stylesheet">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <script src="https://kit.fontawesome.com/2710be599b.js" crossorigin="anonymous"></script>
</head>
<body>
	<div id="landing_preloader" class="u-loading">
		<div class="u-loading_symbol">
			@if(@$detail->landing_logo)
				<img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->landing_logo_thumbnail}}" alt="logo" class="landing_logo" />
			@else
				<img src="{{ URL::to('public/frontend/images/loader-logo.png') }}" alt="loader-logo">
			@endif
		</div>
	</div>

<header class="header_sec">
	<div class="container">
		<nav class="navbar navbar-light nav_top">
			<a class="navbar-brand" href="javascript:;">
				@if(@$detail->landing_logo)
					<img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->landing_logo}}" alt="logo" class="landing_logo" />
				@else
					<img src="{{ URL::to('public/frontend/landing_page3/images/logo.png') }}" alt="logo" class="landing_logo" />
				@endif
			</a>
			{{-- <div class="header_sos">
				<ul>
					<li class="link_icons link_icons_linkedin" @if(!@$detail->link_linkedin) style="display:none;" @endif ><a href="{{ @$detail->link_linkedin ?? 'javascript:;' }}" target="_blank" class="color_primary_font color_secondary_hover"><i class="fab fa-linkedin-in"></i></a></li>
					<li class="link_icons link_icons_facebook" @if(!@$detail->link_facebook) style="display:none;" @endif ><a href="{{ @$detail->link_facebook ?? 'javascript:;' }}" target="_blank" class="color_primary_font color_secondary_hover"><i class="fab fa-facebook-f"></i></a></li>
					<li class="link_icons link_icons_twitter" @if(!@$detail->link_twitter) style="display:none;" @endif ><a href="{{ @$detail->link_twitter ?? 'javascript:;' }}" target="_blank" class="color_primary_font color_secondary_hover"><i class="fab fa-twitter"></i></a></li>
					<li class="link_icons link_icons_instagram" @if(!@$detail->link_instagram) style="display:none;" @endif ><a href="{{ @$detail->link_instagram ?? 'javascript:;' }}" target="_blank" class="color_primary_font color_secondary_hover"><i class="fab fa-instagram"></i></a></li>
					<li class="link_icons link_icons_pinterest" @if(!@$detail->link_pinterest) style="display:none;" @endif ><a href="{{ @$detail->link_pinterest ?? 'javascript:;' }}" target="_blank" class="color_primary_font color_secondary_hover"><i class="fab fa-pinterest"></i></a></li>
					<li class="link_icons link_icons_youtube" @if(!@$detail->link_youtube) style="display:none;" @endif ><a href="{{ @$detail->link_youtube ?? 'javascript:;' }}" target="_blank" class="color_primary_font color_secondary_hover"><i class="fab fa-youtube"></i></a></li>
                    <li class="link_icons link_icons_tiktok" @if(!@$detail->link_tiktok) style="display:none;" @endif ><a href="{{ @$detail->link_tiktok ?? 'javascript:;' }}" target="_blank" class="color_primary_font color_secondary_hover"><i class="fa-brands fa-tiktok"></i></a></li>
				</ul>
			</div> --}}
		</nav>
	</div>
</header>

<div class="bannersec">
	@if(@$detail->header_image)
		<img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->header_image}}" class="header_image" alt="">
	@else
		<img src="{{ URL::to('public/frontend/landing_page3/images/ban.jpg') }}" class="header_image" alt="">
	@endif
	<div class="banner_text">
		<div class="container">
			<div class="banner_box">
				<h5 class="header_line_1">{{@$detail->header_line_1  ?? 'Lorem ipsum dolor sit' }}</h5>
				<p class="header_line_2">{{@$detail->header_line_2  ?? 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.' }}</p>
				@if(@$detail->header_button_type == 'URL')
					<a href="{{@$detail->header_button_link ?? 'javascript:;'}}" class="page_btn header_btn header_button_text color_primary" @if(@$detail->header_button=='N') style="display:none" @endif>{{ @$detail->header_button_text ?? 'Contact Us'}}</a>
				@else
					@if(@$detail->lead_position == 'P')
						<a href="#" class="page_btn header_btn header_button_text color_primary" data-toggle="modal" data-target="#myModal" @if(@$detail->header_button=='N') style="display:none" @endif>{{ @$detail->header_button_text ?? 'Contact Us'}}</a>
					@else
						<a href="#contact-area" class="page_btn banBotArw header_btn header_button_text color_primary" @if(@$detail->header_button=='N') style="display:none" @endif>{{ @$detail->header_button_text ?? 'Contact Us'}}</a>
					@endif
				@endif

				@if(@$detail)
				<div class="counting-date header_reverse" @if(@$detail->header_reverse_counter == 'N') style="display: none;" @endif>
					<p>
                        <p class="header_reverse_text">Esta promoção termina em:<p>
                        <span class="header_counter"></span>
                    </p>
				</div>
				@endif
			</div>
		</div>
	</div>

</div>

<div class="whyus_sec" @if(@$detail->why_us_section == 'N') style="display: none;" @endif>
	<div class="container">
		<div class="page_hed">
			<h1 class="whyus_main_title color_primary_font">{{@$detail->why_us_heading ?? 'How it works'}}</h1>
			<p class="whyus_main_desc">{{@$detail->why_us_description ?? 'Your dream home is in good hands.'}}</p>
		</div>
		<div class="whyus_inr">
			<div class="row">
				@if(@$detail->getWhyUs && count(@$detail->getWhyUs) > 0)
					@foreach(@$detail->getWhyUs as $k=>$row)
						@php
							$icon = 'fas fa-file-invoice';
							if(($k+1)%3 == 0) $icon = 'fas fa-chalkboard-teacher';
							elseif(($k+1)%2 == 0) $icon = 'fas fa-handshake';
						@endphp
						<div class="col-sm-4 mb-3" id="why_box_{{@$k + 1}}">
							<div class="whyus_bx color_secondary">
								<em id="whyus_img_{{@$k + 1}}" class="s">
									@if(strpos( @$row->filetype, 'image' ) !== false)
										<img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$row->filename}}" alt="">
									@else
										<!-- {!! @$row->filename !!} -->
										<i class="{{@$icon}} color_primary_font"></i>
									@endif
								</em>
								<strong><a href="javascript:;" id="whyus_title_{{@$k + 1}}" class="color_tertiary_font color_primary_font_hover">{{@$row->title ?? 'certified contractors'}}</a></strong>
								<p id="whyus_desc_{{@$k + 1}}" class="color_tertiary_font">{{@$row->description ?? 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.'}}</p>
							</div>
						</div>
					@endforeach
				@else
					<div class="col-sm-4 mb-3" id="why_box_1">
						<div class="whyus_bx color_secondary">
							<em id="whyus_img_1" class="">
								<i class="fas fa-file-invoice color_primary_font"></i>
							</em>
							<strong><a href="javascript:;" id="whyus_title_1" class="color_tertiary_font color_primary_font_hover">certified contractors</a></strong>
							<p id="whyus_desc_1" class="color_tertiary_font">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.</p>
						</div>
					</div>
					<div class="col-sm-4 mb-3" id="why_box_2">
						<div class="whyus_bx color_secondary">
							<em id="whyus_img_2" class="">
								<i class="fas fa-handshake color_primary_font"></i>
							</em>
							<strong><a href="javascript:;" id="whyus_title_2" class="color_tertiary_font color_primary_font_hover">employees you can trust</a></strong>
							<p id="whyus_desc_2" class="color_tertiary_font">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.</p>
						</div>
					</div>
					<div class="col-sm-4 mb-3" id="why_box_3">
						<div class="whyus_bx color_secondary">
							<em id="whyus_img_3" class="">
								<i class="fas fa-chalkboard-teacher color_primary_font"></i>
							</em>
							<strong><a href="javascript:;" id="whyus_title_3" class="color_tertiary_font color_primary_font_hover">Your dream is our plan</a></strong>
							<p id="whyus_desc_3" class="color_tertiary_font">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.</p>
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>

<!--start about area-->
<section class="about_pro single_content_div" @if(@$detail->body_single_content == 'Y') @else style="display: none;" @endif>
	<div class="container">
		<div class="about_pro_inr">
			<div class="row">
				<div class="col-md-6">
					<div class="about_pro_img">
						<span class="single_content_file_div @if(@$detail->image_style=='C') square_image @endif" id="content_file_single">
							@if(@$detail->body_single_content_image)
								<img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->body_single_content_image}}" class="single_content_file">
							@else
								<img src="{{ URL::to('public/frontend/landing_page3/images/img.jpg') }}" class="single_content_file" alt="">
							@endif
						</span>
					</div>
				</div>
				<div class="col-md-6">
					<div class="about_pro_text">
						<h2 class="color_primary_font body_single_content_heading">{{ @$detail->body_single_content_heading ?? 'Lorem Ipsum Dolor Sit Amet' }}</h2>
						<p class="body_single_content_desc">{{ @$detail->body_single_content_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.' }}</p>
						<a href="{{ @$detail->body_single_content_button_link ?? 'javascript:;' }}" class="page_btn color_primary body_single_content_button body_single_content_button_text body_single_content_button_link" @if(@$detail->body_single_content_button == 'N') style="display: none;" @endif >
							{{ @$detail->body_single_content_button_text ?? 'Read More' }}
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="about_pro multiple_content_div" @if(@$detail->body_single_content == 'N') @else style="display: none;" @endif>
	<div class="container">
		<div class="about_pro_inr">
			<div class="row" id="content_div_1">
				<div class="col-md-6">
					<div class="about_pro_img single_content_file_div content_file_1 @if((@$detail->getContentDetails[0]->content_file_type == 'I' || @$detail->getContentDetails[0]->content_file_type==null) && @$detail->getContentDetails[0]->image_style == 'C') square_image @endif"  id="content_file_1">
						@if(@$detail->getContentDetails[0]->content_file)
							@if(@$detail->getContentDetails[0]->content_file_type == 'I')
								<img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->getContentDetails[0]->content_file}}" alt="">
							@else
								<iframe src="https://www.youtube.com/embed/{{@$detail->getContentDetails[0]->content_file}}" frameborder="0" allowfullscreen></iframe>
							@endif
						@else
							<img src="{{ URL::to('public/frontend/landing_page3/images/img.jpg') }}" alt="">
						@endif
					</div>
				</div>
				<div class="col-md-6">
					<div class="about_pro_text">
						<h2 class="color_primary_font content_heading_1">{{ @$detail->getContentDetails[0]->content_heading ?? 'Lorem Ipsum Dolor Sit Amet' }}</h2>
						<p class="content_desc_1">{{ @$detail->getContentDetails[0]->content_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'}}</p>
						<a href="{{ @$detail->getContentDetails[0]->content_button_link ?? 'javascript:;' }}" class="page_btn color_primary content_button_1"
							@if(@$detail->getContentDetails[0]->content_button == 'N') style="display: none;" @endif >
							{{ @$detail->getContentDetails[0]->content_button_text ?? 'Read More' }}
						</a>
					</div>
				</div>
			</div>
			<div class="row" id="content_div_2">
				<div class="col-md-6">
					<div class="about_pro_img single_content_file_div content_file_2 @if((@$detail->getContentDetails[1]->content_file_type == 'I' || @$detail->getContentDetails[1]->content_file_type==null) && @$detail->getContentDetails[1]->image_style == 'C') square_image @endif"  id="content_file_2">
						@if(@$detail->getContentDetails[1]->content_file)
							@if(@$detail->getContentDetails[1]->content_file_type == 'I')
								<img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->getContentDetails[1]->content_file}}" alt="">
								@else
								<iframe src="https://www.youtube.com/embed/{{@$detail->getContentDetails[1]->content_file}}" frameborder="0" allowfullscreen></iframe>
							@endif
						@else
							<img src="{{ URL::to('public/frontend/landing_page3/images/img.jpg') }}" alt="">
						@endif
					</div>
				</div>
				<div class="col-md-6">
					<div class="about_pro_text">
						<h2 class="color_primary_font content_heading_2">{{ @$detail->getContentDetails[1]->content_heading ?? 'Lorem Ipsum Dolor Sit Amet' }}</h2>
						<p class="content_desc_2">{{ @$detail->getContentDetails[1]->content_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.' }}</p>
						<a href="{{ @$detail->getContentDetails[1]->content_button_link ?? 'javascript:;' }}" class="page_btn color_primary content_button_2"
							@if(@$detail->getContentDetails[1]->content_button == 'Y') @else style="display: none;" @endif >
							{{ @$detail->getContentDetails[1]->content_button_text ?? 'Read More' }}
						</a>
					</div>
				</div>
			</div>
			<div class="row" id="content_div_3" @if(@$detail->getContentDetails[2] == null) style="display: none;" @endif >
				<div class="col-md-6">
					<div class="about_pro_img single_content_file_div content_file_3 @if((@$detail->getContentDetails[2]->content_file_type == 'I' || @$detail->getContentDetails[2]->content_file_type==null) && @$detail->getContentDetails[2]->image_style == 'C') square_image @endif"  id="content_file_3">
						@if(@$detail->getContentDetails[2]->content_file)
							@if(@$detail->getContentDetails[2]->content_file_type == 'I')
								<img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->getContentDetails[2]->content_file}}" alt="">
							@else
								<iframe src="https://www.youtube.com/embed/{{@$detail->getContentDetails[2]->content_file}}" frameborder="0" allowfullscreen></iframe>
							@endif
						@else
							<img src="{{ URL::to('public/frontend/landing_page3/images/img.jpg') }}" alt="">
						@endif
					</div>
				</div>
				<div class="col-md-6">
					<div class="about_pro_text">
						<h2 class="color_primary_font content_heading_3">{{ @$detail->getContentDetails[2]->content_heading ?? 'Lorem Ipsum Dolor Sit Amet' }}</h2>
						<p class="content_desc_3">{{ @$detail->getContentDetails[2]->content_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'}}</p>
						<a href="{{ @$detail->getContentDetails[2]->content_button_link ?? 'javascript:;' }}" class="page_btn color_primary content_button_3"
							@if(@$detail->getContentDetails[2]->content_button == 'Y') @else style="display: none;" @endif >
							{{ @$detail->getContentDetails[2]->content_button_text ?? 'Read More' }}
						</a>
					</div>
				</div>
			</div>
			<div class="row" id="content_div_4" @if(@$detail->getContentDetails[3] == null) style="display: none;" @endif >
				<div class="col-md-6">
					<div class="about_pro_img single_content_file_div content_file_4 @if((@$detail->getContentDetails[3]->content_file_type == 'I' || @$detail->getContentDetails[3]->content_file_type==null) && @$detail->getContentDetails[3]->image_style == 'C') square_image @endif"  id="content_file_4">
						@if(@$detail->getContentDetails[3]->content_file)
							@if(@$detail->getContentDetails[3]->content_file_type == 'I')
								<img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->getContentDetails[3]->content_file}}" alt="">
								@else
								<iframe src="https://www.youtube.com/embed/{{@$detail->getContentDetails[3]->content_file}}" frameborder="0" allowfullscreen></iframe>
							@endif
						@else
							<img src="{{ URL::to('public/frontend/landing_page3/images/img.jpg') }}" alt="">
						@endif
					</div>
				</div>
				<div class="col-md-6">
					<div class="about_pro_text">
						<h2 class="color_primary_font content_heading_4">{{ @$detail->getContentDetails[3]->content_desc ?? 'Lorem Ipsum Dolor Sit Amet' }}</h2>
						<p class="content_desc_4">{{ @$detail->getContentDetails[3]->content_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.' }}</p>
						<a href="{{ @$detail->getContentDetails[3]->content_button_link ?? 'javascript:;' }}" class="page_btn color_primary content_button_4"
							@if(@$detail->getContentDetails[3]->content_button == 'Y') @else style="display: none;" @endif >
							{{ @$detail->getContentDetails[3]->content_button_text ?? 'Read More' }}
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--end about area-->

<section class="produt_sec" id="products-initial" @if(@$detail->getProductDetails && count(@$detail->getProductDetails) > 0) style="display: none;" @endif>
	<div class="container">
		<div class="page_hed">
			<h3 class="product_section_heading color_primary_font">{{ @$detail->product_section_heading ?? 'Our Products' }}</h3>
			<p class="product_section_desc color_primary_font">{{ @$detail->product_section_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.' }}</p>
		</div>
		<div class="produt_inr">
			<div class="produt_slid" @if(@$detail->product_slider == 'N') style="display: none;" @endif >
				<div class="owl-carousel color_primary_owl">
					<div class="item">
						<div class="produt_img">
							<div class="file_div">
								<img src="{{ URL::to('public/frontend/landing_page3/images/pro1.jpg') }}" alt="">
							</div>
							<div class="produt_text">
								<h5><a href="javascript:;" class="product_heading color_primary_font color_tertary_font_hover">New Product</a></h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
								<span class="color_primary_font">R$800</span>
								<div class="dis_f">
									<a href="javascript:;" class="page_btn color_primary color_tertiary_hover"
									>Read</a>
									<div class="timeer_bx">
										<strong class="color_primary_border"><i class="fas fa-clock color_primary_font"></i> <em class="color_primary_font">0:00:00:00</em></strong>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="produt_img">
							<div class="file_div">
								<img src="{{ URL::to('public/frontend/landing_page3/images/pro1.jpg') }}" alt="">
							</div>
							<div class="produt_text">
								<h5><a href="javascript:;" class="product_heading color_primary_font color_tertary_font_hover">New Product</a></h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
								<span class="color_primary_font">R$800</span>
								<div class="dis_f">
									<a href="javascript:;" class="page_btn color_primary color_tertiary_hover"
									>Read</a>
									<div class="timeer_bx">
										<strong class="color_primary_border"><i class="fas fa-clock color_primary_font"></i> <em class="color_primary_font">0:00:00:00</em></strong>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="produt_img">
							<div class="file_div">
								<img src="{{ URL::to('public/frontend/landing_page3/images/pro1.jpg') }}" alt="">
							</div>
							<div class="produt_text">
								<h5><a href="javascript:;" class="product_heading color_primary_font color_tertary_font_hover">New Product</a></h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
								<span class="color_primary_font">R$800</span>
								<div class="dis_f">
									<a href="javascript:;" class="page_btn color_primary color_tertiary_hover"
									>Read</a>
									<div class="timeer_bx">
										<strong class="color_primary_border"><i class="fas fa-clock color_primary_font"></i> <em class="color_primary_font">0:00:00:00</em></strong>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="produt_img">
							<div class="file_div">
								<img src="{{ URL::to('public/frontend/landing_page3/images/pro1.jpg') }}" alt="">
							</div>
							<div class="produt_text">
								<h5><a href="javascript:;" class="product_heading color_primary_font color_tertary_font_hover">New Product</a></h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
								<span class="color_primary_font">R$800</span>
								<div class="dis_f">
									<a href="javascript:;" class="page_btn color_primary color_tertiary_hover"
									>Read</a>
									<div class="timeer_bx">
										<strong class="color_primary_border"><i class="fas fa-clock color_primary_font"></i> <em class="color_primary_font">0:00:00:00</em></strong>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="produt_img">
							<div class="file_div">
								<img src="{{ URL::to('public/frontend/landing_page3/images/pro1.jpg') }}" alt="">
							</div>
							<div class="produt_text">
								<h5><a href="javascript:;" class="product_heading color_primary_font color_tertary_font_hover">New Product</a></h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
								<span class="color_primary_font">R$800</span>
								<div class="dis_f">
									<a href="javascript:;" class="page_btn color_primary color_tertiary_hover"
									>Read</a>
									<div class="timeer_bx">
										<strong class="color_primary_border"><i class="fas fa-clock color_primary_font"></i> <em class="color_primary_font">0:00:00:00</em></strong>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="produt_img">
							<div class="file_div">
								<img src="{{ URL::to('public/frontend/landing_page3/images/pro1.jpg') }}" alt="">
							</div>
							<div class="produt_text">
								<h5><a href="javascript:;" class="product_heading color_primary_font color_tertary_font_hover">New Product</a></h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
								<span class="color_primary_font">R$800</span>
								<div class="dis_f">
									<a href="javascript:;" class="page_btn color_primary color_tertiary_hover"
									>Read</a>
									<div class="timeer_bx">
										<strong class="color_primary_border"><i class="fas fa-clock color_primary_font"></i> <em class="color_primary_font">0:00:00:00</em></strong>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- <div class="produt_slid row" @if(@$detail->product_slider == 'Y') style="display: none;" @endif >
					<div class="item col-md-4">
						<div class="produt_img">
							<div class="file_div">
								<img src="{{ URL::to('public/frontend/landing_page3/images/pro1.jpg') }}" alt="">
							</div>
							<div class="produt_text">
								<h5><a href="javascript:;" class="product_heading color_primary_font color_tertary_font_hover">New Product</a></h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
								<span class="color_primary_font">R$800</span>
								<div class="dis_f">
									<a href="javascript:;" class="page_btn color_primary color_tertiary_hover"
									>Read</a>
									<div class="timeer_bx">
										<strong class="color_primary_border"><i class="fas fa-clock color_primary_font"></i> <em class="color_primary_font">0:00:00:00</em></strong>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item col-md-4">
						<div class="produt_img">
							<div class="file_div">
								<img src="{{ URL::to('public/frontend/landing_page3/images/pro1.jpg') }}" alt="">
							</div>
							<div class="produt_text">
								<h5><a href="javascript:;" class="product_heading color_primary_font color_tertary_font_hover">New Product</a></h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
								<span class="color_primary_font">R$800</span>
								<div class="dis_f">
									<a href="javascript:;" class="page_btn color_primary color_tertiary_hover"
									>Read</a>
									<div class="timeer_bx">
										<strong class="color_primary_border"><i class="fas fa-clock color_primary_font"></i> <em class="color_primary_font">0:00:00:00</em></strong>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item col-md-4">
						<div class="produt_img">
							<div class="file_div">
								<img src="{{ URL::to('public/frontend/landing_page3/images/pro1.jpg') }}" alt="">
							</div>
							<div class="produt_text">
								<h5><a href="javascript:;" class="product_heading color_primary_font color_tertary_font_hover">New Product</a></h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
								<span class="color_primary_font">R$800</span>
								<div class="dis_f">
									<a href="javascript:;" class="page_btn color_primary color_tertiary_hover"
									>Read</a>
									<div class="timeer_bx">
										<strong class="color_primary_border"><i class="fas fa-clock color_primary_font"></i> <em class="color_primary_font">0:00:00:00</em></strong>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item col-md-4">
						<div class="produt_img">
							<div class="file_div">
								<img src="{{ URL::to('public/frontend/landing_page3/images/pro1.jpg') }}" alt="">
							</div>
							<div class="produt_text">
								<h5><a href="javascript:;" class="product_heading color_primary_font color_tertary_font_hover">New Product</a></h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
								<span class="color_primary_font">R$800</span>
								<div class="dis_f">
									<a href="javascript:;" class="page_btn color_primary color_tertiary_hover"
									>Read</a>
									<div class="timeer_bx">
										<strong class="color_primary_border"><i class="fas fa-clock color_primary_font"></i> <em class="color_primary_font">0:00:00:00</em></strong>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item col-md-4">
						<div class="produt_img">
							<div class="file_div">
								<img src="{{ URL::to('public/frontend/landing_page3/images/pro1.jpg') }}" alt="">
							</div>
							<div class="produt_text">
								<h5><a href="javascript:;" class="product_heading color_primary_font color_tertary_font_hover">New Product</a></h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
								<span class="color_primary_font">R$800</span>
								<div class="dis_f">
									<a href="javascript:;" class="page_btn color_primary color_tertiary_hover"
									>Read</a>
									<div class="timeer_bx">
										<strong class="color_primary_border"><i class="fas fa-clock color_primary_font"></i> <em class="color_primary_font">0:00:00:00</em></strong>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item col-md-4">
						<div class="produt_img">
							<div class="file_div">
								<img src="{{ URL::to('public/frontend/landing_page3/images/pro1.jpg') }}" alt="">
							</div>
							<div class="produt_text">
								<h5><a href="javascript:;" class="product_heading color_primary_font color_tertary_font_hover">New Product</a></h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
								<span class="color_primary_font">R$800</span>
								<div class="dis_f">
									<a href="javascript:;" class="page_btn color_primary color_tertiary_hover"
									>Read</a>
									<div class="timeer_bx">
										<strong class="color_primary_border"><i class="fas fa-clock color_primary_font"></i> <em class="color_primary_font">0:00:00:00</em></strong>
									</div>
								</div>
							</div>
						</div>
					</div>
			</div> -->
		</div>
	</div>
</section>

<section class="produt_sec" id="products-actual" @if(!@$detail->getProductDetails || count(@$detail->getProductDetails) == 0) style="display: none;" @endif>
	<div class="container">
		<div class="page_hed">
			<h3 class="product_section_heading color_primary_font">{{ @$detail->product_section_heading ?? 'Our Products' }}</h3>
			<p class="product_section_desc color_primary_font">{{ @$detail->product_section_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.' }}</p>
		</div>
		<div class="produt_inr with-slider pro-owl-carousel-center" @if(@$detail->product_slider == 'N') style="display: none;" @endif >
			<div class="produt_slid">
				<div class="product-actual-carousel owl-carousel   owl-carousel-center">
					@if(@$detail->getProductDetails && count(@$detail->getProductDetails)>0)
						@foreach(@$detail->getProductDetails as $k=>$product)
							<div class="item product_div_{{ @$k + 1 }} proddiv">
								<div class="product_detail prod-single three">
									<div class="produt_img">
										<div class="file_div">
											@if(@$product->product_file)
												@if(@$product->product_file_type == 'I')
													<img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$product->product_file}}" alt="" class="animation-jump product_file_{{ @$k + 1 }}">
												@elseif(@$product->product_file_type == 'Y')
													<img src="http://img.youtube.com/vi/{{@$product->product_file}}/0.jpg" alt="" class="animation-jump product_file_{{ @$k + 1 }}"/>
													{{-- <div class="playIcon animation-jump">
														<a href="http://www.youtube.com/watch?v={{@$product->product_file}}" class="fancybox-media">
															<img src="{{URL::asset('public/frontend/landing_page3/images/playIcon.png')}}" alt="" rel="media-gallery"></a>
													</div>
                                                    <div class="playIcon animation-jump"> --}}
                                                        <div class="video-player paly_btn vid-icon-you">
                                                            <a href="http://www.youtube.com/watch?v={{@$product->product_file}}" class="fancybox-media color_primary">
                                                                {{-- <img src="{{URL::asset('public/frontend/landing_page1/images/playIcon.png')}}" alt="" rel="media-gallery"> --}}
                                                                <i class="fa fa-play" aria-hidden="true" rel="media-gallery"></i>
                                                            </a>
                                                        </div>
                                                    {{-- </div> --}}
												@endif
                                                @else
                                                <img src="{{ URL::to('public/frontend/images/NoImage.jpg') }}" alt="" class="product_file_{{ @$k + 1 }}">
											@endif
										</div>
										<div class="produt_text">
											<h5><a href="{{ @$product->product_button_url ?? 'javascript:;' }}" class="product_heading_{{ $k + 1 }} color_primary_font color_tertary_font_hover">{{ @$product->product_heading ?? 'Product'. @$k + 1 }}</a></h5>
											<p class="product_desc_{{ $k + 1 }}">{{ @$product->product_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo' }}</p>
											<span class="product_price_{{ $k + 1 }} color_primary_font" @if(@$product->product_price_show == 'N') style="display: none;" @endif >R${{ @$product->product_price ?? '800' }}</span>
											<div class="dis_f">
												<a href="{{ @$product->product_button_url ?? 'javascript:;' }}" class="page_btn product_link_{{ $k + 1 }} color_primary color_tertiary_hover" @if(@$product->product_button == 'N') style="display:none;" @endif >{{@$product->product_button_caption ??'More'}}</a>
												<div></div>
												<div class="timeer_bx product_expiry_div_{{ $k + 1 }}">
													<strong class="color_primary_border"><i class="fas fa-clock color_primary_font"></i> <em class="product_expiry_{{ $k + 1 }} color_primary_font"></em></strong>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						@endforeach
					@else
						<div class="item product_div_1 proddiv">
							<div class="product_detail prod-single three">
								<div class="produt_img">
									<div class="file_div">
									</div>
									<div class="produt_text">
										<h5><a href="javascript:;" class="product_heading_1 color_primary_font color_tertary_font_hover">New Product</a></h5>
										<p class="product_desc_1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo</p>
										<span class="product_price_1 color_primary_font">R$800</span>
										<div class="dis_f">
											<a href="javascript:;" class="page_btn product_link_1 color_primary color_tertiary_hover">More</a>
                                            <div></div>
											<div class="timeer_bx product_expiry_div_1">
												<strong class="color_primary_border"><i class="fas fa-clock color_primary_font"></i> <em  style="font-style: normal !important;" class="product_expiry_1 color_primary_font">3:08:33:23</em></strong>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					@endif
				</div>
			</div>
		</div>
		<div class="produt_inr without-slider" @if(@$detail->product_slider == 'Y') style="display: none;" @endif >
			<div class="produt_slid row row_main">
				@if(@$detail->getProductDetails && count(@$detail->getProductDetails)>0)
					@foreach(@$detail->getProductDetails as $k=>$product)
						<div class="item col-lg-4 col-md-6 product_div_{{ @$k + 1 }} proddiv">
							<div class="product_detail prod-single three">
								<div class="produt_img">
									<div class="file_div">
										@if(@$product->product_file)
											@if(@$product->product_file_type == 'I')
												<img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$product->product_file}}" alt="" class="animation-jump product_file_{{ @$k + 1 }}">
											@elseif(@$product->product_file_type == 'Y')
												<img src="http://img.youtube.com/vi/{{@$product->product_file}}/0.jpg" alt="" class="animation-jump product_file_{{ @$k + 1 }}"/>
												{{-- <div class="playIcon animation-jump">
													<a href="http://www.youtube.com/watch?v={{@$product->product_file}}" class="fancybox-media">
														<img src="{{URL::asset('public/frontend/landing_page3/images/playIcon.png')}}" alt="" rel="media-gallery"></a>
												</div> --}}
                                                <div class="playIcon animation-jump">
                                                    <div class="video-player paly_btn vid-icon-you">
                                                        <a href="http://www.youtube.com/watch?v={{@$product->product_file}}" class="fancybox-media color_primary">
                                                            {{-- <img src="{{URL::asset('public/frontend/landing_page1/images/playIcon.png')}}" alt="" rel="media-gallery"> --}}
                                                            <i class="fa fa-play" aria-hidden="true" rel="media-gallery"></i>
                                                        </a>
                                                    </div>
                                                </div>
											@endif
                                            @else
                                            <img src="{{ URL::to('public/frontend/images/NoImage.jpg') }}" alt="" class="product_file_{{ @$k + 1 }}">
										@endif
									</div>
									<div class="produt_text">
										<h5><a href="{{ @$product->product_button_url ?? 'javascript:;' }}" class="product_heading_{{ $k + 1 }} color_primary_font color_tertary_font_hover">{{ @$product->product_heading ?? 'Product'. @$k + 1 }}</a></h5>
										<p class="product_desc_{{ $k + 1 }}">{{ @$product->product_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo' }}</p>
										<span class="product_price_{{ $k + 1 }} color_primary_font" @if(@$product->product_price_show == 'N') style="display: none;" @endif >R${{ @$product->product_price ?? '800' }}</span>
										<div class="dis_f">
											<a href="{{ @$product->product_button_url ?? 'javascript:;' }}" class="page_btn product_link_{{ $k + 1 }} color_primary color_tertiary_hover" @if(@$product->product_button == 'N') style="display:none;" @endif >{{@$product->product_button_caption ??'More'}}</a>
											<div></div>
											<div class="timeer_bx product_expiry_div_{{ $k + 1 }}">
												<strong class="color_primary_border"><i class="fas fa-clock color_primary_font"></i> <em class="product_expiry_{{ $k + 1 }} color_primary_font"></em></strong>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					@endforeach
				@else
					<div class="item col-lg-4 col-md-6 product_div_1 proddiv">
						<div class="product_detail prod-single three">
							<div class="produt_img">
								<div class="file_div">
								</div>
								<div class="produt_text">
									<h5><a href="javascript:;" class="product_heading_1 color_primary_font color_tertary_font_hover">New Product</a></h5>
									<p class="product_desc_1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo</p>
									<span class="product_price_1 color_primary_font">R$800</span>
									<div class="dis_f">
										<a href="javascript:;" class="page_btn product_link_1 color_primary color_tertiary_hover">More</a>
										<div></div>
										<div class="timeer_bx product_expiry_div_1">
											<strong class="color_primary_border"><i class="fas fa-clock color_primary_font"></i> <em  style="font-style: normal !important;" class="product_expiry_1 color_primary_font">3:08:33:23</em></strong>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
</section>

<section id="video-area" class="bg-gray ved_1 single_video color_secondary" @if(@$detail->body_single_video == 'N') style="display: none;" @endif >
	<div class="container">
		<div class="row">
			<!--start heading-->
			<div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1">
				<div class="page_hed">
					<h2 class="body_single_video_heading color_primary_font">{{ @$detail->body_single_video_heading ?? 'Get product more information from the video' }}</h2>
					<p class="body_single_video_desc">{{ @$detail->body_single_video_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.' }}</p>
				</div>
			</div>
			<!--end heading-->
		</div>
		<div class="row">
			<!--start video player-->
			<div class="col-lg-10 offset-lg-1">
				<div class="video-player-wrap ved_img">
					<div class="single_vdo">
						@if(@$detail->body_single_video_type == "V")
							<video src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->body_single_video_filename}}"></video>
						@elseif(@$detail->body_single_video_type == "I")
							<img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->body_single_video_filename}}" alt="">
						@else
							<img src="{{ URL::to('public/frontend/landing_page3/images/video-bg.jpg') }}" alt="">
						@endif
					</div>
					<div class="video-player paly_btn" @if(@$detail->body_single_video_type == "I") style="display: none;" @endif >
						<span class="before_vedio color_primary_border"></span>
						<a class="popup-video fancybox-media color_primary" href="javascript:;" onclick="play_vdo()"><i class="fa fa-play" aria-hidden="true"  rel="media-gallery"></i></a>
						<!-- <a class="popup-video fancybox-media" href="https://youtu.be/2HgrxH7N7tE"><i class="fa fa-play" aria-hidden="true"  rel="media-gallery"></i></a> -->
						<span class="after_vedio color_primary_border"></span>
					</div>
				</div>
			</div>
			<!--end video player-->
		</div>
	</div>
</section>

<div class="faq_sec" id="faq_sec" @if(@$detail->show_faq == 'N') style="display: none;" @endif >
	<div class="container">
		<div class="page_hed">
			<h3 class="faq_heading color_primary_font">{{@$detail->faq_heading ?? 'Faq'}}</h3>
			<p class="faq_description">{{@$detail->faq_description ?? 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.'}}</p>
		</div>
		<div class="top-main-profile">
			<div class="contact-wrap">
				<div id="accordion" class="accordionlanding1">
					<div class="card mb-0">
						@if(@$detail->getFaq && count(@$detail->getFaq) > 0)
							@foreach(@$detail->getFaq as $k=>$faq)
								<div class="faq_{{@$k + 1}} card-header collapsed color_secondary color_primary_border" data-toggle="collapse" href="#collapse{{@$k + 1}}">
									<a class="card-title faq_title_{{@$k + 1}}">
										{{@$faq->title ?? 'This is a simply dummy question text show here?'}}
									</a>
									<i class="fa fa-plus color_primary_font" aria-hidden="true"></i>
								</div>
								<div id="collapse{{@$k + 1}}" class="faq_{{@$k + 1}} card-body collapse" data-parent="#accordion" >
									<p class="faq_desc_{{@$k + 1 }} color_primary_border">
										{{@$faq->description ?? 'Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur mi gravida ac. Nunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet ante vitae ante facilisis lobortis temporamet ante vitae ante facilisis lobortis sit amet consectetur adipiscfiing. Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur gravidaNunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet ante vitae ante.'}}
									</p>
								</div>
							@endforeach
						@else
							<div class="faq_1 card-header collapsed color_secondary color_primary_border" data-toggle="collapse" href="#collapse1">
								<a class="card-title faq_title_1">
									This is a simply dummy question text show here?
								</a>
                                <i class="fa fa-plus color_primary_font" aria-hidden="true"></i>
							</div>
							<div id="collapse1" class="faq_1 card-body collapse" data-parent="#accordion">
								<p class="faq_desc_1 color_primary_border">
									Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur mi gravida ac. Nunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet ante vitae ante facilisis lobortis temporamet ante vitae ante facilisis lobortis sit amet consectetur adipiscfiing. Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur gravidaNunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet ante vitae ante.
								</p>
							</div>
						@endif
					</div>
				</div>
				<!-- <ul id="accordion" class="accordion">
                    <li>
                      <div class="link">This is a simply dummy question text show here?<i class="fa fa-plus"></i></div>
                      <ul class="submenu">
                        <p class="pm"> Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur mi gravida ac. Nunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet ante vitae ante facilisis lobortis temporamet ante vitae ante facilisis lobortis sit amet consectetur adipiscfiing.
                          Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur gravidaNunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet ante vitae ante. </p>
                      </ul>
                    </li>
                    <li>
                      <div class="link">This is simply dummy question here?<i class="fa fa-plus"></i></div>
                      <ul class="submenu" style="">
                        <p>Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur gravidaNunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet</p>
                      </ul>
                    </li>
                    <li>
                      <div class="link">This is simply dummy name question here?<i class="fa fa-plus"></i></div>
                      <ul class="submenu" style="">
                        <p>Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur gravidaNunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet</p>
                      </ul>
                    </li>

                    <li>
                      <div class="link">This is a simply dummy question text show here?<i class="fa fa-plus"></i></div>
                      <ul class="submenu">
                        <p class="pm"> Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur mi gravida ac. Nunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet ante vitae ante facilisis lobortis temporamet ante vitae ante facilisis lobortis sit amet consectetur adipiscfiing.
                        Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur gravidaNunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet ante vitae ante. </p>

                      </ul>

                    </li>

				</ul> -->
			</div>
		</div>
	</div>
</div>

<section class="contact-sec" id="contact-area" @if(@$detail->header_button_type == 'URL' || @$detail->lead_position == 'P') style="display: none;" @endif>
	<div class="container">
		<div class="row">
			<!--start contact form-->
			<!-- col-lg-8 col-md-7 -->
			<div class="col-md-12">
				<div class="page_hed">
					<h3 class="color_primary_font lead_section_heading">{{@$detail->lead_section_heading ?? 'Contact With Us'}}</h3>
					<p class="lead_section_desc">{{@$detail->lead_section_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.'}}</p>
				</div>
				<form id="lead_form_footer" action="{{route('landing.page.save.lead')}}" method="post" novalidate="true">
					@csrf
					<input type="text" name="landing_page_master_id" value="{{@$detail->id}}" hidden >
					<div class="contact-form row">
						<div class="col-sm-12 col-md-6 form-group lead_fname" @if(@$detail->lead_fname == 'N') style="display: none;" @endif>
                            <label for="fname" class="color_primary_font">@lang('client_site.first_name')*</label>
                            <input type="text" class="form-control color_secondary required" id="fname" name="fname" placeholder="@lang('client_site.first_name')*" >
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="col-sm-12 col-md-6 form-group lead_lname" @if(@$detail->lead_lname == 'N') style="display: none;" @endif>
                            <label for="lname" class="color_primary_font">@lang('client_site.last_name')*</label>
                            <input type="text" class="form-control color_secondary required" id="lname" name="lname" placeholder="@lang('client_site.last_name')*" >
                            <div class="help-block with-errors"></div>
                        </div>
						<div class="col-sm-12 col-md-6 form-group lead_country_code" @if(@$detail->lead_country_code == 'N') style="display: none;" @endif>
                            <label for="mobile" class="color_primary_font">@lang('site.country_code')*</label>
                            <input type="tel" class="form-control color_secondary required number" id="country_code" name="country_code" placeholder="@lang('site.country_code')*" >
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="col-sm-12 col-md-6 form-group lead_mobile" @if(@$detail->lead_mobile == 'N') style="display: none;" @endif>
                            <label for="mobile" class="color_primary_font">@lang('site.contact_form_mobile')*</label>
                            <input type="tel" class="form-control color_secondary required number" id="mobile" name="mobile" placeholder="@lang('site.contact_form_mobile')*" >
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="col-sm-12 col-md-6 form-group lead_email" @if(@$detail->lead_email == 'N') style="display: none;" @endif>
                            <label for="email" class="color_primary_font">@lang('site.contact_form_email')*</label>
                            <input type="email" class="form-control color_secondary required" id="email" name="email" placeholder="@lang('site.contact_form_email')*" >
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="col-sm-12 col-md-6 form-group lead_facebook" @if(@$detail->lead_facebook == 'N') style="display: none;" @endif>
                            <label for="facebook" class="color_primary_font">@lang('site.contact_form_facebook_link')</label>
                            <input type="url" class="form-control color_secondary" id="facebook_link" name="facebook_link" placeholder="@lang('site.contact_form_facebook_link')" >
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="col-sm-12 col-md-6 form-group lead_instagram" @if(@$detail->lead_instagram == 'N') style="display: none;" @endif>
                            <label for="instagram" class="color_primary_font">@lang('site.contact_form_instagram_link')</label>
                            <input type="url" class="form-control color_secondary" id="instagram_link" name="instagram_link" placeholder="@lang('site.contact_form_instagram_link')" >
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="col-sm-12 col-md-6 form-group lead_linkedin" @if(@$detail->lead_linkedin == 'N') style="display: none;" @endif>
                            <label for="linkedin" class="color_primary_font">@lang('site.contact_form_linkedink_link')</label>
                            <input type="url" class="form-control color_secondary" id="linkedin_link" name="linkedin_link" placeholder="@lang('site.contact_form_linkedink_link')" >
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="col-sm-12 col-md-6 form-group lead_website" @if(@$detail->lead_website == 'N') style="display: none;" @endif>
                            <label for="website" class="color_primary_font">@lang('site.contact_form_website')</label>
                            <input type="url" class="form-control color_secondary required" id="website" name="website" placeholder="@lang('site.contact_form_website')*" >
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="col-sm-12 col-md-6 form-group lead_address" @if(@$detail->lead_address == 'N') style="display: none;" @endif>
                            <label for="address" class="color_primary_font">@lang('site.street_address')</label>
                            <input type="text" class="form-control color_secondary required" id="address" name="address" placeholder="@lang('site.street_address')*" >
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="col-sm-12 col-md-6 form-group lead_city" @if(@$detail->lead_city == 'N') style="display: none;" @endif>
                            <label for="city" class="color_primary_font">@lang('site.city')</label>
                            <input type="text" class="form-control color_secondary required" id="city" name="city" placeholder="@lang('site.city')*" >
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="col-sm-12 col-md-6 form-group lead_state" @if(@$detail->lead_state == 'N') style="display: none;" @endif>
                            <label for="state" class="color_primary_font">@lang('site.state')</label>
                            <input type="text" class="form-control color_secondary required" id="state" name="state" placeholder="@lang('site.state')*" >
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="col-sm-12 col-md-6 form-group lead_postal_code" @if(@$detail->lead_postal_code == 'N') style="display: none;" @endif>
                            <label for="postal_code" class="color_primary_font">@lang('site.contact_form_postal_code')</label>
                            <input type="text" class="form-control color_secondary required" id="postal_code" name="postal_code" placeholder="@lang('site.contact_form_postal_code')*" >
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="col-md-12 form-group lead_message" @if(@$detail->lead_message == 'N') style="display: none;" @endif>
                            <label for="message" class="color_primary_font">@lang('site.message')*</label>
                            <textarea class="form-control color_secondary required" id="message" name="message" rows="8" placeholder="@lang('site.message')*" required ></textarea>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="col-md-12 form-group">
                            <label for="lead_privacy_policy_accepted">
                                <input type="checkbox" class="required" id="lead_privacy_policy_accepted" name="lead_privacy_policy_accepted">
                                @lang('client_site.i_agree_with_the') <span id="show_lead_footer_privacy_policy" class="lead_privacy_policy_link color_primary_font" data-toggle="modal" data-target="#myModal3">@lang('client_site.privacy_policy')</span>
                            </label>
                            <div id="lead_privacy_err_footer"></div>
                        </div>
                        <div class="col-md-12 form-group">
                            <button type="button" class="page_btn color_primary" id="lead_form_footer_btn">{{@$detail->lead_section_footer_submit_btn ?? 'Submit Now'}}</button>
                            <div class="messages"></div>
                        </div>
					</div>
				</form>
			</div>
			<!--end contact form-->
			<!--start contact information-->
			<!-- <div class="col-lg-4 col-md-5">
				<div class="cont-info">
					<h3 class="color_primary_font">Contact Info</h3>
					<div class="cont-info-single">
						<h5 class="color_primary_font"><i class="fas fa-map-marker-alt"></i> Address:</h5>
						<p>Lorem ipsum dolor sit amet</p>
					</div>
					<div class="cont-info-single">
						<h5 class="color_primary_font"><i class="fas fa-envelope-open"></i> Email:</h5>
						<p>contato@netbhe.com </p>
					</div>
					<div class="cont-info-single">
						<h5 class="color_primary_font"><i class="fas fa-phone"></i> Phone:</h5>
						<p>1934567890</p>
					</div>
				</div>
			</div> -->
			<!--end contact information-->
		</div>
	</div>
</section>


<footer class="foot_sec color_primary">
	<div class="container">
		<div class="foot_inr">
			<p class="color_secondary_font">© {{date('Y')}} &nbsp; <a href="{{route('home')}}" class="color_tertiary_font" target="_blank"> netbhe.com.br</a>&nbsp; Todos os Direitos Reservados</p>
			<div class="header_sos">
				<ul>
					<li class="link_icons link_icons_linkedin" @if(!@$detail->link_linkedin) style="display:none;" @endif ><a href="{{ @$detail->link_linkedin ?? 'javascript:;' }}" target="_blank" ><i class="fab fa-linkedin-in"></i></a></li>
					<li class="link_icons link_icons_facebook" @if(!@$detail->link_facebook) style="display:none;" @endif ><a href="{{ @$detail->link_facebook ?? 'javascript:;' }}" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
					<li class="link_icons link_icons_twitter" @if(!@$detail->link_twitter) style="display:none;" @endif ><a href="{{ @$detail->link_twitter ?? 'javascript:;' }}" target="_blank" ><i class="fab fa-twitter"></i></a></li>
					<li class="link_icons link_icons_instagram" @if(!@$detail->link_instagram) style="display:none;" @endif ><a href="{{ @$detail->link_instagram ?? 'javascript:;' }}" target="_blank" ><i class="fab fa-instagram"></i></a></li>
					<li class="link_icons link_icons_pinterest" @if(!@$detail->link_pinterest) style="display:none;" @endif ><a href="{{ @$detail->link_pinterest ?? 'javascript:;' }}" target="_blank" ><i class="fab fa-pinterest"></i></a></li>
					<li class="link_icons link_icons_youtube" @if(!@$detail->link_youtube) style="display:none;" @endif ><a href="{{ @$detail->link_youtube ?? 'javascript:;' }}" target="_blank" ><i class="fab fa-youtube"></i></a></li>
                    <li class="link_icons link_icons_tiktok" @if(!@$detail->link_tiktok) style="display:none;" @endif ><a href="{{ @$detail->link_tiktok ?? 'javascript:;' }}" target="_blank" ><i class="fa-brands fa-tiktok"></i></a></li>
				</ul>
			</div>
            <div class="foot-inr-sb">
                <img src="{{ URL::asset('public/frontend/landing_page4/images/logo.png') }}" alt="" class="landing_logo">
            </div>
		</div>
	</div>
</footer>


<!-- <div class="modal fade modal_frm" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Lorem ipsum</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          	<div class="from_panel">
          		<form>
				<div class="row">
					<div class="col-sm-6">
						<div class="formInrInput">
							<input type="text" placeholder="Name">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="formInrInput">
							<input type="email" placeholder="Email">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="formInrInput">
							<input type="tel" placeholder="Mobile">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="formInrInput">
							<input type="text" placeholder="Facebook link">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="formInrInput">
							<input type="text" placeholder=" Instagram link">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="formInrInput">
							<input type="text" placeholder=" Linkedin link">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="formInrInput">
							<input type="text" placeholder="website ">
						</div>
					</div>
					<div class="col-sm-12">
						<div class="formInrInput">
							<textarea placeholder="Address"></textarea>
						</div>
					</div>
				</div>
			</form>
          	</div>
        </div>

        <div class="modal-footer">
          <button type="button" class="page_btn">Submit</button>
        </div>

      </div>
    </div>
</div> -->

<div class="modal fade modal_sus" id="myModal">
    <div class="modal-dialog" id="form-modal">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">@lang('client_site.contact_form')</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          	<div class="from_panel">
          		<form action="{{route('landing.page.save.lead')}}" id="lead_form_popup" method="post">
				@csrf
				<input type="text" name="landing_page_master_id" value="{{@$detail->id}}" hidden >
				<div class="row">
					<div class="col-sm-6 lead_fname" @if(@$detail->lead_fname == 'N') style="display: none;" @endif>
						<div class="formInrInput">
							<input type="text" name="fname" class="required" placeholder="@lang('client_site.first_name')">
						</div>
					</div>
					<div class="col-sm-6 lead_lname" @if(@$detail->lead_lname == 'N') style="display: none;" @endif>
						<div class="formInrInput">
							<input type="text" name="lname" class="required" placeholder="@lang('client_site.last_name')">
						</div>
					</div>
					<div class="col-sm-6 lead_country_code" @if(@$detail->lead_country_code == 'N') style="display: none;" @endif>
						<div class="formInrInput">
							<input type="number" name="country_code" class="required number" placeholder="@lang('site.country_code')">
						</div>
					</div>
					<div class="col-sm-6 lead_mobile" @if(@$detail->lead_mobile == 'N') style="display: none;" @endif>
						<div class="formInrInput">
							<input type="tel" name="mobile" class="required number" placeholder="@lang('site.contact_form_mobile')">
						</div>
					</div>
					<div class="col-sm-6 lead_email" @if(@$detail->lead_email == 'N') style="display: none;" @endif>
						<div class="formInrInput">
							<input type="email" name="email" class="required" placeholder="@lang('site.contact_form_email')">
						</div>
					</div>
					<div class="col-sm-6 lead_address" @if(@$detail->lead_address == 'N') style="display: none;" @endif>
						<div class="formInrInput">
							<input type="text" name="address" class="required" placeholder="@lang('site.street_address')">
						</div>
					</div>
					<div class="col-sm-6 lead_city" @if(@$detail->lead_city == 'N') style="display: none;" @endif>
						<div class="formInrInput">
							<input type="text" name="city" class="required" placeholder="@lang('site.city')">
						</div>
					</div>
					<div class="col-sm-6 lead_state" @if(@$detail->lead_state == 'N') style="display: none;" @endif>
						<div class="formInrInput">
							<input type="text" name="state" class="required" placeholder="@lang('site.state')">
						</div>
					</div>
					<div class="col-sm-6 lead_website" @if(@$detail->lead_website == 'N') style="display: none;" @endif>
						<div class="formInrInput">
							<input type="url" name="website" class="required" placeholder="@lang('site.contact_form_website')">
						</div>
					</div>
					<div class="col-sm-6 lead_facebook" @if(@$detail->lead_facebook == 'N') style="display: none;" @endif>
						<div class="formInrInput">
							<input type="url" name="facebook_link" placeholder="@lang('site.contact_form_facebook_link')">
						</div>
					</div>
					<div class="col-sm-6 lead_instagram" @if(@$detail->lead_instagram == 'N') style="display: none;" @endif>
						<div class="formInrInput">
							<input type="url" name="instagram_link" placeholder="@lang('site.contact_form_instagram_link')">
						</div>
					</div>
					<div class="col-sm-6 lead_linkedin" @if(@$detail->lead_linkedin == 'N') style="display: none;" @endif>
						<div class="formInrInput">
							<input type="url" name="linkedin_link" placeholder="@lang('site.contact_form_instagram_link')">
						</div>
					</div>
					<div class="col-sm-12 lead_message" @if(@$detail->lead_message == 'N') style="display: none;" @endif>
						<div class="formInrInput">
							<textarea name="message" class="required" placeholder="@lang('site.message')"></textarea>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="formInrInput">
							<label for="lead_privacy_policy_accepted">
								<input type="checkbox" class="required" id="lead_privacy_policy_accepted" name="lead_privacy_policy_accepted">
								@lang('client_site.i_agree_with_the') <span id="show_lead_popup_privacy_policy" class="lead_privacy_policy_link color_primary_font">@lang('client_site.privacy_policy')</span>
							</label>
							<div id="lead_privacy_err_popup"></div>
						</div>
					</div>
				</div>
			</form>
          	</div>
        </div>

        <div class="modal-footer">
          <button type="button" id="lead_form_popup_btn" class="page_btn color_primary color_tertiary_hover">{{@$detail->lead_section_footer_submit_btn ?? 'Submit Now'}} <i class="fa fa-paper-plane" aria-hidden="true"></i></button>
        </div>

      </div>
    </div>
	<div class="modal-dialog" id="success-modal" style="display:none;">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<div class="modal-body color_primary">
				<div class="modal_sus_bdy">
					<h2 class="color_secondary_font" id="lead_success_heading_popup">{{@$detail->lead_success_heading ?? 'Heading'}}</h2>
					<p class="color_secondary_font" id="lead_success_desc_popup">{{@$detail->lead_success_desc ?? 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem dolor vero mollitia nam molestiae neque, maiores facilis voluptate eligendi ducimus id fugiat assumenda perspiciatis optio dignissimos eos nemo ipsum architecto unde ea tempore. Ratione eaque exercitationem magnam blanditiis, tenetur minima incidunt maxime sint earum est ut necessitatibus odit tempore vero. Recusandae dolores voluptatem officiis dolorum? Eum veniam minima ducimus odio non, dolore, nulla ullam a nemo cupiditate quae praesentium? Dolorem voluptatem consequuntur provident sapiente magnam consectetur ut omnis saepe libero tenetur id, excepturi nobis odit. Voluptate assumenda tenetur odit neque culpa voluptatibus consectetur, quo sed quis debitis numquam, inventore similique. Et reiciendis distinctio libero itaque ab, culpa voluptatem similique facere aliquid dolorum sint soluta minus deleniti? Commodi, eaque itaque. Porro?'}}</p>
					<div class="net_sus_btn">
						<button type="button" class="btn_pro color_secondary" id="lead_success_ok_popup" @if(@$detail->lead_success_button == 'N') style="display:none;" @endif >{{@$detail->lead_success_button_caption ?? 'OK'}}</button>
					</div>
				</div>
			</div>
		</div>
    </div>
	<div class="modal-dialog" id="privacy-modal" style="display: none;">
		<div class="modal-content">
			<button type="button" class="close privacy_back color_primary color_tertiary_hover">Back</button>
			<div class="modal-body">
				<h2 class="color_primary_font">@lang('client_site.privacy_policy')</h2>
				<p id="lead_privacy_desc">{{@$detail->lead_privacy_policy ?? \Lang::get('client_site.privacy_policy')}}</p>
			</div>
		</div>
	</div>
</div>


<div class="modal fade modal_sus" id="myModal2">
	<div class="modal-dialog">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<div class="modal-body color_primary">
				<div class="modal_sus_bdy">
					<h2 class="color_secondary_font" id="lead_success_heading_footer">{{@$detail->lead_success_heading ?? 'Heading'}}</h2>
					<p class="color_secondary_font" id="lead_success_desc_footer">{{@$detail->lead_success_desc ?? 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem dolor vero mollitia nam molestiae neque, maiores facilis voluptate eligendi ducimus id fugiat assumenda perspiciatis optio dignissimos eos nemo ipsum architecto unde ea tempore. Ratione eaque exercitationem magnam blanditiis, tenetur minima incidunt maxime sint earum est ut necessitatibus odit tempore vero. Recusandae dolores voluptatem officiis dolorum? Eum veniam minima ducimus odio non, dolore, nulla ullam a nemo cupiditate quae praesentium? Dolorem voluptatem consequuntur provident sapiente magnam consectetur ut omnis saepe libero tenetur id, excepturi nobis odit. Voluptate assumenda tenetur odit neque culpa voluptatibus consectetur, quo sed quis debitis numquam, inventore similique. Et reiciendis distinctio libero itaque ab, culpa voluptatem similique facere aliquid dolorum sint soluta minus deleniti? Commodi, eaque itaque. Porro?'}}</p>
					<div class="net_sus_btn">
						<button type="button" class="btn_pro color_secondary" id="lead_success_ok_footer" @if(@$detail->lead_success_button == 'N') style="display:none;" @endif >{{@$detail->lead_success_button_caption ?? 'OK'}}</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade modal_sus modal_privacy" id="myModal3">
	<div class="modal-dialog">
		<div class="modal-content">
			<button type="button" class="close color_primary_hover" data-dismiss="modal">&times;</button>
			<div class="modal-body">
				<h2 class="color_primary_font">@lang('client_site.privacy_policy')</h2>
				<p id="lead_privacy_desc">{{@$detail->lead_privacy_policy ?? \Lang::get('client_site.privacy_policy')}}</p>
			</div>
		</div>
	</div>
</div>



<!-- Bootstrap core JavaScript -->
<script src="{{URL::asset('public/frontend/landing_page3/js/jquerymin.js')}}"></script>
<script src="{{URL::asset('public/frontend/landing_page3/js/bootstrap.js')}}"></script>
<script src="{{URL::asset('public/frontend/landing_page3/js/custom.js')}}"></script>
<script src="{{URL::asset('public/frontend/landing_page3/js/puchmenu.js')}}"></script>
<script src="{{URL::asset('public/frontend/landing_page3/js/fancyjs.js')}}"></script>
<script src="{{URL::asset('public/frontend/landing_page3/js/jquery.fancybox.js')}}"></script>
<script src="{{URL::asset('public/frontend/landing_page3/js/jquery.fancybox.pack.js')}}"></script>
<script src="{{URL::asset('public/frontend/landing_page3/js/jquery.mousewheel-3.0.6.pack.js')}}"></script>
<script src="{{URL::asset('public/frontend/landing_page3/js/jquery.fancybox-media.js')}}"></script>
<script src="{{URL::asset('public/frontend/landing_page3/js/owl.carousel.js')}}"></script>
<script src="{{URL::to('public/admin/js/jquery.validate.js') }}"></script>

<script>
	$('a.banBotArw[href^="#contact-area"]').on('click', function(event) {

		var target = $( $(this).attr('href') );

		if( target.length ) {
			event.preventDefault();
			$('html, body').animate({
				scrollTop: target.offset().top
			}, 3000);
		}

	});
</script>
<script>
    var timer = 0;
	var landing_prod_timer = [];
	var detail = null;
    let hexToRgb= c=> `rgb(${c.match(/\w\w/g).map(x=>+`0x${x}`)})`;

    $(document).ready(function(){
        detail = {!! json_encode(@$detail) !!}
        console.log(detail);

        // $('#contact_form').validate();

        setTimeout(function() {
            $('#landing_preloader').fadeOut('slow', function() {
                $(this).remove();
            });
        }, 2000);
        if(detail !== null){
            changeColours(detail.color_primary, detail.color_secondary, detail.color_tertiary);
            if(detail.header_reverse_counter == 'Y' || detail.header_reverse_counter == 'N'){
				if(new Date() > new Date(detail.header_counter_date)){
					$('.header_reverse_text').text("Promoção terminou");
					$('.header_counter').empty();
				} else {
					timer = setInterval(function() {
						var html = timeDiffCalc( new Date(), new Date(detail.header_counter_date) );
						if(html == 0){
							clearInterval(timer);
							$('.header_reverse_text').text("Promoção terminou");
                    		$('.header_counter').empty();
						}
						$('.header_counter').html(html);
						if ( window.location !== window.parent.location ){
							window.top.callChangeTimerColors();
						} else{
							changeTimerColours(detail.color_primary, detail.color_secondary, detail.color_tertiary);
						}
					}, 1000);
					// changeColours(detail.color_primary, detail.color_secondary, detail.color_tertiary);
				}
            }
        }
        if(detail.get_product_details.length > 0){
            // for(i=0; i<detail.get_product_details.length; i++){

            //     console.log(i);
            //     var id = parseInt(i)+1;
            //     landing_prod_timer[id] = setInterval(function() {
            //         console.log(i);
            //         console.log(detail.get_product_details);
            //         var html = prodTimeDiffCalc( new Date(), new Date(detail.get_product_details[i].product_exp_date) );
            //         if(html == 0){
            //             $('.product_expiry_'+id).html("Promoção terminou");
            //             clearInterval(landing_prod_timer[id]);
            //         } else {
            //             $('.product_expiry_'+id).html(html);
            //         }
            //     }, 1000);
            // }
            $.each(detail.get_product_details, function(i, tim){
                var id = parseInt(i)+1;
                console.log(i);
                landing_prod_timer[id] = setInterval(function() {
                    var html = prodTimeDiffCalc( new Date(), new Date(detail.get_product_details[i].product_exp_date) );
                    if(html == 0){
                        $('.product_expiry_'+id).html("Promoção terminou");
                        clearInterval(landing_prod_timer[id]);
                    } else {
                        $('.product_expiry_'+id).html(html);
                    }
                }, 1000);
            });
        }
    });
    function timeDiffCalc(dateFuture, dateNow) {
        if(Math.abs(dateFuture - dateNow) == 0){
            return 0;
        } else {
            let diffInMilliSeconds = Math.abs(dateFuture - dateNow) / 1000;
            const days = Math.floor(diffInMilliSeconds / 86400);
            diffInMilliSeconds -= days * 86400;
            const hours = Math.floor(diffInMilliSeconds / 3600) % 24;
            diffInMilliSeconds -= hours * 3600;
            const minutes = Math.floor(diffInMilliSeconds / 60) % 60;
            diffInMilliSeconds -= minutes * 60;
            const seconds = Math.round(diffInMilliSeconds);

            let difference = '<ul class="counter_ul">';
            if (days > 0) { difference += (days === 1) ? `<li class="header-timer-hours"><p class="counter_num color_secondary">${days}</p><p class="color_secondary_font">dias</p></li>` : `<li class="header-timer-hours"><p class="counter_num color_secondary">${days}</p><p class="color_secondary_font">dias</p></li>`; }

            difference += (hours < 10 ) ? `<li class="header-timer-hours"><p class="counter_num color_secondary">0${hours}</p><p class="color_secondary_font">horas</p></li>` : `<li class="header-timer-hours"><p class="counter_num color_secondary">${hours}</p><p class="color_secondary_font">horas</p></li>`;
            difference += (minutes < 10 ) ? `<li class="header-timer-hours"><p class="counter_num color_secondary">0${minutes}</p><p class="color_secondary_font">minutos</p></li>` : `<li class="header-timer-hours"><p class="counter_num color_secondary">${minutes}</p><p class="color_secondary_font">minutos</p></li>`;
            difference += (seconds < 10 ) ? `<li class="header-timer-hours"><p class="counter_num color_secondary">0${seconds}</p><p class="color_secondary_font">segundos</p></li>` : `<li class="header-timer-hours"><p class="counter_num color_secondary">${seconds}</p><p class="color_secondary_font">segundos</p></li>`;
            difference += "</ul>";
            return difference;
        }
    }
    function prodTimeDiffCalc(dateFuture, dateNow) {
        if(Math.abs(dateFuture - dateNow) == 0){
            return 0;
        } else {
            let diffInMilliSeconds = Math.abs(dateFuture - dateNow) / 1000;
            const days = Math.floor(diffInMilliSeconds / 86400);
            diffInMilliSeconds -= days * 86400;
            const hours = Math.floor(diffInMilliSeconds / 3600) % 24;
            diffInMilliSeconds -= hours * 3600;
            const minutes = Math.floor(diffInMilliSeconds / 60) % 60;
            diffInMilliSeconds -= minutes * 60;
            const seconds = Math.round(diffInMilliSeconds);

            let difference = '';
            if (days > 0) { difference += (days === 1) ? `${days}:` : `${days}:`; }

            difference += (hours < 10 ) ? `0${hours}:` : `${hours}:`;
            difference += (minutes < 10 ) ? `0${minutes}:` : `${minutes}:`;
            difference += (seconds < 10 ) ? `0${seconds}` : `${seconds}`;
            return difference;
        }
    }
    function changeColours(primary, secondary, tertiary){
        console.log("Changing colours");
        var val = primary;
        var rgb = hexToRgb(val);
        var rgba = [rgb.slice(0, rgb.length-1), ", 0.3", rgb.slice(rgb.length-1)].join('');
        rgba = rgba.replace('rgb','rgba');
		var clr = "";

        $('.color_primary').css('background', val);
        $('.color_primary_font').css('color', val);
        $('.color_primary_border').css('border-color', val);
        $('.color_primary_hover').hover(function(){
            $(this).css('background', val);
            $(this).css('color', '#fff');
        }, function(){
            $(this).css('background', 'transparent');
            $(this).css('color', val);
        });
        $('.color_primary_font_hover').hover(function(){
			clr = $(this).css('color');
            $(this).css('color', val);
        },function(){
            $(this).css('color', clr!="" ? clr : '#000');
			clr = "";
        });
        $('.color_primary_owl').find('.owl-prev').css('color', val).hover(function(){
            $(this).css('background', val);
            $(this).css('color', '#fff');
        }, function(){
            $(this).css('background', 'transparent');
            $(this).css('color', val);
        });
        $('.color_primary_owl').find('.owl-next').css('color', val).hover(function(){
            $(this).css('background', val);
            $(this).css('color', '#fff');
        }, function(){
            $(this).css('background', 'transparent');
            $(this).css('color', val);
        });
        $('.color_primary_light').css('background', rgba);

        $('.color_secondary').css('background', secondary);
        $('.color_secondary_font').css('color', secondary);
        $('.color_secondary_border').css('border-color', secondary);
		$('.color_secondary_font_hover').hover(function(){
			clr = $(this).css('color');
            $(this).css('color', secondary);
        },function(){
            $(this).css('color', clr!="" ? clr : '#000');
			clr = "";
        });

        $('.color_tertiary').css('background', tertiary);
        $('.color_tertiary_font').css('color', tertiary);
		$('.color_tertiary_hover').hover(function(){
			backclr = $(this).css('background-color');
			console.log(backclr);
			$(this).css('background', tertiary);
		}, function(){
			$(this).css('background', backclr!="" ? backclr : 'transparent');
			backclr = "";
		});
    }
    function changeTimerColours(primary, secondary, tertiary){
		$('.counter_num').css('background', primary);
        $('.header-timer-hours').find('.color_primary_font').css('color', primary);
	}
	function clearTheTimer(){
		clearInterval(timer);
	}
    function clearProductTimer(num){
		clearInterval(landing_prod_timer[num]);
	}
	$(document).delegate('.accordionlanding1 .card-header', 'click', function(){
		console.log("Clicked");
		if($(this).hasClass('collapsed')){
			console.log("opened");
			$(this).removeClass('color_primary');
			$(this).addClass('color_secondary');
			$(this).removeClass('color_primary');
			$(this).removeClass('color_tertiary_border');
			$(this).addClass('color_primary_border');
			$(this).css('color','#333');
            $(this).find('i').removeClass('fa-minus');
			$(this).find('i').addClass('fa-plus');
            if( !$(this).find('i').hasClass('color_primary_font') ) $(this).find('i').addClass('color_primary_font');
		} else {
            $('.accordionlanding1 .card-header').each(function(i,elem){
                $(elem).removeClass('color_primary');
				$(elem).addClass('color_secondary');
				$(elem).removeClass('color_tertiary_border');
				$(elem).addClass('color_primary_border');
                $(elem).css('color','#333');
                $(this).find('i').removeClass('fa-minus');
				$(this).find('i').addClass('fa-plus');
				if( !$(this).find('i').hasClass('color_primary_font') ) $(this).find('i').addClass('color_primary_font');
            });
            console.log("collapsed");
			$(this).removeClass('color_secondary');
			$(this).addClass('color_primary');
			$(this).removeClass('color_primary_border');
			$(this).addClass('color_tertiary_border');
			$(this).css('color','#fff');
            $(this).find('i').removeClass('fa-plus');
            $(this).find('i').addClass('fa-minus');
			$(this).find('i').removeClass('color_primary_font');
			$(this).find('i').css('color','#fff');
		}
		if ( window.location !== window.parent.location ){
			window.top.callChangeColours();
		} else{
			changeColours(detail.color_primary, detail.color_secondary, detail.color_tertiary);
		}
	});
    $('.paly_btn').click(function (){
		$('.single_vdo video').attr('controls',true);
		$('.single_vdo video')[0].play();
		$('.paly_btn').hide();
	});
	var validator_popup = $("#lead_form_footer").validate({
		rules: {
			mobile: {
				minlength:8,
				maxlength:13,
			}
		},
        messages: {
            lead_privacy_policy_accepted: {
                required : "{{\Lang::get('client_site.lead_privacy_error')}}."
            },
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "lead_privacy_policy_accepted") {
                $("#lead_privacy_err_footer").append(error);
            } else {
                error.insertAfter(element);
            }
        }
	});
	var validator_footer = $("#lead_form_popup").validate({
		rules: {
			mobile: {
				minlength:8,
				maxlength:13,
			}
		},
        messages: {
            lead_privacy_policy_accepted: {
                required : "{{\Lang::get('client_site.lead_privacy_error')}}."
            },
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "lead_privacy_policy_accepted") {
                $("#lead_privacy_err_popup").append(error);
            } else {
                error.insertAfter(element);
            }
        }
	});
	$('#lead_form_footer_btn').click(function(){
		if ( window.location !== window.parent.location ){
			console.log("In iframe");
			$('#myModal2').modal();
            $('#myModal2').modal("show");
		} else{
			if($('#lead_form_footer').valid()){
				var formData = $("#lead_form_footer").serialize();
				$.ajax({
					url: "{{ route('landing.page.save.lead') }}",
					method: 'post',
					dataType: 'json',
					data: formData,
					async: false,
					success: function(res){
						console.log(res);
						if(res.status == 'success'){
							$('#myModal2').modal();
                            $('#myModal2').modal("show");
							$('#success-modal').fadeIn("slow");
						} else {
							toastr.error(res.error);
						}
					},
					error: function(err){
						console.log(err);
					},
				});
			}
		}
	});
    $('#lead_form_popup_btn').click(function(){
		if ( window.location !== window.parent.location ){
			console.log("In iframe");
			$('#form-modal').fadeOut("slow");
			$('#success-modal').fadeIn("slow");
		} else {
			if($('#lead_form_popup').valid()){
				var formData = $("#lead_form_popup").serialize();
				$.ajax({
					url: "{{ route('landing.page.save.lead') }}",
					method: 'post',
					dataType: 'json',
					data: formData,
					async: false,
					success: function(res){
						console.log(res);
						if(res.status == 'success'){
							$('#lead_form_popup')[0].reset();
							$('#form-modal').fadeOut("slow");
							$('#success-modal').fadeIn("slow");
						} else {
							toastr.error(res.error);
						}
					},
					error: function(err){
						console.log(err);
					},
				});
			}
		}
	});
	$('#lead_success_ok_footer').click(function(){
		// clearTimeout(leadSuccessTimout);
		$("#myModal2").modal();
		$("#myModal2").modal("hide");
		validator_footer.resetForm();
		$('#lead_form_footer')[0].reset();
        window.location.href = "{{@$detail->lead_download_link}}";
	});
    $('#lead_success_ok_popup').click(function(){
		// clearTimeout(leadSuccessTimout);
		$('#form-modal').fadeIn("slow");
		$('#success-modal').fadeOut("slow");
		$('#lead_form_popup')[0].reset();
		$("#myModal").modal();
		$("#myModal").modal("hide");
		validator_popup.resetForm();
        window.location.href = "{{@$detail->lead_download_link}}";
	});
	$('#show_lead_popup_privacy_policy').click(function(){
		$('#form-modal').hide();
		$('#privacy-modal').show();
    });
	$('.privacy_back').click(function(){
		$('#privacy-modal').hide();
		$('#form-modal').show();
	});
</script>
<script type="text/javascript">
    var itemsMainDiv = ('.MultiCarousel');
    var itemsDiv = ('.MultiCarousel-inner');
    var itemWidth = "";
    $(document).ready(function () {

        var count = $('.proddiv').length;
        if(count < 4){
            $('.leftLst').hide();
            $('.rightLst').hide();
        } else {
            $('.leftLst').show();
            $('.rightLst').show();
        }

        $('.leftLst, .rightLst').click(function () {
            var condition = $(this).hasClass("leftLst");
            if (condition)
                click(0, this);
            else
                click(1, this)
        });

        ResCarouselSize();

        $(window).resize(function () {
            ResCarouselSize();
        });

    });
    //this function define the size of the items
    function ResCarouselSize() {
        var incno = 0;
        var dataItems = ("data-items");
        var itemClass = ('.item');
        var id = 0;
        var btnParentSb = '';
        var itemsSplit = '';
        var sampwidth = $(itemsMainDiv).width();
        var bodyWidth = $('body').width();
        $(itemsDiv).each(function () {
            id = id + 1;
            var itemNumbers = $(this).find(itemClass).length;
            btnParentSb = $(this).parent().attr(dataItems);
            itemsSplit = btnParentSb.split(',');
            $(this).parent().attr("id", "MultiCarousel" + id);


            if (bodyWidth >= 1200) {
                incno = itemsSplit[2];
                itemWidth = sampwidth / incno;
            }
            else if (bodyWidth >= 992) {
                incno = itemsSplit[2];
                itemWidth = sampwidth / incno;
            }
            else if (bodyWidth >= 768) {
                incno = itemsSplit[1];
                itemWidth = sampwidth / incno;
                console.log(itemWidth);
            }
            else {
                incno = itemsSplit[0];
                itemWidth = sampwidth / incno;
            }
            $(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
            $(this).find(itemClass).each(function () {
                $(this).outerWidth(itemWidth);
            });

            $(".leftLst").addClass("over");
            $(".rightLst").removeClass("over");

        });
    }
    //this function used to move the items
    function ResCarousel(e, el, s) {
        var leftBtn = ('.leftLst');
        var rightBtn = ('.rightLst');
        var translateXval = '';
        var divStyle = $(el + ' ' + itemsDiv).css('transform');
        var values = divStyle.match(/-?[\d\.]+/g);
        var xds = Math.abs(values[4]);
        if (e == 0) {
            translateXval = parseInt(xds) - parseInt(itemWidth * s);
            $(el + ' ' + rightBtn).removeClass("over");

            if (translateXval <= itemWidth / 2) {
                translateXval = 0;
                $(el + ' ' + leftBtn).addClass("over");
            }
        }
        else if (e == 1) {
            var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
            translateXval = parseInt(xds) + parseInt(itemWidth * s);
            $(el + ' ' + leftBtn).removeClass("over");

            if (translateXval >= itemsCondition - itemWidth / 2) {
                translateXval = itemsCondition;
                $(el + ' ' + rightBtn).addClass("over");
            }
        }
        $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
    }
    //It is used to get some elements from btn
    function click(ell, ee) {
        var Parent = "#" + $(ee).parent().attr("id");
        var slide = $(Parent).attr("data-slide");
        ResCarousel(ell, Parent, slide);
    }
</script>

<script>
	$(function() {
		var Accordion = function(el, multiple) {
			this.el = el || {};
			this.multiple = multiple || false;
			// Variables privadas
			var links = this.el.find('.link');
			// Evento
			links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
		}

		Accordion.prototype.dropdown = function(e) {
			var $el = e.data.el;
			$this = $(this),
			$next = $this.next();
			if($this.parent().hasClass('open')) {
			$next.slideUp();
			$this.parent().removeClass('open');
			console.log($this.children("i.fa"));
			$this.children("i.fa").removeClass("fa-minus").addClass("fa-plus");
			} else {
			$next.slideDown();
			$this.parent().addClass('open');
			$this.children("i.fa").removeClass("fa-plus").addClass("fa-minus");
			}
			if (!e.data.multiple) {
			$el.find('.submenu').not($next).slideUp().parent().removeClass('open');
			console.log($el.find('.link').not($this).children("i.fa").removeClass().addClass("fa fa-plus"))
			};
		}
		var accordion = new Accordion($('#accordion, #accordion1'), false);
	});
</script>

</body>
</html>
