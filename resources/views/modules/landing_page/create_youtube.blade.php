@extends('layouts.app')
@section('title')
    @lang('site.welcome_to_dashboard')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
<link href="{{ URL::to('public/frontend/css/course_style.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ URL::to('public/frontend/css/course_responsive.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ URL::to('public/frontend/css/course_bootstrap.min') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::to('public/frontend/icofont/icofont.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::to('public/frontend/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />
<style>
.error{
	color:red;
}
.success{
	color:forestgreen;
}
.sweet-ok{
    background: #1781d2 !important;
}
.sweet-ok:hover{
    background: #000 !important;
}
.ui-datepicker-div{
    z-index: 99999;
}
</style>
@endsection
@section('content')
	<section class="wrapper2 other_wrapper">
		@section('header')
		@include('includes.professional_header')
		@endsection
		<!-- section-header.// -->
		<!-- ========================= SECTION CONTENT ========================= -->
		<nav class="navbar navbar-expand-lg navbar-dark sticky-top">
			<div class="w-100">
				<!-- <div class="top-bar">
					<div class="top-bar-left-container">
						<a href="#url" class="top-bar-button"> <i class="icofont-close-line"></i> </a>
						<div class="d-none d-md-flex align-items-center">
							<h4 class="ml-4">Courses</h4> </div>
					</div>
					<div class="top-bar-center-container"> <a href="#url" class="user_llk">Your First Course <i class="icofont-caret-down"></i></a>
						<div class="show01 search-category" style="display: none;">
							<div class="select2-search">
								<input type="text" name=""> </div>
							<ul class="select2-results">
								<li class="select2-no-results">No matches found</li>
							</ul>
						</div>
					</div>
					<div class="top-bar-right-container"> <a href="#url" class="topbar-right-btn">
						build landing page <i class="icofont-long-arrow-right"></i>
					</a> </div>
				</div> -->
				<div class="navbar-menu">
					<div class="container-fluid">
						<div class="all-menu-sec">
							<!---<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav"> <span class="navbar-toggler-icon"></span> </button> --->
							<div class="" id="main_nav">
								<ul class="navbar-nav">
									<li class="nav-item active show_course_view tab_choose_temp"> <a class="nav-link" href="javascript:;">   @lang('site.choose_template') </a> </li>
									<li class="nav-item show_course_view tab_header" data-target="header"> <a class="nav-link" href="javascript:;">   @lang('site.header') </a> </li>
									<li class="nav-item show_course_view tab_content" data-target="content"> <a class="nav-link" href="javascript:;">   @lang('site.content') </a> </li>
									<li class="nav-item show_course_view tab_single" data-target="single"> <a class="nav-link" href="javascript:;">   @lang('site.single_video') </a> </li>
									<li class="nav-item show_course_view tab_products" data-target="products"> <a class="nav-link" href="javascript:;">   @lang('site.products') </a> </li>
									<li class="nav-item show_course_view tab_design" data-target="design"> <a class="nav-link" href="javascript:;">   @lang('site.design') </a> </li>
								</ul>
							</div>

							<div class="navbar__actions">
                                <div class="landing-title">
                                    <!-- <span class="temp_title_icon">
                                        <i class="fa fa-play" aria-hidden="true"></i>
                                    </span> -->
                                    <h2 id="temp_title">{{ @$detail->landing_title ?? 'Template Title'}}</h2>
                                </div>
								<!-- <button class="preview-btn" title="Preview course as an enrolled student"> <i class="icofont-eye-alt"></i> Preview </button> -->
							</div>
						</div>
						<!-- navbar-collapse.// -->
					</div>
				</div>
				<!-- container //  -->
			</div>
		</nav>
		<div class="page_builder">
			<div class="course_view mobile_chapter_list height_bottom">
				<div class="list-view-sessoin">
					<div>
						<div id="main">
                            <input type="text" id="template_id" value="{{@$detail->id}}" hidden>
                            <div class="tab_target" id="choose_temp">
                                @foreach(@$templates as $template)
                                    <div class="card landing_temp_card {{ @$template->id == 1 ? 'active_cert' : '' }}" data-id="{{ @$template->id }}">
                                        <img class="card-img-top" src="{{url('/')}}/public/frontend/images/cart_temp{{ @$template->id }}.png" alt="Card image cap">
                                        <div class="card-body">
                                            <h5 class="card-title tabcardtitle">{{ @$template->template_name }}</h5>
                                        </div>
                                    </div>
                                    <br>
                                @endforeach
                            </div>
                            <div class="tab_target" id="header" style="display: none;">
                                <form action="{{ route('store.landing.page.template') }}" id="myHeaderForm">
                                    <div class="cert_cntnt">
                                        <label for="header_image">@lang('site.upload_picture')</label>
                                        <input type="file" name="header_image" id="header_image" class="form-control" accept="image/*">
                                    </div>
                                    <div class="cert_cntnt">
                                        <label for="header_line_1">@lang('client_site.header_line_1')</label>
                                        <input type="text" name="header_line_1" id="header_line_1" class="form-control required mb-3" value="{{ @$detail->header_line_1 ?? @$temp->header_line_1 }}">
                                    </div>
                                    <div class="cert_cntnt">
                                        <label for="header_line_2">@lang('client_site.header_line_2')</label>
                                        <input type="text" name="header_line_2" id="header_line_2" class="form-control required mb-3" value="{{ @$detail->header_line_2 ?? @$temp->header_line_2 }}">
                                    </div>
                                    <div class="cert_cntnt">
                                        <label for="header_button">@lang('client_site.header_button')</label> <br>
                                        <label for="header_btn_yes">
                                            <input type="radio" name="header_btn" value="Y" id="header_btn_yes" @if(@$detail->header_button == 'Y') checked @endif>
                                            @lang('site.yes')
                                        </label>
                                        <label for="header_btn_no">
                                            <input type="radio" name="header_btn" value="N" id="header_btn_no" @if(@$detail->header_button == 'N') checked @endif>
                                            @lang('site.no')
                                        </label>
                                    </div>
                                    <div id="header_btn_action">

                                        <div class="cert_cntnt">
                                            <label for="header_button_text">@lang('client_site.header_button_text')</label>
                                            <input type="text" name="header_button_text" id="header_button_text" class="form-control required mb-3" value="{{ @$detail->header_button_text ?? 'Contact Us' }}">
                                        </div>
                                        <div class="cert_cntnt">
                                            <label for="header_button_type">@lang('client_site.header_button_type')</label> <br>
                                            <label for="header_btn_url">
                                                <input type="radio" name="header_button_type" value="URL" id="header_btn_url" @if(@$detail->header_button_type == 'URL') checked @endif>
                                                URL
                                            </label>
                                            <label for="header_btn_lead">
                                                <input type="radio" name="header_button_type" value="LEAD" id="header_btn_lead" @if(@$detail->header_button_type == 'LEAD') checked @endif>
                                                Lead
                                            </label>
                                        </div>
                                        <div class="cert_cntnt" id="header_button_link_div" @if(@$detail->header_button_type == 'LEAD') style="display: none;" @endif>
                                            <label for="header_button_link">@lang('client_site.header_button_link')</label>
                                            <input type="url" name="header_button_link" id="header_button_link" class="form-control required mb-3" value="">
                                        </div>
                                        <div id="header_button_lead_div" @if(@$detail->header_button_type == 'URL') style="display: none;" @endif>
                                            <div class="cert_cntnt row">
                                                <div class="col-md-6">
                                                    <label for="lead_name">
                                                        <input type="checkbox" name="lead_name" id="lead_name" class="mb-3"  @if(@$detail->lead_name == 'Y') checked @endif>
                                                        @lang('site.Name')
                                                    </label>
                                                    <div class="clearfix"></div>
                                                    <label for="lead_email">
                                                        <input type="checkbox" name="lead_email" id="lead_email" class="mb-3"  @if(@$detail->lead_email == 'Y') checked @endif>
                                                        @lang('site.email')
                                                    </label>
                                                    <div class="clearfix"></div>
                                                    <label for="lead_mobile">
                                                        <input type="checkbox" name="lead_mobile" id="lead_mobile" class="mb-3"  @if(@$detail->lead_mobile == 'Y') checked @endif>
                                                        Mobile
                                                    </label>
                                                    <div class="clearfix"></div>
                                                    <label for="lead_address">
                                                        <input type="checkbox" name="lead_address" id="lead_address" class="mb-3"  @if(@$detail->lead_address == 'Y') checked @endif>
                                                        @lang('site.address')
                                                    </label>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="lead_facebook">
                                                        <input type="checkbox" name="lead_facebook" id="lead_facebook" class="mb-3"  @if(@$detail->lead_facebook == 'Y') checked @endif>
                                                        Facebook link
                                                    </label>
                                                    <div class="clearfix"></div>
                                                    <label for="lead_instagram">
                                                        <input type="checkbox" name="lead_instagram" id="lead_instagram" class="mb-3"  @if(@$detail->lead_instagram == 'Y') checked @endif>
                                                        Instagram link
                                                    </label>
                                                    <div class="clearfix"></div>
                                                    <label for="lead_linkedin">
                                                        <input type="checkbox" name="lead_linkedin" id="lead_linkedin" class="mb-3"  @if(@$detail->lead_linkedin == 'Y') checked @endif>
                                                        LinkedIn link
                                                    </label>
                                                    <div class="clearfix"></div>
                                                    <label for="lead_website">
                                                        <input type="checkbox" name="lead_website" id="lead_website" class="mb-3"  @if(@$detail->lead_website == 'Y') checked @endif>
                                                        Website
                                                    </label>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="col-md-12">
                                                    <label for="lead_message">
                                                        <input type="checkbox" name="lead_message" id="lead_message" class="mb-3"  @if(@$detail->lead_message == 'Y') checked @endif>
                                                        @lang('site.message')
                                                    </label>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cert_cntnt">
                                        <label for="above_course">@lang('client_site.header_reverse_counter')</label>  <br>
                                        <label for="header_reverse_yes">
                                            <input type="radio" name="header_reverse" value="Y" id="header_reverse_yes" @if(@$detail->header_reverse_counter == 'Y') checked @endif>
                                            @lang('site.yes')
                                        </label>
                                        <label for="header_reverse_no">
                                            <input type="radio" name="header_reverse" value="N" id="header_reverse_no"  @if(@$detail->header_reverse_counter == 'N') checked @endif>
                                            @lang('site.no')
                                        </label>
                                    </div>
                                    <div class="cert_cntnt" id="header_reverse_action" @if(@$detail->header_reverse_counter == 'N') style="display: none;" @endif>
                                        <label for="header_counter_date">@lang('client_site.header_reverse_counter') @lang('site.date')</label>
                                        <input type="text" name="header_counter_date" id="header_counter_date" class="form-control required mb-3" value="{{ @$detail->header_counter_date ?? date('Y-m-d', strtotime('+1 day')) }}" autocomplete="off" readonly>
                                    </div>
                                </form>
                            </div>
                            <div class="tab_target" id="content" style="display: none;">
                                <form action="{{ route('store.landing.page.template') }}" id="myContentForm">
                                    <div class="cert_cntnt">
                                        <label for="content_type">@lang('site.content_type')</label>  <br>
                                        <label for="single_content">
                                            <input type="radio" name="body_single_content" value="Y" id="single_content" @if(@$detail->body_single_content) {{ @$detail->body_single_content == 'Y' ? 'checked' : '' }} @else {{ @$temp->body_single_content == 'Y' ? 'checked' : '' }} @endif>
                                            @lang('site.single_content')
                                        </label>
                                        <label for="multiple_content">
                                            <input type="radio" name="body_single_content" value="N" id="multiple_content"  @if(@$detail->body_single_content) {{ @$detail->body_single_content == 'N' ? 'checked' : '' }} @else {{ @$temp->body_single_content == 'N' ? 'checked' : '' }} @endif>
                                            @lang('site.multiple_content')
                                        </label>
                                    </div>
                                    <div id="single_content_div" @if(@$detail->body_single_content != null) @if(@$detail->body_single_content == 'N') style="display: none;" @endif @else @if(@$temp->body_single_content == 'N') style="display: none;" @endif @endif>
                                        <div class="cert_cntnt">
                                            <label for="body_single_content_image">@lang('site.upload_file')</label>  <br>
                                            <input type="file" name="body_single_content_image" id="body_single_content_image" class="form-control" accept="image/*">
                                        </div>
                                        <div class="cert_cntnt">
                                            <label for="body_single_content_heading">@lang('site.title')</label>  <br>
                                            <input type="text" name="body_single_content_heading" id="body_single_content_heading" class="form-control required" value="{{ @$detail->body_single_content_heading ?? @$temp->body_single_content_heading }}">
                                        </div>
                                        <div class="cert_cntnt">
                                            <label for="body_single_content_desc">@lang('site.description')</label>  <br>
                                            <input type="text" name="body_single_content_desc" id="body_single_content_desc" class="form-control required" value="{{ @$detail->body_single_content_desc ?? @$temp->body_single_content_desc }}">
                                        </div>
                                        <div class="cert_cntnt">
                                            <label for="body_single_content_button">@lang('site.button')</label>  <br>
                                            <label for="body_single_content_button_yes">
                                                <input type="radio" name="body_single_content_button" value="Y" id="body_single_content_button_yes" @if(@$detail->body_single_content_button == 'Y') checked @endif>
                                                @lang('site.yes')
                                            </label>
                                            <label for="body_single_content_button_no">
                                                <input type="radio" name="body_single_content_button" value="N" id="body_single_content_button_no"  @if(@$detail->body_single_content_button == 'N') checked @endif>
                                                @lang('site.no')
                                            </label>
                                        </div>
                                        <div id="body_single_content_button_div" @if(@$detail->body_single_content_button == 'N') style="display: none;" @endif>
                                            <div class="cert_cntnt">
                                                <label for="body_single_content_button_text">@lang('site.text')</label>  <br>
                                                <input type="text" name="body_single_content_button_text" id="body_single_content_button_text" class="form-control required" value="{{ @$detail->body_single_content_button_text ?? @$temp->body_single_content_button_text }}">
                                            </div>
                                            <div class="cert_cntnt">
                                                <label for="body_single_content_button_link">Link</label>  <br>
                                                <input type="url" name="body_single_content_button_link" id="body_single_content_button_link" class="form-control required" value="{{ @$detail->body_single_content_button_link }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div id="multiple_content_div" @if(@$detail->body_single_content) @if(@$detail->body_single_content == 'Y') style="display: none;" @endif @else @if(@$temp->body_single_content == 'Y') style="display: none;" @endif @endif>
                                        @if(@$detail->getContentDetails && count(@$detail->getContentDetails)>0)
                                            <div id="add_btn">
                                                <a href="javascript:;" class="btn btn-primary" id="add_mult_cntnt">+ @lang('client_site.add_new_content')</a>
                                            </div> <hr/>
                                            @foreach(@$detail->getContentDetails as $k=>$content)
                                                <div class="multicontent" id="multicontent_{{$k + 1}}" style="padding: 20px 0px;" data-id="{{$k + 1}}" data-saved="true">
                                                    <div class="multi_section_head">
                                                        <h5>Section {{$k + 1}}</h5>
                                                        @if( $k+1 !== 1 && $k+1 !== 2 )
                                                            <a href="javascript:;" class="btn del_multicontent" data-id="{{$k + 1}}"> <i class="fa fa-times" aria-hidden="true"></i> </a>
                                                        @endif
                                                    </div>
                                                    <div class="cert_cntnt">
                                                        <label for="content_file_{{$k + 1}}">@lang('site.upload_file')</label>  <br>
                                                        <input type="file" name="content_file_{{$k + 1}}" id="content_file_{{$k + 1}}" class="form-control content_file_inp" data-id="{{$k + 1}}" accept="image/*">
                                                    </div>
                                                    <div class="cert_cntnt">
                                                        <label for="content_file_youtube_link_{{$k + 1}}">Youtube Link</label>  <br>
                                                        <input type="text" name="content_file_youtube_link_{{$k + 1}}" id="content_file_youtube_link_{{$k + 1}}" class="form-control content_file_youtube" data-id="{{$k + 1}}" >
                                                    </div>
                                                    <div class="cert_cntnt">
                                                        <label for="content_heading_{{$k + 1}}">@lang('site.title')</label>  <br>
                                                        <input type="text" name="content_heading_{{$k + 1}}" id="content_heading_{{$k + 1}}" class="content_heading_inp form-control required" data-id="{{$k + 1}}" value="{{ @$content->content_heading ?? 'Online Learning' }}">
                                                    </div>
                                                    <div class="cert_cntnt">
                                                        <label for="content_desc_{{$k + 1}}">@lang('site.description')</label>  <br>
                                                        <input type="text" name="content_desc_{{$k + 1}}" id="content_desc_{{$k + 1}}" class="content_desc_inp form-control required" data-id="{{$k + 1}}" value="{{ @$content->content_desc ?? 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla.' }}">
                                                    </div>
                                                    <div class="cert_cntnt">
                                                        <label for="content_button_{{$k + 1}}">@lang('site.button')</label>  <br>
                                                        <label for="content_button_yes_{{$k + 1}}">
                                                            <input type="radio" class="content_button" name="content_button_{{$k + 1}}" value="Y" id="content_button_yes_{{$k + 1}}" data-id="{{$k + 1}}" {{ @$content->content_button == 'Y' ? 'checked' : '' }}>
                                                            @lang('site.yes')
                                                        </label>
                                                        <label for="content_button_no_{{$k + 1}}">
                                                            <input type="radio" class="content_button" name="content_button_{{$k + 1}}" value="N" id="content_button_no_{{$k + 1}}" data-id="{{$k + 1}}" {{ @$content->content_button == 'N' ? 'checked' : '' }}>
                                                            @lang('site.no')
                                                        </label>
                                                    </div>
                                                    <div id="content_button_div_{{$k + 1}}" @if(@$content->content_button == 'N') style="display: none;" @endif >
                                                        <div class="cert_cntnt">
                                                            <label for="content_button_text_{{$k + 1}}">@lang('site.text')</label>  <br>
                                                            <input type="text" name="content_button_text_{{$k + 1}}" id="content_button_text_{{$k + 1}}" class="content_button_text_inp form-control required" data-id="{{$k + 1}}" value="{{ @$content->content_button_text ?? 'learn more' }}">
                                                        </div>
                                                        <div class="cert_cntnt">
                                                            <label for="content_button_link_{{$k + 1}}">Link</label>  <br>
                                                            <input type="url" name="content_button_link_{{$k + 1}}" id="content_button_link_{{$k + 1}}" class="content_button_link_inp form-control required" data-id="{{$k + 1}}" value="{{ @$content->content_button_link }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr/>
                                            @endforeach
                                        @else
                                            <div id="add_btn">
                                                <a href="javascript:;" class="btn btn-primary" id="add_mult_cntnt">+ @lang('client_site.add_new_content')</a>
                                            </div> <hr/>
                                            <div class="multicontent" id="multicontent_1" style="padding: 20px 0px;" data-id="1" data-saved="false">
                                                <div class="multi_section_head">
                                                    <h5>Section 1</h5>
                                                    <!-- <a href="javascript:;" class="btn del_multicontent" data-id="1"> <i class="fa fa-times" aria-hidden="true"></i> </a> -->
                                                </div>
                                                <div class="cert_cntnt">
                                                    <label for="content_file_1">@lang('site.upload_file')</label>  <br>
                                                    <input type="file" name="content_file_1" id="content_file_1" class="form-control content_file_inp" data-id="1" accept="image/*">
                                                </div>
                                                <div class="cert_cntnt">
                                                    <label for="content_file_youtube_link_1">Youtube Link</label>  <br>
                                                    <input type="text" name="content_file_youtube_link_1" id="content_file_youtube_link_1" class="form-control content_file_youtube" data-id="1" >
                                                </div>
                                                <div class="cert_cntnt">
                                                    <label for="content_heading_1">@lang('site.title')</label>  <br>
                                                    <input type="text" name="content_heading_1" id="content_heading_1" class="content_heading_inp form-control required" data-id="1" value="Online Learning">
                                                </div>
                                                <div class="cert_cntnt">
                                                    <label for="content_desc_1">@lang('site.description')</label>  <br>
                                                    <input type="text" name="content_desc_1" id="content_desc_1" class="content_desc_inp form-control required" data-id="1" value="Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla.">
                                                </div>
                                                <div class="cert_cntnt">
                                                    <label for="content_button_1">@lang('site.button')</label>  <br>
                                                    <label for="content_button_yes_1">
                                                        <input type="radio" class="content_button" name="content_button_1" value="Y" id="content_button_yes_1" data-id="1" checked>
                                                        @lang('site.yes')
                                                    </label>
                                                    <label for="content_button_no_1">
                                                        <input type="radio" class="content_button" name="content_button_1" value="N" id="content_button_no_1" data-id="1">
                                                        @lang('site.no')
                                                    </label>
                                                </div>
                                                <div id="content_button_div_1">
                                                    <div class="cert_cntnt">
                                                        <label for="content_button_text_1">@lang('site.text')</label>  <br>
                                                        <input type="text" name="content_button_text_1" id="content_button_text_1" class="content_button_text_inp form-control required" data-id="1" value="learn more">
                                                    </div>
                                                    <div class="cert_cntnt">
                                                        <label for="content_button_link_1">Link</label>  <br>
                                                        <input type="url" name="content_button_link_1" id="content_button_link_1" class="content_button_link_inp form-control required" data-id="1" value="{{ @$detail->content_button_link_1 }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <hr/>
                                            <div class="multicontent" id="multicontent_2" style="padding: 20px 0px;" data-id="2" data-saved="false">
                                                <div class="multi_section_head">
                                                    <h5>Section 2</h5>
                                                    <!-- <a href="javascript:;" class="btn del_multicontent" data-id="2"> <i class="fa fa-times" aria-hidden="true"></i> </a> -->
                                                </div>
                                                <div class="cert_cntnt">
                                                    <label for="content_file_2">@lang('site.upload_file')</label>  <br>
                                                    <input type="file" name="content_file_2" id="content_file_2" class="form-control content_file_inp" data-id="2" accept="image/*">
                                                </div>
                                                <div class="cert_cntnt">
                                                    <label for="content_file_youtube_link_2">Youtube Link</label>  <br>
                                                    <input type="text" name="content_file_youtube_link_2" id="content_file_youtube_link_2" class="form-control content_file_youtube" data-id="2" >
                                                </div>
                                                <div class="cert_cntnt">
                                                    <label for="content_heading_2">@lang('site.title')</label>  <br>
                                                    <input type="text" name="content_heading_2" id="content_heading_2" class="content_heading_inp form-control required" data-id="2" value="Online Learning">
                                                </div>
                                                <div class="cert_cntnt">
                                                    <label for="content_desc_2">@lang('site.description')</label>  <br>
                                                    <input type="text" name="content_desc_2" id="content_desc_2" class="content_desc_inp form-control required" data-id="2" value="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.">
                                                </div>
                                                <div class="cert_cntnt">
                                                    <label for="content_button_2">@lang('site.button')</label>  <br>
                                                    <label for="content_button_yes_2">
                                                        <input type="radio" class="content_button" name="content_button_2" value="Y" id="content_button_yes_2" data-id="2">
                                                        @lang('site.yes')
                                                    </label>
                                                    <label for="content_button_no_2">
                                                        <input type="radio" class="content_button" name="content_button_2" value="N" id="content_button_no_2" data-id="2" checked>
                                                        @lang('site.no')
                                                    </label>
                                                </div>
                                                <div id="content_button_div_2" style="display: none;">
                                                    <div class="cert_cntnt">
                                                        <label for="content_button_text_2">@lang('site.text')</label>  <br>
                                                        <input type="text" name="content_button_text_2" id="content_button_text_2" class="content_button_text form-control required" data-id="2" value="learn more">
                                                    </div>
                                                    <div class="cert_cntnt">
                                                        <label for="content_button_link_2">Link</label>  <br>
                                                        <input type="text" name="content_button_link_2" id="content_button_link_2" class="content_button_link form-control required" data-id="2" value="{{ @$detail->content_button_link_2 }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <hr/>
                                        @endif
                                    </div>
                                </form>
                            </div>
                            <div class="tab_target" id="single" style="display: none;">
                                <form action="{{ route('store.landing.page.template') }}" id="mySingleVideoForm">
                                    <div class="cert_cntnt">
                                        <label for="single_video">@lang('site.single_video')</label>  <br>
                                        <label for="single_video_yes">
                                            <input type="radio" name="single_video" value="Y" id="single_video_yes" @if(@$detail->body_single_video == 'Y') checked @endif>
                                            @lang('site.yes')
                                        </label>
                                        <label for="single_video_no">
                                            <input type="radio" name="single_video" value="N" id="single_video_no"  @if(@$detail->body_single_video == 'N') checked @endif>
                                            @lang('site.no')
                                        </label>
                                    </div>
                                    <div id="single_video_div">
                                        <div class="cert_cntnt">
                                            <label for="single_file">@lang('site.upload_file')</label>
                                            <input type="file" name="single_file" id="single_file" class="form-control">
                                        </div>
                                        <div class="cert_cntnt">
                                            <label for="body_single_video_heading">@lang('site.title')</label>
                                            <input type="text" name="body_single_video_heading" id="body_single_video_heading" class="form-control required" value="{{ @$detail->body_single_video_heading ?? 'Get product more information from the video' }}">
                                        </div>
                                        <div class="cert_cntnt">
                                            <label for="body_single_video_desc">@lang('site.description')</label>
                                            <input type="text" name="body_single_video_desc" id="body_single_video_desc" class="form-control required" value="{{ @$detail->body_single_video_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.' }}">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab_target" id="products" style="display: none;">
                                <p>Products</p>
                            </div>
                            <div class="tab_target" id="design" style="display: none;">
                                <form action="{{ route('store.landing.page.template') }}" id="myDesignForm">
                                    <div class="cert_cntnt">
                                        <label for="color_primary">@lang('site.primary_color')</label>
                                        <input type="color" name="color_primary" id="color_primary" class="form-control" value="{{ @$detail->color_primary ?? ( @$temp->color_primary ?? '#000000' ) }}">
                                    </div>
                                    <div class="cert_cntnt">
                                        <label for="color_secondary">@lang('site.secondary_color')</label>
                                        <input type="color" name="color_secondary" id="color_secondary" class="form-control" value="{{ @$detail->color_secondary ?? ( @$temp->color_secondary ?? '#000000' ) }}">
                                    </div>
                                    <div class="cert_cntnt">
                                        <label for="color_tertiary">@lang('site.tertiary_color')</label>
                                        <input type="color" name="color_tertiary" id="color_tertiary" class="form-control" value="{{ @$detail->color_tertiary ?? ( @$temp->color_tertiary ?? '#000000' ) }}">
                                    </div>
                                </form>
                            </div>
						</div>
						{{-- <div class="tree-tip">
							<div class="tip__title"> <img src="{{ URL::to('public/frontend/images/tip.png') }}"> Pro Tip </div>
							<!-- <p class="tip__description">You can customize the course completion experience with a certificate or a custom completion page!</p> <a href="#url">Course completion settings</a> </div> -->
							<p class="tip__description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis ex dicta incidunt eum mollitia rerum pariatur saepe cum quia repellat!</p>
                        </div> --}}
					</div>
					<div></div>
				</div>
				<div class="builder__footer">
					<div class="tree_action">
						<div class="builder-add-chapter">
							<a href="javascript:;" class="button button--primary w-100" id="template_confirm" data-toggle="modal" data-target="#exampleModalCenter"> @lang('site.select_this_template')</a>
							<a href="javascript:;" class="button button--primary target--button w-100" id="save_header" style="display: none;"> @lang('site.save_header')</a>
							<a href="javascript:;" class="button button--primary target--button w-100" id="save_content" style="display: none;"> @lang('site.save_content')</a>
							<a href="javascript:;" class="button button--primary target--button w-100" id="save_single" style="display: none;"> @lang('site.save_single_video')</a>
							<a href="javascript:;" class="button button--primary target--button w-100" id="save_products" style="display: none;"> @lang('site.save_products_section')</a>
							<a href="javascript:;" class="button button--primary target--button w-100" id="save_design" style="display: none;"> @lang('site.save_design')</a>
						</div>
					</div>
				</div>
			</div>
			<div class="builder_section">
				<div class="wrap wrap-edit">
					<div class="ember-view">
                        <input type="text" value="{{@$detail->landing_template_id ?? '1'}}" id="template_number" hidden>
                        <iframe src="@if(@$detail) {{url('/')}}/l/{{auth()->user()->slug}}/{{@$detail->slug}} @else {{URL::to('/landing-page-template/1')}} @endif" style="width:100%; height:100vh;" id="main_iframe" frameborder="0"></iframe>
					</div>
				</div>
			</div>
		</div>
	</section>
    <!-- Button trigger modal -->
    <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
    Launch demo modal
    </button> -->

    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">@lang("site.landing_page_name_title")</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            @lang("site.landing_page_name_text")
            <input type="text" class="form-control" id="template_name" placeholder="@lang('site.landing_page_name')" /><div id="err_template_name"></div>
        </div>
        <div class="modal-footer">
            <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
            <button type="button" class="btn btn-primary" id="save_temp_name">@lang('site.save_and_continue')</button>
        </div>
        </div>
    </div>
    </div>
@endsection
@section('footer')
{{-- @include('includes.footer') --}}
<script>
    var timer = "";
    var template_id = 0;
    var template_confirmed = false;
    var detail = {!! json_encode(@$detail) !!};
    console.log(detail);

    if (window.matchMedia('(min-width: 992px)').matches) {
		// functionality for screens smaller than 1200px
		$('.course_view').removeClass('mobile_chapter_list');
	}
	$(window).resize(function() {
		if (window.matchMedia('(max-width: 991px)').matches) {
			// functionality for screens smaller than 1200px
			$('.course_view').addClass('mobile_chapter_list');
		}
		if (window.matchMedia('(min-width: 992px)').matches) {
			// functionality for screens smaller than 1200px
			$('.course_view').removeClass('mobile_chapter_list');
		}
	});
	$('.show_course_view').click(function(){
		console.log("Toggle Clicked");
		$('.mobile_chapter_list').toggle("slide", { direction: "left" }, 500);
	});

    $(document).ready(function(){
        if(detail !== null){
            console.log("Getting in");
            template_id = detail.id;
            template_confirmed = true;
            $('.active').removeClass('active');
            $('.tab_header').addClass('active');
            $('.tab_target').hide();
            $('.tab_choose_temp').hide();
            $('#header').show();
            $('#template_confirm').hide();
            $('#save_header').show();
            $('#template_id').val(template_id);
            if(detail.header_counter_date != null){
                timer = setInterval(function() {
                    // count--;
                    // if (count == 0) {
                    // clearInterval(timer);
                    // return;
                    // }
                    $("#main_iframe").contents().find('.header_counter').text( timeDiffCalc( new Date(), new Date($("#header_counter_date").val()) ) );
                }, 1000);
            }
            changeColours(detail.color_primary, detail.color_secondary, detail.color_tertiary);
        } else {
            // $('#header_line_1').val("Lorem ipsum dolor sit ");
            // $("#header_line_2").val("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
            $('#header_btn_yes').prop('checked', true);
            $('#header_btn_url').prop('checked', true);
            $('#header_button_lead_div').hide();
            $('#lead_name').prop('checked', true);
            $('#lead_email').prop('checked', true);
            $('#lead_mobile').prop('checked', true);
            $('#lead_address').prop('checked', true);
            $('#lead_facebook').prop('checked', true);
            $('#lead_instagram').prop('checked', true);
            $('#lead_linkedin').prop('checked', true);
            $('#lead_website').prop('checked', true);
            $('#lead_message').prop('checked', false);
            $('#header_reverse_yes').prop('checked', true);
        }
        $('myHeaderForm').validate();

        $('.landing_temp_card').click(function() {
            var id = $(this).data('id');
            console.log(id);
            var url = "{{URL::to('/landing-page-template/')}}/"+id;
            $('#template_number').val(id);
            $('#main_iframe').attr('src', url);
            $('.active_cert').removeClass('active_cert');
            $(this).addClass('active_cert');
            $('#header_reverse_yes').prop('checked', true);

            if(id == 1){
                $('#header_line_1').val("Lorem ipsum dolor sit ");
                $("#header_line_2").val("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
                $('#lead_name').prop('checked', true);
                $('#lead_email').prop('checked', true);
                $('#lead_mobile').prop('checked', true);
                $('#lead_address').prop('checked', true);
                $('#lead_facebook').prop('checked', true);
                $('#lead_instagram').prop('checked', true);
                $('#lead_linkedin').prop('checked', true);
                $('#lead_website').prop('checked', true);
                $('#lead_message').prop('checked', false);

            } else if(id == 2){
                $("#header_line_1").val("Lorem ipsum dolor sit Lorem ipsum");
                $("#header_line_2").val("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Sed pretium ligula.");
                $('#lead_name').prop('checked', true);
                $('#lead_email').prop('checked', true);
                $('#lead_mobile').prop('checked', false);
                $('#lead_address').prop('checked', false);
                $('#lead_facebook').prop('checked', false);
                $('#lead_instagram').prop('checked', false);
                $('#lead_linkedin').prop('checked', false);
                $('#lead_website').prop('checked', false);
                $('#lead_message').prop('checked', true);
                $('#lead_message').prop('checked', true);
            }
        });

        $('#template_confirm').click(function(){
            // sweetAlert();
        });

    });
    $('#template_name').keyup(function(){
        $('#err_template_name').html('');
    });
    $(document).delegate('#save_temp_name', 'click', function () {
        if($('#template_name').val() == ""){
            $('#err_template_name').html('<label class="error">@lang("site.template_name_required")</label>');
        } else {
            var reqData = {
                'jsonrpc' : '2.0',
                '_token' : '{{csrf_token()}}',
                'params' : {
                    'template_number': $('#template_number').val(),
                    'template_name': $('#template_name').val(),
                }
            };
            $.ajax({
                url: "{{ route('check.template.name') }}",
                method: 'post',
                dataType: 'json',
                data: reqData,
                async: false,
                success: function(res){
                    console.log(res);
                    if(res.status == 'NA'){
                        $('#err_template_name').html('<label class="error">@lang("site.this_name_already_exists")</label>');
                    } else {
                        $('#err_template_name').html('');
                        storeTemplate();
                    }
                },error: function(err){
                    console.log(err);
                },
            });
        }
    });

    $('.show_course_view').click(function(){
        if(template_confirmed == true){
            var target = $(this).data('target');
            $('.tab_target').hide();
            $('#'+target).show();
            $('.target--button').hide();
            $('#save_'+target).show();
            $('.show_course_view').removeClass('active');
            $('.tab_'+target).addClass('active');
        } else {
            toastr.error("@lang('client_site.select_temp_first')")
        }
    });

    function sweetAlert(){
        $('.swal_html').remove();
        var html = document.createElement("div");
        html.classList.add('swal_html');
        $(html).html(`<input type="text" class="form-control" id="template_name" placeholder="@lang('site.landing_page_name')" /><div id="err_template_name"></div>`);
        console.log(html);
        swal({
            title: '@lang("site.landing_page_name_title")',
            text : '@lang("site.landing_page_name_text")',
            content: html,
            closeModal: false,
            buttons: {
                cancel : 'Cancel',
                confirm : {text:'OK', className:'sweet-ok'}
            }
        })
        .then((isConfirmed) => {
            if (isConfirmed) {
                if($('#template_name').val() == ""){
                    $('#err_template_name').html('<label class="error">@lang("site.template_name_required")</label>');
                    $('.sweet-ok').attr('disabled',true);
                } else {
                    storeTemplate();
                }
            }
        });
    }

    function storeTemplate(){
        var reqData = {
            'jsonrpc' : '2.0',
            '_token' : '{{csrf_token()}}',
            'params' : {
                'template_number': $('#template_number').val(),
                'template_name': $('#template_name').val(),
            }
        };
        $.ajax({
            url: "{{ route('store.landing.page.template') }}",
            method: 'post',
            dataType: 'json',
            data: reqData,
            async: false,
            success: function(res){
                console.log(res);
                template_id = res.template.id;

                window.location.href = "{{url('/')}}/landing-page/edit/"+template_id;
                // $('.active').removeClass('active');
                // $('.tab_header').addClass('active');
                // $('.tab_target').hide();
                // $('.tab_choose_temp').hide();
                // $('#header').show();
                // $('#template_confirm').hide();
                // $('#save_header').show();
                // $('#temp_title').text($('#template_name').val());
                // template_confirmed = true;
            },
            error: function(err){
                console.log(err);
            },
        });
    }

    // HEADER
        $('#header_image').change(function(){
            var file = this.files[0];
            var reader = new FileReader();
            reader.onloadend = function (e) {
                // console.log(reader.result);
                $("#main_iframe").contents().find('.header_image').attr('src', e.target.result);
            }
            if (file) {
                reader.readAsDataURL(file);
            } else {
                img_array = ["ban-img2.png", "wef-min.jpg"];
                val = $('template_number').val();
                $("#main_iframe").contents().find('.header_image').attr('src', "{{URL::asset('public/frontend/')}}/landing_page"+val+"/images/"+img_array[val+1]);
            }
        });
        $('#header_line_1').on('keyup change', function(){
            var str = $(this).val();
            if($('#template_number').val() == 1){
                var html = "";
                if(str.indexOf(" ") === -1){
                    html = "<span>" + str + "</span>";
                } else {
                    var words = str.split(" ");
                    html = "<span>" + words[0] + "</span> " + str.substring( str.indexOf(" ") + 1, str.length );
                }
                $("#main_iframe").contents().find('.header_line_1').html(html);
            } else if($('#template_number').val() == 2){
                $("#main_iframe").contents().find('.header_line_1').text(str);
            }
        });
        $('#header_line_2').on('keyup change', function(){
            $("#main_iframe").contents().find('.header_line_2').text($(this).val());
        });
        $('input[type=radio][name=header_reverse]').change(function() {
            if (this.value == 'Y') {
                $('#header_reverse_action').show();
                $("#main_iframe").contents().find('.header_reverse').show();
                $('#header_reverse_action').addClass('required');
            } else if (this.value == 'N') {
                $('#header_reverse_action').hide();
                $("#main_iframe").contents().find('.header_reverse').hide();
                $('#header_reverse_action').removeClass('required');
            }
        });
        $('input[type=radio][name=header_btn]').change(function() {
            if (this.value == 'Y') {
                $('#header_btn_action').show();
                $('#header_button_text').addClass('required');
                $("#main_iframe").contents().find('.header_btn').show();
            } else if (this.value == 'N') {
                $('#header_btn_action').hide();
                $('#header_button_text').removeClass('required');
                $("#main_iframe").contents().find('.header_btn').hide();
            }
        });
        $('input[type=radio][name=header_button_type]').change(function(){
            if (this.value == 'URL'){
                $('#header_button_link_div').show();
                $('#header_button_lead_div').hide();
            } else if (this.value == 'LEAD'){
                $('#header_button_lead_div').show();
                $('#header_button_link_div').hide();
            }
        });
        $('#header_button_text').on('keyup change', function(){
            if($('#template_number').val() == 1){
                $("#main_iframe").contents().find('.header_button_text').text($(this).val());
            } else if($('#template_number').val() == 2){
                $("#main_iframe").contents().find('.header_button_text').html($(this).val() + `<i class="fa fa-long-arrow-right"></i>`);
            }
        });
        $('#lead_name').change(function(){
            if($('#lead_name').is(':checked')){
                $("#main_iframe").contents().find('.lead_name').show();
            } else {
                $("#main_iframe").contents().find('.lead_name').hide();
            }
        });
        $('#lead_email').change(function(){
            if($('#lead_email').is(':checked')){
                $("#main_iframe").contents().find('.lead_email').show();
            } else {
                $("#main_iframe").contents().find('.lead_email').hide();
            }
        });
        $('#lead_mobile').change(function(){
            if($('#lead_mobile').is(':checked')){
                $("#main_iframe").contents().find('.lead_mobile').show();
            } else {
                $("#main_iframe").contents().find('.lead_mobile').hide();
            }
        });
        $('#lead_address').change(function(){
            if($('#lead_address').is(':checked')){
                $("#main_iframe").contents().find('.lead_address').show();
            } else {
                $("#main_iframe").contents().find('.lead_address').hide();
            }
        });
        $('#lead_facebook').change(function(){
            if($('#lead_facebook').is(':checked')){
                $("#main_iframe").contents().find('.lead_facebook').show();
            } else {
                $("#main_iframe").contents().find('.lead_facebook').hide();
            }
        });
        $('#lead_instagram').change(function(){
            if($('#lead_instagram').is(':checked')){
                $("#main_iframe").contents().find('.lead_instagram').show();
            } else {
                $("#main_iframe").contents().find('.lead_instagram').hide();
            }
        });
        $('#lead_linkedin').change(function(){
            if($('#lead_linkedin').is(':checked')){
                $("#main_iframe").contents().find('.lead_linkedin').show();
            } else {
                $("#main_iframe").contents().find('.lead_linkedin').hide();
            }
        });
        $('#lead_website').change(function(){
            if($('#lead_website').is(':checked')){
                $("#main_iframe").contents().find('.lead_website').show();
            } else {
                $("#main_iframe").contents().find('.lead_website').hide();
            }
        });
        $('#lead_message').change(function(){
            if($('#lead_message').is(':checked')){
                $("#main_iframe").contents().find('.lead_message').show();
            } else {
                $("#main_iframe").contents().find('.lead_message').hide();
            }
        });

        $('#save_header').click(function(){
            if($("#myHeaderForm").valid()){
                updateTemplate();
            }
        });
        // $("#header_counter_date").on('change', function(){
        //     console.log("Changed");
        //     var str = timeDiffCalc(new Date(), new Date($("#header_counter_date").val()));
        //     console.log(str + " days left until Christmas!");
        //     var count = getCounterData($(".counter"));

        //     var timer = setInterval(function() {
        //         count--;
        //         if (count == 0) {
        //         clearInterval(timer);
        //         return;
        //         }
        //         $("#main_iframe").contents().find('.header_counter').text(timeDiffCalc(new Date(), new Date($("#header_counter_date").val())));
        //     }, 1000);
        // });
        $("#header_counter_date").datepicker({dateFormat: "yy-mm-dd",

            monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            monthNamesShort: [ "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ],
            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],

            defaultDate: new Date(),
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            showMonthAfterYear: true,
            showWeek: true,
            showAnim: "drop",
            constrainInput: true,
            yearRange: "-90:",
            minDate: +1,
            onSelect: function(dateText) {
                clearInterval(timer);
                timer = setInterval(function() {
                    // count--;
                    // if (count == 0) {
                    // clearInterval(timer);
                    // return;
                    // }
                    $("#main_iframe").contents().find('.header_counter').text(timeDiffCalc(new Date(), new Date($("#header_counter_date").val())));
                }, 1000);
            },
            onClose: function( selectedDate ) {
                //$( "#datepicker1").datepicker( "option", "minDate", selectedDate );
            }
        });
    // HEADER

    // DESIGN
        $('#color_primary, #color_secondary, #color_tertiary').change(function(){
            changeColours($('#color_primary').val(), $('#color_secondary').val(), $('#color_tertiary').val());
        });
        $('#save_design').click(function(){
            if($("#myDesignForm").valid()){
                updateTemplate();
            }
        });
        function changeColours(primary, secondary, tertiary){
            console.log("Changing colours");
            var val = primary;
            var rgb = hexToRgb(val);
            var rgba = [rgb.slice(0, rgb.length-1), ", 0.3", rgb.slice(rgb.length-1)].join('');
            rgba = rgba.replace('rgb','rgba');

            $("#main_iframe").contents().find('.color_primary').css('background', val);
            $("#main_iframe").contents().find('.color_primary_font').css('color', val);
            $("#main_iframe").contents().find('.color_primary_border').css('border-color', val);
            $("#main_iframe").contents().find('.color_primary_hover').hover(function(){
                $(this).css('background', val);
                $(this).css('color', '#fff');
            }, function(){
                $(this).css('background', 'transparent');
                $(this).css('color', val);
            });
            $("#main_iframe").contents().find('.color_primary_font_hover').hover(function(){
                $(this).css('color', val);
            },function(){
                $(this).css('color', '#000');
            });
            $("#main_iframe").contents().find('.color_primary_owl').find('.owl-prev').css('color', val).hover(function(){
                $(this).css('background', val);
                $(this).css('color', '#fff');
            }, function(){
                $(this).css('background', 'transparent');
                $(this).css('color', val);
            });
            $("#main_iframe").contents().find('.color_primary_owl').find('.owl-next').css('color', val).hover(function(){
                $(this).css('background', val);
                $(this).css('color', '#fff');
            }, function(){
                $(this).css('background', 'transparent');
                $(this).css('color', val);
            });
            $("#main_iframe").contents().find('.color_primary_light').css('background', rgba);

            $("#main_iframe").contents().find('.color_secondary').css('background', secondary);
            $("#main_iframe").contents().find('.color_secondary_font').css('color', secondary);

            $("#main_iframe").contents().find('.color_tertiary').css('background', tertiary);
            $("#main_iframe").contents().find('.color_tertiary_font').css('color', tertiary);
        }
    // DESIGN

    // SINGLE VIDEO
        $('input[type=radio][name=single_video]').change(function() {
            if (this.value == 'Y') {
                $('#single_video_div').show();
                $("#main_iframe").contents().find('.single_video').show();
            } else if (this.value == 'N') {
                $('#single_video_div').hide();
                $("#main_iframe").contents().find('.single_video').hide();
            }
        });
        $('#single_file').on('change', function(){
            var file = this.files[0];
            var fileType = file["type"];
            if(fileType.search('image') >= 0 || fileType.search('video') >= 0){
                updateTemplate();
                if(detail.body_single_video_type == "I"){
                    $("#main_iframe").contents().find('.single_vdo').empty().append(`<img width="100%" src="{{URL::to('storage/app/public/uploads/landing_page')}}/`+detail.body_single_video_filename+`" />`);
                    $("#main_iframe").contents().find('.paly_btn').hide();
                } else {
                    $("#main_iframe").contents().find('.single_vdo').empty().append(`<video width="100%" src="{{URL::to('storage/app/public/uploads/landing_page')}}/`+detail.body_single_video_filename+`"></video>`);
                    $("#main_iframe").contents().find('.paly_btn').show();
                }
            } else {
                toastr.error("@lang('client_site.upload_image_or_video')");
                $('#single_file').val('');
            }
        });
        $('#body_single_video_heading').on('keyup change', function(){
            $("#main_iframe").contents().find('.body_single_video_heading').text(this.value);
        });
        $('#body_single_video_desc').on('keyup change', function(){
            $("#main_iframe").contents().find('.body_single_video_desc').text(this.value);
        });
        $('#save_single').click(function(){
            if($("#mySingleVideoForm").valid()){
                updateTemplate();
            }
        });
    // SINGLE VIDEO

    // CONTENT
        $('input[type=radio][name=body_single_content]').change(function() {
            if (this.value == 'Y') {
                $('#single_content_div').show();
                $("#main_iframe").contents().find('.single_content_div').show();
                $('#multiple_content_div').hide();
                $("#main_iframe").contents().find('.multiple_content_div').hide();
            } else if (this.value == 'N') {
                $('#single_content_div').hide();
                $("#main_iframe").contents().find('.single_content_div').hide();
                $('#multiple_content_div').show();
                $("#main_iframe").contents().find('.multiple_content_div').show();
            }
        });
        $('#body_single_content_image').change(function(){
            var file = this.files[0];
            var reader = new FileReader();
            reader.onloadend = function (e) {
                $("#main_iframe").contents().find('.single_content_file').attr('src', e.target.result);
            }
            if (file) {
                reader.readAsDataURL(file);
            } else {
                $("#main_iframe").contents().find('.single_content_file').attr('src', "{{URL::asset('public/frontend/')}}/landing_page" + $('template_number').val() + "/images/{{@$temp->body_single_content_image}}");
            }
        });
        $('#body_single_content_heading').keyup(function(){
            var str = $(this).val();
            if($('#template_number').val() == 1){
                var html = "";
                if(str.indexOf(" ") === -1){
                    html = "<span>" + str + "</span>";
                } else {
                    var words = str.split(" ");
                    html = "<span>" + words[0] + "</span> " + str.substring( str.indexOf(" ") + 1, str.length );
                }
                $("#main_iframe").contents().find('.body_single_content_heading').html(html);
            } else if($('#template_number').val() == 2){
                $("#main_iframe").contents().find('.body_single_content_heading').text(str);
            }
        });
        $('#body_single_content_desc').keyup(function(){
            $("#main_iframe").contents().find('.body_single_content_desc').text(this.value);
        });
        $('input[type=radio][name=body_single_content_button]').change(function() {
            if (this.value == 'Y') {
                $('#body_single_content_button_div').show();
                $("#main_iframe").contents().find('.body_single_content_button').show();
            } else if (this.value == 'N') {
                $('#body_single_content_button_div').hide();
                $("#main_iframe").contents().find('.body_single_content_button').hide();
            }
        });
        $('#body_single_content_button_text').keyup(function(){
            $("#main_iframe").contents().find('.body_single_content_button_text').text(this.value);
        });
        $('#body_single_content_button_link').keyup(function(){
            $("#main_iframe").contents().find('.body_single_content_button_link').attr('href', this.value);
        });

        $(document).delegate('.content_heading_inp', 'keyup', function(){
            var str = $(this).val();
            if($('#template_number').val() == 1){
                var html = "";
                if(str.indexOf(" ") === -1){
                    html = "<span>" + str + "</span>";
                } else {
                    var words = str.split(" ");
                    html = "<span>" + words[0] + "</span> " + str.substring( str.indexOf(" ") + 1, str.length );
                }
                $("#main_iframe").contents().find('.content_heading_'+$(this).data('id')).html(html);
            } else if($('#template_number').val() == 2){
                $("#main_iframe").contents().find('.content_heading_'+$(this).data('id')).text(str);
            }
        });
        $(document).delegate('.content_desc_inp', 'keyup', function(){
            $("#main_iframe").contents().find('.content_desc_'+$(this).data('id')).text($(this).val());
        });
        $(document).delegate('.content_file_inp', 'change', function(){
            var id = $(this).data('id');
            console.log("ID: "+id);
            var file = this.files[0];
            var reader = new FileReader();
            reader.onloadend = function (e) {
                // console.log(reader.result);
                // $("#main_iframe").contents().find('.content_file_'+id).find('img').attr('src', e.target.result);
                $("#main_iframe").contents().find('.content_file_'+id).html('<img src="'+e.target.result+'">');
                $('#content_file_youtube_link_'+id).val('');
            }
            if (file) {
                reader.readAsDataURL(file);
            } else {
                img_array = ["vedioimg.jpg", "gfg-min.png"];
                val = $('template_number').val();
                if(id == 1)
                    // $("#main_iframe").contents().find('.content_file_'+id).find('img').attr('src', "{{URL::asset('public/frontend/')}}/landing_page"+val+"/images/"+img_array[val+1]);
                    $("#main_iframe").contents().find('.content_file_'+id).html("<img src='{{URL::asset('public/frontend/')}}/landing_page"+val+"/images/"+img_array[val+1]+"'>");
                else
                    // $("#main_iframe").contents().find('.content_file_'+id).find('img').attr('src', "{{URL::asset('public/frontend/')}}/landing_page"+val+"/images/wef-min.jpg");
                    $("#main_iframe").contents().find('.content_file_'+id).html("<img src='{{URL::asset('public/frontend/')}}/landing_page"+val+"/images/wef-min.jpg'>");
            }
        });
        function ytVidId(url) {
            var p = /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
            return (url.match(p)) ? RegExp.$1 : false;
        }
        $(document).delegate('.content_file_youtube', 'blur', function(){
            var id = $(this).data('id');
            console.log("ID: "+id);
            var url = $(this).val();
            console.log(ytVidId(url))
            if (ytVidId(url) !== false) {
                $('#content_file_'+id).val('');
                $("#content_file_youtube_link_error"+id).html('');
                $("#main_iframe").contents().find('.content_file_'+id).html('<iframe src="https://www.youtube.com/embed/'+ytVidId(url)+'" frameborder="0" allowfullscreen></iframe>');
            } else {
                $("#content_file_youtube_link_error"+id).html('Link do youtube inválido.');
                $('#content_file_youtube_link_'+id).val('');
            }
        });
        $(document).delegate('input[type=radio][class=content_button]', 'change', function() {
            var id = $(this).data('id');
            if (this.value == 'Y') {
                $('#content_button_div_'+id).show();
                $("#main_iframe").contents().find('.content_button_'+id).show();
            } else if (this.value == 'N') {
                $('#content_button_div_'+id).hide();
                $("#main_iframe").contents().find('.content_button_'+id).hide();
            }
        });
        $(document).delegate('.content_button_text_inp', 'keyup', function(){
            $("#main_iframe").contents().find('.content_button_'+$(this).data('id')).text(this.value);
        });
        $(document).delegate('.content_button_link_inp', 'keyup', function(){
            $("#main_iframe").contents().find('.content_button_'+$(this).data('id')).attr('href', this.value);
        });
        $(document).delegate('.del_multicontent', 'click', function(){
            var id = parseInt($(this).data('id'));
            // console.log($('#multicontent_'+id).data('saved'));
            if(id == 3 && $('#multicontent_4').length){
                toastr.error("@lang('client_site.remove_content_tab_4')");
            } else {
                if($('#multicontent_'+id).data('saved') == false){
                    $('#multicontent_'+id).remove();
                    loadContentSection(id);
                    $("#main_iframe").contents().find('#content_div_'+id).hide();
                } else {
                    removeMultiContent($('#template_id').val(), id-1);
                }
            }
        });
        $('#add_mult_cntnt').click(function(){
            var i = parseInt($('.multicontent').length) + 1;
            if(i == 5){
                toastr.error("@lang('client_site.maximum_content_limit_reached')");
            } else {
                var html = `<div class="multicontent" id="multicontent_${i}" style="padding: 20px 0px;" data-id="${i}" data-saved="false">
                    <div class="multi_section_head">
                        <h5>Section ${i}</h5>
                        <a href="javascript:;" class="btn del_multicontent" data-id="${i}"> <i class="fa fa-times" aria-hidden="true"></i> </a>
                    </div>
                    <div class="cert_cntnt">
                        <label for="content_file_${i}">@lang('site.upload_file')</label>  <br>
                        <input type="file" name="content_file_${i}" id="content_file_${i}" class="form-control content_file_inp" data-id="${i}" accept="image/*">
                    </div>
                    <div class="cert_cntnt">
                        <label for="content_file_youtube_link_${i}">Youtube Link</label>  <br>
                        <input type="text" name="content_file_youtube_link_${i}" id="content_file_youtube_link_${i}" class="form-control content_file_youtube" data-id="${i}" >
                        </div>
                    <div class="cert_cntnt">
                        <label for="content_heading_${i}">@lang('site.title')</label>  <br>
                        <input type="text" name="content_heading_${i}" id="content_heading_${i}" class="content_heading_inp form-control required" data-id="${i}" value="Online Learning">
                    </div>
                    <div class="cert_cntnt">
                        <label for="content_desc_${i}">@lang('site.description')</label>  <br>
                        <input type="text" name="content_desc_${i}" id="content_desc_${i}" class="content_desc_inp form-control required" data-id="${i}" value="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.">
                    </div>
                    <div class="cert_cntnt">
                        <label for="content_button_${i}">@lang('site.button')</label>  <br>
                        <label for="content_button_yes_${i}">
                            <input type="radio" class="content_button" name="content_button_${i}" value="Y" id="content_button_yes_${i}" data-id="${i}">
                            @lang('site.yes')
                        </label>
                        <label for="content_button_no_${i}">
                            <input type="radio" class="content_button" name="content_button_${i}" value="N" id="content_button_no_${i}" data-id="${i}" checked>
                            @lang('site.no')
                        </label>
                    </div>
                    <div id="content_button_div_${i}" style="display: none;">
                        <div class="cert_cntnt">
                            <label for="content_button_text_${i}">@lang('site.text')</label>  <br>
                            <input type="text" name="content_button_text_${i}" id="content_button_text_${i}" class="content_button_text_inp form-control required" data-id="${i}" value="learn more">
                        </div>
                        <div class="cert_cntnt">
                            <label for="content_button_link_${i}">Link</label>  <br>
                            <input type="url" name="content_button_link_${i}" id="content_button_link_${i}" class="content_button_link_inp form-control required" data-id="${i}">
                        </div>
                    </div>
                </div>`;
                $('#multiple_content_div').append(html);
                $("#main_iframe").contents().find('#content_div_'+i).show();
            }
        });
        $('#save_content').click(function(){
            if($("#myContentForm").valid()){
                if($('input[type=radio][name=body_single_content]:checked').val() == 'Y'){
                    updateTemplate();
                } else {
                    $.get( "{{url('/')}}/landing-page/all-multiple-content-delete/"+$('#template_id').val(), function( res ) {
                        if(res.status == "success") storeMultiContents();
                    });
                    toastr.success("@lang('site.landing_temp_saved_successfully')");
                }
            }
        });
        function removeMultiContent(temp_id, srno){
            $.get( "{{url('/')}}/landing-page/multiple-content-delete/"+temp_id+"/"+srno, function( res ) {
                console.log(res);
                if(res.status == "success"){
                    var v = parseInt(srno)+1;
                    $('#multicontent_'+v).remove();
                    loadContentSection(v);
                    console.log(v);
                    // console.log($("#main_iframe").contents().find('#content_div_'+v)[0]);
                    $("#main_iframe").contents().find('#content_div_'+v).hide();
                }
            });
        }
        function storeMultiContents(){
            $('.multicontent').each(function(i, content){
                var num = parseInt(i)+1;
                console.log(num);
                console.log($('#content_file_youtube_link_'+num).val());
                var uploadFormData = new FormData();
                uploadFormData.append("jsonrpc", "2.0");
                uploadFormData.append("_token", '{{csrf_token()}}');
                uploadFormData.append("landing_page_master_id", $('#template_id').val());
                uploadFormData.append('content_heading', $('#content_heading_'+num).val());
                uploadFormData.append('content_desc', $('#content_desc_'+num).val());
                uploadFormData.append('content_file', $('#content_file_'+num).val() != "" ? $('#content_file_'+num)[0].files[0]: null);
                uploadFormData.append('content_file_youtube_link', $('#content_file_youtube_link_'+num).val() != "" ? $('#content_file_youtube_link_'+num).val(): null);
                uploadFormData.append('content_button', $(`input[type=radio][name=content_button_${num}]:checked`).val());
                uploadFormData.append('content_button_text', $('#content_button_text_'+num).val());
                uploadFormData.append('content_button_link', $('#content_button_link_'+num).val());
                updateMultiContent(uploadFormData);
                console.log($('#multicontent_'+num)[0]);
                $('#multicontent_'+num).data('saved', true);
                $('#multicontent_'+num).attr('data-saved', true);
            });
        }
        function updateMultiContent(uploadFormData){
            $.ajax({
                url: "{{route('store.landing.page.multiple.content')}}",
                method: 'post',
                data: uploadFormData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function(res){
                    console.log(res);
                    if(res.status == "success") {
                        // toastr.success(res.message);
                    } else toastr.error(res.message);
                },
                error: function(err){
                    console.log(err);
                },
            });
        }
        function loadContentSection(srno){
            // Reset content heding
            var str = "Online Learning";
            if($('#template_number').val() == 1){
                var html = "";
                if(str.indexOf(" ") === -1){
                    html = "<span>" + str + "</span>";
                } else {
                    var words = str.split(" ");
                    html = "<span>" + words[0] + "</span> " + str.substring( str.indexOf(" ") + 1, str.length );
                }
                $("#main_iframe").contents().find('.content_heading_'+srno).html(html);
            } else if($('#template_number').val() == 2){
                $("#main_iframe").contents().find('.content_heading_'+srno).text(str);
            }

            // Reset content description
            var desc = '{{@$temp->body_single_content_desc}}';
            console.log("DESC: "+desc);
            $("#main_iframe").contents().find('.content_desc_'+srno).text(desc);

            // Reset content button
            var btn_text = '{{@$temp->body_single_content_button_text}}';
            $("#main_iframe").contents().find('.content_button_'+srno).attr('href', "javascript:;");
            $("#main_iframe").contents().find('.content_button_'+srno).text(btn_text);
            $("#main_iframe").contents().find('.content_button_'+srno).hide();

            // Reset content image
            $("#main_iframe").contents().find('.content_file_'+srno).find('img').attr('src', "{{URL::asset('public/frontend/')}}/landing_page"+$('#template_number').val()+"/images/wef-min.jpg");

        }
    // CONTENT

    function updateTemplate(){
        var style = "";
        var uploadFormData = new FormData();
        uploadFormData.append("jsonrpc", "2.0");
        uploadFormData.append("_token", '{{csrf_token()}}');
        uploadFormData.append('template_number', $('#template_number').val());
        uploadFormData.append('header_image', $('#header_image').val() != "" ? $('#header_image')[0].files[0] : null);
        uploadFormData.append('header_line_1', $('#header_line_1').val());
        uploadFormData.append('header_line_2', $('#header_line_2').val());
        uploadFormData.append('header_button', $('input[type=radio][name=header_btn]:checked').val());
        uploadFormData.append('header_button_text', $('#header_button_text').val());
        uploadFormData.append('header_button_type', $('input[type=radio][name=header_button_type]:checked').val());
        uploadFormData.append('header_button_link', $('#header_button_link').val());
        uploadFormData.append('lead_name', $('#lead_name').is(':checked') ? 'Y' : 'N');
        uploadFormData.append('lead_email', $('#lead_email').is(':checked') ? 'Y' : 'N');
        uploadFormData.append('lead_mobile', $('#lead_mobile').is(':checked') ? 'Y' : 'N');
        uploadFormData.append('lead_address', $('#lead_address').is(':checked') ? 'Y' : 'N');
        uploadFormData.append('lead_facebook', $('#lead_facebook').is(':checked') ? 'Y' : 'N');
        uploadFormData.append('lead_instagram', $('#lead_instagram').is(':checked') ? 'Y' : 'N');
        uploadFormData.append('lead_linkedin', $('#lead_linkedin').is(':checked') ? 'Y' : 'N');
        uploadFormData.append('lead_website', $('#lead_website').is(':checked') ? 'Y' : 'N');
        uploadFormData.append('lead_message', $('#lead_message').is(':checked') ? 'Y' : 'N');
        uploadFormData.append('header_reverse', $('input[type=radio][name=header_reverse]:checked').val());
        uploadFormData.append('header_counter_date', $('#header_counter_date').val());

        uploadFormData.append('body_single_content', $('input[type=radio][name=body_single_content]:checked').val());
        uploadFormData.append('body_single_content_image', $('#body_single_content_image').val() != "" ? $('#body_single_content_image')[0].files[0] : null);
        uploadFormData.append('body_single_content_heading', $('#body_single_content_heading').val());
        uploadFormData.append('body_single_content_desc', $('#body_single_content_desc').val());
        uploadFormData.append('body_single_content_button', $('input[type=radio][name=body_single_content_button]:checked').val());
        uploadFormData.append('body_single_content_button_text', $('#body_single_content_button_text').val());
        uploadFormData.append('body_single_content_button_link', $('#body_single_content_button_link').val());

        uploadFormData.append('color_primary', $('#color_primary').val());
        uploadFormData.append('color_secondary', $('#color_secondary').val());
        uploadFormData.append('color_tertiary', $('#color_tertiary').val());

        uploadFormData.append('single_video', $('input[type=radio][name=single_video]:checked').val());
        uploadFormData.append('single_file', $('#single_file').val() != "" ? $('#single_file')[0].files[0] : null);
        uploadFormData.append('body_single_video_heading', $('#body_single_video_heading').val());
        uploadFormData.append('body_single_video_desc', $('#body_single_video_desc').val());

        $.ajax({
            url: "{{url('/')}}/landing-page/store/"+template_id,
            method: 'post',
            data: uploadFormData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function(res){
                console.log(res);
                if(res.status == "success") {
                    toastr.success(res.message);
                    template_id = res.template.id;
                    detail = res.template;
                    console.log("TEMP ID: "+template_id);
                    $('#background').val('');
                    $('.template'+ $('#template_number').val() ).attr('style', style);
                } else toastr.error(res.message);
            },
            error: function(err){
                console.log(err);
            },
        });
    }

    let hexToRgb= c=> `rgb(${c.match(/\w\w/g).map(x=>+`0x${x}`)})`;

    function timeDiffCalc(dateFuture, dateNow) {
        let diffInMilliSeconds = Math.abs(dateFuture - dateNow) / 1000;
        const days = Math.floor(diffInMilliSeconds / 86400);
        diffInMilliSeconds -= days * 86400;
        const hours = Math.floor(diffInMilliSeconds / 3600) % 24;
        diffInMilliSeconds -= hours * 3600;
        const minutes = Math.floor(diffInMilliSeconds / 60) % 60;
        diffInMilliSeconds -= minutes * 60;
        const seconds = Math.round(diffInMilliSeconds);

        let difference = '';
        if (days > 0) { difference += (days === 1) ? `${days}:` : `${days}:`; }

        difference += (hours < 10 ) ? `0${hours}:` : `${hours}:`;
        difference += (minutes < 10 ) ? `0${minutes}:` : `${minutes}:`;
        difference += (seconds < 10 ) ? `0${seconds}` : `${seconds}`;
        return difference;
    }

</script>
@endsection
