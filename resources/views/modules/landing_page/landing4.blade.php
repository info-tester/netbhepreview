<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Netbhe</title>
    <link href="{{ URL::asset('public/frontend/landing_page4/css/bootstrap.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('public/frontend/landing_page4/css/style.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('public/frontend/landing_page4/css/responsive.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('public/frontend/landing_page4/css/font-awesome/all.min.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('public/frontend/landing_page4/css/jquery.fancybox.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('public/frontend/landing_page4/css/owl.carousel.css') }}" rel="stylesheet">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet'>
    <script src="https://kit.fontawesome.com/2710be599b.js" crossorigin="anonymous"></script>
</head>
<body class="color_primary">
<div id="landing_preloader" class="u-loading">
	<div class="u-loading_symbol landing_logo_loader">
		@if(@$detail->landing_logo)
			<img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->landing_logo_thumbnail}}" alt="" class="landing_logo" />
		@else
			<img src="{{ URL::to('public/frontend/images/loader-logo.png') }}" alt="">
		@endif
	</div>
</div>

<header class="header_sec">
	<div class="container">
		<nav class="navbar navbar-light nav_top">
			<a class="navbar-brand" href="javascript:;">
				@if(@$detail->landing_logo)
					<img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->landing_logo}}" alt="logo" class="landing_logo" />
				@else
					<img src="{{ URL::asset('public/frontend/landing_page4/images/logo1.png') }}" alt="logo" class="landing_logo" />
				@endif
			</a>
			{{-- <div class="header_sos">
				<ul>
					<li class="link_icons link_icons_linkedin" @if(!@$detail->link_linkedin) style="display:none;" @endif ><a href="{{ @$detail->link_linkedin ?? 'javascript:;' }}" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
					<li class="link_icons link_icons_facebook" @if(!@$detail->link_facebook) style="display:none;" @endif ><a href="{{ @$detail->link_facebook ?? 'javascript:;' }}" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
					<li class="link_icons link_icons_twitter" @if(!@$detail->link_twitter) style="display:none;" @endif ><a href="{{ @$detail->link_twitter ?? 'javascript:;' }}" target="_blank"><i class="fab fa-twitter"></i></a></li>
					<li class="link_icons link_icons_instagram" @if(!@$detail->link_instagram) style="display:none;" @endif ><a href="{{ @$detail->link_instagram ?? 'javascript:;' }}" target="_blank"><i class="fab fa-instagram"></i></a></li>
					<li class="link_icons link_icons_pinterest" @if(!@$detail->link_pinterest) style="display:none;" @endif ><a href="{{ @$detail->link_pinterest ?? 'javascript:;' }}" target="_blank"><i class="fab fa-pinterest"></i></a></li>
					<li class="link_icons link_icons_youtube" @if(!@$detail->link_youtube) style="display:none;" @endif ><a href="{{ @$detail->link_youtube ?? 'javascript:;' }}" target="_blank"><i class="fab fa-youtube"></i></a></li>
                    <li class="link_icons link_icons_tiktok" @if(!@$detail->link_tiktok) style="display:none;" @endif ><a href="{{ @$detail->link_tiktok ?? 'javascript:;' }}" target="_blank"><i class="fa-brands fa-tiktok"></i></a></li>
				</ul>
			</div> --}}
		</nav>
	</div>
</header>

<div class="bannersec">
    @if(@$detail->header_image)
		<img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->header_image}}" class="header_image" alt="">
	@else
		<img src="{{ URL::asset('public/frontend/landing_page4/images/h1-sl1.jpg') }}" class="header_image" alt="">
	@endif
	<div class="banner_text">
		<div class="container">
			<div class="banner_box">
                <h5 class="header_line_1">{{@$detail->header_line_1  ?? 'Lorem ipsum dolor sit' }}</h5>
                <p class="header_line_2">{{@$detail->header_line_2  ?? 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.' }}</p>
                @if(@$detail->header_button_type == 'URL')
					<a href="{{@$detail->header_button_link ?? 'javascript:;'}}" class="page_btn header_btn header_button_text color_primary color_secondary_hover" @if(@$detail->header_button=='N') style="display:none" @endif>{{ @$detail->header_button_text ?? 'Contact Us'}}</a>
				@else
					@if(@$detail->lead_position == 'P')
						<a href="#" class="page_btn header_btn header_button_text color_primary color_secondary_hover" data-toggle="modal" data-target="#myModal" @if(@$detail->header_button=='N') style="display:none" @endif>{{ @$detail->header_button_text ?? 'Contact Us'}}</a>
					@else
						<a href="#contact-area" class="page_btn banBotArw header_btn header_button_text color_primary color_secondary_hover" @if(@$detail->header_button=='N') style="display:none" @endif>{{ @$detail->header_button_text ?? 'Contact Us'}}</a>
					@endif
				@endif

				@if(@$detail)
                <div class="counting-date header_reverse" @if(@$detail->header_reverse_counter == 'N') style="display: none;" @endif>
					<p>
                        <p class="header_reverse_text">Esta promoção termina em:<p>
                        <span class="header_counter"></span>
                    </p>
				</div>
				@endif
			</div>
		</div>
	</div>

</div>

<div class="whyus_sec" @if(@$detail->why_us_section == 'N') style="display: none;" @endif>
	<div class="container">
		<div class="page_hed">
			<h2 class="whyus_main_title color_secondary_font"> {{@$detail->why_us_heading ?? 'How it works'}}</h2>
			<p class="whyus_main_desc">{{@$detail->why_us_description ?? 'Your dream home is in good hands.'}}</p>
			<em class="color_secondary"></em>
		</div>
		<div class="whyus_inr">
			<div class="row">
				@if(@$detail->getWhyUs && count(@$detail->getWhyUs) > 0)
					@foreach(@$detail->getWhyUs as $k=>$row)
						@php
							$icon = 'fas fa-file-invoice';
							if(($k+1)%3 == 0) $icon = 'fas fa-chalkboard-teacher';
							elseif(($k+1)%2 == 0) $icon = 'fas fa-handshake';
						@endphp
						<div class="col-sm-4 mb-3" id="why_box_{{@$k + 1}}">
							<div class="whyus_bx color_tertiary">
								<em id="whyus_img_{{@$k + 1}}" class="color_secondary_border">
									@if(strpos( @$row->filetype, 'image' ) !== false)
                                        <img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$row->filename}}" alt="">
                                    @else
										<i class="{{@$icon}} color_secondary_font"></i>
                                    @endif
								</em>
								<strong><a href="#javascript:;" id="whyus_title_{{@$k + 1}}" class="color_secondary_font_hover">{{@$row->title ?? 'certified contractors'}}</a></strong>
								<p id="whyus_desc_{{@$k + 1}}">{{@$row->description ?? 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.'}}</p>
							</div>
						</div>
					@endforeach
				@else
					<div class="col-sm-4 mb-3" id="why_box_1">
						<div class="whyus_bx color_tertiary">
							<em id="whyus_img_1" class="color_secondary_border">
								<i class="fas fa-file-invoice color_secondary_font"></i>
							</em>
							<strong><a href="javascript:;" id="whyus_title_1" class="color_secondary_font_hover">certified contractors</a></strong>
							<p id="whyus_desc_1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.</p>
						</div>
					</div>
					<div class="col-sm-4 mb-3" id="why_box_2">
						<div class="whyus_bx color_tertiary">
							<em id="whyus_img_2" class="color_secondary_border">
								<i class="fas fa-handshake color_secondary_font"></i>
							</em>
							<strong><a href="javascript:;" id="whyus_title_2" class="color_secondary_font_hover">employees you can trust</a></strong>
							<p id="whyus_desc_2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.</p>
						</div>
					</div>
					<div class="col-sm-4 mb-3" id="why_box_3">
						<div class="whyus_bx color_tertiary">
							<em id="whyus_img_3" class="color_secondary_border">
								<i class="fas fa-chalkboard-teacher color_secondary_font"></i>
							</em>
							<strong><a href="javascript:;" id="whyus_title_3" class="color_secondary_font_hover">Your dream is our plan</a></strong>
							<p id="whyus_desc_3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.</p>
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>

<!--start about area-->
<section class="about_pro single_content_div" @if(@$detail->body_single_content == 'Y') @else style="display: none;" @endif >
	<div class="container">
		<div class="about_pro_inr">
			<div class="row">
				<div class="col-lg-6">
					<div class="about_pro_img color_secondary_border single_content_file_div @if(@$detail->image_style=='C') square_image @endif" id="content_file_single">
						{{-- <span class="single_content_file_div " id="content_file_single"> --}}
							@if(@$detail->body_single_content_image)
								<img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->body_single_content_image}}" class="single_content_file">
							@else
								<img src="{{ URL::to('public/frontend/landing_page4/images/img.jpg') }}" class="single_content_file" alt="">
							@endif
						{{-- </span> --}}
					</div>
				</div>
				<div class="col-lg-6">
					<div class="about_pro_text color_secondary_border">
						<h2 class="body_single_content_heading">{{ @$detail->body_single_content_heading ?? 'Lorem Ipsum Dolor Sit Amet' }}</h2>
						<p class="body_single_content_desc">{{ @$detail->body_single_content_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.' }}</p>
						<a href="{{ @$detail->body_single_content_button_link ?? 'javascrip:;' }}" class="page_btn body_single_content_button body_single_content_button_text body_single_content_button_link color_tertiary color_primary_font_hover color_secondary_hover" @if(@$detail->body_single_content_button == 'N') style="display: none;" @endif >
							{{ @$detail->body_single_content_button_text ?? 'Read More' }}
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="about_pro multiple_content_div" @if(@$detail->body_single_content == 'N') @else style="display: none;" @endif >
	<div class="container">
		<div class="about_pro_inr">
			<div class="row" id="content_div_1">
				<div class="col-lg-6">
					<div class="about_pro_img color_secondary_border content_file_1 @if((@$detail->getContentDetails[0]->content_file_type == 'I' || @$detail->getContentDetails[0]->content_file_type==null) && @$detail->getContentDetails[0]->image_style == 'C') square_image @endif"  id="content_file_1">
						@if(@$detail->getContentDetails[0]->content_file)
							@if(@$detail->getContentDetails[0]->content_file_type == 'I')
								<img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->getContentDetails[0]->content_file}}" alt="">
							@else
								<iframe src="https://www.youtube.com/embed/{{@$detail->getContentDetails[0]->content_file}}" frameborder="0" allowfullscreen></iframe>
							@endif
						@else
							<img src="{{ URL::asset('public/frontend/landing_page4/images/img.jpg') }}" alt="">
						@endif
					</div>
				</div>
				<div class="col-lg-6">
					<div class="about_pro_text color_secondary_border">
						<h2 class="content_heading_1">{{ @$detail->getContentDetails[0]->content_heading ?? 'Lorem Ipsum Dolor Sit Amet' }}</h2>
						<p class="content_desc_1">{{ @$detail->getContentDetails[0]->content_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'}}</p>
						<a href="{{ @$detail->getContentDetails[0]->content_button_link ?? 'javascript:;' }}" class="page_btn color_tertiary color_primary_font_hover color_secondary_hover content_button_1"
							@if(@$detail->getContentDetails[0]->content_button == 'N') style="display: none;" @endif >
							{{ @$detail->getContentDetails[0]->content_button_text ?? 'Read More' }}
						</a>
					</div>
				</div>
			</div>
			<div class="row" id="content_div_2">
				<div class="col-lg-6">
					<div class="about_pro_img color_secondary_border content_file_2 @if((@$detail->getContentDetails[1]->content_file_type == 'I' || @$detail->getContentDetails[1]->content_file_type==null) && @$detail->getContentDetails[1]->image_style == 'C') square_image @endif"  id="content_file_2">
						@if(@$detail->getContentDetails[1]->content_file)
							@if(@$detail->getContentDetails[1]->content_file_type == 'I')
								<img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->getContentDetails[1]->content_file}}" alt="">
								@else
								<iframe src="https://www.youtube.com/embed/{{@$detail->getContentDetails[1]->content_file}}" frameborder="0" allowfullscreen></iframe>
							@endif
						@else
							<img src="{{ URL::asset('public/frontend/landing_page4/images/img.jpg') }}" alt="">
						@endif
					</div>
				</div>
				<div class="col-lg-6">
					<div class="about_pro_text color_secondary_border">
						<h2 class="content_heading_2">{{ @$detail->getContentDetails[1]->content_heading ?? 'Lorem Ipsum Dolor Sit Amet' }}</h2>
						<p class="content_desc_2">{{ @$detail->getContentDetails[1]->content_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.' }}</p>
						<a href="{{ @$detail->getContentDetails[1]->content_button_link ?? 'javascript:;' }}" class="page_btn color_tertiary color_primary_font_hover color_secondary_hover content_button_2"
							@if(@$detail->getContentDetails[1]->content_button == 'Y') @else style="display: none;" @endif >
							{{ @$detail->getContentDetails[1]->content_button_text ?? 'Read More' }}
						</a>
					</div>
				</div>
			</div>
			<div class="row" id="content_div_3" @if(@$detail->getContentDetails[2] == null) style="display: none;" @endif >
				<div class="col-lg-6">
					<div class="about_pro_img color_secondary_border content_file_3 @if((@$detail->getContentDetails[2]->content_file_type == 'I' || @$detail->getContentDetails[2]->content_file_type==null) && @$detail->getContentDetails[2]->image_style == 'C') square_image @endif"  id="content_file_3">
						@if(@$detail->getContentDetails[2]->content_file)
							@if(@$detail->getContentDetails[2]->content_file_type == 'I')
								<img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->getContentDetails[2]->content_file}}" alt="">
							@else
								<iframe src="https://www.youtube.com/embed/{{@$detail->getContentDetails[2]->content_file}}" frameborder="0" allowfullscreen></iframe>
							@endif
						@else
							<img src="{{ URL::asset('public/frontend/landing_page4/images/img.jpg') }}" alt="">
						@endif
					</div>
				</div>
				<div class="col-lg-6">
					<div class="about_pro_text color_secondary_border">
						<h2 class="content_heading_3">{{ @$detail->getContentDetails[2]->content_heading ?? 'Lorem Ipsum Dolor Sit Amet' }}</h2>
						<p class="content_desc_3">{{ @$detail->getContentDetails[2]->content_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'}}</p>
						<a href="{{ @$detail->getContentDetails[2]->content_button_link ?? 'javascript:;' }}" class="page_btn color_tertiary color_primary_font_hover color_secondary_hover content_button_3"
							@if(@$detail->getContentDetails[2]->content_button == 'Y') @else style="display: none;" @endif >
							{{ @$detail->getContentDetails[2]->content_button_text ?? 'Read More' }}
						</a>
					</div>
				</div>
			</div>
			<div class="row" id="content_div_4" @if(@$detail->getContentDetails[3] == null) style="display: none;" @endif >
				<div class="col-lg-6">
					<div class="about_pro_img color_secondary_border content_file_4 @if((@$detail->getContentDetails[3]->content_file_type == 'I' || @$detail->getContentDetails[3]->content_file_type==null) && @$detail->getContentDetails[3]->image_style == 'C') square_image @endif"  id="content_file_4">
						@if(@$detail->getContentDetails[3]->content_file)
							@if(@$detail->getContentDetails[3]->content_file_type == 'I')
								<img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->getContentDetails[3]->content_file}}" alt="">
								@else
								<iframe src="https://www.youtube.com/embed/{{@$detail->getContentDetails[3]->content_file}}" frameborder="0" allowfullscreen></iframe>
							@endif
						@else
							<img src="{{ URL::asset('public/frontend/landing_page4/images/img.jpg') }}" alt="">
						@endif
					</div>
				</div>
				<div class="col-lg-6">
					<div class="about_pro_text color_secondary_border">
						<h2 class="content_heading_4">{{ @$detail->getContentDetails[3]->content_heading ?? 'Lorem Ipsum Dolor Sit Amet' }}</h2>
						<p class="content_desc_4">{{ @$detail->getContentDetails[3]->content_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.' }}</p>
						<a href="{{ @$detail->getContentDetails[3]->content_button_link ?? 'javascript:;' }}" class="page_btn color_tertiary color_primary_font_hover color_secondary_hover content_button_4"
							@if(@$detail->getContentDetails[3]->content_button == 'Y') @else style="display: none;" @endif >
							{{ @$detail->getContentDetails[3]->content_button_text ?? 'Read More' }}
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--end about area-->

<!--start product area-->
<section class="produt_sec color_tertiary" id="products-initial" @if(@$detail->getProductDetails && count(@$detail->getProductDetails) > 0) style="display: none;" @endif>
	<div class="container">
		<div class="page_hed">
			<h3 class="product_section_heading">{{ @$detail->product_section_heading ?? 'Our Products' }}</h3>
			<p class="product_section_desc">{{ @$detail->product_section_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.' }}</p>
			<em></em>
		</div>
		<div class="produt_inr">
			<div class="produt_slid" @if(@$detail->product_slider == 'N') style="display: none;" @endif >
				<div class="product-carousel owl-carousel color_secondary_owl">
					<div class="item">
						<div class="produt_img">
							<div class="pro_ig">
								<img src="{{ URL::asset('public/frontend/landing_page4/images/vedioimg.jpg') }}" alt="">
								<span class="pro_pr_bx">R$800</span>
								<div class="pri_dv color_secondary"><strong><i class="fas fa-clock"></i> 3:08:33:23</strong></div>
							</div>
							<div class="produt_text">
								<h5>
									<a href="#url">Product 1</a>
								</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>

									<a href="javascript:;" class="page_btn"
								>More <i class="fa fa-angle-double-right"></i></a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="produt_img">
							<div class="pro_ig">
								<img src="{{ URL::asset('public/frontend/landing_page4/images/vedioimg.jpg') }}" alt="">
								<span class="pro_pr_bx">R$800</span>
								<div class="pri_dv color_secondary"><strong><i class="fas fa-clock"></i> 3:08:33:23</strong></div>
							</div>
							<div class="produt_text">
								<h5><a href="#url">Product 1</a></h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>

									<a href="javascript:;" class="page_btn"
								>More <i class="fa fa-angle-double-right"></i></a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="produt_img">
							<div class="pro_ig">
								<img src="{{ URL::asset('public/frontend/landing_page4/images/vedioimg.jpg') }}" alt="">
								<span class="pro_pr_bx">R$800</span>
								<div class="pri_dv color_secondary"><strong><i class="fas fa-clock"></i> 3:08:33:23</strong></div>
							</div>
							<div class="produt_text">
								<h5><a href="#url">Product 1</a></h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>

									<a href="javascript:;" class="page_btn"
								>More <i class="fa fa-angle-double-right"></i></a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="produt_img">
							<div class="pro_ig">
								<img src="{{ URL::asset('public/frontend/landing_page4/images/vedioimg.jpg') }}" alt="">
								<span class="pro_pr_bx">R$800</span>
								<div class="pri_dv color_secondary"><strong><i class="fas fa-clock"></i> 3:08:33:23</strong></div>
							</div>
							<div class="produt_text">
								<h5><a href="#url">Product 1</a></h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>

									<a href="javascript:;" class="page_btn"
								>More <i class="fa fa-angle-double-right"></i></a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="produt_img">
							<div class="pro_ig">
								<img src="{{ URL::asset('public/frontend/landing_page4/images/vedioimg.jpg') }}" alt="">
								<span class="pro_pr_bx">R$800</span>
								<div class="pri_dv color_secondary"><strong><i class="fas fa-clock"></i> 3:08:33:23</strong></div>
							</div>
							<div class="produt_text">
								<h5><a href="#url">Product 1</a></h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>

									<a href="javascript:;" class="page_btn"
								>More <i class="fa fa-angle-double-right"></i></a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="produt_img">
							<div class="pro_ig">
								<img src="{{ URL::asset('public/frontend/landing_page4/images/vedioimg.jpg') }}" alt="">
								<span class="pro_pr_bx">R$800</span>
								<div class="pri_dv color_secondary"><strong><i class="fas fa-clock"></i> 3:08:33:23</strong></div>
							</div>
							<div class="produt_text">
								<h5><a href="#url">Product 1</a></h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>

									<a href="javascript:;" class="page_btn"
								>More <i class="fa fa-angle-double-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- <div class="produt_slid row" @if(@$detail->product_slider == 'Y') style="display: none;" @endif >
				<div class="item col-md-4">
					<div class="produt_img">
						<div class="pro_ig">
							<img src="{{ URL::asset('public/frontend/landing_page4/images/vedioimg.jpg') }}" alt="">
							<span class="pro_pr_bx">R$800</span>
							<div class="pri_dv color_secondary"><strong><i class="fas fa-clock"></i> 3:08:33:23</strong></div>
						</div>
						<div class="produt_text">
							<h5>
								<a href="#url">Product 1</a>
							</h5>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>

								<a href="javascript:;" class="page_btn"
							>More <i class="fa fa-angle-double-right"></i></a>
						</div>
					</div>
				</div>
				<div class="item col-md-4">
					<div class="produt_img">
						<div class="pro_ig">
							<img src="{{ URL::asset('public/frontend/landing_page4/images/vedioimg.jpg') }}" alt="">
							<span class="pro_pr_bx">R$800</span>
							<div class="pri_dv color_secondary"><strong><i class="fas fa-clock"></i> 3:08:33:23</strong></div>
						</div>
						<div class="produt_text">
							<h5><a href="#url">Product 1</a></h5>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>

								<a href="javascript:;" class="page_btn"
							>More <i class="fa fa-angle-double-right"></i></a>
						</div>
					</div>
				</div>
				<div class="item col-md-4">
					<div class="produt_img">
						<div class="pro_ig">
							<img src="{{ URL::asset('public/frontend/landing_page4/images/vedioimg.jpg') }}" alt="">
							<span class="pro_pr_bx">R$800</span>
							<div class="pri_dv color_secondary"><strong><i class="fas fa-clock"></i> 3:08:33:23</strong></div>
						</div>
						<div class="produt_text">
							<h5><a href="#url">Product 1</a></h5>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>

								<a href="javascript:;" class="page_btn"
							>More <i class="fa fa-angle-double-right"></i></a>
						</div>
					</div>
				</div>
				<div class="item col-md-4">
					<div class="produt_img">
						<div class="pro_ig">
							<img src="{{ URL::asset('public/frontend/landing_page4/images/vedioimg.jpg') }}" alt="">
							<span class="pro_pr_bx">R$800</span>
							<div class="pri_dv color_secondary"><strong><i class="fas fa-clock"></i> 3:08:33:23</strong></div>
						</div>
						<div class="produt_text">
							<h5><a href="#url">Product 1</a></h5>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>

								<a href="javascript:;" class="page_btn"
							>More <i class="fa fa-angle-double-right"></i></a>
						</div>
					</div>
				</div>
				<div class="item col-md-4">
					<div class="produt_img">
						<div class="pro_ig">
							<img src="{{ URL::asset('public/frontend/landing_page4/images/vedioimg.jpg') }}" alt="">
							<span class="pro_pr_bx">R$800</span>
							<div class="pri_dv color_secondary"><strong><i class="fas fa-clock"></i> 3:08:33:23</strong></div>
						</div>
						<div class="produt_text">
							<h5><a href="#url">Product 1</a></h5>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>

								<a href="javascript:;" class="page_btn"
							>More <i class="fa fa-angle-double-right"></i></a>
						</div>
					</div>
				</div>
				<div class="item col-md-4">
					<div class="produt_img">
						<div class="pro_ig">
							<img src="{{ URL::asset('public/frontend/landing_page4/images/vedioimg.jpg') }}" alt="">
							<span class="pro_pr_bx">R$800</span>
							<div class="pri_dv color_secondary"><strong><i class="fas fa-clock"></i> 3:08:33:23</strong></div>
						</div>
						<div class="produt_text">
							<h5><a href="#url">Product 1</a></h5>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>

								<a href="javascript:;" class="page_btn"
							>More <i class="fa fa-angle-double-right"></i></a>
						</div>
					</div>
				</div>
			</div> -->
		</div>
	</div>
</section>

<section class="produt_sec color_tertiary" id="products-actual" @if(!@$detail->getProductDetails || count(@$detail->getProductDetails) == 0) style="display: none;" @endif>
	<div class="container">
		<div class="page_hed">
			<h3 class="product_section_heading color_secondary_font">{{ @$detail->product_section_heading ?? 'Our Products' }}</h3>
			<p class="product_section_desc">{{ @$detail->product_section_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.' }}</p>
			<em class="color_secondary"></em>
		</div>
		<div class="produt_inr with-slider" @if(@$detail->product_slider == 'N') style="display: none;" @endif >
			<div class="produt_slid">
				<div class="product-actual-carousel owl-carousel color_secondary_owl">
					@if(@$detail->getProductDetails && count(@$detail->getProductDetails)>0)
						@foreach(@$detail->getProductDetails as $k=>$product)
							<div class="item product_div_{{ @$k + 1 }} proddiv">
								<div class="produt_img color_secondary_border">
									<div class="pro_ig">
										<div class="file_div">
											@if(@$product->product_file)
												@if(@$product->product_file_type == 'I')
													<img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$product->product_file}}" alt="" class="animation-jump product_file_1">
												@elseif(@$product->product_file_type == 'Y')
													<img src="http://img.youtube.com/vi/{{@$product->product_file}}/0.jpg" alt="" class="animation-jump product_file_{{ @$k + 1 }}"/>
													{{-- <div class="playIcon animation-jump">
														<a href="http://www.youtube.com/watch?v={{@$product->product_file}}" class="fancybox-media">
															<img src="{{URL::asset('public/frontend/landing_page4/images/playIcon.png')}}" alt="" rel="media-gallery"></a>
													</div> --}}
                                                    <div class="playIcon animation-jump">
                                                        <div class="video-player paly_btn vid-icon-you">
                                                            <a href="http://www.youtube.com/watch?v={{@$product->product_file}}" class="fancybox-media color_primary">
                                                                {{-- <img src="{{URL::asset('public/frontend/landing_page1/images/playIcon.png')}}" alt="" rel="media-gallery"> --}}
                                                                <i class="fa fa-play" aria-hidden="true" rel="media-gallery"></i>
                                                            </a>
                                                        </div>
                                                    </div>
												@endif
                                                @else
                                                <img src="{{ URL::to('public/frontend/images/NoImage.jpg') }}" alt="" class="product_file_{{ @$k + 1 }}">
											@endif
										</div>
										<span class="pro_pr_bx color_primary product_price_{{ @$k + 1 }}" @if(@$product->product_price_show == 'N') style="display: none;" @endif >R${{@$product->product_price ?? '800' }}</span>
										<div class="pri_dv color_secondary">
											<strong class="product_expiry_div_{{ @$k + 1 }}" @if(@$product->product_reverse_counter == 'N') style="display:none;" @endif>
												<i class="fas fa-clock"></i>
												<span class="product_expiry_{{ @$k + 1 }}">0:00:00:00</span>
											</strong>
										</div>
									</div>
									<div class="produt_text">
										<h5 class="color_secondary_border">
											<a href="{{ @$product->product_button_url ?? 'javascript:;' }}" class="product_heading_{{ @$k + 1 }} color_secondary_font">{{ @$product->product_heading ?? 'New Product' }}</a>
										</h5>
										<p class="product_desc_{{ @$k + 1 }}">{{ @$product->product_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo' }}</p>

										<a href="{{ @$product->product_button_url ?? 'javascript:;' }}" class="page_btn color_secondary_font product_link_{{ @$k + 1 }}" @if(@$product->product_button == 'N') style="display:none;" @endif >
											<span>{{@$product->product_button_caption ?? 'More'}}</span>
											<i class="fa fa-angle-double-right"></i>
										</a>
									</div>
								</div>
							</div>
						@endforeach
					@else
						<div class="item product_div_1 proddiv">
							<div class="produt_img color_secondary_border">
								<div class="pro_ig">
									<div class="file_div">
									</div>
									<span class="pro_pr_bx color_primary product_price_1">R$800</span>
									<div class="pri_dv product_expiry_div_1 color_secondary">
										<strong>
											<i class="fas fa-clock"></i>
											<span class="product_expiry_1">0:00:00:00</span>
										</strong>
									</div>
								</div>
								<div class="produt_text">
									<h5 class="color_secondary_border">
										<a href="javascript:;" class="product_heading_1 color_secondary_font">New Product</a>
									</h5>
									<p class="product_desc_1">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
									<a href="javascript:;" class="page_btn color_secondary_font product_link_1">
										<span>More</span>
										<i class="fa fa-angle-double-right"></i>
									</a>
								</div>
							</div>
						</div>
					@endif
				</div>
			</div>
		</div>
		<div class="produt_inr without-slider" @if(@$detail->product_slider == 'Y') style="display: none;" @endif >
			<div class="produt_slid row row_main">
				@if(@$detail->getProductDetails && count(@$detail->getProductDetails)>0)
					@foreach(@$detail->getProductDetails as $k=>$product)
						<div class="item col-md-4 product_div_{{ @$k + 1 }} proddiv">
							<div class="produt_img color_secondary_border">
								<div class="pro_ig">
									<div class="file_div">
										@if(@$product->product_file)
											@if(@$product->product_file_type == 'I')
												<img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$product->product_file}}" alt="" class="animation-jump product_file_1">
											@elseif(@$product->product_file_type == 'Y')
												<img src="http://img.youtube.com/vi/{{@$product->product_file}}/0.jpg" alt="" class="animation-jump product_file_{{ @$k + 1 }}"/>
												{{-- <div class="playIcon animation-jump">
													<a href="http://www.youtube.com/watch?v={{@$product->product_file}}" class="fancybox-media">
														<img src="{{URL::asset('public/frontend/landing_page4/images/playIcon.png')}}" alt="" rel="media-gallery"></a>
												</div> --}}
                                                <div class="playIcon animation-jump">
                                                    <div class="video-player paly_btn vid-icon-you">
                                                        <a href="http://www.youtube.com/watch?v={{@$product->product_file}}" class="fancybox-media color_primary">
                                                            {{-- <img src="{{URL::asset('public/frontend/landing_page1/images/playIcon.png')}}" alt="" rel="media-gallery"> --}}
                                                            <i class="fa fa-play" aria-hidden="true" rel="media-gallery"></i>
                                                        </a>
                                                    </div>
                                                </div>
											@endif
                                            @else
                                            <img src="{{ URL::to('public/frontend/images/NoImage.jpg') }}" alt="" class="product_file_{{ @$k + 1 }}">
										@endif
									</div>
									<span class="pro_pr_bx color_primary product_price_{{ @$k + 1 }}" @if(@$product->product_price_show == 'N') style="display: none;" @endif >R${{@$product->product_price ?? '800' }}</span>
									<div class="pri_dv color_secondary">
										<strong class="product_expiry_div_{{ @$k + 1 }}" @if(@$product->product_reverse_counter == 'N') style="display:none;" @endif>
											<i class="fas fa-clock"></i>
											<span class="product_expiry_{{ @$k + 1 }}">0:00:00:00</span>
										</strong>
									</div>
								</div>
								<div class="produt_text">
									<h5 class="color_secondary_border">
										<a href="{{ @$product->product_button_url ?? 'javascript:;' }}" class="product_heading_{{ @$k + 1 }} color_secondary_font">{{ @$product->product_heading ?? 'New Product' }}</a>
									</h5>
									<p class="product_desc_{{ @$k + 1 }}">{{ @$product->product_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo' }}</p>

									<a href="{{ @$product->product_button_url ?? 'javascript:;' }}" class="page_btn color_secondary_font product_link_{{ @$k + 1 }}" @if(@$product->product_button == 'N') style="display:none;" @endif >
										<span>{{@$product->product_button_caption ?? 'More'}}</span>
										<i class="fa fa-angle-double-right"></i>
									</a>
								</div>
							</div>
						</div>
					@endforeach
				@else
					<div class="item col-md-4 product_div_1 proddiv">
						<div class="produt_img color_secondary_border">
							<div class="pro_ig">
								<div class="file_div">
								</div>
								<span class="pro_pr_bx color_primary product_price_1">R$800</span>
								<div class="pri_dv product_expiry_div_1 color_secondary">
									<strong>
										<i class="fas fa-clock"></i>
										<span class="product_expiry_1">0:00:00:00</span>
									</strong>
								</div>
							</div>
							<div class="produt_text">
								<h5 class="color_secondary_border">
									<a href="javascript:;" class="product_heading_1 color_secondary_font">New Product</a>
								</h5>
								<p class="product_desc_1">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
								<a href="javascript:;" class="page_btn color_secondary_font product_link_1">
									<span>More</span>
									<i class="fa fa-angle-double-right"></i>
								</a>
							</div>
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
</section>
<!--end product area-->

<section id="video-area" class="bg-gray ved_1 single_video" @if(@$detail->body_single_video == 'N') style="display: none;" @endif >
	<div class="container">
		<div class="row">
			<!--start heading-->
			<div class="col-md-6">
				<div class="page_hed">
					<h2 class="body_single_video_heading color_secondary_font">{{ @$detail->body_single_video_heading ?? 'Get product more information from the video' }}</h2>
					<p class="body_single_video_desc">{{ @$detail->body_single_video_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.' }}</p>
				</div>
			</div>
			<div class="col-md-6">
				<div class="video-player-wrap ved_img">
					<div class="single_vdo">
						@if(@$detail->body_single_video_type == "V")
							<video src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->body_single_video_filename}}" class="color_secondary_border color_tertiary_shadow"></video>
						@elseif(@$detail->body_single_video_type == "I")
							<img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->body_single_video_filename}}" alt="" class="color_secondary_border color_tertiary_shadow">
						@else
							<img src="{{ URL::asset('public/frontend/landing_page4/images/skill.jpg') }}" alt="" class="color_secondary_border color_tertiary_shadow">
						@endif
					</div>
					<div class="video-player paly_btn" @if(@$detail->body_single_video_type == "I") style="display: none;" @endif >
						<a class="popup-video fancybox-media color_primary_light" href="javascript:;" onclick="play_vdo()"><i class="fa fa-play" aria-hidden="true"  rel="media-gallery"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="faq_sec" id="faq_sec" @if(@$detail->show_faq == 'N') style="display: none;" @endif>
	<div class="container">
		<div class="page_hed">
			<h3 class="faq_heading color_secondary_font">{{@$detail->faq_heading ?? 'Faq'}}</h3>
			<p class="faq_description">{{@$detail->faq_description ?? 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.'}}</p>
			<em class="color_secondary"></em>
		</div>
		<div class="top-main-profile">
			<div class="contact-wrap">
            <div id="accordion" class="accordionlanding1">
					<div class="card">
						@if(@$detail->getFaq && count(@$detail->getFaq) > 0)
							@foreach(@$detail->getFaq as $k=>$faq)
								<div class="faq_{{@$k + 1}} card-header collapsed color_primary color_secondary_font color_secondary_border" data-toggle="collapse" href="#collapse{{@$k + 1}}">
									<a class="card-title faq_title_{{@$k + 1}}">
										{{@$faq->title ?? 'This is a simply dummy question text show here?'}}
									</a>
									<i class="fa fa fa-angle-double-down color_secondary_font" aria-hidden="true"></i>
								</div>
								<div id="collapse{{@$k + 1}}" class="faq_{{@$k + 1}} card-body mb-3 collapse color_primary color_secondary_border" data-parent="#accordion" >
									<p class="faq_desc_{{@$k + 1 }}">
										{{@$faq->description ?? 'Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur mi gravida ac. Nunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet ante vitae ante facilisis lobortis temporamet ante vitae ante facilisis lobortis sit amet consectetur adipiscfiing. Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur gravidaNunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet ante vitae ante.'}}
									</p>
								</div>
							@endforeach
						@else
							<div class="faq_1 card-header collapsed color_primary color_secondary_font color_secondary_border" data-toggle="collapse" href="#collapse1">
								<a class="card-title faq_title_1">
									This is a simply dummy question text show here?
								</a>
                                <i class="fa fa-angle-double-down color_secondary_font" aria-hidden="true"></i>
							</div>
							<div id="collapse1" class="faq_1 card-body collapse color_primary color_secondary_border" data-parent="#accordion">
								<p class="faq_desc_1">
									Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur mi gravida ac. Nunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet ante vitae ante facilisis lobortis temporamet ante vitae ante facilisis lobortis sit amet consectetur adipiscfiing. Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur gravidaNunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet ante vitae ante.
								</p>
							</div>
						@endif
					</div>
				</div>

				<!-- <ul id="accordion" class="accordion">
                    <li>
                      <div class="link">This is a simply dummy question text show here?<i class="fa fa-angle-double-down"></i></div>
                      <ul class="submenu">
                        <p class="pm"> Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur mi gravida ac. Nunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet ante vitae ante facilisis lobortis temporamet ante vitae ante facilisis lobortis sit amet consectetur adipiscfiing.
                          Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur gravidaNunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet ante vitae ante. </p>
                      </ul>
                    </li>
                    <li>
                      <div class="link">This is simply dummy question here?<i class="fa fa-angle-double-down"></i></div>
                      <ul class="submenu" style="">
                        <p>Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur gravidaNunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet</p>
                      </ul>
                    </li>
                    <li>
                      <div class="link">This is simply dummy name question here?<i class="fa fa-angle-double-down"></i></div>
                      <ul class="submenu" style="">
                        <p>Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur gravidaNunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet</p>
                      </ul>
                    </li>
                    <li>
                      <div class="link">This is a simply dummy question text show here?<i class="fa fa-angle-double-down"></i></div>
                      <ul class="submenu">
                        <p class="pm"> Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur mi gravida ac. Nunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet ante vitae ante facilisis lobortis temporamet ante vitae ante facilisis lobortis sit amet consectetur adipiscfiing.
                        Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur gravidaNunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet ante vitae ante. </p>
                      </ul>
                    </li>
                </ul> -->
			</div>
		</div>
	</div>
</div>

<section id="contact-area" class="contact-sec color_tertiary"  @if(@$detail->header_button_type == 'URL' || @$detail->lead_position == 'P') style="display: none;" @endif>
	<div class="container">
		<div class="row">
			<!-- <div class="col-lg-12">
				<div class="cont-info">
					<div class="row">
						<div class="col-sm-4">
							<div class="cont-info-single">
								<em class="color_secondary"><i class="fas fa-map-marker-alt color_tertiary_font"></i></em>
								<h5>Address</h5>
								<p>Lorem ipsum dolor sit amet</p>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="cont-info-single">
								<em class="color_secondary"><i class="fas fa-envelope-open color_tertiary_font"></i></em>
								<h5>Email</h5>
								<p>contato@netbhe.com </p>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="cont-info-single">
								<em class="color_secondary"><i class="fas fa-phone color_tertiary_font"></i></em>
								<h5>Phone</h5>
								<p>1934567890</p>
							</div>
						</div>
					</div>
				</div>
			</div> -->
			<div class="col-lg-12">
				<div class="contact-form">
					<div class="page_hed">
					<h3 class="lead_section_heading">{{@$detail->lead_section_heading ?? 'Contact With Us'}}</h3>
					<p class="lead_section_desc">{{@$detail->lead_section_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.'}}</p>
					<em class="color_secondary"></em>
				</div>

					<form action="{{route('landing.page.save.lead')}}" id="lead_form_footer" method="post">
						@csrf
						<input type="text" name="landing_page_master_id" value="{{@$detail->id}}" hidden >
						<div class="row">
							<div class="col-sm-12 col-md-6 lead_fname" @if(@$detail->lead_fname == 'N') style="display: none;" @endif>
								<div class="form-group lead_light_back">
									<input type="text" name="fname" id="fname" class="required form-control color_tertiary_light" placeholder="@lang('client_site.first_name')">
								</div>
							</div>
							<div class="col-sm-12 col-md-6 lead_lname" @if(@$detail->lead_lname == 'N') style="display: none;" @endif>
								<div class="form-group lead_light_back">
									<input type="text" name="lname" id="lname" class="required form-control color_tertiary_light" placeholder="@lang('client_site.last_name')">
								</div>
							</div>
							<div class="col-sm-12 col-md-6 lead_country_code" @if(@$detail->lead_country_code == 'N') style="display: none;" @endif>
								<div class="form-group lead_light_back">
									<input type="number" name="country_code" id="country_code" class="required number form-control color_tertiary_light" placeholder="@lang('site.country_code')">
								</div>
							</div>
							<div class="col-sm-12 col-md-6 lead_mobile" @if(@$detail->lead_mobile == 'N') style="display: none;" @endif>
								<div class="form-group lead_light_back">
									<input type="tel" name="mobile" id="mobile" class="required number form-control color_tertiary_light" placeholder="@lang('site.contact_form_mobile')">
								</div>
							</div>
							<div class="col-sm-12 col-md-6 lead_email" @if(@$detail->lead_email == 'N') style="display: none;" @endif>
								<div class="form-group lead_light_back">
									<input type="email" name="email" id="email" class="required form-control color_tertiary_light" placeholder="@lang('site.contact_form_email')">
								</div>
							</div>
							<div class="col-sm-12 col-md-6 lead_address" @if(@$detail->lead_address == 'N') style="display: none;" @endif>
								<div class="form-group lead_light_back">
									<input type="text" name="address" id="address" class="required form-control color_tertiary_light" placeholder="@lang('site.street_address')">
								</div>
							</div>
							<div class="col-sm-12 col-md-6 lead_city" @if(@$detail->lead_city == 'N') style="display: none;" @endif>
								<div class="form-group lead_light_back">
									<input type="text" name="city" id="city" class="required form-control color_tertiary_light" placeholder="@lang('site.city')">
								</div>
							</div>
							<div class="col-sm-12 col-md-6 lead_state" @if(@$detail->lead_state == 'N') style="display: none;" @endif>
								<div class="form-group lead_light_back">
									<input type="text" name="state" id="state" class="required form-control color_tertiary_light" placeholder="@lang('site.state')">
								</div>
							</div>
							<div class="col-sm-12 col-md-6 lead_website" @if(@$detail->lead_website == 'N') style="display: none;" @endif>
								<div class="form-group lead_light_back">
									<input type="url" name="website" id="website" class="required form-control color_tertiary_light" placeholder="@lang('site.contact_form_website')">
								</div>
							</div>
							<div class="col-sm-12 col-md-6 lead_facebook" @if(@$detail->lead_facebook == 'N') style="display: none;" @endif>
								<div class="form-group lead_light_back">
									<input type="url" name="facebook_link" id="facebook_link" class="form-control color_tertiary_light" placeholder="@lang('site.contact_form_facebook_link')">
								</div>
							</div>
							<div class="col-sm-12 col-md-6 lead_instagram" @if(@$detail->lead_instagram == 'N') style="display: none;" @endif>
								<div class="form-group lead_light_back">
									<input type="url" name="instagram_link" id="instagram_link" class="form-control color_tertiary_light" placeholder="@lang('site.contact_form_instagram_link')">
								</div>
							</div>
							<div class="col-sm-12 col-md-6 lead_linkedin" @if(@$detail->lead_linkedin == 'N') style="display: none;" @endif>
								<div class="form-group lead_light_back">
									<input type="url" name="linkedin_link" id="linkedin_link" class="form-control color_tertiary_light" placeholder="@lang('site.contact_form_linkedink_link')">
								</div>
							</div>
							<div class="col-sm-12 lead_message" @if(@$detail->lead_message == 'N') style="display: none;" @endif>
								<div class="form-group lead_light_back">
									<textarea name="message" id="message" class="required form-control color_tertiary_light" placeholder="@lang('site.message')"></textarea>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
								<label for="lead_privacy_policy_accepted">
									<input type="checkbox" class="required" id="lead_privacy_policy_accepted" name="lead_privacy_policy_accepted">
									@lang('client_site.i_agree_with_the') <span id="show_lead_footer_privacy_policy" class="lead_privacy_policy_link color_secondary_font" data-toggle="modal" data-target="#myModal3">@lang('client_site.privacy_policy')</span>
								</label>
								<div id="lead_privacy_err_footer"></div>
								</div>
							</div>
							<div class="col-md-12 form-group">
								<button type="button" class="page_btn color_primary color_primary_font_hover color_secondary_hover" id="lead_form_footer_btn"> {{@$detail->lead_section_footer_submit_btn ?? 'Submit Now'}}</button>
								<div class="messages"></div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<footer class="foot_sec color_primary">
	<div class="container">
		<div class="foot_inr">
            <div class="foot-inr-mn">
                {{-- <div class="foot_logo">
                    @if(@$detail->landing_logo)
                        <img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->landing_logo}}" alt="" class="landing_logo" />
                    @else
                        <img src="{{ URL::asset('public/frontend/landing_page4/images/logo.png') }}" alt="" class="landing_logo">
                    @endif
                </div> --}}
                <p>© {{date('Y')}} &nbsp; <a href="{{route('home')}}" class="color_secondary_font" target="_blank"> netbhe.com.br </a>&nbsp; Todos os Direitos Reservados</p>
                <div class="header_sos">
                    <ul>
                        <li class="link_icons link_icons_linkedin" @if(!@$detail->link_linkedin) style="display:none;" @endif ><a href="{{ @$detail->link_linkedin ?? 'javascript:;' }}" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                        <li class="link_icons link_icons_facebook" @if(!@$detail->link_facebook) style="display:none;" @endif ><a href="{{ @$detail->link_facebook ?? 'javascript:;' }}" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                        <li class="link_icons link_icons_twitter" @if(!@$detail->link_twitter) style="display:none;" @endif ><a href="{{ @$detail->link_twitter ?? 'javascript:;' }}" target="_blank"><i class="fab fa-twitter"></i></a></li>
                        <li class="link_icons link_icons_instagram" @if(!@$detail->link_instagram) style="display:none;" @endif ><a href="{{ @$detail->link_instagram ?? 'javascript:;' }}" target="_blank"><i class="fab fa-instagram"></i></a></li>
                        <li class="link_icons link_icons_pinterest" @if(!@$detail->link_pinterest) style="display:none;" @endif ><a href="{{ @$detail->link_pinterest ?? 'javascript:;' }}" target="_blank"><i class="fab fa-pinterest"></i></a></li>
                        <li class="link_icons link_icons_youtube" @if(!@$detail->link_youtube) style="display:none;" @endif ><a href="{{ @$detail->link_youtube ?? 'javascript:;' }}" target="_blank"><i class="fab fa-youtube"></i></a></li>
                        <li class="link_icons link_icons_tiktok" @if(!@$detail->link_tiktok) style="display:none;" @endif ><a href="{{ @$detail->link_tiktok ?? 'javascript:;' }}" target="_blank"><i class="fa-brands fa-tiktok"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="foot-inr-sb">
                <img src="{{ URL::asset('public/frontend/landing_page4/images/logo.png') }}" alt="" class="landing_logo">
            </div>
		</div>
	</div>
</footer>


<div class="modal fade modal_sus modal_frm" id="myModal">
    <div class="modal-dialog" id="form-modal">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">@lang('client_site.contact_form')</h4>
          <button type="button" class="close color_primary_hover" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          	<div class="from_panel">
          		<form action="{{route('landing.page.save.lead')}}" id="lead_form_popup" method="post">
				@csrf
				<input type="text" name="landing_page_master_id" value="{{@$detail->id}}" hidden >
				<div class="row">
					<div class="col-sm-6 lead_fname" @if(@$detail->lead_fname == 'N') style="display: none;" @endif>
						<div class="formInrInput">
							<input type="text" name="fname" class="required" placeholder="@lang('client_site.first_name')">
						</div>
					</div>
					<div class="col-sm-6 lead_lname" @if(@$detail->lead_lname == 'N') style="display: none;" @endif>
						<div class="formInrInput">
							<input type="text" name="lname" class="required" placeholder="@lang('client_site.last_name')">
						</div>
					</div>
					<div class="col-sm-6 lead_country_code" @if(@$detail->lead_country_code == 'N') style="display: none;" @endif>
						<div class="formInrInput">
							<input type="number" name="country_code" class="required number" placeholder="@lang('site.country_code')">
						</div>
					</div>
					<div class="col-sm-6 lead_mobile" @if(@$detail->lead_mobile == 'N') style="display: none;" @endif>
						<div class="formInrInput">
							<input type="tel" name="mobile" class="required number" placeholder="@lang('site.contact_form_mobile')">
						</div>
					</div>
					<div class="col-sm-6 lead_email" @if(@$detail->lead_email == 'N') style="display: none;" @endif>
						<div class="formInrInput">
							<input type="email" name="email" class="required" placeholder="@lang('site.contact_form_email')">
						</div>
					</div>
					<div class="col-sm-6 lead_address" @if(@$detail->lead_address == 'N') style="display: none;" @endif>
						<div class="formInrInput">
							<input type="text" name="address" class="required" placeholder="@lang('site.street_address')">
						</div>
					</div>
					<div class="col-sm-6 lead_city" @if(@$detail->lead_city == 'N') style="display: none;" @endif>
						<div class="formInrInput">
							<input type="text" name="city" class="required" placeholder="@lang('site.city')">
						</div>
					</div>
					<div class="col-sm-6 lead_state" @if(@$detail->lead_state == 'N') style="display: none;" @endif>
						<div class="formInrInput">
							<input type="text" name="state" class="required" placeholder="@lang('site.state')">
						</div>
					</div>
					<div class="col-sm-6 lead_website" @if(@$detail->lead_website == 'N') style="display: none;" @endif>
						<div class="formInrInput">
							<input type="url" name="website" class="required" placeholder="@lang('site.contact_form_website')">
						</div>
					</div>
					<div class="col-sm-6 lead_facebook" @if(@$detail->lead_facebook == 'N') style="display: none;" @endif>
						<div class="formInrInput">
							<input type="url" name="facebook_link" placeholder="@lang('site.contact_form_facebook_link')">
						</div>
					</div>
					<div class="col-sm-6 lead_instagram" @if(@$detail->lead_instagram == 'N') style="display: none;" @endif>
						<div class="formInrInput">
							<input type="url" name="instagram_link" placeholder="@lang('site.contact_form_instagram_link')">
						</div>
					</div>
					<div class="col-sm-6 lead_linkedin" @if(@$detail->lead_linkedin == 'N') style="display: none;" @endif>
						<div class="formInrInput">
							<input type="url" name="linkedin_link" placeholder="@lang('site.contact_form_linkedink_link')">
						</div>
					</div>
					<div class="col-sm-12 lead_message" @if(@$detail->lead_message == 'N') style="display: none;" @endif>
						<div class="formInrInput">
							<textarea name="message" class="required" placeholder="@lang('site.message')"></textarea>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="formInrInput">
							<label for="lead_privacy_policy_accepted" class="lead_privacy_policy_accepted_label">
								<input type="checkbox" class="required" id="lead_privacy_policy_accepted" name="lead_privacy_policy_accepted">
								@lang('client_site.i_agree_with_the') <span id="show_lead_popup_privacy_policy" class="lead_privacy_policy_link color_primary_font">@lang('client_site.privacy_policy')</span>
							</label>
							<div id="lead_privacy_err_popup"></div>
						</div>
					</div>
				</div>
			</form>
          	</div>
        </div>

        <div class="modal-footer">
          <button type="button" id="lead_form_popup_btn" class="page_btn color_primary color_secondary_hover">{{@$detail->lead_section_footer_submit_btn ?? 'Submit Now'}} <i class="fa fa-paper-plane" aria-hidden="true"></i></button>
        </div>

      </div>
    </div>
	<div class="modal-dialog" id="success-modal" style="display:none;">
		<div class="modal-content">
			<button type="button" class="close color_primary color_tertiary_hover text-white" data-dismiss="modal">&times;</button>
			<div class="modal-body color_secondary">
				<div class="modal_sus_bdy">
					<h2 class="color_primary_font" id="lead_success_heading_popup">{{@$detail->lead_success_heading ?? 'Heading'}}</h2>
					<p class="color_primary_font" id="lead_success_desc_popup">{{@$detail->lead_success_desc ?? 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem dolor vero mollitia nam molestiae neque, maiores facilis voluptate eligendi ducimus id fugiat assumenda perspiciatis optio dignissimos eos nemo ipsum architecto unde ea tempore. Ratione eaque exercitationem magnam blanditiis, tenetur minima incidunt maxime sint earum est ut necessitatibus odit tempore vero. Recusandae dolores voluptatem officiis dolorum? Eum veniam minima ducimus odio non, dolore, nulla ullam a nemo cupiditate quae praesentium? Dolorem voluptatem consequuntur provident sapiente magnam consectetur ut omnis saepe libero tenetur id, excepturi nobis odit. Voluptate assumenda tenetur odit neque culpa voluptatibus consectetur, quo sed quis debitis numquam, inventore similique. Et reiciendis distinctio libero itaque ab, culpa voluptatem similique facere aliquid dolorum sint soluta minus deleniti? Commodi, eaque itaque. Porro?'}}</p>
					<div class="net_sus_btn">
						<button type="button" class="btn_pro color_primary color_secondary_font" id="lead_success_ok_popup" @if(@$detail->lead_success_button == 'N') style="display:none;" @endif >{{@$detail->lead_success_button_caption ?? 'OK'}}</button>
					</div>
				</div>
			</div>
		</div>
    </div>
	<div class="modal-dialog" id="privacy-modal" style="display: none;">
		<div class="modal-content">
			<button type="button" class="close privacy_back color_primary color_tertiary_hover">Back</button>
			<div class="modal-body color_secondary">
				<h2 class="color_primary_font">@lang('client_site.privacy_policy')</h2>
				<p id="lead_privacy_desc">{{@$detail->lead_privacy_policy ?? \Lang::get('client_site.privacy_policy')}}</p>
			</div>
		</div>
	</div>
</div>

<div class="modal fade modal_sus" id="myModal2">
	<div class="modal-dialog">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<div class="modal-body color_secondary">
				<div class="modal_sus_bdy">
					<h2 class="color_primary_font" id="lead_success_heading_footer">{{@$detail->lead_success_heading ?? 'Heading'}}</h2>
					<p class="color_primary_font" id="lead_success_desc_footer">{{@$detail->lead_success_desc ?? 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem dolor vero mollitia nam molestiae neque, maiores facilis voluptate eligendi ducimus id fugiat assumenda perspiciatis optio dignissimos eos nemo ipsum architecto unde ea tempore. Ratione eaque exercitationem magnam blanditiis, tenetur minima incidunt maxime sint earum est ut necessitatibus odit tempore vero. Recusandae dolores voluptatem officiis dolorum? Eum veniam minima ducimus odio non, dolore, nulla ullam a nemo cupiditate quae praesentium? Dolorem voluptatem consequuntur provident sapiente magnam consectetur ut omnis saepe libero tenetur id, excepturi nobis odit. Voluptate assumenda tenetur odit neque culpa voluptatibus consectetur, quo sed quis debitis numquam, inventore similique. Et reiciendis distinctio libero itaque ab, culpa voluptatem similique facere aliquid dolorum sint soluta minus deleniti? Commodi, eaque itaque. Porro?'}}</p>
					<div class="net_sus_btn">
						<button type="button" class="btn_pro color_primary color_secondary_font" id="lead_success_ok_footer" @if(@$detail->lead_success_button == 'N') style="display:none;" @endif >{{@$detail->lead_success_button_caption ?? 'OK'}}</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade modal_sus modal_privacy" id="myModal3">
	<div class="modal-dialog">
		<div class="modal-content">
			<button type="button" class="close color_primary_hover" data-dismiss="modal">&times;</button>
			<div class="modal-body color_secondary">
				<h2 class="color_primary_font">@lang('client_site.privacy_policy')</h2>
				<p id="lead_privacy_desc">{{@$detail->lead_privacy_policy ?? \Lang::get('client_site.privacy_policy')}}</p>
			</div>
		</div>
	</div>
</div>


<!-- Bootstrap core JavaScript -->
<script src="{{ URL::asset('public/frontend/landing_page4/js/jquerymin.js') }}"></script>
<script src="{{ URL::asset('public/frontend/landing_page4/js/bootstrap.js') }}"></script>
<script src="{{ URL::asset('public/frontend/landing_page4/js/custom.js') }}"></script>
<script src="{{ URL::asset('public/frontend/landing_page4/js/puchmenu.js') }}"></script>
<script src="{{ URL::asset('public/frontend/landing_page4/js/fancyjs.js') }}"></script>
<script src="{{ URL::asset('public/frontend/landing_page4/js/jquery.fancybox.js') }}"></script>
<script src="{{ URL::asset('public/frontend/landing_page4/js/jquery.fancybox.pack.js') }}"></script>
<script src="{{ URL::asset('public/frontend/landing_page4/js/jquery.mousewheel-3.0.6.pack.js') }}"></script>
<script src="{{ URL::asset('public/frontend/landing_page4/js/jquery.fancybox-media.js') }}"></script>
<script src="{{ URL::asset('public/frontend/landing_page4/js/owl.carousel.js') }}"></script>
<script src="{{URL::to('public/admin/js/jquery.validate.js') }}"></script>

<script>
	$('a.banBotArw[href^="#"]').on('click', function(event) {

		var target = $( $(this).attr('href') );

		if( target.length ) {
			event.preventDefault();
			$('html, body').animate({
				scrollTop: target.offset().top
			}, 3000);
		}

	});
</script>
<script>
    var timer = 0;
	var landing_prod_timer = [];
	var detail = null;
    let hexToRgb= c=> `rgb(${c.match(/\w\w/g).map(x=>+`0x${x}`)})`;

    $(document).ready(function(){
        detail = {!! json_encode(@$detail) !!}
        console.log(detail);

        // $('#contact_form').validate();

        setTimeout(function() {
            $('#landing_preloader').fadeOut('slow', function() {
                $(this).remove();
            });
        }, 2000);
        if(detail !== null){
            changeColours(detail.color_primary, detail.color_secondary, detail.color_tertiary);
            if(detail.header_reverse_counter == 'Y' || detail.header_reverse_counter == 'N'){
				if(new Date() > new Date(detail.header_counter_date)){
					$('.header_reverse_text').text("Promoção terminou");
					$('.header_counter').empty();
				} else {
					timer = setInterval(function() {
						var html = timeDiffCalc( new Date(), new Date(detail.header_counter_date) );
						if(html == 0){
							clearInterval(timer);
							$('.header_reverse_text').text("Promoção terminou");
                    		$('.header_counter').empty();
						}
						$('.header_counter').html(html);
						if ( window.location !== window.parent.location ){
							console.log('Calling top function');
							window.top.callChangeTimerColors();
						} else{
							console.log('Calling function here');
							changeTimerColours(detail.color_primary, detail.color_secondary, detail.color_tertiary);
						}
					}, 1000);
					// changeColours(detail.color_primary, detail.color_secondary, detail.color_tertiary);
				}
            }
			if(detail.get_product_details.length > 0){
				$.each(detail.get_product_details, function(i, tim){
					var id = parseInt(i)+1;
					console.log(i);
					landing_prod_timer[id] = setInterval(function() {
						var html = prodTimeDiffCalc( new Date(), new Date(detail.get_product_details[i].product_exp_date) );
						if(html == 0){
							$('.product_expiry_'+id).html("Promoção terminou");
							clearInterval(landing_prod_timer[id]);
						} else {
							$('.product_expiry_'+id).html(html);
						}
					}, 1000);
				});
			}
        } else {
			console.log("Got in");
			$('.lead_light_back').each(function(){
				$(this).css('background-color', 'rgba(38, 38, 38, 0.9)');
			});
		}
    });
    function timeDiffCalc(dateFuture, dateNow) {
        if(Math.abs(dateFuture - dateNow) == 0){
            return 0;
        } else {
            let diffInMilliSeconds = Math.abs(dateFuture - dateNow) / 1000;
            const days = Math.floor(diffInMilliSeconds / 86400);
            diffInMilliSeconds -= days * 86400;
            const hours = Math.floor(diffInMilliSeconds / 3600) % 24;
            diffInMilliSeconds -= hours * 3600;
            const minutes = Math.floor(diffInMilliSeconds / 60) % 60;
            diffInMilliSeconds -= minutes * 60;
            const seconds = Math.round(diffInMilliSeconds);

            let difference = '<ul class="counter_ul">';
            if (days > 0) { difference += (days === 1) ? `<li class="header-timer-hours"><p class="counter_num color_primary">${days}</p><p class="">dias</p></li>` : `<li class="header-timer-hours"><p class="counter_num color_primary">${days}</p><p class="">dias</p></li>`; }

            difference += (hours < 10 ) ? `<li class="header-timer-hours"><p class="counter_num color_primary">0${hours}</p><p class="">horas</p></li>` : `<li class="header-timer-hours"><p class="counter_num color_primary">${hours}</p><p class="">horas</p></li>`;
            difference += (minutes < 10 ) ? `<li class="header-timer-hours"><p class="counter_num color_primary">0${minutes}</p><p class="">minutos</p></li>` : `<li class="header-timer-hours"><p class="counter_num color_primary">${minutes}</p><p class="">minutos</p></li>`;
            difference += (seconds < 10 ) ? `<li class="header-timer-hours"><p class="counter_num color_primary">0${seconds}</p><p class="">segundos</p></li>` : `<li class="header-timer-hours"><p class="counter_num color_primary">${seconds}</p><p class="color_secondary_font">segundos</p></li>`;
            difference += "</ul>";
            return difference;
        }
    }
    function prodTimeDiffCalc(dateFuture, dateNow) {
        if(Math.abs(dateFuture - dateNow) == 0){
            return 0;
        } else {
            let diffInMilliSeconds = Math.abs(dateFuture - dateNow) / 1000;
            const days = Math.floor(diffInMilliSeconds / 86400);
            diffInMilliSeconds -= days * 86400;
            const hours = Math.floor(diffInMilliSeconds / 3600) % 24;
            diffInMilliSeconds -= hours * 3600;
            const minutes = Math.floor(diffInMilliSeconds / 60) % 60;
            diffInMilliSeconds -= minutes * 60;
            const seconds = Math.round(diffInMilliSeconds);

            let difference = '';
            if (days > 0) { difference += (days === 1) ? `${days}:` : `${days}:`; }

            difference += (hours < 10 ) ? `0${hours}:` : `${hours}:`;
            difference += (minutes < 10 ) ? `0${minutes}:` : `${minutes}:`;
            difference += (seconds < 10 ) ? `0${seconds}` : `${seconds}`;
            return difference;
        }
    }
    function changeColours(primary, secondary, tertiary){
		if ( window.location !== window.parent.location ){
			window.top.callChangeColours();
		} else{
			console.log("Changing colours");
			var val = primary;
			var rgb = hexToRgb(val);
			var rgba = [rgb.slice(0, rgb.length-1), ", 0.4", rgb.slice(rgb.length-1)].join('');
			rgba = rgba.replace('rgb','rgba');
			var backclr = clr = "";

			$('.color_primary').css('background', val);
			$('.color_primary_font').css('color', val);
			$('.color_primary_border').css('border-color', val);
			$('.color_primary_hover').hover(function(){
				backclr = $(this).css('background-color');
				$(this).css('background', val);
				$(this).css('color', '#fff');
			}, function(){
				console.log("BACKCLR: "+backclr)
				$(this).css('background', backclr);
				$(this).css('color', val);
				backclr = "";
			});
			$('.color_primary_font_hover').hover(function(){
				clr = $(this).css('color');
				console.log(clr);
				$(this).css('color', val);
			},function(){
				$(this).css('color', clr);
				clr = "";
			});
			$('.color_primary_owl').find('.owl-prev').css('color', val).hover(function(){
				$(this).css('background', val);
				$(this).css('color', '#fff');
			}, function(){
				$(this).css('background', 'transparent');
				$(this).css('color', val);
			});
			$('.color_primary_owl').find('.owl-next').css('color', val).hover(function(){
				$(this).css('background', val);
				$(this).css('color', '#fff');
			}, function(){
				$(this).css('background', 'transparent');
				$(this).css('color', val);
			});
			$('.color_primary_light').css('background', rgba);

			val = secondary;
			rgb = hexToRgb(val);
			rgba = [rgb.slice(0, rgb.length-1), ", 0.3", rgb.slice(rgb.length-1)].join('');
			rgba = rgba.replace('rgb','rgba');
			backclr = clr = "";

			$('.color_secondary').css('background', secondary);
			$('.color_secondary_font').css('color', secondary);
			$('.color_secondary_border').css('border-color', secondary);
			$('.color_secondary_light').css('background', rgba);
			$('.color_secondary_hover').hover(function(){
				backclr = $(this).css('background-color');
				$(this).css('background', secondary);
			}, function(){
				$(this).css('background', backclr!="" ? backclr : 'transparent');
				backclr = "";
			});
			$('.color_secondary_font_hover').hover(function(){
				clr = $(this).css('color');
				$(this).css('color', secondary);
			},function(){
				$(this).css('color', clr);
				clr = "";
			});
			$('.color_secondary_owl').find('.owl-dot').css('background', secondary);

			val = tertiary;
			console.log("Tertiary : " + val);
			rgb = hexToRgb(val);
			rgba = [rgb.slice(0, rgb.length-1), ", 0.9", rgb.slice(rgb.length-1)].join('');
			rgba = rgba.replace('rgb','rgba');
			clr = "";

			$('.color_tertiary').css('background', tertiary);
			$('.color_tertiary_font').css('color', tertiary);
			$('.color_tertiary_light').css('background', rgba);
            $('.color_tertiary_shadow').css('box-shadow', "15px 15px 0px 0px "+tertiary);
			$('.color_tertiary_hover').hover(function(){
				backclr = $(this).css('background-color');
				console.log(backclr);
				$(this).css('background', tertiary);
			}, function(){
				$(this).css('background', backclr!="" ? backclr : 'transparent');
				backclr = "";
			});
		}
    }
    function changeTimerColours(primary, secondary, tertiary){
		$('.counter_num').css('background', primary);
        $('.header-timer-hours').find('.color_primary_font').css('color', primary);
	}
	function clearTheTimer(){
		clearInterval(timer);
	}
    function clearProductTimer(num){
		clearInterval(landing_prod_timer[num]);
	}
	$(document).delegate('.accordionlanding1 .card-header', 'click', function(){
		console.log("Clicked");
		if($(this).hasClass('collapsed')){
			console.log("opened");
            $(this).find('i').removeClass('fa-angle-double-up');
			$(this).find('i').addClass('fa-angle-double-down');
			$(this).addClass('color_primary');
			$(this).removeClass('color_secondary');
			$(this).addClass('color_secondary_font');
			$(this).removeClass('color_primary_font');
			$(this).find('.card-title').removeClass('color_primary_font');
			$(this).find('.card-title').addClass('color_secondary_font');
		} else {
            $('.accordionlanding1 .card-header').each(function(i,elem){
                $(elem).find('i').removeClass('fa-angle-double-up');
			    $(elem).find('i').addClass('fa-angle-double-down');
				$(this).addClass('color_primary');
				$(this).removeClass('color_secondary');
				$(this).addClass('color_secondary_font');
				$(this).removeClass('color_primary_font');
				$(this).find('.card-title').removeClass('color_primary_font');
				$(this).find('.card-title').addClass('color_secondary_font');
            });
            console.log("collapsed");
            $(this).find('i').removeClass('fa-angle-double-down');
			$(this).find('i').addClass('fa-angle-double-up');
			$(this).removeClass('color_primary');
			$(this).addClass('color_secondary');
			$(this).addClass('color_primary_font');
			$(this).removeClass('color_secondary_font');
			$(this).find('.card-title').removeClass('color_secondary_font');
			$(this).find('.card-title').addClass('color_primary_font');
		}
		if ( window.location !== window.parent.location ){
			window.top.callChangeColours();
		} else{
			changeColours(detail.color_primary, detail.color_secondary, detail.color_tertiary);
		}
	});
    $('.paly_btn').click(function (){
		$('.single_vdo video').attr('controls',true);
		$('.single_vdo video')[0].play();
		$('.paly_btn').hide();
	});
    var validator_popup = $("#lead_form_footer").validate({
		rules: {
			mobile: {
				minlength:8,
				maxlength:13,
			}
		},
        messages: {
            lead_privacy_policy_accepted: {
                required : "{{\Lang::get('client_site.lead_privacy_error')}}."
            },
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "lead_privacy_policy_accepted") {
                $("#lead_privacy_err_footer").append(error);
            } else {
				$(error).addClass('color_tertiary w-100');
                error.insertAfter(element);
            }
			changeColours(detail.color_primary, detail.color_secondary, detail.color_tertiary);
        }
	});
	var validator_footer = $("#lead_form_popup").validate({
		rules: {
			mobile: {
				minlength:8,
				maxlength:13,
			}
		},
        messages: {
            lead_privacy_policy_accepted: {
                required : "{{\Lang::get('client_site.lead_privacy_error')}}."
            },
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "lead_privacy_policy_accepted") {
                $("#lead_privacy_err_popup").append(error);
            } else {
                error.insertAfter(element);
            }
        }
	});
	$('#lead_form_footer_btn').click(function(){
		if ( window.location !== window.parent.location ){
			console.log("In iframe");
			$('#myModal2').modal();
            $('#myModal2').modal("show");
		} else{
			if($('#lead_form_footer').valid()){
				var formData = $("#lead_form_footer").serialize();
				$.ajax({
					url: "{{ route('landing.page.save.lead') }}",
					method: 'post',
					dataType: 'json',
					data: formData,
					async: false,
					success: function(res){
						console.log(res);
						if(res.status == 'success'){
							$('#myModal2').modal();
                            $('#myModal2').modal("show");
							$('#success-modal').fadeIn("slow");
						} else {
							toastr.error(res.error);
						}
					},
					error: function(err){
						console.log(err);
					},
				});
			}
		}
	});
    $('#lead_form_popup_btn').click(function(){
		if ( window.location !== window.parent.location ){
			console.log("In iframe");
			$('#form-modal').fadeOut("slow");
			$('#success-modal').fadeIn("slow");
		} else {
			if($('#lead_form_popup').valid()){
				var formData = $("#lead_form_popup").serialize();
				$.ajax({
					url: "{{ route('landing.page.save.lead') }}",
					method: 'post',
					dataType: 'json',
					data: formData,
					async: false,
					success: function(res){
						console.log(res);
						if(res.status == 'success'){
							$('#lead_form_popup')[0].reset();
							$('#form-modal').fadeOut("slow");
							$('#success-modal').fadeIn("slow");
						} else {
							toastr.error(res.error);
						}
					},
					error: function(err){
						console.log(err);
					},
				});
			}
		}
	});
	$('#lead_success_ok_footer').click(function(){
		// clearTimeout(leadSuccessTimout);
		$("#myModal2").modal();
		$("#myModal2").modal("hide");
		validator_footer.resetForm();
		$('#lead_form_footer')[0].reset();
        window.location.href = "{{@$detail->lead_download_link}}";
	});
    $('#lead_success_ok_popup').click(function(){
		// clearTimeout(leadSuccessTimout);
		$('#form-modal').fadeIn("slow");
		$('#success-modal').fadeOut("slow");
		$('#lead_form_popup')[0].reset();
		$("#myModal").modal();
		$("#myModal").modal("hide");
		validator_popup.resetForm();
        window.location.href = "{{@$detail->lead_download_link}}";
	});
	$('#show_lead_popup_privacy_policy').click(function(){
		$('#form-modal').hide();
		$('#privacy-modal').show();
    });
	$('.privacy_back').click(function(){
		$('#privacy-modal').hide();
		$('#form-modal').show();
	});
</script>
<script type="text/javascript">
    var itemsMainDiv = ('.MultiCarousel');
    var itemsDiv = ('.MultiCarousel-inner');
    var itemWidth = "";
    $(document).ready(function () {

        var count = $('.proddiv').length;
        if(count < 4){
            $('.leftLst').hide();
            $('.rightLst').hide();
        } else {
            $('.leftLst').show();
            $('.rightLst').show();
        }

        $('.leftLst, .rightLst').click(function () {
            var condition = $(this).hasClass("leftLst");
            if (condition)
                click(0, this);
            else
                click(1, this)
        });

        ResCarouselSize();

        $(window).resize(function () {
            ResCarouselSize();
        });

    });
    //this function define the size of the items
    function ResCarouselSize() {
        var incno = 0;
        var dataItems = ("data-items");
        var itemClass = ('.item');
        var id = 0;
        var btnParentSb = '';
        var itemsSplit = '';
        var sampwidth = $(itemsMainDiv).width();
        var bodyWidth = $('body').width();
        $(itemsDiv).each(function () {
            id = id + 1;
            var itemNumbers = $(this).find(itemClass).length;
            btnParentSb = $(this).parent().attr(dataItems);
            itemsSplit = btnParentSb.split(',');
            $(this).parent().attr("id", "MultiCarousel" + id);


            if (bodyWidth >= 1200) {
                incno = itemsSplit[2];
                itemWidth = sampwidth / incno;
            }
            else if (bodyWidth >= 992) {
                incno = itemsSplit[2];
                itemWidth = sampwidth / incno;
            }
            else if (bodyWidth >= 768) {
                incno = itemsSplit[1];
                itemWidth = sampwidth / incno;
                console.log(itemWidth);
            }
            else {
                incno = itemsSplit[0];
                itemWidth = sampwidth / incno;
            }
            $(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
            $(this).find(itemClass).each(function () {
                $(this).outerWidth(itemWidth);
            });

            $(".leftLst").addClass("over");
            $(".rightLst").removeClass("over");

        });
    }
    //this function used to move the items
    function ResCarousel(e, el, s) {
        var leftBtn = ('.leftLst');
        var rightBtn = ('.rightLst');
        var translateXval = '';
        var divStyle = $(el + ' ' + itemsDiv).css('transform');
        var values = divStyle.match(/-?[\d\.]+/g);
        var xds = Math.abs(values[4]);
        if (e == 0) {
            translateXval = parseInt(xds) - parseInt(itemWidth * s);
            $(el + ' ' + rightBtn).removeClass("over");

            if (translateXval <= itemWidth / 2) {
                translateXval = 0;
                $(el + ' ' + leftBtn).addClass("over");
            }
        }
        else if (e == 1) {
            var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
            translateXval = parseInt(xds) + parseInt(itemWidth * s);
            $(el + ' ' + leftBtn).removeClass("over");

            if (translateXval >= itemsCondition - itemWidth / 2) {
                translateXval = itemsCondition;
                $(el + ' ' + rightBtn).addClass("over");
            }
        }
        $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
    }
    //It is used to get some elements from btn
    function click(ell, ee) {
        var Parent = "#" + $(ee).parent().attr("id");
        var slide = $(Parent).attr("data-slide");
        ResCarousel(ell, Parent, slide);
    }
</script>

</body>
</html>
