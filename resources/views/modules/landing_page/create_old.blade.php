@extends('layouts.app')
@section('title')
    
    Landing Page
@endsection
@section('style')
@include('includes.style')

@endsection
@section('scripts')
@include('includes.scripts')
<script src="{{URL::to('public/js/chosen.jquery.min.js')}}"></script>
@endsection
@section('header')
@include('includes.professional_header')
<link rel="stylesheet" href="{{URL::to('public/css/chosen.css')}}">
<style>
    .tab_no_hover:hover{
        background: #1781D2 !important;
    }
</style>
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.landing_page')</h2>
        
        <div class="like-tab">
        	<ul>
            	<li><a class="active tab tab_no_hover tab_choose_temp" href="javascript:;" data-target="choose_temp">@lang('site.choose_template')</a></li>
                <li><a class="tab tab_header" href="javascript:;" data-target="header">@lang('site.header')</a></li>
                <li><a class="tab tab_design" href="javascript:;" data-target="design">@lang('site.design')</a></li>
            </ul>
        </div>

        <div class="bokcntnt-bdy no-margin-top">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>

            </div>
            {{-- @include('includes.professional_sidebar') --}}

            <form action="{{route('store.landing.page.template')}}" method="post" id="myForm">
                @csrf
                <input type="text" name="template_id" id="template_id" value="0">
                <input type="text" name="template_number" id="template_number" value="1">
                <div class="dshbrd-lftmnu">
                    <div class="dashmnu">
                        
                        <div class="tab_target" id="choose_temp">
                            @foreach(@$templates as $template)
                                <div class="card landing_temp_card {{ @$template->id == 1 ? 'active_cert' : '' }}" data-id="{{ @$template->id }}">
                                    <img class="card-img-top" src="{{url('/')}}/public/frontend/images/cert_temp_{{ @$template->id }}.png" alt="Card image cap">
                                    <div class="card-body">
                                        <h5 class="card-title">{{ @$template->template_name }}</h5>
                                    </div>
                                </div>
                                <br>
                            @endforeach
                        </div>

                        <div class="tab_target" id="header" style="display: none;">
                            <div class="px-2">
                                <div class="cert_cntnt">
                                    <label for="header_image">@lang('site.upload_picture')</label>
                                    <input type="file" name="header_image" id="header_image" class="form-control">
                                </div>
                                <div class="cert_cntnt">
                                    <label for="header_line_1">Header Line 1</label>
                                    <input type="text" name="header_line_1" id="header_line_1" class="form-control required mb-3" value="Lorem ipsum dolor sit ">
                                </div>
                                <div class="cert_cntnt">
                                    <label for="header_line_2">Header Line 2</label>
                                    <input type="text" name="header_line_2" id="header_line_2" class="form-control required mb-3" value="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.">
                                </div>
                                <div class="cert_cntnt">
                                    <label for="header_button">Header Button</label> <br>
                                    <label for="header_btn_yes">
                                        <input type="radio" name="header_btn" value="Y" id="header_btn_yes" checked>
                                        @lang('site.yes')
                                    </label>
                                    <label for="header_btn_no">
                                        <input type="radio" name="header_btn" value="N" id="header_btn_no">
                                        @lang('site.no')
                                    </label>
                                </div>
                                <div id="header_btn_action">

                                    <div class="cert_cntnt">
                                        <label for="header_button_text">Header Button Text</label>
                                        <input type="text" name="header_button_text" id="header_button_text" class="form-control required mb-3" value="">
                                    </div>
                                    <div class="cert_cntnt">
                                        <label for="header_button_type">Header Button Type</label> <br>
                                        <label for="header_btn_url">
                                            <input type="radio" name="header_button_type" value="URL" id="header_btn_url" checked>
                                            URL
                                        </label>
                                        <label for="header_btn_lead">
                                            <input type="radio" name="header_button_type" value="LEAD" id="header_btn_lead">
                                            Lead
                                        </label>
                                    </div>
                                    <div class="cert_cntnt" id="header_button_link_div">
                                        <label for="header_button_link">Header Button Link</label>
                                        <input type="text" name="header_button_link" id="header_button_link" class="form-control required mb-3" value="">
                                    </div>
                                </div>
                                <div class="cert_cntnt">
                                    <label for="above_course">Header reverse counter</label>  <br>
                                    <label for="header_reverse_yes">
                                        <input type="radio" name="header_reverse" value="Y" id="header_reverse_yes">
                                        @lang('site.yes')
                                    </label>
                                    <label for="header_reverse_no">
                                        <input type="radio" name="header_reverse" value="N" id="header_reverse_no" checked>
                                        @lang('site.no')
                                    </label>
                                </div>
                                <div class="cert_cntnt" id="header_reverse_action" style="display: none;">
                                    <label for="header_counter_date">Reverse counter date</label>
                                    <input type="text" name="header_counter_date" id="header_counter_date" class="form-control required mb-3" value="">
                                </div>
                            </div>
                        </div>

                        <div class="tab_target" id="design" style="display: none;">
                            <div class="px-2">
                                <div class="design_sec">
                                    <label for="primary_color" class="w-100">Primary Color</label>
                                    <input type="color" name="primary_color" id="primary_color" class="required w-100 mb-3">
                                </div>
                                <div class="design_sec">
                                    <label for="secondary_color" class="w-100">Secondary Color</label>
                                    <input type="color" name="secondary_color" id="secondary_color" class="required w-100 mb-3">
                                </div>
                                <div class="design_sec">
                                    <label for="font_style">Font Style</label>
                                    <select name="font_style" id="font_style" class="form-control mb-3 required">
                                        <option value="montserrat" class="font_montserrat">Montserrat</option>
                                        <option value="open_sans" class="font_open_sans">Open Sans</option>
                                        <option value="lato" class="font_lato">Lato</option>
                                    </select>
                                </div>
                                <div class="design_sec">
                                    <label for="background" class="w-100">Background <a href="javascript:;" id="reset_background_img" title="Reset background image" class="pull-right">Reset</a></label>
                                    <input type="file" name="background" id="background" class="form-control mb-3">
                                    <small>@lang('client_site.recommended_size') 700 * 700 px</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="dshbrd-rghtcntn">
                    <div class="" style="float: right;">
                        <a href="javascript:;" id="template_confirm" class="btn btn-primary">@lang('site.select_this_template')</a>
                        <a href="javascript:;" id="content_save" class="btn btn-primary" style="display:none;">@lang('site.save_content')</a>
                        <a href="javascript:;" id="save_certificate" class="btn btn-primary" style="display:none;">@lang('site.save_certificate')</a>
                        <a class="btn btn-success" href="{{route('index.certificate.template')}}">< @lang('site.back')</a>
                    </div>
                    <br><br>
                    <div class="clearfix"></div>
                    
                    <div id="template-element">
                        <iframe src="{{URL::to('/landing-page-template/1')}}" style="width:100%; height:100vh;" id="main_iframe" frameborder="0"></iframe>
                    </div>

                    <div class="clearfix"></div>
                    <br><br>
                    <div class="clearfix"></div>

                    <!-- <button type="submit" class="login_submitt">@lang('site.save_certificate')</button> -->
                </div>
            </form>

        </div>
    </div>

</section>




@endsection
@section('footer')
@include('includes.footer')

<script>
const date = new Date();
var template_id = 0;
var template_confirmed = false;
var content_confirmed = false;
// var image_size_limit = 20971520;

$(document).ready(function(){

    // jQuery.validator.addMethod("sizeLimit", function(value, element) {
    //     return this.optional(element) || $('#background')[0].files[0]['size'] < image_size_limit;
    // }, "@lang('client_site.cert_image_too_large')");

    $("#myForm").validate();

    $('#primary_color').val('#353A3E');
    $('#secondary_color').val('#6C6C6C');

    $("#myForm").validate();
    
    $('.show_assign').click(function(){
        var title = $(this).data('title');
        var id = $(this).data('id');
        $('.tool-title').html(title);
        $('.form_id').val(id);
        $('#assignModal').modal('show');
    });

    $('.tab_header').click(function(){
        if(template_confirmed == false){
            toastr.error(`@lang('client_site.select_temp_first')`);
        } else {
            $('.active').removeClass('active');
            $(this).addClass('active');
            $('.tab_target').hide();
            $('#'+$(this).data('target')).show();
            $('#template_confirm').hide();
            $('#save_certificate').hide();
            $('#content_save').show();
        }
    });

    $('.tab_design').click(function(){
        if(content_confirmed == false){
            toastr.error(`@lang('client_site.confirm_cont_first')`);
        } else {
            $('.active').removeClass('active');
            $(this).addClass('active');
            $('.tab_target').hide();
            $('#'+$(this).data('target')).show();
            $('#template_confirm').hide();
            $('#content_save').hide();
            $('#save_certificate').show();
        }
    });

    $('#template_confirm').click(function(){
        storeTemplate();
        $('.active').removeClass('active');
        $('.tab_header').addClass('active');
        $('.tab_target').hide();
        $('#header').show();
        $(this).hide();
        $('#content_save').show();
        if($('#template_number').val() == 1) $('#below_stu_name').removeClass('required');
        template_confirmed = true;
    });

    $('#content_save').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{csrf_token()}}"
            }
        });
        if($('#myForm').valid()){
            console.log("TEMP ID: "+template_id);
            if(template_id == 0) {
                storeCert();
                content_confirmed = true;
                $('.active').removeClass('active');
                $('.tab_design').addClass('active');
                $('.tab_target').hide();
                $('#design').show();
                $('#content_save').hide();
                $('#save_certificate').show();
            } else {
                updateCert();
            }
        }
    });

    $('.landing_temp_card').click(function() {
        var id = $(this).data('id');
        console.log(id);
        var url = "{{URL::to('/landing-page-template/')}}/"+id;
        $('#template_number').val(id);
        $('#main_iframe').attr('src', url);
        $('.active_cert').removeClass('active_cert');
        $(this).addClass('active_cert');

        if(id == 1){
            $('#header_line_1').val("Lorem ipsum dolor sit ");
            $("#header_line_2").val("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
        } else if(id == 2){
            $("#header_line_1").val("Lorem ipsum dolor sit Lorem ipsum");
            $("#header_line_2").val("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Sed pretium ligula.");
        }
    });

    $('#header_image').change(function(){
        var file = this.files[0];
        var reader = new FileReader();
        reader.onloadend = function (e) {
            console.log(reader.result);
            $("#main_iframe").contents().find('.header_image').attr('src', e.target.result);
        }
        if (file) {
            reader.readAsDataURL(file);
        } else {
            $("#main_iframe").contents().find('.header_image').attr('src', "{{URL::asset('public/frontend/landing_page1/images/ban-img2.png')}}");
        }
    });
    $('#header_line_1').on('keyup change', function(){
        var str = $(this).val();
        var html = "";
        console.log(str.indexOf(" "));
        if(str.indexOf(" ") === -1){
            html = "<span>" + str + "</span>";
        } else {
            var words = str.split(" ");
            html = "<span>" + words[0] + "</span> " + str.substring( str.indexOf(" ") + 1, str.length );
        }
        $("#main_iframe").contents().find('.header_line_1').html(html);
        console.log($("#main_iframe").contents().find('.header_line_1'));
    });
    $('#header_line_2').on('keyup change', function(){
        $("#main_iframe").contents().find('.header_line_2').text($(this).val());
    });
    /*
    header_line_1
    header_line_2
    header_button Y/N
    header_button_text
    header_button_type - URL/Lead Form
    header_reverse_counter Y/N
    header_counter_date
    */

    $('#save_certificate').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{csrf_token()}}"
            }
        });
        if($('#myForm').valid()){
            updateCert();
        }
    });
    
    $('input[type=radio][name=header_reverse]').change(function() {
        if (this.value == 'Y') {
            $('#header_reverse_action').show();
        } else if (this.value == 'N') {
            $('#header_reverse_action').hide();
        }
    });
    
    $('input[type=radio][name=header_btn]').change(function() {
        if (this.value == 'Y') {
            $('#header_btn_action').show();
        } else if (this.value == 'N') {
            $('#header_btn_action').hide();
        }
    });
    
    $('input[type=radio][name=header_button_type]').change(function(){
        if (this.value == 'URL'){
            $('#header_button_link_div').show();
        } else if (this.value == 'LEAD'){
            $('#header_button_link_div').hide();
        }
    });
    
    $('#primary_color').change(function(){
        var val = $('#primary_color').val();
        $('.primary_color').css('color', val);

        var rgb = hexToRgb(val);
        var rgba = [rgb.slice(0, rgb.length-1), ", 0.6", rgb.slice(rgb.length-1)].join('');
        var rgba1 = [rgb.slice(0, rgb.length-1), ", 0.4", rgb.slice(rgb.length-1)].join('');
        rgba = rgba.replace('rgb','rgba');
        rgba1 = rgba1.replace('rgb','rgba');

        $('.templete3-cer.primary_color').css('border', "3px "+ rgba1 + " solid");
        $('.new-temp.primary_color').css('border', "2px "+ rgba + " solid");
        $('.second-templte.primary_color').css('border', "3px "+ rgb + " solid");
    });

    $('#secondary_color').change(function(){
        var val = $('#secondary_color').val();
        $('.secondary_color').css('color', val);

        var rgb = hexToRgb(val);
        var rgba = [rgb.slice(0, rgb.length-1), ", 0.6", rgb.slice(rgb.length-1)].join('');
        var rgba1 = [rgb.slice(0, rgb.length-1), ", 0.4", rgb.slice(rgb.length-1)].join('');
        rgba = rgba.replace('rgb','rgba');
        rgba1 = rgba1.replace('rgb','rgba');

        $('.templete3-cer.secondary_color').css('border', "3px "+ rgba1 + " solid");
        $('.new-temp.secondary_color').css('border', "2px "+ rgba + " solid");
        $('.second-templte.secondary_color').css('border', "3px "+ rgb + " solid");
    });
});

$(".chosen-select").chosen();

function show_hide_divs(temp_id){
    if(temp_id == 1){
        $('.cert_cntnt').show();
        $('#above_stu_name_div').hide();
        $('#below_course_div').hide();
    } else if(temp_id == 2){
        $('.cert_cntnt').show();
        $('#below_stu_name_div').hide();
        $('#below_course_div').hide();
    } else if(temp_id == 3){
        $('.cert_cntnt').show();
        $('#above_course_div').hide();
        $('#below_stu_name_div').hide();
    } else if(temp_id == 4){
        $('.cert_cntnt').show();
        $('#below_stu_name_div').hide();
        $('#below_course_div').hide();            
    }
}
function fill_data(temp_id){
    if(temp_id == 1){
        $('.cert_head').text("@lang('client_site.this_is_to_cerify_that')");
        $('#cert_head').val("@lang('client_site.this_is_to_cerify_that')");
        $('.above_course').text("@lang('client_site.above_course_name')");
        $('#above_course').val("@lang('client_site.above_course_name')");
    } else if(temp_id == 2){
        $('.cert_head').text("@lang('client_site.certificate_heading')");
        $('#cert_head').val("@lang('client_site.certificate_heading')");
        $('.above_stu_name').text("@lang('client_site.this_is_to_cerify_that')");
        $('#above_stu_name').val("@lang('client_site.this_is_to_cerify_that')");
        $('.above_course').text("@lang('client_site.above_course_name')");
        $('#above_course').val("@lang('client_site.above_course_name')");
    } else if(temp_id == 3){
        $('.cert_head').text("@lang('client_site.certificate_heading')");
        $('#cert_head').val("@lang('client_site.certificate_heading')");
        $('.above_stu_name').text("@lang('client_site.above_student_name')");
        $('#above_stu_name').val("@lang('client_site.above_student_name')");
        $('.below_course').text("@lang('client_site.below_course_name')");
        $('#below_course').val("@lang('client_site.below_course_name')");
    } else if(temp_id == 4){
        $('.cert_head').text("@lang('client_site.certificate_heading')");
        $('#cert_head').val("@lang('client_site.certificate_heading')");
        $('.above_stu_name').text("@lang('client_site.above_student_name')");
        $('#above_stu_name').val("@lang('client_site.above_student_name')");
        $('.above_course').text("@lang('client_site.above_course_name')");
        $('#above_course').val("@lang('client_site.above_course_name')");
    }
}
function storeTemplate(){
    var reqData = {
        'jsonrpc' : '2.0',
        '_token' : '{{csrf_token()}}',
        'params' : {
            'template_number': $('#template_number').val(),
        }
    };
    $.ajax({
        url: "{{ route('store.landing.page.template') }}",
        method: 'post',
        dataType: 'json',
        data: reqData,
        async: false,
        success: function(res){
            console.log(res);
            template_id = res.template.id;
            console.log("TEMP ID: "+template_id);
            $('#template_id').val(template_id);
        },
        error: function(err){
            console.log(err);
        },
    });
}
function updateTemplate(){
    var style = "";
    var uploadFormData = new FormData();
    uploadFormData.append("jsonrpc", "2.0");
    uploadFormData.append("_token", '{{csrf_token()}}');
    uploadFormData.append('template_number', $('#template_number').val());
    uploadFormData.append('header_image', $('#header_image').val() != "" ? $('#header_image')[0].files[0] : null);
    uploadFormData.append('header_line_1', $('#header_line_1').val());
    uploadFormData.append('header_line_2', $('#header_line_2').val());
    uploadFormData.append('header_btn', $('#header_btn').val());
    uploadFormData.append('header_button_text', $('#header_button_text').val());
    uploadFormData.append('header_button_type', $('#header_button_type').val());
    uploadFormData.append('header_button_link', $('#header_button_link').val());
    uploadFormData.append('header_reverse', $('#header_reverse').val());
    uploadFormData.append('header_counter_date', $('#header_counter_date').val());

    // uploadFormData.append('primary_color', $('#primary_color').val());
    // uploadFormData.append('secondary_color', $('#secondary_color').val());
    // uploadFormData.append('font_style', $('#font_style').val());

    $.ajax({
        url: "{{url('/')}}/professional-certificate-template-update/"+template_id,
        method: 'post',
        data: uploadFormData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function(res){
            console.log(res);
            if(res.status == "success") {
                toastr.success(res.message);
                template_id = res.template.id;
                console.log("TEMP ID: "+template_id);
                $('#background').val('');
                $('.template'+ $('#template_number').val() ).attr('style', style);
            } else toastr.error(res.message);
        },
        error: function(err){
            console.log(err);
        },
    });
}
function sweetAlert(){
  (async () => {

  const { value: formValues } = await swal.fire({
    title: '@lang("site.landing_page_name_title")',
    html:
      '<input id="title" class="swal2-input">',
    focusConfirm: false,
    preConfirm: () => {
        swal("@lang('Please upload lesson video first.')",{icon:"warning"});
    }
  })

  if (formValues) {
    Swal.fire(JSON.stringify(formValues))
  }

  })()
}

let hexToRgb= c=> `rgb(${c.match(/\w\w/g).map(x=>+`0x${x}`)})`;

</script>

<form method="post" id="destroy">
    {{ csrf_field() }}
    {{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
    var confirm = window.confirm('Do you want to delete this question?');
    if(confirm){
        $("#destroy").attr('action',val);
        $("#destroy").submit();
    }
 }
</script>
<style>
    .error{
        color: red !important;
    }
</style>

@endsection