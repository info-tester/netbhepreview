@extends('layouts.app')
@section('title')
@lang('client_site.landing_page_leads')
@endsection
@section('style')
@include('includes.style')
<style>
    .bt_m{
        margin-bottom: 6px !important;
    }
    .block{
        margin-bottom: 10px !important;
    }
    .view-page strong{
        width: 200px !important;
    }
</style>
@endsection
@section('scripts')
@include('includes.scripts')
<script src="{{URL::to('public/js/chosen.jquery.min.js')}}"></script>
@endsection
@section('header')
@include('includes.professional_header')


<link rel="stylesheet" href="{{URL::to('public/css/chosen.css')}}">
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('client_site.landing_page_leads')</h2>

        <div class="bokcntnt-bdy no-margin-top">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0 && $user->sell!='PS')
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>

            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">

                <div class="block" style="float: right;">
                    @if(sizeof(@$leads)>0)
                    <a class="btn btn-success" href="{{route('landing.page.leads.exports',['id'=>@$landing->id])}}" id="add_new_template">@lang('client_site.export_leads')</a>
                    @endif
                    <a class="btn btn-success" href="{{route('list.landing.page.templates')}}" id="add_new_template">@lang('site.back')</a>
                </div>
                <div class="col-md-12 view-page">
                    <p><strong>@lang('site.landing_page_name') :</strong> {{@$landing->landing_title}}</p>
                    <p><strong>@lang('site.template') :</strong> @lang('site.template') - {{@$landing->landing_template_id}}</p>

                </div>


                <div class="buyer_table">
                    <div class="table-responsive">
                        @if(sizeof(@$leads)>0)
                        <div class="table">
                            <div class="one_row1 hidden-sm-down only_shawo">
                                @if(@$landing->lead_fname=='Y')
                                <div class="cell1 tab_head_sheet">@lang('client_site.first_name')</div>
                                @endif
                                @if(@$landing->lead_lname=='Y')
                                <div class="cell1 tab_head_sheet">@lang('client_site.last_name')</div>
                                @endif
                                @if(@$landing->lead_email=='Y')
                                <div class="cell1 tab_head_sheet">@lang('client_site.email')</div>
                                @endif
                                @if(@$landing->lead_mobile=='Y')
                                <div class="cell1 tab_head_sheet">Mobile</div>
                                @endif
                                @if(@$landing->lead_message=='Y')
                                <div class="cell1 tab_head_sheet">@lang('client_site.message')</div>
                                @endif
                                @if(@$landing->lead_address=='Y')
                                <div class="cell1 tab_head_sheet">@lang('client_site.address')</div>
                                @endif
                                @if(@$landing->lead_country_code=='Y')
                                <div class="cell1 tab_head_sheet">@lang('site.country_code')</div>
                                @endif
                                @if(@$landing->lead_city=='Y')
                                <div class="cell1 tab_head_sheet">@lang('client_site.city')</div>
                                @endif
                                @if(@$landing->lead_state=='Y')
                                <div class="cell1 tab_head_sheet">@lang('client_site.state')</div>
                                @endif
                                @if(@$landing->lead_postal_code=='Y')
                                <div class="cell1 tab_head_sheet">@lang('client_site.postal_code')</div>
                                @endif
                                @if(@$landing->lead_website=='Y')
                                <div class="cell1 tab_head_sheet">Website</div>
                                @endif
                                @if(@$landing->lead_facebook=='Y')
                                <div class="cell1 tab_head_sheet">Facebook Link</div>
                                @endif
                                @if(@$landing->lead_instagram=='Y')
                                <div class="cell1 tab_head_sheet">Instagram Link</div>
                                @endif
                                @if(@$landing->lead_linkedin=='Y')
                                <div class="cell1 tab_head_sheet">Linkedin Link</div>
                                @endif
                            </div>
                            <!--row 1-->

                                @foreach(@$leads as $k=>$detail)
                                    <div class="one_row1 small_screen31">
                                        @if(@$landing->lead_fname=='Y')
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('client_site.first_name')</span>
                                            <p class="add_ttrr">{{@$detail->fname??'-'}}</p>
                                        </div>
                                        @endif
                                        @if(@$landing->lead_lname=='Y')
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('client_site.last_name')/span>
                                            <p class="add_ttrr">{{@$detail->lname??'-'}}</p>
                                        </div>
                                        @endif
                                        @if(@$landing->lead_email=='Y')
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('client_site.email')</span>
                                            <p class="add_ttrr">{{@$detail->email??'-'}}</p>
                                        </div>
                                        @endif
                                        @if(@$landing->lead_mobile=='Y')
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">Mobile</span>
                                            <p class="add_ttrr">{{@$detail->mobile??'-'}}</p>
                                        </div>
                                        @endif
                                        @if(@$landing->lead_message=='Y')
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('client_site.message')</span>
                                            <p class="add_ttrr">{{@$detail->message??'-'}}</p>
                                        </div>
                                        @endif
                                        @if(@$landing->lead_address=='Y')
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('client_site.address')</span>
                                            <p class="add_ttrr">{{@$detail->address??'-'}}</p>
                                        </div>
                                        @endif
                                        @if(@$landing->lead_country_code=='Y')
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.country_code')</span>
                                            <p class="add_ttrr">{{@$detail->country_code??'-'}}</p>
                                        </div>
                                        @endif
                                        @if(@$landing->lead_city=='Y')
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('client_site.city')</span>
                                            <p class="add_ttrr">{{@$detail->city??'-'}}</p>
                                        </div>
                                        @endif
                                        @if(@$landing->lead_state=='Y')
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.city')</span>
                                            <p class="add_ttrr">{{@$detail->state??'-'}}</p>
                                        </div>
                                        @endif
                                        @if(@$landing->lead_postal_code=='Y')
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('client_site.postal_code')</span>
                                            <p class="add_ttrr">{{@$detail->postal_code??'-'}}</p>
                                        </div>
                                        @endif
                                        @if(@$landing->lead_website=='Y')
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">website</span>
                                            <p class="add_ttrr">{{@$detail->website??'-'}}</p>
                                        </div>
                                        @endif
                                        @if(@$landing->lead_facebook=='Y')
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">facebook_link</span>
                                            <p class="add_ttrr">{{@$detail->facebook_link??'-'}}</p>
                                        </div>
                                        @endif
                                        @if(@$landing->lead_instagram=='Y')
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">instagram_link</span>
                                            <p class="add_ttrr">{{@$detail->instagram_link??'-'}}</p>
                                        </div>
                                        @endif
                                        @if(@$landing->lead_linkedin=='Y')
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">linkedin_link</span>
                                            <p class="add_ttrr">{{@$detail->linkedin_link??'-'}}</p>
                                        </div>
                                        @endif
                                    </div>
                                @endforeach

                        </div>
                        @else

                            <div class="one_row small_screen31">
                                <center><span><h3 class="error">Você ainda não possui Leads</h3></span></center>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>




@endsection
@section('footer')
@include('includes.footer')

<style>
    .error{
        color: red !important;
    }
</style>

@endsection
