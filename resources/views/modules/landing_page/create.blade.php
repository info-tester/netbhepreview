@extends('layouts.app')
@section('title')
    @lang('site.welcome_to_dashboard')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
<link href="{{ URL::to('public/frontend/css/course_style.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ URL::to('public/frontend/css/course_responsive.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ URL::to('public/frontend/css/course_bootstrap.min') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::to('public/frontend/icofont/icofont.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::to('public/frontend/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />
<style>
.error{
	color:red;
}
.success{
	color:forestgreen;
}
.sweet-ok{
    background: #1781d2 !important;
}
.sweet-ok:hover{
    background: #000 !important;
}
.ui-datepicker-div{
    z-index: 99999;
}
#header_button_lead_div{
    background: #ddd;
}
#save_single:disabled, #save_single[disabled]{
    opacity:0.7;
}
.tooltip {
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted #ccc;
    color: #006080;
    opacity:1 !important;
}

.tooltip .tooltiptext {
    visibility: hidden;
    position: absolute;
    width: 220px;
    background-color: #555;
    color: #fff;
    text-align: center;
    padding: 5px;
    border-radius: 6px;
    z-index: 1;
    opacity: 0;
    transition: opacity 0.3s;
    margin-left:10px
}
.tooltip-right {
    top: 0px;
    left: 125%;
}
.tooltip-right::after {
    content: "";
    position: absolute;
    top: 10%;
    right: 100%;
    margin-top: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: transparent #555 transparent transparent;
}

.tooltip:hover .tooltiptext {
    visibility: visible;
    opacity: 1;
}

.tooltip {
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted black;
}

.tooltip .tooltip-top {
    visibility: hidden;
    width: 300px;
    background-color: #555;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    position: absolute;
    z-index: 1;
    bottom: 125%;
    left: 50%;
    margin-left: -60px;
    opacity: 0;
    transition: opacity 0.3s;
}

.tooltip .tooltip-top::after {
    content: "";
    position: absolute;
    top: 100%;
    left: 20%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: #555 transparent transparent transparent;
}

.tooltip:hover .tooltip-top {
    visibility: visible;
    opacity: 1;
}
.tooltip .tooltiptext.tooltip-top{
    border-radius: 6px !important;
}
.preview_page_btn{
    color: black !important;
    font-size: 25px;
    background: transparent;
    padding: 5px 12px;
    border-radius: 0.25rem;
}
.preview_page_btn:hover{
    background: #1781d2 !important;
    color: white !important;
}
.create_note{
    color: #888;
    padding: 0px 6px 15px;
    font-size: 14px;
}
</style>
@endsection
@section('content')
	<section class="wrapper2 other_wrapper">
		@section('header')
		@include('includes.professional_header')
		@endsection
		<!-- section-header.// -->
		<!-- ========================= SECTION CONTENT ========================= -->
		<nav class="navbar navbar-expand-lg navbar-dark sticky-top">
			<div class="w-100">
				<!-- <div class="top-bar">
					<div class="top-bar-left-container">
						<a href="#url" class="top-bar-button"> <i class="icofont-close-line"></i> </a>
						<div class="d-none d-md-flex align-items-center">
							<h4 class="ml-4">Courses</h4> </div>
					</div>
					<div class="top-bar-center-container"> <a href="#url" class="user_llk">Your First Course <i class="icofont-caret-down"></i></a>
						<div class="show01 search-category" style="display: none;">
							<div class="select2-search">
								<input type="text" name=""> </div>
							<ul class="select2-results">
								<li class="select2-no-results">No matches found</li>
							</ul>
						</div>
					</div>
					<div class="top-bar-right-container"> <a href="#url" class="topbar-right-btn">
						build landing page <i class="icofont-long-arrow-right"></i>
					</a> </div>
				</div> -->
				<div class="navbar-menu navbar-menu-mobile">
					<div class="container-fluid">
						<div class="all-menu-sec">
							<!---<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav"> <span class="navbar-toggler-icon"></span> </button> --->
                            <h2 class="temp_title mobile_title">{{ @$detail->landing_title ?? 'Título do Modelo'}}</h2>
							<div class="" id="main_nav">
								<ul class="navbar-nav">
									<li class="nav-item active show_course_view tab_choose_temp"> <a class="nav-link" href="javascript:;">   @lang('site.choose_template') </a> </li>
									<li class="nav-item show_course_view tab_header" data-target="header"> <a class="nav-link" href="javascript:;">   @lang('site.header') </a> </li>
									<li class="nav-item show_course_view tab_why_us" data-target="why_us"> <a class="nav-link" href="javascript:;">   @lang('site.why_us') </a> </li>
									<li class="nav-item show_course_view tab_content" data-target="content"> <a class="nav-link" href="javascript:;">   @lang('site.content') </a> </li>
									<li class="nav-item show_course_view tab_single" data-target="single"> <a class="nav-link" href="javascript:;">   @lang('site.single_video') </a> </li>
									<li class="nav-item show_course_view tab_faqs" data-target="faqs"> <a class="nav-link" href="javascript:;">   @lang('site.faq') </a> </li>
									<li class="nav-item show_course_view tab_products" data-target="products"> <a class="nav-link" href="javascript:;">   @lang('site.products') </a> </li>
									<li class="nav-item show_course_view tab_design" data-target="design"> <a class="nav-link" href="javascript:;">   @lang('site.design') </a> </li>
									<li class="nav-item show_course_view tab_logo" data-target="logo"> <a class="nav-link" href="javascript:;">   @lang('client_site.logo_or_icons') </a> </li>
								</ul>
							</div>

							<div class="navbar__actions desktop_title">
                                <div class="landing-title">
                                    <!-- <span class="temp_title_icon">
                                        <i class="fa fa-play" aria-hidden="true"></i>
                                    </span> -->
                                    <h2 class="temp_title">{{ @$detail->landing_title ?? 'Título do Modelo'}}</h2>
                                    @if(@$detail->id)
                                    <a href="{{url('/')}}/l/{{@$detail->userDetails->slug}}/{{@$detail->slug}}" class="preview_page_btn" target="_blank" rel="noopener noreferrer" title="@lang('site.preview')">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>
                                    @endif
                                </div>
								<!-- <button class="preview-btn" title="Preview course as an enrolled student"> <i class="icofont-eye-alt"></i> Preview </button> -->
							</div>
						</div>
						<!-- navbar-collapse.// -->
					</div>
				</div>
				<!-- container //  -->
			</div>
		</nav>
		<div class="page_builder">
			<div class="course_view mobile_chapter_list height_bottom">
				<div class="list-view-sessoin">
                    <div class="create_note">@lang('client_site.landing_page_create_note')</div>
					<div>
						<div id="main">
                            <input type="text" id="template_id" value="{{@$detail->id}}" hidden>
                            <div class="tab_target" id="choose_temp">
                                @foreach(@$templates as $template)
                                    <div class="card landing_temp_card {{ @$template->id == 1 ? 'active_cert' : '' }}" data-id="{{ @$template->id }}">
                                        <img class="card-img-top" src="{{url('/')}}/public/frontend/images/cart_temp{{ @$template->id }}.png" alt="Card image cap">
                                        <div class="card-body">
                                            <h5 class="card-title tabcardtitle">{{ @$template->template_name }}</h5>
                                        </div>
                                    </div>
                                    <br>
                                @endforeach
                            </div>
                            <div class="tab_target" id="header" style="display: none;">
                                <form action="{{ route('store.landing.page.template') }}" id="myHeaderForm">
                                    <div class="cert_cntnt">
                                        <label for="header_image">@lang('site.upload_picture')</label>
                                        <input type="file" name="header_image" id="header_image" class="form-control" accept="image/*">
                                        <label class="text-secondary">
                                            @if(@$detail->landing_template_id == 1)
                                                @lang('client_site.recommended_size') 768 * 1024
                                            @elseif(@$detail->landing_template_id == 2)
                                                @lang('client_site.recommended_size') 320 * 320
                                            @else
                                                @lang('client_site.recommended_size') 1920 * 1080
                                            @endif
                                        </label>
                                    </div>
                                    <div class="cert_cntnt">
                                        <label for="header_line_1">@lang('client_site.header_line_1')</label>
                                        <input type="text" name="header_line_1" id="header_line_1" class="form-control required mb-3" value="{{ @$detail->header_line_1 ?? @$temp->header_line_1 }}">
                                    </div>
                                    <div class="cert_cntnt">
                                        <label for="header_line_2">@lang('client_site.header_line_2')</label>
                                        <input type="text" name="header_line_2" id="header_line_2" class="form-control required mb-3" value="{{ @$detail->header_line_2 ?? @$temp->header_line_2 }}">
                                    </div>
                                    <div class="cert_cntnt">
                                        <label for="header_button">@lang('client_site.header_button')</label> <br>
                                        <label for="header_btn_yes">
                                            <input type="radio" name="header_btn" value="Y" id="header_btn_yes" @if(@$detail->header_button == 'Y') checked @endif>
                                            @lang('site.yes')
                                        </label>
                                        <label for="header_btn_no">
                                            <input type="radio" name="header_btn" value="N" id="header_btn_no" @if(@$detail->header_button == 'N') checked @endif>
                                            @lang('site.no')
                                        </label>
                                    </div>
                                    <div id="header_btn_action">

                                        <div class="cert_cntnt">
                                            <label for="header_button_text">@lang('client_site.header_button_text')</label>
                                            <input type="text" name="header_button_text" id="header_button_text" class="form-control required mb-3" value="{{ @$detail->header_button_text ?? 'Contact Us' }}">
                                        </div>
                                        <div class="cert_cntnt">
                                            <label for="header_button_type">@lang('client_site.header_button_type')</label> <br>
                                            <label for="header_btn_url">
                                                <input type="radio" name="header_button_type" value="URL" id="header_btn_url" @if(@$detail->header_button_type == 'URL') checked @endif>
                                                URL
                                            </label>
                                            <label for="header_btn_lead">
                                                <input type="radio" name="header_button_type" value="LEAD" id="header_btn_lead" @if(@$detail->header_button_type == 'LEAD') checked @endif>
                                                Lead
                                            </label>
                                        </div>
                                        <div class="cert_cntnt" id="header_button_link_div" @if(@$detail->header_button_type == 'LEAD') style="display: none;" @endif>
                                            <label for="header_button_link">@lang('client_site.header_button_link')</label>
                                            <input type="url" name="header_button_link" id="header_button_link" class="form-control required mb-3" value="{{ @$detail->header_button_link }}">
                                        </div>
                                        <div id="header_button_lead_div" class="p-2" @if(@$detail->header_button_type == 'URL') style="display: none;" @endif>
                                            <h5>
                                                @lang('client_site.lead_section')
                                                <div class="tooltip">
                                                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                    <span class="tooltiptext tooltip-top">{{__('client_site.lead_section_tooltip')}}</span>
                                                </div>
                                            </h5>
                                            <div class="cert_cntnt row">
                                                <div class="col-md-6">
                                                    <label for="lead_fname">
                                                        <input type="checkbox" name="lead_fname" id="lead_fname" class="mb-3"  @if(@$detail->lead_fname == 'Y') checked @endif>
                                                        @lang('client_site.first_name')
                                                    </label>
                                                    <div class="clearfix"></div>
                                                    <label for="lead_lname">
                                                        <input type="checkbox" name="lead_lname" id="lead_lname" class="mb-3"  @if(@$detail->lead_lname == 'Y') checked @endif>
                                                        @lang('client_site.last_name')
                                                    </label>
                                                    <div class="clearfix"></div>
                                                    <label for="lead_country_code">
                                                        <input type="checkbox" name="lead_country_code" id="lead_country_code" class="mb-3"  @if(@$detail->lead_country_code == 'Y') checked @endif>
                                                        @lang('site.country_code')
                                                    </label>
                                                    <div class="clearfix"></div>
                                                    <label for="lead_mobile">
                                                        <input type="checkbox" name="lead_mobile" id="lead_mobile" class="mb-3"  @if(@$detail->lead_mobile == 'Y') checked @endif>
                                                        Mobile
                                                    </label>
                                                    <div class="clearfix"></div>
                                                    <label for="lead_email">
                                                        <input type="checkbox" name="lead_email" id="lead_email" class="mb-3"  @if(@$detail->lead_email == 'Y') checked @endif>
                                                        @lang('site.email')
                                                    </label>
                                                    <div class="clearfix"></div>
                                                    <label for="lead_address">
                                                        <input type="checkbox" name="lead_address" id="lead_address" class="mb-3"  @if(@$detail->lead_address == 'Y') checked @endif>
                                                        @lang('site.street_address')
                                                    </label>
                                                    <div class="clearfix"></div>
                                                    <label for="lead_city">
                                                        <input type="checkbox" name="lead_city" id="lead_city" class="mb-3"  @if(@$detail->lead_city == 'Y') checked @endif>
                                                        @lang('site.city')
                                                    </label>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="lead_facebook">
                                                        <input type="checkbox" name="lead_facebook" id="lead_facebook" class="mb-3"  @if(@$detail->lead_facebook == 'Y') checked @endif>
                                                        Facebook link
                                                    </label>
                                                    <div class="clearfix"></div>
                                                    <label for="lead_instagram">
                                                        <input type="checkbox" name="lead_instagram" id="lead_instagram" class="mb-3"  @if(@$detail->lead_instagram == 'Y') checked @endif>
                                                        Instagram link
                                                    </label>
                                                    <div class="clearfix"></div>
                                                    <label for="lead_linkedin">
                                                        <input type="checkbox" name="lead_linkedin" id="lead_linkedin" class="mb-3"  @if(@$detail->lead_linkedin == 'Y') checked @endif>
                                                        LinkedIn link
                                                    </label>
                                                    <div class="clearfix"></div>
                                                    <label for="lead_website">
                                                        <input type="checkbox" name="lead_website" id="lead_website" class="mb-3"  @if(@$detail->lead_website == 'Y') checked @endif>
                                                        Website
                                                    </label>
                                                    <div class="clearfix"></div>
                                                    <label for="lead_state">
                                                        <input type="checkbox" name="lead_state" id="lead_state" class="mb-3"  @if(@$detail->lead_state == 'Y') checked @endif>
                                                        @lang('site.state')
                                                    </label>
                                                    <div class="clearfix"></div>
                                                    <label for="lead_postal_code">
                                                        <input type="checkbox" name="lead_postal_code" id="lead_postal_code" class="mb-3"  @if(@$detail->lead_postal_code == 'Y') checked @endif>
                                                        @lang('site.postal_code')
                                                    </label>
                                                    <div class="clearfix"></div>
                                                    <label for="lead_message">
                                                        <input type="checkbox" name="lead_message" id="lead_message" class="mb-3"  @if(@$detail->lead_message == 'Y') checked @endif>
                                                        @lang('site.message')
                                                    </label>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="col-md-12">
                                                    <label for="lead_position">@lang('client_site.lead_position')</label> <br/>
                                                    <label for="on_popup">
                                                        <input type="radio" name="lead_position" value="P" id="on_popup" @if(@$detail->lead_position == 'P') checked @endif>
                                                        @lang('client_site.on_popup')
                                                    </label>
                                                    <label for="on_footer">
                                                        <input type="radio" name="lead_position" value="F" id="on_footer" @if(@$detail->lead_position == 'F') checked @endif>
                                                        @lang('client_site.on_footer')
                                                    </label>
                                                </div>
                                                <div class="col-md-12">
                                                    <label for="lead_section_heading">@lang('client_site.lead_section_heading')</label>
                                                    <input type="text" name="lead_section_heading" id="lead_section_heading" class="form-control mb-3" value="{{@$detail->lead_section_heading ?? 'Contact With Us'}}">
                                                </div>
                                                <div class="col-md-12">
                                                    <label for="lead_section_desc">@lang('client_site.lead_section_desc')</label>
                                                    <input type="text" name="lead_section_desc" id="lead_section_desc" class="form-control mb-3" value="{{@$detail->lead_section_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.'}}">
                                                </div>
                                                <div class="col-md-12">
                                                    <label for="lead_success_heading">@lang('client_site.lead_success_heading')</label>
                                                    <input type="text" name="lead_success_heading" id="lead_success_heading" class="form-control mb-3" value="{{@$detail->lead_success_heading ?? 'Heading'}}">
                                                </div>
                                                <div class="col-md-12">
                                                    <label for="lead_success_desc">@lang('client_site.lead_success_description')</label>
                                                    <textarea name="lead_success_desc" id="lead_success_desc" class="form-control mb-3">{{@$detail->lead_success_desc ?? 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem dolor vero mollitia nam molestiae neque, maiores facilis voluptate eligendi ducimus id fugiat assumenda perspiciatis optio dignissimos eos nemo ipsum architecto unde ea tempore. Ratione eaque exercitationem magnam blanditiis, tenetur minima incidunt maxime sint earum est ut necessitatibus odit tempore vero. Recusandae dolores voluptatem officiis dolorum? Eum veniam minima ducimus odio non, dolore, nulla ullam a nemo cupiditate quae praesentium? Dolorem voluptatem consequuntur provident sapiente magnam consectetur ut omnis saepe libero tenetur id, excepturi nobis odit. Voluptate assumenda tenetur odit neque culpa voluptatibus consectetur, quo sed quis debitis numquam, inventore similique. Et reiciendis distinctio libero itaque ab, culpa voluptatem similique facere aliquid dolorum sint soluta minus deleniti? Commodi, eaque itaque. Porro?'}}</textarea>
                                                </div>
                                                <div class="col-md-12">
                                                    <label for="lead_section_footer_submit_btn">@lang('client_site.lead_section_footer_submit_btn')</label>
                                                    <input type="text" name="lead_section_footer_submit_btn" id="lead_section_footer_submit_btn" class="form-control mb-3" value="{{@$detail->lead_section_footer_submit_btn ?? 'Submit Now'}}">
                                                </div>
                                                <div class="col-md-12">
                                                    <label for="lead_success_button">@lang('client_site.show_lead_success_button')</label> <br/>
                                                    <label for="lead_success_button_yes">
                                                        <input type="radio" name="lead_success_button" value="Y" id="lead_success_button_yes" @if(@$detail->lead_success_button == 'Y') checked @endif>
                                                        @lang('site.yes')
                                                    </label>
                                                    <label for="lead_success_button_no">
                                                        <input type="radio" name="lead_success_button" value="N" id="lead_success_button_no" @if(@$detail->lead_success_button == 'N') checked @endif>
                                                        @lang('site.no')
                                                    </label>
                                                </div>
                                                <div class="col-md-12" id="lead_success_button_div" @if(@$detail->lead_success_button == 'N') style="display: none;" @endif >
                                                    <div class="">
                                                        <label for="lead_success_button_caption">@lang('client_site.lead_success_button_caption')</label>
                                                        <input type="text" name="lead_success_button_caption" id="lead_success_button_caption" class="form-control mb-3" value="{{@$detail->lead_success_button_caption ?? 'OK'}}">
                                                    </div>
                                                    <div class="">
                                                        <label for="lead_download_link">@lang('client_site.lead_success_button_url')</label>
                                                        <input type="url" name="lead_download_link" id="lead_download_link" class="form-control required mb-3" value="{{@$detail->lead_download_link}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <label for="lead_privacy_policy">@lang('client_site.lead_privacy_policy')</label>
                                                    <textarea name="lead_privacy_policy" id="lead_privacy_policy" class="form-control required mb-3">{{@$detail->lead_privacy_policy ?? \Lang::get('client_site.privacy_policy')}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cert_cntnt">
                                        <label for="above_course">@lang('client_site.header_reverse_counter')</label>  <br>
                                        <label for="header_reverse_yes">
                                            <input type="radio" name="header_reverse" value="Y" id="header_reverse_yes" @if(@$detail->header_reverse_counter == 'Y') checked @endif>
                                            @lang('site.yes')
                                        </label>
                                        <label for="header_reverse_no">
                                            <input type="radio" name="header_reverse" value="N" id="header_reverse_no"  @if(@$detail->header_reverse_counter == 'N') checked @endif>
                                            @lang('site.no')
                                        </label>
                                    </div>
                                    <div class="cert_cntnt" id="header_reverse_action" @if(@$detail->header_reverse_counter == 'N') style="display: none;" @endif>
                                        <label for="header_counter_date">@lang('client_site.header_reverse_counter') @lang('site.date')</label>
                                        <input type="text" name="header_counter_date" id="header_counter_date" class="form-control required mb-3" value="{{ @$detail->header_counter_date ?? date('Y-m-d', strtotime('+1 day')) }}" autocomplete="off" readonly>
                                    </div>
                                </form>
                            </div>
                            <div class="tab_target" id="why_us" style="display: none;">
                                <div class="cert_cntnt mb-3">
                                    <label for="why_us_section">@lang('client_site.show_why_us_section')</label> <br>
                                    <label for="why_us_section_yes">
                                        <input type="radio" name="why_us_section" value="Y" id="why_us_section_yes" @if(@$detail->why_us_section == 'Y') checked @endif>
                                        @lang('site.yes')
                                    </label>
                                    <label for="why_us_section_no">
                                        <input type="radio" name="why_us_section" value="N" id="why_us_section_no" @if(@$detail->why_us_section == 'N') checked @endif>
                                        @lang('site.no')
                                    </label>
                                </div>
                                <div id="why_us_total" @if(@$detail->why_us_section == 'N') style="display: none;" @endif >
                                    <div class="cert_cntnt mb-3">
                                        <label for="whyus_main_title">@lang('client_site.why_us_title')</label> <br>
                                        <input type="text" name="whyus_main_title" id="whyus_main_title" class="form-control" value="{{@$detail->why_us_heading ?? 'How it works'}}">
                                    </div>
                                    <div class="cert_cntnt mb-3">
                                        <label for="whyus_main_desc">@lang('client_site.why_us_description')</label> <br>
                                        <input type="text" name="whyus_main_desc" id="whyus_main_desc" class="form-control" value="{{@$detail->why_us_description ?? 'Your dream home is in good hands.'}}">
                                        <a href="javascript:;" class="btn btn-primary mt-3 pull-right" id="save_whyus_head">+ @lang('client_site.save')</a>
                                    </div>
                                    <div class="clearfix"></div>
                                    <hr/>

                                    @if(@$detail->getWhyUs && count(@$detail->getWhyUs) > 0)
                                    <div id="main">
                                        <div class="accordion1 why_us_div" id="faq">
                                            @foreach(@$detail->getWhyUs as $k=>$row)
                                            <div class="card whyus_section" id="whyus_section_{{@$k + 1}}" data-identity="{{@$row->id}}">
                                                <form action="{{ route('store.landing.page.template') }}" id="myWhyUsForm{{@$k + 1}}">
                                                    <div class="card-header d-flex" id="faqhead{{@$k + 1}}" data-id="{{@$k + 1}}">
                                                        <a href="javascript:;" class="btn btn-header-link acco-chap rem_tick collapsed"
                                                            data-toggle="collapse"
                                                            data-target="#faq{{@$k + 1}}"
                                                            aria-expanded="true"
                                                            aria-controls="faq{{@$k + 1}}"
                                                        >
                                                            <img src="{{ URL::to('public/frontend/images/cha.png') }}">
                                                            <h5>@lang('client_site.section') {{@$k + 1}}</h5>
                                                            <a href="javascript:;" class="btn del_whyus" data-id="{{@$k + 1}}" data-identity="{{@$row->id}}"> <i class="fa fa-times" aria-hidden="true"></i> </a>
                                                        </a>
                                                    </div>
                                                    <div id="faq{{@$k + 1}}" class="collapse" aria-labelledby="faqhead{{@$k + 1}}" data-parent="#faq">
                                                        <div class="card-body">
                                                            <div class="whyus_detail" id="whyus_{{@$k + 1}}">
                                                                <div class="cert_cntnt">
                                                                    <label for="whyus_img_{{@$k + 1}}">@lang('site.upload_picture')</label>
                                                                    <input type="file" name="whyus_img_{{@$k + 1}}" id="whyus_img_{{@$k + 1}}" class="form-control whyus_img" data-id="{{@$k + 1}}" accept="image/*">
                                                                    <label class="text-secondary">@lang('client_site.recommended_size') 320 * 320 </label>
                                                                </div>
                                                                <div class="cert_cntnt">
                                                                    <label for="whyus_title_{{@$k + 1}}">@lang('site.title')</label>
                                                                    <input type="text" name="whyus_title_{{@$k + 1}}" id="whyus_title_{{@$k + 1}}" class="form-control required whyus_title" data-id="{{@$k + 1}}" value="{{@$row->title}}">
                                                                </div>
                                                                <div class="cert_cntnt mb-3">
                                                                    <label for="whyus_desc_{{@$k + 1}}">@lang('site.description')</label>
                                                                    <textarea name="whyus_desc_{{@$k + 1}}" id="whyus_desc_{{@$k + 1}}" class="form-control required whyus_desc" data-id="{{@$k + 1}}">{{@$row->description}}</textarea>
                                                                </div>
                                                                <div class="sect_bottom_div">
                                                                    <p style="visibility: hidden;">
                                                                        <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                                                        @lang('client_site.saved_successfully')
                                                                    </p>
                                                                    <a href="javascript:;" class="whyus_save btn btn-primary pull-right mb-3" id="whyus_save_{{@$k + 1}}" data-id="{{@$k + 1}}">@lang('client_site.save')</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    @else
                                    <div id="main">
                                        <div class="accordion1 why_us_div" id="faq">
                                            <div class="card whyus_section" id="whyus_section_1" data-identity="0">
                                                <form action="{{ route('store.landing.page.template') }}" id="myWhyUsForm1">
                                                    <div class="card-header d-flex" id="faqhead1" data-id="1">
                                                        <a href="javascript:;" class="btn btn-header-link acco-chap rem_tick collapsed"
                                                            data-toggle="collapse"
                                                            data-target="#faq1"
                                                            aria-expanded="true"
                                                            aria-controls="faq1"
                                                        >
                                                            <img src="{{ URL::to('public/frontend/images/cha.png') }}">
                                                            <h5>@lang('client_site.section') 1</h5>
                                                            <a href="javascript:;" class="btn del_whyus" data-id="1" data-identity="0"> <i class="fa fa-times" aria-hidden="true"></i> </a>
                                                        </a>
                                                    </div>
                                                    <div id="faq1" class="collapse" aria-labelledby="faqhead1" data-parent="#faq">
                                                        <div class="card-body">
                                                            <div class="whyus_detail" id="whyus_1">
                                                                <div class="cert_cntnt">
                                                                    <label for="whyus_img_1">@lang('site.upload_picture')</label>
                                                                    <input type="file" name="whyus_img_1" id="whyus_img_1" class="form-control whyus_img" data-id="1" accept="image/*">
                                                                    <label class="text-secondary">@lang('client_site.recommended_size') 320 * 320 </label>
                                                                </div>
                                                                <div class="cert_cntnt">
                                                                    <label for="whyus_title_1">@lang('site.title')</label>
                                                                    <input type="text" name="whyus_title_1" id="whyus_title_1" class="form-control required whyus_title" data-id="1" value="certified contractors">
                                                                </div>
                                                                <div class="cert_cntnt mb-3">
                                                                    <label for="whyus_desc_1">@lang('site.description')</label>
                                                                    <textarea name="whyus_desc_1" id="whyus_desc_1" class="form-control required whyus_desc" data-id="1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.</textarea>
                                                                </div>
                                                                <div class="sect_bottom_div">
                                                                    <p style="visibility: hidden;">
                                                                        <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                                                        @lang('client_site.saved_successfully')
                                                                    </p>
                                                                    <a href="javascript:;" class="whyus_save btn btn-primary pull-right mb-3" id="whyus_save_1" data-id="1">@lang('client_site.save')</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="card whyus_section" id="whyus_section_2" data-identity="0">
                                                <form action="{{ route('store.landing.page.template') }}" id="myWhyUsForm2">
                                                    <div class="card-header d-flex" id="faqhead2" data-id="2">
                                                        <a href="javascript:;" class="btn btn-header-link acco-chap rem_tick collapsed"
                                                            data-toggle="collapse"
                                                            data-target="#faq2"
                                                            aria-expanded="true"
                                                            aria-controls="faq2"
                                                        >
                                                            <img src="{{ URL::to('public/frontend/images/cha.png') }}">
                                                            <h5>@lang('client_site.section') 2</h5>
                                                            <a href="javascript:;" class="btn del_whyus" data-id="2" data-identity="0"> <i class="fa fa-times" aria-hidden="true"></i> </a>
                                                        </a>
                                                    </div>
                                                    <div id="faq2" class="collapse" aria-labelledby="faqhead2" data-parent="#faq">
                                                        <div class="card-body">
                                                            <div class="whyus_detail" id="whyus_2">
                                                                <div class="cert_cntnt">
                                                                    <label for="whyus_img_2">@lang('site.upload_picture')</label>
                                                                    <input type="file" name="whyus_img_2" id="whyus_img_2" class="form-control whyus_img" data-id="2" accept="image/*">
                                                                    <label class="text-secondary">@lang('client_site.recommended_size') 320 * 320 </label>
                                                                </div>
                                                                <div class="cert_cntnt">
                                                                    <label for="whyus_title_2">@lang('site.title')</label>
                                                                    <input type="text" name="whyus_title_2" id="whyus_title_2" class="form-control required whyus_title" data-id="2" value="employees you can trust">
                                                                </div>
                                                                <div class="cert_cntnt mb-3">
                                                                    <label for="whyus_desc_2">@lang('site.description')</label>
                                                                    <textarea name="whyus_desc_2" id="whyus_desc_2" class="form-control required whyus_desc" data-id="2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.</textarea>
                                                                </div>
                                                                <div class="sect_bottom_div">
                                                                    <p style="visibility: hidden;">
                                                                        <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                                                        @lang('client_site.saved_successfully')
                                                                    </p>
                                                                    <a href="javascript:;" class="whyus_save btn btn-primary pull-right mb-3" id="whyus_save_2" data-id="2">@lang('client_site.save')</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="card whyus_section" id="whyus_section_3" data-identity="0">
                                                <form action="{{ route('store.landing.page.template') }}" id="myWhyUsForm3">
                                                    <div class="card-header d-flex" id="faqhead3" data-id="3">
                                                        <a href="javascript:;" class="btn btn-header-link acco-chap rem_tick collapsed"
                                                            data-toggle="collapse"
                                                            data-target="#faq3"
                                                            aria-expanded="true"
                                                            aria-controls="faq3"
                                                        >
                                                            <img src="{{ URL::to('public/frontend/images/cha.png') }}">
                                                            <h5>@lang('client_site.section') 3</h5>
                                                            <a href="javascript:;" class="btn del_whyus" data-id="3" data-identity="0"> <i class="fa fa-times" aria-hidden="true"></i> </a>
                                                        </a>
                                                    </div>
                                                    <div id="faq3" class="collapse" aria-labelledby="faqhead3" data-parent="#faq">
                                                        <div class="card-body">
                                                            <div class="whyus_detail" id="whyus_3">
                                                                <div class="cert_cntnt">
                                                                    <label for="whyus_img_3">@lang('site.upload_picture')</label>
                                                                    <input type="file" name="whyus_img_3" id="whyus_img_3" class="form-control whyus_img" data-id="3" accept="image/*">
                                                                    <label class="text-secondary">@lang('client_site.recommended_size') 320 * 320 </label>
                                                                </div>
                                                                <div class="cert_cntnt">
                                                                    <label for="whyus_title_3">@lang('site.title')</label>
                                                                    <input type="text" name="whyus_title_3" id="whyus_title_3" class="form-control required whyus_title" data-id="3" value="Your dream is our plan">
                                                                </div>
                                                                <div class="cert_cntnt mb-3">
                                                                    <label for="whyus_desc_3">@lang('site.description')</label>
                                                                    <textarea name="whyus_desc_3" id="whyus_desc_3" class="form-control required whyus_desc" data-id="3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.</textarea>
                                                                </div>
                                                                <div class="sect_bottom_div">
                                                                    <p style="visibility: hidden;">
                                                                        <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                                                        @lang('client_site.saved_successfully')
                                                                    </p>
                                                                    <a href="javascript:;" class="whyus_save btn btn-primary pull-right mb-3" id="whyus_save_3" data-id="3">@lang('client_site.save')</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    <div>
                                        <a href="javascript:;" class="btn btn-primary" id="add_whyus">+ @lang('client_site.add_new_section')</a>
                                    </div>
                                </div>
                            </div>
                            <div class="tab_target" id="content" style="display: none;">
                                <form action="{{ route('store.landing.page.template') }}" id="myContentForm">
                                    <div class="cert_cntnt">
                                        <label for="content_type">@lang('site.content_type')</label>  <br>
                                        <label for="single_content">
                                            <input type="radio" name="body_single_content" value="Y" id="single_content" @if(@$detail->body_single_content) {{ @$detail->body_single_content == 'Y' ? 'checked' : '' }} @else {{ @$temp->body_single_content == 'Y' ? 'checked' : '' }} @endif>
                                            @lang('site.single_content')
                                        </label>
                                        <label for="multiple_content">
                                            <input type="radio" name="body_single_content" value="N" id="multiple_content"  @if(@$detail->body_single_content) {{ @$detail->body_single_content == 'N' ? 'checked' : '' }} @else {{ @$temp->body_single_content == 'N' ? 'checked' : '' }} @endif>
                                            @lang('site.multiple_content')
                                        </label>
                                    </div>
                                    <div id="single_content_div" @if(@$detail->body_single_content != null) @if(@$detail->body_single_content == 'N') style="display: none;" @endif @else @if(@$temp->body_single_content == 'N') style="display: none;" @endif @endif>
                                        <div class="cert_cntnt" id="image_style_type_div_single">
                                            <label for="image_style_type_single">@lang('client_site.image_style') </label>  <br>
                                            <label for="image_square_single">
                                                <input type="radio" class="image_style_type_single" name="image_style_single" value="S" id="image_square_single" @if(@$detail->image_style=='S') checked @elseif((@$temp->id==3||@$temp->id==4) && @$detail->image_style==null) checked  @endif >
                                                @lang('client_site.square')
                                            </label>
                                            <label for="image_circle_single">
                                                <input type="radio" class="image_style_type_single" name="image_style_single" value="C" id="image_circle_single" @if(@$detail->image_style=='C') checked @elseif((@$temp->id==2||@$temp->id==1) && @$detail->image_style==null) checked @endif >
                                                @lang('client_site.circle')
                                            </label>
                                        </div>
                                        <div class="cert_cntnt">
                                            <label for="body_single_content_image">@lang('site.upload_file')</label>  <br>
                                            <input type="file" name="body_single_content_image" id="body_single_content_image" class="form-control" accept="image/*">
                                            <label class="text-secondary">@lang('client_site.recommended_size') 800 * 800</label>
                                        </div>
                                        <div class="cert_cntnt">
                                            <label for="body_single_content_heading">@lang('site.title')</label>  <br>
                                            <input type="text" name="body_single_content_heading" id="body_single_content_heading" class="form-control required" value="{{ @$detail->body_single_content_heading ?? @$temp->body_single_content_heading }}">
                                        </div>
                                        <div class="cert_cntnt">
                                            <label for="body_single_content_desc">@lang('site.description')</label>  <br>
                                            <!-- <input type="text" name="body_single_content_desc" id="body_single_content_desc" class="form-control required" value="{{ @$detail->body_single_content_desc ?? @$temp->body_single_content_desc }}"> -->
                                            <textarea name="body_single_content_desc" id="body_single_content_desc" class="form-control required">{{ @$detail->body_single_content_desc ?? @$temp->body_single_content_desc }}</textarea>
                                        </div>
                                        <div class="cert_cntnt">
                                            <label for="body_single_content_button">@lang('site.button')</label>  <br>
                                            <label for="body_single_content_button_yes">
                                                <input type="radio" name="body_single_content_button" value="Y" id="body_single_content_button_yes" @if(@$detail->body_single_content_button == 'Y') checked @endif>
                                                @lang('site.yes')
                                            </label>
                                            <label for="body_single_content_button_no">
                                                <input type="radio" name="body_single_content_button" value="N" id="body_single_content_button_no"  @if(@$detail->body_single_content_button == 'N') checked @endif>
                                                @lang('site.no')
                                            </label>
                                        </div>
                                        <div id="body_single_content_button_div" style="margin-bottom:20px; @if(@$detail->body_single_content_button == 'N') display: none; @endif">
                                            <div class="cert_cntnt">
                                                <label for="body_single_content_button_text">@lang('site.button_caption')</label>  <br>
                                                <input type="text" name="body_single_content_button_text" id="body_single_content_button_text" class="form-control required" value="{{ @$detail->body_single_content_button_text ?? @$temp->body_single_content_button_text }}">
                                            </div>
                                            <div class="cert_cntnt">
                                                <label for="body_single_content_button_link">@lang('site.button_link')</label>  <br>
                                                <input type="url" name="body_single_content_button_link" id="body_single_content_button_link" class="form-control required" value="{{ @$detail->body_single_content_button_link }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div id="multiple_content_div" @if(@$detail->body_single_content) @if(@$detail->body_single_content == 'Y') style="display: none;" @endif @else @if(@$temp->body_single_content == 'Y') style="display: none;" @endif @endif>
                                        @if(@$detail->getContentDetails && count(@$detail->getContentDetails)>0)
                                            @foreach(@$detail->getContentDetails as $k=>$content)
                                                <div class="multicontent" id="multicontent_{{$k + 1}}" style="padding: 20px 0px;" data-id="{{$k + 1}}" data-saved="true">
                                                    <div class="multi_section_head">
                                                        <h5>@lang('client_site.section') {{$k + 1}}</h5>
                                                        @if( $k+1 !== 1 && $k+1 !== 2 )
                                                            <a href="javascript:;" class="btn del_multicontent" data-id="{{$k + 1}}"> <i class="fa fa-times" aria-hidden="true"></i> </a>
                                                        @endif
                                                    </div>
                                                    <div class="cert_cntnt">
                                                        <label for="content_file_type_{{$k + 1}}">@lang('site.content_file')</label>  <br>
                                                        <label for="content_image_{{$k + 1}}">
                                                            <input type="radio" class="content_file_type" name="content_file_{{$k + 1}}" value="I" id="content_image_{{$k + 1}}" data-id="{{$k + 1}}" {{ @$content->content_file_type != 'Y' ? 'checked' : '' }}>
                                                            @lang('site.upload_picture')
                                                        </label>
                                                        <label for="content_youtube_{{$k + 1}}">
                                                            <input type="radio" class="content_file_type" name="content_file_{{$k + 1}}" value="Y" id="content_youtube_{{$k + 1}}" data-id="{{$k + 1}}" {{ @$content->content_file_type == 'Y' ? 'checked' : '' }}>
                                                            Youtube Link
                                                        </label>
                                                    </div>
                                                    <div class="cert_cntnt" @if(@$content->content_file_type == 'Y') style="display: none;" @endif id="image_style_type_div_{{$k + 1}}">
                                                        <label for="image_style_type_{{$k + 1}}">@lang('client_site.image_style') </label>  <br>
                                                        <label for="image_square_{{$k + 1}}">
                                                            <input type="radio" class="image_style_type" name="image_style_{{$k + 1}}" value="S" id="image_square_{{$k + 1}}" data-id="{{$k + 1}}" {{ @$content->image_style == 'S' ? 'checked' : '' }}>
                                                            @lang('client_site.square')
                                                        </label>
                                                        <label for="image_circle_{{$k + 1}}">
                                                            <input type="radio" class="image_style_type" name="image_style_{{$k + 1}}" value="C" id="image_circle_{{$k + 1}}" data-id="{{$k + 1}}" {{ @$content->image_style == 'C' ? 'checked' : '' }}>
                                                            @lang('client_site.circle')
                                                        </label>
                                                    </div>
                                                    <div class="cert_cntnt" id="content_image_div_{{$k + 1}}" @if(@$content->content_file_type == 'Y') style="display: none;" @endif >
                                                        <label for="content_file_{{$k + 1}}">@lang('site.upload_picture')</label>  <br>
                                                        <input type="file" name="content_file_{{$k + 1}}" id="content_file_{{$k + 1}}" class="form-control content_file_inp" data-id="{{$k + 1}}" accept="image/*">
                                                        <label class="text-secondary">@lang('client_site.recommended_size') 800 * 800</label>
                                                    </div>
                                                    <div class="cert_cntnt" id="content_youtube_div_{{$k + 1}}" @if(@$content->content_file_type != 'Y') style="display: none;" @endif >
                                                        <label for="content_file_youtube_link_{{$k + 1}}">Youtube Link</label>  <br>
                                                        <input type="text" name="content_file_youtube_link_{{$k + 1}}" id="content_file_youtube_link_{{$k + 1}}" class="form-control required content_file_youtube" @if(@$content->content_file_type == 'Y') value="https://www.youtube.com/embed/{{@$content->content_file}}" @endif data-id="{{$k + 1}}" >
                                                    </div>
                                                    <div class="cert_cntnt">
                                                        <label for="content_heading_{{$k + 1}}">@lang('site.title')</label>  <br>
                                                        <input type="text" name="content_heading_{{$k + 1}}" id="content_heading_{{$k + 1}}" class="content_heading_inp form-control required" data-id="{{$k + 1}}" value="{{ @$content->content_heading ?? @$temp->body_single_content_heading }}">
                                                    </div>
                                                    <div class="cert_cntnt">
                                                        <label for="content_desc_{{$k + 1}}">@lang('site.description')</label>  <br>
                                                        <!-- <input type="text" name="content_desc_{{$k + 1}}" id="content_desc_{{$k + 1}}" class="content_desc_inp form-control required" data-id="{{$k + 1}}" value="{{ @$content->content_desc ?? @$temp->body_single_content_desc }}"> -->
                                                        <textarea name="content_desc_{{$k + 1}}" id="content_desc_{{$k + 1}}" class="content_desc_inp form-control required" data-id="{{$k + 1}}">{{ @$content->content_desc ?? @$temp->body_single_content_desc }}</textarea>
                                                    </div>
                                                    <div class="cert_cntnt">
                                                        <label for="content_button_{{$k + 1}}">@lang('site.button')</label>  <br>
                                                        <label for="content_button_yes_{{$k + 1}}">
                                                            <input type="radio" class="content_button" name="content_button_{{$k + 1}}" value="Y" id="content_button_yes_{{$k + 1}}" data-id="{{$k + 1}}" {{ @$content->content_button == 'Y' ? 'checked' : '' }}>
                                                            @lang('site.yes')
                                                        </label>
                                                        <label for="content_button_no_{{$k + 1}}">
                                                            <input type="radio" class="content_button" name="content_button_{{$k + 1}}" value="N" id="content_button_no_{{$k + 1}}" data-id="{{$k + 1}}" {{ @$content->content_button == 'N' ? 'checked' : '' }}>
                                                            @lang('site.no')
                                                        </label>
                                                    </div>
                                                    <div id="content_button_div_{{$k + 1}}" @if(@$content->content_button == 'N') style="display: none;" @endif >
                                                        <div class="cert_cntnt">
                                                            <label for="content_button_text_{{$k + 1}}">@lang('site.button_caption')</label>  <br>
                                                            <input type="text" name="content_button_text_{{$k + 1}}" id="content_button_text_{{$k + 1}}" class="content_button_text_inp form-control required" data-id="{{$k + 1}}" value="{{ @$content->content_button_text ?? @$temp->body_single_content_button_text }}">
                                                        </div>
                                                        <div class="cert_cntnt">
                                                            <label for="content_button_link_{{$k + 1}}">@lang('site.button') Link</label>  <br>
                                                            <input type="url" name="content_button_link_{{$k + 1}}" id="content_button_link_{{$k + 1}}" class="content_button_link_inp form-control required" data-id="{{$k + 1}}" value="{{ @$content->content_button_link }}">
                                                        </div>
                                                    </div>
                                                    <hr/>
                                                </div>
                                            @endforeach
                                        @else
                                            <div class="multicontent" id="multicontent_1" style="padding: 20px 0px;" data-id="1" data-saved="false">
                                                <div class="multi_section_head">
                                                    <h5>@lang('client_site.section') 1</h5>
                                                    <!-- <a href="javascript:;" class="btn del_multicontent" data-id="1"> <i class="fa fa-times" aria-hidden="true"></i> </a> -->
                                                </div>
                                                <div class="cert_cntnt">
                                                    <label for="content_file_type_1">@lang('site.content_file')</label>  <br>
                                                    <label for="content_image_1">
                                                        <input type="radio" class="content_file_type" name="content_file_1" value="I" id="content_image_1" data-id="1" checked >
                                                        @lang('site.upload_picture')
                                                    </label>
                                                    <label for="content_youtube_1">
                                                        <input type="radio" class="content_file_type" name="content_file_1" value="Y" id="content_youtube_1" data-id="1">
                                                        Youtube Link
                                                    </label>
                                                </div>
                                                <div class="cert_cntnt"id="image_style_type_div_1">
                                                    <label for="image_style_type_1">@lang('client_site.image_style') </label>  <br>
                                                    <label for="image_square_1">
                                                        <input type="radio" class="image_style_type" name="image_style_1" value="S" id="image_square_1" data-id="1" @if(@$temp->id==3 || @$temp->id==4) checked @endif>
                                                        @lang('client_site.square')
                                                    </label>
                                                    <label for="image_circle_1">
                                                        <input type="radio" class="image_style_type" name="image_style_1" value="C" id="image_circle_1" data-id="1" @if(@$temp->id==1 || @$temp->id==2) checked @endif>
                                                        @lang('client_site.circle')
                                                    </label>
                                                </div>
                                                <div class="cert_cntnt" id="content_image_div_1">
                                                    <label for="content_file_1">@lang('site.upload_picture')</label>  <br>
                                                    <input type="file" name="content_file_1" id="content_file_1" class="form-control content_file_inp" data-id="1" accept="image/*">
                                                    <label class="text-secondary">@lang('client_site.recommended_size') 800 * 800</label>
                                                </div>
                                                <div class="cert_cntnt" id="content_youtube_div_1" style="display: none;">
                                                    <label for="content_file_youtube_link_1">Youtube Link</label>  <br>
                                                    <input type="text" name="content_file_youtube_link_1" id="content_file_youtube_link_1" class="form-control required content_file_youtube" data-id="1" >
                                                </div>
                                                <div class="cert_cntnt">
                                                    <label for="content_heading_1">@lang('site.title')</label>  <br>
                                                    <input type="text" name="content_heading_1" id="content_heading_1" class="content_heading_inp form-control required" data-id="1" value="{{ @$temp->body_single_content_heading }}">
                                                </div>
                                                <div class="cert_cntnt">
                                                    <label for="content_desc_1">@lang('site.description')</label>  <br>
                                                    <!-- <input type="text" name="content_desc_1" id="content_desc_1" class="content_desc_inp form-control required" data-id="1" value="Lorem ipsum dolor sit amet, adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."> -->
                                                    <textarea name="content_desc_1" id="content_desc_1" class="content_desc_inp form-control required" data-id="1">Lorem ipsum dolor sit amet, adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</textarea>
                                                </div>
                                                <div class="cert_cntnt">
                                                    <label for="content_button_1">@lang('site.button')</label>  <br>
                                                    <label for="content_button_yes_1">
                                                        <input type="radio" class="content_button" name="content_button_1" value="Y" id="content_button_yes_1" data-id="1" checked>
                                                        @lang('site.yes')
                                                    </label>
                                                    <label for="content_button_no_1">
                                                        <input type="radio" class="content_button" name="content_button_1" value="N" id="content_button_no_1" data-id="1">
                                                        @lang('site.no')
                                                    </label>
                                                </div>
                                                <div id="content_button_div_1">
                                                    <div class="cert_cntnt">
                                                        <label for="content_button_text_1">@lang('site.button_caption')</label>  <br>
                                                        <input type="text" name="content_button_text_1" id="content_button_text_1" class="content_button_text_inp form-control required" data-id="1" value="{{ @$temp->body_single_content_button_text }}">
                                                    </div>
                                                    <div class="cert_cntnt">
                                                        <label for="content_button_link_1">@lang('site.button') Link</label>  <br>
                                                        <input type="url" name="content_button_link_1" id="content_button_link_1" class="content_button_link_inp form-control required" data-id="1" value="">
                                                    </div>
                                                </div>
                                                <hr/>
                                            </div>
                                            <div class="multicontent" id="multicontent_2" style="padding: 20px 0px;" data-id="2" data-saved="false">
                                                <div class="multi_section_head">
                                                    <h5>@lang('client_site.section') 2</h5>
                                                    <!-- <a href="javascript:;" class="btn del_multicontent" data-id="2"> <i class="fa fa-times" aria-hidden="true"></i> </a> -->
                                                </div>
                                                <div class="cert_cntnt">
                                                    <label for="content_file_type_2">@lang('site.content_file')</label>  <br>
                                                    <label for="content_image_2">
                                                        <input type="radio" class="content_file_type" name="content_file_2" value="I" id="content_image_2" data-id="2" checked >
                                                        @lang('site.upload_picture')
                                                    </label>
                                                    <label for="content_youtube_2">
                                                        <input type="radio" class="content_file_type" name="content_file_2" value="Y" id="content_youtube_2" data-id="2">
                                                        Youtube Link
                                                    </label>
                                                </div>
                                                <div class="cert_cntnt" id="image_style_type_div_2">
                                                    <label for="image_style_type_2">@lang('client_site.image_style') </label>  <br>
                                                    <label for="image_square_2">
                                                        <input type="radio" class="image_style_type" name="image_style_2" value="S" id="image_square_2" data-id="2" @if(@$temp->id==3 || @$temp->id==4) checked @endif>
                                                        @lang('client_site.square')
                                                    </label>
                                                    <label for="image_circle_2">
                                                        <input type="radio" class="image_style_type" name="image_style_2" value="C" id="image_circle_2" data-id="2" @if(@$temp->id==1 || @$temp->id==2) checked @endif>
                                                        @lang('client_site.circle')
                                                    </label>
                                                </div>
                                                <div class="cert_cntnt" id="content_image_div_2">
                                                    <label for="content_file_2">@lang('site.upload_picture')</label>  <br>
                                                    <input type="file" name="content_file_2" id="content_file_2" class="form-control content_file_inp" data-id="2" accept="image/*">
                                                    <label class="text-secondary">@lang('client_site.recommended_size') 800 * 800</label>
                                                </div>
                                                <div class="cert_cntnt" id="content_youtube_div_2" style="display: none;">
                                                    <label for="content_file_youtube_link_2">Youtube Link</label>  <br>
                                                    <input type="text" name="content_file_youtube_link_2" id="content_file_youtube_link_2" class="form-control required content_file_youtube" data-id="2" >
                                                </div>
                                                <div class="cert_cntnt">
                                                    <label for="content_heading_2">@lang('site.title')</label>  <br>
                                                    <input type="text" name="content_heading_2" id="content_heading_2" class="content_heading_inp form-control required" data-id="2" value="{{ @$temp->body_single_content_heading }}">
                                                </div>
                                                <div class="cert_cntnt">
                                                    <label for="content_desc_2">@lang('site.description')</label>  <br>
                                                    <!-- <input type="text" name="content_desc_2" id="content_desc_2" class="content_desc_inp form-control required" data-id="2" value="Lorem ipsum dolor sit amet, adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."> -->
                                                    <textarea name="content_desc_2" id="content_desc_2" class="content_desc_inp form-control required" data-id="2">Lorem ipsum dolor sit amet, adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</textarea>
                                                </div>
                                                <div class="cert_cntnt">
                                                    <label for="content_button_2">@lang('site.button')</label>  <br>
                                                    <label for="content_button_yes_2">
                                                        <input type="radio" class="content_button" name="content_button_2" value="Y" id="content_button_yes_2" data-id="2">
                                                        @lang('site.yes')
                                                    </label>
                                                    <label for="content_button_no_2">
                                                        <input type="radio" class="content_button" name="content_button_2" value="N" id="content_button_no_2" data-id="2" checked>
                                                        @lang('site.no')
                                                    </label>
                                                </div>
                                                <div id="content_button_div_2" style="display: none;">
                                                    <div class="cert_cntnt">
                                                        <label for="content_button_text_2">@lang('site.button_caption')</label>  <br>
                                                        <input type="text" name="content_button_text_2" id="content_button_text_2" class="content_button_text form-control required" data-id="2" value="{{ @$temp->body_single_content_button_text }}">
                                                    </div>
                                                    <div class="cert_cntnt">
                                                        <label for="content_button_link_2">@lang('site.button') Link</label>  <br>
                                                        <input type="text" name="content_button_link_2" id="content_button_link_2" class="content_button_link form-control required" data-id="2" value="{{ @$detail->content_button_link_2 }}">
                                                    </div>
                                                </div>
                                                <hr/>
                                            </div>
                                        @endif
                                    </div>
                                    <div id="add_btn">
                                        <a href="javascript:;" class="btn btn-primary" id="add_mult_cntnt">+ @lang('client_site.add_new_content')</a>
                                    </div>
                                </form>
                            </div>
                            <div class="tab_target" id="single" style="display: none;">
                                <form action="{{ route('store.landing.page.template') }}" id="mySingleVideoForm">
                                    <div class="cert_cntnt">
                                        <label for="single_video">@lang('client_site.show_single_video')</label>  <br>
                                        <label for="single_video_yes">
                                            <input type="radio" name="single_video" value="Y" id="single_video_yes" @if(@$detail->body_single_video == 'Y') checked @endif>
                                            @lang('site.yes')
                                        </label>
                                        <label for="single_video_no">
                                            <input type="radio" name="single_video" value="N" id="single_video_no"  @if(@$detail->body_single_video == 'N') checked @endif>
                                            @lang('site.no')
                                        </label>
                                    </div>
                                    <div id="single_video_div" @if(@$detail->body_single_video == 'N') style="display: none;" @endif >
                                        <div class="cert_cntnt">
                                            <label for="single_file">@lang('site.upload_file')</label>
                                            <input type="file" name="single_file" id="single_file" class="form-control">
                                            <label class="text-secondary">@lang('client_site.recommended_size') 1920 * 1080</label>
                                        </div>
                                        <div class="cert_cntnt">
                                            <label for="body_single_video_heading">@lang('site.title')</label>
                                            <input type="text" name="body_single_video_heading" id="body_single_video_heading" class="form-control required" value="{{ @$detail->body_single_video_heading ?? 'Get product more information from the video' }}">
                                        </div>
                                        <div class="cert_cntnt">
                                            <label for="body_single_video_desc">@lang('site.description')</label>
                                            <!-- <input type="text" name="body_single_video_desc" id="body_single_video_desc" class="form-control required" value="{{ @$detail->body_single_video_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.' }}"> -->
                                            <textarea name="body_single_video_desc" id="body_single_video_desc" class="form-control required">{{ @$detail->body_single_video_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.' }}</textarea>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab_target" id="faqs" style="display: none;">
                                <div class="cert_cntnt">
                                    <label for="show_faq">@lang('client_site.show_faq')</label>  <br>
                                    <label for="show_faq_yes">
                                        <input type="radio" name="show_faq" value="Y" id="show_faq_yes" @if(@$detail->show_faq == 'Y') checked @endif>
                                        @lang('site.yes')
                                    </label>
                                    <label for="show_faq_no">
                                        <input type="radio" name="show_faq" value="N" id="show_faq_no" @if(@$detail->show_faq == 'N') checked @endif>
                                        @lang('site.no')
                                    </label>
                                </div>
                                <div id="show_faq_div"  @if(@$detail->show_faq == 'N') style="display: none;" @endif >
                                    <form action="{{ route('store.landing.page.template') }}" id="myFaqHeadForm">
                                        <div class="cert_cntnt">
                                            <label for="faq_main_title">@lang('client_site.faq_section_title')</label>
                                            <input type="text" name="faq_main_title" id="faq_main_title" class="required form-control" value="{{@$detail->faq_heading ?? 'FAQ'}}">
                                        </div>
                                        <div class="cert_cntnt">
                                            <label for="faq_main_desc">@lang('client_site.faq_section_description')</label>
                                            <input type="text" name="faq_main_desc" id="faq_main_desc" class="required form-control" value="{{@$detail->faq_description ?? 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.'}}">
                                            <a href="javascript:;" class="btn btn-primary mt-3 pull-right" id="save_faq_head">+ @lang('client_site.save')</a>
                                        </div>
                                    </form>
                                    <div class="clearfix"></div>
                                    <hr/>
                                    @if(@$detail->getFaq && count(@$detail->getFaq) > 0)
                                    <div id="main">
                                        <div class="accordion1 faq_div" id="faq">
                                            @foreach(@$detail->getFaq as $k=>$row)
                                            <div class="card faq_section" id="faq_section_{{@$k + 1}}" data-identity="{{@$row->id}}">
                                                <form action="{{ route('store.landing.page.template') }}" id="myFaqForm{{@$k + 1}}">
                                                    <div class="card-header d-flex" id="faqhead{{@$k + 1}}" data-id="{{@$k + 1}}">
                                                        <a href="javascript:;" class="btn btn-header-link acco-chap rem_tick collapsed"
                                                            data-toggle="collapse"
                                                            data-target="#faq{{@$k + 1}}"
                                                            aria-expanded="true"
                                                            aria-controls="faq{{@$k + 1}}"
                                                        >
                                                            <img src="{{ URL::to('public/frontend/images/cha.png') }}">
                                                            <h5>@lang('client_site.section') {{@$k + 1}}</h5>
                                                            <a href="javascript:;" class="btn del_faq" data-id="{{@$k + 1}}" data-identity="{{@$row->id}}"> <i class="fa fa-times" aria-hidden="true"></i> </a>
                                                        </a>
                                                    </div>
                                                    <div id="faq{{@$k + 1}}" class="collapse" aria-labelledby="faqhead{{@$k + 1}}" data-parent="#faq">
                                                        <div class="card-body">
                                                            <div class="faq_detail" id="faq_{{@$k + 1}}">
                                                                <div class="cert_cntnt">
                                                                    <label for="faq_title_{{@$k + 1}}">@lang('site.title')</label>
                                                                    <input type="text" name="faq_title_{{@$k + 1}}" id="faq_title_{{@$k + 1}}" class="form-control required faq_title" data-id="{{@$k + 1}}" value="{{@$row->title}}">
                                                                </div>
                                                                <div class="cert_cntnt mb-3">
                                                                    <label for="faq_desc_{{@$k + 1}} color_primary_light">@lang('site.description')</label>
                                                                    <textarea name="faq_desc_{{@$k + 1}}" id="faq_desc_{{@$k + 1}}" class="form-control required faq_desc" data-id="{{@$k + 1}}">{{@$row->description}}</textarea>
                                                                </div>
                                                                <div class="sect_bottom_div">
                                                                    <p style="visibility: hidden;">
                                                                        <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                                                        @lang('client_site.saved_successfully')
                                                                    </p>
                                                                    <a href="javascript:;" class="faq_save btn btn-primary pull-right mb-3" id="faq_save_{{@$k + 1}}" data-id="{{@$k + 1}}" data-identity="{{@$row->id}}">@lang('client_site.save')</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    @else
                                    <div id="main">
                                        <div class="accordion1 faq_div" id="faq">
                                            <div class="card faq_section" id="faq_section_1" data-identity="1">
                                                <form action="{{ route('store.landing.page.template') }}" id="myFaqForm1">
                                                    <div class="card-header d-flex" id="faqhead1" data-id="1">
                                                        <a href="javascript:;" class="btn btn-header-link acco-chap rem_tick collapsed"
                                                            data-toggle="collapse"
                                                            data-target="#faq1"
                                                            aria-expanded="true"
                                                            aria-controls="faq1"
                                                        >
                                                            <img src="{{ URL::to('public/frontend/images/cha.png') }}">
                                                            <h5>@lang('client_site.section') 1</h5>
                                                            <a href="javascript:;" class="btn del_faq" data-id="1" data-identity="{{@$row->id}}"> <i class="fa fa-times" aria-hidden="true"></i> </a>
                                                        </a>
                                                    </div>
                                                    <div id="faq1" class="collapse" aria-labelledby="faqhead1" data-parent="#faq">
                                                        <div class="card-body">
                                                            <div class="faq_detail" id="faq_1">
                                                                <div class="cert_cntnt">
                                                                    <label for="faq_title_1">@lang('site.title')</label>
                                                                    <input type="text" name="faq_title_1" id="faq_title_1" class="form-control required faq_title" data-id="1" value="This is a simply dummy question text show here?">
                                                                </div>
                                                                <div class="cert_cntnt mb-3">
                                                                    <label for="faq_desc_1">@lang('site.description')</label>
                                                                    <textarea name="faq_desc_1" id="faq_desc_1" class="form-control required faq_desc" data-id="1">Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur mi gravida ac. Nunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet ante vitae ante facilisis lobortis temporamet ante vitae ante facilisis lobortis sit amet consectetur adipiscfiing. Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur gravidaNunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet ante vitae ante.</textarea>
                                                                </div>
                                                                <div class="sect_bottom_div">
                                                                    <p style="visibility: hidden;">
                                                                        <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                                                        @lang('client_site.saved_successfully')
                                                                    </p>
                                                                    <a href="javascript:;" class="faq_save btn btn-primary pull-right mb-3" id="faq_save_1" data-id="1" data-identity="0">@lang('client_site.save')</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    <div>
                                        <a href="javascript:;" class="btn btn-primary" id="add_faq">+ @lang('client_site.add_new_faq')</a>
                                    </div>
                                </div>
                            </div>
                            <div class="tab_target" id="products" style="display: none;">
                                <form action="{{ route('store.landing.page.template') }}" id="myProductHeadForm">
                                    <div class="cert_cntnt">
                                        <label for="product_section_heading">@lang('site.title')</label>
                                        <input type="text" name="product_section_heading" id="product_section_heading" class="form-control required" value="{{ @$detail->product_section_heading ?? 'Our Products' }}">
                                    </div>
                                    <div class="cert_cntnt">
                                        <label for="product_section_desc">@lang('site.description')</label>
                                        <textarea name="product_section_desc" id="product_section_desc" class="form-control required">{{ @$detail->product_section_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.' }}</textarea>
                                    </div>
                                    <div class="cert_cntnt">
                                        <label for="product_slider">@lang('client_site.product_slider')</label>  <br>
                                        <label for="product_slider_yes">
                                            <input type="radio" name="product_slider" value="Y" id="product_slider_yes" @if(@$detail->product_slider == 'Y') checked @endif>
                                            @lang('site.yes')
                                        </label>
                                        <label for="product_slider_no">
                                            <input type="radio" name="product_slider" value="N" id="product_slider_no" @if(@$detail->product_slider == 'N') checked @endif>
                                            @lang('site.no')
                                        </label>
                                        <a href="javascript:;" class="btn btn-primary mt-3 pull-right" id="save_product_head">+ @lang('client_site.save')</a>
                                    </div>
                                </form>
                                <div class="clearfix"></div>
                                <hr/>
                                <div id="main">
                                    @if(@$detail->getProductDetails && count(@$detail->getProductDetails)>0)
                                        <div class="accordion1 products_accordion" id="faq">
                                            @foreach(@$detail->getProductDetails as $k=>$product)
                                            <!-- <div class="list-item">
                                                <div class="item-content">
                                                    <span class="order">{{ @$k + 1 }}</span> -->
                                            <div class="card product_card" id="product_card_{{ @$k + 1 }}" data-identity="{{@$product->id}}">
                                                <form action="{{ route('store.landing.page.template') }}" id="myProductForm{{ @$k + 1 }}">
                                                    <div class="card-header d-flex" id="faqhead{{ @$k + 1 }}" data-id="{{ @$k + 1 }}">
                                                        <a href="javascript:;" class="btn btn-header-link acco-chap rem_tick collapsed"
                                                            data-toggle="collapse"
                                                            data-target="#faq{{ @$k + 1 }}"
                                                            aria-expanded="true"
                                                            aria-controls="faq{{ @$k + 1 }}"
                                                        >
                                                            <img src="{{ URL::to('public/frontend/images/cha.png') }}">
                                                            <h5>{{ @$product->product_heading ?? 'Product'. @$k + 1 }}</h5>
                                                            <a href="javascript:;" class="btn del_prod" data-id="{{ @$k + 1 }}"> <i class="fa fa-times" aria-hidden="true"></i> </a>
                                                        </a>
                                                    </div>
                                                    <div id="faq{{ @$k + 1 }}" class="collapse" aria-labelledby="faqhead{{ @$k + 1 }}" data-parent="#faq">
                                                        <div class="card-body">
                                                            <div class="product_detail" id="product_{{ @$k + 1 }}">
                                                                <div class="cert_cntnt">
                                                                    <label for="product_file_type_{{ @$k + 1 }}">@lang('client_site.select_file_type')</label>  <br>
                                                                    <label for="product_file_type_none_{{ @$k + 1 }}">
                                                                        <input type="radio" name="product_file_type_{{ @$k + 1 }}" value="N" id="product_file_type_none_{{ @$k + 1 }}" class="product_file_type_inp" data-id="{{ @$k + 1 }}" @if(@$product->product_file_type == 'N') checked @endif >
                                                                        @lang('client_site.no_file')
                                                                    </label>
                                                                    <label for="product_file_type_image_{{ @$k + 1 }}">
                                                                        <input type="radio" name="product_file_type_{{ @$k + 1 }}" value="I" id="product_file_type_image_{{ @$k + 1 }}" class="product_file_type_inp" data-id="{{ @$k + 1 }}" @if(@$product->product_file_type == 'I') checked @endif >
                                                                        @lang('site.image')
                                                                    </label>
                                                                    <label for="product_file_type_youtube_{{ @$k + 1 }}">
                                                                        <input type="radio" name="product_file_type_{{ @$k + 1 }}" value="Y" id="product_file_type_youtube_{{ @$k + 1 }}" class="product_file_type_inp" data-id="{{ @$k + 1 }}" @if(@$product->product_file_type == 'Y') checked @endif >
                                                                        Youtube Video
                                                                    </label>
                                                                    <!-- <div class="product_file_inp_group"  @if(@$product->product_file_type != 'I') style="display: none;" @endif >
                                                                        <input type="file" name="product_file_{{ @$k + 1 }}" id="product_file_{{ @$k + 1 }}" class="product_file_inp form-control" data-id="{{ @$k + 1 }}" accept="image/*">
                                                                        <span><i class="fa fa-trash del_prod_img" id="del_prod_img_{{ @$k + 1 }}" data-id="{{ @$k + 1 }}" data-identity="{{@$product->id}}" aria-hidden="true" @if(@$product->product_file) @else style="display: none;" @endif></i></span>
                                                                    </div> -->
                                                                    <div class="product_file_inp_group" id="prod_image_{{ @$k + 1 }}" @if(@$product->product_file_type != 'I') style="display: none;" @endif >
                                                                        <input type="file" name="product_file_{{ @$k + 1 }}" id="product_file_{{ @$k + 1 }}" class="product_file_inp form-control" data-id="{{ @$k + 1 }}" accept="image/*">
                                                                        {{-- <span><i class="fa fa-trash del_prod_img" id="del_prod_img_{{ @$k + 1 }}" data-id="{{ @$k + 1 }}" data-identity="0" aria-hidden="true" style="display:none;"></i></span> --}}
                                                                        <label class="text-secondary">@lang('client_site.recommended_size') 1280 * 720</label>
                                                                    </div>
                                                                    <div class="product_file_inp_group" id="prod_youtube_{{ @$k + 1 }}" @if(@$product->product_file_type != 'Y') style="display: none;" @endif >
                                                                        <input type="url" name="product_youtube_{{ @$k + 1 }}" id="product_youtube_{{ @$k + 1 }}" class="product_youtube_inp required form-control" data-id="{{ @$k + 1 }}" placeholder="Youtube Video" @if(@$product->product_file_type == 'Y') value="https://youtu.be/{{@$product->product_file}}" @endif >
                                                                    </div>
                                                                </div>
                                                                <div class="cert_cntnt">
                                                                    <label for="product_heading_{{ @$k + 1 }}">@lang('site.title')</label>
                                                                    <input type="text" name="product_heading_{{ @$k + 1 }}" id="product_heading_{{ @$k + 1 }}" class="product_heading_inp form-control required" data-id="{{ @$k + 1 }}" value="{{ @$product->product_heading ?? 'Product'.@$k + 1 }}">
                                                                </div>
                                                                <div class="cert_cntnt">
                                                                    <label for="product_desc_{{ @$k + 1 }}">@lang('site.description')</label>
                                                                    <textarea name="product_desc_{{ @$k + 1 }}" id="product_desc_{{ @$k + 1 }}" class="product_desc_inp form-control required" data-id="{{ @$k + 1 }}">{{ @$product->product_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipiscing elit' }}</textarea>
                                                                </div>
                                                                <div class="cert_cntnt">
                                                                    <label for="product_price_show_{{ @$k + 1 }}">@lang('client_site.show_product_price')</label>  <br>
                                                                    <label for="product_price_show_yes_{{ @$k + 1 }}">
                                                                        <input type="radio" name="product_price_show_{{ @$k + 1 }}" value="Y" id="product_price_show_yes_{{ @$k + 1 }}" class="product_price_show_inp" data-id="{{ @$k + 1 }}" @if(@$product->product_price_show == 'Y') checked @endif>
                                                                        @lang('site.yes')
                                                                    </label>
                                                                    <label for="product_price_show_no_{{ @$k + 1 }}">
                                                                        <input type="radio" name="product_price_show_{{ @$k + 1 }}" value="N" id="product_price_show_no_{{ @$k + 1 }}" class="product_price_show_inp" data-id="{{ @$k + 1 }}" @if(@$product->product_price_show == 'N') checked @endif>
                                                                        @lang('site.no')
                                                                    </label>
                                                                </div>
                                                                <div class="cert_cntnt price_div_{{ @$k + 1 }}" @if(@$product->product_price_show == 'N') style="display: none;" @endif >
                                                                    <label for="product_price_{{ @$k + 1 }}">@lang('site.price')</label>
                                                                    <input type="text" name="product_price_{{ @$k + 1 }}" id="product_price_{{ @$k + 1 }}" class="product_price_inp form-control required" data-id="{{ @$k + 1 }}" value="{{ @$product->product_price ? ''.@$product->product_price : '800' }}">
                                                                </div>
                                                                <div class="cert_cntnt">
                                                                    <label for="product_button_{{ @$k + 1 }}">@lang('client_site.product_button')</label>  <br>
                                                                    <label for="product_button_yes_{{ @$k + 1 }}">
                                                                        <input type="radio" name="product_button_{{ @$k + 1 }}" value="Y" id="product_button_yes_{{ @$k + 1 }}" class="product_button_inp" data-id="{{ @$k + 1 }}" @if(@$product->product_button == 'Y') checked @endif>
                                                                        @lang('site.yes')
                                                                    </label>
                                                                    <label for="product_button_no_{{ @$k + 1 }}">
                                                                        <input type="radio" name="product_button_{{ @$k + 1 }}" value="N" id="product_button_no_{{ @$k + 1 }}" class="product_button_inp" data-id="{{ @$k + 1 }}" @if(@$product->product_button == 'N') checked @endif>
                                                                        @lang('site.no')
                                                                    </label>
                                                                </div>
                                                                <div id="product_button_div_{{ @$k + 1 }}" @if(@$product->product_button == 'N') style="display: none;" @endif >
                                                                    <div class="cert_cntnt">
                                                                        <label for="product_button_caption_{{ @$k + 1 }}">@lang('site.button_caption')</label>
                                                                        <input type="text" name="product_button_caption_{{ @$k + 1 }}" id="product_button_caption_{{ @$k + 1 }}" class="product_button_caption_inp form-control required" data-id="{{ @$k + 1 }}" value="{{ @$product->product_button_caption ?? 'More' }}">
                                                                    </div>
                                                                    <div class="cert_cntnt">
                                                                        <label for="product_button_url_{{ @$k + 1 }}">@lang('site.button') Link</label>
                                                                        <input type="url" name="product_button_url_{{ @$k + 1 }}" id="product_button_url_{{ @$k + 1 }}" class="product_button_url_inp form-control required" data-id="{{ @$k + 1 }}" value="{{ @$product->product_button_url }}">
                                                                    </div>
                                                                </div>
                                                                <div class="cert_cntnt">
                                                                    <label for="product_reverse_counter_{{ @$k + 1 }}">@lang('client_site.product_reverse_counter')</label>  <br>
                                                                    <label for="product_reverse_counter_yes_{{ @$k + 1 }}">
                                                                        <input type="radio" name="product_reverse_counter_{{ @$k + 1 }}" value="Y" id="product_reverse_counter_yes_{{ @$k + 1 }}" class="product_expires_on" data-id="{{ @$k + 1 }}" @if(@$product->product_reverse_counter == 'Y') checked @endif>
                                                                        @lang('site.yes')
                                                                    </label>
                                                                    <label for="product_reverse_counter_no_{{ @$k + 1 }}">
                                                                        <input type="radio" name="product_reverse_counter_{{ @$k + 1 }}" value="N" id="product_reverse_counter_no_{{ @$k + 1 }}" class="product_expires_on" data-id="{{ @$k + 1 }}" @if(@$product->product_reverse_counter == 'N') checked @endif>
                                                                        @lang('site.no')
                                                                    </label>
                                                                </div>
                                                                <div class="cert_cntnt" id="product_reverse_counter_div_{{ @$k + 1 }}" @if(@$product->product_reverse_counter == 'N') style="display:none;" @endif>
                                                                    <label for="product_exp_date_{{ @$k + 1 }}">@lang('client_site.expires_on')</label>
                                                                    <input type="text" name="product_exp_date_{{ @$k + 1 }}" id="product_exp_date_{{ @$k + 1 }}" class="product_exp_date_inp form-control required" data-id="{{ @$k + 1 }}" value="{{ @$product->product_exp_date ?? date('Y-m-d', strtotime('+1 day')) }}" autocomplete="off" readonly >
                                                                </div>
                                                                <div class="sect_bottom_div">
                                                                    <p style="visibility: hidden;">
                                                                        <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                                                        @lang('client_site.saved_successfully')
                                                                    </p>
                                                                    <a href="javascript:;" class="btn btn-primary my-3 pull-right product_save" id="product_save_{{ @$k + 1 }}" data-id="{{ @$k + 1 }}">@lang('site.completa')</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            <!-- </div>
                                                </div> -->
                                            </div>
                                            @endforeach
                                        </div>
                                    @else
                                        <div class="accordion1 products_accordion" id="faq">
                                            <!-- <div class="list-item">
                                                <div class="item-content">
                                                    <span class="order">1</span> -->
                                            <div class="card product_card" id="product_card_1" data-identity="0">
                                                <form action="{{ route('store.landing.page.template') }}" id="myProductForm1">
                                                    <div class="card-header d-flex" id="faqhead1" data-id="1">
                                                        <a href="javascript:;" class="btn btn-header-link acco-chap rem_tick collapsed"
                                                            data-toggle="collapse"
                                                            data-target="#faq1"
                                                            aria-expanded="true"
                                                            aria-controls="faq1"
                                                        >
                                                            <img src="{{ URL::to('public/frontend/images/cha.png') }}">
                                                            <h5>New Product</h5>
                                                            <a href="javascript:;" class="btn del_prod" data-id="1"> <i class="fa fa-times" aria-hidden="true"></i> </a>
                                                        </a>
                                                    </div>
                                                    <div id="faq1" class="collapse" aria-labelledby="faqhead1" data-parent="#faq">
                                                        <div class="card-body">
                                                            <div class="product_detail" id="product_1">
                                                                <div class="cert_cntnt">
                                                                    <label for="product_file_type_1">@lang('client_site.select_file_type')</label>  <br>
                                                                    <label for="product_file_type_none_1">
                                                                        <input type="radio" name="product_file_type_1" value="N" id="product_file_type_none_1" class="product_file_type_inp" data-id="1" checked >
                                                                        @lang('client_site.no_file')
                                                                    </label>
                                                                    <label for="product_file_type_image_1">
                                                                        <input type="radio" name="product_file_type_1" value="I" id="product_file_type_image_1" class="product_file_type_inp" data-id="1" >
                                                                        @lang('site.image')
                                                                    </label>
                                                                    <label for="product_file_type_youtube_1">
                                                                        <input type="radio" name="product_file_type_1" value="Y" id="product_file_type_youtube_1" class="product_file_type_inp" data-id="1" >
                                                                        Youtube Video
                                                                    </label>
                                                                    <div class="product_file_inp_group" id="prod_image_1" style="display: none;">
                                                                        <input type="file" name="product_file_1" id="product_file_1" class="product_file_inp form-control" data-id="1" accept="image/*">
                                                                        {{-- <span><i class="fa fa-trash del_prod_img" id="del_prod_img_1" data-id="1" data-identity="0" aria-hidden="true"></i></span> --}}
                                                                        <label class="text-secondary">@lang('client_site.recommended_size') 1280 * 720</label>
                                                                    </div>
                                                                    <div class="product_file_inp_group" id="prod_youtube_1" style="display: none;">
                                                                        <input type="url" name="product_youtube_1" id="product_youtube_1" class="product_youtube_inp required form-control" data-id="1" placeholder="Youtube Video">
                                                                    </div>
                                                                </div>
                                                                <div class="cert_cntnt">
                                                                    <label for="product_heading_1">@lang('site.title')</label>
                                                                    <input type="text" name="product_heading_1" id="product_heading_1" class="product_heading_inp form-control required" data-id="1" value="New Product">
                                                                </div>
                                                                <div class="cert_cntnt">
                                                                    <label for="product_desc_1">@lang('site.description')</label>
                                                                    <textarea name="product_desc_1" id="product_desc_1" class="product_desc_inp form-control required" data-id="1">Lorem ipsum dolor sit amet, consectetur adipiscing elit</textarea>
                                                                </div>
                                                                <div class="cert_cntnt">
                                                                    <label for="product_price_show_1">@lang('client_site.show_product_price')</label>  <br>
                                                                    <label for="product_price_show_yes_1">
                                                                        <input type="radio" name="product_price_show_1" value="Y" id="product_price_show_yes_1" class="product_price_show_inp" data-id="1" checked>
                                                                        @lang('site.yes')
                                                                    </label>
                                                                    <label for="product_price_show_no_1">
                                                                        <input type="radio" name="product_price_show_1" value="N" id="product_price_show_no_1" class="product_price_show_inp" data-id="1">
                                                                        @lang('site.no')
                                                                    </label>
                                                                </div>
                                                                <div class="cert_cntnt price_div_1">
                                                                    <label for="product_price_1">@lang('site.price')</label>
                                                                    <input type="text" name="product_price_1" id="product_price_1" class="product_price_inp form-control required" data-id="1" value="800">
                                                                </div>
                                                                <div class="cert_cntnt">
                                                                    <label for="product_button_1">@lang('client_site.product_button')</label>  <br>
                                                                    <label for="product_button_yes_1">
                                                                        <input type="radio" name="product_button_1" value="Y" id="product_button_yes_1" class="product_button_inp" data-id="1" checked >
                                                                        @lang('site.yes')
                                                                    </label>
                                                                    <label for="product_button_no_1">
                                                                        <input type="radio" name="product_button_1" value="N" id="product_button_no_1" class="product_button_inp" data-id="1" >
                                                                        @lang('site.no')
                                                                    </label>
                                                                </div>
                                                                <div id="product_button_div_1">
                                                                    <div class="cert_cntnt">
                                                                        <label for="product_button_caption_1">@lang('site.button_caption')</label>
                                                                        <input type="text" name="product_button_caption_1" id="product_button_caption_1" class="product_button_caption_inp form-control required" data-id="1" value="More">
                                                                    </div>
                                                                    <div class="cert_cntnt">
                                                                        <label for="product_button_url_1">@lang('site.button') Link</label>
                                                                        <input type="url" name="product_button_url_1" id="product_button_url_1" class="product_button_url_inp form-control required" data-id="1">
                                                                    </div>
                                                                </div>
                                                                <div class="cert_cntnt">
                                                                    <label for="product_reverse_counter_1">@lang('client_site.product_reverse_counter')</label>  <br>
                                                                    <label for="product_reverse_counter_yes_1">
                                                                        <input type="radio" name="product_reverse_counter_1" value="Y" id="product_reverse_counter_yes_1" class="product_expires_on" data-id="1" checked >
                                                                        @lang('site.yes')
                                                                    </label>
                                                                    <label for="product_reverse_counter_no_1">
                                                                        <input type="radio" name="product_reverse_counter_1" value="N" id="product_reverse_counter_no_1" class="product_expires_on" data-id="1" >
                                                                        @lang('site.no')
                                                                    </label>
                                                                </div>
                                                                <div class="cert_cntnt" id="product_reverse_counter_div_1">
                                                                    <label for="product_exp_date_1">@lang('client_site.expires_on')</label>
                                                                    <input type="text" name="product_exp_date_1" id="product_exp_date_1" class="product_exp_date_inp form-control required" data-id="1" value="{{ @$product->product_exp_date ?? date('Y-m-d', strtotime('+1 day')) }}" autocomplete="off" readonly >
                                                                </div>
                                                                <div class="sect_bottom_div">
                                                                    <p style="visibility: hidden;">
                                                                        <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                                                        @lang('client_site.saved_successfully')
                                                                    </p>
                                                                    <a href="javascript:;" class="btn btn-primary my-3 pull-right product_save" id="product_save_1" data-id="1">@lang('site.completa')</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                                <!-- </div>
                                            </div> -->
                                        </div>
                                    @endif
                                    <div id="add_btn">
                                        <a href="javascript:;" class="btn btn-primary" id="add_product">+ @lang('client_site.add_new_product')</a>
                                    </div>
                                </div>
                            </div>
                            <div class="tab_target" id="logo" style="display: none;">
                                <form action="{{ route('store.landing.page.template') }}" id="myLogoForm">
                                    <div class="cert_cntnt landing_logo_grp mb-3" @if(@$detail->is_branding_free == 'N' && @$detail->page_type=='P') style="display: none;" @endif >
                                        <label for="landing_logo">@lang('client_site.upload_logo')</label>
                                        <div class="landing_logo_inp_grp">
                                            <input type="file" name="landing_logo" id="landing_logo" class="form-control" accept="image/*">
                                            <span><i class="fa fa-trash" id="del_landing_logo" aria-hidden="true" @if(!@$detail->landing_logo) style="display: none;" @endif></i></span>
                                        </div>
                                        <label class="text-secondary">@lang('client_site.recommended_size') 768 * 768</label>
                                    </div>
                                    <div class="social_grp">
                                        <h5>Links de mídia social</h5>
                                        <div class="cert_cntnt">
                                            <label for="link_linkedin">Link LinkedIn</label>
                                            <input type="url" name="link_linkedin" id="link_linkedin" class="form-control" value="{{ @$detail->link_linkedin }}">
                                        </div>
                                        <div class="cert_cntnt">
                                            <label for="link_facebook">Link Facebook</label>
                                            <input type="url" name="link_facebook" id="link_facebook" class="form-control" value="{{ @$detail->link_facebook }}">
                                        </div>
                                        <div class="cert_cntnt">
                                            <label for="link_twitter">Link Twitter</label>
                                            <input type="url" name="link_twitter" id="link_twitter" class="form-control" value="{{ @$detail->link_twitter }}">
                                        </div>
                                        <div class="cert_cntnt">
                                            <label for="link_instagram">Link Instagram</label>
                                            <input type="url" name="link_instagram" id="link_instagram" class="form-control" value="{{ @$detail->link_instagram }}">
                                        </div>
                                        <div class="cert_cntnt">
                                            <label for="link_pinterest">Link Pinterest</label>
                                            <input type="url" name="link_pinterest" id="link_pinterest" class="form-control" value="{{ @$detail->link_pinterest }}">
                                        </div>
                                        <div class="cert_cntnt">
                                            <label for="link_youtube">Link Youtube</label>
                                            <input type="url" name="link_youtube" id="link_youtube" class="form-control" value="{{ @$detail->link_youtube }}">
                                        </div>
                                        <div class="cert_cntnt">
                                            <label for="link_tiktok">Link Tiktok</label>
                                            <input type="url" name="link_tiktok" id="link_tiktok" class="form-control" value="{{ @$detail->link_tiktok }}">
                                        </div>
                                        <hr/>
                                    </div>
                                </form>
                            </div>
                            <div class="tab_target" id="design" style="display: none;">
                                <form action="{{ route('store.landing.page.template') }}" id="myDesignForm">
                                    <div class="cert_cntnt">
                                        <label for="color_primary">@lang('site.primary_color')</label>
                                        <input type="color" name="color_primary" id="color_primary" class="form-control" value="{{ @$detail->color_primary ?? ( @$temp->color_primary ?? '#000000' ) }}">
                                    </div>
                                    <div class="cert_cntnt">
                                        <label for="color_secondary">@lang('site.secondary_color')</label>
                                        <input type="color" name="color_secondary" id="color_secondary" class="form-control" value="{{ @$detail->color_secondary ?? ( @$temp->color_secondary ?? '#000000' ) }}">
                                    </div>
                                    <div class="cert_cntnt">
                                        <label for="color_tertiary">@lang('site.tertiary_color')</label>
                                        <input type="color" name="color_tertiary" id="color_tertiary" class="form-control" value="{{ @$detail->color_tertiary ?? ( @$temp->color_tertiary ?? '#000000' ) }}">
                                    </div>
                                </form>
                            </div>
						</div>
						{{-- <div class="tree-tip">
							<div class="tip__title"> <img src="{{ URL::to('public/frontend/images/tip.png') }}"> Pro Tip </div>
							<!-- <p class="tip__description">You can customize the course completion experience with a certificate or a custom completion page!</p> <a href="#url">Course completion settings</a> </div> -->
							<p class="tip__description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis ex dicta incidunt eum mollitia rerum pariatur saepe cum quia repellat!</p>
                        </div> --}}
					</div>
					<div></div>
				</div>
				<div class="builder__footer">
					<div class="tree_action">
						<div class="builder-add-chapter">
							<a href="javascript:;" class="button button--primary w-100" id="template_confirm" data-toggle="modal" data-target="#exampleModalCenter"> @lang('site.select_this_template')</a>
							<a href="javascript:;" class="button button--primary target--button w-100" id="save_header" style="display: none;"> @lang('site.save_header')</a>
							<a href="javascript:;" class="button button--primary target--button w-100" id="save_content" style="display: none;"> @lang('site.save_content')</a>
							<a href="javascript:;" class="button button--primary target--button w-100" id="save_single" style="display: none;"> @lang('site.save_single_video')</a>
							<a href="javascript:;" class="button button--primary target--button w-100" id="save_products" style="display: none;"> @lang('site.save_products_section')</a>
							<a href="javascript:;" class="button button--primary target--button w-100" id="save_design" style="display: none;"> @lang('site.save_design')</a>
							<a href="javascript:;" class="button button--primary target--button w-100" id="save_logo" style="display: none;"> @lang('site.save_logo')</a>
						</div>
					</div>
				</div>
			</div>
			<div class="builder_section">
				<div class="wrap wrap-edit">
					<div class="ember-view">
                        <input type="text" value="{{@$detail->landing_template_id ?? '1'}}" id="template_number" hidden>
                        <iframe src="@if(@$detail) {{url('/')}}/l/{{auth()->user()->slug}}/{{@$detail->slug}} @else {{URL::to('/landing-page-template/1')}} @endif" style="width:100%; height:100%;" id="main_iframe" frameborder="0"></iframe>
					</div>
				</div>
			</div>
		</div>
	</section>
    <!-- Button trigger modal -->
    <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
    Launch demo modal
    </button> -->

    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">@lang("site.landing_page_name_title")</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            @lang("site.landing_page_name_text")
            <input type="text" class="form-control" id="template_name" placeholder="@lang('site.landing_page_name')" /><div id="err_template_name"></div>
        </div>
        <div class="modal-footer">
            <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
            <button type="button" class="btn btn-primary" id="save_temp_name">@lang('site.save_and_continue')</button>
        </div>
        </div>
    </div>
    </div>
@endsection
@section('footer')
{{-- @include('includes.footer') --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.4/utils/Draggable.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.4/TweenMax.min.js"></script>
<script>
    var clr = "";
    var timer = "";
    var template_id = 0;
    var template_confirmed = false;
    var detail = {!! json_encode(@$detail) !!};
    var prod_timer = [];
    var prod_i = whyus_i = faq_i = 0;
    var html_arr = [];
    var add_prod_btn_clicked = 0;
    console.log(detail);

    if (window.matchMedia('(min-width: 992px)').matches) {
		// functionality for screens smaller than 1200px
		$('.course_view').removeClass('mobile_chapter_list');
	}
	$(window).resize(function() {
		if (window.matchMedia('(max-width: 991px)').matches) {
			// functionality for screens smaller than 1200px
			$('.course_view').addClass('mobile_chapter_list');
		}
		if (window.matchMedia('(min-width: 992px)').matches) {
			// functionality for screens smaller than 1200px
			$('.course_view').removeClass('mobile_chapter_list');
		}
	});
	$('.show_course_view').click(function(){
		console.log("Toggle Clicked");
		$('.mobile_chapter_list').toggle("slide", { direction: "left" }, 500);
	});

    $(document).ready(function(){
        $("#main_iframe").contents().find('#products-actual').show();
        $("#main_iframe").contents().find('#products-initial').hide();

        $('iframe#main_iframe').on('load', function () {
            console.log("LOADED");
            $("#main_iframe").contents().find('#products-initial').hide();
            $("#main_iframe").contents().find('#products-actual').show();
            if($('#template_id').val() == ""){
                $("#main_iframe").contents().find('#products-actual').find('.without-slider').hide();
            }
            console.log($("#main_iframe").contents().find('#video-area')[0]);
        });
        prod_timer[1] = setInterval(function() {
            var html = prodTimeDiffCalc( new Date(), new Date($('#product_exp_date_1').val()) );
            if(html == 0){
                $("#main_iframe").contents().find('.product_expiry_'+1).html("Promoção terminou");
                window.frames.main_iframe.contentWindow.clearProductTimer(1);
                clearInterval(prod_timer[1]);
            } else {
                $("#main_iframe").contents().find('.product_expiry_'+1).html(html);
            }
        }, 1000);
        if(detail !== null){
            console.log("Getting in");
            template_id = detail.id;
            template_confirmed = true;
            $('.active').removeClass('active');
            $('.tab_header').addClass('active');
            $('.tab_target').hide();
            $('.tab_choose_temp').hide();
            $('#header').show();
            $('#template_confirm').hide();
            $('#save_header').show();
            $('#template_id').val(template_id);
            changeColours(detail.color_primary, detail.color_secondary, detail.color_tertiary);
            prod_i = detail.get_product_details.length != 0 ? detail.get_product_details.length : 1;
            faq_i = detail.get_faq.length != 0 ? detail.get_faq.length : 1;
            whyus_i = detail.get_why_us.length != 0 ? detail.get_why_us.length : 1;
            console.log("Number of products: "+ prod_i);
            if(prod_i == 10) $('#add_product').hide();
        } else {
            // $('#header_btn_yes').prop('checked', true);
            // $('#header_btn_url').prop('checked', true);
            // $('#header_button_lead_div').hide();
            // $('#lead_fname').prop('checked', true);
            // $('#lead_lname').prop('checked', true);
            // $('#lead_email').prop('checked', true);
            // $('#lead_country_code').prop('checked', true);
            // $('#lead_mobile').prop('checked', true);
            // $('#lead_address').prop('checked', true);
            // $('#lead_city').prop('checked', true);
            // $('#lead_state').prop('checked', true);
            // $('#lead_postal_code').prop('checked', true);
            // $('#lead_facebook').prop('checked', true);
            // $('#lead_instagram').prop('checked', true);
            // $('#lead_linkedin').prop('checked', true);
            // $('#lead_website').prop('checked', true);
            // $('#lead_message').prop('checked', false);
            // $('#header_reverse_yes').prop('checked', true);
        }
        $('myHeaderForm').validate();

        $('.landing_temp_card').click(function() {
            var id = $(this).data('id');
            console.log(id);
            var url = "{{URL::to('/landing-page-template/')}}/"+id;
            $('#template_number').val(id);
            $('#main_iframe').attr('src', url);
            $('.active_cert').removeClass('active_cert');
            $(this).addClass('active_cert');
            $('#header_reverse_yes').prop('checked', true);

            if(id == 1){
                $('#header_line_1').val("Lorem ipsum dolor sit ");
                $("#header_line_2").val("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
                $('#lead_fname').prop('checked', true);
                $('#lead_lname').prop('checked', true);
                $('#lead_email').prop('checked', true);
                $('#lead_country_code').prop('checked', true);
                $('#lead_mobile').prop('checked', true);
                $('#lead_address').prop('checked', true);
                $('#lead_city').prop('checked', true);
                $('#lead_state').prop('checked', true);
                $('#lead_postal_code').prop('checked', true);
                $('#lead_facebook').prop('checked', true);
                $('#lead_instagram').prop('checked', true);
                $('#lead_linkedin').prop('checked', true);
                $('#lead_website').prop('checked', true);
                $('#lead_message').prop('checked', false);

            } else if(id == 2){
                $("#header_line_1").val("Lorem ipsum dolor sit Lorem ipsum");
                $("#header_line_2").val("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Sed pretium ligula.");
                $('#lead_fname').prop('checked', true);
                $('#lead_lname').prop('checked', true);
                $('#lead_email').prop('checked', true);
                $('#lead_mobile').prop('checked', false);
                $('#lead_address').prop('checked', false);
                $('#lead_city').prop('checked', true);
                $('#lead_state').prop('checked', true);
                $('#lead_postal_code').prop('checked', true);
                $('#lead_facebook').prop('checked', false);
                $('#lead_instagram').prop('checked', false);
                $('#lead_linkedin').prop('checked', false);
                $('#lead_website').prop('checked', false);
                $('#lead_message').prop('checked', true);
            }
        });

        $('#template_confirm').click(function(){
            // sweetAlert();
        });

    });
    $('#template_name').keyup(function(){
        $('#err_template_name').html('');
    });
    $(document).delegate('#save_temp_name', 'click', function () {
        if($('#template_name').val() == ""){
            $('#err_template_name').html('<label class="error">@lang("site.template_name_required")</label>');
        } else {
            var reqData = {
                'jsonrpc' : '2.0',
                '_token' : '{{csrf_token()}}',
                'params' : {
                    'template_number': $('#template_number').val(),
                    'template_name': $('#template_name').val(),
                }
            };
            $.ajax({
                url: "{{ route('check.template.name') }}",
                method: 'post',
                dataType: 'json',
                data: reqData,
                async: false,
                success: function(res){
                    console.log(res);
                    if(res.status == 'NA'){
                        $('#err_template_name').html('<label class="error">@lang("site.this_name_already_exists")</label>');
                    } else {
                        $('#err_template_name').html('');
                        storeTemplate();
                    }
                },error: function(err){
                    console.log(err);
                },
            });
        }
    });

    $('.show_course_view').click(function(){
        if(template_confirmed == true){
            var target = $(this).data('target');
            $('.tab_target').hide();
            $('#'+target).show();
            $('.target--button').hide();
            $('#save_'+target).show();
            $('.show_course_view').removeClass('active');
            $('.tab_'+target).addClass('active');
            if(target == "products" || target == "why_us" || target == "faqs"){
                $('.course_view').addClass('full_heigth');
                $('.builder__footer').css('display', 'none');
            } else {
                $('.course_view').removeClass('full_heigth');
                $('.builder__footer').css('display', 'block');
            }
        } else {
            toastr.error("@lang('client_site.select_temp_first')")
        }
    });

    function sweetAlert(){
        $('.swal_html').remove();
        var html = document.createElement("div");
        html.classList.add('swal_html');
        $(html).html(`<input type="text" class="form-control" id="template_name" placeholder="@lang('site.landing_page_name')" /><div id="err_template_name"></div>`);
        console.log(html);
        swal({
            title: '@lang("site.landing_page_name_title")',
            text : '@lang("site.landing_page_name_text")',
            content: html,
            closeModal: false,
            buttons: {
                cancel : 'Cancel',
                confirm : {text:'OK', className:'sweet-ok'}
            }
        })
        .then((isConfirmed) => {
            if (isConfirmed) {
                if($('#template_name').val() == ""){
                    $('#err_template_name').html('<label class="error">@lang("site.template_name_required")</label>');
                    $('.sweet-ok').attr('disabled',true);
                } else {
                    storeTemplate();
                }
            }
        });
    }

    function storeTemplate(){
        var reqData = {
            'jsonrpc' : '2.0',
            '_token' : '{{csrf_token()}}',
            'params' : {
                'template_number': $('#template_number').val(),
                'template_name': $('#template_name').val(),
            }
        };
        $.ajax({
            url: "{{ route('store.landing.page.template') }}",
            method: 'post',
            dataType: 'json',
            data: reqData,
            async: false,
            success: function(res){
                console.log(res);
                template_id = res.template.id;

                window.location.href = "{{url('/')}}/landing-page/edit/"+template_id;
                // $('.active').removeClass('active');
                // $('.tab_header').addClass('active');
                // $('.tab_target').hide();
                // $('.tab_choose_temp').hide();
                // $('#header').show();
                // $('#template_confirm').hide();
                // $('#save_header').show();
                // $('.temp_title').text($('#template_name').val());
                // template_confirmed = true;
            },
            error: function(err){
                console.log(err);
            },
        });
    }

    // HEADER
        $('#header_image').change(function(){
            var file = this.files[0];
            var reader = new FileReader();
            reader.onloadend = function (e) {
                // console.log(reader.result);
                $("#main_iframe").contents().find('.header_image').attr('src', e.target.result);
            }
            if (file) {
                reader.readAsDataURL(file);
            } else {
                img_array = ["ban-img2.png", "wef-min.jpg", "ban.jpg", "h1-sl1.jpg"];
                val = parseInt($('#template_number').val());
                $("#main_iframe").contents().find('.header_image').attr('src', "{{URL::asset('public/frontend/')}}/landing_page"+val+"/images/"+img_array[val-1]);
            }
        });
        $('#header_line_1').on('keyup change', function(){
            var str = $(this).val();
            if($('#template_number').val() == 1){
                var html = "";
                if(str.indexOf(" ") === -1){
                    html = "<span>" + str + "</span>";
                } else {
                    var words = str.split(" ");
                    html = "<span>" + words[0] + "</span> " + str.substring( str.indexOf(" ") + 1, str.length );
                }
                $("#main_iframe").contents().find('.header_line_1').html(html);
            } else {
                $("#main_iframe").contents().find('.header_line_1').text(str);
            }
        });
        $('#header_line_2').on('keyup change', function(){
            $("#main_iframe").contents().find('.header_line_2').text($(this).val());
        });
        $('input[type=radio][name=header_reverse]').change(function() {
            if (this.value == 'Y') {
                $('#header_reverse_action').show();
                $("#main_iframe").contents().find('.header_reverse').show();
                $('#header_reverse_action').addClass('required');
            } else if (this.value == 'N') {
                $('#header_reverse_action').hide();
                $("#main_iframe").contents().find('.header_reverse').hide();
                $('#header_reverse_action').removeClass('required');
            }
        });
        $('input[type=radio][name=header_btn]').change(function() {
            if (this.value == 'Y') {
                $('#header_btn_action').show();
                $('#header_button_text').addClass('required');
                $("#main_iframe").contents().find('.header_btn').show();
            } else if (this.value == 'N') {
                $('#header_btn_action').hide();
                $('#header_button_text').removeClass('required');
                $("#main_iframe").contents().find('.header_btn').hide();
            }
        });
        $('input[type=radio][name=header_button_type]').change(function(){
            if (this.value == 'URL'){
                $('#header_button_link_div').show();
                $('#header_button_lead_div').hide();
                $("#main_iframe").contents().find('.header_btn').attr('href', $('#header_button_link').val());
                $("#main_iframe").contents().find('.header_btn').removeAttr('data-toggle');
                $("#main_iframe").contents().find('.header_btn').removeAttr('data-target');
                $("#main_iframe").contents().find('#contact-area').hide();
            } else if (this.value == 'LEAD'){
                $('#header_button_lead_div').show();
                $('#header_button_link_div').hide();
                if($('input[type=radio][name=lead_position]:checked').val() == 'P'){
                    $("#main_iframe").contents().find('.header_btn').attr('href', '#');
                $("#main_iframe").contents().find('.header_btn').removeClass('banBotArw');
                    $("#main_iframe").contents().find('.header_btn').attr('data-toggle', 'modal');
                    $("#main_iframe").contents().find('.header_btn').attr('data-target', '#myModal');
                    $("#main_iframe").contents().find('#contact-area').hide();
                } else {
                    $("#main_iframe").contents().find('.header_button_text').attr('href', '#contact-area');
                    $("#main_iframe").contents().find('.header_btn').addClass('banBotArw');
                    $("#main_iframe").contents().find('.header_btn').removeAttr('data-toggle');
                    $("#main_iframe").contents().find('.header_btn').removeAttr('data-target');
                    $("#main_iframe").contents().find('#contact-area').show();
                }
            }
        });
        $('input[type=radio][name=lead_position]').change(function() {
            if(this.value == 'P'){
                $("#main_iframe").contents().find('.header_btn').attr('href', '#');
                $("#main_iframe").contents().find('.header_btn').removeClass('banBotArw');
                $("#main_iframe").contents().find('.header_btn').attr('data-toggle', 'modal');
                $("#main_iframe").contents().find('.header_btn').attr('data-target', '#myModal');
                $("#main_iframe").contents().find('#contact-area').hide();
            }
            if(this.value == 'F'){
                $("#main_iframe").contents().find('.header_btn').attr('href', '#contact-area');
                $("#main_iframe").contents().find('.header_btn').addClass('banBotArw');
                $("#main_iframe").contents().find('.header_btn').removeAttr('data-toggle');
                $("#main_iframe").contents().find('.header_btn').removeAttr('data-target');
                $("#main_iframe").contents().find('#contact-area').show();
            }
        });
        $('#lead_section_heading').on('keyup change', function(){
            $("#main_iframe").contents().find('.lead_section_heading').text($(this).val());
        });
        $('#lead_section_footer_submit_btn').on('keyup change', function(){
            $("#main_iframe").contents().find('#lead_form_footer_btn').text($(this).val());
            $("#main_iframe").contents().find('#lead_form_popup_btn').text($(this).val());
        });
        $('#lead_section_desc').on('keyup change', function(){
            $("#main_iframe").contents().find('.lead_section_desc').text($(this).val());
        });
        $(document).delegate('input[type=radio][class=content_file_type]', 'change', function(){
            var template_load='{{@$temp->id}}';
            var id = $(this).data('id');
            if(this.value == 'I'){
                $('#content_image_div_'+id).show();
                $('#content_youtube_div_'+id).hide();
                $('#image_style_type_div_'+id).show();
                $val_style = $('input[type=radio][name=image_style_'+id+']:checked').val();
                if((parseInt(template_load)==3 ||parseInt(template_load)==4) && $val_style=='C'){
                    $("#main_iframe").contents().find('#content_file_'+id).addClass('square_image');
                }
                if(parseInt(template_load)==2 ||parseInt(template_load)==1){
                    $("#main_iframe").contents().find('#content_file_'+id).removeClass('video_youtube');
                }
            } else if(this.value == 'Y') {
                $('#content_image_div_'+id).hide();
                $('#content_youtube_div_'+id).show();
                $('#image_style_type_div_'+id).hide();
                $val_style = $('input[type=radio][name=image_style_'+id+']:checked').val();
                console.log($val_style);
                if((parseInt(template_load)==3 ||parseInt(template_load)==4) && $val_style=='C'){
                    $("#main_iframe").contents().find('#content_file_'+id).removeClass('square_image');
                }
                $("#main_iframe").contents().find('#content_file_'+id).addClass('video_youtube');
            }
        });
        $(document).delegate('input[type=radio][class=image_style_type]', 'change', function(){
            var template_load='{{@$temp->id}}';
            var id = $(this).data('id');
            console.log(id);
            console.log(this.value);
            if(this.value == 'S'){
                if(parseInt(template_load)==3 ||parseInt(template_load)==4){
                    $("#main_iframe").contents().find('#content_file_'+id).removeClass('square_image');
                }
                if(parseInt(template_load)==1 ||parseInt(template_load)==2){
                    $("#main_iframe").contents().find('#content_file_'+id).addClass('square_image');
                }
            }

            if(this.value == 'C'){
                if(parseInt(template_load)==3 ||parseInt(template_load)==4){
                    $("#main_iframe").contents().find('#content_file_'+id).addClass('square_image');
                }
                if(parseInt(template_load)==1 ||parseInt(template_load)==2){
                    $("#main_iframe").contents().find('#content_file_'+id).removeClass('square_image');
                }

            }
        });
        $(document).delegate('input[type=radio][class=image_style_type_single]', 'change', function(){
            console.log(this.value);
            var template_load='{{@$temp->id}}';
            if(this.value == 'S'){
                if(parseInt(template_load)==3 ||parseInt(template_load)==4){
                    $("#main_iframe").contents().find('#content_file_single').removeClass('square_image');
                }
                if(parseInt(template_load)==1 ||parseInt(template_load)==2){
                    $("#main_iframe").contents().find('#content_file_single').addClass('square_image');
                }
            }
            if(this.value == 'C'){
                if(parseInt(template_load)==3 ||parseInt(template_load)==4){
                    $("#main_iframe").contents().find('#content_file_single').addClass('square_image');
                }
                if(parseInt(template_load)==1 ||parseInt(template_load)==2){
                    $("#main_iframe").contents().find('#content_file_single').removeClass('square_image');
                }
            }
        });
        $('#header_button_text').on('keyup change', function(){
            if($('#template_number').val() == 2){
                $("#main_iframe").contents().find('.header_button_text').html($(this).val() + `<i class="fa fa-long-arrow-right"></i>`);
            } else {
                $("#main_iframe").contents().find('.header_button_text').text($(this).val());
            }
        });
        $('#lead_fname').change(function(){
            if($('#lead_fname').is(':checked')){
                $("#main_iframe").contents().find('.lead_fname').show();
            } else {
                $("#main_iframe").contents().find('.lead_fname').hide();
            }
        });
        $('#lead_lname').change(function(){
            if($('#lead_lname').is(':checked')){
                $("#main_iframe").contents().find('.lead_lname').show();
            } else {
                $("#main_iframe").contents().find('.lead_lname').hide();
            }
        });
        $('#lead_email').change(function(){
            if($('#lead_email').is(':checked')){
                $("#main_iframe").contents().find('.lead_email').show();
            } else {
                $("#main_iframe").contents().find('.lead_email').hide();
            }
        });
        $('#lead_country_code').change(function(){
            if($('#lead_country_code').is(':checked')){
                $("#main_iframe").contents().find('.lead_country_code').show();
            } else {
                $("#main_iframe").contents().find('.lead_country_code').hide();
            }
        });
        $('#lead_mobile').change(function(){
            if($('#lead_mobile').is(':checked')){
                $("#main_iframe").contents().find('.lead_mobile').show();
            } else {
                $("#main_iframe").contents().find('.lead_mobile').hide();
            }
        });
        $('#lead_address').change(function(){
            if($('#lead_address').is(':checked')){
                $("#main_iframe").contents().find('.lead_address').show();
            } else {
                $("#main_iframe").contents().find('.lead_address').hide();
            }
        });
        $('#lead_city').change(function(){
            if($('#lead_city').is(':checked')){
                $("#main_iframe").contents().find('.lead_city').show();
            } else {
                $("#main_iframe").contents().find('.lead_city').hide();
            }
        });
        $('#lead_state').change(function(){
            if($('#lead_state').is(':checked')){
                $("#main_iframe").contents().find('.lead_state').show();
            } else {
                $("#main_iframe").contents().find('.lead_state').hide();
            }
        });
        $('#lead_postal_code').change(function(){
            if($('#lead_postal_code').is(':checked')){
                $("#main_iframe").contents().find('.lead_postal_code').show();
            } else {
                $("#main_iframe").contents().find('.lead_postal_code').hide();
            }
        });
        $('#lead_facebook').change(function(){
            if($('#lead_facebook').is(':checked')){
                $("#main_iframe").contents().find('.lead_facebook').show();
            } else {
                $("#main_iframe").contents().find('.lead_facebook').hide();
            }
        });
        $('#lead_instagram').change(function(){
            if($('#lead_instagram').is(':checked')){
                $("#main_iframe").contents().find('.lead_instagram').show();
            } else {
                $("#main_iframe").contents().find('.lead_instagram').hide();
            }
        });
        $('#lead_linkedin').change(function(){
            if($('#lead_linkedin').is(':checked')){
                $("#main_iframe").contents().find('.lead_linkedin').show();
            } else {
                $("#main_iframe").contents().find('.lead_linkedin').hide();
            }
        });
        $('#lead_website').change(function(){
            if($('#lead_website').is(':checked')){
                $("#main_iframe").contents().find('.lead_website').show();
            } else {
                $("#main_iframe").contents().find('.lead_website').hide();
            }
        });
        $('#lead_message').change(function(){
            if($('#lead_message').is(':checked')){
                $("#main_iframe").contents().find('.lead_message').show();
            } else {
                $("#main_iframe").contents().find('.lead_message').hide();
            }
        });
        $('input[type=radio][name=lead_success_button]').change(function(){
            if (this.value == 'N'){
                $('#lead_success_button_div').hide();
                $("#main_iframe").contents().find('#lead_success_ok_popup').hide();
                $("#main_iframe").contents().find('#lead_success_ok_footer').hide();
            } else if (this.value == 'Y'){
                $('#lead_success_button_div').show();
                $("#main_iframe").contents().find('#lead_success_ok_popup').show();
                $("#main_iframe").contents().find('#lead_success_ok_footer').show();
            }
        });
        $('#lead_success_heading').keyup(function(){
            $("#main_iframe").contents().find('#lead_success_heading_popup').text(this.value);
            $("#main_iframe").contents().find('#lead_success_heading_footer').text(this.value);
        });
        $('#lead_success_desc').keyup(function(){
            $("#main_iframe").contents().find('#lead_success_desc_popup').text(this.value);
            $("#main_iframe").contents().find('#lead_success_desc_footer').text(this.value);
        });
        $('#lead_success_button_caption').keyup(function(){
            $("#main_iframe").contents().find('#lead_success_ok_popup').text(this.value);
            $("#main_iframe").contents().find('#lead_success_ok_footer').text(this.value);
        });
        $('#lead_privacy_policy').keyup(function(){
            $("#main_iframe").contents().find('#lead_privacy_desc').text(this.value);
        });
        $('#save_header').click(function(){
            if($("#myHeaderForm").valid()){
                updateTemplate();
            }
        });
        $("#header_counter_date").datepicker({dateFormat: "yy-mm-dd",

            monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            monthNamesShort: [ "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ],
            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],

            defaultDate: new Date(),
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            showMonthAfterYear: true,
            showWeek: true,
            showAnim: "drop",
            constrainInput: true,
            yearRange: "-90:",
            minDate: +1,
            onSelect: function(dateText) {
                window.frames.main_iframe.contentWindow.clearTheTimer();
                clearInterval(timer);
                console.log("Cleared timer");
                timer = setInterval(function() {
                    // count--;
                    // if (count == 0) {
                    // clearInterval(timer);
                    // return;
                    // }
                    var html = timeDiffCalc(new Date(), new Date($("#header_counter_date").val()));
                    if(html == 0){
                        clearInterval(timer);
                        $("#main_iframe").contents().find('.header_reverse_text').text("Promoção terminou");
                        $("#main_iframe").contents().find('.header_counter').empty();
                    }
                    $("#main_iframe").contents().find('.header_reverse_text').text("Esta promoção termina em:");
                    $("#main_iframe").contents().find('.header_counter').html(html);
                    changeTimerColours($('#color_primary').val(), $('#color_secondary').val(), $('#color_tertiary').val());
                    console.log($('#color_secondary').val());
                }, 1000);
            },
            onClose: function( selectedDate ) {
                //$( "#datepicker1").datepicker( "option", "minDate", selectedDate );
            }
        });
    // HEADER

    // WHY US
        $('#whyus_main_title').keyup(function(){
            var str = $(this).val();
            if($('#template_number').val() == 1){
                var html = "";
                if(str.indexOf(" ") === -1){
                    html = "<span>" + str + "</span>";
                } else {
                    var words = str.split(" ");
                    html = "<span>" + words[0] + "</span> " + str.substring( str.indexOf(" ") + 1, str.length );
                }
                $("#main_iframe").contents().find('.whyus_main_title').html(html);
            } else {
                $("#main_iframe").contents().find('.whyus_main_title').text(str);
            }
        });
        $('#whyus_main_desc').keyup(function(){
            $("#main_iframe").contents().find('.whyus_main_desc').text(this.value);
        });
        $(document).delegate('.whyus_img', 'change', function(){
            var id = parseInt($(this).data('id'));
            console.log(id);
            var file = this.files[0];
            $("#main_iframe").contents().find('#whyus_img_'+id).empty();
            var reader = new FileReader();
            reader.onloadend = function (e) {
                $("#main_iframe").contents().find('#whyus_img_'+id).html(`<img src="${e.target.result}" />`);
            }
            if (file) {
                reader.readAsDataURL(file);
            } else {
                var icons = ['<i class="fas fa-file-invoice"></i>', '<i class="fas fa-handshake"></i>', '<i class="fas fa-chalkboard-teacher"></i>']
                $("#main_iframe").contents().find('#whyus_img_'+id).html(icons[id-1]);
            }
        });
        $(document).delegate('.whyus_title', 'keyup', function(){
            var id = parseInt($(this).data('id'));
            $("#main_iframe").contents().find('#whyus_title_'+id).text(this.value);
        });
        $(document).delegate('.whyus_desc', 'keyup', function(){
            var id = parseInt($(this).data('id'));
            $("#main_iframe").contents().find('#whyus_desc_'+id).text(this.value);
        });
        $(document).delegate('.whyus_save', 'click', function(){
            var elem = $(this);
            var id = $(this).data('id');
            var idn = $('#whyus_section_'+id).data('identity') == 0 ? 0 : $('#whyus_section_'+id).data('identity');
            if($("#myWhyUsForm"+id).valid()){
                var uploadFormData = new FormData();
                uploadFormData.append("jsonrpc", "2.0");
                uploadFormData.append("_token", '{{csrf_token()}}');
                uploadFormData.append("landing_page_master_id", $('#template_id').val());
                uploadFormData.append("file", $('#whyus_img_'+id).val() != "" ? $('#whyus_img_'+id)[0].files[0] : null);
                uploadFormData.append("title", $('#whyus_title_'+id).val());
                uploadFormData.append("description", $('#whyus_desc_'+id).val());
                if(idn != 0){
                    uploadFormData.append("whyus_id", idn);
                }
                $.ajax({
                    url: "{{route('landing.page.save.whyus')}}",
                    method: 'post',
                    data: uploadFormData,
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(res){
                        console.log(res);
                        if(res.status == "success") {
                            toastr.success(res.message);
                            console.log("Res ID: " +res.details.id);
                            console.log($('#whyus_section_'+id)[0]);
                            $('#whyus_section_'+id).data('identity', res.details.id);
                            $('#whyus_section_'+id).attr('data-identity', res.details.id);
                            $(elem).data('identity', res.details.id);
                            $(elem).attr('data-identity', res.details.id)
                            $(elem).parent().find('p').css('visibility', 'visible');
                            setTimeout(() => {
                                $(elem).parent().find('p').css('visibility', 'hidden');
                            }, 5000);
                        } else toastr.error(res.message);
                    },
                    error: function(err){
                        console.log(err);
                    },
                });
            }
        });
        $('#add_whyus').click(function(){
            var i = parseInt($('.whyus_section').length) + 1;
            console.log(i);
            if(i == 10){
                toastr.error("@lang('client_site.maximum_section_limit_reached')");
            } else {
                var html1 = title = icon = font = border = background = icon_background = icon_font = "";
                if($('#template_number').val() == 1) {
                    font = "";
                    font_hover = "color_primary_font_hover";
                    border = "";
                    background = "color_primary_light";
                    icon_background = "color_primary";
                    icon_font = "";
                    icon_border = "";
                } else if($('#template_number').val() == 2){
                    font = "color_secondary_font";
                    font_hover = "color_primary_font_hover";
                    border = "color_primary_border";
                    background = "color_primary_light";
                    icon_background = "color_primary";
                    icon_font = "";
                    icon_border = "";
                } else if($('#template_number').val() == 3){
                    font = "color_tertiary_font";
                    font_hover = "color_primary_font_hover";
                    border = "";
                    background = "color_secondary";
                    icon_background = "";
                    icon_font = "color_primary_font";
                    icon_border = "";
                } else if($('#template_number').val() == 4){
                    font = "";
                    font_hover = "color_secondary_font_hover";
                    border = "";
                    background = "color_tertiary";
                    icon_background = "";
                    icon_font = "color_secondary_font";
                    icon_border = "color_secondary_border";
                }
                if(i % 3 == 0){
                    title = "Your dream is our plan";
                    icon = `<i class="fas fa-chalkboard-teacher ${icon_font}"></i>`;
                } else if(i % 2 == 0){
                    title = "employees you can trust";
                    icon = `<i class="fas fa-handshake ${icon_font}"></i>`;
                } else {
                    title = "certified contractors";
                    icon = `<i class="fas fa-file-invoice ${icon_font}"></i>`;
                }
                html1 =
                `<div class="col-sm-4 mb-3" id="why_box_${i}">
                    <div class="whyus_bx ${background} ${border}">
                        <em id="whyus_img_${i}" class="${icon_background} ${icon_border}">
                            ${icon}
                        </em>
                        <strong><a href="javascript:;" id="whyus_title_${i}" class="${font} ${font_hover}">${title}</a></strong>
                        <p id="whyus_desc_${i}" class="${font}">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.</p>
                    </div>
                </div>`;
                var html =
                `<div class="card whyus_section" id="whyus_section_${i}" data-identity="0">
                    <form action="{{ route('store.landing.page.template') }}" id="myWhyUsForm${i}">
                        <div class="card-header d-flex" id="faqhead${i}" data-id="${i}">
                            <a href="javascript:;" class="btn btn-header-link acco-chap rem_tick collapsed"
                                data-toggle="collapse"
                                data-target="#faq${i}"
                                aria-expanded="true"
                                aria-controls="faq${i}"
                            >
                                <img src="{{ URL::to('public/frontend/images/cha.png') }}">
                                <h5>@lang('client_site.section') ${i}</h5>
                                <a href="javascript:;" class="btn del_whyus" data-id="${i}" data-identity="0"> <i class="fa fa-times" aria-hidden="true"></i> </a>
                            </a>
                        </div>
                        <div id="faq${i}" class="collapse" aria-labelledby="faqhead${i}" data-parent="#faq">
                            <div class="card-body">
                                <div class="whyus_detail" id="whyus_${i}">
                                    <div class="cert_cntnt">
                                        <label for="whyus_img_${i}">@lang('site.upload_picture')</label>
                                        <input type="file" name="whyus_img_${i}" id="whyus_img_${i}" class="form-control whyus_img" data-id="${i}" accept="image/*">
                                    </div>
                                    <div class="cert_cntnt">
                                        <label for="whyus_title_${i}">@lang('site.title')</label>
                                        <input type="text" name="whyus_title_${i}" id="whyus_title_${i}" class="form-control required whyus_title" data-id="${i}" value="${title}">
                                    </div>
                                    <div class="cert_cntnt mb-3">
                                        <label for="whyus_desc_${i}">@lang('site.description')</label>
                                        <textarea name="whyus_desc_${i}" id="whyus_desc_${i}" class="form-control required whyus_desc" data-id="${i}">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.</textarea>
                                    </div>
                                    <div class="sect_bottom_div">
                                        <p style="visibility: hidden;">
                                            <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                            @lang('client_site.saved_successfully')
                                        </p>
                                        <a href="javascript:;" class="whyus_save btn btn-primary pull-right mb-3" id="whyus_save_${i}" data-id="${i}">@lang('client_site.save')</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>`;
                $('.why_us_div').append(html);
                $("#main_iframe").contents().find('.whyus_inr').find('.row').append(html1);
                changeColours($('#color_primary').val(), $('#color_secondary').val(), $('#color_tertiary').val());
            }
        });
        $(document).delegate('.del_whyus', 'click', function(){
            if($('.whyus_section').length == 1){
                toastr.error("@lang('client_site.must_have_one_section')")
            } else {
                var id = $(this).data('id');
                var idn = $('#whyus_section_'+id).data('identity');
                if(idn == 0){
                    $('#whyus_section_'+id).remove();
                    $("#main_iframe").contents().find('#why_box_'+id).remove();
                    toastr.success("@lang('client_site.section_deleted_successfully')");
                } else {
                    $.get( "{{url('/')}}/landing-page/delete-whyus/"+idn, function( res ) {
                        if(res.status == "success"){
                            $('#whyus_section_'+id).remove();
                            $("#main_iframe").contents().find('#why_box_'+id).remove();
                            toastr.success(res.message);
                        } else toastr.error(res.message);
                    });
                }
            }
        });
        $('input[type=radio][name=why_us_section]').change(function() {
            if (this.value == 'Y') {
                $('#why_us_total').show();
                $("#main_iframe").contents().find('.whyus_sec').show();
            } else if (this.value == 'N') {
                $('#why_us_total').hide();
                $("#main_iframe").contents().find('.whyus_sec').hide();
            }
            updateTemplate();
        });
        $('#save_whyus_head').click(function(){
            updateTemplate();
        });
    // WHY US

    // SINGLE VIDEO
        $('input[type=radio][name=single_video]').change(function() {
            if (this.value == 'Y') {
                $('#single_video_div').show();
                $("#main_iframe").contents().find('.single_video').show();
            } else if (this.value == 'N') {
                $('#single_video_div').hide();
                $("#main_iframe").contents().find('.single_video').hide();
            }
        });
        $('#single_file').on('change', function(){
            var file = this.files[0];
            var fileType = file["type"];
            var temp4Class = "";
            if($('#template_number').val() == 4) temp4Class = "color_secondary_border color_tertiary_shadow";

            var vdo_size = 209715200;
            var valid_video = true;
            const validImageTypes = ['video/mp4'];
            var mimetype = file['type'];
            var size = file['size'];
            var name = file['name'];
            console.log(size);
            console.log(mimetype);
            if(fileType.search('video')>=0){
                if (!validImageTypes.includes(mimetype)) {
                    toastr.error(name+' não é um arquivo de vídeo válido.');
                    // alert(name+' não é um arquivo de vídeo válido.');
                    $('#single_file').val('');
                    valid_video = false;
                } else if (size > vdo_size) {
                    toastr.error(name+' ié maior que 200 MB.');
                    // alert(name+' ié maior que 200 MB.');
                    $('#single_file').val('');
                    valid_video = false;
                } else {
                    toastr.info("Please wait till the video is successfully uploaded.");
                    // $('#save_single').prop("disabled", true);
                    $('#save_single').attr('disabled', 'disabled');
                    valid_video = true;
                }
            }
            if(fileType.search('image') >= 0 || (fileType.search('video') >= 0 && valid_video == true)){
                updateTemplate();
                console.log("Updated Video");
                console.log(detail);
                // $('#save_single').removeAttr('disabled');
                // if(detail.body_single_video_type == "I"){
                //     $("#main_iframe").contents().find('.single_vdo').empty().append(`<img width="100%" src="{{URL::to('storage/app/public/uploads/landing_page')}}/`+detail.body_single_video_filename+`" class="${temp4Class}" />`);
                //     $("#main_iframe").contents().find('.paly_btn').hide();
                // } else {
                //     $("#main_iframe").contents().find('.single_vdo').empty().append(`<video width="100%" src="{{URL::to('storage/app/public/uploads/landing_page')}}/`+detail.body_single_video_filename+`" class="${temp4Class}"></video>`);
                //     $("#main_iframe").contents().find('.paly_btn').show();
                // }
            } else {
                toastr.error("@lang('client_site.upload_image_or_video')");
                $('#single_file').val('');
            }
            console.log("Calling change colorus");
            changeColours($('#color_primary').val(), $('#color_secondary').val(), $('#color_tertiary').val());
        });
        $('#body_single_video_heading').on('keyup change', function(){
            $("#main_iframe").contents().find('.body_single_video_heading').text(this.value);
        });
        $('#body_single_video_desc').on('keyup change', function(){
            $("#main_iframe").contents().find('.body_single_video_desc').text(this.value);
        });
        $('#save_single').click(function(){
            if($("#mySingleVideoForm").valid()){
                updateTemplate();
            }
        });
    // SINGLE VIDEO

    // CONTENT
        $('input[type=radio][name=body_single_content]').change(function() {
            if (this.value == 'Y') {
                $('#single_content_div').show();
                $("#main_iframe").contents().find('.single_content_div').show();
                $('#multiple_content_div').hide();
                $("#main_iframe").contents().find('.multiple_content_div').hide();
                $('#add_mult_cntnt').hide();
            } else if (this.value == 'N') {
                $('#single_content_div').hide();
                $("#main_iframe").contents().find('.single_content_div').hide();
                $('#multiple_content_div').show();
                $("#main_iframe").contents().find('.multiple_content_div').show();
                $('#add_mult_cntnt').show();
            }
        });
        $('#body_single_content_image').change(function(){
            var file = this.files[0];
            var reader = new FileReader();
            reader.onloadend = function (e) {
                $("#main_iframe").contents().find('.single_content_file').attr('src', e.target.result);
            }
            if (file) {
                reader.readAsDataURL(file);
            } else {
                $("#main_iframe").contents().find('.single_content_file').attr('src', "{{URL::asset('public/frontend/')}}/landing_page" + $('#template_number').val() + "/images/{{@$temp->body_single_content_image}}");
            }
        });
        $('#body_single_content_heading').keyup(function(){
            var str = $(this).val();
            if($('#template_number').val() == 1){
                var html = "";
                if(str.indexOf(" ") === -1){
                    html = "<span>" + str + "</span>";
                } else {
                    var words = str.split(" ");
                    html = "<span>" + words[0] + "</span> " + str.substring( str.indexOf(" ") + 1, str.length );
                }
                $("#main_iframe").contents().find('.body_single_content_heading').html(html);
            } else {
                $("#main_iframe").contents().find('.body_single_content_heading').text(str);
            }
        });
        $('#body_single_content_desc').keyup(function(){
            $("#main_iframe").contents().find('.body_single_content_desc').text(this.value);
        });
        $('input[type=radio][name=body_single_content_button]').change(function() {
            if (this.value == 'Y') {
                $('#body_single_content_button_div').show();
                $("#main_iframe").contents().find('.body_single_content_button').show();
            } else if (this.value == 'N') {
                $('#body_single_content_button_div').hide();
                $("#main_iframe").contents().find('.body_single_content_button').hide();
            }
        });
        $('#body_single_content_button_text').keyup(function(){
            $("#main_iframe").contents().find('.body_single_content_button_text').text(this.value);
        });
        $('#body_single_content_button_link').keyup(function(){
            $("#main_iframe").contents().find('.body_single_content_button_link').attr('href', this.value);
        });

        $(document).delegate('.content_heading_inp', 'keyup', function(){
            var str = $(this).val();
            if($('#template_number').val() == 1){
                var html = "";
                if(str.indexOf(" ") === -1){
                    html = "<span>" + str + "</span>";
                } else {
                    var words = str.split(" ");
                    html = "<span>" + words[0] + "</span> " + str.substring( str.indexOf(" ") + 1, str.length );
                }
                $("#main_iframe").contents().find('.content_heading_'+$(this).data('id')).html(html);
            } else {
                $("#main_iframe").contents().find('.content_heading_'+$(this).data('id')).text(str);
            }
        });
        $(document).delegate('.content_desc_inp', 'keyup', function(){
            $("#main_iframe").contents().find('.content_desc_'+$(this).data('id')).text($(this).val());
        });
        $(document).delegate('.content_file_inp', 'change', function(){
            var id = $(this).data('id');
            console.log("ID: "+id);
            var file = this.files[0];
            var reader = new FileReader();
            reader.onloadend = function (e) {
                // console.log(reader.result);
                // $("#main_iframe").contents().find('.content_file_'+id).find('img').attr('src', e.target.result);
                $("#main_iframe").contents().find('.content_file_'+id).html('<img src="'+e.target.result+'">');
                $('#content_file_youtube_link_'+id).val('');
            }
            if (file) {
                reader.readAsDataURL(file);
            } else {
                img_array = ["vedioimg.jpg", "gfg-min.png"];
                val = $('#template_number').val();
                if(id == 1)
                    // $("#main_iframe").contents().find('.content_file_'+id).find('img').attr('src', "{{URL::asset('public/frontend/')}}/landing_page"+val+"/images/"+img_array[val+1]);
                    $("#main_iframe").contents().find('.content_file_'+id).html("<img src='{{URL::asset('public/frontend/')}}/landing_page"+val+"/images/"+img_array[val+1]+"'>");
                else
                    // $("#main_iframe").contents().find('.content_file_'+id).find('img').attr('src', "{{URL::asset('public/frontend/')}}/landing_page"+val+"/images/wef-min.jpg");
                    $("#main_iframe").contents().find('.content_file_'+id).html("<img src='{{URL::asset('public/frontend/')}}/landing_page"+val+"/images/wef-min.jpg'>");
            }
        });
        function ytVidId(url) {
            var p = /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
            return (url.match(p)) ? RegExp.$1 : false;
        }
        $(document).delegate('.content_file_youtube', 'blur', function(){
            var id = $(this).data('id');
            console.log("ID: "+id);
            var url = $(this).val();
            console.log(ytVidId(url))
            if (ytVidId(url) !== false) {
                $('#content_file_'+id).val('');
                $("#content_file_youtube_link_error"+id).html('');
                $("#main_iframe").contents().find('.content_file_'+id).html('<iframe src="https://www.youtube.com/embed/'+ytVidId(url)+'" frameborder="0" allowfullscreen></iframe>');
            } else {
                $("#content_file_youtube_link_error"+id).html('Link do youtube inválido.');
                $('#content_file_youtube_link_'+id).val('');
            }
        });
        $(document).delegate('input[type=radio][class=content_button]', 'change', function() {
            var id = $(this).data('id');
            if (this.value == 'Y') {
                $('#content_button_div_'+id).show();
                $("#main_iframe").contents().find('.content_button_'+id).show();
            } else if (this.value == 'N') {
                $('#content_button_div_'+id).hide();
                $("#main_iframe").contents().find('.content_button_'+id).hide();
            }
        });
        $(document).delegate('.content_button_text_inp', 'keyup', function(){
            console.log("Working");
            $("#main_iframe").contents().find('.content_button_'+$(this).data('id')).text(this.value);
        });
        $(document).delegate('.content_button_link_inp', 'keyup', function(){
            $("#main_iframe").contents().find('.content_button_'+$(this).data('id')).attr('href', this.value);
        });
        $(document).delegate('.del_multicontent', 'click', function(){
            var id = parseInt($(this).data('id'));
            // console.log($('#multicontent_'+id).data('saved'));
            if(id == 3 && $('#multicontent_4').length){
                toastr.error("@lang('client_site.remove_content_tab_4')");
            } else {
                if($('#multicontent_'+id).data('saved') == false){
                    $('#multicontent_'+id).remove();
                    loadContentSection(id);
                    $("#main_iframe").contents().find('#content_div_'+id).hide();
                } else {
                    removeMultiContent($('#template_id').val(), id-1);
                }
            }
            $('#add_mult_cntnt').show();
        });
        $('#add_mult_cntnt').click(function(){
            var i = parseInt($('.multicontent').length) + 1;
            var template_load='{{@$temp->id}}';
            if(parseInt(template_load)==3 ||parseInt(template_load)==4){
                var sq='checked';
                var cr='';
            }
            if(parseInt(template_load)==1 ||parseInt(template_load)==2){
                var cr='checked';
                var sq='';
            }
            if(i == 4){
                // toastr.error("@lang('client_site.maximum_content_limit_reached')");
                $('#add_mult_cntnt').hide();
            } else {
                $('#add_mult_cntnt').show();
            }
            var title = button_text = "";
            if($('#template_number').val() == 1){
                title = "Online Learning";
                button_text = "learn more"
            } else if($('#template_number').val() == 2){
                title = "Online Learning";
                button_text = "Read More";
            } else if($('#template_number').val() == 3){
                title = "Lorem ipsum dolor sit amet";
                button_text = "Read More";
            }  else if($('#template_number').val() == 4){
                title = "Lorem Ipsum Dolor Sit Amet";
                button_text = "Read More";
            }
            var html = `<div class="multicontent" id="multicontent_${i}" style="padding: 20px 0px;" data-id="${i}" data-saved="false">
                <div class="multi_section_head">
                    <h5>@lang('client_site.section') ${i}</h5>
                    <a href="javascript:;" class="btn del_multicontent" data-id="${i}"> <i class="fa fa-times" aria-hidden="true"></i> </a>
                </div>
                <div class="cert_cntnt">
                    <label for="content_file_type_${i}">@lang('site.content_file')</label>  <br>
                    <label for="content_image_${i}">
                        <input type="radio" class="content_file_type" name="content_file_${i}" value="I" id="content_image_${i}" data-id="${i}" checked >
                        @lang('site.upload_picture')
                    </label>
                    <label for="content_youtube_${i}">
                        <input type="radio" class="content_file_type" name="content_file_${i}" value="Y" id="content_youtube_${i}" data-id="${i}" >
                        Youtube Link
                    </label>
                </div>
                <div class="cert_cntnt" id="image_style_type_div_${i}">
                    <label for="image_style_type_${i}">@lang('client_site.image_style') </label>  <br>
                    <label for="image_square_${i}">
                        <input type="radio" class="image_style_type" name="image_style_${i}" value="S" id="image_square_${i}" data-id="${i}" ${sq}>
                        @lang('client_site.square')
                    </label>
                    <label for="image_circle_${i}">
                        <input type="radio" class="image_style_type" name="image_style_${i}" value="C" id="image_circle_${i}" data-id="${i}" ${cr}>
                        @lang('client_site.circle')
                    </label>
                </div>
                <div class="cert_cntnt" id="content_image_div_${i}">
                    <label for="content_file_${i}">@lang('site.upload_picture')</label>  <br>
                    <input type="file" name="content_file_${i}" id="content_file_${i}" class="form-control content_file_inp" data-id="${i}" accept="image/*">
                    <label class="text-secondary">@lang('client_site.recommended_size') 800 * 800</label>
                </div>
                <div class="cert_cntnt" id="content_youtube_div_${i}" style="display: none;">
                    <label for="content_file_youtube_link_${i}">Youtube Link</label>  <br>
                    <input type="text" name="content_file_youtube_link_${i}" id="content_file_youtube_link_${i}" class="form-control required content_file_youtube" data-id="${i}" >
                </div>
                <div class="cert_cntnt">
                    <label for="content_heading_${i}">@lang('site.title')</label>  <br>
                    <input type="text" name="content_heading_${i}" id="content_heading_${i}" class="content_heading_inp form-control required" data-id="${i}" value="${title}">
                </div>
                <div class="cert_cntnt">
                    <label for="content_desc_${i}">@lang('site.description')</label>  <br>
                    <input type="text" name="content_desc_${i}" id="content_desc_${i}" class="content_desc_inp form-control required" data-id="${i}" value="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.">
                </div>
                <div class="cert_cntnt">
                    <label for="content_button_${i}">@lang('site.button')</label>  <br>
                    <label for="content_button_yes_${i}">
                        <input type="radio" class="content_button" name="content_button_${i}" value="Y" id="content_button_yes_${i}" data-id="${i}">
                        @lang('site.yes')
                    </label>
                    <label for="content_button_no_${i}">
                        <input type="radio" class="content_button" name="content_button_${i}" value="N" id="content_button_no_${i}" data-id="${i}" checked>
                        @lang('site.no')
                    </label>
                </div>
                <div id="content_button_div_${i}" style="display: none;">
                    <div class="cert_cntnt">
                        <label for="content_button_text_${i}">@lang('site.button_caption')</label>  <br>
                        <input type="text" name="content_button_text_${i}" id="content_button_text_${i}" class="content_button_text_inp form-control required" data-id="${i}" value="${button_text}">
                    </div>
                    <div class="cert_cntnt">
                        <label for="content_button_link_${i}">@lang('site.button') Link</label>  <br>
                        <input type="url" name="content_button_link_${i}" id="content_button_link_${i}" class="content_button_link_inp form-control required" data-id="${i}">
                    </div>
                </div>
                <hr/>
            </div>`;
            $('#multiple_content_div').append(html);
            $("#main_iframe").contents().find('#content_div_'+i).show();
        });
        $('#save_content').click(function(){
            if($("#myContentForm").valid()){
                if($('input[type=radio][name=body_single_content]:checked').val() == 'Y'){
                    updateTemplate();
                } else {
                    $.get( "{{url('/')}}/landing-page/all-multiple-content-delete/"+$('#template_id').val(), function( res ) {
                        if(res.status == "success") storeMultiContents();
                    });
                    toastr.success("@lang('site.landing_temp_saved_successfully')");
                }
            }
        });
        function removeMultiContent(temp_id, srno){
            $.get( "{{url('/')}}/landing-page/multiple-content-delete/"+temp_id+"/"+srno, function( res ) {
                console.log(res);
                if(res.status == "success"){
                    var v = parseInt(srno)+1;
                    $('#multicontent_'+v).remove();
                    loadContentSection(v);
                    console.log(v);
                    // console.log($("#main_iframe").contents().find('#content_div_'+v)[0]);
                    $("#main_iframe").contents().find('#content_div_'+v).hide();
                }
            });
        }
        function storeMultiContents(){
            $('.multicontent').each(function(i, content){
                var num = parseInt(i)+1;
                console.log(num);
                var uploadFormData = new FormData();
                uploadFormData.append("jsonrpc", "2.0");
                uploadFormData.append("_token", '{{csrf_token()}}');
                uploadFormData.append("landing_page_master_id", $('#template_id').val());
                uploadFormData.append('content_heading', $('#content_heading_'+num).val());
                uploadFormData.append('content_desc', $('#content_desc_'+num).val());
                uploadFormData.append('content_file', $('#content_file_'+num).val() != "" ? $('#content_file_'+num)[0].files[0]: null);
                uploadFormData.append('content_file_youtube_link', $('#content_file_youtube_link_'+num).val() != "" ? $('#content_file_youtube_link_'+num).val(): null);
                uploadFormData.append('content_button', $(`input[type=radio][name=content_button_${num}]:checked`).val());
                uploadFormData.append('image_style', $(`input[type=radio][name=image_style_${num}]:checked`).val());
                uploadFormData.append('content_button_text', $('#content_button_text_'+num).val());
                uploadFormData.append('content_button_link', $('#content_button_link_'+num).val());
                updateMultiContent(uploadFormData);
                console.log($('#multicontent_'+num)[0]);
                $('#multicontent_'+num).data('saved', true);
                $('#multicontent_'+num).attr('data-saved', true);
            });
        }
        function updateMultiContent(uploadFormData){
            $.ajax({
                url: "{{route('store.landing.page.multiple.content')}}",
                method: 'post',
                data: uploadFormData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function(res){
                    console.log(res);
                    if(res.status == "success") {
                        // toastr.success(res.message);
                    } else toastr.error(res.message);
                },
                error: function(err){
                    console.log(err);
                },
            });
        }
        function loadContentSection(srno){
            // Reset content heding
            img = "wef-min.jpg";
            var str = "Online Learning";
            if($('#template_number').val() == 1){
                img = ""
                var html = "";
                if(str.indexOf(" ") === -1){
                    html = "<span>" + str + "</span>";
                } else {
                    var words = str.split(" ");
                    html = "<span>" + words[0] + "</span> " + str.substring( str.indexOf(" ") + 1, str.length );
                }
                $("#main_iframe").contents().find('.content_heading_'+srno).html(html);
            } else if($('#template_number').val() == 2){
                img = "wef-min.jpg";
                $("#main_iframe").contents().find('.content_heading_'+srno).text(str);
            } else if($('#template_number').val() == 3 || $('#template_number').val() == 4){
                img = "img.jpg";
                $("#main_iframe").contents().find('.content_heading_'+srno).text("Lorem ipsum dolor sit amet");
            }

            // Reset content description
            var desc = '{{@$temp->body_single_content_desc}}';
            console.log("DESC: "+desc);
            $("#main_iframe").contents().find('.content_desc_'+srno).text(desc);

            // Reset content button
            var btn_text = '{{@$temp->body_single_content_button_text}}';
            $("#main_iframe").contents().find('.content_button_'+srno).attr('href', "javascript:;");
            $("#main_iframe").contents().find('.content_button_'+srno).text(btn_text);
            $("#main_iframe").contents().find('.content_button_'+srno).hide();

            // Reset content image
            src = "{{URL::asset('public/frontend/')}}/landing_page"+$('#template_number').val()+"/images/"+img;
            $("#main_iframe").contents().find('.content_file_'+srno).html(`<img src="${src}" alt="">`);
            // .attr('src', "{{URL::asset('public/frontend/')}}/landing_page"+$('#template_number').val()+"/images/wef-min.jpg");

        }
    // CONTENT

    // PRODUCT
        $('#save_product_head').click(function(){
            if($("#myProductHeadForm").valid()) updateTemplate();
        });
        $('body').on('focus', ".product_exp_date_inp", function() {
            $(this).datepicker({dateFormat: "yy-mm-dd",

                monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                monthNamesShort: [ "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ],
                dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
                dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
                dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],

                defaultDate: new Date(),
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                showMonthAfterYear: true,
                showWeek: true,
                showAnim: "drop",
                constrainInput: true,
                yearRange: "-90:",
                minDate: +1,
                onSelect: function(dateText) {
                    var num = $(this).data('id');
                    var val = $(this).val();
                    window.frames.main_iframe.contentWindow.clearProductTimer(num);
                    clearInterval(prod_timer[num]);
                    prod_timer[num] = setInterval(function() {
                        var html = prodTimeDiffCalc(new Date(), new Date(val));
                        if(html == 0){
							$("#main_iframe").contents().find('.product_expiry_'+num).html("Promoção terminou");
                            window.frames.main_iframe.contentWindow.clearProductTimer(num);
                            clearInterval(prod_timer[num]);
                        } else {
                            $("#main_iframe").contents().find('.product_expiry_'+num).html(html);
                        }
                    }, 1000);
                },
                onClose: function( selectedDate ) {
                    //$( "#datepicker1").datepicker( "option", "minDate", selectedDate );
                }
            });
        });
        $(document).delegate('#product_section_heading', 'keyup', function(){
            $("#main_iframe").contents().find('.product_section_heading').text(this.value);
        });
        $(document).delegate('#product_section_desc', 'keyup', function(){
            $("#main_iframe").contents().find('.product_section_desc').text(this.value);
        });
        $(document).delegate('input[type=radio][name=product_slider]', 'change', function(){
            // $("#main_iframe").contents().find('.product_slider').text(this.value);
            if(this.value == 'Y'){
                $("#main_iframe").contents().find('.with-slider').show();
                $("#main_iframe").contents().find('.without-slider').hide();
            } else {
                $("#main_iframe").contents().find('.with-slider').hide();
                $("#main_iframe").contents().find('.without-slider').show();
            }
        });
        $('#add_product').click(function(){
            prod_i = prod_i + 1;
            var i = prod_i;
            var owl_settings = null;
            var html = html_col = "";
            console.log("I: " + i);
            if(i > 10) {
                toastr.error("@lang('client_site.maximum_10_products')");
                $('#add_product').hide();
            } else {
                if(i == 10) $('#add_product').hide();
                if($('#template_number').val() == 1){
                    var html =
                    `<div class="item product_div_${i} proddiv">
                        <div class="produt_img">
                            <span class="file_div">
                                <img src="{{ URL::to('public/frontend/images/NoImage.jpg') }}" alt="" class="product_file_${i}">
                            </span>
                            <div class="produt_text">
                                <div>
                                    <h5 class="pro-des"><a href="#url" class="product_heading_${i} color_primary_font_hover">New Product</a></h5>
                                    <h6 class="color_tertiary_font pro-price"><a href="#url" class="product_price_${i}">R$800</a></h6>
                                </div>
                                <p class="color_tertiary_font product_desc_${i} pro-det">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                                <div class="timer-div">
                                    <a href="javascript:;" class="page_btn color_secondary product_link_${i}">More</a>
									<div></div>
                                    <p class="color_secondary timer-pro product_expiry_${i} product_expiry_div_${i}">0:00:00:00</p>
                                </div>
                            </div>
                        </div>
                    </div>`;
                    var html_col =
                    `<div class="item col-md-4 product_div_${i} proddiv">
                        <div class="produt_img">
                            <span class="file_div">
                                <img src="{{ URL::to('public/frontend/images/NoImage.jpg') }}" alt="" class="product_file_${i}">
                            </span>
                            <div class="produt_text">
                                <div>
                                    <h5 class="pro-des"><a href="#url" class="product_heading_${i} color_primary_font_hover">New Product</a></h5>
                                    <h6 class="color_tertiary_font pro-price"><a href="#url" class="product_price_${i}">R$800</a></h6>
                                </div>
                                <p class="color_tertiary_font product_desc_${i} pro-det">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                                <div class="timer-div">
                                    <a href="javascript:;" class="page_btn color_secondary product_link_${i}">More</a>
									<div></div>
                                    <p class="color_secondary timer-pro product_expiry_${i} product_expiry_div_${i}">0:00:00:00</p>
                                </div>
                            </div>
                        </div>
                    </div>`;
                    owl_settings = {
                        margin: 0,
                        nav: true,
                        autoplay: true,
                        loop: false,
                        responsive: {
                            0: {
                                items: 1
                            },
                                575: {
                                items: 1
                            },
                                768: {
                                items: 2
                            },
                            991: {
                                items: 3
                            },
                            1000: {
                                items: 3
                            }
                        }
                    };
                } else if($('#template_number').val() == 2){
                    html =
                    `<div class="item product_div_${i} proddiv" style="${style}">
                        <div class="product_detail prod-single text-center three">
                            <div class="prod-img file_div">
                                <img src="{{ URL::to('public/frontend/images/NoImage.jpg') }}" alt="" class="product_file_${i}">
                            </div>
                            <div class="prod-info three">
                                <h4 class="pro_ac"><a href="#" class="product_heading_${i} color_secondary_font color_primary_font_hover">New Product</a></h4>
                                <p class="color_secondary_font product_desc_${i}">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo</p>
                                <h5 class="product_price_${i} color_primary_font">R$800</h5>
                                <p class="product_expiry_${i} product_expiry_div_${i} timer-pro color_secondary_font color_secondary_border"></p>
                                <a href="#" class="btn_pro product_link_${i} color_primary">learn more</a>
                            </div>
                        </div>
                    </div>`;
                    html_col =
                    `<div class="item col-md-4 mb-3 product_div_${i} proddiv" style="${style}">
                        <div class="product_detail prod-single text-center three">
                            <div class="prod-img file_div">
                                <img src="{{ URL::to('public/frontend/images/NoImage.jpg') }}" alt="" class="product_file_${i}">
                            </div>
                            <div class="prod-info three">
                                <h4 class="pro_ac"><a href="#" class="product_heading_${i} color_secondary_font color_primary_font_hover">New Product</a></h4>
                                <p class="color_secondary_font product_desc_${i}">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo</p>
                                <h5 class="product_price_${i} color_primary_font">R$800</h5>
                                <p class="product_expiry_${i} product_expiry_div_${i} timer-pro color_secondary_font color_secondary_border"></p>
                                <a href="#" class="btn_pro product_link_${i} color_primary">learn more</a>
                            </div>
                        </div>
                    </div>`;
                    owl_settings = {
                        loop: false,
                        navText: ['<i class="icofont-simple-left"></i>', '<i class="icofont-simple-right"></i>'],
                        nav: true,
                        autoplay: true,
                        autoplayTimeout: 5000,
                        smartSpeed: 450,
                        margin: 20,
                        responsive: {
                            0: {
                                items: 1
                            },
                            768: {
                                items: 2
                            },
                            991: {
                                items: 3
                            },
                            1200: {
                                items: 3
                            },
                            1920: {
                                items: 3
                            }
                        }
                    }
                } else if($('#template_number').val() == 3){
                    html =
                    `<div class="item product_div_${i} proddiv">
                        <div class="product_detail prod-single three">
                            <div class="pad15">
                                <div class="produt_img">
                                    <span class="file_div">
                                        <img src="{{ URL::to('public/frontend/images/NoImage.jpg') }}" alt="" class="product_file_${i}">
                                    </span>
                                    <div class="produt_text">
                                        <h5><a href="javascript:;" class="product_heading_${i} color_primary_font color_tertary_font_hover">New Product</a></h5>
                                        <p class="product_desc_${i}">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo</p>
                                        <span class="product_price_${i} color_primary_font">R$800</span>
                                        <div class="dis_f">
                                            <a href="javascript:;" class="page_btn product_link_${i} color_primary color_tertiary_hover">More</a>
                                            <div></div>
                                            <div class="timeer_bx product_expiry_div_${i}">
                                                <strong class="color_primary_border">
                                                    <i class="fas fa-clock color_primary_font"></i>
                                                    <em class="product_expiry_${i} color_primary_font">00:00:00</em>
                                                </strong>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>`;
                    html_col =
                    `<div class="item col-md-4 product_div_${i} proddiv">
                        <div class="product_detail prod-single three">
                            <div class="pad15">
                                <div class="produt_img">
                                    <span class="file_div">
                                        <img src="{{ URL::to('public/frontend/images/NoImage.jpg') }}" alt="" class="product_file_${i}">
                                    </span>
                                    <div class="produt_text">
                                        <h5><a href="javascript:;" class="product_heading_${i} color_primary_font color_tertary_font_hover">New Product</a></h5>
                                        <p class="product_desc_${i}">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo</p>
                                        <span class="product_price_${i} color_primary_font">R$800</span>
                                        <div class="dis_f">
                                            <a href="javascript:;" class="page_btn product_link_${i} color_primary color_tertiary_hover">More</a>
                                            <div></div>
                                            <div class="timeer_bx product_expiry_div_${i}">
                                                <strong class="color_primary_border">
                                                    <i class="fas fa-clock color_primary_font"></i>
                                                    <em class="product_expiry_${i} color_primary_font">00:00:00</em>
                                                </strong>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>`;
                    console.log(i);
                    owl_settings = {
                        nav: true,
                        autoplay: true,
                        loop: false,
                        dots: false,
                        responsive: {
                            0: {
                                items: 1,
                                margin: 0
                            },
                            400: {
                                items: 1,
                                margin: 0
                            },
                            480: {
                                items: 1,
                                margin: 15
                            },
                            575: {
                                items: 1,
                                margin: 20
                            },
                            768: {
                                items: 2,
                                margin: 30
                            },
                            992: {
                                items: 3,
                                margin: 30
                            },
                            1199: {
                                items: 3,
                                margin: 30
                            },
                            1499: {
                                items: 3,
                                margin: 30
                            }
                        }
                    }
                } else if($('#template_number').val() == 4){
                    html =
                    `<div class="item product_div_${i} proddiv">
                        <div class="produt_img color_secondary_border">
                            <div class="pro_ig">
                                <div class="file_div">
                                    <img src="{{ URL::to('public/frontend/images/NoImage.jpg') }}" alt="" class="product_file_${i}">
                                </div>
                                <span class="pro_pr_bx color_primary product_price_${i}">R$800</span>
                                <div class="pri_dv product_expiry_div_${i} color_secondary"><strong><i class="fas fa-clock"></i> <span class="product_expiry_${i}">0:00:00:00</span></strong></div>
                            </div>
                            <div class="produt_text">
                                <h5 class="color_secondary_border">
                                    <a href="javascript:;" class="product_heading_${i} color_secondary_font">New Product</a>
                                </h5>
                                <p class="product_desc_${i}">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                                <a href="javascript:;" class="page_btn color_secondary_font product_link_${i}">
                                    <span class="color_secondary_font">More</span>
                                    <i class="fa fa-angle-double-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>`;
                    html_col =
                    `<div class="item col-md-4 product_div_${i} proddiv">
                        <div class="produt_img color_secondary_border">
                            <div class="pro_ig">
                                <div class="file_div">
                                    <img src="{{ URL::to('public/frontend/images/NoImage.jpg') }}" alt="" class="product_file_${i}">
                                </div>
                                <span class="pro_pr_bx color_primary product_price_${i}">R$800</span>
                                <div class="pri_dv product_expiry_div_${i} color_secondary"><strong><i class="fas fa-clock"></i> <span class="product_expiry_${i}">0:00:00:00</span></strong></div>
                            </div>
                            <div class="produt_text">
                                <h5 class="color_secondary_border">
                                    <a href="javascript:;" class="product_heading_${i} color_secondary_font">New Product</a>
                                </h5>
                                <p class="product_desc_${i}">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                                <a href="javascript:;" class="page_btn color_secondary_font product_link_${i}">
                                    <span class="color_secondary_font">More</span>
                                    <i class="fa fa-angle-double-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>`;
                    owl_settings = {
                        nav: true,
                        autoplay: true,
                        loop: false,
                        responsive: {
                            0: {
                            items: 1,
                            margin: 0
                            },
                            400: {
                            items: 1,
                            margin: 0
                            },
                            480: {
                            items: 1,
                            margin: 0
                            },
                            575: {
                            items: 2,
                            margin: 0
                            },
                            768: {
                            items: 2,
                            margin: 0
                            },
                            992: {
                            items: 3,
                            margin: 0
                            },
                            1199: {
                            items: 3,
                            margin: 0
                            },
                            1499: {
                            items: 3,
                            margin: 0
                            }
                        }
                    }
                }
                var $owl = $("#main_iframe").contents().find('#products-actual').find('.owl-carousel');
                if(add_prod_btn_clicked == 0) {
                    add_prod_btn_clicked = 1;
                    $owl.trigger('destroy.owl.carousel');

                    var htmlowlstage = "";
                    htmlowlstage +=
                    $owl.find('.owl-stage-outer').find('.owl-stage').html();
                    htmlowlstage +=
                    `<div class="owl-item active" style="${style}">${html}</div>
                    <div class="owl-item active" style="${style}"></div>`;

                    var style = $owl.find('.owl-item:first-child').attr('style');

                    $owl.find('.owl-stage-outer').html(htmlowlstage);
                    var htmlowl = $owl.find('.owl-stage-outer').html();
                    console.log(htmlowl);
                    $owl.html(htmlowl).removeClass('owl-loaded');
                    $owl.owlCarousel(owl_settings);
                } else {
                    var pos = parseInt($owl.find('.proddiv').length);
                    console.log("POS: " + pos);
                    $owl.owlCarousel('update');
                    $owl.trigger('add.owl.carousel', [$(html), 999999]).trigger('refresh.owl.carousel');
                    if(pos == 2){
                        $owl.trigger('add.owl.carousel', [$(html), 999999]).trigger('refresh.owl.carousel');
                        $owl.trigger('remove.owl.carousel', [pos-2]).trigger('refresh.owl.carousel');
                    }
                    if(pos == 5){
                        console.log("Entering 6");
                        $owl.trigger('add.owl.carousel', [$(html), 999999]).trigger('refresh.owl.carousel');
                        $owl.trigger('add.owl.carousel', [$(html), 999999]).trigger('refresh.owl.carousel');
                        $owl.trigger('remove.owl.carousel', [0]).trigger('refresh.owl.carousel');
                        $owl.trigger('remove.owl.carousel', [pos-2]).trigger('refresh.owl.carousel');
                    }
                    if(pos == 6 || pos == 8){
                        $owl.trigger('add.owl.carousel', [$(html), 999999]).trigger('refresh.owl.carousel');
                        $owl.trigger('remove.owl.carousel', [0]).trigger('refresh.owl.carousel');
                    }
                }
                console.log($owl.find('.owl-item:last')[0]);
                // $owl.find('.owl-item:last').hide();
                $("#main_iframe").contents().find('.without-slider').find('.row_main').append(html_col);
                var html =
                `<div class="card product_card" id="product_card_${i}" data-identity="0">
                    <form action="{{ route('store.landing.page.template') }}" id="myProductForm${i}">
                        <div class="card-header d-flex" id="faqhead${i}" data-id="${i}">
                            <a href="javascript:;" class="btn btn-header-link acco-chap rem_tick collapsed"
                                data-toggle="collapse"
                                data-target="#faq${i}"
                                aria-expanded="true"
                                aria-controls="faq${i}"
                            >
                                <img src="{{ URL::to('public/frontend/images/cha.png') }}">
                                <h5>New Product</h5>
                                <a href="javascript:;" class="btn del_prod" data-id="${i}"> <i class="fa fa-times" aria-hidden="true"></i> </a>
                            </a>
                        </div>
                        <div id="faq${i}" class="collapse" aria-labelledby="faqhead${i}" data-parent="#faq">
                            <div class="card-body">
                                <div class="product_detail" id="product_${i}">
                                    <div class="cert_cntnt">
                                        <label for="product_file_type_${i}">@lang('client_site.select_file_type')</label>  <br>
                                        <label for="product_file_type_none_${i}">
                                            <input type="radio" name="product_file_type_${i}" value="N" id="product_file_type_none_${i}" class="product_file_type_inp" data-id="${i}" checked >
                                            @lang('client_site.no_file')
                                        </label>
                                        <label for="product_file_type_image_${i}">
                                            <input type="radio" name="product_file_type_${i}" value="I" id="product_file_type_image${i}" class="product_file_type_inp" data-id="${i}" >
                                            @lang('site.image')
                                        </label>
                                        <label for="product_file_type_youtube_${i}">
                                            <input type="radio" name="product_file_type_${i}" value="Y" id="product_file_type_youtube${i}" class="product_file_type_inp" data-id="${i}" >
                                            Youtube Video
                                        </label>
                                        <div class="product_file_inp_group" id="prod_image_${i}" style="display: none;">
                                            <input type="file" name="product_file_${i}" id="product_file_${i}" class="product_file_inp form-control" data-id="${i}" accept="image/*">
                                            {{-- <span><i class="fa fa-trash del_prod_img" id="del_prod_img_${i}" data-id="${i}" data-identity="0" aria-hidden="true"></i></span> --}}
                                            <label class="text-secondary">@lang('client_site.recommended_size') 1280 * 720</label>
                                        </div>
                                        <div class="product_file_inp_group" id="prod_youtube_${i}" style="display: none;">
                                            <input type="url" name="product_youtube_${i}" id="product_youtube_${i}" class="product_youtube_inp required form-control" data-id="${i}" placeholder="Youtube Video">
                                        </div>
                                    </div>
                                    <div class="cert_cntnt">
                                        <label for="product_heading_${i}">@lang('site.title')</label>
                                        <input type="text" name="product_heading_${i}" id="product_heading_${i}" class="product_heading_inp form-control required" data-id="${i}" value="New Product">
                                    </div>
                                    <div class="cert_cntnt">
                                        <label for="product_desc_${i}">@lang('site.description')</label>
                                        <textarea name="product_desc_${i}" id="product_desc_${i}" class="product_desc_inp form-control required" data-id="${i}">Lorem ipsum dolor sit amet, consectetur adipiscing elit</textarea>
                                    </div>
                                    <div class="cert_cntnt">
                                        <label for="product_price_show_${i}">@lang('client_site.show_product_price')</label>  <br>
                                        <label for="product_price_show_yes_${i}">
                                            <input type="radio" name="product_price_show_${i}" value="Y" id="product_price_show_yes_${i}" class="product_price_show_inp" data-id="${i}" checked >
                                            @lang('site.yes')
                                        </label>
                                        <label for="product_price_show_no_${i}">
                                            <input type="radio" name="product_price_show_${i}" value="N" id="product_price_show_no_${i}" class="product_price_show_inp" data-id="${i}">
                                            @lang('site.no')
                                        </label>
                                    </div>
                                    <div class="cert_cntnt price_div_${i}">
                                        <label for="product_price_${i}">@lang('site.price')</label>
                                        <input type="text" name="product_price_${i}" id="product_price_${i}" class="product_price_inp form-control required" data-id="${i}" value="800">
                                    </div>
                                    <div class="cert_cntnt">
                                        <label for="product_button_${i}">@lang('client_site.product_button')</label>  <br>
                                        <label for="product_button_yes_${i}">
                                            <input type="radio" name="product_button_${i}" value="Y" id="product_button_yes_${i}" class="product_button_inp" data-id="${i}" checked >
                                            @lang('site.yes')
                                        </label>
                                        <label for="product_button_no_${i}">
                                            <input type="radio" name="product_button_${i}" value="N" id="product_button_no_${i}" class="product_button_inp" data-id="${i}" >
                                            @lang('site.no')
                                        </label>
                                    </div>
                                    <div id="product_button_div_${i}">
                                        <div class="cert_cntnt">
                                            <label for="product_button_caption_${i}">@lang('site.button_caption')</label>
                                            <input type="text" name="product_button_caption_${i}" id="product_button_caption_${i}" class="product_button_caption_inp form-control required" data-id="${i}" value="More">
                                        </div>
                                        <div class="cert_cntnt">
                                            <label for="product_button_url_${i}">@lang('site.button') Link</label>
                                            <input type="url" name="product_button_url_${i}" id="product_button_url_${i}" class="product_button_url_inp form-control required">
                                        </div>
                                    </div>
                                    <div class="cert_cntnt">
                                        <label for="product_reverse_counter_${i}">@lang('client_site.product_reverse_counter')</label>  <br>
                                        <label for="product_reverse_counter_yes_${i}">
                                            <input type="radio" name="product_reverse_counter_${i}" value="Y" id="product_reverse_counter_yes_${i}" class="product_expires_on" data-id="${i}" checked >
                                            @lang('site.yes')
                                        </label>
                                        <label for="product_reverse_counter_no_${i}">
                                            <input type="radio" name="product_reverse_counter_${i}" value="N" id="product_reverse_counter_no_${i}" class="product_expires_on" data-id="${i}" >
                                            @lang('site.no')
                                        </label>
                                    </div>
                                    <div class="cert_cntnt" id="product_reverse_counter_div_${i}">
                                        <label for="product_exp_date_${i}">@lang('client_site.expires_on')</label>
                                        <input type="text" name="product_exp_date_${i}" id="product_exp_date_${i}" class="product_exp_date_inp form-control required" data-id="${i}" value="{{ date('Y-m-d', strtotime('+1 day')) }}" autocomplete="off" readonly >
                                    </div>
                                    <div class="sect_bottom_div">
                                        <p style="visibility: hidden;">
                                            <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                            @lang('client_site.saved_successfully')
                                        </p>
                                        <a href="javascript:;" class="btn btn-primary my-3 pull-right product_save" id="product_save_${i}" data-id="${i}">@lang('site.completa')</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>`;
                $('.products_accordion').append(html);
                // var count = $("#main_iframe").contents().find('.proddiv').length;
                // console.log(count);
                // if(count < 4){
                //     $("#main_iframe").contents().find('.leftLst').hide();
                //     $("#main_iframe").contents().find('.rightLst').hide();
                // } else {
                //     $("#main_iframe").contents().find('.leftLst').show();
                //     $("#main_iframe").contents().find('.rightLst').show();
                // }
                prod_timer[i] = setInterval(function() {
                    var html = prodTimeDiffCalc( new Date(), new Date($('#product_exp_date_'+i).val()) );
                    if(html == 0){
                        $("#main_iframe").contents().find('.product_expiry_'+i).html("Promoção terminou");
                        clearInterval(prod_timer[i]);
                    } else {
                        $("#main_iframe").contents().find('.product_expiry_'+i).html(html);
                    }
                }, 1000);
                changeColours($('#color_primary').val(), $('#color_secondary').val(), $('#color_tertiary').val());
            }
            callChangeColours();
        });
        $(document).delegate('.del_prod_img', 'click', function(){
            var id = $(this).data('id');
            var elem = $(this);
            var target = $('#product_card_'+id);
            console.log(id);
            swal({
                title: '@lang("site.delete")',
                text : '@lang("client_site.do_delete_product_image")',
                icon: "warning",
                buttons: true,
                dangerMode: true,
                buttons: {
                    cancel : 'Cancel',
                    confirm : 'OK'
                }
            })
            .then((isConfirmed) => {
                if (isConfirmed) {
                    if($(target).data('identity') == 0){
                        toastr.success("@lang('client_site.image_deleted_successfully')");
                        var val = $('#template_number').val();
                        $("#main_iframe").contents().find('.product_file_'+id).attr('src', "{{URL::asset('public/frontend/')}}/landing_page"+val+"/images/pro3.jpg");
                        $("#main_iframe").contents().find('.product_file_'+id).hide();
                        $(elem).hide();
                    } else {
                        $.get( "{{url('/')}}/landing-page/delete-product-image/"+$(target).data('identity'), function( res ) {
                            if(res.status == "success"){
                                toastr.success(res.message);
                                var val = $('#template_number').val();
                                $("#main_iframe").contents().find('.product_file_'+id).attr('src', "{{URL::asset('public/frontend/')}}/landing_page"+val+"/images/pro3.jpg");
                                $("#main_iframe").contents().find('.product_file_'+id).hide();
                                $(elem).hide();
                            } else toastr.error(res.message);
                        });
                    }
                }
            });
        });
        $(document).delegate('.product_file_type_inp', 'change', function(){
            var id = $(this).data('id');
            if(this.value == 'I'){
                $('#prod_image_'+id).show();
                $('#prod_youtube_'+id).hide();
                $('#product_youtube_'+id).val('');
            } else if(this.value == 'Y'){
                $('#product_file_'+id).val('');
                $('#prod_image_'+id).hide();
                $('#prod_youtube_'+id).show();
            } else {
                $('#product_file_'+id).val('');
                $('#product_youtube_'+id).val('');
                $('#prod_image_'+id).hide();
                $('#prod_youtube_'+id).hide();
                $("#main_iframe").contents().find('.product_div_'+id).find('.file_div').html(`<img src="{{ URL::to('public/frontend/images/NoImage.jpg') }}" alt="" class="product_file_${id}">`);
            }
        });
        $(document).delegate('.product_youtube_inp', 'keyup', function(){
            var id = $(this).data('id');
            console.log("ID: "+id);
            var url = $(this).val();
            var temp_id = $('#template_number').val();
            var cls = "";
            if(temp_id == 2) cls = "animation-jump";
            console.log(ytVidId(url))
            if (ytVidId(url) !== false) {
                $("#main_iframe").contents().find('.product_div_'+id).find('.file_div').empty();
                html =
                `<img src="http://img.youtube.com/vi/${ytVidId(url)}/0.jpg" alt="" class="animation-jump product_file_${id}"/>
                <div class="playIcon ${cls}">
                    <div class="video-player paly_btn vid-icon-you">
                        <a href="http://www.youtube.com/watch?v=${ytVidId(url)}" class="fancybox-media color_primary" style="background: ${$('#color_primary').val()}">
                            <i class="fa fa-play" aria-hidden="true" rel="media-gallery"></i>
                        </a>
                    </div>
                </div>
                `;
                console.log($("#main_iframe").contents().find('.product_div_'+id).find('.file_div')[0]);
                $("#main_iframe").contents().find('.product_div_'+id).find('.file_div').append(html);
                $('#product_file_'+id).val('');
            } else {
                $("#product_youtube_inp_error"+id).html('Link do youtube inválido.');
                $('#product_youtube_'+id).val('');
            }
        });
        $(document).delegate('.product_file_inp', 'change', function(){
            var id = $(this).data('id');
            console.log(id);
            var file = this.files[0];
            var reader = new FileReader();
            reader.onloadend = function (e) {
                // console.log(reader.result);
                var temp_cls = "";
                if($('#template_number').val() == 1)
                    temp_cls = "";
                else if($('#template_number').val() == 2)
                    temp_cls = 'img-fluid animation-jump';
                else if($('#template_number').val() == 3)
                    temp_cls = 'animation-jump';
                else if($('#template_number').val() == 4)
                    temp_cls = "";

                $("#main_iframe").contents().find('.product_div_'+id).find('.file_div').empty();
                html = `<img src="${e.target.result}" alt="" class="${temp_cls} product_file_${id}">`;
                $("#main_iframe").contents().find('.product_div_'+id).find('.file_div').append(html);
                $('#product_youtube_'+id).val('');

                // $("#main_iframe").contents().find('.product_file_'+id).attr('src', e.target.result);
                // $("#main_iframe").contents().find('.product_file_'+id).show();
                // $('#del_prod_img_'+id).show();
            }
            if (file) {
                reader.readAsDataURL(file);
            } else {
                val = $('#template_number').val();
                $("#main_iframe").contents().find('.product_div_'+id).find('.file_div').empty();
                $('#product_file_'+id).val('');
                // $("#main_iframe").contents().find('.product_file_'+id).attr('src', "{{URL::asset('public/frontend/')}}/landing_page"+val+"/images/pro3.jpg");
                // $("#main_iframe").contents().find('.product_file_'+id).hide();
                // $('#del_prod_img_'+id).hide();
            }
        });
        $(document).delegate('.product_heading_inp', 'keyup', function(){
            var id = $(this).data('id');
            $("#main_iframe").contents().find('.product_heading_'+id).text(this.value);
            $('#product_card_'+id).find('.acco-chap').find('h5').text(this.value);
        });
        $(document).delegate('.product_desc_inp', 'keyup', function(){
            var id = $(this).data('id');
            $("#main_iframe").contents().find('.product_desc_'+id).text(this.value);
        });
        $(document).delegate('.product_price_show_inp', 'change', function(){
            var id = $(this).data('id');
            if(this.value == 'Y') {
                $('.price_div_'+id).show();
                $("#main_iframe").contents().find('.product_price_'+id).show();
            } else {
                $('.price_div_'+id).hide();
                $("#main_iframe").contents().find('.product_price_'+id).hide();
            }
        });
        $(document).delegate('.product_price_inp', 'keyup', function(){
            var id = $(this).data('id');
            $("#main_iframe").contents().find('.product_price_'+id).text('R$'+this.value);
        });
        $(document).delegate('.product_button_inp', 'change', function(){
            var id = $(this).data('id');
            if(this.value == 'Y') {
                $('#product_button_div_'+id).show();
                $("#main_iframe").contents().find('.product_link_'+id).show();
            } else {
                $('#product_button_div_'+id).hide();
                $("#main_iframe").contents().find('.product_link_'+id).hide();
            }
        });
        $(document).delegate('.product_button_caption_inp', 'keyup', function(){
            var id = $(this).data('id');
            if($('#template_number').val() == 4){
                $("#main_iframe").contents().find('.product_link_'+id).find('span').text(this.value);
            } else {
                $("#main_iframe").contents().find('.product_link_'+id).text(this.value);
            }
        });
        $(document).delegate('.product_button_url_inp', 'keyup', function(){
            var id = $(this).data('id');
            $("#main_iframe").contents().find('.product_link_'+id).attr('src', this.value);
        });
        $(document).delegate('.product_expires_on', 'change', function(){
            var id = $(this).data('id');
            if($(`input[type=radio][name=product_reverse_counter_${id}]:checked`).val() == 'Y'){
                $('#product_reverse_counter_div_'+id).show();
                $("#main_iframe").contents().find('.product_expiry_div_'+id).show();
            } else {
                $('#product_reverse_counter_div_'+id).hide();
                $("#main_iframe").contents().find('.product_expiry_div_'+id).hide();
            }
        });
        $(document).delegate('.product_save', 'click', function(){
            var elem = $(this);
            var id = $(this).data('id');
            console.log(id);
            if($("#myProductForm"+id).valid()){
                var uploadFormData = new FormData();
                uploadFormData.append("jsonrpc", "2.0");
                uploadFormData.append("_token", '{{csrf_token()}}');
                uploadFormData.append("serial", id);
                uploadFormData.append("landing_page_master_id", $('#template_id').val());
                uploadFormData.append('product_file_type', $(`input[type=radio][name=product_file_type_${id}]:checked`).val());
                uploadFormData.append('product_youtube', ytVidId($('#product_youtube_'+id).val()));
                uploadFormData.append('product_file', $('#product_file_'+id).val() != "" ? $('#product_file_'+id)[0].files[0]: null);
                uploadFormData.append('product_heading', $('#product_heading_'+id).val());
                uploadFormData.append('product_desc', $('#product_desc_'+id).val());
                uploadFormData.append('product_price_show', $(`input[type=radio][name=product_price_show_${id}]:checked`).val());
                uploadFormData.append('product_price', $('#product_price_'+id).val());
                uploadFormData.append('product_button', $(`input[type=radio][name=product_button_${id}]:checked`).val());
                uploadFormData.append('product_button_caption', $('#product_button_caption_'+id).val());
                uploadFormData.append('product_button_url', $('#product_button_url_'+id).val());
                uploadFormData.append('product_reverse_counter', $(`input[type=radio][name=product_reverse_counter_${id}]:checked`).val());
                uploadFormData.append('product_exp_date', $('#product_exp_date_'+id).val());

                if($('#product_card_'+id).data('identity') != 0)   uploadFormData.append('product_id', $('#product_card_'+id).data('identity'));

                updateProductDetails(elem, uploadFormData);
            }
        });
        $(document).delegate('.del_prod', 'click', function(){
            var id = $(this).data('id');
            $('#add_product').show();
            if($('.product_detail').length == 1){
                toastr.error("@lang('client_site.must_have_one_product')")
            } else {
                var idn = $('#product_card_'+id).data('identity');
                console.log(idn);
                window.frames.main_iframe.contentWindow.clearProductTimer(id);
                clearInterval(prod_timer[id]);
                if(idn == 0){
                    removeProduct(id);
                    toastr.success("@lang('client_site.product_deleted_succesfully')");
                } else {
                    // removeProduct(id);
                    $.get( "{{url('/')}}/landing-page/delete-product/"+idn, function( res ) {
                        if(res.status == "success"){
                            removeProduct(id);
                            toastr.success(res.message);
                        } else toastr.error(res.message);
                    });
                }
            }
        });
        $(document).delegate('.produt_text', 'click', function(){
            console.log("Clicked");
        });

        // $(document).delegate('.produt_text h5', 'click', function(e) {
        //     console.log("Clicked");
        //     // var carousel = $("#main_iframe").contents().find('#products-actual').find(".product-actual-carousel").data('owl.carousel');
        //     // e.preventDefault();
        //     // console.log(carousel.to(carousel.relative($(this).index())));
        // });
        function removeProduct(id){
            var owl_settings = null;
            console.log('Number to remove: '+id);
            $('#product_card_'+id).remove();
            $("#main_iframe").contents().find('.without-slider').find('.product_div_'+id).remove();

            if($('#template_number').val() == 1){
                owl_settings = {
                    margin: 0,
                    nav: true,
                    autoplay: true,
                    loop: false,
                    responsive: {
                        0: {
                            items: 1
                        },
                            575: {
                            items: 1
                        },
                            768: {
                            items: 2
                        },
                        991: {
                            items: 3
                        },
                        1000: {
                            items: 3
                        }
                    }
                }
            }
            if($('#template_number').val() == 2){
                owl_settings = {
                    loop: false,
                    navText: ['<i class="icofont-simple-left"></i>', '<i class="icofont-simple-right"></i>'],
                    nav: true,
                    autoplay: true,
                    autoplayTimeout: 5000,
                    smartSpeed: 450,
                    margin: 20,
                    responsive: {
                        0: {
                            items: 1
                        },
                        768: {
                            items: 2
                        },
                        991: {
                            items: 3
                        },
                        1200: {
                            items: 3
                        },
                        1920: {
                            items: 3
                        }
                    }
                }
            }
            if($('#template_number').val() == 3){
                owl_settings = {
                    nav: true,
                    autoplay: true,
                    loop: false,
                    responsive: {
                        0: {
                            items: 1,
                            margin: 0
                        },
                        400: {
                            items: 1,
                            margin: 0
                        },
                        480: {
                            items: 2,
                            margin: 15
                        },
                        575: {
                            items: 2,
                            margin: 20
                        },
                        768: {
                            items: 2,
                            margin: 30
                        },
                        992: {
                            items: 3,
                            margin: 30
                        },
                        1199: {
                            items: 3,
                            margin: 30
                        },
                        1499: {
                            items: 3,
                            margin: 30
                        }
                    }
                }
            }
            if($('#template_number').val() == 4){
                owl_settings = {
                    nav: true,
                    autoplay: true,
                    loop: false,
                    responsive: {
                        0: {
                        items: 1,
                        margin: 0
                        },
                        400: {
                        items: 1,
                        margin: 0
                        },
                        480: {
                        items: 1,
                        margin: 0
                        },
                        575: {
                        items: 2,
                        margin: 0
                        },
                        768: {
                        items: 2,
                        margin: 0
                        },
                        992: {
                        items: 3,
                        margin: 0
                        },
                        1199: {
                        items: 3,
                        margin: 0
                        },
                        1499: {
                        items: 3,
                        margin: 0
                        }
                    }
                }
            }
            // if($("#main_iframe").contents().find('.product_div_'+id).parent().parent().hasClass('owl-item')){
            //     console.log("parent's parent");
            //     console.log( $("#main_iframe").contents().find('.with-slider').find('.product_div_'+id).parent().parent()[0]);
            //     $("#main_iframe").contents().find('.with-slider').find('.product_div_'+id).parent().parent().remove();
            // } else {
            //     console.log("only parent");
            //     console.log( $("#main_iframe").contents().find('.product_div_'+id).parent()[0]);
            //     $("#main_iframe").contents().find('.with-slider').find('.product_div_'+id).parent().remove();
            // }
            var $owl = $("#main_iframe").contents().find('#products-actual').find(".product-actual-carousel");
            // $owl.trigger('destroy.owl.carousel');
            // $owl.removeClass('owl-loaded');
            // var htmlowlstage = $owl.find('.owl-stage-outer').html();
            // console.log(htmlowlstage);
            // $owl.html(htmlowlstage);
            // $owl.owlCarousel(owl_settings);
            // $owl.owlCarousel('update');
            // $owl.trigger('refresh.owl.carousel');

            // $owl.trigger('destroy.owl.carousel');
            // $owl.removeClass('owl-loaded');
            // var htmlowlstage = $owl.find('.owl-stage-outer').html();
            // console.log(htmlowlstage);
            // $owl.find('.owl-stage-outer').html(htmlowlstage);
            // $owl.owlCarousel(owl_settings);

            // $owl.refreshOwl($owl);
            var to_rem = null;
            $("#main_iframe").contents().find('#products-actual').find('.owl-carousel').find('.owl-item').find('.item').each(function(i, item){
                if( $(item).hasClass('product_div_'+id) ){
                    console.log($(item)[0]);
                    console.log('I: '+i);
                    //     console.log($(item)[0]);
                    //     console.log('Index to remove: '+i);
                    if(to_rem == null) to_rem = i;
                    // console.log('Index to remove: '+$(item).index());
                    // $owl.trigger('remove.owl.carousel', [i]).trigger('refresh.owl.carousel');
                }
            });
            // if(id = 1) to_rem = 0;
            // else if(id = 2) to_rem = 1;
            // else if(id = 3) to_rem = 2;

            console.log('To Rem: '+to_rem);
            // console.log(typeof to_rem);
            $owl.trigger('remove.owl.carousel', [to_rem]).trigger('refresh.owl.carousel');

            // $("#main_iframe").contents().find('#products-actual').find('.owl-carousel').find('.owl-item').each(function(i, item){
            //     if($(item).is(':empty')){
            //         var carousel = $('.owl-carousel').data('owl.carousel');
            //         var car = carousel.to(carousel.relative($(item).index()));
            //         console.log(car);
            //     }
            // });
            // var len = parseInt($owl.find('.owl-item').length);
            // $owl.trigger('remove.owl.carousel', [len]).trigger('refresh.owl.carousel');
        }
        function updateProductDetails(element, uploadFormData){
            $.ajax({
                url: "{{route('store.landing.page.product.details')}}",
                method: 'post',
                data: uploadFormData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function(res){
                    console.log(res);
                    console.log(res.product.id);
                    if(res.status == "success") {
                        $('#product_card_'+res.serial).data('identity', res.product.id);
                        $('#product_card_'+res.serial).attr('data-identity', res.product.id);
                        $(element).parent().find('p').css('visibility', 'visible');
                        setTimeout(() => {
                            $(element).parent().find('p').css('visibility', 'hidden');
                        }, 5000);
                        toastr.success(res.message);
                    } else toastr.error(res.message);
                },
                error: function(err){
                    console.log(err);
                },
            });
        }
        function prodTimeDiffCalc(dateFuture, dateNow) {
            if(Math.abs(dateFuture - dateNow) == 0){
                return 0;
            } else {
                let diffInMilliSeconds = Math.abs(dateFuture - dateNow) / 1000;
                const days = Math.floor(diffInMilliSeconds / 86400);
                diffInMilliSeconds -= days * 86400;
                const hours = Math.floor(diffInMilliSeconds / 3600) % 24;
                diffInMilliSeconds -= hours * 3600;
                const minutes = Math.floor(diffInMilliSeconds / 60) % 60;
                diffInMilliSeconds -= minutes * 60;
                const seconds = Math.round(diffInMilliSeconds);

                let difference = '';
                if (days > 0) { difference += (days === 1) ? `${days}:` : `${days}:`; }

                difference += (hours < 10 ) ? `0${hours}:` : `${hours}:`;
                difference += (minutes < 10 ) ? `0${minutes}:` : `${minutes}:`;
                difference += (seconds < 10 ) ? `0${seconds}` : `${seconds}`;
                return difference;
            }
        }
        // function refreshOwl($owl){
        //     resizing = true;
        //     $owl = $owl[0];

        //     $owl.enter('refreshing');
        //     $owl.trigger('refresh');

        //     $owl.setup();

        //     $owl.optionsLogic();

        //     $owl.$element.addClass($owl.options.refreshClass);

        //     $owl.update();

        //     if (!resizing) {
        //         $owl.onResize();
        //     }

        //     $owl.$element.removeClass($owl.options.refreshClass);

        //     $owl.leave('refreshing');
        //     $owl.trigger('refreshed');
        // }
    // PRODUCT

    // FAQ
        $('input[type=radio][name=show_faq]').change(function(){
            if (this.value == 'N'){
                $('#show_faq_div').hide();
                $("#main_iframe").contents().find('#faq_sec').hide();
            } else if (this.value == 'Y'){
                $('#show_faq_div').show();
                $("#main_iframe").contents().find('#faq_sec').show();
            }
            updateTemplate();
        });
        $('#save_faq_head').click(function(){
            if($("#myFaqHeadForm").valid()) updateTemplate();
        });
        $('#faq_main_title').keyup(function(){
            $("#main_iframe").contents().find('.faq_heading').text(this.value);
        });
        $('#faq_main_desc').keyup(function(){
            $("#main_iframe").contents().find('.faq_description').text(this.value);
        });
        $('#add_faq').click(function(){
            faq_i = faq_i + 1;
            var j = parseInt($('.faq_section').length) + 1;
            var i = faq_i;
            console.log(i);
            if(i == 10){
                toastr.error("@lang('client_site.maximum_section_limit_reached')");
            } else {
                var html1 = title = icon = font = border = icon_font = head_background = body_background = faq_desc = "";
                if($('#template_number').val() == 1) {
                    font = "";
                    border = "color_secondary_border";
                    head_background = "color_primary_light";
                    body_background = "";
                    body_border = "";
                    faq_desc = "color_primary_light";
                    icon_font = "";
                    icon = "fa-plus";
                    heading_font = "";
                } else if($('#template_number').val() == 2) {
                    font = "color_secondary_font";
                    border = "color_primary_border";
                    head_background = "color_tertiary";
                    body_background = "";
                    body_border = "";
                    faq_desc = "color_primary_border";
                    icon_font = "";
                    icon = "fa-plus";
                    heading_font = "";
                } else if($('#template_number').val() == 3) {
                    font = "color_secondary_font";
                    border = "color_primary_border";
                    head_background = "color_secondary";
                    body_background = "";
                    body_border = "";
                    faq_desc = "color_primary_border";
                    icon_font = "color_primary_font";
                    icon = "fa-plus";
                    heading_font = "";
                } else if($('#template_number').val() == 4) {
                    font = "color_secondary_font";
                    border = "color_secondary_border";
                    head_background = "color_primary";
                    body_background = "color_primary";
                    body_border = "color_primary color_secondary_border";
                    faq_desc = "";
                    icon_font = "color_secondary_font";
                    icon = "fa-angle-double-down";
                    heading_font = "color_secondary_font";
                }
                html1 =
                `<div class="faq_${i} card-header collapsed ${head_background} ${border}" data-toggle="collapse" href="#collapse${i}">
                    <a class="card-title faq_title_${i} ${heading_font}">
                        This is a simply dummy question text show here?
                    </a>
                    <i class="fa ${icon} ${icon_font}" aria-hidden="true"></i>
                </div>
                <div id="collapse${i}" class="faq_${i} card-body ${body_border} collapse" data-parent="#accordion" >
                    <p class="faq_desc_${i} ${faq_desc}">
                        Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur mi gravida ac. Nunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet ante vitae ante facilisis lobortis temporamet ante vitae ante facilisis lobortis sit amet consectetur adipiscfiing. Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur gravidaNunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet ante vitae ante.
                    </p>
                </div>`;
                var html =
                `<div class="card faq_section" id="faq_section_${i}" data-identity="0">
                    <form action="{{ route('store.landing.page.template') }}" id="myFaqForm${i}">
                        <div class="card-header d-flex" id="faqhead${i}" data-id="${i}">
                            <a href="javascript:;" class="btn btn-header-link acco-chap rem_tick collapsed"
                                data-toggle="collapse"
                                data-target="#faq${i}"
                                aria-expanded="true"
                                aria-controls="faq${i}"
                            >
                                <img src="{{ URL::to('public/frontend/images/cha.png') }}">
                                <h5>@lang('client_site.section') ${j}</h5>
                                <a href="javascript:;" class="btn del_faq" data-id="${i}" data-identity="0"> <i class="fa fa-times" aria-hidden="true"></i> </a>
                            </a>
                        </div>
                        <div id="faq${i}" class="collapse" aria-labelledby="faqhead${i}" data-parent="#faq">
                            <div class="card-body">
                                <div class="faq_detail" id="faq_${i}">
                                    <div class="cert_cntnt">
                                        <label for="faq_title_${i}">@lang('site.title')</label>
                                        <input type="text" name="faq_title_${i}" id="faq_title_${i}" class="form-control required faq_title" data-id="${i}" value="This is a simply dummy question text show here?">
                                    </div>
                                    <div class="cert_cntnt mb-3">
                                        <label for="faq_desc_${i}">@lang('site.description')</label>
                                        <textarea name="faq_desc_${i}" id="faq_desc_${i}" class="form-control required faq_desc" data-id="${i}">Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur mi gravida ac. Nunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet ante vitae ante facilisis lobortis temporamet ante vitae ante facilisis lobortis sit amet consectetur adipiscfiing. Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur gravidaNunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet ante vitae ante.</textarea>
                                    </div>
                                    <div class="sect_bottom_div">
                                        <p style="visibility: hidden;">
                                            <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                            @lang('client_site.saved_successfully')
                                        </p>
                                        <a href="javascript:;" class="faq_save btn btn-primary pull-right mb-3" id="faq_save_${i}" data-id="${i}" data-identity="0">@lang('client_site.save')</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>`;
                $('.faq_div').append(html);
                console.log($("#main_iframe").contents().find('#faq_sec').find('.accordionlanding1')[0]);
                $("#main_iframe").contents().find('#faq_sec').find('.accordionlanding1').find('.card').append(html1);
                changeColours($('#color_primary').val(), $('#color_secondary').val(), $('#color_tertiary').val());
            }
        });
        $(document).delegate('.faq_title', 'keyup', function(){
            var id = $(this).data('id');
            $("#main_iframe").contents().find('.faq_title_'+id).html(this.value);
        });
        $(document).delegate('.faq_desc', 'keyup', function(){
            var id = $(this).data('id');
            $("#main_iframe").contents().find('.faq_desc_'+id).html(this.value);
        });
        $(document).delegate('.faq_save', 'click', function(){
            var id = $(this).data('id');
            var idn = $(this).data('identity');
            var elem = $(this);
            console.log($("#myFaqForm"+id)[0]);
            console.log(idn);
            if($("#myFaqForm"+id).valid()){
                var uploadFormData = new FormData();
                uploadFormData.append("jsonrpc", "2.0");
                uploadFormData.append("_token", '{{csrf_token()}}');
                uploadFormData.append("landing_page_master_id", $('#template_id').val());
                uploadFormData.append("title", $('#faq_title_'+id).val());
                uploadFormData.append("description", $('#faq_desc_'+id).val());
                if(idn != 0){
                    uploadFormData.append("faq_id", idn);
                }
                $.ajax({
                    url: "{{route('landing.page.save.faq')}}",
                    method: 'post',
                    data: uploadFormData,
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(res){
                        console.log(res);
                        if(res.status == "success") {
                            toastr.success(res.message);
                            console.log("Res ID: " +res.details.id)
                            $('#faq_section_'+id).data('identity', res.details.id);
                            $('#faq_section_'+id).attr('data-identity', res.details.id);
                            $(elem).data('identity', res.details.id);
                            $(elem).attr('data-identity', res.details.id);
                            $(elem).parent().find('p').css('visibility', 'visible');
                            setTimeout(() => {
                                $(elem).parent().find('p').css('visibility', 'hidden');
                            }, 5000);
                        } else toastr.error(res.message);
                    },
                    error: function(err){
                        console.log(err);
                    },
                });
            }
        });
        $(document).delegate('.del_faq', 'click', function(){
            if($('.faq_section').length == 1){
                toastr.error("@lang('client_site.must_have_one_faq')")
            } else {
                var id = $(this).data('id');
                var idn = $('#faq_section_'+id).data('identity');
                if(idn == 0){
                    $('#faq_section_'+id).remove();
                    $("#main_iframe").contents().find('.faq_'+id).remove();
                    toastr.success("@lang('client_site.faq_deleted_successfully')");
                } else {
                    $.get( "{{url('/')}}/landing-page/delete-faq/"+idn, function( res ) {
                        if(res.status == "success"){
                            $('#faq_section_'+id).remove();
                            $("#main_iframe").contents().find('.faq_'+id).remove();
                            toastr.success(res.message);
                        } else toastr.error(res.message);
                    });
                }
            }
        });
    // FAQ

    // DESIGN
        $('#color_primary, #color_secondary, #color_tertiary').change(function(){
            changeColours($('#color_primary').val(), $('#color_secondary').val(), $('#color_tertiary').val());
        });
        $('#save_design').click(function(){
            if($("#myDesignForm").valid()){
                updateTemplate();
            }
        });
        function changeColours(primary, secondary, tertiary){
            console.log("Changing colours");
            var val = primary;
            var rgb = hexToRgb(val);
            var opacity = "0.3";
            if($('#template_number').val() == 4) opacity = "0.4";
            var rgba = [rgb.slice(0, rgb.length-1), `, ${opacity}`, rgb.slice(rgb.length-1)].join('');
            rgba = rgba.replace('rgb','rgba');
            var backclr = clr = "";

            $("#main_iframe").contents().find('.color_primary').css('background', val);
            $("#main_iframe").contents().find('.color_primary_font').css('color', val);
            $("#main_iframe").contents().find('.color_primary_border').css('border-color', val);
            $("#main_iframe").contents().find('.color_primary_hover').hover(function(){
				backclr = $(this).css('background-color');
				$(this).css('background', val);
				$(this).css('color', '#fff');
			}, function(){
				$(this).css('background', backclr);
				$(this).css('color', val);
				backclr = "";
			});
            $("#main_iframe").contents().find('.color_primary_font_hover').hover(function(){
			    clr = $(this).css('color');
                $(this).css('color', primary);
            },function(){
                $(this).css('color', clr);
			    clr = "";
            });
            $("#main_iframe").contents().find('.color_primary_owl').find('.owl-prev').css('color', primary).hover(function(){
                $(this).css('background', primary);
                $(this).css('color', '#fff');
            }, function(){
                $(this).css('background', 'transparent');
                $(this).css('color', primary);
            });
            $("#main_iframe").contents().find('.color_primary_owl').find('.owl-next').css('color', primary).hover(function(){
                $(this).css('background', primary);
                $(this).css('color', '#fff');
            }, function(){
                $(this).css('background', 'transparent');
                $(this).css('color', primary);
            });
		    $("#main_iframe").contents().find('.color_primary_owl').find('.owl-dot').css('background', val);
            $("#main_iframe").contents().find('.color_primary_light').css('background', rgba);

            $("#main_iframe").contents().find('.color_secondary').css('background', secondary);
            $("#main_iframe").contents().find('.color_secondary_font').css('color', secondary);
            $("#main_iframe").contents().find('.color_secondary_border').css('border-color', secondary);
            $("#main_iframe").contents().find('.color_secondary_hover').hover(function(){
				backclr = $(this).css('background-color');
				console.log(backclr);
				$(this).css('background', secondary);
			}, function(){
				$(this).css('background', backclr!="" ? backclr : 'transparent');
				backclr = "";
			});
            $("#main_iframe").contents().find('.color_secondary_font_hover').hover(function(){
                clr = $(this).css('color');
                $(this).css('color', secondary);
            },function(){
                console.log(clr);
                $(this).css('color', clr);
                clr = "";
            });
			$("#main_iframe").contents().find('.color_secondary_owl').find('.owl-dot').css('background', secondary);

            val = tertiary;
			console.log("Tertiary : " + val);
			rgb = hexToRgb(val);
			rgba = [rgb.slice(0, rgb.length-1), ", 0.9", rgb.slice(rgb.length-1)].join('');
			rgba = rgba.replace('rgb','rgba');
			clr = "";

            $("#main_iframe").contents().find('.color_tertiary').css('background', tertiary);
			$("#main_iframe").contents().find('.color_tertiary_light').css('background', rgba);
            $("#main_iframe").contents().find('.color_tertiary_font').css('color', tertiary);
            $("#main_iframe").contents().find('.color_tertiary_shadow').css('box-shadow', "15px 15px 0px 0px "+tertiary);
            $("#main_iframe").contents().find('.color_tertiary_hover').hover(function(){
			    backclr = $(this).css('background-color');
			    console.log(backclr);
                $(this).css('background', tertiary);
            }, function(){
                $(this).css('background', backclr!="" ? backclr : 'transparent');
                backclr = "";
            });
        }
        function changeTimerColours(primary, secondary, tertiary){
            if($('#template_number').val() == 1){
                $("#main_iframe").contents().find('.counter_num').css('background', secondary);
            } else if($('#template_number').val() == 2){
                $("#main_iframe").contents().find('.counter_num').css('background', primary);
                $("#main_iframe").contents().find('.header-timer-hours').find('.color_primary_font').css('color', primary);
            } else if($('#template_number').val() == 3){
                $("#main_iframe").contents().find('.counter_num').css('background', primary);
                $("#main_iframe").contents().find('.header-timer-hours').find('.color_secondary_font').css('color', secondary);
            } else if($('#template_number').val() == 4){
                $("#main_iframe").contents().find('.counter_num').css('background', primary);
            }
        }
        function callChangeColours(){
            changeColours($('#color_primary').val(), $('#color_secondary').val(), $('#color_tertiary').val());
        }
        function callChangeTimerColors(){
            changeTimerColours($('#color_primary').val(), $('#color_secondary').val(), $('#color_tertiary').val());
        }
    // DESIGN

    // LOGO
        $('#myLogoForm').validate();
        $('#landing_logo').change(function(e){
            var file = this.files[0];
            var reader = new FileReader();
            reader.onloadend = function (e) {
                $("#main_iframe").contents().find('.landing_logo').attr('src', e.target.result);
            }
            if (file) {
                reader.readAsDataURL(file);
            } else {
                val = $('#template_number').val();
                $("#main_iframe").contents().find('.header_image').attr('src', "{{URL::asset('public/frontend/')}}/landing_page"+val+"/images/logo.png");
            }
        });
        $('#save_logo').click(function(){
            if($('#myLogoForm').valid()){
                updateTemplate();
                $('#del_landing_logo').show();
            }
        });
        $('#link_linkedin').keyup(function(){
            if(this.value != ""){
                $("#main_iframe").contents().find('.link_icons_linkedin').show();
                $("#main_iframe").contents().find('.link_icons_linkedin').find('a').attr('href', this.value);
            } else {
                $("#main_iframe").contents().find('.link_icons_linkedin').hide();
                $("#main_iframe").contents().find('.link_icons_linkedin').find('a').attr('href', 'javascript:;');
            }
        });
        $('#link_facebook').keyup(function(){
            if(this.value != ""){
                $("#main_iframe").contents().find('.link_icons_facebook').show();
                $("#main_iframe").contents().find('.link_icons_facebook').find('a').attr('href', this.value);
            } else {
                $("#main_iframe").contents().find('.link_icons_facebook').hide();
                $("#main_iframe").contents().find('.link_icons_facebook').find('a').attr('href', 'javascript:;');
            }
        });
        $('#link_twitter').keyup(function(){
            if(this.value != ""){
                $("#main_iframe").contents().find('.link_icons_twitter').show();
                $("#main_iframe").contents().find('.link_icons_twitter').find('a').attr('href', this.value);
            } else {
                $("#main_iframe").contents().find('.link_icons_twitter').hide();
                $("#main_iframe").contents().find('.link_icons_twitter').find('a').attr('href', 'javascript:;');
            }
        });
        $('#link_instagram').keyup(function(){
            if(this.value != ""){
                $("#main_iframe").contents().find('.link_icons_instagram').show();
                $("#main_iframe").contents().find('.link_icons_instagram').find('a').attr('href', this.value);
            } else {
                $("#main_iframe").contents().find('.link_icons_instagram').hide();
                $("#main_iframe").contents().find('.link_icons_instagram').find('a').attr('href', 'javascript:;');
            }
        });
        $('#link_pinterest').keyup(function(){
            if(this.value != ""){
                $("#main_iframe").contents().find('.link_icons_pinterest').show();
                $("#main_iframe").contents().find('.link_icons_pinterest').find('a').attr('href', this.value);
            } else {
                $("#main_iframe").contents().find('.link_icons_pinterest').hide();
                $("#main_iframe").contents().find('.link_icons_pinterest').find('a').attr('href', 'javascript:;');
            }
        });
        $('#link_youtube').keyup(function(){
            if(this.value != ""){
                $("#main_iframe").contents().find('.link_icons_youtube').show();
                $("#main_iframe").contents().find('.link_icons_youtube').find('a').attr('href', this.value);
            } else {
                $("#main_iframe").contents().find('.link_icons_youtube').hide();
                $("#main_iframe").contents().find('.link_icons_youtube').find('a').attr('href', 'javascript:;');
            }
        });
        $('#link_tiktok').keyup(function(){
            if(this.value != ""){
                $("#main_iframe").contents().find('.link_icons_tiktok').show();
                $("#main_iframe").contents().find('.link_icons_tiktok').find('a').attr('href', this.value);
            } else {
                $("#main_iframe").contents().find('.link_icons_tiktok').hide();
                $("#main_iframe").contents().find('.link_icons_tiktok').find('a').attr('href', 'javascript:;');
            }
        });
        $('#del_landing_logo').click(function(){
            var id = $('#template_id').val();
            $.get( "{{url('/')}}/landing-page/reset-landing-logo/"+id, function( res ) {
                if(res.status == "success"){
                    $("#main_iframe").contents().find('.landing_logo').attr('src', "{{URL::asset('public/frontend/landing_page1/images/logo.png')}}");
                    $('#del_landing_logo').hide();
                    toastr.success(res.message);
                } else toastr.error(res.message);
            });
        });
    // LOGO

    function updateTemplate(){
        var style = "";
        var uploadFormData = new FormData();
        uploadFormData.append("jsonrpc", "2.0");
        uploadFormData.append("_token", '{{csrf_token()}}');
        uploadFormData.append('template_number', $('#template_number').val());

        uploadFormData.append('landing_logo', $('#landing_logo').val() != "" ? $('#landing_logo')[0].files[0] : null);
        uploadFormData.append('link_facebook', $('#link_facebook').val());
        uploadFormData.append('link_linkedin', $('#link_linkedin').val());
        uploadFormData.append('link_twitter', $('#link_twitter').val());
        uploadFormData.append('link_instagram', $('#link_instagram').val());
        uploadFormData.append('link_pinterest', $('#link_pinterest').val());
        uploadFormData.append('link_youtube', $('#link_youtube').val());
        uploadFormData.append('link_tiktok', $('#link_tiktok').val());

        uploadFormData.append('header_image', $('#header_image').val() != "" ? $('#header_image')[0].files[0] : null);
        uploadFormData.append('header_line_1', $('#header_line_1').val());
        uploadFormData.append('header_line_2', $('#header_line_2').val());
        uploadFormData.append('header_button', $('input[type=radio][name=header_btn]:checked').val());
        uploadFormData.append('header_button_text', $('#header_button_text').val());
        uploadFormData.append('header_button_type', $('input[type=radio][name=header_button_type]:checked').val());
        uploadFormData.append('header_button_link', $('#header_button_link').val());
        uploadFormData.append('lead_position', $('input[type=radio][name=lead_position]:checked').val());
        uploadFormData.append('lead_section_heading', $('#lead_section_heading').val());
        uploadFormData.append('lead_section_footer_submit_btn', $('#lead_section_footer_submit_btn').val());
        uploadFormData.append('lead_section_desc', $('#lead_section_desc').val());
        uploadFormData.append('lead_fname', $('#lead_fname').is(':checked') ? 'Y' : 'N');
        uploadFormData.append('lead_lname', $('#lead_lname').is(':checked') ? 'Y' : 'N');
        uploadFormData.append('lead_email', $('#lead_email').is(':checked') ? 'Y' : 'N');
        uploadFormData.append('lead_country_code', $('#lead_country_code').is(':checked') ? 'Y' : 'N');
        uploadFormData.append('lead_mobile', $('#lead_mobile').is(':checked') ? 'Y' : 'N');
        uploadFormData.append('lead_address', $('#lead_address').is(':checked') ? 'Y' : 'N');
        uploadFormData.append('lead_city', $('#lead_city').is(':checked') ? 'Y' : 'N');
        uploadFormData.append('lead_state', $('#lead_state').is(':checked') ? 'Y' : 'N');
        uploadFormData.append('lead_postal_code', $('#lead_postal_code').is(':checked') ? 'Y' : 'N');
        uploadFormData.append('lead_facebook', $('#lead_facebook').is(':checked') ? 'Y' : 'N');
        uploadFormData.append('lead_instagram', $('#lead_instagram').is(':checked') ? 'Y' : 'N');
        uploadFormData.append('lead_linkedin', $('#lead_linkedin').is(':checked') ? 'Y' : 'N');
        uploadFormData.append('lead_website', $('#lead_website').is(':checked') ? 'Y' : 'N');
        uploadFormData.append('lead_message', $('#lead_message').is(':checked') ? 'Y' : 'N');
        uploadFormData.append('lead_success_button', $('input[type=radio][name=lead_success_button]:checked').val());
        uploadFormData.append('lead_success_heading', $('#lead_success_heading').val());
        uploadFormData.append('lead_success_desc', $('#lead_success_desc').val());
        uploadFormData.append('lead_success_button_caption', $('#lead_success_button_caption').val());
        uploadFormData.append('lead_privacy_policy', $('#lead_privacy_policy').val());
        uploadFormData.append('lead_download_link', $('#lead_download_link').val());
        uploadFormData.append('header_reverse', $('input[type=radio][name=header_reverse]:checked').val());
        uploadFormData.append('header_counter_date', $('#header_counter_date').val());

        uploadFormData.append('why_us_section', $('input[type=radio][name=why_us_section]:checked').val());
        uploadFormData.append('why_us_heading', $('#whyus_main_title').val());
        uploadFormData.append('why_us_description', $('#whyus_main_desc').val());

        uploadFormData.append('body_single_content', $('input[type=radio][name=body_single_content]:checked').val());
        uploadFormData.append('body_single_content_image', $('#body_single_content_image').val() != "" ? $('#body_single_content_image')[0].files[0] : null);
        uploadFormData.append('body_single_content_heading', $('#body_single_content_heading').val());
        uploadFormData.append('body_single_content_desc', $('#body_single_content_desc').val());
        uploadFormData.append('body_single_content_button', $('input[type=radio][name=body_single_content_button]:checked').val());
        uploadFormData.append('body_single_content_button_text', $('#body_single_content_button_text').val());
        uploadFormData.append('body_single_content_button_link', $('#body_single_content_button_link').val());

        uploadFormData.append('product_section_heading', $('#product_section_heading').val());
        uploadFormData.append('product_section_desc', $('#product_section_desc').val());
        uploadFormData.append('product_slider', $('input[type=radio][name=product_slider]:checked').val());

        uploadFormData.append('show_faq', $('input[type=radio][name=show_faq]:checked').val());

        uploadFormData.append('color_primary', $('#color_primary').val());
        uploadFormData.append('color_secondary', $('#color_secondary').val());
        uploadFormData.append('color_tertiary', $('#color_tertiary').val());

        uploadFormData.append('single_video', $('input[type=radio][name=single_video]:checked').val());
        uploadFormData.append('single_file', $('#single_file').val() != "" ? $('#single_file')[0].files[0] : null);
        uploadFormData.append('body_single_video_heading', $('#body_single_video_heading').val());
        uploadFormData.append('body_single_video_desc', $('#body_single_video_desc').val());

        uploadFormData.append('faq_heading', $('#faq_main_title').val());
        uploadFormData.append('faq_description', $('#faq_main_desc').val());
        uploadFormData.append('image_style',  $('input[type=radio][name=image_style_single]:checked').val());


        $.ajax({
            url: "{{url('/')}}/landing-page/store/"+template_id,
            method: 'post',
            data: uploadFormData,
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            success: function(res){
                console.log(res);
                if(res.status == "success") {
                    toastr.success(res.message);
                    template_id = res.template.id;
                    detail = res.template;
                    var temp4Class = "";
                    if($('#template_number').val() == 4) temp4Class = "color_secondary_border color_tertiary_shadow";
                    console.log("TEMP ID: "+template_id);
                    $('#background').val('');
                    $('.template'+ $('#template_number').val() ).attr('style', style);
                    $('#save_single').removeAttr('disabled');
                    if(detail.body_single_video_type == "I"){
                        $("#main_iframe").contents().find('.single_vdo').empty().append(`<img width="100%" src="{{URL::to('storage/app/public/uploads/landing_page')}}/`+detail.body_single_video_filename+`" class="${temp4Class}" />`);
                        $("#main_iframe").contents().find('.paly_btn').hide();
                    } else {
                        $("#main_iframe").contents().find('.single_vdo').empty().append(`<video width="100%" src="{{URL::to('storage/app/public/uploads/landing_page')}}/`+detail.body_single_video_filename+`" class="${temp4Class}"></video>`);
                        $("#main_iframe").contents().find('.paly_btn').show();
                    }
                    $('#single_file').val('');
                } else toastr.error(res.message);
            },
            error: function(err){
                console.log(err);
            },
        });
    }

    let hexToRgb= c=> `rgb(${c.match(/\w\w/g).map(x=>+`0x${x}`)})`;

    function timeDiffCalc(dateFuture, dateNow) {
        if(Math.abs(dateFuture - dateNow) == 0){
            return 0;
        } else {
            let diffInMilliSeconds = Math.abs(dateFuture - dateNow) / 1000;
            const days = Math.floor(diffInMilliSeconds / 86400);
            diffInMilliSeconds -= days * 86400;
            const hours = Math.floor(diffInMilliSeconds / 3600) % 24;
            diffInMilliSeconds -= hours * 3600;
            const minutes = Math.floor(diffInMilliSeconds / 60) % 60;
            diffInMilliSeconds -= minutes * 60;
            const seconds = Math.round(diffInMilliSeconds);

            let difference = '<ul class="counter_ul">';

            if($('#template_number').val() == 1){
                if (days > 0) { difference += (days === 1) ? `<li class="header-timer-hours"><p class="counter_num color_secondary">${days}</p><p class="color_secondary_font">days</p></li>` : `<li class="header-timer-hours"><p class="counter_num color_secondary">${days}</p><p class="color_secondary_font">days</p></li>`; }

                difference += (hours < 10 ) ? `<li class="header-timer-hours"><p class="counter_num color_secondary">0${hours}</p><p class="color_secondary_font">hours</p></li>` : `<li class="header-timer-hours"><p class="counter_num color_secondary">${hours}</p><p class="color_secondary_font">hours</p></li>`;
                difference += (minutes < 10 ) ? `<li class="header-timer-hours"><p class="counter_num color_secondary">0${minutes}</p><p class="color_secondary_font">minutos</p></li>` : `<li class="header-timer-hours"><p class="counter_num color_secondary">${minutes}</p><p class="color_secondary_font">minutos</p></li>`;
                difference += (seconds < 10 ) ? `<li class="header-timer-hours"><p class="counter_num color_secondary">0${seconds}</p><p class="color_secondary_font">segundos</p></li>` : `<li class="header-timer-hours"><p class="counter_num color_secondary">${seconds}</p><p class="color_secondary_font">segundos</p></li>`;
            } else if($('#template_number').val() == 2){
                if (days > 0) { difference += (days === 1) ? `<li class="header-timer-hours"><p class="counter_num color_primary">${days}</p><p class="color_primary_font">days</p></li>` : `<li class="header-timer-hours"><p class="counter_num color_primary">${days}</p><p class="color_primary_font">days</p></li>`; }

                difference += (hours < 10 ) ? `<li class="header-timer-hours"><p class="counter_num color_primary">0${hours}</p><p class="color_primary_font">hours</p></li>` : `<li class="header-timer-hours"><p class="counter_num color_primary">${hours}</p><p class="color_primary_font">hours</p></li>`;
                difference += (minutes < 10 ) ? `<li class="header-timer-hours"><p class="counter_num color_primary">0${minutes}</p><p class="color_primary_font">minutos</p></li>` : `<li class="header-timer-hours"><p class="counter_num color_primary">${minutes}</p><p class="color_primary_font">minutos</p></li>`;
                difference += (seconds < 10 ) ? `<li class="header-timer-hours"><p class="counter_num color_primary">0${seconds}</p><p class="color_primary_font">segundos</p></li>` : `<li class="header-timer-hours"><p class="counter_num color_primary">${seconds}</p><p class="color_primary_font">segundos</p></li>`;
            } else if($('#template_number').val() == 3){
                if (days > 0) { difference += (days === 1) ? `<li class="header-timer-hours"><p class="counter_num color_primary">${days}</p><p class="">days</p></li>` : `<li class="header-timer-hours"><p class="counter_num color_primary">${days}</p><p class="">days</p></li>`; }

                difference += (hours < 10 ) ? `<li class="header-timer-hours"><p class="counter_num color_primary">0${hours}</p><p class="">hours</p></li>` : `<li class="header-timer-hours"><p class="counter_num color_primary">${hours}</p><p class="">hours</p></li>`;
                difference += (minutes < 10 ) ? `<li class="header-timer-hours"><p class="counter_num color_primary">0${minutes}</p><p class="">minutos</p></li>` : `<li class="header-timer-hours"><p class="counter_num color_primary">${minutes}</p><p class="">minutos</p></li>`;
                difference += (seconds < 10 ) ? `<li class="header-timer-hours"><p class="counter_num color_primary">0${seconds}</p><p class="">segundos</p></li>` : `<li class="header-timer-hours"><p class="counter_num color_primary">${seconds}</p><p class="">segundos</p></li>`;
            } else if($('#template_number').val() == 4){
                if (days > 0) { difference += (days === 1) ? `<li class="header-timer-hours"><p class="counter_num color_primary">${days}</p><p class="">days</p></li>` : `<li class="header-timer-hours"><p class="counter_num color_primary">${days}</p><p class="">days</p></li>`; }

                difference += (hours < 10 ) ? `<li class="header-timer-hours"><p class="counter_num color_primary">0${hours}</p><p class="">hours</p></li>` : `<li class="header-timer-hours"><p class="counter_num color_primary">${hours}</p><p class="">hours</p></li>`;
                difference += (minutes < 10 ) ? `<li class="header-timer-hours"><p class="counter_num color_primary">0${minutes}</p><p class="">minutos</p></li>` : `<li class="header-timer-hours"><p class="counter_num color_primary">${minutes}</p><p class="">minutos</p></li>`;
                difference += (seconds < 10 ) ? `<li class="header-timer-hours"><p class="counter_num color_primary">0${seconds}</p><p class="">segundos</p></li>` : `<li class="header-timer-hours"><p class="counter_num color_primary">${seconds}</p><p class="">segundos</p></li>`;
            }

            difference += "</ul>";
            return difference;
        }
    }

</script>
<script>
    var rowSize   = 100; // => container height / number of items
    var container = document.querySelector("#faq");
    var listItems = Array.from(document.querySelectorAll(".list-item")); // Array of elements
    var sortables = listItems.map(Sortable); // Array of sortables
    var total     = sortables.length;

    TweenLite.to(container, 0.5, { autoAlpha: 1 });

    function changeIndex(item, to) {

        // Change position in array
        arrayMove(sortables, item.index, to);

        // Change element's position in DOM. Not always necessary. Just showing how.
        if (to === total - 1) {
            container.appendChild(item.element);
        } else {
            var i = item.index > to ? to : to + 1;
            container.insertBefore(item.element, container.children[i]);
        }

        // Set index for each sortable
        sortables.forEach((sortable, index) => sortable.setIndex(index));
    }

    function Sortable(element, index) {

        var content = element.querySelector(".item-content");
        var order   = element.querySelector(".order");

        var animation = TweenLite.to(content, 0.3, {
            boxShadow: "rgba(0,0,0,0.2) 0px 16px 32px 0px",
            force3D: true,
            scale: 1.1,
            paused: true
        });

        var dragger = new Draggable(element, {
            onDragStart: downAction,
            onRelease: upAction,
            onDrag: dragAction,
            cursor: "inherit",
            type: "y"
        });

        // Public properties and methods
        var sortable = {
            dragger:  dragger,
            element:  element,
            index:    index,
            setIndex: setIndex
        };

        TweenLite.set(element, { y: index * rowSize });

        function setIndex(index) {

            sortable.index = index;
            order.textContent = index + 1;

            // Don't layout if you're dragging
            if (!dragger.isDragging) layout();
        }

        function downAction() {
            animation.play();
            this.update();
        }

        function dragAction() {

            // Calculate the current index based on element's position
            var index = clamp(Math.round(this.y / rowSize), 0, total - 1);

            if (index !== sortable.index) {
            changeIndex(sortable, index);
            }
        }

        function upAction() {
            animation.reverse();
            layout();
        }

        function layout() {
            TweenLite.to(element, 0.3, { y: sortable.index * rowSize });
        }

        return sortable;
    }

    // Changes an elements's position in array
    function arrayMove(array, from, to) {
        array.splice(to, 0, array.splice(from, 1)[0]);
    }

    // Clamps a value to a min/max
    function clamp(value, a, b) {
        return value < a ? a : (value > b ? b : value);
    }

</script>
@endsection
