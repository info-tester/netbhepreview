@extends('layouts.app')
@section('title')

    @lang('site.landing_page')
@endsection
@section('style')
@include('includes.style')
<style>
    .bt_m{
        margin-bottom: 6px !important;
    }
    .success_bt {
        background: #28a745 !important;
    }
</style>
@endsection
@section('scripts')
@include('includes.scripts')
<script src="{{URL::to('public/js/chosen.jquery.min.js')}}"></script>
@endsection
@section('header')
@include('includes.professional_header')


<link rel="stylesheet" href="{{URL::to('public/css/chosen.css')}}">
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.landing_page')</h2>

        <div class="bokcntnt-bdy no-margin-top">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0 && $user->sell!='PS')
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>

            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">

                <div class="" style="float: right;">
                    @if($free==1)
                    <a class="btn btn-success" href="{{route('create.landing.page.template')}}">+ @lang('site.add_new_template')</a>
                    @elseif($free==0)
                    <a class="btn btn-success" href="javascript:;" id="add_new_template"> + @lang('site.add_new_template')</a>
                    @endif
                    <a class="btn btn-success" href="{{route('landing.page.payment')}}"> @lang('client_site.view') @lang('client_site.payment')</a>
                </div>

                <div class="buyer_table">
                    <div class="table-responsive">
                        @if(sizeof(@$templates)>0)
                        <div class="table">
                            <div class="one_row1 hidden-sm-down only_shawo">
                                <div class="cell1 tab_head_sheet">@lang('client_site.landing_page_title')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.template')</div>
                                <div class="cell1 tab_head_sheet">@lang('client_site.date')</div>
                                <div class="cell1 tab_head_sheet">@lang('client_site.page_type')</div>
                                <div class="cell1 tab_head_sheet">@lang('client_site.branding_free')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.status')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.action')</div>
                            </div>
                            <!--row 1-->

                                @foreach(@$templates as $k=>$detail)
                                    <div class="one_row1 small_screen31">
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('client_site.landing_page_title')</span>
                                            <p class="add_ttrr">{{@$detail->landing_title}}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.template')</span>
                                            <p class="add_ttrr">@lang('site.template') {{@$detail->landing_template_id}}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('client_site.date')</span>
                                            <p class="add_ttrr">{{toUserTime(@$detail->created_at,'Y-m-d')}}</p>
                                        </div>

                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('client_site.page_type')</span>
                                            <p class="add_ttrr">
                                                @if(@$detail->page_type=='P')
                                                @lang('site.paid')
                                                @elseif(@$detail->page_type=='F')
                                                @lang('client_site.free')
                                                @endif
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('client_site.branding_free')</span>
                                            <p class="add_ttrr">
                                                @if(@$detail->is_branding_free=='Y')
                                                @lang('site.yes')
                                                @elseif(@$detail->is_branding_free=='N')
                                                @lang('site.no')
                                                @endif
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.status')</span>
                                            <p class="add_ttrr">
                                                @if(@$detail->page_status=='B')
                                                @lang('client_site.blocked')
                                                @elseif(@$detail->page_status=='P')
                                                @lang('client_site.published')
                                                @elseif(@$detail->page_status=='UP')
                                                @lang('client_site.unpublished')
                                                @endif
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">

                                            @if($detail->page_status!='B')
                                            <a href="{{ route('edit.landing.page.template', ['id'=>$detail->id]) }}" title="@lang('site.edit')" class="acpt editbtn bt_m">@lang('site.edit')</a>
                                            @endif
                                            <a href="javascript:;" title="@lang('site.copy_product_link')"  class="acpt bt_m" onclick="copyText(`{{url('/')}}/l/{{auth()->user()->slug}}/{{@$detail->slug}}`)">@lang('site.copy_link')</a>
                                            <a href="{{ route('delete.landing.page.template', ['id'=>$detail->id]) }}" title="Delete" class="del_cert rjct bt_m">@lang('site.delete')</a>
                                            <a href="{{route('landing.page.leads',['id'=>$detail->id])}}" title="@lang('client_site.purchase')" class="acpt assginbtn bt_m">@lang('client_site.view_leads')</a>
                                            @if($detail->is_branding_free=='N' && $detail->page_type!='F')
                                            <a href="javascript:;" data-link="{{ route('landing.page.payment.create', ['template_id'=>$detail->id,'banding'=>'Y']) }}" data-num="{{(int)$landing_pricing->price_branding_free}}" title="@lang('client_site.purchase')" class="acpt bt_m purchs_brnd_free" >@lang('client_site.branding_free')</a>
                                            @endif
                                            @if($detail->page_status!='B')
                                                @if($detail->page_status=='UP')
                                                    <a href="{{route('landing.page.publish',['id'=>$detail->id])}}" title="@lang('client_site.publish')" class="acpt bt_m success_bt">@lang('client_site.publish')</a>
                                                @elseif($detail->page_status=='P')
                                                    <a href="{{route('landing.page.unpublish',['id'=>$detail->id])}}" title="@lang('client_site.unpublish')" class="acpt bt_m success_bt">@lang('client_site.unpublish')</a>
                                                @endif
                                            @endif

                                        </div>
                                    </div>
                                @endforeach

                        </div>
                        @else

                            <div class="one_row small_screen31">
                                <center><span><h3 class="error">{{__('site.oops!_not_found')}}</h3></span></center>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

<!--View tool help Modal start -->
<div class="modal fade" id="view_tool_help_Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">@lang('client_site.landing_page') @lang('client_site.purchase') <span class="tool-title"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <h5> @lang('client_site.landing_creation_limit_reached')</h5>
                <label>@lang('client_site.landing_page') @lang('site.price') : R${{$landing_pricing->price_per_page}}</label>

                <br>
                <label >@lang('client_site.branding_free') @lang('site.price') : R${{$landing_pricing->price_branding_free}}</label>
                <label for="radio_1" class="personal-label">@lang('client_site.want_branding_free')</label>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom:15px">
                    <div class="your-mail">
                        {{-- <label for="radio_1" class="personal-label">@lang('client_site.want_branding_free')</label> --}}
                        <input type="radio" name="branding" id="radio_1" value="Y"><span class="tool-title" for="radio_1">@lang('site.yes')</span>
                        {{-- <label for="radio_2" class="personal-label">No</label> --}}
                        <input type="radio" name="branding" id="radio_2" value="N" checked><span class="tool-title" for="radio_2">@lang('site.no')</span>
                    </div>
                </div>
                <label id="tot_price">@lang('site.total_amount') : R${{$landing_pricing->price_per_page}}</label>
                <span class="tool-help-desc-view"></span>
                {{-- <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom:15px">
                <button type="submit" value="submit" class="btn btn-primary">@lang('client_site.purchase')</button>
                </div> --}}
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" id="parches_temp">@lang('client_site.purchase') @lang('client_site.landing_page')</button>
            </div>
        </div>
    </div>
</div>
<!--View tool help Modal end -->



</section>




@endsection
@section('footer')
@include('includes.footer')

<script>

function copyText(text){
    var input = document.createElement('input');
    input.setAttribute('value', text);
    document.body.appendChild(input);
    input.select();
    var result = document.execCommand('copy');
    document.body.removeChild(input);
    toastr.success("Link Copied");
    return result;
}

$('.purchs_brnd_free').click(function(){
    var num = $(this).data('num');
    var link = $(this).data('link');
    if(confirm(`@lang('client_site.confirm_branding_free_purchase') $${num}`)){
        // console.log(link);
        window.location.href = link;
    }
});

$(document).ready(function(){
    $('.del_cert').click(function(e){
        if(confirm('Do you really want to delete this certificate template?')){
            if($(this).data('assigned') == 'Y'){
                toastr.error('Cannot delete certificate as it is already assigned to product');
                e.preventDefault();
                return false;
            } else {
                return true;
            }
        } else {
            e.preventDefault();
            return false;
        }
    })

    // $('.view_tools_help').click(function(){
    //     var title = $(this).data('title');
    //     var help_desc = $(this).data('helpdesc');
    //     $('.tool-title').html(title);
    //     $('.tool-help-desc-view').html(help_desc);
    //     $('#view_tool_help_Modal').modal('show');
    // });
    $('#add_new_template').click(function(){
        $('#view_tool_help_Modal').modal('show');
    });
    $(document).delegate('input[type=radio][name=branding]', 'change', function(){
        console.log(this.value);
        var t_price='{{$landing_pricing->price_per_page}}';
        var b_price ='{{$landing_pricing->price_branding_free}}';
        if(this.value=='Y'){
            $('#tot_price').html('Total Payable : R$'+(parseFloat(t_price)+parseFloat(b_price)).toFixed(2));
        }
        if(this.value=='N'){
            $('#tot_price').html('Total Payable : R$'+t_price);
        }
    });
    $('#parches_temp').click(function(){
        var band_check= $(`input[type=radio][name=branding]:checked`).val();
        if(band_check=='Y'){
            window.location.href="{{route('landing.page.payment.create',['template'=>'Y'])}}&banding=Y";
        }
        if(band_check=='N'){
            window.location.href='{{route('landing.page.payment.create',['template'=>'Y'])}}'
        }
    })

});

$(".chosen-select").chosen();

$(document).ready(function(){
    $("#myform").validate();
});
</script>
<script>
 function deleteCategory(val){
    var confirm = window.confirm('Do you want to delete this question?');
    if(confirm){
        $("#destroy").attr('action',val);
        $("#destroy").submit();
    }
 }
</script>
<style>
    .error{
        color: red !important;
    }
</style>

@endsection
