@extends('layouts.app')
@section('title')

@lang('client_site.landing_page_payments')
@endsection
@section('style')
@include('includes.style')
<style>
    .bt_m{
        margin-bottom: 6px !important;
    }
</style>
@endsection
@section('scripts')
@include('includes.scripts')
<script src="{{URL::to('public/js/chosen.jquery.min.js')}}"></script>
@endsection
@section('header')
@include('includes.professional_header')


<link rel="stylesheet" href="{{URL::to('public/css/chosen.css')}}">
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('client_site.landing_page_payments')</h2>

        <div class="bokcntnt-bdy no-margin-top">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>

            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">

                <div class="" style="float: right;">
                    {{-- @if($free==1)
                    <a class="btn btn-success" href="{{route('create.landing.page.template')}}">+ @lang('site.add_new_template')</a>
                    @elseif($free==0)
                    <a class="btn btn-success" href="javascript:;" id="add_new_template"> + @lang('site.add_new_template')</a>
                    @endif --}}
                    <a class="btn btn-success" href="{{route('list.landing.page.templates')}}" id="add_new_template"> <- Back</a>
                </div>

                <div class="buyer_table">
                    <div class="table-responsive">
                        @if(sizeof(@$payments)>0)
                        <div class="table">
                            <div class="one_row1 hidden-sm-down only_shawo">
                                <div class="cell1 tab_head_sheet">@lang('client_site.order_no')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.purchase_date')</div>
                                <div class="cell1 tab_head_sheet">@lang('client_site.landing_page_title')</div>
                                <div class="cell1 tab_head_sheet">@lang('client_site.payment_for')</div>
                                <div class="cell1 tab_head_sheet">@lang('client_site.is_used')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.payment_type')</div>
                                <div class="cell1 tab_head_sheet">@lang('client_site.amount')</div>
                            </div>
                            <!--row 1-->

                                @foreach(@$payments as $k=>$detail)
                                    <div class="one_row1 small_screen31">
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('client_site.order_no')</span>
                                            <p class="add_ttrr">{{@$detail->token_no}}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.purchase_date')</span>
                                            <p class="add_ttrr">{{toUserTime(@$detail->created_at,'Y-m-d')}}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('client_site.landing_page_title')</span>
                                            <p class="add_ttrr">{{@$detail->landingDetails->landing_title??'--'}}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('client_site.payment_for')</span>
                                            <p class="add_ttrr">
                                                @if(@$detail->payment_for=='T') @lang('client_site.template')
                                                @elseif(@$detail->payment_for=='B') @lang('client_site.branding')
                                                @elseif(@$detail->payment_for=='BT') @lang('client_site.template_n_branding')
                                                @endif
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('client_site.is_used')</span>
                                            <p class="add_ttrr">
                                                @if(@$detail->is_use=='Y') Yes
                                                @elseif(@$detail->is_use=='N')No
                                                @endif
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.payment_type')</span>
                                            <p class="add_ttrr">
                                                @if(@$detail->payment_type=='C') WireCard
                                                @elseif(@$detail->payment_type=='S') Stripe
                                                @elseif(@$detail->payment_type=='P') Paypal
                                                @endif
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('client_site.amount')</span>
                                            <p class="add_ttrr">R$ {{(int)@$detail->total_amount}}</p>
                                        </div>
                                    </div>
                                @endforeach

                        </div>
                        @else

                            <div class="one_row small_screen31">
                                <center><span><h3 class="error">{{__('site.oops!_not_found')}}</h3></span></center>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>


</section>




@endsection
@section('footer')
@include('includes.footer')

<script>



$(".chosen-select").chosen();

$(document).ready(function(){
    $("#myform").validate();
});
</script>
<script>
 function deleteCategory(val){
    var confirm = window.confirm('Do you want to delete this question?');
    if(confirm){
        $("#destroy").attr('action',val);
        $("#destroy").submit();
    }
 }
</script>

@endsection
