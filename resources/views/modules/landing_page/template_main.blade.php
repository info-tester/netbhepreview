@if(@$page->landing_template_id == 1)

    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <meta name="description" content="">
            <meta name="author" content="">
            <title>Netbhe</title>
            <link href="{{URL::asset('public/frontend/landing_page1/css/bootstrap.css')}}" rel="stylesheet">
            <link href="{{URL::asset('public/frontend/landing_page1/css/style.css')}}" rel="stylesheet">
            <link href="{{URL::asset('public/frontend/landing_page1/css/responsive.css')}}" rel="stylesheet">
            <link href="{{URL::asset('public/frontend/landing_page1/css/font-awesome/all.min.css')}}" rel="stylesheet">
            <link href="{{URL::asset('public/frontend/landing_page1/css/jquery.fancybox.css')}}" rel="stylesheet">
            <link rel="preconnect" href="https://fonts.googleapis.com">
            <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
            <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&family=Raleway:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet"> 
        </head>
        <body>

            <header class="header_sec">
                <div class="container">
                    <nav class="navbar navbar-light nav_top">
                        <a class="navbar-brand" href="#"><img src="{{URL::asset('public/frontend/landing_page1/images/logo.png')}}" alt="logo" /></a>
                        <div class="header_sos">
                            <ul>
                                <li><a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>

                            </ul>
                        </div>
                    </nav>
                </div>
            </header>

            <div class="bannersec">
                <div class="container">
                    <div class="banner_box">
                        <div class="banner_text">
                            <h5 class="header_line_1"><span>Lorem</span> ipsum dolor sit </h5>
                            <p class="header_line_2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <a href="#" class="page_btn header_btn header_button_text" data-toggle="modal" data-target="#myModal">Contact Us</a>
                        </div>
                        <div class="banner_img">
                            <img src="{{URL::asset('public/frontend/landing_page1/images/ban-img2.png')}}" class="header_image" alt="">
                        </div>
                    </div>
                </div>
            </div>

            <div class="vedio_sec">
                <div class="container">
                    <div class="vedio_sec_inr">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="vedio_text">
                                    <h6> <span>Online</span> Learning </h6>
                                    <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla.</p>
                                    <a href="#url" class="page_btn">learn more</a>
                                </div>
                            </div>	
                            <div class="col-sm-6">
                                <div class="vedio_img">
                                    <div class="vedio_img_inr">
                                        <img src="{{URL::asset('public/frontend/landing_page1/images/vedioimg.jpg')}}" alt="">
                                        <div class="playIcon">
                                            <a href="http://www.youtube.com/watch?v=opj24KnzrWo" class="fancybox-media">
                                                <img src="{{URL::asset('public/frontend/landing_page1/images/playIcon.png')}}" alt="" rel="media-gallery"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single_box">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="single_img">
                                <img src="{{URL::asset('public/frontend/landing_page1/images/single_img.png')}}" alt="">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="single_text">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <section id="video-area" class="bg-gray ved_1">
                    <div class="container">
                        <div class="row">
                            <!--start heading-->
                            <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1">
                                <div class="sec-heading text-center">
                                    <h6>Product Demo</h6>
                                    <h2>Get product more information from the video</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.</p>
                                </div>
                            </div>
                            <!--end heading-->
                        </div>
                        <div class="row">
                            <!--start video player-->
                            <div class="col-lg-10 offset-lg-1">
                                <div class="video-player-wrap ved_img">
                                    <img src="{{URL::asset('public/frontend/landing_page1/images/watch-img-2.jpg')}}" alt="">
                                    <div class="video-player paly_btn">
                                        <a class="popup-video fancybox-media" href="https://youtu.be/2HgrxH7N7tE"><i class="fa fa-play" aria-hidden="true"  rel="media-gallery"></i></a>
                                    </div>
                                </div>
                            </div>
                            <!--end video player-->
                        </div>
                    </div>
                </section>


            <div class="produt_sec">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="produt_img">
                                <img src="{{URL::asset('public/frontend/landing_page1/images/pro1.jpg')}}" alt="">
                                <div class="produt_text">
                                    <h5><a href="#url">Product 1</a></h5>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                                    <a href="https://www.netbhe.com.br/devNet/product-details/my-new-product-24-july-82" class="page_btn">More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="produt_img">
                                <img src="{{URL::asset('public/frontend/landing_page1/images/pro2.jpg')}}" alt="">
                                <div class="produt_text">
                                    <h5><a href="#url">Product 1</a></h5>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                                    <a href="https://www.netbhe.com.br/devNet/product-details/my-new-product-24-july-82" class="page_btn">More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="produt_img">
                                <img src="{{URL::asset('public/frontend/landing_page1/images/pro3.jpg')}}" alt="">
                                <div class="produt_text">
                                    <h5><a href="#url">Product 1</a></h5>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                                    <a href="https://www.netbhe.com.br/devNet/product-details/my-new-product-24-july-82" class="page_btn">More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="produt_img">
                                <img src="{{URL::asset('public/frontend/landing_page1/images/vedioimg.jpg')}}" alt="">
                                <div class="produt_text">
                                    <h5><a href="#url">Product 1</a></h5>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                                    <a href="#url" class="page_btn">More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="produt_img">
                                <img src="{{URL::asset('public/frontend/landing_page1/images/pro1.jpg')}}" alt="">
                                <div class="produt_text">
                                    <h5><a href="#url">Product 1</a></h5>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                                    <a href="https://www.netbhe.com.br/devNet/product-details/my-new-product-24-july-82" class="page_btn">More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="produt_img">
                                <img src="{{URL::asset('public/frontend/landing_page1/images/pro2.jpg')}}" alt="">
                                <div class="produt_text">
                                    <h5><a href="#url">Product 1</a></h5>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                                    <a href="https://www.netbhe.com.br/devNet/product-details/my-new-product-24-july-82" class="page_btn">More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="produt_img">
                                <img src="{{URL::asset('public/frontend/landing_page1/images/pro3.jpg')}}" alt="">
                                <div class="produt_text">
                                    <h5><a href="#url">Product 1</a></h5>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                                    <a href="https://www.netbhe.com.br/devNet/product-details/my-new-product-24-july-82" class="page_btn">More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="produt_img">
                                <img src="{{URL::asset('public/frontend/landing_page1/images/vedioimg.jpg')}}" alt="">
                                <div class="produt_text">
                                    <h5><a href="#url">Product 1</a></h5>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                                    <a href="https://www.netbhe.com.br/devNet/product-details/my-new-product-24-july-82" class="page_btn">More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <footer class="foot_sec">
                <div class="container">
                    <div class="foot_inr">
                        <p>© 2021 <a href="#url"> netbhe.com.br</a> Todos os Direitos Reservados</p>
                        <div class="header_sos">
                            <ul>
                                <li><a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>

                            </ul>
                        </div>
                    </div>
                </div>
            </footer>


            <div class="modal fade modal_frm" id="myModal">
                <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <h4 class="modal-title">Lorem ipsum</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>        
                    <div class="modal-body">
                        <div class="from_panel">
                            <form>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="formInrInput">					
                                        <input type="text" placeholder="Name">							
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="formInrInput">							
                                        <input type="email" placeholder="Email">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="formInrInput">							
                                        <input type="tel" placeholder="Mobile">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="formInrInput">							
                                        <input type="text" placeholder="Facebook link">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="formInrInput">							
                                        <input type="text" placeholder=" Instagram link">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="formInrInput">							
                                        <input type="text" placeholder=" Linkedin link">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="formInrInput">							
                                        <input type="text" placeholder="website ">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="formInrInput">							
                                        <textarea placeholder="Address"></textarea>
                                    </div>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                    
                    <div class="modal-footer">
                    <button type="button" class="page_btn">Submit</button>
                    </div>
                    
                </div>
                </div>
            </div>


            <div class="navbarclose"></div>
            <!-- Bootstrap core JavaScript -->
            <script src="{{URL::asset('public/frontend/landing_page1/js/jquerymin.js')}}"></script>
            <script src="{{URL::asset('public/frontend/landing_page1/js/bootstrap.js')}}"></script>
            <script src="{{URL::asset('public/frontend/landing_page1/js/custom.js')}}"></script>
            <script src="{{URL::asset('public/frontend/landing_page1/js/puchmenu.js')}}"></script>
            <script src="{{URL::asset('public/frontend/landing_page1/js/fancyjs.js')}}"></script>
            <script src="{{URL::asset('public/frontend/landing_page1/js/jquery.fancybox.js')}}"></script>
            <script src="{{URL::asset('public/frontend/landing_page1/js/jquery.fancybox.pack.js')}}"></script>
            <script src="{{URL::asset('public/frontend/landing_page1/js/jquery.mousewheel-3.0.6.pack.js')}}"></script>
            <script src="{{URL::asset('public/frontend/landing_page1/js/jquery.fancybox-media.js')}}"></script>

        </body>
    </html>

@elseif(@$page->landing_template_id == 2)
    <!DOCTYPE html>
    <html lang="zxx">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <meta name="keywords" content="">
            <meta name="description" content="">
            <title>{{@$page->landing_title}}</title>
            <!--favicon-->
            <link rel="shortcut icon" type="image/png" href="{{URL::asset('public/frontend/landing_page2/images/favicon.png')}}">
            <!--bootstrap css-->
            <link rel="stylesheet" type="text/css" href="{{URL::asset('public/frontend/landing_page2/css/bootstrap.min.css')}}">
            <!--owl carousel css-->
            <link rel="stylesheet" type="text/css" href="{{URL::asset('public/frontend/landing_page2/css/owl.carousel.min.css')}}">
            <!--magnific popup css-->
            <link rel="stylesheet" type="text/css" href="{{URL::asset('public/frontend/landing_page2/css/magnific-popup.css')}}">
            <!--font awesome css-->
            <link rel="stylesheet" type="text/css" href="{{URL::asset('public/frontend/landing_page2/css/font-awesome.min.css')}}">
            <!--icofont css-->
            <link rel="stylesheet" type="text/css" href="{{URL::asset('public/frontend/landing_page2/css/icofont.min.css')}}">
            <!--animate css-->
            <link rel="stylesheet" type="text/css" href="{{URL::asset('public/frontend/landing_page2/css/animate.css')}}">
            <!--main css-->
            <link rel="stylesheet" type="text/css" href="{{URL::asset('public/frontend/landing_page2/css/style.css')}}">
            <!--responsive css-->
            <link rel="stylesheet" type="text/css" href="{{URL::asset('public/frontend/landing_page2/css/responsive.css')}}">
        </head>

        <body>
            <!--start header-->
            <header id="header">
                <div class="mainmenu">
                    <div class="container">
                        <nav class="navbar navbar-expand-lg">
                            <div class="container">
                                <!--logo-->
                                <a class="logo" href="#"><img src="{{URL::asset('public/frontend/landing_page2/images/logo.png')}}" alt="logo" /></a>
                            <div class="header-social text-right">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
                <!--end mainmenu-->
            </header>
            <!--end header-->
            <!--start hero area-->
            <section id="hero-area" data-scroll-index="0" class="ban_sec">
                <div class="container">
                    <div class="row">
                        <!--start caption-->
                        <div class="col-md-7">
                            <div class="caption d-table">
                                <div class="d-table-cell align-middle">
                                    <h1 class="header_line_1">
                                        @if(@$page->header_line_1)
                                            <span>{{strtok($page->header_line_1, " ")}}</span> {{strstr(@$page->header_line_1," ")}}
                                        @else
                                            <span>Lorem</span> ipsum dolor sit 
                                        @endif
                                    </h1>
                                    <p class="header_line_2">
                                        @if(@$page->header_line_2)
                                            {{@$page->header_line_2}}
                                        @else
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                        @endif
                                    </p>
                                    <ul class="header_btn">
                                        <li><a href="#contact-area" class="page_btn banBotArw header_button_text">{{@$page->header_button_text}}<i class="fa fa-long-arrow-right"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--end caption-->
                        <!--start caption image-->
                        <div class="col-md-5">
                            <div class="caption-img d-table text-center">
                                <div class="d-table-cell align-middle">
                                    @if(@$page->header_image)
                                        <img src="{{URL::asset('public/frontend/landing_page1/images/ban-img2.png')}}" class="header_image" alt="">
                                    @else
                                        <img src="{{URL::asset('public/frontend/landing_page2/images/watch-img-1.png')}}" class="img-fluid animation-jump header_image" alt="image">
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!--end caption image-->
                    </div>
                </div>
            </section>
            <!--end hero area-->
            <!--start about area-->
            <section id="about-area">
                <div class="container">
                    <div class="row">
                        <!--start about image-->
                        <div class="col-lg-6 col-md-4">
                            <div class="about-img bg d-table">
                                <div class="align-middle d-table-cell">
                                    <img src="{{URL::asset('public/frontend/landing_page2/images/gfg-min.png')}}" class="img-fluid animation-jump" alt="ima)ge">
                                </div>
                            </div>
                        </div>
                        <!--end about image-->
                        <!--start about content-->
                        <div class="col-lg-6 col-md-8">
                            <div class="about-cont">
                                <h6>About the product</h6>
                                <h2>Lorem ipsum dolor sit amet</h2>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis.Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>
                                <div class="row">
                                    <div class="col-md-6">
                                        <ul>
                                            <li><i class="fa fa-check"></i>Lorem ipsum dolor</li>
                                            <li><i class="fa fa-check"></i>Lorem ipsum</li>
                                            <li><i class="fa fa-check"></i>Lorem ipsum dolor</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6">
                                        <ul>
                                            <li><i class="fa fa-check"></i>Lorem ipsum dolor</li>
                                            <li><i class="fa fa-check"></i>Lorem ipsum</li>
                                            <li><i class="fa fa-check"></i>Lorem ipsum dolor</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="about-btn">
                                    <a href="#product-area">Read More</a>
                                </div>
                            </div>
                        </div>
                        <!--end about content-->
                    </div>
                </div>
            </section>
            <!--end about area-->
        
            <!--start video area-->
            <section id="video-area" class="bg-gray">
                <div class="container">
                    <div class="row">
                        <!--start heading-->
                        <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1">
                            <div class="sec-heading text-center">
                                <h6>Product Demo</h6>
                                <h2>Get product more information from the video</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.</p>
                            </div>
                        </div>
                        <!--end heading-->
                    </div>
                    <div class="row">
                        <!--start video player-->
                        <div class="col-lg-10 offset-lg-1">
                            <div class="video-player-wrap ved_img">
                                <img src="{{URL::asset('public/frontend/landing_page2/images/watch-img-2.jpg')}}" alt)="">
                                <div class="video-player paly_btn">
                                        <a class="popup-video" href="https://youtu.be/2HgrxH7N7tE"><i class="icofont-play"></i></a>
                                </div>
                            </div>
                        </div>
                        <!--end video player-->
                    </div>
                </div>
            </section>
            <!--end video area-->
            
            <!--start product area-->
            <section id="product-area" data-scroll-index="3">
                <div class="container">
                    <div class="row">
                        <!--start heading-->
                        <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1">
                            <div class="sec-heading text-center">
                                <h2>Our Products</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.</p>
                            </div>
                        </div>
                        <!--end heading-->
                    </div>
                </div>
                <!--end product area top-->
                <!--start product area bottom-->
                <div class="product-area-btm">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="product-carousel owl-carousel">
                                    <!--start product single-->
                                    <div class="prod-single text-center three">
                                        <div class="prod-img">
                                            <img src="{{URL::asset('public/frontend/landing_page2/images/pro1.jpg')}}" class="img-fluid animation-jump" alt)="">
                                        </div>
                                        <div class="prod-info three">
                                            <h4 class="pro_ac"><a href="#">Products 1</a></h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo</p>
                                            <h5>$169</h5>
                                            <a href="#" class="btn_pro">learn more</a>
                                        </div>
                                    </div>
                                    <!--end product single-->
                                    <!--start product single-->
                                    <div class="prod-single text-center three">
                                        <div class="prod-img">
                                            <img src="{{URL::asset('public/frontend/landing_page2/images/pro2.jpg')}}" class="img-fluid animation-jump" alt)="">
                                        </div>
                                        <div class="prod-info three">
                                            <h4 class="pro_ac"><a href="#">Products 2</a></h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo</p>
                                            <h5>$189</h5>
                                            <a class="btn_pro" href="https://www.netbhe.com.br/devNet/product-details/my-new-product-24-july-82">learn more</a>
                                        </div>
                                    </div>
                                    <!--end product single-->
                                    <!--start product single-->
                                    <div class="prod-single text-center three">
                                        <div class="prod-img">
                                            <img src="{{URL::asset('public/frontend/landing_page2/images/pro3.jpg')}}" class="img-fluid animation-jump" alt)="">
                                        </div>
                                        <div class="prod-info three">
                                            <h4 class="pro_ac"><a href="#">Products 3</a></h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo</p>
                                            <h5>$169</h5>
                                            <a class="btn_pro" href="https://www.netbhe.com.br/devNet/product-details/my-new-product-24-july-82">learn more</a>
                                        </div>
                                    </div>
                                    <!--end product single-->
                                    <!--start product single-->
                                    <div class="prod-single text-center three">
                                        <div class="prod-img">
                                            <img src="{{URL::asset('public/frontend/landing_page2/images/pro1.jpg')}}" class="img-fluid animation-jump" alt)="">
                                        </div>
                                        <div class="prod-info three">
                                            <h4 class="pro_ac"><a href="#">Products 4</a></h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo</p>
                                            <h5>$345</h5>
                                            <a class="btn_pro" href="https://www.netbhe.com.br/devNet/product-details/my-new-product-24-july-82">learn more</a>
                                        </div>
                                    </div>
                                    <!--end product single-->
                                    <!--start product single-->
                                    <div class="prod-single text-center three">
                                        <div class="prod-img">
                                            <img src="{{URL::asset('public/frontend/landing_page2/images/pro2.jpg')}}" class="img-fluid animation-jump" alt)="">
                                        </div>
                                        <div class="prod-info three">
                                        <h4 class="pro_ac"><a href="#">Products 5</a></h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo</p>
                                            <h5>$169</h5>
                                            <a class="btn_pro" href="https://www.netbhe.com.br/devNet/product-details/my-new-product-24-july-82">learn more</a>
                                        </div>
                                    </div>
                                    <!--end product single-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--end product area-->
            
            <!--start contact area-->
            <section id="contact-area" data-scroll-index="6">
                <div class="container">
                    <div class="row">
                        <!--start heading-->
                        <div class="col-md-8">
                            <div class="sec-heading">
                                <h6>Need Help</h6>
                                <h2>Contact with us</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium in.</p>
                            </div>
                        </div>
                        <!--end heading-->
                    </div>
                    <div class="row">
                        <!--start contact form-->
                        <div class="col-lg-8 col-md-7">
                            <div class="contact-form">
                                <form id="ajax-contact" action="https://omexer.com/tf/html/tm/emexso/demo/contact.php" method="post">
                                    <div class="form-group">
                                        <label for="name">Name*</label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Name*" required="required" data-error="name is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email*</label>
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Enter email*" required="required" data-error="valid email is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="message">Message*</label>
                                        <textarea class="form-control" id="message" name="message" rows="8" placeholder="Write your message*" required="required" data-error="Please, leave us a message."></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <button type="submit">Submit Now</button>
                                    <div class="messages"></div>
                                </form>
                            </div>
                        </div>
                        <!--end contact form-->
                        <!--start contact information-->
                        <div class="col-lg-4 col-md-5">
                            <div class="cont-info">
                                <div class="cont-info-single">
                                    <h5><i class="icofont-google-map"></i> Address:</h5>
                                    <p>Lorem ipsum dolor sit amet</p>
                                </div>
                                <div class="cont-info-single">
                                    <h5><i class="icofont-email"></i> Email:</h5>
                                    <p>contato@netbhe.com </p>
                                </div>
                                <div class="cont-info-single">
                                    <h5><i class="icofont-phone"></i> Phone:</h5>
                                    <p>1934567890</p>
                                </div>
                            </div>
                        </div>
                        <!--end contact information-->
                    </div>
                </div>
            </section>
            <!--end contact area-->
            <!--start footer-->
            <footer id="footer" class="bg-gray">
                <!--start footer bottom-->
                <div class="footer-btm text-center">
                    <div class="container">
                        <p>© 2021 <a href="#">netbhe.com.br</a> Todos os Direitos Reservados</p>
                        <div class="header-social text-center foo_sos">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>


            <div class="modal fade con_frm" id="myModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                        <h4 class="modal-title">Lorem ipsum</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>        
                        <div class="modal-body">
                            <div class="contact-form">
                                <form>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">              
                                            <input type="text" placeholder="Name" class="form-control">                          
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">                       
                                            <input type="email" placeholder="Email" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">                          
                                        <input type="tel" placeholder="Mobile" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">                         
                                            <input type="text" placeholder="Facebook link" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">                           
                                            <input type="text" placeholder="Instagram link" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">                          
                                            <input type="text" placeholder="Linkedin link" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">                         
                                            <input type="text" placeholder="website " class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">                         
                                            <textarea placeholder="Address" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                        <button type="submit" class="disabled">Submit Now</button>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

                

        

            <script src="{{URL::asset('public/frontend/landing_page2/js/jquery-3.3.1.min.js')}}"></script>
            <!--proper js-->
            <script src="{{URL::asset('public/frontend/landing_page2/js/popper.min.js')}}"></script>
            <!--bootstrap js-->
            <script src="{{URL::asset('public/frontend/landing_page2/js/bootstrap.min.js')}}"></script>
            <!--magnic popup js-->
            <script src="{{URL::asset('public/frontend/landing_page2/js/magnific-popup.min.js')}}"></script>
            <!--owl carousel js-->
            <script src="{{URL::asset('public/frontend/landing_page2/js/owl.carousel.min.js')}}"></script>
            <!--scrollIt js-->
            <script src="{{URL::asset('public/frontend/landing_page2/js/scrollIt.min.js')}}"></script>
            <!--contact js-->
            <script src="{{URL::asset('public/frontend/landing_page2/js/contact.js')}}"></script>
            <!--validator js-->
            <script src="{{URL::asset('public/frontend/landing_page2/js/validator.min.js')}}"></script>
            <!--main js-->
            <script src="{{URL::asset('public/frontend/landing_page2/js/custom.js')}}"></script>


            <script>
                

                $('a.banBotArw[href^="#"]').on('click', function(event) {

                    var target = $( $(this).attr('href') );

                    if( target.length ) {
                        event.preventDefault();
                        $('html, body').animate({
                            scrollTop: target.offset().top
                        }, 3000);
                    }

                });
            </script>

        </body>

    </html>

@endif