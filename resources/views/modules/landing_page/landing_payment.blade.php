@extends('layouts.app')
@section('title')
Landing page Payment
@endsection
@section('style')
@include('includes.style')
<style type="text/css">
    .checkout-div {
        width: 100% !important;
    }
    .row {
        display: -ms-flexbox; /* IE10 */
        display: flex;
        -ms-flex-wrap: wrap; /* IE10 */
        flex-wrap: wrap;
        margin: 0 -16px;
    }
    .col-25 {
        -ms-flex: 25%; /* IE10 */
        flex: 25%;
    }
    .col-50 {
        -ms-flex: 50%; /* IE10 */
        flex: 50%;
    }
    .col-75 {
        -ms-flex: 75%; /* IE10 */
        flex: 75%;
    }
	.col-100 {
        -ms-flex: 100%; /* IE10 */
        flex: 100%;
    }
    .col-25,
    .col-50,
    .col-75
	.col-100 {
        padding: 0 8px;
    }
    input[type=text] {
        width: 100%;
        margin-bottom: 20px;
        padding: 12px;
        border: 1px solid #ccc;
        border-radius: 3px;
    }
    label {
        margin-bottom: 10px;
        display: block;
    }
    .icon-container {
        margin-bottom: 20px;
        padding: 7px 0;
        font-size: 24px;
    }
    .btn {
        background-color: #4CAF50;
        color: white;
        padding: 12px;
        margin: 10px 0;
        border: none;
        width: 100%;
        border-radius: 3px;
        cursor: pointer;
        font-size: 17px;
    }
    .btn:hover {
        background-color: #45a049;
    }
    span.price {
        float: right;
        color: grey;
    }
    /* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other (and change the direction - make the "cart" column go on top) */

	.lftmm{
		width:48%;
		float:left;
    }
	.lftyy{
		width:48%;
		float:right;
    }

    .cc-input {
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
        font-size:16px;
        outline: none;
        border-color: #ccc !important;
        box-shadow: none !important;
    }

    @media (max-width: 800px) {
        .row {
            flex-direction: column-reverse;
        }
        .col-25 {
            margin-bottom: 20px;
        }
    }
</style>
<style>
    .card-wrapper {
        border: 1px dashed #adadad;
        padding: 10px 5px;
        border-radius: 4px;
        box-shadow: 1px 3px 8px 0px #00000063;
        display: none;
    }

    .sk-chase {
        width: 70px;
        height: 70px;
        position: relative;
        animation: sk-chase 2.5s infinite linear both;
    }

    .sk-chase-dot {
        width: 100%;
        height: 100%;
        position: absolute;
        left: 0;
        top: 0;
        animation: sk-chase-dot 2.0s infinite ease-in-out both;
    }

    .sk-chase-dot:before {
        content: '';
        display: block;
        width: 25%;
        height: 25%;
        background-color: #0245c1;
        border-radius: 100%;
        animation: sk-chase-dot-before 2.0s infinite ease-in-out both;
    }

    .sk-chase-dot:nth-child(1) {
        animation-delay: -1.1s;
    }

    .sk-chase-dot:nth-child(2) {
        animation-delay: -1.0s;
    }

    .sk-chase-dot:nth-child(3) {
        animation-delay: -0.9s;
    }

    .sk-chase-dot:nth-child(4) {
        animation-delay: -0.8s;
    }

    .sk-chase-dot:nth-child(5) {
        animation-delay: -0.7s;
    }

    .sk-chase-dot:nth-child(6) {
        animation-delay: -0.6s;
    }

    .sk-chase-dot:nth-child(1):before {
        animation-delay: -1.1s;
    }

    .sk-chase-dot:nth-child(2):before {
        animation-delay: -1.0s;
    }

    .sk-chase-dot:nth-child(3):before {
        animation-delay: -0.9s;
    }

    .sk-chase-dot:nth-child(4):before {
        animation-delay: -0.8s;
    }

    .sk-chase-dot:nth-child(5):before {
        animation-delay: -0.7s;
    }

    .sk-chase-dot:nth-child(6):before {
        animation-delay: -0.6s;
    }

    @keyframes sk-chase {
        100% {
            transform: rotate(360deg);
        }
    }

    @keyframes sk-chase-dot {

        80%,
        100% {
            transform: rotate(360deg);
        }
    }

    @keyframes sk-chase-dot-before {
        50% {
            transform: scale(0.4);
        }

        100%,
        0% {
            transform: scale(1.0);
        }
    }

    .container-sk-chase {
        height: 100vh;
        width: 100vw;
        display: flex;
        justify-content: center;
        align-items: center;
        background: #00000075;
        position: fixed;
        top: 0;
        left: 0;
        z-index: 30;
    }
    </style>
@endsection
@section('scripts')
@include('includes.scripts')
<link href="{{ URL::to('public/frontend/css/calender.css') }}" rel="stylesheet" type="text/css">
<script src="{{ URL::to('public/frontend/js/jquery-ui.js') }}"></script>
<script>
    $(function() {
            $("#datepicker11, #datepicker12").datepicker({dateFormat: "yy-mm-dd",
            monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho',
            'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],

            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
                defaultDate: new Date(),
                // showButtonPanel: true,
                showMonthAfterYear: true,
                showWeek: true,
                showAnim: "drop",
                constrainInput: true,
                yearRange: "-90:",
                maxDate:new Date(),
                onClose: function( selectedDate ) {
                   //$( "#datepicker1").datepicker( "option", "minDate", selectedDate );
                }
            });
        });
</script>
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')
<div class="bkng-hstrybdy">
    <div class="container">
        <h2>{{__('client_site.payment_request')}}</h2>
    </div>
</div>
<div class="all_logo_area message_compose">
    <div class="container">
        <div class="row rwmrgn">
            <div class="checkout-div nobgbx">
                <div class="head-check">
                    <h3>{{__('client_site.review_your_purchase')}}</h3>

                </div>

                <div class="mid-checkout">
                    <div class="pymtinfo">

                        <div class="singleee">
                        <strong>@lang('site.token') @lang('site.no') <span>:</span></strong>
                         <p>{{ @$booking->token_no }}</p>
                    	</div>
                        <div class="singleee">
                        <strong>{{__('site.total_amount')}} <span>:</span></strong>
                        <p>R$ {{ number_format((float)(@$booking->total_amount), 2, '.', '') }}</p>
                    	</div>
                        <div class="singleee">
                        <strong>{{__('site.payment_type')}} <span>:</span></strong>
                        <p>
                            @if(@$booking->payment_type=='C') {{__('site.payment_method_card')}}
                            @elseif(@$booking->payment_type=='BA') {{__('site.payment_method_bank')}}
                            @elseif(@$booking->payment_type=='S') Stripe
                            @elseif(@$booking->payment_type=='P') Paypal
                            @endif
                        </p>
                    	</div>
                        <div class="singleee">
                            <strong> Purchase For <span>:</span></strong>
                            <p>
                                @if(@$booking->payment_for=='T') Template
                                @elseif(@$booking->payment_for=='B')Banding
                                @elseif(@$booking->payment_for=='BT') Template & Banding
                                @endif
                            </p>
                        </div>

                    </div>


                </div>

                @if(@$booking->payment_type=='C')
                <form name="myForm1" id="myform1" action="{{ route('landing.user.profile') }}" method="post">
                    @csrf
                    <input type="hidden" name="is_profile" id="is_profile" value="{{@$profile}}">
                    {{-- @if(@$profile>0) --}}
                    @if((@Auth::guard('web')->user()->is_credit_card_added == 'N' || Auth::guard('web')->user()->customer_id == '') && @$booking->payment_status=='I')
                    <div class="book-from">
                        <div class="form_body pymtbox">
                            <div class="row">
                                <div class="col-12">
                                    <h3>@lang('site.buyer_information')</h3>
                                </div>
                                <div class="col-12">
                                    <label for="cname">@lang('site.Name')</label>
                                    <input type="text" id="name" name="name" placeholder="@lang('site.Name')" value="{{ @Auth::guard('web')->user()->nick_name ? @Auth::guard('web')->user()->nick_name : @Auth::guard('web')->user()->name }}" required>
                                </div>
                                <div class="col-12">
                                    <label>@lang('site.email')</label>
                                    <input type="text" placeholder="@lang('site.email')" id="buyer_email" value="{{ @Auth::guard('web')->user()->email }}" disabled>
                                </div>
                                <div class="col-12">
                                    <label>@lang('site.cpf_no')</label>
                                    <input type="text" placeholder="@lang('site.enter_cpf_no')" id="buyer_cpf_no" value="{{ @Auth::guard('web')->user()->cpf_no }}" name="cpf_no" required>
                                </div>
                                <div class="col-12">
                                    <label>@lang('site.dob')</label>
                                    <input type="text" id="datepicker11" value="{{ @Auth::guard('web')->user()->dob }}" name="dob" placeholder="Selecione sua data de nascimento" required>
                                </div>
                                <div class="col-12">
                                    <label>@lang('site.phone')</label>
                                    <input type="text" placeholder="@lang('site.phone')" id="phone_no" value="{{ @Auth::guard('web')->user()->mobile }}" name="phone_no" required>
                                </div>

                                <div class="col-6">
                                    <label>@lang('site.country')</label>
                                    <select name="country" class="required personal-type personal-select" id="country">
                                        <option value="">@lang('site.select') @lang('site.country')</option>
                                        @foreach(@$country as $cn)
                                        <option value="{{@$cn->id}}" @if(@Auth::guard('web')->user()->country_id==$cn->id) selected @endif>{{@$cn->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-6">
                                    <label>@lang('site.state')</label>
                                    <select name="state" id="state" class="required personal-type personal-select">
                                        <option value="">@lang('site.select') @lang('site.state')</option>
                                        @if(@Auth::guard('web')->user()->state_id)
                                        @foreach(@$state as $cn)
                                        @if(@Auth::guard('web')->user()->country_id==$cn->country_id)
                                        <option value="{{@$cn->id}}"  @if(@Auth::guard('web')->user()->state_id==$cn->id) selected @endif>{{@$cn->name}}</option>
                                        @endif
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="col-6">
                                    <label>@lang('site.city')</label>
                                    <input type="text" placeholder="Digite o nome da sua cidade" class="validate required" value="{{ @Auth::guard('web')->user()->city }}" name="city">
                                </div>
                                <div class="col-6">
                                    <label>@lang('site.area_code')</label>
                                    <input type="text" placeholder="Digite seu código de área" class="validate required" value="{{ @Auth::guard('web')->user()->area_code >0 ? @Auth::guard('web')->user()->area_code: ""}}" name="area_code">
                                </div>
                                <div class="col-6">
                                    <label>@lang('site.street_name')</label>
                                    <input type="text" placeholder="Digite o nome da rua" class="required" value="{{ @Auth::guard('web')->user()->street_name }}" name="street_name">
                                </div>
                                <div class="col-6">
                                    <label>@lang('site.street_number')</label>
                                    <input type="text" placeholder="Digite o número da rua" class="required" value="{{ @Auth::guard('web')->user()->street_number >0 ? @Auth::guard('web')->user()->street_number : "" }}" name="street_number">
                                </div>
                                <div class="col-6">
                                    <label>@lang('site.address_complement')</label>
                                    <input type="text" placeholder="Inserir complemento de endereço" class="required" value="{{ @Auth::guard('web')->user()->complement }}" name="complement">
                                </div>
                                <div class="col-6">
                                    <label>@lang('site.district_name')</label>
                                    <input type="text" placeholder="Digite o nome do seu distrito" class="required" value="{{ @Auth::guard('web')->user()->district }}" name="district">
                                </div>
                                <div class="col-6">
                                    <label>@lang('site.zipcode')</label>
                                    <input type="text" placeholder="Coloque seu codigo Postal" class="required" value="{{ @Auth::guard('web')->user()->zipcode }}" name="zipcode">
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- @endif --}}
                    {{-- @if(@Auth::guard('web')->user()->is_credit_card_added=="N") --}}
                    <div class="book-from">
                        <div class="form_body pymtbox">
                            <div class="row">
                                <div class="col-75">
                                    <div class="container">
                                        <div class="">
                                        <div class="" style="width:100%;">
                                        <div class="row">
                                            <div class="col-100">
                                                <h3>@lang('site.payment')</h3>
                                                <label for="fname">@lang('site.accepted_cards')</label>
                                                <div class="icon-container ">
                                                    <i class="fa fa-cc-visa" style="color:navy;"></i>
                                                    <i class="fa fa-cc-amex" style="color:blue;"></i>
                                                    <i class="fa fa-cc-mastercard" style="color:red;"></i>
                                                    <i class="fa fa-cc-discover" style="color:orange;"></i>
                                                </div></div></div>
                                                <div class="row">
                                                    <div class="col-100">
                                                        <label for="cname">@lang('site.name_on_card')</label>
                                                        <input type="text" id="card_name" name="card_name" placeholder="@lang('site.card_name')" value="{{ @Auth::guard('web')->user()->nick_name ? @Auth::guard('web')->user()->nick_name : @Auth::guard('web')->user()->name }}">
                                                    </div>

                                                    <div class="col-100">
                                                        <label for="cname">@lang('site.cpf_no')</label>
                                                        <input type="text" id="card_cpf_no" name="card_cpf_no" placeholder="@lang('site.enter_cpf_no')" value="{{ Auth::guard('web')->user()->cpf_no }}">
                                                    </div>

                                                    <div class="col-100">
                                                        <label>@lang('site.dob')</label>
                                                        {{-- <input type="text" id="datepicker12" value="{{ @Auth::guard('web')->user()->dob }}" name="card_dob" placeholder="Selecione sua data de nascimento" required> --}}
                                                    </div>
                                                    <div class="col-25">
                                                        <div class="lftyy" style="width: 100%">
                                                            {{-- <label for="dob_year"> @lang('site.year') de validade</label> --}}
                                                            <select id="dob_year" name="dob_year" class="pymtboxslct">
                                                                <option value="">YY</option>
                                                                {{-- @for($i=1950; $i<=date('Y')-18; $i++) <option value="{{@$i}}">{{@$i}}</option>
                                                                    @endfor --}}
                                                            </select>
                                                        </div>

                                                    </div>
                                                    <div class="col-50">
                                                        {{-- <label>@lang('site.dob')</label> --}}
                                                        <div class="lftmm">
                                                            {{-- <label for="dob_month">Mês de validade</label> --}}
                                                            <select id="dob_month" name="dob_month" class="pymtboxslct">
                                                                <option value="">MM</option>
                                                                <option value="01">01</option>
                                                                <option value="02">02</option>
                                                                <option value="03">03</option>
                                                                <option value="04">04</option>
                                                                <option value="05">05</option>
                                                                <option value="06">06</option>
                                                                <option value="07">07</option>
                                                                <option value="08">08</option>
                                                                <option value="09">09</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div>
                                                        <div class="lftyy">
                                                            {{-- <label for="dob_day"> @lang('site.year') de validade</label> --}}
                                                            <select id="dob_day" name="dob_day" class="pymtboxslct">
                                                                {{-- <option value="">DD</option> --}}
                                                                {{-- @for($i=1; $i<=31; $i++)
                                                                @if($i<10)
                                                                <option value="0{{@$i}}">{{@$i}}</option>
                                                                @else
                                                                <option value="{{@$i}}">{{@$i}}</option>
                                                                @endif
                                                                    @endfor --}}
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-50">
                                                        @php
                                                            $cntry = App\Models\Country::find(@Auth::guard('web')->user()->country_id);
                                                        @endphp
                                                        <label>@lang('site.phone_code')</label>
                                                        <input type="text" placeholder="@lang('site.phone_code')" class="validate required" value="{{ @$cntry->phonecode }}" name="card_phonecode">
                                                    </div>
                                                    <div class="col-50">
                                                        <label>@lang('site.area_code')</label>
                                                        <input type="text" placeholder="Digite seu código de área" class="validate required" value="{{  @Auth::guard('web')->user()->area_code > 0 ? @Auth::guard('web')->user()->area_code : "" }}" name="card_area_code">
                                                    </div>
                                                    <div class="col-100">
                                                        <label>@lang('site.phone')</label>
                                                        <input type="text" placeholder="@lang('site.phone')" class="validate required" value="{{ @Auth::guard('web')->user()->mobile > 0 ? @Auth::guard('web')->user()->mobile : "" }}" name="card_mobile">
                                                    </div>

                                                    <div class="col-100 mb-3">
                                                        <label for="ccnum">@lang('site.credit_card_number')</label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1"><i id="cc-icon" class="fa fa-credit-card fa-lg" style=""></i></span>
                                                            </div>
                                                            <input type="text" class="form-control form-control-lg cc-input" id="ccnum" name="cardnumber" placeholder="@lang('site.credit_card_number')">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text" id="ind-backg"><i class="fa fa-check" id="valid-indicator" style="color: lightgray"></i></span>
                                                            </div>
                                                        </div>
                                                        <label for="ccnum" id="ccnum-error" style="display: none;" class="error">É necessário o número do cartão de crédito.</label>
                                                    </div>


                                                    <div class="col-50">
                                                    <div class="lftmm">
                                                        <label for="expmonth">Mês de validade</label>
                                 <select id="expiry_month" name="expiry_month" class="pymtboxslct">
                                                            <option value="">MM</option>
                                                            <option value="01">01</option>
                                                            <option value="02">02</option>
                                                            <option value="03">03</option>
                                                            <option value="04">04</option>
                                                            <option value="05">05</option>
                                                            <option value="06">06</option>
                                                            <option value="07">07</option>
                                                            <option value="08">08</option>
                                                            <option value="09">09</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                        </div>

                                                        <div class="lftyy">
                                                        <label for="expyear"> @lang('site.year') de validade</label>
                                 <select id="expiry_year" name="expiry_year" class="pymtboxslct">
                                                            <option value="">YY</option>
                                                            @for($i=date('Y'); $i<=date('Y') + 20; $i++)
                                                            <option value="{{@$i}}">{{@$i}}</option>
                                                            @endfor
                                                        </select>
                                                        </div>

                                                    </div>
                                                    <div class="col-50">

                                                    	<label for="cvv">CVC</label>
                                                        <input type="text" id="cvc" name="cvc" placeholder="@lang('site.card_cvc')">

                                                    </div>
                                                </div>
                                                <!--<div class="row">
                                                    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-6">

                                                    </div>
                                                </div>-->


                                        <label>
                                        </label>




                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif

                    @if((@Auth::guard('web')->user()->is_credit_card_added=="N" || $profile>0)&& @$booking->payment_status=='I')
                    <div class="pymnt-check">
                        <a href="{{ route('landing.remove.payment.method',['slug'=>@$booking->token_no])}}"> <i class="fa fa-angle-left" aria-hidden="true"></i> @lang('site.back')</a>
                        <a class="pynw login_submit" id="pynw">@lang('site.proceed_to_pay')</a>
                    </div>
                    {{-- @else
                    <div class="last-check">
                        <center> <a class="pay-btn" href="{{route('user.my.booking',['type'=>'UC'])}}">@lang('site.upcoming_classes')</a></center>
                    </div> --}}
                    @endif
                    <input type="hidden" name="order_no" value="{{@$booking->token_no}}">
                </form>
                @endif
                @if(@$booking->payment_type!='C' && @$booking->payment_type!='BA' && @$booking->total_amount!=0 && @$booking->payment_type!='S' && @$booking->payment_type!='P')
                <form name="myForm2" id="myform2" action="{{route('landing.page.payment.type',['slug'=>@$booking->token_no])}}" method="post">
                    @csrf
                    <div class="book-from">
                        <div class="form_body pymtbox">
                            <div class="row">
                                <div class="col-75">
                                    <div class="container">
                                        <div class="">
                                            <div class="" style="width:100%;">
                                                <div class="row">
                                                    <div class="col-100">
                                                        <h3>@lang('site.payment')</h3>

                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-100">
                                                        <label for="payment_method">{{__('site.payment_method')}}</label>
                                                            <select id="payment_method" name="payment_method" class="pymtboxslct required">
                                                                <option value="">{{__('site.payment_method_select')}}</option>
                                                                <option value="C">{{__('site.payment_method_card')}}</option>
                                                                <option value="S">Stripe</option>
                                                                <option value="P">Paypal</option>
                                                                {{-- <option value="BA">{{__('site.payment_method_bank')}}</option> --}}
                                                            </select>
                                                    </div>
                                                    @php
                                                        $x2 = 4.50;
                                                        $x3 =5.00;
                                                        $x4 =5.50;
                                                        $x5 =6.50;
                                                        $x6 =7.50;
                                                        $x7 =8.50;
                                                        $x8 =9.50;
                                                        $x9 =10.50;
                                                        $x10 =11.50;
                                                        $x11 =12.00;
                                                        $x12 =12.50;
                                                    @endphp
                                                    @if(@$booking->profDetails->no_of_installment>1)
                                                    <div class="col-100 installment_checkbox" style="display:none">
                                                        <label for="installment_select">{{__('site.payment_in_instalment')}}
                                                        <input type="checkbox" id="installment_select" name="installment_select"></label>
                                                    </div>
                                                    <div class="mid-checkout no_installment" style="display:none">
                                                        <div class="pymtinfo">
                                                            @for ($i = 1; $i <= $booking->profDetails->no_of_installment; $i++)
                                                                @if($i==1)
                                                                <div class="singleee">
                                                                    <label for="ins{{$i}}"><strong style="width: 200px"> <input type="radio" name="no_installment" value="{{$i}}" id="ins{{$i}}" class="no_installment_radio"> {{$i}}x R$ {{@$booking->sub_total}}<span></span></strong>
                                                                    <p style="text-align: right">R$ {{@$booking->sub_total}}</p></label>
                                                                </div>
                                                                @else
                                                                @php
                                                                if($i==2){
                                                                $x=$x2;
                                                                }
                                                                if($i==3){
                                                                $x=$x3;
                                                                }
                                                                if($i==4){
                                                                $x=$x4;
                                                                }
                                                                if($i==5){
                                                                $x=$x5;
                                                                }
                                                                if($i==6){
                                                                $x=$x6;
                                                                }
                                                                if($i==7){
                                                                $x=$x7;
                                                                }
                                                                if($i==8){
                                                                $x=$x8;
                                                                }
                                                                if($i==9){
                                                                $x=$x9;
                                                                }
                                                                if($i==10){
                                                                $x=$x10;
                                                                }
                                                                if($i==11){
                                                                $x=$x11;
                                                                }
                                                                if($i==12){
                                                                $x=$x12;
                                                                }
                                                                if(@$booking->installment_charge=='Y'){
                                                                $x=0;
                                                                }
                                                                $down=(1-($x/100)-(5.49/100));
                                                                $up=@$booking->sub_total*($x/100);
                                                                $additional =($up/$down);
                                                                @endphp
                                                                <div class="singleee">
                                                                    <label for="ins{{$i}}"><strong style="width: 200px" ><input type="radio" name="no_installment" value="{{$i}}" id="ins{{$i}}" class="no_installment_radio">  {{$i}}x R$
                                                                        {{number_format((float)(number_format((float)@$additional+@$booking->sub_total, 2, '.', '')/$i), 2, '.', '')}}<span></span></strong>
                                                                    <p style="text-align: right">R${{number_format((float)@$additional+@$booking->sub_total, 2, '.', '')}}</p></label>
                                                                </div>
                                                                @endif
                                                                @endfor
                                                                <label for="no_installment" generated="true" class="error" style="display: none"></label>
                                                            </div>
                                                    </div>
                                                    @endif
                                                    {{-- <div class="col-100">
                                                        <label for="cname">{{__('site.apply_coupon')}}</label>
                                                        <input type="text" id="coupon" name="coupon" placeholder="{{__('site.enter_coupon')}}">
                                                    </div> --}}
                                                    {{-- <input type="hidden" name="couponCode" id="couponCode"> --}}
                                                </div>
                                                <label>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pymnt-check">
                                <a href="{{ route('list.landing.page.templates') }}"> <i
                                        class="fa fa-angle-left" aria-hidden="true"></i> @lang('site.back')</a>
                                <a class="pynw login_submit" id="add_Pament_method">{{__('site.pay')}}</a>
                                {{-- <a class="pynw login_submit" id="check_Coupon">{{__('site.apply_coupon')}}</a> --}}
                            </div>
                            <input type="hidden" name="order_no" value="{{@$booking->token_no}}">
                </form>
                @endif
                @if(@$booking->payment_type == 'BA' && @$booking->payment_document==NULL)
                <form name="myForm3" id="myform3" action="{{route('upload.payment.document',['slug'=>@$booking->token_no])}}"
                    method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="book-from">
                        <div class="form_body pymtbox">
                            <div class="row">
                                <div class="col-75">
                                    <div class="container">
                                        <div class="">
                                            <div class="" style="width:100%;">
                                                <div class="row">
                                                    <div class="col-100">
                                                        <h3></h3>

                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-100">
                                                        <label  style="text-decoration: underline;">{{__('site.account_information')}}:-</label>
                                                        <label >{{__('site.bank')}}: {{$accountInfo->bank_information}}</label>
                                                        <label >{{__('site.beneficiary ')}} : {{$accountInfo->beneficiary}}</label>
                                                        <label >{{__('site.agency')}} : {{$accountInfo->agency}}</label>
                                                        <label >{{__('site.current_account')}} : {{$accountInfo->current_account}}</label>
                                                        <label >{{__('site.cnpj')}}: {{$accountInfo->cnpj}}</label>
                                                    </div>
                                                    <div class="col-100" style="margin-bottom: 25px;">
                                                        <label  style="text-decoration: underline;">{{__('site.payment_instruction')}}:-</label>
                                                        <label >{{$accountInfo->instruction}}</label>
                                                    </div>
                                                    <div class="col-100">
                                                        <label for="upload_file">{{__('site.upload_payment_document')}}</label>
                                                        <input type="file" id="upload_file" name="upload_file" placeholder="{{__('site.upload_document')}}" title = "Choose a video please">
                                                    </div>

                                                </div>
                                                <label>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pymnt-check">
                                <a href="{{ route('user.my.booking', ['type' => 'UC']) }}"> <i class="fa fa-angle-left"
                                        aria-hidden="true"></i> @lang('site.back')</a>
                                <a class="pynw login_submit" id="upload">{{__('site.upload_document')}}</a>

                            </div>
                            <input type="hidden" name="order_no" value="{{@$booking->token_no}}">
                </form>
                @endif
                @if(@$booking->payment_type=='S'&& @$booking->payment_status=='I')
                <form id="payment-form">
                    @csrf
                    <div class="book-from">
                        <div class="form_body pymtbox">
                            <div class="row">
                                <div class="col-75">
                                    <div class="container">
                                        <div class="">
                                            <div class="" style="width:100%;">
                                                <div class="row">
                                                    <div class="col-100">
                                                        <h3>@lang('site.payment')</h3>

                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-100">
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <div class="card-wrapper">
                                                                    <div id="card-element">
                                                                        <!--Stripe.js injects the Card Element-->
                                                                    </div>
                                                                </div>
                                                                <label class="text-danger error" id="card-error" role="alert" style="display: none;"></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="couponCode" id="couponCode">
                                                </div>
                                                <label>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pymnt-check">
                                <a href="{{ route('landing.remove.payment.method',['slug'=>@$booking->token_no])}}"> <i class="fa fa-angle-left" aria-hidden="true"></i> @lang('site.back')</a>
                                {{-- <a class="pynw login_submit" id="add_Pament_method">{{__('site.submit')}}</a> --}}
                                <button id="submit" type="submit" class="pynw login_submit login_submitt" style="float: none !important;height: 40px !important;">
                                    <div class="spinner hidden" id="spinner"></div>
                                    <span id="button-text">{{__('site.submit')}}</span>
                                </button>
                            </div>
                            <input type="hidden" name="order_no" value="{{@$booking->token_no}}">
                </form>
                @endif

                {{-- @if(@$booking->payment_status=='P'||(@$booking->payment_status=='PR'&& @$booking->payment_document!=NULL))
                    <div class="last-check">
                        <center> <a class="pay-btn" href="{{route('user.my.booking',['type'=>'UC'])}}">@lang('site.upcoming_classes')</a></center>
                    </div>
                @endif --}}
            </div>
             </div>
        </div>
    </div>
    </section>
</div>
@if(@$booking->payment_type=='S'&& @$booking->payment_status=='I')
<div class="container-sk-chase loading">
    <div class="sk-chase ">
        <div class="sk-chase-dot"></div>
        <div class="sk-chase-dot"></div>
        <div class="sk-chase-dot"></div>
        <div class="sk-chase-dot"></div>
        <div class="sk-chase-dot"></div>
        <div class="sk-chase-dot"></div>
    </div>
</div>
@endif
@endsection
@section('footer')
@include('includes.footer')
<script src="{{ URL::to('public/frontend/js/jquery.creditCardValidator.js') }}"></script>
<script type="text/javascript">
    // $('#myform').validate();
    $('#myform1').validate({
        rules:{
            // phone_no:{
            //     digits:true,
            //     maxlength:8,
            //     minlength:8
            // },
            // cpf_no:{
            //     validateCPF: true
            // },
            // zipcode:{
            //     required:true,
            //     maxlength:8,
            //     minlength:8
            //     },
            cardnumber:{
                digits:true,
                required:true,
                // maxlength:16,
                // minlength:16
            },
            cvc:{
                digits:true,
                required:true,
                maxlength:4,
                minlength:3
            },
            expiry_year:{
                required:true
            },
            expiry_month:{
                required:true
            },
            card_name:{
                required:true
            },
            phone_no: {
                digits: true,
                minlength: 9,
                maxlength: 9
            },
            card_mobile: {
                digits: true,
                minlength: 9,
                maxlength: 9
            },
            zipcode:{
                // digits:true,
                required:true,
                minlength:4,
                maxlength:10
            },
            area_code: {
                digits:true,
                minlength:2,
                maxlength:2
            },
            card_area_code: {
                digits:true,
                minlength:2,
                maxlength:2
            },
            street_number:{
                digits:true,
                minlength:3,
                maxlength:3
            },
            card_phonecode:{
                digits:true,
            },
            dob_year:{
            required:true,
            },
            dob_month:{
            required:true,
            },
            dob_day:{
            required:true,
            },
        },
        messages:{
            cardnumber:{
                required: "É necessário o número do cartão de crédito.",
                digits: "O número do cartão de crédito não pode ser um caractere.",
                minlength: "Número de cartão de crédito inválido.",
                maxlength: "Número de cartão de crédito inválido."
            },
            cvc:{
                required: "Digite o número do cvc.",
                digits: "Número CVV Não pôde ser um caractere.",
                minlength: "Número CVC inválido.",
                maxlength: "Número CVC inválido."
            },
            expiry_year:{
                required: "Digite a data de validade do seu cartão de crédito.",
                digits: "O ano de vencimento do cartão de crédito não pode ser um personagem.",
                minlength: "Data de validade inválida.",
                maxlength: "Data de validade inválida."
            },
            expiry_month:{
                required: "Digite o mês de validade do seu cartão de crédito.",
                digits: "O mês de vencimento do cartão de crédito não pode ser um personagem.",
                minlength: "Mês de validade inválido.",
                maxlength: "Mês de validade inválido."
            },
            card_name:{
                required:"Digite seu nome completo impresso no seu cartão."
            }
        },
        submitHandler: function(form) {
            // if (isCardValid) {
            //     $(form).submit();
            // }
            let ccResult = $('#ccnum').validateCreditCard();
            if (ccResult.valid) {
                form.submit();
            } else {
                return false;
            }
        }
        // messages:{
        //     cardnumber:{
        //         required: "Please enter your 16 digit Credit card number.",
        //         digits: "Credit card number could not be a charecter.",
        //         minlength: "Invalid Credit Card number.",
        //         maxlength: "Invalid Credit Card number."
        //     },
        //     cvc:{
        //         required: "Please enter 3 digit cvc number.",
        //         digits: "CVC number Could not be a charecter.",
        //         minlength: "Invalid CVC number.",
        //         maxlength: "Invalid CVC number."
        //     },
        //     expiry_year:{
        //         required: "Please enter your credit card exipry date.",
        //         digits: "Credit card Expiration year could not be a charecter.",
        //         minlength: "Invalid Expiry date.",
        //         maxlength: "Invalid Expiry date."
        //     },
        //     expiry_month:{
        //         required: "Please enter your credit card exipry month.",
        //         digits: "Credit card Expiration month could not be a charecter.",
        //         minlength: "Invalid Expiry month.",
        //         maxlength: "Invalid Expiry month."
        //     },
        //     card_name:{
        //         required:"Please enter your full name that was printed over your card."
        //     }
        // }
    });
    $('#myform2').validate({
        rules:{
           payment_method:{
                required:true,
            },
        //    no_installment:{
        //         required:true,
        //     },
        },
        messages:{
            no_installment:{
                required: "{{__('site.no_of_installment_required')}}",
            },
            payment_method:{
                required: "{{__('site.select_payment_method')}}",
            }
        },
    });
    if ($('#ccnum').length > 0) {
        $('#ccnum').validateCreditCard(function(res) {
            console.log(res);
            if (res.card_type !== null && res.card_type.name != '') {
                const tp = res.card_type.name;
                let clasForCCIcon = '';
                let colorForCCIcon = '';
                if (tp == 'visa') {
                    clasForCCIcon = 'fa-lg fa fa-cc-visa';
                    colorForCCIcon = 'navy';
                } else if (tp == 'mastercard') {
                    clasForCCIcon = 'fa-lg fa fa-cc-mastercard';
                    colorForCCIcon = 'red';
                } else if (tp == 'amex') {
                    clasForCCIcon = 'fa-lg fa fa-cc-amex';
                    colorForCCIcon = 'blue';
                } else if (tp == 'diners_club_international') {
                    clasForCCIcon = 'fa-lg fa fa-cc-diners-club';
                    colorForCCIcon = '#188fc0';
                } else {
                    clasForCCIcon = 'fa-lg fa fa-credit-card';
                    colorForCCIcon = 'gray';
                }
                $('#cc-icon').removeClass().addClass(clasForCCIcon).css('color', colorForCCIcon);
            } else {
                $('#cc-icon').removeClass().addClass('fa-lg fa fa-credit-card').css('color', 'lightgray');
            }
            if (res.valid) {
                $('#valid-indicator').css('color', 'green');
                $('#ind-backg').css('background-color', '#cbefcb');
            } else {
                $('#valid-indicator').css('color', 'lightgray');
                $('#ind-backg').css('background-color', '#e9ecef');
            }
        });
    }
    $('#pynw').click(function(){
        $('#myform1').submit();
    });
    $('#add_Pament_method').click(function(){
        $('#myform2').submit();
    });
    $('#upload').click(function(){
        $('#myform3').submit();
    });
    $('#check_Coupon').click(function(){
        var coupon= $('#coupon').val();
        console.log(coupon);
        if(coupon!=''){
            $.ajax({
             url: "{{route('check.coupon')}}",
             method: 'POST',
             data: {
                 jsonrpc: 2.0,
                 _token: "{{ csrf_token() }}",
                 params: {
                     coupon: coupon,
                    },
                },
                dataType: 'JSON'
            })
            .done(function (response) {
                if(response.status==1){
                    $('#coupon').attr('disabled', 'disabled');
                    $("#couponmessage").remove();
                    $("#coupon").after("<label class='validation' id='couponmessage' style='color:green;margin-bottom: 20px;'>"+response.message+"</label>");
                    $('#couponCode').val(coupon);

                    // toastr.success(response.message);
                }
                else if(response.status==2){
                    $("#couponmessage").remove();
                    $("#coupon").after("<label class='validation' id='couponmessage' style='color:red;margin-bottom: 20px;'>"+response.message+"</label>");
                    $('#coupon').val('');
                    // toastr.error(response.message);
                }
                else if(response.status==0){
                    $("#couponmessage").remove();
                    $("#coupon").after("<label class='validation' id='couponmessage' style='color:red;margin-bottom: 20px;'>"+response.message+"</label>");
                    $('#coupon').val('');
                    // toastr.error(response.message);
                }
                console.log(response)
            })
            .fail(function (error) {

            });
        } else{
            $('#coupon').val('');
        }

    });
    $('#country').change(function(){
        if($('#country').val()!=""){
            var reqData = {
              'jsonrpc' : '2.0',
              '_token' : '{{csrf_token()}}',
              'params' : {
                    'cn' : $('#country').val()
                }
            };
            $.ajax({
                url: "{{ route('front.get.state') }}",
                method: 'post',
                dataType: 'json',
                data: reqData,
                success: function(response){
                    if(response.status==1) {
                        var i=0, html="";
                        html = '<option value="">Selecionar Estado</option>';
                        for(;i<response.result.length;i++){
                            html+='<option value='+response.result[i].id+'>'+response.result[i].name+'</option>';
                        }
                        $('#state').html(html);
                        $('#state').addClass('valid');
                    }
                    else{

                    }
                }, error: function(error) {
                    console.error(error);
                }
            });
        }
    });
    $('#payment_method').change(function(){
        if($('#payment_method').val()!=""){
            if($('#payment_method').val()=='C'){
                $('.installment_checkbox').css('display','block');
                console.log('1');
            }
            else{
                $('.installment_checkbox').css('display','none');
                $('#installment_select').prop('checked', false);
                $('.no_installment').css('display','none');
                $('.no_installment_radio').removeClass('required');
            }
        }
        else{
            $('.installment_checkbox').css('display','none');
            $('.no_installment').css('display','none');
            $('#installment_select').prop('checked', false);
            $('.no_installment_radio').removeClass('required');
        }
    });
    $('#installment_select').click(function(){
        if($(this).prop("checked") == true){
            $('.no_installment').css('display','block');
            $('.no_installment_radio').addClass('required');
            }
            else if($(this).prop("checked") == false){
                $('.no_installment').css('display','none');
                $('.no_installment_radio').removeClass('required');
            }
    });
    $('#no_installment').change(function(){
        var no_ins=$(this).val();
        var sub_total='{{@$booking->sub_total}}';
        var x2 = 4.50;
        var x3 =5.00;
        var x4 =5.50;
        var x5 =6.50;
        var x6 =7.50;
        var x7 =8.50;
        var x8 =9.50;
        var x9 =10.50;
        var x10 =11.50;
        var x11 =12.00;
        var x12 =12.50;
        if(no_ins==2){
            var down=(1-(x2/100)-(5.49/100))
            var up=parseInt(sub_total)*(x2/100);
            var additional =(up/down);
        }
        if(no_ins==3){
            var down=(1-(x3/100)-(5.49/100))
            var up=parseInt(sub_total)*(x3/100);
            var additional =(up/down);
        }
        if(no_ins==4){
            var down=(1-(x4/100)-(5.49/100))
            var up=parseInt(sub_total)*(x4/100);
            var additional =(up/down);
        }
        if(no_ins==5){
            var down=(1-(x5/100)-(5.49/100))
            var up=parseInt(sub_total)*(x5/100);
            var additional =(up/down);
        }
        if(no_ins==6){
            var down=(1-(x6/100)-(5.49/100))
            var up=parseInt(sub_total)*(x6/100);
            var additional =(up/down);
        }
        if(no_ins==7){
            var down=(1-(x7/100)-(5.49/100))
            var up=parseInt(sub_total)*(x7/100);
            var additional =(up/down);
        }
        if(no_ins==8){
            var down=(1-(x8/100)-(5.49/100))
            var up=parseInt(sub_total)*(x8/100);
            var additional =(up/down);
        }
        if(no_ins==9){
            var down=(1-(x9/100)-(5.49/100))
            var up=parseInt(sub_total)*(x9/100);
            var additional =(up/down);
        }
        if(no_ins==10){
            var down=(1-(x10/100)-(5.49/100))
            var up=parseInt(sub_total)*(x10/100);
            var additional =(up/down)
        }
        if(no_ins==11){
            var down=(1-(x11/100)-(5.49/100))
            var up=parseInt(sub_total)*(x11/100);
            var additional =(up/down);
        }
        if(no_ins==12){
            var down=(1-(x12/100)-(5.49/100))
            var up=parseInt(sub_total)*(x12/100);
            var additional =(up/down);
        }
        console.log(additional.toFixed(2));
    });

    for (i = new Date().getFullYear()-18; i > 1900; i--){
        $('#dob_year').append($('<option />').val(i).html(i));
    }
    // for (i = 1; i < 13; i++){ $('#dob_month').append($('<option />').val(i).html(i));
    // }
    updateNumberOfDays();
    $('#dob_year, #dob_month').on("change", function(){
        updateNumberOfDays();
    });
    function updateNumberOfDays(){
        $('#dob_day').html('');
        month=$('#dob_month').val();
        year=$('#dob_year').val();
        days=daysInMonth(month, year);
        $('#dob_day').append($('<option />').val('').html('DD'));
        for(i=1; i < days+1 ; i++){
            if(i<10){
                $('#dob_day').append($('<option />').val(0+''+i).html(i));
            } else{
                $('#dob_day').append($('<option />').val(i).html(i));
            }

        }
    }
    function daysInMonth(month, year) {
        return new Date(year, month, 0).getDate();
    }
</script>
@if(@$booking->payment_type=='S'&& @$booking->payment_status=='I')
<script src="https://js.stripe.com/v3/"></script>
<script>
    const loading = (loading) => {
        if (loading) {
            $('.loading').css('display', 'flex');
        } else {
            $('.loading').css('display', 'none');
        }
    }
    $(document).ready(function() {
            var stripe = Stripe("{{ config('services.stripe.key') }}");
            console.log(stripe);
            const submitButton = document.getElementById('submit');
            const cardWrapper = $('.card-wrapper');
            loading(true);
            submitButton.disabled = true;
            cardWrapper.hide();
            $.ajax({
                url: "{{ route('landing.payment.stripe.create') }}",
                dataType: 'JSON',
                type: 'POST',
                data: {
                    jsonrpc: 2.0,
                    _token: "{{ csrf_token() }}",
                    params: {
                        id: "{{ encrypt(@$booking->id) }}"
                    }
                },
                success: (response) => {
                    loading(false);
                    if (response.error) {
                        toastr.error(response.error.message);

                    } else if (response.result) {
                        console.log(response.result);
                        initAndHandleCard(response.result);
                    } else {
                        toastr.error('Oops! Something went wrong initializing the payment.');
                    }
                }
            });
            const initAndHandleCard = (data) => {

                var elements = stripe.elements();
                var style = {
                    base: {
                        color: "#0245c1",
                        fontFamily: 'Arial, sans-serif',
                        fontWeight: 'bold',
                        fontSmoothing: "antialiased",
                        fontSize: "16px",
                        border: '1px solid black',
                        "::placeholder": {
                        }
                    },
                    invalid: {
                        fontFamily: 'Arial, sans-serif',
                        color: "red",
                    }
                };
                var card = elements.create("card", { style: style });
                // Stripe injects an iframe into the DOM
                cardWrapper.show();
                card.mount("#card-element");
                card.on("change", function (event) {
                    // Disable the Pay button if there are no card details in the Element
                    submitButton.disabled = event.empty;
                    if (event.error) {
                        $('#card-error').html(event.error.message).show(200);
                    } else {
                        $('#card-error').html('').hide(200);
                    }
                });
                $('#payment-form').submit(function(event) {
                    event.preventDefault();
                    // Complete payment when the submit button is clicked
                    payWithCard(stripe, card, data.clientSecret);
                });
            }
            const payWithCard = function(stripe, card, clientSecret) {
                loading(true);
                stripe.confirmCardPayment(clientSecret, {
                    payment_method: {
                        card: card
                    }
                })
                .then(function(result) {
                    console.log(result);
                    if (result.error) {
                        loading(false);
                        toastr.error(result.error.message);
                    } else {
                        $.ajax({
                        url: "{{ route('landing.payment.stripe') }}",
                        dataType: 'JSON',
                        type: 'POST',
                        data: {
                            jsonrpc: 2.0,
                            _token: "{{ csrf_token() }}",
                            params: {
                                id: "{{ encrypt(@$booking->id) }}",
                                result: result
                            }
                        },
                        success: (response) => {
                            toastr.success(response.success.message);
                            loading(false);
                            // location.reload();
                            // setTimeout(function () {
                                location.reload(true);
                            // }, 1000);
                        }
                    });
                    }
                });
            };
        });
</script>
@endif
@endsection
