@extends('layouts.app')
@section('title')
    @lang('site.welcome_to_dashboard')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
<link href="{{ URL::to('public/frontend/css/course_style.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ URL::to('public/frontend/css/course_responsive.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ URL::to('public/frontend/css/course_bootstrap.min') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::to('public/frontend/icofont/icofont.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::to('public/frontend/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />
<style>
.error{
	color:red;
}
.success{
	color:forestgreen;
}
.sweet-ok{
    background: #1781d2 !important;
}
.sweet-ok:hover{
    background: #000 !important;
}
</style>
@endsection
@section('content')
	<section class="wrapper2">
		@section('header')
		@include('includes.professional_header')
		@endsection
		<!-- section-header.// -->
		<!-- ========================= SECTION CONTENT ========================= -->
		<nav class="navbar navbar-expand-lg navbar-dark sticky-top">
			<div class="w-100">
				<!-- <div class="top-bar">
					<div class="top-bar-left-container">
						<a href="#url" class="top-bar-button"> <i class="icofont-close-line"></i> </a>
						<div class="d-none d-md-flex align-items-center">
							<h4 class="ml-4">Courses</h4> </div>
					</div>
					<div class="top-bar-center-container"> <a href="#url" class="user_llk">Your First Course <i class="icofont-caret-down"></i></a>
						<div class="show01 search-category" style="display: none;">
							<div class="select2-search">
								<input type="text" name=""> </div>
							<ul class="select2-results">
								<li class="select2-no-results">No matches found</li>
							</ul>
						</div>
					</div>
					<div class="top-bar-right-container"> <a href="#url" class="topbar-right-btn">
						build landing page <i class="icofont-long-arrow-right"></i>
					</a> </div>
				</div> -->
				<div class="navbar-menu">
					<div class="container-fluid">
						<div class="all-menu-sec">
							<!---<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav"> <span class="navbar-toggler-icon"></span> </button> --->
							<div class="" id="main_nav">
								<ul class="navbar-nav">
									<li class="nav-item active show_course_view tab_choose_temp"> <a class="nav-link" href="javascript:;">   @lang('site.choose_template') </a> </li>
									<li class="nav-item show_course_view tab_header"> <a class="nav-link" href="javascript:;">   @lang('site.header') </a> </li>
									<li class="nav-item show_course_view tab_content"> <a class="nav-link" href="javascript:;">   @lang('site.content') </a> </li>
									<li class="nav-item show_course_view tab_demo"> <a class="nav-link" href="javascript:;">   @lang('site.demo') </a> </li>
									<li class="nav-item show_course_view tab_products"> <a class="nav-link" href="javascript:;">   @lang('site.products') </a> </li>
									<li class="nav-item show_course_view tab_design"> <a class="nav-link" href="javascript:;">   @lang('site.design') </a> </li>
								</ul>
							</div>
							<div class="navbar__actions">
								<button class="preview-btn" title="Preview course as an enrolled student"> <i class="icofont-eye-alt"></i> Preview </button>
							</div>
						</div>
						<!-- navbar-collapse.// -->
					</div>
				</div>
				<!-- container //  -->
			</div>
		</nav>
		<div class="page_builder">
			<div class="course_view mobile_chapter_list height_bottom">
				<div class="list-view-sessoin">
					<div>
						<div id="main">
                            <div class="tab_target" id="choose_temp">
                                @foreach(@$templates as $template)
                                    <div class="card landing_temp_card {{ @$template->id == 1 ? 'active_cert' : '' }}" data-id="{{ @$template->id }}">
                                        <img class="card-img-top" src="{{url('/')}}/public/frontend/images/cart_temp{{ @$template->id }}.png" alt="Card image cap">
                                        <div class="card-body">
                                            <h5 class="card-title">{{ @$template->template_name }}</h5>
                                        </div>
                                    </div>
                                    <br>
                                @endforeach
                            </div>
                            <div class="tab_target" id="header" style="display: none;">
                                <form action="{{ route('store.landing.page.template') }}" id="myHeaderForm">
                                    <div class="cert_cntnt">
                                        <label for="header_image">@lang('site.upload_picture')</label>
                                        <input type="file" name="header_image" id="header_image" class="form-control">
                                    </div>
                                    <div class="cert_cntnt">
                                        <label for="header_line_1">Header Line 1</label>
                                        <input type="text" name="header_line_1" id="header_line_1" class="form-control required mb-3" value="{{@$detail->header_line_1}}">
                                    </div>
                                    <div class="cert_cntnt">
                                        <label for="header_line_2">Header Line 2</label>
                                        <input type="text" name="header_line_2" id="header_line_2" class="form-control required mb-3" value="{{@$detail->header_line_2}}">
                                    </div>
                                    <div class="cert_cntnt">
                                        <label for="header_button">Header Button</label> <br>
                                        <label for="header_btn_yes">
                                            <input type="radio" name="header_btn" value="Y" id="header_btn_yes" @if(@$detail->header_btn == 'Y') checked @endif>
                                            @lang('site.yes')
                                        </label>
                                        <label for="header_btn_no">
                                            <input type="radio" name="header_btn" value="N" id="header_btn_no" @if(@$detail->header_btn == 'N') checked @endif>
                                            @lang('site.no')
                                        </label>
                                    </div>
                                    <div id="header_btn_action">

                                        <div class="cert_cntnt">
                                            <label for="header_button_text">Header Button Text</label>
                                            <input type="text" name="header_button_text" id="header_button_text" class="form-control required mb-3" value="Contact Us">
                                        </div>
                                        <div class="cert_cntnt">
                                            <label for="header_button_type">Header Button Type</label> <br>
                                            <label for="header_btn_lead">
                                                <input type="radio" name="header_button_type" value="LEAD" id="header_btn_lead" @if(@$detail->header_button_type == 'LEAD') checked @endif>
                                                Lead
                                            </label>
                                            <label for="header_btn_url">
                                                <input type="radio" name="header_button_type" value="URL" id="header_btn_url" @if(@$detail->header_button_type == 'URL') checked @endif>
                                                URL
                                            </label>
                                        </div>
                                        <div class="cert_cntnt" id="header_button_link_div">
                                            <label for="header_button_link">Header Button Link</label>
                                            <input type="url" name="header_button_link" id="header_button_link" class="form-control mb-3" value="">
                                        </div>
                                        <div id="header_button_lead_div" style="display: none;">
                                            <div class="cert_cntnt row">
                                                <div class="col-md-6">
                                                    <label for="lead_name">
                                                        <input type="checkbox" name="lead_name" id="lead_name" class="mb-3" @if(@$detail->lead_name) checked @endif>
                                                        Name
                                                    </label>
                                                    <div class="clearfix"></div>
                                                    <label for="lead_email">
                                                        <input type="checkbox" name="lead_email" id="lead_email" class="mb-3" @if(@$detail->lead_email) checked @endif>
                                                        Email
                                                    </label>
                                                    <div class="clearfix"></div>
                                                    <label for="lead_mobile">
                                                        <input type="checkbox" name="lead_mobile" id="lead_mobile" class="mb-3" @if(@$detail->lead_mobile) checked @endif>
                                                        Mobile
                                                    </label>
                                                    <div class="clearfix"></div>
                                                    <label for="lead_address">
                                                        <input type="checkbox" name="lead_address" id="lead_address" class="mb-3" @if(@$detail->lead_address) checked @endif>
                                                        Address
                                                    </label>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="lead_facebook">
                                                        <input type="checkbox" name="lead_facebook" id="lead_facebook" class="mb-3" @if(@$detail->lead_facebook) checked @endif>
                                                        Facebook link
                                                    </label>
                                                    <div class="clearfix"></div>
                                                    <label for="lead_instagram">
                                                        <input type="checkbox" name="lead_instagram" id="lead_instagram" class="mb-3" @if(@$detail->lead_instagram) checked @endif>
                                                        Instagram link
                                                    </label>
                                                    <div class="clearfix"></div>
                                                    <label for="lead_linkedin">
                                                        <input type="checkbox" name="lead_linkedin" id="lead_linkedin" class="mb-3" @if(@$detail->lead_linkedin) checked @endif>
                                                        LinkedIn link
                                                    </label>
                                                    <div class="clearfix"></div>
                                                    <label for="lead_website">
                                                        <input type="checkbox" name="lead_website" id="lead_website" class="mb-3" @if(@$detail->lead_website) checked @endif>
                                                        Website
                                                    </label>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="col-md-12">
                                                    <label for="lead_message">
                                                        <input type="checkbox" name="lead_message" id="lead_message" class="mb-3" @if(@$detail->lead_message) checked @endif>
                                                        Message
                                                    </label>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cert_cntnt">
                                            <label for="above_course">Header reverse counter</label>  <br>
                                            <label for="header_reverse_yes">
                                                <input type="radio" name="header_reverse" value="Y" id="header_reverse_yes">
                                                @lang('site.yes')
                                            </label>
                                            <label for="header_reverse_no">
                                                <input type="radio" name="header_reverse" value="N" id="header_reverse_no" checked>
                                                @lang('site.no')
                                            </label>
                                        </div>
                                        <div class="cert_cntnt" id="header_reverse_action" style="display: none;">
                                            <label for="header_counter_date">Reverse counter date</label>
                                            <input type="text" name="header_counter_date" id="header_counter_date" class="form-control mb-3" value="">
                                        </div>
                                    </div>
                                </form>
                            </div>
						</div>
						{{-- <div class="tree-tip">
							<div class="tip__title"> <img src="{{ URL::to('public/frontend/images/tip.png') }}"> Pro Tip </div>
							<!-- <p class="tip__description">You can customize the course completion experience with a certificate or a custom completion page!</p> <a href="#url">Course completion settings</a> </div> -->
							<p class="tip__description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis ex dicta incidunt eum mollitia rerum pariatur saepe cum quia repellat!</p>
                        </div> --}}
					</div>
					<div></div>
				</div>
				<div class="builder__footer">
					<div class="tree_action">
						<div class="builder-add-chapter">
							<a href="javascript:;" class="button button--primary w-100" id="template_confirm"> @lang('site.select_this_template')</a>
							<a href="javascript:;" class="button button--primary w-100" id="save_header" style="display: none;"> @lang('site.save_header')</a>
							<a href="javascript:;" class="button button--primary w-100" id="save_content" style="display: none;"> @lang('site.save_content')</a>
						</div>
					</div>
				</div>
			</div>
			<div class="builder_section">
				<div class="wrap wrap-edit">
					<div class="ember-view">
                        <input type="text" value="1" id="template_number" hidden>
                        <h1 id="temp_title">Título do Modelo</h1>
                        <iframe src="{{URL::to('/landing-page-template/1')}}" style="width:100%; height:100vh;" id="main_iframe" frameborder="0"></iframe>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
@section('footer')
{{-- @include('includes.footer') --}}
<script>
    var template_id = 0;
    var template_confirmed = false;

    $(document).ready(function(){
        
        $('myHeaderForm').validate();

        $('.landing_temp_card').click(function() {
            var id = $(this).data('id');
            console.log(id);
            var url = "{{URL::to('/landing-page-template/')}}/"+id;
            $('#template_number').val(id);
            $('#main_iframe').attr('src', url);
            $('.active_cert').removeClass('active_cert');
            $(this).addClass('active_cert');

            if(id == 1){
                $('#header_line_1').val("Lorem ipsum dolor sit ");
                $("#header_line_2").val("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
            } else if(id == 2){
                $("#header_line_1").val("Lorem ipsum dolor sit Lorem ipsum");
                $("#header_line_2").val("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Sed pretium ligula.");
            }
        });

        $('#template_confirm').click(function(){
            sweetAlert();
        });

    });
    
    $(document).delegate('#template_name', 'keyup', function(e){
        e.preventDefault();
        // console.log("Event fired");
        var reqData = {
            'jsonrpc' : '2.0',
            '_token' : '{{csrf_token()}}',
            'params' : {
                'template_number': $('#template_number').val(),
                'template_name': $('#template_name').val(),
            }
        };
        $.ajax({
            url: "{{ route('check.template.name') }}",
            method: 'post',
            dataType: 'json',
            data: reqData,
            async: false,
            success: function(res){
                console.log(res);
                if(res.status == 'NA'){
                    $('#err_template_name').html('<label class="error">@lang("site.this_name_already_exists")</label>');
                    $('.sweet-ok').attr('disabled',true);
                } else {
                    $('#err_template_name').html('');
                    $('.sweet-ok').removeAttr('disabled');
                }
            },error: function(err){
                console.log(err);
            },
        });
    });

    function sweetAlert(){
        var html = document.createElement("div");
        $(html).html(`<input type="text" class="form-control" id="template_name" placeholder="@lang('site.landing_page_name')" /><div id="err_template_name"></div>`);
        console.log(html);

        swal({
            title: '@lang("site.landing_page_name_title")',
            text : '@lang("site.landing_page_name_text")',
            content: html,
            buttons: {
                cancel : 'Cancel',
                confirm : {text:'OK', className:'sweet-ok'}
            },
        }).then((isConfirmed) => {
            if (isConfirmed) {
                storeTemplate();
            }
        });
    }

    function storeTemplate(){
        var reqData = {
            'jsonrpc' : '2.0',
            '_token' : '{{csrf_token()}}',
            'params' : {
                'template_number': $('#template_number').val(),
                'template_name': $('#template_name').val(),
            }
        };
        $.ajax({
            url: "{{ route('store.landing.page.template') }}",
            method: 'post',
            dataType: 'json',
            data: reqData,
            async: false,
            success: function(res){
                console.log(res);
                template_id = res.template.id;
                console.log("TEMP ID: "+template_id);
                $('#template_id').val(template_id);

                $('.active').removeClass('active');
                $('.tab_header').addClass('active');
                $('.tab_target').hide();
                $('.tab_choose_temp').hide();
                $('#header').show();
                $('#template_confirm').hide();
                $('#save_header').show();
                $('#temp_title').text($('#template_name').val());
                template_confirmed = true;
            },
            error: function(err){
                console.log(err);
            },
        });
    }

    // HEADER
        $('#header_image').change(function(){
            var file = this.files[0];
            var reader = new FileReader();
            reader.onloadend = function (e) {
                // console.log(reader.result);
                $("#main_iframe").contents().find('.header_image').attr('src', e.target.result);
            }
            if (file) {
                reader.readAsDataURL(file);
            } else {
                img_array = ["ban-img2.png", "watch-img-1.png"];
                val = $('template_number').val();
                $("#main_iframe").contents().find('.header_image').attr('src', "{{URL::asset('public/frontend/landing_page2/images/')}}/"+img_array[val+1]);
            }
        });
        $('#header_line_1').on('keyup change', function(){
            var str = $(this).val();
            if($('#template_number').val() == 1){
                var html = "";
                if(str.indexOf(" ") === -1){
                    html = "<span>" + str + "</span>";
                } else {
                    var words = str.split(" ");
                    html = "<span>" + words[0] + "</span> " + str.substring( str.indexOf(" ") + 1, str.length );
                }
                $("#main_iframe").contents().find('.header_line_1').html(html);
            } else if($('#template_number').val() == 2){
                $("#main_iframe").contents().find('.header_line_1').text(str);
            }
        });
        $('#header_line_2').on('keyup change', function(){
            $("#main_iframe").contents().find('.header_line_2').text($(this).val());
        });
        $('input[type=radio][name=header_reverse]').change(function() {
            if (this.value == 'Y') {
                $('#header_reverse_action').show();
                $('#header_reverse_action').addClass('required');
            } else if (this.value == 'N') {
                $('#header_reverse_action').hide();
                $('#header_reverse_action').removeClass('required');
            }
        });
        $('input[type=radio][name=header_btn]').change(function() {
            if (this.value == 'Y') {
                $('#header_btn_action').show();
                $('#header_button_text').addClass('required');
                $("#main_iframe").contents().find('.header_btn').show();
            } else if (this.value == 'N') {
                $('#header_btn_action').hide();
                $('#header_button_text').removeClass('required');
                $("#main_iframe").contents().find('.header_btn').hide();
            }
        });
        $('input[type=radio][name=header_button_type]').change(function(){
            if (this.value == 'URL'){
                $('#header_button_link_div').show();
                $('#header_button_lead_div').hide();
            } else if (this.value == 'LEAD'){
                $('#header_button_lead_div').show();
                $('#header_button_link_div').hide();
            }
        });
        $('#header_button_text').on('keyup change', function(){
            if($('#template_number').val() == 1){
                $("#main_iframe").contents().find('.header_button_text').text($(this).val());
            } else if($('#template_number').val() == 2){
                $("#main_iframe").contents().find('.header_button_text').html($(this).val() + `<i class="fa fa-long-arrow-right"></i>`);
            }
        });
        $('#save_header').click(function(){
            if($("#myHeaderForm").valid()){
                updateTemplate();
            }
        });
    // HEADER

    function updateTemplate(){
        var style = "";
        var uploadFormData = new FormData();
        uploadFormData.append("jsonrpc", "2.0");
        uploadFormData.append("_token", '{{csrf_token()}}');
        uploadFormData.append('template_number', $('#template_number').val());
        uploadFormData.append('header_image', $('#header_image').val() != "" ? $('#header_image')[0].files[0] : null);
        uploadFormData.append('header_line_1', $('#header_line_1').val());
        uploadFormData.append('header_line_2', $('#header_line_2').val());
        uploadFormData.append('header_button', $('input[type=radio][name=header_btn]').val());
        uploadFormData.append('header_button_text', $('#header_button_text').val());
        uploadFormData.append('header_button_type', $('input[type=radio][name=header_button_type]').val());
        uploadFormData.append('header_button_link', $('#header_button_link').val());
        uploadFormData.append('header_reverse', $('input[type=radio][name=header_reverse]').val());
        uploadFormData.append('header_counter_date', $('#header_counter_date').val());
        // uploadFormData.append('content', $("#main_iframe").contents());

        // uploadFormData.append('primary_color', $('#primary_color').val());
        // uploadFormData.append('secondary_color', $('#secondary_color').val());
        // uploadFormData.append('font_style', $('#font_style').val());

        $.ajax({
            url: "{{url('/')}}/landing-page/store/"+template_id,
            method: 'post',
            data: uploadFormData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function(res){
                console.log(res);
                if(res.status == "success") {
                    toastr.success(res.message);
                    template_id = res.template.id;
                    console.log("TEMP ID: "+template_id);
                    $('#background').val('');
                    $('.template'+ $('#template_number').val() ).attr('style', style);
                } else toastr.error(res.message);
            },
            error: function(err){
                console.log(err);
            },
        });
    }

</script>
@endsection
