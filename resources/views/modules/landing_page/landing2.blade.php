<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>Netbhe</title>
    <!--favicon-->
    <link rel="shortcut icon" type="image/png" href="{{URL::asset('public/frontend/landing_page2/images/favicon.png')}}">
    <!--bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('public/frontend/landing_page2/css/bootstrap.min.css')}}">
    <!--owl carousel css-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('public/frontend/landing_page2/css/owl.carousel.min.css')}}">
    <!--magnific popup css-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('public/frontend/landing_page2/css/magnific-popup.css')}}">
    <!-- fancybox-media -->
	<link rel="stylesheet" href="{{URL::asset('public/frontend/landing_page2/css/jquery.fancybox.css')}}">
    <!--font awesome css-->
	<link rel="stylesheet" type="text/css" href="{{URL::asset('public/frontend/landing_page1/css/font-awesome/all.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('public/frontend/landing_page2/css/font-awesome.min.css')}}">
    <!--icofont css-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('public/frontend/landing_page2/css/icofont.min.css')}}">
    <!--animate css-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('public/frontend/landing_page2/css/animate.css')}}">
    <!--main css-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('public/frontend/landing_page2/css/style.css')}}">
    <!--responsive css-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('public/frontend/landing_page2/css/responsive.css')}}">
    <style>
        .MultiCarousel { float: left; overflow: hidden; padding: 15px; width: 100%; position:relative; }
        .MultiCarousel .MultiCarousel-inner { transition: 1s ease all; float: left; }
            .MultiCarousel .MultiCarousel-inner .item { float: left;}
            .MultiCarousel .MultiCarousel-inner .item > div { margin:10px; }
        .MultiCarousel .leftLst, .MultiCarousel .rightLst { position:absolute; border-radius:50%;top:calc(50% - 20px); }
        .MultiCarousel .leftLst { left:0; }
        .MultiCarousel .rightLst { right:0; }
        .contact-form label.error{
            font-size: 15px !important;
            font-weight: normal !important;
        }
        .contact-form .error{
            color: red;
        }
    </style>
        <script src="https://kit.fontawesome.com/2710be599b.js" crossorigin="anonymous"></script>
</head>

<body>
    <div id="landing_preloader" class="u-loading">
        <div class="u-loading_symbol landing_logo_loader">
            @if(@$detail->landing_logo)
                <img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->landing_logo_thumbnail}}" alt="" class="landing_logo" />
            @else
                <img src="{{ URL::to('public/frontend/images/loader-logo.png') }}" alt="">
            @endif
        </div>
    </div>
    <!--start header-->
    <header id="header">
        <!-- <div class="header-top">
            <div class="container">
                <div class="row header-top-wrap">
                    <div class="col-md-8">
                        <div class="header-cont-info">
                            <ul>
                                <li><i class="fa fa-phone"></i> Call Us: +98 54321 67890</li>
                                <li><i class="fa fa-envelope"></i> Email Us: support@example.com</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="header-social text-right">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="mainmenu">
            <div class="container">
                <nav class="navbar navbar-expand-lg">
                    <div class="container">
                        <!--logo-->
                        <a class="logo" href="javascript:;">
                            @if(@$detail->landing_logo)
                                <img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->landing_logo}}" alt="logo" class="landing_logo" />
                            @else
                                <img src="{{URL::asset('public/frontend/landing_page2/images/logo.png')}}" alt="logo" class="landing_logo" />
                            @endif
                        </a>

                        <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="icon-bar"><i class="icofont-navigation-menu"></i></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarContent">
                            <ul class="navbar-nav mx-auto text-center">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#" data-scroll-nav="0">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-scroll-nav="1">Features</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-scroll-nav="2">Review</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-scroll-nav="3">Product</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-scroll-nav="4">Faq</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-scroll-nav="5">Contact</a>
                                </li>
                            </ul>
                            <ul class="navbar-nav shop-btn">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#" data-scroll-nav="3">Shop Now</a>
                                </li>
                            </ul>
                        </div>
                    </div> -->

                    {{-- <div class="header-social text-right">
                        <ul>
                            <li class="link_icons link_icons_linkedin" @if(!@$detail->link_linkedin) style="display:none;" @endif ><a href="{{ @$detail->link_linkedin ?? 'javascript:;' }}" class="color_primary_font color_primary_border color_primary_hover" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                            <li class="link_icons link_icons_facebook" @if(!@$detail->link_facebook) style="display:none;" @endif ><a href="{{ @$detail->link_facebook ?? 'javascript:;' }}" class="color_primary_font color_primary_border color_primary_hover" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li class="link_icons link_icons_twitter" @if(!@$detail->link_twitter) style="display:none;" @endif ><a href="{{ @$detail->link_twitter ?? 'javascript:;' }}" class="color_primary_font color_primary_border color_primary_hover" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li class="link_icons link_icons_instagram" @if(!@$detail->link_instagram) style="display:none;" @endif ><a href="{{ @$detail->link_instagram ?? 'javascript:;' }}" class="color_primary_font color_primary_border color_primary_hover" target="_blank"><i class="fab fa-instagram"></i></a></li>
                            <li class="link_icons link_icons_pinterest" @if(!@$detail->link_pinterest) style="display:none;" @endif ><a href="{{ @$detail->link_pinterest ?? 'javascript:;' }}" class="color_primary_font color_primary_border color_primary_hover" target="_blank"><i class="fa fa-pinterest"></i></a></li>
                            <li class="link_icons link_icons_youtube" @if(!@$detail->link_youtube) style="display:none;" @endif ><a href="{{ @$detail->link_youtube ?? 'javascript:;' }}" class="color_primary_font color_primary_border color_primary_hover" target="_blank"><i class="fa fa-youtube"></i></a></li>
                            <li class="link_icons link_icons_tiktok" @if(!@$detail->link_tiktok) style="display:none;" @endif ><a href="{{ @$detail->link_tiktok ?? 'javascript:;' }}" target="_blank" class="color_primary_font color_primary_border color_primary_hover"><i class="fa-brands fa-tiktok"></i></a></li>
                        </ul>
                    </div> --}}
                </nav>
            </div>
        </div>
        <!--end mainmenu-->
    </header>
    <!--end header-->
    <!--start hero area-->
    <section id="hero-area" data-scroll-index="0" class="ban_sec color_tertiary">
        <div class="container">
            <div class="row">
                <!--start caption-->
                <div class="col-md-7">
                    <div class="caption d-table">
                        <div class="d-table-cell align-middle">
                            <h1 class="header_line_1 color_secondary_font">{{@$detail->header_line_1  ?? 'Lorem ipsum dolor sit Lorem ipsum' }}</h1>
                            <p class="header_line_2 color_secondary_font">{{@$detail->header_line_2  ?? 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Sed pretium ligula.' }}</p>

                            <ul class="header_btn" @if(@$detail->header_button=='N') style="display:none" @endif>
				                @if(@$detail->header_button_type == 'URL')
                                    <li><a href="{{@$detail->header_button_link ?? 'javascript:;'}}" class="page_btn header_button_text color_primary">{{ @$detail->header_button_text ?? 'Contact Us'}}<i class="fa fa-long-arrow-right"></i></a></li>
                                @else
					                @if(@$detail->lead_position == 'P')
                                        <li><a href="#" class="page_btn header_button_text color_primary" data-toggle="modal" data-target="#myModal">{{ @$detail->header_button_text ?? 'Contact Us'}}<i class="fa fa-long-arrow-right"></i></a></li>
                                    @else
                                        <li><a href="#contact-area" class="page_btn banBotArw header_button_text color_primary">{{ @$detail->header_button_text ?? 'Contact Us'}}<i class="fa fa-long-arrow-right"></i></a></li>
                                    @endif
                                @endif
                            </ul>
                            @if(@$detail)
                            <div class="counting-date header_reverse" @if(@$detail->header_reverse_counter == 'N') style="display: none;" @endif>
                                <p>
                                    <p class="header_reverse_text">Esta promoção termina em:<p>
                                    <span class="header_counter color_primary_font"></span>
                                </p>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                <!--end caption-->
                <!--start caption image-->
                <div class="col-md-5">
                    <div class="caption-img d-table text-center">
                        <div class="d-table-cell align-middle">
                            <!-- <img src="{{URL::asset('public/frontend/landing_page2/images/watch-img-1.png')}}" class="img-fluid animation-jump header_image" alt="image"> -->
                            <div class="banner-rightimg img-fluid animation-jump banner_only_img">
                                <span>
                                    @if(@$detail->header_image)
                                        <img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->header_image}}" class="header_image" alt="">
                                    @else
                                        <img src="{{URL::asset('public/frontend/landing_page2/images/wef-min.jpg')}}" class="header_image">
                                    @endif
                                </span>
						        <em class="banner_right_after color_primary"></em>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end caption image-->
            </div>
        </div>
    </section>
    <!--end hero area-->
    <div class="whyus_sec" @if(@$detail->why_us_section == 'N') style="display: none;" @endif>
        <div class="container">
            <div class="page_hed">
                <h2 class="whyus_main_title color_secondary_font"> {{@$detail->why_us_heading ?? 'How it works'}} </h2>
                <p class="whyus_main_desc color_secondary_font">{{@$detail->why_us_description ?? 'Your dream home is in good hands.'}}</p>
            </div>
            <div class="whyus_inr">
                <div class="row">
                    @if(@$detail->getWhyUs && count(@$detail->getWhyUs) > 0)
                        @foreach(@$detail->getWhyUs as $k=>$row)
                            @php
                                $icon = 'fas fa-file-invoice';
                                if(($k+1)%3 == 0) $icon = 'fas fa-chalkboard-teacher';
                                elseif(($k+1)%2 == 0) $icon = 'fas fa-handshake';
                            @endphp
                            <div class="col-sm-4 mb-3" id="why_box_{{@$k + 1}}">
                                <div class="whyus_bx color_primary_light color_primary_border">
                                    <em id="whyus_img_{{@$k + 1}}" class="color_primary">
                                        @if(strpos( @$row->filetype, 'image' ) !== false)
                                            <img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$row->filename}}" alt="">
                                        @else
                                            <!-- {!! @$row->filename !!} -->
                                            <i class="{{@$icon}}"></i>
                                        @endif
                                    </em>
                                    <strong><a href="javascript:;" id="whyus_title_{{@$k + 1}}" class="color_secondary_font color_primary_font_hover">{{@$row->title ?? 'certified contractors'}}</a></strong>
                                    <p id="whyus_desc_{{@$k + 1}}" class="color_secondary_font">{{@$row->description ?? 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.'}}</p>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="col-sm-4 mb-3" id="why_box_1">
                            <div class="whyus_bx color_primary_light color_primary_border">
                                <em id="whyus_img_1" class="color_primary">
                                    <i class="fas fa-file-invoice"></i>
                                </em>
                                <strong><a href="javascript:;" id="whyus_title_1" class="color_secondary_font color_primary_font_hover">certified contractors</a></strong>
                                <p id="whyus_desc_1" class="color_secondary_font">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.</p>
                            </div>
                        </div>
                        <div class="col-sm-4 mb-3" id="why_box_2">
                            <div class="whyus_bx color_primary_light color_primary_border">
                                <em id="whyus_img_2" class="color_primary">
                                    <i class="fas fa-handshake"></i>
                                </em>
                                <strong><a href="javascript:;" id="whyus_title_2" class="color_secondary_font color_primary_font_hover">employees you can trust</a></strong>
                                <p id="whyus_desc_2" class="color_secondary_font">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.</p>
                            </div>
                        </div>
                        <div class="col-sm-4 mb-3" id="why_box_3">
                            <div class="whyus_bx color_primary_light color_primary_border">
                                <em id="whyus_img_3" class="color_primary">
                                    <i class="fas fa-chalkboard-teacher"></i>
                                </em>
                                <strong><a href="javascript:;" id="whyus_title_3" class="color_secondary_font color_primary_font_hover">Your dream is our plan</a></strong>
                                <p id="whyus_desc_3" class="color_secondary_font">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.</p>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!--start about area-->
    <div class="single_content_div" @if(@$detail->body_single_content == 'Y') @else style="display: none;" @endif>
        <section id="about-area">
            <div class="container">
                <div class="row">
                    <!--start about image-->
                    <div class="col-lg-6 col-md-4">
                        <div class="about-img bg d-table">

                            <em class="cur1 color_primary"></em>
                            <em class="cur2 color_secondary"></em>
                            <em class="cur3 color_primary"></em>

                            <div class="align-middle d-table-cell">
                                <!-- <img src="{{URL::asset('public/frontend/landing_page2/images/gfg-min.png')}}" class="img-fluid animation-jump" alt="image"> -->
                                <div class="about-imgs img-fluid animation-jump">
                                    <span class="single_content_file_div @if(@$detail->image_style=='S') square_image @endif" id="content_file_single" >
							            @if(@$detail->body_single_content_image)
                                            <img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->body_single_content_image}}" class="single_content_file">
                                        @else
                                            <img src="{{URL::asset('public/frontend/landing_page2/images/gfg-min.png')}}" class="single_content_file">
                                        @endif
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end about image-->
                    <!--start about content-->
                    <div class="col-lg-6 col-md-8">
                        <div class="about-cont">
                            {{-- <h6 class="color_primary_font">About</h6> --}}
                            <h2 class="color_secondary_font body_single_content_heading">{{ @$detail->body_single_content_heading ?? 'Lorem ipsum dolor sit amet' }}</h2>
                            <p class="body_single_content_desc">{{ @$detail->body_single_content_desc ?? 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis.Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.' }}</p>
                            <!-- <div class="row">
                                <div class="col-md-6">
                                    <ul>
                                        <li><i class="fa fa-check color_primary_font"></i>Lorem ipsum dolor</li>
                                        <li><i class="fa fa-check color_primary_font"></i>Lorem ipsum</li>
                                        <li><i class="fa fa-check color_primary_font"></i>Lorem ipsum dolor</li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <ul>
                                        <li><i class="fa fa-check color_primary_font"></i>Lorem ipsum dolor</li>
                                        <li><i class="fa fa-check color_primary_font"></i>Lorem ipsum</li>
                                        <li><i class="fa fa-check color_primary_font"></i>Lorem ipsum dolor</li>
                                    </ul>
                                </div>
                            </div> -->
                            <div class="about-btn body_single_content_button" @if(@$detail->body_single_content_button == 'N') style="display: none;" @endif>
                                <a href="#product-area" class="color_primary body_single_content_button_text">{{ @$detail->body_single_content_button_text ?? 'Read More' }}</a>
                            </div>
                        </div>
                    </div>
                    <!--end about content-->
                </div>
            </div>
        </section>
    </div>
    <div class="multiple_content_div" @if(@$detail->body_single_content == 'N') @else style="display: none;" @endif>
        <div id="content_div_1">
            <section id="about-area">
                <div class="container">
                    <div class="row">
                        <!--start about image-->
                        <div class="col-lg-6 col-md-4">
                            <div class="about-img bg d-table">

                                <em class="cur1 color_primary"></em>
                                <em class="cur2 color_secondary"></em>
                                <em class="cur3 color_primary"></em>

                                <div class="align-middle d-table-cell">
                                    <!-- <img src="{{URL::asset('public/frontend/landing_page2/images/gfg-min.png')}}" class="img-fluid animation-jump" alt="image"> -->
                                    <div class="about-imgs img-fluid animation-jump" >
                                        <span class="single_content_file_div content_file_1 @if(@$detail->getContentDetails[0]->content_file_type == 'Y') video_youtube @endif @if(@$detail->getContentDetails[0]->content_file_type != 'Y' && @$detail->getContentDetails[0]->image_style=='S') square_image @endif" id="content_file_1">
                                            @if(@$detail->getContentDetails[0]->content_file)
                                                @if(@$detail->getContentDetails[0]->content_file_type == 'I')
                                                    <img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->getContentDetails[0]->content_file}}" alt="">

                                                @else
                                                <iframe src="https://www.youtube.com/embed/{{@$detail->getContentDetails[0]->content_file}}" frameborder="0" allowfullscreen></iframe>
                                                @endif
                                            @else
                                                <img src="{{URL::asset('public/frontend/landing_page2/images/gfg-min.png')}}">
                                            @endif
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end about image-->
                        <!--start about content-->
                        <div class="col-lg-6 col-md-8">
                            <div class="about-cont">
                                {{-- <h6 class="color_primary_font">About</h6> --}}
                                <h2 class="color_secondary_font content_heading_1">{{@$detail->getContentDetails[0]->content_heading ?? 'Lorem ipsum dolor sit amet'}}</h2>
                                <p class="content_desc_1">{{ @$detail->getContentDetails[0]->content_desc ?? 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis.Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.' }}</p>
                                <!-- <div class="row">
                                    <div class="col-md-6">
                                        <ul>
                                            <li><i class="fa fa-check color_primary_font"></i>Lorem ipsum dolor</li>
                                            <li><i class="fa fa-check color_primary_font"></i>Lorem ipsum</li>
                                            <li><i class="fa fa-check color_primary_font"></i>Lorem ipsum dolor</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6">
                                        <ul>
                                            <li><i class="fa fa-check color_primary_font"></i>Lorem ipsum dolor</li>
                                            <li><i class="fa fa-check color_primary_font"></i>Lorem ipsum</li>
                                            <li><i class="fa fa-check color_primary_font"></i>Lorem ipsum dolor</li>
                                        </ul>
                                    </div>
                                </div> -->
                                <div class="about-btn">
                                    <!-- <a href="#product-area" class="color_primary content_button_1">{{ @$detail->body_single_content_button_text ?? 'Read More' }}</a> -->
                                    <a href="{{ @$detail->getContentDetails[0]->content_button_link ?? 'javascript:;' }}" class="color_primary content_button_1 butoon" @if(@$detail->getContentDetails[0]->content_button == 'Y') @else style="display: none;" @endif>{{ @$detail->getContentDetails[0]->content_button_text ?? 'Read More' }}</a>
                                </div>
                            </div>
                        </div>
                        <!--end about content-->
                    </div>
                </div>
            </section>
        </div>
        <div class="single_box cont_move_right" id="content_div_2">
            <div class="container">
                <div class="row">
                    <div class="col-sm-7">
                        <h2 class="color_secondary_font content_heading_2">
                            {{-- @if(@$detail->getContentDetails[1]->content_heading)
                                <span>{{strtok(@$detail->getContentDetails[1]->content_heading, " ")}}</span> {{strstr(@$detail->getContentDetails[1]->content_heading," ")}}
                            @else
                                <span>Online</span> Learning
                            @endif --}}
                            {{ @$detail->getContentDetails[1]->content_heading ?? 'Lorem ipsum dolor sit amet' }}
                        </h6>
                        <div class="single_text">
                            <p class="color_secondary_font content_desc_2">
                                {{ @$detail->getContentDetails[1]->content_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.' }}
                            </p>
                        </div>
                        <a href="{{ @$detail->getContentDetails[1]->content_button_link ?? 'javascript:;' }}" class="color_primary content_button_2 butoon" @if(@$detail->getContentDetails[1]->content_button == 'Y') @else style="display: none;" @endif>{{ @$detail->getContentDetails[1]->content_button_text ?? 'Read More' }}</a>
                    </div>
                    <div class="col-sm-5">
                        <div class="single_img">
                            <!-- <img src="{{URL::asset('public/frontend/landing_page1/images/single_img.png')}}" alt=""> -->
                            <div class="banner-rightimg img-fluid animation-jump image-move-right">
                                <div class="multi_content_file_div">
                                    <span class="content_file_2 @if(@$detail->getContentDetails[1]->content_file_type == 'Y') video_youtube @endif @if(@$detail->getContentDetails[1]->content_file_type != 'Y' && @$detail->getContentDetails[1]->image_style=='S') square_image @endif" id="content_file_2" >
                                        @if(@$detail->getContentDetails[1]->content_file)
                                            @if(@$detail->getContentDetails[1]->content_file_type == 'I')
                                                <img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->getContentDetails[1]->content_file}}" alt="">
                                                @else
                                                <iframe src="https://www.youtube.com/embed/{{@$detail->getContentDetails[1]->content_file}}" frameborder="0" allowfullscreen></iframe>
                                            @endif
                                        @else
                                            <img src="{{URL::asset('public/frontend/landing_page1/images/wef-min.jpg')}}" alt="">
                                        @endif
                                    </span>
                                    <em class="banner_right_after color_primary"></em>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="single_box" id="content_div_3" @if(@$detail->getContentDetails[2] == null) style="display: none;" @endif >
            <div class="container">
                <div class="row">
                    <div class="col-sm-5">
                        <div class="single_img">
                            <!-- <img src="{{URL::asset('public/frontend/landing_page1/images/single_img.png')}}" alt=""> -->
                            <div class="banner-rightimg img-fluid animation-jump">
                                <div class="multi_content_file_div">
                                    <span class="content_file_3 @if(@$detail->getContentDetails[2]->content_file_type == 'Y') video_youtube @endif @if(@$detail->getContentDetails[2]->content_file_type != 'Y' && @$detail->getContentDetails[2]->image_style=='S') square_image @endif" id="content_file_3">
                                        @if(@$detail->getContentDetails[2]->content_file)
                                            @if(@$detail->getContentDetails[2]->content_file_type == 'I')
                                                <img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->getContentDetails[2]->content_file}}" alt="">
                                                @else
                                                <iframe src="https://www.youtube.com/embed/{{@$detail->getContentDetails[2]->content_file}}" frameborder="0" allowfullscreen></iframe>
                                            @endif
                                        @else
                                            <img src="{{URL::asset('public/frontend/landing_page1/images/wef-min.jpg')}}" alt="">
                                        @endif
                                    </span>
                                    <em class="banner_right_after color_primary"></em>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <h2 class="color_secondary_font content_heading_3">
                            @if(@$detail->getContentDetails[2]->content_heading)
                                <span>{{strtok(@$detail->getContentDetails[2]->content_heading, " ")}}</span> {{strstr(@$detail->getContentDetails[2]->content_heading," ")}}
                            @else
                                <span>Online</span> Learning
                            @endif
                        </h6>
                        <div class="single_text">
                            <p class="color_secondary_font content_desc_3">
                                {{ @$detail->getContentDetails[2]->content_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.' }}
                            </p>
                        </div>
                        <a href="{{ @$detail->getContentDetails[2]->content_button_link ?? 'javascript:;' }}" class="color_primary content_button_3 butoon" @if(@$detail->getContentDetails[2]->content_button == 'Y') @else style="display: none;" @endif>{{ @$detail->getContentDetails[2]->content_button_text ?? 'Read More' }}</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="single_box cont_move_right" id="content_div_4" @if(@$detail->getContentDetails[3] == null) style="display: none;" @endif >
            <div class="container">
                <div class="row">
                    <div class="col-sm-7">
                        <h2 class="color_secondary_font content_heading_4">
                            @if(@$detail->getContentDetails[3]->content_heading)
                                <span>{{strtok(@$detail->getContentDetails[3]->content_heading, " ")}}</span> {{strstr(@$detail->getContentDetails[3]->content_heading," ")}}
                            @else
                                <span>Online</span> Learning
                            @endif
                        </h6>
                        <div class="single_text">
                            <p class="color_secondary_font content_desc_4">
                                {{ @$detail->getContentDetails[3]->content_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.' }}
                            </p>
                        </div>
                        <a href="{{ @$detail->getContentDetails[3]->content_button_link ?? 'javascript:;' }}" class="color_primary content_button_4 butoon" @if(@$detail->getContentDetails[3]->content_button == 'Y') @else style="display: none;" @endif>{{ @$detail->getContentDetails[3]->content_button_text ?? 'Read More' }}</a>
                    </div>
                    <div class="col-sm-5">
                        <div class="single_img">
                            <!-- <img src="{{URL::asset('public/frontend/landing_page1/images/single_img.png')}}" alt=""> -->
                            <div class="banner-rightimg img-fluid animation-jump image-move-right">
                                <div class="multi_content_file_div">
                                    <span class="content_file_4 @if(@$detail->getContentDetails[3]->content_file_type == 'Y') video_youtube @endif @if(@$detail->getContentDetails[3]->content_file_type != 'Y' && @$detail->getContentDetails[3]->image_style=='S') square_image @endif" id="content_file_4">
                                        @if(@$detail->getContentDetails[3]->content_file)
                                            @if(@$detail->getContentDetails[3]->content_file_type == 'I')
                                                <img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->getContentDetails[3]->content_file}}" alt="">
                                                @else
                                                <iframe src="https://www.youtube.com/embed/{{@$detail->getContentDetails[3]->content_file}}" frameborder="0" allowfullscreen></iframe>
                                            @endif
                                        @else
                                            <img src="{{URL::asset('public/frontend/landing_page1/images/wef-min.jpg')}}" alt="">
                                        @endif
                                    </span>
                                    <em class="banner_right_after color_primary"></em>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end about area-->


    <!--start video area-->
    <section id="video-area" class="bg-gray single_video color_primary_light" @if(@$detail->body_single_video == 'N') style="display: none;" @endif>
        <div class="container">
            <div class="row">
                <!--start heading-->
                <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1">
                    <div class="sec-heading text-center">
                        <!-- <h6 class="color_primary_font">Product Demo</h6> -->
                        <h2 class="body_single_video_heading color_secondary_font">{{ @$detail->body_single_video_heading ?? 'Get product more information from the video' }}</h2>
                        <p class="color_secondary_font body_single_video_desc">{{ @$detail->body_single_video_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.' }}</p>
                    </div>
                </div>
                <!--end heading-->
            </div>
            <div class="row">
                <!--start video player-->
                <div class="col-lg-10 offset-lg-1">
                    <div class="video-player-wrap ved_img">
                        <!-- <img src="{{URL::asset('public/frontend/landing_page2/images/watch-img-2.jpg')}}" alt)=""> -->
                        <div class="single_vdo">
                            @if(@$detail->body_single_video_type == "V")
                                <video src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->body_single_video_filename}}"></video>
                            @elseif(@$detail->body_single_video_type == "I")
                                <img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$detail->body_single_video_filename}}" alt="">
                            @else
                                <img src="{{URL::asset('public/frontend/landing_page2/images/watch-img-2.jpg')}}" alt="">
                            @endif
                        </div>
                        <div class="video-player paly_btn" @if(@$detail->body_single_video_type == "I") style="display: none;" @endif >
                            <span class="before_vedio color_primary_border"></span>
						    <a class="popup-video color_primary" href="javascript:;" onclick="play_vdo()"><i class="icofont-play color_primary"></i></a>
                            <!-- <a class="popup-video color_primary" href="https://youtu.be/2HgrxH7N7tE"><i class="icofont-play color_primary"></i></a> -->
                            <span class="after_vedio color_primary_border"></span>
                        </div>
                    </div>
                </div>
                <!--end video player-->
            </div>
        </div>
    </section>
    <!--end video area-->

    <!--start product area-->
        <section id="products-initial" class="product-area" data-scroll-index="3"  @if(@$detail->getProductDetails && count(@$detail->getProductDetails) > 0) style="display: none;" @endif>
            <div class="container">
                <div class="row">
                    <!--start heading-->
                    <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1">
                        <div class="sec-heading text-center">
                            <h2 class="product_section_heading color_secondary_font">{{ @$detail->product_section_heading ?? 'Our Products' }}</h2>
                            <p class="product_section_desc color_secondary_font">{{ @$detail->product_section_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.' }}</p>
                        </div>
                    </div>
                    <!--end heading-->
                </div>
            </div>
            <!--end product area top-->
            <!--start product area bottom-->
            <div class="product-area-btm">
                <div class="container">
                    <!-- <div class="row" @if(@$detail->product_slider == 'Y') style="display: none;" @endif >
                        <div class="col-md-4 mb-3">
                            <div class="prod-single text-center three">
                                <div class="prod-img file_div">
                                    <img src="{{URL::asset('public/frontend/landing_page2/images/pro1.jpg')}}" class="img-fluid animation-jump" alt="">
                                </div>
                                <div class="prod-info three">
                                    <h4 class="pro_ac"><a href="#" class="color_secondary_font">Products 1</a></h4>
                                    <p class="color_secondary_font">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo</p>
                                    <h5 class="color_primary_font">$169</h5>
                                    <a href="javascript:;" class="btn_pro color_primary">learn more</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 mb-3">
                            <div class="prod-single text-center three">
                                <div class="prod-img file_div">
                                    <img src="{{URL::asset('public/frontend/landing_page2/images/pro2.jpg')}}" class="img-fluid animation-jump" alt="">
                                </div>
                                <div class="prod-info three">
                                    <h4 class="pro_ac"><a href="#" class="color_secondary_font">Products 2</a></h4>
                                    <p class="color_secondary_font">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo</p>
                                    <h5 class="color_primary_font">$189</h5>
                                    <a class="btn_pro color_primary" href="javascript:;">learn more</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 mb-3">
                            <div class="prod-single text-center three">
                                <div class="prod-img file_div">
                                    <img src="{{URL::asset('public/frontend/landing_page2/images/pro3.jpg')}}" class="img-fluid animation-jump" alt="">
                                </div>
                                <div class="prod-info three">
                                    <h4 class="pro_ac"><a href="#" class="color_secondary_font">Products 3</a></h4>
                                    <p class="color_secondary_font">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo</p>
                                    <h5 class="color_primary_font">$169</h5>
                                    <a class="btn_pro color_primary" href="javascript:;">learn more</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 mb-3">
                            <div class="prod-single text-center three">
                                <div class="prod-img file_div">
                                    <img src="{{URL::asset('public/frontend/landing_page2/images/pro3.jpg')}}" class="img-fluid animation-jump" alt="">
                                </div>
                                <div class="prod-info three">
                                    <h4 class="pro_ac"><a href="#" class="color_secondary_font">Products 4</a></h4>
                                    <p class="color_secondary_font">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo</p>
                                    <h5 class="color_primary_font">$345</h5>
                                    <a class="btn_pro color_primary" href="javascript:;">learn more</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 mb-3">
                            <div class="prod-single text-center three">
                                <div class="prod-img file_div">
                                    <img src="{{URL::asset('public/frontend/landing_page2/images/pro2.jpg')}}" class="img-fluid animation-jump" alt="">
                                </div>
                                <div class="prod-info three">
                                <h4 class="pro_ac"><a href="#" class="color_secondary_font">Products 5</a></h4>
                                    <p class="color_secondary_font">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo</p>
                                    <h5 class="color_primary_font">$169</h5>
                                    <a class="btn_pro color_primary" href="javascript:;">learn more</a>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="row" @if(@$detail->product_slider == 'N') style="display: none;" @endif >
                        <div class="col-lg-12">
                            <div class="product-carousel owl-carousel color_primary_owl">
                                <div class="prod-single text-center three">
                                    <div class="prod-img file_div">
                                        <img src="{{URL::asset('public/frontend/landing_page2/images/pro1.jpg')}}" class="img-fluid animation-jump" alt="">
                                    </div>
                                    <div class="prod-info three">
                                        <h4 class="pro_ac"><a href="#" class="color_secondary_font">Products 1</a></h4>
                                        <p class="color_secondary_font">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo</p>
                                        <h5 class="color_primary_font">$169</h5>
                                        <a href="javascript:;" class="btn_pro color_primary">learn more</a>
                                    </div>
                                </div>
                                <div class="prod-single text-center three">
                                    <div class="prod-img file_div">
                                        <img src="{{URL::asset('public/frontend/landing_page2/images/pro2.jpg')}}" class="img-fluid animation-jump" alt="">
                                    </div>
                                    <div class="prod-info three">
                                        <h4 class="pro_ac"><a href="#" class="color_secondary_font">Products 2</a></h4>
                                        <p class="color_secondary_font">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo</p>
                                        <h5 class="color_primary_font">$189</h5>
                                        <a class="btn_pro color_primary" href="javascript:;">learn more</a>
                                    </div>
                                </div>
                                <div class="prod-single text-center three">
                                    <div class="prod-img file_div">
                                        <img src="{{URL::asset('public/frontend/landing_page2/images/pro3.jpg')}}" class="img-fluid animation-jump" alt="">
                                    </div>
                                    <div class="prod-info three">
                                        <h4 class="pro_ac"><a href="#" class="color_secondary_font">Products 3</a></h4>
                                        <p class="color_secondary_font">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo</p>
                                        <h5 class="color_primary_font">$169</h5>
                                        <a class="btn_pro color_primary" href="javascript:;">learn more</a>
                                    </div>
                                </div>
                                <div class="prod-single text-center three">
                                    <div class="prod-img file_div">
                                        <img src="{{URL::asset('public/frontend/landing_page2/images/pro3.jpg')}}" class="img-fluid animation-jump" alt="">
                                    </div>
                                    <div class="prod-info three">
                                        <h4 class="pro_ac"><a href="#" class="color_secondary_font">Products 4</a></h4>
                                        <p class="color_secondary_font">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo</p>
                                        <h5 class="color_primary_font">$345</h5>
                                        <a class="btn_pro color_primary" href="javascript:;">learn more</a>
                                    </div>
                                </div>
                                <div class="prod-single text-center three">
                                    <div class="prod-img file_div">
                                        <img src="{{URL::asset('public/frontend/landing_page2/images/pro2.jpg')}}" class="img-fluid animation-jump" alt="">
                                    </div>
                                    <div class="prod-info three">
                                    <h4 class="pro_ac"><a href="#" class="color_secondary_font">Products 5</a></h4>
                                        <p class="color_secondary_font">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo</p>
                                        <h5 class="color_primary_font">$169</h5>
                                        <a class="btn_pro color_primary" href="javascript:;">learn more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="products-actual" class="product-area" data-scroll-index="3" @if(!@$detail->getProductDetails || count(@$detail->getProductDetails) == 0) style="display: none;" @endif>
            <div class="container">
                <div class="row">
                    <!--start heading-->
                    <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1">
                        <div class="sec-heading text-center">
                            <h2 class="product_section_heading color_secondary_font">{{ @$detail->product_section_heading ?? 'Our Products' }}</h2>
                            <p class="product_section_desc color_secondary_font">{{ @$detail->product_section_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.' }}</p>
                        </div>
                    </div>
                    <!--end heading-->
                </div>
            </div>
            <!--end product area top-->
            <!--start product area bottom-->
            <div class="product-area-btm with-slider" @if(@$detail->product_slider == 'N') style="display: none;" @endif >
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="product-actual-carousel owl-carousel color_primary_owl">
                                @if(@$detail->getProductDetails && count(@$detail->getProductDetails)>0)
                                    @foreach(@$detail->getProductDetails as $k=>$product)
                                    <div class="item product_div_{{ @$k + 1 }} proddiv">
                                        <div class="product_detail prod-single text-center three">
                                            <div class="prod-img file_div">
                                                @if(@$product->product_file)
                                                    @if(@$product->product_file_type == 'I')
                                                        <img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$product->product_file}}" alt="" class="animation-jump product_file_{{ @$k + 1 }}">
                                                    @elseif(@$product->product_file_type == 'Y')
                                                        <img src="http://img.youtube.com/vi/{{@$product->product_file}}/0.jpg" alt="" class="animation-jump product_file_{{ @$k + 1 }}"/>
                                                        <!-- <iframe width="100%" height="100%" src="https://www.youtube.com/embed/{{@$product->product_file}}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
                                                        {{-- <div class="playIcon animation-jump">
                                                            <a href="http://www.youtube.com/watch?v={{@$product->product_file}}" class="fancybox-media">
                                                                <img src="{{URL::asset('public/frontend/landing_page2/images/playIcon.png')}}" alt="" rel="media-gallery"></a>
                                                        </div> --}}
                                                        <div class="playIcon animation-jump">
                                                            <div class="video-player paly_btn vid-icon-you">
                                                                <a href="http://www.youtube.com/watch?v={{@$product->product_file}}" class="fancybox-media color_primary">
                                                                    {{-- <img src="{{URL::asset('public/frontend/landing_page1/images/playIcon.png')}}" alt="" rel="media-gallery"> --}}
                                                                    <i class="fa fa-play" aria-hidden="true" rel="media-gallery"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    @else
                                                    <img src="{{ URL::to('public/frontend/images/NoImage.jpg') }}" alt="" class="product_file_{{ @$k + 1 }}">
                                                @endif
                                            </div>
                                            <div class="prod-info three">
                                                <h4 class="pro_ac"><a href="#" class="product_heading_{{ $k + 1 }} color_secondary_font color_primary_font_hover">{{ @$product->product_heading ?? 'Product'. @$k + 1 }}</a></h4>
                                                <p class="color_secondary_font product_desc_{{ $k + 1 }}">{{ @$product->product_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo' }}</p>
                                                <h5 class="product_price_{{ $k + 1 }} color_primary_font" @if(@$product->product_price_show == 'N') style="display: none;" @endif >R${{ @$product->product_price ?? '800' }} </h5>
                                                <p class="product_expiry_{{ $k + 1 }} product_expiry_div_{{ $k + 1 }} timer-pro color_secondary_font color_secondary_border" @if(@$product->product_reverse_counter == 'N') style="display:none;" @endif ></p>
                                                <a href="{{ @$product->product_button_url ?? 'javascript:;' }}" class="btn_pro product_link_{{ $k + 1 }} color_primary" @if(@$product->product_button == 'N') style="display:none;" @endif >{{ @$product->product_button_caption ?? 'learn more' }}</a>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                @else
                                    <div class="item product_div_1 proddiv">
                                        <div class="product_detail prod-single text-center three">
                                            <div class="prod-img file_div">
                                            </div>
                                            <div class="prod-info three">
                                                <h4 class="pro_ac"><a href="#" class="product_heading_1 color_secondary_font color_primary_font_hover">New Product</a></h4>
                                                <p class="product_desc_1 color_secondary_font">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo</p>
                                                <h5 class="product_price_1 color_primary_font">R$800</h5>
                                                <p class="product_expiry_1 product_expiry_div_1 timer-pro color_secondary_font color_secondary_border"></p>
                                                <a href="#" class="btn_pro product_link_1 color_primary">learn more</a>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="product-area-btm without-slider" @if(@$detail->product_slider == 'Y') style="display: none;" @endif >
                <div class="container">
                    <div class="row row_main">
                        @if(@$detail->getProductDetails && count(@$detail->getProductDetails)>0)
                            @foreach(@$detail->getProductDetails as $k=>$product)
                            <div class="item col-md-4 mb-3 product_div_{{ @$k + 1 }} proddiv">
                                <div class="product_detail prod-single text-center three">
                                    <div class="prod-img file_div">
                                        @if(@$product->product_file)
                                            @if(@$product->product_file_type == 'I')
                                                <img src="{{URL::to('storage/app/public/uploads/landing_page').'/'.@$product->product_file}}" alt="" class="animation-jump product_file_{{ @$k + 1 }}">
                                            @elseif(@$product->product_file_type == 'Y')
                                                <img src="http://img.youtube.com/vi/{{@$product->product_file}}/0.jpg" alt="" class="animation-jump product_file_{{ @$k + 1 }}"/>
												{{-- <div class="playIcon animation-jump">
													<a href="http://www.youtube.com/watch?v={{@$product->product_file}}" class="fancybox-media">
														<img src="{{URL::asset('public/frontend/landing_page2/images/playIcon.png')}}" alt="" rel="media-gallery"></a>
												</div> --}}
                                                <div class="playIcon animation-jump">
                                                    <div class="video-player paly_btn vid-icon-you">
                                                        <a href="http://www.youtube.com/watch?v={{@$product->product_file}}" class="fancybox-media color_primary">
                                                            {{-- <img src="{{URL::asset('public/frontend/landing_page1/images/playIcon.png')}}" alt="" rel="media-gallery"> --}}
                                                            <i class="fa fa-play" aria-hidden="true" rel="media-gallery"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            @endif
                                        @else
                                        <img src="{{ URL::to('public/frontend/images/NoImage.jpg') }}" alt="" class="product_file_{{ @$k + 1 }}">
                                        @endif
                                    </div>
                                    <div class="prod-info three">
                                        <h4 class="pro_ac"><a href="#" class="product_heading_{{ $k + 1 }} color_primary_font_hover">{{ @$product->product_heading ?? 'Product'. @$k + 1 }}</a></h4>
                                        <p class="product_desc_{{ $k + 1 }}">{{ @$product->product_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo' }}</p>
                                        <h5 class="product_price_{{ $k + 1 }} color_primary_font" @if(@$product->product_price_show == 'N') style="display: none;" @endif >R${{ @$product->product_price ?? '800' }}</h5>
                                        <p class="product_expiry_{{ $k + 1 }} product_expiry_div_{{ $k + 1 }} timer-pro color_secondary_font color_secondary_border" @if(@$product->product_reverse_counter == 'N') style="display:none;" @endif ></p>
                                        <a href="{{ @$product->product_button_url ?? 'javascript:;' }}" class="btn_pro product_link_{{ $k + 1 }} color_primary" @if(@$product->product_button == 'N') style="display:none;" @endif >{{ @$product->product_button_caption ?? 'learn more' }}</a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        @else
                            <div class="item col-md-4 mb-3 product_div_1 proddiv">
                                <div class="product_detail prod-single text-center three">
                                    <div class="prod-img file_div">
                                    </div>
                                    <div class="prod-info three">
                                        <h4 class="pro_ac"><a href="#" class="product_heading_1 color_primary_font_hover">New Product</a></h4>
                                        <p class="product_desc_1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo</p>
                                        <h5 class="product_price_1 color_primary_font">R$800</h5>
                                        <p class="product_expiry_1 product_expiry_div_1 timer-pro color_secondary_font color_secondary_border"></p>
                                        <a href="#" class="btn_pro product_link_1 color_primary">learn more</a>
                                    </div>
                                </div>
                            </div>
                        @endif
                </div>
            </div>
        </section>
    <!--end product area-->

    <!--start FAQ area-->
    <div class="faq_sec" id="faq_sec" @if(@$detail->show_faq == 'N') style="display: none;" @endif >
        <div class="container">
            <div class="page_hed">
                <h3 class="color_secondary_font faq_heading">{{@$detail->faq_heading ?? 'Faq'}}</h3>
                <p class="color_secondary_font faq_description">{{@$detail->faq_description ?? 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic explicabo voluptas nisi iste reiciendis, tempore commodi necessitatibus, laborum laudantium.'}}</p>
            </div>
            <div class="accordion_div">
                <div id="accordion" class="accordionlanding1">
					<div class="card mb-0">
						@if(@$detail->getFaq && count(@$detail->getFaq) > 0)
							@foreach(@$detail->getFaq as $k=>$faq)
								<div class="faq_{{@$k + 1}} card-header collapsed color_tertiary color_primary_border" data-toggle="collapse" href="#collapse{{@$k + 1}}">
									<a class="card-title faq_title_{{@$k + 1}}">
										{{@$faq->title ?? 'This is a simply dummy question text show here?'}}
									</a>
									<i class="fa fa-plus" aria-hidden="true"></i>
								</div>
								<div id="collapse{{@$k + 1}}" class="faq_{{@$k + 1}} card-body collapse" data-parent="#accordion" >
									<p class="faq_desc_{{@$k + 1 }} color_primary_border">
										{{@$faq->description ?? 'Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur mi gravida ac. Nunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet ante vitae ante facilisis lobortis temporamet ante vitae ante facilisis lobortis sit amet consectetur adipiscfiing. Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur gravidaNunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet ante vitae ante.'}}
									</p>
								</div>
							@endforeach
						@else
							<div class="faq_1 card-header collapsed color_tertiary color_primary_border" data-toggle="collapse" href="#collapse1">
								<a class="card-title faq_title_1">
									This is a simply dummy question text show here?
								</a>
                                <i class="fa fa-plus" aria-hidden="true"></i>
							</div>
							<div id="collapse1" class="faq_1 card-body collapse" data-parent="#accordion">
								<p class="faq_desc_1 color_primary_border">
									Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur mi gravida ac. Nunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet ante vitae ante facilisis lobortis temporamet ante vitae ante facilisis lobortis sit amet consectetur adipiscfiing. Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur gravidaNunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet ante vitae ante.
								</p>
							</div>
						@endif
					</div>
				</div>
                <!-- <ul id="accordion" class="accordion">
                    <li>
                    <div class="link">This is a simply dummy question text show here?<i class="fa fa-plus"></i></div>
                    <ul class="submenu">
                        <p class="pm"> Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur mi gravida ac. Nunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet ante vitae ante facilisis lobortis temporamet ante vitae ante facilisis lobortis sit amet consectetur adipiscfiing.
                        Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur gravidaNunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet ante vitae ante. </p>
                    </ul>
                    </li>
                    <li>
                    <div class="link">This is simply dummy question here?<i class="fa fa-plus"></i></div>
                    <ul class="submenu" style="">
                        <p>Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur gravidaNunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet</p>
                    </ul>
                    </li>
                    <li>
                    <div class="link">This is simply dummy name question here?<i class="fa fa-plus"></i></div>
                    <ul class="submenu" style="">
                        <p>Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur gravidaNunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet</p>
                    </ul>
                    </li>

                    <li>
                    <div class="link">This is a simply dummy question text show here?<i class="fa fa-plus"></i></div>
                    <ul class="submenu">
                        <p class="pm"> Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur mi gravida ac. Nunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet ante vitae ante facilisis lobortis temporamet ante vitae ante facilisis lobortis sit amet consectetur adipiscfiing.
                        Lorem ipsum dolor sit amet consectetur adipiscfiing elit amet ante vitae ante facilisis lobortis mauris egestas sem tellus, consectetur gravidaNunc sit amet ante vitae ante facilisis lobortis lorem ipsum dolor sit amet consectetur adipiscfiing elit mauris egestas sit amet ante vitae ante. </p>

                    </ul>

                    </li>

                </ul> -->
            </div>
        </div>
    </div>
    <!--end FAQ area-->

    <!--start contact area-->
    <section id="contact-area" class="lead" data-scroll-index="6" @if(@$detail->header_button_type == 'URL' || @$detail->lead_position == 'P') style="display: none;" @endif>
        <div class="container">
            <div class="row">
                <!--start heading-->
                <div class="col-md-8">
                    <div class="sec-heading">
                        {{-- <h6 class="color_primary_font">Need Help</h6> --}}
                        <h2 class="color_secondary_font lead_section_heading">{{@$detail->lead_section_heading ?? 'Contact With Us'}}</h2>
                        <p class="color_secondary_font lead_section_desc">{{@$detail->lead_section_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.'}}</p>
                    </div>
                </div>
                <!--end heading-->
            </div>
            <form action="{{route('landing.page.save.lead')}}" id="lead_form_footer" method="post">
                @csrf
				<input type="text" name="landing_page_master_id" value="{{@$detail->id}}" hidden >
                <div class="contact-form row">
                    <div class="col-md-6 col-sm-12 form-group lead_fname" @if(@$detail->lead_fname == 'N') style="display: none;" @endif>
                        <label for="fname" class="color_secondary_font">@lang('client_site.first_name')*</label>
                        <input type="text" class="form-control required" id="fname" name="fname" placeholder="@lang('client_site.first_name')*" >
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="col-md-6 col-sm-12 form-group lead_lname" @if(@$detail->lead_lname == 'N') style="display: none;" @endif>
                        <label for="lname" class="color_secondary_font">@lang('client_site.last_name')*</label>
                        <input type="text" class="form-control required" id="lname" name="lname" placeholder="@lang('client_site.last_name')*" >
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="col-md-6 col-sm-12 form-group lead_country_code" @if(@$detail->lead_country_code == 'N') style="display: none;" @endif>
                        <label for="mobile" class="color_secondary_font">@lang('site.country_code')*</label>
                        <input type="number" class="form-control required number" id="country_code" name="country_code" placeholder="@lang('site.country_code')*" >
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="col-md-6 col-sm-12 form-group lead_mobile" @if(@$detail->lead_mobile == 'N') style="display: none;" @endif>
                        <label for="mobile" class="color_secondary_font">@lang('site.contact_form_mobile')*</label>
                        <input type="tel" class="form-control required number" id="mobile" name="mobile" placeholder="@lang('site.contact_form_mobile')*" >
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="col-md-6 col-sm-12 form-group lead_email" @if(@$detail->lead_email == 'N') style="display: none;" @endif>
                        <label for="email" class="color_secondary_font">@lang('site.contact_form_email')*</label>
                        <input type="email" class="form-control required" id="email" name="email" placeholder="@lang('site.contact_form_email')*" >
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="col-md-6 col-sm-12 form-group lead_facebook" @if(@$detail->lead_facebook == 'N') style="display: none;" @endif>
                        <label for="facebook" class="color_secondary_font">@lang('site.contact_form_facebook_link')</label>
                        <input type="url" class="form-control" id="facebook_link" name="facebook_link" placeholder=@lang('site.contact_form_facebook_link')" >
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="col-md-6 col-sm-12 form-group lead_instagram" @if(@$detail->lead_instagram == 'N') style="display: none;" @endif>
                        <label for="instagram" class="color_secondary_font">@lang('site.contact_form_instagram_link')</label>
                        <input type="url" class="form-control" id="instagram_link" name="instagram_link" placeholder="@lang('site.contact_form_instagram_link')" >
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="col-md-6 col-sm-12 form-group lead_linkedin" @if(@$detail->lead_linkedin == 'N') style="display: none;" @endif>
                        <label for="linkedin" class="color_secondary_font">@lang('site.contact_form_linkedink_link')</label>
                        <input type="url" class="form-control" id="linkedin_link" name="linkedin_link" placeholder="@lang('site.contact_form_linkedink_link')" >
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="col-md-6 col-sm-12 form-group lead_website" @if(@$detail->lead_website == 'N') style="display: none;" @endif>
                        <label for="website" class="color_secondary_font">@lang('site.contact_form_website')</label>
                        <input type="url" class="form-control required" id="website" name="website" placeholder="@lang('site.contact_form_website')*" >
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="col-md-6 col-sm-12 form-group lead_address" @if(@$detail->lead_address == 'N') style="display: none;" @endif>
                        <label for="address" class="color_secondary_font">@lang('site.street_address')</label>
                        <input type="text" class="form-control required" id="address" name="address" placeholder="@lang('site.street_address')*" >
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="col-md-6 col-sm-12 form-group lead_city" @if(@$detail->lead_city == 'N') style="display: none;" @endif>
                        <label for="city" class="color_secondary_font">@lang('site.city')</label>
                        <input type="text" class="form-control required" id="city" name="city" placeholder="@lang('site.city')*" >
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="col-md-6 col-sm-12 form-group lead_state" @if(@$detail->lead_state == 'N') style="display: none;" @endif>
                        <label for="state" class="color_secondary_font">@lang('site.state')</label>
                        <input type="text" class="form-control required" id="state" name="state" placeholder="@lang('site.state')*" >
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="col-md-6 col-sm-12 form-group lead_postal_code" @if(@$detail->lead_postal_code == 'N') style="display: none;" @endif>
                        <label for="postal_code" class="color_secondary_font">@lang('site.contact_form_postal_code')</label>
                        <input type="text" class="form-control required" id="postal_code" name="postal_code" placeholder="@lang('site.contact_form_postal_code')*" >
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="col-md-12 form-group lead_message" @if(@$detail->lead_message == 'N') style="display: none;" @endif>
                        <label for="message" class="color_secondary_font">@lang('site.message')*</label>
                        <textarea class="form-control required" id="message" name="message" rows="8" placeholder="@lang('site.message')*" required ></textarea>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="col-md-12 form-group">
                        <label for="lead_footer_privacy_policy_accepted color_tertiary_font">
                            <input type="checkbox" class="required" id="lead_footer_privacy_policy_accepted" name="lead_privacy_policy_accepted">
                            @lang('client_site.i_agree_with_the') <span id="show_lead_footer_privacy_policy" class="lead_privacy_policy_link color_primary_font" data-toggle="modal" data-target="#myModal3">@lang('client_site.privacy_policy')</span>
                        </label>
                        <div id="lead_privacy_err_footer"></div>
                    </div>
                    <div class="col-md-12 form-group">
                        <button type="button" class="color_primary" id="lead_form_footer_btn">{{@$detail->lead_section_footer_submit_btn ?? 'Submit Now'}}</button>
                        <div class="messages"></div>
                    </div>
                </div>
            </form>
        </div>
    </section>
    <!--end contact area-->
    <!--start footer-->
    <footer id="footer" class="bg-gray">
        <!--start footer bottom-->
        <div class="footer-btm text-center color_primary_light">
            <div class="container">
                <div class="foot_inr">
                    <p>© {{date('Y')}} <a href="{{route('home')}}" class="color_primary_font" target="_blank">netbhe.com.br</a> Todos os Direitos Reservados</p>
                <div class="header-social text-center foo_sos">
                    <ul>
                        <li class="link_icons link_icons_linkedin" @if(!@$detail->link_linkedin) style="display:none;" @endif ><a href="{{ @$detail->link_linkedin ?? 'javascript:;' }}" class="color_primary_font color_primary_border color_primary_hover" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                        <li class="link_icons link_icons_facebook" @if(!@$detail->link_facebook) style="display:none;" @endif ><a href="{{ @$detail->link_facebook ?? 'javascript:;' }}" class="color_primary_font color_primary_border color_primary_hover" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li class="link_icons link_icons_twitter" @if(!@$detail->link_twitter) style="display:none;" @endif ><a href="{{ @$detail->link_twitter ?? 'javascript:;' }}" class="color_primary_font color_primary_border color_primary_hover" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li class="link_icons link_icons_instagram" @if(!@$detail->link_instagram) style="display:none;" @endif ><a href="{{ @$detail->link_instagram ?? 'javascript:;' }}" class="color_primary_font color_primary_border color_primary_hover" target="_blank"><i class="fab fa-instagram"></i></a></li>
                        <li class="link_icons link_icons_pinterest" @if(!@$detail->link_pinterest) style="display:none;" @endif ><a href="{{ @$detail->link_pinterest ?? 'javascript:;' }}" class="color_primary_font color_primary_border color_primary_hover" target="_blank"><i class="fa fa-pinterest"></i></a></li>
                        <li class="link_icons link_icons_youtube" @if(!@$detail->link_youtube) style="display:none;" @endif ><a href="{{ @$detail->link_youtube ?? 'javascript:;' }}" class="color_primary_font color_primary_border color_primary_hover" target="_blank"><i class="fa fa-youtube"></i></a></li>
                        <li class="link_icons link_icons_tiktok" @if(!@$detail->link_tiktok) style="display:none;" @endif ><a href="{{ @$detail->link_tiktok ?? 'javascript:;' }}" target="_blank" class="color_primary_font color_primary_border color_primary_hover"><i class="fa-brands fa-tiktok"></i></a></li>
                    </ul>
                </div>
                <div class="foot-inr-sb">
                    <img src="{{ URL::asset('public/frontend/landing_page4/images/logo.png') }}" alt="" >
                </div>
                </div>
            </div>
        </div>
    </footer>


    <div class="modal fade con_frm modal_sus" id="myModal">
        <div class="modal-dialog" id="form-modal">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title color_secondary_font">@lang('client_site.contact_form')</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form action="{{route('landing.page.save.lead')}}" id="lead_form_popup" method="post">
                        @csrf
                        <input type="text" name="landing_page_master_id" value="{{@$detail->id}}" hidden >
                        <div class="contact-form row">
                            <div class="col-sm-6 form-group lead_fname" @if(@$detail->lead_fname == 'N') style="display: none;" @endif>
                                <input type="text" class="color_primary_border form-control required" id="fname" name="fname" placeholder="@lang('client_site.first_name')*" >
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="col-sm-6 form-group lead_lname" @if(@$detail->lead_lname == 'N') style="display: none;" @endif>
                                <input type="text" class="color_primary_border form-control required" id="lname" name="lname" placeholder="@lang('client_site.last_name')*" >
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="col-sm-6 form-group lead_country_code" @if(@$detail->lead_country_code == 'N') style="display: none;" @endif>
                                <input type="tel" class="color_primary_border form-control required number" id="country_code" name="country_code" placeholder="@lang('site.country_code')*" >
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="col-sm-6 form-group lead_mobile" @if(@$detail->lead_mobile == 'N') style="display: none;" @endif>
                                <input type="tel" class="color_primary_border form-control required number" id="mobile" name="mobile" placeholder="@lang('site.contact_form_mobile')*" >
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="col-sm-6 form-group lead_email" @if(@$detail->lead_email == 'N') style="display: none;" @endif>
                                <input type="email" class="color_primary_border form-control required" id="email" name="email" placeholder="@lang('site.contact_form_email')*" >
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="col-sm-6 form-group lead_facebook" @if(@$detail->lead_facebook == 'N') style="display: none;" @endif>
                                <input type="url" class="color_primary_border form-control" id="facebook_link" name="facebook_link" placeholder="@lang('site.contact_form_facebook_link')" >
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="col-sm-6 form-group lead_instagram" @if(@$detail->lead_instagram == 'N') style="display: none;" @endif>
                                <input type="url" class="color_primary_border form-control" id="instagram_link" name="instagram_link" placeholder="@lang('site.contact_form_instagram_link')" >
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="col-sm-6 form-group lead_linkedin" @if(@$detail->lead_linkedin == 'N') style="display: none;" @endif>
                                <input type="url" class="color_primary_border form-control" id="linkedin_link" name="linkedin_link" placeholder="@lang('site.contact_form_linkedink_link')" >
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="col-sm-6 form-group lead_website" @if(@$detail->lead_website == 'N') style="display: none;" @endif>
                                <input type="url" class="color_primary_border form-control required" id="website" name="website" placeholder="@lang('site.contact_form_website')*" >
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="col-sm-6 form-group lead_address" @if(@$detail->lead_address == 'N') style="display: none;" @endif>
                                <input type="text" class="color_primary_border form-control required" id="address" name="address" placeholder="@lang('site.street_address')*" >
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="col-sm-6 form-group lead_city" @if(@$detail->lead_city == 'N') style="display: none;" @endif>
                                <input type="text" class="color_primary_border form-control required" id="city" name="city" placeholder="@lang('site.city')*" >
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="col-sm-6 form-group lead_state" @if(@$detail->lead_state == 'N') style="display: none;" @endif>
                                <input type="text" class="color_primary_border form-control required" id="state" name="state" placeholder="@lang('site.state')*" >
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="col-sm-6 form-group lead_postal_code" @if(@$detail->lead_postal_code == 'N') style="display: none;" @endif>
                                <input type="text" class="color_primary_border form-control required" id="postal_code" name="postal_code" placeholder="@lang('site.contact_form_postal_code')*" >
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="col-sm-12 form-group lead_message" @if(@$detail->lead_message == 'N') style="display: none;" @endif>
                                <textarea class="color_primary_border form-control required" id="message" name="message" rows="8" placeholder="@lang('site.message')*" required ></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label for="lead_popup_privacy_policy_accepted" class="color_secondary_font">
                                    <input type="checkbox" class="required" id="lead_popup_privacy_policy_accepted" name="lead_privacy_policy_accepted">
                                    @lang('client_site.i_agree_with_the') <span id="show_lead_popup_privacy_policy" class="lead_privacy_policy_link color_primary_font">@lang('client_site.privacy_policy')</span>
                                </label>
                                <div id="lead_privacy_err_popup"></div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <button type="button" class="color_primary" id="lead_form_popup_btn">{{@$detail->lead_section_footer_submit_btn ?? 'Submit Now'}}</button>
                                <div class="messages"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal-dialog" id="success-modal" style="display:none;">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="modal-body color_tertiary">
                    <div class="modal_sus_bdy">
                        <h2 class="color_secondary_font" id="lead_success_heading_popup">{{@$detail->lead_success_heading ?? 'Heading'}}</h2>
                        <p class="color_secondary_font" id="lead_success_desc_popup">{{@$detail->lead_success_desc ?? 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem dolor vero mollitia nam molestiae neque, maiores facilis voluptate eligendi ducimus id fugiat assumenda perspiciatis optio dignissimos eos nemo ipsum architecto unde ea tempore. Ratione eaque exercitationem magnam blanditiis, tenetur minima incidunt maxime sint earum est ut necessitatibus odit tempore vero. Recusandae dolores voluptatem officiis dolorum? Eum veniam minima ducimus odio non, dolore, nulla ullam a nemo cupiditate quae praesentium? Dolorem voluptatem consequuntur provident sapiente magnam consectetur ut omnis saepe libero tenetur id, excepturi nobis odit. Voluptate assumenda tenetur odit neque culpa voluptatibus consectetur, quo sed quis debitis numquam, inventore similique. Et reiciendis distinctio libero itaque ab, culpa voluptatem similique facere aliquid dolorum sint soluta minus deleniti? Commodi, eaque itaque. Porro?'}}</p>
                        <div class="net_sus_btn">
                            <button type="button" class="btn_pro color_secondary" id="lead_success_ok_popup" @if(@$detail->lead_success_button == 'N') style="display:none;" @endif >{{@$detail->lead_success_button_caption ?? 'OK'}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-dialog" id="privacy-modal" style="display: none;">
            <div class="modal-content">
                <button type="button" class="close privacy_back color_primary color_tertiary_hover">Back</button>
                <div class="modal-body">
                    <h2>@lang('client_site.privacy_policy')</h2>
                    <p id="lead_privacy_desc" class="mt-3">{{@$detail->lead_privacy_policy ?? \Lang::get('client_site.privacy_policy')}}</p>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal_sus" id="myModal2">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="modal-body color_tertiary">
                    <div class="modal_sus_bdy">
                        <h2 class="color_secondary_font" id="lead_success_heading_footer">{{@$detail->lead_success_heading ?? 'Heading'}}</h2>
                        <p class="color_secondary_font" id="lead_success_desc_footer">{{@$detail->lead_success_desc ?? 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem dolor vero mollitia nam molestiae neque, maiores facilis voluptate eligendi ducimus id fugiat assumenda perspiciatis optio dignissimos eos nemo ipsum architecto unde ea tempore. Ratione eaque exercitationem magnam blanditiis, tenetur minima incidunt maxime sint earum est ut necessitatibus odit tempore vero. Recusandae dolores voluptatem officiis dolorum? Eum veniam minima ducimus odio non, dolore, nulla ullam a nemo cupiditate quae praesentium? Dolorem voluptatem consequuntur provident sapiente magnam consectetur ut omnis saepe libero tenetur id, excepturi nobis odit. Voluptate assumenda tenetur odit neque culpa voluptatibus consectetur, quo sed quis debitis numquam, inventore similique. Et reiciendis distinctio libero itaque ab, culpa voluptatem similique facere aliquid dolorum sint soluta minus deleniti? Commodi, eaque itaque. Porro?'}}</p>
                        <div class="net_sus_btn">
                            <button type="button" class="btn_pro color_secondary" id="lead_success_ok_footer" @if(@$detail->lead_success_button == 'N') style="display:none;" @endif >{{@$detail->lead_success_button_caption ?? 'OK'}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal_sus modal_privacy" id="myModal3">
        <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="modal-body">
                <h2>@lang('client_site.privacy_policy')</h2>
                <p id="lead_privacy_desc">{{@$detail->lead_privacy_policy ?? \Lang::get('client_site.privacy_policy')}}</p>
            </div>
        </div>
        </div>
    </div>


    <script src="{{URL::asset('public/frontend/landing_page2/js/jquery-3.3.1.min.js')}}"></script>
    <!--proper js-->
    <script src="{{URL::asset('public/frontend/landing_page2/js/popper.min.js')}}"></script>
    <!--bootstrap js-->
    <script src="{{URL::asset('public/frontend/landing_page2/js/bootstrap.min.js')}}"></script>


    <script src="{{URL::asset('public/frontend/landing_page1/js/jquerymin.js')}}"></script>
    <!--owl carousel js-->
    <script src="{{URL::asset('public/frontend/landing_page2/js/owl.carousel.min.js')}}"></script>
    <!--magnic popup js-->
    <script src="{{URL::asset('public/frontend/landing_page2/js/magnific-popup.min.js')}}"></script>

    <!--scrollIt js-->
    <script src="{{URL::asset('public/frontend/landing_page2/js/scrollIt.min.js')}}"></script>

    <!--main js-->
    <script src="{{URL::asset('public/frontend/landing_page2/js/custom.js')}}"></script>
    <!--contact js-->
    <script src="{{URL::asset('public/frontend/landing_page2/js/contact.js')}}"></script>

    <!-- fancybox-media -->
    <script src="{{URL::asset('public/frontend/landing_page2/js/fancyjs.js')}}"></script>
    <script src="{{URL::asset('public/frontend/landing_page2/js/jquery.fancybox.js')}}"></script>
    <script src="{{URL::asset('public/frontend/landing_page2/js/jquery.fancybox.pack.js')}}"></script>
    <script src="{{URL::asset('public/frontend/landing_page2/js/jquery.mousewheel-3.0.6.pack.js')}}"></script>
    <script src="{{URL::asset('public/frontend/landing_page2/js/jquery.fancybox-media.js')}}"></script>

    <script src="{{URL::asset('public/frontend/landing_page2/js/validator.min.js')}}"></script>
    <script src="{{URL::to('public/admin/js/jquery.validate.js') }}"></script>

<script>
$('a.banBotArw[href^="#contact-area"]').on('click', function(event) {

    var target = $( $(this).attr('href') );

    if( target.length ) {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: target.offset().top
        }, 3000);
    }

});
</script>
<script>
    var timer = 0;
	var landing_prod_timer = [];
	var detail = null;

    $(document).ready(function(){
        detail = {!! json_encode(@$detail) !!}
        console.log(detail);

        $('#contact_form').validate();

        setTimeout(function() {
            $('#landing_preloader').fadeOut('slow', function() {
                $(this).remove();
            });
        }, 2000);
        if(detail !== null){
            changeColours(detail.color_primary, detail.color_secondary, detail.color_tertiary);
            if(detail.header_reverse_counter == 'Y' || detail.header_reverse_counter == 'N'){
				if(new Date() > new Date(detail.header_counter_date)){
					$('.header_reverse_text').text("Promoção terminou");
					$('.header_counter').empty();
				} else {
					timer = setInterval(function() {
						var html = timeDiffCalc( new Date(), new Date(detail.header_counter_date) );
						if(html == 0){
							clearInterval(timer);
							$('.header_reverse_text').text("Promoção terminou");
                    		$('.header_counter').empty();
						}
						$('.header_counter').html(html);
						changeTimerColours(detail.color_primary, detail.color_secondary, detail.color_tertiary);
					}, 1000);
					// changeColours(detail.color_primary, detail.color_secondary, detail.color_tertiary);
				}
            }
        }
        if(detail.get_product_details.length > 0){
            // for(i=0; i<detail.get_product_details.length; i++){

            //     console.log(i);
            //     var id = parseInt(i)+1;
            //     landing_prod_timer[id] = setInterval(function() {
            //         console.log(i);
            //         console.log(detail.get_product_details);
            //         var html = prodTimeDiffCalc( new Date(), new Date(detail.get_product_details[i].product_exp_date) );
            //         if(html == 0){
            //             $('.product_expiry_'+id).html("Promoção terminou");
            //             clearInterval(landing_prod_timer[id]);
            //         } else {
            //             $('.product_expiry_'+id).html(html);
            //         }
            //     }, 1000);
            // }
            // if ( window.location !== window.parent.location ){
            //     console.log("In iframe");
            //     $('#products-initial').show();
            //     $('#products-actual').hide();
            // } else{
            //     $('#products-initial').hide();
            //     $('#products-actual').show();
            // }
            $.each(detail.get_product_details, function(i, tim){
                var id = parseInt(i)+1;
                console.log(i);
                landing_prod_timer[id] = setInterval(function() {
                    var html = prodTimeDiffCalc( new Date(), new Date(detail.get_product_details[i].product_exp_date) );
                    if(html == 0){
                        $('.product_expiry_'+id).html("Promoção terminou");
                        clearInterval(landing_prod_timer[id]);
                    } else {
                        $('.product_expiry_'+id).html(html);
                    }
                }, 1000);
            });
        } else {
            // if ( window.location !== window.parent.location ){
            //     console.log("In iframe");
            //     $('#products-initial').hide();
            //     $('#products-actual').show();
            // } else{
            //     $('#products-initial').show();
            //     $('#products-actual').hide();
            // }
        }
    });
    function timeDiffCalc(dateFuture, dateNow) {
        if(Math.abs(dateFuture - dateNow) == 0){
            return 0;
        } else {
            let diffInMilliSeconds = Math.abs(dateFuture - dateNow) / 1000;
            const days = Math.floor(diffInMilliSeconds / 86400);
            diffInMilliSeconds -= days * 86400;
            const hours = Math.floor(diffInMilliSeconds / 3600) % 24;
            diffInMilliSeconds -= hours * 3600;
            const minutes = Math.floor(diffInMilliSeconds / 60) % 60;
            diffInMilliSeconds -= minutes * 60;
            const seconds = Math.round(diffInMilliSeconds);

            let difference = '<ul class="counter_ul">';
            if (days > 0) { difference += (days === 1) ? `<li class="header-timer-hours"><p class="counter_num color_primary">${days}</p><p class="color_primary_font">dias</p></li>` : `<li class="header-timer-hours"><p class="counter_num color_primary">${days}</p><p class="color_primary_font">dias</p></li>`; }

            difference += (hours < 10 ) ? `<li class="header-timer-hours"><p class="counter_num color_primary">0${hours}</p><p class="color_primary_font">horas</p></li>` : `<li class="header-timer-hours"><p class="counter_num color_primary">${hours}</p><p class="color_primary_font">horas</p></li>`;
            difference += (minutes < 10 ) ? `<li class="header-timer-hours"><p class="counter_num color_primary">0${minutes}</p><p class="color_primary_font">minutos</p></li>` : `<li class="header-timer-hours"><p class="counter_num color_primary">${minutes}</p><p class="color_primary_font">minutos</p></li>`;
            difference += (seconds < 10 ) ? `<li class="header-timer-hours"><p class="counter_num color_primary">0${seconds}</p><p class="color_primary_font">segundos</p></li>` : `<li class="header-timer-hours"><p class="counter_num color_primary">${seconds}</p><p class="color_primary_font">segundos</p></li>`;
            difference += "</ul>";
            return difference;
        }
    }
    function prodTimeDiffCalc(dateFuture, dateNow) {
        if(Math.abs(dateFuture - dateNow) == 0){
            return 0;
        } else {
            let diffInMilliSeconds = Math.abs(dateFuture - dateNow) / 1000;
            const days = Math.floor(diffInMilliSeconds / 86400);
            diffInMilliSeconds -= days * 86400;
            const hours = Math.floor(diffInMilliSeconds / 3600) % 24;
            diffInMilliSeconds -= hours * 3600;
            const minutes = Math.floor(diffInMilliSeconds / 60) % 60;
            diffInMilliSeconds -= minutes * 60;
            const seconds = Math.round(diffInMilliSeconds);

            let difference = '';
            if (days > 0) { difference += (days === 1) ? `${days}:` : `${days}:`; }

            difference += (hours < 10 ) ? `0${hours}:` : `${hours}:`;
            difference += (minutes < 10 ) ? `0${minutes}:` : `${minutes}:`;
            difference += (seconds < 10 ) ? `0${seconds}` : `${seconds}`;
            return difference;
        }
    }
    function changeColours(primary, secondary, tertiary){
        console.log("Changing colours");
        var val = primary;
        var rgb = hexToRgb(val);
        var rgba = [rgb.slice(0, rgb.length-1), ", 0.3", rgb.slice(rgb.length-1)].join('');
        rgba = rgba.replace('rgb','rgba');
		var clr = "";

        $('.color_primary').css('background', val);
        $('.color_primary_font').css('color', val);
        $('.color_primary_border').css('border-color', val);
        $('.color_primary_hover').hover(function(){
            $(this).css('background', primary);
            $(this).css('color', '#fff');
        }, function(){
            $(this).css('background', 'transparent');
            $(this).css('color', primary);
        });
        $('.color_primary_font_hover').hover(function(){
			clr = $(this).css('color');
            $(this).css('color', val);
        },function(){
            $(this).css('color', clr);
			clr = "";
        });
        $('.color_primary_owl').find('.owl-prev').css('color', val).hover(function(){
            console.log("HOv");
            $(this).css('background', val);
            $(this).css('color', '#fff');
        }, function(){
            $(this).css('background', 'transparent');
            $(this).css('color', val);
        });
        $('.color_primary_owl').find('.owl-next').css('color', val).hover(function(){
            $(this).css('background', val);
            $(this).css('color', '#fff');
        }, function(){
            $(this).css('background', 'transparent');
            $(this).css('color', val);
        });
        $('.color_primary_light').css('background', rgba);

        $('.color_secondary').css('background', secondary);
        $('.color_secondary_font').css('color', secondary);
        $('.color_secondary_border').css('border-color', secondary);
        $('.color_secondary_font_hover').hover(function(){
			clr = $(this).css('color');
            $(this).css('color', secondary);
        },function(){
            $(this).css('color', clr!="" ? clr : '#000');
			clr = "";
        });

        $('.color_tertiary').css('background', tertiary);
        $('.color_tertiary_font').css('color', tertiary);
    }
    function changeTimerColours(primary, secondary, tertiary){
		$('.counter_num').css('background', primary);
        $('.header-timer-hours').find('.color_primary_font').css('color', primary);
	}
	function clearTheTimer(){
		clearInterval(timer);
	}
    function clearProductTimer(num){
		clearInterval(landing_prod_timer[num]);
	}
	$(document).delegate('.accordionlanding1 .card-header', 'click', function(){
		console.log("Clicked");
		if($(this).hasClass('collapsed')){
			console.log("opened");
			$(this).removeClass('color_primary');
			$(this).addClass('color_tertiary');
			$(this).css('color','#333');
            $(this).find('i').removeClass('fa-minus');
			$(this).find('i').addClass('fa-plus');
		} else {
            $('.accordionlanding1 .card-header').each(function(i,elem){
                $(elem).removeClass('color_primary');
                $(elem).addClass('color_tertiary');
                $(elem).css('color','#333');
                $(this).find('i').removeClass('fa-minus');
				$(this).find('i').addClass('fa-plus');
            });
            console.log("collapsed");
			$(this).removeClass('color_tertiary');
			$(this).addClass('color_primary');
			$(this).css('color','#fff');
            $(this).find('i').removeClass('fa-plus');
            $(this).find('i').addClass('fa-minus');
		}
        if ( window.location !== window.parent.location ){
			window.top.callChangeColours();
		} else{
			changeColours(detail.color_primary, detail.color_secondary, detail.color_tertiary);
		}
	});
    // $(document).delegate('.header_btn', 'click', function(){
	// 	var height = $('#myModal').innerHeight();
	// 	$('#myModal').find('#privacy-modal').find('.modal-body').css('min-height', height);
	// });
    $('.paly_btn').click(function (){
		$('.single_vdo video').attr('controls',true);
		$('.single_vdo video')[0].play();
		$('.paly_btn').hide();
	});
    $("#lead_form_footer").validate({
		rules: {
			mobile: {
				minlength:8,
				maxlength:13,
			}
		},
        messages: {
            lead_privacy_policy_accepted: {
                required : "{{\Lang::get('client_site.lead_privacy_error')}}."
            },
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "lead_privacy_policy_accepted") {
                $("#lead_privacy_err_footer").append(error);
            } else {
                error.insertAfter(element);
            }
        }
	});
	$("#lead_form_popup").validate({
		rules: {
			mobile: {
				minlength:8,
				maxlength:13,
			}
		},
        messages: {
            lead_privacy_policy_accepted: {
                required : "{{\Lang::get('client_site.lead_privacy_error')}}."
            },
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "lead_privacy_policy_accepted") {
                $("#lead_privacy_err_popup").append(error);
            } else {
                error.insertAfter(element);
            }
        }
	});
	$('#lead_form_footer_btn').click(function(){
		if ( window.location !== window.parent.location ){
			console.log("In iframe");
			$('#myModal2').modal();
            $('#myModal2').modal("show");
		} else{
			if($('#lead_form_footer').valid()){
				var formData = $("#lead_form_footer").serialize();
				$.ajax({
					url: "{{ route('landing.page.save.lead') }}",
					method: 'post',
					dataType: 'json',
					data: formData,
					async: false,
					success: function(res){
						console.log(res);
						if(res.status == 'success'){
							$('#myModal2').modal();
                            $('#myModal2').modal("show");
							$('#success-modal').fadeIn("slow");
						} else {
							toastr.error(res.error);
						}
					},
					error: function(err){
						console.log(err);
					},
				});
			}
		}
	});
    $('#lead_form_popup_btn').click(function(){
		if ( window.location !== window.parent.location ){
			console.log("In iframe");
			$('#form-modal').fadeOut("slow");
			$('#success-modal').fadeIn("slow");
		} else {
			if($('#lead_form_popup').valid()){
				var formData = $("#lead_form_popup").serialize();
				$.ajax({
					url: "{{ route('landing.page.save.lead') }}",
					method: 'post',
					dataType: 'json',
					data: formData,
					async: false,
					success: function(res){
						console.log(res);
						if(res.status == 'success'){
							$('#lead_form_popup')[0].reset();
							$('#form-modal').fadeOut("slow");
							$('#success-modal').fadeIn("slow");
						} else {
							toastr.error(res.error);
						}
					},
					error: function(err){
						console.log(err);
					},
				});
			}
		}
	});
	$('#lead_success_ok_footer').click(function(){
		// clearTimeout(leadSuccessTimout);
		$("#myModal2").modal();
		$("#myModal2").modal("hide");
		$('#lead_form_footer')[0].reset();
        window.location.href = "{{@$detail->lead_download_link}}";
	});
    $('#lead_success_ok_popup').click(function(){
		// clearTimeout(leadSuccessTimout);
		$('#form-modal').fadeIn("slow");
		$('#success-modal').fadeOut("slow");
		$('#lead_form_popup')[0].reset();
        $("#myModal").modal();
		$("#myModal").modal("hide");
        window.location.href = "{{@$detail->lead_download_link}}";
	});
	$('#show_lead_popup_privacy_policy').click(function(){
		$('#form-modal').hide();
		$('#privacy-modal').show();
    });
	$('.privacy_back').click(function(){
		$('#privacy-modal').hide();
		$('#form-modal').show();
	});

    let hexToRgb= c=> `rgb(${c.match(/\w\w/g).map(x=>+`0x${x}`)})`;
</script>
<script>
    $(function() {
        var Accordion = function(el, multiple) {
            this.el = el || {};
            this.multiple = multiple || false;
            // Variables privadas
            var links = this.el.find('.link');
            // Evento
            links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
        }

        Accordion.prototype.dropdown = function(e) {
            var $el = e.data.el;
            $this = $(this),
            $next = $this.next();
            if($this.parent().hasClass('open')) {
            $next.slideUp();
            $this.parent().removeClass('open');
            console.log($this.children("i.fa"));
            $this.children("i.fa").removeClass("fa-minus").addClass("fa-plus");
            } else {
            $next.slideDown();
            $this.parent().addClass('open');
            $this.children("i.fa").removeClass("fa-plus").addClass("fa-minus");
            }
            if (!e.data.multiple) {
            $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
            console.log($el.find('.link').not($this).children("i.fa").removeClass().addClass("fa fa-plus"))
            };
        }
        var accordion = new Accordion($('#accordion, #accordion1'), false);
    });
</script>
</body>

</html>
