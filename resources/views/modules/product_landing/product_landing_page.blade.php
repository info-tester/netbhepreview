@extends('layouts.app')
@section('title', 'Product Landing Page')
@section('style')
@include('includes.style')


      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <!--style-->
      <link href="css/style.css" type="text/css" rel="stylesheet" media="all" />
      <link href="css/responsive.css" type="text/css" rel="stylesheet" media="all" />
      <!--bootstrape-->
      <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet" media="all" />
      <!--font-awesome-->
      <link href="css/font-awesome.min.css" type="text/css" rel="stylesheet" media="all" />
      <!--fonts-->
      <!--<link href="https://fonts.googleapis.com/css?family=Didact+Gothic" rel="stylesheet"> -->
      <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,900" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900&display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900&display=swap" rel="stylesheet">
      <!-- Owl Stylesheets -->
      <link rel="stylesheet" href="css/owl.carousel.min.css">
      <link rel="stylesheet" href="css/owl.theme.default.min.css">
      <!--price range (Calender/date picker)-->
      <link href="css/themes_smoothness_jquery-ui.css" rel="stylesheet" type="text/css">

@endsection
@section('scripts')
@include('includes.scripts')
      <script type="text/javascript" src="js/jquery-3.2.1.js"></script>
      <script type="text/javascript" src="js/bootstrap.min.js"></script>
      <!-- Owl javascript -->
      <script src="js/jquery.min.js"></script>
      <script src="js/owl.carousel.js"></script>
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')

      <!--wrapper start-->
      <div class="wrapper">

      <section class="feature_banner">
         <img src="{{URL::to('storage/app/public/uploads/content_images/'.@$banner1->image)}}">
         <div class="fea_banner_cont">
            <div class="container">
               <div class="row">
                  @php
                     $class = "";
                     if(@$banner1->description_6 == "C") $class = "col-lg-10 col-md-10 mx-auto text-center";
                     else if(@$banner1->description_6 == "R") $class = "col-lg-5 col-md-5 offset-md-7 text-right";
                     else $class = "col-lg-5 col-md-5";
                  @endphp
                  <div class="{{@$class}}">
                     <div class="fea_banner">
                        <h2>{{@$banner1->title}}</h2>
                        <p>{{@$banner1->description}}</p>
                        <a href="{{@$banner1->description_4}}" class="get_btn">{{@$banner1->heading_4}}</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>


      <section class="build_course">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="build_head text-center">
                     <p>{{@$banner1->heading_1}}</p>
                     <h2>{{@$banner1->description_1}}</h2>
                  </div>
                  <div class="inner_fea text-center">
                     <img src="{{URL::to('storage/app/public/uploads/content_images/'.@$banner1->image_1)}}">
                     <div class="inner_fea_con">
                        <h3>{{@$banner1->short_description_1}}</h3>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="skill_sec">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <h3 class="skill_head"><b>{{@$banner1->heading_2}}</b></h3>
               </div>
               @foreach(@$smallSections1 as $s1)
               <div class="col-md-3 col-sm-4 col-6">
                  <div class="skill_info">
                     <img src="{{URL::to('storage/app/public/uploads/content_images/'.@$s1->image)}}">
                     <p>{{@$s1->title}}</p>
                  </div>
               </div>
               @endforeach
            </div>
         </div>
      </section>

      <section class="customize_sec">
         <div class="container">
            <div class="row align-items-center">

               <div class="col-md-6">
                  <div class="custom_content">
                     <h3>{{@$banner1->heading_3}}</h3>
                     <ul>
                        @foreach(@$smallSections2 as $s2)
                        <li><img src="{{ URL::to('public/frontend/images/ti.png') }}">{{@$s2->title}}</li>
                        @endforeach

                     </ul>
                  </div>

               </div>

               <div class="col-md-6">
                  <img src="{{URL::to('storage/app/public/uploads/content_images/'.@$banner1->image_3)}}" width="100%" class="cus_banner">
               </div>


            </div>
         </div>
      </section>

      <section class="pro_sec">
         <div class="container">
            <div class="row align-items-center">

               <div class="col-md-12">
                  <div class="prog_heading">
                     <h3>{{@$banner1->heading_5}}</h3>
                  </div>
               </div>
               @foreach(@$smallSections3 as $s3)
               <div class="col-md-6">
                  <div class="prog_info">
                     <h6>{{@$s3->title}}</h6>
                     <p>{{@$s3->description}}</p>
                  </div>
               </div>
               @endforeach
            </div>
         </div>
      </section>

      <section class="deli_sec">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 ">
                  <div class="deli_info">
                     <h3>{{@$banner1->heading_6}}</h3>
                  </div>
                  @foreach(@$smallSections4 as $s4)
                  <div class="deli_cont">
                     <h6>{{@$s4->title}}</h6>
                     <p>{{@$s4->description}}</p>
                  </div>
                  @endforeach


               </div>

               <div class="col-md-6">
                  <img src="{{URL::to('storage/app/public/uploads/content_images/'.@$banner1->image_6)}}" class="deli_banner">
               </div>
            </div>
         </div>
      </section>

       <section class="start_sec">
         <div class="container">
             <div class="row">
               <div class="col-12">
                  <div class="get_start text-center">
                     <h4><span class="extra_block">{{@$banner1->heading_7}}</span></h4>
                     <a href="{{@$banner1->button_link_7}}" class="start_btn">{{@$banner1->description_7}}</a>
                  </div>
               </div>
            </div>
         </div>
       </section>



          <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal_product">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close btn_modal" data-dismiss="modal">&times;</button>

        </div>
        <div class="modal-body">
          <p>Course Preview</p>
          <h4>2021 Complete Python Bootcamp From Zero to Hero in Python</h4>

          <div class="video_les">
              <iframe width="560" height="315" src="https://www.youtube.com/embed/jnovz-qihrU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>

          <div class="smap_video">
              <h4>Free Sample Videos:</h4>
              <div class="video-smaple_list">
                <div class="video_details">
                  <img src="images/thumb-1.jpg">
                  <p>Course Introduction</p>
                  </div>
                  <span>03:45</span>
              </div>
              <div class="video-smaple_list active">
                <div class="video_details">
                  <img src="images/thumb-1.jpg">
                  <p>Course Introduction</p>
                  </div>
                  <span>03:45</span>
              </div>
              <div class="video-smaple_list">
                <div class="video_details">
                  <img src="images/thumb-1.jpg">
                  <p>Course Introduction</p>
                  </div>
                  <span>03:45</span>
              </div>
              <div class="video-smaple_list">
                <div class="video_details">
                  <img src="images/thumb-1.jpg">
                  <p>Course Introduction</p>
                  </div>
                  <span>03:45</span>
              </div>
          </div>

        </div>

      </div>

    </div>
  </div>

@endsection
@section('footer')
@include('includes.footer')


      </div>
      <!--wrapper end-->
      <!--price range (Calender/date picker)-->
      <script src="js/jquery.com_ui_1.11.4_jquery-ui.js"></script>
      <script>
         $(function() {
             $("#slider-range").slider({
                 range: true,
                 min: 0,
                 max: 1000,
                 values: [75, 300],
                 slide: function(event, ui) {
                     $("#amount").val("$" + ui.values[0] + " - $." + ui.values[1]);
                 }
             });
             $("#amount").val("$" + $("#slider-range").slider("values", 0) + " - $" + $("#slider-range").slider("values", 1));
         });
      </script>
      <script type="text/javascript">
         // The function toggles more (hidden) text when the user clicks on "Read more". The IF ELSE statement ensures that the text 'read more' and 'read less' changes interchangeably when clicked on.
         $('.moreless-button').click(function() {
             $('.moretext').slideToggle();
             if($('.moreless-button').text() == "Read More +") {
                 $(this).text("Read Less -")
             } else {
                 $(this).text("Read More +")
             }
         });
      </script>
@endsection
