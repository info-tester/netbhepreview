@extends('layouts.app')
@section('title')
  @lang('site.get_the_right_advice')
@endsection

@section('style')
	@include('includes.style')
@endsection

@section('scripts')
	@include('includes.scripts')
@endsection

@section('header')
	@include('includes.header')
@endsection
<style>
.dropdown_dash2 {

    background: #fff;
    box-shadow: 1px 3px 4px -1px rgb(43 43 43);
    position: absolute;
    left: 0;
    top: 30px;
    width: 180px;
    z-index: 99999;
    border-radius: 6px;

}

.dropdown_dash2 ul {

    margin: 0;

    padding: 0;

}

.dropdown_dash2 ul li {

    display: block;

    margin: 0;

    overflow: hidden;

    width: 100%;

    padding: 0 !important;

    text-align: left;

}

.dropdown_dash2 ul li:last-child {

    padding-left: 0px !important;

    border: none;

}

.dropdown_dash2 ul li a {

    color: #747474;

    float: left;

    font-family: 'Poppins', sans-serif;

    font-size: 14px;

    font-weight: normal;

    padding:5px 12px !important;

    text-decoration: none;

    width: 100%;

}

.dropdown_dash2 ul li a:hover {

    color: #4290fb;

}

#primary_nav_wrap ul a:active
{

  color: #8ABF29;
background-color: #2d2f33; 
    -webkit-transition: all 50ms linear;
-moz-transition: all 50ms linear;
-ms-transition: all 50ms linear;
-o-transition: all 50ms linear;
transition: all 50ms linear;
}

#primary_nav_wrap ul ul li.dir33
{
  position:relative;
}
#primary_nav_wrap ul li{
  float:left;
  margin:0;
  padding:0
background: #121314;
}

#primary_nav_wrap ul li.current-menu-item
{
  background-color: #222326; 
}



#primary_nav_wrap ul ul
{
  display:none;
  top:100%;
  left:0;
  padding:0;
}

#primary_nav_wrap ul ul li
{
  float:none;
  width:120px
    
}

#primary_nav_wrap ul ul a
{
  padding:10px 15px;
  
}


#primary_nav_wrap ul ul ul
{
    position: absolute;
  top:55px;
  left:88%;
  background: #ffff;
    border-radius: 5px;
    padding: 10px;
    box-shadow: 1px 3px 4px -1px rgb(43 43 43);

}
.dropdown_dash2 ul li{
    overflow: inherit !important;
}

#primary_nav_wrap ul li:hover > ul
{
  display:block

}
.dropdown_dash2 ul {



    margin: 0;



    padding: 0;



}
</style>

@section('content')
<section class="category">
    <div class="container">
        <div class="row">
            <div class="page-h2">
                <h2>{{ $meetNeeds->title ?? __('client_site.browse_by_category') }}</h2>
                <p>
                    {!! $meetNeeds->description ?? '' !!}
                </p>
            </div>
          
            <div class="all-categories">
                <div class="row">
                    @foreach(@$categories as $ct)
                        <div class="col-lg-3 col-md-6 col-sm-12">
                            <div class="categori-box">
                                <a href="{{ route('all.product.search',['slug'=>@$ct->slug]) }}">
                                    @if(@$ct->image)
                                        <img src="{{URL::to('storage/app/public/product_category').'/'.@$ct->image}}" alt="">
                                    @else
                                    <img src="{{URL::to('public/frontend/images').'/cat1.png'}}" alt="">
                                    @endif
                                    <p>{{ @$ct->category_name }}</p>
                                </a>
                            </div>
                        </div>
                    @endforeach


                    <!-- <div class="col-lg-12 text-center">
                        <a class="view-all" href="{{ route('browse.categories') }}">@lang('client_site.view_all')<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    </div> -->

                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
	@include('includes.footer')
        <script src="{{ URL::to('public/frontend/js/jquery.com_ui_1.11.4_jquery-ui.js') }}"></script>
        <script>
            $(function() {
                $( "#slider-range" ).slider({
                    range: true,
                    min: {{ @$minprice->rate_price }},
                    max: {{ @$maxprice->rate_price }},
                    values: [ {{ @$key['min_price'] ? @$key['min_price']: @$minprice->rate_price }}, {{ @$key['max_price'] ? $key['max_price']: @$maxprice->rate_price }} ],
                    slide: function( event, ui ) {
                        $( "#amount" ).val( "R$" + ui.values[ 0 ] + " - R$" + ui.values[ 1 ] );
                        $('#min_price').val(ui.values[ 0 ]);
                        $('#max_price').val(ui.values[ 1 ]);
                    }
                });
                $( "#amount" ).val( "R$" + $( "#slider-range" ).slider( "values", 0 ) +
                    " - R$" + $( "#slider-range" ).slider( "values", 1 ) );

                        $('#min_price').val($( "#slider-range" ).slider( "values", 0 ));
                        $('#max_price').val($( "#slider-range" ).slider( "values", 1 ));
            });

            $('.frm1').click(function(){
                $('#myForm').submit();
            });

            $('#category').change(function(){
                if($('#category').val()==""){
                    $('#subcategory').html("");
                }else{
                    var reqData = {
                      'jsonrpc' : '2.0',
                      '_token' : '{{csrf_token()}}',
                      'params' : {
                            'cat' : $('#category').val()
                        }
                    };
                    $.ajax({
                        url: "{{ route('fetch.subcat') }}",
                        method: 'post',
                        dataType: 'json',
                        data: reqData,
                        success: function(response){
                            if(response.status==1) {
                                var i=0, html="";
                                html = '<option value="">@lang('client_site.select_option')</option>';
                                for(;i<response.result.length;i++){
                                    html+='<option value='+response.result[i].id+'>'+response.result[i].name+'</option>';
                                }
                                $('#subcategory').html(html);
                                }
                        }, error: function(error) {
                            console.error(error);
                        }
                    });
                }
            });

            $('#sort_by').change(function(){
                $('form').submit();
            });

            $('.ssnbtnslg').click(function(){
                if($(this).data('slug')!=""){
                    localStorage.removeItem('bookSlug');
                    localStorage.setItem('bookSlug', $(this).data('slug'));
                    location.href="{{ route('login') }}"
                }
            });
        </script>

@endsection
