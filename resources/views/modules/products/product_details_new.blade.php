@extends('layouts.app')
@section('title', 'Search Results')
@section('style')
@include('includes.style')

      <!--style-->
      <link href="css/style.css" type="text/css" rel="stylesheet" media="all" />
      <link href="css/responsive.css" type="text/css" rel="stylesheet" media="all" />
      <!--bootstrape-->
      <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet" media="all" />
      <!--font-awesome-->
      <link href="css/font-awesome.min.css" type="text/css" rel="stylesheet" media="all" />
      <!--fonts-->
      <!--<link href="https://fonts.googleapis.com/css?family=Didact+Gothic" rel="stylesheet"> -->
      <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,900" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900&display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900&display=swap" rel="stylesheet">
      <!-- Owl Stylesheets -->
      <link rel="stylesheet" href="css/owl.carousel.min.css">
      <link rel="stylesheet" href="css/owl.theme.default.min.css">
      <!--price range (Calender/date picker)-->
      <link href="css/themes_smoothness_jquery-ui.css" rel="stylesheet" type="text/css">
      <style>
         .video_details video {
            width: 70px !important;
            margin-right: 15px !important;
         }
         .main_vdo .p-reative {
            width: 70px !important;
         }
      </style>
@endsection
@section('scripts')
@include('includes.scripts')
      <!-- <script type="text/javascript" src="js/jquery-3.2.1.js"></script>
      <script type="text/javascript" src="js/bootstrap.min.js"></script> -->
      <!-- Owl javascript -->
      <!-- <script src="js/jquery.min.js"></script>
      <script src="js/owl.carousel.js"></script> -->
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')


      <!--wrapper start-->
      <div class="wrapper">




         <section class="product_details rm_stylee01">
            <div class="top_sec">

               <div class="dark-area">
               <div class="container">
               <div class="row m-0">

               <div class="prro_top">
                              <div class="topic_heading">
                                 <ul>
                                    <li><a href="#">{{@$product->category->category_name}}</a></li>
                                    <li><i class="fa fa-angle-right"></i></li>
                                    <li><a href="#">{{@$product->subCategory->name}}</a></li>
                                    <div class="clearfix"></div>
                                 </ul>
                                 <h1 class="prod_heading">{{@$product->title}}</h1>
                                 <p class="pro_para">
                                    @if(strlen(@$product->description)>200)
                                       {{substr(@$product->description,0,200)}}<span class="moretext1 moretext1_dis" id="moreTargetInf">{{substr(@$product->description,200)}}</span><a href="#url" class="readMore light_bl" data-target="moreTargetInf"> @lang('site.read_more')</a>
                                    @else
                                       {{@$product->description}}
                                    @endif
                                 </p>
                              </div>
                              <div class="rating_sec">
                                 <div class="rating_pro"> <span class="ratingstar">
                                    {{round(@$prod_avg_rating, 1)}}


                                    <i class="fa fa-star"></i>


                                    </span> ({{ round(@$product->total_rating) }} @lang('client_site.ratings'))
                                 </div>
                                 <p>{{@$totalStudents ?? '0'}} Aluno{{@$totalStudents == 1 ? '' : 's'}}</p>
                              </div>
                              <div class="creat"> <span>@lang('client_site.created_by') </span><a href="{{route('pblc.pst',['slug' => @$product->professional->slug])}}">{{@$product->professional->name}}</a> </div>
                              <div class="ins_ele">
                                 <ul>
                                    <li><img src="{{ URL::to('public/frontend/images/clock.png') }}">
                                       @lang('client_site.last_updated')
                                       @php $month=[ "0", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ] @endphp
                                       {{date('d',strtotime(@$product->updated_at))}}
                                       {{$month[(int)date('m',strtotime(@$product->updated_at))]}}
                                       {{date('Y',strtotime(@$product->updated_at))}}
                                    </li>
                                    <!-- <li><img src="images/www.png"> English</li>
                                    <li><img src="images/lan.png"> English [Auto], French [Auto]</li>
                                    <li><a href="#">5 more</a></li> -->
                                 </ul>
                                 <div class="clearfix"></div>
                              </div>
                              <div class="btn_div">
                                 <ul>
                                    <input type="hidden" name="" id="product_id" value="{{@$product->id}}">
                                    @if(@$wishlist)
                                    <li><a href="javascript:;" id="remove_from_wishlist">@lang('client_site.wishlist')<i class="fa fa-heart"></i></a></li>

                                    @else
                                    <li><a href="javascript:;" id="add_to_wishlist">@lang('client_site.wishlist')<i class="fa fa-heart-o"></i></a></li>

                                    @endif
                                    <li>
                                       <!-- <a href="#">@lang('client_site.share') <i class="fa fa-share"></i></a> -->
                                       <div class="st-custom-button" data-network="sharethis" data-url="{{ url()->current() }}">
                                          <a href="javascript:;" class="shre" style="border-radius: 4px !important;">@lang('client_site.share')<i class="fa fa-share"></i></a>
                                       </div>
                                    </li>
                                    @if(Auth::check() && !in_array(@$product->id, $orderedProducts))
                                       <li><a href="{{ route('view.free.lessons', @$product->slug) }}">@lang('client_site.view_free_lessons')</a></li>
                                    @endif

                                    <!-- <li><a href="#">@lang('client_site.send_to_a_friend')</a></li> -->
                                 </ul>
                                 <div class="clearfix"></div>
                              </div>
                           </div>

               </div>
               </div>
               </div>

               <div class="container">
                  <!--<div class="row m-0">-->
                  <div class="">
                     <div class="zin">
                        <div class="product_right_panel sticky_right">

                           <div class="product_video">
                            <div class="play_video">
                              <div class="pro_video_info">
                                 @if(@$lesson_video)


                                    @if(@$lesson_video->filetype == 'V')
                                     <video id="video_link" src="{{ URL::to('storage/app/public/lessons/type_video/'.@$lesson_video->filename)}}" width="303.25" height="175" @if(@$lesson_video_thumbnail) poster="{{ URL::to('storage/app/public/lessons/type_video/thumbnails/'.@$lesson_video_thumbnail->filename)}}" @endif ></video>
                                     <input type="hidden" name="" id="video_link_id" value="{{@$lesson_video->id}}">



                                     <a href="javascript:;" id="play_btn" data-toggle="modal" data-target="#myModal" data-id="{{@$lesson_video->id}}"><img src="{{ URL::to('public/frontend/images/play.png') }}"></a>

                                     <p>@lang('client_site.preview_this_course')</p>

                                     <!-- <a href="javascript:;" id="play_btn" data-toggle="modal" data-target="#myModal" data-id="{{@$lesson->getLessonFile->id}}"><img src="images/play.png"></a> -->
                                    @else
                                       <iframe width="100%" height="100%" src="https://www.youtube.com/embed/{{@$lesson_video->filename}}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                                       <input type="hidden" name="" id="video_link_id" value="{{@$lesson_video->id}}">



                                     <a href="javascript:;" id="play_btn" data-toggle="modal" data-target="#myModal" data-id="{{@$lesson_video->id}}"><img src="{{ URL::to('public/frontend/images/play.png') }}"></a>

                                     <p>@lang('client_site.preview_this_course')</p>

                                    @endif

                                 @else
                                    <img width="303.25" height="175" src="{{URL::to('storage/app/public/uploads/product_cover_images/'.@$product->cover_image)}}">

                                 @endif

                              </div>

                            </div>

                            <div class="price_list">
                                <div class="price_tag">
                                    @if(@$product->discounted_price>0)
                                    <span class="re_price">R${{@$product->discounted_price}}</span>
                                    <span class="dis_price mark_red">
                                       <del>R${{@$product->price}}</del>
                                       <span class="dis_price_span">
                                          <?php  $a = ((@$product->price - @$product->discounted_price)/@$product->price)*100;
                                          echo ceil($a)
                                          ?>% de desconto
                                       </span>
                                    </span>
                                    <!-- <span class="dis_price">

                                    </span> -->
                                    @else
                                    <span class="re_price">R${{@$product->price}}</span>
                                    @endif
                                </div>
                                @if(@auth()->user()->id != $product->professional_id)
                                <div class="pro_btn">
                                    @if(in_array(@$product->id, $orderedProducts))
                                        <p class="text-center text-secondary w-100" style="font-size:1rem;">@lang('client_site.already_ordered')</p>
                                    @else
                                       @if(date('Y-m-d') >= date('Y-m-d', strtotime(@$product->purchase_start_date)))
                                          @if(@$product->purchase_end_date && date('Y-m-d') >= date('Y-m-d', strtotime(@$product->purchase_end_date)))
                                             <p class="text-center text-secondary w-100" style="font-size:1rem;">@lang('client_site.sale_over')</p>
                                          @else
                                             <a href="javascript:;" class="solid_btn" id="addToCart" data-product="{{$product->id}}" @if(@$cartData) style="display: none" @endif>@lang('client_site.add_to_cart')</a>
                                             <a href="{{route('product.cart')}}" class="solid_btn" id="goToCart" @if(@$cartData==null) style="display: none" @endif>@lang('client_site.go_to_cart')</a>
                                             <a href="javascript:;" id="buyNow" data-product="{{$product->id}}" class="out_btn">{{__('site.buy_now')}}</a>
                                          @endif
                                       @else
                                          <p class="text-center w-100 hilited_text" style="font-size:1rem;"><span style="font-size:0.8rem;">@lang('client_site.available_from')</span><br>
                                             @php $month=[ "0", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ] @endphp
                                             {{date('d',strtotime(@$product->purchase_start_date))}}
                                             {{$month[(int)date('m',strtotime(@$product->purchase_start_date))]}}
                                             {{date('Y',strtotime(@$product->purchase_start_date))}}
                                          </p>
                                       @endif
                                    @endif
                                </div>
                                @endif
                                <div class="money_back">
                                    <!-- <p>30-Day Money-Back Guarantee</p> -->
                                </div>
                                <div class="course_info">
                                    <h6>@lang('client_site.this_course_includes'):</h6>
                                    <ul>
                                        <li><i class="fa fa-file-video-o"></i> {{count(@$chapters)}} @lang('client_site.chapters')</li>
                                        <li><i class="fa fa-play-circle"></i> {{@$product->total_lessons}} @lang('client_site.lessons')</li>
                                        <li><i class="fa fa-file-o"></i> {{@$product->course_duration}}  @lang('client_site.total_duration')</li>

                                    </ul>
                                </div>
                            </div>

                           </div>
                        </div>
                        <div class="product_left_panel">

                           <div class="prro_btm">
                              @if(@$product->affiliate_program == 'Y')
                                 @if(Auth::check() && Auth::user()->is_join_affiliate=="Y")
                                    <div class="what_sec">
                                       <h2>@lang('client_site.for_affiliates')</h2>
                                       <ul class="what_list ">
                                          <li class="w-100">
                                             <!-- <img src="{{ URL::to('public/frontend/images/tick.png') }}"> -->
                                             {{@$product->affiliate_text}}
                                          </li>
                                       </ul>
                                       <div class="clearfix"></div>
                                    </div>

                                    <br><br>
                                    <div class="clearfix"></div>
                                 @endif
                              @endif
                              <div class="what_sec">
                                 <h2>@lang('client_site.description')</h2>
                                 {{--<ul class="what_list ffro_lli">
                                    <li><img src="{{ URL::to('public/frontend/images/tick.png') }}">
                                       @if(strlen(@$product->how_it_works)>200)
                                          {{substr(@$product->how_it_works,0,200)}}
                                          <span class="moretext1 moretext1_dis" id="moreTarget0">{{substr(@$product->how_it_works,200)}}</span>
                                          <a href="#url" class="readMore red_mr" data-target="moreTarget0"> @lang('site.read_more')</a></p>
                                       @else
                                          {{@$product->how_it_works}}
                                       @endif
                                    </li>
                                 </ul>--}}
                                 <div>{!! @$product->admin_description !!}</div>
                                 <div class="clearfix"></div>
                              </div>
                              <br><br>
                              <div class="clearfix"></div>

                              <div class="what_sec">
                                 <!-- <h2>@lang('client_site.what_you_will_learn')</h2> -->
                                 <h2>@lang('client_site.requirements')</h2>
                                 <ul class="what_list ffro_lli">

                                    @if(@$product->how_it_works)
                                    <li><img src="{{ URL::to('public/frontend/images/tick.png') }}">
                                       @if(strlen(@$product->how_it_works)>200)
                                          {{substr(@$product->how_it_works,0,200)}}
                                          <span class="moretext1 moretext1_dis" id="moreTarget0">{{substr(@$product->how_it_works,200)}}</span>
                                          <a href="#url" class="readMore red_mr" data-target="moreTarget0"> @lang('site.read_more')</a></p>
                                       @else
                                          {{@$product->how_it_works}}
                                       @endif
                                    </li>
                                    @endif

                                    @if(@$product->can_help)
                                    <li><img src="{{ URL::to('public/frontend/images/tick.png') }}">
                                       @if(strlen(@$product->can_help)>200)
                                          {{substr(@$product->can_help,0,200)}}
                                          <span class="moretext1 moretext1_dis" id="moreTarget1">{{substr(@$product->can_help,200)}}</span>
                                          <a href="#url" class="readMore red_mr" data-target="moreTarget1"> @lang('site.read_more')</a></p>
                                       @else
                                          {{@$product->can_help}}
                                       @endif
                                    </li>
                                    @endif

                                    @if(@$product->for_whom)
                                    <li><img src="{{ URL::to('public/frontend/images/tick.png') }}">
                                       @if(strlen(@$product->for_whom)>200)
                                          {{substr(@$product->for_whom,0,200)}}
                                          <span class="moretext1 moretext1_dis" id="moreTarget2">{{substr(@$product->for_whom,200)}}</span>
                                          <a href="#url" class="readMore red_mr" data-target="moreTarget2"> @lang('site.read_more')</a></p>
                                       @else
                                          {{@$product->for_whom}}
                                       @endif
                                    </li>
                                    @endif

                                    @if(@$product->benefits)
                                    <li><img src="{{ URL::to('public/frontend/images/tick.png') }}">
                                       @if(strlen(@$product->benefits)>200)
                                          {{substr(@$product->benefits,0,200)}}
                                          <span class="moretext1 moretext1_dis" id="moreTarget3">{{substr(@$product->benefits,200)}}</span>
                                          <a href="#url" class="readMore red_mr" data-target="moreTarget3"> @lang('site.read_more')</a></p>
                                       @else
                                          {{@$product->benefits}}
                                       @endif
                                    </li>
                                    @endif

                                    @if(@$product->when_to_do)
                                    <li><img src="{{ URL::to('public/frontend/images/tick.png') }}">
                                       @if(strlen(@$product->when_to_do)>200)
                                          {{substr(@$product->when_to_do,0,200)}}
                                          <span class="moretext1 moretext1_dis" id="moreTarget4">{{substr(@$product->when_to_do,200)}}</span>
                                          <a href="#url" class="readMore red_mr" data-target="moreTarget4"> @lang('site.read_more')</a></p>
                                       @else
                                          {{@$product->when_to_do}}
                                       @endif
                                    </li>
                                    @endif

                                    @if(@$product->previous_preparation)
                                    <li><img src="{{ URL::to('public/frontend/images/tick.png') }}">
                                       @if(strlen(@$product->previous_preparation)>200)
                                          {{substr(@$product->previous_preparation,0,200)}}
                                          <span class="moretext1 moretext1_dis" id="moreTarget5">{{substr(@$product->previous_preparation,200)}}</span>
                                          <a href="#url" class="readMore red_mr" data-target="moreTarget5"> @lang('site.read_more')</a></p>
                                       @else
                                          {{@$product->previous_preparation}}
                                       @endif
                                    </li>

                                    @endif
                                 </ul>
                                 <div class="clearfix"></div>
                                 @if(@$has_certificate)
                                 <h6>
                                    <img src="{{ URL::to('public/frontend/images/certiffc.png') }}" alt="" height="10px" width="10px">
                                    @lang('client_site.certificate_after_course_completion')
                                 </h6>
                                 @endif
                                 <div class="clearfix"></div>
                              </div>

                              <div class="course_details">
                                 <h2>@lang('client_site.course_content')</h2>
                                 <div class="cou_list">
                                    <ul>
                                       <li>{{count(@$chapters)}} @lang('client_site.chapters')</li>
                                       <li> • </li>

                                       <li>{{@$product->total_lessons}} @lang('client_site.lessons') </li>

                                       <li> • </li>
                                       <li>{{@$product->course_duration}} @lang('client_site.total_length')</li>
                                    </ul>
                                    <a href="javascript:;"  class="collapsed" id="exp_all_sec">@lang('client_site.expand_all_sections')</a>
                                 </div>
                                 <div class="course_accor">
                                    <div class="accordian-faq">
                                       @if(@$chapters)
                                       @foreach(@$chapters as $k=>$chapter)
                                          <div class="accordion" id="faq{{$k}}">
                                             <div class="card">
                                                <div class="card-header" id="faqhead{{$k}}">
                                                   <a href="#" class="btn btn-header-link chapter_acco acco-chap collapsed" data-toggle="collapse" data-target="#faq{{$k}}_{{@$chapter->id}}" aria-expanded="true" aria-controls="faq1_{{@$chapter->id}}">
                                                      <p>{{@$chapter->chapter_title}}</p>
                                                      <ul>
                                                         <li>{{count(@$chapter->getLessons)}} @lang('client_site.lessons')</li>
                                                         <li> • </li>
                                                         <li>{{@$chapter->no_of_hours}} hr {{@$chapter->no_of_minutes}} min(s)</li>
                                                      </ul>
                                                   </a>
                                                </div>
                                                <div id="faq{{$k}}_{{@$chapter->id}}" class="collapse " aria-labelledby="faqhead{{$k}}" data-parent="#faq{{$k}}">
                                                   <div class="card-body">
                                                      <div class="inner_accor">
                                                         <div class="accordion" id="faq2">
                                                            @if(count(@$chapter->getLessons)>0)
                                                            @foreach(@$chapter->getLessons as $lesson)
                                                            <div class="card">
                                                               <div class="card-header" id="faqheadnew1">

                                                                  <a href="#" class="btn btn-header-link acco-chap acco-chap-1 collapsed" data-toggle="collapse" data-target="#faqnew1_{{@$lesson->id}}" aria-expanded="true" aria-controls="faqnew1_{{@$lesson->id}}" style="cursor: default;">

                                                                  <a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faqnew1_{{@$lesson->id}}" aria-expanded="true" aria-controls="faqnew1_{{@$lesson->id}}">

                                                                     <p>
                                                                        @if(@$lesson->lesson_type == 'V')
                                                                           <img src="{{ URL::to('public/frontend/images/video.png') }}">
                                                                        @elseif(@$lesson->lesson_type == 'Q')
                                                                           <img src="{{ URL::to('public/frontend/images/quiz1001.png') }}">
                                                                        @elseif(@$lesson->lesson_type == 'M')
                                                                           <img src="{{ URL::to('public/frontend/images/multimedia.png') }}">
                                                                        @elseif(@$lesson->lesson_type == 'T')
                                                                           <img src="{{ URL::to('public/frontend/images/text.png') }}">
                                                                        @elseif(@$lesson->lesson_type == 'A')
                                                                           <img src="{{ URL::to('public/frontend/images/audio.png') }}">
                                                                        @elseif(@$lesson->lesson_type == 'D')
                                                                           <img src="{{ URL::to('public/frontend/images/Download.png') }}">
                                                                        @elseif(@$lesson->lesson_type == 'P')
                                                                           <img src="{{ URL::to('public/frontend/images/presentaion.png') }}">
                                                                        @endif
                                                                        {{@$lesson->lesson_title}}
                                                                     </p>
                                                                     <span>{{@$lesson->duration}} mins</span>
                                                                  </a>
                                                               </div>
                                                               <div id="faqnew1_{{@$lesson->id}}" class="collapse" aria-labelledby="faqheadnew1" data-parent="#faq2">
                                                                  @php
                                                                     $height = 'height: 0px;';
                                                                     if(@$lesson->description == null){
                                                                        $height = 'height: 0px;';
                                                                     } else if(strlen(@$lesson->description)<102){
                                                                        $height = 'height: 43px;';
                                                                     } else if(strlen(@$lesson->description)<202){
                                                                        $height = 'height: 63px;';
                                                                     } else {
                                                                        $height = 'height: 98px;';
                                                                     }
                                                                  @endphp
                                                                  <div class="card-body detailed_descrptn" style="{{@$height}}">
                                                                     {!! @$lesson->description !!}
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            @endforeach
                                                            @else

                                                            @endif
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       @endforeach
                                       @endif
                                    </div>
                                 </div>
                              </div>
                              <div class="course_details">
                                 <h2>@lang('client_site.creator')</h2>
                                 <div class="inst_info">
                                    <h3>{{@$product->professional->name}}</h3>
                                    @php
                                    $vrr=0;
                                    @endphp
                                    @foreach(@$product->professional->userDetails as $us)
                                       @if($vrr>=2)
                                          @break
                                       @endif
                                       @if($us->level==0)
                                          <p>{{ @$us->categoryName->name }}
                                          @php
                                          $vrr++;
                                          @endphp
                                       @else
                                          <i class="fa fa-angle-right" aria-hidden="true"></i> {{ @$us->categoryName->name }}</p>
                                          @php
                                          $vrr++;
                                          @endphp
                                       @endif
                                    @endforeach
                                    <!-- <p>{{ @$product->professional->categoryName->name }} <i class="fa fa-angle-right" aria-hidden="true"></i> </p> -->
                                 </div>
                                 <div class="inst_img_sec">

                                       <span><img src="{{ @$product->professional->profile_pic ? URL::to('storage/app/public/uploads/profile_pic').'/'.@$product->professional->profile_pic : URL::to('public/frontend/images/no_img.png') }}"></span>

                                    <ul>
                                       <li><img src="{{ URL::to('public/frontend/images/star11.png') }}"> {{ @$averageRating }} @lang('client_site.creator_rating')</li>
                                       <li><img src="{{ URL::to('public/frontend/images/pr.png') }}"> {{ @$totalComments }} @lang('client_site.reviews')</li>
                                       <li><img src="{{ URL::to('public/frontend/images/us.png') }}"> {{ @$totalStudents }} @lang('client_site.students')</li>
                                       <li><img src="{{ URL::to('public/frontend/images/vv.png') }}"> {{ @$professionalProductsCount }} @lang('client_site.courses')</li>
                                    </ul>
                                 </div>
                                 <div class="ins_details">
                                    @if(strlen(@$product->professional->description)>200)

                                       <p id="short_desc" data-desc="{{substr(@$product->professional->description, 0, 200)}}">
                                          <!-- <a href="javascript:;" class="read_more"> @lang('site.read_more')</a> -->
                                       </p>
                                       <p id="full_desc" style="display:none;" data-desc="{{@$product->professional->description}}">
                                          <!-- <a href="javascript:;" class="read_more"> @lang('site.read_less')</a> -->
                                       </p>
                                    @else
                                       <p>{{@$product->professional->description}}</p>
                                    @endif
                                 </div>
                              </div>
                              <div class="course_details">
                                 <h2>@lang('client_site.student_feedback')</h2>
                                 <div class="review_box">
                                    <div class="review_left">
                                       <b>{{round(@$prod_avg_rating,1)}} <i class="fa fa-star"></i></b>
                                       <ul>
                                          <!-- <li><i class="fa fa-star"></i></li>
                                          <li><i class="fa fa-star"></i></li>
                                          <li><i class="fa fa-star"></i></li>
                                          <li><i class="fa fa-star"></i></li>
                                          <li><i class="fa fa-star-half-o"></i></li> -->
                                       </ul>
                                       <strong>@lang('client_site.course_rating')</strong>
                                    </div>
                                    @php
                                    $tot_review=count(@$reviews);
                                    if($tot_review>0){
                                        $per_five_star= round((count(@$reviews->where('rate', 5))/$tot_review)*100);
                                        $per_four_star = round((count(@$reviews->where('rate', 4))/$tot_review)*100);
                                        $per_three_star = round((count(@$reviews->where('rate', 3))/$tot_review)*100);
                                        $per_two_star = round((count(@$reviews->where('rate', 2))/$tot_review)*100);
                                        $per_one_star= round((count(@$reviews->where('rate', 1))/$tot_review)*100);
                                    }

                                    @endphp
                                    <div class="review_right">
                                       <ul>
                                          <li>
                                             <span>
                                                <div class="progress">
                                                   <div class="progress-bar" role="progressbar" style="width: {{round(@$per_five_star)}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                             </span>
                                             <ul class="star_ul">
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                             </ul>
                                             <b>{{ round(@$per_five_star) }}%</b>
                                          </li>
                                          <li>
                                             <span>
                                                <div class="progress">
                                                   <div class="progress-bar" role="progressbar" style="width: {{round(@$per_four_star)}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                             </span>
                                             <ul class="star_ul">
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                             </ul>
                                             <b>{{round(@$per_four_star)}}%</b>
                                          </li>
                                          <li>
                                             <span>
                                                <div class="progress">
                                                   <div class="progress-bar" role="progressbar" style="width: {{round(@$per_three_star)}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                             </span>
                                             <ul class="star_ul">
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                             </ul>
                                             <b>{{round(@$per_three_star)}}%</b>
                                          </li>
                                          <li>
                                             <span>
                                                <div class="progress">
                                                   <div class="progress-bar" role="progressbar" style="width: {{round(@$per_two_star)}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                             </span>
                                             <ul class="star_ul">
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                             </ul>
                                             <b>{{round(@$per_two_star)}}%</b>
                                          </li>
                                          <li>
                                             <span>
                                                <div class="progress">
                                                   <div class="progress-bar" role="progressbar" style="width: {{round(@$per_one_star)}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                             </span>
                                             <ul class="star_ul">
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                             </ul>
                                             <b>{{round(@$per_one_star)}}%</b>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                              <div class="course_details">
                                 <div class="re-prod">
                                 <h2>@lang('client_site.reviews')</h2>
                                 @if(@$is_purchased && @$completion > 0)
                                 <a href="{{route('rate.review.product',['slug'=> @$product->slug])}}" class="rt">@lang('client_site.rate_product')</a>
                                 @endif
                                 </div>
                                 <div class="sort_sec">

                                    <div class="search-section">
                                       <div class="input-group">
                                          <input type="text" class="form-control next-search" placeholder="{{__('client_site.search')}} ..." aria-label="" aria-describedby="basic-addon1" id="keyword">
                                          <div class="input-group-append">
                                             <button class="btn btn-success ser-img" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                                          </div>
                                       </div>
                                    </div>
                                    <select class="inst_selct">
                                       <option value=" ">@lang('client_site.search_by_ratings')</option>
                                       <option value="0">@lang('client_site.all_ratings')</option>
                                       <option value="5">@lang('client_site.five') @lang('client_site.stars')</option>
                                       <option value="4">@lang('client_site.four') @lang('client_site.stars')</option>
                                       <option value="3">@lang('client_site.three') @lang('client_site.stars')</option>
                                       <option value="2">@lang('client_site.two') @lang('client_site.stars')</option>
                                       <option value="1">@lang('client_site.one') @lang('client_site.stars')</option>
                                    </select>
                                 </div>
                                 <div class="show_reviews">
                                    @if(@$product->total_rating > 0)
                                       @foreach(@$reviews as $review)
                                          <div class="review_sec_con" id="review{{$review->id}}">
                                             <div class="rev_img">
                                                <span><img src="{{ @$review->getCustomer->profile_pic ? URL::to('storage/app/public/uploads/profile_pic').'/'.@$review->getCustomer->profile_pic : URL::to('public/frontend/images/no_img.png') }}" width="45 px" height="45 px"></span>

                                             </div>
                                             <div class="rev_profile">
                                                <h5>{{@$review->getCustomer->name}}</h5>
                                                <ul>
                                                   <li>{{(@$review->rate)}}.0</li>
                                                   <li><i class="fa fa-star"></i></li>

                                                   {{-- <li><i class="fa fa-star-half-o"></i></li> --}}
                                                   <?php
                                                      $today = date('W');
                                                      $review_date = date('W' ,strtotime($review->created_at));
                                                      $diff = $today - $review_date; ?>

                                                   <li><span> <?php echo $diff ?> semanas atrás</span></li>

                                                </ul>
                                                <div class="clearfix"></div>
                                                <p>{{@$review->review}}</p>
                                             </div>
                                          </div>
                                       @endforeach
                                    @else
                                    <!-- <div class="review_sec_con"> -->
                                       @lang('client_site.no_reviews_yet')
                                    <!-- </div> -->

                                    @endif
                                 </div>
                                 <div class="show_searched_reviews"></div>
                                 <!-- <div class="review_sec_con">
                                    <div class="rev_img">
                                       KR
                                    </div>
                                    <div class="rev_profile">
                                       <h5>Kesav Kumar R</h5>
                                       <ul>
                                          <li><i class="fa fa-star"></i></li>
                                          <li><i class="fa fa-star"></i></li>
                                          <li><i class="fa fa-star"></i></li>
                                          <li><i class="fa fa-star"></i></li>
                                          <li><i class="fa fa-star-half-o"></i></li>
                                          <li><span>3 weeks ago</span></li>
                                       </ul>
                                       <div class="clearfix"></div>
                                       <p>As an beginner this course greatly helped me.I certainly learnt all methods in Python and I have good confidence on solving problems in python now.I will recommend this course for students who wants to learn python for kickstart into their programming career.</p>
                                    </div>
                                 </div>
                                 <div class="review_sec_con">
                                    <div class="rev_img">
                                       KR
                                    </div>
                                    <div class="rev_profile">
                                       <h5>Kesav Kumar R</h5>
                                       <ul>
                                          <li><i class="fa fa-star"></i></li>
                                          <li><i class="fa fa-star"></i></li>
                                          <li><i class="fa fa-star"></i></li>
                                          <li><i class="fa fa-star"></i></li>
                                          <li><i class="fa fa-star-half-o"></i></li>
                                          <li><span>3 weeks ago</span></li>
                                       </ul>
                                       <div class="clearfix"></div>
                                       <p>As an beginner this course greatly helped me.I certainly learnt all methods in Python and I have good confidence on solving problems in python now.I will recommend this course for students who wants to learn python for kickstart into their programming career.</p>
                                    </div>
                                 </div> -->
                              </div>
                           </div>
                        </div>

                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- <footer class="footer-area">
            <div class="top-footer ftrshdw">
               <div class="container">
                  <div class="row">
                     <div class="fot-links">
                        <div class="row">
                           <div class="col-lg-4 col-md-6 col-sm-12">
                              <div class="fot-box">
                                 <h3>About Abc Group</h3>
                                 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris egestas sem tellus, ac consectetur mi gravida ac. Nunc sit amet ante vitae ante facilisis lobortis. Mauris tempor purus mauris, eu molestie</p>
                                 <a class="red_more" href="#">Read More <i class="fa fa-angle-double-right"></i></a>
                              </div>
                           </div>
                           <div class="col-lg-5 col-md-6 col-sm-12 mid-box">
                              <div class="fot-box">
                                 <h3>Quick Links</h3>
                                 <ul>
                                    <li><a href="index.html">Home</a></li>
                                    <li><a href="#">About us</a></li>
                                    <li><a href="#">Sign Up</a></li>
                                    <li><a href="#">Experts</a></li>
                                    <li><a href="#">FAQ</a></li>
                                    <li><a href="#">How it works</a></li>
                                    <li><a href="#">Become an Expert</a></li>
                                 </ul>
                                 <ul style="border:none;">
                                    <li><a href="#">Testimonials</a></li>
                                    <li><a href="#">Team</a></li>
                                    <li><a href="#">Terms of Service</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                    <li><a href="#">Jobs</a></li>
                                    <li><a href="#">Blog</a></li>
                                 </ul>
                              </div>
                           </div>
                           <div class="col-lg-3 col-md-12 col-sm-12">
                              <div class="fot-box" style="border:none;">
                                 <h3>Get In Touch</h3>
                                 <p>China Office:
                                    <br> Room 201, Block A , No. 1, Qianwan Road 1, Qianhaishen Port Cooperative District, Shenzhen, China.
                                 </p>
                                 <p>Phone : +1 9876543210 / +1 1234567890</p>
                                 <p>Email : companyname55@gmail.com </p>
                                 <ul class="fot-social">
                                    <li>
                                       <a href="#"><img src="images/social1.png" alt=""></a>
                                    </li>
                                    <li>
                                       <a href="#"><img src="images/social2.png" alt=""></a>
                                    </li>
                                    <li>
                                       <a href="#"><img src="images/social3.png" alt=""></a>
                                    </li>
                                    <li>
                                       <a href="#"><img src="images/social4.png" alt=""></a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="below-footer">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-12">
                        <p>© 2019 Consultation Marketplace All Right Reserved</p>
                     </div>
                  </div>
               </div>
            </div>
         </footer>
 -->
          <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal_product">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close btn_modal" data-dismiss="modal">&times;</button>

        </div>
        <div class="modal-body">
            <p>@lang('client_site.course_preview')</p>
            <h4>{{@$product->title}}</h4>

            <div class="video_les">
               <iframe id="video_show" width="560" height="315" src="" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            <p id="desc_title">@lang('client_site.description')</p>
            <span id="desc_text">{{@$my_lesson->description}}</span>
            @if(@$attachments)
            @if(count(@$attachments) > 0)
            <div class="smap_video">
               <h4>@lang('client_site.free_preview'):</h4>
            </div>
            @endif
            @endif
        </div>

      </div>

    </div>
  </div>
@endsection
@section('footer')
@include('includes.footer')

      </div>
      <!--wrapper end-->
      <!--price range (Calender/date picker)-->
      <!-- <script src="js/jquery.com_ui_1.11.4_jquery-ui.js"></script> -->

      <!--  -->

      <script>
         var attachments = {!! json_encode(@$attachments) !!};
         console.log(attachments);
         $(document).ready(function(){
            var html = "";
            html = `<div class="video-smaple_list" data-id="#" data-type="video_main">
               <div class="video_details main_vdo">
                  <div class="p-reative">
                     <video src="{{ URL::to('storage/app/public/lessons/type_video/'.@$lesson_video->filename)}}" @if(@$lesson_video_thumbnail) poster="{{ URL::to('storage/app/public/lessons/type_video/thumbnails/'.@$lesson_video_thumbnail->filename)}}" @endif ></video>
                  </div>
                  Main Video
               </div>
               <span>{{@$lesson_video->duration}}</span>
            </div>`;
            if(attachments.length > 0){
               $(attachments).each(function(i, attachment){
                  var text = dur = img = "";
                  if(attachment.mimetype.split('/')[0] == "audio"){
                     text = attachment.name != null ? attachment.name : "Audio";
                     img =
                     `<div class="p-reative">
                        <img src="{{URL::to('storage/app/public/uploads/product_cover_images/'.@$product->cover_image)}}">
                        <span class="over_audio"> <i class="fa fa-volume-up" aria-hidden="true"></i></span>
                     </div>`;
                     dur = attachment.duration != null ? attachment.duration : "";
                  } else if(attachment.mimetype.split('/')[0] == "image"){
                     text = attachment.name != null ? attachment.name : "Image";
                     img = `<img src="{{url('/')}}/storage/app/public/lessons/type_video/attachments/${attachment.filename}">`;
                     dur = attachment.duration != null ? attachment.duration : "";
                  } else if(attachment.mimetype.split('/')[0] == "video"){
                     text = attachment.name != null ? attachment.name : "Video";
                     img = `<img src="{{URL::to('storage/app/public/uploads/product_cover_images/'.@$product->cover_image)}}">`;
                     img = `<video src="{{url('/')}}/storage/app/public/lessons/type_video/attachments/${attachment.filename}" height="100%" width="100%",></video>`
                     dur = attachment.duration != null ? attachment.duration : "";
                  } else {
                     text = attachment.name != null ? attachment.name : "Attachment";
                     text = `<a class="text-white" href="{{url('/')}}/storage/app/public/lessons/type_video/attachments/${attachment.filename}" download="${attachment.name}">${text}</a>`;
                     img =
                     `<div class="p-reative">
                        <img src="{{URL::to('storage/app/public/uploads/product_cover_images/'.@$product->cover_image)}}">
                        <span class="over_audio"> <i class="fa fa-file" aria-hidden="true"></i></span>
                     </div>`;
                     dur = `<a class="text-white" href="{{url('/')}}/storage/app/public/lessons/type_video/attachments/${attachment.filename}" download="${attachment.name}"><i class="fa fa-download" aria-hidden="true"></i></a>`;
                  }
                  html +=
                  `<div class="video-smaple_list" data-id="${i}" data-type="${attachment.mimetype.split('/')[0]}">
                     <div class="video_details">
                        ${img}
                        ${text}
                     </div>
                     <span>${dur}</span>
                  </div>`;
               });
               $('.smap_video').append(html);
            }
         });
      </script>

      <script>
         $(function() {
             $("#slider-range").slider({
                 range: true,
                 min: 0,
                 max: 1000,
                 values: [75, 300],
                 slide: function(event, ui) {
                     $("#amount").val("$" + ui.values[0] + " - $." + ui.values[1]);
                 }
             });
             $("#amount").val("$" + $("#slider-range").slider("values", 0) + " - $" + $("#slider-range").slider("values", 1));
         });
      </script>
      <script type="text/javascript">
         // The function toggles more (hidden) text when the user clicks on "Read more". The IF ELSE statement ensures that the text 'read more' and 'read less' changes interchangeably when clicked on.
         $('.moreless-button').click(function() {
             $('.moretext').slideToggle();
             if($('.moreless-button').text() == " @lang('site.read_more')") {
                 $(this).text(" @lang('site.read_less')")
             } else {
                 $(this).text(" @lang('site.read_more')")
             }
         });
      </script>
      <script>
         $(document).delegate('.video-smaple_list', 'click', function(){
            var id = $(this).data('id');
            var type = $(this).data('type');
            console.log(id);
            console.log(type);
            if(type == "image") {
               var html = `<img src="{{url('/')}}/storage/app/public/lessons/type_video/attachments/${attachments[id].filename}" />`
               $('.video_les').empty();
               $('.video_les').append(html);
            } else if(type == "audio") {
               var html = `<audio controls loop autoplay src="{{url('/')}}/storage/app/public/lessons/type_video/attachments/${attachments[id].filename}"></audio>`
               $('.video_les').empty();
               $('.video_les').append(html);
            } else if(type == "video") {
               var html = `<video controls loop autoplay src="{{url('/')}}/storage/app/public/lessons/type_video/attachments/${attachments[id].filename}"></video>`
               $('.video_les').empty();
               $('.video_les').append(html);
            } else if(type == "video_main") {
               var html = `<video controls loop autoplay src="{{ URL::to('storage/app/public/lessons/type_video/'.@$lesson_video->filename)}}" @if(@$lesson_video_thumbnail) poster="{{ URL::to('storage/app/public/lessons/type_video/thumbnails/'.@$lesson_video_thumbnail->filename)}}" @endif ></video>`
               $('.video_les').empty();
               $('.video_les').append(html);
            }
         });
         $('#myModal').on('show.bs.modal', function (event) {
            var id = $(this).data('id');
            var reqData = {
            'jsonrpc' : '2.0',
            '_token' : '{{csrf_token()}}',
            'params' : {
                  'id' :  $('#video_link_id').val()
               }
            };
            $.ajax({
            url: "{{ route('product.preview.video') }}",
            method: 'post',
            dataType: 'json',
            data: reqData,
            success: function(response){
                if(response.status==1) {
                     console.log(response);
                    if(response.lesson_file_video){
                     if(response.lesson_video_type == 'V'){

                        var base_url = "{{url('/')}}";
                        var url = base_url+'/storage/app/public/lessons/type_video/'+response.lesson_file_video.filename;
                        $('#video_show').attr('src',url);

                     }else{
                        var youtube_url ='https://www.youtube.com/embed/';
                        var url = youtube_url+response.lesson_file_video.filename;
                        $('#video_show').attr('src',url);
                     }
                     if(response.lesson.description == null){
                        $('#desc_title').hide();
                        $('#desc_text').hide();
                     }
                     else{
                        $('#desc_title').show();
                        $('#desc_text').html(response.lesson.description);
                     }

                    }
                }

            }, error: function(error) {
                console.error(error);
            }
         });

      });
      </script>
      <script>
         $('#add_to_wishlist').click(function(){
            var product_id =$('#product_id').val();
            var reqData = {
            'jsonrpc' : '2.0',
            '_token' : '{{csrf_token()}}',
            'params' : {
                  'product_id' :  product_id
               }
            };
            $.ajax({
            url: "{{ route('add.to.wishlist') }}",
            method: 'post',
            dataType: 'json',
            data: reqData,
            success: function(response){
                if(response.status==1) {
                    toastr.success('@lang('client_site.successfully_added_to_wishlist')');
                    location.reload()
                }
                if(response.status==3){
                    localStorage.removeItem('slug');
                    localStorage.setItem('slug','{{ @$user->slug }}');
                    location.href = "{{ route('login') }}";
                }

                if(response.status==2){
                    toastr.info('@lang('client_site.you_have_already_added_to_your_wishlist')');
                }
                if(response.status==4){
                    toastr.info('@lang('client_site.you_cannot_wishlist_your_product')');
                }

            }, error: function(error) {
                console.error(error);
            }
        });

         })
      </script>
<script>
      // $("#short_desc").html($('#short_desc').data('desc'));

   var str1 = $('#short_desc').data('desc');
   $("#short_desc").html(str1 + "..." + `<a href="javascript:;" class="read_more"> @lang('site.read_more')</a>`);

   var str2 = $('#full_desc').data('desc');
   $("#full_desc").html(str2 + `<a href="javascript:;" class="read_more"> @lang('site.read_less')</a>`);

   // $('.read_more').click(function(){
   $("body").delegate(".read_more", "click", function(e) {
      if($('#short_desc').css('display') == "block"){
         $('#short_desc').hide();
         $('#full_desc').show();
      } else {
         $('#full_desc').hide();
         $('#short_desc').show();
      }
   });
</script>

<script>


   $('#remove_from_wishlist').click(function(){
            var product_id =$('#product_id').val();
            var reqData = {
            'jsonrpc' : '2.0',
            '_token' : '{{csrf_token()}}',
            'params' : {
                  'product_id' :  product_id
               }
            };
            $.ajax({
            url: "{{ route('remove.from.wishlist') }}",
            method: 'post',
            dataType: 'json',
            data: reqData,
            success: function(response){
                if(response.status==1) {
                    toastr.success('@lang('client_site.successfully_removed_from_wishlist')');
                    location.reload();
                }
                if(response.status==0){
                  toastr.error('@lang('client_site.something_went_be_wrong')');
                }
                if(response.status==3){
                    localStorage.removeItem('slug');
                    localStorage.setItem('slug','{{ @$user->slug }}');
                    location.href = "{{ route('login') }}";
                }

                if(response.status==2){
                    toastr.info('@lang('client_site.add_to_wishlist_first_to_remove')');
                }
            }, error: function(error) {
                console.error(error);
            }
        });

         })
</script>

<script>


   var str1 = $('#short_hiw').data('desc');
   $("#short_hiw").html(str1 + "..." + `<a href="javascript:;" class="read_more_hiw"> @lang('site.read_more')</a>`);

   var str2 = $('#full_hiw').data('desc');
   $("#full_hiw").html(str2 + `<a href="javascript:;" class="read_more_hiw"> @lang('site.read_less')</a>`);

   // $('.read_more').click(function(){
   $("body").delegate(".read_more_hiw", "click", function(e) {
      if($('#short_hiw').css('display') == "block"){
         $('#short_hiw').hide();
         $('#full_hiw').show();
      } else {
         $('#full_hiw').hide();
         $('#short_hiw').show();
      }
   });
</script>

<script>


   var str1 = $('#short_chlp').data('desc');
   $("#short_chlp").html(str1 + "..." + `<a href="javascript:;" class="read_more_chlp"> @lang('site.read_more')</a>`);

   var str2 = $('#full_chlp').data('desc');
   $("#full_chlp").html(str2 + `<a href="javascript:;" class="read_more_chlp"> @lang('site.read_less')</a>`);

   // $('.read_more').click(function(){
   $("body").delegate(".read_more_chlp", "click", function(e) {
      if($('#short_chlp').css('display') == "block"){
         $('#short_chlp').hide();
         $('#full_chlp').show();
      } else {
         $('#full_chlp').hide();
         $('#short_chlp').show();
      }
   });
</script>

<script>


   var str1 = $('#short_for_whom').data('desc');
   $("#short_for_whom").html(str1 + "..." + `<a href="javascript:;" class="read_more_for_whom"> @lang('site.read_more')</a>`);

   var str2 = $('#full_for_whom').data('desc');
   $("#full_for_whom").html(str2 + `<a href="javascript:;" class="read_more_for_whom"> @lang('site.read_less')</a>`);

   // $('.read_more').click(function(){
   $("body").delegate(".read_more_for_whom", "click", function(e) {
      if($('#short_for_whom').css('display') == "block"){
         $('#short_for_whom').hide();
         $('#full_for_whom').show();
      } else {
         $('#full_for_whom').hide();
         $('#short_for_whom').show();
      }
   });
</script>

<script>


   var str1 = $('#short_benefits').data('desc');
   $("#short_benefits").html(str1 + "..." + `<a href="javascript:;" class="read_more_benefits"> @lang('site.read_more')</a>`);

   var str2 = $('#full_benefits').data('desc');
   $("#full_benefits").html(str2 + `<a href="javascript:;" class="read_more_benefits"> @lang('site.read_less')</a>`);

   // $('.read_more').click(function(){
   $("body").delegate(".read_more_benefits", "click", function(e) {
      if($('#short_benefits').css('display') == "block"){
         $('#short_benefits').hide();
         $('#full_benefits').show();
      } else {
         $('#full_benefits').hide();
         $('#short_benefits').show();
      }
   });
</script>


<script>


   var str1 = $('#short_when_to_do').data('desc');
   $("#short_when_to_do").html(str1 + "..." + `<a href="javascript:;" class="read_more_when_to_do"> @lang('site.read_more')</a>`);

   var str2 = $('#full_when_to_do').data('desc');
   $("#full_when_to_do").html(str2 + `<a href="javascript:;" class="read_more_when_to_do"> @lang('site.read_less')</a>`);

   // $('.read_more').click(function(){
   $("body").delegate(".read_more_when_to_do", "click", function(e) {
      if($('#short_when_to_do').css('display') == "block"){
         $('#short_when_to_do').hide();
         $('#full_when_to_do').show();
      } else {
         $('#full_when_to_do').hide();
         $('#short_when_to_do').show();
      }
   });
</script>



<script>


   var str1 = $('#short_previous_preparation').data('desc');
   $("#short_previous_preparation").html(str1 + "..." + `<a href="javascript:;" class="read_more_previous_preparation"> @lang('site.read_more')</a>`);

   var str2 = $('#full_previous_preparation').data('desc');
   $("#full_previous_preparation").html(str2 + `<a href="javascript:;" class="read_more_previous_preparation"> @lang('site.read_less')</a>`);

   // $('.read_more').click(function(){
   $("body").delegate(".read_more_previous_preparation", "click", function(e) {
      if($('#short_previous_preparation').css('display') == "block"){
         $('#short_previous_preparation').hide();
         $('#full_previous_preparation').show();
      } else {
         $('#full_previous_preparation').hide();
         $('#short_previous_preparation').show();
      }
   });
   $('#exp_all_sec').click(function(){
      if($(this).hasClass('collapsed')){
         $('#exp_all_sec').removeClass('collapsed');
         $('#exp_all_sec').addClass('shown');
         $('#exp_all_sec').text("@lang('client_site.collapse_all_sections')");
         $('.chapter_acco').each(function(i){
            var btn = $(this);
            if($(this).hasClass('collapsed')){
               setTimeout(btn.trigger.bind(btn, "click"), i * 100);
            }
         });
      } else {
         $('#exp_all_sec').removeClass('shown');
         $('#exp_all_sec').addClass('collapsed');
         $('#exp_all_sec').text('Expand all sections');
         $('.chapter_acco').each(function(i){
            var btn = $(this);
            if(!$(this).hasClass('collapsed')){
               setTimeout(btn.trigger.bind(btn, "click"), i * 100);
            }
         });
      }
   })
</script>

<script>
    $(document).ready(function(){
    $('#addToCart').click(function(){
        var productId = $(this).data('product');
        console.log(productId)
        var reqData = {
         'jsonrpc': '2.0',
         '_token': '{{csrf_token()}}',
         'params': {
            productId: productId,
         }
      };
        $.ajax({
         url: '{{ route('product.add.to.cart') }}',
         type: 'post',
         dataType: 'json',
         data: reqData,
      })
      .done(function(response) {
         console.log(response);
            $('.cou_cart').text(response.result.cart.length);
            $('#addToCart').css('display','none');
            $('#goToCart').css('display','block');
      })
      .fail(function(error) {
         console.log("error", error);
      })
      .always(function() {
         console.log("complete");
      })
    })
    $('#buyNow').click(function(){
        var productId = $(this).data('product');
        console.log(productId)
        var reqData = {
         'jsonrpc': '2.0',
         '_token': '{{csrf_token()}}',
         'params': {
            productId: productId,
         }
      };
        $.ajax({
         url: '{{ route('product.add.to.cart') }}',
         type: 'post',
         dataType: 'json',
         data: reqData,
      })
      .done(function(response) {
            window.location.href = '{{route('product.order.store')}} ';
      })
      .fail(function(error) {
         console.log("error", error);
      })
      .always(function() {
         console.log("complete");
      })
    })
});
$('#myModal').on('hide.bs.modal', function () { //Change #myModal with your modal id
      $('audio').each(function(){
        this.pause(); // Stop playing
        this.currentTime = 0; // Reset time
      });
      $('video').each(function(){
        this.pause(); // Stop playing
        this.currentTime = 0; // Reset time
      });
})
</script>
<script>
   $('.ser-img').on('click',function(){
      var key = $('#keyword').val();
      var op_rate = $('.inst_selct').val();
      var product_id =$('#product_id').val();
      if(key == ''){

         $('.review_sec_con').show();

      }else{

         var reqData = {
            'jsonrpc' : '2.0',
            '_token' : '{{csrf_token()}}',
            'params' : {
                  'key' :  key,
                  'rating': op_rate,
                  'prod_id':product_id
               }
         };
         $.ajax({
            url: "{{ route('search.review') }}",
            method: 'post',
            dataType: 'json',
            data: reqData,
            success: function(response){
                if(response.status == 1){

                  $('.review_sec_con').hide();

                  if(response.count>0){

                     $(response.review).each(function(i, review){
                        $('#review'+review.id).show();
                     });

                     console.log(response.review);

                  }
                  else{


                  }

                }

            }, error: function(error) {
                console.error(error);
            }
         });

      }

   });
</script>
<script>

$(".inst_selct").change(function(){
   var rate_op = $('.inst_selct').val();
   var product_id = $('#product_id').val();
   if(rate_op == ' '){
      $('.review_sec_con').show();

   }else{
         var reqData = {
            'jsonrpc' : '2.0',
            '_token' : '{{csrf_token()}}',
            'params' : {
                  'rating': rate_op,
                  'prod_id':product_id
               }
         };
         $.ajax({
            url: "{{ route('search.rate') }}",
            method: 'post',
            dataType: 'json',
            data: reqData,
            success: function(response){
                if(response.status == 1){

                  $('.review_sec_con').hide();

                  if(response.count>0){

                     $(response.rate).each(function(i, rate){
                        $('#review'+rate.id).show();
                     });

                     console.log(response.rate);

                  }
                  else{


                  }

                }

            }, error: function(error) {
                console.error(error);
            }
         });
      }

});

$(".readMore").click(function() {
   var elem = $(this);
   var target = $(this).data('target');
   if ($(elem).text() == " @lang('site.read_more')") {
      $(elem).text(" @lang('site.read_less')");
      $('.'+target+'_dots').hide();
      $("#"+target).slideDown();
   } else {
      $(elem).text(" @lang('site.read_more')");
      $('.'+target+'_dots').show();
      $("#"+target).slideUp();
   }
});

</script>
@endsection
