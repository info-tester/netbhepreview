@extends('layouts.app')
@section('title', 'Product Details')
@section('style')
@include('includes.style')
<link href='{{ URL::to('public/frontend/packages/core/main.css') }}' rel='stylesheet' />
<link href='{{ URL::to('public/frontend/packages/daygrid/main.css') }}' rel='stylesheet' />
<link href='{{ URL::to('public/frontend/packages/timegrid/main.css') }}' rel='stylesheet' />
<link href='{{ URL::to('public/frontend/packages/list/main.css') }}' rel='stylesheet' />
@endsection
@section('scripts')
@include('includes.scripts')
<script src='{{ URL::to('public/frontend/packages/core/main.js') }}'></script>
<script src='{{ URL::to('public/frontend/packages/interaction/main.js') }}'></script>
<script src='{{ URL::to('public/frontend/packages/daygrid/main.js') }}'></script>
<script src='{{ URL::to('public/frontend/packages/timegrid/main.js') }}'></script>
<script src='{{ URL::to('public/frontend/packages/list/main.js') }}'></script>
<script src="{{ URL::to('public/frontend/js/moment.min.js') }}"></script>
<style type="text/css">
    .fc-available {
        background: #4f4f4e !important;
        color: #fff !important;
    }

    .fc-more-popover .fc-event-container {
        height: 167px;
        overflow-y: scroll;
    }

    .p-name {
        color: #090909;
        font-size: 16px;
        font-family: 'Roboto', sans-serif;
        display: inline-block;
        margin: 0px;
    }

    .disabled_link {
        pointer-events: none;
        cursor: not-allowed;
        opacity: 0.8;
    }
</style>
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')
<div class="login-body about-body">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-6 col-sm-12">
                <div class="pro-dl-image">
                    <img src="{{ URL::to('storage/app/public/uploads/product_cover_images').'/'.@$product->cover_image }}"
                        alt="">
                </div>
            </div>
            <div class="col-md-6 col-lg-6 col-sm-12">
                <div class="pro-dls">
                    <h2>{{ @$product->title }}</h2>
                    @if(@$product->intro_video)
                    <a href="javascript:;" id="introduction_video"><i class="fa fa-film"></i> {{__('site.introductory_video')}}</a>
                    @endif
                    <h4>{{ @$product->price }}</h4>
                    <a @if(@$product->professional->id == @$me->id)
                        href=""
                        class="pro-btns disabled_link"
                        @else
                        href="{{route('product.order.store',['product_id'=>@$product->id])}}"
                        class="pro-btns"
                        @endif
                        >{{__('site.buy_now')}}</a>

                    @if(strlen(@$product->description) > 150)
                    <p>
                        <span id="full_content">{{ @$product->description }}</span>
                        <span id="short_content">{{ substr(@$product->description, 0, 250 ) . '...'}}</span>
                        <a href="#" id="desc_more" onclick="showhidepara()" style="font-weight:bold;">Read More +</a>
                    </p>
                    <!-- <a class="pro-btns" href="#" id="desc_more" onclick="showhidepara()">Read More <i
                            class="fa fa-angle-right" aria-hidden="true"></i></a> -->
                    @else
                    <p><span id="full_content">{{ @$product->description }}</span></p>
                    @endif
                </div>
            </div>
            <div class="col-lg-12">
                <div class="seller-box">
                    <div class="seller-image">
                        <span>
                            <img src="{{ @$product->professional->profile_pic ? URL::to('storage/app/public/uploads/profile_pic').'/'.@$product->professional->profile_pic: URL::to('public/frontend/images/no_img.png') }}"
                                alt="">
                        </span>
                    </div>
                    <div class="seller-descriptions">
                        <h2>{{ @$product->professional->nick_name ? @$product->professional->nick_name : @$product->professional->name}}</h2>
                        <ul class="des-rating">
                            {!! getStars($product->professional->avg_review) !!}
                            <li>({{ $product->professional->total_review ?? 0 }})</li>
                        </ul>
                        <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit
                            anim id est laborum.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@if( @$product->how_it_works || @$product->can_help || @$product->for_whom || @$product->benefits ||
@$product->when_to_do || @$product->previous_preparation)
<div class="prfltab">
    <div class="container">
        <div class="row rwmrgn">
            <div class="lfttbvrsn" style="width: 100% !important;">
                <ul>
                    @if(@$product->how_it_works)
                    <li>
                        <a href="javascript:void(0);" id="top">
                            <span><img src="{{URL::to('public/frontend/images/p1.png')}}"></span>
                            <p>{{__('site.how_it_works')}}</p>
                        </a>
                    </li>
                    @endif
                    @if(@$product->can_help)
                    <li>
                        <a href="javascript:void(0);" id="top1">
                            <span><img src="{{URL::to('public/frontend/images/p2.png')}}"></span>
                            <p>{{__('site.how_it_can_help')}}</p>
                        </a>
                    </li>
                    @endif
                    @if(@$product->for_whom)
                    <li>
                        <a href="javascript:void(0);" id="top2">
                            <span><img src="{{URL::to('public/frontend/images/p3.png')}}"></span>
                            <p>{{__('site.for_whom')}}</p>
                        </a>
                    </li>
                    @endif
                    @if(@$product->benefits)
                    <li>
                        <a href="javascript:void(0);" id="top3">
                            <span><img src="{{URL::to('public/frontend/images/p4.png')}}"></span>
                            <p>{{__('site.benefits')}}</p>
                        </a>
                    </li>
                    @endif
                    @if(@$product->when_to_do)
                    <li>
                        <a href="javascript:void(0);" id="top4">
                            <span><img src="{{URL::to('public/frontend/images/p5.png')}}"></span>
                            <p>{{__('site.when_to_do_it')}}</p>
                        </a>
                    </li>
                    @endif
                    @if(@$product->previous_preparation)
                    <li>
                        <a href="javascript:void(0);" id="top5">
                            <span><img src="{{URL::to('public/frontend/images/p6.png')}}"></span>
                            <p>{{__('site.previous_preparation')}}</p>
                        </a>
                    </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="pblcprfl-bdy">
    <div class="container">
        <div class="row rwmrgn">
            @if(@$product->how_it_works)
            <div class="pbdy-box" id="dwn" >
                <h3>{{__('site.how_it_works')}}</h3>
                <div class="eductn-sec">
                    <p>{{@$product->how_it_works}}
                    </p>
                </div>
            </div>
            @endif
            @if(@$product->can_help)
            <div class="pbdy-box" id="dwn1" >
                <h3>{{__('site.how_it_can_help')}}</h3>
                <div class="eductn-sec">
                    <p>{{@$product->can_help}}
                    </p>
                </div>
            </div>
            @endif
            @if(@$product->for_whom)
            <div class="pbdy-box" id="dwn2">
                <h3>{{__('site.for_whom')}}</h3>
                <div class="eductn-sec">
                    <p>{{@$product->for_whom}}
                    </p>
                </div>
            </div>
            @endif
            @if(@$product->benefits)
            <div class="pbdy-box" id="dwn3" >
                <h3>{{__('site.benefits')}}</h3>
                <div class="eductn-sec">
                    <p>{{@$product->benefits}}
                    </p>
                </div>
            </div>
            @endif
            @if(@$product->when_to_do)
            <div class="pbdy-box" id="dwn4">
                <h3>{{__('site.when_to_do_it')}}</h3>
                <div class="eductn-sec">
                    <p>{{@$product->when_to_do}}
                    </p>
                </div>
            </div>
            @endif
            @if(@$product->previous_preparation)
            <div class="pbdy-box" id="dwn5" >
                <h3>{{__('site.previous_preparation')}}</h3>
                <div class="eductn-sec">
                    <p>{{@$product->previous_preparation}}</p>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endif
@if(count(@$suggested) > 0)
<section class="product-area">
    <div class="container">
        <div class="row">
            <div class="page-h2">
                <h2>{{__('site.similar_products')}}</h2>
                <!-- <p>{{ @$exp3->description }}</p> -->
            </div>
            <div class="all-product">
                <div id="owl-demo-6" class="owl-carousel owl-theme">
                    @foreach(@$suggested as $pro)
                    @if($pro->id !== $product->id)
                    <div class="item">
                        <div class="product-box">
                            <div class="product-cover-image image_resize">
                                <a href="{{route('product.details',@$pro->slug)}}">
                                    <img src="{{ URL::to('storage/app/public/uploads/product_cover_images').'/'.@$pro->cover_image }}">
                                </a>
                            </div>
                            <div class="product-dtls">
                                <div class="product-intro">
                                    <a href="{{route('product.details',@$pro->slug)}}">
                                        {{-- <h3>
                                            {{ @$pro->title }}
                                        </h3> --}}
                                        <h3>
                                            @if(strlen(@$pro->title) < 50)
                                            {{ @$pro->title }}
                                            @else
                                            {{substr(@$pro->title, 0, 47 ) . '...'}}
                                            @endif
                                        </h3>
                                    </a>
                                    <p>{{ @$pro->category->category_name }}</p>
                                </div>
                                @if(@$pro->description)
                                <div class="product-more more_det">
                                    <p class="pr-desc" data-desc="{{$pro->description}}"></p>
                                </div>
                                @endif
                                <div class="price-and-more">
                                    @if(@$pro->price)
                                    <div class="price-lefts">
                                        <span>@lang('client_site.fees')</span>
                                        <p>{{@$pro->price}}</p>
                                    </div>
                                    @endif
                                    <a class="product-btn pull-right pull-right-1" href="{{route('product.details',@$pro->slug)}}">@lang('client_site.view_more')</a>
                                    <a class="product-btn pull-right" href="{{route('product.order.store',['product_id'=>@$pro->id])}}">{{__('site.buy_now')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endif
<!-- Return to Top -->
<a href="javascript:" id="return-to-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
@if(@$product->intro_video)
<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body" style="padding: 1rem;">
                {{-- <iframe frameborder="0" height="315" id="ytplayer" type="text/html" width="100%"
                    src="https://www.youtube.com/embed/{{$product->intro_video}}?rel=0&amp;autoplay=1&amp;modestbranding=1"></iframe> --}}
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
@endif
@endsection
@section('footer')
@include('includes.footer')
<script>
    //     $(document).ready(function(){
// $('#myModal').modal('show');
//     })
$('#introduction_video').click(function(){
    $('#myModal .modal-body').html(`<iframe frameborder="0" height="315" id="ytplayer" type="text/html" width="100%"
        src="https://www.youtube.com/embed/{{$product->intro_video}}?rel=0&amp;autoplay=1&amp;modestbranding=1"></iframe>`);
$('#myModal').modal('show');
})
$("#myModal").on('hidden.bs.modal', function (e) {
console.log('close');
$('#myModal .modal-body').empty();
});
    $(document).ready(function(){
        $('#full_content').hide();
    });
    var limit = 100;
    var productCount = "{{count(@$suggested)}}";
    function decodeEntities(encodedString) {
        var div = document.createElement('div');
        div.innerHTML = encodedString;
        return div.textContent;
    }
    $('#pr-desc').html($('#pr-desc').attr('data-desc'));
    $('.pr-desc').each(function(){
        var str = decodeEntities($(this).attr('data-desc'));
        if(str.length > limit){
            str = str.substring(0,limit) + "...";
        }
        $(this).text(str);
    });
    function showhidepara(){
        if($('#full_content').css('display') == 'none'){
            $('#desc_more').html("Read Less -");
            $('#full_content').show();
            $('#short_content').hide();
        } else {
            $('#desc_more').html("Read More +");
            $('#full_content').hide();
            $('#short_content').show();
        }
    }
</script>
<!-- <script>
    $("#top").click(function() {
        // $('.pbdy-box').css('display', 'none');
        // $('#dwn').css('display', 'block');
        $("html, body").animate({ scrollTop: $('#dwn1').offset().top }, "slow");
        return false;
    });
    $("#top1").click(function() {
        // $('.pbdy-box').css('display', 'none');
        // $('#dwn1').css('display', 'block');
        $("html, body").animate({ scrollTop: $('#dwn1').offset().top }, "slow");
        return false;
    });
    $("#top2").click(function() {
        // $('.pbdy-box').css('display', 'none');
        // $('#dwn2').css('display', 'block');
        $("html, body").animate({ scrollTop: $('#dwn2').offset().top }, "slow");
        return false;
    });
    $("#top3").click(function() {
        // $('.pbdy-box').css('display', 'none');
        // $('#dwn3').css('display', 'block');
        $("html, body").animate({ scrollTop: $('#dwn3').offset().top }, "slow");
        return false;
    });
    $("#top4").click(function() {
        // $('.pbdy-box').css('display', 'none');
        // $('#dwn4').css('display', 'block');
        $("html, body").animate({ scrollTop: $('#dwn4').offset().top }, "slow");
        return false;
    });
    $("#top5").click(function() {
        // $('.pbdy-box').css('display', 'none');
        // $('#dwn5').css('display', 'block');
        $("html, body").animate({ scrollTop: $('#dwn5').offset().top }, "slow");
        return false;
    });
</script> -->
@endsection
