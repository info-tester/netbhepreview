@extends('layouts.app')
@section('title')
    @lang('site.welcome_to_dashboard')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
<link href="{{ URL::to('public/frontend/css/course_style.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ URL::to('public/frontend/css/course_responsive.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ URL::to('public/frontend/css/course_bootstrap.min') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::to('public/frontend/icofont/icofont.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::to('public/frontend/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />
<style>
.error{
	color:red;
}
.success{
	color:forestgreen;
}
.modal-dialog {
  height: 100vh !important;
  display: flex;
}

.modal-content {
  margin: auto !important;
  height: fit-content !important;
}
.types__link p{
    font-size: 10px !important;
}
</style>
@endsection
@section('content')
	<section class="wrapper2">
		@section('header')
		@include('includes.professional_header')
		@endsection
		<!-- section-header.// -->
		<!-- ========================= SECTION CONTENT ========================= -->
		<nav class="navbar navbar-expand-lg navbar-dark sticky-top rm_chaptr01">
			<div class="w-100">
				<!-- <div class="top-bar">
					<div class="top-bar-left-container">
						<a href="#url" class="top-bar-button"> <i class="icofont-close-line"></i> </a>
						<div class="d-none d-md-flex align-items-center">
							<h4 class="ml-4">Courses</h4> </div>
					</div>
					<div class="top-bar-center-container"> <a href="#url" class="user_llk">Your First Course <i class="icofont-caret-down"></i></a>
						<div class="show01 search-category" style="display: none;">
							<div class="select2-search">
								<input type="text" name=""> </div>
							<ul class="select2-results">
								<li class="select2-no-results">No matches found</li>
							</ul>
						</div>
					</div>
					<div class="top-bar-right-container"> <a href="#url" class="topbar-right-btn">
						build landing page <i class="icofont-long-arrow-right"></i>
					</a> </div>
				</div> -->
				<div class="navbar-menu">
					<div class="container-fluid">
						<div class="all-menu-sec">
							<!---<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav"> <span class="navbar-toggler-icon"></span> </button> --->
							<div class="rm_chaptr02" id="main_nav">
								<ul class="navbar-nav">
									<li class="nav-item active show_course_view"> <a class="nav-link" href="javascript:;">   Plataforma </a> </li>
									<!-- <li class="nav-item"><a class="nav-link" href="#">                 Settings </a></li> -->
								</ul>
							</div>

						</div>
						<!-- navbar-collapse.// -->
					</div>
				</div>
				<!-- container //  -->
			</div>
		</nav>


        <div class="navbar__actions rm_chaptr03">
								<button class="preview-btn" title="@lang('client_site.preview_as_enrolled_student')" onclick=" window.open(`{{route('preview.lesson',@$product->id)}}`)"> <i class="icofont-eye-alt"></i> Visualizar curso como aluno </button>
							</div>


		<div class="page_builder ">
			<div class="course_view mobile_chapter_list rm_chaptr05">
				<div class="list-view-sessoin">
					<div>
						<div id="main">
							<div class="accordion_1" id="faq">
								@if(count(@$chapters)>0)
									@foreach($chapters as $ck=>$chapter)
										<div class="card">
											<div class="card-header chapter_header" id="faqhead{{$ck+1}}" data-id="{{$chapter->id}}">
												<a href="javascript:;" class="btn btn-header-link acco-chap collapsed"
													data-toggle="collapse"
													data-target="#faq{{$ck+1}}"
													aria-expanded="true"
													aria-controls="faq{{$ck+1}}"
													data-id="{{$chapter->id}}"
													data-title="{{$chapter->chapter_title}}"
												>
													<img src="{{ URL::to('public/frontend/images/cha01.png') }}">
													<p>{{$chapter->chapter_title}}</p>
												</a>
											</div>
											<div id="faq{{$ck+1}}" class="collapse" aria-labelledby="faqhead{{$ck+1}}" data-parent="#faq">
												<div class="card-body">
													<div class="session-content">
															<ul class="session-box">
																<li>
																<div class="session-btn" id="section_chapter_{{$chapter->id}}">
																	<button class="button add-lesson"><i class="icofont-plus"></i> @lang('client_site.add_lesson')</button>
																</div>
																<div class="add-lesson-info" style="display: none;">
																	<span class="types__item" data-ltype="V" data-chapter="{{$chapter->id}}">
																		<a href="javascript:;" class="types__link">
																			<img src="{{ URL::to('public/frontend/images/video.png') }}">
																			<p>@lang('client_site.video')</p>
																		</a>
																	</span>
																	<span class="types__item" data-ltype="Q" data-chapter="{{$chapter->id}}">
																		<a href="javascript:;" class="types__link">
																			<img src="{{ URL::to('public/frontend/images/quiz.png') }}">
																			<p>@lang('client_site.quiz')</p>
																		</a>
																	</span>
																	<span class="types__item" data-ltype="M" data-chapter="{{$chapter->id}}">
																		<a href="javascript:;" class="types__link">
																			<img src="{{ URL::to('public/frontend/images/multimedia.png') }}">
																			<p>@lang('client_site.multimedia')</p>
																		</a>
																	</span>
																	<span class="types__item" data-ltype="T" data-chapter="{{$chapter->id}}">
																		<a href="javascript:;" class="types__link">
																			<img src="{{ URL::to('public/frontend/images/text.png') }}">
																			<p>@lang('client_site.text')</p>
																		</a>
																	</span>
																	<!-- <span class="types__item" data-ltype="P" data-chapter="{{$chapter->id}}">
																		<a href="javascript:;" class="types__link">
																			<img src="{{ URL::to('public/frontend/images/pd.png') }}">
																			<p>Pdf</p>
																		</a>
																	</span> -->
																	<span class="types__item" data-ltype="A" data-chapter="{{$chapter->id}}">
																		<a href="javascript:;" class="types__link">
																			<img src="{{ URL::to('public/frontend/images/audio.png') }}">
																			<p>@lang('client_site.audio')</p>
																		</a>
																	</span>
																	<span class="types__item" data-ltype="D" data-chapter="{{$chapter->id}}">
																		<a href="javascript:;" class="types__link">
																			<img src="{{ URL::to('public/frontend/images/Download.png') }}">
																			<p>@lang('client_site.download')</p>
																		</a>
																	</span>
																	<span class="types__item" data-ltype="P" data-chapter="{{$chapter->id}}">
																		<a href="javascript:;" class="types__link">
																			<img src="{{ URL::to('public/frontend/images/presentaion.png') }}">
																			<p>@lang('client_site.presentation')</p>
																		</a>
																	</span>
																</div>
															</li>

													</div>
												</div>
											</div>
										</div>
									@endforeach
								@else
								@endif
							</div>
						</div>
						<div class="tree-tip">
							<div class="tip__title"> <img src="{{ URL::to('public/frontend/images/tip.png') }}"> @lang('client_site.pro_tip') </div>
							<!-- <p class="tip__description">You can customize the course completion experience with a certificate or a custom completion page!</p> <a href="#url">Course completion settings</a> </div> -->
							<p class="tip__description">@lang('client_site.chapter_section_description')</p>
                        <a href="{{route('list.landing.page.templates')}}">@lang('client_site.landing_page_free_link')</a></div>
					</div>
					<div></div>
				</div>
				<div class="builder__footer">
					<div class="tree_action">
						<div class="builder-add-chapter">
							<a href="javascript:;" class="button button--primary w-100" id="add_chapter"> @lang('client_site.add_chapter') </a>
						</div>
					</div>
				</div>
			</div>
			<div class="builder_section rm_chaptr04">
				<div class="wrap wrap-edit">
					<div class="ember-view">
						@if(count(@$chapters) == 0)
							<div class="admin-build-section right-section">
								<div class="empty-state">
									<div class="toga-product-icon">
										<i class="icofont-file-text"></i>
									</div>
									<h2 class="empty-state__title">@lang('client_site.edit_chapter_welcome_text')</h2>
									<p class="empty-state__description mb-3">
										@lang('client_site.curriculam_tab_instructions')
									</p>
									<!-- <div class="empty-state__reference-link mt-4">
										<small>Want to add a bunch of content at once? Try our bulk importer</small>
									</div> -->
								</div>
							</div>
						@else
							<div class="admin-build-section right-section start_section">
								<div class="empty-state">
									<div class="toga-product-icon">
										<i class="icofont-file-text"></i>
									</div>
									<h2 class="empty-state__title">@lang('client_site.get_started_text')</h2>
									<p class="empty-state__description mb-3">
										@lang('client_site.get_started_desc')
									</p>
									<div class="empty-state__reference-link mt-4">
									<!-- <small>Ready to launch?
										<a target="_blank" href="#">
											Read our Marketing &amp; Launch Guide
										</a>
									</small> -->
									</div>
								</div>
							</div>
						@endif
						<div class="admin-build-section lessons_section right-section" style="display:none">
							<div class="builder-bar mb-3 mb-lg-4">
								<div class="clearfix mobile--show">
									<div class="pull-right action-buttons d-inline-flex align-items-center">
										<div class="custom-control custom-checkbox mr-2">
											<input type="checkbox" id="chk5" name="radios" class="radio-box" value="all">
											<label for="chk5">@lang('client_site.draft')</label>
										</div>
										<div class="tools-tip" data-toggle="tooltip" data-placement="top" title="@lang('site.draft_tooltip')"><i class="icofont-info-circle"></i></div>
										<button class="button button--secondary content-action-bar__discard-btn" data-ember-action="1560"> @lang('client_site.discard_changes') </button>
										<button class="button button--primary" data-ember-action="1561"> @lang('client_site.save') </button>
									</div>
								</div>
								<div class="mobile-hide">
									<div class="pull-right action-buttons d-inline-flex align-items-center">
										<div class="custom-control custom-checkbox mr-2">
											<input type="checkbox" id="chk6" name="radios" class="radio-box" value="all">
											<label for="chk6">@lang('client_site.draft')</label>
										</div>
										<div class="tools-tip" data-toggle="tooltip" data-placement="top" title="@lang('site.draft_tooltip')"><i class="icofont-info-circle"></i></div>
										<button type="button" class="button button--secondary content-action-bar__discard-btn discard_lesson" data-ember-action="1560"> @lang('client_site.discard_changes') </button>
										<button type="button" class="button button--primary save_lesson_btn" data-ember-action="1561"> @lang('client_site.save') </button>
									</div>
								</div>
								<div class="builder_header-container">
									<div class="builder__header mt-3 mt-lg-0" id="lsn_title"> @lang('client_site.new_lesson') </div>
								</div>
							</div>
							<div class="content__sections">
								<form action="{{route('save.lesson')}}" method="post" enctype="multipart/form-data" id="save_lesson_form">
									@csrf
									<input type="text" name="course_id" value="{{@$product->id}}" hidden>
									<input type="text" name="chapter_id" id="chapter_id" value="" hidden>
									<input type="text" name="lesson_id" id="lesson_id" value="" hidden>
									<input type="text" name="lesson_type" id="lesson_type" value="V" hidden>
									<input type="text" id="file_duration" hidden>

									<!-- for video -->
									<div class="course-content-grow lesson_section" id="for_V">
										<div class="form-row">
											<div class="form-group col-sm-12 ">
												<label class="label">@lang('site.title')</label>
												<input class="ember-view lesson_title form-control" maxlength="255" size="50" type="text" name="lesson_title_V" id="lesson_title_V" data-ltype="V">
												<div id="err_lesson_title"></div>
											</div>
										</div>
										<div class="form-row">
											<div class="form-group col-sm-12 ">
												<label class="label">@lang('client_site.lesson_duration_in_minutes')</label>
												<input class="ember-view form-control duration" maxlength="255" size="50" type="text" placeholder="@lang('client_site.lesson_duration_in_minutes')" name="duration_V" id="duration_V" data-ltype="V">
											</div>
										</div>
										<div class="form-row" id="upload_vdo_section">
											<div class="form-group col-sm-12">
												<label class="label">@lang('client_site.upload_file')</label>
												<div class="dropbx drop-zone clearfix show">
													<div class="drop-zone-header p-4">
														<div class="drop-zone__title">@lang('client_site.drag_drop_video_file')
															<br> <strong>@lang('client_site.or')</strong>
														</div>
														<label class="button button--primary manual-upload mt-2" for="video-attch2"> @lang('client_site.upload_file') </label>
														<input type="file" class="manual-file-select" id="video-attch2" name="lesson_main_video">
														<div class="video_msg text-center"></div>
													</div>
													<div class="drop-zone__details p-3"> <small>@lang('client_site.video_upload_instruction')</small>
														<br>
														<!---->
													</div>
												</div>
											</div>
                                            <div class="form-group col-sm-12">
												<h4 class="text-center m-0">@lang('client_site.or')</h4>
											</div>
											<div class="form-group col-sm-12">
												<label class="label">Subir arquivo de vídeo em Vimeo</label>
												<div class="dropbx_vimeo drop-zone clearfix show">
													<div class="drop-zone-header p-4">
														<div class="drop-zone__title">@lang('client_site.drag_drop_video_file')
															<br> <strong>@lang('client_site.or')</strong>
														</div>
														<label class="button button--primary manual-upload mt-2" for="video-attch3"> @lang('client_site.upload_file') </label>
														<input type="file" class="manual-file-select" id="video-attch3" name="lesson_main_video">
														<div class="video_msg1 text-center"></div>
													</div>
													<div class="drop-zone__details p-3"> <small>@lang('client_site.video_upload_instruction')</small>
														<br>
														<!---->
													</div>
												</div>
											</div>
											<div class="form-group col-sm-12">
												<h4 class="text-center m-0">@lang('client_site.or')</h4>
											</div>
											<div class="form-group col-sm-12">
												<label class="label">@lang('client_site.enter_youtube_url') ( Compartilhar é possível)</label>
												<input class="ember-view form-control" maxlength="255" size="50" type="text" name="video_url" id="video_url">
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="form-row" id="progress_list_video">
											<div class="form-group col-sm-12 ">
												<label class="label">@lang('client_site.upload_file')</label>
												<ul class="progress-ul-video">
												</ul>
											</div>
										</div>
										<!-- <div class="mt-2"> <small>
											It’s all about the details. Pick your thumbnail image, add closed captions, update settings, and track your video performance analytics in the video library. <a href="#" target="_blank">Manage video settings</a>. <a target="_blank" href="#">Learn more about the video library</a>
										</small> </div> -->
										<div class="form-row"></div>
										<div class="mt-4" style="display:none;" id="upload_thumbnail_section">
											<div id="main2">
												<label class="label">@lang('client_site.upload_thumbnail')</label>
												<br>
												<label class="button button--primary manual-upload mt-2" for="thumbnail"> @lang('client_site.upload_file') </label>
												<input type="file" class="manual-file-select" id="thumbnail" name="thumbnail" accept="image/*">
												<img src="" alt="" style="display:none;" id="thumb" width="200px">
											</div>
										</div>
										<div class=" mt-4">
											<div id="main2">
												<div class="accordion" id="faqnew">
													<div class="card">
														<div class="card-header" id="faqheadnew1"> <a href="#" class=" btn-header-link" data-toggle="collapse" data-target="#faqnews" aria-expanded="true" aria-controls="faqnews">@lang('client_site.add_text') <span class="extra-class"> Optional </span></a> </div>
														<div id="faqnews" class="collapse show" aria-labelledby="faqheadnew1" data-parent="#faqnew">
															<div class="card-body">
																<textarea class="video_description" name="video_description" id="video_description"></textarea>
															</div>
														</div>
													</div>
													<div class="card">
														<div class="card-header" id="faqheadnew2"> <a href="#" class=" btn-header-link collapsed" data-toggle="collapse" data-target="#faqnews2" aria-expanded="true" aria-controls="faqnews2">@lang('client_site.add_downloads') <span class="extra-class"> Optional </span></a> </div>
														<div id="faqnews2" class="collapse show" aria-labelledby="faqheadnew2" data-parent="#faqnew">
															<div class="card-body">
																<div class="form-row mt-3">
																	<div class="form-group col-sm-12">
																		<div class="video_attachment drop-zone clearfix show">
																			<div class="drop-zone-header p-4">
																				<div class="drop-zone__title">@lang('client_site.drag_drop_video_file')
																					<br> <strong>@lang('client_site.or')</strong>
																				</div>
																				<label class="button button--primary manual-upload mt-2" for="video_attachment"> @lang('client_site.upload_file') </label>
																				<input type="file" class="manual-file-select" id="video_attachment" name="video_attachment[]" multiple>
																				<div class="video_attachment_msg text-center"></div>
																			</div>
																			<div class="drop-zone__details p-3"> <small>@lang('client_site.any_file_instruction')</small>
																				<br>
																				<!---->
																			</div>
																		</div>
																		<!--  -->
																	</div>
																</div>
																<div class="form-row"></div>
																<div class="form-row" id="progress_list_video_atch">
																	<div class="form-group col-sm-12 ">
																		<label class="label">@lang('client_site.upload_file')</label>
																		<ul class="progress-ul-video-atch">
																		</ul>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- for video -->

									<!-- for quiz -->
									<div class="lesson_section" id="for_Q">
										<div class="course-content-grow">
											<div class="form-row">
												<div class="form-group col-sm-12 ">
													<input type="text" name="quiz_count" id="quiz_count" value="1" hidden>
													<label class="label">@lang('site.title')</label>
													<input class="ember-view lesson_title form-control" maxlength="255" size="50" type="text" name="lesson_title_Q" id="lesson_title_Q" data-ltype="Q">
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-sm-12 ">
													<label class="label">@lang('client_site.lesson_duration_in_minutes')</label>
													<input class="ember-view form-control duration" maxlength="255" size="50" type="text" placeholder="@lang('client_site.lesson_duration_in_minutes')" name="duration_Q" id="duration_Q" data-ltype="Q">
												</div>
											</div>
										</div>
										<div class="course-content-grow quiz_question" data-id="1">
											<div id="main3">
												<div class="accordion3" id="faqnew2">
													<div class="card">
														<div class="card-header" id="faqnewhead1">
															<a href="#" class=" btn-header-link" data-toggle="collapse" data-target="#qhead1" aria-expanded="true" aria-controls="qhead1">
																<h5>@lang('client_site.question') 1 <sup style="display:none;"><i class="fa fa-circle" aria-hidden="true"></i></sup></h5>
																<div class="d-flex align-items-center">
																	<div class="button-group">
																		<span class="user_llllk new-link3 button del_quiz" id="del_quiz1" data-count="1">
																			<i class="fa fa-trash-o" aria-hidden="true"></i>
																		</span>
																	</div>
																</div>
															</a>
														</div>
														<div id="qhead1" class="collapse" aria-labelledby="faqnewhead1" data-parent="#faqnew2">
															<div class="card-body">
																<div class="clearfix mt-3"></div>
																<div id="err_correct1"></div>
																<div class="clearfix mt-2"></div>
																<label>@lang('client_site.question')</label>
																<textarea class="question1" id="question1" name="question1"></textarea>

																<div class="accordion4 mt-3" id="faqnew3">
																	@for($i=1; $i<=6; $i++)
																	<div class="clearfix mt-3"></div>
																	<div class="card">
																		<div class="card-header" id="faqheadnew">
																			<a href="javascript:;" class=" btn-header-link" data-toggle="collapse" data-target="#choice1_{{$i}}" aria-expanded="true" aria-controls="choice1_{{$i}}">Alternativa {{$i}}</a>
																		</div>
																		<div id="choice1_{{$i}}" class="collapse {{ $i<=2 ? 'show' : ''}}" aria-labelledby="faqheadnew" data-parent="#faqnew">
																			<div class="card-body">
																				<textarea class="answer1_{{$i}}" id="answer1_{{$i}}" name="answer1_{{$i}}"></textarea>
																				<div class="clearfix"></div>
																				<div class="dash-d das-radio">
																					<input type="radio" name="correct1" id="correct1_{{$i}}" class="correct1 inp_radio" value="{{$i}}">
																					<label for="correct1_{{$i}}">@lang('client_site.this_is_correct_answer')</label>
																				</div>
																			</div>
																		</div>
																	</div>
																	@endfor
																</div>

																<div class="clearfix mt-3"></div>
																<label>@lang('client_site.explanation')</label>
																<textarea class="explanation1" id="explanation1" name="explanation1"></textarea>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="course-content-grow" id="quiz_btns">
											<div class="mb-2">
												<button type="button" class="button button--primary" id="add_question"><i class="icofont-plus"></i> @lang('client_site.add_question')</button>
												<button type="button" class="button button--secondary" id="import_questions" data-toggle="modal" data-target="#exampleModal"><i class="icofont-plus"></i> @lang('client_site.import_questions')</button>
											</div>
										</div>
									</div>
									<!-- for quiz -->

									<!-- for multimedia -->
									<div class="lesson_section" id="for_M">
										<div class="course-content-grow">
											<div class="form-row">
												<div class="form-group col-sm-12 ">
													<label class="label">@lang('site.title')</label>
													<input class="ember-view lesson_title form-control" maxlength="255" size="50" type="text" name="lesson_title_M" id="lesson_title_M" data-ltype="M">
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-sm-12 ">
													<label class="label">@lang('client_site.lesson_duration_in_minutes')</label>
													<input class="ember-view form-control duration" maxlength="255" size="50" type="text" placeholder="@lang('client_site.lesson_duration_in_minutes')" name="duration_M" id="duration_M" data-ltype="M">
												</div>
											</div>
											<div class="section--gray">
												<div class="col-sm-12 ">
													<div class="form-row">
														<label class="label" for="source_url">
															@lang('client_site.multimedia_url_label')
														</label>
														<input type="url" name="source_url" id="source_url" class="form-control" placeholder="ex: https://docs.google.com/document/d/xxxx">
														<!-- <div class="mt-2">
															<p class="tip__description">
															<small>
																Dynamic variables can be added to your URL to automatically populate relevant fields in your content.
																<br>
																<a target="_blank" href="">Learn more</a> about how they can be used in your Multimedia Lesson.
															</small>
															</p>
															<p class="tip__description">
															<small>
																<span class="font-weight-semi-bold">Available Variables:</span> love
															</small>
															</p>
														</div> -->
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- for multimedia -->

									<!-- for text -->
									<div class="lesson_section" id="for_T">
										<div class="course-content-grow">
											<div class="form-row">
												<div class="form-group col-sm-12 ">
													<label class="label">@lang('site.title')</label>
													<input class="ember-view lesson_title form-control" maxlength="255" size="50" type="text" name="lesson_title_T" id="lesson_title_T" data-ltype="T">
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-sm-12 ">
													<label class="label">@lang('client_site.lesson_duration_in_minutes')</label>
													<input class="ember-view form-control duration" maxlength="255" size="50" type="text" placeholder="@lang('client_site.lesson_duration_in_minutes')" name="duration_T" id="duration_T" data-ltype="T">
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-sm-12 ">
													<label class="label">@lang("site.content")</label>
													<textarea class="description" name="description" id="description"></textarea>
													<div id="err_description"></div>
												</div>
											</div>
										</div>
									</div>
									<!-- for text -->

									<!-- for audio -->
									<div class="lesson_section" id="for_A">
										<div class="course-content-grow">
											<div class="form-row">
												<div class="form-group col-sm-12 ">
													<label class="label">@lang('site.title')</label>
													<input class="ember-view lesson_title form-control" maxlength="255" size="50" type="text" name="lesson_title_A" id="lesson_title_A" data-ltype="A">
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-sm-12 ">
													<label class="label">@lang('client_site.lesson_duration_in_minutes')</label>
													<input class="ember-view form-control duration" maxlength="255" size="50" type="text" placeholder="@lang('client_site.lesson_duration_in_minutes')" name="duration_A" id="duration_A" data-ltype="A">
												</div>
											</div>
											<div class="form-row" id="upload_ado_section" style="display:none;">
												<div class="form-group col-sm-12">
													<label class="label">@lang('client_site.upload_file')</label>
													<div class="audio_dropbx drop-zone clearfix show">
														<div class="drop-zone-header p-4">
															<div class="drop-zone__title">@lang('client_site.drag_drop_audio_file')
																<br> <strong>@lang('client_site.or')</strong>
															</div>
															<label class="button button--primary manual-upload mt-2" for="audio-attch2"> @lang('client_site.upload_file') </label>
															<input type="file" class="manual-file-select" id="audio-attch2" name="lesson_main_audio">
															<div class="audio_msg text-center"></div>
														</div>
														<div class="drop-zone__details p-3"> <small>@lang('client_site.audio_upload_instruction')</small>
															<br>
															<!---->
														</div>
													</div>
												</div>
											</div>
											<div class="form-row" id="progress_list_audio">
												<div class="form-group col-sm-12 ">
													<label class="label">@lang('client_site.upload_file')</label>
													<ul class="progress-ul-audio">
													</ul>
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-sm-12 ">
													<br>
													<label class="label">@lang('client_site.description')</label>
													<textarea class="audio_description" name="audio_description" id="audio_description"></textarea>
													<div id="err_audio_description"></div>
												</div>
											</div>
										</div>
									</div>
									<!-- for audio -->

									<!-- for downloads -->
									<div class="lesson_section" id="for_D">
										<div class="course-content-grow">
											<div class="form-row">
												<div class="form-group col-sm-12 ">
													<label class="label">@lang('site.title')</label>
													<input class="ember-view lesson_title form-control" maxlength="255" size="50" type="text" name="lesson_title_D" id="lesson_title_D" data-ltype="D">
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-sm-12 ">
													<label class="label">@lang('client_site.lesson_duration_in_minutes')</label>
													<input class="ember-view form-control duration" maxlength="255" size="50" type="text" placeholder="@lang('client_site.lesson_duration_in_minutes')" name="duration_D" id="duration_D" data-ltype="D">
												</div>
											</div>
											<div class="form-row" id="upload_dwnlds_section">
												<div class="form-group col-sm-12">
													<label class="label">@lang('client_site.upload_file')</label>
													<div class="download_dropbx drop-zone clearfix show">
														<div class="drop-zone-header p-4">
															<div class="drop-zone__title">@lang('client_site.drag_drop_files')
																<br> <strong>@lang('client_site.or')</strong>
															</div>
															<label class="button button--primary manual-upload mt-2" for="download-attch2"> @lang('client_site.upload_file') </label>
															<input type="file" class="manual-file-select" id="download-attch2" name="lesson_main_download" multiple>
															<div class="download_msg text-center"></div>
														</div>
														<div class="drop-zone__details p-3"> <small>@lang('client_site.any_file_instruction')</small>
															<br>
															<!---->
														</div>
													</div>
												</div>
											</div>
											<div class="form-row" id="progress_list">
												<div class="form-group col-sm-12 ">
													<label class="label">@lang('client_site.upload_file')</label>
													<ul class="progress-ul">
													</ul>
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-sm-12 ">
													<br>
													<label class="label">@lang('site.description')</label>
													<textarea class="download_description" name="download_description" id="download_description"></textarea>
													<div id="err_download_description"></div>
												</div>
											</div>
										</div>
									</div>
									<!-- for downloads -->

									<!-- for presentation -->
									<div class="lesson_section" id="for_P">
										<div class="course-content-grow">
											<div class="form-row">
												<div class="form-group col-sm-12 ">
													<label class="label">@lang('site.title')</label>
													<input class="ember-view lesson_title form-control" maxlength="255" size="50" type="text" name="lesson_title_P" id="lesson_title_P" data-ltype="P">
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-sm-12 ">
													<label class="label">@lang('client_site.lesson_duration_in_minutes')</label>
													<input class="ember-view form-control duration" maxlength="255" size="50" type="text" placeholder="@lang('client_site.lesson_duration_in_minutes')" name="duration_P" id="duration_P" data-ltype="P">
												</div>
											</div>
											<div class="form-row" id="upload_img_section">
												<div class="form-group col-sm-12">
													<label class="label">@lang('client_site.upload_file')</label>
													<div class="image_dropbx drop-zone clearfix show">
														<div class="drop-zone-header p-4">
															<div class="drop-zone__title">@lang('client_site.drag_drop_image_file')
																<br> <strong>@lang('client_site.or')</strong>
															</div>
															<label class="button button--primary manual-upload mt-2" for="image-attch2"> @lang('client_site.upload_file') </label>
															<input type="file" class="manual-file-select" id="image-attch2" name="lesson_main_image">
															<div class="image_msg text-center"></div>
														</div>
														<div class="drop-zone__details p-3"> <small>@lang('client_site.image_upload_instruction')</small>
															<br>
															<!---->
														</div>
													</div>
													<label class="label">
														<p>@lang('client_site.slide_options_info')</p>
													</label>
												</div>
											</div>
											<div class="form-row" id="progress_list_image">
												<div class="form-group col-sm-12 ">
													<label class="label">@lang('client_site.upload_file')</label>
													<ul class="progress-ul-image">
													</ul>
												</div>
											</div>
											<!-- <div class="form-row">
												<div class="form-group col-sm-12 ">
													<label class="label">Content</label>
													<textarea class="image_description" name="image_description" id="image_description"></textarea>
													<div id="err_image_description"></div>
												</div>
											</div> -->
										</div>
										<!-- <div class="course-content-grow" id="presentation_btns">
											<div class="mb-2">
												<button type="button" class="button button--primary" id="add_slide"><i class="icofont-plus"></i> Add Slide</button>
												<button type="button" class="button button--secondary"> Copy slide from</button>
											</div>
										</div> -->
									</div>
									<!-- for presentation -->

									<div class="course-content-grow">
										<h3 class="form-title">@lang('client_site.lesson_settings')</h3>
										<div class="form-group custom-control" id="is_main_preview_video_div">
											<input class="ember-view ember-checkbox custom-control-input" type="checkbox" name="is_main_preview_video" id="is_main_preview_video">
											<label for="is_main_preview_video"> @lang('client_site.make_main_preview_video_lesson') &nbsp; </label>
											<div class="tools-tip tools-tip-right" data-toggle="tooltip" data-placement="top" title="@lang('client_site.make_main_preview_video_lesson_tooltip')"><i class="icofont-info-circle"></i></div>
										</div>
										<div class="form-group custom-control">
											<input class="ember-view ember-checkbox custom-control-input" type="checkbox" name="is_free_preview" id="is_free_preview">
											<label for="is_free_preview"> @lang('client_site.make_free_preview_lesson') &nbsp; </label>
											<div class="tools-tip tools-tip-right" data-toggle="tooltip" data-placement="top" title="@lang('client_site.make_free_preview_lesson_text')"><i class="icofont-info-circle"></i></div>
										</div>
										<div class="form-group custom-control">
											<input class="ember-view ember-checkbox custom-control-input" type="checkbox" name="is_prerequisite" id="is_prerequisite">
											<label for="is_prerequisite"> @lang('client_site.make_prerequisite') &nbsp; </label>
											<div class="tools-tip tools-tip-right" data-toggle="tooltip" data-placement="top" title="@lang('client_site.make_prerequisite_text')"><i class="icofont-info-circle"></i></div>
										</div>
										<div class="form-group custom-control">
											<input class="ember-view ember-checkbox custom-control-input" type="checkbox" name="is_allow_discussion" id="is_allow_discussion">
											<label for="is_allow_discussion"> @lang('client_site.allow_discussion') &nbsp; </label>
											<div class="tools-tip tools-tip-right" data-toggle="tooltip" data-placement="top" title="@lang('client_site.allow_discussion_text')"><i class="icofont-info-circle"></i></div>
										</div>
										<!-- <div class="form-group custom-control">
											<p><i class="icofont-lock"></i> Make this a prerequisite </p> <a href="#" class="badge--upsell">Pro</a>
										</div> -->
										<button type="button" class="button button--knockout button--knockout-danger button--icon-left mt-4 mb-4" data-ember-action="1527" id="delete_lesson" data-id="{{@$lesson->id}}"> <i class="fa fa-trash-o"></i> @lang('client_site.delete_lesson') </button>
									</div>
									<div class="admin-build-section__inner clearfix pb-5 mb-5">
										<div class="pull-right action-buttons__container d-inline-flex">
											<button type="button" class="button button--secondary content-action-bar__discard-btn discard_lesson" data-ember-action="1548"> @lang('client_site.discard_changes') </button>
											<button type="button" data-ember-action="1549" class="button button--primary save_lesson_btn"> @lang('client_site.save') </button>
										</div>
									</div>
								</form>
							</div>
						</div>
						<div class="admin-build-section_new right-section px-md-2" style="display:none">
                            <form action="{{route('add.chapter')}}" id="save_chapter" method="post">
                                <input type="text" name="ID" id="main_chapter_id" value="0" hidden>
                                <div class="builder-bar chapter-builder-bar mb-3 mb-lg-4">

                                    <div class="clearfix mobile--show">
                                        <div class="pull-right action-buttons d-inline-flex align-items-center chapter-action-buttons">

                                            <button type="button" class="button button--secondary content-action-bar__discard-btn discard_chapter" data-ember-action="1560" data-title="{{@$chapter->chapter_title}}"> @lang('client_site.discard_changes') </button>
                                            <button type="button" class="button button--primary save_chapter_btn" data-ember-action="1561"> @lang('client_site.save') </button>
											<button type="button" class="button button--knockout button--knockout-danger button--icon-left mt-4 mb-4 del_chapter" data-ember-action="1527" data-id="{{@$chapter->id}}"> <i class="fa fa-trash-o m-0"></i></button>
                                        </div>
                                    </div>
                                    <div class="mobile-hide ">
                                        <div class="pull-right action-buttons d-inline-flex align-items-center chapter-action-buttons">

                                            <button type="button" class="button button--secondary content-action-bar__discard-btn discard_chapter" data-ember-action="1560" data-title="{{@$chapter->chapter_title}}"> @lang('client_site.discard_changes') </button>
                                            <button type="button" class="button button--primary save_chapter_btn" data-ember-action="1561"> @lang('client_site.save') </button>
											<button type="button" class="button button--knockout button--knockout-danger button--icon-left mt-4 mb-4 del_chapter" data-ember-action="1527" data-id="{{@$chapter->id}}"> <i class="fa fa-trash-o m-0"></i></button>

										</div>

                                    </div>
                                    <div class="builder_header-container">
                                        <div class="builder__header mt-3 mt-lg-0" id="chapter_name"> @lang('client_site.new_chapter') : @if(@$chapter) {{$chapter->chapter_title}} @else @lang('client_site.untitled_chapter') @endif</div>
                                    </div>
                                </div>
                                <div class="content__sections">
                                    <div class="course-content-grow">
                                        <div class="form-row">
                                            <div class="form-group col-sm-12 ">
                                                <label class="label">@lang('client_site.chapter_title')</label>
                                                <input class="ember-view form-control" maxlength="255" size="50" type="text" name="chapter_title" id="chapter_title" placeholder="@lang('client_site.untitled_chapter')" value="{{@$chapter ? $chapter->chapter_title : __('client_site.untitled_chapter') }}">
												<!-- <label class="error err_chapter_title" style="display:none;">This chapter already exists in this course.</label> -->
												<div class="custom-control custom-checkbox p-0 mt-1">
													<input type="checkbox" id="set_lessons_status" name="radios" class="radio-box" value="all" @if(@$chapter->set_lessons_status == 'D') checked @endif>
													<label for="set_lessons_status">
														@lang('client_site.set_lesson_to_draft')
														<div class="tools-tip tools-tip-right" data-toggle="tooltip" data-placement="top" title="@lang('client_site.lesson_to_draft')"><i class="icofont-info-circle"></i></div>
													</label>
												</div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="tree-tip mt-5">
                                    <div class="tip__title"> <img src="{{ URL::to('public/frontend/images/tip.png') }}"> @lang('client_site.pro_tip') </div>
                                    <p class="tip__description">@lang('client_site.draft_chapter_info') </div>
                                </div>
                            </form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">@lang('client_site.import_questions')</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-center">
				<div>
					@lang('client_site.import_questions_instructions')
				</div>
				<br>
				<div class="text-left">@lang('site.step_1'):</div>
				<div>
					<a href="{{url('/')}}/storage/framework/quiz_csv_format/upload_format.csv" class="button button--primary">@lang('client_site.download_format')</a>
				</div>
				<!-- <div class="mt-3"><h5>@lang('client_site.or')</h5></div> -->
				<div class="mt-3 text-left">@lang('site.step_2'):</div>
				<div>
					<label class="button button--secondary manual-upload mt-2" for="import"> @lang('client_site.upload_file')</label>
					<input type="file" class="manual-file-select" id="import" name="import">
				</div>
			</div>
			<!-- <div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
			</div> -->
			</div>
		</div>
	</div>
@endsection
@section('footer')
{{-- @include('includes.footer') --}}
<script src="https://player.vimeo.com/api/player.js"></script>
<script>
	// (function(){
	// 	$('#exampleModal').css()
	// })();

	// var file_size_limit = 10485760;
	var file_size_limit = 20971520;
	// var file_size_limit = 1073741824;
	var vdo_size_limit = 209715200;
	var vdo_file_size_limit = 209715200;
	var freelesson = "{{@$free_lesson ?? ''}}";
	var Chapter = null, upload_progress = false, upload_progress_lesson_id = 0;
	var success_message = quizErrMsg = ajaxCall = audioAjaxCall = "", attchAjaxCall = dwnldAjaxCall = [];
	var lesson_main_video_required = lesson_main_audio_required = lesson_main_download_required = lesson_main_image_required = true;
	document.addEventListener("keydown", function(e) {
		if (e.key === 's' && (navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)) {
			e.preventDefault();
		}
	}, false);
	var is_allow_discussion = is_prerequisite = is_free_preview = is_main_preview_video = 'N';

	$('.duration').keyup(function(){
		$(".duration").inputFilter(function(value) {
		return /^\d*$/.test(value);
		});
	});
	$('#video_url').keyup(function(){
		console.log($('#video_url').val());
		if($('#video_url').val() == "") lesson_main_video_required = true;
		else lesson_main_video_required = false;
	});
	// if (window.matchMedia('(max-width: 991px)').matches) {
	// 	// functionality for screens smaller than 1200px
	// 	$('.course_view').addClass('mobile_chapter_list');
	// }
	if (window.matchMedia('(min-width: 992px)').matches) {
		// functionality for screens smaller than 1200px
		$('.course_view').removeClass('mobile_chapter_list');
	}
	$(window).resize(function() {
		if (window.matchMedia('(max-width: 991px)').matches) {
			// functionality for screens smaller than 1200px
			$('.course_view').addClass('mobile_chapter_list');
		}
		if (window.matchMedia('(min-width: 992px)').matches) {
			// functionality for screens smaller than 1200px
			$('.course_view').removeClass('mobile_chapter_list');
		}
	});
	$('.show_course_view').click(function(){
		console.log("Toggle Clicked");
		$('.mobile_chapter_list').toggle("slide", { direction: "left" }, 500);
	});

	// validation rules
		var submit_form = false;
		function isMainVdoRequired(){
			var reqd = false;
			if($("#video_url").val() == ""){
				reqd = true;
			} else {
				reqd = false;
			}
			return reqd;
		}

		jQuery.validator.addMethod("youtuberegex", function(value, element) {
			return this.optional(element) || /^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/.test( value );
		}, "@lang('client_site.please_enter_valid_youtube_url')");

		jQuery.validator.addMethod("notzero", function(value, element) {
			return this.optional(element) || value != 0;
		});


		var video_rules = {
			lesson_title_V: {required:true},
			duration_V: {required:true, notzero:true},
			lesson_main_video: {
				required: lesson_main_video_required
			},
			video_url: {youtuberegex:true}
		};
		var video_messege = {
			lesson_title_V:"@lang('client_site.enter_lesson_title')",
			duration_V: {required:"@lang('client_site.enter_lesson_duration')", notzero:"@lang('client_site.duration_cannot_be_zero')"},
			lesson_main_video:{
				required:"@lang('client_site.enter_video_file')"
			}
		};
		var video_err = function(error, element) {
			if (element.attr("name") == "lesson_main_video") {
				$(".video_msg").append(error);
				console.log(error);
			}
			else {
				error.insertAfter(element);
			}
		};


		var text_rules = {
			lesson_title_T: {required:true},
			duration_T: {required:true, notzero:true},
			description: {required:true}
		};
		var text_messege = {
			lesson_title_T:"@lang('client_site.enter_lesson_title')",
			duration_T: {required:"@lang('client_site.enter_lesson_duration')", notzero:"@lang('client_site.duration_cannot_be_zero')"},
			description:"@lang('client_site.enter_text_content')",
		};
		var text_err = function(error, element) {
			if (element.attr("name") == "description") {
				$("#err_description").append(error);
				console.log(error);
			}
			else {
				error.insertAfter(element);
			}
		};


		var quiz_rules = {
			lesson_title_Q: {required:true},
			duration_Q: {required:true}
		};
		var quiz_messege = {
			lesson_title_Q:"@lang('client_site.enter_lesson_title')",
			duration_Q: {required:"@lang('client_site.enter_lesson_duration')", notzero:"@lang('client_site.duration_cannot_be_zero')"},
		};


		var audio_rules = {
			lesson_title_A: {required:true},
			duration_A: {required:true, notzero:true},
			lesson_main_audio: {
				required: lesson_main_audio_required
			},
			// audio_description: {required:true},
		};
		var audio_messege = {
			lesson_title_A:"@lang('client_site.enter_lesson_title')",
			duration_A: {required:"@lang('client_site.enter_lesson_duration')", notzero:"@lang('client_site.duration_cannot_be_zero')"},
			// audio_description:"@lang('client_site.enter_lesson_description')",
			lesson_main_audio:"@lang('client_site.enter_audio_file')"
		};
		var audio_err = function(error, element) {
			if (element.attr("name") == "lesson_main_audio") {
				// console.log($(".audio_msg")[0]);
				$(".audio_msg").append(error);
			}
			if (element.attr("name") == "audio_description") {
				$('#err_audio_description').append(error);
			}
			else {
				error.insertAfter(element);
			}
		};


		var download_rules = {
			lesson_title_D: {required:true},
			duration_D: {required:true, notzero:true},
			// download_description: {required:true},
			lesson_main_download: {
				required: lesson_main_download_required
			},
		};
		var download_messege = {
			lesson_title_D:"@lang('client_site.enter_lesson_title')",
			duration_D: {required:"@lang('client_site.enter_lesson_duration')", notzero:"@lang('client_site.duration_cannot_be_zero')"},
			// download_description:"@lang('client_site.enter_lesson_description')",
			lesson_main_download:"@lang('client_site.enter_files')"
		};
		var download_err = function(error, element) {
			if (element.attr("name") == "lesson_main_download") {
				// console.log($(".download_msg")[0]);
				$(".download_msg").append(error);
			}
			// if (element.attr("name") == "download_description") {
			// 	$('#err_download_description').append(error);
			// }
			else {
				error.insertAfter(element);
			}
		};


		var multimedia_rules = {
			lesson_title_M: {required:true},
			duration_M: {required:true, notzero:true},
			source_url: {required:true}
		};
		var multimedia_messege = {
			lesson_title_M:"@lang('client_site.enter_lesson_title')",
			duration_M: {required:"@lang('client_site.enter_lesson_duration')", notzero:"@lang('client_site.duration_cannot_be_zero')"},
		};
		var multimedia_err = function(error, element) {
			error.insertAfter(element);
		};


		var presentation_rules = {
			lesson_title_P: {required:true},
			duration_P: {required:true, notzero:true},
			lesson_main_image: {
				required: lesson_main_image_required
			},
		};
		var presentation_messege = {
			lesson_title_P:"@lang('client_site.enter_lesson_title')",
			duration_P: {required:"@lang('client_site.enter_lesson_duration')", notzero:"@lang('client_site.duration_cannot_be_zero')"},
			lesson_main_image: {required:"@lang('client_site.enter_image_file')"},
		};
		var presentation_err = function(error, element) {
			if (element.attr("name") == "lesson_main_image") {
				$(".image_msg").append(error);
			}
			else {
				error.insertAfter(element);
			}
		};


	// validation rules




	// LESSONS
		$(document).delegate('.add-lesson', 'click', function(){
			$(".add-lesson-info").slideToggle();
		});
		$('.chapter_header').click(function(){
			$('#chapter_id').val($(this).data('id'));
		});
		$(document).delegate('.types__item', 'click', function(){
			$('#delete_lesson').hide();
			$('#save_lesson_form')[0].reset();
			$('.right-section').hide();
			$('#lesson_id').val('');
			$('#lsn_title').text('@lang("client_site.new_lesson")');
			$('#lesson_title_'+$(this).data('ltype')).val('@lang("client_site.new_lesson")');
			$('#is_main_preview_video_div').hide();
			var chapter_id = $(this).data('chapter');
			$('#chapter_id').val(chapter_id);
			var id = "section_chapter_"+chapter_id;
			var img = badge = "";
			if(Chapter.set_lessons_status == 'D'){
				$('#chk6').prop('checked', true);
				$('#chk5').prop('checked', true);
				badge = `<span class="ml-2 badge badge--warning">@lang("client_site.draft")</span>`;
			} else {
				$('#chk6').prop('checked', false);
				$('#chk5').prop('checked', false);
				badge = "";
			}

			if($(this).data('ltype') == 'V'){
				$('#is_main_preview_video_div').show();
				img = `<img src="{{ URL::to('public/frontend/images/video.png') }}">`;
				lesson_main_video_required = true;
				$('#show_vdo_section').remove();
				$('#upload_vdo_section').show();
				$('#progress_list_video').hide();
				$('#progress_list_video_atch').hide();
				$('#thumb').attr('src', "");
				$('#thumb').hide();
				$('#upload_thumbnail_section').hide();
				if($('#video_attachment_list').length) $('#video_attachment_list').remove();
				initMCEexact("video_description");
			} else if($(this).data('ltype') == 'Q'){
				$('.quiz_question').remove();
				if(typeof(tinyMCE) !== 'undefined') {
					var length = tinyMCE.editors.length;
					for (var i=length; i>0; i--) {
						tinyMCE.editors[i-1].remove();
					};
				}
				var html =
				`<div class="course-content-grow quiz_question old_questns" data-id="1" style="display:none;">
					<div id="main3">
						<div class="accordion3" id="faqnew2">
							<div class="card">
								<div class="card-header" id="faqnewhead1">
									<a href="#" class=" btn-header-link" data-toggle="collapse" data-target="#qhead1" aria-expanded="true" aria-controls="qhead1">
										<h5>@lang('client_site.question') 1 <sup style="display:none;"><i class="fa fa-circle" aria-hidden="true"></i></sup></h5>
										<div class="d-flex align-items-center">
											<div class="button-group">
												<span class="user_llllk new-link3 button del_quiz" id="del_quiz1" data-count="1">
													<i class="fa fa-trash-o" aria-hidden="true"></i>
												</span>
											</div>
										</div>
									</a>
								</div>
								<div id="qhead1" class="collapse" aria-labelledby="faqnewhead1" data-parent="#faqnew2">
									<div class="card-body">
										<div class="clearfix mt-3"></div>
										<div id="err_correct1"></div>
										<div class="clearfix mt-2"></div>
										<label>@lang('client_site.question')</label>
										<textarea class="question1" id="question1" name="question1"></textarea>

										<div class="accordion4 mt-3" id="faqnew3">
											@for($i=1; $i<=6; $i++)
											<div class="card">
												<div class="card-header" id="faqheadnew">
													<a href="javascript:;" class=" btn-header-link" data-toggle="collapse" data-target="#choice1_{{$i}}" aria-expanded="true" aria-controls="choice1_{{$i}}">Alternativa {{$i}}</a>
												</div>
												<div id="choice1_{{$i}}" class="collapse {{ $i<=2 ? 'show' : ''}}" aria-labelledby="faqheadnew" data-parent="#faqnew">
													<div class="card-body">
														<textarea class="answer1_{{$i}}" id="answer1_{{$i}}" name="answer1_{{$i}}"></textarea>
														<div class="clearfix"></div>
														<div class="dash-d das-radio">
															<input type="radio" name="correct1" id="correct1_{{$i}}" class="correct1 inp_radio" value="{{$i}}">
															<label for="correct1_{{$i}}">@lang('client_site.this_is_correct_answer')</label>
														</div>
													</div>
												</div>
											</div>
											@endfor
										</div>

										<div class="clearfix mt-3"></div>
										<label>Explanation</label>
										<textarea class="explanation1" id="explanation1" name="explanation1"></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>`;
				$(html).insertBefore('#quiz_btns');
				initMCEshort("question1");
				initMCEshort("answer1_1");
				initMCEshort("answer1_2");
				initMCEshort("answer1_3");
				initMCEshort("answer1_4");
				initMCEshort("answer1_5");
				initMCEshort("answer1_6");
				initMCEshort("explanation1");
				$('.quiz_question').fadeIn();
				$('#quiz_count').val(1);
				img = `<img src="{{ URL::to('public/frontend/images/quiz.png') }}">`;
			} else if($(this).data('ltype') == 'M'){
				img = `<img src="{{ URL::to('public/frontend/images/multimedia.png') }}">`;
				$('#source_url').val('');
			} else if($(this).data('ltype') == 'T'){
				if(typeof(tinyMCE) !== 'undefined') {
					var length = tinyMCE.editors.length;
					for (var i=length; i>0; i--) {
						tinyMCE.editors[i-1].remove();
					};
				}
				$('#description').val('');
				initMCEexact("description");

				img = `<img src="{{ URL::to('public/frontend/images/text.png') }}">`;
			} else if($(this).data('ltype') == 'A'){
				lesson_main_audio_required = true;
				$('#audio_description').val('');
				initMCEexact('audio_description');
				$('#upload_ado_section').show();
				$('#progress_list_audio').hide();
				$('#audios').remove();
				$('.audio_msg').empty();

				img = `<img src="{{ URL::to('public/frontend/images/audio.png') }}">`;
			} else if($(this).data('ltype') == 'D'){
				lesson_main_download_required = true;
				$('#download_description').val('');
				initMCEexact('download_description');
				$('.download_msg').empty();
				$('#progress_list').hide();
				img = `<img src="{{ URL::to('public/frontend/images/Download.png') }}">`;
			} else if($(this).data('ltype') == 'P'){
				lesson_main_image_required = true;
				$('.presentation_slide').remove();
				$('#progress_list_image').hide();
				img = `<img src="{{ URL::to('public/frontend/images/presentaion.png') }}">`;
			}

			if ($(".latest_lesson")[0] && upload_progress == false){
				$("ui-effects-wrapper").remove();
				$('.latest_lesson').remove();
			}
			if($('.active_lesson').length) $('.active_lesson').removeClass('active_lesson');
			var html = `<div class="secsions latest_lesson active_lesson" style="display:none;">
				<div class="session_info" data-ltype="${$(this).data('ltype')}">
					<div class="content-handle"><img src="{{ URL::to('public/frontend/images/cha.png') }}"></div>
					<div class="lesson-se"> <span>@lang('client_site.new_lesson')</span>
						<div class="tool-tip d-flex flex-row align-items-center">
							${img}
							${badge}
						</div>
					</div>
				</div>
			</div>`;
			$(html).insertBefore($(".session-btn"));
			$('.latest_lesson').show("slide", { direction: "left" }, 500);

			$('.lessons_section').show();
			var ltype = $(this).data('ltype');
			$('#lesson_type').val(ltype);
			$('.lesson_section').each(function(i, lesson_section){
				$(lesson_section).hide();
			});
			console.log($('#for_'+ltype)[0]);
			$('#for_'+ltype).show();
			$('.mobile_chapter_list').hide("slide", { direction: "left" }, 500);
		});
		$('#chk6').change(function(){
			if($('#chk6').is(":checked")){
				$('#chk5').prop('checked', true);
				$('.active_lesson').find('.tool-tip').append(`<span class="ml-2 badge badge--warning">@lang("client_site.draft")</span>`);
			} else {
				$('#chk5').prop('checked', false);
				$('.active_lesson').find('.tool-tip').find('.badge--warning').remove();
			}
		});
		$('#chk5').change(function(){
			if($('#chk5').is(":checked")){
				$('#chk6').prop('checked', true);
				$('.active_lesson').find('.tool-tip').append(`<span class="ml-2 badge badge--warning">@lang("client_site.draft")</span>`);
			} else {
				$('#chk6').prop('checked', false);
				$('.active_lesson').find('.tool-tip').find('.badge--warning').remove();
			}
		});
		$('.discard_lesson').click(function(){
			if($('#lesson_id').val() != ""){
				swal({
					title: "@lang('client_site.discard_changes')?",
					text: "@lang('client_site.discard_changes')?",
					icon: "warning",
					buttons: true,
					dangerMode: true,
					buttons: ['@lang("client_site.cancel")', '@lang("client_site.discard")']
				})
				.then((willDelete) => {
					if (willDelete) {
						var lid = $('#lesson_id').val();
						var cid = $('#chapter_id').val();
						var ltp = $('#lesson_type').val();
						loadLessons(lid,cid,ltp);
					} else {
						return false;
					}
				});
			} else {
				$('.right-section').hide();
				$('.empty-state').parent().show();
				if ($(".latest_lesson")[0]){
					$("ui-effects-wrapper").remove();
					$('.latest_lesson').remove();
				}
			}
		});
		async function getTime(i, file, type) {
			var filetime = "";

			if(type == "video"){
				var video = document.createElement('video');
				video.preload = 'metadata';
				dur = video.onloadedmetadata = function() {
					window.URL.revokeObjectURL(video.src);
					dur = video.duration;
					var hour = parseInt((video.duration) / 3600);
					if (hour<10) hour = "0" + hour;
					var minute = parseInt((video.duration % 3600) / 60);
					if (minute<10) minute = "0" + minute;
					var second = Math.ceil(video.duration % 60);
					if (second<10) second = "0" + second;
					if(hour != "00" || hour != 0) filetime = hour + ":" + minute + ":" + second;
					else filetime =  minute + ":" + second;
					console.log(filetime);
					$("#file_duration").val(filetime);
				}
				video.src = URL.createObjectURL(file);
				const result = await videoSave();
			}
			if(type == "audio"){
				var audio = document.createElement('audio');
				audio.preload = 'metadata';
				dur = audio.onloadedmetadata = function() {
					window.URL.revokeObjectURL(audio.src);
					dur = audio.duration;
					var hour = parseInt((audio.duration) / 3600);
					if (hour<10) hour = "0" + hour;
					var minute = parseInt((audio.duration % 3600) / 60);
					if (minute<10) minute = "0" + minute;
					var second = Math.ceil(audio.duration % 60);
					if (second<10) second = "0" + second;
					if(hour != "00" || hour != 0) filetime = hour + ":" + minute + ":" + second;
					else filetime =  minute + ":" + second;
					console.log(filetime);
					$("#file_duration").val(filetime);
				}
				audio.src = URL.createObjectURL(file);
				const result = await audioSave();
			}
			if(type == "video_attachment"){
				console.log(i);
				console.log(file);
				console.log(file['type']);
				if(file['type'].split('/')[0] == 'video'){
					var video = document.createElement('video');
					video.preload = 'metadata';
					dur = video.onloadedmetadata = function() {
						window.URL.revokeObjectURL(video.src);
						dur = video.duration;
						var hour = parseInt((video.duration) / 3600);
						if (hour<10) hour = "0" + hour;
						var minute = parseInt((video.duration % 3600) / 60);
						if (minute<10) minute = "0" + minute;
						var second = Math.ceil(video.duration % 60);
						if (second<10) second = "0" + second;
						if(hour != "00" || hour != 0) filetime = hour + ":" + minute + ":" + second;
						else filetime =  minute + ":" + second;
						console.log(filetime);
						$('#file_duration_'+i).val(filetime);
					}
					video.src = URL.createObjectURL(file);
					const result = await videoAttachmentSave(i, file);
				} else if(file['type'].split('/')[0] == 'audio'){
					var audio = document.createElement('audio');
					audio.preload = 'metadata';
					dur = audio.onloadedmetadata = function() {
						window.URL.revokeObjectURL(audio.src);
						dur = audio.duration;
						var hour = parseInt((audio.duration) / 3600);
						if (hour<10) hour = "0" + hour;
						var minute = parseInt((audio.duration % 3600) / 60);
						if (minute<10) minute = "0" + minute;
						var second = Math.ceil(audio.duration % 60);
						if (second<10) second = "0" + second;
						if(hour != "00" || hour != 0) filetime = hour + ":" + minute + ":" + second;
						else filetime =  minute + ":" + second;
						console.log(filetime);
						$('#file_duration_'+i).val(filetime);
					}
					audio.src = URL.createObjectURL(file);
					const result = await videoAttachmentSave(i, file);
				} else {
					$('#file_duration_'+i).val("");
					const result = await videoAttachmentSave(i, file);
				}
			}
			if(type == "download"){
				console.log(i);
				console.log(file);
				console.log(file['type']);
				if(file['type'].split('/')[0] == 'video'){
					var video = document.createElement('video');
					video.preload = 'metadata';
					dur = video.onloadedmetadata = function() {
						window.URL.revokeObjectURL(video.src);
						dur = video.duration;
						var hour = parseInt((video.duration) / 3600);
						if (hour<10) hour = "0" + hour;
						var minute = parseInt((video.duration % 3600) / 60);
						if (minute<10) minute = "0" + minute;
						var second = Math.ceil(video.duration % 60);
						if (second<10) second = "0" + second;
						if(hour != "00" || hour != 0) filetime = hour + ":" + minute + ":" + second;
						else filetime =  minute + ":" + second;
						console.log(filetime);
						$('#file_duration_'+i).val(filetime);
					}
					video.src = URL.createObjectURL(file);
					const result = await downloadSave(i, file);
				} else if(file['type'].split('/')[0] == 'audio'){
					var audio = document.createElement('audio');
					audio.preload = 'metadata';
					dur = audio.onloadedmetadata = function() {
						window.URL.revokeObjectURL(audio.src);
						dur = audio.duration;
						var hour = parseInt((audio.duration) / 3600);
						if (hour<10) hour = "0" + hour;
						var minute = parseInt((audio.duration % 3600) / 60);
						if (minute<10) minute = "0" + minute;
						var second = Math.ceil(audio.duration % 60);
						if (second<10) second = "0" + second;
						if(hour != "00" || hour != 0) filetime = hour + ":" + minute + ":" + second;
						else filetime =  minute + ":" + second;
						console.log(filetime);
						$('#file_duration_'+i).val(filetime);
					}
					audio.src = URL.createObjectURL(file);
					const result = await downloadSave(i, file);
				} else {
					$('#file_duration_'+i).val("");
					const result = await downloadSave(i, file);
				}
			}
			if(type == "pres_audio"){
				var audio = document.createElement('audio');
				audio.preload = 'metadata';
				dur = audio.onloadedmetadata = function() {
					window.URL.revokeObjectURL(audio.src);
					dur = audio.duration;
					var hour = parseInt((audio.duration) / 3600);
					if (hour<10) hour = "0" + hour;
					var minute = parseInt((audio.duration % 3600) / 60);
					if (minute<10) minute = "0" + minute;
					var second = Math.ceil(audio.duration % 60);
					if (second<10) second = "0" + second;
					if(hour != "00" || hour != 0) filetime = hour + ":" + minute + ":" + second;
					else filetime =  minute + ":" + second;
					console.log(filetime);
					$("#file_duration").val(filetime);
				}
				audio.src = URL.createObjectURL(file);
				const result = await presAudioSave(i, file);
			}
		}
		function showErrorMsg(text){
			toastr.error(text);
		}


		// Add/Edit/Delete Lesson
			function saveLesson(from, reqData){
				console.log(from);
				console.log(reqData);

				if($('#is_allow_discussion').is(":checked")){

                    is_allow_discussion = 'Y';
                }else{
                    is_allow_discussion = 'N';

                }
				if($('#is_prerequisite').is(":checked")){

                    is_prerequisite = 'Y';
                } else{

                    is_prerequisite = 'N';

                }
				if($('#is_free_preview').is(":checked")){

                    is_free_preview = 'Y';
                }else{

                    is_free_preview = 'N';
                }
				if($('#is_main_preview_video').is(":checked")){
                    is_main_preview_video = 'Y';
                }else{
                    is_main_preview_video = 'N';

                }

				reqData.append("is_allow_discussion", is_allow_discussion);
				reqData.append("is_prerequisite", is_prerequisite);
				reqData.append("is_free_preview", is_free_preview);
				reqData.append("is_main_preview_video", is_main_preview_video);

				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': "{{csrf_token()}}"
					}
				});
				$.ajax({
					url: "{{ route('save.lesson') }}",
					method: 'post',
					dataType: 'json',
					data: reqData,
					async: false,
					cache: false,
					contentType: false,
					processData: false,
					success: function(res){
						console.log(res);
						if(res.status == "success"){
							var html = "";

							if(res.lesson.lesson_type == 'V'){
								$('#upload_vdo_section label.error').remove();
								$('.video_msg').empty();
								$('.video_msg1').empty();
								// var html3 = `<div class="form-row" id="show_vdo_section"><video src="{{url('/')}}/storage/app/public/lessons/type_video/${res.main_video.filename}" width="100%" height="320" controls></video><a href="javascript:;" class="pull-right mt-3 rem_vdo" onclick="removeMainVideo()">@lang('client_site.remove_video')</a></div>`;
								// $(html3).insertAfter($("#upload_vdo_section"));
								$('.video_attachment_msg').empty();
								// loadAttachments(res.attachments);
								if($('#video_url').val() != ""){
									var embedCode = youTubeIdFromLink($('#video_url').val());
									if($('#show_vdo_section').length) $('#show_vdo_section').remove();
									var html = `<div class="form-row mb-3" id="show_vdo_section" style="height:320px"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/${embedCode}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><a href="javascript:;" class="pull-right mt-3 rem_vdo" onclick="removeMainVideo()">@lang('client_site.remove_video')</a></div>`
									$(html).insertAfter($("#upload_vdo_section"));
									$('#upload_vdo_section').hide();
									$('#show_vdo_section').show();
								}
								if(res.lesson.is_main_preview_video == 'Y') freelesson = res.lesson.id;
								if(res.lesson.id == freelesson && res.lesson.is_main_preview_video == 'N') freelesson = '';
							}
							if(res.lesson.lesson_type == 'A'){
								$('.audio_msg').empty();
							}
							if(res.lesson.lesson_type == 'D' || res.lesson.lesson_type == 'A' || res.lesson.lesson_type == 'V'){
								$('#lesson_id').val(res.lesson.id);
							}
							success_message = res.message;
							$('#lesson_id').val(res.lesson.id);
							$('#delete_lesson').attr('data-id',res.lesson.id);
							$('#delete_lesson').show();
							if(res.lesson.lesson_type != 'Q' && res.lesson.lesson_type != 'D'){
								setTimeout( function(){
									$('.save_lesson_btn').parent().find('.progrs').remove();
									$('.save_lesson_btn').show();
									$('.save_lesson_btn').parent().find('.discard_lesson').show();
									$('.save_lesson_btn').parent().find('.custom-checkbox').show();
									$('.save_lesson_btn').parent().find('.tools-tip').show();
								},500);
							}
							if(res.lesson.lesson_type == 'D' && from == 'save_btn'){
								setTimeout( function(){
									$('.save_lesson_btn').parent().find('.progrs').remove();
									$('.save_lesson_btn').show();
									$('.save_lesson_btn').parent().find('.discard_lesson').show();
									$('.save_lesson_btn').parent().find('.custom-checkbox').show();
									$('.save_lesson_btn').parent().find('.tools-tip').show();
								},500);
							}
							var elem = $('.active_chapter').parent().parent().find('.session-box li');
							loadLessonCards(Chapter.id, elem);
                            if(res.lesson.status == 'D') {
								$('#chk6').prop('checked', true);
								$('#chk5').prop('checked', true);
							} else {
								$('#chk6').prop('checked', false);
								$('#chk5').prop('checked', false);
							}
						} else {
							setTimeout( function(){
								toastr.error(res.message);
								$('.save_lesson_btn').parent().find('.progrs').remove();
								$('.save_lesson_btn').show();
								$('.save_lesson_btn').parent().find('.discard_lesson').show();
								$('.save_lesson_btn').parent().find('.custom-checkbox').show();
								$('.save_lesson_btn').parent().find('.tools-tip').show();
							},500);
						}
					},
					error: function(err){
						console.log(err);
					},
				});
			}
			$('.save_lesson_btn').click(function(){
				$('sup').hide();
				if($('#lesson_type').val() != 'Q'){
					console.log("Initiated");
					setTimeout( function(){
						$('.save_lesson_btn').hide();
						$('.save_lesson_btn').parent().find('.discard_lesson').hide();
						$('.save_lesson_btn').parent().find('.custom-checkbox').hide();
						$('.save_lesson_btn').parent().find('.tools-tip').hide();
						$('.save_lesson_btn').parent().find('.progrs').remove();
						$('.save_lesson_btn').parent().append(`<div class="progrs">@lang('client_site.saving_for_progress')...</div>`);
					},10);
				}
				$('.video_msg').empty();
				$('.video_msg1').empty();
				$('.audio_msg').empty();
				console.log("Emptied");

				// $('.save_lesson_btn').addClass('disable_btns');
				// $('.save_lesson_btn').parent().find('.discard_lesson').addClass('disable_btns');
				// $('.save_lesson_btn').parent().find('.custom-checkbox').addClass('disable_btns');
				console.log($('.save_lesson_btn')[0]);

				if($('#lesson_type').val() == 'V'){
					var settings = $("#save_lesson_form").validate().settings;
					video_rules = $.extend({}, video_rules, {lesson_main_video: {required:lesson_main_video_required}});
					$.extend(settings, {
						rules: video_rules,
						messages: video_messege,
						errorPlacement: video_err
					});
				}
				if($('#lesson_type').val() == 'Q'){
					var quiz_count = parseInt($('#quiz_count').val());
					var new_quiz_rules = {};
					var new_quiz_messege = {};
					//prepare list of counts to show error for

					for(i=1; i<=quiz_count; i++){
						if($('#question'+i).length != 0){
							new_quiz_rules["question"+i] = {required:true};
							for(j=1; j<3; j++){
								new_quiz_rules["answer"+i+"_"+j] = {required:true};
							}
							new_quiz_rules["explanation"+i] = {required:true};
							new_quiz_rules["correct"+i] = {required:true};
							new_quiz_messege["correct"+i] = {required:"@lang('client_site.enter_correct_answer')"}
						}
					}
					quiz_rules = $.extend({}, quiz_rules, new_quiz_rules);
					quiz_messege = $.extend({}, quiz_messege, new_quiz_messege);

					var quiz_err = function(error, element) {
						var err_field_list = [];
						for(i=1; i<=quiz_count; i++){
							if($('#question'+i).length != 0){
								err_field_list.push("correct"+i);
							}
						}
						if (err_field_list.includes(element.attr("name"))) {
							$("#err_"+element.attr("name")).append(error);
						}
						else {
							error.insertAfter(element);
						}
						console.log(err_field_list);
						if(err_field_list.length){
							var id = err_field_list[0].substring(7);
							quizErrMsg = $('#qhead'+id).parent().find('.card-header').find('h5').text();
							quizErrMsg = "@lang('client_site.select_correct_option') " + quizErrMsg;
						}
					};
					var settings = $("#save_lesson_form").validate().settings;
					$.extend(settings, {
						ignore: [],
						rules: quiz_rules,
						messages: quiz_messege,
						errorPlacement: quiz_err
					});
				}
				if($('#lesson_type').val() == 'T'){
					var settings = $("#save_lesson_form").validate().settings;
					$.extend(settings, {
						ignore: [],
						rules: text_rules,
						messages: text_messege,
						errorPlacement: text_err
					});
				}
				if($('#lesson_type').val() == 'A'){
					var settings = $("#save_lesson_form").validate().settings;
					audio_rules = $.extend({}, audio_rules, {lesson_main_audio: {required:lesson_main_audio_required}});
					$.extend(settings, {
						ignore: [],
						rules: audio_rules,
						messages: audio_messege,
						errorPlacement: audio_err
					});
				}
				if($('#lesson_type').val() == 'D'){
					var settings = $("#save_lesson_form").validate().settings;
					download_rules = $.extend({}, download_rules, {lesson_main_download: {required:lesson_main_download_required}});
					$.extend(settings, {
						ignore: [],
						rules: download_rules,
						messages: download_messege,
						errorPlacement: download_err
					});
				}
				if($('#lesson_type').val() == 'M'){
					var settings = $("#save_lesson_form").validate().settings;
					$.extend(settings, {
						ignore: [],
						rules: multimedia_rules,
						messages: multimedia_messege,
						errorPlacement: multimedia_err
					});
				}
				if($('#lesson_type').val() == 'P'){
					var settings = $("#save_lesson_form").validate().settings;
					presentation_rules = $.extend({}, presentation_rules, {lesson_main_image: {required:lesson_main_image_required}});
					$.extend(settings, {
						rules: presentation_rules,
						messages: presentation_messege,
						errorPlacement: presentation_err
					});
				}
				// force a test of the form
				if($("#save_lesson_form").valid()){
					submit_form = true;

					if($('#is_allow_discussion').is(":checked")) is_allow_discussion = 'Y';
					if($('#is_prerequisite').is(":checked")) is_prerequisite = 'Y';
					if($('#is_free_preview').is(":checked")) is_free_preview = 'Y';

					if($('#lesson_type').val() == 'V'){
						// var attachment_names = [];

						var formData = new FormData();
						formData.append("jsonrpc", "2.0");
						formData.append("_token", '{{csrf_token()}}');
						formData.append("lesson_title", $('#lesson_title_V').val());
						formData.append("lesson_type", $('#lesson_type').val());
						formData.append("lesson_id", $('#lesson_id').val());
						formData.append("chapter_id", $('#chapter_id').val());
						formData.append("product_id", '{{@$product->id}}');
						formData.append("duration", $('#duration_V').val());
						formData.append("description", $('#video_description').val());
						formData.append("status", ($('#chk6').is(":checked") && $('#chk5').is(":checked")) ? 'D' : 'A');
						formData.append("video_url", $('#video_url').val());
						// if($('#video-attch2')[0].files.length){
						// 	formData.append("main_video", $('#video-attch2')[0].files[0]);
						// }
						// if($('#video_attachment')[0].files.length){
						// 	$.each($('#video_attachment')[0].files, function(i, file) {
						// 		formData.append('video_attachment['+i+']', file);
						// 	});
						// }
						if($('.attachment_names').length){
							$('.attachment_names').each(function(i, att_name) {
								name = $(att_name).val();
								id = $(att_name).data('id');
								console.log(name);
								console.log(id);
								// attachment_names[id] = name;
								formData.append('attachment_names['+id+']', name);
							});
						}
						console.log($('#video-attch2').length);
						saveLesson("save_btn", formData);
						setTimeout( function(){
							toastr.success(success_message);
						}, 500);
					}
					if($('#lesson_type').val() == 'Q'){
						var quizFormData = $("#save_lesson_form").serializeArray();
						// console.log(quizFormData);
						var n = parseInt($('#quiz_count').val());
						console.log("QUIZ COUNT : "+n);
						var questions = [];
						var flag = 0;
						for(i=1; i<=n; i++){
							var val = $("input[name='correct"+i+"']:checked").val();
							if($("#answer"+i+"_"+val).val() == ""){
								flag = 1;
								$("#faqnewhead"+i).find('sup').show();
								console.log($("#faqnewhead"+i)[0]);
								var html = `<label for="answer${i}_${val}" class="error w-100">@lang('client_site.text_for_correct')</label>`
								$(html).insertBefore(`input[name='correct${i}']:checked`);
							}
						}
						if(flag == 0){
							setTimeout( function(){
								$('.save_lesson_btn').hide();
								$('.save_lesson_btn').parent().find('.discard_lesson').hide();
								$('.save_lesson_btn').parent().find('.custom-checkbox').hide();
								$('.save_lesson_btn').parent().find('.tools-tip').hide();
								$('.save_lesson_btn').parent().find('.progrs').remove();
								$('.save_lesson_btn').parent().append(`<div class="progrs">@lang('client_site.saving_for_progress')...</div>`);
							},10);

							for(i=1; i<=n; i++){
								var q = {};
								$.each(quizFormData, function(k,data){
									if(data.name == ("question"+i)){
										q['question'] = tinymce.get("question"+i).getContent();
									}
									if(data.name == ("explanation"+i)){
										q['explanation'] = tinymce.get("explanation"+i).getContent();
									}
									if(data.name == ("correct"+i)){
										q['correct'] = $("input[name='correct"+i+"']:checked").val();
									}
									if(data.name == ("ID_"+i)){
										q['id'] = $('#quiz_id'+i).val();
									}
									for(j=1; j<=6; j++){
										// console.log("J: "+j);
										if(data.name == ("answer"+i+"_"+j)){
											q['answer_'+j] = tinymce.get("answer"+i+"_"+j).getContent();
										}
									}
								});
								questions.push(q);
							}
							console.log(questions);
							var newQuestions = questions.filter(value => Object.keys(value).length !== 0);
							console.log(newQuestions);

							console.log($('.save_lesson_btn')[0]);

							var formData = new FormData();
							formData.append("jsonrpc", "2.0");
							formData.append("_token", '{{csrf_token()}}');
							formData.append("lesson_title", $('#lesson_title_Q').val());
							formData.append("lesson_type", $('#lesson_type').val());
							formData.append("lesson_id", $('#lesson_id').val());
							formData.append("chapter_id", $('#chapter_id').val());
							formData.append("product_id", '{{@$product->id}}');
							formData.append("duration", $('#duration_Q').val());
							formData.append("is_allow_discussion", is_allow_discussion);
							formData.append("is_prerequisite", is_prerequisite);
							formData.append("is_free_preview", is_free_preview);
							formData.append("status", ($('#chk6').is(":checked") && $('#chk5').is(":checked")) ? 'D' : 'A');
							formData.append("questions", JSON.stringify(newQuestions));

							saveLesson("0", formData);
							var lid = $('#lesson_id').val();
							var cid = $('#chapter_id').val();
							var ltp = $('#lesson_type').val();
							loadLessons(lid,cid,ltp);
							setTimeout( function(){
								toastr.success(success_message);
							}, 500);
						}
					}
					if($('#lesson_type').val() == 'M'){
						var formData = new FormData();
						formData.append("jsonrpc", "2.0");
						formData.append("_token", '{{csrf_token()}}');
						formData.append("lesson_title", $('#lesson_title_M').val());
						formData.append("lesson_type", $('#lesson_type').val());
						formData.append("lesson_id", $('#lesson_id').val());
						formData.append("chapter_id", $('#chapter_id').val());
						formData.append("product_id", '{{@$product->id}}');
						formData.append("duration", $('#duration_M').val());
						formData.append("status", ($('#chk6').is(":checked") && $('#chk5').is(":checked")) ? 'D' : 'A');
						formData.append("source_url", $('#source_url').val());

						saveLesson("save_btn", formData);
						setTimeout( function(){
							toastr.success(success_message);
						}, 500);
					}
					if($('#lesson_type').val() == 'T'){
						var formData = new FormData();
						formData.append("jsonrpc", "2.0");
						formData.append("_token", '{{csrf_token()}}');
						formData.append("lesson_title", $('#lesson_title_T').val());
						formData.append("lesson_type", $('#lesson_type').val());
						formData.append("lesson_id", $('#lesson_id').val());
						formData.append("chapter_id", $('#chapter_id').val());
						formData.append("product_id", '{{@$product->id}}');
						formData.append("duration", $('#duration_T').val());
						formData.append("status", ($('#chk6').is(":checked") && $('#chk5').is(":checked")) ? 'D' : 'A');
						formData.append("description", $('#description').val());

						saveLesson("save_btn", formData);
						setTimeout( function(){
							toastr.success(success_message);
						}, 500);
					}
					if($('#lesson_type').val() == 'A'){
						var formData = new FormData();
						formData.append("jsonrpc", "2.0");
						formData.append("_token", '{{csrf_token()}}');
						formData.append("lesson_title", $('#lesson_title_A').val());
						formData.append("lesson_type", $('#lesson_type').val());
						formData.append("lesson_id", $('#lesson_id').val());
						formData.append("chapter_id", $('#chapter_id').val());
						formData.append("product_id", '{{@$product->id}}');
						formData.append("duration", $('#duration_A').val());
						formData.append("status", ($('#chk6').is(":checked") && $('#chk5').is(":checked")) ? 'D' : 'A');
						formData.append("description", $('#audio_description').val());
						// formData.append("audio", $('#audio-attch2')[0].files[0]);

						saveLesson("save_btn", formData);
						setTimeout( function(){
							toastr.success(success_message);
						}, 500);
					}
					if($('#lesson_type').val() == 'D'){
						var formData = new FormData();
						formData.append("jsonrpc", "2.0");
						formData.append("_token", '{{csrf_token()}}');
						formData.append("lesson_title", $('#lesson_title_D').val());
						formData.append("lesson_type", $('#lesson_type').val());
						formData.append("lesson_id", $('#lesson_id').val());
						formData.append("chapter_id", $('#chapter_id').val());
						formData.append("product_id", '{{@$product->id}}');
						formData.append("duration", $('#duration_D').val());
						formData.append("status", ($('#chk6').is(":checked") && $('#chk5').is(":checked")) ? 'D' : 'A');
						formData.append("description", $('#download_description').val());
						if($('.download_names').length){
							$('.download_names').each(function(i, att_name) {
								name = $(att_name).val();
								id = $(att_name).data('id');
								formData.append('download_names['+id+']', name);
							});
						}
						saveLesson("save_btn", formData);
						setTimeout( function(){
							toastr.success(success_message);
						}, 500);
					}
					if($('#lesson_type').val() == 'P'){
						var formData = new FormData();
						formData.append("jsonrpc", "2.0");
						formData.append("_token", '{{csrf_token()}}');
						formData.append("lesson_title", $('#lesson_title_P').val());
						formData.append("lesson_type", $('#lesson_type').val());
						formData.append("lesson_id", $('#lesson_id').val());
						formData.append("chapter_id", $('#chapter_id').val());
						formData.append("product_id", '{{@$product->id}}');
						formData.append("duration", $('#duration_P').val());
						formData.append("status", ($('#chk6').is(":checked") && $('#chk5').is(":checked")) ? 'D' : 'A');
						// var slide_count = $('.presentation_slide').length;
						// for(i=1; i<=slide_count; i++){
						// 	console.log("SLIDE COUNT : "+i);
						// 	val = tinymce.get("slide_description_"+i).getContent();
						// 	id = $('.slide_description_'+i).data('id');
						// 	formData.append('slide_descriptions['+id+']', val);
						// }
						if($('.slide_description').length){
							$('.slide_description').each(function(i, desc) {
								val = $(desc).val();
								id = $(desc).data('id');
								console.log(val);
								console.log(id);
								formData.append('slide_descriptions['+id+']', val);
							});
						}
						saveLesson("save_btn", formData);
						setTimeout( function(){
							toastr.success(success_message);
						}, 500);
					}
				} else {
					var validator = $("#save_lesson_form").validate();
					console.log(validator.errorList);
					$(validator.errorList).each(function(i, error){
						element = error.element;
						if( $(element).attr('id').includes("question") || $(element).attr('id').includes("answer") || $(element).attr('id').includes("explanation") || $(element).hasClass('inp_radio') ){
							if($(element).attr('id').includes("question")){
								var id = $(element).attr('id').slice(8);
								$("#faqnewhead"+id).find('sup').show();
								console.log($("#faqnewhead"+id)[0]);
								// toastr.error("Please choose the correct answer for question" + id);
							}
							if($(element).attr('id').includes("answer")){
								var id = $(element).attr('id').split("_")[0].slice(6);
								$("#faqnewhead"+id).find('sup').show();
								console.log($("#faqnewhead"+id)[0]);
							}
							if($(element).attr('id').includes("explanation")){
								var id = $(element).attr('id').slice(11);
								$("#faqnewhead"+id).find('sup').show();
								console.log($("#faqnewhead"+id)[0]);
							}
							if($(element).hasClass('inp_radio')){
								var id = $(element).attr('id').split("_")[0].slice(7);
								$("#faqnewhead"+id).find('sup').show();
								console.log($("#faqnewhead"+id)[0]);
								// toastr.error("Please choose the correct answer for question" + id);
							}
						} else {
							toastr.error(error.message);
						}
					});
					setTimeout( function(){
						$('.save_lesson_btn').parent().find('.progrs').remove();
						$('.save_lesson_btn').show();
						$('.save_lesson_btn').parent().find('.discard_lesson').show();
						$('.save_lesson_btn').parent().find('.custom-checkbox').show();
						$('.save_lesson_btn').parent().find('.tools-tip').show();
					},500);
					// if(quizErrMsg != "") {
					// 	showErrorMsg(quizErrMsg);
					// } else {
					// 	showErrorMsg("Please fill in all fields");
					// }
				}
			});
			$('#delete_lesson').click(function(){
				var id = $('#delete_lesson').data('id');
				console.log("ID : "+id);
				// window.location.href = "{{url('/')}}/delete-lesson/"+id;
				$.get( "{{url('/')}}/delete-lesson/"+id+"/ajax", function( res ) {
					if(res.status == "success"){
						console.log(res.message);
						toastr.success(res.message);
						$('#lesson_id').val('');
						var elem = $('.active_chapter').parent().parent().find('.session-box li');
						loadLessonCards(Chapter.id, elem);
						$('.right-section').hide();
						$('.start_section').show();
					}
				});

			});
			$('#is_main_preview_video').change(function(e){
				e.preventDefault();
				console.log(freelesson);
				console.log($('#lesson_id').val());
				if(freelesson != '' && $('#is_main_preview_video').is(":checked") && freelesson != $('#lesson_id').val()){
					swal({
						title: "@lang('client_site.preview_video_lesson_exists')",
						text: "@lang('client_site.change_preview_video_lesson')",
						icon: "warning",
						buttons: true,
						dangerMode: true,
						buttons: ['@lang("site.no")', '@lang("site.yes")']
					})
					.then((willDelete) => {
						if (willDelete) {
							$('#is_main_preview_video').prop('checked', true);
							return false;
						} else {
							$('#is_main_preview_video').prop('checked', false);
							return false;
						}
					});
				}
			});
			$(document).delegate('.session_info', 'click', function(){
				if(!$(this).hasClass('active_lesson')){
					$('#delete_lesson').show();
					var lesson_id = $(this).data('id');
					var chapter_id = $(this).data('chapter');
					var ltype = $(this).data('ltype');
					if($('.active_lesson').length) $('.active_lesson').removeClass('active_lesson');
					$(this).addClass('active_lesson');
					$('.right-section').hide();
					var flag = 0;
					if (upload_progress == true && upload_progress_lesson_id == lesson_id) {
						// still uploading
						console.log("Still Uploading");
					} else {
						loadLessons(lesson_id,chapter_id,ltype);
					}
					$('.mobile_chapter_list').hide("slide", { direction: "left" }, 500);
					// finally show section
					$('.lessons_section').show();
					var ltype = $(this).data('ltype');
					$('#lesson_type').val(ltype);
					$('.lesson_section').each(function(i, lesson_section){
						$(lesson_section).hide();
					});
					console.log($('#for_'+ltype)[0]);
					$('#for_'+ltype).show();
					if ($(".latest_lesson")[0] && upload_progress == false){
						$('.latest_lesson').remove();
					}
				}
			});
			$('.lesson_title').keyup(function(){
				$('#lsn_title').text($(this).val());
				$('.active_lesson').find('.lesson-se > span').text($(this).val());
			});
			function readURL(input) {
				if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function(e) {
					$('.uploaded_ppc').show();
					var f = input;
					var size = input.files[0].size;
					console.log(size);
				}

				reader.readAsDataURL(input.files[0]);
				}
			}
			function readMultipleURL(files) {
				var reader = new FileReader();
				function readFile(index) {
					if( index >= files.length ) return;
					var file = files[index];
					reader.onload = function(e) {
					// get file content
					var bin = e.target.result;
					// do sth with bin
					readFile(index+1)
					}
					reader.readAsBinaryString(file);
				}
				readFile(0);
			}
		// Add/Edit/Delete Lesson


		// Video
			$(document).on('dragover dragleave drop', '.dropbx', function(event) {
				event.preventDefault();
				event.stopPropagation();
			});
			$('.dropbx').on('dragover dragenter', function(e){
				e.preventDefault();
				$(this).css("border-style", "solid");
			});
			$(document).on('dragover dragleave drop', '.dropbx_vimeo', function(event) {
				event.preventDefault();
				event.stopPropagation();
			});
			$('.dropbx_vimeo').on('dragover dragenter', function(e){
				e.preventDefault();
				$(this).css("border-style", "solid");
			});
			$(document).on('dragover dragleave drop', '.video_attachment', function(event) {
				event.preventDefault();
				event.stopPropagation();
			});
			$('.video_attachment').on('dragover dragenter', function(e){
				e.preventDefault();
				$(this).css("border-style", "solid");
			});
			$('.dropbx').bind('dragleave drop', function(e){
				e.preventDefault();
				if (upload_progress == true) {
					toastr.error("@lang('site.upload_in_progress')");
					$('#video-attch2').val('');
				} else {
					$('#progress_list_video').show();
					$(this).css("border-style", "dashed");
					$('#video-attch2')[0].files = e.originalEvent.dataTransfer.files;

					const validImageTypes = ['video/mp4'];
					var mimetype = $('#video-attch2')[0].files[0]['type'];
					var size = $('#video-attch2')[0].files[0]['size'];
					var name = $('#video-attch2')[0].files[0]['name'];
					console.log(size);
					console.log(mimetype);
					if (!validImageTypes.includes(mimetype)) {
						$('.video_msg').html(`<label class="error">${name} não é um arquivo de vídeo válido.</label>`);
						$('#video-attch2').val('');
					} else if (size > vdo_size_limit) {
						$('.video_msg').html(`<label class="error">${name} ié maior que 200 MB.</label>`);
						$('#video-attch2').val('');
					} else {
						$('.save_lesson_btn').hide();
						$('.save_lesson_btn').parent().find('.discard_lesson').hide();
						$('.save_lesson_btn').parent().find('.custom-checkbox').hide();
						$('.save_lesson_btn').parent().find('.tools-tip').hide();
						$('.save_lesson_btn').parent().find('.progrs').remove();
						$('.save_lesson_btn').parent().append(`<div class="progrs">@lang('client_site.saving_for_progress')...</div>`);
						getTime(0, $('#video-attch2')[0].files[0], "video");
					}
				}
			});

            $('.dropbx_vimeo').bind('dragleave drop', function(e){
				e.preventDefault();
				if (upload_progress == true) {
					toastr.error("@lang('site.upload_in_progress')");
					$('#video-attch3').val('');
				} else {
					$('#progress_list_video').show();
					$(this).css("border-style", "dashed");
					$('#video-attch3')[0].files = e.originalEvent.dataTransfer.files;

					const validImageTypes = ['video/mp4'];
					var mimetype = $('#video-attch3')[0].files[0]['type'];
					var size = $('#video-attch3')[0].files[0]['size'];
					var name = $('#video-attch3')[0].files[0]['name'];
					console.log(size);
					console.log(mimetype);
					if (!validImageTypes.includes(mimetype)) {
						$('.video_msg').html(`<label class="error">${name} não é um arquivo de vídeo válido.</label>`);
						$('#video-attch3').val('');
					} else if (size > vdo_size_limit) {
						$('.video_msg').html(`<label class="error">${name} ié maior que 200 MB.</label>`);
						$('#video-attch3').val('');
					} else {
						$('.save_lesson_btn').hide();
						$('.save_lesson_btn').parent().find('.discard_lesson').hide();
						$('.save_lesson_btn').parent().find('.custom-checkbox').hide();
						$('.save_lesson_btn').parent().find('.tools-tip').hide();
						$('.save_lesson_btn').parent().find('.progrs').remove();
						$('.save_lesson_btn').parent().append(`<div class="progrs">@lang('client_site.saving_for_progress')...</div>`);
						getTime(0, $('#video-attch3')[0].files[0], "video");
					}
				}
			});
			$('#video-attch2').change(function(){
				if (upload_progress == true) {
					toastr.error("@lang('site.upload_in_progress')");
					$('#video-attch2').val('');
				} else {
					$('#progress_list_video').show();
					const validImageTypes = ['video/mp4'];
					var mimetype = $('#video-attch2')[0].files[0]['type'];
					var size = $('#video-attch2')[0].files[0]['size'];
					var name = $('#video-attch2')[0].files[0]['name'];
					console.log(size);
					console.log(mimetype);
					if (!validImageTypes.includes(mimetype)) {
						$('.video_msg').html(`<label class="error">${name} não é um arquivo de vídeo válido.</label>`);
						$('#video-attch2').val('');
					} else if (size > vdo_size_limit) {
						$('.video_msg').html(`<label class="error">${name} ié maior que 200 MB.</label>`);
						$('#video-attch2').val('');
					} else {
						$('.save_lesson_btn').hide();
						$('.save_lesson_btn').parent().find('.discard_lesson').hide();
						$('.save_lesson_btn').parent().find('.custom-checkbox').hide();
						$('.save_lesson_btn').parent().find('.tools-tip').hide();
						$('.save_lesson_btn').parent().find('.progrs').remove();
						$('.save_lesson_btn').parent().append(`<div class="progrs">@lang('client_site.saving_for_progress')...</div>`);
						getTime(0, $('#video-attch2')[0].files[0], "video");
					}
				}
			});

            $('#video-attch3').change(function(){
				if (upload_progress == true) {
					toastr.error("@lang('site.upload_in_progress')");
					$('#video-attch3').val('');
				} else {
                    $('.video_msg1').empty();
                    $('.video_msg').empty();
					$('#progress_list_video').show();
					const validImageTypes = ['video/mp4'];
					var mimetype = $('#video-attch3')[0].files[0]['type'];
					var size = $('#video-attch3')[0].files[0]['size'];
					var name = $('#video-attch3')[0].files[0]['name'];
					console.log(size);
					console.log(mimetype);
					if (!validImageTypes.includes(mimetype)) {
						$('.video_msg1').html(`<label class="error">${name} não é um arquivo de vídeo válido.</label>`);
						$('#video-attch3').val('');
					} else if (size > vdo_size_limit) {
						$('.video_msg1').html(`<label class="error">${name} ié maior que 200 MB.</label>`);
						$('#video-attch3').val('');
					} else {
						$('.save_lesson_btn').hide();
						$('.save_lesson_btn').parent().find('.discard_lesson').hide();
						$('.save_lesson_btn').parent().find('.custom-checkbox').hide();
						$('.save_lesson_btn').parent().find('.tools-tip').hide();
						$('.save_lesson_btn').parent().find('.progrs').remove();
						$('.save_lesson_btn').parent().append(`<div class="progrs">@lang('client_site.saving_for_progress')...</div>`);
						getTime(0, $('#video-attch3')[0].files[0], "video");
					}
				}
			});

			// $('.rem_vdo').click(function(){
			// 	console.log("REmove");
			// 	var id = $('#lesson_id').val();
			// 	console.log(id);
			// 	$.get( "{{url('/')}}/delete-main-video/"+id, function( res ) {
			// 		console.log(res);
			// 		// if(res.status == "success"){
			// 		// }
			// 	});
			// });
			function removeMainVideo(){
				var id = $('#lesson_id').val();
				console.log(id);
				$.get( "{{url('/')}}/delete-main-video/"+id, function( res ) {
					console.log(res);
					if(res.status == "success"){
						toastr.success(res.message);
						if($('#show_vdo_section').length) $('#show_vdo_section').remove();
						$('#upload_vdo_section').show();
						lesson_main_video_required = true;
						$('#thumb').attr('src','');
						$('#thumb').hide();
						$('#chk6').prop('checked', true);
						$('#chk5').prop('checked', true);
						$('.active_lesson').find('.tool-tip').find('.badge--warning').remove();
						$('.active_lesson').find('.tool-tip').append(`<span class="ml-2 badge badge--warning">@lang("client_site.draft")</span>`);
						$('#video_url').val('');
						$('#video-attch2').val('');
					}
				});
			}
			function videoSave() {
				return new Promise(resolve => {
					setTimeout(() => {
						if($('#video-attch2')[0].files.length){
							var name = $('#video-attch2')[0].files[0]['name'];
							if($('#lesson_id').val() == ""){
								console.log("Entering");
								var formData = new FormData();
								formData.append("jsonrpc", "2.0");
								formData.append("_token", '{{csrf_token()}}');
								formData.append("lesson_title", $('#lesson_title_V').val());
								formData.append("lesson_type", $('#lesson_type').val());
								formData.append("lesson_id", $('#lesson_id').val());
								formData.append("chapter_id", $('#chapter_id').val());
								formData.append("product_id", '{{@$product->id}}');
								formData.append("is_allow_discussion", $('#is_allow_discussion').val());
								formData.append("is_prerequisite", $('#is_prerequisite').val());
								formData.append("is_free_preview", $('#is_free_preview').val());
								saveLesson("0", formData);
								console.log(success_message);
							} else {
								console.log("Outside");
							}
							var html =
							`<li class="vdo_upload_li">
								<div class="upload-info">
									<div class="video-pro-info"><p>${name} (@lang('client_site.queued_for_upload'))</p>
										<div class="progress-bars">
											<div class="vdo_bar" style="width:0%">
												<p class="percent">0%</p>
											</div>
										</div>
									</div>
									<button type="button" class="button--default--small button--secondary_n" id="stopUpload">@lang('client_site.cancel')</button>
								</div>
							</li>`;
							$('.progress-ul-video').append(html);
							var uploadFormData = new FormData();
							uploadFormData.append("jsonrpc", "2.0");
							uploadFormData.append("_token", '{{csrf_token()}}');
							uploadFormData.append("lesson_title", $('#lesson_title_V').val());
							uploadFormData.append("lesson_type", $('#lesson_type').val());
							uploadFormData.append("lesson_id", $('#lesson_id').val());
							uploadFormData.append("chapter_id", $('#chapter_id').val());
							uploadFormData.append("product_id", '{{@$product->id}}');
							uploadFormData.append("file", $('#video-attch2')[0].files[0]);
							uploadFormData.append("duration", $("#file_duration").val());
							console.log("Inside function");
							ajaxCall = $.ajax({
								url: "{{route('upload.video')}}",
								type: 'post',
								data: uploadFormData,
								dataType: 'json',
								contentType: false,
								cache: false,
								processData:false,
								xhr: function(){
									//upload Progress
									var xhr = $.ajaxSettings.xhr();
									if (xhr.upload) {
										xhr.upload.addEventListener('progress', function(event) {
											var percent = 0;
											var position = event.loaded || event.position;
											var total = event.total;
											if (event.lengthComputable) {
												percent = Math.ceil(position / total * 100);
											}
											//update progressbar
											console.log(percent);
											$('.vdo_bar').css('width', + percent +"%");
											$('.vdo_bar'+' .percent').text(percent +" %");
											upload_progress = true;
											upload_progress_lesson_id = $('#lesson_id').val();
											// console.log($('.vdo_bar')[0]);
											// console.log($('.vdo_bar'+' .percent')[0]);
											if(percent == 100){
												// console.log("Getting to 100");
												$('.vdo_upload_li').find('.video-pro-info > p').text(`${name} (Processing...)`);
												$('#stopUpload').attr("disabled", true);
												$('#stopUpload').addClass('button--default--disabled');
												upload_progress = false;
											}
										}, true);
									}
									return xhr;
								},
								mimeType:"multipart/form-data",
								success: function(result) {
									console.log(result);
									setTimeout( function(){
										if(result.status == "success"){
											lesson_main_video_required = false;
											$('.vdo_upload_li').remove();
											if($('#show_vdo_section').length) $('#show_vdo_section').remove();
											var html3 = `<div class="form-row" id="show_vdo_section"><video src="{{url('/')}}/storage/app/public/lessons/type_video/${result.main_video.filename}" width="100%" height="320" controls></video><a href="javascript:;" class="pull-right mt-3 rem_vdo" onclick="removeMainVideo()">@lang('client_site.remove_video')</a></div>`;
											$(html3).insertAfter($("#progress_list_video"));
											$('#upload_vdo_section').hide();
											$('#progress_list_video').hide();
											$('.progress-ul-video').empty();
											$('#stopUpload').removeAttr('disabled');
											$('#stopUpload').removeClass('button--default--disabled');
											$('#upload_thumbnail_section').show();
											$('.save_lesson_btn').parent().find('.progrs').remove();
											$('.save_lesson_btn').show();
											$('.save_lesson_btn').parent().find('.discard_lesson').show();
											$('.save_lesson_btn').parent().find('.custom-checkbox').show();
											$('.save_lesson_btn').parent().find('.tools-tip').show();
										}
									},1000);
								},
								error: function(err) {
									console.log(err);
								}
							});
						}

                        if($('#video-attch3')[0].files.length){
							var name = $('#video-attch3')[0].files[0]['name'];
							if($('#lesson_id').val() == ""){
								console.log("Entering");
								var formData = new FormData();
								formData.append("jsonrpc", "2.0");
								formData.append("_token", '{{csrf_token()}}');
								formData.append("lesson_title", $('#lesson_title_V').val());
								formData.append("lesson_type", $('#lesson_type').val());
								formData.append("lesson_id", $('#lesson_id').val());
								formData.append("chapter_id", $('#chapter_id').val());
								formData.append("product_id", '{{@$product->id}}');
								formData.append("is_allow_discussion", $('#is_allow_discussion').val());
								formData.append("is_prerequisite", $('#is_prerequisite').val());
								formData.append("is_free_preview", $('#is_free_preview').val());
                                formData.append("videoType", 'VM');
								saveLesson("0", formData);
								console.log(success_message);
							} else {
								console.log("Outside");
							}
							var html =
							`<li class="vdo_upload_li">
								<div class="upload-info">
									<div class="video-pro-info"><p>${name} (@lang('client_site.queued_for_upload'))</p>
										<div class="progress-bars">
											<div class="vdo_bar" style="width:0%">
												<p class="percent">0%</p>
											</div>
										</div>
									</div>
									<button type="button" class="button--default--small button--secondary_n" id="stopUpload">@lang('client_site.cancel')</button>
								</div>
							</li>`;
							$('.progress-ul-video').append(html);
							var uploadFormData = new FormData();
							uploadFormData.append("jsonrpc", "2.0");
							uploadFormData.append("_token", '{{csrf_token()}}');
							uploadFormData.append("lesson_title", $('#lesson_title_V').val());
							uploadFormData.append("lesson_type", $('#lesson_type').val());
							uploadFormData.append("lesson_id", $('#lesson_id').val());
							uploadFormData.append("chapter_id", $('#chapter_id').val());
							uploadFormData.append("product_id", '{{@$product->id}}');
							uploadFormData.append("file", $('#video-attch3')[0].files[0]);
							uploadFormData.append("duration", $("#file_duration").val());
							uploadFormData.append("videoType", 'VM');
							console.log("Inside function");
							ajaxCall = $.ajax({
								url: "{{route('upload.video')}}",
								type: 'post',
								data: uploadFormData,
								dataType: 'json',
								contentType: false,
								cache: false,
								processData:false,
								xhr: function(){
									//upload Progress
									var xhr = $.ajaxSettings.xhr();
									if (xhr.upload) {
										xhr.upload.addEventListener('progress', function(event) {
											var percent = 0;
											var position = event.loaded || event.position;
											var total = event.total;
											if (event.lengthComputable) {
												percent = Math.ceil(position / total * 100);
											}
											//update progressbar
											console.log(percent);
											$('.vdo_bar').css('width', + percent +"%");
											$('.vdo_bar'+' .percent').text(percent +" %");
											upload_progress = true;
											upload_progress_lesson_id = $('#lesson_id').val();
											// console.log($('.vdo_bar')[0]);
											// console.log($('.vdo_bar'+' .percent')[0]);
											if(percent == 100){
												// console.log("Getting to 100");
												$('.vdo_upload_li').find('.video-pro-info > p').text(`${name} (Processing...)`);
												$('#stopUpload').attr("disabled", true);
												$('#stopUpload').addClass('button--default--disabled');
												upload_progress = false;
											}
										}, true);
									}
									return xhr;
								},
								mimeType:"multipart/form-data",
								success: function(result) {
									console.log(result);
									setTimeout( function(){
										if(result.status == "success"){
											lesson_main_video_required = false;
											$('.vdo_upload_li').remove();
											if($('#show_vdo_section').length) $('#show_vdo_section').remove();
											// var html3 = `<div class="form-row" id="show_vdo_section"><video src="{{url('/')}}/storage/app/public/lessons/type_video/${result.main_video.filename}" width="100%" height="320" controls></video><a href="javascript:;" class="pull-right mt-3 rem_vdo" onclick="removeMainVideo()">@lang('client_site.remove_video')</a></div>`;
											// var html3 = `<div class="form-row" id="show_vdo_section">
                                            //     <iframe src="${result.main_video.filename}" width="100%" height="320" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

                                            //     <a href="javascript:;" class="pull-right mt-3 rem_vdo" onclick="removeMainVideo()">@lang('client_site.remove_video')</a></div>`;

											var html3 = `<div class="form-row" id="show_vdo_section">
                                                <h3>Video uploaded successfully video previw available comming soon</h3>

                                                <a href="javascript:;" class="pull-right mt-3 rem_vdo" onclick="removeMainVideo()">@lang('client_site.remove_video')</a></div>`;
											$(html3).insertAfter($("#progress_list_video"));
											$('#upload_vdo_section').hide();
											$('#progress_list_video').hide();
											$('.progress-ul-video').empty();
											$('#stopUpload').removeAttr('disabled');
											$('#stopUpload').removeClass('button--default--disabled');
											$('#upload_thumbnail_section').show();
											$('.save_lesson_btn').parent().find('.progrs').remove();
											$('.save_lesson_btn').show();
											$('.save_lesson_btn').parent().find('.discard_lesson').show();
											$('.save_lesson_btn').parent().find('.custom-checkbox').show();
											$('.save_lesson_btn').parent().find('.tools-tip').show();
										}
									},1000);
								},
								error: function(err) {
									console.log(err);
								}
							});
						}
					},1000);
				});
			}
			$(document).on('click','#stopUpload', function(e){
				ajaxCall.abort();
				$('#progress_list_video').hide();
				$('.progress-ul-video').empty();
				$('#video-attch2').val('');
				$('.save_lesson_btn').show();
				$('.save_lesson_btn').parent().find('.progrs').remove();
				$('.save_lesson_btn').parent().find('.discard_lesson').show();
				$('.save_lesson_btn').parent().find('.custom-checkbox').show();
				$('.save_lesson_btn').parent().find('.tools-tip').show();
				var id = $('#lesson_id').val();
				$.get( "{{url('/')}}/delete-lesson/"+id+"/ajax", function( res ) {
					if(res.status == "success"){
						console.log(res.message);
						$('#lesson_id').val('');
					}
				});
			});
			$('#thumbnail').change(function(){
				$('#thumb').attr('src', window.URL.createObjectURL($('#thumbnail')[0].files[0]));
				$('#thumb').show();
				var formData = new FormData();
				formData.append("jsonrpc", "2.0");
				formData.append("_token", '{{csrf_token()}}');
				formData.append("lesson_title", $('#lesson_title_V').val());
				formData.append("lesson_type", $('#lesson_type').val());
				formData.append("lesson_id", $('#lesson_id').val());
				formData.append("chapter_id", $('#chapter_id').val());
				formData.append("product_id", '{{@$product->id}}');
				formData.append("is_allow_discussion", $('#is_allow_discussion').val());
				formData.append("is_prerequisite", $('#is_prerequisite').val());
				formData.append("is_free_preview", $('#is_free_preview').val());
				formData.append("duration", $("#duration_V").val());
				formData.append("thumbnail", $('#thumbnail')[0].files[0]);
				saveLesson("0", formData);
			});

			$('#video_attachment').bind('dragleave drop', function(e){
				e.preventDefault();
				if($('#show_vdo_section').length == 0){
                    swal('@lang("client_site.upload_video_first")',{icon:"warning"});
				} else if (upload_progress == true) {
					toastr.error("@lang('site.upload_in_progress')");
					$('#video_attachment').val('');
				} else {
					$(this).css("border-style", "dashed");
					$('#progress_list_video_atch').show();
					if(!$('#video_attachment_list').length){
						var html4 = `<ul class="delete-deta-ul mt-2" id="video_attachment_list"></ul>`;
						$(html4).insertAfter($("#progress_list_video_atch"));
					}
					$('#video_attachment')[0].files = e.originalEvent.dataTransfer.files;
					var err_msg = "", xxx=0;
					while (xxx < $('#video_attachment')[0].files.length) {
						$('body').append(`<input type="text" class="attachment_file_duration" id="file_duration_${xxx}" />`);
						xxx++;
					}
					$.each($('#video_attachment')[0].files, function(i, file) {
						if($('#video_attachment')[0].files.length){
							const invalidImageTypes = ['application/bat'];
							var mimetype = file['type'];
							var size = file['size'];
							var name = file['name'];
							console.log(mimetype);
							if (invalidImageTypes.includes(mimetype)) {
								err_msg += `<label class="error">${name} não é um arquivo de download válido.</label><br>`
								$('.video_attachment_msg').html(err_msg);
							} else if (size > vdo_file_size_limit) {
								err_msg += `<label class="error">${name} ié maior que 200 MB.</label>`;
								$('.video_attachment_msg').html(err_msg);
							} else {
								if(i == 0){
									$('.save_lesson_btn').hide();
									$('.save_lesson_btn').parent().find('.discard_lesson').hide();
									$('.save_lesson_btn').parent().find('.custom-checkbox').hide();
									$('.save_lesson_btn').parent().find('.tools-tip').hide();
									$('.save_lesson_btn').parent().find('.progrs').remove();
									$('.save_lesson_btn').parent().append(`<div class="progrs">@lang('client_site.saving_for_progress')...</div>`);
								}
								getTime(i, file, "video_attachment");
							}
						}
					});
				}
			});
			$('#video_attachment').change(function(){
				if($('#show_vdo_section').length == 0){
                    swal('@lang("client_site.upload_video_first")',{icon:"warning"});
				} else if (upload_progress == true) {
					toastr.error("@lang('site.upload_in_progress')");
					$('#video_attachment').val('');
				} else {
					var err_msg = "", xxx=0;
					while (xxx < $('#video_attachment')[0].files.length) {
						$('body').append(`<input type="text" class="attachment_file_duration" id="file_duration_${xxx}" />`);
						xxx++;
					}
					$.each($('#video_attachment')[0].files, function(i, file) {
						if($('#video_attachment')[0].files.length){
							const invalidImageTypes = ['application/bat'];
							var mimetype = file['type'];
							var size = file['size'];
							var name = file['name'];
							// console.log(mimetype);
							if (invalidImageTypes.includes(mimetype)) {
								err_msg += `<label class="error">${name} não é um arquivo de download válido.</label><br>`
								$('.video_attachment_msg').html(err_msg);
							} else if (size > vdo_file_size_limit) {
								err_msg += `<label class="error">${name} ié maior que 200 MB.</label>`;
								$('.video_attachment_msg').html(err_msg);
							} else {
								if(i == 0){
									$('.save_lesson_btn').hide();
									$('.save_lesson_btn').parent().find('.discard_lesson').hide();
									$('.save_lesson_btn').parent().find('.custom-checkbox').hide();
									$('.save_lesson_btn').parent().find('.tools-tip').hide();
									$('.save_lesson_btn').parent().find('.progrs').remove();
									$('.save_lesson_btn').parent().append(`<div class="progrs">@lang('client_site.saving_for_progress')...</div>`);
								}
								getTime(i, file, "video_attachment");
							}
						}
					});
				}
			});
			function videoAttachmentSave(i, file){
				return new Promise(resolve => {
					setTimeout(() => {
						$('#progress_list_video_atch').show();
						var name = file['name'];
						var html =
						`<li class="upload_li_atch${i}">
							<div class="upload-info">
								<div class="video-pro-info"><p>${name} (@lang('client_site.queued_for_upload'))</p>
									<div class="progress-bars">
										<div class="bar_atch bar_atch${i}" style="width:0%">
											<p class="percent">0%</p>
										</div>
									</div>
								</div>
								<button class="button--default--small button--secondary_n stopAtchUpload" id="stopAtchUpload${i}" data-id="${i}">@lang('client_site.cancel')</button>
							</div>
						</li>`;
						$('.progress-ul-video-atch').append(html);
						var uploadFormData = new FormData();
						uploadFormData.append("jsonrpc", "2.0");
						uploadFormData.append("_token", '{{csrf_token()}}');
						uploadFormData.append("lesson_title", $('#lesson_title_V').val());
						uploadFormData.append("lesson_type", $('#lesson_type').val());
						uploadFormData.append("lesson_id", $('#lesson_id').val());
						uploadFormData.append("chapter_id", $('#chapter_id').val());
						uploadFormData.append("product_id", '{{@$product->id}}');
						uploadFormData.append("duration", $("#file_duration_"+i).val());
						uploadFormData.append("file", file);

						attchAjaxCall[i] = $.ajax({
							url: "{{route('upload.attachments')}}",
							type: 'post',
							data: uploadFormData,
							dataType: 'json',
							contentType: false,
							cache: false,
							processData:false,
							xhr: function(){
								//upload Progress
								var xhr = $.ajaxSettings.xhr();
								if (xhr.upload) {
									xhr.upload.addEventListener('progress', function(event) {
										var percent = 0;
										var position = event.loaded || event.position;
										var total = event.total;
										if (event.lengthComputable) {
											percent = Math.ceil(position / total * 100);
										}
										//update progressbar
										console.log(percent);
										$('.bar_atch'+i).css('width', + percent +"%");
										$('.bar_atch'+i+' .percent').text(percent +" %");
										upload_progress = true;
										upload_progress_lesson_id = $('#lesson_id').val();
										if(percent == 100){
											$('.upload_li_atch'+i).find('.video-pro-info > p').text(`${name} (Processing...)`);
											$('#stopAtchUpload'+i).attr("disabled", true);
											$('#stopAtchUpload'+i).addClass('button--default--disabled');
										}
									}, true);
								}
								return xhr;
							},
							mimeType:"multipart/form-data",
							success: function(result) {
								setTimeout( function(){
									if(!$('#video_attachment_list').length){
										var html4 = `<ul class="delete-deta-ul mt-2" id="video_attachment_list"></ul>`;
										$(html4).insertAfter($("#progress_list_video_atch"));
									}
									if(result.status == "success"){
										$('.upload_li_atch'+i).remove();
										var html0 = `<li class="contentli-item">
											<div class="downloads-reorder">
												<div class="recoder-ff">
													<img src="{{ URL::to('public/frontend/images/cha.png') }}">
												</div>
												<div class="recoder-mm">
													<input type="text" name="attachment_names[${result.attachment.id}]" value="${name}" class="form-control attachment_names" data-id="${result.attachment.id}">
												</div>
												<div class="recoder-ll">
													<button type="button" class="button button--icon-only button--knockout del_attach_download" id="del_attach_download_${result.attachment.id}" data-id="${result.attachment.id}">
														<i class="fa fa-trash-o"></i>
													</button>
												</div>
											</div>
										</li>`;
										$('#video_attachment_list').append(html0);
										if($('#progress_list_video_atch').find('li').length == 0 ){
											$('#progress_list_video_atch').hide();
											upload_progress = false;
											$('#video_attachment').val('');
											$('.attachment_file_duration').remove();
											$('.save_lesson_btn').parent().find('.progrs').remove();
											$('.save_lesson_btn').show();
											$('.save_lesson_btn').parent().find('.discard_lesson').show();
											$('.save_lesson_btn').parent().find('.custom-checkbox').show();
											$('.save_lesson_btn').parent().find('.tools-tip').show();
											console.log('Emptied attachment files');
										}
									}
								},1000);
							},
							error: function(err) {
								console.log(err);
								$('.upload_li'+i).remove();
								toastr.error(`There was some problem uploading ${name} file.`);
							}
						});
					}, 2000);
				});
			}

			$(document).on('click','.stopAtchUpload', function(e){
				var id = $(this).data('id');
				attchAjaxCall[id].abort();
				$('.upload_li_atch'+id).remove();
				if($('#progress_list_video_atch').find('li').length == 0 ){
					$('#progress_list_video_atch').hide();
					$('#progress_list_video_atch').hide();
					$('#video_attachment').val('');
					console.log('Emptied attachment files');
				}
			});

			$(document).on("click", '.del_attach_download', function(){
				swal({
					title: "@lang('client_site.delete_attachemnt')",
					text: "@lang('client_site.delete_attachemnt_text')",
					icon: "warning",
					buttons: true,
					dangerMode: true,
					buttons: ['@lang("client_site.cancel")', '@lang("client_site.delete")']
				})
				.then((willDelete) => {
					if (willDelete) {
						var id = $(this).data('id');
						$.ajax({
							url: "{{url('/')}}/delete-attachment/"+id,
							method: 'get',
							dataType: 'json',
							async: false,
							success: function(res){
								console.log(res);
								if(res.status == "success"){
									$('#del_attach_download_'+id).parent().parent().parent().fadeOut();
									// console.log($('#video_attachment_list li:visible').length);
									if( $('#video_attachment_list li:visible').length === 1 ){
										$('#video_attachment_list').remove();
										$('#progress_list_video_atch').hide();
										$('#video_attachment').val('');
										console.log('Emptied attachment files');
									}
									toastr.success(res.message);
								}
							},
							error: function(err){
								console.log(err);
							},
						});
					} else {
						return false;
					}
				});
			});
			function loadAttachments(attachments){
				var html = "";
				if($('#video_attachment_list').length){
					$('#video_attachment_list').remove();
				}
				$(attachments).each(function(i, attachment){
					var filename = attachment.filename;
					var name = attachment.name == null ? "" : attachment.name;
					html += `<li class="contentli-item">
						<div class="downloads-reorder">
							<div class="recoder-ff">
								<img src="{{ URL::to('public/frontend/images/cha.png') }}">
							</div>
							<div class="recoder-mm">
								<input type="text" name="attachment_names[${attachment.id}]" value="${name}" class="form-control attachment_names" data-id="${attachment.id}">
							</div>
							<div class="recoder-ll">
								<button type="button" class="button button--icon-only button--knockout del_attach_download" id="del_attach_download_${attachment.id}" data-id="${attachment.id}">
									<i class="fa fa-trash-o"></i>
								</button>
							</div>
						</div>
					</li>`;
				});
				var html3 = `<ul class="delete-deta-ul mt-2" id="video_attachment_list">${html}</ul>`;
				$(html3).insertAfter($(".video_attachment"));
			}
		// Video


		// Audio
			$(document).on('dragover dragleave drop', '.audio_dropbx', function(event) {
				event.preventDefault();
				event.stopPropagation();
			});
			$('.audio_dropbx').on('dragover dragenter', function(e){
				e.preventDefault();
				$(this).css("border-style", "solid");
			});
			$('.audio_dropbx').bind('dragleave drop', function(e){
				e.preventDefault();
				if (upload_progress == true) {
					toastr.error("@lang('site.upload_in_progress')");
					$('#audio-attch2').val('');
				} else {
					$(this).css("border-style", "dashed");
					$('#audio-attch2')[0].files = e.originalEvent.dataTransfer.files;

					if($('#audio-attch2')[0].files.length){
						const validImageTypes = ['audio/mp3', 'audio/mpeg'];
						var mimetype = $('#audio-attch2')[0].files[0]['type'];
						var size = $('#audio-attch2')[0].files[0]['size'];
						var name = $('#audio-attch2')[0].files[0]['name'];
						console.log(mimetype);
						if (!validImageTypes.includes(mimetype)) {
							$('.audio_msg').html(`<label class="error">${name} não é um arquivo de áudio válido.</label>`);
							$('audio-attch2').val('');
						} else if (size > file_size_limit) {
							$('.audio_msg').html(`<label class="error">${name} ié maior que 20 MB.</label>`);
							$('audio-attch2').val('');
						} else {
							$('.save_lesson_btn').hide();
							$('.save_lesson_btn').parent().find('.discard_lesson').hide();
							$('.save_lesson_btn').parent().find('.custom-checkbox').hide();
							$('.save_lesson_btn').parent().find('.tools-tip').hide();
							$('.save_lesson_btn').parent().find('.progrs').remove();
							$('.save_lesson_btn').parent().append(`<div class="progrs">@lang('client_site.saving_for_progress')...</div>`);
							getTime(0, $('#audio-attch2')[0].files[0], "audio");
						}
					}
				}
			});
			$('#audio-attch2').change(function(){
				$(this).css("border-style", "dashed");
				if (upload_progress == true) {
					toastr.error("@lang('site.upload_in_progress')");
					$('#audio-attch2').val('');
				} else {
					if($('#audio-attch2')[0].files.length){
						const validImageTypes = ['audio/mp3', 'audio/mpeg'];
						var mimetype = $('#audio-attch2')[0].files[0]['type'];
						var size = $('#audio-attch2')[0].files[0]['size'];
						var name = $('#audio-attch2')[0].files[0]['name'];
						console.log(mimetype);
						if (!validImageTypes.includes(mimetype)) {
							$('.audio_msg').html(`<label class="error">${name} não é um arquivo de áudio válido.</label>`);
							$('audio-attch2').val('');
						} else if (size > file_size_limit) {
							$('.audio_msg').html(`<label class="error">${name} ié maior que 20 MB.</label>`);
							$('audio-attch2').val('');
						} else {
							$('.save_lesson_btn').hide();
							$('.save_lesson_btn').parent().find('.discard_lesson').hide();
							$('.save_lesson_btn').parent().find('.custom-checkbox').hide();
							$('.save_lesson_btn').parent().find('.tools-tip').hide();
							$('.save_lesson_btn').parent().find('.progrs').remove();
							$('.save_lesson_btn').parent().append(`<div class="progrs">@lang('client_site.saving_for_progress')...</div>`);
							getTime(0, $('#audio-attch2')[0].files[0], "audio");
						}
					}
				}
			});
			$(document).on('click','#stopAudioUpload', function(e){
				audioAjaxCall.abort();
				$('#progress_list_audio').hide();
				$('.progress-ul-audio').empty();
				$('#audio-attch2').val('');
				$('.save_lesson_btn').show();
				$('.save_lesson_btn').parent().find('.progrs').remove();
				$('.save_lesson_btn').parent().find('.discard_lesson').show();
				$('.save_lesson_btn').parent().find('.custom-checkbox').show();
				$('.save_lesson_btn').parent().find('.tools-tip').show();
				var id = $('#lesson_id').val();
				$.get( "{{url('/')}}/delete-lesson/"+id+"/ajax", function( res ) {
					if(res.status == "success"){
						console.log(res.message);
						$('#lesson_id').val('');
					}
				});
			});
			function audioSave() {
				return new Promise(resolve => {
					setTimeout(() => {
						console.log("Saving audio");
						if(!$('#audios').length) $('#audios').remove();
						var name = $('#audio-attch2')[0].files[0]['name'];
						if($('#lesson_id').val() == ""){
							var formData = new FormData();
							formData.append("jsonrpc", "2.0");
							formData.append("_token", '{{csrf_token()}}');
							formData.append("lesson_title", $('#lesson_title_A').val());
							formData.append("lesson_type", $('#lesson_type').val());
							formData.append("lesson_id", $('#lesson_id').val());
							formData.append("chapter_id", $('#chapter_id').val());
							formData.append("product_id", '{{@$product->id}}');
							formData.append("description", $('#audio_description').val());
							formData.append("is_allow_discussion", $('#is_allow_discussion').val());
							formData.append("is_prerequisite", $('#is_prerequisite').val());
							formData.append("is_free_preview", $('#is_free_preview').val());
							saveLesson("0", formData);
						}
						$('#progress_list_audio').show();
						var html =
						`<li class="ado_upload_li">
							<div class="upload-info">
								<div class="video-pro-info"><p>${name} (@lang('client_site.queued_for_upload'))</p>
									<div class="progress-bars">
										<div class="ado_bar" style="width:0%">
											<p class="percent">0%</p>
										</div>
									</div>
								</div>
								<button type="button" class="button--default--small button--secondary_n" id="stopAudioUpload">@lang("client_site.cancel")</button>
							</div>
						</li>`;
						$('.progress-ul-audio').append(html);
						var uploadFormData = new FormData();
						uploadFormData.append("jsonrpc", "2.0");
						uploadFormData.append("_token", '{{csrf_token()}}');
						uploadFormData.append("lesson_title", $('#lesson_title_V').val());
						uploadFormData.append("lesson_type", $('#lesson_type').val());
						uploadFormData.append("lesson_id", $('#lesson_id').val());
						uploadFormData.append("chapter_id", $('#chapter_id').val());
						uploadFormData.append("product_id", '{{@$product->id}}');
						uploadFormData.append("file", $('#audio-attch2')[0].files[0]);
						uploadFormData.append("duration", $("#file_duration").val());
						audioAjaxCall = $.ajax({
							url: "{{route('upload.audio')}}",
							type: 'post',
							data: uploadFormData,
							dataType: 'json',
							contentType: false,
							cache: false,
							processData:false,
							xhr: function(){
								//upload Progress
								var xhr = $.ajaxSettings.xhr();
								if (xhr.upload) {
									xhr.upload.addEventListener('progress', function(event) {
										var percent = 0;
										var position = event.loaded || event.position;
										var total = event.total;
										if (event.lengthComputable) {
											percent = Math.ceil(position / total * 100);
										}
										//update progressbar
										console.log(percent);
										$('.ado_bar').css('width', + percent +"%");
										$('.ado_bar'+' .percent').text(percent +" %");
										upload_progress = true;
										upload_progress_lesson_id = $('#lesson_id').val();
										if(percent == 100){
											$('.ado_upload_li').find('.video-pro-info > p').text(`${name} (Processing...)`);
											$('#stopAudioUpload').attr("disabled", true);
											$('#stopAudioUpload').addClass('button--default--disabled');
											upload_progress = false;
										}
									}, true);
								}
								return xhr;
							},
							mimeType:"multipart/form-data",
							success: function(result) {
								setTimeout( function(){
									console.log(result);
									if(result.status == "success"){
										lesson_main_audio_required = false;
										$('.ado_upload_li').remove();
										upload_progress = false;
										if($('#audios').length) $('#audios').remove();
										var html0 = `<div class="form-row" id="audios"><audio src="{{URL::to('storage/app/public/lessons/type_audio/')}}/${result.main_audio.filename}" controls ></audio></div>`;
										$(html0).insertAfter($("#progress_list_audio"));
										$('#progress_list_audio').hide();
										$('.progress-ul-audio').empty();
										$('.audio_msg').empty();
										$('.save_lesson_btn').parent().find('.progrs').remove();
										$('.save_lesson_btn').show();
										$('.save_lesson_btn').parent().find('.discard_lesson').show();
										$('.save_lesson_btn').parent().find('.custom-checkbox').show();
										$('.save_lesson_btn').parent().find('.tools-tip').show();
									}
								},1000);
							},
							error: function(err) {
								console.log(err);
							}
						});
					}, 1000);
				});
			}
		// Audio


		// Download
			$(document).on('dragover dragleave drop', '.download_dropbx', function(event) {
				event.preventDefault();
				event.stopPropagation();
			});
			$('.download_dropbx').on('dragover dragenter', function(e){
				e.preventDefault();
				$(this).css("border-style", "solid");
			});
			$('.download_dropbx').bind('dragleave drop', function(e){
				if (upload_progress == true) {
					toastr.error("@lang('site.upload_in_progress')");
					$('#download-attch2').val('');
				} else {
					e.preventDefault();
					var formData = new FormData();
					formData.append("jsonrpc", "2.0");
					formData.append("_token", '{{csrf_token()}}');
					formData.append("lesson_title", $('#lesson_title_D').val());
					formData.append("lesson_type", $('#lesson_type').val());
					formData.append("lesson_id", $('#lesson_id').val());
					formData.append("chapter_id", $('#chapter_id').val());
					formData.append("product_id", '{{@$product->id}}');
					formData.append("description", $('#download_description').val());
					formData.append("is_allow_discussion", $('#is_allow_discussion').val());
					formData.append("is_prerequisite", $('#is_prerequisite').val());
					formData.append("is_free_preview", $('#is_free_preview').val());
					saveLesson("0", formData);

					$(this).css("border-style", "dashed");
					$('#download-attch2')[0].files = e.originalEvent.dataTransfer.files;
					var err_msg = "", xxx=0;
					while (xxx < $('#download-attch2')[0].files.length) {
						$('body').append(`<input type="text" class="download_file_duration" id="file_duration_${xxx}" />`);
						xxx++;
					}
					$.each($('#download-attch2')[0].files, function(i, file) {
						const invalidImageTypes = ['application/bat'];
						if($('#download-attch2')[0].files.length){
							var mimetype = file['type'];
							var size = file['size'];
							var name = file['name'];
							if (invalidImageTypes.includes(mimetype)) {
								err_msg += `<label class="error">${name} não é um arquivo de download válido.</label><br>`
								$('.download_msg').html(err_msg);
								// $('download-attch2').val('');
							}
							else if (size > file_size_limit) {
								err_msg += `<label class="error">${name} ié maior que 20 MB.</label>`;
								$('.download_msg').html(err_msg);
								// $('download-attch2').val('');
							}
							else {
								if(i == 0){
									$('.save_lesson_btn').hide();
									$('.save_lesson_btn').parent().find('.discard_lesson').hide();
									$('.save_lesson_btn').parent().find('.custom-checkbox').hide();
									$('.save_lesson_btn').parent().find('.tools-tip').hide();
									$('.save_lesson_btn').parent().find('.progrs').remove();
									$('.save_lesson_btn').parent().append(`<div class="progrs">@lang('client_site.saving_for_progress')...</div>`);
								}
								getTime(i, file, "download");
							}
						}
					});
				}
			});
			$('#download-attch2').change(function(){
				if (upload_progress == true) {
					toastr.error("@lang('site.upload_in_progress')");
					$('#download-attch2').val('');
				} else {
					if(!$('#downloads_attachment_list').length){
						var html4 = `<ul class="delete-deta-ul mt-2" id="downloads_attachment_list"></ul>`;
						$(html4).insertAfter($("#progress_list"));
					}
					var err_msg = "", xxx=0;
					while (xxx < $('#download-attch2')[0].files.length) {
						$('body').append(`<input type="text" class="download_file_duration" id="file_duration_${xxx}" />`);
						xxx++;
					}
					var formData = new FormData();
					formData.append("jsonrpc", "2.0");
					formData.append("_token", '{{csrf_token()}}');
					formData.append("lesson_title", $('#lesson_title_D').val());
					formData.append("lesson_type", $('#lesson_type').val());
					formData.append("lesson_id", $('#lesson_id').val());
					formData.append("chapter_id", $('#chapter_id').val());
					formData.append("product_id", '{{@$product->id}}');
					formData.append("description", $('#download_description').val());
					formData.append("is_allow_discussion", $('#is_allow_discussion').val());
					formData.append("is_prerequisite", $('#is_prerequisite').val());
					formData.append("is_free_preview", $('#is_free_preview').val());
					saveLesson("0", formData);

					$.each($('#download-attch2')[0].files, function(i, file) {
						const invalidImageTypes = ['application/bat'];
						if($('#download-attch2')[0].files.length){
							var mimetype = file['type'];
							var size = file['size'];
							var name = file['name'];
							if (invalidImageTypes.includes(mimetype)) {
								err_msg += `<label class="error">${name} não é um arquivo de download válido.</label><br>`
								$('.download_msg').html(err_msg);
								// $('download-attch2').val('');
							}
							else if (size > file_size_limit) {
								err_msg += `<label class="error">${name} ié maior que 20 MB.</label>`;
								$('.download_msg').html(err_msg);
								// $('download-attch2').val('');
							}
							else {
								if(i == 0){
									console.log('Removing buttons');
									$('.save_lesson_btn').hide();
									$('.save_lesson_btn').parent().find('.discard_lesson').hide();
									$('.save_lesson_btn').parent().find('.custom-checkbox').hide();
									$('.save_lesson_btn').parent().find('.tools-tip').hide();
									$('.save_lesson_btn').parent().find('.progrs').remove();
									$('.save_lesson_btn').parent().append(`<div class="progrs">@lang('client_site.saving_for_progress')...</div>`);
								}
								getTime(i, file, "download");
							}
						}
					});
				}
			});
			function downloadSave(i, file){
				return new Promise(resolve => {
					setTimeout(() => {
						var name = file['name'];
						$('#progress_list').show();
						var html =
						`<li class="upload_li${i}">
							<div class="upload-info">
								<div class="video-pro-info"><p>${name} (@lang('client_site.queued_for_upload'))</p>
									<div class="progress-bars">
										<div class="bar bar${i}" style="width:0%">
											<p class="percent">0%</p>
										</div>
									</div>
								</div>
								<button class="button--default--small button--secondary_n stopDwnldUpload" id="stopDwnldUpload${i}" data-id="${i}">@lang("client_site.cancel")</button>
							</div>
						</li>`;
						$('.progress-ul').append(html);
						var uploadFormData = new FormData();
						uploadFormData.append("jsonrpc", "2.0");
						uploadFormData.append("_token", '{{csrf_token()}}');
						uploadFormData.append("lesson_title", $('#lesson_title_V').val());
						uploadFormData.append("lesson_type", $('#lesson_type').val());
						uploadFormData.append("lesson_id", $('#lesson_id').val());
						uploadFormData.append("chapter_id", $('#chapter_id').val());
						uploadFormData.append("product_id", '{{@$product->id}}');
						uploadFormData.append("duration", $("#file_duration_"+i).val());
						uploadFormData.append("file", file);

						dwnldAjaxCall[i] = $.ajax({
							url: "{{route('upload.downloads')}}",
							type: 'post',
							data: uploadFormData,
							dataType: 'json',
							contentType: false,
							cache: false,
							processData:false,
							xhr: function(){
								//upload Progress
								var xhr = $.ajaxSettings.xhr();
								if (xhr.upload) {
									xhr.upload.addEventListener('progress', function(event) {
										var percent = 0;
										var position = event.loaded || event.position;
										var total = event.total;
										if (event.lengthComputable) {
											percent = Math.ceil(position / total * 100);
										}
										//update progressbar
										console.log(percent);
										$('.bar'+i).css('width', + percent +"%");
										$('.bar'+i+' .percent').text(percent +" %");
										upload_progress = true;
										upload_progress_lesson_id = $('#lesson_id').val();
										if(percent == 100){
											$('.upload_li'+i).find('.video-pro-info > p').text(`${name} (Processing...)`);
											$('#stopDwnldUpload'+i).attr("disabled", true);
											$('#stopDwnldUpload'+i).addClass('button--default--disabled');
										}
									}, true);
								}
								return xhr;
							},
							mimeType:"multipart/form-data",
							success: function(result) {
								setTimeout( function(){
									if(result.status == "success"){
										lesson_main_download_required = false;
										$('.upload_li'+i).remove();
										var html0 = `<li class="contentli-item">
											<div class="downloads-reorder">
												<div class="recoder-ff">
													<img src="{{ URL::to('public/frontend/images/cha.png') }}">
												</div>
												<div class="recoder-mm">
													<input type="text" name="download_names[${result.attachment.id}]" value="${result.attachment.name}" class="form-control download_names" data-id="${result.attachment.id}">
												</div>
												<div class="recoder-ll">
													<button type="button" class="button button--icon-only button--knockout del_download" id="del_download_${result.attachment.id}" data-id="${result.attachment.id}">
														<i class="fa fa-trash-o"></i>
													</button>
												</div>
											</div>
										</li>`;
										$('#downloads_attachment_list').append(html0);
										if($('#progress_list').find('li').length == 0 ) {
											$('#progress_list').hide();
											$('.download_file_duration').remove();
											$('#download-attch2').val('');
											upload_progress = false;
											$('.save_lesson_btn').parent().find('.progrs').remove();
											$('.save_lesson_btn').show();
											$('.save_lesson_btn').parent().find('.discard_lesson').show();
											$('.save_lesson_btn').parent().find('.custom-checkbox').show();
											$('.save_lesson_btn').parent().find('.tools-tip').show();
											console.log('Emptied download');
										}
									}
								},1000);
							},
							error: function(err) {
								console.log(err);
								$('.upload_li'+i).remove();
								toastr.error(`There was some problem uploading ${name} file.`);
							}
						});
					}, 2000);
				});
			}
			$(document).on('click','.stopDwnldUpload', function(e){
				var id = $(this).data('id');
				dwnldAjaxCall[id].abort();
				$('.upload_li'+id).remove();
				if($('#progress_list').find('li').length == 0 ) $('#progress_list').hide();
			});
			$(document).on("click", '.del_download', function(){
				if($('.del_download').length == 1){
                    swal('@lang('client_site.last_attachment')',{icon:"warning"});
				} else {
					swal({
						title: "@lang('client_site.delete_attachemnt')",
						text: "@lang('client_site.delete_attachemnt_text')",
						icon: "warning",
						buttons: true,
						dangerMode: true,
						buttons: ['@lang("client_site.cancel")', '@lang("client_site.delete")']
					})
					.then((willDelete) => {
						if (willDelete) {
							var id = $(this).data('id');
							$.ajax({
								url: "{{url('/')}}/delete-download/"+id,
								method: 'get',
								dataType: 'json',
								async: false,
								success: function(res){
									console.log(res);
									if(res.status == "success"){
										$('#del_download_'+id).parent().parent().parent().fadeOut();
										setTimeout( function(){
											$('#del_download_'+id).parent().parent().parent().remove();
											if($('.del_download').length == 0) $('#downloads_attachment_list').remove();
										},1000);
										toastr.success(res.message);
									}
								},
								error: function(err){
									console.log(err);
								},
							});
						} else {
							return false;
						}
					});
				}
			});
		// Download


		// Quiz
			$('#add_question').click(function(){
				var question_count = parseInt($('.quiz_question').length)+1;
				var quiz_count = parseInt($('#quiz_count').val())+1
				console.log(question_count);
				if(typeof(tinyMCE) !== 'undefined') {
					var length = tinyMCE.editors.length;
					for (var i=length; i>0; i--) {
						tinyMCE.editors[i-1].remove();
					};
				}
				$('#quiz_count').val(quiz_count);
				var html =
				`<div class="course-content-grow quiz_question new_questns" data-id="${quiz_count}" style="display:none;">
					<div id="main3">
						<div class="accordion3" id="faqnew${quiz_count+1}">
							<div class="card">
								<div class="card-header" id="faqnewhead${quiz_count}">
									<a href="#" class=" btn-header-link" data-toggle="collapse" data-target="#qhead${quiz_count}" aria-expanded="true" aria-controls="qhead${quiz_count}">
										<h5>@lang('client_site.question') ${question_count} <sup style="display:none;"><i class="fa fa-circle" aria-hidden="true"></i></sup></h5>
										<div class="d-flex align-items-center">
											<div class="button-group">
												<span class="user_llllk new-link3 button del_quiz latest_quiz${quiz_count}" id="del_quiz${quiz_count}" data-count="${quiz_count}">
													<i class="fa fa-trash-o" aria-hidden="true"></i>
												</span>
											</div>
										</div>
									</a>
								</div>
								<div id="qhead${quiz_count}" class="collapse" aria-labelledby="faqnewhead${quiz_count}" data-parent="#faqnew${quiz_count+1}">
									<div class="card-body">
										<div class="clearfix mt-3"></div>
										<div id="err_correct${quiz_count}"></div>
										<div class="clearfix mt-2"></div>
										<label>@lang('client_site.question')</label>
										<textarea class="question${quiz_count}" id="question${quiz_count}" name="question${quiz_count}"></textarea>

										<div class="accordion4 mt-3" id="faqnew3">
											@for($i=1; $i<=6; $i++)
											<div class="card">
												<div class="card-header" id="faqheadnew">
													<a href="javascript:;" class=" btn-header-link" data-toggle="collapse" data-target="#choice${quiz_count}_{{$i}}" aria-expanded="true" aria-controls="choice${quiz_count}_{{$i}}">Alternativa {{$i}}</a>
												</div>
												<div id="choice${quiz_count}_{{$i}}" class="collapse {{ $i<=2 ? 'show' : ''}}" aria-labelledby="faqheadnew" data-parent="#faqnew">
													<div class="card-body">
														<textarea class="answer${quiz_count}_{{$i}}" id="answer${quiz_count}_{{$i}}" name="answer${quiz_count}_{{$i}}"></textarea>
														<div class="clearfix"></div>
														<div class="dash-d das-radio">
															<input type="radio" name="correct${quiz_count}" id="correct${quiz_count}_{{$i}}" class="correct${quiz_count} inp_radio" value="{{$i}}">
															<label for="correct${quiz_count}_{{$i}}">@lang('client_site.this_is_correct_answer')</label>
														</div>
													</div>
												</div>
											</div>
											@endfor
										</div>

										<div class="clearfix mt-3"></div>
										<label>Explanation</label>
										<textarea class="explanation${quiz_count}" id="explanation${quiz_count}" name="explanation${quiz_count}"></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>`;
				$('.quiz_question:last').after(html);
				for(i=1; i<=quiz_count; i++){
					if($('#question'+i).length != 0){
						initMCEshort("question"+i);
						initMCEshort("answer"+i+"_1");
						initMCEshort("answer"+i+"_2");
						initMCEshort("answer"+i+"_3");
						initMCEshort("answer"+i+"_4");
						initMCEshort("answer"+i+"_5");
						initMCEshort("answer"+i+"_6");
						initMCEshort("explanation"+i);
					}
				}
				$('.quiz_question:last').fadeIn();
			});
			$('#exampleModal').on('hidden.bs.modal', function () {
				$('#import').val('');
			});
			$('#import').change(function(){
				if($('#lesson_id').val() == ""){
					console.log("No lesson");
					var formData = new FormData();
					formData.append("jsonrpc", "2.0");
					formData.append("_token", '{{csrf_token()}}');
					formData.append("lesson_title", $('#lesson_title_Q').val());
					formData.append("lesson_type", $('#lesson_type').val());
					formData.append("lesson_id", $('#lesson_id').val());
					formData.append("chapter_id", $('#chapter_id').val());
					formData.append("product_id", '{{@$product->id}}');
					formData.append("duration", $('#duration_Q').val());
					formData.append("is_allow_discussion", is_allow_discussion);
					formData.append("is_prerequisite", is_prerequisite);
					formData.append("is_free_preview", is_free_preview);
					formData.append("status", ($('#chk6').is(":checked") && $('#chk5').is(":checked")) ? 'D' : 'A');
					// formData.append("questions", JSON.stringify(newQuestions));

					saveLesson("0", formData);
				}
				var importFormData = new FormData();
				importFormData.append("jsonrpc", "2.0");
				importFormData.append("_token", '{{csrf_token()}}');
				importFormData.append("lesson_id", $('#lesson_id').val());
				importFormData.append("file", $('#import')[0].files[0]);
				if($('#import')[0].files.length){
					const validImageTypes = ['application/vnd.ms-excel'];
					var mimetype = $('#import')[0].files[0]['type'];
					var size = $('#import')[0].files[0]['size'];
					var name = $('#import')[0].files[0]['name'];
					console.log(mimetype);
					if (!validImageTypes.includes(mimetype)) {
						// err_msg += `<label class="error">${name} is not a valid download file.</label><br>`
						// $('.video_attachment_msg').html(err_msg);
						toastr.error("O arquivo fornecido não é um arquivo csv");
					}
					$.ajax({
						url: "{{route('quiz.import')}}",
						type: 'post',
						data: importFormData,
						dataType: 'json',
						contentType: false,
						cache: false,
						processData:false,
						xhr: function(){
							//upload Progress
							var xhr = $.ajaxSettings.xhr();
							// if (xhr.upload) {
							// 	xhr.upload.addEventListener('progress', function(event) {
							// 		var percent = 0;
							// 		var position = event.loaded || event.position;
							// 		var total = event.total;
							// 		if (event.lengthComputable) {
							// 			percent = Math.ceil(position / total * 100);
							// 		}
							// 		//update progressbar
							// 		console.log(percent);
							// 		$('.ado_bar').css('width', + percent +"%");
							// 		$('.ado_bar'+' .percent').text(percent +" %");
							// 		console.log($('.ado_bar')[0]);
							// 		console.log($('.ado_bar'+' .percent')[0]);
							// 	}, true);
							// }
							return xhr;
						},
						mimeType:"multipart/form-data",
						success: function(result) {
							console.log(result);
							if(result.status == "success"){
								$('#exampleModal').modal('hide');
								var lid = $('#lesson_id').val();
								var cid = $('#chapter_id').val();
								var ltp = $('#lesson_type').val();
								loadLessons(lid,cid,ltp);
								toastr.success(result.message);
							} else {
								toastr.error(result.message);
							}
						},
						error: function(err) {
							console.log(err);
						}
					});
				}
			});
			$(document).delegate('.del_quiz', 'click', function(e){
				e.preventDefault();
				if($('.quiz_question').length == 1){
                    swal('@lang('client_site.last_question')',{icon:"warning"});
				} else {
					swal({
						title: "@lang('client_site.del_ques')",
						text: "@lang('site.del_question')",
						icon: "warning",
						buttons: true,
						dangerMode: true,
						buttons: ['@lang("client_site.cancel")', '@lang("client_site.delete")']
					})
					.then((willDelete) => {
						if (willDelete) {
							var id = $(this).data('id');
							var count = $(this).data('count');
							if(id==null || id==undefined){
								// count
								$('.latest_quiz'+count).parent().parent().parent().parent().parent().parent().parent().parent().remove();
								var len = parseInt($('.old_questns').length);
								var newlen = parseInt($('.new_questns').length);
								$('.new_questns').each(function(i, questn){
									$(questn).find('h5').html(`@lang('client_site.question') ${len+i+1} <sup style="display:none;"><i class="fa fa-circle" aria-hidden="true"></i></sup>`);
								});
							} else {
								// id
								console.log(id);
								$.ajax({
									url: "{{ url('/') }}" + "/delete-question/" + id,
									method: 'get',
									dataType: 'json',
									async: false,
									success: function(res){
										console.log(res);
										if(res.status == "success"){
											var lesson_id = $('#lesson_id').val();
											var chapter_id = $('#chapter_id').val();
											var ltype = 'Q';
											loadLessons(lesson_id,chapter_id,ltype);
											toastr.success(res.message);
										}
										if(res.status == "error"){
											toastr.error(res.message);
										}
									},
									error: function(err){
										console.log(err);
									},
								});
							}
							var question_count = parseInt($('#quiz_count').val());
							console.log(question_count);
							if(typeof(tinyMCE) !== 'undefined') {
								var length = tinyMCE.editors.length;
								for (var i=length; i>0; i--) {
									tinyMCE.editors[i-1].remove();
								}
							}
							for(i=1; i<=question_count; i++){
								initMCEshort("question"+i);
								initMCEshort("answer"+i+"_1");
								initMCEshort("answer"+i+"_2");
								initMCEshort("answer"+i+"_3");
								initMCEshort("answer"+i+"_4");
								initMCEshort("answer"+i+"_5");
								initMCEshort("answer"+i+"_6");
								initMCEshort("explanation"+i);
							}
						} else {
							return false;
						}
					});
				}
			});

		// Quiz


		// Presentation
			$(document).on('dragover dragleave drop', '.image_dropbx', function(event) {
				event.preventDefault();
				event.stopPropagation();
			});
			$('.image_dropbx').on('dragover dragenter', function(e){
				e.preventDefault();
				$(this).css("border-style", "solid");
			});
			$('.image_dropbx').bind('dragleave drop', function(e){
				if (upload_progress == true) {
					toastr.error("@lang('site.upload_in_progress')");
					$('#image-attch2').val('');
				} else {
					e.preventDefault();
					$('#progress_list_image').show();
					$(this).css("border-style", "dashed");
					$('#image-attch2')[0].files = e.originalEvent.dataTransfer.files;
					var formData = new FormData();
					formData.append("jsonrpc", "2.0");
					formData.append("_token", '{{csrf_token()}}');
					formData.append("lesson_title", $('#lesson_title_A').val());
					formData.append("lesson_type", $('#lesson_type').val());
					formData.append("lesson_id", $('#lesson_id').val());
					formData.append("chapter_id", $('#chapter_id').val());
					formData.append("product_id", '{{@$product->id}}');
					formData.append("duration", $('#duration_P').val());
					formData.append("is_allow_discussion", $('#is_allow_discussion').val());
					formData.append("is_prerequisite", $('#is_prerequisite').val());
					formData.append("is_free_preview", $('#is_free_preview').val());
					saveLesson("0", formData);

					if($('#image-attch2')[0].files.length){
						const validImageTypes = ['image/jpeg', 'image/jpg', 'image/png'];
						var mimetype = $('#image-attch2')[0].files[0]['type'];
						var size = $('#image-attch2')[0].files[0]['size'];
						var name = $('#image-attch2')[0].files[0]['name'];
						console.log(mimetype);
						if (!validImageTypes.includes(mimetype)) {
							$('.image_msg').html(`<label class="error">${name} não é um arquivo de imagem válido.</label>`);
							$('image-attch2').val('');
						} else if (size > file_size_limit) {
							$('.image_msg').html(`<label class="error">${name} ié maior que 20 MB.</label>`);
							$('image-attch2').val('');
						} else {
							$('#progress_list_image').show();
							var html =
							`<li class="img_upload_li">
								<div class="upload-info">
									<div class="audio-pro-info"><p>${name} (@lang('client_site.queued_for_upload'))</p>
										<div class="progress-bars">
											<div class="img_bar" style="width:0%">
												<p class="percent">0%</p>
											</div>
										</div>
									</div>
									{{-- <button class="button--default--small button--secondary_n">@lang("client_site.cancel")</button> --}}
								</div>
							</li>`;
							$('.progress-ul-image').append(html);
							var uploadFormData = new FormData();
							uploadFormData.append("jsonrpc", "2.0");
							uploadFormData.append("_token", '{{csrf_token()}}');
							uploadFormData.append("lesson_title", $('#lesson_title_P').val());
							uploadFormData.append("lesson_type", $('#lesson_type').val());
							uploadFormData.append("lesson_id", $('#lesson_id').val());
							uploadFormData.append("chapter_id", $('#chapter_id').val());
							uploadFormData.append("product_id", '{{@$product->id}}');
							uploadFormData.append("file", $('#image-attch2')[0].files[0]);
							uploadSlide(uploadFormData, "new_slide");
						}
					}
					setTimeout( function(){
						$('#progress_list_image').hide();
						$('#progress_list_image .img_upload_li').remove();
					}, 2000);
				}
			});
			$('#image-attch2').change(function(){
				if (upload_progress == true) {
					toastr.error("@lang('site.upload_in_progress')");
					$('#image-attch2').val('');
				} else {
					$('.image_msg').empty();
					$(this).css("border-style", "dashed");

					var formData = new FormData();
					formData.append("jsonrpc", "2.0");
					formData.append("_token", '{{csrf_token()}}');
					formData.append("lesson_title", $('#lesson_title_P').val());
					formData.append("lesson_type", $('#lesson_type').val());
					formData.append("lesson_id", $('#lesson_id').val());
					formData.append("chapter_id", $('#chapter_id').val());
					formData.append("product_id", '{{@$product->id}}');
					formData.append("duration", $('#duration_P').val());
					formData.append("is_allow_discussion", $('#is_allow_discussion').val());
					formData.append("is_prerequisite", $('#is_prerequisite').val());
					formData.append("is_free_preview", $('#is_free_preview').val());
					saveLesson("0", formData);

					if($('#image-attch2')[0].files.length){
						const validImageTypes = ['image/jpeg', 'image/jpg', 'image/png'];
						var mimetype = $('#image-attch2')[0].files[0]['type'];
						var size = $('#image-attch2')[0].files[0]['size'];
						var name = $('#image-attch2')[0].files[0]['name'];
						console.log(mimetype);
						if (!validImageTypes.includes(mimetype)) {
							$('.image_msg').html(`<label class="error">${name} não é um arquivo de imagem válido.</label>`);
							$('image-attch2').val('');
						} else if (size > file_size_limit) {
							$('.image_msg').html(`<label class="error">${name} ié maior que 20 MB.</label>`);
							$('image-attch2').val('');
						} else {
							$('#progress_list_image').show();
							var html =
							`<li class="img_upload_li">
								<div class="upload-info">
									<div class="audio-pro-info"><p>${name} (@lang('client_site.queued_for_upload'))</p>
										<div class="progress-bars">
											<div class="img_bar" style="width:0%">
												<p class="percent">0%</p>
											</div>
										</div>
									</div>
									{{-- <button class="button--default--small button--secondary_n">@lang("client_site.cancel")</button> --}}
								</div>
							</li>`;
							$('.progress-ul-image').append(html);
							var uploadFormData = new FormData();
							uploadFormData.append("jsonrpc", "2.0");
							uploadFormData.append("_token", '{{csrf_token()}}');
							uploadFormData.append("lesson_title", $('#lesson_title_P').val());
							uploadFormData.append("lesson_type", $('#lesson_type').val());
							uploadFormData.append("lesson_id", $('#lesson_id').val());
							uploadFormData.append("chapter_id", $('#chapter_id').val());
							uploadFormData.append("product_id", '{{@$product->id}}');
							uploadFormData.append("file", $('#image-attch2')[0].files[0]);
							uploadSlide(uploadFormData, "new_slide");
						}
					}
					setTimeout( function(){
						$('#progress_list_image').hide();
						$('#progress_list_image .img_upload_li').remove();
					}, 2000);
				}
			});
			$(document).on('change', '.pres_audio', function() {
				var id = $(this).data('id');
				if (upload_progress == true) {
					toastr.error("@lang('site.upload_in_progress')");
					$('#pres_audio'+id).val('');
				} else {
					var file = $('#pres_audio'+id)[0].files[0];
					if($('#pres_audios'+id).length){
						$('#pres_audios'+id).remove();
					}
					const validImageTypes = ['audio/mp3', 'audio/mpeg'];
					var mimetype = file['type'];
					var size = file['size'];
					var name = file['name'];
					console.log(mimetype);
					if (!validImageTypes.includes(mimetype)) {
						$('#pres_audio_msg'+id).html(`<label class="error">${name} não é um arquivo de áudio válido.</label>`);
					} else if (size > file_size_limit) {
						$('#pres_audio_msg'+id).html(`<label class="error">${name} ié maior que 20 MB.</label>`);
					} else {
						getTime(id, $('#pres_audio'+id)[0].files[0], "pres_audio");
					}
				}
			});
			function presAudioSave(id, file){
				return new Promise(resolve => {
					setTimeout(() => {
						var name = file['name'];
						$('#pres_audio_progress'+id+' .presado_upload_li').remove();
						$('#pres_audio_progress'+id).show();
						$('#progress-ul-pres'+id).show();
						var html =
						`<li class="presado_upload_li">
							<div class="upload-info">
								<div class="audio-pro-info"><p>${name} (@lang('client_site.queued_for_upload'))</p>
									<div class="progress-bars">
										<div class="presado_bar presado_bar${id}" style="width:0%">
											<p class="percent">0%</p>
										</div>
									</div>
								</div>
								{{-- <button class="button--default--small button--secondary_n">@lang("client_site.cancel")</button> --}}
							</div>
						</li>`;
						$('#progress-ul-pres'+id).append(html);
						var uploadFormData = new FormData();
						uploadFormData.append("jsonrpc", "2.0");
						uploadFormData.append("_token", '{{csrf_token()}}');
						uploadFormData.append("lesson_title", $('#lesson_title_P').val());
						uploadFormData.append("lesson_type", $('#lesson_type').val());
						uploadFormData.append("lesson_id", $('#lesson_id').val());
						uploadFormData.append("chapter_id", $('#chapter_id').val());
						uploadFormData.append("product_id", '{{@$product->id}}');
						uploadFormData.append("description", $('#slide_description_'+id).val());
						uploadFormData.append("audio_duration", $("#file_duration").val());
						uploadFormData.append("slide_id", id);
						uploadFormData.append("audio", file);
						uploadSlide(uploadFormData, id);
					}, 2000);
				});
			}
			$(document).on('click', '.del_slide', function(){
				if($('.presentation_slide').length == 1){
                    swal('@lang('client_site.last_slide')',{icon:"warning"});
				} else {
					swal({
						title: "@lang('client_site.del_slide')",
						text: "@lang('client_site.del_slide')",
						icon: "warning",
						buttons: true,
						dangerMode: true,
						buttons: ['@lang("client_site.cancel")', '@lang("client_site.delete")']
					})
					.then((willDelete) => {
						if (willDelete) {
							var id = $(this).data('id');
							console.log("Delete slide : "+id);
							$.ajax({
								url: "{{ url('/') }}" + "/delete-slide/" + id,
								method: 'get',
								dataType: 'json',
								async: false,
								success: function(res){
									console.log(res);
									if(res.status == "success"){
										var lesson_id = $('#lesson_id').val();
										var chapter_id = $('#chapter_id').val();
										var ltype = 'P';
										loadLessons(lesson_id,chapter_id,ltype);
										toastr.success(res.message);
									}
									if(res.status == "error"){
										toastr.error(res.message);
									}
								},
								error: function(err){
									console.log(err);
								},
							});
						} else {
							return false;
						}
					});
				}
			});
			function uploadSlide(uploadFormData, foo){
				$.ajax({
					url: "{{route('upload.slide')}}",
					type: 'post',
					data: uploadFormData,
					dataType: 'json',
					contentType: false,
					cache: false,
					processData:false,
					xhr: function(){
						//upload Progress
						var xhr = $.ajaxSettings.xhr();
						if (xhr.upload) {
							xhr.upload.addEventListener('progress', function(event) {
								var percent = 0;
								var position = event.loaded || event.position;
								var total = event.total;
								if (event.lengthComputable) {
									percent = Math.ceil(position / total * 100);
								}
								//update progressbar
								console.log(percent);
								upload_progress = true;
								upload_progress_lesson_id = $('#lesson_id').val();
								if(foo == 'new_slide'){
									$('.img_bar').css('width', + percent +"%");
									$('.img_bar'+' .percent').text(percent +" %");
									// console.log($('.img_bar')[0]);
									// console.log($('.img_bar'+' .percent')[0]);
								} else {
									$('.presado_bar'+foo).css('width', + percent +"%");
									$('.presado_bar'+foo+' .percent').text(percent +" %");
									// console.log($('.presado_bar'+foo)[0]);
									// console.log($('.presado_bar'+foo+' .percent')[0]);
								}
							}, true);
						}
						return xhr;
					},
					mimeType:"multipart/form-data",
					success: function(result) {
						console.log(result);
						if(result.status == "success"){
							lesson_main_image_required = false;
							if(foo == 'new_slide'){
								$('.presado_upload_li').remove();
								var slide_count = $('.presentation_slide').length;
								console.log(slide_count);
								var img = `<img src="{{url('/')}}/storage/app/public/lessons/type_presentation/${result.slide.filename}"/>`;
								var description = result.slide.description == null ? "" : result.slide.description;
								var html =
								`<div class="course-content-grow presentation_slide" data-id="${result.slide.id}">
									<div id="main3">
										<div class="accordion3" id="faqnew2">
											<div class="card">
												<div class="card-header" id="faqnewhead${slide_count+1}">
													<a href="#" class=" btn-header-link" data-toggle="collapse" data-target="#shead${slide_count+1}" aria-expanded="true" aria-controls="shead${slide_count+1}">
														<h5>Slide ${slide_count+1}</h5>
														<div class="d-flex align-items-center">
															<div class="button-group">
																<span class="user_llllk new-link3 button del_slide" id="del_slide${result.slide.id}" data-id="${result.slide.id}">
																	<i class="fa fa-trash-o" aria-hidden="true"></i>
																</span>
															</div>
														</div>
													</a>
												</div>
												<div id="shead${slide_count+1}" class="collapse" aria-labelledby="faqnewhead${slide_count+1}" data-parent="#faqnew2">
													<div class="card-body row mt-2">
														<div class="col-md-6 prs_img_section prs_img_section${result.slide.id}">
															${img}
														</div>
														<div class="col-md-6">
															<div class="prs_upd_section prs_upd_section${result.slide.id}">
																<label class="button button--primary manual-upload mt-2" for="pres_audio${result.slide.id}"> Upload Audio </label>
																<input type="file" class="manual-file-select pres_audio" id="pres_audio${result.slide.id}" data-id="${result.slide.id}">
																<div class="pres_audio_msg" id="pres_audio_msg${result.slide.id}"></div>
															</div>
															<div class="clearfix mt-3"></div>
															<div class="pres_audio_progress" id="pres_audio_progress${result.slide.id}" style="display:none;">
																<ul class="progress-ul-pres" id="progress-ul-pres${result.slide.id}">
																</ul>
															</div>
														</div>
														<div class="col-md-12 mt-2 prs_desc_section prs_desc_section${result.slide.id}">
															<label>@lang('client_site.description')</label>
															<textarea class="slide_description_${slide_count+1} slide_description" id="slide_description_${slide_count+1}" name="slide_description" data-id="${result.slide.id}">${description}</textarea>
														</div>
														<div class="clearfix mt-3"></div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>`;
								$("#for_P").append(html);
								for(i=1; i<=(slide_count+1); i++){
									initMCEshort("slide_description_"+i);
								}
								upload_progress = false;
							} else {
								setTimeout( function(){
									$('.progress-ul-pres').hide();
									upload_progress = false;
								}, 2000);
								var html5 = `<div class="form-row pres_audios" id="pres_audios${foo}"><audio src="{{URL::to('storage/app/public/lessons/type_presentation/audio/')}}/${result.slide.audio}" controls ></audio></div>`;
								$(html5).insertAfter($("#pres_audio_progress"+foo));
							}
						}
					},
					error: function(err) {
						console.log(err);
					}
				});
			}
		// Presentation







		function loadLessons(lesson_id,chapter_id,ltype){
			$.ajax({
				url: "{{ url('/') }}" + "/get-lesson-details/" + lesson_id,
				method: 'get',
				dataType: 'json',
				async: false,
				success: function(res){
					console.log(res);
					if(typeof(tinyMCE) !== 'undefined') {
						var length = tinyMCE.editors.length;
						for (var i=length; i>0; i--) {
							tinyMCE.editors[i-1].remove();
						};
					}
					$('#chapter_id').val(chapter_id);
					$('#lesson_id').val(lesson_id);
					$('#delete_lesson').attr('data-id', lesson_id);
					$('#delete_lesson').data('id', lesson_id);
					$('#lesson_type').val(ltype);
					$('#lesson_title_'+ltype).val(res.lesson.lesson_title);
					$('#duration_'+ltype).val(res.lesson.duration);
					$('#lsn_title').text(res.lesson.lesson_title);
					$('#is_main_preview_video_div').hide();
					$('.active_lesson').find('.lesson-se > span').text(res.lesson.lesson_title);
					$('#audios').hide();
					$('#thumb').attr('src', "");
					$('#upload_thumbnail_section').show();
					$('#thumb').hide();
					if(ltype == 'V'){
						var html3 = "";
						if($('#video_attachment_list').length) $('#video_attachment_list').remove();
						$('#show_vdo_section').each(function(i, elem){
							$(elem).remove();
						});
						$('#progress_list_video').hide();
						$('#progress_list_video_atch').hide();
						if(res.lesson.is_main_preview_video == 'Y') $('#is_main_preview_video').prop('checked', true);
						else $('#is_main_preview_video').prop('checked', false);
						$('#is_main_preview_video_div').show();

						if(res.main_video != null){
							lesson_main_video_required = false;

							if(res.main_video.filetype == 'V')
								html3 = `<div class="form-row" id="show_vdo_section"><video src="{{url('/')}}/storage/app/public/lessons/type_video/${res.main_video.filename}" width="100%" height="320" controls></video><a href="javascript:;" class="pull-right mt-3 rem_vdo" onclick="removeMainVideo()">@lang('client_site.remove_video')</a></div>`;
							else if(res.main_video.filetype == 'VM')
                            if(res.vimeo_status=='A'){
                                html3 = `<div class="form-row" id="show_vdo_section">
                                    <iframe src="${res.main_video.filename}" width="100%" height="500" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                    <a href="javascript:;" class="pull-right mt-3 rem_vdo" onclick="removeMainVideo()">@lang('client_site.remove_video')</a></div>`;
                                }
                                else{
                                    html3 = `<div class="form-row" id="show_vdo_section">
                                    <h3>Video uploaded successfully video previw available comming soon</h3>
                                    <a href="javascript:;" class="pull-right mt-3 rem_vdo" onclick="removeMainVideo()">@lang('client_site.remove_video')</a></div>`;
                                }

							else
								html3 = `<div class="form-row mb-3" id="show_vdo_section" style="height:320px"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/${res.main_video.filename}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><a href="javascript:;" class="pull-right mt-3 rem_vdo" onclick="removeMainVideo()">@lang('client_site.remove_video')</a></div>`

							$(html3).insertAfter($("#upload_vdo_section"));
							$('#upload_vdo_section').hide();
							$('#show_vdo_section').show();
						} else {
							lesson_main_video_required = true;
							$('#thumb').attr('src','');
							$('#thumb').hide();
							$('#chk6').prop('checked', false);
							$('#chk5').prop('checked', false);
							$('.active_lesson').find('.tool-tip').find('.badge--warning').remove();
							$('.active_lesson').find('.tool-tip').append(`<span class="ml-2 badge badge--warning">@lang("client_site.draft")</span>`);
							$('#upload_vdo_section').show();
							if($('#show_vdo_section').length) $('#show_vdo_section').remove();
						}

						if(res.thumbnail != null){
							$('#thumb').attr('src', `{{url('/')}}/storage/app/public/lessons/type_video/thumbnails/${res.thumbnail.filename}`);
							$('#thumb').show();
						}
						var html = "";
						console.log(res.downloads);
						if(res.downloads.length != 0){
							loadAttachments(res.downloads);
						} else {
							if($('#video_attachment_list').length) $('#video_attachment_list').remove();
						}

						if(res.lesson.description != null){
                            console.log('2');
							$('#video_description').val(res.lesson.description);
						}else{
                            $('#video_description').val('');
                        }
                        initMCEexact("video_description");
					}
					if(ltype == 'T'){
						$('#description').val(res.lesson.description)
						initMCEexact("description");
					}
					if(ltype == 'Q'){
						console.log(res);
						$('.quiz_question').remove();
						$(res.questions).each(function(i, question){
							var qid = parseInt(question.id);
							console.log("CORRECT : "+question.correct_answer);
							var answers = `
							<div class="card">
								<div class="card-header" id="faqheadnew">
									<a href="javascript:;" class=" btn-header-link" data-toggle="collapse" data-target="#choice${i+1}_1" aria-expanded="true" aria-controls="choice${i+1}_1">Alternativa 1</a>
								</div>
								<div id="choice${i+1}_1" class="collapse show" aria-labelledby="faqheadnew" data-parent="#faqnew">
									<div class="card-body">
										<textarea class="answer${i+1}_1" id="answer${i+1}_1" name="answer${i+1}_1">${question.answer_1 == null ? "" : question.answer_1}</textarea>
										<div class="clearfix"></div>
										<div class="dash-d das-radio">
											<input type="radio" name="correct${i+1}" id="correct${i+1}_1" class="correct${i+1} inp_radio" value="1" ${ (question.correct_answer == 1) ? 'checked' : '' }>
											<label for="correct${i+1}_1">@lang('client_site.this_is_correct_answer')</label>
										</div>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-header" id="faqheadnew">
									<a href="javascript:;" class=" btn-header-link" data-toggle="collapse" data-target="#choice${i+1}_2" aria-expanded="true" aria-controls="choice${i+1}_2">Alternativa 2</a>
								</div>
								<div id="choice${i+1}_2" class="collapse show" aria-labelledby="faqheadnew" data-parent="#faqnew">
									<div class="card-body">
										<textarea class="answer${i+1}_2" id="answer${i+1}_2" name="answer${i+1}_2">${question.answer_2 == null ? "" : question.answer_2}</textarea>
										<div class="clearfix"></div>
										<div class="dash-d das-radio">
											<input type="radio" name="correct${i+1}" id="correct${i+1}_2" class="correct${i+1} inp_radio" value="2" ${ (question.correct_answer == 2) ? 'checked' : '' }>
											<label for="correct${i+1}_2">@lang('client_site.this_is_correct_answer')</label>
										</div>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-header" id="faqheadnew">
									<a href="javascript:;" class=" btn-header-link" data-toggle="collapse" data-target="#choice${i+1}_3" aria-expanded="true" aria-controls="choice${i+1}_3">Alternativa 3</a>
								</div>
								<div id="choice${i+1}_3" class="collapse" aria-labelledby="faqheadnew" data-parent="#faqnew">
									<div class="card-body">
										<textarea class="answer${i+1}_3" id="answer${i+1}_3" name="answer${i+1}_3">${question.answer_3 == null ? "" : question.answer_3}</textarea>
										<div class="clearfix"></div>
										<div class="dash-d das-radio">
											<input type="radio" name="correct${i+1}" id="correct${i+1}_3" class="correct${i+1} inp_radio" value="3" ${ (question.correct_answer == 3) ? 'checked' : '' }>
											<label for="correct${i+1}_3">@lang('client_site.this_is_correct_answer')</label>
										</div>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-header" id="faqheadnew">
									<a href="javascript:;" class=" btn-header-link" data-toggle="collapse" data-target="#choice${i+1}_4" aria-expanded="true" aria-controls="choice${i+1}_4">Alternativa 4</a>
								</div>
								<div id="choice${i+1}_4" class="collapse" aria-labelledby="faqheadnew" data-parent="#faqnew">
									<div class="card-body">
										<textarea class="answer${i+1}_4" id="answer${i+1}_4" name="answer${i+1}_4">${question.answer_4 == null ? "" : question.answer_4}</textarea>
										<div class="clearfix"></div>
										<div class="dash-d das-radio">
											<input type="radio" name="correct${i+1}" id="correct${i+1}_4" class="correct${i+1} inp_radio" value="4" ${ (question.correct_answer == 4) ? 'checked' : '' }>
											<label for="correct${i+1}_4">@lang('client_site.this_is_correct_answer')</label>
										</div>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-header" id="faqheadnew">
									<a href="javascript:;" class=" btn-header-link" data-toggle="collapse" data-target="#choice${i+1}_5" aria-expanded="true" aria-controls="choice${i+1}_5">Alternativa 5</a>
								</div>
								<div id="choice${i+1}_5" class="collapse" aria-labelledby="faqheadnew" data-parent="#faqnew">
									<div class="card-body">
										<textarea class="answer${i+1}_5" id="answer${i+1}_5" name="answer${i+1}_5">${question.answer_5 == null ? "" : question.answer_5}</textarea>
										<div class="clearfix"></div>
										<div class="dash-d das-radio">
											<input type="radio" name="correct${i+1}" id="correct${i+1}_5" class="correct${i+1} inp_radio" value="5" ${ (question.correct_answer == 5) ? 'checked' : '' }>
											<label for="correct${i+1}_5">@lang('client_site.this_is_correct_answer')</label>
										</div>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-header" id="faqheadnew">
									<a href="javascript:;" class=" btn-header-link" data-toggle="collapse" data-target="#choice${i+1}_6" aria-expanded="true" aria-controls="choice${i+1}_6">Alternativa 6</a>
								</div>
								<div id="choice${i+1}_6" class="collapse" aria-labelledby="faqheadnew" data-parent="#faqnew">
									<div class="card-body">
										<textarea class="answer${i+1}_6" id="answer${i+1}_6" name="answer${i+1}_6">${question.answer_6 == null ? "" : question.answer_6}</textarea>
										<div class="clearfix"></div>
										<div class="dash-d das-radio">
											<input type="radio" name="correct${i+1}" id="correct${i+1}_6" class="correct${i+1} inp_radio" value="6" ${ (question.correct_answer == 6) ? 'checked' : '' }>
											<label for="correct${i+1}_6">@lang('client_site.this_is_correct_answer')</label>
										</div>
									</div>
								</div>
							</div>
							`;
							var html1 =
							`<div class="course-content-grow quiz_question old_questns" data-id="${i+1}" style="display:none;">
								<div id="main3">
									<div class="accordion3" id="faqnew${i+1+1}">
										<div class="card">
											<div class="card-header" id="faqnewhead${i+1}">
												<input id="quiz_id${i+1}" name="ID_${i+1}" value="${qid}" hidden>
												<a href="#" class=" btn-header-link" data-toggle="collapse" data-target="#qhead${i+1}" aria-expanded="true" aria-controls="qhead${i+1}">
													<h5>@lang('client_site.question') ${i+1} <sup style="display:none;"><i class="fa fa-circle" aria-hidden="true"></i></sup></h5>
													<div class="d-flex align-items-center">
														<div class="button-group">
															<span class="user_llllk new-link3 button del_quiz" id="del_quiz${i+1}" data-id="${qid}" data-count="${i+1}">
																<i class="fa fa-trash-o" aria-hidden="true"></i>
															</span>
														</div>
													</div>
												</a>
											</div>
											<div id="qhead${i+1}" class="collapse" aria-labelledby="faqnewhead${i+1}" data-parent="#faqnew${i+1+1}">
												<div class="card-body">
													<div class="clearfix mt-3"></div>
													<div id="err_correct${i+1}"></div>
													<div class="clearfix mt-2"></div>
													<label>@lang('client_site.question')</label>
													<textarea class="question${i+1}" id="question${i+1}" name="question${i+1}">${question.question}</textarea>

													<div class="accordion4 mt-3" id="faqnew3">
														${answers}
													</div>

													<div class="clearfix mt-3"></div>
													<label>Explanation</label>
													<textarea class="explanation${i+1}" id="explanation${i+1}" name="explanation${i+1}">${question.explanation}</textarea>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>`;
							$('#quiz_count').val(res.questions.length);
							$(html1).insertBefore('#quiz_btns');
						});
						$('.quiz_question').fadeIn();
						var count = $('#quiz_count').val();
						for(i=1; i<=count; i++){
							// console.log("I : " +i);
							initMCEshort("question"+i);
							initMCEshort("answer"+i+"_1");
							initMCEshort("answer"+i+"_2");
							initMCEshort("answer"+i+"_3");
							initMCEshort("answer"+i+"_4");
							initMCEshort("answer"+i+"_5");
							initMCEshort("answer"+i+"_6");
							initMCEshort("explanation"+i);
						}
						setTimeout( function(){
							$('.save_lesson_btn').parent().find('.progrs').remove();
							$('.save_lesson_btn').show();
							$('.save_lesson_btn').parent().find('.discard_lesson').show();
							$('.save_lesson_btn').parent().find('.custom-checkbox').show();
							$('.save_lesson_btn').parent().find('.tools-tip').show();
						},1000);
					}
					if(ltype == 'M'){
						$('#source_url').val(res.lesson.multimedia_url);
					}
					if(ltype == 'A'){
						$('#upload_ado_section').show();
						$('#upload_ado_section label.error').remove();
						$('.audio_msg').empty();
						console.log(res.lesson.description);
						initMCEexact('audio_description');
						$('#audio_description').val(res.lesson.description);
						$('#progress_list_audio').hide();
						if(res.main_audio != null){
							if($('#audios').length) $('#audios').remove();
							var html = `<div class="form-row" id="audios"><audio src="{{URL::to('storage/app/public/lessons/type_audio/')}}/${res.main_audio.filename}" controls ></audio></div>`;
							$(html).insertAfter($("#upload_ado_section"));
							lesson_main_audio_required = false;
						}
					}
					if(ltype == 'D'){
						initMCEexact('download_description');
						$('#download_description').val(res.lesson.description);
						$('#progress_list').hide();
						lesson_main_download_required = false;
						if(res.downloads.length > 0){
							if(!$('#downloads_attachment_list').length){
								var html4 = `<ul class="delete-deta-ul mt-2" id="downloads_attachment_list"></ul>`;
								$(html4).insertAfter($("#progress_list"));
							} else {
								$('#downloads_attachment_list').empty();
							}
							$(res.downloads).each(function(i, download){
								var html0 = `<li class="contentli-item">
									<div class="downloads-reorder">
										<div class="recoder-ff">
											<img src="{{ URL::to('public/frontend/images/cha.png') }}">
										</div>
										<div class="recoder-mm">
											<input type="text" name="download_names[${download.id}]" value="${download.name}" class="form-control download_names" data-id="${download.id}">
										</div>
										<div class="recoder-ll">
											<button type="button" class="button button--icon-only button--knockout del_download" id="del_download_${download.id}" data-id="${download.id}">
												<i class="fa fa-trash-o"></i>
											</button>
										</div>
									</div>
								</li>`;
								$('#downloads_attachment_list').append(html0);
							});
						}
					}
					if(ltype == 'P'){
						$('.presentation_slide').remove();
						$('#progress_list_image').hide();
						var html = "";
						$(res.slides).each(function(i, slide){
							var img = `<img src="{{url('/')}}/storage/app/public/lessons/type_presentation/${slide.filename}"/>`;
							var audio = "";
							if(slide.audio != null) { audio = `<div class="form-row pres_audios" id="pres_audios${slide.id}"><audio src="{{URL::to('storage/app/public/lessons/type_presentation/audio/')}}/${slide.audio}" controls ></audio></div>`; }
							var description = slide.description == null ? "" : slide.description;
							html +=
							`<div class="course-content-grow presentation_slide" id="presentation_slide${slide.id}" data-id="${slide.id}">
								<div id="main3">
									<div class="accordion3" id="faqnew2">
										<div class="card">
											<div class="card-header" id="faqnewhead${i+1}">
												<a href="#" class=" btn-header-link" data-toggle="collapse" data-target="#shead${i+1}" aria-expanded="true" aria-controls="shead${i+1}">
													<h5>Slide ${i+1}</h5>
													<div class="d-flex align-items-center">
														<div class="button-group">
															<span class="user_llllk new-link3 button del_slide" id="del_slide${slide.id}" data-id="${slide.id}">
																<i class="fa fa-trash-o" aria-hidden="true"></i>
															</span>
														</div>
													</div>
												</a>
											</div>
											<div id="shead${i+1}" class="collapse" aria-labelledby="faqnewhead${i+1}" data-parent="#faqnew2">
												<div class="card-body row mt-2">
													<div class="col-md-6 prs_img_section prs_img_section${slide.id}">
														${img}
													</div>
													<div class="col-md-6">
														<div class="prs_upd_section prs_upd_section${slide.id}">
															<label class="button button--primary manual-upload mt-2" for="pres_audio${slide.id}"> Upload Audio </label>
															<input type="file" class="manual-file-select pres_audio" id="pres_audio${slide.id}" data-id="${slide.id}">
															<div class="pres_audio_msg" id="pres_audio_msg${slide.id}"></div>
														</div>
														<div class="clearfix mt-3"></div>
														<div class="pres_audio_progress" id="pres_audio_progress${slide.id}" style="display:none;">
															<ul class="progress-ul-pres" id="progress-ul-pres${slide.id}">
															</ul>
														</div>
														${audio}
													</div>
													<div class="col-md-12 mt-2 prs_desc_section prs_desc_section${slide.id}">
														<label>@lang('client_site.description')</label>
														<textarea class="slide_description_${i+1} slide_description" id="slide_description_${i+1}" name="slide_description" data-id="${slide.id}">${description}</textarea>
													</div>
													<div class="clearfix mt-3"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div></div>`;
						});
						lesson_main_image_required = false;
						$("#for_P").append(html);
						for(i=1; i<=(res.slides.length); i++){
							initMCEshort("slide_description_"+i);
						}
						$('.pres_audio_progress').hide();
					}

					if(res.lesson.is_allow_discussion == 'Y') $('#is_allow_discussion').prop('checked', true);
					else $('#is_allow_discussion').prop('checked', false);

					if(res.lesson.is_prerequisite == 'Y') $('#is_prerequisite').prop('checked', true);
					else $('#is_prerequisite').prop('checked', false);

					if(res.lesson.is_free_preview == 'Y') $('#is_free_preview').prop('checked', true);
					else $('#is_free_preview').prop('checked', false);

					if(res.lesson.status == 'D') {
						$('#chk6').prop('checked', true);
						$('#chk5').prop('checked', true);
					} else {
						$('#chk6').prop('checked', false);
						$('#chk5').prop('checked', false);
					}

				},
				error: function(err){
					console.log(err);
				},
			});
		}

		$(document).on('click', '#chk6', function(e){
			// e.preventDefault();
			if($('#lesson_type').val() == 'V' && $('#upload_vdo_section').is(':visible')){
				toastr.error("@lang('site.upload_video_before_publish')");
				e.preventDefault();
			} else {
				return true;
			}
		});
		$(document).on('click', '#chk5', function(e){
			if($('#lesson_type').val() == 'V' && $('#upload_vdo_section').is(':visible')){
				toastr.error("@lang('site.upload_video_before_publish')");
				e.preventDefault();
			} else {
				return true;
			}
		});

        const youTubeIdFromLink = (url) => url.match(/(?:https?:\/\/)?(?:www\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\/?\?v=|\/embed\/|\/)([^\s&]+)/)[1];

	// LESSONS



	// CHAPTER add/update/delete
		$('#add_chapter').click(function(){
			var id = $('#main_chapter_id').val();
			// $.ajax({
			// 	url: "{{url('/')}}/get-lessons/"+id,
			// 	method: 'get',
			// 	dataType: 'json',
			// 	async: false,
			// 	success: function(res){
			// 		$('.active_chapter p').text(res.chapter.chapter_title);
			// 		$('#chapter_name').text(res.chapter.chapter_title);
			// 		$('#chapter_title').val(res.chapter.chapter_title);
			// 	}
			// });
			$('.right-section').hide();
			$('.admin-build-section_new').show();
			$('.mobile_chapter_list').hide("slide", { direction: "left" }, 500);
			$('.del_chapter').hide();
			$('.discard_chapter').each(function(){
				$(this).attr('data-title', "");
			});
			$('.active_chapter').removeClass('active_chapter');
			$('#chapter_name').text('@lang("client_site.new_chapter") : @lang("client_site.untitled_chapter")');
			$('#chapter_title').val('@lang("client_site.untitled_chapter")');
			$('#main_chapter_id').val('');
			if (!$(".latest_card")[0]){
				$('.accordion_1').append(`<div class="card latest_card" style="display:none">
					<div class="card-header chapter_header" id="faqhead0">
						<a href="#" class="btn btn-header-link acco-chap collapsed active_chapter" data-toggle="collapse" data-target="#faq0" aria-expanded="true" aria-controls="faq0"><img src="{{ URL::to('public/frontend/images/cha01.png') }}">
							<p id="new_chapter_title">@lang('client_site.untitled_chapter')</p>
						</a>
					</div>
				</div>`);
				$('.latest_card').show("slide", { direction: "left" }, 500);
			}
		});
		$('.discard_chapter').click(function(){
			var title = $('.active_chapter p').text();
			console.log(title);
			var id = $('#main_chapter_id').val();
			if(id == ""){
				$('.right-section').hide();
				$('.empty-state').parent().show();
				if ($(".latest_card")[0]){
					$('.latest_card').remove();
				}
			} else {
				$.ajax({
					url: "{{url('/')}}/get-lessons/"+id,
					method: 'get',
					dataType: 'json',
					async: false,
					success: function(res){
						$('.active_chapter p').text(res.chapter.chapter_title);
						$('#chapter_name').text(res.chapter.chapter_title);
						$('#chapter_title').val(res.chapter.chapter_title);
					}
				});
			}
		});
		$('body').delegate('.acco-chap', 'click', function(){
			var id = $(this).parent().data('id');
			var elem = $(this).parent().parent().find('.session-box li');
			// elem.empty();
			$('.secsions').remove();
			$('.active_chapter p').text($('.active_chapter').data('title'))
			$('.active_chapter').removeClass('active_chapter');
			$(this).addClass('active_chapter');
			if(id != null){
				$('.del_chapter').attr('data-id',id);
				$('.del_chapter').show();
			} else {
				$('.del_chapter').hide();
			}
			if( !$(this).parent().parent().hasClass('latest_card') && $(this).hasClass('collapsed') )
			{
				loadLessonCards(id, elem);
				$('#chapter_name').text(Chapter.chapter_title);
				$('.discard_chapter').attr('data-title', Chapter.chapter_title);
				$('#chapter_title').val(Chapter.chapter_title);
				$('.active_chapter p').text(Chapter.chapter_title)
				$('.right-section').hide();
				$('.admin-build-section_new').show();
				$('#main_chapter_id').val($(this).data('id'));
				if ($(".latest_card")[0]){
					$('.latest_card').remove();
				}
			}
		});
		$('#chapter_title').keyup(function(){
			var val = $(this).val();
			$('#chapter_name').text(val);
			$('.active_chapter p').text(val);
			// $('.active_chapter').attr('data-title',val);
			$('#err_chapter_title').remove();

		});
		$('.save_chapter_btn').click(function(){
			var flag = 0;
			var save_chapter_validator = $('#save_chapter').validate({
				rules:{
					chapter_title: {required: true},
				},
				messages:{
					required: "@lang('client_site.enter_chapter_title')",
				},
			});
			var reqData = {
				'jsonrpc' : '2.0',
				'_token' : '{{csrf_token()}}',
				'id' : $('#main_chapter_id').val(),
				'product_id' : '{{@$product->id}}',
				'chapter_title' : $('#chapter_title').val(),
			};
			$.ajax({
				url: "{{ route('check.chapter.title') }}",
				method: 'post',
				dataType: 'json',
				data: reqData,
				async: false,
				success: function(res){
					console.log(res);
					if(res.result == "false"){
						$('#err_chapter_title').remove();
						$('<label id="err_chapter_title" style="color:red;">@lang("client_site.chapter_already_exists")</label>').insertAfter($("#chapter_title"));
						$("#chapter_title").addClass("error")
						flag = 1;
					} else {
						flag = 0;
					}
				},
				error: function(err){
					console.log(err);
				},
			});
			console.log(flag);
			if($('#save_chapter').valid() && flag == 0){
				var set_lessons_status = "P";
				if($('#set_lessons_status').is(":checked")) set_lessons_status = 'D';
				console.log(set_lessons_status);
				var reqData = {
					'jsonrpc' : '2.0',
					'_token' : '{{csrf_token()}}',
					'params' : {
						'ID' : $('#main_chapter_id').val(),
						'product_id' : '{{@$product->id}}',
						'chapter_title' : $('#chapter_title').val(),
						'set_lessons_status' : set_lessons_status,
					}
				};
				$.ajax({
					url: "{{ route('add.chapter') }}",
					method: 'post',
					dataType: 'json',
					data: reqData,
					success: function(response){
						console.log(response);
						if(response.status=="success"){
							loadChapters(response.chapters);
							toastr.success(response.message);
							$('#err_chapter_title').remove();
						}
					},
					error: function(err){
						console.log(err);
					}
				});
			} else {
				console.log(save_chapter_validator.errorList);
				console.log(flag);
			}
		});
		$('.del_chapter').click(function(){
			swal({
				title: "@lang('client_site.delete_chapter')",
				text: "@lang('client_site.delete_chapter_warning')",
				icon: "warning",
				buttons: true,
				dangerMode: true,
				buttons: ['@lang("client_site.cancel")', '@lang("client_site.delete")']
			})
			.then((willDelete) => {
				if (willDelete) {
					var id = $(this).data('id');
					window.location.href = "{{url('/')}}/delete-chapter/"+id;
				} else {
					return false;
				}
			});
		});
		function loadChapters(chapters){
			var html = "";
			$(chapters).each(function(i, chapter){
				html +=
				`<div class="card">
					<div class="card-header chapter_header" id="faqhead${i+1}" data-id="${chapter.id}">
						<a href="#" class="btn btn-header-link acco-chap collapsed"
							data-toggle="collapse"
							data-target="#faq${i+1}"
							aria-expanded="true"
							aria-controls="faq${i+1}"
							data-id="${chapter.id}"
							data-title="${chapter.chapter_title}"
						>
							<img src="{{ URL::to('public/frontend/images/cha01.png') }}">
							<p>${chapter.chapter_title}</p>
						</a>
					</div>
					<div id="faq${i+1}" class="collapse" aria-labelledby="faqhead${i+1}" data-parent="#faq">
						<div class="card-body">
							<div class="session-content">
								<ul class="session-box">
									<li>
										<div class="session-btn" id="section_chapter_${chapter.id}">
											<button class="button add-lesson "><i class="icofont-plus"></i> @lang('client_site.add_lesson')</button>
										</div>
										<div class="add-lesson-info" style="display: none;">
											<span class="types__item" data-ltype="V" data-chapter="${chapter.id}">
												<a href="javascript:;" class="types__link">
													<img src="{{ URL::to('public/frontend/images/video.png') }}">
													<p>@lang('client_site.video')</p>
												</a>
											</span>
											<span class="types__item" data-ltype="Q" data-chapter="${chapter.id}">
												<a href="javascript:;" class="types__link">
													<img src="{{ URL::to('public/frontend/images/quiz.png') }}">
													<p>@lang('client_site.quiz')</p>
												</a>
											</span>
											<span class="types__item" data-ltype="M" data-chapter="${chapter.id}">
												<a href="javascript:;" class="types__link">
													<img src="{{ URL::to('public/frontend/images/multimedia.png') }}">
													<p>@lang('client_site.multimedia')</p>
												</a>
											</span>
											<span class="types__item" data-ltype="T" data-chapter="${chapter.id}">
												<a href="javascript:;" class="types__link">
													<img src="{{ URL::to('public/frontend/images/text.png') }}">
													<p>@lang('client_site.text')</p>
												</a>
											</span>
											<!-- <span class="types__item" data-ltype="P" data-chapter="${chapter.id}">
												<a href="javascript:;" class="types__link">
													<img src="{{ URL::to('public/frontend/images/pd.png') }}">
													<p>Pdf</p>
												</a>
											</span> -->
											<span class="types__item" data-ltype="A" data-chapter="${chapter.id}">
												<a href="javascript:;" class="types__link">
													<img src="{{ URL::to('public/frontend/images/audio.png') }}">
													<p>@lang('client_site.audio')</p>
												</a>
											</span>
											<span class="types__item" data-ltype="D" data-chapter="${chapter.id}">
												<a href="javascript:;" class="types__link">
													<img src="{{ URL::to('public/frontend/images/Download.png') }}">
													<p>@lang('client_site.download')</p>
												</a>
											</span>
											<span class="types__item" data-ltype="P" data-chapter="${chapter.id}">
												<a href="javascript:;" class="types__link">
													<img src="{{ URL::to('public/frontend/images/presentaion.png') }}">
													<p>@lang('client_site.presentation')</p>
												</a>
											</span>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>`;
			});
			$('#faq').empty();
			$('#faq').append(html);
		}
		function loadLessonCards(id, elem){
			console.log("Load lesson cards called");
			console.log(elem);
			$.ajax({
				url: "{{url('/')}}/get-lessons/"+id,
				method: 'get',
				dataType: 'json',
				async: false,
				success: function(res){
					console.log(res);
					Chapter = res.chapter;
					if(res.chapter.set_lessons_status == 'D'){
						$('#set_lessons_status').prop('checked', true);
					} else {
						$('#set_lessons_status').prop('checked', false);
					}
					var html = "";
					$(res.lessons).each(function(i, lesson){
						var img = "";
						if(lesson.lesson_type == 'V')
							img = `<img src="{{ URL::to('public/frontend/images/video.png') }}">`;
						else if(lesson.lesson_type == 'Q')
							img = `<img src="{{ URL::to('public/frontend/images/quiz.png') }}">`;
						else if(lesson.lesson_type == 'M')
							img = `<img src="{{ URL::to('public/frontend/images/multimedia.png') }}">`;
						else if(lesson.lesson_type == 'T')
							img = `<img src="{{ URL::to('public/frontend/images/text.png') }}">`;
						else if(lesson.lesson_type == 'A')
							img = `<img src="{{ URL::to('public/frontend/images/audio.png') }}">`;
						else if(lesson.lesson_type == 'D')
							img = `<img src="{{ URL::to('public/frontend/images/Download.png') }}">`;
						else if(lesson.lesson_type == 'P')
							img = `<img src="{{ URL::to('public/frontend/images/presentaion.png') }}">`;

						html +=
						`<div class="secsions">
							<div class="session_info" data-id="${lesson.id}" data-ltype="${lesson.lesson_type}" data-chapter="${id}">
								<div class="content-handle"><img src="{{ URL::to('public/frontend/images/cha.png') }}"></div>
								<div class="lesson-se"> <span>${lesson.lesson_title}</span>
									<div class="tool-tip d-flex flex-row align-items-center">
										${img}
										${lesson.status == 'D'  ? `<span class="ml-2 badge badge--warning">@lang("client_site.draft")</span>` : ''}
									</div>
								</div>
							</div>
						</div>`;
					});
					$(elem).find('.secsions').remove();
					elem.prepend(html);
				},
				error: function(err){
					console.log(err);
				},
			});
		}
	// CHAPTER add/update/delete




	// text editors

		function initMCEexact(elem){
			tinyMCE.init({
				mode : "specific_textareas",
				editor_selector : elem,
				height: '320px',
				plugins: [
					'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
					'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
					'save table contextmenu directionality emoticons template paste textcolor'
				],
				relative_urls : false,
				remove_script_host : false,
				convert_urls : true,
				setup: function(editor) {
					editor.on('change keyup', function(e){
						// console.log(editor.getContent());
						$('#'+elem).val(editor.getContent());
					})
				},
				toolbar: 'insertfile undo redo | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor emoticons',
				images_upload_url: '{{ URL::to('storage/app/public/lessons/textarea_uploads/') }}',
				images_upload_handler: function(blobInfo, success, failure) {
					var formD = new FormData();
					formD.append('file', blobInfo.blob(), blobInfo.filename());
					formD.append( "_token", '{{csrf_token()}}');
					$.ajax({
						url: '{{route("chapter.text.img.upload")}}',
						data: formD,
						type: 'POST',
						contentType: false,
						cache: false,
						processData:false,
						dataType: 'JSON',
						success: function(jsn) {
							console.log(jsn.status);
							if(jsn.status == 'ERROR') {
								failure(jsn.error);
							} else if(jsn.status == 'SUCCESS') {
								success(jsn.location);
							}
						},
						error: function(err){
							console.log(err);
						}
					});
				},
			});
		}

		function initMCEshort(elem){
			tinyMCE.init({
				mode : "specific_textareas",
				editor_selector : elem,
				height: '150px',
				plugins: [
					'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
					'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
					'save table contextmenu directionality emoticons template paste textcolor'
				],
				relative_urls : false,
				remove_script_host : false,
				convert_urls : true,
				setup: function(editor) {
					editor.on('change keyup', function(e){
						// console.log(editor.getContent());
						$('#'+elem).val(editor.getContent());
					})
				},
				toolbar: 'insertfile undo redo | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor emoticons',
				images_upload_url: '{{ URL::to('storage/app/public/lessons/textarea_uploads/') }}',
				images_upload_handler: function(blobInfo, success, failure) {
					var formD = new FormData();
					formD.append('file', blobInfo.blob(), blobInfo.filename());
					formD.append( "_token", '{{csrf_token()}}');
					$.ajax({
						url: '{{route("chapter.text.img.upload")}}',
						data: formD,
						type: 'POST',
						contentType: false,
						cache: false,
						processData:false,
						dataType: 'JSON',
						success: function(jsn) {
							console.log(jsn.status);
							if(jsn.status == 'ERROR') {
								failure(jsn.error);
							} else if(jsn.status == 'SUCCESS') {
								success(jsn.location);
							}
						},
						error: function(err){
							console.log(err);
						}
					});
				},
			});
			// console.log("TINY : "+elem);
			// console.log(tinyMCE);
		}


	// text editors

</script>

@endsection
