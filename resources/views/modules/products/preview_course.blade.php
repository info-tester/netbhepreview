@extends('layouts.app')
@section('title')
    @lang('site.welcome_to_dashboard')
@endsection
@section('style')
@include('includes.style')
<link href="{{ URL::to('public/frontend/css/course_style.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ URL::to('public/frontend/css/course_responsive.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ URL::to('public/frontend/css/course_bootstrap.min') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::to('public/frontend/icofont/icofont.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::to('public/frontend/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />
<style>
.error{
	color:red;
}
.success{
	color:forestgreen;
}
.quiz-qus > h2{
	display:none !important;
}
.quiz-qus .question{
	display:block !important;
}
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')
    <section class="wrapper2">
		<!-- section-header.// -->
		<!-- ========================= SECTION CONTENT ========================= -->
		<nav class="navbar navbar-expand-lg navbar-dark sticky-top">
			<div class="w-100 main-top-bar">
				<div class="container-fluid">
					<div class="header-con">
						<p>@lang('client_site.you_are_previewing_as_enrolled_student')</p>
					</div>
				</div>
				<!-- container //  -->
			</div>
		</nav>


		<div class="mobile-show">
		  <div class="toggle-wrap ">
		    <span class="toggle-bar"></span>
		  </div>
		  <hgroup>
		    <h3>@lang('client_site.view_course_heading')</h3>

		  </hgroup>
		</div>

		<div class="course-wrap">
			<div class="course-player__content">



				<aside>
				  <div class="course-player__left-drawer">
					<div class="course-player">
						<div class="ember-view">

							<div class="ember-upper-sec">
								<div class="ember-infosec">
									<div class="ember-info-top">
										<p>@lang('client_site.view_course_heading')</p>
									</div>
									<a href="{{route('edit.course.chapters',@$product->id)}}" class="ember-link"><i class="fa fa-angle-left"></i> @lang('site.back')</a>
									<div class="ember-info-bottom">
										<h2>{{@$product->title}}</h2>

										<div class="progress">
										    <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
										      <span class="sr-only">0% @lang('client_site.complete')</span>
										    </div>
										  </div>
										<p><span id="prgrs">0%</span> @lang('client_site.complete')</p>
									</div>
								</div>
							</div>

							<div class="ember-middile-sec">
								<div class="user_lllllk drop-box">
									<span>@lang('client_site.search_lesson_by_title')</span>
									<span><i class="fa fa-caret-down"></i></span>
								</div>
								<div class="show04 drop-search" style="display:none;">
									<div class="drop-btn">
										<input type="text" name="find_chapters" id="find_chapters" onkeyup="findChapters()">
										<input type="hidden" name="product_id" id="product_id" value="{{@$product->id}}">
										<button type="submit" class="serch-submit"><i class="fa fa-search" aria-hidden="true"></i></button>
									</div>

									<div class="drop-list">

									</div>


                                    <!-- <div class="drop-list">
										<h4>Chapter 2</h4>
										<ul>
											<li>Video Lesson</li>
											<li>Untitled quiz</li>
											<li>Test</li>
											<li>Untitled quiz</li>
											<li>Test</li>
										</ul>
									</div> -->

								</div>
							</div>

							<div class="ember-bottom-sec">

								<div id="main4">
									<div class="accordion4" id="faqnewst">
										@foreach($chapters as $k=>$chapter)
											<div class="card" id="card_chap{{$chapter->id}}">
                                                <div class="card-header chapter_head" id="faqhead{{$k+1}}" data-id="{{$chapter->id}}" data-count="{{$k+1}}" data-level="{{$chapter->maxLevel}}">
                                                    <a href="#" class="btn btn-header-link acco-chap chapter_level_{{$chapter->minLevel}}" data-toggle="collapse" data-target="#faq{{$k+1}}" aria-expanded="true" aria-controls="faq{{$k+1}}">
                                                        <!-- <div class="progress-cir">
                                                            <span class="title timer" data-from="0" data-to="85" data-speed="1800">85</span>
                                                            <div class="overlay"></div>
                                                            <div class="left"></div>
                                                            <div class="right"></div>
                                                        </div> -->
														<div class="round_sec">
														</div>

                                                        <p class="cahp-p">{{$chapter->chapter_title}}</p>
                                                        <span class="ml-auto">0/{{$chapter->total_lessons}}</span>
                                                    </a>
                                                </div>
                                                <div id="faq{{$k+1}}" class="collapse lesson_list" aria-labelledby="faqhead{{$k+1}}" data-parent="#faqnewst" data-id="{{$chapter->id}}">
                                                    <div class="card-body">
                                                        <ul class="all-chap">
														@foreach($chapter->lessons as $i=>$lesson)
															<li>
											 					<a href="javascript:;" class="lesson-cont lesson_level_{{@$lesson->level}}" data-id="{{$lesson->id}}" data-count="{{(int)$i + 1}}" data-ltype="{{$lesson->lesson_type}}" data-level="{{$lesson->level}}">
											 						<div class="brand-color__text">
																		@if(@$lesson->progress == true)
																			<i class="fa fa-circle content-item"></i>
																		@else
																			<i class="fa fa-circle-thin content-item"></i>
																		@endif
											 						</div>
											 						<div class="cont-item">
											 							{{@$lesson->lesson_title}}
											 							<div class="lesso-details" id="lesso-details{{@$lesson->id}}">

																			@if(@$lesson->lesson_type == 'V')
																				<img src="{{ URL::to('public/frontend/images/video.png') }}">
																			@elseif(@$lesson->lesson_type == 'Q')
																				<img src="{{ URL::to('public/frontend/images/quiz1.png') }}">
																			@elseif(@$lesson->lesson_type == 'M')
																				<img src="{{ URL::to('public/frontend/images/multimedia.png') }}">
																			@elseif(@$lesson->lesson_type == 'T')
																				<img src="{{ URL::to('public/frontend/images/text.png') }}">
																			@elseif(@$lesson->lesson_type == 'A')
																				<img src="{{ URL::to('public/frontend/images/audio.png') }}">
																			@elseif(@$lesson->lesson_type == 'D')
																				<img src="{{ URL::to('public/frontend/images/Download.png') }}">
																			@elseif(@$lesson->lesson_type == 'P')
																				<img src="{{ URL::to('public/frontend/images/presentaion.png') }}">
																			@endif

											 								<span>
																			 	@if(@$lesson->lesson_type == 'V')
																					@if(@$lesson->main_video->filetype == 'V')
																						@lang('client_site.video') · {{@$lesson->main_video->duration}}
																					@elseif(@$lesson->main_video->filetype == 'Y')
																						@lang('site.video_youTube_embed')
																					@endif
																				@elseif(@$lesson->lesson_type == 'Q')
																					@lang('client_site.quiz') · {{@$lesson->question_count}} {{ (@$lesson->question_count==1) ? 'Question' : 'Questions' }}
																				@elseif(@$lesson->lesson_type == 'M')
																					@lang('client_site.multimedia')
																				@elseif(@$lesson->lesson_type == 'T')
																					@lang('client_site.text')
																				@elseif(@$lesson->lesson_type == 'A')
																					@lang('client_site.audio') · {{@$lesson->main_audio->duration}}
																				@elseif(@$lesson->lesson_type == 'D')
																					@lang('client_site.download')
																				@elseif(@$lesson->lesson_type == 'P')
																					@lang('client_site.presentation') · {{@$lesson->slide_count}} {{ (@$lesson->slide_count==1) ? 'slide' : 'slides' }}
																				@endif
																			</span>

											 							</div>
											 						</div>
											 					</a>
											 				</li>
															@endforeach
                                                        </ul>
                                                        <br>
                                                    </div>
                                                </div>
											</div>
										@endforeach
								</div>

							</div>


						</div>
					</div>
				</div>
				</aside>

				<div class="course-player__right-drawer">
					<div class="course-player__right-inner">
						<div class="course-information_right">
							<div class="rightpannel_title">
								<h2>@lang('site.title')</h2>
								<input type="text" name="course_id" value="{{@$product->id}}" hidden>
								<input type="text" name="chapter_id" id="chapter_id" value="" hidden>
								<input type="text" name="lesson_id" id="lesson_id" value="" hidden>
								<input type="text" name="lesson_type" id="lesson_type" value="" hidden>
								<div class="multimedia pull-right">
								</div>
							</div>

							<div class="rightpannel_middle">
								<div class="rightpannel_middle-info">

								</div>
							</div>


						</div>
					</div>
				</div>

			</div>
		</div>

	</section>
@endsection
@section('footer')
{{-- @include('includes.footer') --}}
<script>
	var questions = slides = allProgress = quizans = [], noOfCorrect = lvl = downloadTimer = 0;

    (function() {
        $('.toggle-wrap').on('click', function() {
            $(this).toggleClass('active');
            $('aside').animate({width: 'toggle'}, 200);
        });
    })();

    $('.drop-box').on('click', function(){
    	$('.drop-search').toggle();
    });

    $(document).ready(function(){

		setTimeout(function() {
			console.log($('.lesson_level_'+0)[0]);
			$('.lesson_level_'+0).trigger( "click" );
			$('.chapter_level_'+0).trigger( "click" );
		}, 10);

        $('.chapter_head').click(function(){
            var id = $(this).data('id');
			$('#chapter_id').val(id);
        });

		$(document).delegate('.lesson-cont', 'click', function(){
			if(!$(this).hasClass('active_lesson_view')){
				var id = $(this).data('id');
				var chapter_id = $('#chapter_id').val();
				var ltype = $(this).data('ltype');
				$('#lesson_id').val(id);
				$('#lesson_type').val(ltype);
				$('.active_lesson_view').removeClass('active_lesson_view');
				$(this).addClass('active_lesson_view');
				loadLessons(id, chapter_id, ltype);
			}
		});

		$('body').delegate('.quiz_options', 'change', function(){
			var val = $(this).val();
			var dataVal = $(this).data('value');
			var correct = $(this).parent().data('correct');
			var correct_count = $('#radio'+correct).data('count');
			$('.ans-status').find('span').remove();
			if(val == correct){
				// $(this).addClass('das-success');
				$('.ans-status').prepend(`<span class="color-green">@lang('client_site.this_answer_is_correct')</span>`);
				$('.ans-status').attr('data-color', "success");
				quizans[dataVal] = 'Y';
			} else {
				// $(this).addClass('das-error');
				$('.ans-status').prepend(`<span class="color-red">@lang('client_site.this_answer_is_incorrect')</span> <span>@lang('client_site.the_correct_answer_is') <b>${correct_count}</b></span>`);
				$('.ans-status').attr('data-color', "error");
				quizans[dataVal] = 'N';
			}
			$('#con-btn').removeClass('button--primary--disabled');
			$('#con-btn').removeAttr('disabled');
		});

		$('body').delegate('#next-quiz-btn', 'click', function(){
			console.log("Next Clicked");
			var id = parseInt($(this).data('id'));
			$('.quiz-part').empty();
			var loadedhtml = loadQuestion(questions, id+1);
			$('.quiz-part').append(loadedhtml);
			if(id+1 == questions.length){
				$('.cmplt_lesson').removeAttr('disabled');
				$('.cmplt_lesson').removeClass('button--primary--disabled');
				$('.lesson_level_'+lvl).find('.brand-color__text').empty().append('<i class="fa fa-circle content-item"></i>');
				// $('.lesson-timer').hide();
				// $('.rightpannel_footer').hide();
			}
		});

		$('body').delegate('.next-slide-btn', 'click', function(){
			console.log("Next Clicked");
			var id = parseInt($(this).data('id'));
			$('.quiz-part').empty();
			var loadedhtml = loadSlide(slides, id);
			$('.quiz-part').append(loadedhtml[0]);
			$('.rightpannel_footer').empty();
			$('.rightpannel_footer').append(loadedhtml[1]);
		});

		$('body').delegate('.prev-slide-btn', 'click', function(){
			console.log("Prev Clicked");
			var id = parseInt($(this).data('id'));
			$('.quiz-part').empty();
			var loadedhtml = loadSlide(slides, id);
			$('.quiz-part').append(loadedhtml[0]);
			$('.rightpannel_footer').empty();
			$('.rightpannel_footer').append(loadedhtml[1]);
		});

		$('body').delegate('#con-btn', 'click', function(){
			$('.quiz_options').attr("disabled",true);
			console.log($('.quiz_options:checked').val());
			var clr = $('.ans-status').data('color');
			$('.quiz_options:checked').parent().addClass('das-'+clr);
			$('#con-btn').parent().parent().parent().find('.ans-status').show();
			$('#con-btn').hide();
			$('#next-quiz-btn').show();
		});

		$('body').delegate('.cmplt_lesson', 'click', function(){
			console.log("Complete Clicked");
			var levels = JSON.parse("{{ json_encode(@$levels) }}");
			var level = parseInt($('.active_lesson_view').data('level'));
			var next_level = levels[levels.indexOf(level)+1];
			if(level == levels[levels.length -1]) next_level = levels[0];
			var chapter_id = $('#chapter_id').val();

			$('.lesson_level_'+level).find('.brand-color__text').empty().append('<i class="fa fa-circle content-item"></i>');
			flag = lowest_level = 0;
			$('.lesson_level_'+lvl).parent().parent().find('.lesson-cont').each(function(i, lsnn){
				if(i==0) lowest_level = $(lsnn).data('level');
				if($(lsnn).find('.content-item').hasClass('fa-circle-thin')){
					flag = 1;
				}
			});
			if(flag == 0){
				$('.chapter_level_'+lowest_level).find('.round_sec').removeClass('round_sec').addClass('round_sec_h');
			}
			$('.lesson_level_'+next_level).trigger( "click" );
			if(level == $('#card_chap'+chapter_id).find('.chapter_head').data('level')){
				$('.chapter_level_'+next_level).trigger( "click" );
			}
		});

		$('body').delegate('.retake-btn', 'click', function(){
			retakeQuiz();
		});

		function loadLessons(lesson_id,chapter_id,ltype){
			$.ajax({
				url: "{{ url('/') }}" + "/get-lesson-details/" + lesson_id,
				method: 'get',
				dataType: 'json',
				async: false,
				success: function(res){
					$('#chapter_id').val(chapter_id);
					$('#lesson_id').val(lesson_id);
					$('#delete_lesson').attr('data-id', lesson_id);
					$('#lesson_type').val(ltype);
					$('#lesson_title_'+ltype).val(res.lesson.lesson_title);
					$('#lsn_title').text(res.lesson.lesson_title);
					lvl = res.lesson.level;
					$('#audios').hide();
					$('.rightpannel_middle-info').empty();
					// $('.lesson-timer').show();
					clearInterval(downloadTimer);
					$('.rightpannel_title').find('.lesson-timer').remove();
					$('.rightpannel_title').find('h2').text(res.lesson.lesson_title);
					secs = parseInt(res.lesson.duration) * 60;
					lsnTimer = `<div class="lesson-timer" style="display:none;"> <progress value="0" max="${secs}" id="progressBar"></progress> <span id="timer_text">00:00:00</span> </div>`
					$('.rightpannel_title').append(lsnTimer);
					lessonTimer(secs);
					$('.course-information_right').removeClass('new_cc_vedo');
					$('.multimedia').empty();
					cmplt_btn = `<button class="button--default--small button--primary_n cmplt_lesson">@lang('site.complete_and_continue') <i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>`;

					var levels = JSON.parse("{{ json_encode(@$levels) }}");
					noOfComplete = parseInt($('.fa-circle').length);
					var prcnt = ((noOfComplete/levels.length)*100).toFixed(2);
					if(prcnt % 1 == 0) prcnt = parseInt(prcnt);
					$('.progress-bar').attr('aria-valuenow', prcnt);
					$('.progress-bar').css('width', prcnt+"%");
					$('#prgrs').text(prcnt+"%");

					if(ltype == 'V'){
						var html1 = vdo = "";
						if(res.downloads.length > 0){
							$(res.downloads).each(function(i, attachment){
								var str = attachment.mimetype;
								str.indexOf("welcome")
								str = str.substring(0,str.indexOf("/")).toUpperCase();
								html1 +=
								`<div class="attachment">
									<div class="attch_names">
										<p>${attachment.name}</p>
										<span>${str}</span>
									</div>
									<a href="{{url('/')}}/storage/app/public/lessons/type_video/attachments/${attachment.filename}" class="btn btn-secondary attch_dwnld" download="${attachment.name}">Download</a>
								</div>`;
							});
						}
						if(res.main_video.filetype == 'V'){
							var poster = res.thumbnail == null ? "" : `{{url('/')}}/storage/app/public/lessons/type_video/thumbnails/${res.thumbnail.filename}`;
							vdo = `<video poster="${poster}" controls loop playsinline autoplay id="vid" controlsList="nodownload"><source src="{{url('/')}}/storage/app/public/lessons/type_video/${res.main_video.filename}" type="video/mp4"></video>`;
						}
                        else if(res.main_video.filetype == 'VM'){
                                console.log(res.thumbnail);
                                var poster = res.thumbnail == null ? "" : `{{url('/')}}/storage/app/public/lessons/type_video/thumbnails/${res.thumbnail.filename}`;
                                // vdo = `<video poster="${poster}" controls loop playsinline autoplay id="vid"><source src="{{url('/')}}/storage/app/public/lessons/type_video/${res.main_video.filename}" type="video/mp4"></video>`;
                                    vdo = `<iframe src="${res.main_video.filename}" width="100%" height="520px" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>`

                            }
                         else {
							vdo = `<iframe width="100%" height="400px" src="https://www.youtube-nocookie.com/embed/${res.main_video.filename}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
						}
						var html =
						`<div class="rightpannel_body">
							<div class="rightpannel_body_apart mt-0">
								${vdo}
								<div class="video_extras">
									<p>${res.lesson.description != null ? res.lesson.description : ''}</p>
									${html1}
								</div>
							</div>
						</div>
						<div class="rightpannel_footer">
							${cmplt_btn}
						</div>`;
						$(".rightpannel_middle-info").append(html);
						document.getElementsByClassName("ytp-chrome-top-buttons")[0];
					}
					if(ltype == 'T'){
						var html =
						`<div class="rightpannel_body">
							<div class="rightpannel_body_apart mt-0">
								<div class="video_extras">
									<p>${res.lesson.description}</p>
								</div>
							</div>
						</div>
						<div class="rightpannel_footer">
							${cmplt_btn}
						</div>`;
						$(".rightpannel_middle-info").append(html);
					}
					if(ltype == 'Q'){
						if(quizans.length != res.questions.length) quizans = [];
						console.log(res);
						questions = res.questions;
						var loadedhtml = "";
						if(quizans.length == 0){
							loadedhtml = loadQuestion(res.questions, 0);
						} else {
							loadedhtml = loadQuestion(res.questions, res.questions.length);
						}
						html =
						`<div class="rightpannel_body">
							<div class="rightpannel_body_apart">
								<div class="quiz-part">
									${loadedhtml}
								</div>
							</div>
						</div>
						<div class="rightpannel_footer">
							${cmplt_btn}
						</div>`;
						$(".rightpannel_middle-info").append(html);
						if(quizans.length == 0){
							$('.cmplt_lesson').attr('disabled', true);
							$('.cmplt_lesson').addClass('button--primary--disabled');
						} else {
							$('.cmplt_lesson').removeAttr('disabled');
							$('.cmplt_lesson').removeClass('button--primary--disabled');
							// $('.lesson-timer').hide();
							// $('.rightpannel_footer').hide();
						}
					}
					if(ltype == 'M'){
						// $('.course-information_right').addClass('new_cc_vedo');
						// var html = `<iframe src="${res.lesson.multimedia_url}" title="${res.lesson.lesson_title}" width="100%" height="100%"></iframe>`;
						// $(".rightpannel_middle-info").append(html);
						var html =
						`<div class="rightpannel_body">
							<iframe src="${res.lesson.multimedia_url}" title="${res.lesson.lesson_title}" width="100%" height="100%"></iframe>
						</div>
						<div class="rightpannel_footer">
							${cmplt_btn}
						</div>`;
						$(".rightpannel_middle-info").append(html);
						var html =
						`<a href="${res.lesson.multimedia_url}" target="_blank" rel="noopener noreferrer" title="Open in new tab">
							<i class="fa fa-external-link" aria-hidden="true"></i>
						</a>`;
						$('.multimedia').append(html);
					}
					if(ltype == 'D'){
						var html1 = "";
						if(res.downloads.length > 0){
							$(res.downloads).each(function(i, attachment){
								var str = attachment.mimetype;
								str.indexOf("welcome")
								str = str.substring(0,str.indexOf("/")).toUpperCase();
								html1 +=
								`<div class="attachment">
									<div class="attch_names">
										<p>${attachment.name}</p>
										<span>${str}</span>
									</div>
									<a href="{{url('/')}}/storage/app/public/lessons/type_video/attachments/${attachment.filename}" class="btn btn-secondary attch_dwnld" download="${attachment.name}">Download</a>
								</div>`;
							});
						}
						var html =
						`<div class="rightpannel_body">
							<div class="rightpannel_body_apart mt-0">
								<div class="video_extras">
									${html1}
									<span class="mt-4 w-100">${res.lesson.description != null ? res.lesson.description : ''}</span>
								</div>
							</div>
						</div>
						<div class="rightpannel_footer">
							${cmplt_btn}
						</div>`;
						$(".rightpannel_middle-info").append(html);
					}
					if(ltype == 'A'){
						var html =
						`<div class="rightpannel_body">
							<div class="rightpannel_body_apart mt-0">
								<div class="video_extras">
									<audio id="aud" src="{{URL::to('storage/app/public/lessons/type_audio/')}}/${res.main_audio.filename}" controls loop playsinline autoplay ></audio>
									<p class="mt-2"></p>
									${res.lesson.description}
								</div>
							</div>
						</div>
						<div class="rightpannel_footer">
							${cmplt_btn}
						</div>`;
						$(".rightpannel_middle-info").append(html);
					}
					if(ltype == 'P'){
						console.log(res);
						slides = res.slides;
						loadSlide(slides, 0);
						var loadedhtml = loadSlide(res.slides, 0);
						html =
						`<div class="rightpannel_body">
							<div class="rightpannel_body_apart">
								<div class="quiz-part">
									${loadedhtml[0]}
								</div>
							</div>
						</div>
						<div class="rightpannel_footer righ_footer_odio">
						</div>`;
						$(".rightpannel_middle-info").append(html);
						$('.rightpannel_footer').append(loadedhtml[1]);
					}

					if(res.lesson.is_allow_discussion == 'Y') $('#is_allow_discussion').prop('checked', true);
					else $('#is_allow_discussion').prop('checked', false);

					if(res.lesson.is_prerequisite == 'Y') $('#is_prerequisite').prop('checked', true);
					else $('#is_prerequisite').prop('checked', false);

					if(res.lesson.is_free_preview == 'Y') $('#is_free_preview').prop('checked', true);
					else $('#is_free_preview').prop('checked', false);

				},
				error: function(err){
					console.log(err);
				},
			});
		}

		function loadQuestion(questions, val){
			var options = ["A", "B", "C", "D", "E", "F"];
			var question = "";
			var c = 0;
			var html = qhtml = next = "";
			next = `<button type="button" class="m-0 con-btn button--primary" id="next-quiz-btn" style="display:none;" data-id="${val}">@lang('client_site.next')</button>`;

			if(val != questions.length){
				for(i=1; i<=6; i++){
					if(questions[val]['answer_'+i] != null){
						question +=
						`<div class="das-radio dash-d1" data-correct="${questions[val]['correct_answer']}">
							<input type="radio" id="radio${i}" name="radios" class="quiz_options" value="${i}" data-count="${options[c]}" data-value="${val}">
							<label for="radio${i}" class="p-0 mb-2"><span class="color-span">${options[c]}</span> <span class="label-cont">${questions[val]['answer_'+i]}</span> </label>
						</div>`;
						c+=1;
					}
				}
				html =
				`<div class="quiz-qus">
					<h5>Questão <span class="question_count">${val+1}</span> de ${questions.length}</h5>
					<input type="text" value="${questions[val]['id']}" id="quiz_id"hidden>
					<h2 class="question">${questions[val]['question']}</h2>
					<p>@lang('client_site.choose_only_one_best_answer')</p>
					<div class="quiz-ans">
						${question}
					</div>
					<div>
						<button type="button" class="m-0 con-btn button--primary button--primary--disabled" id="con-btn" disabled>Confirme</button>
						${next}
					</div>
				</div>
				<div class="ans-status" style="display:none;">
					<div><b>@lang('client_site.explanation'): </b>${questions[val]['explanation']}</div>
				</div><br><br>`;
			} else {
				noOfCorrect = countOccurrences(quizans, 'Y');
				percent = ((parseInt(noOfCorrect)/parseInt(questions.length))*100).toFixed(2);
				if(percent % 1 == 0) percent = parseInt(percent);
				$(questions).each(function(i, question){
					prog = `<span class="quiz-an-red"><i class="fa fa-times-circle" ></i></span>`;
					if(quizans[i] == 'Y') prog = `<span class="quiz-an"><i class="fa fa-check-circle" ></i></span>`;
					qhtml +=
					`<li class="quiz-layer">
						<div class="qz_qstn">
							<span> ${i+1}. </span> ${question['question']} ${prog}
						</div>
						<hr class="quiz-hr">
						${question['explanation']}
					</li>`;
				});
				html =
				`<div class="quiz-result text-center">
					<h5> @lang('client_site.you_completed') Untitled quiz
					<br>
					@lang('client_site.your_score')</h5>
					<h1>${percent}%</h1>
					<div>
						<button class="button--default--small button--primary_n uppercase cmplt_lesson">Continue <i class="fa fa-long-arrow-right" ></i></button>
					</div>
					<div class="mt-2">
						<button class="button--default--small uppercase retake-btn">@lang('site.retake_quiz')</button>
					</div>
				</div>
				<div class="quiz-player">
					<p>Você respondeu ${noOfCorrect} de ${questions.length} perguntas corretamente</p>
					<ul class="quiz-player-ul">
						${qhtml}
					</ul>
				</div>`;
				clearInterval(downloadTimer);
			}
			return html;
		}

		function loadSlide(slides, val){
			var next = prev = audio = "";
			flag = 1;
			cmplt_btn_sld = `<button class="button--default--small button--primary_n button--primary--disabled cmplt_lesson" disabled="true">@lang('site.complete_and_continue') <i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>`;
			prev = `<button class="btn btn-success prv_bt prev-slide-btn" data-id="${val-1}">@lang('client_site.previous')</button>`;
			next = `<button class="btn btn-success prv_bt next-slide-btn" data-id="${val+1}">@lang('client_site.next')</button>`;
			if(val+1 == slides.length){
				// prev = `<button class="btn btn-success prv_bt disable_btns prev-slide-btn" data-id="${val-1}">PREV</button>`;
				next = `<button class="btn btn-success prv_bt disable_btns next-slide-btn" data-id="${val+1}" disabled>@lang('client_site.next')</button>`;
				cmplt_btn_sld = `<button class="button--default--small button--primary_n cmplt_lesson">@lang('site.complete_and_continue') <i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>`;
			}
			if(val == 0){
				prev = `<button class="btn btn-success prv_bt disable_btns prev-slide-btn" data-id="${val-1}" disabled>@lang('client_site.previous')</button>`;
				// next = `<button class="btn btn-success prv_bt next-slide-btn" data-id="${val+1}">NEXT</button>`;
			}
			console.log(slides[val].audio);
			if(slides[val].audio != null)
				audio = `<audio id="presaudio" controls loop autoplay src="{{url('/')}}/storage/app/public/lessons/type_presentation/audio/${slides[val].audio}"></audio>`;
			else
				audio = "";
			var html =
			`<div class="slide">
				<img src="{{url('/')}}/storage/app/public/lessons/type_presentation/${slides[val]['filename']}" width="100%">
				<div class="description mt-3">
					${slides[val].description == null ? "" : slides[val].description}
				</div>
			</div>`;
			var shtml =
			`<div class="pag_next">
				<ul class="dex_pagi">
					<li>${prev}</li>
					<li>${next}</li>
					<li><span>${val+1}/${slides.length}</span></li>
				</ul>
				<ul class="dex_pagiph_pagi">
					<li>${prev}</li>
					<li><span>${val+1}/${slides.length}</span></li>
					<li>${next}</li>
				</ul>
			</div>
			${cmplt_btn_sld}
			<div class="pres_audios">
				${audio}
			</div>`;
			return [ html, shtml ];
		}

		function retakeQuiz(){
			quizans = [];
			lid = $('#lesson_id').val();
			ltype = $('#lesson_type').val();
			cid = $('#chapter_id').val();
			loadLessons(lid, cid, ltype);
			$('.lesson_level_'+lvl).find('.brand-color__text').empty().append('<i class="fa fa-circle-thin content-item"></i>');
		}

		function lessonTimer(time){
			time = parseInt(time);
			var timetotal = time;
			var timeleft = time;
			downloadTimer = setInterval(function(){
			if(timeleft <= 0){
				clearInterval(downloadTimer);
				ltype = $('#lesson_type').val();
				if(ltype == 'V'){
					$('#vid').get(0).pause();
				}
				if(ltype == 'A'){
					$('#aud').get(0).pause();
				}
				if(ltype == 'P'){
					$('#presaudio').get(0).pause();
				}
				swal({
					title: "@lang('site.lesson_complete')",
					text: "@lang('site.continue_or_redo')",
					icon: "success",
					buttons: true,
					dangerMode: true,
					buttons: ["@lang('site.redo_lesson')", "@lang('site.continue')"],
					allowOutsideClick: false,
					closeOnClickOutside: false,
					closeOnEsc: false
				})
				.then((willContinue) => {
					if (willContinue) {
						if(ltype=='Q'){
							for(i=quizans.length; i<questions.length; i++){
								quizans[i] = 'N';
								console.log("I : "+i);
								console.log(quizans);
							}
							console.log(quizans.length);
							$('.cmplt_lesson').removeAttr('disabled');
							$('.cmplt_lesson').removeClass('button--primary--disabled');
							$('.lesson_level_'+lvl).find('.brand-color__text').empty().append('<i class="fa fa-circle content-item"></i>');

							$('.quiz-part').empty();
							var loadedhtml = loadQuestion(questions, questions.length);
							$('.quiz-part').append(loadedhtml);
						} else {
							$('.cmplt_lesson').trigger( "click" );
						}
					} else {
						lessonTimer(time);
						ltype = $('#lesson_type').val();
						if(ltype == 'Q'){
							retakeQuiz();
						} else {
							lid = $('#lesson_id').val();
							ltype = $('#lesson_type').val();
							cid = $('#chapter_id').val();
							loadLessons(lid, cid, ltype);
						}
					}
				});
			}
			document.getElementById("progressBar").value = timetotal - timeleft;
			$('#timer_text').html(convertHMS(timeleft));
			// console.log(convertHMS(timeleft));
			timeleft -= 1;
			}, 1000);
		}

		function convertHMS(value) {
			const sec = parseInt(value, 10); // convert value to number if it's string
			let hours   = Math.floor(sec / 3600); // get hours
			let minutes = Math.floor((sec - (hours * 3600)) / 60); // get minutes
			let seconds = sec - (hours * 3600) - (minutes * 60); //  get seconds
			// add 0 if value < 10; Example: 2 => 02
			if (hours   < 10) {hours   = "0"+hours}
			if (minutes < 10) {minutes = "0"+minutes}
			if (seconds < 10) {seconds = "0"+seconds}
			// if(hours == "00")
			// return minutes+':'+seconds;
			// else
			return hours+':'+minutes+':'+seconds;
		}

		const countOccurrences = (arr, val) => arr.reduce((a, v) => (v === val ? a + 1 : a), 0);

    });
</script>
<script>
	function findChapters(){
		var key = $('#find_chapters').val();

		if(key == ''){

			 $('.card').show();
		}else{
			var reqData = {
            'jsonrpc' : '2.0',
            '_token' : '{{csrf_token()}}',
            'params' : {
                  'key' :  key,
                  'product_id' : $('#product_id').val()
               }
            };
            $.ajax({
            url: "{{ route('find.chapters') }}",
            method: 'post',
            dataType: 'json',
            data: reqData,
            success: function(response){
            	if(response.status == 1){
            		$('.card').hide();
            		$(response.chapters).each(function(i, chapter){
            			$('#card_chap'+chapter.id).show();
            		});
            	}
            	console.log(response.chapters);
            }
        });
		}
	}
</script>
@endsection
