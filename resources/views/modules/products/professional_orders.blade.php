@extends('layouts.app')
@section('title')
    @lang('site.welcome_to_dashboard')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')
<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('client_site.my_sales')</h2>

        <div class="bokcntnt-bdy">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile')<a href="{{ route('professional_qualification') }}">@lang('client_site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <form method="post" action="{{route('professional.orders.search')}}">
                    @csrf
                    <div class="from-field">
                        <div class="row">
                            <div class="col-lg-7">
                                <label class="search_label">@lang('site.keyword')</label>
                                <input type="text" class="form-control" name="keyword" id="keyword" value="{{@$key['keyword']}}">
                            </div>
                            <div class="col-lg-2"></div>
                        </div>


                        <div class="frmfld">
                            <div class="form-group">
                                <label class="search_label">@lang('site.select') @lang('site.status')</label>
                                <select class="dashboard-type dashboard_select" name="status" id="status">
                                    <option value="">@lang('site.select') @lang('site.status')</option>
                                    <option value="I" @if(@$key['status']=='I') selected @endif>@lang('site.initiated') </option>
                                    <option value="PR" @if(@$key['status']=='PR') selected @endif>@lang('site.processing') </option>
                                    <option value="P" @if(@$key['status']=='P') selected @endif>@lang('site.paid')</option>
                                    <option value="F" @if(@$key['status']=='F') selected @endif>@lang('site.failed') </option>
                                </select>
                            </div>
                        </div>
                        <div class="frmfld">
                            <button class="banner_subb fmSbm">@lang('site.filter')</button>
                        </div>
                    </div>
                </form>
                <div class="buyer_table">
                    <div class="table-responsive">
                        @if(sizeof(@$orders)>0)
                        <div class="table">
                            <div class="one_row1 hidden-sm-down only_shawo">
                                 <!-- <div class="cell1 tab_head_sheet" style="white-space: normal !important;">@lang('site.order_id')</div> -->
                                <!-- <div class="cell1 tab_head_sheet" style="white-space: normal !important;">@lang('site.product_image')</div> -->
                                <div class="cell1 tab_head_sheet" style="white-space: normal !important;">@lang('site.title')</div>
                                <div class="cell1 tab_head_sheet" style="white-space: normal !important;">@lang('client_site.ordered_by')</div>
                                <div class="cell1 tab_head_sheet" style="white-space: normal !important;">@lang('site.evaluation_percentage')</div>
                                <!-- <div class="cell1 tab_head_sheet" style="white-space: normal !important;">@lang('site.order_id')</div> -->
                                <!-- <div class="cell1 tab_head_sheet" style="white-space: normal !important;">@lang('site.category')</div> -->
                                <!-- <div class="cell1 tab_head_sheet" style="white-space: normal !important;">@lang('site.ordered_by')</div> -->
                                <div class="cell1 tab_head_sheet" style="white-space: normal !important;">@lang('site.purchase_date')</div>
                                <!-- <div class="cell1 tab_head_sheet" style="white-space: normal !important;">@lang('site.total_amount')</div> -->
                                <div class="cell1 tab_head_sheet" style="white-space: normal !important;">@lang('site.price')</div>
                                {{-- <div class="cell1 tab_head_sheet" style="white-space: normal !important;">posted on</div> --}}
                               <!--  <div class="cell1 tab_head_sheet" style="white-space: normal !important;">@lang('site.status')</div> -->
                                <div class="cell1 tab_head_sheet" style="white-space: normal !important;">{{__('site.payment_status_1')}}<br>{{__('site.payment_status_2')}}
                                </div>
                                <div class="cell1 tab_head_sheet" style="white-space: normal !important;">{{__('site.status')}}</div>
                                <div class="cell1 tab_head_sheet" style="white-space: normal !important;">@lang('site.action')</div>
                            </div>
                            <!--row 1-->
                                @php
                                    $i=1;
                                @endphp
                                @foreach(@$orders as $order)

                                    <div class="one_row1 small_screen31">
                                        <!-- <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.order_id')</span>
                                            <p class="add_ttrr">{{@$order->orderMaster->token_no}}</p>
                                        </div> -->
                                        <!-- <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.product_image')</span>
                                            <img src="{{URL::to('storage/app/public/uploads/product_cover_images/') . '/' . @$order->product->cover_image}}" height="50" width="60">
                                        </div> -->

                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.title')</span>
                                            <p class="add_ttrr">
                                                <small>({{@$order->orderMaster->token_no}})</small>
                                                <br/>
                                                <a href="{{ route('product.details', @$order->product->slug) }}">{{@$order->product->title}}</a>
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.ordered_by')</span>
                                            <p class="add_ttrr">{{@$order->orderMaster->userDetails->nick_name ? @$order->orderMaster->userDetails->nick_name : @$order->orderMaster->userDetails->name}}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.evaluation_percentage')</span>
                                            <p class="add_ttrr">{{@$order->percentage}} %</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.purchase_date')</span>
                                            <p class="add_ttrr">
                                                @php $month=[ "0", 'Jan', 'Fev', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Set', 'Out', 'Nov', 'Dez' ] @endphp
                                                {{date('d',strtotime(@$order->created_at))}}
                                                {{$month[(int)date('m',strtotime(@$order->created_at))]}}
                                                {{date('Y',strtotime(@$order->created_at))}}
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.price')</span>
                                            <p class="add_ttrr">R$ {{@$order->amount}} </p>
                                        </div>

                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.status')</span>
                                            @if(@$order->orderMaster->payment_status == 'I')
                                            <p class="add_ttrr">
                                                @lang('client_site.initiated')
                                            </p>
                                            @elseif(@$order->orderMaster->payment_status == 'F')
                                            <p class="add_ttrr">
                                                @lang('client_site.failed')
                                            </p>
                                            @elseif(@$order->orderMaster->payment_status == 'P')
                                            <p class="add_ttrr">
                                                @lang('client_site.paid')
                                            </p>
                                            @elseif(@$order->orderMaster->payment_status == 'PR')
                                            <p class="add_ttrr">
                                               @lang('client_site.processing_payment')
                                            </p>
                                            @endif
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.status')</span>
                                            <p class="add_ttrr">

                                                @if(@$order->order_status == 'A' && @$order->cancel_request =='N' && $order->orderMaster->payment_status == 'P') {{__('site.product_active')}}
                                                @elseif(@$order->order_status == 'C' && $order->orderMaster->payment_status == 'P' && @$order->is_cancel_responce=='N'){{__('site.product_cancel')}}
                                                @elseif(@$order->order_status == 'C' && $order->orderMaster->payment_status == 'P' && @$order->is_cancel_responce=='A'){{__('site.product_order_cancelation_accept')}}
                                                @elseif($order->orderMaster->payment_status == 'P' && @$order->is_cancel_responce=='R'){{__('site.product_order_cancelation_reject')}}
                                                @elseif(@$order->order_status == 'A' && @$order->cancel_request =='Y' && $order->orderMaster->payment_status == 'P') {{__('site.product_request')}}
                                                @else
                                                {{__('site.product_inprogress')}}
                                                @endif
                                            </p>
                                        </div>


                                        <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                            <a href="{{route('view.professional.order',[@$order->orderMaster->id,@$order->product->id])}}" class="acpt mb-1">
                                            @lang('site.view_order')</a>

                                            <a href="{{route('view.course', ['id' => @$order->product->id, 'master_id' => @$order->orderMaster->id])}}" class="acpt assginbtn mb-1">@lang('client_site.view_course')</a>
                                            {{-- <a href="{{route('delete.product',@$order->id)}}" class="rjct mb-1" onclick="return confirm('@lang('site.do_delete_product')');">@lang('site.delete')</a> --}}
                                            @if(@$order->percentage == 100)

                                                @if(@$order->is_allowed_certificate == 'Y')
                                                <p>@lang('site.sended_certificate')</p>
                                                @else
                                                    @if(@$order->product->assignedTemplate)
                                                        <a href="javascript:;" data-product="{{@$order->product->id}}" data-user="{{@$order->orderMaster->user_id}}" data-token="{{@$order->orderMaster->token_no}}" data-order="{{@$order->id}}" class="rjct allow_download">@lang('site.send_certificate')</a>
                                                    @endif
                                                    @if(@$order->product->assignedTemplate)
                                                    <a href="{{route('preview.template.certificate',[@$order->id,@$order->product->id])}}" class="rjct">@lang('site.preview_certificate')</a>
                                                    @else
                                                    <a href="{{route('assign.product.certificate.template',@$order->product->id)}}" class="acpt">@lang('site.assign_template')</a>
                                                    @endif
                                                @endif


                                            @endif
                                            @if($order->orderMaster->sub_total != 0 && $order->orderMaster->payment_status=='P' && @$order->order_status=='A' && $order->cancel_request=='N' && ($order->percentage<20 && toUTCTime(date('Y-m-d H:i:s'))<toUTCTime(date('Y-m-d H:i:s', strtotime($order->product->purchase_end_date."+".@$order->product->cancel_day." days")))))
                                            <a href="javascript:;" class="rjct mb-1 w-100 text-center cancelbtn" data-token="{{$order->orderMaster->token_no}}" data-id="{{$order->id}}">{{__('site.product_order_cancel')}}</a>
                                            @endif
                                            @if($order->orderMaster->sub_total != 0 && $order->orderMaster->payment_status=='P' && @$order->order_status=='A' && $order->cancel_request=='Y' && $order->is_cancel_responce=='N' && ($order->percentage < 20 && toUTCTime(date('Y-m-d H:i:s'))<toUTCTime(date('Y-m-d H:i:s', strtotime($order->product->purchase_end_date."+".@$order->product->cancel_day." days")))))
                                            <a href="{{route('product.order.cancel',['token'=>$order->orderMaster->token_no,'id'=>$order->id])}}" class="rjct mb-1 w-100 text-center" onclick="return confirm('{{__('site.product_order_cancel_acceped_message')}}')">{{__('site.product_order_cancel_acceped')}}</a>
                                            <a href="javascript:;" class="rjct mb-1 w-100 text-center rejectCancelBtn"  data-token="{{$order->orderMaster->token_no}}" data-id="{{$order->id}}">{{__('site.product_order_cancel_reject')}}</a>
                                            @endif
                                            {{-- @if(toUTCTime(date('Y-m-d H:i:s'))<toUTCTime(date('Y-m-d H:i:s', strtotime($order->created_at."+6 days"))))
                                            1
                                            @endif --}}
                                            {{-- {{toUTCTime(date('Y-m-d H:i:s', strtotime($order->created_at."+6 days")))}}
                                        {{toUTCTime(date('Y-m-d H:i:s'))}} --}}
                                        </div>
                                    </div>
                                    @php
                                        $i++;
                                    @endphp
                                @endforeach
                        </div>
                        <div class="float-right">
                            {{ @$orders->links('vendor.pagination.default') }}
                        </div>
                        @else
                            <div class="one_row small_screen31">
                                <center><span><h3 class="error">OOPS! @lang('site.product_not_found').</h3></span></center>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

        {{-- modal --}}
        <div class="modal fade" id="modalcancel">
            <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
                  <!-- Modal Header -->
                <div class="modal-header">
                  <h4 class="modal-title">{{__('site.product_order_cancel')}} </h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form name="myForm3" id="myForm3" method="post">
                        @csrf
                        <input type="hidden" name="token_no" id="token_no">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group" style="width: 100%;">
                                        <label>{{__('site.cancellation_reason')}}</label>
                                        <input type="text" class="required form-control" name="reason" value="" >
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <input type="submit" value="@lang('client_site.cancel')" class="btn btn-primary">
                                    </div>
                                </div>
                          </div>
                        </div>
                    </form>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('site.close')</button>
                </div>

              </div>
            </div>
        </div>

        {{-- modal --}}
        <div class="modal fade" id="modalcancelreject">
            <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
                  <!-- Modal Header -->
                <div class="modal-header">
                  <h4 class="modal-title">{{__('site.product_order_cancel_reject')}} </h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form name="myForm4" id="myForm4" method="post">
                        @csrf
                        <input type="hidden" name="token_no" id="token_no">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group" style="width: 100%;">
                                        <label>{{__('site.rejection_reason')}}</label>
                                        <input type="text" class="required form-control" name="reason" value="" >
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <input type="submit" value="@lang('client_site.reject')" class="btn btn-primary">
                                    </div>
                                </div>
                          </div>
                        </div>
                    </form>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('site.close')</button>
                </div>

              </div>
            </div>
        </div>
</section>

@endsection
@section('footer')
@include('includes.footer')
<script>
    $('.allow_download').on('click',function(){
        var order_id = $(this).attr('data-order');

        if(confirm("@lang('site.allow_certificate')")){
            var reqData = {
                'jsonrpc' : '2.0',
                '_token' : '{{csrf_token()}}',
                'params' : {
                    'order_id'   : order_id
                }
            };

            $.ajax({
                url: "{{ route('allow.download.certificate') }}",
                method: 'post',
                dataType: 'json',
                data: reqData,
                success: function(response){
                    if(response.status == 1){
                        toastr.success("@lang('site.send_certificate_successfully')");
                        location.reload();
                    }
                    if(response.status == 0){
                        toastr.info('Algo deu errado');
                    }
                    if(response.status == 2){
                        toastr.info('Atribua um modelo de certificado antes de enviar o certificado');
                    }
                }, error: function(error) {
                    console.error(error);
                }
            });



        }
    });
    $(document).ready(function(){
        $('#myForm3').validate({});
    })
    $('.cancelbtn').click(function(){
        var id= $(this).data('id');
        var token= $(this).data('token');
        console.log('1')
        $('#modalcancel').modal('show');
        var url= '{{route('home')}}/product-order-cancel/'+token+'/'+id;
        $('#myForm3').attr('action', url)
    });
    $('.rejectCancelBtn').click(function(){
        if(confirm('{{__('site.product_order_cancel_reject_message')}}')){
            var id = $(this).data('id');
            var token = $(this).data('token');
            console.log('2');
            $('#modalcancelreject').modal('show');
            var url= '{{route('home')}}/product-order-cancel-reject/'+token+'/'+id;
            $('#myForm4').attr('action', url)
        } else {
            return false;
        }
    });
</script>
@endsection
