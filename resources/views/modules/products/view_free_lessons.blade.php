@extends('layouts.app')
@section('title')
    @lang('site.welcome_to_dashboard')
@endsection
@section('style')
@include('includes.style')
<link href="{{ URL::to('public/frontend/css/course_style.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ URL::to('public/frontend/css/course_responsive.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ URL::to('public/frontend/css/course_bootstrap.min') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::to('public/frontend/icofont/icofont.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::to('public/frontend/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />
<style>
.error{
	color:red;
}
.success{
	color:forestgreen;
}
.quiz-qus > h2{
	display:none !important;
}
.quiz-qus .question{
	display:block !important;
}
#view_questions{
	font-size: 20px important;
}
.ytp-chrome-top-buttons{
	display: none !important;
}
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
{{-- @include('includes.professional_header') --}}
@include('includes.header')
@endsection
@section('content')
    <section class="wrapper2">
		<div class="mobile-show">
		  <div class="toggle-wrap ">
		    <span class="toggle-bar"></span>
		  </div>
		  <hgroup>
		    <h3>@lang('client_site.view_course_heading')</h3>
		  </hgroup>
		</div>

		<div class="course-wrap">
			<div class="course-player__content">
				<aside>
				  <div class="course-player__left-drawer">
					<div class="course-player">
						<div class="ember-view">
							<div class="ember-upper-sec">
								<div class="ember-infosec">
									<div class="ember-info-top">
										<p>@lang('client_site.view_course_heading')</p>
									</div>
                                    <a href="{{route('product.details', @$product->slug)}}" class="ember-link"><i class="fa fa-angle-left"></i> @lang('site.back')</a>
									<div class="ember-info-bottom">
										<h2>{{@$product->title}}</h2>
										<div class="progress">
										    <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
										      <span class="sr-only">0% @lang('client_site.complete')</span>
										    </div>
										  </div>
										<p><span id="prgrs">0%</span> @lang('client_site.complete')</p>
									</div>
								</div>
							</div>
							<div class="ember-middile-sec">
								<div class="user_lllllk drop-box">
									<span>@lang('client_site.search_lesson_by_title')</span>
									<span><i class="fa fa-caret-down"></i></span>
								</div>
								<div class="show04 drop-search" style="display:none;">
									<div class="drop-btn">
										<input type="text" name="find_chapters" id="find_chapters" onkeyup="findChapters()">
										<input type="hidden" name="product_id" id="product_id" value="{{@$product->id}}">
										<button type="submit" class="serch-submit"><i class="fa fa-search" aria-hidden="true"></i></button>
									</div>
									<div class="drop-list">
									</div>
								</div>
							</div>

							<div class="ember-bottom-sec">
								<div id="main4">
									<div class="accordion4" id="faqnewst">
										@foreach($chapters as $k=>$chapter)
											<div class="card" id="card_chap{{$chapter->id}}">
                                                <div class="card-header chapter_head" id="faqhead{{$k+1}}" data-id="{{$chapter->id}}" data-count="{{$k+1}}" data-level="{{$chapter->maxLevel}}">
                                                    <a href="#" class="btn btn-header-link acco-chap chapter_level_{{$chapter->minLevel}}" data-toggle="collapse" data-target="#faq{{$k+1}}" aria-expanded="true" aria-controls="faq{{$k+1}}">
                                                        <!-- <div class="progress-cir">
                                                            <span class="title timer" data-from="0" data-to="85" data-speed="1800">85</span>
                                                            <div class="overlay"></div>
                                                            <div class="left"></div>
                                                            <div class="right"></div>
                                                        </div> -->
														<div class="{{ (@$chapter->complete == true) ? 'round_sec_h' : 'round_sec' }}">
														</div>
                                                        <p class="cahp-p">{{$chapter->chapter_title}}</p>
                                                        <span class="ml-auto"><span class="increment">{{$chapter->progress_count}}</span>/{{$chapter->total_lessons}}</span>
                                                    </a>
                                                </div>
                                                <div id="faq{{$k+1}}" class="collapse lesson_list" aria-labelledby="faqhead{{$k+1}}" data-parent="#faqnewst" data-id="{{$chapter->id}}">
                                                    <div class="card-body">
                                                        <ul class="all-chap">
															@foreach($chapter->lessons as $i=>$lesson)
															<li>
											 					<a href="javascript:;" class="lesson-cont lesson_level_{{@$lesson->level}}" data-id="{{$lesson->id}}" data-count="{{(int)$i + 1}}" data-ltype="{{$lesson->lesson_type}}" data-level="{{$lesson->level}}">
											 						<div class="brand-color__text">
																		@if(@$lesson->is_free_preview == 'N')
																			<i class="fa fa-lock" style="color:grey; font-size:16px;"></i>
																		@else
																			@if(@$lesson->progress == true)
																				<i class="fa fa-circle content-item"></i>
																			@else
																				<i class="fa fa-circle-thin content-item"></i>
																			@endif
																		@endif
											 						</div>
											 						<div class="cont-item">
											 							{{@$lesson->lesson_title}}
											 							<div class="lesso-details" id="lesso-details{{@$lesson->id}}">

																			@if(@$lesson->lesson_type == 'V')
																				<img src="{{ URL::to('public/frontend/images/video.png') }}">
																			@elseif(@$lesson->lesson_type == 'Q')
																				<img src="{{ URL::to('public/frontend/images/quiz1.png') }}">
																			@elseif(@$lesson->lesson_type == 'M')
																				<img src="{{ URL::to('public/frontend/images/multimedia.png') }}">
																			@elseif(@$lesson->lesson_type == 'T')
																				<img src="{{ URL::to('public/frontend/images/text.png') }}">
																			@elseif(@$lesson->lesson_type == 'A')
																				<img src="{{ URL::to('public/frontend/images/audio.png') }}">
																			@elseif(@$lesson->lesson_type == 'D')
																				<img src="{{ URL::to('public/frontend/images/Download.png') }}">
																			@elseif(@$lesson->lesson_type == 'P')
																				<img src="{{ URL::to('public/frontend/images/presentaion.png') }}">
																			@endif

											 								<span>
																			 	@if(@$lesson->lesson_type == 'V')
																					@if(@$lesson->main_video->filetype == 'V')
                                                                                    @lang('client_site.video') · {{@$lesson->main_video->duration}}
																					@elseif(@$lesson->main_video->filetype == 'Y')
                                                                                    @lang('site.video_youTube_embed')
																					@endif
																				@elseif(@$lesson->lesson_type == 'Q')
                                                                                @lang('client_site.quiz') · {{@$lesson->question_count}} {{ (@$lesson->question_count==1) ? 'Question' : 'Questions' }}
																				@elseif(@$lesson->lesson_type == 'M')
                                                                                @lang('client_site.multimedia')
																				@elseif(@$lesson->lesson_type == 'T')
                                                                                @lang('client_site.text')
																				@elseif(@$lesson->lesson_type == 'A')
                                                                                @lang('client_site.audio') · {{@$lesson->main_audio->duration}}
																				@elseif(@$lesson->lesson_type == 'D')
                                                                                @lang('client_site.download')
																				@elseif(@$lesson->lesson_type == 'P')
                                                                                @lang('client_site.presentation') · {{@$lesson->slide_count}} {{ (@$lesson->slide_count==1) ? 'slide' : 'slides' }}
																				@endif
																			</span>

											 							</div>
											 						</div>
											 					</a>
											 				</li>
															@endforeach
                                                        </ul>
                                                        <br>
                                                    </div>
                                                </div>
											</div>
										@endforeach
								</div>
							</div>
						</div>
					</div>
				</div>
				</aside>

				<div class="course-player__right-drawer">
					<div class="course-player__right-inner">
						<div class="course-information_right">
							<div class="qua_secc" style="display:none;">
								<div class="question_posted_sec">
								</div>
								<div id="ask_here" style="display:none;" class="bottom_fxdd">
									<label class="label">@lang('client_site.ask_question_here')</label>
									<input type="text" class="form-control" name="question" id="question" placeholder="@lang('site.enter_your_question_here')">
                                    <input type="submit" value="Submit" class="sum_btn" id="submit_qstn_btn" />
								</div>
								<div id="answer_here" style="display:none;" class="bottom_fxdd">
									<label class="label">@lang('client_site.answer_to'): </label>
									<input type="text" name="question_id" id="question_id" hidden>
									<input type="text" class="form-control" name="answer" id="answer" placeholder="@lang('site.enter_your_answer_here')">
                                    <input type="submit" value="Submit" class="sum_btn" id="submit_ans_btn"/>
								</div>
							</div>
							<div class="rightpannel_title">
								<h2></h2>
								<div>
									<div class="lesson-timer" style="display:none;"> </div>
									<div class="title_right_btns pull-right"> </div>
								</div>
								<input type="text" name="course_id" value="{{@$product->id}}" hidden>
								<input type="text" name="chapter_id" id="chapter_id" value="" hidden>
								<input type="text" name="lesson_id" id="lesson_id" value="" hidden>
								<input type="text" name="lesson_type" id="lesson_type" value="" hidden>
								<input type="text" name="course_progress_id" id="course_progress_id" value="" hidden>
							</div>
							<div class="rightpannel_middle">
								<div class="rightpannel_middle-info">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

@endsection
@section('footer')
{{-- @include('includes.footer') --}}

<script>

	var questions = slides = allProgress = quizans = quizvisted = [], quizReqData = {}, noOfCorrect = lvl = downloadTimer = timeleft = 0;

    (function() {
        $('.toggle-wrap').on('click', function() {
            $(this).toggleClass('active');
            $('aside').animate({width: 'toggle'}, 200);
        });
    })();

	$('body').delegate('#view_questions', 'click', function(){
		console.log($('#view_questions').css('display'));
		if($('.qua_secc').css('display') =='none'){
        	$(".qua_secc").slideDown();
			$('.rightpannel_title h2').html($('.rightpannel_title h2').text() + `   &#62;    Q/A`);
			$('#view_questions').html(`&#60; @lang('site.back_to_lesson')`);
		} else {
			$(".qua_secc").slideUp();
			$('.rightpannel_title h2').html($('.rightpannel_title h2').text().slice(0,-10));
			$('#view_questions').html(`<img src="{{ URL::to('public/frontend/images/qa.png') }}" height="30px" title="@lang('site.view_question_answers')"/><span style="color: #007bff; font-size: 20px;"> Q/A</span>`);
		}
    });

    $('.drop-box').on('click', function(){
    	$('.drop-search').toggle();
    });

	$("#question").on('keyup', function (e) {
		if (e.key === 'Enter' || e.keyCode === 13) {
			submitQuestion();
		}
	});

	$('#submit_qstn_btn').click(function(){
		submitQuestion();
	});

	function submitQuestion(){
		var reqData = {
			'jsonrpc' : '2.0',
			'_token' : '{{csrf_token()}}',
			'params' : {
				'lesson_id' : $('#lesson_id').val(),
				'question' : $('#question').val()
			}
		};
		$.ajax({
			url: "{{route('post.lesson.question')}}",
			method: 'post',
			dataType: 'json',
			data: reqData,
			success: function(res){
				console.log(res);
				if(res.status == "success"){
					img = ("{{Auth::user()->profile_pic}}" != "") ? "{{url('/').'/storage/app/public/uploads/profile_pic/'.Auth::user()->profile_pic}}" : "{{url('/')}}/public/frontend/images/no_img.png";
					name = "{{Auth::user()->nick_name ?? Auth::user()->name}}";
					var html =
					`<div class="question_box quesqq" id="question_box_${res.question.id}">
						<span class="question_user"><img src="${img}"></span>
						<div class="question_details">
							<a href="javascript:;" class="btn btn-danger pull-right del_qs" data-id="${res.question.id}"><i class="fa fa-trash" aria-hidden="true"></i></a>
							<h4>${res.question.question}</h4>
							<h5>${name} <strong>Just now</strong></h5>
						</div>
					</div>`;
					if($('.question_posted_sec > p').text() == "@lang('site.no_questions_posted_yet')"){
						$('.question_posted_sec').empty();
					}
					$('.question_posted_sec').append(html);
					$('#question').val("");
					toastr.success(res.message);
				} else {
					toastr.error(res.message);
				}
			},
			error: function(err){
				console.log(err);
			},
		});
	}

	$('#answer').on('keyup', function (e) {
		if (e.key === 'Enter' || e.keyCode === 13) {
			submitAnswer();
		}
	});

	$('#submit_ans_btn').click(function(){
		submitAnswer();
	});

	function submitAnswer(){
		var reqData = {
			'jsonrpc' : '2.0',
			'_token' : '{{csrf_token()}}',
			'params' : {
				'question_id' : $('#question_id').val(),
				'answer' : $('#answer').val()
			}
		};
		$.ajax({
			url: "{{route('post.lesson.answer')}}",
			method: 'post',
			dataType: 'json',
			data: reqData,
			success: function(res){
				console.log(res);
				if(res.status == "success"){
					img = ("{{Auth::user()->profile_pic}}" != "") ? "{{url('/').'/storage/app/public/uploads/profile_pic/'.Auth::user()->profile_pic}}" : "{{url('/')}}/public/frontend/images/no_img.png";
					name = "{{Auth::user()->nick_name ?? Auth::user()->name}}";
					style = "";
					if($('#question_box_'+res.question.id+' .ansqq').css("display") == "none")
						style = `style="display:none;"`;
					else
						style = `style="display:block;"`;

					var html =
					`<div class="question_box ansqq" id="answer_box_${res.answer.id}" ${style}>
						<span class="question_user"><img src="${img}"></span>
						<div class="question_details">
							<a href="javascript:;" class="btn btn-danger pull-right del_ans" data-id="${res.answer.id}"><i class="fa fa-trash" aria-hidden="true"></i></a>
							<h5>${name} <strong>Just now</strong></h5>
							<p>${res.answer.answers}</p>
						</div>
					</div>`;
					if($('#question_box_'+res.question.id+' > .question_details').find('.ansqq').length == 0){
						$('#question_box_'+res.question.id+' > .question_details').append(`<h6><a href="javascript:;" class="view_ans" data-id="${res.question.id}">Hide Answers</a></h6>`);
					}
					$('#question_box_'+res.question.id+' > .question_details').append(html);
					$('#answer').val("")
					toastr.success(res.message);
				} else {
					toastr.error(res.message);
				}
			},
			error: function(err){
				console.log(err);
			},
		});
	}

    $(document).ready(function(){
		setTimeout(function() {
			var last_visit_level = "{{@$last_visit_level}}";
			console.log(" LAST VISITED : "+ last_visit_level);
			console.log($('.lesson_level_'+last_visit_level)[0]);
			$('.lesson_level_'+last_visit_level).trigger( "click" );
			$('.chapter_level_'+last_visit_level).trigger( "click" );
		}, 10);

        $('body').delegate('#buyNow', 'click', function(){
            var productId = $(this).data('product');
            console.log("Buy Product");
            console.log(productId)
            var reqData = {
                'jsonrpc': '2.0',
                '_token': '{{csrf_token()}}',
                'params': {
                    productId: productId,
                }
            };
                $.ajax({
                url: '{{ route('product.add.to.cart') }}',
                type: 'post',
                dataType: 'json',
                data: reqData,
            })
            .done(function(response) {
                    window.location.href = '{{route('product.order.store')}} ';
            })
            .fail(function(error) {
                console.log("error", error);
            })
            .always(function() {
                console.log("complete");
            });
        });
		$('.chapter_head').click(function(){
            var id = $(this).data('id');
			$('#chapter_id').val(id);
		});

		$('body').delegate('.lesson-cont', 'click', function(){
			if(!$(this).hasClass('active_lesson_view')){
				var id = $(this).data('id');
				var chapter_id = $('#chapter_id').val();
				var ltype = $(this).data('ltype');
				$('.active_lesson_view').removeClass('active_lesson_view');
				$(this).addClass('active_lesson_view');
				$('#lesson_id').val(id);
				$('#lesson_type').val(ltype);
				$('#question').val('');
				loadLessons(id, chapter_id, ltype);
			}
		});

		$('body').delegate('.quiz_options', 'change', function(){
			var val = $(this).val();
			var dataVal = $(this).data('value');
			var correct = $(this).parent().data('correct');
			var correct_count = $('#radio'+correct).data('count');
			$('.ans-status').find('span').remove();
			$('#user_answer').val(val);

			if(val == correct){
				$('.ans-status').prepend(`<span class="color-green">@lang('client_site.this_answer_is_correct')</span>`);
				$('.ans-status').attr('data-color', "success");
				if("{{Auth::id()}}" == "{{@$product->professional->id}}") quizans[dataVal] = 'Y';
				else {
					quizReqData = {
						'jsonrpc' : '2.0',
						'_token' : '{{csrf_token()}}',
						'params' : {
							'course_id' : $('#product_id').val(),
							'lesson_id' : $('#lesson_id').val(),
							'course_progress_id' : $('#course_progress_id').val(),
							'question_id' : $('#quiz_id').val(),
							'user_answer' : val,
							'correct_answer' : correct,
							'is_correct' : 'Y'
						}
					};
				}
			} else {
				$('.ans-status').prepend(`<span class="color-red">@lang('client_site.this_answer_is_incorrect')</span> <span>@lang('client_site.the_correct_answer_is') <b>${correct_count}</b></span>`);
				$('.ans-status').attr('data-color', "error");
				if("{{Auth::id()}}" == "{{@$product->professional->id}}") quizans[dataVal] = 'N';
				else {
					quizReqData = {
						'jsonrpc' : '2.0',
						'_token' : '{{csrf_token()}}',
						'params' : {
							'course_id' : $('#product_id').val(),
							'lesson_id' : $('#lesson_id').val(),
							'course_progress_id' : $('#course_progress_id').val(),
							'question_id' : $('#quiz_id').val(),
							'user_answer' : val,
							'correct_answer' : correct,
							'is_correct' : 'N'
						}
					};
				}
			}
			// console.log(quizans);
			$('#con-btn').removeClass('button--primary--disabled');
			$('#con-btn').removeAttr('disabled');
		});

		$('body').delegate('#next-quiz-btn', 'click', function(){
			console.log("Next Clicked");
			var id = parseInt($(this).data('id'));
			$('.quiz-part').empty();
			var loadedhtml = loadQuestion(questions, id+1);
			$('.quiz-part').append(loadedhtml);
			if(id+1 == questions.length){
				$('.cmplt_lesson').removeAttr('disabled');
				$('.cmplt_lesson').removeClass('button--primary--disabled');
				$('.lesson_level_'+lvl).find('.brand-color__text').empty().append('<i class="fa fa-circle content-item"></i>');
				// $('.lesson-timer').hide();
				// $('.rightpannel_footer').hide();
			}
		});

		$('body').delegate('.next-slide-btn', 'click', function(){
			console.log("Next Clicked");
			var id = parseInt($(this).data('id'));
			$('.quiz-part').empty();
			var loadedhtml = loadSlide(slides, id);
			$('.quiz-part').append(loadedhtml[0]);
			$('.rightpannel_footer').empty();
			$('.rightpannel_footer').append(loadedhtml[1]);
		});

		$('body').delegate('.prev-slide-btn', 'click', function(){
			console.log("Prev Clicked");
			var id = parseInt($(this).data('id'));
			$('.quiz-part').empty();
			var loadedhtml = loadSlide(slides, id);
			$('.quiz-part').append(loadedhtml[0]);
			$('.rightpannel_footer').empty();
			$('.rightpannel_footer').append(loadedhtml[1]);
		});

		$('body').delegate('#con-btn', 'click', function(){
			$('.quiz_options').attr("disabled",true);
			console.log($('.quiz_options:checked').val());
			var clr = $('.ans-status').data('color');
			$('.quiz_options:checked').parent().addClass('das-'+clr);
			$('#con-btn').parent().parent().parent().find('.ans-status').show();
			$('#con-btn').hide();
			$('#next-quiz-btn').show();
			if("{{Auth::id()}}" != "{{@$product->professional->id}}"){
				$.ajax({
					url: "{{ route('user.quiz.progress') }}",
					method: 'post',
					dataType: 'json',
					data: quizReqData,
					success: function(response){
						console.log(response);
						console.log(response.allProgress);
						noOfCorrect = response.noOfCorrect;
						allProgress = response.allProgress;
					}
				});
			}
		});

		// $('body').delegate('.cmplt_lesson', 'click', function(){
		// 	console.log("Complete Clicked");
		// 	var levels = JSON.parse("{{ json_encode(@$levels) }}");
		// 	var level = parseInt($('.active_lesson_view').data('level'));
		// 	var next_level = levels[levels.indexOf(level)+1];
		// 	if(level == levels[levels.length -1]) next_level = levels[0];
		// 	var chapter_id = $('#chapter_id').val();

        //     if($('.lesson_level_'+level).find('.brand-color__text').find('i').hasClass('fa-circle-thin')){
        //         $('.lesson_level_'+level).find('.brand-color__text').empty().append('<i class="fa fa-circle content-item"></i>');
        //     }
        //     flag = lowest_level = 0;
        //     $('.lesson_level_'+level).parent().parent().find('.lesson-cont').each(function(i, lsnn){
        //         if(i==0) lowest_level = $(lsnn).data('level');
        //         if($(lsnn).find('.content-item').hasClass('fa-circle-thin')){
        //             flag = 1;
        //         }
        //     });
        //     if(flag == 0){
        //         $('.chapter_level_'+lowest_level).find('.round_sec').removeClass('round_sec').addClass('round_sec_h');
        //     }
        //     $('.lesson_level_'+next_level).trigger( "click" );
        //     if(level == $('#card_chap'+chapter_id).find('.chapter_head').data('level')){
        //         $('.chapter_level_'+next_level).trigger( "click" );
        //     }
		// });

		$('body').delegate('.cmplt_lesson', 'click', function(){
			console.log("Complete Clicked");
			var levels = JSON.parse("{{ json_encode(@$levels) }}");
			var level = parseInt($('.active_lesson_view').data('level'));
			var next_level = levels[levels.indexOf(level)+1];
			if(level == levels[levels.length -1]) next_level = levels[0];
			var chapter_id = $('#chapter_id').val();

			if("{{Auth::id()}}" != "{{@$product->professional->id}}"){
				var reqData = {
					'jsonrpc' : '2.0',
					'_token' : '{{csrf_token()}}',
					'params' : {
						'course_id' : $('#product_id').val(),
						'lesson_id' : $('#lesson_id').val()
					}
				};
				$.ajax({
					url: "{{ route('user.course.progress') }}",
					method: 'post',
					dataType: 'json',
					data: reqData,
					success: function(response){
						console.log(response);
						var next_level = levels[levels.indexOf(response.level)+1];
						if(response.level == levels[levels.length -1]) next_level = levels[0];
						if(response.status == "success"){
							$('.lesson_level_'+response.level).find('.brand-color__text').empty().append('<i class="fa fa-circle content-item"></i>');
							$('.lesson_level_'+next_level).trigger( "click" );
							if(response.chapter_complete == true){
								$('#card_chap'+response.chapter.id).find('.acco-chap').find('.round_sec').removeClass('round_sec').addClass('round_sec_h');
							}
							if(response.level == $('#card_chap'+response.chapter.id).find('.chapter_head').data('level')){
								$('.chapter_level_'+next_level).trigger( "click" );
							}
							if(response.increment == true){
								if(response.chapter.totalLessons != parseInt($('#card_chap'+response.chapter.id).find('.increment').text()) )
								$('#card_chap'+response.chapter.id).find('.increment').text(parseInt($('#card_chap'+response.chapter.id).find('.increment').text()) + 1);
							}
						}
					}
				});
			} else {
				console.log("PROFESSIONAL COMPLETE");
				$('.lesson_level_'+level).find('.brand-color__text').empty().append('<i class="fa fa-circle content-item"></i>');
				flag = lowest_level = 0;
				$('.lesson_level_'+level).parent().parent().find('.lesson-cont').each(function(i, lsnn){
					if(i==0) lowest_level = $(lsnn).data('level');
					if($(lsnn).find('.content-item').hasClass('fa-circle-thin')){
						flag = 1;
					}
				});
				if(flag == 0){
					$('.chapter_level_'+lowest_level).find('.round_sec').removeClass('round_sec').addClass('round_sec_h');
				}
				$('.lesson_level_'+next_level).trigger( "click" );
				if(level == $('#card_chap'+chapter_id).find('.chapter_head').data('level')){
					$('.chapter_level_'+next_level).trigger( "click" );
				}
			}
		});

		$('body').delegate('.view_ans', 'click', function(){
			var elem = $(this);
			var id = $(this).data('id');
			$('#answer_here').hide();
			$('#question_box_'+id).find('.ansqq').slideToggle('slow', function() {
				if ($(this).is(':visible')) {
					$(elem).text("@lang('client_site.hide_answers')");
				} else {
					$(elem).text("@lang('client_site.show_answers')");
				}
			});
		});

		$('body').delegate('.ans_btn', 'click', function(){
			var id = $(this).data('id');
			$('#question_id').val(id);
			$('#answer_here').show();
			$('#answer').val("");
		});

		$('body').delegate('.del_qs', 'click', function(){
			swal({
				title: "@lang('client_site.del_ques')",
				text: "@lang('site.del_question')",
				icon: "warning",
				buttons: true,
				dangerMode: true,
				buttons: ["@lang('client_site.cancel')", "@lang('client_site.delete')"]
			})
			.then((willDelete) => {
				if (willDelete) {
					var id = $(this).data('id');
					$.get( "{{ url('/') }}/delete-lesson-question/" + id, function( data ) {
						console.log('deleted');
						$('#question_box_'+id).fadeOut();
						if($('.question_posted_sec').find('.question_box').length == 1){
							$('.question_posted_sec').append('<p class="text-secondary text-center pt-5">@lang('site.no_questions_posted_yet')</p>');
						}
						setTimeout(() => {
							$('#question_box_'+id).remove();
						}, 1000);
					});
				} else {
					return false;
				}
			});
		});

		$('body').delegate('.del_ans', 'click', function(){
			swal({
				title: "@lang('client_site.del_ans')",
				text: "@lang('site.del_answer')",
				icon: "warning",
				buttons: true,
				dangerMode: true,
				buttons: ["@lang('client_site.cancel')", "@lang('client_site.delete')"]
			})
			.then((willDelete) => {
				if (willDelete) {
					var id = $(this).data('id');
					$.get( "{{ url('/') }}/delete-lesson-answer/" + id, function( data ) {
						console.log('deleted');
						$('#answer_box_'+id).fadeOut();
						if($('#answer_box_'+id).parent().find('.ansqq').length == 1){
							$('#answer_box_'+id).parent().find('h6').fadeOut();
						}
						setTimeout(() => {
							$('#answer_box_'+id).remove();
						}, 1000);
					});
				} else {
					return false;
				}
			});
		});

		$('body').delegate('.retake-btn', 'click', function(){
			retakeQuiz();
		});

		function retakeQuiz(){
			if("{{Auth::id()}}" == "{{@$product->professional->id}}"){
				quizans = [];
				$('.lesson_level_'+lvl).find('.brand-color__text').empty().append('<i class="fa fa-circle-thin content-item"></i>');
				lid = $('#lesson_id').val();
				ltype = $('#lesson_type').val();
				cid = $('#chapter_id').val();
				loadLessons(lid, cid, ltype);
			} else {
				var id = $('#lesson_id').val();
				$.get( "{{ url('/') }}/delete-quiz-progress/" + id, function( data ) {
					console.log(data);
					$('.lesson_level_'+data.courseProgress.level).find('.brand-color__text').empty().append('<i class="fa fa-circle-thin content-item"></i>');
					allProgress = 0;
					console.log(allProgress);
				});
				lid = $('#lesson_id').val();
				ltype = $('#lesson_type').val();
				cid = $('#chapter_id').val();
				loadLessons(lid, cid, ltype);
			}
		}

		function loadLessons(lesson_id,chapter_id,ltype){
			$.ajax({
				url: "{{ url('/') }}" + "/get-lesson-details/" + lesson_id,
				method: 'get',
				dataType: 'json',
				async: false,
				success: function(res){
					console.log(res);

					$('#chapter_id').val(chapter_id);
					$('#lesson_id').val(lesson_id);
					$('#lesson_type').val(ltype);
					$('#course_progress_id').val(res.courseProgress.id);
					$('.rightpannel_middle-info').empty();
					// $('.lesson-timer').show();
					clearInterval(downloadTimer);
					$('.rightpannel_title').find('h2').text(res.lesson.lesson_title);
					secs = parseInt(res.lesson.duration) * 60;
					lsnTimer = `<span id="timer_text">00:00:00</span> <progress value="0" max="${secs}" id="progressBar"></progress>`
					$('.rightpannel_title').find('.lesson-timer').empty().append(lsnTimer);

					lessonTimer(secs);

					$('.course-information_right').removeClass('new_cc_vedo');
					$('.title_right_btns').empty();
					$('.question_posted_sec').empty();
					lvl = res.lesson.level;
					noOfCorrect = res.noOfCorrect;
					allProgress = res.allProgress;
					cmplt_btn = `<button class="button--default--small button--primary_n cmplt_lesson">@lang('client_site.next_lesson') <i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>`;

					var levels = JSON.parse("{{ json_encode(@$levels) }}");
					noOfComplete = parseInt($('.fa-circle').length);
                    console.log(noOfComplete)
					var prcnt = ((noOfComplete/levels.length)*100).toFixed(2);
					if(prcnt % 1 == 0) prcnt = parseInt(prcnt);
					$('.progress-bar').attr('aria-valuenow', prcnt);
					$('.progress-bar').css('width', prcnt+"%");
					$('#prgrs').text(prcnt+"%");

					if(!$('#card_chap'+res.chapter.id).find('.lesson_list').hasClass('show'))
						$('#card_chap'+res.chapter.id).find('.acco-chap').trigger( "click" );

                    if(res.lesson.is_free_preview == 'N'){
                        var html =
                            `<div class="rightpannel_body">
                                <div class="rightpannel_body_apart mt-0">
                                    <div class="locked_lesson_div">
                                        <img src="{{ URL::to('public/frontend/images/locked_lesson.png') }}" alt=""/>
                                        <p>@lang('client_site.lesson_locked')</p>
                                        <a href="javascript:;" id="buyNow" data-product="{{$product->id}}" class="button--default--small button--primary_n">@lang('client_site.purchase_course')</a>
                                    </div>
                                </div>
                            </div>`;
                        $(".rightpannel_middle-info").append(html);
                    } else {
                        if(ltype == 'V'){
                            var html1 = vdo = "";
                            if(res.downloads.length > 0){
                                $(res.downloads).each(function(i, attachment){
                                    var str = attachment.mimetype;
                                    str.indexOf("welcome")
                                    str = str.substring(0,str.indexOf("/")).toUpperCase();
                                    html1 +=
                                    `<div class="attachment">
                                        <div class="attch_names">
                                            <p>${attachment.name}</p>
                                            <span>${str}</span>
                                        </div>
                                        <a href="{{url('/')}}/storage/app/public/lessons/type_video/attachments/${attachment.filename}" class="btn btn-secondary attch_dwnld" download="${attachment.name}">@lang('site.download')</a>
                                    </div>`;
                                });
                            }
                            if(res.main_video.filetype == 'V'){
                                console.log(res.thumbnail);
                                var poster = res.thumbnail == null ? "" : `{{url('/')}}/storage/app/public/lessons/type_video/thumbnails/${res.thumbnail.filename}`;
                                vdo = `<video poster="${poster}" controls loop playsinline autoplay id="vid" controlsList="nodownload"><source src="{{url('/')}}/storage/app/public/lessons/type_video/${res.main_video.filename}" type="video/mp4"></video>`;
                            }
                            else if(res.main_video.filetype == 'VM'){
                                console.log(res.thumbnail);
                                var poster = res.thumbnail == null ? "" : `{{url('/')}}/storage/app/public/lessons/type_video/thumbnails/${res.thumbnail.filename}`;
                                // vdo = `<video poster="${poster}" controls loop playsinline autoplay id="vid"><source src="{{url('/')}}/storage/app/public/lessons/type_video/${res.main_video.filename}" type="video/mp4"></video>`;
                                    vdo = `<iframe src="${res.main_video.filename}" width="100%" height="420" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>`

                            } else {
                                vdo = `<iframe id="youTubeVid" class="youtube_iframe" width="100%" height="520px" src="https://www.youtube-nocookie.com/embed/${res.main_video.filename}?modestbranding=1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
                            }
                            var html =
                            `<div class="rightpannel_body">
                                <div class="rightpannel_body_apart mt-0">
                                    ${vdo}
                                    <div class="video_extras">
                                        <p>${res.lesson.description != null ? res.lesson.description : ''}</p>
                                        ${html1}
                                    </div>
                                </div>
                            </div>
                            <div class="rightpannel_footer">
                                ${cmplt_btn}
                            </div>`;
                            $(".rightpannel_middle-info").append(html);
                            // console.log($('#youTubeVid')[0]);
                            // console.log($('#youTubeVid').contents().find('.ytp-chrome-top-buttons')[0]);
                        }
                        if(ltype == 'T'){
                            var html =
                            `<div class="rightpannel_body">
                                <div class="rightpannel_body_apart mt-0">
                                    <div class="video_extras">
                                        <p>${res.lesson.description}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="rightpannel_footer">
                                ${cmplt_btn}
                            </div>`;
                            $(".rightpannel_middle-info").append(html);
                        }
                        if(ltype == 'Q'){
                            if(quizans.length != res.questions.length) quizans = [];
                            questions = res.questions;
                            if("{{Auth::id()}}" == "{{@$product->professional_id}}") {
                                var loadedhtml = "";
                                if(quizans.length == 0){
                                    loadedhtml = loadQuestion(res.questions, 0);
                                } else {
                                    loadedhtml = loadQuestion(res.questions, res.questions.length);
                                }
                                html =
                                `<div class="rightpannel_body">
                                    <div class="rightpannel_body_apart">
                                        <div class="quiz-part">
                                            ${loadedhtml}
                                        </div>
                                    </div>
                                </div>
                                <div class="rightpannel_footer">
                                    ${cmplt_btn}
                                </div>`;
                                $(".rightpannel_middle-info").append(html);
                                if(quizans.length == 0){
                                    $('.cmplt_lesson').attr('disabled', true);
                                    $('.cmplt_lesson').addClass('button--primary--disabled');
                                } else {
                                    $('.cmplt_lesson').removeAttr('disabled');
                                    $('.cmplt_lesson').removeClass('button--primary--disabled');
                                    // $('.lesson-timer').hide();
                                    // $('.rightpannel_footer').hide();
                                }
                            } else {
                                console.log(allProgress.length);
                                quizvisted.push(res.lesson.id);
                                if(allProgress.length > 0 && allProgress.length != questions.length && $.inArray(res.lesson.id, quizvisted) === -1){
                                    percent = ((parseInt(noOfCorrect)/parseInt(allProgress.length))*100).toFixed(2);
                                    if(percent % 1 == 0) percent = parseInt(percent);
                                    swal({
                                        title: "@lang('site.retake_quiz')?",
                                        text: `@lang('site.question_updated_retake') ${percent}%`,
                                        icon: "warning",
                                        buttons: true,
                                        dangerMode: true,
                                        buttons: ["@lang('site.not_now')", "@lang('site.retake_quiz')"]
                                    })
                                    .then((willRetake) => {
                                        if (willRetake) {
                                            retakeQuiz();
                                            str = $('.lesson_level_'+res.lesson.level).find('.lesso-details').find('span').text().trim();
                                            console.log(str);
                                            ar = str.split(" ");
                                            console.log(ar);
                                            ar[2] = res.questions.length;
                                            $('.lesson_level_'+res.lesson.level).find('.lesso-details').find('span').text(ar.join(" "));
                                            console.log(allProgress.length);
                                            lid = $('#lesson_id').val();
                                            ltype = $('#lesson_type').val();
                                            cid = $('#chapter_id').val();
                                            loadLessons(lid, cid, ltype);
                                        } else {
                                            var levels = JSON.parse("{{ json_encode(@$levels) }}");
                                            var next_level = levels[levels.indexOf(lvl)+1];
                                            console.log(levels);
                                            console.log(lvl);
                                            console.log(next_level);
                                            if(lvl == levels[levels.length -1]) next_level = levels[0];
                                            // flag = lowest_level = 0;
                                            // $('.lesson_level_'+lvl).parent().parent().find('.lesson-cont').each(function(i, lsnn){
                                            // 	if(i==0) lowest_level = $(lsnn).data('level')
                                            // 	if(!$(lsnn).find('.content-item').hasClass('fa-circle-thin')){
                                            // 		$(lsnn).find('.content-item').removeClass('fa-circle').addClass('fa-circle-thin');
                                            // 	}
                                            // });
                                            $('.lesson_level_'+next_level).trigger( "click" );
                                            if(lvl == $('#card_chap'+$('#chapter_id').val()).find('.chapter_head').data('level')){
                                                $('.chapter_level_'+next_level).trigger( "click" );
                                            }
                                        }
                                    });
                                } else {
                                    var loadedhtml = "";
                                    if(res.courseProgress.status == 'C'){
                                        noOfCorrect = res.noOfCorrect;
                                        allProgress = res.allProgress;
                                        percent = ((parseInt(noOfCorrect)/parseInt(questions.length))*100).toFixed(2);
                                        if(percent % 1 == 0) percent = parseInt(percent);
                                        loadedhtml = loadQuestion(questions, questions.length);
                                    } else {
                                        loadedhtml = loadQuestion(res.questions, 0);
                                    }
                                    html =
                                    `<div class="rightpannel_body">
                                        <div class="rightpannel_body_apart">
                                            <div class="quiz-part">
                                                ${loadedhtml}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="rightpannel_footer">
                                        ${cmplt_btn}
                                    </div>`;
                                    $(".rightpannel_middle-info").append(html);
                                }
                                if(res.courseProgress.status == 'C'){
                                    $('.cmplt_lesson').removeAttr('disabled');
                                    $('.cmplt_lesson').removeClass('button--primary--disabled');
                                    // $('.lesson-timer').hide();
                                    // $('.rightpannel_footer').hide();
                                } else {
                                    $('.cmplt_lesson').attr('disabled', true);
                                    $('.cmplt_lesson').addClass('button--primary--disabled');
                                }
                            }
                        }
                        if(ltype == 'M'){
                            // $('.course-information_right').addClass('new_cc_vedo');
                            // var html = `<iframe src="${res.lesson.multimedia_url}" title="${res.lesson.lesson_title}" width="100%" height="100%"></iframe>`;
                            // $(".rightpannel_middle-info").append(html);
                            var html =
                            `<div class="rightpannel_body">
                                <iframe src="${res.lesson.multimedia_url}" title="${res.lesson.lesson_title}" width="100%" height="100%"></iframe>
                            </div>
                            <div class="rightpannel_footer">
                                ${cmplt_btn}
                            </div>`;
                            $(".rightpannel_middle-info").append(html);
                            var html =
                            `<a href="${res.lesson.multimedia_url}" target="_blank" rel="noopener noreferrer" title="Open in new tab">
                                <i class="fa fa-external-link" aria-hidden="true"></i>
                            </a>`;
                            $('.title_right_btns').append(html);
                        }
                        if(ltype == 'D'){
                            var html1 = "";
                            if(res.downloads.length > 0){
                                $(res.downloads).each(function(i, attachment){
                                    var str = attachment.mimetype;
                                    str.indexOf("welcome")
                                    str = str.substring(0,str.indexOf("/")).toUpperCase();
                                    html1 +=
                                    `<div class="attachment">
                                        <div class="attch_names">
                                            <p>${attachment.name}</p>
                                            <span>${str}</span>
                                        </div>
                                        <a href="{{url('/')}}/storage/app/public/lessons/type_video/attachments/${attachment.filename}" class="btn btn-secondary attch_dwnld" download="${attachment.name}">@lang('site.download')</a>
                                    </div>`;
                                });
                            }
                            var html =
                            `<div class="rightpannel_body">
                                <div class="rightpannel_body_apart mt-0">
                                    <div class="video_extras">
                                        ${html1}
                                        <span class="mt-4 w-100">${res.lesson.description != null ? res.lesson.description : ''}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="rightpannel_footer">
                                ${cmplt_btn}
                            </div>`;
                            $(".rightpannel_middle-info").append(html);
                        }
                        if(ltype == 'A'){
                            var html =
                            `<div class="rightpannel_body">
                                <div class="rightpannel_body_apart mt-0">
                                    <div class="video_extras">
                                        <audio id="aud" src="{{URL::to('storage/app/public/lessons/type_audio/')}}/${res.main_audio.filename}" controls loop playsinline autoplay ></audio>
                                        <p class="mt-2"></p>
                                        ${res.lesson.description}
                                    </div>
                                </div>
                            </div>
                            <div class="rightpannel_footer">
                                ${cmplt_btn}
                            </div>`;
                            $(".rightpannel_middle-info").append(html);
                        }
                        if(ltype == 'P'){
                            console.log(res);
                            slides = res.slides;
                            loadSlide(slides, 0);
                            var loadedhtml = loadSlide(res.slides, 0);
                            html =
                            `<div class="rightpannel_body">
                                <div class="rightpannel_body_apart">
                                    <div class="quiz-part">
                                        ${loadedhtml[0]}
                                    </div>
                                </div>
                            </div>
                            <div class="rightpannel_footer righ_footer_odio">
                            </div>`;
                            $(".rightpannel_middle-info").append(html);
                            $('.rightpannel_footer').append(loadedhtml[1]);
                        }

                        /*if(res.lesson.is_allow_discussion == 'Y'){
                            $('.title_right_btns').append(`<a href="javascript:;" class="text-secondary" id="view_questions"><img src="{{ URL::to('public/frontend/images/qa.png') }}" height="30px" title="@lang('site.view_question_answers')"/><span style="color: #007bff; font-size: 20px;"> Q/A</span></a>`);
                            var html = answers = del_btn = "";
                            if(res.discussion.length > 0){
                                $(res.discussion).each(function(i, question){
                                    console.log(question);
                                    answers = "";
                                    if(question.get_answers.length > 0){
                                        answers = `<h6><a href="javascript:;" class="view_ans" data-id="${question.id}">Show Answers</a></h6>`;
                                        $(question.get_answers).each(function(i, answer){
                                            img = answer.get_answerer.profile_pic != null ? "{{url('/')}}/storage/app/public/uploads/profile_pic/"+answer.get_answerer.profile_pic : "{{url('/')}}/public/frontend/images/no_img.png";
                                            name = answer.answered_by.nick_name != null ? answer.answered_by.nick_name : answer.answered_by.name;
                                            del_btn = "{{Auth::id()}}" == "{{@$product->professional_id}}" ? `<a href="javascript:;" class="btn btn-danger pull-right del_ans" data-id="${answer.id}"><i class="fa fa-trash" aria-hidden="true"></i></a>` : "";
                                            answers +=
                                            `<div class="question_box ansqq" id="answer_box_${answer.id}" style="display:none;">
                                                <span class="question_user"><img src="${img}"></span>
                                                <div class="question_details">
                                                    ${del_btn}
                                                    <h5>${name} <strong>${answer.answered_on}</strong></h5>
                                                    <p>${answer.answers}</p>
                                                </div>
                                            </div>`;
                                        });
                                    }
                                    ans_btn = "{{Auth::id()}}" == "{{@$product->professional_id}}" ? `<small><a href="javascript:;" class="ans_btn" data-id="${question.id}">Answer</a></small>` : "";
                                    img = question.get_questioner.profile_pic != null ? "{{url('/')}}/storage/app/public/uploads/profile_pic/"+question.get_questioner.profile_pic : "{{url('/')}}/public/frontend/images/no_img.png";
                                    del_btn = (question.get_questioner.id == "{{Auth::id()}}" && question.get_answers.length == 0) ? `<a href="javascript:;" class="btn btn-danger pull-right del_qs" data-id="${question.id}"><i class="fa fa-trash" aria-hidden="true"></i></a>` : "";
                                    html +=
                                    `<div class="question_box quesqq" id="question_box_${question.id}">
                                        <span class="question_user"><img src="${img}"></span>
                                        <div class="question_details">
                                            ${del_btn}
                                            <h4>${question.question} ${ans_btn}</h4>
                                            <h5>${question.get_questioner.nick_name ? question.get_questioner.nick_name : question.get_questioner.name}<strong>${question.asked_on}</strong></h5>
                                            ${answers}
                                        </div>
                                    </div>`;
                                });
                            } else {
                                html = '<p class="text-secondary text-center pt-5">@lang('site.no_questions_posted_yet')</p>';
                            }
                            $('.question_posted_sec').append(html);
                            if("{{Auth::id()}}" != "{{@$product->professional_id}}"){
                                $('#ask_here').show();
                            }
                        }*/
                    }
				},
				error: function(err){
					console.log(err);
				},
			});
		}

		function loadQuestion(questions, val){
			var options = ["A", "B", "C", "D", "E", "F"];
			var question = "";
			var c = 0;
			var html = qhtml = next = "";
			next = `<button type="button" class="m-0 con-btn button--primary" id="next-quiz-btn" style="display:none;" data-id="${val}">@lang('client_site.next')</button>`;

			if(val != questions.length){
				for(i=1; i<=6; i++){
					if(questions[val]['answer_'+i] != null){
						question +=
						`<div class="das-radio dash-d1" data-correct="${questions[val]['correct_answer']}">
							<input type="radio" id="radio${i}" name="radios" class="quiz_options" value="${i}" data-count="${options[c]}" data-value="${val}">
							<label for="radio${i}" class="p-0 mb-2"><span class="color-span">${options[c]}</span> <span class="label-cont">${questions[val]['answer_'+i]}</span> </label>
						</div>`;
						c+=1;
					}
				}
				html =
				`<div class="quiz-qus">
					<h5>Questão <span class="question_count">${val+1}</span> de ${questions.length}</h5>
					<input type="text" value="${questions[val]['id']}" id="quiz_id"hidden>
					<h2 class="question">${questions[val]['question']}</h2>
					<p>@lang('client_site.choose_only_one_best_answer')</p>
					<div class="quiz-ans">
						${question}
					</div>
					<div>
						<button type="button" class="m-0 con-btn button--primary button--primary--disabled" id="con-btn" disabled>Confirme</button>
						${next}
					</div>
				</div>
				<div class="ans-status" style="display:none;">
					<div><b>@lang('client_site.explanation'): </b>${questions[val]['explanation']}</div>
				</div><br><br>`;
			} else {
				if("{{Auth::id()}}" == "{{@$product->professional->id}}"){
					noOfCorrect = countOccurrences(quizans, 'Y');
					console.log("correct : "+ noOfCorrect);
				}
				percent = ((parseInt(noOfCorrect)/parseInt(questions.length))*100).toFixed(2);
				if(percent % 1 == 0) percent = parseInt(percent);
				$(questions).each(function(i, question){
					prog = `<span class="quiz-an-red"><i class="fa fa-times-circle" ></i></span>`;
					if("{{Auth::id()}}" == "{{@$product->professional->id}}"){
						if(quizans[i] == 'Y') prog = `<span class="quiz-an"><i class="fa fa-check-circle" ></i></span>`;
					} else {
						if(allProgress[i]['is_correct'] == 'Y') prog = `<span class="quiz-an"><i class="fa fa-check-circle" ></i></span>`;
					}
					qhtml +=
					`<li class="quiz-layer">
						<div class="qz_qstn">
							<span> ${i+1}. </span> ${question['question']} ${prog}
						</div>
						<hr class="quiz-hr">
						${question['explanation']}
					</li>`;
				});
				l_title = $('.rightpannel_title > h2').text();
				html =
				`<div class="quiz-result text-center">
					<h5> @lang('client_site.you_completed') ${l_title}
					<br>
					@lang('client_site.your_score')</h5>
					<h1>${percent}%</h1>
					<div>
						<button class="button--default--small button--primary_n uppercase cmplt_lesson">Continue <i class="fa fa-long-arrow-right" ></i></button>
					</div>
					<div class="mt-2">
						<button class="button--default--small uppercase retake-btn">@lang('site.retake_quiz')</button>
					</div>
				</div>
				<div class="quiz-player">
					<p>Você respondeu ${noOfCorrect} de ${questions.length} perguntas corretamente</p>
					<ul class="quiz-player-ul">
						${qhtml}
					</ul>
				</div>`;
				// $('.lesson-timer').hide();
				if("{{Auth::id()}}" != "{{@$product->professional->id}}"){
					var reqData = {
						'jsonrpc' : '2.0',
						'_token' : '{{csrf_token()}}',
						'params' : {
							'course_id' : $('#product_id').val(),
							'lesson_id' : $('#lesson_id').val()
						}
					};
					$.ajax({
						url: "{{ route('user.course.progress') }}",
						method: 'post',
						dataType: 'json',
						data: reqData,
						success: function(response){
							console.log(response);
							var levels = JSON.parse("{{ json_encode(@$levels) }}");
							console.log(levels);
							var next_level = levels[levels.indexOf(response.level)+1];
							if(response.level == levels[levels.length -1]) next_level = levels[0];
							console.log(next_level);
							if(response.status == "success"){
								$('.lesson_level_'+response.level).find('.brand-color__text').empty().append('<i class="fa fa-circle content-item"></i>');
								if(response.chapter_complete == true){
									$('#card_chap'+response.chapter.id).find('.acco-chap').find('.round_sec').removeClass('round_sec').addClass('round_sec_h');
								}
								if(response.increment == true){
									if(response.chapter.totalLessons != parseInt($('#card_chap'+response.chapter.id).find('.increment').text()) )
									$('#card_chap'+response.chapter.id).find('.increment').text(parseInt($('#card_chap'+response.chapter.id).find('.increment').text()) + 1);
								}
							}
						}
					});
				}
				clearInterval(downloadTimer);
			}
			return html;
		}

		function loadSlide(slides, val){
			var next = prev = audio = "";
			flag = 1;
			cmplt_btn_sld = `<button class="button--default--small button--primary_n button--primary--disabled cmplt_lesson" disabled="true">@lang('client_site.next_lesson') <i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>`;
			prev = `<button class="btn btn-success prv_bt prev-slide-btn" data-id="${val-1}">@lang('client_site.previous')</button>`;
			next = `<button class="btn btn-success prv_bt next-slide-btn" data-id="${val+1}">@lang('client_site.next')</button>`;
			if(val+1 == slides.length){
				// prev = `<button class="btn btn-success prv_bt disable_btns prev-slide-btn" data-id="${val-1}">@lang('client_site.previous')</button>`;
				next = `<button class="btn btn-success prv_bt disable_btns next-slide-btn" data-id="${val+1}" disabled>@lang('client_site.next')</button>`;
				cmplt_btn_sld = `<button class="button--default--small button--primary_n cmplt_lesson">@lang('client_site.next_lesson') <i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>`;
			}
			if(val == 0){
				prev = `<button class="btn btn-success prv_bt disable_btns prev-slide-btn" data-id="${val-1}" disabled>@lang('client_site.previous')</button>`;
				// next = `<button class="btn btn-success prv_bt next-slide-btn" data-id="${val+1}">@lang('client_site.next')</button>`;
			}
			console.log(slides[val].audio);
			if(slides[val].audio != null)
				audio = `<audio id="presaudio" controls loop autoplay src="{{url('/')}}/storage/app/public/lessons/type_presentation/audio/${slides[val].audio}"></audio>`;
			else
				audio = "";
			var html =
			`<div class="slide">
				<img src="{{url('/')}}/storage/app/public/lessons/type_presentation/${slides[val]['filename']}" width="100%">
				<div class="description mt-3">
					${slides[val].description == null ? "" : slides[val].description}
				</div>
			</div>`;
			var shtml =
			`<div class="pag_next">
				<ul class="dex_pagi">
					<li>${prev}</li>
					<li>${next}</li>
					<li><span>${val+1}/${slides.length}</span></li>
				</ul>
				<ul class="dex_pagiph_pagi">
					<li>${prev}</li>
					<li><span>${val+1}/${slides.length}</span></li>
					<li>${next}</li>
				</ul>
			</div>
			${cmplt_btn_sld}
			<div class="pres_audios">
				${audio}
			</div>`;
			return [ html, shtml ];
		}

		function continueWithoutComplete(qstn){
			if("{{Auth::id()}}" != "{{@$product->professional->id}}"){
				console.log(qstn);
				quizReqData = {
					'jsonrpc' : '2.0',
					'_token' : '{{csrf_token()}}',
					'params' : {
						'course_id' : $('#product_id').val(),
						'lesson_id' : $('#lesson_id').val(),
						'course_progress_id' : $('#course_progress_id').val(),
						'question_id' : qstn.id,
						'user_answer' : 0,
						'correct_answer' : qstn.correct_answer,
						'is_correct' : 'N'
					}
				};
				$.ajax({
					url: "{{ route('user.quiz.progress') }}",
					method: 'post',
					dataType: 'json',
					data: quizReqData,
					async: false,
					success: function(response){
						console.log(response);
						console.log(response.allProgress);
						noOfCorrect = response.noOfCorrect;
						allProgress = response.allProgress;
					}
				});
			}
		}

		function lessonTimer(time){
			time = parseInt(time);
			var timetotal = time;

			if(localStorage.getItem('pause_array')){
				pauseArray = JSON.parse(localStorage.getItem('pause_array'));
				// pauseArray = localStorage.getItem('pause_array').split(",");
				if(pauseArray["{{@$product->id}}"] == "" || pauseArray["{{@$product->id}}"] == undefined || pauseArray["{{@$product->id}}"] == null || pauseArray["{{@$product->id}}"] <= 0){
					timeleft = time;
				} else {
					timeleft = pauseArray["{{@$product->id}}"] >= 0 ? pauseArray["{{@$product->id}}"] : 0;
					console.log(pauseArray["{{@$product->id}}"]);
				}
			} else timeleft = time;

			downloadTimer = setInterval(function(){
			if(timeleft <= 0){
				clearInterval(downloadTimer);
				ltype = $('#lesson_type').val();
				if(ltype == 'V'){
					$('#vid').get(0).pause();
				}
				if(ltype == 'A'){
					$('#aud').get(0).pause();
				}
				if(ltype == 'P'){
					$('#presaudio').get(0).pause();
				}
				swal({
					title: "@lang('site.lesson_complete')",
					text: "@lang('site.continue_or_redo')",
					icon: "success",
					buttons: true,
					dangerMode: true,
					buttons: ["@lang('site.redo_lesson')", "@lang('site.continue')"],
					allowOutsideClick: false,
					closeOnClickOutside: false,
					closeOnEsc: false
				})
				.then((willContinue) => {
					if (willContinue) {
						if(ltype == 'Q'){
							if("{{Auth::id()}}" == "{{@$product->professional->id}}") {
								// $('.cmplt_lesson').trigger( "click" );
								for(i=quizans.length; i<questions.length; i++){
									quizans[i] = 'N';
									console.log("I : "+i);
									console.log(quizans);
								}
								console.log(quizans.length);
								$('.cmplt_lesson').removeAttr('disabled');
								$('.cmplt_lesson').removeClass('button--primary--disabled');
								$('.lesson_level_'+lvl).find('.brand-color__text').empty().append('<i class="fa fa-circle content-item"></i>');

								$('.quiz-part').empty();
								var loadedhtml = loadQuestion(questions, questions.length);
								$('.quiz-part').append(loadedhtml);
							} else {
								console.log(allProgress);
								for(i=allProgress.length; i<questions.length; i++){
									console.log("I : "+i);
									continueWithoutComplete(questions[i]);
									console.log(allProgress);
								}
								$('.cmplt_lesson').removeAttr('disabled');
								$('.cmplt_lesson').removeClass('button--primary--disabled');
								// $('.cmplt_lesson').trigger( "click" );
								$('.quiz-part').empty();
								var loadedhtml = loadQuestion(questions, questions.length);
								$('.quiz-part').append(loadedhtml);
							}
						} else {
							// var levels = JSON.parse("{{ json_encode(@$levels) }}");
							// var next_level = levels[levels.indexOf(lvl)+1];
							// if(lvl == levels[levels.length -1]) next_level = levels[0];
							// $('.lesson_level_'+next_level).trigger( "click" );
							$('.cmplt_lesson').removeAttr('disabled');
							$('.cmplt_lesson').removeClass('button--primary--disabled');
							$('.cmplt_lesson').trigger( "click" );
						}
					} else {
						lessonTimer(time);
						ltype = $('#lesson_type').val();
						if(ltype == 'Q'){
							retakeQuiz();
						} else {
							lid = $('#lesson_id').val();
							ltype = $('#lesson_type').val();
							cid = $('#chapter_id').val();
							loadLessons(lid, cid, ltype);
						}
					}
				});
			}
			document.getElementById("progressBar").value = timetotal - timeleft;
			$('#timer_text').html(convertHMS(timeleft));
			// console.log(convertHMS(timeleft));
			timeleft -= 1;
			}, 1000);
		}

		function convertHMS(value) {
			const sec = parseInt(value, 10); // convert value to number if it's string
			let hours   = Math.floor(sec / 3600); // get hours
			let minutes = Math.floor((sec - (hours * 3600)) / 60); // get minutes
			let seconds = sec - (hours * 3600) - (minutes * 60); //  get seconds
			// add 0 if value < 10; Example: 2 => 02
			if (hours   < 10) {hours   = "0"+hours}
			if (minutes < 10) {minutes = "0"+minutes}
			if (seconds < 10) {seconds = "0"+seconds}
			// if(hours == "00")
			// return minutes+':'+seconds;
			// else
			return hours+':'+minutes+':'+seconds;
		}

		const countOccurrences = (arr, val) => arr.reduce((a, v) => (v === val ? a + 1 : a), 0);

    });
</script>
@if(Auth::id() != @$product->professional->id)
<script>
	window.addEventListener("beforeunload", function (e) {
		var confirmationMessage = "@lang('site.continue_with_course_later')";
		var pauseArray = localStorage.getItem('pause_array') ? JSON.parse(localStorage.getItem('pause_array')) : [];
		// if("{{@$product->id}}" in pauseArray)
		pauseArray["{{@$product->id}}"] = timeleft;

		console.log(pauseArray);
		pauseArray = JSON.stringify(pauseArray);
		localStorage.setItem('pause_array', pauseArray);
		(e || window.event).returnValue = confirmationMessage; //Gecko + IE
		return confirmationMessage;                            //Webkit, Safari, Chrome
	});
</script>
@endif
<script>
	function findChapters(){
		var key = $('#find_chapters').val();
		if(key == ''){
			$('.card').show();
		} else {
			var reqData = {
            'jsonrpc' : '2.0',
            '_token' : '{{csrf_token()}}',
            'params' : {
                  'key' :  key,
                  'product_id' : $('#product_id').val()
               }
            };
            $.ajax({
				url: "{{ route('find.chapters') }}",
				method: 'post',
				dataType: 'json',
				data: reqData,
				success: function(response){
					if(response.status == 1){
						$('.card').hide();
						$(response.chapters).each(function(i, chapter){
						$('#card_chap'+chapter.id).show();
						});
					}
					console.log(response.chapters);
				}
			});
		}
	}

</script>

@endsection
