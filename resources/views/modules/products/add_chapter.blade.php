@extends('layouts.app')
@section('title')
    @lang('site.welcome_to_dashboard')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
<link href="{{ URL::to('public/frontend/css/course_style.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ URL::to('public/frontend/css/course_responsive.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ URL::to('public/frontend/css/course_bootstrap.min') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::to('public/frontend/icofont/icofont.min.css') }}" rel="stylesheet" type="text/css" />
<style>
.error{
	color:red;
}
.success{
	color:forestgreen;
}
</style>
@endsection
@section('content')
<section class="wrapper2">
    @section('header')
    @include('includes.professional_header')
    @endsection
		<!-- section-header.// -->
		<!-- ========================= SECTION CONTENT ========================= -->
		<nav class="navbar navbar-expand-lg navbar-dark sticky-top">
			<div class="w-100">
				<!-- <div class="top-bar">
					<div class="top-bar-left-container">
						<a href="#url" class="top-bar-button"> <i class="icofont-close-line"></i> </a>
						<div class="d-none d-md-flex align-items-center">
							<h4 class="ml-4">Courses</h4> </div>
					</div>
					<div class="top-bar-center-container"> <a href="#url" class="user_llk">Your First Course <i class="icofont-caret-down"></i></a>
						<div class="show01 search-category" style="display: none;">
							<div class="select2-search">
								<input type="text" name=""> </div>
							<ul class="select2-results">
								<li class="select2-no-results">No matches found</li>
							</ul>
						</div>
					</div>
					<div class="top-bar-right-container"> <a href="#url" class="topbar-right-btn">
						build landing page <i class="icofont-long-arrow-right"></i>
					</a> </div>
				</div> -->
				<div class="navbar-menu">
					<div class="container-fluid">
						<div class="all-menu-sec">
							<!---<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav"> <span class="navbar-toggler-icon"></span> </button> --->
							<div class="" id="main_nav">
								<ul class="navbar-nav">
									<li class="nav-item active"> <a class="nav-link" href="#">   Curriculum </a> </li>
									<li class="nav-item"><a class="nav-link" href="#">                 Settings </a></li>
								</ul>
							</div>
							<div class="navbar__actions">
								<button class="preview-btn"> <i class="icofont-eye-alt"></i> Preview </button>
							</div>
						</div>
						<!-- navbar-collapse.// -->
					</div>
				</div>
				<!-- container //  -->
			</div>
		</nav>
		<div class="page_builder">
			<div class="course_view">
                <div class="list-view-sessoin">
					<div>
						<div id="main">
						@if(count(@$chapters)>0)
							@foreach($chapters as $ck=>$chapter)
								<div class="accordion_1" id="faq">
									<div class="card">
										<div class="card-header chapter_header" id="faqhead{{$ck}}" data-id="{{$chapter->id}}">
											<a href="{{route('add.chapter', @$product->id, @$chapter->id)}}" class="btn btn-header-link acco-chap" data-toggle="collapse" data-target="#faq{{$ck}}" aria-expanded="true" aria-controls="faq{{$ck}}"><img src="{{ URL::to('public/frontend/images/cha.png') }}">
												<p>{{$chapter->chapter_title}}</p>
											</a>
										</div>
										<div id="faq{{$ck}}" class="collapse show" aria-labelledby="faqhead{{$ck}}" data-parent="#faq">
											<div class="card-body">
												<div class="session-content">
                                                        <ul class="session-box">
                                                            <li>
                                                            @php
                                                                $lessons = \App\Models\Lesson::where('chapter_id', $chapter->id)->get();
                                                            @endphp
                                                            @if(count(@$lessons)>0)
														        @foreach($lessons as $lesson)
																	<div class="secsions">
																		<div class="session_info">
																			<div class="content-handle"><img src="{{ URL::to('public/frontend/images/cha.png') }}"></div>
																			<div class="lesson-se"> <span>New lesson</span>
																				<div class="tool-tip d-flex flex-row align-items-center"> <img src="{{ URL::to('public/frontend/images/video.png') }}"> <span class="ml-2 badge badge--warning">Draft</span> </div>
																			</div>
																		</div>
																	</div>
                                                                @endforeach
													        @endif
                                                            <div class="session-btn">
                                                                <button class="button add-lesson "><i class="icofont-plus"></i> Add lesson</button>
                                                                <button class="button button--secondary"> Copy lesson from</button>
                                                            </div>
                                                            <div class="add-lesson-info" style="display: none;">
                                                                <span class="types__item" data-ltype="V">
                                                                    <a href="#url" class="types__link">
                                                                        <img src="{{ URL::to('public/frontend/images/video.png') }}">
                                                                        <p>Video</p>
                                                                    </a>
                                                                </span>
                                                                <span class="types__item" data-ltype="Q">
                                                                    <a href="#url" class="types__link">
                                                                        <img src="{{ URL::to('public/frontend/images/quiz.png') }}">
                                                                        <p>Quiz</p>
                                                                    </a>
                                                                </span>
                                                                <span class="types__item" data-ltype="M">
                                                                    <a href="#url" class="types__link">
                                                                        <img src="{{ URL::to('public/frontend/images/multimedia.png') }}">
                                                                        <p>Multimedia</p>
                                                                    </a>
                                                                </span>
                                                                <span class="types__item" data-ltype="T">
                                                                    <a href="#url" class="types__link">
                                                                        <img src="{{ URL::to('public/frontend/images/text.png') }}">
                                                                        <p>Text</p>
                                                                    </a>
                                                                </span>
                                                                <!-- <span class="types__item" data-ltype="P">
                                                                    <a href="#url" class="types__link">
                                                                        <img src="{{ URL::to('public/frontend/images/pd.png') }}">
                                                                        <p>Pdf</p>
                                                                    </a>
                                                                </span> -->
                                                                <span class="types__item" data-ltype="A">
                                                                    <a href="#url" class="types__link">
                                                                        <img src="{{ URL::to('public/frontend/images/audio.png') }}">
                                                                        <p>Audio</p>
                                                                    </a>
                                                                </span>
                                                                <span class="types__item" data-ltype="D">
                                                                    <a href="#url" class="types__link">
                                                                        <img src="{{ URL::to('public/frontend/images/Download.png') }}">
                                                                        <p>download</p>
                                                                    </a>
                                                                </span>
                                                                <span class="types__item" data-ltype="P">
                                                                    <a href="#url" class="types__link">
                                                                        <img src="{{ URL::to('public/frontend/images/presentaion.png') }}">
                                                                        <p>Presentation</p>
                                                                    </a>
                                                                </span>
                                                            </div>
                                                        </li>
                                                
												</div>
											</div>
										</div>
									</div>
								</div>
							@endforeach
						@else
						@endif
						</div>
						<div class="tree-tip">
							<div class="tip__title"> <img src="{{ URL::to('public/frontend/images/tip.png') }}"> Pro Tip </div>
							<p class="tip__description">You can customize the course completion experience with a certificate or a custom completion page!</p> <a href="#url">Course completion settings</a> </div>
					</div>
					<div></div>
				</div>
				<div class="builder__footer">
					<div class="tree_action">
						<div class="builder-add-chapter">
							<a href="{{route('add.chapter',@$product->id)}}" class="button button--primary w-100"> Add Chapter </a>
						</div>
					</div>
				</div>
			</div>
			<div class="builder_section">
				<div class="wrap wrap-edit">
					<div class="ember-view">
						<div class="admin-build-section_new px-md-2">
                            <form action="{{route('store.chapter', @$product->id)}}" id="save_chapter">
                                <input type="text" name="ID" value="{{@$chapter->id}}" hidden>
                                <div class="builder-bar mb-3 mb-lg-4">
                                    <div class="clearfix mobile--show">
                                        <div class="pull-right action-buttons d-inline-flex align-items-center">
                                            
                                            <button type="button" class="button button--secondary content-action-bar__discard-btn" data-ember-action="1560"> Discard changes </button>
                                            <button type="submit" class="button button--primary" data-ember-action="1561"> Save </button>
                                        </div>
                                    </div>
                                    <div class="mobile-hide ">
                                        <div class="pull-right action-buttons d-inline-flex align-items-center">
                                            
                                            <button class="button button--secondary content-action-bar__discard-btn" data-ember-action="1560"> Discard changes </button>
                                            <button class="button button--primary" data-ember-action="1561"> Save </button>
                                        </div>
                                    </div>
                                    <div class="builder_header-container">
                                        <div class="builder__header mt-3 mt-lg-0"> New Chapter : @if(@$chapter) {{$chapter->chapter_title}} @else {{'Untitled Chapter'}} @endif</div>
                                    </div>
                                </div>
                                <div class="content__sections">
                                    <div class="course-content-grow">
                                        <div class="form-row">
                                            <div class="form-group col-sm-12 ">
                                                <label class="label">Chapter title</label>
                                                <input class="ember-view form-control" maxlength="255" size="50" type="text" name="chapter_title" placeholder="My Chapter" value="{{@$chapter ? $chapter->chapter_title : 'Untitled Chapter' }}">
                                                @if(@$chapter)
                                                    <div class="custom-control custom-checkbox p-0 mt-1">
                                                        <input type="checkbox" id="chk9" name="radios" class="radio-box" value="all">
                                                        <label for="chk9">Set new lessons to draft by default</label>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>								
                                        
                                    </div>
                                    <div class="tree-tip mt-5">
                                    <div class="tip__title"> <img src="{{ URL::to('public/frontend/images/tip.png') }}"> Pro Tip </div>
                                    <p class="tip__description">Chapter will remain in draft if there are no lessons or all are set to draft. </div>
                                    
                                </div>
                            </form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
@section('footer')
{{-- @include('includes.footer') --}}
<script>
    $('save_chapter').validate({
        rules: {
            chapter_title:{required:true}
        }
    });
    var video_rules = {
		lesson_title_V: {required:true},
		lesson_main_video: {required:true}
	};
	var video_messege = {
		lesson_title_V:"Please give your lesson a title",
		lesson_main_video:"Please enter a video file"
	};
	var video_err = function(error, element) {
		if (element.attr("name") == "lesson_main_video") {
			$(".video_msg").append(error);
			console.log(error);
		}
		else {
			error.insertAfter(element);
		}
	};

	var text_rules = {
		lesson_title_T: {required:true},
		description: {required:true}
	};
	var text_messege = {
		lesson_title_T:"Please give your lesson a title",
		description:"Please enter the text content",
	};
	var text_err = function(error, element) {
		if (element.attr("name") == "description") {
			$("#err_description").append(error);
			console.log(error);
		}
		else {
			error.insertAfter(element);
		}
	};







	// var editor = new FroalaEditor('.froala-editor',{
	// 	imageUploadParam: 'image_param',
	// 	imageUploadURL: '/upload_image',
	// 	imageUploadParams: {id: 'my_editor'},
	// 	imageUploadMethod: 'POST',
	// 	imageMaxSize: 5 * 1024 * 1024,
	// 	imageAllowedTypes: ['jpeg', 'jpg', 'png'],
	// })

	// $(function() {
	// 	$('textarea.froala-editor').froalaEditor()
	// });
    $(".add-lesson").click(function() {
		$(".add-lesson-info").slideToggle();
	});
	$(document).on('dragover dragleave drop', '.dropbx', function(event) {
		event.preventDefault();
		event.stopPropagation();
	});
	$('.dropbx').on('dragover dragenter', function(e){
		e.preventDefault();
		$(this).css("border-style", "solid");
	});
	$('.dropbx').bind('dragleave drop', function(e){
		e.preventDefault();
		$(this).css("border-style", "dashed");

		const validImageTypes = ['video/avi', 'video/flv', 'video/mp4', 'video/mpeg', 'video/ogv', 'video/webm'];
		if(e.originalEvent.dataTransfer.files.length){
			var mimetype = e.originalEvent.dataTransfer.files[0]['type'];
			var size = e.originalEvent.dataTransfer.files[0]['size'];
			var name = e.originalEvent.dataTransfer.files[0]['name'];
			if (!validImageTypes.includes(mimetype)) {
				$('.video_msg').html(`<p class="text-center error m-0">Please give a valid video file.</p>`);
			} else {
				$('#file')[0].files = e.originalEvent.dataTransfer.files;
				readURL($('#file')[0]);
				$('.video_msg').html(`<p class="text-center success m-0"><i class="fa fa-video-camera mr-1" aria-hidden="true"></i>${name}</p>`);
			}
		}
	});
	$('.chapter_header').click(function(){
		$('#chapter_id').val($(this).data(id))
	});
	$('.types__item').click(function(){
		$('.admin-build-section_new').hide();
		$('.admin-build-section').show();
		var ltype = $(this).data('ltype');
		$('#lesson_type').val(ltype);
		$('.lesson_section').each(function(i, lesson_section){
			$(lesson_section).hide();
		});
		console.log($('#for_'+ltype)[0]);
		$('#for_'+ltype).show();
	});

	tinyMCE.init({
        mode : "specific_textareas",
        editor_selector : "wysiwyg",
        height: '320px',
        plugins: [
            'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
            'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
            'save table contextmenu directionality emoticons template paste textcolor'
        ],
        relative_urls : false,
        remove_script_host : false,
        convert_urls : true,
        toolbar: 'insertfile undo redo | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor emoticons',
        images_upload_url: '{{ URL::to('storage/app/public/lessons/textarea_uploads/') }}',
        images_upload_handler: function(blobInfo, success, failure) {
            var formD = new FormData();
            formD.append('file', blobInfo.blob(), blobInfo.filename());
            formD.append( "_token", '{{csrf_token()}}');
            $.ajax({
                url: '{{route("chapter.text.img.upload")}}',
                data: formD,
                type: 'POST',
                contentType: false,
                cache: false,
                processData:false,
                dataType: 'JSON',
                success: function(jsn) {
                    console.log(jsn.status);
                    if(jsn.status == 'ERROR') {
                        failure(jsn.error);
                    } else if(jsn.status == 'SUCCESS') {
                        success(jsn.location);
                    }
                },
                error: function(err){
                    console.log(err);
                }
            });
        }, 
    });



	$('#save_lesson_btn').click(function(){
		if($('#lesson_type').val() == 'V'){
			var settings = $("#save_lesson_form").validate().settings;
			// Modify validation settings
			$.extend(settings, {
				rules: video_rules,
				messages: video_messege,
				errorPlacement: video_err,
				// debug:true
			});
		}
		if($('#lesson_type').val() == 'T'){
			console.log("Entering T");
			var settings = $("#save_lesson_form").validate().settings;
			// Modify validation settings
			$('#description').val(tinyMCE.get('description').getContent());
			console.log($('#description').val());
			console.log(tinyMCE.get('description').getContent());
			$.extend(settings, {
				ignore: [],
				rules: text_rules,
				messages: text_messege,
				errorPlacement: text_err,
				// debug:true
			});
		}
		// force a test of the form
		if($("#save_lesson_form").valid()){
			$("#save_lesson_form").submit();
		} else {
			var validator = $("#save_lesson_form").validate();
			console.log(validator.errorList);
		}
	});
	function readURL(input) {
		if (input.files && input.files[0]) {
		var reader = new FileReader();
		
		reader.onload = function(e) {
			$('.uploaded_ppc').show();
			var f = $("#file")[0];
			var size = input.files[0].size;
			console.log(size);
			// if(size <=5000){
			// 	$('#blah').attr("width", "5%");
			// } else if(size >=5000 && size <= 50000){
			// 	$('#blah').attr("width", "20%");
			// } else if(size >=50000 && size <= 100000){
			// 	$('#blah').attr("width", "50%");
			// } else if(size >= 100000 && size <= 500000){
			// 	$('#blah').attr("width", "70%");
			// } else if(size > 500000){
			// 	$('#blah').attr("width", "90%");
			// }
			// $('#blah').attr('src', e.target.result);
		}
		
		reader.readAsDataURL(input.files[0]);
		}
	}
</script>

@endsection
