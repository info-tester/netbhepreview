@extends('layouts.app')
@section('title', 'Rate & Reveiw Product')
@section('style')
@include('includes.style')


      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Rate & Reveiw</title>
      <!--style-->
      <link href="css/style.css" type="text/css" rel="stylesheet" media="all" />
      <link href="css/responsive.css" type="text/css" rel="stylesheet" media="all" />
      <!--bootstrape-->
      <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet" media="all" />
      <!--font-awesome-->
      <link href="css/font-awesome.min.css" type="text/css" rel="stylesheet" media="all" />
      <!--fonts-->
      <!--<link href="https://fonts.googleapis.com/css?family=Didact+Gothic" rel="stylesheet"> -->
      <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,900" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900&display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900&display=swap" rel="stylesheet">
      <!-- Owl Stylesheets -->
      <link rel="stylesheet" href="css/owl.carousel.min.css">
      <link rel="stylesheet" href="css/owl.theme.default.min.css">
      <!--price range (Calender/date picker)-->
      <link href="css/themes_smoothness_jquery-ui.css" rel="stylesheet" type="text/css">

@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')
<div class="">

   <section class="product_details">
      <div class="top_sec">
         <div class="dark-area">
         </div>
         <div class="container">
            <div class="row m-0">
               <div class="zin">
                  <div class="product_right_panel sticky_right foright_secc">

                     <div class="product_video">
                        <div class="play_video">
                        <div class="pro_video_info">

                              <img width="303.25" height="175" src="{{URL::to('storage/app/public/uploads/product_cover_images/'.@$product->cover_image)}}">



                        </div>

                        </div>

                        <div class="price_list">
                           <div class="price_tag">
                              @if(@$product->discounted_price>0)
                              <span class="re_price">R${{@$product->discounted_price}}</span>
                              <span class="dis_price"><del>R${{@$product->price}}</del></span>
                              <span class="dis_price">
                                    <?php  $a = ((@$product->price - @$product->discounted_price)/@$product->price)*100;
                                    echo ceil($a)
                                    ?>% de desconto
                              </span>
                              @else
                              <span class="re_price">R${{@$product->price}}</span>
                              @endif
                           </div>
                           @if(@auth()->user()->id != $product->professional_id)
                           <div class="pro_btn">
                              @if(in_array(@$product->id , $orderedProducts))
                                    <p class="text-center text-secondary w-100" style="font-size:1rem;">@lang('client_site.already_ordered')</p>
                              @else
                                 @if(date('Y-m-d') >= date('Y-m-d', strtotime(@$product->purchase_start_date)))
                                    @if(@$product->purchase_end_date && date('Y-m-d') <= date('Y-m-d', strtotime(@$product->purchase_end_date)))
                                       <p class="text-center text-secondary w-100" style="font-size:1rem;">@lang('client_site.sale_over')</p>
                                    @else
                                       <a href="javascript:;" class="solid_btn" id="addToCart" data-product="{{$product->id}}" @if(@$cartData) style="display: none" @endif>@lang('client_site.add_to_cart')</a>
                                       <a href="{{route('product.cart')}}" class="solid_btn" id="goToCart" @if(@$cartData==null) style="display: none" @endif>@lang('client_site.go_to_cart')</a>
                                       <a href="javascript:;" id="buyNow" data-product="{{$product->id}}" class="out_btn">{{__('site.buy_now')}}</a>
                                    @endif
                                 @else
                                    <p class="text-center text-secondary w-100" style="font-size:1rem;"><span style="font-size:0.8rem;">@lang('client_site.available_from')</span><br>{{date('jS F, Y', strtotime(@$product->purchase_start_date))}}</p>
                                 @endif
                              @endif
                           </div>
                           @endif
                           <div class="money_back">
                              <!-- <p>30-Day Money-Back Guarantee</p> -->
                           </div>
                           <div class="course_info">
                              <h6>@lang('client_site.this_course_includes'):</h6>
                              <ul>
                                    <li><i class="fa fa-file-video-o"></i> {{count(@$chapters)}} @lang('client_site.chapters')</li>
                                    <li><i class="fa fa-play-circle"></i> {{@$product->total_lessons}} @lang('client_site.lessons')</li>
                                    <li><i class="fa fa-file-o"></i> {{@$product->course_duration}}  @lang('client_site.total_duration')</li>

                              </ul>
                           </div>
                        </div>

                     </div>
                  </div>
                  <div class="product_left_panel">
                     <div class="prro_top">
                        <div class="topic_heading">
                           <ul>
                              <li><a href="#">{{@$product->category->category_name}}</a></li>
                              <li><i class="fa fa-angle-right"></i></li>
                              <li><a href="#">{{@$product->subCategory->name}}</a></li>
                              <div class="clearfix"></div>
                           </ul>
                           <h1 class="prod_heading">{{@$product->title}}</h1>
                           <p class="pro_para">
                              @if(strlen(@$product->description) < 330)
                              {{ @$product->description }}
                              @else
                              {{substr(@$product->description, 0, 327 ) . '...'}}
                              @endif
                           </p>
                        </div>
                        <div class="rating_sec">
                           <div class="rating_pro"> <span class="ratingstar">
                                 {{round(@$prod_avg_rating, 1)}}
                              <i class="fa fa-star"></i>
                              <!-- <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star-o"></i> -->
                              </span> ({{ round(@$product->total_rating) }} @lang('client_site.ratings'))
                           </div>
                           <p>{{@$totalStudents ?? '0'}} Aluno{{@$totalStudents == 1 ? '' : 's'}}</p>
                        </div>
                        <div class="creat"> <span>@lang('client_site.created_by') </span><a href="{{route('pblc.pst',['slug' => @$product->professional->slug])}}">{{@$product->professional->name}}</a> </div>
                        <div class="ins_ele">
                           <ul>
                               <li>
                                   <img src="{{ URL::to('public/frontend/images/clock.png') }}"> @lang('client_site.last_updated')
                                   {{-- {{date('dS M Y',strtotime(@$product->updated_at))}} --}}
                                   {{-- @lang('client_site.last_updated') --}}
                                   @php $month=[ "0", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ] @endphp
                                       {{date('d',strtotime(@$product->updated_at))}}
                                       {{$month[(int)date('m',strtotime(@$product->updated_at))]}}
                                       {{date('Y',strtotime(@$product->updated_at))}}
                                </li>
                              <!-- <li><img src="images/www.png"> English</li>
                              <li><img src="images/lan.png"> English [Auto], French [Auto]</li>
                              <li><a href="#">5 more</a></li> -->
                           </ul>
                           <div class="clearfix"></div>
                        </div>

                     </div>

               </div>
            </div>
         </div>
      </div>
   </section>
   <div class="clearfix"></div>
   <div class="container">
       <div class="row">
           <div class="col-12 new_add_rm01">
               <div class="title-wrapper text-left">
                   <h3 class="title title-simple text-left text-normal">@lang('site.add_a_review')</h3>
                </div>
            </div>
            <form action="{{route('add.rate.review.product')}}" method="post">
                <div class="col-lg-12 new_add_rm">
                    <div class="rating-form">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="float-left pr-2 pt-1">
                                    <label for="rating">@lang('client_site.your_rating'): </label>
                                </div>
                                @csrf
                                @if(@$review->rate != null)
                                @php
                                $total = @$review->rate;
                                @endphp
                                @for($i=1; $i<=5; $i++)
                                @if($total>0)
                                @if($i <= $total)
                                <a href="javascript:;"><img src="{{URL::to('public/frontend/images/star.png')}}" alt=""></a>
                                @else
                                <a href="javascript:;"><img src="{{URL::to('public/frontend/images/star2.png')}}" alt=""></a>
                                @endif
                                @else
                                <a href="javascript:;"><img src="{{URL::to('public/frontend/images/star2.png')}}" alt=""></a>
                                @endif
                                @endfor
                                @else
                                <i class="fa fa-star stary" aria-hidden="true" id="st1"></i>
                                <i class = "fa fa-star stary" aria-hidden = "true" id = "st2"></i>
                                <i class = "fa fa-star stary" aria-hidden = "true" id = "st3"></i>
                                <i class = "fa fa-star stary" aria-hidden = "true" id = "st4"></i>
                                <i class = "fa fa-star stary" aria-hidden = "true" id = "st5"></i>
                                @endif
                                <input type="hidden" name="selected_star" id="selected_star" value="{{@$review->rate}}">
                                <input type="hidden" name="product_id" value="{{@$id}}">
                                <!--  <div class="float-left">
                                    <div class="star-rating"> <span class="fa fa-star-o" data-rating="1"></span> <span class="fa fa-star-o" data-rating="2"></span> <span class="fa fa-star-o" data-rating="3"></span> <span class="fa fa-star-o" data-rating="4"></span> <span class="fa fa-star-o" data-rating="5"></span>
                                        <input type="hidden" name="whatever1" class="rating-value" value="2.56"> </div>
                                </div> -->
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                           <div class="pr-2 pt-1">
                              <label for="rating">@lang('client_site.your_review'): </label>
                           </div>
                           @if(@$review->review)
                              <p style="font-size: 1rem;">{{@$review->review}}</p>
                           @else
                              <textarea id="review"  name="review" cols="30" rows="4" class="form-control mb-4" placeholder="@lang('client_site.product_review') *" required="">{{@$review->review}}</textarea>
                           @endif
                        </div>
                    </div>
                    <input type="submit" class="btn btn-default" name="" value="@lang('client_site.product_review_submit')" @if(@$review->review) style="display:none;" @endif>
                </div>
            </form>
        </div>
    </div>
</div>




@endsection
@section('footer')
@include('includes.footer')
<script>
      $(document).ready(function() {
          $("#st1").click(function() {
              $(".stary").css("color", "black");
              $("#st1").css("color", "yellow");
              $('#selected_star').val(1);

          });
          $("#st2").click(function() {
              $(".stary").css("color", "black");
              $("#st1, #st2").css("color", "yellow");
              $('#selected_star').val(2);

          });
          $("#st3").click(function() {
              $(".stary").css("color", "black")
              $("#st1, #st2, #st3").css("color", "yellow");
              $('#selected_star').val(3);

          });
          $("#st4").click(function() {
              $(".stary").css("color", "black");
              $("#st1, #st2, #st3, #st4").css("color", "yellow");
              $('#selected_star').val(4);

          });
          $("#st5").click(function() {
              $(".stary").css("color", "black");
              $("#st1, #st2, #st3, #st4, #st5").css("color", "yellow");
              $('#selected_star').val(5);

          });
        });
</script>
@endsection
