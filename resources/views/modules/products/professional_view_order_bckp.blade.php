@extends('layouts.app')
@section('title')
    @lang('site.welcome_to_dashboard')
@endsection
@section('style')
@include('includes.style')
<style>
    .main-boxes{
        font-family: 'Poppins', sans-serif;
        color: #252424;
        font-size: 15px;
        width: 100%;
        float: left;
        padding: 5px 0;
        font-weight: 400;
        margin: 0;
    }
    .main-boxes strong{
        font-weight: 500;
        color: #1781d2;
        width: 122px;
        float: left;
    }
    .main-boxes .files {
        display: inline-block;
        float: left;
        width: auto;
        width: calc(100% - 124px);
    }
    .main-boxes .files a{
        margin-right:4px;
    } 
    .w-100 {
    	width: 100% !important;
    	display: block;
    	overflow: hidden;
    }
    @media(max-width:767px) {
        .main-boxes .files {
            width: 100% !important;
        }
    }
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('client_site.view_order')</h2>

        <div class="bokcntnt-bdy">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile')<a href="{{ route('professional_qualification') }}">@lang('client_site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">

                <div class="col-md-12 view-page">
                    <p><strong>@lang('client_site.ordered_by') </strong>{{ @$order->userDetails->nick_name ? @$order->userDetails->nick_name : @$order->userDetails->name }}</p>

                    <p><strong>Order ID :</strong>{{@$order->id}}</p>
                    <!-- <p><strong>@lang('client_site.product_title') :</strong> {{ @$order->productDetails->title }}</p> -->
                    <!-- <p><strong>@lang('client_site.category') :</strong> {{@$order->productCategoryDetails ? @$order->productCategoryDetails->category_name: 'N.A' }}</p> -->
                    <p><strong>@lang('client_site.ordered_on') :</strong> {{ @$order->order_date }}</p>
                    <p><strong>@lang('client_site.amount') :</strong>R$ {{ @$order->amount }}</p>
                    @if(@$order->product_file || @$order->url)
                        <p><strong>@lang('client_site.status') :</strong> @lang('client_site.complete')</p>
                    @elseif(@$order->payment_status == 'I')
                        <p><strong>@lang('client_site.status') :</strong> @lang('client_site.initiated')</p>
                    @elseif(@$order->payment_status == 'PR')
                        <p><strong>@lang('client_site.status') :</strong> @lang('client_site.processing_payment')</p>
                    @elseif(@$order->payment_status == 'F')
                        <p><strong>@lang('client_site.status') :</strong> @lang('client_site.failed')</p>
                    @elseif(@$order->payment_status == 'P')
                        <p><strong>@lang('client_site.status') :</strong> @lang('client_site.paid')</p>
                    @endif
                    <!-- if product in not uploaded, provision to upload product-->
                    @if(count(@$product_files) == 0)
                        <form id="upload_product_files" action="{{route('upload.product.files', @$order->id)}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="main-boxes">
                                <strong>@lang('client_site.your_files') :</strong>
                                <div class="files">
                                    @foreach(@$order_files as $file)
                                        <p>
                                        @if(@$file->file_type == 'file')
                                            @if(strpos(@$file->mime_type, 'video') !== false)
                                                <a href="{{ route('js.player',@$file->file_name) }}" target="_blank">{{@$file->caption}}</a>&nbsp;
                                                <small style="color:black;"><a title="Play" href="{{ route('js.player',@$file->file_name) }}" target="_blank"><i class="fa fa-external-link" aria-hidden="true"></i></a></small>
                                            @else
                                                <a href="{{ URL::to('storage/app/public/uploads/product_files/') . '/' . @$file->file_name}}" target="_blank">{{@$file->caption}}</a>&nbsp;
                                                <small style="color:black;"><a title="Download" href="{{ URL::to('storage/app/public/uploads/product_files').'/'.@$file->file_name }}" download="{{@$file->caption}}"><i class="fa fa-download" aria-hidden="true"></i></a></small>
                                            @endif
                                        @elseif(@$file->file_type == 'link')
                                            <a target="_blank" href="{{ @$file->file_name }}">{{@$file->caption}}</a>&nbsp;
                                            <small style="color:black;"><a target="_blank" title="Go to link" href="{{ @$file->file_name }}"><i class="fa fa-external-link" aria-hidden="true"></i></a></small>
                                        @endif
                                        </p>
                                    @endforeach
                                </div>
                            </div>
                            <div class="w-100"></div>
                            <p><strong class="w-100">@lang('client_site.add_files_for_order') :</strong></p>
                            <div class="row p-0">
                                <div class="col-md-5">
                                    <input type="text" name="caption" placeholder="Caption" class="form-control" id="caption">
                                </div>
                                <div class="col-md-7 text-center">
                                    <input type="file" id="product_file" name="product_file" class="form-control file">
                                    OR
                                    <input type="url" id="product_link" name="product_link" class="form-control link" placeholder="Add a link">
                                    <label id="product_file_error" style="color:red;"></label>
                                </div>
                                <!-- <div class="col-md-2">
                                    <button type="button" class="login_submitt m-0">+</button>
                                </div> -->
                            </div>
                            <button type="submit" class="login_submitt">Save</button>
                        </form>
                    @else
                        <div class="main-boxes">
                            <strong>@lang('client_site.your_files') :</strong>
                            <div class="files">
                                @foreach(@$product_files as $file)
                                    <p>
                                        @if(@$file->file_type == 'file')
                                            @if(strpos(@$file->mime_type, 'video') !== false)
                                                <a href="{{ route('js.player',@$file->product_file) }}" target="_blank">{{@$file->caption}}</a>&nbsp;
                                                <small style="color:black;"><a title="Play" href="{{ route('js.player',@$file->product_file) }}" target="_blank"><i class="fa fa-external-link" aria-hidden="true"></i></a></small>
                                            @else
                                                <a href="{{ URL::to('storage/app/public/uploads/product_files/') . '/' . @$file->product_file}}" target="_blank">{{@$file->caption}}</a>&nbsp;
                                                <small style="color:black;"><a title="Download" href="{{ URL::to('storage/app/public/uploads/product_files').'/'.@$file->product_file }}" download="{{@$file->caption}}"><i class="fa fa-download" aria-hidden="true"></i></a></small>
                                            @endif
                                        @elseif(@$file->file_type == 'link')
                                            <a href="{{@$file->product_file}}" target="_blank">{{@$file->caption}}</a>&nbsp;
                                            <small style="color:black;"><a target="_blank" title="Go to link" href="{{@$file->product_file}}"><i class="fa fa-external-link" aria-hidden="true"></i></a></small>
                                        @endif
                                    </p>
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
<script>
    $(document).ready(function(){
        $('#upload_product_files').validate({
            submitHandler: function(form) {
                var flag = 0;
                $("#product_file_error").html("");
                $('#caption').parent().find('label').remove();
                if($('#product_link').val() == '' && $('#product_file').val() == ''){
                    $("#product_file_error").html("Please add a file or a link.");
                    flag = 1;
                }
                if($('#caption').val() == ''){       
                    $('#caption').addClass('error');
                    $('#caption').parent().append('<label class="error">This field is required</label>');
                    flag = 1;
                }
                // $('.content').each(function(content){
                //     if($(this).find('.file').val() == '' && $(this).find('.link').val() == ''){
                //         $(this).append('<label class="error">Please add a file or a link.</label>');
                //         flag = 1;
                //     }
                // });
                if($('#product_file')[0].files.length){
                    console.log('files present');
                    var fullPath = $('#product_file').val();
                    var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
                    var filename = fullPath.substring(startIndex);
                    if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                        filename = filename.substring(1);
                    }
                    var fileExt = filename.split('.').pop();

                    if(fileExt == "exe" || fileExt == "bat" || fileExt == "img") {
                        $("#product_file_error").html("Please enter a valid file");
                        flag = 1;
                    }
                    if($('#product_file')[0].files[0].size > 20971520) {
                        $("#product_file_error").html("Please enter file less than 20 MB.");
                        flag = 1;
                    }
                }
                if(flag == 1)
                    return false;
                else
                    form.submit();
            },
        });
    });
</script>
@endsection
