@extends('layouts.app')
@section('title')
@lang('site.welcome_to_dashboard')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('client_site.dashboard')</h2>

        <div class="bokcntnt-bdy">
            @php
            $user = Auth::guard('web')->user();
            $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0) <center>
                <p class="alert alert-info">@lang('client_site.please_complete_your_profile')<a
                        href="{{ route('professional_qualification') }}">@lang('client_site.click_here')</a></p>
                </center>
                @endif
                <div class="mobile_filter">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                    <p>@lang('client_site.menu')</p>
                </div>
                @include('includes.professional_sidebar')
                <div class="dshbrd-rghtcntn">
                    <div class="from-field">
                        <div class="weicome">
                            <h3>@lang('site.step_2') - Upload Files - {{$product->title}}</h3>
                        </div>
                        <div class="dash_main">
                            <center>
                                <p class="alert alert-info">
                                    Aqui você pode adicionar arquivos, documentos, imagens ou vídeoaulas, pois tudo será hospedado na plataforma, mas você também pode adicionar o link de onde estão seus arquivos que o usuário irá visualizá-los tudo dentro da plataforma. Lembrando sempre do tamanho máximo dos arquivos.
                                </p>
                            </center>
                            <form action="{{ route('product.file.upload',['id'=>$product->id]) }}" method="post"
                                id="product_file_upload" enctype="multipart/form-data">
                                @csrf
                                <div class="form_body">

                                    <div class="row">

                                        <div class="col-lg-12 col-md-6 col-sm-12">
                                            <div class="form-group" style="width: 100%;!important">
                                                <label class="personal-label">Caption</label>
                                                <input type="text" placeholder="Digite o título da postagem"
                                                    class="required personal-type" name="caption">
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-6 col-sm-12">
                                            <div class="form-group" style="margin-top: 22px; overflow: hidden;">
                                                <label class="personal-label">@lang('site.product_file') (Maximum Size 20 MB)</label>
                                                <input type="file" class="personal-type" id="product_file" placeholder="File" name="product_file">
                                                <label id="product_file_error" style="color:red;"></label>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-6 col-sm-12">
                                            <div class="form-group" style="margin-top: 22px">
                                                <label class="personal-label">OR</label>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-6 col-sm-12">
                                            <div class="form-group" style="margin-top: 22px">
                                                <label class="personal-label">@lang('site.product_url')</label>
                                                <input type="url" class="personal-type" id="product_link"
                                                    placeholder="File" name="product_link">
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            <button type="submit"
                                                class="login_submitt">Upload</button>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
<script>
    $(document).ready(function(){
        console.log("doucment ready");
        // jQuery.validator.addMethod("lessthan100", function(value, element) {
		// 	return this.optional(element) || value < 100;
		// });
        $('#product_file_upload').validate({
            rules: {
                caption: "required",
            },
            submitHandler: function(form) {
                var flag = 0;
                $("#product_file_error").html("");
                // console.log($('#product_file')[0].files[0].size);

                if($('#product_link').val() == '' && $('#product_file').val() == ''){
                    $("#product_file_error").html("Please add a file or a link.");
                    flag = 1;
                }
                if($('#product_file')[0].files.length){
                    console.log('files present');
                    var fullPath = $('#product_file').val();
                    var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
                    var filename = fullPath.substring(startIndex);
                    if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                        filename = filename.substring(1);
                    }
                    var fileExt = filename.split('.').pop();

                    if(fileExt == "exe" || fileExt == "bat" || fileExt == "img") {
                        $("#product_file_error").html("Please enter a valid file");
                        flag = 1;
                    }
                    if($('#product_file')[0].files[0].size > 20971520) {
                        $("#product_file_error").html("Please enter file less than 20 MB.");
                        flag = 1;
                    }
                }
                if(flag == 1){
                    return false;
                } else {
                    form.submit();
                }
            }
        });
    });

</script>
@endsection
