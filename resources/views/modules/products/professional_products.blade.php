@extends('layouts.app')
@section('title')
    @lang('site.welcome_to_dashboard')
@endsection
@section('style')
@include('includes.style')
<style>
    .bt_m{
        margin-bottom: 6px !important;
    }
    .success_bt{
        background:#28a745 !important;
    }
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('client_site.my_products')</h2>

        <div class="bokcntnt-bdy">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0 && $user->sell!='PS')
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile')<a href="{{ route('professional_qualification') }}">@lang('client_site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <form name="myForm" id="myForm" method="post" action="{{route('search.product')}}">
                    @csrf
                    <div class="from-field">
                        <div class="row">
                            <div class="col-lg-7">
                                <label class="search_label">@lang('site.keyword')</label>
                                <input type="text" class="form-control" name="keyword" id="keyword" value="{{@$key['keyword']}}">
                            </div>
                            <div class="col-lg-2"></div>
                            @if(auth()->user()->sell != 'VS')
                            <div class="col lg-3">
                                <a class="btn btn-success" href="{{route('store.product')}}">+ @lang('site.add_product')</a>
                            </div>
                            @endif
                        </div>

                        <div class="frmfld">
                            <div class="form-group">
                                <label class="search_label">@lang('site.select') @lang('site.category')</label>
                                <select class="dashboard-type dashboard_select" name="category" id="category">
                                    <option value="">@lang('site.select') @lang('site.category')</option>
                                    @foreach(@$category as $cat)
                                        <option value="{{@$cat->id}}" @if(@$cat->id==@$key['category']) selected @endif>{{@$cat->category_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="frmfld">
                            <div class="form-group">
                                <label class="search_label">@lang('site.select') @lang('site.status')</label>
                                <select class="dashboard-type dashboard_select" name="status" id="status">
                                    <option value="">@lang('site.select') @lang('site.status')</option>
                                    <option value="A" @if(@$key['status']=='A') selected @endif>@lang('site.active')</option>
                                    <option value="I" @if(@$key['status']=='I') selected @endif>@lang('site.inactive') </option>
                                     <option value="AA" @if(@$key['status']=='AA') selected @endif>@lang('site.awaiting_approval') </option>
                                </select>
                            </div>
                        </div>
                        <div class="frmfld">
                            <button class="banner_subb fmSbm">@lang('site.filter')</button>
                        </div>
                    </div>
                </form>
                <div class="buyer_table">
                    <div class="table-responsive">
                        @if(sizeof(@$products)>0)
                        <div class="table">
                            <div class="one_row1 hidden-sm-down only_shawo">
                                <div class="cell1 tab_head_sheet">@lang('site.image')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.title')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.price')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.category')</div>
                                {{-- <div class="cell1 tab_head_sheet">posted on</div> --}}
                                <div class="cell1 tab_head_sheet">@lang('site.status')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.action')</div>
                            </div>
                            <!--row 1-->
                                @php
                                    $i=1;
                                @endphp
                                @foreach(@$products as $product)
                                    <div class="one_row1 small_screen31">
                                        <div class="cell1 tab_head_sheet_1">
                                            <!-- <span class="W55_1">@lang('site.no')</span> -->
                                            <img src="{{URL::to('storage/app/public/uploads/product_cover_images/') . '/' . @$product->cover_image}}" height="50" width="60">
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.title')</span>
                                            <p class="add_ttrr">{{@$product->title}}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.price')</span>
                                            <p class="add_ttrr">{{ @$product->discounted_price == '0.00' ? @$product->price : @$product->discounted_price }}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.category')</span>
                                            <p class="add_ttrr">{{@$product->category->category_name}}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.status')</span>
                                            <p class="add_ttrr">
                                                @if(@$product->admin_status=="I")
                                                    @lang('site.inactive')
                                                @elseif(@$product->admin_status=="AA")
                                                    @lang('site.awaiting_approval')
                                                @else
                                                    @if(@$product->status=="A")
                                                        @lang('site.active')
                                                    @else
                                                        @lang('site.inactive')
                                                    @endif
                                                @endif
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                            <!-- <a href="{{route('product.details',@$product->slug)}}" class="rjct">@lang('site.view')</a> -->
                                            <a href="{{route('store.product',@$product->id)}}" class="rjct bt_m">@lang('site.edit')</a>
                                            <a href="{{route('edit.course.chapters',@$product->id)}}" class="acpt editbtn bt_m">Produtos</a>
                                            <a href="javascript:;" class="acpt success_bt bt_m" title="@lang('site.copy_product_link')" onclick="copyText(`{{url('/')}}/product-details/{{@$product->slug}}`)">@lang('site.copy_link')</a>
                                            
                                            @if(auth()->user()->sell != 'VS')
                                            <a href="{{route('changestatus.product',@$product->id)}}" class="acpt bt_m" onclick="confirmStatusChange('{{@$product->status}}')">
                                                @if(@$product->status=="A")
                                                    @lang('site.inactivate')
                                                @else
                                                    @lang('site.activate')
                                                @endif
                                            </a>
                                            @endif
                                            <a href="{{route('delete.product',@$product->id)}}" class="rjct  bt_m" onclick="return confirm('@lang('site.do_delete_product')');">@lang('site.delete')</a>
                                            @if(@$product->admin_status == 'A')
                                            <a href="{{route('assign.product.certificate.template',@$product->id)}}" class="acpt success_bt bt_m">@lang('site.assign_template')</a>
                                            @endif
                                        </div>
                                    </div>
                                    @php
                                        $i++;
                                    @endphp
                                @endforeach

                        </div>
                        @else
                            <div class="one_row small_screen31">
                                <center><span><h3 class="error">OOPS! @lang('site.product_not_found').</h3></span></center>
                            </div>
                        @endif
                    </div>


                </div>

            </div>
            {{ @$products->links('vendor.pagination.default') }}
        </div>
    </div>
</section>
@endsection
@section('footer')
<script>
function confirmStatusChange(status){
    var question = status == 'A' ? "@lang('site.inactivate_product')" : "@lang('site.activate_product')";
    return confirm(question);
}
function copyText(text){
    var input = document.createElement('input');
    input.setAttribute('value', text);
    document.body.appendChild(input);
    input.select();
    var result = document.execCommand('copy');
    document.body.removeChild(input);
    toastr.success("Link Copied");
    return result;
}
</script>
@include('includes.footer')

@endsection
