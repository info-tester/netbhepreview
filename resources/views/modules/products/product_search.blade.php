@extends('layouts.app')
@section('title')
@lang('site.get_the_right_advice')
@endsection

@section('style')
@include('includes.style')
<style>
    .dropdown_dash2 {

        background: #fff;
        box-shadow: 1px 3px 4px -1px rgb(43 43 43);
        position: absolute;
        left: 0;
        top: 30px;
        width: 180px;
        z-index: 99999;
        border-radius: 6px;

    }

    .dropdown_dash2 ul {

        margin: 0;

        padding: 0;

    }

    .dropdown_dash2 ul li {

        display: block;

        margin: 0;

        overflow: hidden;

        width: 100%;

        padding: 0 !important;

        text-align: left;

    }

    .dropdown_dash2 ul li:last-child {

        padding-left: 0px !important;

        border: none;

    }

    .dropdown_dash2 ul li a {

        color: #747474;

        float: left;

        font-family: 'Poppins', sans-serif;

        font-size: 14px;

        font-weight: normal;

        padding:5px 12px !important;

        text-decoration: none;

        width: 100%;

    }

    .dropdown_dash2 ul li a:hover {

        color: #4290fb;

    }

    #primary_nav_wrap ul a:active
    {

      color: #8ABF29;
    background-color: #2d2f33;
        -webkit-transition: all 50ms linear;
    -moz-transition: all 50ms linear;
    -ms-transition: all 50ms linear;
    -o-transition: all 50ms linear;
    transition: all 50ms linear;
    }

    #primary_nav_wrap ul ul li.dir33
    {
      position:relative;
    }
    #primary_nav_wrap ul li{
      float:left;
      margin:0;
      padding:0
    background: #121314;
    }

    #primary_nav_wrap ul li.current-menu-item
    {
      background-color: #222326;
    }



    #primary_nav_wrap ul ul
    {
      display:none;
      top:100%;
      left:0;
      padding:0;
    }

    #primary_nav_wrap ul ul li
    {
      float:none;
      width:135px

    }

    #primary_nav_wrap ul ul a
    {
      padding:10px 15px;

    }


    #primary_nav_wrap ul ul {
        position: absolute;
        top: 0;
        left: 99%;
        background: #ffff;
        border-radius: 5px;
        padding: 10px;
        box-shadow: 1px 3px 4px -1px rgb(43 43 43);
    }
    .dropdown_dash2 ul li{
        overflow: inherit !important;
    }

    #primary_nav_wrap ul li:hover > ul
    {
      display:block

    }
    .dropdown_dash2 ul {



        margin: 0;



        padding: 0;



    }
    </style>
@endsection

@section('scripts')
@include('includes.scripts')
@endsection

@section('header')
@include('includes.header')
@endsection


@section('content')
<form action="{{ route('all.product.search.filter') }}" method="post" name="myForm" id="myForm">
    @csrf
    <section class="search-filter">
        <div class="container">
            <div class="row">
                  <div class="p-relative">
                <div class="new-menucate"><span>Categories <i class="fa fa-sort-desc" aria-hidden="true"></i></span></div>
                <div class="dropdown_dash2" style="display:none;"   id="primary_nav_wrap">
                 <ul>
                    @foreach($allProductCategory as $cat)
                    <li><a class="dir dir33" href="javascript:;">{{@$cat->category_name}}  @if(count(@$cat->getSubCategories) > 0)<i class="fa fa-angle-right"></i> @endif</a>
                        <ul>
                            @if(count(@$cat->getSubCategories) > 0)
                                @foreach($cat->getSubCategories as $row)
                                  <li><a href="javascript:;" class="search_by_dropdwn" data-cat="{{$cat->slug}}" data-scat="{{$row->slug}}">{{@$row->name}}</a></li>
                                @endforeach
                            @else
                                  <li>No Subcategories</li>
                            @endif
                        </ul>

                    </li>

                    @endforeach

                 </ul>
              </div>
          </div>
                <div class="search-white">
                    <div class="form-group one-type">
                        <label class="search-label">@lang('client_site.keywords')</label>
                        <input type="text" placeholder="Digite suas palavras-chave" class="result-type" name="keyword"
                            id="keyword" value="{{ @$key['keyword'] }}">
                    </div>
                    <div class="form-group one-type">
                        <label class="search-label">@lang('client_site.category')</label>

                        <select class="result-type result-select" name="category" id="category">
                            <option value="0">@lang('client_site.select_category')</option>
                            @foreach($allProductCategory as $cat)
                            <option value="{{ @$cat->id }}" @if(@$key['category']==@$cat->id || Request::segment(2) == $cat->slug) selected @endif>{{ @$cat->category_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group one-type">
                        <label class="search-label">@lang('client_site.sub_category')</label>

                        <select class="result-type result-select" name="sub_category" id="sub_category">
                            <option value="0">@lang('client_site.select_sub_category')</option>
                            @foreach($allProductSubCategory as $cat)
                            <option value="{{ @$cat->id }}" @if(@$key['sub_category']==@$cat->id || Request::segment(2) == $cat->slug) selected @endif>{{ @$cat->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    {{-- <div class="form-group tow-type">
                                <label class="search-label">Experience Level</label>
                                <!--<input type="text" placeholder="Type your keywords" class="result-type">-->
                                <select class="result-type result-select" name="experience">
                                    <option>Select Experience</option>
                                        <option value="1" @if(@$key['experience']==1) selected @endif>Less than or 1 Year</option>
                                        <option value="3" @if(@$key['experience']==3) selected @endif>Less than or 1 to 3 Year</option>
                                        <option value="5" @if(@$key['experience']==5) selected @endif>Less than or 3 to 5 Year</option>
                                </select>
                            </div> --}}
                    <div class="form-group two-type">
                        <label class="search-label">Preço</label>
                        <div class="slider_rnge">
                            <div id="slider-range"
                                class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
                                <div class="ui-slider-range ui-widget-header ui-corner-all"
                                    style="left: 0%; width: 100%;"></div>
                                <span tabindex="0" class="ui-slider-handle ui-state-default ui-corner-all"
                                    style="left: 0%;"></span>
                                <span tabindex="0" class="ui-slider-handle ui-state-default ui-corner-all"
                                    style="left: 100%;"></span>
                            </div>
                            <span>
                                <input type="text" class="price_numb" readonly id="amount" name="amount">
                            </span>
                        </div>
                    </div>
                    <input type="hidden" name="max_price" id="max_price">
                    <input type="hidden" name="min_price" id="min_price">

                    <button class="submit-search12345 frm1" type="submit">@lang('client_site.search')</button>
                </div>

            </div>
        </div>
    </section>
</form>
<section class="search-body">
    <div class="container">
        <div class="row rwmrgn">
            <div class="srchpgi">
                <form method="post" action="{{ route('all.product.search.filter') }}" id="form2">
                    @csrf
                    <select name="sort_by" id="sort_by" class="result-type srtby" style="width: 120px !important;">
                        <option>@lang('client_site.sort_by')</option>
                        <option value="A" @if(@$key['sort_by']=="A" ) selected @endif>@lang('client_site.assending')
                        </option>
                        <option value="D" @if(@$key['sort_by']=="D" ) selected @endif>@lang('client_site.desending')
                        </option>
                    </select>
                </form>
                {{ @$allProduct->links('vendor.pagination.default') }}
            </div>
            @if(@$allProduct)
            <div class="all-search" style="width: 100%;">
                <div class="row rwmrgn">
                    @php
                        $cartProduct=getAllCart();
                    @endphp
                    @foreach(@$allProduct as $pro)

                    <div class="col-lg-4 col-md-6 col-sm-12 show-prod" id="card_chap{{@$pro->id}}">
                        <div class="guiede-box">
                            <div class="guide-image">
                                <a href="{{route('product.details',@$pro->slug)}}">
                                    <img src="{{ @$pro->cover_image ? URL::to('storage/app/public/uploads/product_cover_images').'/'.@$pro->cover_image: URL::to('public/frontend/images/no_img.png') }}" alt="">
                                </a>
                            </div>
                            <div class="guide-dtls">
                                <div class="product-intro">
                                    <a href="{{route('product.details',@$pro->slug)}}">
                                        <h3>
                                            @if(strlen(@$pro->title) < 50)
                                            {{ @$pro->title }}
                                            @else {{substr(@$pro->title, 0, 47 ) . '...'}}
                                            @endif
                                        </h3>
                                    </a>
                                    <p><i class="fa fa-tag" aria-hidden="true"></i> {{ @$pro->category->category_name }} <i class="fa fa-angle-right" aria-hidden="true"></i> {{ @$pro->subCategory->name }}</p>
                                    <p><i class="fa fa-user" aria-hidden="true"></i> {{ @$pro->professional->name }}</p>
                                </div>
                                <div class="rrt_stll01">
                                    @php $month=[ "0", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ] @endphp
                                    <p class="small_dates"><strong>@lang('client_site.sale_starts_on'): </strong>
                                        {{date('d',strtotime(@$pro->purchase_start_date))}}
                                        {{$month[(int)date('m',strtotime(@$pro->purchase_start_date))]}}
                                        {{date('Y',strtotime(@$pro->purchase_start_date))}}
                                    </p>
                                    <p class="small_dates">
                                        <strong>@lang('client_site.sale_ends_on'): </strong>
                                        @if(@$pro->purchase_end_date)
                                            {{date('d',strtotime(@$pro->purchase_end_date))}}
                                            {{$month[(int)date('m',strtotime(@$pro->purchase_end_date))]}}
                                            {{date('Y',strtotime(@$pro->purchase_end_date))}}
                                        @else
                                            @lang('client_site.not_specified')
                                        @endif
                                    </p>
                                    <p class="ttyu01 small_dates">
                                        <strong>@lang('client_site.available_from'): </strong>
                                        @if(@$pro->course_start_date)
                                            {{date('d',strtotime(@$pro->course_start_date))}}
                                            {{$month[(int)date('m',strtotime(@$pro->course_start_date))]}}
                                            {{date('Y',strtotime(@$pro->course_start_date))}}
                                        @else
                                            @lang('client_site.not_specified')
                                        @endif
                                    </p>
                                </div>
                                @if(@$pro->description)
                                    <div class="product-more more_det">
                                        <p class="pr-desc" data-desc="{{$pro->description}}"></p>
                                    </div>
                                @endif
                                <div class="price-and-more price-100">
                                    @if(@$pro->price)
                                    <div class="price-lefts">
                                        <span>@lang('client_site.fees')</span>
                                        @if(@$pro->discounted_price > 0)
                                        <p><del>R${{@$pro->price}}</del>&nbsp;
                                            {{-- {{ ((@$pro->price-@$pro->discounted_price)/@$pro->price)*100 }}% de desconto --}}

                                            <?php  $a = ((@$pro->price-@$pro->discounted_price)/@$pro->price)*100;
                                          echo ceil($a)
                                          ?>% de desconto
                                        </p>
                                        <p style="font-size: 1rem;">R${{@$pro->discounted_price}}</p>
                                        @else
                                        <p style="font-size: 1rem;">R${{@$pro->price}}</p>
                                        @endif
                                    </div>
                                    @endif
                                    {{-- <a class="guide-btn pull-right pull-right-1" href="{{route('product.details',@$pro->slug)}}">@lang('client_site.view_more')</a> --}}
                                    @if(in_array(@$pro->id , $orderedProducts))
                                        <p class="text-secondary" style="font-size:1rem;">@lang('client_site.already_ordered')</p>
                                    @else
                                        @if(date('Y-m-d') >= date('Y-m-d', strtotime(@$pro->purchase_start_date)))
                                            @if(@$pro->purchase_end_date && date('Y-m-d') >= date('Y-m-d', strtotime(@$pro->purchase_end_date)))
                                                <p class="text-secondary" style="font-size:1rem;">@lang('client_site.sale_over')</p>
                                            @else
                                                <a class="guide-btn pull-left buyNow" href="javascript:void(0);" data-product="{{@$pro->id}}">{{__('site.buy_now')}}</a>
                                                <a href="javascript:void(0);" class="guide-btn pull-left pull-right-1 addToCart cartClass{{$pro->id}}" data-product="{{@$pro->id}}" @if(@in_array($pro->id, $cartProduct)) style="display: none" @endif> @lang('client_site.add_to_cart')</a>
                                                <a href="{{route('product.cart')}}" class="guide-btn pull-left pull-right-1 GoToCart{{$pro->id}}" @if(!@in_array(@$pro->id, $cartProduct)) style="display: none" @endif>@lang('client_site.go_to_cart')</a>
                                            @endif
                                        @else
                                            <p class="text-secondary" style="font-size:1rem;">
                                                <span style="font-size:0.7rem; text-align:center;">@lang('client_site.available_from')</span><br>
                                                @php $month=[ "0", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ] @endphp
                                                {{date('d',strtotime(@$pro->purchase_start_date))}}
                                                {{$month[(int)date('m',strtotime(@$pro->purchase_start_date))]}}
                                                {{date('Y',strtotime(@$pro->purchase_start_date))}}
                                            </p>

                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="float-right w-100">
                {{ @$allProduct->links('vendor.pagination.default') }}
            </div>
            @else
            {{-- <div class="all-search"> --}}
            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                <center>
                    <h2><span class="error">@lang('client_site.search_result_not_found')</span></h2>
                </center>
            </div>
            {{-- </div> --}}
            @endif

        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
<script src="{{ URL::to('public/frontend/js/jquery.com_ui_1.11.4_jquery-ui.js') }}"></script>
<script>
    $(function() {
                $( "#slider-range" ).slider({
                    range: true,
                    min: {{ @$minprice->price }},
                    max: {{ @$maxprice->price }},
                    values: [ {{ @$key['min_price'] ? @$key['min_price']: @$minprice->price }}, {{ @$key['max_price'] ? $key['max_price']: @$maxprice->price }} ],
                    slide: function( event, ui ) {
                        $( "#amount" ).val( "R$" + ui.values[ 0 ] + " - R$" + ui.values[ 1 ] );
                        $('#min_price').val(ui.values[ 0 ]);
                        $('#max_price').val(ui.values[ 1 ]);
                    }
                });
                $( "#amount" ).val( "R$" + $( "#slider-range" ).slider( "values", 0 ) +
                    " - R$" + $( "#slider-range" ).slider( "values", 1 ) );

                        $('#min_price').val($( "#slider-range" ).slider( "values", 0 ));
                        $('#max_price').val($( "#slider-range" ).slider( "values", 1 ));
            });

            $('.frm1').click(function(){
                $('#myForm').submit();
            });

            // $('#category').change(function(){
            //     if($('#category').val()==""){
            //         $('#subcategory').html("");
            //     }else{
            //         var reqData = {
            //           'jsonrpc' : '2.0',
            //           '_token' : '{{csrf_token()}}',
            //           'params' : {
            //                 'cat' : $('#category').val()
            //             }
            //         };
            //         $.ajax({
            //             url: "{{ route('fetch.subcat') }}",
            //             method: 'post',
            //             dataType: 'json',
            //             data: reqData,
            //             success: function(response){
            //                 if(response.status==1) {
            //                     var i=0, html="";
            //                     html = '<option value="">@lang('client_site.select_option')</option>';
            //                     for(;i<response.result.length;i++){
            //                         html+='<option value='+response.result[i].id+'>'+response.result[i].name+'</option>';
            //                     }
            //                     $('#subcategory').html(html);
            //                     }
            //             }, error: function(error) {
            //                 console.error(error);
            //             }
            //         });
            //     }
            // });

            $('#sort_by').change(function(){
                $('#form2').submit();
            });

            $('.ssnbtnslg').click(function(){
                if($(this).data('slug')!=""){
                    localStorage.removeItem('bookSlug');
                    localStorage.setItem('bookSlug', $(this).data('slug'));
                    location.href="{{ route('login') }}"
                }
            });
</script>
<script>
    $(document).ready(function(){
            $('body').on('click', '.addToCart', function() {
            // $('.addToCart').click(function(){
                var productId = $(this).data('product');
                console.log(productId)
                var reqData = {
                    'jsonrpc': '2.0',
                    '_token': '{{csrf_token()}}',
                    'params': {
                        productId: productId,
                    }
                };
                $.ajax({
                    url: '{{ route('product.add.to.cart') }}',
                    type: 'post',
                    dataType: 'json',
                    data: reqData,
                })
                .done(function(response) {
                    console.log(response);
                    $('.cou_cart').text(response.result.cart.length);
                    $('.cartClass'+productId).css('display','none');
                    $('.GoToCart'+productId).css('display','block');
                    console.log($(this));
                })
                .fail(function(error) {
                    console.log("error", error);
                })
                .always(function() {
                    console.log("complete");
                })
            })
            $('.buyNow').click(function(){
                var productId = $(this).data('product');
                console.log(productId)
                var reqData = {
                    'jsonrpc': '2.0',
                    '_token': '{{csrf_token()}}',
                    'params': {
                        productId: productId,
                    }
                };
                $.ajax({
                    url: '{{ route('product.add.to.cart') }}',
			        type: 'post',
			        dataType: 'json',
			        data: reqData,
                })
                .done(function(response) {
                    window.location.href = '{{route('product.order.store')}} ';
                })
                .fail(function(error) {
                    console.log("error", error);
                })
                .always(function() {
                    console.log("complete");
                })
            })
        })

</script>
<script>
     $(document).ready(function(){
        $(".new-menucate").mouseenter(function(){
            $(".dropdown_dash2").slideDown();
        });
        $(".dropdown_dash2").mouseleave(function(){
            $(".dropdown_dash2").slideUp();
        });
    });
</script>
<script>
    $('.search_by_dropdwn').click(function(){

        var cat = $(this).data('cat');
        var scat= $(this).data('scat');
        var reqData = {
                    'jsonrpc': '2.0',
                    '_token': '{{csrf_token()}}',
                    'params': {
                        cat: cat,
                        scat:scat,
                    }
        };
        $.ajax({
            url: "{{ route('search.cat.subcat') }}",
            method: 'post',
            dataType: 'json',
            data: reqData,
            success: function(response){
                if(response.status == 1){
                    $('.show-prod').hide();
                    $(response.result).each(function(i, prod){
                        console.log(prod);

                        $('#card_chap'+prod.id).show();
                    });
                }
            }
            ,error: function(error) {
                console.error(error);
            }
        });

    });
</script>
@endsection
