@extends('layouts.app')
@section('title')
    @lang('site.welcome_to_dashboard')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')
<style>
    .myCertModal .modal-dialog {
        max-width: 742px !important;
    }
</style>
<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.choose_certificate_template')</h2>

        <div class="bokcntnt-bdy">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile')<a href="{{ route('professional_qualification') }}">@lang('client_site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="col-md-12 view-page">
                    <p><strong>@lang('site.product_name') :</strong>{{@$product->title}}</p>
                    <p><strong>@lang('site.price')  :</strong>{{@$product->discounted_price}}</p>
                    <p><strong>@lang('site.category') :</strong>{{@$product->category->category_name}}</p>
                    <p><strong>@lang('site.status') :</strong>
                        @if(@$product->admin_status=="I")
                            @lang('site.inactive')
                        @elseif(@$product->admin_status=="AA")
                            @lang('site.awaiting_approval')
                        @else
                            @if(@$product->status=="A")
                                @lang('site.active')
                            @else
                                @lang('site.inactive')
                            @endif
                        @endif
                    </p>
                    <p><strong>@lang('site.already_assigned_template') :</strong>{{@$already_assigned_temp->getTool->name}}</p>


                    <div class="buyer_table">
                        <div class="table-responsive">
                            @if(sizeof(@$certificates)>0)
                            <div class="table">
                                <div class="one_row1 hidden-sm-down only_shawo">
                                    <div class="cell1 tab_head_sheet">@lang('site.title')</div>
                                    <div class="cell1 tab_head_sheet">@lang('site.action')</div>
                                </div>
                                <!--row 1-->
                                @foreach(@$certificates as $row)
                                    <div class="one_row1 small_screen31">
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.title')</span>
                                            <p class="add_ttrr">
                                                {{@$row->name}}
                                                @if(@$already_assigned_temp->tool_id == $row->id)
                                                    <br>
                                                    <small class="text-secondary">(@lang('site.already_assigned_template'))</small>
                                                @endif
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                            @if(@$already_assigned_temp->getTool->id != $row->id)
                                                <a href="{{ route('to.assign.product.certificate.template',[@$row->id,@$product->id]) }}" id="assign-{{@$row->id}}" class="rjct">@lang('site.assign_template')</a>
                                            
                                            @endif
                                            <a href="javascript:;" class="preview_btn acpt" data-obj="{{@$row}}" data-toggle="modal" data-target="#exampleModal">@lang('site.preview')</a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            @else
                                <div class="one_row small_screen31">
                                    <center><span><h3 class="error">OOPS! @lang('client_site.no_certificates_found').</h3></span></center>
                                </div>
                            @endif
                        </div>   
                    </div>
                </div>
                 
            </div>
            
        </div>
    </div>
</section>

<div class="modal myCertModal fade big_modal_rm" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {{-- <div class="template-element template-element-1" style="display: none;">
            @include('modules.prof_certificate_temp.template_1')
        </div>
        <div class="template-element template-element-2" style="display: none;">
            @include('modules.prof_certificate_temp.template_2')
        </div>
        <div class="template-element template-element-3" style="display: none;">
            @include('modules.prof_certificate_temp.template_3')
        </div>
        <div class="template-element template-element-4" style="display: none;">
            @include('modules.prof_certificate_temp.template_4')
        </div> --}}
      </div>
      <div class="modal-footer ressdbyjj">
        <a href="javascript:;" class="btn btn-secondary" data-dismiss="modal">@lang('client_site.close')</a>
        <a href="javascript:;" class="btn btn-primary" id="assign_btn" data-id="">@lang('site.assign_template')</a>
      </div>
    </div>
  </div>
</div>
@endsection
@section('footer')
<script>
function confirmStatusChange(status){
    var question = status == 'A' ? "@lang('site.inactivate_product')" : "@lang('site.activate_product')";
    return confirm(question);
}
function copyText(text){
    var input = document.createElement('input');
    input.setAttribute('value', text);
    document.body.appendChild(input);
    input.select();
    var result = document.execCommand('copy');
    document.body.removeChild(input);
    toastr.success("Link Copied");
    return result;
}
$('.preview_btn').click(function(){
    var obj = $(this).data('obj');
    console.log(obj.name);
    var html = "";
    $('.template-element').hide();
    $('.template-element-'+obj.template_number).show();
    // $('.cert_head').text(obj.title);
    // $('.above_stu_name').text(obj.above_stu_name);
    // $('.below_stu_name').text(obj.below_stu_name);
    // $('.above_course').text(obj.above_course);
    // $('.below_course').text(obj.below_course);
    // $('.note .new_cont_1').text(obj.description);
    // $('.note .new_cont').text(obj.description);
    // $('.issued_on').text(obj.issued_on);
    // $('.expires_on').text(obj.expires_on);
    // $('.cert_id').text(obj.certificate_id);
    var html1 = `@if(@Auth::user()->signature)
                <img src="{{ url('storage/app/public/uploads/signature/'.@Auth::user()->signature) }}" height="50px"/>
            @else
                <p>{{ @Auth::user()->nick_name ?? @Auth::user()->name }}</p>
            @endif
            <p class="prof_name">{{ @Auth::user()->nick_name ?? @Auth::user()->name }}</p>`;
    $('.modal-body').html(obj.content);
    if(obj.background_filename != null) {
        background = "url('{{url('/')}}/storage/app/public/uploads/certificate_template/" + obj.background_filename + "') no-repeat";
    } else {
        background = "url('{{url('/')}}/public/frontend/images/template" + obj.template_number + ".jpg') no-repeat";
    }
    $('.modal-body .cert_template').css('background', background);
    $('.modal-body .cert_template').css('background-size', 'cover');
    $('.modal-body .cert_template').css('background-position', 'center');
    $('.modal-body .cert_template .below_course_div').html(html1);

    $('.primary_color').css('color', obj.primary_color);
    $('.secondary_color').css('color', obj.secondary_color);
    $('#assign_btn').attr('data-id', obj.id);
});

$('#assign_btn').click(function(){
    var id = $(this).data('id');
    console.log($('#assign-'+id)[0]);
    var reqData = new FormData();
    reqData.append("jsonrpc", "2.0");
    reqData.append("_token", '{{csrf_token()}}');
    reqData.append('content', $('.modal-body').html());
    window.location.href = $('#assign-'+id).attr('href');

    // $.ajax({
    //     url: "{{ route('update.certificate.signature') }}",
    //     method: 'post',
    //     async: false,
    //     cache: false,
    //     contentType: false,
    //     processData: false,
    //     data: reqData,
    //     success: function(response){
    //         if(response.status == "success"){
    //             window.location.href = $('#assign-'+id).attr('href');
    //         } else {
    //             swal("internal server error.",{icon:"warning"});
    //         }
    //     }, error: function(error) {
    //         swal("internal server error.",{icon:"warning"});
    //     }
    // });
});

</script>
@include('includes.footer')

@endsection
