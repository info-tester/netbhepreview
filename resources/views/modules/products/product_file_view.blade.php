@extends('layouts.app')
@section('title')
@lang('site.welcome_to_dashboard')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('client_site.my_products')</h2>

        <div class="bokcntnt-bdy">
            @php
            $user = Auth::guard('web')->user();
            $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0) <center>
                <p class="alert alert-info">@lang('client_site.please_complete_your_profile')<a
                        href="{{ route('professional_qualification') }}">@lang('client_site.click_here')</a></p>
                </center>
                @endif
                <div class="mobile_filter">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                    <p>@lang('client_site.menu')</p>
                </div>
                @include('includes.professional_sidebar')
                <div class="dshbrd-rghtcntn">
                    <div class="row">
                        <div class="col-lg-9">
                            <h3>@lang('site.product_files') - {{$product->title}}</h3>
                        </div>
                        <div class="col lg-3">
                            <a class="btn btn-success" href="{{route('product.file.upload.view',['id'=>$product->id])}}">+
                                Upload File</a>
                            <a class="btn btn-primary mt-1" href="{{route('store.product',@$product->id)}}">+
                                @lang('site.edit_product')</a>
                        </div>
                    </div>
                    <div class="buyer_table">
                        <div class="table-responsive">
                            @if(sizeof(@$fileList)>0)
                            <div class="table">
                                <div class="one_row1 hidden-sm-down only_shawo">
                                    {{-- <div class="cell1 tab_head_sheet">@lang('site.image')</div> --}}
                                    <div class="cell1 tab_head_sheet">Caption</div>
                                    {{-- <div class="cell1 tab_head_sheet">@lang('site.price')</div>
                                    <div class="cell1 tab_head_sheet">@lang('site.category')</div> --}}
                                    {{-- <div class="cell1 tab_head_sheet">posted on</div> --}}
                                    {{-- <div class="cell1 tab_head_sheet">@lang('site.status')</div> --}}
                                    <div class="cell1 tab_head_sheet" style="width:40px">@lang('site.action')</div>
                                </div>
                                <!--row 1-->
                                @php
                                $i=1;
                                @endphp
                                @foreach(@$fileList as $productfile)
                                <div class="one_row1 small_screen31">
                                    <div class="cell1 tab_head_sheet_1">
                                        <span class="W55_1">@lang('site.title')</span>
                                        <p class="add_ttrr">
                                            {{@$productfile->caption}}
                                        </p>
                                    </div>
                                    <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                    @if(@$productfile->file_type == 'file')
                                    <a href="{{ URL::to('storage/app/public/uploads/product_files').'/'.@$productfile->product_file }}" class="acpt" title="Download" download="{{@$productfile->caption}}"><i class="fa fa-download" aria-hidden="true"></i></a>
                                    @elseif(@$productfile->file_type == 'link')
                                    <a target="_blank" href="{{@$productfile->product_file}}" class="acpt" title="Go to link"><i class="fa fa-external-link" aria-hidden="true"></i></a>
                                    @endif
                                        <a href="{{route('product.file.delete',@$productfile->id)}}" class="rjct"
                                            onclick="return confirm('@lang('site.do_delete_product')');">@lang('site.delete')</a>
                                    </div>
                                </div>
                                @php
                                $i++;
                                @endphp
                                @endforeach

                            </div>
                            @else
                            <div class="one_row small_screen31">
                                <center><span>
                                        <h3 class="error">OOPS! File Not Found.</h3>
                                    </span></center>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')

@endsection

