@extends('layouts.app')
@section('title')
  @lang('site.get_the_right_advice')
@endsection

@section('style')
	@include('includes.style')
@endsection

@section('scripts')
	@include('includes.scripts')
@endsection

@section('header')
	@include('includes.header')
@endsection


@section('content')
<section class="category">
    <div class="container">
        <div class="row">
            <div class="page-h2">
                <h2>{{ $meetNeeds->title ?? __('client_site.browse_by_category') }}</h2>
                <p>
                    {!! $meetNeeds->description ?? '' !!}
                </p>
            </div>
            <div class="all-categories">
                <div class="row">
                    @foreach(@$subcategory as $ct)
                        <div class="col-lg-3 col-md-6 col-sm-12">
                            <div class="categori-box">
                                <a href="{{ route('all.product.search',['slug'=>@$ct->slug]) }}">
                                    @if(@$ct->image)
                                        <img src="{{URL::to('storage/app/public/product_category').'/'.@$ct->image}}" alt="">
                                    @else
                                    <img src="{{URL::to('public/frontend/images').'/cat1.png'}}" alt="">
                                    @endif
                                    <p>{{ @$ct->name }}</p>
                                </a>
                            </div>
                        </div>
                    @endforeach


                    <!-- <div class="col-lg-12 text-center">
                        <a class="view-all" href="{{ route('browse.categories') }}">@lang('client_site.view_all')<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    </div> -->

                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
	@include('includes.footer')
        <script src="{{ URL::to('public/frontend/js/jquery.com_ui_1.11.4_jquery-ui.js') }}"></script>
        <script>
            $(function() {
                $( "#slider-range" ).slider({
                    range: true,
                    min: {{ @$minprice->rate_price }},
                    max: {{ @$maxprice->rate_price }},
                    values: [ {{ @$key['min_price'] ? @$key['min_price']: @$minprice->rate_price }}, {{ @$key['max_price'] ? $key['max_price']: @$maxprice->rate_price }} ],
                    slide: function( event, ui ) {
                        $( "#amount" ).val( "R$" + ui.values[ 0 ] + " - R$" + ui.values[ 1 ] );
                        $('#min_price').val(ui.values[ 0 ]);
                        $('#max_price').val(ui.values[ 1 ]);
                    }
                });
                $( "#amount" ).val( "R$" + $( "#slider-range" ).slider( "values", 0 ) +
                    " - R$" + $( "#slider-range" ).slider( "values", 1 ) );

                        $('#min_price').val($( "#slider-range" ).slider( "values", 0 ));
                        $('#max_price').val($( "#slider-range" ).slider( "values", 1 ));
            });

            $('.frm1').click(function(){
                $('#myForm').submit();
            });

            $('#category').change(function(){
                if($('#category').val()==""){
                    $('#subcategory').html("");
                }else{
                    var reqData = {
                      'jsonrpc' : '2.0',
                      '_token' : '{{csrf_token()}}',
                      'params' : {
                            'cat' : $('#category').val()
                        }
                    };
                    $.ajax({
                        url: "{{ route('fetch.subcat') }}",
                        method: 'post',
                        dataType: 'json',
                        data: reqData,
                        success: function(response){
                            if(response.status==1) {
                                var i=0, html="";
                                html = '<option value="">@lang('client_site.select_option')</option>';
                                for(;i<response.result.length;i++){
                                    html+='<option value='+response.result[i].id+'>'+response.result[i].name+'</option>';
                                }
                                $('#subcategory').html(html);
                                }
                        }, error: function(error) {
                            console.error(error);
                        }
                    });
                }
            });

            $('#sort_by').change(function(){
                $('form').submit();
            });

            $('.ssnbtnslg').click(function(){
                if($(this).data('slug')!=""){
                    localStorage.removeItem('bookSlug');
                    localStorage.setItem('bookSlug', $(this).data('slug'));
                    location.href="{{ route('login') }}"
                }
            });
        </script>
@endsection
