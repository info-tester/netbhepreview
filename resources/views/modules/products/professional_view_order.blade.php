@extends('layouts.app')
@section('title')
    @lang('site.welcome_to_dashboard')
@endsection
@section('style')
@include('includes.style')
<style>
    .main-boxes{
        font-family: 'Poppins', sans-serif;
        color: #252424;
        font-size: 15px;
        width: 100%;
        float: left;
        padding: 5px 0;
        font-weight: 400;
        margin: 0;
    }
    .main-boxes strong{
        font-weight: 500;
        color: #1781d2;
        width: 122px;
        float: left;
    }
    .main-boxes .files {
        display: inline-block;
        float: left;
        width: auto;
        width: calc(100% - 124px);
    }
    .main-boxes .files a{
        margin-right:4px;
    }
    .w-100 {
        width: 100% !important;
        display: block;
        overflow: hidden;
    }
    @media(max-width:767px) {
        .main-boxes .files {
            width: 100% !important;
        }
    }
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')
<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('client_site.view_order')</h2>

        <div class="bokcntnt-bdy">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile')<a href="{{ route('professional_qualification') }}">@lang('client_site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <a href="{{route('professional.orders')}}" class="btn btn-primary pull-right mb-1">@lang('site.back')</a>
                <div class="col-md-12 view-page">
                    <p><strong>@lang('client_site.ordered_by') </strong>{{ @$order->userDetails->nick_name ? @$order->userDetails->nick_name : @$order->userDetails->name }}</p>

                    <p><strong>@lang('site.order_id'):</strong>{{@$order->token_no}}</p>
                    <p><strong>@lang('client_site.ordered_on') :</strong>
                        @php $month=[ "0", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ] @endphp
                        {{date('d',strtotime($order->order_date))}}
                        {{$month[(int)date('m',strtotime($order->order_date))]}}
                        {{date('Y',strtotime($order->order_date))}}
                    </p>
                    <p><strong>@lang('client_site.product_title') :</strong> {{ @$prod_order_det->product->title }}</p>
                    <p><strong>@lang('site.product_category') :</strong> {{@$prod_order_det->product->category->category_name}} > {{@$prod_order_det->product->subcategory->name}}</p>
                    <!-- <p><strong>@lang('client_site.category') :</strong> {{@$order->productCategoryDetails ? @$order->productCategoryDetails->category_name: 'N.A' }}</p> -->

                    <p><strong>@lang('client_site.amount') :</strong>R$ {{ @$prod_order_det->amount }}</p>
                    <!-- @if(@$order->product_file || @$order->url)
                        <p><strong>@lang('client_site.status') :</strong> @lang('client_site.complete')</p>
                        @endif
                     -->
                    @if(@$order->payment_status == 'I')
                        <p><strong>@lang('client_site.status') :</strong> @lang('client_site.initiated')</p>
                    @elseif(@$order->payment_status == 'PR')
                        <p><strong>@lang('client_site.status') :</strong> @lang('client_site.processing_payment')</p>
                    @elseif(@$order->payment_status == 'F')
                        <p><strong>@lang('client_site.status') :</strong> @lang('client_site.failed')</p>
                    @elseif(@$order->payment_status == 'P')
                        <p><strong>@lang('client_site.status') :</strong> @lang('client_site.paid')</p>
                    @endif
                    @if(@$order->payment_type == 'C')
                    <p><strong>@lang('site.payment_type') :</strong>@lang('site.card')</p>
                    @elseif(@$order->payment_type == 'BA')
                    <p><strong>@lang('site.payment_type') :</strong>@lang('site.payment_method_bank')</p>
                    @elseif(@$order->payment_type == 'S')
                    <p><strong>@lang('site.payment_type') :</strong>Stripe</p>
                    @elseif(@$order->payment_type == 'P')
                    <p><strong>@lang('site.payment_type') :</strong>Paypal</p>
                    @elseif(@$order->sub_total==0)
                    <p><strong>@lang('site.payment_type') :</strong>{{__('site.free_product_purchess')}}</p>
                    @elseif(@$order->wallet == @$order->sub_total)
                    <p><strong>@lang('site.payment_type') :</strong>{{__('site.wallet')}}</p>
                    @endif
                    @if($prod_order_det->order_status =='A' && @$prod_order_det->cancel_request =='N' && $order->payment_status == 'P')
                    <p><strong>@lang('site.status') :</strong>{{__('site.product_active')}}</p>
                    @elseif($prod_order_det->order_status =='C' && $order->payment_status == 'P' &&  $prod_order_det->is_cancel_responce=='N')
                    <p><strong>@lang('site.status') :</strong>{{__('site.product_cancel')}}</p>
                    <p><strong>{{__('site.cancellation_reason')}} :</strong> {{@$prod_order_det->cancellation_reason}}</p>
                    @elseif($prod_order_det->order_status =='A' && @$prod_order_det->cancel_request =='Y' && $order->payment_status == 'P' && $prod_order_det->is_cancel_responce=='N')
                    <p><strong>@lang('site.status') :</strong>{{__('site.product_request')}}</p>
                    <p><strong>{{__('site.cancellation_reason')}} :</strong> {{@$prod_order_det->cancellation_reason}}</p>
                    @elseif($prod_order_det->order_status =='C' && @$prod_order_det->cancel_request =='Y' && $order->payment_status == 'P' &&  $prod_order_det->is_cancel_responce=='A')
                    <p><strong>@lang('site.status') :</strong>{{__('site.product_order_cancelation_accept')}}</p>
                    <p><strong>{{__('site.cancellation_reason')}} :</strong> {{@$prod_order_det->cancellation_reason}}</p>
                    @elseif($prod_order_det->order_status =='A' && @$prod_order_det->cancel_request =='Y' && $order->payment_status == 'P' &&  $prod_order_det->is_cancel_responce=='R')
                    <p><strong>@lang('site.status') :</strong>{{__('site.product_order_cancelation_reject')}}</p>
                    <p><strong>{{__('site.cancellation_reason')}} :</strong> {{@$prod_order_det->cancellation_reason}}</p>
                    @else
                    <p><strong>@lang('site.status') :</strong>{{__('site.product_inprogress')}}</p>
                    @endif
                    <!-- <p><strong>@lang('site.no_of_installment') :</strong>{{@$order->no_of_installment}}</p> -->
                    <!-- if product in not uploaded, provision to upload product-->
                    <div class="buyer_table">

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
<script>
    $(document).ready(function(){
        $('#upload_product_files').validate({
            submitHandler: function(form) {
                var flag = 0;
                $("#product_file_error").html("");
                $('#caption').parent().find('label').remove();
                if($('#product_link').val() == '' && $('#product_file').val() == ''){
                    $("#product_file_error").html("Please add a file or a link.");
                    flag = 1;
                }
                if($('#caption').val() == ''){
                    $('#caption').addClass('error');
                    $('#caption').parent().append('<label class="error">This field is required</label>');
                    flag = 1;
                }
                // $('.content').each(function(content){
                //     if($(this).find('.file').val() == '' && $(this).find('.link').val() == ''){
                //         $(this).append('<label class="error">Please add a file or a link.</label>');
                //         flag = 1;
                //     }
                // });
                if($('#product_file')[0].files.length){
                    console.log('files present');
                    var fullPath = $('#product_file').val();
                    var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
                    var filename = fullPath.substring(startIndex);
                    if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                        filename = filename.substring(1);
                    }
                    var fileExt = filename.split('.').pop();

                    if(fileExt == "exe" || fileExt == "bat" || fileExt == "img") {
                        $("#product_file_error").html("Please enter a valid file");
                        flag = 1;
                    }
                    if($('#product_file')[0].files[0].size > 20971520) {
                        $("#product_file_error").html("Please enter file less than 20 MB.");
                        flag = 1;
                    }
                }
                if(flag == 1)
                    return false;
                else
                    form.submit();
            },
        });
    });
</script>
<script>
    $('.allow_download').on('click',function(){
            var product_id = $(this).attr('data-product');
            var user_id = $(this).attr('data-user');
            var token_no = $(this).attr('data-token');
            var order_id = $(this).attr('data-order');


        if(confirm("@lang('site.allow_certificate')")){
            var reqData = {
            'jsonrpc' : '2.0',
            '_token' : '{{csrf_token()}}',
            'params' : {
                  'product_id' :  product_id,
                  'user_id'    : user_id,
                  'token_no'   : token_no ,
                  'order_id'   : order_id
               }
            };

            $.ajax({
            url: "{{ route('allow.download.certificate') }}",
            method: 'post',
            dataType: 'json',
            data: reqData,
            success: function(response){
                if(response.status == 1){
                    toastr.success("@lang('site.send_certificate_successfully')");
                    location.reload();
                }
                if(response.status == 0){
                    toastr.info('Algo deu errado');
                }
                if(response.status == 2){
                    toastr.info('Atribua um modelo de certificado antes de enviar o certificado');
                }
            }, error: function(error) {
                console.error(error);
            }
        });



        }
    });
</script>
@endsection
