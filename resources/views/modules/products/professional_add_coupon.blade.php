@extends('layouts.app')
@section('title')
    @lang('site.welcome_to_dashboard')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')
<style>
     .tooltip {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted #ccc;
        color: #006080;
        opacity:1 !important;
    }
    .tooltip .tooltiptext {
        visibility: hidden;
        position: absolute;
        width: 250px;
        background-color: #555;
        color: #fff;
        text-align: center;
        padding: 5px;
        border-radius: 6px;
        z-index: 1;
        opacity: 0;
        transition: opacity 0.3s;
        margin-left:10px
    }
    .tooltip-right {
        top: 0px;
        left: 125%;
    }
    .tooltip-right::after {
        content: "";
        position: absolute;
        top: 10%;
        right: 100%;
        margin-top: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: transparent #555 transparent transparent;
    }
    .tooltip:hover .tooltiptext {
        visibility: visible;
        opacity: 1;
    }
    .tooltip_img i{
        margin-left: 10px !important;
        margin-top: 20px !important;
    }
    .tooltip_img .tooltiptext{
        margin-top: 15px !important;
    }
</style>
<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@if(@$coupon) @lang('site.edit_product_coupon') @else @lang('site.add_product_coupon') @endif</h2>

        <div class="bokcntnt-bdy">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile')<a href="{{ route('professional_qualification') }}">@lang('client_site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="from-field">
                    <div class="weicome">
                        <h3>@if(@$coupon) @lang('site.edit_product_coupon') @else @lang('site.add_product_coupon') @endif</h3>
                    </div>
                    <div class="dash_main">
                        <form action="{{ route('store.coupon') }}" method="post" id="coupon_save" enctype="multipart/form-data">
                            @csrf
                            <input type="text" name="id" value="{{@$coupon->id}}" hidden>
                            <div class="form_body">

                                <div class="row">

                                    <div class="col-lg-12 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">
                                                @lang('site.select')
                                                <small class="tooltip">
                                                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                    <span class="tooltiptext tooltip-right">@lang('site.coupon_select_product_tooltip')</span>
                                                </small>
                                            </label>
                                            <!-- <input type="text" placeholder="Enter product title" class="personal-type" name="title"> -->
                                            <select class="personal-type" name="product">
                                                <option value="">@lang('site.select')</option>
                                                @if(count(@$products)>0)
                                                    @foreach($products as $product)
                                                        <option value="{{$product->id}}" @if(@$coupon->couponToProduct->productDetails->id == $product->id) selected @endif >{{$product->title}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">
                                                @lang('site.coupon_code')
                                                <small class="tooltip">
                                                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                    <span class="tooltiptext tooltip-right">@lang('site.coupon_code_tooltip')</span>
                                                </small>
                                            </label>
                                            <input type="text" placeholder="Adicionar codigo do cupom" class="personal-type" name="code" id="code" value="{{@$coupon->coupon_code}}">
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">
                                                @lang('site.discount')
                                                <small class="tooltip">
                                                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                    <span class="tooltiptext tooltip-right">@lang('site.coupon_discount_tooltip')</span>
                                                </small>
                                            </label>
                                            <input type="text" placeholder="Adicionar desconto" class="personal-type" name="discount" id="discount" value="{{@$coupon->discount}}">
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">
                                                @lang('site.Start_date')
                                                <small class="tooltip">
                                                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                    <span class="tooltiptext tooltip-right">@lang('site.coupon_valid_from_tooltip')</span>
                                                </small>
                                            </label>
                                            <input type="text" id="datepicker_start" class="personal-type" name="start_date" placeholder="Selecione sua data de início" required value="{{@$coupon->start_date}}">
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">
                                                @lang('site.end_date')
                                                <small class="tooltip">
                                                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                    <span class="tooltiptext tooltip-right">@lang('site.coupon_valid_till_tooltip')</span>
                                                </small>
                                            </label>
                                            <input type="text" id="datepicker_end" class="personal-type" name="end_date" placeholder="Selecione sua data de término" required value="{{@$coupon->exp_date}}">
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <button type="submit" id="coupon_save_btn" class="login_submitt">@lang('site.submit')</button>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
<script>
    $(function() {
        $("#datepicker_start, #datepicker_end").datepicker({dateFormat: "yy-mm-dd",
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho',
        'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],

        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
        dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
            defaultDate: new Date(),
            // showButtonPanel: true,
            showMonthAfterYear: true,
            showWeek: true,
            showAnim: "drop",
            constrainInput: true,
            yearRange: "-90:",
            minDate:new Date(),
            onClose: function( selectedDate ) {
                $( "#datepicker_end").datepicker( "option", "minDate", selectedDate );
            }
        });
    });
//on submit
    $(document).ready(function(){
        $('#discount').keyup(function(){
            $("#discount").inputFilter(function(value) {
            return /^\d*$/.test(value);
            });
        });
        jQuery.validator.addMethod("lessthanequal100", function(value, element) {
			return this.optional(element) || value <= 100;
		});
        $('#coupon_save').validate({
            rules: {
                product: {required:true},
                discount: {required:true, lessthanequal100:true},
                code: {required:true, alphanumeric:true},
            },
            messages: {
                discount: {
					lessthanequal100 : "Please enter a number less than or equal to 100."
                },
                code: {
                    alphanumeric: "Adicionar letras e números."
                },
            }
        });        
    });
</script>
@endsection