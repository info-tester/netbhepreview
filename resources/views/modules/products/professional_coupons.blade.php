@extends('layouts.app')
@section('title')
    @lang('site.welcome_to_dashboard')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')
<style>
     .tooltip {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted #ccc;
        color: #006080;
        opacity:1 !important;
    }
    .tooltip .tooltiptext {
        visibility: hidden;
        position: absolute;
        width: 250px;
        background-color: #555;
        color: #fff;
        text-align: center;
        padding: 5px;
        border-radius: 6px;
        z-index: 1;
        opacity: 0;
        transition: opacity 0.3s;
        margin-left:10px
    }
    .tooltip-right {
        top: 0px;
        left: 125%;
    }
    .tooltip-right::after {
        content: "";
        position: absolute;
        top: 10%;
        right: 100%;
        margin-top: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: transparent #555 transparent transparent;
    }
    .tooltip:hover .tooltiptext {
        visibility: visible;
        opacity: 1;
    }
    .tooltip_img i{
        margin-left: 10px !important;
        margin-top: 20px !important;
    }
    .tooltip_img .tooltiptext{
        margin-top: 15px !important;
    }
</style>

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.my_coupons')</h2>

        <div class="bokcntnt-bdy">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile')<a href="{{ route('professional_qualification') }}">@lang('client_site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <form name="myForm" id="myForm" method="post" action="{{route('search.product')}}">
                    @csrf
                    <div class="from-field">
                        <div class="row">
                            <div class="col-lg-7">
                                <h3>@lang('site.my_coupons')

                                    <small class="tooltip">
                                                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                    <span class="tooltiptext tooltip-right">@lang('site.tooltip_coupon')</span>
                                    </small>
                                </h3>
                                
                            </div>
                            <div class="col-lg-2"></div>
                            @if(auth()->user()->sell != 'VS')
                            <div class="col lg-3">
                                <a class="btn btn-success" href="{{route('professional.add.coupon')}}">+ @lang('site.add_coupon')</a>
                            </div>
                            @endif
                        </div>
                    </div>
                </form>
                <div class="buyer_table">
                    <div class="table-responsive">
                        @if(sizeof(@$coupons)>0)
                        <div class="table">
                            <div class="one_row1 hidden-sm-down only_shawo">
                                <div class="cell1 tab_head_sheet">@lang('site.coupon_code')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.product_title')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.discount')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.Start_date')</div>
                                {{-- <div class="cell1 tab_head_sheet">posted on</div> --}}
                                <div class="cell1 tab_head_sheet">@lang('site.end_date')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.action')</div>
                            </div>
                            <!--row 1-->
                                @php
                                    $i=1;
                                @endphp
                                @foreach(@$coupons as $coupon)
                                    <div class="one_row1 small_screen31">
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.coupon_code')</span>
                                            <p class="add_ttrr">{{@$coupon->coupon_code}}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.product_title')</span>
                                            <p class="add_ttrr">{{@$coupon->couponToProduct->productDetails->title}}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.discount')</span>
                                            <p class="add_ttrr">{{@$coupon->discount}}%</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.Start_date')</span>
                                            <p class="add_ttrr">
                                                @php $month=[ "0", 'Jan', 'Fev', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Set', 'Out', 'Nov', 'Dez' ] @endphp
                                                {{date('d',strtotime(@$coupon->start_date))}}
                                                {{$month[(int)date('m',strtotime(@$coupon->start_date))]}}
                                                {{date('Y',strtotime(@$coupon->start_date))}}
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.end_date')</span>
                                            <p class="add_ttrr">
                                                @php $month=[ "0", 'Jan', 'Fev', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Set', 'Out', 'Nov', 'Dez' ] @endphp
                                                {{date('d',strtotime(@$coupon->exp_date))}}
                                                {{$month[(int)date('m',strtotime(@$coupon->exp_date))]}}
                                                {{date('Y',strtotime(@$coupon->exp_date))}}
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                            <a href="{{route('professional.add.coupon',@$coupon->id)}}" class="rjct">@lang('site.edit')</a>
                                            <a href="{{route('delete.coupon',@$coupon->id)}}" class="acpt">@lang('site.delete')</a>
                                        </div>
                                    </div>
                                    @php
                                        $i++;
                                    @endphp
                                @endforeach

                        </div>
                        @else
                            <div class="one_row small_screen31">
                                <center><span><h3 class="error">@lang('site.oops!_not_found').</h3></span></center>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
<script>
function confirmStatusChange(status){
    var question = status == 'A' ? "@lang('site.inactivate_product')" : "@lang('site.activate_product')";
    return confirm(question);
}
</script>
@include('includes.footer')

@endsection
