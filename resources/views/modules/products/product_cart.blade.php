@extends('layouts.app')
@section('title', 'Cart')
@section('style')
@include('includes.style')


@endsection
@section('scripts')
@include('includes.scripts')
<script type="text/javascript">
    $(document).ready(function() {
    var count = parseInt("{{count(@$similarProducts)}}");
    var loop = true;
    console.log("COUNT: "+count);
    if(count<=4) loop = false;
    else loop = true;
    $("#owl-demo-7").owlCarousel({
        margin: 25,
        nav: true,
        responsive: {
            0: {
                items: 1,
                loop: 1,
            },
            768: {
                items: 4,
                loop: 2,
            },
            1078: {
                items: 4,
                loop: 3,
            },
            1200: {
                items: 4,
                loop: loop,
            },
        }
    });
});
</script>
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')
<section class="cart_sec">
    <div class="container">
        <div class="cart_inr">
            <h1>@lang('client_site.cart')</h1>
            <div class="cart_left">
                <div class="table_sec">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">@lang('client_site.products')</th>
                                    <th scope="col">@lang('site.price')</th>
                                    <th scope="col">@lang('site.total_amount')</th>
                                    <th scope="col">@lang('client_site.action')</th>

                                </tr>
                            </thead>
                            <tbody>
                                @if(@$cartData->isNotEmpty())
                                @foreach (@$cartData as $cart)
                                <tr>
                                    <td>
                                        <div class="cart_media">
                                            <div class="media">
                                                <em><img src="{{URL::to('storage/app/public/uploads/product_cover_images/'.@$cart->product->cover_image)}}" alt=""></em>
                                                <div class="media-body">
                                                    <h4>{{@$cart->product->title}}</h4>
                                                    <p><?= substr(strip_tags(@$cart->product->description),0,15) ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        @if(@$cart->discount_amount>0)
                                        <span class="dis_off">
                                            <del>R$ {{@$cart->discount_amount}}</del>
                                            <?php  $a = ((@$cart->discount_amount - @$cart->amount)/@$cart->discount_amount)*100;
                                            echo ceil($a)
                                            ?>% off
                                        </span>
                                        <br>
                                        @endif
                                        R$ {{@$cart->amount}}
                                    </td>
                                    <td>R$ {{@$cart->amount}}</td>
                                    <td><a href="{{route('product.cart.remove',['id'=>$cart->product->id])}}" class="act_cart"><i class="fa fa-trash"></i></a></td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="4" style="text-align: center;">@lang('client_site.no_product_in_cart')</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="cart_right">
                <div class="cart_right_bx">
                    <ul>
                        <li><span>@lang('client_site.no_of_products')</span> <b>{{@$cartData->count()}}</b></li>
                        <li><span>@lang('site.total_amount')</span> <b>R$ {{@$cartData->sum('amount')}}</b></li>
                    </ul>
                    @if(@$cartData->count()>0)
                    <a href="{{route('product.order.store')}}" class="solid_btn">@lang('client_site.check_out')</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
<section class="product-area adjusttr_rmm01">
    <div class="container">
        <div class="row">
            <div class="all-product">
                <div id="owl-demo-7" class="owl-carousel owl-theme">
                    @foreach ($similarProducts as $pro)
                    <div class="item">
                        <div class="product-box">
                            <div class="product-cover-image image_resize">
                                <a href="{{route('product.details',@$pro->slug)}}">
                                    <img src="{{ @$pro->cover_image ? URL::to('storage/app/public/uploads/product_cover_images').'/'.@$pro->cover_image: URL::to('public/frontend/images/no_img.png') }}" alt="">
                                </a>
                            </div>
                            @if(in_array(@$pro->id , $wishList))
                            <span class="rhhertt" ><a href="javascript:;" class="remove_from_wishlist" data-product="{{$pro->id}}" title="Remove from save for later"><i class="fa fa-heart" aria-hidden="true"></i></a></span>
                            @else
                            <span class="hhertt"><a href="javascript:;" class="add_to_wishlist" data-product="{{$pro->id}}" title="Save for Later "><i class="fa fa-heart" aria-hidden="true"></i></a></span>
                            @endif
                            <div class="product-dtls">
                                <div class="product-intro">
                                    <a href="{{route('product.details',@$pro->slug)}}">
                                        <h3>
                                            @if(strlen(@$pro->title) < 50)
                                            {{ @$pro->title }}
                                            @else
                                            {{substr(@$pro->title, 0, 50 ) . '...'}}
                                            @endif
                                        </h3>
                                    </a>
                                </div>
                                    <div class="product-more more_det">
                                        <p class="pr-desc" ><?= substr(strip_tags(@$pro->description),0,66)?>...</p>
                                    </div>
                                <div class="price-and-more">
                                            <div class="price-lefts">
                                                <span>@lang('client_site.fees')</span>
                                                @if(@$pro->discounted_price > 0)
                                                <p><del>R${{@$pro->price}}</del>&nbsp;
                                                    {{-- {{ ((@$pro->price-@$pro->discounted_price)/@$pro->price)*100 }}% de desconto --}}

                                                    <?php  $a = ((@$pro->price-@$pro->discounted_price)/@$pro->price)*100;
                                                echo ceil($a)
                                                ?>% de desconto
                                                </p>
                                                <p style="font-size: 1rem;">R${{@$pro->discounted_price}}</p>
                                                @else
                                                <p style="font-size: 1rem;">R${{@$pro->price}}</p>
                                                @endif
                                                {{-- <p>R$ {{@$pro->price}}</p> --}}
                                            </div>



                                    @if(in_array(@$pro->id , $orderedProducts))
                                        <p class="text-secondary" style="font-size:1rem; float:right;">@lang('client_site.already_ordered')</p>
                                    @else
                                        @if(date('Y-m-d') >= date('Y-m-d', strtotime(@$pro->purchase_start_date)))
                                            @if(@$pro->purchase_end_date && date('Y-m-d') <= date('Y-m-d', strtotime(@$pro->purchase_end_date)))
                                                <p class="text-secondary" style="font-size:1rem; float:right;">@lang('client_site.sale_over')</p>
                                            @else
                                                {{-- <a href="javascript:void(0);" class="guide-btn pull-right pull-right-1 addToCart cartClass{{$pro->id}}" data-product="{{@$pro->id}}" @if(@in_array($pro->id, getAllCartProduct())) style="display: none" @endif>  @lang('client_site.add_to_cart')</a> --}}
                                                <a href="javascript:void(0);" class="product-btn pull-right pull-right-1 addToCart cartClass{{$pro->id}}" data-product="{{$pro->id}}">  @lang('client_site.add_to_cart')</a>
                                            @endif
                                        @else
                                            <!-- <p class="text-secondary" style="font-size:1rem; float:right;">Coming Soon</p> -->
                                            <p class="text-secondary" style="font-size:1rem; float:right;"><span style="font-size:0.7rem; text-align:center;">@lang('client_site.available_from')</span><br>{{date('jS F, Y', strtotime(@$product->purchase_start_date))}}</p>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
              </div>
            </div>
        </div>
    </div>
</section>

    @endsection
    @section('footer')
    @include('includes.footer')
    <script>
        $(document).ready(function(){
            $('body').on('click', '.addToCart', function() {
            // $('.addToCart').click(function(){
                var productId = $(this).data('product');
                console.log(productId)
                var reqData = {
                    'jsonrpc': '2.0',
                    '_token': '{{csrf_token()}}',
                    'params': {
                        productId: productId,
                    }
                };
                $.ajax({
                    url: '{{ route('product.add.to.cart') }}',
                    type: 'post',
                    dataType: 'json',
                    data: reqData,
                })
                .done(function(response) {
                    window.location.href = '{{route('product.cart')}} ';
                    console.log(response);
                    $('.cou_cart').text(response.result.cart.length);
                    $('.cartClass'+productId).css('display','none');
                    $('.GoToCart'+productId).css('display','block');
                    console.log($(this));
                })
                .fail(function(error) {
                    console.log("error", error);
                })
                .always(function() {
                    console.log("complete");
                })
            })
            $('.buyNow').click(function(){
                var productId = $(this).data('product');
                console.log(productId)
                var reqData = {
                    'jsonrpc': '2.0',
                    '_token': '{{csrf_token()}}',
                    'params': {
                        productId: productId,
                    }
                };
                $.ajax({
                    url: '{{ route('product.add.to.cart') }}',
			        type: 'post',
			        dataType: 'json',
			        data: reqData,
                })
                .done(function(response) {
                    window.location.href = '{{route('product.order.store')}} ';
                })
                .fail(function(error) {
                    console.log("error", error);
                })
                .always(function() {
                    console.log("complete");
                })
            });
            $('.add_to_wishlist').click(function(){
                var product_id =$(this).data('product');
                var reqData = {
                    'jsonrpc' : '2.0',
                    '_token' : '{{csrf_token()}}',
                    'params' : {
                        'product_id' :  product_id
                    }
                };
            $.ajax({
            url: "{{ route('add.to.wishlist') }}",
            method: 'post',
            dataType: 'json',
            data: reqData,
            success: function(response){
                if(response.status==1) {
                    toastr.success('@lang('client_site.successfully_added_to_wishlist')');
                    location.reload()
                }
                if(response.status==3){
                    localStorage.removeItem('slug');
                    localStorage.setItem('slug','{{ @$user->slug }}');
                    location.href = "{{ route('login') }}";
                }

                if(response.status==2){
                    toastr.info('@lang('client_site.you_have_already_added_to_your_wishlist')');
                }
            }, error: function(error) {
                console.error(error);
            }
        });

         })
         $('.remove_from_wishlist').click(function(){
            var product_id =$(this).data('product');
            var reqData = {
            'jsonrpc' : '2.0',
            '_token' : '{{csrf_token()}}',
            'params' : {
                  'product_id' :  product_id
               }
            };
            $.ajax({
            url: "{{ route('remove.from.wishlist') }}",
            method: 'post',
            dataType: 'json',
            data: reqData,
            success: function(response){
                if(response.status==1) {
                    toastr.success('@lang('client_site.successfully_removed_from_wishlist')');
                    location.reload();
                }
                if(response.status==0){
                  toastr.error('@lang('client_site.something_went_be_wrong')');
                }
                if(response.status==3){
                    localStorage.removeItem('slug');
                    localStorage.setItem('slug','{{ @$user->slug }}');
                    location.href = "{{ route('login') }}";
                }

                if(response.status==2){
                    toastr.info('@lang('client_site.add_to_wishlist_first_to_remove')');
                }
            }, error: function(error) {
                console.error(error);
            }
        });

         })
        })

    </script>
@endsection
