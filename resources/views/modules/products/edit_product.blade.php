@extends('layouts.app')
@section('title')
    @lang('site.welcome_to_dashboard')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
<style>
    .tooltip {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted #ccc;
        color: #006080;
        opacity: 1 !important;
    }

    .tooltip .tooltiptext {
        visibility: hidden;
        position: absolute;
        width: 250px;
        background-color: #555;
        color: #fff;
        text-align: center;
        padding: 5px;
        border-radius: 6px;
        z-index: 1;
        opacity: 0;
        transition: opacity 0.3s;
        margin-left: 10px
    }

    .tooltip-right {
        top: 0px;
        left: 125%;
    }

    .tooltip-right::after {
        content: "";
        position: absolute;
        top: 10%;
        right: 100%;
        margin-top: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: transparent #555 transparent transparent;
    }

    .tooltip:hover .tooltiptext {
        visibility: visible;
        opacity: 1;
    }
</style>
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.edit_product_details')</h2>

        <div class="bokcntnt-bdy">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile')<a href="{{ route('professional_qualification') }}">@lang('client_site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="from-field product_page_form">
                    <div class="weicome row">
                        <h3>@lang('site.edit_product_details')</h3>
                        <div class="product_dates">
                        @php
                            $months = ['0', 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
                        @endphp
                        Adicionado em {{@$product->created_at->format('j')}} de {{$months[(int)@$product->created_at->format('m')]}} de {{@$product->created_at->format('Y')}}
                        <br>
                        Última atualização {{@$product->updated_at->format('j')}} de {{$months[(int)@$product->created_at->format('m')]}} de {{@$product->created_at->format('Y')}}
                        </div>
                    </div>
                    <div class="dash_main">
                        <form action="{{ route('store.product') }}" method="post" id="product_edit" enctype="multipart/form-data">
                                    @csrf
                                <input type="text" name="ID" value="{{@$product->id}}" hidden>
                            <div class="form_body">

                                <div class="row">

                                    <div class="col-lg-12 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">@lang('site.product_title')</label>
                                            <input type="text" placeholder="@lang('client_site.enter_prod_title')" class="required personal-type" value="{{$product->title}}" name="title">
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">@lang('site.category')</label>
                                            <select class="form-control" name="category" id="category">
                                                <option value="">--@lang('client_site.select')--</option>
                                                @foreach($categories as $category)
                                                    <option value="{{$category->id}}" @if(@$product->category_id == $category->id) selected @endif>{{$category->category_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">@lang('site.subcategory')</label>
                                            <select class="form-control" name="subcategory" id="subcategory">
                                                <option value="">--@lang('client_site.select')--</option>
                                                @foreach($subcategories as $subcategory)
                                                    <option value="{{$subcategory->id}}" @if(@$product->subcategory_id == $subcategory->id) selected @endif>{{$subcategory->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">@lang('site.price')</label>
                                            <input type="text" placeholder="@lang('client_site.enter_prod_price')" class="required personal-type" value="{{@$product->price}}" name="price" id="price">
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">@lang('site.discounted_price')</label>
                                            <input type="text" placeholder="@lang('client_site.enter_prod_disc_price')" class="personal-type" value="{{@$product->discounted_price != '0.00' ? @$product->discounted_price : ''}}" name="discounted_price" id="discounted_price">
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">
                                                @lang('client_site.when_you_want_to_start') *
                                                <!-- <div class="tooltip">
                                                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                    <span class="tooltiptext tooltip-right">{{__('site.purchase_start_date_tooltip')}}</span>
                                                </div> -->
                                            </label>
                                            <label for="start_date_only">
                                                <input type="radio" id="start_date_only" class="required set_date_radio" name="set_date_radio" value="S" @if(@$product->sale_type == 'S') checked @endif >
                                                @lang('client_site.start_immediately')
                                            </label>
                                            &nbsp;&nbsp;
                                            <label for="set_end_date">
                                                <input type="radio" id="set_end_date" class="required set_date_radio" name="set_date_radio" value="E" @if(@$product->sale_type == 'E') checked @endif >
                                                @lang('client_site.set_end_date')
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">
                                                @lang('site.purchase_start_date')
                                                <div class="tooltip">
                                                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                    <span class="tooltiptext tooltip-right">{{__('site.purchase_start_date_tooltip')}}</span>
                                                </div>
                                            </label>
                                            <input type="text" id="datepicker_psd" class="required personal-type" value="{{@$product->purchase_start_date}}" name="purchase_start_date" placeholder="@lang('site.enter_purchase_start_date')" autocomplete="off" readonly>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-6 col-sm-12" id="end_date_div" @if(@$product->sale_type == 'S') style="display: none;" @endif >
                                        <div class="form-group">
                                            <label class="personal-label">
                                                @lang('site.purchase_end_date')
                                                <div class="tooltip">
                                                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                    <span class="tooltiptext tooltip-right">{{__('site.purchase_end_date_tooltip')}}</span>
                                                </div>
                                            </label>
                                            <input type="text" id="datepicker_ped" class="required personal-type" value="{{@$product->purchase_end_date}}" name="purchase_end_date" placeholder="@lang('site.enter_purchase_end_date')" autocomplete="off" readonly>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">
                                                @lang('client_site.course_start_date')
                                                <div class="tooltip">
                                                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                    <span class="tooltiptext tooltip-right">{{__('client_site.course_start_date_tooltip')}}</span>
                                                </div>
                                            </label>
                                            <input type="text" id="datepicker_csd" class="required personal-type" value="{{@$product->course_start_date}}" name="course_start_date" placeholder="@lang('client_site.enter_course_start_date')" autocomplete="off" readonly>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">
                                                <input type="checkbox" id="is_affiliate_product"  @if(@$product->affiliate_program == 'Y') checked @endif>
                                                <input type="text" value="N" name="affiliate_program" id="affiliate_program" hidden>
                                                @lang('site.is_affiliate_product')
                                                <div class="tooltip">
                                                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                    <span class="tooltiptext tooltip-right">{{__('site.is_affiliate_tooltip')}}</span>
                                                </div>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label"  @if(@$product->affiliate_program == 'N') style="opacity: 0.7;" @endif >
                                                @lang('site.affiliate_commission')
                                                <div class="tooltip">
                                                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                    <span class="tooltiptext tooltip-right">{{__('site.affiliate_commission_tooltip')}}</span>
                                                </div>
                                                <input type="text" class="required personal-type" name="affiliate_commission" id="affiliate_commission" placeholder="@lang('site.affiliate_commission')" value="{{@$product->affiliate_percentage}}" @if(@$product->affiliate_program == 'N') disabled @endif >
                                                <p id="affiliate_comm_err" style="font-size: 16px; font-weight: normal; margin: 0;"></p>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label" @if(@$product->affiliate_program == 'N') style="opacity: 0.7;" @endif>
                                                @lang('site.affiliate_text')
                                                <div class="tooltip">
                                                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                    <span class="tooltiptext tooltip-right">{{__('site.affiliate_text_tooltip')}}</span>
                                                </div>
                                                <textarea name="affiliate_text" id="affiliate_text" class="required personal-type99" placeholder="@lang('site.affiliate_text')" cols="80" rows="7" disabled>{{@$product->affiliate_text}}</textarea>
                                                <p id="affiliate_text_err" style="font-size: 16px; font-weight: normal; margin: 0;"></p>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">
                                                @lang('site.content_available')
                                                <div class="tooltip">
                                                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                    <span class="tooltiptext tooltip-right">{{__('site.content_available_tooltip')}}</span>
                                                </div>
                                            </label>
                                            <input type="text" placeholder="@lang('site.content_available')" class="personal-type" value="{{@$product->content_available_days}}" name="content_available" id="content_available">
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">
                                                @lang('site.cancel_day_label') *
                                                <div class="tooltip">
                                                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                    <span class="tooltiptext tooltip-right">{{__('site.cancel_day_tooltip')}}</span>
                                                </div>
                                            </label>
                                            <input type="text" placeholder="@lang('site.cancel_day_label')" class="required personal-type" name="cancel_day" id="cancel_day" value="{{@$product->cancel_day}}">
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">{{__('site.no_of_installment_for_product')}}
                                                <div class="tooltip">
                                                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                    <span class="tooltiptext tooltip-right">{{__('site.no_of_installment_for_product_info')}}</span>
                                                </div>
                                            </label>
                                            <select class="form-control" name="no_of_installment" id="no_of_installment">
                                                <option value="1" @if($product->no_of_installment==1) selected @endif>--{{__('site.no_installment')}}--</option>
                                                @for ($i = 2; $i <= 12; $i++)
                                                    <option value="{{$i}}" @if(@$i== $product->no_of_installment) selected @endif>{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">{{__('site.installment_charge_given_product_by_you')}}
                                                <div class="tooltip">
                                                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                    <span class="tooltiptext tooltip-right">{{__('site.installment_charge_given_product_by_you_info')}}</span>
                                                </div>
                                            </label>
                                            <select class="form-control" name="installment_charge" id="installment_charge">
                                                <option value="N" @if($product->installment_charge=='N') selected @endif>{{__('site.no')}}</option>
                                                    <option value="Y" @if($product->installment_charge=='Y') selected @endif>{{__('site.yes')}}</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-6 col-sm-12">
                                        <div class="form-group" style="margin-top: 22px;">
                                            <label class="personal-label">
                                                @lang('site.cover_image') * (Tamanho recomendado para a imagem 1280 * 720px)
                                                <div class="tooltip">
                                                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                    <span class="tooltiptext tooltip-right">{{__('client_site.tooltip_text_image')}}</span>
                                                </div>
                                            </label>
                                            <input type="file" class="personal-type" id="cover_image" placeholder="Image" name="cover_image" accept="image/*">
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">@lang('site.existing_picture')</label>

                                            <img src="{{URL::to('storage/app/public/uploads/product_cover_images').'/'.@$product->cover_image}}" width="200">
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">@lang('site.description') *</label>
                                            <textarea class="required short_description personal-type99" placeholder="@lang('client_site.enter_prod_desc')" name="desc" id="admin_description" cols="80" rows="7">{{@$product->admin_description}}</textarea>
                                            <label class="desc_error" style="color: red"></label>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">Descrição *</label>
                                            <textarea class="required short_description personal-type99" placeholder="@lang('client_site.enter_prod_intro')" name="intro" id="short_description" cols="80" rows="7">{{@$product->description}}</textarea>
                                            <label class="intro_error" style="color: red"></label>
                                        </div>
                                    </div>

                                    <div id="description_section">

                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <label class="personal-label">@lang('site.how_it_works')</label>
                                            <div class="form-group">
                                                <textarea class="short_description personal-type99" placeholder="@lang('client_site.enter_how_it_works')" name="how_it_works" id="how_it_works" cols="80" rows="7">{{@$product->how_it_works}}</textarea>
                                                <label class="how_it_works_error error error1" style="color: red"></label>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <label class="personal-label">@lang('site.how_it_can_help')</label>
                                            <div class="form-group">
                                                <textarea class="short_description personal-type99" placeholder="@lang('client_site.enter_how_it_can_help')" name="can_help" id="can_help" cols="80" rows="7">{{@$product->can_help}}</textarea>
                                                <label class="can_help_error error error1" style="color: red"></label>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <label class="personal-label">@lang('site.for_whom')</label>
                                            <div class="form-group">
                                                <textarea class="short_description personal-type99" placeholder="@lang('client_site.enter_for_whom')" name="for_whom" id="for_whom" cols="80" rows="7">{{@$product->for_whom}}</textarea>
                                                <label class="for_whom_error error error1" style="color: red"></label>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <label class="personal-label">@lang('site.benefits')</label>
                                            <div class="form-group">
                                                <textarea class="short_description personal-type99" placeholder="@lang('client_site.enter_benefits')" name="benefits" id="benefits" cols="80" rows="7">{{@$product->benefits}}</textarea>
                                                <label class="benefits_error error error1" style="color: red"></label>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <label class="personal-label">@lang('site.when_to_do_it')</label>
                                            <div class="form-group">
                                                <textarea class="short_description personal-type99" placeholder="@lang('client_site.enter_when_to_do_it')" name="when_to_do" id="when_to_do" cols="80" rows="7">{{@$product->when_to_do}}</textarea>
                                                <label class="when_to_do_error error error1" style="color: red"></label>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <label class="personal-label">@lang('site.previous_preparation')</label>
                                            <div class="form-group">
                                                <textarea class="short_description personal-type99" placeholder="@lang('client_site.enter_prev_prep')" name="previous_preparation" id="previous_preparation" cols="80" rows="7">{{@$product->previous_preparation}}</textarea>
                                                <label class="previous_preparation_error error error1" style="color: red"></label>
                                            </div>
                                        </div>

                                        <!-- <div class="col-lg-12 col-md-12 col-sm-12">
                                            <label class="personal-label">@lang('site.introductory_video') [Add youtube video url]</label>
                                            <div class="form-group w-50">
                                                <input type="url" name="intro_video" id="intro_video" class="personal-type" placeholder="Embed an introductory video url" value="@if(@$product->intro_video) {{'https://www.youtube.com/watch?v=' . @$product->intro_video}} @endif">
                                                <label class="intro_video_error error error1" style="color: red"></label>
                                            </div>
                                        </div> -->

                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12 my-0">
                                        <div class="form-group my-0">
                                            <label class="form_error error" id="form_error" style="color: red"></label>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <button type="button" id="product_save" class="login_submitt btn-success mr-2 product_edit_btn">@lang('site.product_save')</button>
                                        <button type="button" id="product_save_continue" class="login_submitt mr-2 product_edit_btn">@lang('site.save_and_continue')</button>
                                        <!-- <button type="button" id="edit_course_chapters" class="login_submitt">@lang('site.step_2')</button> -->
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    @if(@$product->admin_status == 'A')
                        <div class="weicome row">
                            <div class="col-lg-12 col-md-6 col-sm-12">
                                <h3>@lang('site.copy_product_link')</h3>
                            </div>
                        </div>
                        <div class="dash_main">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group copy_input">
                                    <!-- <label class="personal-label">@lang('site.product_title')</label> -->
                                    <input type="text" class="required personal-type" value="{{url('/')}}/product-details/{{@$product->slug}}" id="product_details_link" disabled >
                                    <div class="copy-icon" title="Copy Link" onclick="copyText(`{{url('/')}}/product-details/{{@$product->slug}}`)">
                                        <i class="fa fa-clone" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')

<script>
    $(function() {
        $("#datepicker_psd").datepicker({dateFormat: "yy-mm-dd",

            monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            monthNamesShort: [ "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ],
            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],

            defaultDate: new Date(),
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            showMonthAfterYear: true,
            showWeek: true,
            showAnim: "drop",
            constrainInput: true,
            yearRange: "-1:+1",
            minDate:new Date(),
            onClose: function( selectedDate ) {
                $( "#datepicker_ped").datepicker( "option", "minDate", selectedDate );
            }
        });

        $("#datepicker_ped").datepicker({dateFormat: "yy-mm-dd",

            monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            monthNamesShort: [ "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ],
            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],

            defaultDate: new Date(),
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            showMonthAfterYear: true,
            showWeek: true,
            showAnim: "drop",
            constrainInput: true,
            yearRange: "-1:+1",
            minDate:new Date(),
            onClose: function( selectedDate ) {
                var dt = new Date(selectedDate);
                dt.setDate(dt.getDate() + 1);
                $( "#datepicker_csd").datepicker( "option", "minDate", dt );
            }
        });
    });

    $("#datepicker_csd").datepicker({dateFormat: "yy-mm-dd",

        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        monthNamesShort: [ "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ],
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
        dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],

        defaultDate: new Date(),
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        showMonthAfterYear: true,
        showWeek: true,
        showAnim: "drop",
        constrainInput: true,
        yearRange: "-1:+1",
        minDate: $('#datepicker_ped').val() == "" ? new Date($('#datepicker_ped').val()) : new Date(),
        onClose: function( selectedDate ) {
            //$( "#datepicker1").datepicker( "option", "minDate", selectedDate );
        }
    });

    function copyText(text){
        var input = document.createElement('input');
        input.setAttribute('value', text);
        document.body.appendChild(input);
        input.select();
        var result = document.execCommand('copy');
        document.body.removeChild(input);
        toastr.success("Link Copied");
        return result;
    }
</script>
<script>
//on submit
var aff_comm = "{{ @$product->affiliate_percentage ? @$product->affiliate_percentage : '' }}";
var is_affiliate_comm_reqd = false;
    $(document).ready(function(){
        $('#price').keyup(function(){
            $("#price").inputFilter(function(value) {
            return /^\d*$/.test(value);
            });
        });
        $('#discounted_price').keyup(function(){
            $("#discounted_price").inputFilter(function(value) {
            return /^\d*$/.test(value);
            });
        });
        $('#content_available').keyup(function(){
            $("#content_available").inputFilter(function(value) {
            return /^\d*$/.test(value);
            });
        });
        $('#cancel_day').keyup(function(){
            $("#cancel_day").inputFilter(function(value) {
            return /^\d*$/.test(value);
            });
        });
        $('#affiliate_commission').keyup(function(){
            $("#affiliate_commission").inputFilter(function(value) {
            return /^\d*$/.test(value);
            });
        });
        $('#is_affiliate_product').on('change', function(){
            console.log("Changed");
            if ($('#is_affiliate_product').is(':checked')) {
                is_affiliate_comm_reqd = true;
                $('#affiliate_program').val('Y');
                $('#affiliate_commission').val(aff_comm);
                $('#affiliate_commission').removeAttr('disabled');
                $('#affiliate_commission').parent().css('opacity', 1);
                $('#affiliate_text').removeAttr('disabled');
                $('#affiliate_text').parent().css('opacity', 1);
            } else {
                is_affiliate_comm_reqd = false;
                $('#affiliate_program').val('N');
                $('#affiliate_commission').val('');
                $('#affiliate_commission').attr('disabled', true);
                $('#affiliate_commission').parent().css('opacity', 0.7);
                $('#affiliate_text').attr('disabled', true);
                $('#affiliate_text').parent().css('opacity', 0.7);
                $('#affiliate_text').val('');
            }
        });
        $('#category').on('change', function(){
            var id = $(this).val();
            $.ajax({
				url: "{{url('/')}}/get-product-subcategories/"+id,
				method: 'get',
				dataType: 'json',
				async: false,
				success: function(res){
                    var html = `<option value="">--Select--</option>`;
                    $.each(res.subcategories, function(i, subcategory){
                        html += `<option value="${subcategory.id}">${subcategory.name}</option>`;
                    });
                    $('#subcategory').empty();
                    $('#subcategory').append(html);
				}
			});
        });
        // $('.copy-icon').click(function(){
        //     console.log("Copying");
        //     var copyText = document.getElementById("product_details_link");
        //     copyText.select();
        //     copyText.setSelectionRange(0, 99999); /* For mobile devices */
        //     document.execCommand("copy");
        //     alert("Copied the text: " + copyText.value);
        //     // toastr.success("Link Copied");
        // })
        $('.product_edit_btn').on('click', function(e){
            var this_btn = $(this);
            if($("#product_edit").valid()) {
                $('.product_edit_btn').attr('disabled', true);
                $('.product_edit_btn').addClass('save_btn_disabled');
                $.ajax({
                    url: "{{ route('store.product') }}",
                    type: "POST",
                    data:  new FormData($("#product_edit").get(0)),
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(data)
                    {
                        console.log(data);
                        if($(this_btn).attr('id') == "product_save_continue")
                            window.location.href = "{{URL::to('/edit-course')}}" + "/" + data;
                        else
                            window.location.reload();
                    },
                    error: function(error){
                        console.log(error);
                    }
                });
            } else {
                return false;
            }
        });
        jQuery.validator.addMethod("greaterThanStartDate", function(value, element) {
            if($('#datepicker_psd').val() == ""){
                return true;
            } else{
                var date = $('#datepicker_psd').val();
                return this.optional(element) || new Date(value) > new Date(date);
            }
		}, "@lang('site.greater_than_start_date')");

        jQuery.validator.addMethod("greaterThanEndDate", function(value, element) {
            if($('#datepicker_ped').val() == ""){
                return true;
            } else {
                var date = $('#datepicker_ped').val();
                return this.optional(element) || new Date(value) > new Date(date);
            }
		}, "@lang('client_site.greater_than_end_date')");

        jQuery.validator.addMethod("lessThanPrice", function(value, element) {
            if($('#price').val() == ""){
                return true;
            } else {
                var price = $('#price').val();
                return this.optional(element) || parseInt(value) < parseInt(price);
            }
		}, "@lang('site.less_than_price')");

        jQuery.validator.addMethod("greaterThanZero", function(value, element) {
                return this.optional(element) || parseInt(value) > 0;
		}, "@lang('site.greater_than_zero')");

        jQuery.validator.addMethod("commGreaterThanZero", function(value, element) {
                return this.optional(element) || parseInt(value) > 0;
		}, "@lang('site.comm_greater_than_zero')");

        jQuery.validator.addMethod("percentage", function(value, element) {
                return this.optional(element) || parseInt(value) < 100;


		}, "@lang('site.less_than_100')");

        $('#product_edit').validate({
            rules: {
                title: "required",
                price: {required:true, greaterThanZero:true},
                discounted_price: "lessThanPrice",
                category: "required",
                subcategory: "required",
                purchase_end_date: {required:true, greaterThanStartDate:true},
                affiliate_commission: {required: function() { return is_affiliate_comm_reqd; }, commGreaterThanZero:true, percentage:true},
                affiliate_text: {required: function() { return is_affiliate_comm_reqd; }},
                content_available: "required",
                cancel_day: "required",
                desc: "required",
                intro: "required",
                course_start_date: {required: true, greaterThanEndDate:true}
            },
            errorPlacement: function(error, element) {
                if (element.attr("name") == "affiliate_commission") {
                    $("#affiliate_comm_err").append(error);
                    console.log(error);
                } else if (element.attr("name") == "affiliate_text") {
                    $("#affiliate_text_err").append(error);
                    console.log(error);
                } else {
                    error.insertAfter(element);
                }
            }
        });
    });

    function ytVidId(url) {
        var p = /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
        return (url.match(p)) ? RegExp.$1 : false;
    }

    $('#intro_video').bind("change", function() {
        var url = $(this).val();
        if (ytVidId(url) !== false) {
            $('.intro_video_error').html('');
        } else {
            $(".intro_video_error").html('Link do youtube inválido.');
            $('#intro_video').val('');
        }
    });

    $('.set_date_radio').change(function(){
        if($(this).val() == 'S'){
            $("#datepicker_psd").datepicker('setDate', new Date());
            $('#end_date_div').hide();
        } else {
            $('#end_date_div').show();
        }
    });

</script>
@endsection
