@extends('layouts.app')
@section('title')
    @lang('site.post') @lang('site.details')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.preview_certificate')</h2>

   

        <div class="bokcntnt-bdy no-margin-top">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="view-page col-md-12">
                            <p><strong>@lang('site.order_id') :</strong>{{@$order->orderMaster->token_no}}</p>
                            <p><strong>@lang('site.product_title') :</strong>{{@$order->product->title}}</p>
                            <p><strong>@lang('site.price')  :</strong>{{@$order->amount}}</p>
                            <p><strong>@lang('site.purchase_date') :</strong>{{date('jS F Y' ,strtotime(@$order->orderMaster->order_date))}}</p>
                           
                        </div>
                         <div class="blog-right details-right">
                    <div class="">
                        <div class=""> 
                            <div class="from-field">
                                <div class="weicome">
                                    <div class="clearfix"></div>
                                    <div class="clearfix"></div>
                                    <div id="cert_desc">{!! $certificatedesc !!}</div>
                                </div>                       
                            </div>
                                        
                            
                        </div>
                    </div>
                </div>
            </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
    
    <script>
        $(document).ready(function(){
            $('#myForm').validate();
            var background = "";
            @if(@$template->background_filename)
                background = "url('{{url('/')}}/storage/app/public/uploads/certificate_template/{{@$template->background_filename}}') no-repeat";
            @else
                background = "url('{{url('/')}}/public/frontend/images/template{{@$template->template_number}}.jpg') no-repeat";
            @endif
            $('.cert_template').css('background', background);
            $('.cert_template').css('background-size', 'cover');
            $('.cert_template').css('background-position', 'center');
        });
    </script>


@endsection