@extends('layouts.app')
@section('title')
@lang('site.refer')
@endsection
@section('style')
@include('includes.style')
<style>
    </style>
@endsection

@section('scripts')
@include('includes.scripts')
@endsection

@section('header')
@include('includes.header')
@endsection


@section('content')
<section class="free_subMain">
        <div class="container">
            <div class="page-h2">
                <h2>{{__('site.invite_benefit_heading')}}</h2>
                <p style="background-image:none !important;">{{__('site.invite_benefit_content')}}.</p>
            </div>
            <div class="row w-100">
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="work-box wb_box">
                        <img src="{{ URL::to('storage/app/public/uploads/content_images/'.@$content->image_1)}}" alt="">
                        <!-- <span>Free Registration</span> -->
                        <p>{{@$content->content_1}}</p>
    
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="work-box wb_box">
                        <img src="{{ URL::to('storage/app/public/uploads/content_images/'.@$content->image_2)}}" alt="">
                        <!-- <span>Find your Advisory</span> -->
                        <p>{{@$content->content_2}}</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="work-box wb_box">
                        <img src="{{ URL::to('storage/app/public/uploads/content_images/'.@$content->image_3)}}" alt="">
                        <!-- <span>Schedule a lesson</span> -->
                        <p>{{@$content->content_3}}</p>
                    </div>
                </div>
    
            </div>
            <div class="invite_btnBox">
                <button type="button" class="btn btn-primary model_popup" data-toggle="modal" data-target="#mdlfree">
                    {{__('site.invite')}}
                </button>
            </div>
            <div class="prog_cp">
                <div class="pr_div">
                    <h3>{{__('site.invite_process')}}</h3>
                    {!!@$content->process_content!!}
                    <div class="copy_url">
                        <input type="text" value="{{route('refer',['id'=> @Auth::guard('web')->user()->slug])}}" class="input_txtBox" ><button type="button"
                            title="Copy link" class="copy_refer_link">{{__('site.copy_link')}}</button>
                    </div>
                </div>
            </div>
            @if($totalRefer>0)
            <div class="prog_cp">
                <div class="pr_div">
                    <h3>{{__('site.your_invite')}} </h3>
                    <ul class="dot_txt">
                        <li>{{__('site.total_refer')}}:-> {{ @$totalRefer }}</li>
                        <li>{{__('site.total_complete_refer')}}:-> {{ @$referBenefit }}</li>
                        <li>{{__('site.total_benefit')}}:-> {{ @$referBenefitRemaining }}</li>
                    </ul>
                    <div class="invite_btnBox">
                        <a href="{{route('refer.page.list')}}" class="btn btn-primary model_popup">{{__('site.invite_friend_list')}}</a>
                    </div>
                </div>
            </div>
            @endif
        </div>
    
    </section>
    <!-- The Modal -->
    <div class="modal" id="mdlfree">
        <div class="modal-dialog">
            <div class="modal-content">
    
                <!-- Modal Header -->
                <div class="modal-header">
                    <!-- <h4 class="modal-title">Modal Heading</h4> -->
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
    
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="md_body_img">
                        <img src="{{ URL::to('storage/app/public/uploads/content_images/'.@$content->modal_image)}}">
                    </div>
                    @php
                    $message='Olá! Como você está? Estou enviando para você um desconto para se cadastrar e ser atendida com um profissional da Netbhe. O desconto só é válido através deste link: '.route('refer',['id'=> @Auth::guard('web')->user()->slug])
                    @endphp
                    <div class="div_invit_body">
                        <p>
                            {{__('site.where_invite')}}
                        </p>
                        <a href="https://wa.me/?text={{$message}}" class="scl_btns scl_btn1" target="blank">{{__('site.whats_app')}}</a>
                        <a href="https://www.facebook.com/share.php?u=https://www.netbhe.com/&quote={{$message}}" class="scl_btns scl_btn2" target="blank">{{__('site.facebook')}}</a>
                        <a href="mailto:?subject=Discount on new user&body={{$message}}" class="scl_btns scl_btn3" target="blank">{{__('site.email')}}</a>
                        <label class="copy_box">
                            <span>{{__('site.link_to_share')}}</span>
                            <a href="#">{{route('refer',['id'=> @Auth::guard('web')->user()->slug])}}</a>
                        </label>
                        <a href="#" class="scl_btns scl_btn3 copy_refer_link">{{__('site.copy_link')}}</a>
                    </div>
                </div>
    
                <!-- Modal footer -->
                <!-- <div class="modal-footer"> -->
                <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
                <!-- </div> -->
    
            </div>
        </div>
    </div>
    <script>
        $('.copy_refer_link').click(function (e) {
            e.preventDefault();
            var copyText = '{{ route('refer',['id'=> @Auth::guard('web')->user()->slug]) }}';
            document.addEventListener('copy', function(e) {
                e.clipboardData.setData('text/plain', copyText);
                e.preventDefault();
            }, true);
            document.execCommand('copy');
            // alert('Copied Refer Link: ' + copyText);
            toastr.success('{{__('site.link_copied')}}');
        });
    </script>
@endsection



@section('footer')
@include('includes.footer')

@endsection