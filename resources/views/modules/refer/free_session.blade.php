@extends('layouts.app')
@section('title')
@lang('site.free_session')
@endsection
@section('style')
@include('includes.style')
<style>
</style>
@endsection

@section('scripts')
@include('includes.scripts')
@endsection

@section('header')
@include('includes.header')
@endsection


@section('content')
<section class="free_subMain">
    <div class="container">
        <div class="page-h2">
            <h2>{{__('site.free_session')}}</h2>
            {{-- <p style="background-image:none !important;">{{__('site.invite_benefit_content')}}.</p> --}}
        </div>
        <div class="prog_cp">
            <div class="pr_div">
                <h3>{{__('site.free_session_link')}}</h3>
                <p>Copie o link e envie para quem você gostaria de oferecer sessão gratuita, às pessoas que usarem este link, poderá agendar horário com você, desde que você tenha disponibilidade no site. Com esse link a Netbhe não cobra taxas de comissão de uso da plataforma. Conheça as funções da Netbhe, envie para alguém e teste a plataforma. </p>
                {{-- <ul class="dot_txt">
                    <li>{{__('site.invite_process_1')}}</li>
                    <li>{{__('site.invite_process_2')}}</li>
                    <li>{{__('site.invite_process_3')}}</li>
                </ul> --}}
                <div class="copy_url">
                    <input type="text" value="{{route('freeSession.invite',['slug'=> @Auth::guard('web')->user()->slug,'id'=>@Auth::guard('web')->user()->free_session_id])}}"
                        class="input_txtBox"><button type="button" title="Copy link"
                        class="copy_refer_link">{{__('site.copy_link')}}</button>
                </div>
            </div>
        </div>
        
    </div>

</section>
<!-- The Modal -->
<div class="modal" id="mdlfree">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <!-- <h4 class="modal-title">Modal Heading</h4> -->
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="md_body_img">
                    <img src="{{ URL::to('public/frontend/images/im4.png')}}">
                </div>
                <div class="div_invit_body">
                    <p>
                        {{__('site.where_invite')}}
                    </p>
                    <a href="https://wa.me" class="scl_btns scl_btn1" target="blank">{{__('site.whats_app')}}</a>
                    <a href="https://www.facebook.com/" class="scl_btns scl_btn2"
                        target="blank">{{__('site.facebook')}}</a>
                    <a href="mailto:" target="_blank" class="scl_btns scl_btn3" target="blank">{{__('site.email')}}</a>
                    <label class="copy_box">
                        <span>{{__('site.link_to_share')}}</span>
                        <a href="#">{{route('refer',['id'=> @Auth::guard('web')->user()->slug])}}</a>
                    </label>
                    <a href="#" class="scl_btns scl_btn3 copy_refer_link">{{__('site.copy_link')}}</a>
                </div>
            </div>

            <!-- Modal footer -->
            <!-- <div class="modal-footer"> -->
            <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
            <!-- </div> -->

        </div>
    </div>
</div>
<script>
    $('.copy_refer_link').click(function (e) {
            e.preventDefault();
            var copyText = '{{route('freeSession.invite',['slug'=> @Auth::guard('web')->user()->slug,'id'=>@Auth::guard('web')->user()->free_session_id])}}';
            document.addEventListener('copy', function(e) {
                e.clipboardData.setData('text/plain', copyText);
                e.preventDefault();
            }, true);
            document.execCommand('copy');
            // alert('Copied Refer Link: ' + copyText);
            toastr.success('{{__('site.link_copied')}}');
        });
</script>
@endsection



@section('footer')
@include('includes.footer')

@endsection