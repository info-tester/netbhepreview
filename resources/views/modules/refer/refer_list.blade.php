@extends('layouts.app')
@section('title')
@lang('site.refer')
@endsection
@section('style')
@include('includes.style')
<style>
</style>
@endsection

@section('scripts')
@include('includes.scripts')
@endsection

@section('header')
@include('includes.header')
@endsection


@section('content')
<section class="free_subMain">
    <div class="container">
        <div class="page-h2">
            <h2>{{__('site.invite_benefit_heading')}}</h2>
            <p style="background-image:none !important;">{{__('site.invite_benefit_content')}}.</p>
        </div>
        
        
        <div class="prog_cp">
            <div class="pr_div">
                <h3>{{__('site.invite_friend_list')}}</h3>
                <div class="mainDiv">
                    <div class="buyer_table">
                
                        <div class="table-responsive">
                            <div class="table">
                                <div class="one_row1 hidden-sm-down only_shawo">
                                    <div class="cell1 tab_head_sheet">{{__('site.no')}}</div>
                
                                    <div class="cell1 tab_head_sheet">{{__('client_site.name')}}</div>
                                    <div class="cell1 tab_head_sheet">{{__('site.first_booking')}}</div>
                
                                    {{-- <div class="cell1 tab_head_sheet">@lang('client_site.action')</div> --}}
                                </div>
                                <!--row 1-->
                                @php
                                $i=1;
                                @endphp
                                @foreach (@$totalRefer as $item)
                
                                <div class="one_row1 small_screen31">
                                    <div class="cell1 tab_head_sheet_1">
                                        {{-- <span class="W55_1">@lang('client_site.day')</span> --}}
                                        <p class="add_ttrr">{{$i}}.</p>
                                    </div>
                                    <div class="cell1 tab_head_sheet_1">
                                        {{-- <span class="W55_1">@lang('client_site.from_time')</span> --}}
                                        <p class="add_ttrr">{{ @$item->nick_name ? @$item->nick_name : @$item->name }}</p>
                                    </div>
                                    <div class="cell1 tab_head_sheet_1">
                                        {{-- <span class="W55_1">@lang('client_site.to_time')</span> --}}
                                        <p class="add_ttrr">{{ @$item->is_paid_activity== 'C' ? __('site.first_booking_complete'):__('site.first_booking_not_complete') }}</p>
                                    </div>
                                </div>
                                @php $i++ @endphp
                                @endforeach
                            </div>
                        </div>
                    </div>
                
                </div>
                {{-- <ul class="dot_txt">
                    <li style="list-style: none">{{__('client_site.name')}}</li>
                    @php 
                    $i=1;
                    @endphp
                    @foreach (@$totalRefer as $item)
                       <li style="list-style: none">{{$i}}. {{ @$item->nick_name ? @$item->nick_name : @$item->name }} ({{ @$item->is_paid_activity== 'C' ? 'C':' P' }})</li>
                       @php $i++ @endphp 
                    @endforeach
                    
                </ul> --}}
            </div>
        </div>
        
    </div>

</section>
@endsection



@section('footer')
@include('includes.footer')

@endsection