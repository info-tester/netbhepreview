@extends('layouts.app')
@section('title')
    @lang('site.post') @lang('site.details')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.Content_templates_details'){{-- Content templates details --}}</h2>

       {{-- @include('includes.professional_tab_menu') --}}

        <div class="bokcntnt-bdy no-margin-top">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.user_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="blog-right details-right">
                    <div class="blog-box">

                        <div class="blog-dtls">

                            <div class="col-md-12 view-page coach_tools">
                                <p>
                                    <strong>@lang('site.subject'):</strong>
                                    {{ @$details->getContentTemplatesdata->cont_subject }}
                                </p>
                                <p>
                                    <strong>@lang('site.category'):</strong>
                                    @php
                                        switch (@$details->getContentTemplatesdata->cont_category) {
                                            case 'audio':
                                                echo __('site.audio');
                                                break;
                                            case 'video':
                                                echo __('site.video');
                                                break;
                                            case 'image':
                                                echo __('site.image');
                                                break;

                                            default:
                                                echo '-';
                                                break;
                                        }
                                    @endphp
                                </p>

                                <p>
                                    <strong>@lang('site.description'):</strong>
                                    {!! $details->getContentTemplatesdata->cont_description !!}
                                </p>

                                @if(@$details->getContentTemplatesdata->cont_category == "video")
                                    <p>
                                        <strong>@lang('site.video'):</strong>
                                        <div class="col-lg-12">
                                            <div class="order-id">
                                                <div class="col-lg-12">
                                                  <span>
                                                    <iframe width="350" height="220" src="https://www.youtube.com/embed/{{ @$details->getContentTemplatesdata->cont_cat_video_img_audio }}"></iframe>
                                                  </span>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </p>
                                @elseif(@$details->getContentTemplatesdata->cont_category == "audio")
                                    <p>
                                        <strong>@lang('site.audio'):</strong>
                                        <audio src="{{ url('storage/app/public/uploads/user_template_content/audio/'.$details->getContentTemplatesdata->cont_cat_video_img_audio) }}" class="form-control form-control-lg" controls></audio>
                                    </p>
                                @elseif(@$details->getContentTemplatesdata->cont_category == "image")
                                    <p>
                                        <strong>@lang('site.image'):</strong>
                                        <img src="{{ url('storage/app/public/uploads/user_template_content/'.@$details->getContentTemplatesdata->cont_cat_video_img_audio) }}" alt="" style="width: 400px;">
                                    </p>

                                @endif
                            </div>

                        </div>
                    </div>

                </div>
            </div>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
    <script>
        $(document).ready(function(){
            $('#myForm').validate();
        });
    </script>

@endsection
