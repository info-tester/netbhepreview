@extends('layouts.app')
@section('title')
    {{-- @lang('site.my_blogs') --}}
    Content templates
@endsection
@section('style')
@include('includes.style')

@endsection
@section('scripts')
@include('includes.scripts')
<script src="{{URL::to('public/js/chosen.jquery.min.js')}}"></script>
@endsection
@section('header')
@include('includes.professional_header')
<link rel="stylesheet" href="{{URL::to('public/css/chosen.css')}}">
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.content_template')</h2>


		{{-- <div class="like-tab">
        	<ul>
            	<li><a class="active" href="#">Form tools</a></li>
                <li><a href="#">Tab tow</a></li>
                <li><a href="#">Tab three</a></li>
                <li><a href="#">Tab four</a></li>
            </ul>
        </div> --}}

        @include('includes.professional_tab_menu')

        <div class="bokcntnt-bdy no-margin-top">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>

            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">

                <div class="" style="float: right;">
                    <a class="btn btn-success" href="{{route('user-content-temp.create')}}">+ @lang('site.add_new_content_template') {{-- @lang('site.add_new_content_template') --}}</a>
                </div>



                <div class="buyer_table">
                    <div class="table-responsive">
                        @if(sizeof(@$all_content_template)>0)
                        <div class="table">
                            <div class="one_row1 hidden-sm-down only_shawo">
                                <div class="cell1 tab_head_sheet">@lang('site.subject')</div>

                                <div class="cell1 tab_head_sheet">@lang('site.category')</div>


                                {{-- <div class="cell1 tab_head_sheet">posted on</div> --}}
                                <div class="cell1 tab_head_sheet">@lang('site.status')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.action'){{-- @lang('site.action') --}}</div>
                            </div>
                            <!--row 1-->
                            <?php
                            //pr1($all360data->toArray());
                            //die();
                            ?>

                                @foreach(@$all_content_template as $details)
                                    <div class="one_row1 small_screen31">
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.no')</span>
                                            <p class="add_ttrr">{{$details->cont_subject}}</p>
                                        </div>

                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.no')</span>
                                            <p class="add_ttrr">{{$details->cont_category}}</p>
                                        </div>




                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.status')</span>
                                            <p class="add_ttrr">
                                                {{-- {{ $details['status'] }} --}}
                                                @if($details['status'] == "ACTIVE")
                                                    ATIVO
                                                @elseif($details['status'] == "INACTIVE")
                                                    INATIVA
                                                @endif
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                            @if($details->status=="ACTIVE")
                                                @if($details->added_by=="P" && $details->added_by_id == Auth::id())
                                                    <a href="{{route('user-content-temp.edit',[$details->id])}}" class="acpt editbtn" style="margin: 2px;" title="Edit">@lang('site.edit')</a>
                                                    <a href="javascript:void(0)" onclick="deleteCategory('{{route('user-content-temp.destroy',[$details->id])}}')" title="Delete" class="rjct delbtn"> @lang('site.delete')</a>
                                                    @if($details->status=="ACTIVE")
                                                        <a href="{{ route('prof.contenttemp.status', ['id'=>$details->id]) }}" onclick="return confirm('Deseja realmente desativar as ferramentas deste modelo de contato?')" class="acpt" title="@lang('site.inactive')">@lang('site.inactive')</a>
                                                    @endif
                                                @endif
                                                <a href="javascript:;" class="show_assign acpt assginbtn" data-title="{{$details->title}}" data-id="{{ $details->id }}" style="margin: 2px;">@lang('site.send')</a>
                                                <a href="{{route('user-content-temp.show', [$details->id])}}" class="acpt viewbtn" style="margin: 2px;" title="View Question">@lang('site.view')</a>
                                                <a href="{{route('prof.content.temp.user.assigne.list', ['contract_templats_id'=>@$details->id])}}" class="acpt view_asgi_btn" style="margin: 2px;">@lang('site.view_assign')</a>
                                            @else
                                                <a href="{{ route('prof.contenttemp.status', ['id'=>$details->id]) }}" title="@lang('site.active')" onclick="return confirm('Deseja realmente ativar esta ferramenta de modelo de contato?')" class="acpt rjct">@lang('site.active')</a>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach

                        </div>
                        @else
                            <div class="one_row small_screen31">
                                <center><span><h3 class="error">@lang('client_site.oops_no_found')</h3></span></center>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>



<!--assigne Modal start -->
<div class="modal fade" id="assignModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Assign User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="myform" method="post" action="{{ route('prof.content.temp.user.assigne') }}">
                {{ csrf_field() }}
                <div class="modal-body">
                    <h5>Título da ferramenta: <span class="tool-title"></span></h5>

                        <label for="exampleInputEmail1">Select User</label>
                        <select name="user_id[]" class="required form-control newdrop required chosen-select" multiple="true" required="">
                            <option value="">Select</option>
                            @foreach($all_paid_users as $row)
                                <option value="{{$row->user_id}}">{{@$row->userDetails->nick_name ? @$row->userDetails->nick_name : @$row->userDetails->name}}</option>
                            @endforeach
                        </select>
                    <input type="hidden" name="content_temp_id" class="form_id"> {{-- this calass form id get form id --}}
                </div>

                <div class="modal-footer">
                    <button type="submit" value="submit" class="btn btn-primary">@lang('site.assign')</button>
                </div>

            </form>
        </div>
    </div>
</div>
<!--assigne Modal end -->


</section>


@endsection
@section('footer')
@include('includes.footer')

<script>

$(document).ready(function(){
    $('.show_assign').click(function(){
        var title = $(this).data('title');
        var id = $(this).data('id');
        $('.tool-title').html(title);
        $('.form_id').val(id);
        $('#assignModal').modal('show');
    });
});

$(".chosen-select").chosen();

$(document).ready(function(){
        $("#myform").validate();
    });
</script>

<form method="post" id="destroy">
    {{ csrf_field() }}
    {{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
    var confirm = window.confirm('Do you want to delete this question?');
    if(confirm){
        $("#destroy").attr('action',val);
        $("#destroy").submit();
    }
 }
</script>
<style>
    .error{
        color: red !important;
    }
</style>

@endsection
