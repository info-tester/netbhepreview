@extends('layouts.app')
@section('title')
    {{-- @lang('site.add_new_blog') --}}
     Content templates
@endsection
@section('style')
@include('includes.style')
<style type="text/css">
    div.mce-edit-area {
        background:
        #FFF;
        filter: none;
        min-height: 317px;
    }
    .chck_eml_rd{
        color: red;
        font-weight: bold;
        font-size: 14px;
    }

    .tooltip {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted #ccc;
        color: #006080;
        opacity: 1 !important;
    }

    .tooltip .tooltiptext {
        visibility: hidden;
        position: absolute;
        width: 250px;
        background-color: #555;
        color: #fff;
        text-align: center;
        padding: 5px;
        border-radius: 6px;
        z-index: 1;
        opacity: 0;
        transition: opacity 0.3s;
        margin-left: 10px
    }

    .tooltip-right {
        top: 0px;
        left: 125%;
    }

    .tooltip-right::after {
        content: "";
        position: absolute;
        top: 10%;
        right: 100%;
        margin-top: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: transparent #555 transparent transparent;
    }

    .tooltip:hover .tooltiptext {
        visibility: visible;
        opacity: 1;
    }
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.Add_new_content_templates')</h2>

         @include('includes.professional_tab_menu')
        <div class="bokcntnt-bdy">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">

                    <form id="myform" method="post" action="{{ route('user-content-temp.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form_body">
                            <div class="row">

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
                                    <div class="your-mail">
                                        <label for="exampleInputEmail1" class="personal-label">@lang('site.subject') *</label>
                                        <input type="text" name="cont_subject" class="personal-type required" id="cont_subject" placeholder="@lang('site.subject')" >
                                    </div>
                                </div>



                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="personal-label" for="exampleInputEmail1">
                                            @lang('site.description') *
                                            <div class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">{{__('site.cont_temp_desc_tooltip')}}</span>
                                            </div>
                                        </label>
                                        <div class="clearfix"></div>
                                        <textarea name="cont_description" id="desc" rows="10" placeholder="Description" style="width:100%" class="required desc"></textarea>
                                    </div>
                                    <div class="clearfix"></div>
                                    <p class="error_1" id="cntnt"></p>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="your-mail">
                                        <label class="personal-label">
                                            @lang('site.category')
                                            <div class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">{{__('site.cont_temp_category_tooltip')}}</span>
                                            </div>
                                        </label>
                                        <select class="personal-type personal-select" name="cont_category" id="filetype">
                                            <option value="">@lang('site.select')</option>
                                            <option value="video">@lang('site.video')</option>
                                            <option value="audio">@lang('site.audio')</option>
                                            <option value="image">@lang('site.image')</option>
                                        </select>

                                    </div>
                                </div>

                                {{-- for video --}}
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px" id="videofile">

                                    <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                                        <div class="your-mail">
                                            <label for="exampleInputEmail1">Youtube Video</label>
                                            <input type="text" name="cont_cat_video_img_audio" placeholder="Youtube Video" class="required personal-type" id="youTubeUrl">
                                            {{-- <textarea name="youtube_video" rows="4" cols="50" class="form-control required"></textarea> --}}
                                            Example: https://www.youtube.com/watch?v=Kb8CW3axqRE
                                            <div id="youtube_err"></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12" id="iframeBox" style="display: none">
                                        <iframe id="videoObject" type="text/html" width="300" height="200" frameborder="0" allowfullscreen></iframe>
                                    </div>

                                </div>

                                {{-- for audio --}}
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px" id="audiofile">
                                    <div class="your-mail">
                                        <label for="exampleInputEmail1" class="personal-label">Upload audio *</label>
                                        <input class="personal-type" type="file" name="cont_cat_audio" id="cont_audio" class="required" style="margin-top: 5px;" accept=".mp3">
                                    </div>
                                </div>
                                {{-- for image --}}
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px" id="imagefile">
                                    <div class="your-mail">
                                        <label for="exampleInputEmail1" class="personal-label">Upload Image *</label>
                                        <input class="required personal-type" type="file" name="cont_cat_video_img_audio" id="cont_image" style="margin-top: 5px;" accept="image/*">
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="submit-login add_btnm">
                                        <input value="@lang('site.add')" type="submit" class="login_submitt">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')


<style type="text/css">
    .upldd {
    display: block;
    width: auto;
    border-radius: 4px;
    text-align: center;
    background: #9caca9;
    cursor: pointer;
    overflow: hidden;
    padding: 10px 15px;
    font-size: 15px;
    color: #fff;
    cursor: pointer;
    float: left;
    margin-top: 15px;
    font-family: 'Poppins', sans-serif;
    position: relative;
    }
    .upldd input {
        position: absolute;
        font-size: 50px;
        opacity: 0;
        left: 0;
        top: 0;
        width: 200px;
        cursor: pointer;
    }
    .upldd:hover{
        background: #1781d2;
    }

</style>

    <script>
    $(document).ready(function(){
        $("#myform").validate({
            errorPlacement: function(error, element) {
                if (element.attr("id") == "youTubeUrl") {
                    $("#youtube_err").append(error);
                } else {
                    error.insertAfter(element);
                }
            }
        });
        tinyMCE.init({
            mode : "specific_textareas",
            editor_selector : "desc",
            height: '320px',
            plugins: [
              'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
              'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
              'save table contextmenu directionality emoticons template paste textcolor'
            ],
            relative_urls : false,
            remove_script_host : false,
            convert_urls : true,
            toolbar: 'insertfile undo redo | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor emoticons',
                images_upload_url: '{{ URL::to('storage/app/public/uploads/content_image/') }}',
                images_upload_handler: function(blobInfo, success, failure) {
                    var formD = new FormData();
                    formD.append('file', blobInfo.blob(), blobInfo.filename());
                    formD.append( "_token", '{{csrf_token()}}');
                    $.ajax({
                        url: '{{route("admin.artical.img.upload")}}',
                        data: formD,
                        type: 'POST',
                        contentType: false,
                        cache: false,
                        processData:false,
                        dataType: 'JSON',
                        success: function(jsn) {
                            if(jsn.status == 'ERROR') {
                                failure(jsn.error);
                            } else if(jsn.status == 'SUCCESS') {
                                success(jsn.location);
                            }
                        }
                    });
                },
            });
            $("#myform").submit(function (event) {
        if(tinyMCE.get('desc').getContent()==""){
            event.preventDefault();
            $("#cntnt").html("Description field is required").css('color','red');
        }
        else{
            $("#cntnt").html("");
        }
    });
    });



    $(function() {
        $('#videofile').hide();
        $('#audiofile').hide();
        $('#imagefile').hide();
        $('#filetype').change(function(){
            if($('#filetype').val() == 'video') {
                $('#videofile').show();
                $('#audiofile').hide();
                $('#imagefile').hide();
            }else if($('#filetype').val() == 'audio') {
                $('#audiofile').show();
                $('#videofile').hide();
                $('#imagefile').hide();
            }else if($('#filetype').val() == 'image') {
                $('#imagefile').show();
                $('#audiofile').hide();
                $('#videofile').hide();

            }else {
                $('#reminderdate').hide();
                $('#dayofweek').hide();
                $('#imagefile').hide();
            }
        });
    });


</script>

<script>
$(document).ready(function(){
    $('#youTubeUrl').blur(function(){


    var url = $('#youTubeUrl').val();
        if(url != ''){
        $('#iframeBox').show();
    } else {
    $('#iframeBox').hide()
    };


    validateYouTubeUrl();
    });
});

// $(document).ready(function(){
//  var url = $('#youTubeUrl').val();
//  if(url != ''){
//      $('#iframeBox').show();
//  } else {
//  $('#iframeBox').hide()
//  }
// });

function validateYouTubeUrl() {
    var url = $('#youTubeUrl').val();
    if (url != undefined || url != '') {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
        var match = url.match(regExp);
        if (match && match[2].length == 11) {
            '<div class="munmundey">';
            // Do anything for being valid
            // if need to change the url to embed url then use below line
            $('#videoObject').attr('src', 'https://www.youtube.com/embed/' + match[2] + '?autoplay=1&enablejsapi=1');
        '</div>';

        } else {
            alert('not valid');
            // Do anything for not being valid
        }
    }
}
</script>


<style>
    .error{
        color: red !important;
    }
</style>



@endsection
