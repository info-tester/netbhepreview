@extends('layouts.app')
@section('title')
    @lang('site.post') @lang('site.details')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        {{-- <h2>@lang('site.view_imported_tools')</h2> --}}
        <h2>@lang('site.View_contract_template_details'){{-- View contract template details --}}</h2>

        {{-- @include('includes.professional_tab_menu') --}}

        <div class="bokcntnt-bdy no-margin-top">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.user_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="blog-right details-right">
                            <div class="blog-box">
                                
                                <div class="blog-dtls">
                                    
                                    <div class="from-field">
                                        <div class="weicome">

                                            <h3><strong> @lang('site.title'): </strong>{{ @$details->getContractTemplatesdata->title }}</h3>
                                            <div class="clearfix"></div>
                                            <strong>@lang('site.Added_by') : </strong> {{ @$details->getContractTemplatesdata['added_by']=='A'?'Admin': ( @$details->getUserData->nick_name ? @$details->getUserData->nick_name : @$details->getUserData->name ) }}
                                            <div class="clearfix"></div>

                                            <strong>@lang('site.Assign_date') : </strong> {{ @$details->contract_start_date }} - {{ @$details->contract_end_date }}
                                            <div class="clearfix"></div>

                                            <strong>@lang('site.description') : </strong> 


                                            <?php
                                                $contractdesc = @$details->getContractTemplatesdata->contract_desc;
                                            ?>
                                           
                                            <?php
                                                foreach($profdata as $key => $val){       
                                                    $contractdesc = str_replace('__'.$key.'__',$val,$contractdesc);            
                                                }
                                            ?>
                                            <?= $contractdesc ?>

                                            <!-- @if(@$details->contract_desc)
                                                < ?= @$details->getContractTemplatesdata->contract_desc ?>
                                            @else    
                                                < ?= @$details->getContractTemplatesdata->contract_desc ?>
                                            @endif  -->   

                                            {{-- <h3>{{ @$form_data->form_title }}</h3>
                                            <strong>{{ @$form_data->category->form_category }}</strong>
                                            <p>{{ @$form_data->form_title }}</p> --}}
                                        </div>                       
                                    </div>

                                    
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
    <script>
        $(document).ready(function(){
            $('#myForm').validate();     
        });
    </script>

@endsection