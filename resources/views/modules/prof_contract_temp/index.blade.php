@extends('layouts.app')
@section('title')
    {{-- @lang('site.my_blogs') --}}
   Contract Template
@endsection
@section('style')
@include('includes.style')
<style>
    #ui-datepicker-div {
        z-index: 1600 !important; /* has to be larger than 1050 */
    }
</style>

@endsection
@section('scripts')
@include('includes.scripts')
<script src="{{URL::to('public/js/chosen.jquery.min.js')}}"></script>
@endsection
@section('header')
@include('includes.professional_header')
<link rel="stylesheet" href="{{URL::to('public/css/chosen.css')}}">
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.contract_template')</h2>


		{{-- <div class="like-tab">
        	<ul>
            	<li><a class="active" href="#">Form tools</a></li>
                <li><a href="#">Tab tow</a></li>
                <li><a href="#">Tab three</a></li>
                <li><a href="#">Tab four</a></li>
            </ul>
        </div> --}}

        @include('includes.professional_tab_menu')

        <div class="bokcntnt-bdy no-margin-top">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>

            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">

                <div class="" style="float: right;">
                    <a class="btn btn-success" href="{{route('prof-contract-temp.create')}}">+ @lang('site.add_new_contract_template'){{--  @lang('site.add_imported_tools') --}}</a>
                </div>


                <?php /*
                <form name="myForm" id="myForm" method="post" action="{{route('my.blog.search')}}">
                    @csrf
                    <div class="from-field">

                        <div class="frmfld">
                            <div class="form-group">
                                <label class="search_label">@lang('site.select') @lang('site.category')</label>
                                <select class="dashboard-type dashboard_select" name="cat_id" id="cat_id">
                                    <option value="">@lang('site.select') @lang('site.category')</option>
                                    @foreach(@$category as $cat)
                                        <option value="{{@$cat->id}}" @if(@$cat->id==@$key['cat_id']) selected @endif>{{@$cat->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="frmfld">
                            <div class="form-group">
                                <label class="search_label">@lang('site.select') @lang('site.status')</label>
                                <select class="dashboard-type dashboard_select" name="status" id="status">
                                    <option value="">@lang('site.select') @lang('site.status')</option>
                                    <option value="A" @if(@$key['status']=='A') selected @endif>@lang('site.active')</option>
                                    <option value="I" @if(@$key['status']=='I') selected @endif>@lang('site.inactive') </option>
                                </select>
                            </div>
                        </div>
                        <div class="frmfld">
                            <button class="banner_subb fmSbm">@lang('site.filter')</button>

                        </div>

                        <div class="frmfld">
                            <a class="btn btn-success" href="{{route('add.blog.post')}}">+@lang('site.add_new_post')</a>

                        </div>
                    </div>
                </form>

                */?>
                <div class="buyer_table">
                    <div class="table-responsive">
                        @if(sizeof(@$contract_templats)>0)
                        <div class="table">
                            <div class="one_row1 hidden-sm-down only_shawo">
                                <div class="cell1 tab_head_sheet">@lang('site.title')</div>
                                {{-- <div class="cell1 tab_head_sheet">Description</div> --}}

                                <div class="cell1 tab_head_sheet">@lang('site.added_by')</div>
                                {{-- <div class="cell1 tab_head_sheet">posted on</div> --}}
                                <div class="cell1 tab_head_sheet">@lang('site.status')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.action')</div>
                            </div>
                            <!--row 1-->
                                @foreach(@$contract_templats as $detail)
                                    <div class="one_row1 small_screen31">
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.no')</span>
                                            <p class="add_ttrr">{{$detail->title}}</p>
                                        </div>

                                        {{-- <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">description</span>
                                            <p class="add_ttrr">
                                                {{ substr(strip_tags(@$form_data->form_dsc),0,20) }}...
                                            </p>
                                        </div> --}}

                                        {{-- @dump(@$blg->desc) --}}
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">added by</span>
                                            <p class="add_ttrr">{{ $detail['added_by']=='A'?'Adionado por': ( @$detail->user->nick_name ? @$detail->user->nick_name : @$detail->user->name ) }}</p>
                                        </div>


                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.status')</span>
                                            <p class="add_ttrr">
                                                {{-- {{ $detail['status'] }} --}}
                                                @if($detail['status'] == "ACTIVE")
                                                    ATIVO
                                                @elseif($detail['status'] == "INACTIVE")
                                                    INATIVA
                                                @endif
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                            @if($detail->status=="ACTIVE")
                                                @if($detail->added_by=="P" && $detail->added_by_id == Auth::id())
                                                    <a href="{{route('prof-contract-temp.edit',[$detail->id])}}" class="acpt editbtn" style="margin: 2px;" title="Edit">@lang('site.edit')</a>
                                                    <a href="javascript:void(0)" onclick="deleteCategory('{{route('prof-contract-temp.destroy',[$detail->id])}}')" title="Delete" class="rjct delbtn"> @lang('site.delete')</a>

                                                    @if($detail->status=="ACTIVE")
                                                        <a href="{{ route('prof.contract.status', ['id'=>$detail->id]) }}" onclick="return confirm(`@lang('client_site.inactivate_contract_temp')`)" class="acpt" title="@lang('site.inactive')">@lang('site.inactive')</a>
                                                    @else
                                                        <a href="{{ route('prof.contract.status', ['id'=>$detail->id]) }}" title="@lang('site.active')" onclick="return confirm(`@lang('client_site.activate_contract_temp')`)" class="acpt rjct">@lang('site.active')</a>
                                                    @endif

                                                @endif
                                                <a href="{{route('prof-contract-temp.show', [$detail->id])}}" class="acpt viewbtn" style="margin: 2px;" title="View Question">@lang('site.view')</a>
                                                <a href="javascript:;" class="show_assign acpt assginbtn" data-title="{{$detail->title}}" data-id="{{ $detail->id }}" style="margin: 2px;">@lang('site.assign')</a>
                                                <a href="{{route('prof.contract.temp.user.assigne.list', ['contract_templats_id'=>@$detail->id])}}" class="acpt view_asgi_btn" style="margin: 2px;">@lang('site.view_assign')</a>
                                            @else
                                                <a href="{{ route('prof.contract.status', ['id'=>$detail->id]) }}" title="@lang('site.active')" onclick="return confirm(`@lang('client_site.activate_contract_temp')`)" class="acpt rjct">@lang('site.active')</a>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach

                        </div>
                        @else
                            <div class="one_row small_screen31">
                                <center><span><h3 class="error">@lang('client_site.oops_no_found')</h3></span></center>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>



<!--assigne Modal -->
<div class="modal fade" id="assignModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">@lang('site.Assign_User')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="myform" method="post" action="{{ route('prof.contract.temp.user.assigne') }}">
                {{ csrf_field() }}
                <div class="modal-body">
                    <h5>Título da ferramenta: <span class="tool-title"></span></h5>

                        <label for="exampleInputEmail1">@lang('site.Select_User')</label>
                        <select name="user_id[]" class="required form-control newdrop required chosen-select" multiple="true" required="" >
                            <option value="">Select</option>
                            @foreach($all_paid_users as $row)
                                <option value="{{$row->user_id}}">{{@$row->userDetails->nick_name ? @$row->userDetails->nick_name : @$row->userDetails->name}}</option>
                            @endforeach
                        </select>


                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom:15px">
                            <div class="your-mail">
                                <label for="datepicker" class="personal-label">@lang('site.Start_date')*</label>
                                <input type="text" autocomplete="off" name="contract_start_date" id="datepicker" class="form-control fdte  required" placeholder="{{__('site.Start_date')}}">
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom:15px">
                            <div class="your-mail">
                                <label for="datepicker1" class="personal-label">@lang('site.end_date')*</label>
                                <input type="text" autocomplete="off" name="contract_end_date" id="datepicker1"  class="form-control sdte  required" placeholder="{{__('site.end_date')}}">
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-6 col-xs-6" style="margin-bottom:15px">
                            <div class="your-mail">
                                <label for="exampleInputEmail1" class="personal-label">@lang('site.installment')*</label>
                                <input type="text" autocomplete="off" name="contact_temp_installment"   class="form-control  required" placeholder="@lang('site.installment')">
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-6 col-sm-6 col-xs-6" style="margin-bottom:15px">
                            <div class="your-mail">
                                <!-- <label for="exampleInputEmail1" class="personal-label">@lang('site.neighborhood')*</label> -->
                                <!-- <input type="text" autocomplete="off" name="contact_temp_neighborhood" class="form-control  required" placeholder="@lang('site.neighborhood')"> -->
                                <label for="exampleInputEmail1" class="personal-label">Local*</label>
                                <input type="text" autocomplete="off" name="contact_temp_neighborhood" class="form-control  required" placeholder="Local">
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-6 col-xs-6" style="margin-bottom:15px">
                            <div class="your-mail">
                                <label for="exampleInputEmail1" class="personal-label">@lang('site.marital_status')*</label>

                                <select class="form-control" name="contact_temp_marital_status" id="marital_status">
                                    <option value="">-{{__('site.select_marital_status')}}-</option>
                                    <option value="Single">@lang('site.mrital_status_single')</option>
                                    <option value="Married">@lang('site.mrital_status_married')</option>
                                    <option value="Widowed">@lang('site.mrital_status_widowed')</option>
                                    <option value="Separated">@lang('site.mrital_status_separated')</option>
                                    <option value="Divorced">@lang('site.mrital_status_divorced')</option>
                                </select>
                            </div>
                        </div>



                    <input type="hidden" name="contract_template_id" class="form_id"> {{-- this calass form id get form id --}}
                </div>

                <div class="modal-footer">
                    <button type="submit" value="submit" class="btn btn-primary">@lang('site.assign')</button>
                </div>

            </form>
        </div>
    </div>
</div>


</section>




@endsection
@section('footer')
@include('includes.footer')

<script>

$(document).ready(function(){
    $('.show_assign').click(function(){
        var title = $(this).data('title');
        var id = $(this).data('id');
        $('.tool-title').html(title);
        $('.form_id').val(id);
        $('#assignModal').modal('show');
    });
});

$(".chosen-select").chosen();

$(document).ready(function(){
        $("#myform").validate();
    });
</script>

<form method="post" id="destroy">
    {{ csrf_field() }}
    {{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
    var confirm = window.confirm('@lang('site.confrm_del_import_tool')');
    if(confirm){
        $("#destroy").attr('action',val);
        $("#destroy").submit();
    }
 }
</script>

<script>
    $(document).ready(function(){
        $('.datepicker').datepicker();
        $("#myform").validate();
    });


    $(function() {

        $("#datepicker").datepicker({
            dateFormat: "dd-mm-yy",
            monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho',
            'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
            defaultDate: new Date(),
            container: '#assignModal modal-body',
            onClose: function( selectedDate ) {
                $( "#datepicker1").datepicker( "option", "minDate", selectedDate );
            }
        });
        $("#datepicker1").datepicker({
            dateFormat: "dd-mm-yy",
            monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho',
            'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
            defaultDate: new Date(),
            onClose: function( selectedDate ) {
                $( "#datepicker" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });


</script>



<style>
    .error{
        color: red !important;
    }
</style>

@endsection
