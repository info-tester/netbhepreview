@extends('layouts.app')
@section('title')
    @lang('site.welcome_to_dashboard')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="message-pagbody">
	<div class="container">
		<div class="mainDasbordInr">
			<div class="row">
				<div class="top-main-profile">
          <div class="mobile_filter"> <i class="fa fa-eye" aria-hidden="true"></i>
              <p>Show all users</p>
          </div>
	  
        <div class="left-list">
          @if(count(@$bookings_to_user)>0 && count(@$bookings)>0)
          <!-- <div class="message-search-heads">
          	<div class="main-search-msg">
          		<input type="text" name="" class="msg-types" placeholder="Search">
          		<button type="submit" class="msg-submit"><img src="{{ URL::to('public/frontend/images/msg-src.png') }}" alt=""></button>
          	</div>
          </div> -->
          <div class="all-chat-lists scrollbars no3">
              @foreach($bookings as $booking)
              <div class="chat-holders chat_btn" data-bkid="{{@$booking->id}}" data-msgid="{{@$booking->getMsgMaster?@$booking->getMsgMaster->id:0}}">
                <span class="holder-image">
                  @if(@$booking->userDetails->id == Auth::id())
                  <img src="{{ @$booking->profDetails->profile_pic ? url('storage/app/public/uploads/profile_pic/'.@$booking->profDetails->profile_pic) : url('public/frontend/images/blank.png') }}" alt="" style="border-radius:50%">
                  @else
                  <img src="{{ @$booking->userDetails->profile_pic ? url('storage/app/public/uploads/profile_pic/'.@$booking->userDetails->profile_pic) : url('public/frontend/images/blank.png') }}" alt="" style="border-radius:50%">
                  @endif
                  <!-- <img class="online-image" src="{{ URL::to('public/frontend/images/online.png') }}" alt=""> -->
                </span>
                <h4>@if(@$booking->userDetails->id == Auth::id()){{@$booking->profDetails->nick_name ? @$booking->profDetails->nick_name : @$booking->profDetails->name}}@else{{@$booking->userDetails->nick_name ? @$booking->userDetails->nick_name : @$booking->userDetails->name}}@endif</h4>
                <h5>{{@$booking->id}}</h5>
                <br>
                <p>{{@$booking->token_no}}</p>
              </div>
              @endforeach
          </div>
          @else
          <p class="no_msg">@lang('client_site.no_history')</p>
          @endif
        </div>

        <div class="right-profle msg_rightpanel" style="margin: 0px; padding: 0px;">
          <div class="main-infor p-relative" style="margin-top: 0px;" id="conv">
              <div class="edit-froms messages-body">
                <div class="common-top">
                  <div class="messsage_heading">
                    <h4>@lang('site.messages')</h4>
                    <p></p>
                  </div>
                  @if(@$booking->profDetails->id == Auth::id())
                  <div class="button_div" id="button_div">
                  </div>
                  @endif
                </div>
                <div class="all-conversation scrollbars no3">
                  <!-- Chat Box-->
                  <div class="col-12 px-0 chat_chat_body">
                    <div class="px-1 py-1 chat-box bg-white">

                    </div>
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
            <div class="row">
              <div class="file-append-chats"></div>
              <div class="typing-text-chats"></div>
            </div>

            <div class="rm_emoji_area new-emoji-top" >
              <ul>
                  
              </ul>
            </div>


              <div class="attachedd-type p-relative" >
                <textarea class="hire-type" placeholder="{{__('site.type_messages_here')}}" style="height: 112px;resize: none;" readonly></textarea>  
                <a href="#" class="rm_emoji new-top disabled">ðŸ™‚</a>
                <div class="mesg-btns">
                  <div class="upload_box edit-upload disabled">
                        <input type="file" id="file" disabled>
                        <label for="file" class="btn-2 disabled">@lang('site.attach_file')</label>
                  </div>
                  <button type="submit" class="reply-btns disabled"><img src="{{ URL::to('public/frontend/images/reply.png') }}" alt=""> @lang('site.reply')</button>
                </div>
              </div>
        </div>
      </div>
			</div>
		</div>
	</div>
</section>
@endsection
@section('footer')
@include('includes.footer')
<script>
    $(document).ready(function() {
        // $("body").delegate(".chat-holders", "click", function() {
        $('.scrollbars').ClassyScroll({
          onscroll: function(){
            console.log("SCrolling");
          },
        });
    });
</script>


<script type="text/javascript">
	$(document).ready(function(){
    $(".profidrop").click(function(){
        $(".profidropdid").slideToggle();
    });
    $(".mobile_filter").click(function(){
        $(".left-list").slideToggle();
    });
    $(".rm_emoji").hide();
});
</script>
 
 
<!--rabin add--> 
<!-- <script type="text/javascript">
	$(document).ready(function() {
		$(".rm_emoji").click(function() {
			$(".rm_emoji_area").slideToggle("slow");
		});
	});
</script> -->
@endsection
