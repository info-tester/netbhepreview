@extends('layouts.app')
@section('title')
  @lang('site.my_account')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
    <link href="{{ URL::to('public/frontend/css/calender.css') }}" rel="stylesheet" type="text/css">
    <script>
    $(function() {
            $("#datepicker").datepicker({dateFormat: "yy-mm-dd",
                monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho',
            'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            monthNamesShort: [ "Janeiro", "Fevereiro", "Março", "Abril",
                   "Maio", "Junho", "Julho", "Agosto", "Setembro",
                   "Outubro", "Novembro", "Dezembro" ],
                   dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
                dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
                dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
                defaultDate: new Date(),
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                showMonthAfterYear: true,
                showWeek: true,
                showAnim: "drop",
                constrainInput: true,
                yearRange: "-90:",
                maxDate:new Date(),
               onClose: function( selectedDate ) {
                   $( "#datepicker1").datepicker( "option", "minDate", selectedDate );
                }
            });
        });
    </script>
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.my_account')</h2>
        <div class="bokcntnt-bdy">
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.user_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">
                    <form action="{{ route('user.update.profile') }}" method="post" name="myForm" id="myForm" enctype="multipart/form-data">
                                @csrf
                        <div class="form_body">

                            <div class="row">
                                    @php
                                        @$name = explode(' ', @Auth::guard('web')->user()->name);
                                    @endphp
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('site.first') @lang('site.Name')</label>
                                        <input type="text" placeholder="@lang('site.enter_first_name')" class="personal-type" value="{{ old('first_name',@$name[0]) }}" name="first_name">
                                    </div>
                                </div>
                                {{-- {{ dd(@$name) }} --}}
                                    @php
                                     $nc = "";
                                    @endphp
                                @for($i=1;$i<sizeof($name);$i++)

                                    @if($i>1)
                                        @php
                                            $nc .= ' '.@$name[$i];
                                        @endphp
                                    @else
                                        @php
                                            $nc .= @$name[$i];
                                        @endphp
                                    @endif

                                @endfor
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('site.last') @lang('site.Name')</label>
                                        <input type="text" placeholder="@lang('site.enter_last_name')" class="personal-type" value="{{ old('last_name',@$nc) }}" name="last_name">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('site.email')</label>
                                        <input type="text" placeholder="@lang('client_site.enter_email')" readonly class="personal-type" value="{{ old('email',@Auth::guard('web')->user()->email) }}" name="email">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('site.phone')</label>
                                        <input type="text" placeholder="@lang('site.phone')" id="phone_no" class="personal-type" value="{{ old('phone_no',@Auth::guard('web')->user()->mobile) }}" name="phone_no">
                                        {{-- <label class="personal-label" id="extraLavelPhone" @if(auth()->user()->country_id == 30) style="display:block" @else style="display:none" @endif>please add your state code with your phone number</label> --}}
                                        <label for="phone_no" generated="true" class="error" id="ph_error"></label>
                                    </div>
                                    {{-- <label class="error" id="ph_error"></label> --}}
                                </div>
                                @if(Auth::guard('web')->user()->is_professional=="N")

                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">@lang('site.dob')</label>
                                            <input type="text" id="datepicker" class="personal-type" value="{{ old('dob',@Auth::guard('web')->user()->dob) }}" name="dob" placeholder="Selecione sua data de nascimento" autocomplete="off" readonly>
                                        </div>
                                    </div>

                                    {{-- @if(auth()->user()->country_id == 30) --}}
                                    <div class="col-lg-4 col-md-6 col-sm-12" id="cpf_no" @if(auth()->user()->country_id == 30) style="display:block" @else style="display:none" @endif>
                                        <div class="form-group">
                                            <label class="personal-label">@lang('site.cpf_no')</label>
                                            <input type="text" placeholder="@lang('site.enter_cpf_no')" class="validate personal-type" value="{{ old('cpf_no',@Auth::guard('web')->user()->cpf_no) }}" name="cpf_no" id="cpf_no_val">
                                        </div>
                                    </div>
                                    {{-- @endif --}}

                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">@lang('site.country')</label>
                                            <select name="country" class="personal-type personal-select" id="country">
                                                <option value="">@lang('site.select') @lang('site.country')</option>
                                                @foreach($country as $cn)
                                                    <option value="{{@$cn->id}}" @if(@Auth::guard('web')->user()->country_id==$cn->id || old('country')==$cn->id) selected @endif>{{@$cn->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">@lang('site.state')</label>
                                            <select name="state" id="state" class="personal-type personal-select">
                                                <option value="">@lang('site.select') @lang('site.state')</option>
                                                @if(@Auth::guard('web')->user()->state_id)
                                                    @foreach($state as $cn)
                                                        @if(@Auth::guard('web')->user()->country_id==$cn->country_id)
                                                            <option value="{{@$cn->id}}"  @if(@Auth::guard('web')->user()->state_id==$cn->id || old('state')==$cn->id) selected @endif>{{@$cn->name}}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                   <div class="col-lg-4 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">@lang('site.city')</label>
                                            <input type="text" placeholder="@lang('site.enter_city')" class="validate personal-type" value="{{  old('city',@Auth::guard('web')->user()->city) }}" name="city">
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">@lang('site.area_code')</label>
                                            <input type="text" placeholder="@lang('site.enter_area_code')" class="validate personal-type" value="{{ old('area_code',@Auth::guard('web')->user()->area_code  ? @Auth::guard('web')->user()->area_code: '') }}" name="area_code">
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">@lang('site.street_name')</label>
                                            <input type="text" placeholder="@lang('site.enter_street')" class="personal-type" value="{{  old('street_name',@Auth::guard('web')->user()->street_name) }}" name="street_name">
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">@lang('site.street_number')</label>
                                            <input type="text" placeholder="@lang('site.enter_street_no')" class="personal-type" value="{{ old('street_number',@Auth::guard('web')->user()->street_number >0 ? @Auth::guard('web')->user()->street_number : '') }}" name="street_number">
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">@lang('site.address_complement')</label>
                                            <input type="text" placeholder="@lang('site.enter_address_complement')" class="personal-type" value="{{ old('complement',@Auth::guard('web')->user()->complement) }}" name="complement">
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">@lang('site.district_name')</label>
                                            <input type="text" placeholder="@lang('site.enter_district')" class="personal-type" value="{{ old('district',@Auth::guard('web')->user()->district) }}" name="district">
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">@lang('site.zipcode')</label>
                                            <input type="text" placeholder="@lang('site.enter_zip')" class="personal-type" value="{{ old('zipcode',@Auth::guard('web')->user()->zipcode) }}" name="zipcode">
                                        </div>
                                    </div>
                                @endif


                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('site.gender')</label>
                                        <select name="gender" class="personal-type personal-select">
                                            <option value="">@lang('site.select') @lang('site.gender')</option>
                                            <option value="M" @if(@Auth::guard('web')->user()->gender=="M" || old('gender')=="M") selected @endif>@lang('site.male')</option>
                                            <option value="F" @if(@Auth::guard('web')->user()->gender=="F" || old('gender')=="F") selected @endif>@lang('site.female')</option>

                                            <option value="B" @if(@Auth::guard('web')->user()->gender=="B" || old('gender')=="B") selected @endif>@lang('site.both')</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('site.time') @lang('site.Timezone')</label>
                                        <select class="personal-type personal-select" name="time_zone">
                                            <option value="">@lang('site.select') @lang('site.timezone')</option>
                                            @foreach($time as $tm)
                                                <option value="{{ old('time_zone',@$tm->timezone_id) }}" @if(@$tm->timezone_id==Auth::guard('web')->user()->timezone_id) selected @endif>{{ @$tm->timezone_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group" style="margin-top: 21px">
                                        <div class="upldd" >
                                             @lang('site.upload_picture') <i class="fa fa-cloud-upload" aria-hidden="true"></i> <input type="file" name="profile_pic" id="imgInp" accept="image/*" onchange="loadFile(event)">
                                        </div>
                                        <label class="recc-sizeinfo"><i class="fa-info-circle fa"></i> Resolução de imagem recomendada 360 x 230</label>
                                        <img id="output" style="width: 100px;margin-top: 16px;" @if(@Auth::guard('web')->user()->profile_pic)  src="{{ URL::to('storage/app/public/uploads/profile_pic/'.Auth::guard('web')->user()->profile_pic) }}" @endif>
                                    </div>
                                </div>
                                {{-- <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('site.about_me')</label>
                                        <!-- <textarea class="personal-type99" placeholder="@lang('client_site.enter_some_text_that_describe_yourself')" name="description">{{ @Auth::guard('web')->user()->description }}</textarea> -->
                                        <textarea class="personal-type99"
                                        placeholder="Qual sua formação? Qual sua especialização? Metodologia e abordagem de trabalho que você usa? Adicione dados interessante e relevante que você gostaria de ressaltar sobre você e seus serviços."
                                        name="description">{{ old('description',@Auth::guard('web')->user()->description) }}</textarea>
                                    </div>
                                </div> --}}
                                @if(Auth::guard('web')->user()->is_credit_card_added=='Y')
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">Cedit Card
                                            <div class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">This is your card details. You can delete your card data click on delete button</span>
                                            </div>
                                        </label><p style="font-size:18px !important">{{@Auth::guard('web')->user()->userCard->first_six}}******{{@Auth::guard('web')->user()->userCard->last_four}}
                                        <a href="{{route('credit.card.delete')}}" onclick="return confirm(`@lang('client_site.delete_credit_card_details')`);"><i class="fa fa-trash-o delet" aria-hidden="true"></i></a>
                                        </p>
                                    </div>
                                </div>
                                @endif

                                {{-- <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">Deseja adicionar um novo cartão de crédito</label>
                                        <select name="creditcard" class="personal-type personal-select valid">
                                            <option value="">Selecionar</option>
                                            <option value="Y" @if(@Auth::guard('web')->user()->creditcard=="Y" || old('creditcard') == "Y") selected @endif>Sim</option>
                                            <option value="N" @if(@Auth::guard('web')->user()->creditcard=="N" || old('creditcard') == "N") selected @endif>Não</option>
                                        </select>
                                    </div>
                                </div> --}}


                                <div class="col-sm-12">
                                    <button type="submit" class="login_submitt">@lang('site.save')</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
<script>
  var loadFile = function(event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
  };
</script>
<style type="text/css">
    .upldd {
    display: block;
    width: auto;
    border-radius: 4px;
    text-align: center;
    background: #9caca9;
    cursor: pointer;
    overflow: hidden;
    padding: 10px 15px;
    font-size: 15px;
    color: #fff;
    cursor: pointer;
    float: left;
    margin-top: 15px;
    font-family: 'Poppins', sans-serif;
    position: relative;
}
.upldd input {
    position: absolute;
    font-size: 50px;
    opacity: 0;
    left: 0;
    top: 0;
    width: 200px;
    cursor: pointer;
}
.upldd:hover{
    background: #1781d2;
}
</style>
    <script>
        $(document).ready(function(){
            var phonemax = phonemin = 9;
            if("{{auth()->user()->country_id}}" != 30){
                phonemax = 8;
                phonemin = 10;
            }
            jQuery.validator.addMethod("alphanumberspace", function(value, element) {
                return this.optional(element) || /^[A-Z0-9 ]*$/.test(value);
            },"Adicione caracteres alfanuméricos ou espaços.");
            $('#myForm').validate({
                rules:{
                    phone_no:{
                        digits:true,
                        minlength:function(){
                            if($('#country').val()==30){
                                return 9;
                            }else{
                                return 8;
                            }
                        },
                        maxlength:function(){
                            if($('#country').val()==30){
                                return 9;

                            }else{
                                return 10;
                            }
                        },
                        required:true
                    },
                    zipcode:{
                        // required:true,
                        minlength:8,
                        maxlength:10
                    },
                    street_number:{
                        alphanumberspace:true,
                        minlength:3,
                        // maxlength:3
                    },
                    country:{
                        required:true
                    },
                    area_code:{
                        digits:true,
                        minlength:2,
                        maxlength:2,
                        required:function(){
                            if($('#country').val()==30){
                                return true;
                            }else{
                                return false;
                            }
                        }
                    },
                }
            });

            $('#myForm1').validate({
                rules:{
                    old_password:{
                        required:true
                    },
                    new_password:{
                        required:true,
                        minlength:8,
                        // maxlength:8
                    },
                    confirm_password:{
                        required:true,
                        minlength:8,
                        // maxlength:8,
                        equalTo:"#new_password"
                    }
                }
            });
        });

        $('#phone_no').change(function(){
            if($('#phone_no').val()!=""){
                var reqData = {
                  'jsonrpc' : '2.0',
                  '_token' : '{{csrf_token()}}',
                  'params' : {
                        'no' : $('#phone_no').val()
                    }
                };
                $.ajax({
                    url: "{{ route('chk.mobile') }}",
                    method: 'post',
                    dataType: 'json',
                    data: reqData,
                    success: function(response){
                        if(response.status==1) {
                            $('#phone_no').val("");
                            $('#ph_error').text('@lang('client_site.phone_number_allready_exist').');
                            $('#ph_error').css('display','Block');
                        }else{
                            $('#ph_error').css('display','none');
                        }

                    }, error: function(error) {
                        toastr.info('@lang('client_site.Try_again_after_sometime')');
                    }
                });
            }
        });
        $('.validate').cpfcnpj();
        $('#country').change(function(){
            if($('#country').val()!=""){
                var reqData = {
                  'jsonrpc' : '2.0',
                  '_token' : '{{csrf_token()}}',
                  'params' : {
                        'cn' : $('#country').val()
                    }
                };
                $.ajax({
                    url: "{{ route('front.get.state') }}",
                    method: 'post',
                    dataType: 'json',
                    data: reqData,
                    success: function(response){
                        if($('#country').val()==30){
                            $('#cpf_no').css('display','block');
                            $('#extraLavelPhone').css('display','block');

                            $('#cpf_no_val').val('');
                        }else{
                            $('#cpf_no').css('display','none');
                            $('#extraLavelPhone').css('display','none');
                            $('#cpf_no_val').val("{{@Auth::guard('web')->user()->cpf_no}}");
                        }
                        if(response.status==1) {
                            var i=0, html="";
                            html = '<option value="">@lang('site.select') @lang('site.state')</option>';
                            for(;i<response.result.length;i++){
                                html+='<option value='+response.result[i].id+'>'+response.result[i].name+'</option>';
                            }
                            $('#state').html(html);
                            $('#state').addClass('valid');
                        }
                        else{

                        }
                    }, error: function(error) {
                        console.error(error);
                    }
                });
            }
        });

    </script>
@endsection
