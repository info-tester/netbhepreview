@extends('layouts.app')
@section('title')
  @lang('site.view_product_order_details')
@endsection
@section('style')
@include('includes.style')
<style>
   .ui-timepicker-standard {
        position: absolute;
        z-index: 9999 !important;
    }
    .main-boxes{
        font-family: 'Poppins', sans-serif;
        color: #252424;
        font-size: 15px;
        width: 100%;
        float: left;
        padding: 5px 0;
        font-weight: 400;
        margin: 0;
    }
    .main-boxes strong{
        font-weight: 500;
        color: #1781d2;
        width: 122px;
        float: left;
    }
    .main-boxes .files {
        display: inline-block;
        float: left;
        width: auto;
        width: calc(100% - 124px);
    }
    .main-boxes .files a{
        margin-right:4px;
    }
    @media(max-width:767px) {
        .main-boxes .files {
            width: 100% !important;
        }
    }
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')
<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('client_site.order_details')</h2>
        <div class="bokcntnt-bdy">
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
            @include('includes.user_sidebar')
            <div class="dshbrd-rghtcntn">
                <a href="{{route('user.products')}}" class="btn btn-primary pull-right mb-1">@lang('site.back')</a>
                <div class="col-md-12 view-page">
                    <p><strong>@lang('site.ordered_by') :</strong> {{ @$order->orderMaster->userDetails->nick_name ?? @$order->orderMaster->userDetails->name}}</p>
                    <p>
                        <strong>@lang('client_site.ordered_on') :</strong>
                        @php $month=[ "0", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ] @endphp
                        {{date('d',strtotime($order->orderMaster->order_date))}}
                        {{$month[(int)date('m',strtotime($order->orderMaster->order_date))]}}
                        {{date('Y',strtotime($order->orderMaster->order_date))}}
                    </p>
                    <p><strong>@lang('site.token') :</strong> {{ @$order->orderMaster->token_no }}</p>
                    <p><strong>@lang('site.product_title') :</strong> {{ @$order->product->title }}</p>
                    <p><strong>@lang('site.product_category') :</strong> {{@$order->product->category->category_name}} > {{@$order->product->subcategory->name}}</p>
                    <p><strong>@lang('site.professional') :</strong> {{@$order->product->professional->nick_name ?? @$order->product->professional->name}}</p>
                    <p><strong>@lang('client_site.amount') :</strong>R$ {{ number_format((float)(@$order->orderMaster->sub_total+@$order->orderMaster->extra_installment_charge), 2, '.', '') }}</p>
                    @if(@$order->extra_installment_charge>0)
                    <p><strong>Instalement Charges :</strong>R$ {{ @$order->orderMaster->extra_installment_charge}}</p>
                    @endif
                    <p>
                        <strong>@lang('client_site.payment_status') :</strong>
                        @if($order->orderMaster->payment_status == 'P') @lang('site.paid')
                        @elseif($order->orderMaster->payment_status == 'I') @lang('site.initiated')
                        @elseif($order->orderMaster->payment_status == 'PR') @lang('site.processing')
                        @elseif($order->orderMaster->payment_status == 'F') @lang('site.failed')
                        @endif
                    </p>
                    <p>
                        <strong>@lang('site.payment_type') :</strong>
                        @if($order->orderMaster->payment_type == 'C') @lang('site.payment_method_card')
                        @elseif($order->orderMaster->payment_type == 'BA') @lang('site.payment_method_bank')
                        @elseif($order->orderMaster->payment_type == 'S') Stripe
                        @elseif($order->orderMaster->payment_type == 'P') Paypal
                        @elseif(@$order->sub_total==0) {{__('site.free_product_purchess')}}
                        @elseif(@$order->orderMaster->wallet == @$order->orderMaster->sub_total)  {{__('site.wallet')}}
                        @endif
                    </p>
                    @if($order->no_of_installment > 1)
                        <p><strong>@lang('site.no_of_installment_for_product') :</strong>{{ @$order->orderMaster->no_of_installment }}</p>
                    @endif

                    @if($order->order_status =='A' && @$order->cancel_request =='N' && $order->orderMaster->payment_status == 'P')
                    <p><strong>@lang('site.status') :</strong>{{__('site.product_active')}}</p>
                    @elseif($order->order_status =='C' && $order->orderMaster->payment_status == 'P' &&  $order->is_cancel_responce=='N')
                    <p><strong>@lang('site.status') :</strong>{{__('site.product_cancel')}}</p>
                    <p><strong>{{__('site.cancellation_reason')}} :</strong> {{@$order->cancellation_reason}}</p>
                    @elseif($order->order_status =='A' && @$order->cancel_request =='Y' && $order->orderMaster->payment_status == 'P' &&  $order->is_cancel_responce=='N')
                    <p><strong>@lang('site.status') :</strong>{{__('site.product_request')}}</p>
                    <p><strong>{{__('site.cancellation_reason')}} :</strong> {{@$order->cancellation_reason}}</p>
                    @elseif($order->order_status =='C' && @$order->cancel_request =='Y' && $order->orderMaster->payment_status == 'P' &&  $order->is_cancel_responce=='A')
                    <p><strong>@lang('site.status') :</strong>{{__('site.product_order_cancelation_accept')}}</p>
                    <p><strong>{{__('site.cancellation_reason')}} :</strong> {{@$order->cancellation_reason}}</p>
                    @elseif($order->order_status =='A' && @$order->cancel_request =='Y' && $order->orderMaster->payment_status == 'P' &&  $order->is_cancel_responce=='R')
                    <p><strong>@lang('site.status') :</strong>{{__('site.product_order_cancelation_reject')}}</p>
                    <p><strong>{{__('site.cancellation_reason')}} :</strong> {{@$order->cancellation_reason}}</p>
                    @else
                    <p><strong>@lang('site.status') :</strong>{{__('site.product_inprogress')}}</p>
                    @endif
                    @if($order->orderMaster->payment_status=='P' && @$order->order_status=='A' && $order->cancel_request=='N'  && toUTCTime(date('Y-m-d H:i:s'))<toUTCTime(date('Y-m-d H:i:s', strtotime($order->product->course_start_date))))
                        <p>
                            <strong>@lang('client_site.course_availibilty') :</strong>
                            {{__('client_site.available_till')}}
                            @php $month=[ "0", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ] @endphp
                            {{date('d',strtotime($order->product->course_start_date))}}
                            {{$month[(int)date('m',strtotime($order->product->course_start_date))]}}
                            {{date('Y',strtotime($order->product->course_start_date))}}
                        </p>
                    @else
                        <p><strong>@lang('client_site.course_availibilty') :</strong>@lang('client_site.course_is_over')</p>
                    @endif
                    @if($order->orderMaster->payment_status=='P' && @$order->order_status=='A' && $order->cancel_request=='N'  && (@$order->percentage < 20 && toUTCTime(date('Y-m-d H:i:s'))<toUTCTime(date('Y-m-d H:i:s', strtotime($order->product->purchase_end_date."+".@$order->product->cancel_day." days")))))
                        <p>
                            <strong>@lang('client_site.cancellation_availibilty') :</strong>
                            {{__('client_site.available_till')}}
                            @php $month=[ "0", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ] @endphp
                            {{date('d',strtotime($order->product->purchase_end_date."+".@$order->product->cancel_day." days"))}}
                            {{$month[(int)date('m',strtotime($order->product->purchase_end_date."+".@$order->product->cancel_day." days"))]}}
                            {{date('Y',strtotime($order->product->purchase_end_date."+".@$order->product->cancel_day." days"))}}
                        </p>
                    @elseif($order->orderMaster->payment_status=='P' && @$order->order_status=='A' && $order->cancel_request=='N'  && (@$order->percentage < 20 && toUTCTime(date('Y-m-d H:i:s'))>toUTCTime(date('Y-m-d H:i:s', strtotime($order->product->purchase_end_date."+".@$order->product->cancel_day." days")))))
                        <p><strong>@lang('client_site.cancellation_availibilty') :</strong>@lang('client_site.expired')</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
<link href="{{ URL::to('public/frontend/css/calender.css') }}" rel="stylesheet" type="text/css">
<script src="{{ URL::to('public/frontend/js/jquery-ui.js') }}"></script>


<script>

    function setVl(tn){
        $('#token_no').val(tn);
    }
    $('#myForm2').validate();
    $('.timepicker').timepicker({
        timeFormat: 'H:mm',
        interval: 15,
        minTime: '00',
        maxTime: '11:00pm',
        startTime: '09:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
$(function() {
        // $('.parent_loader').show();
        $("#date").datepicker({dateFormat: "dd-mm-yy",
           defaultDate: new Date(),
           onClose: function( selectedDate ) {

          }
        });

        $("#datepicker").datepicker({dateFormat: "dd-mm-yy",
           defaultDate: new Date(),
           onClose: function( selectedDate ) {
           $( "#datepicker1").datepicker( "option", "minDate", selectedDate );
          }
        });
    $("#datepicker1").datepicker({dateFormat: "dd-mm-yy",
     defaultDate: new Date(),
          onClose: function( selectedDate ) {
          $( "#datepicker" ).datepicker( "option", "maxDate", selectedDate );
        }
     });

    });
</script>
<script>
    $('#srch').click(function(){
        $('#myForm').submit();
    });
    $('.sbmt').click(function(){
        if($('#date').val()!="" && $('#time').val()!=""){
            $('.parent_loader').show();
            setTimeout(function(){ $('#myForm2').submit(); }, 3000);
        }
        else{
             $('#myForm2').submit();
        }

    });
    $(document).ready(function(){
        $('#myForm').validate({
            rules:{
                phone_no:{
                    digits:true,
                    maxlength:10,
                    minlength:10
                }
            }
        });

        $('#myForm1').validate({
            rules:{
                old_password:{
                    required:true
                },
                new_password:{
                    required:true,
                    minlength:8,
                    maxlength:8
                },
                confirm_password:{
                    required:true,
                    minlength:8,
                    maxlength:8,
                    equalTo:"#new_password"
                }
            }
        });
    });

    $('#phone_no').change(function(){
        if($('#phone_no').val()!=""){
            var reqData = {
              'jsonrpc' : '2.0',
              '_token' : '{{csrf_token()}}',
              'params' : {
                    'no' : $('#phone_no').val()
                }
            };
            $.ajax({
                url: "{{ route('chk.mobile') }}",
                method: 'post',
                dataType: 'json',
                data: reqData,
                success: function(response){
                    if(response.status==1) {
                        $('#phone_no').val("");
                        $('#ph_error').text('@lang('client_site.phone_number_allready_exist')');
                    }

                }, error: function(error) {
                    toastr.info('@lang('client_site.Try_again_after_sometime')');
                }
            });
        }
    });


</script>

@endsection
