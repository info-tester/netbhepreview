@extends('layouts.app')
@section('title')
  @lang('site.view_product_order_details')
@endsection
@section('style')
@include('includes.style')
<style>
   .ui-timepicker-standard {
        position: absolute;
        z-index: 9999 !important;
    }
    .main-boxes{
        font-family: 'Poppins', sans-serif;
        color: #252424;
        font-size: 15px;
        width: 100%;
        float: left;
        padding: 5px 0;
        font-weight: 400;
        margin: 0;
    }
    .main-boxes strong{
        font-weight: 500;
        color: #1781d2;
        width: 122px;
        float: left;
    }
    .main-boxes .files {
        display: inline-block;
        float: left;
        width: auto;
        width: calc(100% - 124px);
    }
    .main-boxes .files a{
        margin-right:4px;
    }
    @media(max-width:767px) {
        .main-boxes .files {
            width: 100% !important;
        }
    }
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')
<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('client_site.order_details')</h2>
        <div class="bokcntnt-bdy">
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
           @include('includes.user_sidebar')
           <div class="dshbrd-rghtcntn">
                <div class="col-md-12 view-page">
                    <p><strong>@lang('site.ordered_by') :</strong> {{ @$order->userDetails->nick_name ?? @$order->userDetails->name}}</p>
                    <p><strong>@lang('client_site.ordered_on') :</strong> {{date('jS F Y', strtotime($order->order_date))}}</p>
                    <p><strong>@lang('client_site.amount') :</strong>R$ {{ number_format((float)(@$order->sub_total+@$order->extra_installment_charge), 2, '.', '') }}</p>
                    @if(@$order->extra_installment_charge>0)
                    <p><strong>Instalement Charges :</strong>R$ {{ @$order->extra_installment_charge}}</p>
                    @endif
                    <p>
                        <strong>@lang('client_site.payment_status') :</strong>
                        @if($order->payment_status == 'P') @lang('site.paid')
                        @elseif($order->payment_status == 'I') @lang('site.initiated')
                        @elseif($order->payment_status == 'PR') @lang('site.processing')
                        @elseif($order->payment_status == 'F') @lang('site.failed')
                        @endif
                    </p>
                    <p>
                        <strong>@lang('site.payment_type') :</strong>
                        @if($order->payment_type == 'C') @lang('site.payment_method_card')
                        @elseif($order->payment_type == 'BA') @lang('site.payment_method_bank')
                        @elseif($order->payment_type == 'S') Stripe
                        @elseif($order->payment_type == 'P') Paypal
                        @endif
                    </p>
                    @if($order->no_of_installment > 1)
                        <p><strong>@lang('site.no_of_installment_for_product') :</strong>{{ @$order->no_of_installment }}</p>
                    @endif
                    <div class="buyer_table">
                        <div class="table-responsive">
                        @if(@$order->orderDetails)
                            <div class="table">
                                <div class="one_row1 hidden-sm-down only_shawo">
                                    <div class="cell1 tab_head_sheet">@lang('site.image')</div>
                                    <div class="cell1 tab_head_sheet">@lang('site.title')</div>
                                    <div class="cell1 tab_head_sheet">@lang('site.product_category')</div>
                                    <div class="cell1 tab_head_sheet">@lang('site.professional_name')</div>
                                    <div class="cell1 tab_head_sheet">@lang('site.price')</div>
                                    <div class="cell1 tab_head_sheet">@lang('site.action')</div>
                                </div>
                                <!--row 1-->
                                @foreach(@$order->orderDetails as $ord)
                                    <div class="one_row1 small_screen31">
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.image')</span>
                                            <img src="{{URL::to('storage/app/public/uploads/product_cover_images/') . '/' . @$ord->product->cover_image}}" height="50" width="60">
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.title')</span>
                                            <p class="add_ttrr">{{@$ord->product->title}}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.product_category')</span>
                                            <p class="add_ttrr">{{@$ord->product->category->category_name}} {{@$ord->product->subcategory->name}}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.professional')</span>
                                            <p class="add_ttrr">{{@$ord->product->professional->nick_name ?? @$ord->product->professional->name}}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.price')</span>
                                            <p class="add_ttrr">{{@$ord->amount}}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                            @if($order->payment_status == 'P')
                                                <a href="{{route('view.course', @$ord->product->id)}}" class="acpt mb-1 w-100 text-center">@lang('site.curriculum')</a>
                                            @endif
                                            @if($ord->is_allowed_certificate == 'Y')
                                            <!-- <a href="{{route('view.certificate',[@$order->id,@$ord->id])}}" class="rjct mb-1 w-100 text-center">View Certificate</a> -->

                                            @endif
                                             <a href="{{route('view.invoice',[@$ord->product->id,@$order->id])}}" class="rjct mb-1 w-100 text-center">@lang('site.view')</a>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        @else
                            <center>
                                <h3 class="error">@lang('site.product_not_found') !</h3>
                            </center>
                        @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
<link href="{{ URL::to('public/frontend/css/calender.css') }}" rel="stylesheet" type="text/css">
<script src="{{ URL::to('public/frontend/js/jquery-ui.js') }}"></script>


<script>

    function setVl(tn){
        $('#token_no').val(tn);
    }
    $('#myForm2').validate();
    $('.timepicker').timepicker({
        timeFormat: 'H:mm',
        interval: 15,
        minTime: '00',
        maxTime: '11:00pm',
        startTime: '09:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
$(function() {
        // $('.parent_loader').show();
        $("#date").datepicker({dateFormat: "dd-mm-yy",
           defaultDate: new Date(),
           onClose: function( selectedDate ) {

          }
        });

        $("#datepicker").datepicker({dateFormat: "dd-mm-yy",
           defaultDate: new Date(),
           onClose: function( selectedDate ) {
           $( "#datepicker1").datepicker( "option", "minDate", selectedDate );
          }
        });
    $("#datepicker1").datepicker({dateFormat: "dd-mm-yy",
     defaultDate: new Date(),
          onClose: function( selectedDate ) {
          $( "#datepicker" ).datepicker( "option", "maxDate", selectedDate );
        }
     });

    });
</script>
<script>
    $('#srch').click(function(){
        $('#myForm').submit();
    });
    $('.sbmt').click(function(){
        if($('#date').val()!="" && $('#time').val()!=""){
            $('.parent_loader').show();
            setTimeout(function(){ $('#myForm2').submit(); }, 3000);
        }
        else{
             $('#myForm2').submit();
        }

    });
    $(document).ready(function(){
        $('#myForm').validate({
            rules:{
                phone_no:{
                    digits:true,
                    maxlength:10,
                    minlength:10
                }
            }
        });

        $('#myForm1').validate({
            rules:{
                old_password:{
                    required:true
                },
                new_password:{
                    required:true,
                    minlength:8,
                    maxlength:8
                },
                confirm_password:{
                    required:true,
                    minlength:8,
                    maxlength:8,
                    equalTo:"#new_password"
                }
            }
        });
    });

    $('#phone_no').change(function(){
        if($('#phone_no').val()!=""){
            var reqData = {
              'jsonrpc' : '2.0',
              '_token' : '{{csrf_token()}}',
              'params' : {
                    'no' : $('#phone_no').val()
                }
            };
            $.ajax({
                url: "{{ route('chk.mobile') }}",
                method: 'post',
                dataType: 'json',
                data: reqData,
                success: function(response){
                    if(response.status==1) {
                        $('#phone_no').val("");
                        $('#ph_error').text('@lang('client_site.phone_number_allready_exist')');
                    }

                }, error: function(error) {
                    toastr.info('@lang('client_site.Try_again_after_sometime')');
                }
            });
        }
    });


</script>

@endsection
