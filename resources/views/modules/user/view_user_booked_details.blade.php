@extends('layouts.app')
@section('title')
  @lang('site.view_booked_user_details')
@endsection
@section('style')
@include('includes.style')
<style>
   .ui-timepicker-standard {
    position: absolute;
    z-index: 9999 !important;
}
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')
<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('client_site.class_details')</h2>
        <div class="bokcntnt-bdy">
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
           @include('includes.user_sidebar')
            <div class="dshbrd-rghtcntn">

                <div class="col-md-12 view-page">
                    <p><strong>@lang('client_site.professional_name') </strong>{{ @$booked->profDetails->nick_name ? @$booked->profDetails->nick_name :@$booked->profDetails->name }}</p>
                    <p><strong>@lang('client_site.date_time') :</strong> {{ @$booked->date }}, {{ date('H:i a' ,strtotime(@$booked->start_time)) }}</p>
                    <p><strong>@lang('client_site.category') :</strong> {{ @$booked->parentCatDetails ? @$booked->parentCatDetails->name: 'N.A' }} / {{@$booked->childCatDetails ? @$booked->childCatDetails->name: 'N.A' }}</p>
                    <p><strong>@lang('client_site.amount') :</strong>R$ {{ @$booked->sub_total +@$booked->extra_installment_charge}}
                        @if($booked->no_of_installment>1)
                        - installment({{$booked->no_of_installment}})
                        @endif
                    </p>
                    <p><strong>@lang('site.booking_type') :</strong> @if(@$booked->booking_type=='C') @lang('site.booking_type_chat') @else @lang('site.booking_type_video') @endif</p>
                    @if(@$booked->video_status=='I' && @$booked->order_status != 'C')
                        <p><strong>@lang('client_site.status') :</strong> @lang('client_site.initiated')</p>
                    @elseif(@$booked->video_status=='C' && @$booked->order_status != 'C')
                        <p><strong>@lang('client_site.status') :</strong> @lang('client_site.complete')</p>
                    @elseif(@$booked->order_status == 'C')
                        <p><strong>@lang('client_site.status') :</strong> @lang('site.order_cancel')</p>
                    @endif
                    @if(@$booked->order_status == 'C')
                    <p><strong>{{__('site.booking_cancelled_by')}} :</strong> @if(@$booked->order_cancelled_by=='P'){{__('site.professional')}} @else{{__('site.User_point')}} @endif</p>
                    <p><strong>{{__('site.cancellation_reason')}} :</strong> {{@$booked->cancellation_reason}}</p>
                    @endif
                    @if(@$booked->getReview->points)
                        <p><strong>@lang('client_site.ratting')</strong>
                            <?php  $tmp = @$booked->getReview->points ?>
                           @for($i=1; $i<= $tmp;$i++)
                                <i class="fa fa-star point re1" aria-hidden="true" data-id="1" data-toggle="tooltip" style="color: #d85450"></i>
                           @endfor
                        </p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
<link href="{{ URL::to('public/frontend/css/calender.css') }}" rel="stylesheet" type="text/css">
<script src="{{ URL::to('public/frontend/js/jquery-ui.js') }}"></script>


<script>

    function setVl(tn){
        $('#token_no').val(tn);
    }
    $('#myForm2').validate();
    $('.timepicker').timepicker({
        timeFormat: 'H:mm',
        interval: 15,
        minTime: '00',
        maxTime: '11:00pm',
        startTime: '09:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
$(function() {
        // $('.parent_loader').show();
        $("#date").datepicker({dateFormat: "dd-mm-yy",
           defaultDate: new Date(),
           onClose: function( selectedDate ) {

          }
        });

        $("#datepicker").datepicker({dateFormat: "dd-mm-yy",
           defaultDate: new Date(),
           onClose: function( selectedDate ) {
           $( "#datepicker1").datepicker( "option", "minDate", selectedDate );
          }
        });
    $("#datepicker1").datepicker({dateFormat: "dd-mm-yy",
     defaultDate: new Date(),
          onClose: function( selectedDate ) {
          $( "#datepicker" ).datepicker( "option", "maxDate", selectedDate );
        }
     });

    });
</script>
<script>
    $('#srch').click(function(){
        $('#myForm').submit();
    });
    $('.sbmt').click(function(){
        if($('#date').val()!="" && $('#time').val()!=""){
            $('.parent_loader').show();
            setTimeout(function(){ $('#myForm2').submit(); }, 3000);
        }
        else{
             $('#myForm2').submit();
        }

    });
    $(document).ready(function(){
        $('#myForm').validate({
            rules:{
                phone_no:{
                    digits:true,
                    maxlength:10,
                    minlength:10
                }
            }
        });

        $('#myForm1').validate({
            rules:{
                old_password:{
                    required:true
                },
                new_password:{
                    required:true,
                    minlength:8,
                    maxlength:8
                },
                confirm_password:{
                    required:true,
                    minlength:8,
                    maxlength:8,
                    equalTo:"#new_password"
                }
            }
        });
    });

    $('#phone_no').change(function(){
        if($('#phone_no').val()!=""){
            var reqData = {
              'jsonrpc' : '2.0',
              '_token' : '{{csrf_token()}}',
              'params' : {
                    'no' : $('#phone_no').val()
                }
            };
            $.ajax({
                url: "{{ route('chk.mobile') }}",
                method: 'post',
                dataType: 'json',
                data: reqData,
                success: function(response){
                    if(response.status==1) {
                        $('#phone_no').val("");
                        $('#ph_error').text('@lang('client_site.phone_number_allready_exist')');
                    }

                }, error: function(error) {
                    toastr.info('@lang('client_site.Try_again_after_sometime')');
                }
            });
        }
    });


</script>

@endsection
