@extends('layouts.app')
@section('title')
  @lang('client_site.review')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('client_site.review')</h2>
        <div class="bokcntnt-bdy">
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
            @include('includes.user_sidebar')
            <div class="dshbrd-rghtcntn professionls">
                <div class="myusr">
                    <div class="form_body">
                        <div class="row">
                            @if(!$no_of_review)
                                <form method="post" action="{{ route('user.post.review') }}" id="review_form" style="width: 100%">
                                    {{csrf_field()}}
                                    <div class="clearfix"></div>
                                    <input type="hidden" class="review_point" value="" name="review_point">
                                    <input type="hidden" value="{{ $booking_id }}" name="booking_id">
                                    <input type="hidden" value="{{ $professional_id }}" name="professional_id">
                                    <div class="col-sm-12 col-lg-12 col-xl-12 col-md-12">
                                        <h4 style="padding-bottom: 20px;">{{__('site.review_name professional_heading')}} {{ @$book->profDetails->nick_name ?@$book->profDetails->nick_name : @$book->profDetails->name }} / {{ @$book->date }}, {{ date('H:i a' ,strtotime(@$book->start_time)) }}</h4>

                                        <p>
                                            <strong style="font-size: 18px;font-weight: 500;margin-right: 10px;">@lang('client_site.give_rating') </strong>
                                            <span>
                                                <b class="review_sstar1" style="font-size: 20px;">
                                                    <i class="fa fa-star point re1" aria-hidden="true" data-id="1" data-toggle="tooltip" title="Very Bad"></i>
                                                    <i class="fa fa-star point re2" aria-hidden="true" data-id="2" data-toggle="tooltip" title="Bad"></i>
                                                    <i class="fa fa-star point re3" aria-hidden="true" data-id="3" data-toggle="tooltip" title="Good"></i>
                                                    <i class="fa fa-star point re4" aria-hidden="true" data-id="4" data-toggle="tooltip" title="Very Good"></i>
                                                    <i class="fa fa-star point re5" aria-hidden="true" data-id="5" data-toggle="tooltip" title="Excellent"></i>
                                                </b>
                                            </span>
                                        </p>
                                        <span class="review_point_span"></span>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12 col-lg-12 col-xl-12 col-md-12">
                                        {{-- <div class="review_posting" >
                                            <h4 style="font-size: 17px;">@lang('client_site.review_heading')</h4>
                                            <div class="form-group" style="width: 100%">
                                                <input type="text" class="dashboard-type required" name="review_heading" id="review_heading" value="" placeholder="">
                                            </div>
                                        </div> --}}
                                        <div class="review_posting">
                                            <h4 style="font-size: 17px;">@lang('client_site.review_comments')</h4>
                                            <div class="form-group" style="width: 100%">
                                                <textarea class="dashboard-type required" id="exampleFormControlTextarea1" name="review_comment" style="height: 200px"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-lg-3 col-md-3 mb-3 ">
                                        <button type="submit" class="banner_subb srch" style="margin-bottom: 30px;">@lang('client_site.submit')</button>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            @else
                            <form method="post" style="width: 100%">
                                <div class="clearfix"></div>
                                {{-- <input type="hidden" class="review_point" value="" name="review_point">
                                <input type="hidden" value="{{ $booking_id }}" name="booking_id">
                                <input type="hidden" value="{{ $professional_id }}" name="professional_id"> --}}
                                <div class="col-sm-12 col-lg-12 col-xl-12 col-md-12">
                                    <h4 style="padding-bottom: 20px;">{{__('site.review_name professional_heading')}} {{ $book->profDetails->nick_name ? $book->profDetails->nick_name : $book->profDetails->name }} / {{ @$book->date }},
                                        {{ date('H:i a' ,strtotime(@$book->start_time)) }}</h4>

                                    <p>
                                        <strong style="font-size: 18px;font-weight: 500;margin-right: 10px;">@lang('client_site.give_rating')
                                        </strong>
                                        <span>
                                            <b class="review_sstar1" style="font-size: 20px;">
                                                <i class="fa fa-star point re1" aria-hidden="true" data-id="1" data-toggle="tooltip"
                                                    title="Very Bad"></i>
                                                <i class="fa fa-star point re2" aria-hidden="true" data-id="2" data-toggle="tooltip"
                                                    title="Bad"></i>
                                                <i class="fa fa-star point re3" aria-hidden="true" data-id="3" data-toggle="tooltip"
                                                    title="Good"></i>
                                                <i class="fa fa-star point re4" aria-hidden="true" data-id="4" data-toggle="tooltip"
                                                    title="Very Good"></i>
                                                <i class="fa fa-star point re5" aria-hidden="true" data-id="5" data-toggle="tooltip"
                                                    title="Excellent"></i>
                                            </b>
                                        </span>
                                    </p>
                                    <span class="review_point_span"></span>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-sm-12 col-lg-12 col-xl-12 col-md-12">
                                    {{-- <div class="review_posting">
                                        <h4 style="font-size: 17px;">@lang('client_site.review_heading')</h4>
                                        <div class="form-group" style="width: 100%">
                                            {{$no_of_review->review_heading}}
                                        </div>
                                    </div> --}}
                                    <div class="review_posting">
                                        <h4 style="font-size: 17px;">@lang('client_site.review_comments')</h4>
                                        <div class="form-group" style="width: 100%">
                                            {{$no_of_review->comments}}
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
@if(@!$no_of_review)
<script type="text/javascript">
    $(document).ready(function(){
        $('#review_form').validate();
        $('#review_form').submit(function(event) {
            var review_point = $('.review_point').val();
            if(review_point == '') {
                $('.review_point_span').html('@lang('client_site.please_select_at_least_one_review')');
                $('.review_point_span').css('color', '#f00');
                return false;
            } else {
                $('.review_point_span').html('');
                return true;
            }
        });
        $('.point').click(function(){
            var point = $(this).data('id');
            $('.point').css('color','#b0b6b6');
            $('.point').attr('data-id1','out');
            for(var i = 1; i <= point; i++)
            {
                $('.re'+i).css('color','#d85450');
                $('.re'+i).attr('data-id1','in');
                $('.review_point').val(point);
                $('.review_point_span').html('');
            }
        });
        $('.point').mouseover(function(){
            var point = $(this).data('id');
            //event.preventDefault();
            $('.point').css('color','#b0b6b6');
            for(var i = 1; i <= point; i++)
            {

                $('.re'+i).css('color','#d85450');

            }
            $('.point').each(function(){
                if($(this).attr('data-id1') == 'in')
                {
                    $(this).css('color','#d85450');
                }
            });
        });
        $('.point').mouseout(function(){
            $('.point').each(function(){
                if($(this).attr('data-id1') == 'in')
                {
                    $(this).css('color','#d85450');
                }
                else
                {
                    $(this).css('color','#b0b6b6');
                }
            });
        });
    });
</script>
@else
<script>
    // $('.point').click(function(){
    var point = {{$no_of_review->points}};
    $('.point').css('color','#b0b6b6');
    $('.point').attr('data-id1','out');
    for(var i = 1; i <= point; i++) {
        $('.re'+i).css('color','#d85450');
        $('.re'+i).attr('data-id1','in');
        $('.review_point').val(point); $('.review_point_span').html('');
    }
    // });
    console.log('1');
</script>
@endif
@endsection
