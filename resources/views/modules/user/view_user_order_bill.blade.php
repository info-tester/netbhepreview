@extends('layouts.app')
@section('title')
  @lang('site.view_product_order_details')
@endsection
@section('style')
@include('includes.style')
<style>
   .ui-timepicker-standard {
        position: absolute;
        z-index: 9999 !important;
    }
    .main-boxes{
        font-family: 'Poppins', sans-serif;
        color: #252424;
        font-size: 15px;
        width: 100%;
        float: left;
        padding: 5px 0;
        font-weight: 400;
        margin: 0;
    }
    .main-boxes strong{
        font-weight: 500;
        color: #1781d2;
        width: 122px;
        float: left;
    }
    .main-boxes .files {
        display: inline-block;
        float: left;
        width: auto;
        width: calc(100% - 124px);
    }
    .main-boxes .files a{
        margin-right:4px;
    }
    @media(max-width:767px) {
        .main-boxes .files {
            width: 100% !important;
        }
    }
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')
<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('client_site.order_details')</h2>
        <div class="bokcntnt-bdy">
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
           @include('includes.user_sidebar')
            <div class="dshbrd-rghtcntn">

                <div class="col-md-12 view-page">
                    <p><strong>@lang('site.title') :</strong> {{ @$order->product->title }}</p>
                    <p><strong>@lang('client_site.amount') :</strong>R$ {{ @$order->amount }}</p>
                    <p><strong>@lang('client_site.ordered_on') :</strong> {{ @$order->created_at->format('jS F, y') }}</p>
                    <div class="mt-2">
                        <a href="{{route('view.course', @$order->product->id)}}" class="btn btn-primary pull-right">@lang('site.curriculum')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
<link href="{{ URL::to('public/frontend/css/calender.css') }}" rel="stylesheet" type="text/css">
<script src="{{ URL::to('public/frontend/js/jquery-ui.js') }}"></script>
@endsection
