@extends('layouts.app')
@section('title')
  @lang('client_site.upcomming_class')
@endsection
@section('style')
@include('includes.style')
<style>
   .ui-timepicker-standard {
    position: absolute;
    z-index: 9999 !important;
}
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')
<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('client_site.upcomming_class')</h2>
        <div class="bokcntnt-bdy">
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
            @include('includes.user_sidebar')
            <div class="dshbrd-rghtcntn">
                <form id="myForm" name="myForm" action="{{ route('user.upcomming.class') }}" method="post">
                    @csrf
                    <div class="from-field">
                        <div class="frmfld">
                            <div class="form-group">
                                <label class="search_label">@lang('client_site.professional')</label>
                                <input type="text" name="professional" class="dashboard-type" placeholder="Write professional name" value="{{ @$key['professional'] }}">
                            </div>
                        </div>
                        <div class="frmfld">
                            <div class="form-group">
                                <label class="search_label">@lang('client_site.from_date')</label>
                                <input type="text" id="datepicker" name="from_date" class="dashboard-type" placeholder="Select Date" value="{{ @$key['from_date'] }}">
                                <img class="pstn" src="{{ URL::to('public/frontend/images/clndr.png') }}">
                            </div>
                        </div>
                        <div class="frmfld">
                            <div class="form-group">
                                <label class="search_label">@lang('client_site.to_date')</label>
                                <input type="text" id="datepicker1" name="to_date" class="dashboard-type" placeholder="Select Date" value="{{ @$key['to_date'] }}">
                                <img class="pstn" src="{{ URL::to('public/frontend/images/clndr.png') }}">
                            </div>
                        </div>
                        <div class="frmfld">
                            <button class="banner_subb srch">@lang('client_site.filter')</button>
                        </div>
                    </div>
                </form>
                @if(count(@$upcommingList)>0)
                <div class="buyer_table">
                    <div class="table-responsive">
                        <div class="table">
                            <div class="one_row1 hidden-sm-down only_shawo">
                                <div class="cell1 tab_head_sheet">@lang('client_site.name')</div>
                                <div class="cell1 tab_head_sheet">@lang('client_site.professional')</div>
                                <div class="cell1 tab_head_sheet">@lang('client_site.date_time')</div>
                                <div class="cell1 tab_head_sheet">@lang('client_site.duration')</div>
                                <div class="cell1 tab_head_sheet">@lang('client_site.topic')</div>
                                <div class="cell1 tab_head_sheet">{{__('site.payment_type')}}</div>
                                <div class="cell1 tab_head_sheet">@lang('site.payment_status')</div>
                                <div class="cell1 tab_head_sheet">@lang('client_site.action')</div>
                            </div>
                            <!--row 1-->
                            
                                @foreach(@$upcommingList as $row)                        
                                <div class="one_row1 small_screen31">
                                    <div class="cell1 tab_head_sheet_1">
                                        <span class="W55_1">@lang('client_site.id')</span>
                                        <p class="add_ttrr">{{ @$row->userDetails->nick_name ? @$row->userDetails->nick_name : @$row->userDetails->name }}</p>
                                    </div>
                                    <div class="cell1 tab_head_sheet_1">
                                        <span class="W55_1">@lang('client_site.professional')</span>
                                        <p class="add_ttrr">{{ @$row->profDetails->nick_name ? @$row->profDetails->nick_name : @$row->profDetails->name }}</p>
                                    </div>
                                    <div class="cell1 tab_head_sheet_1">
                                        <span class="W55_1">@lang('client_site.date_time')</span>
                                        <p class="add_ttrr">{{ @$row->date }}, {{ date('H:i a' ,strtotime(@$row->start_time)) }}</p>
                                    </div>
                                    <div class="cell1 tab_head_sheet_1">
                                        <span class="W55_1">@lang('client_site.duration')</span>
                                        <p class="add_ttrr">{{ @$row->duration }} @lang('client_site.minutes')</p>
                                    </div>
                                    <div class="cell1 tab_head_sheet_1">
                                        <span class="W55_1">@lang('client_site.topic')</span>
                                        <p class="add_ttrr">{{ @$row->parentCatDetails->name }} <br>{{ @$bk->childCatDetails->name }}</p>
                                    </div>
                                    <div class="cell1 tab_head_sheet_1">
                                        <span class="W55_1">{{__('site.payment_type')}}</span>
                                        <p class="add_ttrr">
                                            @if(@$bk->payment_type == 'C') {{__('site.payment_method_card')}}
                                            @elseif(@$bk->payment_type == 'BA') {{__('site.payment_method_bank')}}
                                            @elseif(@$bk->amount == 0){{__('site.free_session')}}
                                            @endif
                                        </p>
                                    </div>
                                    <div class="cell1 tab_head_sheet_1">
                                        <span class="W55_1">@lang('client_site.payment_status')</span>
                                        <p class="add_ttrr">
                                            @if(@$bk->payment_status == 'I') {{__('site.Payment_initiated')}} @elseif(@$bk->payment_status == 'P')
                                            {{__('site.Payment_paid')}}
                                            @elseif(@$bk->payment_status == 'F') {{__('site.Payment_failed')}} @elseif(@$bk->payment_status == 'PR')
                                            {{__('site.Payment_processing')}}
                                            @endif
                                        </p>
                                    </div>
                                    <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                        <span class="W55_1">@lang('client_site.action')</span>
                                        <a href="javascripts:;" class="rjct msg_prof" data-user_id="
                                            {{ $row->user_id }}" data-prof_id="{{ $row->professional_id }}">@lang('client_site.message')</a>
                                        <a href="{{ route('user.review', ['booking_id' => $row->id, 'professional_id' => $row->professional_id]) }}" class="rjct">@lang('client_site.review')</a>
                                    </div>
                                </div>
                                @endforeach
                        </div>
                    </div>
                </div>
                 @else
                    <center>
                        <h3 class="error">@lang('client_site.booking_details_not_found')</h3>
                    </center>            
                @endif
            </div>
        </div>
    </div>

    {{-- modal --}}

      <div class="modal fade" id="upcommingModal">
            <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
              
                <!-- Modal Header -->
                <div class="modal-header">
                  <h4 class="modal-title">@lang('client_site.reschedule_appoinment') </h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                
                <!-- Modal body -->
                <div class="modal-body">
                    <form name="myForm2" id="myForm2" method="post" action="{{ route('user.booking.reschedule') }}">
                        @csrf
                        <input type="hidden" name="token_no" id="token_no">
                        <div class="form-body">
                          <div class="row">
                              <div class="col-md-4 col-sm-4 col-xs-4">
                                  <div class="form-group">
                                      <label>@lang('client_site.date')</label>
                                      <input type="text" class="required form-control" name="date" id="date">
                                  </div>
                              </div>

                              <div class="col-md-4 col-sm-4 col-xs-4">
                                  <div class="form-group">
                                      <label>@lang('client_site.time')</label>
                                      <input type="text" class="required form-control timepicker" name="time" id="time">
                                  </div>
                              </div>
                          </div>
                        </div>
                    </form>
                </div>
                
                <!-- Modal footer -->
                <div class="modal-footer">
                  <button type="button" class="btn btn-primary sbmt">@lang('client_site.submit')</button>

                  <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('client_site.close')</button>
                </div>
                
              </div>
            </div>
      </div>

    {{-- modal --}}

</section>
@endsection
@section('footer')
@include('includes.footer')
<link href="{{ URL::to('public/frontend/css/calender.css') }}" rel="stylesheet" type="text/css">
<script src="{{ URL::to('public/frontend/js/jquery-ui.js') }}"></script>


<script>

    function setVl(tn){
        $('#token_no').val(tn);
    }
    $('#myForm2').validate();
    $('.timepicker').timepicker({
        timeFormat: 'H:mm',
        interval: 15,
        minTime: '00',
        maxTime: '11:00pm',
        startTime: '09:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
$(function() {
        // $('.parent_loader').show();  
        $("#date").datepicker({dateFormat: "dd-mm-yy",
           defaultDate: new Date(),
           onClose: function( selectedDate ) {
           
          }
        });

        $("#datepicker").datepicker({dateFormat: "dd-mm-yy",
           defaultDate: new Date(),
           onClose: function( selectedDate ) {
           $( "#datepicker1").datepicker( "option", "minDate", selectedDate );
          }
        }); 
    $("#datepicker1").datepicker({dateFormat: "dd-mm-yy",
     defaultDate: new Date(),
          onClose: function( selectedDate ) {
          $( "#datepicker" ).datepicker( "option", "maxDate", selectedDate );
        }
     }); 
        
    });
</script>
<script type="text/javascript">
    $(document).ready(function(e){
        $('.msg_prof').on('click', function(){
            $('#upcommingModal').modal('show');
        })
    })
</script>
@endsection