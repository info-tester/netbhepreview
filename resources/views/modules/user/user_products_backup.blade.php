@extends('layouts.app')
@section('title')
  @lang('site.my_booking')
@endsection
@section('style')
@include('includes.style')
<style>
   .ui-timepicker-standard {
    position: absolute;
    z-index: 9999 !important;
}
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')
<section class="bkng-hstrybdy">
    <div class="container custom_container">
        <h2>@lang('site.my_purchases')</h2>
        <div class="bokcntnt-bdy">
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @if(auth()->user()->menu_type == 'P')
                @include('includes.user_sidebar')
            @elseif(auth()->user()->menu_type == 'U')
                @include('includes.user_sidebar')
            @endif
            <div class="dshbrd-rghtcntn">
                <form name="myForm" id="myForm" method="post" action="{{route('user.products.search')}}">
                    @csrf
                    <div class="from-field">
                        <div class="row">
                            <div class="col-lg-7">
                                <label class="search_label">@lang('site.keyword')</label>
                                <input type="text" class="form-control" name="keyword" id="keyword" value="{{@$key['keyword']}}">
                            </div>
                            <div class="col-lg-2"></div>
                        </div>
                        <div class="frmfld">
                            <div class="form-group">
                               <label class="search_label">@lang('site.select') @lang('site.status')</label>
                                <select class="dashboard-type dashboard_select" name="status" id="status">
                                    <option value="">@lang('site.select') @lang('site.status')</option>
                                    <option value="I" @if(@$key['status']=='I') selected @endif>@lang('site.initiated') </option>
                                    <option value="PR" @if(@$key['status']=='PR') selected @endif>@lang('site.processing') </option>
                                    <option value="P" @if(@$key['status']=='P') selected @endif>@lang('site.paid')</option>
                                    <option value="F" @if(@$key['status']=='F') selected @endif>@lang('site.failed') </option>
                                </select>
                            </div>
                        </div>

                        <div class="frmfld">
                            <button class="banner_subb fmSbm">@lang('site.filter')</button>
                        </div>
                    </div>

                </form>
                <div class="buyer_table">
                    <div class="table-responsive">
                    @if(sizeof(@$orders)>0)
                        <div class="table">
                            <div class="one_row1 hidden-sm-down only_shawo">
                                <!-- <div class="cell1 tab_head_sheet">@lang('site.id').</div> -->
                                <div class="cell1 tab_head_sheet">@lang('site.image')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.title')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.price')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.evaluation_percentage')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.purchase_date')</div>
                                <div class="cell1 tab_head_sheet">{{--{{__('site.payment_status_1')}}--}}{{__('site.payment_status_2')}}
                                </div>
                                <div class="cell1 tab_head_sheet">{{__('site.status')}}</div>
                                <div class="cell1 tab_head_sheet">@lang('site.action')</div>
                            </div>
                            <!--row 1-->
                                @php
                                    $i=1;
                                @endphp
                                @foreach(@$orders as $order)
                                  @foreach(@$order->orders_det as $ord)
                                    <div class="one_row1 small_screen31">

                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('site.image')</span>
                                                <p class="add_ttrr">
                                                    <a href="{{route('product.details',@$ord->product->slug)}}">
                                                        <img src="{{URL::to('storage/app/public/uploads/product_cover_images/') . '/' . @$ord->product->cover_image}}" height="50" width="60">
                                                    </a>
                                                </p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('site.title')</span>
                                                <p class="add_ttrr">
                                                    <a href="{{route('product.details',@$ord->product->slug)}}">
                                                        {{@$ord->product->title}}
                                                    </a>
                                                </p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('site.price')</span>
                                                <p class="add_ttrr">{{ number_format((float)(@$ord->amount), 2, '.', '') }}</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('site.evaluation_percentage')</span>
                                                <p class="add_ttrr text-center">{{@$ord->percentage}}%</p>
                                            </div>
                                            <!-- <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('site.title')</span>
                                                <p class="add_ttrr">{{@$order->token_no}}</p>
                                            </div> -->
                                        <!-- <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.price')</span>
                                            <p class="add_ttrr">{{ number_format((float)(@$order->sub_total+@$order->extra_installment_charge), 2, '.', '') }}</p>
                                        </div> -->
                                         <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.purchase_date')</span>
                                            <p class="add_ttrr">
                                                @php $month=[ "0", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ] @endphp
                                                {{date('d',strtotime(@$order->order_date))}}
                                                {{$month[(int)date('m',strtotime(@$order->order_date))]}}
                                                {{date('Y',strtotime(@$order->order_date))}}
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.payment_status')</span>
                                            <p class="add_ttrr">
                                                @if($order->payment_status == 'P') @lang('site.paid')
                                                @elseif($order->payment_status == 'I') @lang('site.initiated')
                                                @elseif($order->payment_status == 'PR') @lang('site.processing')
                                                @elseif($order->payment_status == 'F') @lang('site.failed')
                                                @endif
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.status')</span>
                                            <p class="add_ttrr">
                                                @if(@$ord->order_status == 'A' && @$ord->cancel_request =='N' && $order->payment_status == 'P') {{__('site.product_active')}}
                                                @elseif(@$ord->order_status == 'C' && $order->payment_status == 'P' && @$ord->is_cancel_responce=='N'){{__('site.product_cancel')}}
                                                @elseif(@$ord->order_status == 'C' && $order->payment_status == 'P' && @$ord->is_cancel_responce=='A'){{__('site.product_order_cancelation_accept')}}
                                                @elseif($order->payment_status == 'P' && @$ord->is_cancel_responce=='R'){{__('site.product_order_cancelation_reject')}}
                                                @elseif(@$ord->order_status == 'A' && @$ord->cancel_request =='Y' && $order->payment_status == 'P') {{__('site.product_request')}}
                                                @else
                                                {{__('site.product_inprogress')}}
                                                @endif
                                            </p>
                                        </div>



                                        <div class="cell1 tab_head_sheet_1" style="min-width: 100px;">
                                            <a href="{{route('order.details',[@$order->id,@$ord->product->id])}}" class="acpt mb-1 w-100 text-center">@lang('site.view_order')</a>
                                            @if(@$order->payment_type=='BA' && @$order->payment_document==null)
                                                <a href="{{ route('product.order.store.success', ['id' => @$order->token_no]) }}" class="rjct mb-1 w-100 text-center">{{__('site.upload_document')}}</a>
                                            @endif
                                            @if($order->payment_status == 'P' && $ord->is_cancel_responce != 'A')
                                                <a href="{{route('view.course', @$ord->product->id)}}" class="acpt assginbtn mb-1 w-100 text-center">@lang('client_site.view_course')</a>
                                            @endif
                                            @if(@$ord->percentage > 0)
                                                <a href="{{route('rate.review.product',['slug'=> @$ord->product->slug])}}" class="acpt editbtn mb-1">@lang('client_site.rate_product')</a>
                                            @endif
                                            @if(@$ord->is_allowed_certificate == 'Y' && @$ord->percentage == 100)
                                                <a href="{{route('view.certificate',[@$ord->id])}}" class="rjct mb-1 w-100 text-center">@lang('client_site.view_certificate')</a>
                                                @endif
                                            @if($order->payment_status=='P' && @$ord->order_status=='A' && $ord->cancel_request=='N'  && (@$ord->percentage < 20 && toUTCTime(date('Y-m-d H:i:s'))<toUTCTime(date('Y-m-d H:i:s', strtotime($ord->product->purchase_end_date."+".@$ord->product->cancel_day." days")))))
                                            <a href="javascript:;" class="rjct mb-1 w-100 text-center cancelbtn" data-token="{{$order->token_no}}" data-id="{{$ord->id}}">{{__('site.product_order_cancel')}}</a>
                                            @endif

                                        </div>
                                    </div>
                                    @php
                                        $i++;
                                    @endphp
                                  @endforeach
                                @endforeach

                        </div>
                    @else
                        <center>
                            <h3 class="error">@lang('site.product_not_found') !</h3>
                        </center>
                    @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- modal --}}

      <div class="modal fade" id="myModal">
            <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                  <h4 class="modal-title">@lang('site.reschedule') @lang('site.appoinment') </h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form name="myForm2" id="myForm2" method="post" action="{{ route('user.booking.reschedule') }}">
                        @csrf
                        <input type="hidden" name="token_no" id="token_no">
                        <input type="hidden" name="professional_id" id="professional_id">
                        <input type="hidden" id="start-time">
                        <input type="hidden" id="end-time">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>@lang('site.date')</label>
                                        <input type="text" class="required form-control" name="date" id="date" value="">
                                    </div>
                                </div>

                              <div class="col-md-6 col-sm-6 col-xs-12">
                                {{-- @dump($order->productDetails) --}}
                                  <div class="form-group">
                                      <label>@lang('site.time')</label>
                                      <select class="required form-control " name="time" id="time">
                                      {{-- <option value="">Select Time Slot</option>
                                      @foreach($booking as $val)
                                        <option value="{{ $val->start_time.'-'.$val->end_time }}">{{ $val->start_time }}-{{ $val->end_time }}</option>
                                      @endforeach --}}
                                      </select>
                                      <span id="time_err" style="font-weight: bold;color: red;"></span>
                                  </div>
                              </div>
                          </div>
                        </div>
                    </form>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                  <button type="button" class="btn btn-primary sbmt">@lang('site.submit')</button>

                  <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('site.close')</button>
                </div>

              </div>
            </div>
      </div>

    {{-- modal --}}
    <div class="modal fade" id="modalcancel">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
              <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title">{{__('site.cancelation_request')}} </h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form name="myForm3" id="myForm3" method="post">
                    @csrf
                    <input type="hidden" name="token_no" id="token_no">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group" style="width: 100%;">
                                    <label>{{__('site.cancellation_reason')}}</label>
                                    <input type="text" class="required form-control" name="reason" value="" >
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <input type="submit" value="@lang('client_site.cancel')" class="btn btn-primary">
                                </div>
                            </div>
                      </div>
                    </div>
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('site.close')</button>
            </div>

          </div>
        </div>
  </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
<link href="{{ URL::to('public/frontend/css/calender.css') }}" rel="stylesheet" type="text/css">
<script src="{{ URL::to('public/frontend/js/jquery-ui.js') }}"></script>
<script src="{{ URL::to('public/frontend/js/moment.min.js') }}"></script>


<script>
    function setVl(tn, dt,tm, pid, startTime, endTime){
        $('#token_no').val(tn);
        $('#date').val(dt);
        $('#professional_id').val(pid);
        $('#start-time').val(startTime);
        $('#end-time').val(endTime);
        // $('#time').html('<option value="'+tm+'" selected>'+tm+'</option>');
        $('#date').trigger('change');
    }

    $('#date').change(function(){
        if($("#date").val()!="") {
            var html='';
            var time_formate='';
            var from_time='';
            var to_time='';
            var date = $(this).val();
            var professional_id = $('#professional_id').val();
            html ='<option value="">@lang('site.select_time_slot')</option>';
            $.ajax({
                url: '{{ route("rescheduled.time.list") }}',
                type:"GET",
                data: {
                    date: date,
                    user_id: professional_id,
                    start: $('#start-time').val(),
                    end: $('#end-time').val(),
                },
                success: function(response) {
                    console.log(response);
                    response.forEach(function(item, index){
                        slot_start = (item.slot_start).substr(0,5);
                        slot_end = (item.slot_end).substr(0,5);
                        time_formate = slot_start.concat('-', slot_end);
                        // alert(time_formate);
                        html+='<option value="'+time_formate+'">'+time_formate+'</option>';
                    });
                    $('#time').html(html);
                },
            });
        }
    });
    $('#myForm2').validate();
    $('.timepicker').timepicker({
        timeFormat: 'H:mm',
        interval: 15,
        minTime: '00',
        maxTime: '11:00pm',
        startTime: '09:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
    $(function() {
        $("#date").datepicker({
            dateFormat: "dd-mm-yy",
            defaultDate: new Date(),
            minDate:new Date(),
        });

        $("#datepicker").datepicker({dateFormat: "dd-mm-yy",
            monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
            defaultDate: new Date(),
            onClose: function( selectedDate ) {
                $( "#datepicker1").datepicker( "option", "minDate", selectedDate );
            }
        });
    $("#datepicker1").datepicker({dateFormat: "dd-mm-yy",
         monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho',
            'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
     defaultDate: new Date(),
          onClose: function( selectedDate ) {
          $( "#datepicker" ).datepicker( "option", "maxDate", selectedDate );
        }
     });

    });
</script>
<script>
    $('#srch').click(function(){
        $('#myForm').submit();
    });
    $('.sbmt').click(function(){
        if($('#date').val()!="" && $('#time').val()!=""){
            $('.parent_loader').show();
            setTimeout(function(){ $('#myForm2').submit(); }, 3000);
        }
        else{
             $('#myForm2').submit();
        }

    });
    $('#time').click(function(){
        if($('#date').val()==''){
            $(this).val('');
            $('#time_err').html('select date first');
            return false;
        }
        else{
            $('#time_err').html('');
        }
    });
    $(document).ready(function(){
        $('#myForm').validate({
            rules:{
                phone_no:{
                    digits:true,
                    maxlength:10,
                    minlength:10
                }
            }
        });

        $('#myForm1').validate({
            rules:{
                old_password:{
                    required:true
                },
                new_password:{
                    required:true,
                    minlength:8,
                    maxlength:8
                },
                confirm_password:{
                    required:true,
                    minlength:8,
                    maxlength:8,
                    equalTo:"#new_password"
                }
            }
        });
        $('#myForm3').validate({});
    });

    $('#phone_no').change(function(){
        if($('#phone_no').val()!=""){
            var reqData = {
              'jsonrpc' : '2.0',
              '_token' : '{{csrf_token()}}',
              'params' : {
                    'no' : $('#phone_no').val()
                }
            };
            $.ajax({
                url: "{{ route('chk.mobile') }}",
                method: 'post',
                dataType: 'json',
                data: reqData,
                success: function(response){
                    if(response.status==1) {
                        $('#phone_no').val("");
                        $('#ph_error').text('@lang('client_site.phone_number_allready_exist').');
                    }

                }, error: function(error) {
                    toastr.info('@lang('client_site.Try_again_after_sometime')');
                }
            });
        }
    });
    $('.cancelbtn').click(function(){
        var id= $(this).data('id');
        var token= $(this).data('token');
        $('#modalcancel').modal('show');
        var url= '{{route('home')}}/product-order-cancel-request/'+token+'/'+id;
        $('#myForm3').attr('action', url)
    });


</script>

@endsection
