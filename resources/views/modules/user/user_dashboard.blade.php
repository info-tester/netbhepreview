@extends('layouts.app')
@section('title')
  @lang('site.welcome_to_dashboard')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container custom_container">
        <h2>@lang('site.dashboard')</h2>
        <div class="bokcntnt-bdy">
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>

                @include('includes.user_sidebar')



            <div class="dshbrd-rghtcntn">
                <div class="d-flex banner-side">
                    <div class="banner_side_info">
                        <span><img src="{{ URL::to('public/frontend/images/bannerimg2.png') }}"></span>
                        @if(auth()->user()->is_join_affiliate=='N')
                        <p >{{__('site.aff_join_text')}}, <a href="{{ route('request.affiliate.aprove') }}">{{__('site.click_link')}}</a></p>
                        @else
                        <p >{{__('site.aff_after_join_text')}}, <a href="{{route('affiliate.products.search.filter')}}">{{__('site.click_link')}}</a></p>
                        @endif
                        {{-- <p> You can join our affiliate program to</p> --}}
                    </div>
                    <div class="banner_side_info">
                        <span><img src="{{ URL::to('public/frontend/images/bannerimg1.png') }}"></span>
                        @if(auth()->user()->is_video_affiliate=='N')
                        <p>{{__('site.video_aff_join_text')}}, <a href="{{ route('request.video.affiliate.aprove') }}">{{__('site.click_link')}}</a></p>
                        @else
                        <p>{{__('site.video_aff_after_join_text')}}, <a href="{{route('video.affiliate.earning.list')}}">{{__('site.click_link')}}</a></p>
                        @endif
                        {{-- <p> You can join our affiliate program to</p> --}}
                    </div>
                </div>
                {{-- @if(auth()->user()->is_join_affiliate=='N')
                <center><p class="alert alert-info">You can join our affiliate program to , <a href="{{ route('request.affiliate.aprove') }}">Click this link</a></p></center>
                @else
                <center><p class="alert alert-info">Your account associated with our affiliate program to view product click this link , <a href="{{route('affiliate.products.search.filter')}}">Click this link</a></p></center>
                @endif
                @if(auth()->user()->is_video_affiliate=='N')
                <center><p class="alert alert-info">You can join our video affiliate program to , <a href="{{ route('request.video.affiliate.aprove') }}">Click this link</a></p></center>
                @else
                <center><p class="alert alert-info">Your account associated with our video affiliate program view earning, <a href="{{route('video.affiliate.earning.list')}}">Click this link</a></p></center>
                @endif --}}



                @if(sizeof(@$booking)>0)
                    <div class="buyer_table" style="margin-top:0px;">
                        <h3 class="up-class">@lang('site.upcoming_classes')</h3>
                        <div class="table-responsive">
                            <div class="table ">
                                <div class="one_row1 hidden-sm-down only_shawo">
                                    <div class="cell1 tab_head_sheet">Token no</div>
                                    <div class="cell1 tab_head_sheet">@lang('site.professional')</div>
                                    <div class="cell1 tab_head_sheet">@lang('site.date')/@lang('site.time')</div>
                                    <div class="cell1 tab_head_sheet">@lang('site.duration')</div>
                                    <div class="cell1 tab_head_sheet">@lang('site.topic')</div>
                                    <div class="cell1 tab_head_sheet">{{__('site.payment_type_1')}}<br>{{__('site.payment_type_2')}}</div>
                                    <div class="cell1 tab_head_sheet">{{__('site.payment_status_1')}}<br>{{__('site.payment_status_2')}}</div>
                                    {{-- <div class="cell1 tab_head_sheet">Status</div> --}}
                                    <div class="cell1 tab_head_sheet">@lang('site.action')</div>
                                </div>
                                <!--row 1-->
                                @foreach(@$booking as $bk)
                                    <div class="one_row1 small_screen31">
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.id')</span>
                                            <p class="add_ttrr">{{@$bk->token_no}}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.professional')</span>
                                            <p class="add_ttrr">{{@$bk->profDetails->nick_name ? @$bk->profDetails->nick_name : @$bk->profDetails->name}}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.date')/@lang('site.time')</span>
                                            <p class="add_ttrr">{{date('d-m-Y', strtotime(@$bk->date))}} {{ toUserTime($bk->start_time, 'g:i A') }}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.duration')</span>
                                            <p class="add_ttrr">{{@$bk->duration}}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.topic')</span>
                                            <p class="add_ttrr">{{@$bk->parentCatDetails->name}} <br>{{@$bk->childCatDetails->name}}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">{{__('site.payment_type')}}</span>
                                            <p class="add_ttrr">
                                                @if(@$bk->payment_type == 'C') {{__('site.payment_method_card')}}
                                                @elseif(@$bk->payment_type == 'BA') {{__('site.payment_method_bank')}}
                                                @elseif(@$bk->amount == 0){{__('site.free_session')}}
                                                @endif
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('client_site.payment_status')</span>
                                            <p class="add_ttrr">
                                                @if(@$bk->payment_status == 'I') {{__('site.Payment_initiated')}} @elseif(@$bk->payment_status == 'P')
                                                {{__('site.Payment_paid')}}
                                                @elseif(@$bk->payment_status == 'F') {{__('site.Payment_failed')}} @elseif(@$bk->payment_status == 'PR')
                                                {{__('site.Payment_processing')}}
                                                @endif
                                            </p>
                                        </div>
                                        {{-- <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">Status</span>
                                            <p class="add_ttrr">New</p>
                                        </div> --}}
                                        <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                            <span class="W55_1">@lang('site.action')</span>
                                            <a href="javascript:void(0);" class="rjct">@lang('site.reschedule')</a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif




                @if(sizeof(@$formdata)>0)
                    <h3>@lang('site.coaching_tools')</h3>
                    <div class="buyer_table">
                        <div class="table-responsive">
                            <div class="table">
                                <div class="one_row1 hidden-sm-down only_shawo">

                                    <div class="cell1 tab_head_sheet">@lang('site.professional_name')</div>
                                    <div class="cell1 tab_head_sheet">Title</div>
                                        <div class="cell1 tab_head_sheet">@lang('site.tool_type')</div>
                                        <div class="cell1 tab_head_sheet">@lang('site.assign_date')</div>

                                        <div class="cell1 tab_head_sheet">@lang('site.status')</div>
                                        <div class="cell1 tab_head_sheet">@lang('site.action')</div>
                                </div>
                                <!--row 1-->
                                @foreach(@$formdata as $ql)
                                <div class="one_row1 small_screen31">

                                    <div class="cell1 tab_head_sheet_1">
                                        <span class="W55_1">@lang('site.professional_name')</span>
                                        <p class="add_ttrr">{{ @$ql->getUserData->nick_name  ? @$ql->getUserData->nick_name : @$ql->getUserData->name }}</p>
                                    </div>
                                    <div class="cell1 tab_head_sheet_1">
                                        <span class="W55_1">Title</span>
                                        <p class="add_ttrr">
                                            {{-- {{ @$ql->tool_type }} --}}
                                            @if(@$ql->tool_type == "FORM")
                                            {{@$ql->getFormDetails->form_title??'N.A'}}
                                            @elseif(@$ql->tool_type == "Evaluation360")
                                            N.A
                                            @elseif(@$ql->tool_type == "Imported Tools")
                                            {{@$ql->getImportingToolsdata->title??'N.A'}}
                                            @elseif(@$ql->tool_type == "Log Book")
                                            N.A
                                            @elseif(@$ql->tool_type == "Contract Template")
                                            {{@$ql->getContractTemplatesdata->title??'N.A'}}
                                            @elseif(@$ql->tool_type == "Content Templates")
                                            {{@$ql->getContentTemplatesdata->cont_subject??'N.A'}}
                                            @elseif(@$ql->tool_type == "SMART Goals")
                                            {{@$ql->getSmartGoalsData->goal_title??'N.A'}}
                                                {{-- @lang('site.smart_goals') --}}
                                            @elseif(@$ql->tool_type == "360 evaluation")
                                                {{-- @lang('site.360tools') --}}
                                                {{@$ql->getTool360MasterDetails->title??'N.A'}}
                                            @endif
                                        </p>
                                    </div>
                                    <div class="cell1 tab_head_sheet_1">
                                        <span class="W55_1">tool_type</span>
                                        <p class="add_ttrr">
                                            {{-- {{ @$ql->tool_type }} --}}
                                            @if(@$ql->tool_type == "FORM")
                                                @lang('site.form')
                                            @elseif(@$ql->tool_type == "Evaluation360")
                                                @lang('site.evaluation360')
                                            @elseif(@$ql->tool_type == "Imported Tools")
                                                @lang('site.imported_tools')
                                            @elseif(@$ql->tool_type == "Log Book")
                                                @lang('site.logbook')
                                            @elseif(@$ql->tool_type == "Contract Template")
                                                @lang('site.contract_template')
                                            @elseif(@$ql->tool_type == "Content Templates")
                                                @lang('site.content_template')
                                            @elseif(@$ql->tool_type == "SMART Goals")
                                                @lang('site.smart_goals')
                                            @elseif(@$ql->tool_type == "360 evaluation")
                                                @lang('site.360tools')
                                            @endif
                                        </p>
                                    </div>
                                    <div class="cell1 tab_head_sheet_1">
                                        <span class="W55_1">assign_date</span>
                                        <p class="add_ttrr">{{ date('m/d/Y (D)', strtotime(@$ql->assign_date)) }}</p>
                                    </div>

                                    <div class="cell1 tab_head_sheet_1">
                                        <span class="W55_1">status</span>
                                        <p class="add_ttrr">{{ @$ql->status }}</p>
                                    </div>


                                     <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                                <span class="W55_1">@lang('client_site.action')</span>

                                                @if($ql->tool_type == "Imported Tools")
                                                    {{-- <a href="{{ route('user.view.form.details',['id'=>@$ql->id]) }}" ><i class="fa fa-eye"></i>View</a> --}}
                                                    <a href="{{ route('user.importing.answer.show',['id'=>@$ql->id]) }}" ><i class="fa fa-eye"></i>@lang('site.view')</a>

                                                    @if($ql->status == "INVITED")
                                                        |<a href="{{ route('user.importing.assigne.view',['id'=>@$ql->id]) }}" ><i class="fa fa-eye"></i>@lang('site.answer_submit')</a>

                                                        {{-- | <a href="{{ route('professional.importing.update.status',['id'=>@$ql->id]) }}" onclick="return confirm('Do you want to Complete this tools?');" >Completed</a> --}}
                                                    @endif

                                                    @if(@$ql->getImportedToolsFeedback)
                                                        |<a href="javascript:;" class="view_feedback_btn" data-title="{{$ql->getUserData->name}}" data-userfeedback="{{ @$ql->getImportedToolsFeedback->user_feedback }}" data-proffeedback="{{ @$ql->getImportedToolsFeedback->professional_feedback }}" style="margin: 2px;">@lang('site.view_feedback')</a>
                                                    @else
                                                        |<a href="javascript:;" class="show_assign" data-title="{{$ql->tool_type}}" data-id="{{ $ql->id }}" style="margin: 2px;">@lang('site.feedback')</a>
                                                    @endif
                                                    {{-- |<a href="javascript:;" class="view_tools_help" data-title="{{@$ql->getImportingToolsdata->title}}" data-helpdesc="{{ @$ql->getImportingToolsdata->help_tools_view }}" style="margin: 2px;">View tool help</a> --}}

                                                @elseif($ql->tool_type == "360 evaluation")
                                                    @if($ql->status == "INVITED")
                                                        <a href="{{ route('user.360evaluation',['id'=>@$ql->id]) }}" ><i class="fa fa-eye"></i>@lang('site.submit_answer_score')</a>
                                                    @elseif($ql->status =="COMPLETED")
                                                        <a href="{{ route('user.360evaluation.view',['id'=>@$ql->id]) }}" ><i class="fa fa-eye"></i>@lang('site.view')</a>
                                                    @endif

                                                @elseif($ql->tool_type == "SMART Goals")
                                                    @if($ql->status == "INVITED")
                                                        <a href="{{ route('user.smart.goal.view',['id'=>@$ql->id]) }}" ><i class="fa fa-eye"></i>@lang('site.submit_your_goal')</a>
                                                    @endif

                                                @elseif($ql->tool_type == "Content Templates")
                                                    @if($ql->status == "INVITED")
                                                        <a href="{{ route('user.content.temp.view',['id'=>@$ql->id]) }}" ><i class="fa fa-eye"></i>@lang('site.view')</a>
                                                    @endif


                                                @elseif($ql->tool_type == "Contract Template")
                                                    <a href="{{ route('user.contract.temp.view',['id'=>@$ql->id]) }}" ><i class="fa fa-eye"></i>@lang('site.view')</a>
                                                    @if($ql->status == "INVITED")
                                                        | <a href="{{ route('user.contract.temp.update.status',['id'=>@$ql->id]) }}" onclick="return confirm(`@lang('client_site.accept_contract')`);" >@lang('site.accept_contract')</a>
                                                    @endif


                                                @elseif($ql->tool_type == "Log Book")

                                                   <!--  @if($ql->status == "INVITED")
                                                        <a href="javascript:;" class="logbook_answer_btn" data-title="{{@$ql->getlogbookDetails->question}}" data-id="{{ $ql->id }}" style="margin: 2px;">Submit Answer</a>
                                                    @else
                                                        <a href="{{ route('user.logbook.view',['id'=>@$ql->id]) }}" ><i class="fa fa-eye"></i>View</a>
                                                        {{-- | <a href="{{ route('user.loogbook.answer.submit',['id'=>@$ql->id]) }}">Submit Answer</a> --}}
                                                    @endif -->
                                                    <a href="{{ route('user.logbook.all.ansdate') }}" ><i class="fa fa-eye"></i>@lang('site.view')</a> |
                                                    <!-- <a href="{{ route('user.logbook.view',['id'=>@$ql->id]) }}" ><i class="fa fa-eye"></i>View</a> | -->

                                                    <a href="{{route('user.logbook.question.list',[$ql->id])}}" class="" style="margin: 2px;" title="Edit">@lang('site.answer_submit')</a>

                                                @elseif($ql->tool_type == "Evaluation360")

                                                        <a href="{{ route('user.evaluation360.quelist',['id'=>@$ql->id]) }}" ></i>@lang('site.share_and_preview')</a> |

                                                        <a href="{{ route('user.evaluation360.score.listing',['id'=>@$ql->id]) }}" ><i class="fa fa-eye"></i>@lang('site.view')</a>

                                                @else
                                                    @if($ql->status == "INVITED")
                                                        <a href="{{ route('user.view.form.details',['id'=>@$ql->id]) }}" ><i class="fa fa-eye"></i> @lang('site.submit_form')</a>
                                                    @elseif($ql->status =="COMPLETED")
                                                        <a href="{{ route('user.view.form.submit.details',['id'=>@$ql->id]) }}" ><i class="fa fa-eye"></i> @lang('site.view')</a>
                                                    @endif

                                                    @if(@$ql->getImportedToolsFeedback)
                                                        |<a href="javascript:;" class="view_feedback_btn" data-title="{{$ql->getUserData->name}}" data-userfeedback="{{ @$ql->getImportedToolsFeedback->user_feedback }}" data-proffeedback="{{ @$ql->getImportedToolsFeedback->professional_feedback }}" style="margin: 2px;">@lang('site.view_feedback')</a>
                                                    @else
                                                        |<a href="javascript:;" class="show_formbtn" data-title="{{$ql->tool_type}}" data-id="{{ $ql->id }}" style="margin: 2px;">@lang('site.feedback')</a>
                                                    @endif


                                                @endif
                                                    {{-- <a href="#" class="rjct" style="background: #098005;"><i class="fa fa-pencil"></i> @lang('client_site.edit') </a> --}}
                                            </div>
                                </div>
                                @endforeach
                                {{-- {{ $avl->->onEachSide(5)->links() }} --}}
                            </div>
                        </div>
                    </div>
                @endif



            </div>



        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')

<script>
    $(function() {
        $("#datepicker").datepicker({
            dateFormat: "dd-mm-yy",
            defaultDate: new Date(),
            onClose: function( selectedDate ) {
                $( "#datepicker1").datepicker( "option", "minDate", selectedDate );
            }
        });
        $("#datepicker1").datepicker({
            dateFormat: "dd-mm-yy",
            defaultDate: new Date(),
            onClose: function( selectedDate ) {
                $( "#datepicker" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
    function copyLink(text){
        var input = document.createElement('input');
        input.setAttribute('value', text);
        document.body.appendChild(input);
        input.select();
        var result = document.execCommand('copy');
        document.body.removeChild(input);
        toastr.success("Link Copied");
        return result;
    }
</script>
@endsection
