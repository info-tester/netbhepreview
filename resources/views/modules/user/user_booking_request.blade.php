@extends('layouts.app')
@section('title')
  @lang('site.my_booking')
@endsection
@section('style')
@include('includes.style')
<style>
   .ui-timepicker-standard {
    position: absolute;
    z-index: 9999 !important;
}
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')
<section class="bkng-hstrybdy">
    <div class="container custom_container">
        <h2>@lang('site.my_booking')</h2>
        <div class="bokcntnt-bdy">
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.user_sidebar')
            <div class="dshbrd-rghtcntn">
                <form id="myForm" name="myForm" action="{{ route('srch.user.my.booking', ['type' => @$key['type']]) }}" method="get">
                    @csrf
                    <div class="from-field">
                        <div class="frmfld">
                            <div class="form-group">
                                <label class="search_label">@lang('client_site.from_date')</label>
                                <input type="text" id="datepicker" name="from_date" class="dashboard-type" placeholder="@lang('client_site.select_date')" value="{{ @$key['from_date'] }}">
                                <img class="pstn" src="{{ URL::to('public/frontend/images/clndr.png') }}">
                            </div>
                        </div>
                        <div class="frmfld">
                            <div class="form-group">
                                <label class="search_label">@lang('client_site.to_date')</label>
                                <input type="text" id="datepicker1" name="to_date" class="dashboard-type" placeholder="@lang('client_site.select_date')" value="{{ @$key['to_date'] }}">
                                <img class="pstn" src="{{ URL::to('public/frontend/images/clndr.png') }}">
                            </div>
                        </div>
                        <div class="frmfld">
                            <div class="form-group">
                                <label class="search_label">@lang('site.select') @lang('site.status')</label>
                                <select class="dashboard-type dashboard_select" name="status">
                                    <option value="">@lang('site.select') @lang('site.status')</option>
                                    <option value="AA" @if(@$key['status']=="AA") selected @endif>@lang('site.awaiting_approval') </option>
                                    <option value="A" @if(@$key['status']=="A") selected @endif>@lang('site.approved') </option>
                                    <option value="R" @if(@$key['status']=="R") selected @endif>@lang('site.rejected') </option>
                                    <option value="C" @if(@$key['status']=="C") selected @endif>@lang('site.order_cancel') </option>
                                </select>
                            </div>
                        </div>
                        <div class="frmfld">
                            <div class="form-group">
                                <label class="search_label">@lang('site.type')</label>
                                <select class="dashboard-type dashboard_select required" name="type">
                                    <option value="">@lang('site.select') @lang('site.type')</option>
                                    <option value="UC" @if(@$key['type']=="UC") selected @endif>@lang('site.upcoming_classes') </option>
                                    <option value="PC" @if(@$key['type']=="PC") selected @endif>@lang('site.past_classes') </option>
                                </select>
                            </div>
                        </div>
                        <div class="frmfld">
                            <button class="banner_subb srch">@lang('site.filter')</button>
                        </div>
                    </div>
                </form>
                @if(sizeof(@$booking)>0)
                <div class="buyer_table">
                    <div class="table-responsive">
                        <div class="table for_btn_nn60">
                            <div class="one_row1 hidden-sm-down only_shawo">
                                <div class="cell1 tab_head_sheet">@lang('site.id')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.professional')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.date')/@lang('site.time')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.duration')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.topic')</div>
                                <div class="cell1 tab_head_sheet">{{__('site.payment_type_1')}}<br>{{__('site.payment_type_2')}}</div>
                                <div class="cell1 tab_head_sheet">{{__('site.payment_status_1')}}<br>{{__('site.payment_status_2')}}</div>
                                <div class="cell1 tab_head_sheet">@lang('site.status')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.action')</div>
                            </div>
                            @foreach(@$booking as $bk)

                                @if(@$bk->order_status == 'AA' || @$bk->order_status == 'R')
                                    <div class="one_row1 small_screen31">
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.id')</span>
                                            <p class="add_ttrr">{{ @$bk->id }}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.professional')</span>
                                            <p class="add_ttrr">{{ @$bk->profDetails->nick_name ? @$bk->profDetails->nick_name : @$bk->profDetails->name }}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.date')/@lang('site.time')</span>
                                            <p class="add_ttrr">{{ toUserTime($bk->date . ' ' . $bk->start_time, 'd-m-Y, g:i A') }}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.duration')</span>
                                            <p class="add_ttrr">{{ @$bk->duration }}@lang('site.minute')</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.topic')</span>
                                            <p class="add_ttrr">{{ @$bk->parentCatDetails->name }} <br>{{ @$bk->childCatDetails->name }}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">{{__('site.payment_type')}}</span>
                                                <p class="add_ttrr">
                                                    @if(@$bk->payment_type == 'C') {{__('site.payment_method_card')}}
                                                    @elseif(@$bk->payment_type == 'BA') {{__('site.payment_method_bank')}}
                                                    @elseif(@$bk->payment_type == 'S') Stripe
                                                    @elseif(@$bk->payment_type == 'P') Paypal
                                                    @elseif(@$bk->amount == 0){{__('site.free_session')}}
                                                    @elseif(@$bk->wallet == @$bk->sub_total)  {{__('site.wallet')}}
                                                    @endif
                                                </p>
                                            </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.payment_status')</span>
                                            <p class="add_ttrr">
                                                @if(@$bk->payment_status == 'I') {{__('site.Payment_initiated')}} @elseif(@$bk->payment_status == 'P')
                                                {{__('site.Payment_paid')}}
                                                @elseif(@$bk->payment_status == 'F') {{__('site.Payment_failed')}} @elseif(@$bk->payment_status == 'PR')
                                                {{__('site.Payment_processing')}}
                                                @endif
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.status')</span>
                                            <p class="add_ttrr">
                                                @if(@$bk->order_status == 'AA')
                                                    {{ 'Awaiting Approval' }}
                                                @elseif(@$bk->order_status == 'R')
                                                    @lang('site.reject')
                                                @endif
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                            <span class="W55_1">@lang('site.action')</span>
                                            @if(@$bk->order_status!="R" && @$bk->reschedule_status!="AA")
                                                <a href="javascript::void(0);" class="rjct mb-1" data-toggle="modal" data-prof="{{ @$bk->profDetails->name }}" data-target="#myModal" onclick="setVl('{{ @$bk->token_no }}');">@lang('site.reschedule')</a>

                                                {{-- <a href="{{ route('user.cancel.req', ['token'=>@$bk->token_no]) }}" class="rjct">Cancel</a> --}}
                                            @elseif(@$bk->order_status=="R")
                                                <strong>@lang('site.booking_cancelled_by') {{ @$bk->order_cancelled_by == "U" ? (@Auth::guard('web')->user()->nick_name ? @Auth::guard('web')->user()->nick_name : @Auth::guard('web')->user()->name): (@$bk->profDetails->nick_name ? @$bk->profDetails->nick_name : @$bk->profDetails->name)}}</strong>
                                            @elseif(@$bk->reschedule_status=="AA")
                                                <strong>@lang('site.awaiting_rescheduled_approval')</strong>
                                            @endif
                                            @if($bk->payment_type=='BA')

                                            @endif

                                        </div>
                                    </div>

                                @elseif((@$bk->order_status == 'C') && @$key['type'] == 'UC')
                                    <div class="one_row1 small_screen31">
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">ID</span>
                                            <p class="add_ttrr">{{ @$bk->id }}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.professional')</span>
                                            <p class="add_ttrr">{{ @$bk->profDetails->nick_name ? @$bk->profDetails->nick_name : @$bk->profDetails->name }}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.date')/@lang('site.time')</span>
                                            <p class="add_ttrr">{{ toUserTime($bk->date . ' ' . $bk->start_time, 'd-m-Y, g:i A') }}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.duration')</span>
                                            <p class="add_ttrr">{{ @$bk->duration }} @lang('site.minute')</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.topic')</span>
                                            <p class="add_ttrr">{{ @$bk->parentCatDetails->name }} <br>{{ @$bk->childCatDetails->name }}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">{{__('site.payment_type')}}</span>
                                            <p class="add_ttrr">
                                                @if(@$bk->payment_type == 'C') {{__('site.payment_method_card')}}
                                                @elseif(@$bk->payment_type == 'BA') {{__('site.payment_method_bank')}}
                                                @elseif(@$bk->payment_type == 'P') Paypal
                                                @elseif(@$bk->payment_type == 'S') Stripe
                                                @elseif(@$bk->amount == 0){{__('site.free_session')}}
                                                @elseif(@$bk->wallet == @$bk->sub_total)  {{__('site.wallet')}}
                                                @endif
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.payment_status')</span>
                                            <p class="add_ttrr">
                                                @if(@$bk->payment_status == 'I') {{__('site.Payment_initiated')}} @elseif(@$bk->payment_status == 'P')
                                                {{__('site.Payment_paid')}}
                                                @elseif(@$bk->payment_status == 'F') {{__('site.Payment_failed')}} @elseif(@$bk->payment_status == 'PR')
                                                {{__('site.Payment_processing')}}
                                                @endif
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.status')</span>
                                            <p class="add_ttrr">
                                                @if(@$bk->order_status == 'C')
                                                <strong>@lang('site.order_cancel')</strong>
                                                @endif
                                            </p>
                                        </div>

                                        <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                            <span class="W55_1">@lang('site.action')</span>
                                            <a href="{{ route('user.booking.details1', ['id' => @$bk->id]) }}" class="rjct">@lang('site.view')</a>
                                        </div>
                                    </div>
                                @elseif((@$bk->order_status == 'A') && @$key['type'] == 'UC')
                                    <div class="one_row1 small_screen31">
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">ID</span>
                                            <p class="add_ttrr">{{ @$bk->id }}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.professional')</span>
                                            <p class="add_ttrr">{{ @$bk->profDetails->nick_name ? @$bk->profDetails->nick_name : @$bk->profDetails->name }}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.date')/@lang('site.time')</span>
                                            <p class="add_ttrr">{{ toUserTime($bk->date . ' ' . $bk->start_time, 'd-m-Y, g:i A') }}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.duration')</span>
                                            <p class="add_ttrr">{{ @$bk->duration }} @lang('site.minute')</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.topic')</span>
                                            <p class="add_ttrr">{{ @$bk->parentCatDetails->name }} <br>{{ @$bk->childCatDetails->name }}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">{{__('site.payment_type')}}</span>
                                            <p class="add_ttrr">
                                                @if(@$bk->payment_type == 'C') {{__('site.payment_method_card')}}
                                                @elseif(@$bk->payment_type == 'BA') {{__('site.payment_method_bank')}}
                                                @elseif(@$bk->payment_type == 'P') Paypal
                                                @elseif(@$bk->payment_type == 'S') Stripe
                                                @elseif(@$bk->amount == 0){{__('site.free_session')}}
                                                @elseif(@$bk->wallet == @$bk->sub_total)  {{__('site.wallet')}}
                                                @endif
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.payment_status')</span>
                                            <p class="add_ttrr">
                                                @if(@$bk->payment_status == 'I') {{__('site.Payment_initiated')}} @elseif(@$bk->payment_status == 'P')
                                                {{__('site.Payment_paid')}}
                                                @elseif(@$bk->payment_status == 'F') {{__('site.Payment_failed')}} @elseif(@$bk->payment_status == 'PR')
                                                {{__('site.Payment_processing')}}
                                                @endif
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.status')</span>
                                            <p class="add_ttrr">
                                                @if(@$bk->order_status == 'A' && @$bk->reschedule_status==null)
                                                   @lang('site.approved')

                                                @elseif(@$bk->reschedule_status=='AA')
                                                    <strong>@lang('site.awaiting_rescheduled_approval')</strong>

                                                @elseif(@$bk->reschedule_status=='A')
                                                    <strong>@lang('site.rescheduled_approval')</strong>

                                                @elseif(@$bk->reschedule_status=='R')
                                                    <strong>@lang('site.rescheduled_rejected')</strong>
                                                @endif
                                            </p>
                                        </div>
                                        @php
                                        @$rescheduletime=@$bk->profDetails->reschedule_time;
                                        @$rescheduleTimeDate = date("Y-m-d H:i:s", strtotime(@$words["return_dt"]." + $rescheduletime  hours"));
                                        @$timedate=date("Y-m-d H:i:s", strtotime($bk->date . ' ' . $bk->start_time));
                                        @endphp
                                        <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                            <span class="W55_1">@lang('site.action')</span>
                                            @if(@$bk->order_status!="R" && @$bk->reschedule_status!="AA" && @ $rescheduleTimeDate<@$timedate )
                                                <a href="javascript::void(0);" class="rjct mb-1" data-toggle="modal" data-prof="{{ @$bk->profDetails->name }}" data-target="#myModal" onclick="setVl('{{ @$bk->token_no }}', '{{ @$bk->date }}','{{ @$bk->start_time."-".@$bk->end_time }}','{{ @$bk->professional_id }}', '{{ $bk->start_time }}', '{{ $bk->end_time }}');">@lang('site.reschedule')</a>

                                                {{-- <a href="{{ route('user.cancel.req', ['token'=>@$bk->token_no]) }}" class="rjct">Cancel</a> --}}
                                            @elseif(@$bk->order_status=="R")
                                                <strong>@lang('site.booking_cancelled_by') {{ @$bk->order_cancelled_by == "U" ? (@Auth::guard('web')->user()->nick_name ? @Auth::guard('web')->user()->nick_name : @Auth::guard('web')->user()->name) : (@$bk->profDetails->nick_name ? @$bk->profDetails->nick_name : @$bk->profDetails->name)}}</strong>
                                            @elseif(@$bk->reschedule_status=="AA")
                                                <strong>@lang('site.awaiting_rescheduled_approval')</strong>
                                            @endif
                                            {{-- @if(@!$bk->getReview && @$bk->video_status=='C')
                                                <a href="{{ route('user.review', ['booking_id' => $bk->id, 'professional_id' => $bk->professional_id]) }}" class="rjct mb-1">@lang('site.review')</a>
                                            @endif --}}
                                            <a href="{{ route('user.booking.details1', ['id' => @$bk->id]) }}" class="rjct">@lang('site.view')</a>

                                            <a href="{{ route('compose') }}" class="rjct">@lang('site.message')</a>
                                            @if(@$bk->payment_type=='BA'&& @$bk->payment_document==null)
                                            <a></a><a href="{{ route('booking.sces', ['id' => @$bk->token_no]) }}" class="rjct">{{__('site.upload_document')}}</a>
                                            @endif
                                            {{-- {{ strtotime(toUserTime($bk->date . ' ' . $bk->start_time, 'd-m-Y, g:i A')) }}
                                            {{ strtotime(now()->format('d-m-Y, g:i A')) }} --}}
                                            @if(strtotime(now()->format('d-m-Y, g:i A'))>strtotime(toUserTime($bk->date . ' ' . $bk->start_time, 'd-m-Y, g:i A'))|| (@!$bk->getReview && @$bk->video_status=='C'))
                                            <a href="{{ route('user.review', ['booking_id' => $bk->id, 'professional_id' => $bk->professional_id]) }}" class="rjct mb-1">@lang('site.review')</a>
                                            @endif
                                            @php
                                                 $startTime =date('Y-m-d H:i:s', strtotime(@$bk->date.@$bk->start_time));
                                            @endphp
                                            @if((toUTCTime(date('Y-m-d H:i:s', strtotime("+24 hours"))) < $startTime) && @$bk->order_status == 'A' && $bk->payment_status=='P')
                                            {{-- <a href="{{ route('booking.cancel', ['token' => @$bk->token_no,'id'=>@$bk->id]) }}" class="rjct cancelbtn" onclick="return confirm(`@lang('client_site.cancel_order')`)">booking cancel</a> --}}
                                            <a href="javascript:;" class="rjct cancelbtn" data-token="{{@$bk->token_no}}" data-id="{{@$bk->id}}">{{__('site.booking_cancel')}}</a>
                                            @endif
                                        </div>
                                    </div>

                                @elseif($bk->video_status == 'C' && @$key['type'] == 'PC')
                                    <div class="one_row1 small_screen31">
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">ID</span>
                                            <p class="add_ttrr">{{ @$bk->id }}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.professional')</span>
                                            <p class="add_ttrr">{{ @$bk->profDetails->nick_name ? @$bk->profDetails->nick_name : @$bk->profDetails->name }}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.date')/@lang('site.time')</span>
                                            <p class="add_ttrr">{{ toUserTime($bk->date . ' ' . $bk->start_time, 'd-m-Y g:i A') }}</p>
                                            <input type="hidden" name="selected_date" value="{{ @$bk->date }}">
                                            <input type="hidden" name="selected_time" value="{{ date('H:i a' ,strtotime(@$bk->start_time)) }}">
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.duration')</span>
                                            <p class="add_ttrr">{{ @$bk->duration }} @lang('site.minute')</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.topic')</span>
                                            <p class="add_ttrr">{{ @$bk->parentCatDetails->name }} <br>{{ @$bk->childCatDetails->name }}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">{{__('site.payment_type')}}</span>
                                                <p class="add_ttrr">
                                                    @if(@$bk->payment_type == 'C') {{__('site.payment_method_card')}}
                                                    @elseif(@$bk->payment_type == 'BA') {{__('site.payment_method_bank')}}
                                                    @elseif(@$bk->payment_type == 'P') Paypal
                                                    @elseif(@$bk->payment_type == 'S') Stripe
                                                    @elseif(@$bk->amount == 0){{__('site.free_session')}}
                                                    @elseif(@$bk->wallet == @$bk->sub_total)  {{__('site.wallet')}}
                                                    @endif
                                                </p>
                                            </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.payment_status')</span>
                                            <p class="add_ttrr">
                                                @if(@$bk->payment_status == 'I') {{__('site.Payment_initiated')}} @elseif(@$bk->payment_status == 'P')
                                                {{__('site.Payment_paid')}}
                                                @elseif(@$bk->payment_status == 'F') {{__('site.Payment_failed')}} @elseif(@$bk->payment_status == 'PR')
                                                {{__('site.Payment_processing')}}
                                                @endif
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.status')</span>
                                            <p class="add_ttrr">
                                                @if(@$bk->video_status == 'C')
                                                   {{ 'Completed' }}
                                                @endif
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                            <span class="W55_1">@lang('site.action')</span>
                                            <a href="{{ route('user.review', ['booking_id' => $bk->id, 'professional_id' => $bk->professional_id]) }}" class="rjct mb-1">@lang('site.review')</a>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
                @else
                    <center>
                        <h3 class="error">@lang('site.booking_details_not_found') !</h3>
                    </center>
                @endif
            </div>
        </div>
    </div>

    {{-- modal --}}

      <div class="modal fade" id="myModal">
            <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                  <h4 class="modal-title">@lang('site.reschedule') @lang('site.appoinment') </h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form name="myForm2" id="myForm2" method="post" action="{{ route('user.booking.reschedule') }}">
                        @csrf
                        <input type="hidden" name="token_no" id="token_no">
                        <input type="hidden" name="professional_id" id="professional_id">
                        <input type="hidden" id="start-time">
                        <input type="hidden" id="end-time">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>@lang('site.date')</label>
                                        <input type="text" class="required form-control" name="date" id="date" value="">
                                    </div>
                                </div>

                              <div class="col-md-6 col-sm-6 col-xs-12">
                                {{-- @dump($booking) --}}
                                  <div class="form-group">
                                      <label>@lang('site.time')</label>
                                      <select class="required form-control " name="time" id="time">
                                      {{-- <option value="">Select Time Slot</option>
                                      @foreach($booking as $val)
                                        <option value="{{ $val->start_time.'-'.$val->end_time }}">{{ $val->start_time }}-{{ $val->end_time }}</option>
                                      @endforeach --}}
                                      </select>
                                      <span id="time_err" style="font-weight: bold;color: red;"></span>
                                  </div>
                              </div>
                          </div>
                        </div>
                    </form>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                  <button type="button" class="btn btn-primary sbmt">@lang('site.submit')</button>

                  <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('site.close')</button>
                </div>

              </div>
            </div>
      </div>

    {{-- modal --}}

    <div class="modal fade" id="modalcancel">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
              <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title">{{__('site.booking_cancel')}} </h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form name="myForm3" id="myForm3" method="post">
                    @csrf
                    <input type="hidden" name="token_no" id="token_no">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group" style="width: 100%;">
                                    <label>{{__('site.cancellation_reason')}}</label>
                                    <input type="text" class="required form-control" name="reason" value="" >
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <input type="submit" value="Cancel" class="btn btn-primary">
                                </div>
                            </div>
                      </div>
                    </div>
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('site.close')</button>
            </div>

          </div>
        </div>
  </div>

</section>
@endsection
@section('footer')
@include('includes.footer')
<link href="{{ URL::to('public/frontend/css/calender.css') }}" rel="stylesheet" type="text/css">
<script src="{{ URL::to('public/frontend/js/jquery-ui.js') }}"></script>
<script src="{{ URL::to('public/frontend/js/moment.min.js') }}"></script>


<script>
    function setVl(tn, dt,tm, pid, startTime, endTime){
        $('#token_no').val(tn);
        $('#date').val(dt);
        $('#professional_id').val(pid);
        $('#start-time').val(startTime);
        $('#end-time').val(endTime);
        // $('#time').html('<option value="'+tm+'" selected>'+tm+'</option>');
        $('#date').trigger('change');
    }

    $('#date').change(function(){
        if($("#date").val()!="") {
            var html='';
            var time_formate='';
            var from_time='';
            var to_time='';
            var date = $(this).val();
            var professional_id = $('#professional_id').val();
            html ='<option value="">@lang('site.select_time_slot')</option>';
            $.ajax({
                url: '{{ route("rescheduled.time.list") }}',
                type:"GET",
                data: {
                    date: date,
                    user_id: professional_id,
                    start: $('#start-time').val(),
                    end: $('#end-time').val(),
                },
                success: function(response) {
                    console.log(response);
                    response.forEach(function(item, index){
                        slot_start = (item.slot_start).substr(0,5);
                        slot_end = (item.slot_end).substr(0,5);
                        time_formate = slot_start.concat('-', slot_end);
                        // alert(time_formate);
                        html+='<option value="'+time_formate+'">'+time_formate+'</option>';
                    });
                    $('#time').html(html);
                },
            });
        }
    });
    $('#myForm2').validate();
    $('.timepicker').timepicker({
        timeFormat: 'H:mm',
        interval: 15,
        minTime: '00',
        maxTime: '11:00pm',
        startTime: '09:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
    $(function() {
        $("#date").datepicker({
            dateFormat: "dd-mm-yy",
            defaultDate: new Date(),
            minDate:new Date(),
        });

        $("#datepicker").datepicker({dateFormat: "dd-mm-yy",
            monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
            defaultDate: new Date(),
            onClose: function( selectedDate ) {
                $( "#datepicker1").datepicker( "option", "minDate", selectedDate );
            }
        });
    $("#datepicker1").datepicker({dateFormat: "dd-mm-yy",
         monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho',
            'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
     defaultDate: new Date(),
          onClose: function( selectedDate ) {
          $( "#datepicker" ).datepicker( "option", "maxDate", selectedDate );
        }
     });

    });
</script>
<script>
    $('#srch').click(function(){
        $('#myForm').submit();
    });
    $('.sbmt').click(function(){
        if($('#date').val()!="" && $('#time').val()!=""){
            $('.parent_loader').show();
            setTimeout(function(){ $('#myForm2').submit(); }, 3000);
        }
        else{
             $('#myForm2').submit();
        }

    });
    $('#time').click(function(){
        if($('#date').val()==''){
            $(this).val('');
            $('#time_err').html('select date first');
            return false;
        }
        else{
            $('#time_err').html('');
        }
    });
    $(document).ready(function(){
        $('#myForm').validate({
            rules:{
                phone_no:{
                    digits:true,
                    maxlength:10,
                    minlength:10
                }
            }
        });

        $('#myForm1').validate({
            rules:{
                old_password:{
                    required:true
                },
                new_password:{
                    required:true,
                    minlength:8,
                    maxlength:8
                },
                confirm_password:{
                    required:true,
                    minlength:8,
                    maxlength:8,
                    equalTo:"#new_password"
                }
            }
        });
        $('#myForm3').validate({});
    });

    $('#phone_no').change(function(){
        if($('#phone_no').val()!=""){
            var reqData = {
              'jsonrpc' : '2.0',
              '_token' : '{{csrf_token()}}',
              'params' : {
                    'no' : $('#phone_no').val()
                }
            };
            $.ajax({
                url: "{{ route('chk.mobile') }}",
                method: 'post',
                dataType: 'json',
                data: reqData,
                success: function(response){
                    if(response.status==1) {
                        $('#phone_no').val("");
                        $('#ph_error').text('@lang('client_site.phone_number_allready_exist').');
                    }

                }, error: function(error) {
                    toastr.info('@lang('client_site.Try_again_after_sometime')');
                }
            });
        }
    });
    $('.cancelbtn').click(function(){
        var id= $(this).data('id');
        var token= $(this).data('token');
        $('#modalcancel').modal('show');
        var url= '{{route('home')}}/booking-cancel/'+token+'/'+id;
        $('#myForm3').attr('action', url)
    });


</script>

@endsection
