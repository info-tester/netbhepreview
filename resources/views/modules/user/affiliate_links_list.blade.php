@extends('layouts.app')
@section('title')
  @lang('site.my_affiliate_products')
@endsection
@section('style')
@include('includes.style')
<style>
   .ui-timepicker-standard {
    position: absolute;
    z-index: 9999 !important;
}
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')
<section class="bkng-hstrybdy">
    <div class="container custom_container">
        <h2>@lang('site.my_affiliate_products')</h2>
        <div class="bokcntnt-bdy">
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.user_sidebar')
            <div class="dshbrd-rghtcntn">
                <a class="btn btn-success pull-right" href="{{ route('user.affiliation.earnings') }}">@lang('site.my_affiliation_earnings')</a>
                <div class="buyer_table">
                    <div class="table-responsive">
                    @if(sizeof(@$affiliate_products)>0)
                        <div class="table">
                            <div class="one_row1 hidden-sm-down only_shawo">
                                <!-- <div class="cell1 tab_head_sheet">@lang('site.id').</div> -->
                                <div class="cell1 tab_head_sheet">#</div>
                                <div class="cell1 tab_head_sheet">@lang('site.image')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.title')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.price')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.estimated_earning')</div>
                                <div class="cell1 tab_head_sheet">@lang('client_site.commission')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.action')</div>
                            </div>
                            <!--row 1-->
                                @foreach(@$affiliate_products as $k=>$row)

                                 <div class="one_row1 small_screen31">
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">#</span>
                                            <p class="add_ttrr">{{(@$k+1)}}</p>
                                        </div>

                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.image')</span>
                                            <img src="{{URL::to('storage/app/public/uploads/product_cover_images/') . '/' . @$row->product->cover_image}}" height="50" width="60">
                                        </div>

                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.title')</span>
                                            <p class="add_ttrr"><a href="{{route('product.details',@$row->product->slug)}}" target="_blank">{{@$row->product->title}}</a></p>
                                        </div>

                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.price')</span>
                                            <p class="add_ttrr">{{ @$row->product->discounted_price > 0 ?  @$row->product->discounted_price : @$row->product->price }}</p>
                                        </div>

                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.estimated_earning')</span>
                                            @php
                                                $price = @$row->product->discounted_price > 0 ?  @$row->product->discounted_price : @$row->product->price;
                                                $discount = $row->product->category->commission;
                                                $amount = (@$price * @$discount) / 100;
                                                $affiliate_amount = (@$amount*$row->product->affiliate_percentage)/ 100;
                                            @endphp
                                            <p class="add_ttrr">{{@$affiliate_amount}}.00</p>
                                        </div>

                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('client_site.commission')</span>
                                            <p class="add_ttrr">{{@$row->product->affiliate_percentage}}%</p>
                                        </div>

                                        <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                            <a href="javascript:;" class="acpt" onclick="copyText(`{{url('/')}}/product-details/{{@$row->product->slug}}/{{$row->affiliate_code}}`)">@lang('site.copy_link')</a>
                                            <a href="javascript:;" class="aff_rem rjct" data-product="{{@$row->product->id}}">@lang('site.delete')</a>
                                        </div>
                                    </div>

                                @endforeach

                                

                        </div>
                    @else
                        <center>
                            <h3 class="error">@lang('site.product_not_found') !</h3>
                        </center>
                    @endif
                   
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- modal --}}

      <div class="modal fade" id="myModal">
            <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                  <h4 class="modal-title">@lang('site.reschedule') @lang('site.appoinment') </h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form name="myForm2" id="myForm2" method="post" action="{{ route('user.booking.reschedule') }}">
                        @csrf
                        <input type="hidden" name="token_no" id="token_no">
                        <input type="hidden" name="professional_id" id="professional_id">
                        <input type="hidden" id="start-time">
                        <input type="hidden" id="end-time">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>@lang('site.date')</label>
                                        <input type="text" class="required form-control" name="date" id="date" value="">
                                    </div>
                                </div>

                              <div class="col-md-6 col-sm-6 col-xs-12">
                                {{-- @dump($order->productDetails) --}}
                                  <div class="form-group">
                                      <label>@lang('site.time')</label>
                                      <select class="required form-control " name="time" id="time">
                                      {{-- <option value="">Select Time Slot</option>
                                      @foreach($booking as $val)
                                        <option value="{{ $val->start_time.'-'.$val->end_time }}">{{ $val->start_time }}-{{ $val->end_time }}</option>
                                      @endforeach --}}
                                      </select>
                                      <span id="time_err" style="font-weight: bold;color: red;"></span>
                                  </div>
                              </div>
                          </div>
                        </div>
                    </form>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                  <button type="button" class="btn btn-primary sbmt">@lang('site.submit')</button>

                  <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('site.close')</button>
                </div>

              </div>
            </div>
      </div>

    {{-- modal --}}

</section>
@endsection
@section('footer')
@include('includes.footer')
<link href="{{ URL::to('public/frontend/css/calender.css') }}" rel="stylesheet" type="text/css">
<script src="{{ URL::to('public/frontend/js/jquery-ui.js') }}"></script>
<script src="{{ URL::to('public/frontend/js/moment.min.js') }}"></script>
<script>
    $('.aff_rem').click(function(){
        var thisaff = $(this);
        var productId = $(this).data('product');
        $.ajax({
            url: "{{ url('/') }}/affiliate-products-remove-list/"+productId,
            type: 'get',
            dataType: 'json',
        })
        .done(function(response) {
            console.log(response);
            if(response.status == 'success'){
                // toastr.success(response.message);
                location.reload();
            } else {
                toastr.error(response.message);
            }
        })
        .fail(function(error) {
            console.log(response);
        });
    });

    function copyText(text){
        var input = document.createElement('input');
        input.setAttribute('value', text);
        document.body.appendChild(input);
        input.select();
        var result = document.execCommand('copy');
        document.body.removeChild(input);
        toastr.success("Link Copied");
        return result;
    }
</script>

@endsection
