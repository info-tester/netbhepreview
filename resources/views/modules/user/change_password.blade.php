@extends('layouts.app')
@section('title')
  @lang('site.change_password')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.change_password')</h2>
        <div class="bokcntnt-bdy">
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.user_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">
                    <form action="{{ route('user.update.profile') }}" method="post" name="myForm" id="myForm" enctype="multipart/form-data">
                        @csrf
                        <div class="form_body">
                            <div class="row">
                            <div class="col-sm-12">
                                <h4>@lang('client_site.change_password')</h4>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="personal-label">@lang('site.old_password')</label>
                                    <input type="Password" name="old_password" id="old_password" placeholder="@lang('site.old_password')" class="personal-type">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="personal-label">@lang('site.new_password')</label>
                                    <input type="Password" placeholder="@lang('site.new_password')" id="new_password" name="new_password" class="personal-type">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="personal-label">@lang('site.confirm_password')</label>
                                    <input type="Password" id="confirm_password" placeholder="@lang('site.confirm_password')" name="confirm_password" class="personal-type">
                                </div>
                            </div>

                                <div class="col-sm-12">
                                    <button type="submit" class="login_submitt btnCP">@lang('site.change_password')</button>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')

<script>
    $(document).ready(function(){
        $(".chosen-select").chosen();
        $('#myForm').validate();
    });
</script>
@endsection