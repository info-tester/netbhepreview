@extends('layouts.app')
@section('title')
    @lang('site.post') @lang('site.details')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')
<!-- <style>
    .cert_template {
        padding: 20px;
    }
    .template1 {
        background: url("/public/frontend/images/template1.jpg") no-repeat;
        background-position: top;
        background-size: cover;
    }
    .template2 {
        background: url("/public/frontend/images/template2.jpg") no-repeat;
        background-position: top;
        background-size: cover;
    }
    .template3 {
        background: url("/public/frontend/images/template3.jpg") no-repeat;
        background-position: top;
        background-size: cover;
    }
    .template4 {
        background: url("/public/frontend/images/template4.jpg") no-repeat;
        background-position: -50px;
        background-size: cover;
    }
    .new-temp {
        border:2px solid #2968a252;
        padding: 15px;
    }
    .first-templte {
        display: flex;
        flex-direction: column;
        padding: 2rem 2rem 2rem 12rem;
        box-sizing: border-box;
    }
    .second-templte {
        display: flex;
        flex-direction: column;
        padding: 2rem 2rem 2rem 2rem;
        box-sizing: border-box;
        border:2px solid #29649c;
        text-align: center;
    }
    .third-templte {
        display: flex;
        flex-direction: column;
        padding: 2rem 2rem 2rem 14rem;
        box-sizing: border-box;
    }
    .four-templte {
        display: flex;
        flex-direction: column;
        padding: 2rem 13.5rem 2rem 1rem;
        padding-right: 220px;
        text-align: right;
        box-sizing: border-box;
    }
    .cert_det {
        display: flex;
        justify-content: space-between;
        flex-direction: row;
        flex-wrap: wrap;
        margin: 0 0 0.6in 0;
    }
    small {
        font-size: 12px;
        color: #909090;
        font-weight: 300;
    }
    p.cert_head.secondary_color {
        color: #6C6C6C;
        font-size: 15px;
        text-transform: uppercase;
    }
    h4.stu_name.primary_color {
        color: #353A3E;
        margin: 10px 0;
        font-size: 30px;
        font-weight: 700;
    }
    .next-content {
        margin-top: 40px;
    }
    p.above_course.secondary_color {
        color: #6C6C6C;
        font-size: 15px;
    }
    h4.course_name.primary_color {
        margin:10px 0 15px 0;
        font-size: 22px;
        font-weight: 700;
    }
    .new_cont {
        color: #8c8c8c;
        font-weight: normal;
        font-size: 13px;
        line-height: 20px;
        margin-top: 5px;
    }
    .below_course_div {
        margin-top: 30px;
        text-align: right;
    }
    .below_course_div img {
        height: 40px;
    }
    .prof_name {
        color: #353A3E;
        font-size: 14px;
        line-height: 15px;
        margin-top: 5px;
    }
    h2.cert_head {
        margin: 30px 0 40px;
        font-size: 31px;
        font-weight:600;
        text-transform: uppercase;
        background-image: none !important;
    }
    p.above_stu_name {
        color: #A8A8A8;
        font-size: 14px;
    }
    h4.stu_name {
        margin:10px 0 45px 0;
        font-size: 28px;
        line-height: 28px;
        font-weight: 700;
    }
    p.above_course {
        color: #A8A8A8;
        font-size: 14px;
        margin: 0 auto;
    }
    h4.course_name {
        margin:10px 0 10px 0;
        font-size: 24px;
        line-height: 28px;
        font-weight: 700;
    }
    .content__bottom {
        display: flex;
        justify-content: space-between;
        align-items: flex-end;
        flex-direction: row;
        margin:0;
    }
    .new_sign {
        margin-bottom: 40px;
    }
    .w-80 {
        width: 85%;
        margin:0 auto;
    }
    p.cert_head {
        color: #565656;
        font-size: 17px;
        margin: 0;
        font-weight: 300;
    }
    p.below_course {
        color: #A3A3A3;
        font-size: 17px;
        font-weight: 300;
        margin-top: 15px;
    }
    p.above_stu_name {
        color: #A8A8A8;
        font-size: 15px;
        margin-top: 4px;
    }
    .templete3-cer {
        margin: 35px 0 20px 0;
        border: 3px #D7D7D7 solid;
        padding: 20px 20px 0 20px;
    }
    h2.cert_head.new-card-head {
        margin: 10px 0 0px;
        font-size: 30px;
        text-transform: capitalize
    }
    h4.stu_name.new_4 {
        margin-top: 40px;
    }
    p.above_course.new-corss {
        width: 100%;
    }
    .four_cert {
        text-align: right !important;
        background-position: right bottom !important;
        background-image: none !important;
        font-size: 40px !important;
    }
    .cert_temp_card {
        margin-bottom: 10px;
    }
    .prof_name {
        color: #9B9B9B;
    }
    .font_lato,  .font_lato>.four-templte div p,  .font_lato>.four-templte p,  .font_lato>.third-templte div p,  .font_lato>.third-templte p,  .font_lato>.second-templte p,  .font_lato>.second-templte div p {
        font-family: 'Lato', sans-serif !important;
    }
    .font_montserrat,  .font_montserrat>.four-templte div p,  .font_montserrat>.four-templte p,  .font_montserrat>.third-templte div p,  .font_montserrat>.third-templte p,  .font_montserrat>.second-templte p {
        font-family: 'Montserrat', sans-serif !important;
    }
    .font_open_sans,  .font_open_sans>.four-templte div p,  .font_open_sans>.four-templte p,  .font_open_sans>.third-templte div p,  .font_open_sans>.third-templte p,  .font_open_sans>.second-templte p {
        font-family: 'Open Sans', sans-serif !important;
    }
    .font_w_300 {
        font-weight:300 !important;
    }
    .font_w_400 {
        font-weight:400 !important;
    }
    .font_w_600 {
        font-weight:600 !important;
    }
    .font_w_700 {
        font-weight:700 !important;
    }
    .font_w_800 {
        font-weight:800 !important;
    }
    .active_cert {
        border: 1px solid rgb(0, 123, 255) !important;
    }
    .note {
        font-size: 12px;
    }
    .big_modal_rm .modal-dialog{
        max-width: 722px !important;
        overflow: hidden;
    }
    .big_modal_rm{
        padding-right: 0 !important;
    }
</style> -->
<section class="bkng-hstrybdy">
    <div class="container">
        <h2></h2>

        

        <div class="bokcntnt-bdy no-margin-top">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="view-page col-md-12">
                            <p><strong>@lang('site.order_id') :</strong>{{@$order->orderMaster->token_no}}</p>
                            <p><strong>@lang('site.product_title') :</strong>{{@$order->product->title}}</p>
                            <p><strong>@lang('site.price')  :</strong>{{@$order->amount}}</p>
                            <p><strong>@lang('site.purchase_date') :</strong>{{date('jS F Y' ,strtotime(@$order->orderMaster->order_date))}}</p>
                           
                    </div>
                    <br>
                    <br>
                    <br>
                <div class="blog-right details-right">
                    <div class="">
                        
                        <div class="">
                            
                            <div class="from-field">

                                <div class="weicome">

                                    <div class="clearfix"></div>
                                    
                                    <div class="clearfix"></div>

                                    <div id="cert_desc">{!! $certificatedesc !!}</div>
                                    
                                </div>                       
                            </div>
                                        
                            <div align="center">
                                <!-- <a href="{{route('certificate.download',[@$order->id])}}" download="Certificate" class="btn btn-success">Download Certificate</a> -->
                                <a href="javascript:;" id="dwnld" class="btn btn-success">Download Certificate</a>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
                    
                </div>
            </div>
        </div>
    </div>
    <div id="previewImage" style="display:none;"></div>
</section>
@endsection
@section('footer')
@include('includes.footer')
    
    <script>
        var element = $("#cert_desc").html(); // global variable
        var getCanvas; // global variable
        var background = "";
        // background = `url('{{url('/')}}/public/frontend/images/template{{@$template->template_number}}.jpg') no-repeat`;
        @if(@$template->background_filename)
            background = "url('{{url('/')}}/storage/app/public/uploads/certificate_template/{{@$template->background_filename}}') no-repeat";
        @else
            background = "url('{{url('/')}}/public/frontend/images/template{{@$template->template_number}}.jpg') no-repeat";
        @endif
        $('.cert_template').css('background', background);
        $('.cert_template').css('background-size', 'cover');
        $('.cert_template').css('background-position', 'center');

            $("#dwnld").on('click', function () {
                // window.scrollTo(0,0);
                // var w = document.getElementById("cert_desc").scrollWidth;
                // var h = document.getElementById("cert_desc").scrollHeight;
                // html2canvas(document.getElementById("cert_desc"), {
                //     useCORS: true,
                //     onrendered: function(canvas) {
                //         var tempcanvas = document.createElement('canvas');
                //         tempcanvas.width=465;
                //         tempcanvas.height=524;
                //         var context=tempcanvas.getContext('2d');
                //         context.drawImage(canvas,465,40,465,524,0,0,465,524);
                //         var link=document.createElement("a");
                //         link.href=canvas.toDataURL('image/jpg');
                //         link.download = 'screenshot.jpg';
                //         link.click();
                //         document.getElementById("previewImage").appendChild(tempcanvas);
                //         // var img = canvas.toDataURL("image/jpeg", 1);
                //         // var doc = new jsPDF('L', 'px', [w, h]);
                //         // doc.addImage(img, 'JPEG', 0, 0, w, h);
                //         // doc.save('sample-file.pdf');
                //     }
                // });

                var w = document.getElementById("cert_desc").scrollWidth;
                var h = document.getElementById("cert_desc").scrollHeight;
                window.scrollTo(0,0);
                html2canvas(document.getElementById("cert_desc"), {
                    dpi: 300, // Set to 300 DPI
                    scale: 3, // Adjusts your resolution
                    useCORS: true,
                    background: "#fff",
                    onrendered: function(canvas) {
                        document.getElementById("previewImage").appendChild(canvas);
                        var img = canvas.toDataURL("image/jpeg", 1);
                        var doc = new jsPDF('L', 'px', [w, h]);
                        doc.addImage(img, 'JPEG', 0, 0, w, h);
                        doc.save('sample-file.pdf');
                    }
                });
            //     var reqData = {
            //         'jsonrpc' : '2.0',
            //         '_token' : '{{csrf_token()}}',
            //         'html' : $('#previewImage').html(),
            //     };
            //     $.ajax({
            //         url: "{{ route('certificate.download') }}",
            //         method: 'post',
            //         dataType: 'json',
            //         data: reqData,
            //         async: false,
            //         success: function(res){
            //             console.log(res);
            //         },
            //         error: function(res){}
            //     });
            });

    </script>


@endsection