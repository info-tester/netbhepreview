@extends('layouts.app')
@section('title', 'Become A Professional')
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.become_a_professional')</h2>
        <div class="bokcntnt-bdy">
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.user_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">
                    <form action="{{ route('store.professional') }}" method="post" name="myForm" id="myForm" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="total_dyn" id="total_dyn">
                        <div class="form_body">

                            <div class="row">

                                <div id="maindiv" class="row" style="width: 100%;">
                                    <div class="col-lg-8 col-md-8 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">@lang('site.sell_what')</label>

                                            <div class="tooltip">
                                                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                    <span class="tooltiptext tooltip-right">@lang('site.tooltip_sell_what')</span>
                                            </div>
                                            <select name="sell" class="required personal-type personal-select" id="sell_what">
                                                <!-- <option value="">@lang('client_site.select')</option> -->
                                                <option value="VS">Apenas atendimentos online</option>
                                                <option value="PS">Apenas vendas de produtos</option>
                                                <option value="BOTH" selected>Todos acima</option>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- <div class="col-md-12 m-0 p-0"></div> -->

                                    <div class="category_hide row m-0 p-0">
                                        <div class="col-lg-4 col-md-4 col-sm-12">
                                            <div class="form-group">
                                                <label class="personal-label">@lang('site.select') @lang('site.category')<span style="color: red;margin-left: 5px">*</span></label>
                                                <select name="category[]" id="category" class="required personal-type personal-select" @if(@Auth::guard('web')->user()->is_professional=="Y") disabled @endif>
                                                    <option value="">@lang('site.select') @lang('site.category')</option>
                                                    @if(@Auth::guard('web')->user()->is_professional=="N")
                                                        @foreach($cat as $ct)
                                                            <option value="{{ @$ct->id }}">{{ @$ct->name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-4 col-md-4 col-sm-12">
                                            <div class="form-group">
                                                <label class="personal-label"> @lang('site.sub')-@lang('site.category')</label>
                                                <select name="sub_category[]" id="sub_category" class="personal-type personal-select" @if(@Auth::guard('web')->user()->is_professional=="Y") disabled @endif>
                                                    <option value="">@lang('site.sub')-@lang('site.category')</option>

                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-4 col-md-4 col-sm-12">
                                            <div class="form-group">
                                                <a href="javascript:void(0);" class="add_new_btn btn btn-primary" style="margin-top: 37px;">+@lang('site.add')</a>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                    <div class="col-sm-12">
                                        <button type="submit" @if(@Auth::guard('web')->user()->is_professional=="Y") disabled @endif class="login_submitt">@lang('site.save')</button>
                                    </div>



                                @if(@Auth::guard('web')->user()->is_professional=="Y")

                                        <div class="buyer_table">
                                            <div class="table-responsive">
                                                <div class="table">
                                                    <div class="one_row1 hidden-sm-down only_shawo">
                                                        <div class="cell1 tab_head_sheet">@lang('site.name')</div>
                                                        <div class="cell1 tab_head_sheet">@lang('site.type')</div>

                                                    </div>
                                                    @php
                                                        $user = Auth::guard('web')->user();
                                                        $user = $user->load('userDetails');

                                                    @endphp
                                                    @foreach(@$user->userDetails as $ql)
                                                    <div class="one_row1 small_screen31">
                                                        <div class="cell1 tab_head_sheet_1">
                                                            <span class="W55_1">@lang('site.name')</span>
                                                            <p class="add_ttrr">{{ @$ql->categoryName->name }}</p>
                                                        </div>
                                                        <div class="cell1 tab_head_sheet_1">
                                                            <span class="W55_1">@lang('site.type')</span>
                                                            <p class="add_ttrr">{{ @$ql->level==0 ? "Category": "Sub-Category" }}</p>
                                                        </div>
                                                        <div class="cell1 tab_head_sheet_1">
                                                            <span class="W55_1">@lang('site.experience')</span>
                                                            <p class="add_ttrr">
                                                                @if($ql->pursuing!="Y")
                                                                {{ @$month[@$ql->from_month].' '.@$ql->from_year }} - {{ @$month[@$ql->to_month].' '.@$ql->to_year }}
                                                                @else
                                                                    @lang('site.pursuing')
                                                                @endif
                                                            </p>
                                                        </div>


                                                    </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                @endif

                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
    <script>
        $('#sell_what').change(function(){
            if($(this).val() == 'PS'){
                $('.category_hide').hide();
                $('#category').removeClass('required');
            } else {
                $('.category_hide').show();
                if(!$('#category').hasClass('required')) $('#category').addClass('required');
            }
        });
        $('#category').change(function(){
            if($('#category').val()!=""){
                var reqData = {
                  'jsonrpc' : '2.0',
                  '_token' : '{{csrf_token()}}',
                  'params' : {
                        'cat' : $('#category').val()
                    }
                };
                $.ajax({
                    url: "{{ route('front.get.subcat') }}",
                    method: 'post',
                    dataType: 'json',
                    data: reqData,
                    success: function(response){
                        if(response.status==1) {
                            var i=0, html="";
                            html = '<option value="">@lang('client_site.select_option')</option>';
                            for(;i<response.result.length;i++){
                                html+='<option value='+response.result[i].id+'>'+response.result[i].name+'</option>';
                            }
                            $('#sub_category').html(html);
                            $('#sub_category').addClass('valid');
                            $('#sub_category').addClass('required');
                        }
                        else{

                        }
                    }, error: function(error) {
                        console.error(error);
                    }
                });
            }
        });
        $('#myForm').validate({
            rules: {
                sell: {
                    required: true
                },
            },
			submitHandler: function(form) {
                var flag = 0;
                $('.category').each(function() {
                    if($(this).val()==""){
                        $(this).addClass('error');
                        $(this).parent().append('<label class="error">Este campo é obrigatório.</label>');
                        flag = 1;
                    }
                });
                $('.sub_category').each(function() {
                    if($(this).val()==""){
                        $(this).addClass('error');
                        $(this).parent().append('<label class="error">Este campo é obrigatório.</label>');
                        flag = 1;
                    }
                });
                if(flag == 1){
                    return false;
                } else {
                    $('#total_dyn').val(total);
                    swal({
                        title: "Confirme?",
                        text: "{{__('site.sell_confirm')}}",
                        icon: "warning",
                        buttons: true,
                        buttons: ["{{__('site.no')}}", "{{__('site.yes')}}"]
                    })
                    .then((willSave) => {
                        if (willSave) {
                            form.submit();
                        } else {
                            return false;
                        }
                    });
                }
            }
        });
        @if(Auth::guard('web')->user()->is_professional=="N")
            var total = 1;
            // $(document).ready(function(){
            $('.add_new_btn').click(function(){
                if($('#category_1').prop('disabled')==true){
                    return false;
                }
                total++;
                var html = '<div  class="row col-md-12" id="dyn_'+total+'"><div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"><div class="form-group"><label class="personal-label">@lang('site.select') @lang('site.category')<span style="color: red;margin-left: 5px">*</span></label><select class="personal-type personal-select category" name="category[]" id="category_'+total+'" onchange="call('+total+');" ><option value="" >@lang('client_site.select_option')</option>@foreach($cat as $ct)<option value="{{ @$ct->id }}">{{ @$ct->name }}</option>@endforeach</select></div></div>\
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"><div class="form-group">\
                                        <label class="personal-label">@lang('site.sub')-@lang('site.category')</label>\
                                        <select class="personal-type personal-select sub_category" name="sub_category[]" id="sub_category'+total+'">\
                                            <option value="">@lang('client_site.select_option')</option>\
                                        </select>\
                                    </div></div>\
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">\
                                    <div class="form-group">\
                                    <a href="javascript:void(0);" class="rmv_btn btn btn-primary" style="margin-top: 37px;" onclick="minusDiv('+total+');">-</a></div></div></div>';
                $('.category_hide').append(html);
                return false;
            });
            function minusDiv(id){
                total--;
                $('#dyn_'+id).remove();
                return false;
            }

            function call(id){
                if($('#category_'+id).val()!=""){
                    var reqData = {
                      'jsonrpc' : '2.0',
                      '_token' : '{{csrf_token()}}',
                      'params' : {
                            'cat' : $('#category_'+id).val()
                        }
                    };
                    $.ajax({
                        url: "{{ route('front.get.subcat') }}",
                        method: 'post',
                        dataType: 'json',
                        data: reqData,
                        success: function(response){
                            if(response.status==1) {
                                var i=0, html="";
                                html = '<option value="">@lang('client_site.select_option')</option>';
                                for(;i<response.result.length;i++){
                                    html+='<option value='+response.result[i].id+'>'+response.result[i].name+'</option>';
                                }
                                $('#sub_category'+id).html(html);
                                $('#sub_category'+id).addClass('valid');
                                $('#sub_category'+id).addClass('required');
                            }
                            else{

                            }
                        }, error: function(error) {
                            console.error(error);
                        }
                    });
                }
            }
        @endif
    </script>
@endsection
