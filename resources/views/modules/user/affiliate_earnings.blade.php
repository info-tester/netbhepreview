@extends('layouts.app')
@section('title')
  @lang('site.my_affiliation_earnings')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')
<section class="bkng-hstrybdy">
    <div class="container custom_container">
        <h2>@lang('site.my_affiliation_earnings')</h2>
        <div class="bokcntnt-bdy">
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.user_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="from-field">
                    <div class="weicome">
                        {{-- <h3>{{ @$dashBoardPage->title }} {{ @Auth::guard('web')->user()->nick_name ? @Auth::guard('web')->user()->nick_name : @Auth::guard('web')->user()->name }}</h3>
                        <p style="white-space: pre-wrap;">{!! @$dashBoardPage->description !!}</p> --}}
                    </div>
                    <div class="dash_main">
                        <ul>
                            <li>
                                <span><img src="{{ URL::to('public/frontend/images/dashboard_icon1.png') }}" alt=""></span>
                                <h4>@lang('client_site.net_earning')</h4>
                                <h3>R$ {{ @$totalEarning }}</h3>
                            </li>
                            <li>
                                <span><img src="{{ URL::to('public/frontend/images/dashboard_icon2.png') }}" alt=""></span>
                                <h4>@lang('client_site.paid')</h4>
                                <h3>R$ {{ @$paid }}</h3>
                            </li>
                            <li>
                                <span><img src="{{ URL::to('public/frontend/images/dashboard_icon3.png') }}" alt=""></span>
                                <h4>@lang('client_site.due')</h4>
                                <h3>R$ {{ @$due }}</h3>
                            </li>
                            <li>
                                <span><img src="{{ URL::to('public/frontend/images/dashboard_icon4.png') }}" alt=""></span>
                                <h4>Total sell products</h4>
                                <h3>{{ @$earnings->count() }}</h3>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="buyer_table">
                    <div class="table-responsive">
                    @if(sizeof(@$earnings)>0)
                        <div class="table">
                            <div class="one_row1 hidden-sm-down only_shawo">
                                <!-- <div class="cell1 tab_head_sheet">@lang('site.id').</div> -->
                                <div class="cell1 tab_head_sheet">#</div>
                                <div class="cell1 tab_head_sheet">@lang('site.image')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.title')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.category')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.subcategory')</div>
                                <div class="cell1 tab_head_sheet">@lang('client_site.user_name')</div>
                                <div class="cell1 tab_head_sheet">@lang('client_site.earning')</div>
                            </div>
                            <!--row 1-->
                                @foreach(@$earnings as $k=>$row)

                                 <div class="one_row1 small_screen31">
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">#</span>
                                            <p class="add_ttrr">{{(@$k+1)}}</p>
                                        </div>

                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.image')</span>
                                            <img src="{{URL::to('storage/app/public/uploads/product_cover_images/') . '/' . @$row->product->cover_image}}" height="50" width="60">
                                        </div>

                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.title')</span>
                                            <p class="add_ttrr"><a href="{{route('product.details',@$row->product->slug)}}" target="_blank">{{@$row->product->title}}</a></p>
                                        </div>

                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.category')</span>
                                            <p class="add_ttrr">{{@$row->product->category->category_name}}</p>
                                        </div>

                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.subcategory')</span>
                                            <p class="add_ttrr">{{@$row->product->subCategory->name}}</p>
                                        </div>

                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('client_site.user_name')</span>
                                            <p class="add_ttrr">{{ @$row->orderMaster->userDetails->nick_name ?? @$row->orderMaster->userDetails->name }}</p>
                                        </div>

                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('client_site.earning')</span>
                                            <p class="add_ttrr">{{ @$row->affiliate_commission }}</p>
                                        </div>

                                    </div>

                                @endforeach



                        </div>
                    @else
                        <center>
                            <h3 class="error">@lang('site.product_not_found') !</h3>
                        </center>
                    @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
@endsection
@section('footer')
@include('includes.footer')
<link href="{{ URL::to('public/frontend/css/calender.css') }}" rel="stylesheet" type="text/css">
<script src="{{ URL::to('public/frontend/js/jquery-ui.js') }}"></script>
<script src="{{ URL::to('public/frontend/js/moment.min.js') }}"></script>
<script>
    $('.aff_rem').click(function(){
        var thisaff = $(this);
        var productId = $(this).data('product');
        $.ajax({
            url: "{{ url('/') }}/affiliate-products-remove-list/"+productId,
            type: 'get',
            dataType: 'json',
        })
        .done(function(response) {
            console.log(response);
            if(response.status == 'success'){
                // toastr.success(response.message);
                location.reload();
            } else {
                toastr.error(response.message);
            }
        })
        .fail(function(error) {
            console.log(response);
        });
    });

    function copyText(text){
        var input = document.createElement('input');
        input.setAttribute('value', text);
        document.body.appendChild(input);
        input.select();
        var result = document.execCommand('copy');
        document.body.removeChild(input);
        toastr.success("Link Copied");
        return result;
    }
</script>

@endsection
