@extends('layouts.app')
@section('title')
  @lang('site.my_wishlist')
@endsection
@section('style')
@include('includes.style')
<style>
   .ui-timepicker-standard {
    position: absolute;
    z-index: 9999 !important;
}
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')
<section class="bkng-hstrybdy">
    <div class="container custom_container">
        <h2>@lang('site.my_wishlist')</h2>
        <div class="bokcntnt-bdy">
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.user_sidebar')
            <div class="dshbrd-rghtcntn">
                
                <div class="buyer_table">
                    <div class="table-responsive">
                    @if(sizeof(@$my_wishlist)>0)
                        <div class="table">
                            <div class="one_row1 hidden-sm-down only_shawo">
                                <!-- <div class="cell1 tab_head_sheet">@lang('site.id').</div> -->
                                <div class="cell1 tab_head_sheet">#</div>
                                <div class="cell1 tab_head_sheet">@lang('site.image')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.title')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.professional')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.category')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.subcategory')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.action')</div>
                            </div>
                            <!--row 1-->
                                @php
                                    $i=1;
                                @endphp

                                @foreach(@$my_wishlist as $k=>$row)

                                 <div class="one_row1 small_screen31">
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">#</span>
                                            <p class="add_ttrr">{{(@$k+1)}}</p>
                                        </div>

                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.image')</span>
                                            <img src="{{URL::to('storage/app/public/uploads/product_cover_images/') . '/' . @$row->products->cover_image}}" height="50" width="60">
                                           
                                        </div>


                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.title')</span>
                                            <p class="add_ttrr"><a href="{{route('product.details',@$row->products->slug)}}" target="_blank">{{@$row->products->title}}</a></p>
                                        </div>

                                         <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.title')</span>
                                            <p class="add_ttrr">{{ @$row->products->professional->nick_name ? @$row->products->professional->nick_name :@$row->products->professional->name }}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.title')</span>
                                            <p class="add_ttrr">{{@$row->products->category->category_name}}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.title')</span>
                                            <p class="add_ttrr">{{@$row->products->subCategory->name}}</p>
                                        </div>

                                        <input type="hidden" name="p_id" id="p_id">
                                        <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                            
                                        @if(in_array(@$row->products->id , $orderedProducts))
                                            <p class="text-center text-secondary w-100" style="font-size:1rem;">@lang('client_site.already_ordered')</p>
                                        @else
                                            @if(date('Y-m-d') >= date('Y-m-d', strtotime(@$row->products->purchase_start_date)))
                                                @if(@$row->products->purchase_end_date && date('Y-m-d') <= date('Y-m-d', strtotime(@$row->products->purchase_end_date)))
                                                    <p class="text-center text-secondary w-100" style="font-size:1rem;">@lang('client_site.sale_over')</p>
                                                @else
                                                    
                                                    
                                                    <a href="{{route('product.cart')}}" class="solid_btn goToCart_{{@$row->products->id}}" @if(in_array(@$row->products->id , $cartData)) style="display: block;" @else style="display:none;" @endif > @lang('client_site.go_to_cart')</a>

                                                     <a href="javascript:;" class="solid_btn addToCart addcrt_{{@$row->products->id}}"  data-product="{{$row->products->id}}" @if(in_array(@$row->products->id , $cartData)) style="display:none;" @else style="display:block;" @endif > @lang('client_site.add_to_cart')</a>
                                                    
                                                @endif
                                            @else
                                                    <p class="text-center text-secondary w-100" style="font-size:1rem;"><span style="font-size:0.8rem;">@lang('client_site.available_from')</span><br>{{date('jS F, Y', strtotime(@$row->products->purchase_start_date))}}</p>
                                            @endif
                                        @endif
                                            
                                        </div>
                                    </div>

                                @endforeach

                                

                        </div>
                    @else
                        <center>
                            <h3 class="error">@lang('site.product_not_found') !</h3>
                        </center>
                    @endif
                   
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- modal --}}

      <div class="modal fade" id="myModal">
            <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                  <h4 class="modal-title">@lang('site.reschedule') @lang('site.appoinment') </h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form name="myForm2" id="myForm2" method="post" action="{{ route('user.booking.reschedule') }}">
                        @csrf
                        <input type="hidden" name="token_no" id="token_no">
                        <input type="hidden" name="professional_id" id="professional_id">
                        <input type="hidden" id="start-time">
                        <input type="hidden" id="end-time">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>@lang('site.date')</label>
                                        <input type="text" class="required form-control" name="date" id="date" value="">
                                    </div>
                                </div>

                              <div class="col-md-6 col-sm-6 col-xs-12">
                                {{-- @dump($order->productDetails) --}}
                                  <div class="form-group">
                                      <label>@lang('site.time')</label>
                                      <select class="required form-control " name="time" id="time">
                                      {{-- <option value="">Select Time Slot</option>
                                      @foreach($booking as $val)
                                        <option value="{{ $val->start_time.'-'.$val->end_time }}">{{ $val->start_time }}-{{ $val->end_time }}</option>
                                      @endforeach --}}
                                      </select>
                                      <span id="time_err" style="font-weight: bold;color: red;"></span>
                                  </div>
                              </div>
                          </div>
                        </div>
                    </form>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                  <button type="button" class="btn btn-primary sbmt">@lang('site.submit')</button>

                  <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('site.close')</button>
                </div>

              </div>
            </div>
      </div>

    {{-- modal --}}

</section>
@endsection
@section('footer')
@include('includes.footer')
<link href="{{ URL::to('public/frontend/css/calender.css') }}" rel="stylesheet" type="text/css">
<script src="{{ URL::to('public/frontend/js/jquery-ui.js') }}"></script>
<script src="{{ URL::to('public/frontend/js/moment.min.js') }}"></script>


<script>
    function setVl(tn, dt,tm, pid, startTime, endTime){
        $('#token_no').val(tn);
        $('#date').val(dt);
        $('#professional_id').val(pid);
        $('#start-time').val(startTime);
        $('#end-time').val(endTime);
        // $('#time').html('<option value="'+tm+'" selected>'+tm+'</option>');
        $('#date').trigger('change');
    }

    $('#date').change(function(){
        if($("#date").val()!="") {
            var html='';
            var time_formate='';
            var from_time='';
            var to_time='';
            var date = $(this).val();
            var professional_id = $('#professional_id').val();
            html ='<option value="">@lang('site.select_time_slot')</option>';
            $.ajax({
                url: '{{ route("rescheduled.time.list") }}',
                type:"GET",
                data: {
                    date: date,
                    user_id: professional_id,
                    start: $('#start-time').val(),
                    end: $('#end-time').val(),
                },
                success: function(response) {
                    console.log(response);
                    response.forEach(function(item, index){
                        slot_start = (item.slot_start).substr(0,5);
                        slot_end = (item.slot_end).substr(0,5);
                        time_formate = slot_start.concat('-', slot_end);
                        // alert(time_formate);
                        html+='<option value="'+time_formate+'">'+time_formate+'</option>';
                    });
                    $('#time').html(html);
                },
            });
        }
    });
    $('#myForm2').validate();
    $('.timepicker').timepicker({
        timeFormat: 'H:mm',
        interval: 15,
        minTime: '00',
        maxTime: '11:00pm',
        startTime: '09:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
    $(function() {
        $("#date").datepicker({
            dateFormat: "dd-mm-yy",
            defaultDate: new Date(),
            minDate:new Date(),
        });

        $("#datepicker").datepicker({dateFormat: "dd-mm-yy",
            monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
            defaultDate: new Date(),
            onClose: function( selectedDate ) {
                $( "#datepicker1").datepicker( "option", "minDate", selectedDate );
            }
        });
    $("#datepicker1").datepicker({dateFormat: "dd-mm-yy",
         monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho',
            'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
     defaultDate: new Date(),
          onClose: function( selectedDate ) {
          $( "#datepicker" ).datepicker( "option", "maxDate", selectedDate );
        }
     });

    });
</script>
<script>
    $('#srch').click(function(){
        $('#myForm').submit();
    });
    $('.sbmt').click(function(){
        if($('#date').val()!="" && $('#time').val()!=""){
            $('.parent_loader').show();
            setTimeout(function(){ $('#myForm2').submit(); }, 3000);
        }
        else{
             $('#myForm2').submit();
        }

    });
    $('#time').click(function(){
        if($('#date').val()==''){
            $(this).val('');
            $('#time_err').html('select date first');
            return false;
        }
        else{
            $('#time_err').html('');
        }
    });
    $(document).ready(function(){
        $('#myForm').validate({
            rules:{
                phone_no:{
                    digits:true,
                    maxlength:10,
                    minlength:10
                }
            }
        });

        $('#myForm1').validate({
            rules:{
                old_password:{
                    required:true
                },
                new_password:{
                    required:true,
                    minlength:8,
                    maxlength:8
                },
                confirm_password:{
                    required:true,
                    minlength:8,
                    maxlength:8,
                    equalTo:"#new_password"
                }
            }
        });
    });

    $('#phone_no').change(function(){
        if($('#phone_no').val()!=""){
            var reqData = {
              'jsonrpc' : '2.0',
              '_token' : '{{csrf_token()}}',
              'params' : {
                    'no' : $('#phone_no').val()
                }
            };
            $.ajax({
                url: "{{ route('chk.mobile') }}",
                method: 'post',
                dataType: 'json',
                data: reqData,
                success: function(response){
                    if(response.status==1) {
                        $('#phone_no').val("");
                        $('#ph_error').text('@lang('client_site.phone_number_allready_exist').');
                    }

                }, error: function(error) {
                    toastr.info('@lang('client_site.Try_again_after_sometime')');
                }
            });
        }
    });


</script>

<script>
    $(document).ready(function(){
        
    $('.addToCart').click(function(){
        var productId = $(this).data('product');
        console.log(productId)
        var reqData = {
            'jsonrpc': '2.0',
            '_token': '{{csrf_token()}}',
            'params': {
                productId: productId,
            }
        };
        $.ajax({
            url: '{{ route('product.add.to.cart') }}',
            type: 'post',
            dataType: 'json',
            data: reqData,
        })
        .done(function(response) {
            console.log(response);
            $('.cou_cart').text(response.result.cart.length);
            $('.addcrt_'+productId).css('display','none');
            $('.goToCart_'+productId).css('display','block');
        })
        .fail(function(error) {
            console.log("error", error);
        })
        .always(function() {
            console.log("complete");
        })
    })
    $('#buyNow').click(function(){
        var productId = $(this).data('product');
        console.log(productId)
        var reqData = {
            'jsonrpc': '2.0',
            '_token': '{{csrf_token()}}',
            'params': {
                productId: productId,
            }
        };
        $.ajax({
            url: '{{ route('product.add.to.cart') }}',
            type: 'post',
            dataType: 'json',
            data: reqData,
        })
        .done(function(response) {
            window.location.href = '{{route('product.order.store')}} ';
        })
        .fail(function(error) {
            console.log("error", error);
        })
        .always(function() {
            console.log("complete");
        })
    })
})
</script>

@endsection
