@extends('layouts.app')
@section('title', 'My Bookings')
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')
<section class="bkng-hstrybdy">
    <div class="container">
        <h2>My Account</h2>
        <div class="bokcntnt-bdy">
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>Menu</p>
            </div>
            @include('includes.user_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="from-field">
                    <div class="frmfld">
                        <div class="form-group">
                            <label class="search_label">From Date</label>
                            <input type="text" id="datepicker" class="dashboard-type" placeholder="Select Date">
                            <img class="pstn" src="images/clndr.png">
                        </div>
                    </div>
                    <div class="frmfld">
                        <div class="form-group">
                            <label class="search_label">To Date</label>
                            <input type="text" id="datepicker1" class="dashboard-type" placeholder="Select Date">
                            <img class="pstn" src="images/clndr.png">
                        </div>
                    </div>
                    <div class="frmfld">
                        <div class="form-group">
                            <label class="search_label">Select Status</label>
                            <select class="dashboard-type dashboard_select">
                                <option>Select Status</option>
                                <option>Select Status </option>
                            </select>
                        </div>
                    </div>
                    <div class="frmfld">
                        <button class="banner_subb">Filter</button>
                    </div>
                </div>
                <div class="buyer_table">
                    <div class="table-responsive">
                        <div class="table">
                            <div class="one_row1 hidden-sm-down only_shawo">
                                <div class="cell1 tab_head_sheet">ID</div>
                                <div class="cell1 tab_head_sheet">Professional</div>
                                <div class="cell1 tab_head_sheet">Date/Time</div>
                                <div class="cell1 tab_head_sheet">Duration</div>
                                <div class="cell1 tab_head_sheet">Topic</div>
                                <div class="cell1 tab_head_sheet">Status</div>
                                <div class="cell1 tab_head_sheet">Action</div>
                            </div>
                            <!--row 1-->                        
                            <div class="one_row1 small_screen31">
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">ID</span>
                                    <p class="add_ttrr">BA250</p>
                                </div>
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">Professional</span>
                                    <p class="add_ttrr">R. Manna</p>
                                </div>
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">Date/Time</span>
                                    <p class="add_ttrr">12th May 2019, 05:00 pm</p>
                                </div>
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">Duration</span>
                                    <p class="add_ttrr">3 Hour</p>
                                </div>
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">Topic</span>
                                    <p class="add_ttrr">MBA <br>Guidance</p>
                                </div>
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">Status</span>
                                    <p class="add_ttrr">Awaiting Approval</p>
                                </div>
                                <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                    <span class="W55_1">Action</span>
                                    --
                                </div>
                            </div>
                            <!--row 1-->
                            <!--row 1-->                        
                            <div class="one_row1 small_screen31">
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">ID</span>
                                    <p class="add_ttrr">BA250</p>
                                </div>
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">Professional</span>
                                    <p class="add_ttrr">R. Manna</p>
                                </div>
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">Date/Time</span>
                                    <p class="add_ttrr">12th May 2019, 05:00 pm</p>
                                </div>
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">Duration</span>
                                    <p class="add_ttrr">3 Hour</p>
                                </div>
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">Topic</span>
                                    <p class="add_ttrr">MBA <br>Guidance</p>
                                </div>
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">Status</span>
                                    <p class="add_ttrr">Approved</p>
                                </div>
                                <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                    <span class="W55_1">Action</span>
                                    <a href="#" class="rjct">Pay now </a>
                                </div>
                            </div>
                            <!--row 1-->
                            <!--row 1-->                        
                            <div class="one_row1 small_screen31">
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">ID</span>
                                    <p class="add_ttrr">BA250</p>
                                </div>
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">Professional</span>
                                    <p class="add_ttrr">R. Manna</p>
                                </div>
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">Date/Time</span>
                                    <p class="add_ttrr">12th May 2019, 05:00 pm</p>
                                </div>
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">Duration</span>
                                    <p class="add_ttrr">3 Hour</p>
                                </div>
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">Topic</span>
                                    <p class="add_ttrr">MBA <br>Guidance</p>
                                </div>
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">Status</span>
                                    <p class="add_ttrr">Awaiting Approval</p>
                                </div>
                                <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                    <span class="W55_1">Action</span>
                                    --
                                </div>
                            </div>
                            <!--row 1-->
                            <!--row 1-->                        
                            <div class="one_row1 small_screen31">
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">ID</span>
                                    <p class="add_ttrr">BA250</p>
                                </div>
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">Professional</span>
                                    <p class="add_ttrr">R. Manna</p>
                                </div>
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">Date/Time</span>
                                    <p class="add_ttrr">12th May 2019, 05:00 pm</p>
                                </div>
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">Duration</span>
                                    <p class="add_ttrr">3 Hour</p>
                                </div>
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">Topic</span>
                                    <p class="add_ttrr">MBA <br>Guidance</p>
                                </div>
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">Status</span>
                                    <p class="add_ttrr">Approved</p>
                                </div>
                                <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                    <span class="W55_1">Action</span>
                                    <a href="#" class="rjct">Pay now </a>
                                </div>
                            </div>
                            <!--row 1-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
<script>
    $(document).ready(function(){
        $('#myForm').validate({
            rules:{
                phone_no:{
                    digits:true,
                    maxlength:10,
                    minlength:10
                }
            }
        });
    
        $('#myForm1').validate({
            rules:{
                old_password:{
                    required:true
                },
                new_password:{
                    required:true,
                    minlength:8,
                    maxlength:8
                },
                confirm_password:{
                    required:true,
                    minlength:8,
                    maxlength:8,
                    equalTo:"#new_password"
                }
            }
        });
    });
    
    $('#phone_no').change(function(){
        if($('#phone_no').val()!=""){
            var reqData = {
              'jsonrpc' : '2.0',
              '_token' : '{{csrf_token()}}',
              'params' : {
                    'no' : $('#phone_no').val()
                }
            };
            $.ajax({
                url: "{{ route('chk.mobile') }}",
                method: 'post',
                dataType: 'json',
                data: reqData,
                success: function(response){
                    if(response.status==1) {
                        $('#phone_no').val("");
                        $('#ph_error').text('Phone number allready exist.');
                    }
                    
                }, error: function(error) {
                    toastr.info('Try again after sometime');
                }
            });
        }
    });
</script>
@endsection