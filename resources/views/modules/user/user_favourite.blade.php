@extends('layouts.app')
@section('title')
  @lang('site.my_professionals')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.my_professionals')</h2>
        <div class="bokcntnt-bdy">
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.user_sidebar')
            <div class="dshbrd-rghtcntn professionls">
                @if(sizeof(@$fav)>0)
                    @foreach(@$fav as $fv)

                        <div class="myusr">
                        
                            <div class="myusr-pic"><img src="{{ @$fv->userDetails->profile_pic ? URL::to('storage/app/public/uploads/profile_pic').'/'.@$fv->userDetails->profile_pic: URL::to('public/frontend/images/no_img.png') }}"></div>
                            <a class="rem-pro" href="{{ route('remove.my.favourite',['id'=>@$fv->professional_id]) }}">@lang('site.remove')</a>
                            <div class="usrdsp">
                                <h3>{{ @$fv->userDetails->nick_name ? @$fv->userDetails->nick_name : @$fv->userDetails->name }}</h3>
                                <h5>CRP 06/129759</h5>
                                <ul>
                                    {!! getStars(@$fv->avg_review) !!}
                                    <li>({{ $fv->total_review ?? 0 }})</li>
                                </ul>
                                <!-- <ul>
                                    <li><img src="{{ URL::to('public/frontend/images/star.png') }}" alt=""></li>
                                    <li><img src="{{ URL::to('public/frontend/images/star.png') }}" alt=""></li>
                                    <li><img src="{{ URL::to('public/frontend/images/star.png') }}" alt=""></li>
                                    <li><img src="{{ URL::to('public/frontend/images/star.png') }}" alt=""></li>
                                    <li><img src="{{ URL::to('public/frontend/images/star1.png') }}" alt=""></li>
                                    <li>(35)</li>
                                </ul> -->
                                <p style="white-space: pre-wrap;">{!! @$fv->userDetails->description !!}</p>
                            </div>
                            
                        </div>
                    @endforeach
                @else
                    <div class="myusr">
                        <center><h3><span class="error">@lang('site.empty_favorite')</span></h3></center>
                    </div>
                @endif
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
@endsection