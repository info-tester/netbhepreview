@extends('layouts.app')
@section('title')
  @lang('site.live_video_conference')
@endsection
@section('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
@include('includes.style')
<style type="text/css" media="screen">
.fade:not(.show) {
    opacity: 1 !important;
}    
</style>
@endsection
@section('scripts')
{{-- @include('includes.scripts') --}}
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')
<section class="bkng-hstrybdy contt">
    <div class="bokcntnt-bdy add_for_video1">
        <div class="container mobile_width">
            <div class="row">
                <h1 class="video_hhd">@lang('site.live_video_conference')</h1>
                <div class="fulscrn " id="fulvido"> 
                    <div id="remote-media" class="full_bg"></div>
                    <div class="mutevdo">
                        <div class="mutxt">    
                        <p id="remote-audio-mute" style="display: none"><i class="fa fa-microphone-slash" aria-hidden="true"></i> @lang('client_site.audio_muted')</p>
                        <p id="remote-video-mute" style="display: none"><i class="fas fa-video-slash"></i>@lang('client_site.video_muted') </p>
                        <span id="connecting">@lang('client_site.connecting')...</span>
                        </div>
                    </div>
                    <div id="controls">
                        <div id="preview">
                            <div class="vdomutetxt">
                                <div class="twobtn">
                                    <span id="local-audio-mute" style="display: none"><i class="fa fa-microphone-slash" aria-hidden="true"></i></span>
                                    <span id="local-video-mute" style="display: none"><i class="fas fa-video-slash"></i></span>
                                </div>
                            </div>
                            <div style="display: none" id="local-media"></div>
                            <button id="button-preview">Preview My Camera</button>
                        </div>
                    </div>
                    <div class="all_video_btn">
                        <button title="Mute audio" data-toggle="tooltip" id="audio-mute" class="add_for_video2 icon_rm_001"><i class="fa fa-microphone-slash" aria-hidden="true"></i></button>
                        <button title="Unmute audio" data-toggle="tooltip" id="audio-unmute" class="add_for_video2 icon_rm_001 icon_rm_003" style="display: none"><i class="fa fa-microphone-slash" aria-hidden="true"></i></button>

                        <button id="button-leave" class="add_for_video2"><i class="fa fa-phone" aria-hidden="true"></i></button>

                        <button title="Mute video" data-toggle="tooltip" id="video-mute" class="add_for_video2 icon_rm_002"><i class="fas fa-video-slash"></i></button>
                        <button title="Unmute video" data-toggle="tooltip" id="video-unmute" class="add_for_video2 icon_rm_002 icon_rm_003" style="display: none"><i class="fas fa-video-slash"></i></button>

                        <button title="Full screen" data-toggle="tooltip" title="Full screen" data-toggle="tooltip" id="video-full" class="add_for_video2 icon_rm_002"><i class="fas fa-compress"></i></button>
                        <button title="Exit full screen" data-toggle="tooltip" id="video-small" class="add_for_video2 icon_rm_002 icon_rm_003" style="display: none"><i class="fas fa-compress"></i></button>

                        <button title="Chat" data-toggle="tooltip" id="video-chat" class="add_for_video2 icon_rm_002 chat_btn video-chat"><i class="fa fa-comment"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
    <input type="text" class="message_master_id" hidden >
    <div class="cht-popup"></div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="{{URL::to('public/admin/js/jquery.toast.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
    <script src="//media.twiliocdn.com/sdk/js/video/releases/2.0.0-beta15/twilio-video.min.js"></script>
    <script>var userType = localStorage.getItem('userType');</script>
    <script src="{{URL::to('public/frontend/js/twilio-quickstart.js') }}"></script>
    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
    {{-- @include('includes.footer') --}}
    <script>
    $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();
    });
    </script>
    <script>
        var myTimeInterval = '';
        var videoMute = false;
        var audioMute = false;
        var increment=1;
        var user_arr = [];
        var message_master_id;
        var keyup = 0;
        var name = "{{@Auth::user()->username}}"; 
        var my_id = parseInt("{{@Auth::user()->id}}"); 
        var emoji_array = ["😀", "😃", "😄", "😁", "😆", "😅", "😂", "🤣","😊", "😇", "🙂", "🙃", "😉", "😌", "😍", "🥰", "😘", "😗", "😙", "😚", "😋", "😛", "😝", "😜", "🤪", "🤨", "🧐", "🤓", "😎", "🤩", "🥳", "😏", "😒", "😞", "😔", "😟", "😕", "🙁", "☹️", "😣", "😖", "😫", 
        "😩", "🥺", "😢", "😭", "😤", "😠", "😡", "🤬", "🤯", "😳", "🥵", "🥶", "😱", "😨", "😰", "😥", "😓", "🤗", "🤔", "🤭", "🤫", "🤥", "😶", "😐", "😑", "😬", "🙄", "😯", "😦", "😧", "😮", "😲", "🥱", "😴", "🤤", "😪", "😵", "🤐", "🥴", "🤢", "🤮", "🤧", "😷", "🤒", "🤕", 
        "🤑", "🤠", "😈", "👿", "👹", "👺", "🤡", "💩", "👻", "💀", "☠️", "👽", "👾", "🤖", "🎃", "😺", "😸", "😹", "😻", "😼", "😽", "🙀", "😿", "😾", "👋", "🤚", "🖐", "✋", "🖖", "👌", "🤏", "✌️", "🤞", "🤟", "🤘", "🤙", "👈", "👉", "👆", "🖕", "👇", "☝️", "👍", "👎", "✊", "👊", "🤛", 
        "🤜", "👏", "🙌", "👐", "🤲", "🤝", "🙏",   "✍️", "💅", "🤳", "💪", "🦾", "🦵", "🦿", "🦶", "👣", "👂", "🦻", "👃", "🧠", "🦷", "🦴", "👀", "👁", "👅", "👄", "💋", "🩸", "👶", "👧", "🧒", "👦", "👩", "🧑", "👨", "👩‍🦱", "👨‍🦱", "👩‍🦰", "👨‍🦰", "👱‍♀️", "👱", "👱‍♂️", "👩‍🦳", "👨‍🦳", "👩‍🦲", "👨‍🦲", 
        "🧔", "👵", "🧓", "👴", "👲", "👳‍♀️", "👳", "👳‍♂️", "🧕", "👮‍♀️", "👮", "👮‍♂️", "👷‍♀️", "👷", "👷‍♂️", "💂‍♀️", "💂", "💂‍♂️", "🕵️‍♀️", "🕵️", "🕵️‍♂️", "👩‍⚕️", "👨‍⚕️", "👩‍🌾", "👨‍🌾", "👩‍🍳", "👨‍🍳", "👩‍🎓", "🧑‍🎓", "👨‍🎓", "👩‍🎤", "👨‍🎤", "👩‍🏫", "👨‍🏫", "👩‍🏭", "👨‍🏭", "👩‍💻", "💻", 
        "👨‍💻", "👩‍💼", "👨‍💼", "👩‍🔧", "🧑‍🔧", "👨‍🔧", "👩‍🔬", "🧑‍🔬", "👨‍🔬", "👩‍🎨", "🧑‍🎨", "👨‍🎨", "👩‍🚒", "👨‍🚒", "👩‍✈️", "🧑‍✈️", "👨‍✈️", "👩‍🚀", "👨‍🚀", "👩‍⚖️", "👨‍⚖️", "👰", "🤵", "👸", "🤴", "🦸‍♀️", "🦸", "🦸‍♂️", "🦹‍♀️", "🦹", "🦹‍♂️", "🤶", "🎅", "🧙‍♀️", 
        "🧙", "🧙‍♂️", "🧝‍♀️", "🧝", "🧝‍♂️", "🧛‍♀️", "🧛", "🧛‍♂️", "🧟‍♀️", "🧟", "🧟‍♂️", "🧞‍♀️", "🧞", "🧞‍♂️", "🧜‍♀️", "🧜", "🧜‍♂️", "🧚‍♀️", "🧚", "🧚‍♂️", "👼", "🤰", "🤱", "🙇‍♀️", "🙇", "🙇‍♂️", "💁‍♀️", "💁", "💁‍♂️", "🙅‍♀️", "🙅", "🙅‍♂️", "🙆‍♀️", "🙆", "🙆‍♂️", "🙋‍♀️", "🙋", "🙋‍♂️", "🧏‍♀️", "🧏", "🧏‍♂️", "🤦‍♀️", "🤦", 
        "🤦‍♂️", "🤷‍♀️", "🤷", "🤷‍♂️", "🙎‍♀️", "🙎", "🙎‍♂️", "🙍‍♀️", "🙍", "🙍‍♂️", "💇‍♀️", "💇", "💇‍♂️", "💆‍♀️", "💆", "💆‍♂️", "🧖‍♀️", "🧖", "🧖‍♂️", "💅", "🤳", "💃", "🕺", "👯‍♀️", "👯", "👯‍♂️", "🕴", "👩‍🦽", "👨‍🦽", "👩‍🦼", "👨‍🦼", "🚶‍♀️", "🚶", "🚶‍♂️", "👩‍🦯", "👨‍🦯", "🧎‍♀️", "🧎", "🧎‍♂️", "🏃‍♀️", "🏃", "🏃‍♂️", "🧍‍♀️", "🧍", 
        "🧍‍♂️", "👭", "🧑‍🤝‍🧑", "👬", "👫", "👩‍❤️‍👩", "💑", "👨‍❤️‍👨", "👩‍❤️‍👨", "👩‍❤️‍💋‍👩", "💏", "👨‍❤️‍💋‍👨", "👩‍❤️‍💋‍👨", "👪", "👨‍👩‍👦", "👨‍👩‍👧", "👨‍👩‍👧‍👦", "👨‍👩‍👦‍👦", "👨‍👩‍👧‍👧", "👨‍👨‍👦", "👨‍👨‍👧", "👨‍👨‍👧‍👦", "👨‍👨‍👦‍👦", "👨‍👨‍👧‍👧", "👩‍👩‍👦", "👩‍👩‍👧", "👩‍👩‍👧‍👦", "👩‍👩‍👦‍👦", "👩‍👩‍👧‍👧", "👨‍👦", "👨‍👦‍👦", "👨‍👧", "👨‍👧‍👦", "👨‍👧‍👧", "👩‍👦", "👩‍👦‍👦", "👩‍👧", "👩‍👧‍👦", "👩‍👧‍👧", "🗣", "👤", "👥", 
        "🧳", "🌂", "☂️", "🧵", "🧶", "👓", "🕶", "🥽", "🥼", "🦺", "👔", "👕", "👖", "🧣", "🧤", "🧥", "🧦", "👗", "👘", "🥻", "🩱", "🩲", "🩳", "👙", "👚", "👛", "👜", "👝", "🎒", "👞", "👟", "🥾", "🥿", "👠", "👡", "🩰", "👢", "👑", "👒", "🎩", "🎓", "🧢", "💄", "💍", "💼"
        ];
        var emojis = "";
        for(var _k = 0; _k < emoji_array.length; _k++){
            emojis+=`<li class="emoticon">${emoji_array[_k]}</li>`;
        }
        
        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;

        var pusher = new Pusher('8b0b4b0f1112e526c284', {
            cluster: 'ap2'
        });

        var socketId = null;
        pusher.connection.bind('connected', function() {
            socketId = pusher.connection.socket_id;
        });
        var channel = pusher.subscribe('netbhe');
        // channel.bind('my-event', function(data) {
        //   alert(JSON.stringify(data));
        // });

        //Typing
        //setup before functions
        var typingTimer;                //timer identifier
        var doneTypingInterval = 3000;  //time in ms (3 seconds)
        
        function nl2br (str, is_xhtml) {
        if (typeof str === 'undefined' || str === null) {
            return '';
        }
        var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
            return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
        }
        function getTimeDiff(datetime) {
            var datetime = new Date( datetime ).getTime();
            var Current = new Date().getTime();
            var CalcTime = Current - datetime;
            var Years = Math.floor(CalcTime / 1000 / 60 / 60 / 24 / 7 / 4 / 12);
            CalcTime -= Years * (1000 * 60 * 60 * 24 * 7 * 4 * 12);
            var Months = Math.floor(CalcTime / 1000 / 60 / 60 / 24 / 7 / 4);
            CalcTime -= Months * (1000 * 60 * 60 * 24 * 7 * 4);
            var Days = Math.floor(CalcTime / 1000 / 60 / 60 / 24);
            CalcTime -= Days * (1000 * 60 * 60 * 24);
            var Hours = Math.floor(CalcTime / 1000 / 60 / 60);
            CalcTime -= Hours * (1000 * 60 * 60);
            var Minutes = Math.floor(CalcTime / 1000 / 60);
            CalcTime -= Minutes * (1000 * 60);
            var Seconds = Math.floor(CalcTime / 1000 / 60);
            if(Years>1) {
                timeDiff = Years+" Years ago";
            }else if (Months>1) {
                timeDiff = Months+" Months ago";
            }else if (Days>1) {
                timeDiff = Days+" Days ago";
            }else if (Hours>1) {
                timeDiff = Hours+' Hours ago';
            }else if (Minutes>1) {
                timeDiff = Minutes+' Minutes ago';
            }else {
                timeDiff = Seconds+' Seconds ago';
            }   
            return timeDiff;
        }
        function sendMsg1(id){
            var replymsg = "";
            var err = 0;
            var filecnt = 0;
            if($('.chat-msg-'+id).val()!="")
                replymsg = $.trim($('.chat-msg-'+id).val());
            var files = $('.file-'+id).prop('files');
            if(Object.keys(files).length>0){
                $('.typing-text-'+id).text("uploading...");
                $('.chat-bodys-'+id).css("height", "228px");
            }
            data = new FormData();
            data.append('_token', "{{ csrf_token() }}");
            data.append('booking_id',id);
            data.append('message',replymsg);
            data.append('socket_id',socketId);
            console.log("SEND MSG 1");
            var err = 0;
            var filecnt = 0;
            console.log("Stopped typing");
            startStopTypingAJAX('end');
            keyup = 0;
            $.each(files, function(k,file){
                var fileExt = file.name.split('.').pop();
                if(fileExt == "bat" || fileExt == "img" || fileExt == "exe") {
                    alert("This file cannot be uploaded.");
                    err = 1;
                } else {
                    data.append('file', file);
                }
            });
            $('.file-append-'+id).empty();
            if(err == 0){
                filecnt = Object.keys(files).length;
            }
            if (replymsg || filecnt>0) {
                $.ajax({
                    url: '{{ route("send.ajax") }}',
                    type: 'POST',
                    dataType: 'JSON',
                    data: data,
                    enctype: 'multipart/form-data',
                    processData: false,  
                    contentType: false,
                })
                .done(result => {
                    $('.typing-text-'+id).text("");
                    $('.chat-bodys-'+id).css("height", "242px");
                    if (result.result) {
                        console.log(user_arr);
                        console.log(id);
                        index=user_arr.indexOf(id);
                        console.log(index);
                        message_master_id=result.result.message_master_id;
                        user_arr[index]=message_master_id;
                        type="I";
                        token=result.result.token;
                        $('.main-chat-box-'+id).remove();
                        project_title=result.result.project_title;
                        var style='';
                        if(result.result.online_status=='N'){
                            style='style="color:red"';
                        }
                            var curroute = "{{Route::is('chats')}}";
                            if(curroute!=1){
                                html=`<div class="main-chat-box main-chat-box-${message_master_id}" style="margin-right: ${(index) * 300}px">
                                    <div class="chat-heads">
                                        <div class="chat-title">
                                        <input type="hidden" id="type-${message_master_id}" value="${type}">
                                            <span><b class="username-${message_master_id}">${result.result.username}</b></span>
                                        </div>
                                        <div class="chat-action-des" data-id="${message_master_id}">
                                            <a href="javascript:;" class="minimise-chat-box" data-id="${message_master_id}"><i aria-hidden="true" class="fa fa-minus"></i></a>
                                            <a href="javascript:;" class="chat-action" data-id="${message_master_id}"><i aria-hidden="true" class="fa fa-times"></i></a>
                                        </div>
                                    </div>
                                    <div class="buildme">
                                        <p>${token}<span class="id_text pull-right">${result.result.booking_id}</span></p>
                                    </div>
                                    <div class="chat-bodys chat-bodys-${message_master_id}"></div>
                                    <div class="file-append file-append-${message_master_id}"></div>
                                    <div class="typing-text typing-text-${message_master_id}"></div>
                                    <form class="frm_msg" onsubmit="return sendMsg(${message_master_id})" data-id="${message_master_id}">
                                        <div class="rm_emoji_area"><ul>${emojis}</ul></div>
                                        <div class="chat-fots">
                                            <textarea class="type-msgs chat-msg-${message_master_id}" data-id="${message_master_id}" placeholder="Type your message.."></textarea>
                                            <div class="upload_box">
                                                <input type="file" id="file" data-id="${message_master_id}" class="file-upload-chat file-${message_master_id}">
                                                <label for="file" class="btn-2"></label>
                                            </div>
                                            <a href="#" class="rm_emoji">🙂</a>
                                            <p class="file-name-foots file-name-${message_master_id}"></p>
                                            <a href="#" class="chat-send" data-id="${message_master_id}"><i class="fa fa-paper-plane"></i></a>
                                        </div>
                                    </form>
                                </div>`;
                                $('.cht-popup').append(html);
                                // user_arr.push(message_master_id);
                                // localStorage['popup_arr'] = JSON.stringify(msgmaster_arr);
                            } else {
                                console.log('supposed to attach');
                                var subcat = "";
                                if(result.subcategory !== null || result.subcategory !== undefined){ 
                                    subcat = ` > ${result.subcategory}`;
                                }
                                var chat_body_html=`<div class="px-1 py-1 chat-box bg-white chat-box-${message_master_id}"></div>`
                                var text_area_html=`<textarea class="hire-type chat-msg-${message_master_id}" data-id="${message_master_id}" placeholder="Type your message here..." style="height: 112px;resize: none;"></textarea>  
                                            <a href="#" class="rm_emoji new-top">🙂</a>
                                            <div class="mesg-btns">
                                                <div class="upload_box edit-upload">
                                                    <input type="file" id="file" data-id="${message_master_id}" class="file-${message_master_id}">
                                                    <label for="file" class="btn-2">Attach File</label>
                                                </div>
                                            <button type="submit" class="reply-btns" data-id="${message_master_id}"><img src="{{ URL::to('public/frontend/images/reply.png') }}" alt=""> Reply</button>
                                            </div>`;
                                var close_button = `<button type="button" class="close-chat pull-right m-0" data-id="${message_master_id}">End Chat</button>`;
                                // var emoji_html = `<div class="rm_emoji_area new-emoji-top" ><ul>${emojis}</ul></div>`;
                                $('#conv').find('.common-top').find('h4').text(`${result.result.username}`);
                                $('#conv').find('.common-top').find('p').text(`${result.result.category}`);
                                $('#conv').find('.common-top').find('#button_div').empty();
                                $('#conv').find('.common-top').find('#button_div').append(close_button);
                                $('#conv').find('.chat_chat_body').empty();
                                $('#conv').find('.chat_chat_body').append(chat_body_html);
                                $('#conv').find('.attachedd-type').empty();
                                $('#conv').find('.attachedd-type').append(text_area_html);
                                $('#conv').find('.attachedd-type').parent().find('.new-emoji-top').find('ul').empty();
                                $('#conv').find('.attachedd-type').parent().find('.new-emoji-top').find('ul').append(emojis);
                            }
                            $.ajax({
                                url: '{{ route("user.message") }}',
                                type: 'POST',
                                dataType: 'JSON',
                                data: {
                                    message_master_id:message_master_id,
                                    type:type,
                                    _token: '{{ csrf_token() }}',
                                }
                            })
                            .done(result => {
                            if (result.result) {
                                if(curroute!=1){
                                    result.result.forEach(function(item, index){
                                        var imgg1 = "@if(@Auth::user()->profile_pic){{url('storage/app/public/uploads/profile_pic/').'/'.Auth::user()->profile_pic}}@else{{url('public/frontend/images/blank.png')}}@endif";
                                        var imgg2 = "{{url('public/frontend/images/blank.png')}}";
                                        if(item.get_user.profile_pic!=null){
                                            imgg2 = "{{url('storage/app/public/uploads/profile_pic/')}}"+item.get_user.profile_pic;
                                        }
                                        var file='';
                                        // if(item.get_user.image){
                                        //     imgg = "{{url('storage/app/public/userImage/')}}";
                                        //     imgg = imgg+'/'+item.get_user.image;
                                        // }
                                        if(item.file !== null){
                                            file = "{{url('storage/app/public/uploads/message_files/')}}";
                                            file = file+'/'+item.file;
                                            filename=item.file.substr(item.file.lastIndexOf("_")+1);
                                            if(item.user_id==my_id){
                                                if(item.message == null)
                                                    attach_file=`<a href="${file}" class="msg-link-color" target=_blank download>${filename}</a>`;
                                                else
                                                    attach_file=`<br><a href="${file}" class="msg-link-color" target=_blank download>${filename}</a>`;
                                            } else {
                                                if(item.message == null)
                                                    attach_file=`<a href="${file}" target=_blank download>${filename}</a>`;
                                                else
                                                    attach_file=`<br><a href="${file}" target=_blank download>${filename}</a>`;
                                            }
                                            // attach_file=`hi`; 
                                        }else{
                                            attach_file=``; 
                                        }
                                    
                                        var img_div1 = `<a class="avatar avatar-online"><img src="${imgg1}" alt=""><i></i></a>`;
                                        var img_div2 = `<a class="avatar avatar-online recive_img" href="javascript:;"><img src="${imgg2}" alt=""></a>`;
                                        var username_div = `<span class="username">${item.get_user.name}</span>`;
                                        if(item.user_id==my_id) {
                                            var msgg = `<div class="chat">
                                                <div class="chat-avatar">${img_div1}</div>
                                                <div class="chat-body">
                                                    <div class="chat-content">
                                                        <p class="send_msgg">${nl2br(item.message)}${attach_file}</p>
                                                    </div>
                                                    <time class="chat-time">${getTimeDiff(item.created_at)}</time>
                                                </div>
                                            </div>`;
                                        } else {
                                            var msgg = `<div class="chat chat-left">
                                                <div class="chat-avatar">${img_div2}</div>
                                                <div class="chat-body">
                                                    ${username_div}
                                                    <div class="chat-content">
                                                        <p class="recive_msg">${nl2br(item.message)}${attach_file}</p>
                                                    </div>
                                                    <time class="chat-time">${getTimeDiff(item.created_at)}</time>
                                                </div>
                                            </div>`;
                                        }
                                        $('.chat-bodys-'+message_master_id).append(msgg);
                                    });
                                    $('.chat-bodys-'+message_master_id).scrollTop(1000000);
                                } else {
                                    result.result.forEach(function(item, index){
                                        var imgg1 = "@if(@Auth::user()->profile_pic){{url('storage/app/public/uploads/profile_pic/').'/'.Auth::user()->profile_pic}}@else{{url('public/frontend/images/blank.png')}}@endif";
                                        var imgg2 = "{{url('public/frontend/images/blank.png')}}";
                                        if(item.get_user.profile_pic!=null){
                                            imgg2 = "{{url('storage/app/public/uploads/profile_pic/')}}"+item.get_user.profile_pic;
                                        }
                                        var file='';
                                        // if(item.get_user.image){
                                        //     imgg = "{{url('storage/app/public/userImage/')}}";
                                        //     imgg = imgg+'/'+item.get_user.image;
                                        // }
                                        if(item.file !== null) {
                                            file = "{{url('storage/app/public/uploads/message_files/')}}";
                                            file = file+'/'+item.file;
                                            filename=item.file.substr(item.file.lastIndexOf("_")+1);
                                            if(item.user_id==my_id) {
                                                if(item.message == null)
                                                    attach_file=`<a href="${file}" class="msg-link-color" target=_blank download>${filename}</a>`;
                                                else
                                                    attach_file=`<br><a href="${file}" class="msg-link-color" target=_blank download>${filename}</a>`;
                                            } else {
                                                if(item.message == null)
                                                    attach_file=`<a href="${file}" target=_blank download>${filename}</a>`;
                                                else
                                                    attach_file=`<br><a href="${file}" target=_blank download>${filename}</a>`;
                                            }
                                            // attach_file=`hi`; 
                                        } else {
                                            attach_file=``; 
                                        }
                                        var img_div1 = `<a class="avatar avatar-online"><img src="${imgg1}" alt=""><i></i></a>`;
                                        var img_div2 = `<a class="avatar avatar-online recive_img" href="javascript:;"><img src="${imgg2}" alt=""></a>`;
                                        var username_div = `<span class="username">${item.get_user.name}</span>`;
                                        if(item.user_id==my_id){
                                            var msgg = `<div class="media w-100 ml-auto mb-3 reciever-div">
                                                            <div class="media-body mr-3">
                                                                <div class="bg-styled rounded py-2 px-3 mb-1">
                                                                    <p class="text-small mb-0 text-white">${nl2br(item.message)}${attach_file}</p>
                                                                </div>
                                                                <div class="w-100"></div>
                                                                <p class="small">${getTimeDiff(item.created_at)}</p>
                                                            </div>
                                                        <img src="${imgg1}" alt="" width="30" class="rounded-circle">
                                            </div>`;
                                        } else {
                                            var msgg = `<div class="media w-100 mb-3 sender-div"><img src="${imgg2}" alt="" width="32" class="rounded-circle">
                                                        <div class="media-body ml-3">
                                                            <h5>${item.get_user.name}</h5>
                                                            <div class="bg-light rounded py-2 px-3 mb-1">
                                                                <p class="text-small mb-0 text-muted">${nl2br(item.message)}${attach_file}</p>
                                                            </div>
                                                            <br>
                                                            <p class="small">${getTimeDiff(item.created_at)}</p>
                                                        </div>
                                                    </div>`;
                                        }
                                        $('.chat-box-'+message_master_id).append(msgg);
                                    });
                                    // console.log($('.chat-box').height());
                                    // var scrollToHeight = "-" + $('.chat-box').height() + "px";
                                    // $('.all-conversation').scrollTop(scrollToHeight);
                                    // $('.chat-box').scrollHeight;
                                    $('.all-conversation').find('.scrollbar-content').css('top', 0);
                                    console.log('Scrolled');
                                    // $('.chat-box-'+message_master_id).scrollTop(1000000);
                                    // $('.media').last()[0].scrollIntoView();
                                }
                            } 
                            });
                        // return false;
                    } else {
                        console.log(result);
                    }
                    
                });
            } else {
                alert('Please type some message or insert file.');
            }
            // alert(id);
            return false;
        }
        function sendMsg(id){
            var replymsg = "";
            var err = 0;
            var filecnt = 0;
            if($('.chat-msg-'+id).val()!="")
                replymsg = $.trim($('.chat-msg-'+id).val());
            var files = $('.file-'+id).prop('files');
            if(Object.keys(files).length>0){
                $('.typing-text-'+id).text("uploading...");
                $('.chat-bodys-'+id).css("height", "228px");
            }
            data = new FormData();
            data.append('_token', "{{ csrf_token() }}");
            data.append('message_master_id',id);
            data.append('message',replymsg);
            data.append('socket_id',socketId);
            console.log("SEND MSG");
            console.log("Stopped typing");
            console.log(data);
            console.log('_token '+ "{{ csrf_token() }}")
            console.log('message_master_id '+id)
            console.log('message '+replymsg)
            console.log('socket_id '+socketId)
            startStopTypingAJAX('end');
            keyup = 0;
            $.each(files, function(k,file){
                var fileExt = file.name.split('.').pop();
                if(fileExt == "bat" || fileExt == "img" || fileExt == "exe") {
                    alert("This file cannot be uploaded.");
                    err = 1;
                } else {
                    data.append('file', file);
                }
            });
            $('.file-append-'+id).empty();
            if(err == 0){
                filecnt = Object.keys(files).length;
            }
            if (replymsg || filecnt>0) {
                $('.chat-msg-'+id).val("");
                $('.file-'+id).val('');
                $('.file-name-'+id).html('');
                $.ajax({
                    url: '{{ route("send.ajax") }}',
                    type: 'POST',
                    dataType: 'JSON',
                    data: data,
                    enctype: 'multipart/form-data',
                    processData: false,  
                    contentType: false
                })
                .done(result => {
                    $('.typing-text-'+id).text("");
                    $('.chat-bodys-'+id).css("height", "242px");
                    if (result.result) {
                        var curroute = "{{Route::is('chats')}}";
                        // if(data.to_id[0]!=my_id){
                        if(result.result.file !== null){
                            file = "{{url('storage/app/public/uploads/message_files/')}}";
                            file = file+'/'+result.result.file;
                            filename=result.result.file.substr(result.result.file.lastIndexOf("_")+1)
                            if(replymsg == "")
                                attach_file=`<a href="${file}" class="msg-link-color" target=_blank download>${filename}</a>`
                            else
                                attach_file=`<br><a href="${file}" class="msg-link-color" target=_blank download>${filename}</a>`
                            // attach_file=`hi`;
                        } else {
                            attach_file=``;
                        }
                        if(curroute!=1){
                            var msgg2 = `<div class="chat">
                                            <div class="chat-avatar">
                                                <a class="avatar avatar-online">
                                                <img src="{{ @Auth::user()->profile_pic ? url('storage/app/public/uploads/profile_pic/'.@Auth::user()->profile_pic) : url('public/frontend/images/blank.png') }}" alt="">
                                                <i></i>
                                                </a>
                                            </div>
                                            <div class="chat-body">
                                                <div class="chat-content">
                                                    <p class="send_msgg">${nl2br(replymsg)}${attach_file}</p>
                                                </div>
                                                <time class="chat-time">Just a second</time>
                                            </div>
                                        </div>`;
                            $('.chat-bodys-'+id).append(msgg2);
                            $('.chat-msg-'+id).val(""); 
                            $('.chat-bodys-'+id).scrollTop(1000000); 
                            // return false;
                        } else {
                            var imgg1 = "{{ @Auth::user()->profile_pic ? url('storage/app/public/uploads/profile_pic/').'/'.@Auth::user()->profile_pic : url('public/frontend/images/blank.png') }}";
                            var msgg2 = `<div class="media w-100 ml-auto mb-3 reciever-div">
                                            <div class="media-body mr-3">
                                                <div class="bg-styled rounded py-2 px-3 mb-1">
                                                    <p class="text-small mb-0 text-white">${nl2br(replymsg)}${attach_file}</p>
                                                </div>
                                                <div class="w-100"></div>
                                                <p class="small">Just a second</p>
                                            </div>
                                            <img src="${imgg1}" alt="" width="30" class="rounded-circle">
                            </div>`;
                            $('.chat-box-'+id).append(msgg2);
                            $('.chat-msg-'+id).val(""); 
                            $('.chat-box-'+id).scrollTop(1000000); 
                        }
                    } else {
                        console.log("sendID function return error.");
                        console.log(result);
                    }
                });
            } else {
                alert('Please type some message or insert file.');
            }
            // alert(id);
            return false;
        }
        //user is "finished typing," do something
        function doneTyping () {
            console.log("Stopped typing");
            startStopTypingAJAX('end');
            keyup = 0;
        }
        function startStopTypingAJAX(typing) {
            message_master_id = $('.message_master_id').val();
            $.ajax({
                url: '{{ route("typing.ajax") }}',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    message_master_id:message_master_id,
                    from: name,
                    typing: typing,
                    _token: '{{ csrf_token() }}',
                    socket_id: socketId,
                }
            });
        }

        $(document).ready(function() {
            // localStorage.removeItem('popup_arr');
            var booking_id, token, frrid, username;
            // if refresh after start the video call
            if(localStorage.getItem('startTime')){
                // start video call
                roomName = localStorage.getItem('userChatToken') ? localStorage.getItem('userChatToken'): localStorage.getItem('token');
                var reqData = {
                    roomName: roomName
                };
                $.getJSON(tokenUrl, reqData, function(data) {
                    identity = data.identity;
                    
                    var connectOptions = {
                        name: roomName,
                        logLevel: 'debug',
                        _useTwilioConnection: true
                    };

                    if (previewTracks) {
                        connectOptions.tracks = previewTracks;
                    }

                    // Join the Room with the token from the server and the
                    // LocalParticipant's Tracks.
                    Video.connect(data.token, connectOptions).then(roomJoined, function(error) {
                    });

                    // Bind button to leave Room.
                    document.getElementById('button-leave').onclick = function() {
                        activeRoom.disconnect();
                    };
                });
            } else {
                toastr.error('@lang('site.could_not_initiate_video'). @lang('site.please_try_again_later')!');
                location.href="my-dashboard";
            }

            if(localStorage.getItem('userChatToken')){
                $('#video-chat').hide();
            }
            
            channel.bind('receive-event', function(data) {
                console.log("receive event triggered");
                newmsg = data.message;
                if(data.file !== null){
                    file = "{{url('storage/app/public/uploads/message_files/')}}";
                    file = file+'/'+data.file;
                    filename=data.file.substr(data.file.lastIndexOf("_")+1);
                    if(data.message==null)
                        attach_file=`<a href="${file}"  target=_blank download>${filename}</a>`;
                    else
                        attach_file=`<br><a href="${file}"  target=_blank download>${filename}</a>`;
                    // attach_file=`hi`; 
                } else {
                    attach_file=``; 
                }
                var curroute = "{{Route::is('chats')}}";
                if(curroute!=1){
                    var rmsgg = `<div class="media w-100 mb-3 sender-div">
                        <img src="${data.image}" alt="" width="32" class="rounded-circle">
                        <div class="media-body ml-3">
                            <h5>${data.from}</h5>
                            <div class="bg-light rounded py-2 px-3 mb-1">
                                <p class="text-small mb-0 text-muted">${data.message}${attach_file}</p>
                            </div>
                            <br>
                            <p class="small">Just a second</p>
                        </div>
                    </div>`;

                    if(jQuery.inArray(my_id, ) !== -1 && data.message_master_id==$('.message_master_id').val()){
                        $('.chat-box').append(rmsgg);
                        $('.sp-viewport').scrollTop(1000000000); 
                        refershListing();
                    }
                    
                    if(jQuery.inArray(my_id, data.to_id) !== -1){
                        console.log("getting in");
                        console.log(data);
                        console.log(data.pusher_message_master_id);
                        if(!(jQuery.inArray(data.pusher_message_master_id, user_arr) !== -1)){
                            html=`<div class="main-chat-box main-chat-box-${data.pusher_message_master_id}" style="margin-right: ${user_arr.length * 300}px">
                                <div class="chat-heads">
                                    <div class="chat-title">
                                    <input type="hidden" id="type-${data.pusher_message_master_id}" >
                                        <span><b class="username-${data.pusher_message_master_id}">${data.from}</b></span>
                                    </div>
                                    <div class="chat-action-des"  data-id="${data.pusher_message_master_id}">
                                        <a href="javascript:;" class="minimise-chat-box" data-id="${data.pusher_message_master_id}"><i aria-hidden="true" class="fa fa-minus"></i></a>
                                        <a href="javascript:;" class="chat-action" data-id="${data.pusher_message_master_id}"><i aria-hidden="true" class="fa fa-times"></i></a>
                                    </div>
                                </div>
                                <div class="buildme">
                                    <p><a href="{{url('view-booked-details')}}/${data.booking_id}" target="_blank">${data.token}</a><span class="id_text pull-right">${data.booking_id}</span></p>
                                </div>
                                <div class="chat-bodys chat-bodys-${data.pusher_message_master_id}"></div>
                                <div class="file-append file-append-${data.pusher_message_master_id}"></div>
                                <div class="typing-text typing-text-${data.pusher_message_master_id}"></div>
                                <form class="frm_msg" onsubmit="return sendMsg(${data.pusher_message_master_id})" data-id="${data.pusher_message_master_id}">
                                    <div class="rm_emoji_area"><ul>${emojis}</ul></div>
                                    <div class="chat-fots">
                                        <textarea class="type-msgs chat-msg-${data.pusher_message_master_id}" data-id="${data.pusher_message_master_id}" placeholder="Type your message.."></textarea>
                                        <div class="upload_box">
                                            <input type="file" id="file" data-id="${data.pusher_message_master_id}" class="file-upload-chat file-${data.pusher_message_master_id}">
                                            <label for="file" class="btn-2"></label>
                                        </div>
                                        <a href="#" class="rm_emoji">🙂</a>
                                        <p class="file-name-foots file-name-${data.pusher_message_master_id}"></p>
                                        <a href="#" class="chat-send" data-id="${data.pusher_message_master_id}"><i class="fa fa-paper-plane"></i></a>
                                    </div>
                                </form>
                            </div>`;
                            $('.cht-popup').append(html);
                            user_arr.push(data.pusher_message_master_id);
                            // msgmaster_arr.push(data.pusher_message_master_id);
                            // localStorage['popup_arr'] = JSON.stringify(msgmaster_arr);
                            $.ajax({
                                url: '{{ route("user.message") }}',
                                type: 'POST',
                                dataType: 'JSON',
                                data: {
                                    message_master_id:data.pusher_message_master_id,
                                    _token: '{{ csrf_token() }}',
                                }
                            })
                            .done(result => {
                                // $('.chat-bodys').html('');
                                console.log(result);
                                if (result.result) {
                                    result.result.forEach(function(item, index){
                                        var imgg1 = "@if(@Auth::user()->profile_pic){{url('storage/app/public/uploads/profile_pic/').'/'.Auth::user()->profile_pic}}@else{{url('public/frontend/images/blank.png')}}@endif";
                                        var file='';
                                        // if(item.get_user.image){
                                        //     imgg = "{{url('storage/app/public/userImage/')}}";
                                        //     imgg = imgg+'/'+item.get_user.image;
                                        // }
                                        if(item.file !== null){
                                            file = "{{url('storage/app/public/uploads/message_files/')}}";
                                            file = file+'/'+item.file;
                                            filename=item.file.substr(item.file.lastIndexOf("_")+1);
                                            if(item.user_id==my_id){
                                                if(item.message == null)
                                                    attach_file=`<a href="${file}" class="msg-link-color" target=_blank download>${filename}</a>`;
                                                else
                                                    attach_file=`<br><a href="${file}" class="msg-link-color" target=_blank download>${filename}</a>`;
                                            } else {
                                                if(item.message == null)
                                                    attach_file=`<a href="${file}" target=_blank download>${filename}</a>`;
                                                else
                                                    attach_file=`<br><a href="${file}" target=_blank download>${filename}</a>`;
                                            }
                                            // attach_file=`hi`; 
                                        } else {
                                            attach_file=``; 
                                        }
                                        var img_div1 = `<a class="avatar avatar-online"><img src="${imgg1}" alt=""><i></i></a>`;
                                        var img_div2 = `<a class="avatar avatar-online recive_img" href="javascript:;"><img src="${data.image}" alt=""></a>`;
                                        var username_div = `<span class="username">${item.get_user.name}</span>`;
                                        if(item.user_id==my_id){
                                            var msgg = `<div class="chat">
                                                <div class="chat-avatar">${img_div1}</div>
                                                <div class="chat-body">
                                                    <div class="chat-content">
                                                        <p class="send_msgg">${nl2br(item.message)}${attach_file}</p>
                                                    </div>
                                                    <time class="chat-time">${getTimeDiff(item.created_at)}</time>
                                                </div>
                                            </div>`;
                                        } else {
                                            var msgg = `<div class="chat chat-left">
                                                <div class="chat-avatar">${img_div2}</div>
                                                <div class="chat-body">
                                                    ${username_div}
                                                    <div class="chat-content">
                                                        <p class="recive_msg">${nl2br(item.message)}${attach_file}</p>
                                                    </div>
                                                    <time class="chat-time">${getTimeDiff(item.created_at)}</time>
                                                </div>
                                            </div>`;
                                        }
                                        $('.chat-bodys-'+data.pusher_message_master_id).append(msgg);
                                    });
                                $('.chat-bodys-'+data.pusher_message_master_id).scrollTop(1000000); 
                                } 
                            });
                        } else {
                            var msgg = `<div class="chat chat-left">
                                        <div class="chat-avatar">
                                            <a class="avatar avatar-online recive_img" href="javascript:;">
                                                <img src="${data.image}" alt="">
                                            </a>
                                        </div>
                                        <div class="chat-body">
                                            <span class="username">${data.from}</span>
                                            <div class="chat-content">
                                                <p class="recive_msg">${nl2br(data.message)}${attach_file}</p>
                                            </div>
                                            <time class="chat-time">Just a second</time>
                                        </div>
                                    </div>`;
                            console.log($('.chat-bodys-'+data.pusher_message_master_id)[0]);
                            $('.chat-bodys-'+data.pusher_message_master_id).append(msgg);
                            $('.chat-bodys-'+data.pusher_message_master_id).scrollTop(1000000); 
                        }
                    }
                    // alert("no message found");
                    // alert(data.to_name);
                    // if((jQuery.inArray(my_id, data.to_id) !== -1)){
                    // console.log(user_arr);
                } else {
                    if(jQuery.inArray(data.booking_id, booking_id_array) !== -1){
                        // $(".chat_btn[data-bkid="+data.booking_id+"]").trigger( "click" );
                        booking_id_array.push(data.booking_id);
                        $(".chat_btn[data-bkid="+data.booking_id+"]").remove();
                        var holder_html = `<div class="chat-holders chat_btn" data-bkid="${data.booking_id}" data-msgid="${data.pusher_message_master_id}">
                            <span class="holder-image">
                                <img src="${data.image}" alt="" style="border-radius:50%;">
                                <img class="online-image" src="{{ URL::to('public/frontend/images/online.png') }}" alt="">
                            </span>
                            <h4>${data.from}</h4>
                            <h5>${data.booking_id}</h5>
                            <br>
                            <p>${data.token}</p>
                        </div>`;
                        $('.all-chat-lists').find('.scrollbar-content').prepend(holder_html);
                        var text_html = `<div class="media w-100 mb-3 sender-div"><img src="${data.image}" alt="" width="32" class="rounded-circle">
                            <div class="media-body ml-3">
                                <h5>${data.from}</h5>
                                <div class="bg-light rounded py-2 px-3 mb-1">
                                    <p class="text-small mb-0 text-muted">${nl2br(data.message)}${attach_file}</p>
                                </div>
                                <br>
                                <p class="small">Just a second</p>
                            </div>
                        </div>`;
                        $(".chat-box-"+data.pusher_message_master_id).append(text_html);
                    } else {
                        var holder_html = `<div class="chat-holders chat_btn" data-bkid="${data.booking_id}" data-msgid="${data.pusher_message_master_id}">
                            <span class="holder-image">
                                <img src="${data.image}" alt="" style="border-radius:50%;">
                                <img class="online-image" src="{{ URL::to('public/frontend/images/online.png') }}" alt="">
                            </span>
                            <h4>${data.from}</h4>
                            <h5>${data.booking_id}</h5>
                            <br>
                            <p>${data.token}</p>
                        </div>`;
                        $('.all-chat-lists').prepend(holder_html);
                    }
                }
            });

            channel.bind('start-end-typing', function(data) {
                var curroute = "{{Route::is('chats')}}";
                if(curroute!=1){
                    if(data.typing == 'start'){
                        $('.typing-text-'+data.message_master_id).text("typing...");
                        $('.chat-bodys-'+data.message_master_id).css("height", "228px");
                    } else if(data.typing == 'end') {
                        $('.typing-text-'+data.message_master_id).text("");
                        $('.chat-bodys-'+data.message_master_id).css("height", "242px");
                    }
                }
            });

            $("body").delegate(".rm_emoji", "click", function() {
                $(".rm_emoji_area").slideToggle("slow");
            });

            $("body").delegate(".emoticon", "click", function() {
                var emoticon = $(this).text();
                var textvalue = "";
                if($(this).parent().parent().hasClass('new-emoji-top')){
                    textvalue = $(this).parent().parent().parent().find('.attachedd-type').find('textarea').val();
                    $(this).parent().parent().parent().find('.attachedd-type').find('textarea').val(textvalue + emoticon);
                } else {
                    textvalue = $(this).parent().parent().parent().find('.type-msgs').val();
                    $(this).parent().parent().parent().find('.type-msgs').val(textvalue + emoticon);
                }
            });

            $("body").delegate(".chat-send", "click", function(e) {
                e.preventDefault();
                if($(this).data('type')=="P") {
                    sendMsg1($(this).data('id'));
                } else {
                    sendMsg($(this).data('id'));
                }
                $(this).val("");
            });

            $("body").delegate(".type-msgs", "keyup", function(e) {
                // Enter was pressed without shift key
                e.preventDefault();
                if(keyup == 0)
                    keyup = 1;
                $('.message_master_id').val($(this).data('id'));
                console.log($('.message_master_id').val());
                if(e.key == 'Enter' && e.ctrlKey) {
                    test=$(this).val()+"\r\n";
                    $(this).val(test);
                    e.preventDefault();
                } else if(e.key == 'Enter') {
                    // console.log($(this).data('id'));
                    e.preventDefault();
                    if($(this).data('type')=="P") {
                        sendMsg1($(this).data('id'));
                    } else {
                        sendMsg($(this).data('id'));
                    }
                    $(this).val("");
                }
                if(keyup == 1){
                    // fire typing event
                    console.log('Start typing');
                    startStopTypingAJAX('start');
                    keyup = 2;
                }
                clearTimeout(typingTimer);
                if ($(this).val()) {
                    typingTimer = setTimeout(doneTyping, doneTypingInterval);
                }
            });

            $('#video-chat').click(function(){
                var bktk = localStorage.getItem('token');
                var type = 'I';
                var message_master_id = 0;
                $.ajax({
                    url: '{{ route("video.booking.details") }}',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        booking_token:bktk,
                        _token: '{{ csrf_token() }}',
                    },
                })
                .done(result => {
                    var booking_id = result.result.booking_id;
                    var token = result.result.token;
                    var frrid = result.result.user_id;
                    var username = result.result.username;

                    $.ajax({
                        url: '{{ route("get.message.master") }}',
                        type: 'POST',
                        dataType: 'JSON',
                        data: {
                            booking_id:booking_id,
                            _token: '{{ csrf_token() }}',
                        }
                    })
                    .done(result => {
                        if (result.result) {
                            message_master_id=result.result.id;
                            if(!(jQuery.inArray(message_master_id, user_arr) != -1)){
                                var style='';
                                var html=`<div class="main-chat-box main-chat-box-${message_master_id}"  style="margin-right: ${user_arr.length * 300}px">
                                    <div class="chat-heads">
                                        <input type="hidden" id="type-${message_master_id}" value="${type}">
                                            <span><b class="username-${message_master_id}">${result.username}</b></span>
                                        <div class="chat-action-des"  data-id="${message_master_id}">
                                            <a href="javascript:;" class="minimise-chat-box" data-id="${message_master_id}"><i aria-hidden="true" class="fa fa-minus"></i></a>
                                            <a href="javascript:;" class="chat-action" data-id="${message_master_id}" style="margin-left: 5px;"><i aria-hidden="true" class="fa fa-times"></i></a>
                                        </div>
                                    </div>
                                    <div class="buildme">
                                        <p><a href="{{url('view-booking-details')}}/${booking_id}" target="_blank">${token}</a><span class="id_text pull-right">${booking_id}</span></p>
                                    </div>
                                    <div class="chat-bodys chat-bodys-${message_master_id}"></div>
                                    <div class="file-append file-append-${message_master_id}"></div>
                                    <div class="typing-text typing-text-${message_master_id}"></div>
                                    <form class="frm_msg" onsubmit="return sendMsg(${message_master_id})" data-id="${message_master_id}">
                                        <div class="rm_emoji_area"><ul>${emojis}</ul></div>
                                        <div class="chat-fots">
                                            <textarea  class="type-msgs chat-msg-${message_master_id}" data-id="${message_master_id}" placeholder="Type your message.."></textarea>
                                            <div class="upload_box">
                                                <input type="file" id="file" data-id="${message_master_id}" class="file-upload-chat file-${message_master_id}">
                                                <label for="file" class="btn-2"></label>
                                            </div>
                                            <a href="#" class="rm_emoji">🙂</a>
                                            <p class="file-name-foots file-name-${message_master_id}"></p>
                                            <a href="#" class="chat-send" data-id="${message_master_id}"><i class="fa fa-paper-plane"></i></a>
                                        </div>
                                    </form>
                                </div>`;
                                $('.cht-popup').append(html);
                                user_arr.push(message_master_id);
                            }
                            $.ajax({
                                url: '{{ route("user.message") }}',
                                type: 'POST',
                                dataType: 'JSON',
                                data: {
                                    message_master_id:message_master_id,
                                    type:type,
                                    _token: '{{ csrf_token() }}',
                                }
                            })
                            .done(result => {
                                if (result.result) {
                                    result.result.forEach(function(item, index){
                                        var imgg1 = "@if(@Auth::user()->profile_pic){{url('storage/app/public/uploads/profile_pic/').'/'.Auth::user()->profile_pic}}@else{{url('public/frontend/images/blank.png')}}@endif";
                                        var imgg2 = "{{url('public/frontend/images/blank.png')}}";
                                        if(item.get_user.profile_pic!=null){
                                            imgg2 = "{{url('storage/app/public/uploads/profile_pic/')}}"+"/"+item.get_user.profile_pic;
                                        }
                                        var file='';
                                        // if(item.get_user.image){
                                        //     imgg = "{{url('storage/app/public/userImage/')}}";
                                        //     imgg = imgg+'/'+item.get_user.image;
                                        // }
                                        if(item.file !== null){
                                            file = "{{url('storage/app/public/uploads/message_files/')}}";
                                            file = file+'/'+item.file;
                                            filename=item.file.substr(item.file.lastIndexOf("_")+1);
                                            if(item.user_id==my_id){
                                                if(item.message == null)
                                                    attach_file=`<a href="${file}" class="msg-link-color" target=_blank download>${filename}</a>`;
                                                else
                                                    attach_file=`<br><a href="${file}" class="msg-link-color" target=_blank download>${filename}</a>`;
                                            }else{
                                                if(item.message == null)
                                                    attach_file=`<a href="${file}" target=_blank download>${filename}</a>`;
                                                else
                                                    attach_file=`<br><a href="${file}" target=_blank download>${filename}</a>`;
                                            }
                                            // attach_file=`hi`; 
                                        }else{
                                            attach_file=``; 
                                        }
                                        var img_div1 = `<a class="avatar avatar-online"><img src="${imgg1}" alt=""><i></i></a>`;
                                        var img_div2 = `<a class="avatar avatar-online recive_img" href="javascript:;"><img src="${imgg2}" alt=""></a>`;
                                        var username_div = `<span class="username">${item.get_user.name}</span>`;
                                        if(item.user_id==my_id){
                                            var msgg = `<div class="chat">
                                                <div class="chat-avatar">${img_div1}</div>
                                                <div class="chat-body">
                                                    <div class="chat-content">
                                                        <p class="send_msgg">${nl2br(item.message)}${attach_file}</p>
                                                    </div>
                                                    <time class="chat-time">${getTimeDiff(item.created_at)}</time>
                                                </div>
                                            </div>`;
                                        } else {
                                            var msgg = `<div class="chat chat-left">
                                                <div class="chat-avatar">${img_div2}</div>
                                                <div class="chat-body">
                                                    ${username_div}
                                                    <div class="chat-content">
                                                        <p class="recive_msg">${nl2br(item.message)}${attach_file}</p>
                                                    </div>
                                                    <time class="chat-time">${getTimeDiff(item.created_at)}</time>
                                                </div>
                                            </div>`;
                                        }
                                        $('.chat-bodys-'+message_master_id).append(msgg);
                                    });
                                    $('.chat-bodys-'+message_master_id).scrollTop(1000000);
                                } 
                            });
                        } else {
                            if(!(jQuery.inArray(booking_id, user_arr) !== -1)){
                                html=`<div class="main-chat-box main-chat-box-${booking_id}"  style="margin-right: ${user_arr.length * 300}px">
                                    <div class="chat-heads">
                                        <div class="chat-title">
                                        <input type="hidden" id="type-${booking_id}" value="${type}">
                                            <span><b class="username-${booking_id}">${username}</b></span>
                                        </div>
                                        <input type="hidden" class="frrid${booking_id}" value="${frrid}">
                                        <div class="chat-action-des"  data-id="${booking_id}">
                                            <a href="javascript:;" class="minimise-chat-box" data-id="${booking_id}"><i aria-hidden="true" class="fa fa-minus"></i></a>
                                            <a href="javascript:;"  class="chat-action" data-id="${booking_id}"><i aria-hidden="true" class="fa fa-times"></i></a>
                                        </div>
                                    </div>
                                    <div class="buildme">
                                        <p><a href="{{url('view-booking-details')}}/${booking_id}" target="_blank">${token}</a><span class="id_text pull-right">${booking_id}</span></p>
                                    </div>
                                    <div class="chat-bodys chat-bodys-${booking_id}"></div>
                                    <div class="file-append file-append-${booking_id}"></div>
                                    <div class="typing-text typing-text-${booking_id}"></div>
                                    <form class="frm_msg" onsubmit="return sendMsg1(${booking_id})" data-id="${booking_id}">
                                        <div class="rm_emoji_area"><ul>${emojis}</ul></div>
                                        <div class="chat-fots">
                                            <textarea class="type-msgs chat-msg-${booking_id}" data-id="${booking_id}" data-type="P" placeholder="Type your message.."></textarea>
                                            <div class="upload_box">
                                                <input type="file" id="file" data-id="${booking_id}" class="file-upload-chat file-${booking_id}">
                                                <label for="file" class="btn-2"></label>
                                            </div>
                                            <a href="#" class="rm_emoji">🙂</a>
                                            <p class="file-name-foots file-name-${booking_id}"></p>
                                            <a href="#" class="chat-send" data-id="${booking_id}"><i class="fa fa-paper-plane"></i></a>
                                        </div>
                                    </form>
                                </div>`;
                                $('.cht-popup').append(html);
                                user_arr.push(Number(booking_id));
                            }
                        }
                    });
                });
            });

            $('body').on('click','.chat-action',function(){
                id=$(this).data('id');
                index=user_arr.indexOf(id);
                if(index>-1){
                    if(index!=(user_arr.length-1)){
                        for (let i = index+1; i < user_arr.length; i++) {
                            var elem = user_arr[i];
                            $('.main-chat-box-'+elem).css("margin-right", ((i-1)*300)+"px");
                            
                        }
                    }
                    user_arr.splice(index, 1);
                }
                $('.main-chat-box-'+id).remove();

                // update local storage variable
                indexx=msgmaster_arr.indexOf(id);
                if(indexx>-1){
                    if(indexx!=(msgmaster_arr.length-1)){
                        for (let i = indexx+1; i < msgmaster_arr.length; i++) {
                            var elem = msgmaster_arr[i];
                            $('.main-chat-box-'+elem).css("margin-right", ((i-1)*300)+"px");
                            
                        }
                    }
                    msgmaster_arr.splice(indexx, 1);
                }
                localStorage['popup_arr'] = JSON.stringify(msgmaster_arr);
            });

            $('body').on('click','.minimise-chat-box',function(){
                id=$(this).data('id');
                if($('.main-chat-box-'+id).css("margin-bottom")==-322+"px")
                $('.main-chat-box-'+id).css("margin-bottom",0+"px");
                else
                $('.main-chat-box-'+id).css("margin-bottom",-322+"px");
            });
            
            $('body').on('change','.file-upload-chat',function(){
                console.log($(this)[0]);
                var files = $(this).prop('files');
                filecnt = Object.keys(files).length;
                if(filecnt>0){
                    console.log('file appending');
                    var id = $(this).data('id');
                    var html = `<div class="file_display">${$(this)[0].files[0].name}<i aria-hidden="true" class="fa fa-times rem_file"></i></div>`;
                    $('.file-append-'+id).append(html);
                }
            });

            $('body').on('click','.rem_file',function(){
                console.log("Removed file");
                var id = $(this).parent().parent().parent().find('form').data('id');
                $('.file-'+id).val('');
                $('.file-append-'+id).empty();
                var files = $('.file-'+id).prop('files');
                filecnt = Object.keys(files).length;
                console.log(filecnt>0);
            });

        });
       

        // This method is used to start timer
        function start_timer() {
            // update video_status
            updateVideoInitiated(localStorage.getItem('userChatToken') ? localStorage.getItem('userChatToken'): localStorage.getItem('token'));
            if(window.localStorage.getItem('startTime')<window.localStorage.getItem('maxTime')){
                if(window.localStorage.getItem('startTime')){
                    timer = parseInt(window.localStorage.getItem('startTime'));
                    window.localStorage.setItem('startTime', timer);
                }
                else{
                    timer = 0;
                    window.localStorage.setItem('startTime', timer);
                }

                myTimeInterval = setInterval(function () {
                    if(parseInt(window.localStorage.getItem('startTime'))<parseInt(window.localStorage.getItem('maxTime'))){
                        minutes = parseInt(timer / 60, 10);
                        seconds = parseInt(timer % 60, 10);
                        numberminutes = parseInt(timer / (60 * increment), 10);
                        minutes = minutes < 10 ? "0" + minutes : minutes;
                        seconds = seconds < 10 ? "0" + seconds : seconds;
                        timer++;
                        
                        window.localStorage.setItem('startTime', timer);
                        if(numberminutes>0){
                            updateCallTime(localStorage.getItem('userChatToken') ? localStorage.getItem('userChatToken'):
                            localStorage.getItem('token'),window.localStorage.getItem('startTime'),userType);
                            increment++;
                        }
                        if(parseInt(window.localStorage.getItem('maxTime'))-parseInt(window.localStorage.getItem('startTime')) == 600){
                            toastr.info('@lang('site.video_call_message')');
                        }
                        if(parseInt(window.localStorage.getItem('startTime')) == parseInt(window.localStorage.getItem('maxTime'))){
                            updateVideoStatus(localStorage.getItem('userChatToken') ? localStorage.getItem('userChatToken'): localStorage.getItem('token'));
                            
                        }
                    }
                    else{
                        // call videoCloseFunCustomer for stop timer and remove localstorage
                        if(userType == 'C') {
                            videoCloseFunCustomer();
                            swal('@lang('site.video_conference_is_over').',{icon:"info"});
                        }

                        // call videoCloseFunProfessional for stop timer and remove localstorage 
                        if(userType == 'P') {
                            videoCloseFunProfessional();
                            swal('@lang('site.video_conference_is_over').',{icon:"info"});
                        }
                        window.localStorage.removeItem('videoStart');
                        window.localStorage.removeItem('userChatToken');
                        window.localStorage.removeItem('startTime');
                        window.localStorage.removeItem('maxTime');
                        window.localStorage.removeItem('userChatId');
                        window.localStorage.removeItem('userId');
                        window.localStorage.removeItem('token');
                        clearInterval(myTimeInterval);
                    }
                }, 1000);
            } else {
                // call videoCloseFunCustomer for stop timer and remove localstorage
                if(userType == 'C') {
                    videoCloseFunCustomer();
                    swal('@lang('site.video_conference_is_over').',{icon:"info"});
                }

                // call videoCloseFunProfessional for stop timer and remove localstorage 
                if(userType == 'P') {
                    videoCloseFunProfessional();
                    swal('@lang('client_site.video_conference_is_over').',{icon:"info"});
                }
                window.localStorage.removeItem('videoStart');
                window.localStorage.removeItem('userChatToken');
                window.localStorage.removeItem('startTime');
                window.localStorage.removeItem('maxTime');
                window.localStorage.removeItem('userChatId');
                window.localStorage.removeItem('userId');
                window.localStorage.removeItem('token');
                clearInterval(myTimeInterval);
            }
        }
        
        function videoCloseFunCustomer(){
            clearInterval(myTimeInterval);
            window.localStorage.removeItem('videoStart');
            window.localStorage.removeItem('userChatToken');
            window.localStorage.removeItem('startTime');
            window.localStorage.removeItem('maxTime');
            window.localStorage.removeItem('userChatId');
            window.localStorage.removeItem('userId');
            window.localStorage.removeItem('token');
            location.href="{{ url('my-dashboard') }}";
        }

        function videoCloseFunProfessional() {
            clearInterval(myTimeInterval);
            window.localStorage.removeItem('videoStart');
            window.localStorage.removeItem('userChatToken');
            window.localStorage.removeItem('startTime');
            window.localStorage.removeItem('maxTime');
            window.localStorage.removeItem('userChatId');
            window.localStorage.removeItem('userId');
            window.localStorage.removeItem('token');
            location.href="{{ url('dashboard-bookings/UC') }}";
        }

        // this method is used to update video status to initiate
        function updateVideoInitiated(token) {
            if(token!=null || token!=""){
                var reqData = {
                    'jsonrpc' : '2.0',
                    '_token' : '{{csrf_token()}}',
                    'params' : {
                        'token' : token
                    }
                };
                $.ajax({
                    url: "{{ route('update.video.initiated') }}",
                    method: 'post',
                    dataType: 'json',
                    data: reqData,
                    success: function(response){
                        if(response.status==1){
                            return 1;
                        }
                    }, error: function(error) {
                        toastr.info('@lang('client_site.Try_again_after_sometime')');
                    }
                });
            }
        }
        // This method is used to update video call status
        function updateVideoStatus(token){
            if(token!=null || token!=""){
                var reqData = {
                    'jsonrpc' : '2.0',
                    '_token' : '{{csrf_token()}}',
                    'params' : {
                        'token' : token
                    }
                };
                $.ajax({
                    url: "{{ route('update.video.status') }}",
                    method: 'post',
                    dataType: 'json',
                    data: reqData,
                    success: function(response){
                        if(response.status==1){
                            return 1;
                        }
                    }, error: function(error) {
                        toastr.info('@lang('client_site.Try_again_after_sometime')');
                    }
                });
            }
        }
        // This method is used to update video call status
        function updateCallTime(token,callTime,userType){
            if(token!=null || token!=""){
                var reqData = {
                    'jsonrpc' : '2.0',
                    '_token' : '{{csrf_token()}}',
                    'params' : {
                        'token' : token,
                        'completed_call':callTime,
                        'user_type':userType
                    }
                };
                $.ajax({
                    url: "{{ route('update.call.time') }}",
                    method: 'post',
                    dataType: 'json',
                    data: reqData,
                    success: function(response){
                        if(response.status==1){
                            return 1;
                        }
                    }, error: function(error) {
                        toastr.info('@lang('client_site.Try_again_after_sometime')');
                    }
                });
            }
        }

        $(document).ready(function() {
            $('#video-full').click(openFullscreen);
            $('#video-small').click(closeFullscreen);
        })



        var elem = document.getElementById("fulvido");
        function openFullscreen() {
            $('#video-full').hide();
            $('#video-small').show();
            $('#fulvido').addClass('escp');
            if (elem.requestFullscreen) {
                elem.requestFullscreen();
            } else if (elem.mozRequestFullScreen) { /* Firefox */
                elem.mozRequestFullScreen();
            } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
                elem.webkitRequestFullscreen();
            } else if (elem.msRequestFullscreen) { /* IE/Edge */
                elem.msRequestFullscreen();
            }
        }

        function closeFullscreen() {
            $('#video-full').show();
            $('#video-small').hide();
            $('#fulvido').removeClass('escp');
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            }
        }

        document.addEventListener('fullscreenchange', exitHandler);
        document.addEventListener('webkitfullscreenchange', exitHandler);
        document.addEventListener('mozfullscreenchange', exitHandler);
        document.addEventListener('MSFullscreenChange', exitHandler);

        function exitHandler() {
            if (!document.fullscreenElement && !document.webkitIsFullScreen && !document.mozFullScreen && !document.msFullscreenElement) {
                $('#video-full').show();
                $('#video-small').hide();
                $('#fulvido').removeClass('escp');
            }
        } 
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/js/all.js"></script>
@endsection