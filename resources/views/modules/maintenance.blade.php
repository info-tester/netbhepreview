<!DOCTYPE html>
<html lang="en">
<head>
    <style>
        body{
            margin:0;
            padding:0;
            height:100vh;
        }
        .maintenance{
            width: 100%;
            height: 100%;
            display: table;
        }
        .maintenance .content{
            display: table-cell;
            vertical-align: middle;
            text-align: center;
        }
        .maintenance img{
            height: 100px;
            width:auto;
        }
        .maintenance p{
            font-family:Helvetica;
        }

    </style>
</head>
<body>
    <div class="maintenance">
        <div class="content">
            <div class="text-center">
                <img src="{{ URL::to('public/frontend/images/logo.png') }}" alt="">
                <p>Maintenance work in progress</p>
            </div>
        </div>
    </div>
</body>
</html>