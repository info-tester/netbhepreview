@extends('layouts.app')
@section('title')
    {{-- @lang('site.my_blogs') --}}
    Coaching tools
@endsection
@section('style')
@include('includes.style')

@endsection
@section('scripts')
@include('includes.scripts')
<script src="{{URL::to('public/js/chosen.jquery.min.js')}}"></script>
@endsection
@section('header')
@include('includes.professional_header')
<link rel="stylesheet" href="{{URL::to('public/css/chosen.css')}}">
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.coaching_tools')</h2>
        

		{{-- <div class="like-tab">
        	<ul>
            	<li><a class="active" href="#">Form tools</a></li>
                <li><a href="#">Tab tow</a></li>
                <li><a href="#">Tab three</a></li>
                <li><a href="#">Tab four</a></li>
            </ul>
        </div> --}}

        @include('includes.professional_tab_menu')

        <div class="bokcntnt-bdy no-margin-top">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>

            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">

                <div class="" style="float: right;">
                    <a class="btn btn-success" href="{{route('professional-form.create')}}">+ @lang('site.add_new_form')</a>
                </div>


                <?php /*
                <form name="myForm" id="myForm" method="post" action="{{route('my.blog.search')}}">
                    @csrf
                    <div class="from-field">
                        
                        <div class="frmfld">
                            <div class="form-group">
                                <label class="search_label">@lang('site.select') @lang('site.category')</label>
                                <select class="dashboard-type dashboard_select" name="cat_id" id="cat_id">
                                    <option value="">@lang('site.select') @lang('site.category')</option>
                                    @foreach(@$category as $cat)
                                        <option value="{{@$cat->id}}" @if(@$cat->id==@$key['cat_id']) selected @endif>{{@$cat->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="frmfld">
                            <div class="form-group">
                                <label class="search_label">@lang('site.select') @lang('site.status')</label>
                                <select class="dashboard-type dashboard_select" name="status" id="status">
                                    <option value="">@lang('site.select') @lang('site.status')</option>
                                    <option value="A" @if(@$key['status']=='A') selected @endif>@lang('site.active')</option>
                                    <option value="I" @if(@$key['status']=='I') selected @endif>@lang('site.inactive') </option>
                                </select>
                            </div>
                        </div>
                        <div class="frmfld">
                            <button class="banner_subb fmSbm">@lang('site.filter')</button>
                            
                        </div>

                        <div class="frmfld">
                            <a class="btn btn-success" href="{{route('add.blog.post')}}">+@lang('site.add_new_post')</a>
                            
                        </div>
                    </div>
                </form>

                */?>
                <div class="buyer_table">
                    <div class="table-responsive">
                        @if(sizeof(@$form_tools)>0)
                        <div class="table">
                            <div class="one_row1 hidden-sm-down only_shawo">
                                <div class="cell1 tab_head_sheet">@lang('site.title')</div>
                                {{-- <div class="cell1 tab_head_sheet">Description</div> --}}
                                <div class="cell1 tab_head_sheet">@lang('site.category')</div>
                                <div class="cell1 tab_head_sheet">{{-- Added By --}}@lang('site.added_by')</div>
                                {{-- <div class="cell1 tab_head_sheet">posted on</div> --}}
                                <div class="cell1 tab_head_sheet">@lang('site.status')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.action'){{-- @lang('site.action') --}}</div>
                            </div>
                            <!--row 1-->                                
                                @foreach(@$form_tools as $form_data)                        
                                    <div class="one_row1 small_screen31">
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.no')</span>
                                            <p class="add_ttrr">{{$form_data->form_title}}</p>
                                        </div>

                                        {{-- <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">description</span>
                                            <p class="add_ttrr">
                                                {{ substr(strip_tags(@$form_data->form_dsc),0,20) }}...
                                            </p>
                                        </div> --}}
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">category</span>
                                            <p class="add_ttrr">{{ @$form_data->category->form_category }}</p>
                                        </div>
                                        {{-- @dump(@$blg->desc) --}}
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">added by</span>
                                            <p class="add_ttrr">{{ $form_data['added_by']=='A'?'Admin': ( @$form_data->user->nick_name ? @$form_data->user->nick_name : @$form_data->user->name ) }}</p>
                                        </div>
                                        
                                        
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.status')</span>
                                            <p class="add_ttrr">
                                                @if($form_data->show_status=="I")
                                                    {{-- INACTIVE --}}Inativa
                                                @else  
                                                    @if($form_data['status'] == "ACTIVE")
                                                        ATIVO
                                                    @elseif($form_data['status'] == "INCOMPLETE")
                                                        INCOMPLETA
                                                    @endif
                                                @endif    
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">

                                            @if($form_data->show_status=="A")
                                                @if($form_data->added_by=="P" && $form_data->added_by_id == Auth::id())
                                                    <a href="{{route('professional-form.edit',[$form_data->id])}}" class="acpt editbtn" style="margin: 2px;" title="Edit">@lang('site.edit')</a>                                                

                                                    {{-- <a href="javascript:void(0)" onclick="deleteCategory('{{route('professional-form.destroy',[$detail->id])}}')" title="Delete"> <i class="fa fa-trash-o delet" aria-hidden="true"></i></a> --}}


                                                    <a href="javascript:void(0)" onclick="deleteCategory('{{route('professional-form.destroy',[$form_data->id])}}')" title="Delete" class="rjct delbtn"> @lang('site.delete')</a>
                                                    
                                                    @if($form_data->show_status=="A")
                                                        <a href="{{ route('professional.formtools.status', ['id'=>$form_data->id]) }}" onclick="return confirm(`@lang('client_site.inactivate_form')`)" class="acpt" title="@lang('site.inactive')">@lang('site.inactive')</a>
                                                    @else
                                                        <a href="{{ route('professional.formtools.status', ['id'=>$form_data->id]) }}" title="@lang('site.active')" onclick="return confirm(`@lang('client_site.activate_form')`)" class="acpt rjct">@lang('site.active')</a>
                                                    @endif

                                                    {{-- <a href="{{route('remove.blog.post', ['id'=>@$blg->id])}}" class="rjct delbtn" style="margin: 2px;">@lang('site.delete')</a> --}}
                                                    {{--   <a href="{{route('view.blog.post', ['id'=>@$blg->id])}}" class="acpt">Assign</a> --}}                                                
                                                
                                                    {{--   <a href="{{route('view.blog.post', ['id'=>@$blg->id])}}" class="acpt">Assign</a> --}}          
                                                @endif
                                                {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-title="{{$form_data->form_title}}" data-target="#exampleModalCenter">
                                                  Assign
                                                </button> --}}
                                                <a href="{{route('professional-form.show', [$form_data->id])}}" class="acpt viewbtn" style="margin: 2px;" title="View Question">@lang('site.view')</a>
                                                <a href="javascript:;" class="show_assign acpt assginbtn" data-title="{{$form_data->form_title}}" data-id="{{ $form_data->id }}" style="margin: 2px;">@lang('site.assign')</a>
                                                <a href="{{route('professional.view.form', ['form_id'=>@$form_data->id])}}" class="acpt view_asgi_btn" style="margin: 2px;">@lang('site.view_assign')</a>
                                                <a href="{{route('prof.view.alluserform.feedback', [$form_data->id])}}" class="acpt assginbtn"style="margin: 2px;">@lang('site.view_feedback')</a>
                                                <a href="javascript:;" class="view_tools_help acpt assginbtn" data-title="{{$form_data->form_title}}" data-helpdesc="{{ $form_data->help_tools_view }}" style="margin: 2px;">@lang('site.view_tool_help')</a>
                                            @else
                                                <a href="{{ route('professional.formtools.status', ['id'=>$form_data->id]) }}" title="@lang('site.active')" onclick="return confirm(`@lang('client_site.activate_form')`)" class="acpt">@lang('site.active')</a>
                                            @endif    

                                        </div>
                                    </div>                                    
                                @endforeach
                            
                        </div>
                        @else
                            <div class="one_row small_screen31">
                                <center><span><h3 class="error">{{__('site.oops!_not_found')}}</h3></span></center>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

<!--assigne Modal -->
<div class="modal fade" id="assignModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Escolher usuário</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="myform" method="post" action="{{ route('professional-user-assigne') }}">
                {{ csrf_field() }}
                <div class="modal-body">
                    <h5>Titulo: <span class="tool-title"></span></h5>

                        <label for="exampleInputEmail1">Selecionar Usuário</label>
                        <select name="user_id[]" class="required form-control newdrop required chosen-select" data-placeholder="Selecione uma Opção" multiple="true" required="">
                            <option value="">Selecionar</option>
                            @foreach($all_paid_users as $row)
                                <option value="{{$row->user_id}}">{{@$row->userDetails->nick_name ? @$row->userDetails->nick_name : @$row->userDetails->name}}</option>
                            @endforeach
                        </select>
                    <input type="hidden" name="form_id" class="form_id"> {{-- this calass form id get form id --}}
                </div>
            
                <div class="modal-footer">                
                    <button type="submit" value="submit" class="btn btn-primary">@lang('site.assign')</button>
                </div>

            </form>
        </div>
    </div>
</div>

<!--View tool help Modal start -->
<div class="modal fade" id="view_tool_help_Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Titulo : <span class="tool-title"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
            <div class="modal-body">
                <span class="tool-help-desc-view"></span>
            </div>           
        </div>
    </div>
</div>
<!--View tool help Modal end -->



</section>




@endsection
@section('footer')
@include('includes.footer')

<script>

$(document).ready(function(){
    $('.show_assign').click(function(){
        var title = $(this).data('title');
        var id = $(this).data('id');
        $('.tool-title').html(title);
        $('.form_id').val(id);
        $('#assignModal').modal('show');
    });

    $('.view_tools_help').click(function(){
        var title = $(this).data('title'); 
        var help_desc = $(this).data('helpdesc'); 
        $('.tool-title').html(title);
        $('.tool-help-desc-view').html(help_desc);       
        $('#view_tool_help_Modal').modal('show');
    });
    
});

$(".chosen-select").chosen();

$(document).ready(function(){
        $("#myform").validate();
    });
</script>

<form method="post" id="destroy">
    {{ csrf_field() }}
    {{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
    var confirm = window.confirm('Do you want to delete this question?');
    if(confirm){
        $("#destroy").attr('action',val);
        $("#destroy").submit();
    }
 }
</script>
<style>
    .error{
        color: red !important;
    }
</style>

@endsection