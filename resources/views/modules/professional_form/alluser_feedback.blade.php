@extends('layouts.app')
@section('title')
  @lang('site.my_availability')
@endsection
@section('style')
@include('includes.style')
<style>
.time_slot_span{
    background: #1781d2;
    padding: 2px 7px;
    border: solid 1px #475dfb;
    border-radius: 12px;
    color: #fff;
    margin: 0 15px 10px 0;
    float: left;
    cursor: pointer;

}

}
.ui-datepicker{
	z-index:2 !important;
	}
.form-group {
	 width: 100% !important; 
}
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        {{-- <h2>@lang('client_site.availability')</h2> --}}
        <h2>@lang('site.assigned_users')</h2>

        @include('includes.professional_tab_menu')

        <div class="bokcntnt-bdy no-margin-top">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            
            <div class="dshbrd-rghtcntn">

                <div class="mainDiv">
                    @if(sizeof(@$allfeedback)>0)
                        <div class="buyer_table">
                            <div class="table-responsive">
                                <div class="table">
                                    <div class="one_row1 hidden-sm-down only_shawo">                                       
                                        
                                        <div class="cell1 tab_head_sheet">@lang('site.user_name')</div>
                                        <div class="cell1 tab_head_sheet">Feedback</div>
                                        <div class="cell1 tab_head_sheet">Date</div>
                                        
                                        <div class="cell1 tab_head_sheet">@lang('site.status')</div>
                                        <div class="cell1 tab_head_sheet">@lang('site.action')</div>
                                    </div>
                                    <!--row 1-->                                    
                                    @foreach(@$allfeedback as $ql)                     
                                        <div class="one_row1 small_screen31">

                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">user_id</span>
                                                <p class="add_ttrr">{{ @$ql->getUserData->nick_name ? @$ql->getUserData->nick_name : @$ql->getUserData->name }}</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">user_feedback</span>
                                                <p class="add_ttrr">{{ @$ql->user_feedback }}</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">user_feedback_date</span>
                                                <p class="add_ttrr">{{ date('m/d/Y (D)', strtotime(@$ql->user_feedback_date)) }}</p>
                                            </div>

                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">status</span>
                                                <p class="add_ttrr">{{ @$ql->status }}</p>
                                            </div>
                                            
                                            
                                            <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                                <span class="W55_1">@lang('client_site.action')</span>
                                                
                                                
                                                @if($ql->status == "COMPLETED")                                            
                                                    {{-- <a href="{{ route('user.view.form.submit.details',['id'=>@$ql->id]) }}" ><i class="fa fa-eye"></i> View</a> --}}
                                                    {{-- <a href="{{ route('prof.importing.assigne.view.details',['id'=>@$ql->id]) }}" ><i class="fa fa-eye"></i> View</a> --}}
                                                    <a href="javascript:;" class="view_feedback_btn" data-title="{{$ql->getUserData->name}}" data-userfeedback="{{ @$ql->user_feedback }}" data-proffeedback="{{ @$ql->professional_feedback }}" style="margin: 2px;">View feedback</a>
                                                @else
                                                    <a href="javascript:;" class="show_assign" data-title="{{ @$ql->getUserData->name }}" data-feedback ="{{ @$ql->user_feedback }}" data-id="{{ $ql->id }}" style="margin: 2px;">Feedback</a>
                                                @endif    
                                                {{-- <a href="#" class="rjct" style="background: #098005;"><i class="fa fa-pencil"></i> @lang('client_site.edit') </a> --}}
                                            </div>
                                        </div>
                                    @endforeach
                                    {{-- {{ $avl->->onEachSide(5)->links() }} --}}
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>




<!--insert imp[orted tools feedback Modal start -->
<div class="modal fade" id="assignModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Feedback to <span class="tool-type"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="myform" method="post" action="{{ route('prof.formfeedback.post.touser') }}">
                {{ csrf_field() }}
                <div class="modal-body">
                    {{-- <h5>Tool Title: <span class="tool-type"></span></h5> --}}
                    <h5>User feedback: <span class="userfeedback"></span></h5>

                        <label for="exampleInputEmail1" class="personal-label">Feedback</label>
                        <textarea name="professional_feedback" class="required personal-type99" rows="7"></textarea>
                        
                    <input type="hidden" name="imported_tool_feedback_id" class="feedback_id"> {{-- this calass form id get form id --}}
                </div>
            
                <div class="modal-footer">                
                    <button type="submit" value="submit" class="btn btn-primary">Save</button>
                </div>

            </form>
        </div>
    </div>
</div>
<!--View tool help Modal end -->

<!--View feedback both (user and professional) Modal start -->
<div class="modal fade" id="view_both_feedback_Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Feedback <span class="tool-title"></span></h5>
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
            <div class="modal-body">
                <p>User : <span class="user-feedback"></span> </p>
                <p>Professional : <span class="prof-feedback"></span></p>
            </div>           
        </div>
    </div>
</div>
<!--View feedback Modal end -->



</section>
@endsection
@section('footer')
@include('includes.footer')


<script>

$(document).ready(function(){

    $('.show_assign').click(function(){
        var title = $(this).data('title');
        var id = $(this).data('id');
        var user_feedback = $(this).data('feedback');
        $('.tool-type').html(title);
        $('.feedback_id').val(id);
        $('.userfeedback').html(user_feedback);
        $('#assignModal').modal('show');
    });


    $('.view_feedback_btn').click(function(){
        var title = $(this).data('title'); 
        var user_feed = $(this).data('userfeedback'); 
        var prof_feed = $(this).data('proffeedback');

        $('.tool-title').html(title);
        $('.user-feedback').html(user_feed);       
        $('.prof-feedback').html(prof_feed);       
        $('#view_both_feedback_Modal').modal('show');
    });

});


$(document).ready(function(){
    $("#myform").validate();
});
</script>


@endsection