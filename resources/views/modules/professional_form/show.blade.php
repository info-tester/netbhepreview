@extends('layouts.app')
@section('title')
    @lang('site.post') @lang('site.details')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.view_form')</h2>

        @include('includes.professional_tab_menu')

        <div class="bokcntnt-bdy no-margin-top">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="blog-right details-right">
                    <div class="blog-box">
                        
                        <div class="blog-dtls">
                            
                            <div class="from-field">
                                <div class="weicome manage-designs">

                                    <h3><strong style="width: 120px;">@lang('site.form_name') <b>:</b> </strong> {{ $details->form_title }}</h3>
                                    <p><strong style="width: 120px;">@lang('client_site.category') <b>:</b></strong> {{ $details->category->form_category }}</p>
                                    <p><strong style="width: 120px;">@lang('client_site.description') <b>:</b></strong> <?= $details->form_dsc ?></p>
                                    <p><strong style="width: 120px;">@lang('site.tool_type') <b>:</b></strong> @if(@$details->tool_type == 'Form') @lang('site.tool_form') @else {{ @$details->tool_type }} @endif</p>
                                    <p><strong style="width: 120px;">@lang('site.Added_by') <b>:</b></strong> {{ $details['added_by']=='A'?'Admin':(@$details->user->nick_name ? @$details->user->nick_name : @$details->user->name) }}</p>
                                    {{-- <h3>{{ @$form_data->form_title }}</h3>
                                    <strong>{{ @$form_data->category->form_category }}</strong>
                                    <p>{{ @$form_data->form_title }}</p> --}}
                                </div>                       
                            </div>

                            {{-- <h4><a href="javascript::void(0);">{{ @$details->form_title }}</a></h4>
                            <ul class="blog-post">                               
                                <li>{{ @$details->form_dsc }}</li>
                                <li>{{ @$details->tool_type }}</li>
                                <li>{{ $details['added_by']=='A'?'Admin': ( @$details->user->nick_name ? @$details->user->nick_name : @$details->user->name ) }}</li>
                                <li>{{ @$details->category->form_category }}</li>
                            </ul> --}}
                            
                            @if(@$details['added_by']=='P')
                                <div class="frmfld" style="float: right;width: auto;">
                                    <a class="btn btn-success" href="{{route('form.question.create',[$details->id])}}">+ @lang('site.add_new_question')</a>
                                </div>
                            @endif    
                            <div class="buyer_table">
                                <div class="table-responsive">
                                    @if(@$questions->isNotEmpty())
                                    <div class="table">
                                        <div class="one_row1 hidden-sm-down only_shawo">
                                            <div class="cell1 tab_head_sheet">#</div>
                                            <div class="cell1 tab_head_sheet">@lang('site.question_type')</div>
                                            <div class="cell1 tab_head_sheet">@lang('site.question_name')</div>
                                            <div class="cell1 tab_head_sheet">@lang('site.action')</div>
                                        </div>
                                        <!--row 1-->                                
                                            @foreach(@$questions as $key=>$data)                        
                                                <div class="one_row1 small_screen31">
                                                    <div class="cell1 tab_head_sheet_1">
                                                        <span class="W55_1">@lang('site.no')</span>
                                                        <p class="add_ttrr">{{$key+1}}</p>
                                                    </div>

                                                    <div class="cell1 tab_head_sheet_1">
                                                        <span class="W55_1">Question Type</span>
                                                        <p class="add_ttrr">
                                                            @if($data['field_type']=='TEXTBOX')
                                                            @lang('site.textbox')
                                                            @elseif($data['field_type']=='TEXTAREA')
                                                            {{__('site.textarea')}}
                                                            @elseif($data['field_type']=='SINGLESELECT')
                                                            {{__('site.single_select')}}
                                                            @elseif($data['field_type']=='MULTISELECT')
                                                            {{__('site.multi_select')}}
                                                            @endif
                                                        </p>
                                                    </div>
                                                    <div class="cell1 tab_head_sheet_1">
                                                        <span class="W55_1">Question Name</span>
                                                        <p class="add_ttrr">{{ @$data['field_name'] }}</p>
                                                    </div>
                                                    
                                                   
                                                    <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                                        @if($details->added_by=="P" && $details->added_by_id == Auth::id())
                                                            <a href="{{route('form.question.edit',['form_id'=>$details->id,'id'=>$data['id']])}}" class="rjct">@lang('site.edit')</a>
                                                            {{-- <a href="{{route('professional-form.show', ['id'=>@$blg->id])}}" class="acpt">@lang('site.view')</a> --}}
                                                            <a onclick="return confirm(`@lang('client_site.delete_question')`)" href="{{route('form.question.delete',['form_id'=>$details->id,'id'=>$data['id']])}}" class="rjct">@lang('site.delete')</a>
                                                        @else
                                                        N.A
                                                        @endif
                                                    </div>
                                                </div>                                    
                                            @endforeach
                                        
                                    </div>
                                    @else
                                        <div class="one_row small_screen31">
                                            <center><span><h3 class="error">{{__('site.oops!_not_found')}}</h3></span></center>
                                        </div>
                                    @endif
                                </div>
                            </div>


                            
                        </div>
                    </div>
                    
                </div>
            </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
    <script>
        $(document).ready(function(){
            $('#myForm').validate();     
        });
    </script>

@endsection