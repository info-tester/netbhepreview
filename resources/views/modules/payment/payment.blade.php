@extends('layouts.app')
@section('title')
@lang('client_site.my_payments')
@endsection
@section('style')
@include('includes.style')
<style>
    .ui-timepicker-standard {
        position: absolute;
        z-index: 9999 !important;
    }

    .frmfld {
        width: 42%;
        float: left;
        margin-right: 1.5%;
    }

    .frmfld .form-group {
        position: relative;
        width: 100%;
    }

    .btwdth {
        width: auto;
    }

    @media(max-width:1199px) {
        .frmfld {
            width: 40%;
        }
    }

    @media(max-width:991px) {
        .frmfld {
            width: 48%;
        }

        .banner_subb {
            margin-top: 10px;
        }
    }

    @media(max-width:400px) {
        .frmfld {
            width: 98%;
        }
    }
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')
<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('client_site.my_payments')</h2>
        <div class="bokcntnt-bdy">
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <form id="myForm" name="myForm" action="{{ route('my.payments') }}" method="get">
                    <div class="from-field">
                        <div class="frmfld">
                            <div class="form-group">
                                <label class="search_label">@lang('client_site.order_id')</label>
                                <input type="text" name="order_id" class="dashboard-type"
                                    placeholder="@lang('client_site.order_id')" value="{{ @$key['order_id'] }}">
                            </div>
                        </div>
                        <div class="frmfld">
                            <div class="form-group">
                                <label class="search_label">@lang('client_site.payment_status')</label>
                                <select class="dashboard-type dashboard_select" name="balance_status">
                                    <option value="">@lang('client_site.payment_status')</option>
                                    <option value="W" @if(@$key['balance_status']=="W" ) selected @endif>
                                        @lang('client_site.already_setteled')</option>
                                    <option value="R" @if(@$key['balance_status']=="R" ) selected @endif>
                                        @lang('client_site.under_settelement')</option>
                                </select>
                            </div>
                        </div>

                        <div class="btwdth">
                            <button class="banner_subb srch">@lang('client_site.filter')</button>
                        </div>
                    </div>
                </form>
                @if(count(@$payment)>0)
                <div class="buyer_table">
                    <div class="table-responsive">
                        <div class="table">
                            <div class="one_row1 hidden-sm-down only_shawo">
                                <div class="cell1 tab_head_sheet">@lang('client_site.order_id')</div>
                                <div class="cell1 tab_head_sheet">@lang('client_site.user_name')</div>
                                <div class="cell1 tab_head_sheet">@lang('client_site.date')</div>
                                <div class="cell1 tab_head_sheet">@lang('client_site.amount')</div>
                                <div class="cell1 tab_head_sheet">@lang('client_site.commission')</div>
                                <div class="cell1 tab_head_sheet">
                                    @lang('client_site.net')<br>@lang('client_site.earning')</div>
                                <div class="cell1 tab_head_sheet">
                                    @lang('client_site.payment')<br>@lang('client_site.status')</div>
                            </div>
                            @foreach(@$payment as $row)
                            <div class="one_row1 small_screen31">
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">@lang('client_site.order_id')</span>
                                    <p class="add_ttrr">{{ @$row->paymentDetails->pg_order_id }}</p>
                                </div>
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">@lang('client_site.user')</span>
                                    <p class="add_ttrr">
                                        {{ @$row->paymentDetails->userDetails->nick_name ? @$row->paymentDetails->userDetails->nick_name : @$row->paymentDetails->userDetails->name }}
                                    </p>
                                </div>
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">@lang('client_site.date')</span>
                                    <p class="add_ttrr">{{ date('Y-m-d' ,strtotime(@$row->created_at)) }}</p>
                                </div>
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">@lang('client_site.amount')</span>
                                    <p class="add_ttrr">${{ @$row->admin_commission+@$row->professional_amount+@$row->affiliate_commission }}</p>
                                </div>
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">@lang('client_site.commission')</span>
                                    <p class="add_ttrr">${{ @$row->admin_commission }}</p>
                                </div>
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">@lang('client_site.net')<br>@lang('client_site.earning')</span>
                                    <p class="add_ttrr">${{ @$row->professional_amount }}</p>
                                </div>

                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">>@lang('client_site.payment') <br>
                                        @lang('client_site.status')</span>
                                    <p class="add_ttrr">
                                        @if(@$row->balance_status=="R")
                                        @lang('client_site.under_settelement')
                                        @else
                                        @lang('client_site.balance_transfered')
                                        @endif
                                    </p>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                @else
                <center>
                    <h3 class="error">@lang('client_site.payment_details_not_found')</h3>
                </center>
                @endif
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
<link href="{{ URL::to('public/frontend/css/calender.css') }}" rel="stylesheet" type="text/css">
<script src="{{ URL::to('public/frontend/js/jquery-ui.js') }}"></script>


<script>
    function setVl(tn){
        $('#token_no').val(tn);
    }
    $('#myForm2').validate();
    $('.timepicker').timepicker({
        timeFormat: 'H:mm',
        interval: 15,
        minTime: '00',
        maxTime: '11:00pm',
        startTime: '09:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
$(function() {
        // $('.parent_loader').show();
        $("#date").datepicker({dateFormat: "dd-mm-yy",
           defaultDate: new Date(),
           onClose: function( selectedDate ) {

          }
        });

        $("#datepicker").datepicker({dateFormat: "dd-mm-yy",
           defaultDate: new Date(),
           onClose: function( selectedDate ) {
           $( "#datepicker1").datepicker( "option", "minDate", selectedDate );
          }
        });
    $("#datepicker1").datepicker({dateFormat: "dd-mm-yy",
     defaultDate: new Date(),
          onClose: function( selectedDate ) {
          $( "#datepicker" ).datepicker( "option", "maxDate", selectedDate );
        }
     });

    });
</script>
<script>
    $('#srch').click(function(){
        $('#myForm').submit();
    });
    $('.sbmt').click(function(){
        if($('#date').val()!="" && $('#time').val()!=""){
            $('.parent_loader').show();
            setTimeout(function(){ $('#myForm2').submit(); }, 3000);
        }
        else{
             $('#myForm2').submit();
        }

    });
</script>
@endsection
