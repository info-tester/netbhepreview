@extends('layouts.app')
@section('title')
    @lang('site.post') @lang('site.details')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.view_imported_tools')</h2>

        {{-- @include('includes.professional_tab_menu') --}}

        <div class="bokcntnt-bdy no-margin-top">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.user_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="blog-right details-right">
                    <div class="blog-box">
                        
                        <div class="blog-dtls">
                            
                            <div class="from-field">
                                <div class="weicome">
                                    <a href="javascript:;" class="view_tools_help acpt assginbtn pull-right" data-title="{{$details->getImportingToolsdata['title']}}" data-helpdesc="{{ $details->getImportingToolsdata['help_tools_view'] }}" style="margin: 2px;">@lang('site.view_tool_help')</a>
                                    <h3><strong> @lang('site.title'): </strong>{{ @$details->getImportingToolsdata->title }}</h3>
                                    <strong>@lang('site.category') : </strong>{{ @$details->getImportingToolsdata->category->form_category }}
                                    <div class="clearfix"></div>
                                    {{-- <strong>Added by : </strong> {{ @$details->getImportingToolsdata['added_by']=='A'?'Admin': ( @$details->user->nick_name ?@$details->user->nick_name : @$details->user->name ) }} --}}
                                    <div class="clearfix"></div>
                                    <strong>@lang('site.description') : </strong> 
                                    @if($details->user_imoprt_tools_ans_desc)
                                        <?= @$details->user_imoprt_tools_ans_desc ?>
                                    @else    
                                        <?= @$details->getImportingToolsdata->description ?>
                                    @endif    

                                    {{-- <h3>{{ @$form_data->form_title }}</h3>
                                    <strong>{{ @$form_data->category->form_category }}</strong>
                                    <p>{{ @$form_data->form_title }}</p> --}}
                                </div>                       
                            </div>

                            
                        </div>
                    </div>
                    
                </div>
            </div>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="view_tool_help_Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Título: <span class="tool-title"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                
                <div class="modal-body">
                    <span class="tool-help-desc-view"></span>
                </div>           
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
    <script>
        $(document).ready(function(){
            $('#myForm').validate();
            $('.view_tools_help').click(function(){
                var title = $(this).data('title'); 
                var help_desc = $(this).data('helpdesc'); 
                $('.tool-title').html(title);
                $('.tool-help-desc-view').html(help_desc);       
                $('#view_tool_help_Modal').modal('show');
            });

            $('.view_importtools_help').click(function(){          
                $('#view_toolimport_help_Modal').modal('show');
            });
        });
    </script>

@endsection