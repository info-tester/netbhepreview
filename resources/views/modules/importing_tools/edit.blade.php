@extends('layouts.app')
@section('title')
    {{-- @lang('site.edit') @lang('site.blog')-@lang('site.post') --}}
    Edit Importing tools
@endsection
@section('style')
@include('includes.style')
<style type="text/css">
    .chck_eml_rd{
        color: red;
        font-weight: bold;
        font-size: 14px;
    }
    .tooltip {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted #ccc;
        color: #006080;
        opacity: 1 !important;
    }

    .tooltip .tooltiptext {
        visibility: hidden;
        position: absolute;
        width: 250px;
        background-color: #555;
        color: #fff;
        text-align: center;
        padding: 5px;
        border-radius: 6px;
        z-index: 1;
        opacity: 0;
        transition: opacity 0.3s;
        margin-left: 10px
    }

    .tooltip-right {
        top: 0px;
        left: 125%;
    }

    .tooltip-right::after {
        content: "";
        position: absolute;
        top: 10%;
        right: 100%;
        margin-top: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: transparent #555 transparent transparent;
    }

    .tooltip:hover .tooltiptext {
        visibility: visible;
        opacity: 1;
    }
</style>
@endsection
@section('scripts')
@include('includes.scripts')

@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>
            @lang('site.edit_imported_tools')
            {{-- @lang('site.edit') @lang('site.blog')-@lang('site.post') --}}
        </h2>
        @include('includes.professional_tab_menu')
        <div class="bokcntnt-bdy no-margin-top">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.professional_sidebar')


            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">
                    <form id="myform" method="post" action="{{ route('professional-importing-tools.update',['form_id'=>@$details->id]) }}">
                    {{-- <form id="myform" method="post" action="{{ route('professional-form.store') }}"> --}}
                        <input name="_method" type="hidden" value="PUT">
                        {{ csrf_field() }}
                        <div class="form_body">

                            <div class="row">


                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="personal-label" for="exampleInputEmail1">@lang('site.title') *</label>
                                    <input type="text" name="title" class="personal-type required" id="title" placeholder="Title" value="{{ @$details->title }}">
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="personal-label" for="exampleInputEmail1">@lang('site.category') *</label>
                                        <select name="cat_id" class="personal-type newdrop required">
                                            <option value="">Select</option>
                                            @foreach($category as $row)
                                            <option value="{{ $row['id'] }}" @if(@$details->cat_id==$row['id']) selected @endif >{{ $row['form_category'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="personal-label" for="exampleInputEmail1">@lang('site.description') *</label>
                                        <div class="clearfix"></div>
                                        <textarea name="desc" id="desc" rows="10" placeholder="Description" style="width:100%" class="required desc">{{ @$details->description }}</textarea>
                                    </div>
                                    <div class="clearfix"></div>
                                        <p class="error_1" id="cntnt"></p>
                                        <div class="clearfix"></div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1" class="personal-label">
                                            @lang('site.help_tools')
                                            <div class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">{{__('site.help_tools_tooltip')}}</span>
                                            </div>
                                        </label>
                                        <div class="clearfix"></div>
                                        <textarea name="help_tools_view" id="desc" placeholder="@lang('site.help_tools')" style="width:100%" class="personal-type99 desc" rows="7">{{ @$details->help_tools_view }}</textarea>
                                    </div>
                                </div>
                                            
                                            
                                <div class="clearfix"></div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="submit-login add_btnm">
                                        <input value="@lang('site.update')" type="submit" class="login_submitt">
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>


        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')

<script>
    $(document).ready(function(){
        $("#myform").validate();
        tinyMCE.init({
            mode : "specific_textareas",
            editor_selector : "desc",
            height: '320px',
            plugins: [
              'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
              'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
              'save table contextmenu directionality emoticons template paste textcolor'
            ],
            relative_urls : false,
            remove_script_host : false,
            convert_urls : true,
            toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            //toolbar: 'insertfile undo redo | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor emoticons',
            force_br_newlines : false,
            force_p_newlines : false,
            forced_root_block : '',
                images_upload_url: '{{ URL::to('storage/app/public/uploads/content_image/') }}',
                images_upload_handler: function(blobInfo, success, failure) {
                    var formD = new FormData();
                    formD.append('file', blobInfo.blob(), blobInfo.filename());
                    formD.append( "_token", '{{csrf_token()}}');
                    $.ajax({
                        url: '{{route("admin.artical.img.upload")}}',
                        data: formD,
                        type: 'POST',
                        contentType: false,
                        cache: false,
                        processData:false,
                        dataType: 'JSON',
                        success: function(jsn) {
                            if(jsn.status == 'ERROR') {
                                failure(jsn.error);
                            } else if(jsn.status == 'SUCCESS') {
                                success(jsn.location);
                            }
                        }
                    });
                }, 
            });
            $("#myform").submit(function (event) {
        if(tinyMCE.get('desc').getContent()==""){
            event.preventDefault();
            $("#cntnt").html("Description field is required").css('color','red');
        }
        else{
            $("#cntnt").html("");
        }
    });
    });

</script>
   <script>
    $(document).ready(function(){
        $("#myform").validate();
    });
</script>
<style>
    .error{
        color: red !important;
    }
</style>

@endsection