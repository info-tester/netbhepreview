@extends('layouts.app')
{{-- @section('title', 'Post Details') --}}
@section('title', 'Detalhes da postagem')
    {{-- @lang('site.post') @lang('site.details')
@endsection --}}
@section('style')
	@include('includes.style')
    <style>
    .sel {
        color: #007bff !important;
    }
    .munmun img {
        width: 100% !important;
        height: auto !important;
    }
    </style>
@endsection

@section('scripts')
	@include('includes.scripts')
    <script type="text/javascript">
        $(document).ready(function() {
    $("#owl-demo-4").owlCarousel({
        margin:10,
        nav: true,
        loop: true,
        responsive: {
          0: {
            items: 1
          },
          475: {
            items:1
          },
          768: {
            items:1
          },
        }
      });
    $("#owl-demo-5").owlCarousel({
        margin:10,
        nav: true,
        loop: true,
        responsive: {
          0: {
            items: 1
          },
          475: {
            items:1
          },
          768: {
            items:1
          },
        }
      });
})
    </script>
@endsection

@section('header')
	@include('includes.header')
@endsection


@section('content')
    <section class="blog-body">
    <div class="container">
        <div class="row">
            <div class="blog-head">
                <h2>@lang('site.blog')</h2>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-12">
                <div class="left-blog">
                    <div class="blog-categori">
                        <h3>@lang('site.categories')</h3>
                        <ul class="catte-ul">
                            @foreach(@$category as $cat)
                            <li><a href="{{ route('blog.frontend.slug',['slug'=>@$cat->slug]) }}" @if(@$blog->blogCategoryName->slug==$cat->slug) class="sel" @endif>{{ @$cat->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="blog-profile">
                        <div id="owl-demo-4" class="owl-carousel owl-theme">
                            @foreach(@$user as $us)
                                <div class="item">
                                  <div class="guiede-box">
                                        <div class="guide-image"><img src="{{ URL::to('storage/app/public/uploads/profile_pic/'.$us->profile_pic) }}" alt=""></div>
                                        <div class="guide-dtls">
                                            <div class="guide-intro">
                                                <h3>
                                                    {{ @$us->nick_name ? @$us->nick_name : @$us->name }}

                                                    <ul>
                                                        {!! getStars($us->avg_review) !!}
                                                        <li>({{ $us->total_review ?? 0 }})</li>
                                                    </ul>

                                                </h3>
                                                @if(@Auth::guard('web')->user()->crp_no==null || @Auth::guard('web')->user()->crp_no=="")
                                                <p>{{@Auth::guard('web')->user()->crp_no  }}</p>
                                                @endif
                                            </div>
                                            <div class="guide-more blog-profl">
                                                <ul>
                                                    <li>
                                                        <span>@lang('site.fees')</span>
                                                        <p>R${{ @$us->rate_price }} / {{  @$us->rate=="M" ? __('client_site.minute') : __('client_site.hour') }}</p>
                                                    </li>
                                                    <li>
                                                        <span>@lang('client_site.language')</span>
                                                        <p>
                                                            @php
                                                                $j=1;
                                                            @endphp
                                                            @foreach(@$us->userLang as $u)
                                                                {{ @$u->languageName->name }}{{ sizeof(@$us->userLang)>$j ? ', ' : '' }}
                                                                @php
                                                                    $j++;
                                                                @endphp
                                                            @endforeach
                                                        </p>
                                                    </li>
                                                </ul>
                                                <a  data-slug="{{ @$us->slug }}" href="javascript:void(0);" class="guide-btn pull-left ssnbtnslg">@lang('site.request_a_session') </a>
                                                <a class="guide-btn pull-right" href="{{ route('pblc.pst',['slug'=>@$us->slug]) }}">@lang('site.view_profile') </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="blog-categori news-leter">
                        <p>{{ $nlContent->description ?? '' }}</p>
                        <form>
                            <input type="text" id="news_email" placeholder="@lang('site.type_email')" class="personal-type">
                            <button type="button" id="news_btn" class="login_submitt">@lang('site.submit')</button>
                        </form>

                    </div>

                    <div class="blog-profile">
                        <div id="owl-demo-5" class="owl-carousel owl-theme">
                            @foreach(@$userOther as $us)
                                <div class="item">
                                  <div class="guiede-box">
                                        <div class="guide-image"><img src="{{ URL::to('storage/app/public/uploads/profile_pic/'.$us->profile_pic) }}" alt=""></div>
                                        <div class="guide-dtls">
                                            <div class="guide-intro">
                                                <h3>
                                                    {{ @$us->nick_name ? @$us->nick_name : @$us->name }}

                                                    <ul>
                                                        {!! getStars($us->avg_review) !!}
                                                        <li>({{ $us->total_review ?? 0 }})</li>
                                                    </ul>

                                                </h3>
                                                @if(@Auth::guard('web')->user()->crp_no==null || @Auth::guard('web')->user()->crp_no=="")
                                                <p>{{@Auth::guard('web')->user()->crp_no  }}</p>
                                                @endif
                                            </div>
                                            <div class="guide-more blog-profl">
                                                <ul>
                                                    <li>
                                                        <span>@lang('site.fees')</span>
                                                        <p>R${{ @$us->rate_price }} / {{  @$us->rate=="M" ? __('client_site.minute') : __('client_site.hour') }}</p>
                                                    </li>
                                                    <li>
                                                        <span>@lang('client_site.language')</span>
                                                        <p>
                                                            @php
                                                                $j=1;
                                                            @endphp
                                                            @foreach(@$us->userLang as $u)
                                                                {{ @$u->languageName->name }}{{ sizeof(@$us->userLang)>$j ? ', ' : '' }}
                                                                @php
                                                                    $j++;
                                                                @endphp
                                                            @endforeach
                                                        </p>
                                                    </li>
                                                </ul>
                                                <a  data-slug="{{ @$us->slug }}" href="javascript:void(0);" class="guide-btn pull-left ssnbtnslg">@lang('site.request_a_session') </a>
                                                <a class="guide-btn pull-right" href="{{ route('pblc.pst',['slug'=>@$us->slug]) }}">@lang('site.view_profile') </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="blog-categori">
                        <h3>@lang('site.popular_post')</h3>


                        @foreach(@$popularblogs as $pblog)
                        <div class="post-box">
                            <h4><a href="{{ route('blog.frontend.details', ['slug'=>@$pblog->slug]) }}">{{ @$pblog->title }}</a></h4>
                            <p>
                                {!! substr(strip_tags(@$pblog->desc), 0, 150) !!}...
                            </p>
                            <ul>
                                <li> {{ @$pblog->postedBy->nick_name ? @$pblog->postedBy->nick_name : @$pblog->postedBy->name }}</li>
                                <li>{{ Date::parse(@$pblog->created_at)->format('jS-M-Y')}}</li>
                                <li>{{ @$pblog->blogCategoryName->name }}</li>
                            </ul>
                        </div>
                        @endforeach


                    </div>
                </div>
            </div>
           <div class="col-lg-9 col-md-8 col-sm-12">
                <div class="blog-right details-right for_editor_text">
                    <div class="blog-box">

                        <div class="blog-dtls">
                            <h4>{{ @$blog->title }}</h4>
                            <ul class="blog-post">
                                <li><img src="{{-- {{ @$blog->postedBy->profile_pic ? URL::to('storage/app/public/uploads/profile_pic').'/'.@@$blog->postedBy->profile_pic: URL::to('public/frontend/images/no_img.png')}} --}}{{ URL::to('public/frontend/images/post-1.png') }}"> @lang('site.author') :@if(@$blog->posted_by != 0) {{ @$blog->postedBy->nick_name ? @$blog->postedBy->nick_name : @$blog->postedBy->name }} @else Netbhe @endif</li>
                                <li>{{ Date::parse(@$blog->created_at)->format('jS-M-Y')}}</li>
                                <li>{{ @$blog->blogCategoryName->name }}</li>
                            </ul>

                            @if(@$blog->you_tube_video)
                                <div class="">
                                    <iframe id="videoObject" width="100%" height="300" src="https://www.youtube.com/embed/{{@$blog->you_tube_video}}" frameborder="2" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                                </div>
                            @else
                            <div class="blog-image" style="height: auto;">
                                <img src="{{ URL::to('storage/app/public/uploads/blog_image').'/'.@$blog->image }}" alt="" style="position: unset; width: 100%">
                            </div>
                            @endif
                            {{-- <div class="blog-image">
                                <img src="{{ URL::to('storage/app/public/uploads/blog_image').'/'.@$blog->image }}" alt="">
                            </div> --}}
                            <div class="prev-next">
                                <ul>
                                    <li>@lang('site.share_on'):</li>
                                    <li>
                                        <!-- ShareThis BEGIN -->
                                        {{-- <div class="sharethis-inline-share-buttons"></div> --}}
                                        <!-- ShareThis END -->
                                    </li>
                                    {{-- <li><a href="#"><img src="{{ URL::to('public/frontend/images/social1.png') }}" alt=""></a></li>
                                    <li><a href="#"><img src="{{ URL::to('public/frontend/images/social2.png') }}" alt=""></a></li>
                                    <li><a href="#"><img src="{{ URL::to('public/frontend/images/social3.png') }}" alt=""></a></li>
                                    <li><a href="#"><img src="{{ URL::to('public/frontend/images/social4.png') }}" alt=""></a></li> --}}
                                </ul>
                                <div class="sharethis-inline-share-buttons"></div>
                                <a href="#">{{ count(@$blog->blogComments) }} @lang('site.comments')</a>
                                <a class="post-commant" href="#">@lang('site.post') a @lang('site.comments')</a>
                            </div>
                            {{-- <h6>{{ @$blog->title }}</h6> --}}
                            {{-- <div class="munmun">{!! @$blog->desc !!}</p> --}}
                            {!! @$blog->desc !!}

                            {{-- <div class="prev-next">
                                <a href="#">Previous</a>
                                <a href="#">Next</a>
                            </div> --}}
                            <div class="blog-commants">
                                <h3>@lang('site.comments')</h3>
                                @if(sizeof(@$blog->blogComments)>0)
                                @foreach(@$blog->blogComments as $cmnt)
                                    <div class="commant-div">
                                        <span><img src="{{ URL::to('storage/app/public/uploads/blog_image').'/'.@$blog->image }}" alt=""></span>
                                        <p>{!! @$cmnt->msg !!}</p>
                                        <ul class="blog-post">
                                            <li><img src="{{ URL::to('public/frontend/images/post-1.png') }}" alt=""> Name : {{ @$cmnt->name }}</li>
                                            <li><img src="{{ URL::to('public/frontend/images/post-2.png') }}" alt=""> {{ Date::parse(@$cmnt->created_at)->format('D-M-Y')}}</li>
                                        </ul>
                                    </div>
                                @endforeach
                                @endif

                                <form name="myForm" id="myForm" method="post" action="{{ route('publish.comment') }}">
                                    @csrf
                                    <input type="hidden" name="blog_id" id="blog_id" value="{{ @$blog->id }}">
                                    <div class="row" style="width: 100%;">
                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                <div class="form-group">
                                                <input type="text" name="name" id="name" value="{{ @Auth::user()->nick_name ? @Auth::user()->nick_name : @Auth::user()->name }}" class="required personal-type" placeholder="Digite seu nome">
                                            </div>
                                            </div>

                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                <div class="form-group">
                                                <input type="text" name="email" id="email" value="{{ @Auth::user()->email }}" class="email required personal-type" placeholder="Insira seu email">
                                            </div>
                                            </div>


                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                           <div class="form-group">

                                        <textarea class="required personal-type99" placeholder="Digite seu comentário" name="msg" id="msg"></textarea>
                                        <button type="submit" class="login_submitt">@lang('site.post')</button>
                                    </div>
                                     </div>
                                 </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<style>
    .munmun ul li {
        list-style: disc;
        margin-left: 14px;
     }

</style>
@endsection
@section('footer')
	@include('includes.footer')
    <script>
        $('#myForm').validate();
    </script>
@endsection
