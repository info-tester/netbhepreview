@extends('layouts.app')
@section('title')
    @lang('site.blog')
@endsection
@section('style')
	@include('includes.style')
    <style>
     .sel {
        color: #007bff !important;
     }
    </style>
@endsection

@section('scripts')
	@include('includes.scripts')
    <script>
$(document).ready(function(){
    $(".mobile_filter").click(function(){
        $(".dshbrd-lftmnu").slideToggle();
        });

    });
    </script>

 <script>
$(document).ready(function(){
    $(".menu_dash").mouseenter(function(){
        $(".dropdown_dash").slideDown();
    });
    $(".dropdown_dash").mouseleave(function(){
        $(".dropdown_dash").slideUp();
    });
});
</script>

<script>
$(document).ready(function() {
    $("#owl-demo-4").owlCarousel({
        margin:10,
        nav: true,
        loop: true,
        responsive: {
          0: {
            items: 1
          },
          475: {
            items:1
          },
          768: {
            items:1
          },
        }
      });
    $("#owl-demo-5").owlCarousel({
        margin:10,
        nav: true,
        loop: true,
        responsive: {
          0: {
            items: 1
          },
          475: {
            items:1
          },
          768: {
            items:1
          },
        }
      });
})
</script>
@endsection

@section('header')
	@include('includes.header')
@endsection


@section('content')
    <section class="blog-body">
    <div class="container">
        <div class="row">
            <div class="blog-head">
                <h2>@lang('site.blog')</h2>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-12">
                <div class="left-blog">
                    <div class="blog-categori">
                        <h3>@lang('site.categories')</h3>
                        <ul class="catte-ul">
                            @foreach(@$category as $cat)
                            <li><a href="{{ route('blog.frontend.slug',['slug'=>@$cat->slug]) }}" @if(@$key['slug']==$cat->slug) class="sel" @endif>{{ @$cat->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="blog-profile">
                        <div id="owl-demo-4" class="owl-carousel owl-theme">
                            @foreach(@$user as $us)
                                <div class="item">
                                  <div class="guiede-box">
                                        <div class="guide-image"><img src="{{ URL::to('storage/app/public/uploads/profile_pic/'.$us->profile_pic) }}" alt=""></div>
                                        <div class="guide-dtls">
                                            <div class="guide-intro">
                                                <h3>
                                                    {{ @$us->nick_name ? @$us->nick_name : @$us->name }}

                                                    <ul>
                                                        {!! getStars($us->avg_review) !!}
                                                        <li>({{ $us->total_review ?? 0 }})</li>
                                                    </ul>

                                                </h3>
                                                @if(@Auth::guard('web')->user()->crp_no==null || @Auth::guard('web')->user()->crp_no=="")
                                                <p>{{@Auth::guard('web')->user()->crp_no  }}</p>
                                                @endif
                                            </div>
                                            <div class="guide-more blog-profl">
                                                <ul>
                                                    <li>
                                                        <span>@lang('site.fees')</span>
                                                        <p>R${{ @$us->rate_price }} / {{  @$us->rate=="M" ? __('client_site.minute') : __('client_site.hour') }}</p>
                                                    </li>
                                                    <li>
                                                        <span>@lang('client_site.language')</span>
                                                        <p>
                                                            @php
                                                                $j=1;
                                                            @endphp
                                                            @foreach(@$us->userLang as $u)

                                                                {{ @$u->languageName->name }}{{ sizeof(@$us->userLang)>$j ? ', ' : '' }}
                                                                @php
                                                                    $j++;
                                                                @endphp
                                                            @endforeach
                                                        </p>
                                                    </li>
                                                </ul>
                                                <a  data-slug="{{ @$us->slug }}" href="javascript:void(0);" class="guide-btn pull-left ssnbtnslg">@lang('site.request_a_session') </a>
                                                <a class="guide-btn pull-right" href="{{ route('pblc.pst',['slug'=>@$us->slug]) }}">@lang('site.view_profile') </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="blog-categori news-leter">
                        <p>{{ $nlContent->description }}</p>
                        <form>
                            <input type="text" id="news_email" placeholder="@lang('site.type_email')" class="personal-type">
                            <button type="button" id="news_btn" class="login_submitt">@lang('site.submit')</button>
                        </form>

                    </div>

                    <div class="blog-profile">
                        <div id="owl-demo-5" class="owl-carousel owl-theme">
                            @foreach(@$userOther as $us)
                                <div class="item">
                                  <div class="guiede-box">
                                        <div class="guide-image"><img src="{{ URL::to('storage/app/public/uploads/profile_pic/'.$us->profile_pic) }}" alt=""></div>
                                        <div class="guide-dtls">
                                            <div class="guide-intro">
                                                <h3>
                                                    {{ @$us->nick_name ? @$us->nick_name : @$us->name }}

                                                    <ul>
                                                        {!! getStars($us->avg_review) !!}
                                                        <li>({{ $us->total_review ?? 0 }})</li>
                                                    </ul>

                                                </h3>
                                                @if(@Auth::guard('web')->user()->crp_no==null || @Auth::guard('web')->user()->crp_no=="")
                                                <p>{{@Auth::guard('web')->user()->crp_no  }}</p>
                                                @endif
                                            </div>
                                            <div class="guide-more blog-profl">
                                                <ul>
                                                    <li>
                                                        <span>@lang('site.fees')</span>
                                                        <p>R${{ @$us->rate_price }} / {{  @$us->rate=="M" ? __('client_site.minute') : __('client_site.hour') }}</p>
                                                    </li>
                                                    <li>
                                                        <span>@lang('client_site.language')</span>
                                                        <p>
                                                            @php
                                                                $j=1;
                                                            @endphp
                                                            @foreach(@$us->userLang as $u)

                                                                {{ @$u->languageName->name }}{{ sizeof(@$us->userLang)>$j ? ', ' : '' }}
                                                                @php
                                                                    $j++;
                                                                @endphp
                                                            @endforeach
                                                        </p>
                                                    </li>
                                                </ul>
                                                <a  data-slug="{{ @$us->slug }}" href="javascript:void(0);" class="guide-btn pull-left ssnbtnslg">@lang('site.request_a_session') </a>
                                                <a class="guide-btn pull-right" href="{{ route('pblc.pst',['slug'=>@$us->slug]) }}">@lang('site.view_profile') </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>


                    <div class="blog-categori">
                        <h3>@lang('site.popular_post')</h3>

                        @foreach(@$popularblogs as $pblog)
                        <div class="post-box">
                            <h4><a href="{{ route('blog.frontend.details', ['slug'=>@$pblog->slug]) }}">{{ @$pblog->title }}</a></h4>
                            <p>
                                {!! substr(strip_tags(@$pblog->desc), 0, 150) !!}...
                            </p>
                            <ul>
                                <li> {{ @$pblog->postedBy->nick_name ? @$pblog->postedBy->nick_name : @$pblog->postedBy->name }}</li>
                                <li>{{ Date::parse(@$pblog->post_date)->format('jS-M-Y')}}</li>
                                <li>{{ @$pblog->blogCategoryName->name }}</li>
                            </ul>
                        </div>
                        @endforeach

                    </div>



                </div>
            </div>
            @if(sizeof(@$blogs)>0)
                <div class="col-lg-9 col-md-8 col-sm-12">
                    <div class="blog-right">
                        @foreach(@$blogs as $blg)
                        <div class="blog-box">
                            <div class="blog-image">
                                <img src="{{ URL::to('storage/app/public/uploads/blog_image').'/'.@$blg->image }}" alt="">
                                <span class="blog-ribbon">{{ @$blg->title }}</span>
                            </div>
                            <div class="blog-dtls">
                                <h4><a href="{{ route('blog.frontend.details', ['slug'=>@$blg->slug]) }}">{{ @$blg->title }}</a></h4>
                                <ul class="blog-post">
                                    <li><img src="{{-- {{ @$blg->postedBy->profile_pic ? URL::to('storage/app/public/uploads/profile_pic').'/'.@@$blg->postedBy->profile_pic: URL::to('public/frontend/images/no_img.png')}} --}}{{ URL::to('public/frontend/images/post-1.png') }}"> @lang('site.author') :@if(@$blg->posted_by != 0) {{ @$blg->postedBy->nick_name ? @$blg->postedBy->nick_name : @$blg->postedBy->name }} @else Netbhe @endif</li>
                                    <li> {{ Date::parse(@$blg->post_date)->format('jS-M-Y')}}</li>
                                    <li>{{ @$blg->blogCategoryName->name }}</li>
                                </ul>
                                <p>{!! substr(strip_tags(@$blg->desc), 0, 150) !!}...</p>
                                <a class="blog-more" href="{{ route('blog.frontend.details', ['slug'=>@$blg->slug]) }}">@lang('site.read_more')</a>
                                <ul class="review">
                                    <li><a href="#"><img src="{{ URL::to('public/frontend/images/blog-icon1.pn') }}g" alt=""> {{ count(@$blg->blogComments) }} @lang('site.comments')</a></li>
                                    {{-- <li><a href="#"><img src="{{ URL::to('public/frontend/images/blog-icon2.png') }}" alt=""> 55 view</a></li> --}}
                                </ul>
                            </div>
                        </div>
                        @endforeach
                        <div class="pagination_area">
                        {{ @$blogs->links('vendor.pagination.default') }}
                        </div>
                    </div>
                </div>
            @else
                <div class="col-lg-9 col-md-8 col-sm-12">
                    <div class="blog-right">

                        <div class="blog-box">
                            <center><span><h3 class="error">OPSS ! @lang('site.blog_post_not_found')</h3></span></center>
                        </div>
                    </div>
                </div>
            @endif

        </div>
    </div>
</section>
@endsection
@section('footer')
	@include('includes.footer')
    <script>
        $('.ssnbtnslg').click(function(){
            if($(this).data('slug')!=""){
                localStorage.removeItem('bookSlug');
                localStorage.setItem('bookSlug', $(this).data('slug'));
                location.href="{{ route('login') }}"
            }
        });
    </script>
@endsection
