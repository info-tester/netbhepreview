<!-- <div class="cert_template font_montserrat template1" style="width: 700px; height: 550px;">
    
    <div class="first-templte">
        <div class="cert_det">
            <small class="issued_on"></small>
            <small class="expires_on"></small>
        </div>
        <div>
            <p class="cert_head secondary_color"></p>
            <h4 class="stu_name primary_color">&lcub;&lcub;@lang('client_site.student_name')&rcub;&rcub;</h4>
            <p class="below_stu_name primary_color"></p>
        </div>
        <div class="next-content">
            <p class="above_course secondary_color"></p>
            <h4 class="course_name primary_color">&lcub;&lcub;@lang('client_site.course_name')&rcub;&rcub;</h4>
        </div>

        <div class="note"><p class="new_cont"></p></div>
        <div class="below_course_div secondary_color font_w_600">
            @if(@Auth::user()->signature)
                <img src="{{ url('storage/app/public/uploads/signature/'.@Auth::user()->signature) }}" height="50px"/>
            @else
                <p>{{ @Auth::user()->nick_name ?? @Auth::user()->name }}</p>
            @endif
            <p class="prof_name">{{ @Auth::user()->nick_name ?? @Auth::user()->name }}</p>
        </div>

    </div>
</div> -->

<div class="cert_template font_montserrat template1 template1_new primary_color_border primary_color_light_background" style="width: 700px; height: 550px;">
    
    <div class="first-templte">
        <div class="cert_det">
            <small class="issued_on secondary_color primary_color_border"></small>
            <small class="expires_on secondary_color primary_color_border"></small>
        </div>
        <div>
            <p class="cert_head secondary_color"></p>
            <h4 class="stu_name primary_color">&lcub;&lcub;@lang('client_site.student_name')&rcub;&rcub;</h4>
            <p class="below_stu_name primary_color"></p>
        </div>
        <div class="next-content">
            <p class="above_course secondary_color"></p>
            <h4 class="course_name primary_color">&lcub;&lcub;@lang('client_site.course_name')&rcub;&rcub;</h4>
        </div>

        <div class="note"><p class="new_cont secondary_color"></p></div>
        <div class="clearfix"></div>
        <div class="below_course_div secondary_color font_w_600">
            @if(@Auth::user()->signature)
                <img src="{{ url('storage/app/public/uploads/signature/'.@Auth::user()->signature) }}" height="50px"/>
            @else
                <p>&lcub;&lcub;Assinatura&rcub;&rcub;</p>
            @endif
            <p class="prof_name secondary_color">{{ @Auth::user()->nick_name ?? @Auth::user()->name }}</p>
        </div>
        <em class="icon_certificate primary_color"><i class="fa fa-certificate"></i></em>
    </div>
</div>