@extends('layouts.app')
@section('title')

    Certificate Template
@endsection
@section('style')
@include('includes.style')

@endsection
@section('scripts')
@include('includes.scripts')
<script src="{{URL::to('public/js/chosen.jquery.min.js')}}"></script>
@endsection
@section('header')
@include('includes.professional_header')


<link rel="stylesheet" href="{{URL::to('public/css/chosen.css')}}">
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.certificate_template')</h2>

        <div class="bokcntnt-bdy no-margin-top">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0 && $user->sell!='PS')
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>

            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">

                @if(auth()->user()->sell != 'VS')
                <div class="" style="float: right;">
                    <a class="btn btn-success" href="{{route('create.certificate.template')}}">+ @lang('site.add_new_template')</a>
                </div>
                @endif

                <div class="buyer_table">
                    <p class="alert alert-info">
                        {{__('client_site.certificate_template_assing_1')}}
                        <br>
                        {{__('client_site.certificate_template_assing_2')}}
                    </p>
                    <div class="table-responsive">
                        @if(sizeof(@$templates)>0)
                        <div class="table">
                            <div class="one_row1 hidden-sm-down only_shawo">
                                <div class="cell1 tab_head_sheet">@lang('site.title')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.status')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.action')</div>
                            </div>
                            <!--row 1-->

                                    @foreach(@$templates as $k=>$detail)
                                    <div class="one_row1 small_screen31">
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.no')</span>
                                            <p class="add_ttrr">{{@$detail->name}}</p>
                                        </div>

                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.status')</span>
                                            <p class="add_ttrr">
                                                @if($detail['status'] == "A")
                                                    Ativo
                                                @elseif($detail['status'] == "I")
                                                    Inativa
                                                @endif
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                            @if($detail->status=="A")
                                                <a href="{{route('edit.certificate.template',[$detail->id])}}" class="acpt editbtn" style="margin: 2px;" title="Edit">@lang('site.edit')</a>

                                                @if($detail->status=="A")
                                                    <a href="{{ route('prof.certificate.status', ['id'=>$detail->id]) }}" onclick="return confirm('{{__('client_site.certificate_confrontation_inactive')}}')" class="acpt" title="@lang('site.inactive')">{{__('client_site.certificate_inactive')}}</a>
                                                @else
                                                    <a href="{{ route('prof.certificate.status', ['id'=>$detail->id]) }}" title="@lang('site.active')" onclick="return confirm('{{__('client_site.certificate_confrontation_active')}}')" class="acpt">{{__('client_site.certificate_active')}}</a>
                                                @endif

                                                <a href="{{ route('delete.certificate.template', ['id'=>$detail->id]) }}" title="@lang('site.delete')" data-assigned="{{$detail->assigned}}" class="del_cert rjct">{{__('client_site.certificate_delete')}}</a>


                                                <!-- <a href="{{route('view.product.certificate.template', [$detail->id])}}" class="acpt viewbtn" style="margin: 2px;" title="Show">Antevisão</a> -->

                                                    <!-- <a href="{{route('certificate.copy', ['id'=>@$detail->id])}}" class="acpt view_asgi_btn" style="margin: 2px;">Copy</a> -->

                                            @else
                                                <a href="{{ route('prof.certificate.status', ['id'=>$detail->id]) }}" title="@lang('site.active')" onclick="return confirm('{{__('client_site.certificate_confrontation_logbook')}}')" class="acpt rjct">{{__('client_site.certificate_active')}}</a>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach

                        </div>
                        @else

                            <div class="one_row small_screen31">
                                <center><span><h3 class="error">{{__('site.oops!_not_found')}}</h3></span></center>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

<!--assigne Modal -->
<div class="modal fade" id="assignModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">@lang('site.Assign_User')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="myform" method="post" action="{{ route('prof.certificate.temp.user.assigne') }}">
                {{ csrf_field() }}
                <div class="modal-body">
                    <h5>Título da ferramenta: <span class="tool-title"></span></h5>

                        <label for="exampleInputEmail1">@lang('site.Select_User')</label>
                        <select name="user_id[]" class="required form-control newdrop required chosen-select" multiple="true" required="" >
                            <option value="">Select</option>
                            @foreach($all_paid_users as $row)
                                <option value="{{$row->id}}">{{@$row->nick_name ? @$row->nick_name : @$row->name}}</option>
                            @endforeach
                        </select>




                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom:15px">
                            <div class="your-mail">
                                <label for="exampleInputEmail1" class="personal-label">Course Name*</label>
                                <input type="text" autocomplete="off" name="course_name" id="course_name" class="form-control fdte  required" placeholder="Course Name">
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom:15px">
                            <div class="your-mail">
                                <label for="exampleInputEmail1" class="personal-label">Course Duration*</label>
                                <input type="text" autocomplete="off" name="course_duration" id="course_duration" class="form-control fdte  required" placeholder="Course Duration">
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom:15px">
                            <div class="your-mail">
                                <label for="exampleInputEmail1" class="personal-label">Signature*</label>
                                <input type="text" autocomplete="off" name="signature" id="signature" class="form-control fdte  required" placeholder="Signature">
                            </div>
                        </div>

                    <input type="hidden" name="certificate_template_id" class="form_id"> {{-- this calass form id get form id --}}
                </div>

                <div class="modal-footer">
                    <button type="submit" value="submit" class="btn btn-primary">@lang('site.assign')</button>
                </div>

            </form>
        </div>
    </div>
</div>

<!--View tool help Modal start -->
<div class="modal fade" id="view_tool_help_Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Titulo : <span class="tool-title"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <span class="tool-help-desc-view"></span>
            </div>
        </div>
    </div>
</div>
<!--View tool help Modal end -->



</section>




@endsection
@section('footer')
@include('includes.footer')

<script>

$(document).ready(function(){
    $('.del_cert').click(function(e){
        if(confirm('{{__('client_site.certificate_confrontation_delete')}}')){
            if($(this).data('assigned') == 'Y'){
                toastr.error('{{__('client_site.certificate_confrontation_delete_error')}}');
                e.preventDefault();
                return false;
            } else {
                return true;
            }
        } else {
            e.preventDefault();
            return false;
        }
    })
    $('.show_assign').click(function(){
        var title = $(this).data('title');
        var id = $(this).data('id');
        $('.tool-title').html(title);
        $('.form_id').val(id);
        $('#assignModal').modal('show');
    });

    $('.view_tools_help').click(function(){
        var title = $(this).data('title');
        var help_desc = $(this).data('helpdesc');
        $('.tool-title').html(title);
        $('.tool-help-desc-view').html(help_desc);
        $('#view_tool_help_Modal').modal('show');
    });

});

$(".chosen-select").chosen();

$(document).ready(function(){
    $("#myform").validate();
});
</script>

<form method="post" id="destroy">
    {{ csrf_field() }}
    {{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
    var confirm = window.confirm('Do you want to delete this question?');
    if(confirm){
        $("#destroy").attr('action',val);
        $("#destroy").submit();
    }
 }
</script>
<style>
    .error{
        color: red !important;
    }
</style>

@endsection
