<!-- <div class="cert_template font_montserrat template2" style="width: 700px; height: 550px;">
    <div class="new-temp secondary_color">
        <div class="second-templte primary_color">
            <h2 class="cert_head primary_color"></h2>
            <div>
                <p class="above_stu_name secondary_color"></p>
                <h4 class="stu_name primary_color">&lcub;&lcub;@lang('client_site.student_name')&rcub;&rcub;</h4>
            </div>
            <div>
                <p class="above_course w-80 secondary_color"></p>
                <h4 class="course_name primary_color">&lcub;&lcub;@lang('client_site.course_name')&rcub;&rcub;</h4>
                <div class="note"><p class="new_cont secondary_color w-80"></p></div>
            </div>
            <div class="below_course_div secondary_color font_w_600 new_sign">
                @if(@Auth::user()->signature)
                    <img src="{{ url('storage/app/public/uploads/signature/'.@Auth::user()->signature) }}" height="50px"/>
                @else
                    <p>{{ @Auth::user()->nick_name ?? @Auth::user()->name }}</p>
                @endif
                <p class="prof_name">{{ @Auth::user()->nick_name ?? @Auth::user()->name }}</p>
            </div>
            <div class="cert_det content__bottom secondary_color">
                <small class="issued_on"></small>
                <small class="expires_on"></small>
            </div>
        </div>
    </div>
</div> -->


<div class="cert_template font_montserrat template2_new" style="width: 700px; height: 550px;">
    <div class="new-temp primary_color_lighter_border">
        <div class="second-templte primary_color_dark_border">
            <h2 class="cert_head secondary_color_dark"></h2>
            <div>
                <b class="above_stu_name primary_color_background"></b>
                <h4 class="stu_name secondary_color_dark">&lcub;&lcub;@lang('client_site.student_name')&rcub;&rcub;</h4>
            </div>
            <div>
                <p class="above_course w-80 secondary_color"></p>
                <h4 class="course_name secondary_color_dark">&lcub;&lcub;@lang('client_site.course_name')&rcub;&rcub;</h4>
                <div class="note"><p class="new_cont secondary_color w-80"></p></div>
            </div>
            <div class="below_course_div secondary_color font_w_600 new_sign">
                @if(@Auth::user()->signature)
                    <img src="{{ url('storage/app/public/uploads/signature/'.@Auth::user()->signature) }}" height="50px"/>
                @else
                    <p>&lcub;&lcub;Assinatura&rcub;&rcub;</p>
                @endif
                <p class="prof_name secondary_color">{{ @Auth::user()->nick_name ?? @Auth::user()->name }}</p>
            </div>
            <div class="cert_det content__bottom secondary_color">
                <small class="issued_on"></small>
                <small class="expires_on"></small>
            </div>
        </div>
    </div>
</div>