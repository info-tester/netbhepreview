@extends('layouts.app')
@section('title')

    Certificate Template
@endsection
@section('style')
@include('includes.style')

@endsection
@section('scripts')
@include('includes.scripts')
<script src="{{URL::to('public/js/chosen.jquery.min.js')}}"></script>
@endsection
@section('header')
@include('includes.professional_header')
<link rel="stylesheet" href="{{URL::to('public/css/chosen.css')}}">
<style>
    .tab_no_hover:hover{
        background: #1781D2 !important;
    }
</style>
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.certificate_template')</h2>

        <div class="like-tab">
        	<ul>
            	<li><a class="active tab tab_no_hover tab_choose_temp" href="javascript:;" data-target="choose_temp">@lang('site.choose_template')</a></li>
                <li><a class="tab tab_content" href="javascript:;" data-target="content">@lang('site.content')</a></li>
                <li><a class="tab tab_design" href="javascript:;" data-target="design">@lang('site.design')</a></li>
            </ul>
        </div>

        <div class="bokcntnt-bdy no-margin-top">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>

            </div>
            {{-- @include('includes.professional_sidebar') --}}

            <form action="{{route('store.certificate.template')}}" method="post" id="myForm">
                @csrf

                <div class="dshbrd-lftmnu">
                    <div class="dashmnu">

                        <div class="tab_target" id="choose_temp">
                            <input type="text" name="template_number" id="template_number" value="1" hidden>
                            <div class="card cert_temp_card active_cert" data-temp="1">
                                <img class="card-img-top" src="{{url('/')}}/public/frontend/images/cert_temp1.jpg" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">@lang('client_site.cert_temp_title_1')</h5>
                                </div>
                            </div>

                            <div class="card cert_temp_card" data-temp="2">
                                <img class="card-img-top" src="{{url('/')}}/public/frontend/images/cert_temp2.jpg" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">@lang('client_site.cert_temp_title_2')</h5>
                                </div>
                            </div>

                            <div class="card cert_temp_card" data-temp="3">
                                <img class="card-img-top" src="{{url('/')}}/public/frontend/images/cert_temp3.jpg" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">@lang('client_site.cert_temp_title_3')</h5>
                                </div>
                            </div>

                            <div class="card cert_temp_card" data-temp="4">
                                <img class="card-img-top" src="{{url('/')}}/public/frontend/images/cert_temp4.jpg" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">@lang('client_site.cert_temp_title_4')</h5>
                                </div>
                            </div>
                        </div>

                        <div class="tab_target" id="content" style="display: none;">
                            <div class="px-2">
                                <div class="cert_cntnt" id="cert_head_div">
                                    <label for="cert_name">@lang('client_site.name')</label>
                                    <input type="text" name="name" id="cert_name" class="form-control required" placeholder="Dê um nome ao seu certificado">
                                </div>
                                <div class="cert_cntnt" id="cert_head_div">
                                    <label for="cert_head">@lang('site.title')</label>
                                    <input type="text" name="cert_head" id="cert_head" class="form-control required mb-3" value="">
                                </div>
                                <div class="cert_cntnt" id="above_stu_name_div">
                                    <label for="above_stu_name">@lang('client_site.text_above_student_name')</label>
                                    <input type="text" name="above_stu_name" id="above_stu_name" class="form-control required mb-3" value="">
                                </div>
                                <div class="cert_cntnt" id="below_stu_name_div">
                                    <label for="below_stu_name">@lang('client_site.text_below_student_name')</label>
                                    <input type="text" name="below_stu_name" id="below_stu_name" class="form-control required mb-3" value="">
                                </div>
                                <div class="cert_cntnt" id="above_course_div">
                                    <label for="above_course">@lang('client_site.text_above_course_name')</label>
                                    <input type="text" name="above_course" id="above_course" class="form-control required mb-3" value="">
                                </div>
                                <div class="cert_cntnt" id="below_course_div">
                                    <label for="below_course">@lang('client_site.text_below_course_name')</label>
                                    <input type="text" name="below_course" id="below_course" class="form-control required mb-3" value="">
                                </div>
                                <div class="cert_cntnt">
                                    <label for="issued_on">
                                        <input type="checkbox" name="issued_on" id="issued_on">
                                        @lang('client_site.issued_on')
                                    </label>
                                    <!-- <input type="text" name="issued_on" id="issued_on" class="form-control required mb-3" value="{{@$template->issued_on}}"> -->
                                </div>
                                <div class="cert_cntnt">
                                    <label for="expires_on">
                                        <input type="checkbox" name="expires_on" id="expires_on">
                                        @lang('client_site.expires_on')
                                    </label>
                                    <!-- <input type="text" name="expires_on" id="expires_on" class="form-control required mb-3" value="{{@$template->expires_on}}"> -->
                                </div>
                                <div class="cert_cntnt" id="expires_by_div">
                                    <label for="expires_by">@lang('client_site.expires_by')</label>
                                    <select name="expires_by" id="expires_by" class="form-control">
                                        <option value="1" @if(@$template->expires_by == 1) selected @endif >1 Anos</option>
                                        <option value="2" @if(@$template->expires_by == 2) selected @endif >2 Anos</option>
                                        <option value="3" @if(@$template->expires_by == 3) selected @endif >3 Anos</option>
                                    </select>
                                </div>
                                <!-- <div class="cert_cntnt">
                                    <label for="cert_id">Certificate ID</label>
                                    <input type="text" name="cert_id" id="cert_id" class="form-control mb-3" value="Certificate ID: ">
                                </div> -->
                                <div class="cert_cntnt" id="note_div">
                                    <label for="note">Nota</label>
                                    <textarea name="note" id="note" class="form-control mb-3"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="tab_target" id="design" style="display: none;">
                            <div class="px-2">
                                <div class="design_sec">
                                    <label for="primary_color" class="w-100">@lang('client_site.primary_color')</label>
                                    <input type="color" name="primary_color" id="primary_color" class="required w-100 mb-3">
                                </div>
                                <div class="design_sec">
                                    <label for="secondary_color" class="w-100">@lang('client_site.secondary_color')</label>
                                    <input type="color" name="secondary_color" id="secondary_color" class="required w-100 mb-3">
                                </div>
                                <div class="design_sec">
                                    <label for="font_style">@lang('client_site.font_style')</label>
                                    <select name="font_style" id="font_style" class="form-control mb-3 required">
                                        <option value="montserrat" class="font_montserrat">Montserrat</option>
                                        <option value="open_sans" class="font_open_sans">Open Sans</option>
                                        <option value="lato" class="font_lato">Lato</option>
                                        <option value="dancing" class="font_dancing">Dancing Script</option>
                                    </select>
                                </div>
                                <div class="design_sec">
                                    <label for="background" class="w-100">@lang('client_site.background') <a href="javascript:;" id="reset_background_img" title="Reset background image" class="pull-right">Reset</a></label>
                                    <input type="file" name="background" id="background" class="form-control mb-3">
                                    <small>@lang('client_site.recommended_size') 700 * 550 px</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="dshbrd-rghtcntn">
                    <div class="" style="float: right;">
                        <a href="javascript:;" id="template_confirm" class="btn btn-primary">@lang('site.select_this_template')</a>
                        <a href="javascript:;" id="content_save" class="btn btn-primary" style="display:none;">@lang('site.save_content')</a>
                        <a href="javascript:;" id="save_certificate" class="btn btn-primary" style="display:none;">@lang('site.save_certificate')</a>
                        <a class="btn btn-success" href="{{route('index.certificate.template')}}">< @lang('site.back')</a>
                    </div>
                    <br><br>
                    <div class="clearfix"></div>

                    <div class="template-element selected_temp template-element-1">
                        @include('modules.prof_certificate_temp.template_1')
                    </div>
                    <div class="template-element template-element-2" style="display: none;">
                        @include('modules.prof_certificate_temp.template_2')
                    </div>
                    <div class="template-element template-element-3" style="display: none;">
                        @include('modules.prof_certificate_temp.template_3')
                    </div>
                    <div class="template-element template-element-4" style="display: none;">
                        @include('modules.prof_certificate_temp.template_4')
                    </div>

                    <div class="clearfix"></div>
                    <br><br>
                    <div class="clearfix"></div>

                    <!-- <button type="submit" class="login_submitt">@lang('site.save_certificate')</button> -->
                </div>
            </form>

        </div>
    </div>

</section>




@endsection
@section('footer')
@include('includes.footer')

<script>
const date = new Date();
var html = "";
var template_id = 0;
var template_confirmed = false;
var content_confirmed = false;
// var image_size_limit = 20971520;

$(document).ready(function(){

    // jQuery.validator.addMethod("sizeLimit", function(value, element) {
    //     return this.optional(element) || $('#background')[0].files[0]['size'] < image_size_limit;
    // }, "@lang('client_site.cert_image_too_large')");

    $("#myForm").validate();

    $('#primary_color').val('#353A3E');
    $('#secondary_color').val('#6C6C6C');

    $('.cert_template').removeClass('font_lato font_montserrat font_open_sans font_dancing');
    $('.cert_template').addClass('font_dancing');

    $('.cert_cntnt').show();
    $('#above_stu_name_div').hide();
    $('#below_course_div').hide();
    $('.cert_head').text("@lang('client_site.this_is_to_cerify_that')");
    $('#cert_head').val("@lang('client_site.this_is_to_cerify_that')");
    $('.above_course').text("@lang('client_site.above_course_name')");
    $('#above_course').val("@lang('client_site.above_course_name')");

    // $('.issued_on').text('Issues On : '+`${date.getDate()}-${date.getMonth()+1}-${date.getFullYear()}`);
    // $('.expires_on').text('Expires On : '+`${date.getDate()}-${date.getMonth()+1}-${date.getFullYear()}`);
    // $('.cert_id').text('Certificate ID : ');

    $("#myForm").validate();

    $('.show_assign').click(function(){
        var title = $(this).data('title');
        var id = $(this).data('id');
        $('.tool-title').html(title);
        $('.form_id').val(id);
        $('#assignModal').modal('show');
    });

    $('#reset_background_img').click(function(){
        console.log("RESET BACKGROUND CERTIFICATE ID: "+template_id);
        if(confirm("@lang('client_site.reset_background')")){
            if(template_id > 0){
                $.get( "{{url('/')}}/reset-certificate-background/"+template_id, function( res ) {
                    console.log(res.message);
                });
            }
            var num = $('#template_number').val();
            var path = "{{url('/')}}/public/frontend/images/template" + num + ".jpg";
            console.log(path);
            $('.template'+num).css('background', 'url("' + path + '") no-repeat');
            $('.template'+num).css('background-size', 'cover');
            $('.template'+num).css('background-position', 'center');
        } else {
            return false;
        }
    });

    $('#issued_on').change(function(){
        if($('#issued_on').is(':checked'))
        $('.issued_on').text('Issued On : '+`${date.getDate()<10?'0':''}${date.getDate()}-${date.getMonth()+1<10?'0':''}${date.getMonth()+1}-${date.getFullYear()}`);
        else
        $('.issued_on').text('');
    });

    $('#expires_on').change(function(){
        if($('#expires_on').is(':checked')){
            $('#expires_by_div').show();
            var expby = parseInt($('#expires_by').val());
            console.log(expby);
            future = new Date(date.getFullYear() + expby, date.getMonth()+1, date.getDate());
            $('.expires_on').text('Expires On : '+`${future.getDate()<10?'0':''}${future.getDate()}-${future.getMonth()<10?'0':''}${future.getMonth()}-${future.getFullYear()}`);
        } else {
            $('#expires_by_div').hide();
            $('.expires_on').text('');
        }
    });

    $('#expires_by').change(function(){
        var expby = parseInt($('#expires_by').val());
        console.log(expby);
        future = new Date(date.getFullYear() + expby, date.getMonth()+1, date.getDate());
        $('.expires_on').text('Expires On : '+`${future.getDate()<10?'0':''}${future.getDate()}-${future.getMonth()<10?'0':''}${future.getMonth()}-${future.getFullYear()}`);
    });

    $('.view_tools_help').click(function(){
        var title = $(this).data('title');
        var help_desc = $(this).data('helpdesc');
        $('.tool-title').html(title);
        $('.tool-help-desc-view').html(help_desc);
        $('#view_tool_help_Modal').modal('show');
    });

    $('.cert_temp_card').click(function(){
        var temp_id = $(this).data('temp');
        console.log(temp_id);
        $('#template_number').val(temp_id);
        $('.template-element').hide();
        $('.selected_temp').removeClass('selected_temp');
        $('.template-element-'+temp_id).show();
        $('.template-element-'+temp_id).addClass('selected_temp');
        $('#template_number').val(temp_id);
        $('.active_cert').removeClass('active_cert');
        $(this).addClass('active_cert');
        show_hide_divs(temp_id);
        fill_data(temp_id);
        change_colors_font(temp_id);
    });

    function change_colors_font(temp_id){
        var primary = secondary = font ="";
        if(temp_id == 1){
            primary = "#3c3333";
            secondary = "#6C6C6C";
            font = "dancing";
            console.log(font);
        } else if(temp_id == 2){
            primary = "#1781d2";
            secondary = "#6C6C6C";
            font = "montserrat";
            console.log(font);
        } else if(temp_id == 3){
            primary = "#1781d2";
            secondary = "#6C6C6C";
            font = "lato";
            console.log(font);
        } else if(temp_id == 4){
            primary = "#3a0200";
            secondary = "#6C6C6C";
            font = "montserrat";
            console.log(font);
        }
        $('.cert_template').removeClass('font_lato font_montserrat font_open_sans font_dancing');
        $('.cert_template').addClass('font_'+font);

        var dark_primary = colorShade(primary, -60);
        var dark_secondary = colorShade(secondary, -50);

        var rgb_primary = hexToRgb(primary);
        var rgba_primary = [rgb_primary.slice(0, rgb_primary.length-1), ", 0.6", rgb_primary.slice(rgb_primary.length-1)].join('');
        var rgba1_primary = [rgb_primary.slice(0, rgb_primary.length-1), ", 0.4", rgb_primary.slice(rgb_primary.length-1)].join('');
        var rgba2_primary = [rgb_primary.slice(0, rgb_primary.length-1), ", 0.05", rgb_primary.slice(rgb_primary.length-1)].join('');
        rgba_primary = rgba_primary.replace('rgb','rgba');
        rgba1_primary = rgba1_primary.replace('rgb','rgba');
        rgba2_primary = rgba2_primary.replace('rgb','rgba');

        var rgb_secondary = hexToRgb(secondary);
        var rgba_secondary = [rgb_secondary.slice(0, rgb_secondary.length-1), ", 0.6", rgb_secondary.slice(rgb_secondary.length-1)].join('');
        var rgba1_secondary = [rgb_secondary.slice(0, rgb_secondary.length-1), ", 0.4", rgb_secondary.slice(rgb_secondary.length-1)].join('');
        rgba_secondary = rgba_secondary.replace('rgb','rgba');
        rgba1_secondary = rgba1_secondary.replace('rgb','rgba');

        $('.primary_color').css('color', primary);
        $('.secondary_color').css('color', secondary);
        $('.primary_color_dark').css('color', dark_primary);
        $('.primary_color_border').css('border-color', primary);
        $('.primary_color_border_top').css('border-top', `203px solid ${primary}`);
        $('.primary_color_light_background').css('background-color', rgba2_primary);
        $('.primary_color_dark_border').css('border-color', dark_primary);
        $('.primary_color_lighter_border').css('border-color', rgba1_primary);
        $('.secondary_color_light').css('color', rgba_secondary);
        $('.secondary_color_dark').css('color', dark_secondary);
        $('.secondary_color_light').css('color', rgba_secondary);
        $('.secondary_color_lighter').css('color', rgba1_secondary);
        $('.secondary_color_lighter_border').css('border-color', rgba1_secondary);

        $('#primary_color').val(primary);
        $('#secondary_color').val(secondary);
    }
    $('.tab_content').click(function(){
        if(template_confirmed == false){
            toastr.error(`@lang('client_site.select_temp_first')`);
        } else {
            $('.active').removeClass('active');
            $(this).addClass('active');
            $('.tab_target').hide();
            $('#'+$(this).data('target')).show();
            $('#template_confirm').hide();
            $('#save_certificate').hide();
            $('#content_save').show();
        }
    });

    $('.tab_design').click(function(){
        if(content_confirmed == false){
            toastr.error(`@lang('client_site.confirm_cont_first')`);
        } else {
            $('.active').removeClass('active');
            $(this).addClass('active');
            $('.tab_target').hide();
            $('#'+$(this).data('target')).show();
            $('#template_confirm').hide();
            $('#content_save').hide();
            $('#save_certificate').show();
        }
    });

    $('#template_confirm').click(function(){
        $('.active').removeClass('active');
        $('.tab_content').addClass('active');
        $('.tab_target').hide();
        $('#content').show();
        $(this).hide();
        $('#content_save').show();
        if($('#template_number').val() == 1) $('#below_stu_name').removeClass('required');
        template_confirmed = true;
    });

    $('#content_save').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{csrf_token()}}"
            }
        });
        if($('#myForm').valid()){
            console.log("TEMP ID: "+template_id);
            if(template_id == 0) {
                storeCert();
                content_confirmed = true;
                $('.active').removeClass('active');
                $('.tab_design').addClass('active');
                $('.tab_target').hide();
                $('#design').show();
                $('#content_save').hide();
                $('#save_certificate').show();
            } else {
                updateCert();
            }
        }
    });

    $('#background').change(function(){
        console.log($('#background')[0].files[0]);
        var num = $('#template_number').val();
        var file = this.files[0];
        var reader = new FileReader();
        reader.onloadend = function () {
            $('.template'+num).css('background', 'url("' + reader.result + '")');
            $('.template'+num).css('background-size', 'cover');
            $('.template'+num).css('background-position', 'center');
        }
        if (file) {
            reader.readAsDataURL(file);
        } else {
        }
    });

    $('#save_certificate').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{csrf_token()}}"
            }
        });
        if($('#myForm').valid()){
            updateCert();
        }
    });

    $('#cert_head').on('keyup', function(){
        $('.cert_head').text($(this).val());
    });
    $('#above_stu_name').on('keyup', function(){
        $('.above_stu_name').text($(this).val());
    });
    $('#below_stu_name').on('keyup', function(){
        $('.below_stu_name').text($(this).val());
    });
    $('#above_course').on('keyup', function(){
        $('.above_course').text($(this).val());
    });
    $('#issued_on').on('keyup', function(){
        $('.issued_on').text($(this).val());
    });
    $('#expires_on').on('keyup', function(){
        $('.expires_on').text($(this).val());
    });
    // $('#cert_id').on('keyup', function(){
    //     $('.cert_id').text($(this).val());
    // });
    $('#note').on('keyup', function(){
        $('.note .new_cont_1').text($(this).val());
        $('.note .new_cont').text($(this).val());
    });

    $('#font_style').change(function(){
        $('.cert_template').removeClass('font_lato font_montserrat font_open_sans font_dancing');
        $('.cert_template').addClass('font_'+$('#font_style').val());
    });

    $('#primary_color').change(function(){
        var val = $('#primary_color').val();
        var dark = colorShade(val, -80);
        var rgb = hexToRgb(val);
        var rgba = [rgb.slice(0, rgb.length-1), ", 0.6", rgb.slice(rgb.length-1)].join('');
        var rgba1 = [rgb.slice(0, rgb.length-1), ", 0.4", rgb.slice(rgb.length-1)].join('');
        var rgba2 = [rgb.slice(0, rgb.length-1), ", 0.05", rgb.slice(rgb.length-1)].join('');
        rgba = rgba.replace('rgb','rgba');
        rgba1 = rgba1.replace('rgb','rgba');
        rgba2 = rgba2.replace('rgb','rgba');

        $('.primary_color').css('color', val);
        $('.primary_color_dark').css('color', dark);
        $('.secondary_color_light').css('color', rgba);
        $('.primary_color_border').css('border-color', val);
        $('.primary_color_border_top').css('border-top', `203px solid ${val}`);
        $('.new-temp.primary_color').css('border', "2px "+ rgba + " solid");
        $('.primary_color_dark_border').css('border', "3px "+ dark + " solid");
        $('.primary_color_background').css('background', val);
        $('.primary_color_light_background').css('background-color', rgba2);
    });

    $('#secondary_color').change(function(){
        var val = $('#secondary_color').val();
        var rgb = hexToRgb(val);
        var dark = colorShade(val, -80);
        var rgba = [rgb.slice(0, rgb.length-1), ", 0.6", rgb.slice(rgb.length-1)].join('');
        var rgba1 = [rgb.slice(0, rgb.length-1), ", 0.4", rgb.slice(rgb.length-1)].join('');
        rgba = rgba.replace('rgb','rgba');
        rgba1 = rgba1.replace('rgb','rgba');

        $('.secondary_color').css('color', val);
        $('.secondary_color_dark').css('color', dark);
        $('.secondary_color_light').css('color', rgba);
        $('.secondary_color_lighter').css('color', rgba1);
        $('.secondary_color_lighter_border').css('border', "3px "+ rgba1 + " solid");
    });
});

$(".chosen-select").chosen();

function show_hide_divs(temp_id){
    if(temp_id == 1){
        $('.cert_cntnt').show();
        $('#above_stu_name_div').hide();
        $('#below_course_div').hide();
    } else if(temp_id == 2){
        $('.cert_cntnt').show();
        $('#below_stu_name_div').hide();
        $('#below_course_div').hide();
    } else if(temp_id == 3){
        $('.cert_cntnt').show();
        $('#above_course_div').hide();
        $('#below_stu_name_div').hide();
    } else if(temp_id == 4){
        $('.cert_cntnt').show();
        $('#below_stu_name_div').hide();
        $('#below_course_div').hide();
    }
}
function fill_data(temp_id){
    if(temp_id == 1){
        $('.cert_head').text("@lang('client_site.this_is_to_cerify_that')");
        $('#cert_head').val("@lang('client_site.this_is_to_cerify_that')");
        $('.above_course').text("@lang('client_site.above_course_name')");
        $('#above_course').val("@lang('client_site.above_course_name')");
    } else if(temp_id == 2){
        $('.cert_head').text("@lang('client_site.certificate_heading')");
        $('#cert_head').val("@lang('client_site.certificate_heading')");
        $('.above_stu_name').text("@lang('client_site.this_is_to_cerify_that')");
        $('#above_stu_name').val("@lang('client_site.this_is_to_cerify_that')");
        $('.above_course').text("@lang('client_site.above_course_name')");
        $('#above_course').val("@lang('client_site.above_course_name')");
    } else if(temp_id == 3){
        $('.cert_head').text("@lang('client_site.certificate_heading')");
        $('#cert_head').val("@lang('client_site.certificate_heading')");
        $('.above_stu_name').text("@lang('client_site.above_student_name')");
        $('#above_stu_name').val("@lang('client_site.above_student_name')");
        $('.below_course').text("@lang('client_site.below_course_name')");
        $('#below_course').val("@lang('client_site.below_course_name')");
    } else if(temp_id == 4){
        $('.cert_head').text("@lang('client_site.certificate_heading')");
        $('#cert_head').val("@lang('client_site.certificate_heading')");
        $('.above_stu_name').text("@lang('client_site.above_student_name')");
        $('#above_stu_name').val("@lang('client_site.above_student_name')");
        $('.above_course').text("@lang('client_site.above_course_name')");
        $('#above_course').val("@lang('client_site.above_course_name')");
    }
}
function storeCert(){
    if($('.below_course_div').find('img').length != 0){
        html = $('.below_course_div').html();
        $('.below_course_div').find('img').remove();
        $('.below_course_div').prepend("&lcub;&lcub;Assinatura&rcub;&rcub;");
        console.log(html);
    } else {
        console.log("No image in signature");
    }
    // $('.below_course_div').text("@lang('client_site.below_course_name')");
    var reqData = {
        'jsonrpc' : '2.0',
        '_token' : '{{csrf_token()}}',
        'params' : {
            'template_number': $('#template_number').val(),
            'name': $('#cert_name').val(),
            'cert_head': $('#cert_head').val(),
            'above_stu_name': $('#above_stu_name').val(),
            'below_stu_name': $('#below_stu_name').val(),
            'above_course': $('#above_course').val(),
            'below_course': $('#below_course').val(),
            'issued_on': $('#issued_on').is(':checked') ? 'Y' : 'N',
            'expires_on': $('#expires_on').is(':checked') ? 'Y' : 'N',
            'expires_by': $('#expires_by').val(),
            // 'cert_id': $('#cert_id').val(),
            'note': $('#note').val(),
            'primary_color': $('#primary_color').val(),
            'secondary_color': $('#secondary_color').val(),
            'font_style': $('#font_style').val(),
            'background': $('#background').val() != "" ? $('#background')[0].files[0] : null,
            'content': $('.selected_temp').html(),
        }
    };
    $.ajax({
        url: "{{ route('store.certificate.template') }}",
        method: 'post',
        dataType: 'json',
        data: reqData,
        async: false,
        success: function(res){
            console.log(res);
            template_id = res.template.id;
            if(html != "") $('.below_course_div').html(html);
            html = "";
            console.log("TEMP ID: "+template_id);
        },
        error: function(err){
            console.log(err);
        },
    });
}
function updateCert(){

    if($('.below_course_div').find('img').length){
        html = $('.below_course_div').html();
        $('.below_course_div').find('img').remove();
        $('.below_course_div').prepend("&lcub;&lcub;Assinatura&rcub;&rcub;");
        console.log($('.below_course_div').html());
    }

    var style = "";
    var uploadFormData = new FormData();
    uploadFormData.append("jsonrpc", "2.0");
    uploadFormData.append("_token", '{{csrf_token()}}');
    uploadFormData.append('template_number', $('#template_number').val());
    uploadFormData.append('name', $('#cert_name').val());
    uploadFormData.append('cert_head', $('#cert_head').val());
    uploadFormData.append('above_stu_name', $('#above_stu_name').val());
    uploadFormData.append('below_stu_name', $('#below_stu_name').val());
    uploadFormData.append('above_course', $('#above_course').val());
    uploadFormData.append('below_course', $('#below_course').val());
    uploadFormData.append('issued_on', $('#issued_on').is(':checked') ? 'Y' : 'N');
    uploadFormData.append('expires_on', $('#expires_on').is(':checked') ? 'Y' : 'N');
    uploadFormData.append('expires_by', $('#expires_by').val());
    // uploadFormData.append('cert_id', $('#cert_id').val());
    uploadFormData.append('note', $('#note').val());
    uploadFormData.append('primary_color', $('#primary_color').val());
    uploadFormData.append('secondary_color', $('#secondary_color').val());
    uploadFormData.append('font_style', $('#font_style').val());
    uploadFormData.append('background', $('#background').val() != "" ? $('#background')[0].files[0] : null);
    if($('.template'+ $('#template_number').val() ).attr('style').length > 140){
        style = $('.template'+ $('#template_number').val() ).attr('style');
        $('.template'+ $('#template_number').val() ).attr('style', "width:700px;");
    }
    uploadFormData.append('content', $('.selected_temp').html());

    $.ajax({
        url: "{{url('/')}}/professional-certificate-template-update/"+template_id,
        method: 'post',
        data: uploadFormData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function(res){
            console.log(res);
            if(res.status == "success") {
                toastr.success(res.message);
                template_id = res.template.id;
                console.log("TEMP ID: "+template_id);
                $('#background').val('');
                $('.below_course_div').html(html);
                $('.template'+ $('#template_number').val() ).attr('style', style);
            } else toastr.error(res.message);
        },
        error: function(err){
            console.log(err);
        },
    });
}

let hexToRgb= c=> `rgb(${c.match(/\w\w/g).map(x=>+`0x${x}`)})`;

const colorShade = (col, amt) => {
  col = col.replace(/^#/, '')
  if (col.length === 3) col = col[0] + col[0] + col[1] + col[1] + col[2] + col[2]

  let [r, g, b] = col.match(/.{2}/g);
  ([r, g, b] = [parseInt(r, 16) + amt, parseInt(g, 16) + amt, parseInt(b, 16) + amt])

  r = Math.max(Math.min(255, r), 0).toString(16)
  g = Math.max(Math.min(255, g), 0).toString(16)
  b = Math.max(Math.min(255, b), 0).toString(16)

  const rr = (r.length < 2 ? '0' : '') + r
  const gg = (g.length < 2 ? '0' : '') + g
  const bb = (b.length < 2 ? '0' : '') + b

  return `#${rr}${gg}${bb}`
}
</script>

<form method="post" id="destroy">
    {{ csrf_field() }}
    {{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
    var confirm = window.confirm('Do you want to delete this question?');
    if(confirm){
        $("#destroy").attr('action',val);
        $("#destroy").submit();
    }
 }
</script>
<style>
    .error{
        color: red !important;
    }
</style>

@endsection
