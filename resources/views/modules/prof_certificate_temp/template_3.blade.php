<!-- <div class="cert_template font_montserrat template3" style="width: 700px; height: 550px;">
    <div class="third-templte">
        <p class="cert_head primary_color"></p>
        <h4 class="course_name primary_color">&lcub;&lcub;@lang('client_site.course_name')&rcub;&rcub;</h4>
        <p class="below_course secondary_color"></p>
        <div class="templete3-cer primary_color">
            <p class="above_stu_name secondary_color"></p>
            <h4 class="stu_name primary_color">&lcub;&lcub;@lang('client_site.student_name')&rcub;&rcub;</h4>
        </div>
        <div class="note"> <p class="new_cont secondary_color"></p></div>
        <div class="below_course_div secondary_color font_w_600 new_sign">
            @if(@Auth::user()->signature)
                <img src="{{ url('storage/app/public/uploads/signature/'.@Auth::user()->signature) }}" height="50px"/>
            @else
                <p>{{ @Auth::user()->nick_name ?? @Auth::user()->name }}</p>
            @endif
            <p class="prof_name">{{ @Auth::user()->nick_name ?? @Auth::user()->name }}</p>
        </div>
        <div class="cert_det content__bottom">
            <small class="issued_on"></small>
            <small class="expires_on"></small>
        </div>
    </div>
</div> -->

<div class="cert_template font_montserrat template3 template3_new primary_color_border" style="width: 700px; height: 550px;">
        <em class="bg_thard primary_color_border_top"></em>
        <em class="bg_thard2 primary_color_border_top"></em>
        <span class="carti_img"><img src="{{ url('/public/frontend/images/carti_fac.png') }}" alt=""></span>
    <div class="third-templte">
        <p class="cert_head secondary_color"></p>
        <h4 class="course_name primary_color">&lcub;&lcub;@lang('client_site.course_name')&rcub;&rcub;</h4>
        <p class="below_course secondary_color"></p>
        <div class="templete3-cer primary_color_border">
            <p class="above_stu_name secondary_color"></p>
            <h4 class="stu_name primary_color">&lcub;&lcub;@lang('client_site.student_name')&rcub;&rcub;</h4>
        </div>
        <div class="note"> <p class="new_cont secondary_color"></p></div>
        <div class="below_course_div secondary_color font_w_600 new_sign">
            @if(@Auth::user()->signature)
                <img src="{{ url('storage/app/public/uploads/signature/'.@Auth::user()->signature) }}" height="50px"/>
            @else
                <p>&lcub;&lcub;Assinatura&rcub;&rcub;</p>
            @endif
            <p class="prof_name secondary_color">{{ @Auth::user()->nick_name ?? @Auth::user()->name }}</p>
        </div>
        <div class="cert_det content__bottom">
            <small class="issued_on secondary_color"></small>
            <small class="expires_on secondary_color"></small>
        </div>
    </div>
</div>