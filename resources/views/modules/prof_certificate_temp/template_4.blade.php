<!-- <div class="cert_template font_montserrat template4" style="width: 700px; height: 550px;">
    <div class="four-templte">
        <h2 class="cert_head new-card-head four_cert font_w_700 font_w_800 primary_color"></h2>
        <p class="above_stu_name secondary_color"></p>
        <h4 class="stu_name new_4 primary_color">&lcub;&lcub;@lang('client_site.student_name')&rcub;&rcub;</h4>
        <p class="above_course secondary_color new-corss"></p>
        <h4 class="course_name primary_color">&lcub;&lcub;@lang('client_site.course_name')&rcub;&rcub;</h4>
        <div class="note"> <p class="new_cont secondary_color"></p></div>
        <div class="below_course_div secondary_color font_w_600 new_sign">
            @if(@Auth::user()->signature)
                <img src="{{ url('storage/app/public/uploads/signature/'.@Auth::user()->signature) }}" height="50px"/>
            @else
                <p>{{ @Auth::user()->nick_name ?? @Auth::user()->name }}</p>
            @endif
            <p class="prof_name">{{ @Auth::user()->nick_name ?? @Auth::user()->name }}</p>
        </div>
        <div class="cert_det content__bottom">
            <small class="issued_on"></small>
            <small class="expires_on"></small>
        </div>
    </div>
</div> -->

<div class="cert_template font_montserrat template4 template4_new" style="width: 700px; height: 550px;">
    <div class="four-templte">
        <h2 class="cert_head new-card-head four_cert font_w_700 font_w_800 primary_color"></h2>
        <p class="above_stu_name secondary_color"></p>
        <h4 class="stu_name new_4 primary_color">&lcub;&lcub;@lang('client_site.student_name')&rcub;&rcub;</h4>
        <p class="above_course secondary_color new-corss"></p>
        <h4 class="course_name primary_color">&lcub;&lcub;@lang('client_site.course_name')&rcub;&rcub;</h4>
        <div class="note"> <p class="new_cont secondary_color"></p></div>
        <div class="below_course_div secondary_color font_w_600 new_sign">
            @if(@Auth::user()->signature)
                <img src="{{ url('storage/app/public/uploads/signature/'.@Auth::user()->signature) }}" height="50px"/>
            @else
                <p>&lcub;&lcub;Assinatura&rcub;&rcub;</p>
            @endif
            <p class="prof_name secondary_color">{{ @Auth::user()->nick_name ?? @Auth::user()->name }}</p>
        </div>
        <div class="cert_det content__bottom">
            <small class="issued_on secondary_color"></small>
            <small class="expires_on secondary_color"></small>
        </div>
    </div>
</div>