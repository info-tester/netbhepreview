@extends('layouts.app')
@section('title')
    @lang('site.post') @lang('site.details')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.view_imported_tools')</h2>

   

        <div class="bokcntnt-bdy no-margin-top">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="blog-right details-right">
                    <div class="blog-box">
                        
                        <div class="blog-dtls">
                            
                            <div class="from-field">
                                <div class="weicome">

                                    <h3><strong> @lang('site.title'): </strong>{{ $details->title }}</h3>
                                    
                                    <div class="clearfix"></div>
                                    
                                    <div class="clearfix"></div>
                                    <!-- <strong>@lang('site.Assign_date') : </strong> {{ @$details->getUsertoolsData->contract_start_date }} - {{ @$details->getUsertoolsData->contract_end_date }} -->
                                    <div class="clearfix"></div>

                                    
                                    <?php
                                        $certificatedesc = $details['certificate_description'];
                                        
                                    ?>
                                   
                                    <?php
                                        foreach($profdata as $key => $val){       
                                            $certificatedesc = str_replace('__'.$key.'__',$val,$certificatedesc);            
                                        }
                                    ?>

                                    <strong>@lang('site.description') : </strong> <?= $certificatedesc ?>
                                    
                                    
                                </div>                       
                            </div>

                            
                        </div>
                    </div>
                    
                </div>
            </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
    <script>
        $(document).ready(function(){
            $('#myForm').validate();     
        });
    </script>

@endsection