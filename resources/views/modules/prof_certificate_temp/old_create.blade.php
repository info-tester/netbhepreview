@extends('layouts.app')
@section('title')
    {{-- @lang('site.add_new_blog') --}}
    Certificate Template
@endsection
@section('style')
@include('includes.style')
<style type="text/css">
    div.mce-edit-area {
        background:
        #FFF;
        filter: none;
        min-height: 317px;
    }
    .chck_eml_rd{
        color: red;
        font-weight: bold;
        font-size: 14px;
    }

    .tooltip {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted #ccc;
        color: #006080;
        opacity: 1 !important;
    }

    .tooltip .tooltiptext {
        visibility: hidden;
        position: absolute;
        width: 250px;
        background-color: #555;
        color: #fff;
        text-align: center;
        padding: 5px;
        border-radius: 6px;
        z-index: 1;
        opacity: 0;
        transition: opacity 0.3s;
        margin-left: 10px
    }

    .tooltip-right {
        top: 0px;
        left: 125%;
    }

    .tooltip-right::after {
        content: "";
        position: absolute;
        top: 10%;
        right: 100%;
        margin-top: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: transparent #555 transparent transparent;
    }

    .tooltip:hover .tooltiptext {
        visibility: visible;
        opacity: 1;
    }
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.certificate_template')</h2>

         
        <div class="bokcntnt-bdy">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">

                    <form id="myform" method="post" action="{{ route('store.certificate.template') }}">
                        @csrf
                        <div class="form_body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
                                    <div class="your-mail">
                                        <label for="exampleInputEmail1" class="personal-label">@lang('site.title') *</label>
                                        <input type="text" name="title" class="personal-type required" id="title" placeholder="@lang('site.title')" >
                                    </div>
                                </div>
                                <div class="clearfix"></div>


                                {{-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1" class="personal-label">Description *</label>
                                        <textarea name="desc" id="desc" placeholder="Description" style="width:100%" class="required personal-type99" rows="7"></textarea>
                                    </div>
                                </div> --}}


                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="personal-label" for="exampleInputEmail1">
                                            @lang('site.description') *
                                            <div class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">{{__('site.contract_temp_desc_tooltip')}}</span>
                                            </div>
                                        </label>
                                        <div class="clearfix"></div>
                                        <textarea name="certificate_desc" id="desc" rows="10" placeholder="Description" style="width:100%" class="required desc"></textarea>
                                    </div>
                                    <div class="clearfix"></div>
                                    <p class="error_1" id="cntnt"></p>
                                    <div class="clearfix"></div>
                                    <strong>Note: Please use this paramete </strong></br>
                                    <strong>
                                    Note: @lang('site.parameter_use_for_user'). </br>
                                        <!-- {{implode(', ', $parameters)}} -->
                                        __COURSE__    >> @lang('site.name_of_course')<br>
                                        __DURATION__  >> @lang('site.duration_of_course')<br>
                                        __NAME__      >> @lang('site.name_of_student')<br>
                                        __PROF__      >> @lang('site.name_of_prof')<br>
                                        __SIGN__      >> @lang('site.sign_of_prof')<br>
                                    </strong>
                                    </br>
                                    <!-- <strong>
                                        Note: @lang('site.parameter_use_for_prof').</br>
                                        __NAME_COACH__ ,
                                        __CITY_COACH__  ,
                                        __PRICE__      ,

                                        __MONTH__   ,
                                        __DAY__  ,
                                        __YEAR__,

                                        __INSTALLMENT__,
                                        __NEIGHBORHOOD__,
                                        __ESTADO_CIVIL__,

                                    </strong> -->
                                </div>


                                <div class="clearfix"></div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="submit-login add_btnm">
                                        <input value="@lang('site.add')" type="submit" class="login_submitt">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
<style type="text/css">
    .upldd {
    display: block;
    width: auto;
    border-radius: 4px;
    text-align: center;
    background: #9caca9;
    cursor: pointer;
    overflow: hidden;
    padding: 10px 15px;
    font-size: 15px;
    color: #fff;
    cursor: pointer;
    float: left;
    margin-top: 15px;
    font-family: 'Poppins', sans-serif;
    position: relative;
}
.upldd input {
    position: absolute;
    font-size: 50px;
    opacity: 0;
    left: 0;
    top: 0;
    width: 200px;
    cursor: pointer;
}
.upldd:hover{
    background: #1781d2;
}

</style>

    <script>
    var parameters = {!! json_encode($parameters) !!};
    
    $(document).ready(function(){
        $("#myform").validate();
        tinyMCE.init({
            mode : "specific_textareas",
            editor_selector : "desc",
            height: '320px',
            plugins: [
              'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
              'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
              'save table contextmenu directionality emoticons template paste textcolor'
            ],
            relative_urls : false,
            remove_script_host : false,
            convert_urls : true,
            toolbar: 'insertfile undo redo | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor emoticons',
                images_upload_url: '{{ URL::to('storage/app/public/uploads/content_image/') }}',
                images_upload_handler: function(blobInfo, success, failure) {
                    var formD = new FormData();
                    formD.append('file', blobInfo.blob(), blobInfo.filename());
                    formD.append( "_token", '{{csrf_token()}}');
                    $.ajax({
                        url: '{{route("admin.artical.img.upload")}}',
                        data: formD,
                        type: 'POST',
                        contentType: false,
                        cache: false,
                        processData:false,
                        dataType: 'JSON',
                        success: function(jsn) {
                            if(jsn.status == 'ERROR') {
                                failure(jsn.error);
                            } else if(jsn.status == 'SUCCESS') {
                                success(jsn.location);
                            }
                        }
                    });
                },
            });
            $("#myform").submit(function (event) {
                if(tinyMCE.get('desc').getContent()==""){
                    event.preventDefault();
                    $("#cntnt").html("Description field is required").css('color','red');
                } else {
                    flag = 0;
                    content = tinyMCE.get('desc').getContent();
                    arr = content.match(/__([A-Z]+)__/g);
                    console.log(arr);
                    $(arr).each(function(i, param){
                        if (parameters.indexOf(param) == -1) {
                            flag = 1;
                            console.log(param);
                        }
                    });
                    if(flag == 0) {
                        $("#cntnt").html("");
                    } else {
                        event.preventDefault();
                        $("#cntnt").html("Please use only the parameters mentioned below.").css('color','red');
                    }
                }
            });
    });

</script>
<style>
    .error{
        color: red !important;
    }
</style>
@endsection
