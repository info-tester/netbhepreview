@extends('layouts.app')
@section('title')
    @lang('site.welcome_to_home_page')
@endsection
@section('style')
    @include('includes.style')
    <style>
        .with-anc a {
            color: inherit;
            /* text-decoration: underline; */
        }
    </style>
@endsection

@section('scripts')
	@include('includes.scripts')
@endsection

@section('header')
	@include('includes.header')
@endsection


@section('content')
<section class="banner_area" style="/* background: none; */">
    {{-- <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            @foreach ($banners as $k => $banner)
                <div class="container">
                    <div class="row">
                        <div class="carousel-item {{ $k == 0 ? 'active' : '' }}">
                            <div class="banner-contant">
                                <h1>{{ $banner->heading }}</h1>
                                <p>{!! $banner->description !!}</p>
                                <form name="myForm" action="{{ route('srch.pst') }}" method="post">
                                    @csrf
                                    <div class="form_custom">
                                    <input type="text" class="search-type" placeholder="Digite sua palavra-chave aqui ..." name="keyword" value="{{ @$key['keyword'] }}">
                                    <span><img src="{{URL::to('public/frontend/images/search.png')}}" alt=""></span>
                                    <span><img src="{{URL::to('public/frontend/ images/banner.jpg')}}" alt=""></span>


                                    <button class="submit-search" type="submit">@lang('client_site.search')</button>
                                    </div>
                                </form>
                            </div>
                        </div>




                    </div>
                </div>
            @endforeach
        </div>
    </div> --}}
    <div id="demo" class="carousel slide" data-ride="carousel">


        <!-- Indicators -->
        <ul class="carousel-indicators">
          <li data-target="#demo" data-slide-to="0" class="active"></li>
          <li data-target="#demo" data-slide-to="1"></li>
          <li data-target="#demo" data-slide-to="2"></li>
        </ul>
        <div class="carousel-inner">
            @foreach ($banners as $k => $banner)
                <!-- The slideshow -->
                @php
                    $class = "";
                    if(@$banner->alignment == 'L') $class = "banner-contant-left";
                    else if(@$banner->alignment == 'R') $class = "banner-contant-right";
                @endphp
                <div class="carousel-item {{ $k == 0 ? 'active' : '' }}">
                    <div class="header_bannerImgMain" style="background-image: url({{ URL::to('storage/app/public/uploads/content_images/' . $banner->media) }})">
                        <div class="container">
                            <div class="item_containerBox">
                        <div class="banner-contant banner_item_form_Box {{$class}}">
                            <h1 class="with-anc" style="color: {{$banner->text_color}} !important;">{!! $banner->heading !!}</h1>
                            <p class="with-anc" style="color: {{$banner->text_color}} !important;">{!! $banner->description !!}</p>
                            <form name="myForm" action="{{ route('srch.pst') }}" method="post">
                                @csrf
                                <div class="form_custom">
                                <input type="text" class="search-type" placeholder="Digite sua palavra-chave aqui ..." name="keyword" value="{{ @$key['keyword'] }}">
                                <span><img src="{{URL::to('public/frontend/images/search.png')}}" alt=""></span>
                                <button class="submit-search" type="submit">@lang('client_site.search')</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                    </div>
                </div>
            @endforeach
        </div>

        <!-- Left and right controls -->
        <a class="carousel-control-prev" href="#demo" data-slide="prev">
          <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#demo" data-slide="next">
          <span class="carousel-control-next-icon"></span>
        </a>
      </div>

</section>

<section class="how-work">
    <div class="container">
        <div class="row">
            <div class="work-white">
                <div class="page-h2">
                    <h2>{{ @$content[0]->title }}</h2>
                    {!! @$content[0]->description !!}
                </div>
                <div class="all-work">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-12">
                            <div class="work-box">
                                <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$content[0]->image_1) }}" alt="">
                                <span>{{ @$content[0]->heading_1 }}</span>
                                <p>{!! @$content[0]->description_1 !!}</p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12">
                            <div class="work-box">
                                <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$content[0]->image_2) }}" alt="">
                                <span>{{ @$content[0]->heading_2 }}</span>
                                <p>{!! @$content[0]->description_2 !!}</p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12">
                            <div class="work-box">
                                <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$content[0]->image_3) }}" alt="">
                                <span>{{ @$content[0]->heading_3 }}</span>
                                <p>{!! @$content[0]->description_3 !!}</p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12">
                            <div class="work-box">
                                <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$content[0]->image_4) }}" alt="">
                                <span>{{ @$content[0]->heading_4 }}</span>
                                <p>{!! @$content[0]->description_4 !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="category">
    <div class="container">
        <div class="row">
            <div class="page-h2">
                <h2>{{ $content[1]->title ?? __('client_site.browse_by_category') }}</h2>
                <p>
                    {!! @$content[1]->description ?? '' !!}
                </p>
            </div>
            <div class="all-categories">
                <div class="row">
                    @foreach(@$cat as $ct)
                        <div class="col-lg-3 col-md-6 col-sm-12">
                            <div class="categori-box">
                               <a href="{{ route('srch.slug',['slug'=>@$ct->slug]) }}"><img src="{{URL::to('storage/app/public/category_images').'/'.@$ct->image}}" alt="">
                               <p>{{ @$ct->name }}</p></a>
                            </div>
                        </div>
                    @endforeach


                    <div class="col-lg-12 text-center">
                        <a class="view-all" href="{{ route('browse.categories') }}">@lang('client_site.view_all')<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
@php
    $productnotshow='N';
@endphp
@if(count(@$products) > 0 && $productnotshow!='Y')
<section class="product-area">
    <div class="container">
        <div class="row">
            <div class="page-h2">
                <h2>{{ @$content[6]->title }}</h2>
                <p id="prod-desc" data-desc="{{ @$content[6]->description }}"></p>
            </div>
            <div class="all-product">
                <div id="owl-demo-6" class="owl-carousel owl-theme">
                    @php
                        $cartProduct=getAllCart();
                    @endphp
                @foreach(@$products as $product)
                <div class="item">
                    <div class="product-box">
                        <div class="product-cover-image image_resize">
                            <a href="{{route('product.details',@$product->slug)}}"><img src="{{ URL::to('storage/app/public/uploads/product_cover_images').'/'.@$product->cover_image }}"></a>
                        </div>
                        <div class="product-dtls">
                            <div class="product-intro">
                                <a href="{{route('product.details',@$product->slug)}}">
                                    {{-- <h3>
                                        {{ @$product->title }}
                                    </h3> --}}
                                    <h3>
                                        @if(strlen(@$product->title) < 50)
                                        {{ @$product->title }}
                                        @else {{substr(@$product->title, 0, 47 ) . '...'}}
                                        @endif
                                    </h3>
                                </a>
                                <p><i class="fa fa-tag" aria-hidden="true"></i> {{ @$product->category->category_name }} <i class="fa fa-angle-right" aria-hidden="true"></i> {{ @$pro->subCategory->name }}</p>
                                <p><i class="fa fa-user" aria-hidden="true"></i> {{ @$product->professional->name }}</p>
                            </div>
                            <div class="rrt_stll01">
                                @php $month=[ "0", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ] @endphp
                                <p class="small_dates"><strong>@lang('client_site.sale_starts_on'): </strong>
                                    {{date('d',strtotime(@$product->purchase_start_date))}}
                                    {{$month[(int)date('m',strtotime(@$product->purchase_start_date))]}}
                                    {{date('Y',strtotime(@$product->purchase_start_date))}}
                                </p>
                                <p class="small_dates">
                                    <strong>@lang('client_site.sale_ends_on'): </strong>
                                    @if(@$product->purchase_end_date)
                                        {{date('d',strtotime(@$product->purchase_end_date))}}
                                        {{$month[(int)date('m',strtotime(@$product->purchase_end_date))]}}
                                        {{date('Y',strtotime(@$product->purchase_end_date))}}
                                    @else
                                        @lang('client_site.not_specified')
                                    @endif
                                </p>
                                <p class="ttyu01 small_dates">
                                    <strong>@lang('client_site.available_from'): </strong>
                                    @if(@$product->course_start_date)
                                        {{date('d',strtotime(@$product->course_start_date))}}
                                        {{$month[(int)date('m',strtotime(@$product->course_start_date))]}}
                                        {{date('Y',strtotime(@$product->course_start_date))}}
                                    @else
                                        @lang('client_site.not_specified')
                                    @endif
                                </p>
                            </div>
                            @if(@$product->description)
                                <div class="product-more more_det">
                                    <p class="pr-desc" data-desc="{{$product->description}}"></p>
                                </div>
                            @endif
                            <div class="price-and-more price-100">
                                @if(@$product->price)
                                    <div class="price-lefts">
                                        <span>@lang('client_site.fees')</span>
                                        <p>{{@$product->discounted_price > 0 ? @$product->discounted_price : @$product->price}}</p>@if(@$pro->discounted_price > 0)
                                        <p><del>R${{@$product->price}}</del>&nbsp;{{ ((@$product->price-@$product->discounted_price)/@$product->price)*100 }}% de desconto </p>
                                        <p style="font-size: 1rem;">R${{@$product->discounted_price}}</p>
                                        @else
                                        <p style="font-size: 1rem;">R${{@$product->price}}</p>
                                        @endif
                                    </div>
                                @endif
                                @if(in_array(@$product->id , $orderedProducts))
                                <p class="text-secondary" style="font-size:1rem; float:right;">@lang('client_site.already_ordered')</p>
                                @else
                                @if(date('Y-m-d') >= date('Y-m-d', strtotime(@$product->purchase_start_date)))
                                @if(@$product->purchase_end_date && date('Y-m-d') >= date('Y-m-d', strtotime(@$product->purchase_end_date)))
                                <p class="text-secondary" style="font-size:1rem; float:right;">@lang('client_site.sale_over')</p>
                                @else
                                <a class="product-btn pull-left buyNow" href="javascript:void(0);" data-product="{{$product->id}}">{{__('site.buy_now')}}</a>
                                <a href="javascript:void(0);" class="product-btn pull-left pull-right-1 addToCart cartClass{{$product->id}}" data-product="{{$product->id}}" @if(@in_array($product->id, $cartProduct)) style="display: none" @endif> @lang('client_site.add_to_cart')</a>
                                <a href="{{route('product.cart')}}" class="product-btn pull-left pull-right-1 GoToCart{{$product->id}}" data-product="{{$product->id}}" @if(!@in_array($product->id, $cartProduct)) style="display: none" @endif>@lang('client_site.go_to_cart')</a>
                                @endif
                                @else
                                <p class="text-secondary" style="font-size:1rem; float:right;">
                                    <span style="font-size:0.7rem; text-align:center;">@lang('client_site.available_from')</span><br>
                                    @php $month=[ "0", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ] @endphp
                                    {{date('d',strtotime(@$product->purchase_start_date))}}
                                    {{$month[(int)date('m',strtotime(@$product->purchase_start_date))]}}
                                    {{date('Y',strtotime(@$product->purchase_start_date))}}
                                </p>
                                @endif
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
              </div>
            </div>
            <div class="col-lg-12 text-center">
                <a class="view-all" href="{{ route('all.product.search') }}">@lang('site.view_all_products')<i
                        class="fa fa-angle-right" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</section>
@endif

<section class="expart-banner">
    <div class="container">
        <div class="row">
            <h2>{{ @$content[3]->title }}</h2>
            <p>{{ @$content[3]->description }}</p>
            <a href="{{ route('exp.register') }}">@lang('client_site.get_started')</a>
        </div>
    </div>
</section>

<section class="different-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12">
                <div class="dif-detls">
                    <h2>{{ @$content[4]->title }}</h2>
                    <p>{{ @$content[4]->description }}</p>
                    <ul>
                        <li>
                            <span><img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$content[4]->image_1) }}" alt=""></span>
                            <h6>{{ @$content[4]->heading_1 }}</h6>
                            <p>{{ @$content[4]->description_1 }}</p>
                        </li>
                        <li>
                            <span><img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$content[4]->image_2) }}" alt=""></span>
                            <h6>{{ @$content[4]->heading_2 }}</h6>
                            <p>{{ @$content[4]->description_2 }}</p>
                        </li>
                        <li>
                            <span><img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$content[4]->image_3) }}" alt=""></span>
                            <h6>{{ @$content[4]->heading_3 }}</h6>
                            <p>{{ @$content[4]->description_3 }}</p>
                        </li>
                        <li>
                            <span><img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$content[4]->image_4) }}" alt=""></span>
                            <h6>{{ @$content[4]->heading_4 }}</h6>
                            <p>{{ @$content[4]->description_4 }}</p>
                        </li>
                    </ul>
                    <div class="learn-btn-div">
                        <a class="learn-btn" href="{{ route('frontend.about') }}">@lang('client_site.learn_more')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="differ-image">
        <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$content[4]->image) }}" alt="">
    </div>
    <div class="right-white"></div>
</section>

<section class="guidance-area">
    <div class="container">
        <div class="row">
            <div class="page-h2">
                <h2>{{ @$content[5]->title }}</h2>
                <p>{{ @$content[5]->description }}</p>
            </div>
            <div class="all-guidance">
                <div id="owl-demo-1" class="owl-carousel owl-theme">
                @foreach(@$user as $us)
                <div class="item">
                    <div class="guiede-box">
                        <div class="guide-image"><img src="{{ @$us->profile_pic ? URL::to('storage/app/public/uploads/profile_pic').'/'.@$us->profile_pic: URL::to('public/frontend/images/no_img.png') }}"></div>
                        <div class="guide-dtls">
                            <div class="guide-intro">
                                <h3>
                                    {{ @$us->nick_name ? @$us->nick_name : @$us->name }}
                                    <ul>
                                        {!! getStars($us->avg_review) !!}
                                        <li>({{ $us->total_review ?? 0 }})</li>
                                    </ul>
                                </h3>
                                <p>{!! @$us->specializationName->name ? $us->specializationName->name . ', ' . $us->crp_no : '&nbsp;' !!}</p>
                            </div>
                            <div class="guide-more">
                                <ul>
                                    @if(@$us->rate_price)
                                        <li>
                                            <span>@lang('client_site.fees')</span>
                                            <p>R${{ @$us->rate_price }} / @if(@$us->rate=="M") "Minute" @else @lang('client_site.hour') @endif</p>
                                        </li>
                                    @endif
                                    @if(@$us->userLanguage->name!="")
                                        <li>
                                            <span>@lang('client_site.speaks') </span>
                                            <p>{{ @$us->userLanguage->name }}</p>
                                        </li>
                                    @endif
                                </ul>
                                <a class="guide-btn pull-left ssnbtnslg" href="javascript:void(0);" data-slug="{{ @$us->slug }}">@lang('client_site.request_a_session') </a>
                                <a class="guide-btn pull-right" href="{{ route('pblc.pst',['slug'=>@$us->slug]) }}"><!-- @lang('client_site.view_all') --> Ver perfil </a>
                            </div>
                        </div>
                    </div>
                </div>
               @endforeach
              </div>
            </div>
        </div>
    </div>
</section>

<section class="testimonial-area">
    <div class="container">
        <div class="row">
            <div class="page-h2">
                <h2>{{ @$content[2]->title }}</h2>
                <p>{{ @$content[2]->description }}</p>
                {{-- <h2>{{ $testiContent->title }}</h2>
                <p>{{ $testiContent->description }}</p> --}}
            </div>
            <div class="all-testi">
                <div id="owl-demo-2" class="owl-carousel owl-theme">
                @foreach(@$testi as $tt)
                <div class="item">
                  <div class="testi-box">
                    <div class="testi-image-area">
                        <span>
                            @if(@$tt->image)
                                <img src="{{ url('storage/app/public/testimonial/'.@$tt->image) }}">
                            @else
                                <img src="{{URL::to('public/frontend/images/test1.png')}}" alt="">
                            @endif

                        </span>
                    </div>
                    <div class="testi-dtls">
                        <span class="coute"><img src="{{URL::to('public/frontend/images/coute.png')}}" alt=""></span>
                        <h4>{{@$tt->name}}</h4>
                        <h5>{{@$tt->location}}</h5>
                        <p>{!!strip_tags(nl2br(@$tt->comment), '<br>')!!}</p>
                        {{-- <h6>@lang('client_site.posted_on') {{date('Y-m-d', strtotime(@$tt->created_at))}}</h6> --}}
                    </div>
                  </div>
                </div>
                @endforeach
              </div>
            </div>
        </div>
    </div>
</section>

<section class="guidance-area">
    <div class="container">
        <div class="row">
            <div class="page-h2">
                <h2>{{__('site.popular_blogs')}}</h2>
                <p>{{__('site.popular_blog_content')}}</p>
            </div>
            <div class="all-blogss">
                <div class="row">
                    @foreach ($blog as $item)
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="blog-box blog-box-make blog-home">
                            <div class="blog-image">
                                <img src="{{URL::to('storage/app/public/uploads/blog_image/'.@$item->image)}}" alt="">
                                <span class="blog-ribbon">Health &amp; Wellness</span>
                            </div>
                            <div class="blog-dtls">
                                <h4><a href="{{route('blog.frontend.details',['slug'=>$item->slug])}}">{{@$item->title}}</a></h4>
                                <ul class="blog-post">
                                    <li><img src="{{URL::to('public/frontend/images/post-1.png')}}" alt=""> {{__('site.author')}} : @if($item->posted_by != 0){{ @$item->postedBy->name }} @else Netbhe  @endif</li>
                                    <li><img src="{{URL::to('public/frontend/images/post-2.png')}}" alt="">{{ Date::parse(@$item->post_date)->format('jS M, Y')}}</li>
                                    <li><img src="{{URL::to('public/frontend/images/post-3.png')}}" alt=""> {{$item->blogCategoryName->name}}</li>
                                </ul>
                                <p><?= substr(strip_tags(@$item->desc),0,100) ?>...</p>
                                <a class="blog-more" href="{{route('blog.frontend.details',['slug'=>$item->slug])}}">{{__('site.view_blog')}}</a>
                                <ul class="review">
                                    <li><a href="#"><img src="{{URL::to('public/frontend/images/blog-icon1.png')}}" alt=""> {{ count($item->blogComments) }} @lang('site.comments')</a></li>
                                    <li><a href="#"><img src="{{URL::to('public/frontend/images/blog-icon2.png')}}" alt=""> {{$item->view_count}} {{__('site.view')}}</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    @endforeach

                    {{-- <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="blog-box blog-box-make">
                            <div class="blog-image">
                                <img src="images/blog2.png" alt="">
                                <span class="blog-ribbon">Health &amp; Wellness</span>
                            </div>
                            <div class="blog-dtls">
                                <h4><a href="blog-details.html">This is Dummy Blog Post Title One</a></h4>
                                <ul class="blog-post">
                                    <li><img src="images/post-1.png" alt=""> Author : Andrews Collins</li>
                                    <li><img src="images/post-2.png" alt=""> 25th March 2019</li>
                                    <li><img src="images/post-3.png" alt=""> Legal Advisory</li>
                                </ul>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                    Ipsum has been the industry's standard dummy text ever .</p>
                                <a class="blog-more" href="blog-details.html">READ MORE</a>
                                <ul class="review">
                                    <li><a href="#"><img src="images/blog-icon1.png" alt=""> 3 Comments</a></li>
                                    <li><a href="#"><img src="images/blog-icon2.png" alt=""> 55 view</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="blog-box blog-box-make">
                            <div class="blog-image">
                                <img src="images/blog3.png" alt="">
                                <span class="blog-ribbon">Health &amp; Wellness</span>
                            </div>
                            <div class="blog-dtls">
                                <h4><a href="blog-details.html">This is Dummy Blog Post Title One</a></h4>
                                <ul class="blog-post">
                                    <li><img src="images/post-1.png" alt=""> Author : Andrews Collins</li>
                                    <li><img src="images/post-2.png" alt=""> 25th March 2019</li>
                                    <li><img src="images/post-3.png" alt=""> Legal Advisory</li>
                                </ul>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                    Ipsum has been the industry's standard dummy text ever .</p>
                                <a class="blog-more" href="blog-details.html">READ MORE</a>
                                <ul class="review">
                                    <li><a href="#"><img src="images/blog-icon1.png" alt=""> 3 Comments</a></li>
                                    <li><a href="#"><img src="images/blog-icon2.png" alt=""> 55 view</a></li>
                                </ul>
                            </div>
                        </div>
                    </div> --}}

                </div>
            </div>
        </div>
    </div>
</section>

<section class="testimonial-area" id="testimonial-area">
    <div class="container">
        <div class="row">
            <div class="page-h2">
                {{-- <h2>{{ $testiContent->title }}</h2> --}}
                <p style="background: none;">Atenção: A plataforma da Netbhe não oferece tratamento ou aconselhamento imediato para pessoas em estado de crise
                suicida. Se você perceber que está passando por uma crise ou conhece alguém com esses sintomas, ligue para 188 (CVV) ou
                acesse o site www.cvv.org.br. Em caso de emergência, procure atendimento em um hospital mais próximo.</p>
            </div>
            {{-- <div class="all-testi">
                <div id="owl-demo-2" class="owl-carousel owl-theme">
                    @foreach(@$testi as $tt)
                    <div class="item">
                        <div class="testi-box">
                            <div class="testi-image-area">
                                <span>
                                    @if(@$tt->image)
                                    <img src="{{ url('storage/app/public/testimonial/'.@$tt->image) }}">
                                    @else
                                    <img src="{{URL::to('public/frontend/images/test1.png')}}" alt="">
                                    @endif

                                </span>
                            </div>
                            <div class="testi-dtls">
                                <span class="coute"><img src="{{URL::to('public/frontend/images/coute.png')}}"
                                        alt=""></span>
                                <h4>{{@$tt->name}}</h4>
                                <h5>{{@$tt->location}}</h5>
                                <p>{!!strip_tags(nl2br(@$tt->comment), '<br>')!!}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div> --}}
        </div>
    </div>
</section>
@endsection



@section('footer')
	@include('includes.footer')
    <script type="text/javascript">
    var limit = 100;
    productCount = "{{count(@$products)}}";
    function decodeEntities(encodedString) {
        var div = document.createElement('div');
        div.innerHTML = encodedString;
        return div.textContent;
    }
    $('#prod-desc').text(decodeEntities($('#prod-desc').attr('data-desc')));
    $('.pr-desc').each(function(){
        var str = decodeEntities($(this).attr('data-desc'));
        if(str.length > limit){
            str = str.substring(0,limit) + "...";
        }
        $(this).text(str);
    });
        $('.ssnbtnslg').click(function(){
                if($(this).data('slug')!=""){
                    localStorage.removeItem('bookSlug');
                    localStorage.setItem('bookSlug', $(this).data('slug'));
                    location.href="{{ route('login') }}"
                }
        });
    var elmnt = document.getElementById("testimonial-area");
        console.log(elmnt.offsetTop);
    </script>
    <script>
        $(document).ready(function(){
            $('body').on('click', '.addToCart', function() {
            // $('.addToCart').click(function(){
                var productId = $(this).data('product');
                console.log(productId)
                var reqData = {
                    'jsonrpc': '2.0',
                    '_token': '{{csrf_token()}}',
                    'params': {
                        productId: productId,
                    }
                };
                $.ajax({
                    url: '{{ route('product.add.to.cart') }}',
                    type: 'post',
                    dataType: 'json',
                    data: reqData,
                })
                .done(function(response) {
                    console.log(response);
                    $('.cou_cart').text(response.result.cart.length);
                    $('.cartClass'+productId).css('display','none');
                    $('.GoToCart'+productId).css('display','block');
                    console.log($(this));
                })
                .fail(function(error) {
                    console.log("error", error);
                })
                .always(function() {
                    console.log("complete");
                })
            })
            $('.buyNow').click(function(){
                var productId = $(this).data('product');
                console.log(productId)
                var reqData = {
                    'jsonrpc': '2.0',
                    '_token': '{{csrf_token()}}',
                    'params': {
                        productId: productId,
                    }
                };
                $.ajax({
                    url: '{{ route('product.add.to.cart') }}',
			        type: 'post',
			        dataType: 'json',
			        data: reqData,
                })
                .done(function(response) {
                    window.location.href = '{{route('product.order.store')}} ';
                })
                .fail(function(error) {
                    console.log("error", error);
                })
                .always(function() {
                    console.log("complete");
                })
            })
        })

    </script>
@endsection
