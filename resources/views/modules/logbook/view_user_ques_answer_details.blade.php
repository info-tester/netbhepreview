@extends('layouts.app')
@section('title')
  {{-- @lang('site.view_booked_user_details') --}}
  User submit answer
@endsection
@section('style')
@include('includes.style')
<style>
   .ui-timepicker-standard {
    position: absolute;
    z-index: 9999 !important;
}
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')
<section class="bkng-hstrybdy">
    <div class="container">
        {{-- <h2>@lang('client_site.class_details')</h2> --}}
        <h2>@lang('site.Logbook_Question_Answer')</h2>
        {{-- @include('includes.professional_tab_menu') --}}
        
        <div class="bokcntnt-bdy">
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
            @include('includes.user_sidebar')
            <div class="dshbrd-rghtcntn">

                <div class="from-field">
                    <div class="weicome manage-designs"> 
                        <h3><strong>Date <b>:</b> </strong> {{ date('Y-m-d', strtotime($logbook_date->ans_date))  }}</h3>
                    </div>                       
                </div>
                 

                <div class="col-md-12 view-page coach_tools">

                    @foreach(@$logbook_que_ans_list as $details)                       
                        
                        <p><strong>{{ $details->question }}:</strong></p>
                        <p>{{ $details->answer }}</p>
                       
                    @endforeach    

                	

                </div>

                
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
<link href="{{ URL::to('public/frontend/css/calender.css') }}" rel="stylesheet" type="text/css">
<script src="{{ URL::to('public/frontend/js/jquery-ui.js') }}"></script>


@endsection