@extends('layouts.app')
@section('title')
    {{-- @lang('site.add_new_blog') --}}
    Logbook
@endsection
@section('style')
@include('includes.style')
<style type="text/css">
    div.mce-edit-area {
        background:
        #FFF;
        filter: none;
        min-height: 317px;
    }
    .chck_eml_rd{
        color: red;
        font-weight: bold;
        font-size: 14px;
    }

    .tooltip {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted #ccc;
        color: #006080;
        opacity: 1 !important;
    }

    .tooltip .tooltiptext {
        visibility: hidden;
        position: absolute;
        width: 250px;
        background-color: #555;
        color: #fff;
        text-align: center;
        padding: 5px;
        border-radius: 6px;
        z-index: 1;
        opacity: 0;
        transition: opacity 0.3s;
        margin-left: 10px
    }

    .tooltip-right {
        top: 0px;
        left: 125%;
    }

    .tooltip-right::after {
        content: "";
        position: absolute;
        top: 10%;
        right: 100%;
        margin-top: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: transparent #555 transparent transparent;
    }

    .tooltip:hover .tooltiptext {
        visibility: visible;
        opacity: 1;
    }
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.logbook')</h2>

         @include('includes.professional_tab_menu')
        <div class="bokcntnt-bdy">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">

                    <form id="myform" method="post" action="{{ route('prof-log-book.store') }}">
                        @csrf
                        <div class="form_body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
                                    <div id="fields">
                                        <div class="col-md-12 plus_row" id="fieldRow0">
                                            <div class="row">
                                                <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">
                                                            @lang('site.question') *
                                                            <div class="tooltip">
                                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                                <span class="tooltiptext tooltip-right">{{__('site.logbook_qestion_tooltip')}}</span>
                                                            </div>
                                                        </label>
                                                        <input type="text" name="question[]" class="qstn personal-type required" id="field_value1" placeholder="@lang('site.enter_question')">
                                                        <div class="clearfix"></div>
                                                        <div id="question_err"></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                                                    <button type="button" class="btn btn-success add-button addsingleselect" title="@lang('site.add')"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <input type="hidden" class="countvalues" value="1">


                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="submit-login add_btnm">
                                        <input value="@lang('site.add')" type="submit" class="login_submitt" id="add_logbook">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
<style type="text/css">
    .upldd {
    display: block;
    width: auto;
    border-radius: 4px;
    text-align: center;
    background: #9caca9;
    cursor: pointer;
    overflow: hidden;
    padding: 10px 15px;
    font-size: 15px;
    color: #fff;
    cursor: pointer;
    float: left;
    margin-top: 15px;
    font-family: 'Poppins', sans-serif;
    position: relative;
}
.upldd input {
    position: absolute;
    font-size: 50px;
    opacity: 0;
    left: 0;
    top: 0;
    width: 200px;
    cursor: pointer;
}
.upldd:hover{
    background: #1781d2;
}

</style>

  <script>
    $('body').on('click','.addsingleselect',function(){
        var count = parseInt($('.countvalues').val());
        console.log(count);
        var singleselect = '<div class="col-md-12" id="fieldRow'+count+'"><div class="row"><div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">\
                                <div class="your-mail">\
                                    <label for="exampleInputEmail1">@lang('site.question') *</label>\
                                    <input type="text" name="question[]" class="qstn form-control required" id="field_value'+count+'" placeholder="@lang('site.enter_question')">\
                                </div>\
                            </div>\
                            <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">\
                                <button type="button" class="btn btn-danger add-button removesingleselect" title="Remove" data-id="'+count+'"><i class="fa fa-minus" aria-hidden="true"></i></button>\
                            </div></div></div><div class="clearfix"></div>';
        $('#fieldRow1').before(singleselect);
        $('.countvalues').val(count+1);
    });
    $('body').on('click','.removesingleselect',function(){
        var id = $(this).data('id');
        $('#fieldRow'+id).remove();
        $('.countvalues').val(parseInt($('.countvalues').val())-1);
    });
    $("#myform").validate({
        errorPlacement: function(error, element) {
            if (element.attr("id") == "field_value1") {
                $("#question_err").append(error);
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function(form) {
            var flag = 0;
            $('.qstn').each(function(i,elem){
                if($(elem).val() == "" || $(elem).val() == null){
                    flag = 1;
                    // var id = $(elem).attr('id');
                    console.log($(elem).attr('id'));
                    console.log($(`label[for=${$(elem).attr('id')}][class=error]`)[0]);
                    $(`label[for=${$(elem).attr('id')}][class=error]`).show();
                } else {
                    console.log($(elem).attr('id'));
                }
            })
            console.log("FLAG: "+ flag);
            if(flag == 0){
                console.log("Submitted");
                // form.submit();
            }
        }
    });

    $('#add_logbook').click(function(){
        $('.qstn').each(function() {
            $(this).rules("add", 
            {
                required: true,
            });
        });
        if($("#myform").valid()){
            $("#myform").submit();
        } else {
            console.log("Errors");
        }
    });
</script>


<style>
    .error{
        color: red !important;
    }
</style>
@endsection
