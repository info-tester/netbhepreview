@extends('layouts.app')
@section('title')
    {{-- @lang('site.my_blogs') --}}
   Log Book
@endsection
@section('style')
@include('includes.style')

@endsection
@section('scripts')
@include('includes.scripts')
<script src="{{URL::to('public/js/chosen.jquery.min.js')}}"></script>
@endsection
@section('header')
@include('includes.professional_header')
<link rel="stylesheet" href="{{URL::to('public/css/chosen.css')}}">
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.logbook')</h2>

        @include('includes.professional_tab_menu')

        <div class="bokcntnt-bdy no-margin-top">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>

            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">

                <form id="myform" method="post" action="{{ route('prof.logbook.user.assigne') }}">
                {{ csrf_field() }}

                <div class="" style="float: right;">
                    <a class="btn btn-success" href="{{route('prof-log-book.create')}}">+ @lang('site.add_new_question')</a>
                    <a href="{{route('prof.logbook.user.assigne.list')}}" class="btn btn-success" style="margin: 2px;">@lang('site.view_assign')</a>

                    <!-- <a href="javascript:;" class="show_assign btn btn-success" style="margin: 2px;">@lang('site.assign')</a> -->
                    <input value="@lang('site.assign')"  type="submit" id="allusersendemail" class="btn btn-success tpg btnmail">
                </div>


                <?php /*
                <form name="myForm" id="myForm" method="post" action="{{route('my.blog.search')}}">
                    @csrf
                    <div class="from-field">

                        <div class="frmfld">
                            <div class="form-group">
                                <label class="search_label">@lang('site.select') @lang('site.category')</label>
                                <select class="dashboard-type dashboard_select" name="cat_id" id="cat_id">
                                    <option value="">@lang('site.select') @lang('site.category')</option>
                                    @foreach(@$category as $cat)
                                        <option value="{{@$cat->id}}" @if(@$cat->id==@$key['cat_id']) selected @endif>{{@$cat->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="frmfld">
                            <div class="form-group">
                                <label class="search_label">@lang('site.select') @lang('site.status')</label>
                                <select class="dashboard-type dashboard_select" name="status" id="status">
                                    <option value="">@lang('site.select') @lang('site.status')</option>
                                    <option value="A" @if(@$key['status']=='A') selected @endif>@lang('site.active')</option>
                                    <option value="I" @if(@$key['status']=='I') selected @endif>@lang('site.inactive') </option>
                                </select>
                            </div>
                        </div>
                        <div class="frmfld">
                            <button class="banner_subb fmSbm">@lang('site.filter')</button>

                        </div>

                        <div class="frmfld">
                            <a class="btn btn-success" href="{{route('add.blog.post')}}">+@lang('site.add_new_post')</a>

                        </div>
                    </div>
                </form>

                */?>


                <div class="make-buty">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-12">
                        <label for="exampleInputEmail1">Selecionar usuário para enviar : </label>
                    </div>
                    <div class="col-lg-9 col-md-6 col-sm-12">
                        <select name="user_id[]" class="required form-control newdrop required chosen-select" data-placeholder="Selecione uma Opção" multiple="true" required="">
                            <option value="">Selecionar</option>
                            @foreach($all_paid_users as $row)
                                <option value="{{$row->user_id}}">{{ @$row->userDetails->nick_name ? @$row->userDetails->nick_name : @$row->userDetails->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
                <div class="buyer_table" style="margin-top: 5px;">
                    <div class="table-responsive">
                        @if(sizeof(@$all_log_book)>0)
                        <div class="table">
                            <div class="one_row1 hidden-sm-down only_shawo">
                                <div class="cell1 tab_head_sheet"><input type="checkbox" class="user" name="users" id="user" value="all"> All</div>
                                <div class="cell1 tab_head_sheet">ID da pergunta</div>
                                {{-- <div class="cell1 tab_head_sheet">Description</div> --}}

                                <div class="cell1 tab_head_sheet">@lang('site.added_by')</div>
                                {{-- <div class="cell1 tab_head_sheet">posted on</div> --}}
                                <div class="cell1 tab_head_sheet">@lang('site.status')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.action')</div>
                            </div>
                            <!--row 1-->
                                @foreach(@$all_log_book as $detail)
                                    <div class="one_row1 small_screen31">
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">question id</span>
                                            <td>
                                                <input type="checkbox" class="users" name="allquestionid[]" value="{{ $detail->id }}">
                                                <input type="hidden" class="users" name="question_name[]" value="{{ $detail->question }}">
                                            </td>
                                        </div>

                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.no')</span>
                                            <p class="add_ttrr">{{$detail->question}}</p>
                                        </div>

                                        {{-- <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">description</span>
                                            <p class="add_ttrr">
                                                {{ substr(strip_tags(@$form_data->form_dsc),0,20) }}...
                                            </p>
                                        </div> --}}

                                        {{-- @dump(@$blg->desc) --}}
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">added by</span>
                                            <p class="add_ttrr">{{ $detail['added_by']=='A'?'Adionado por': ( @$detail->user->nick_name ? @$detail->user->nick_name : @$detail->user->name ) }}</p>
                                        </div>


                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.status')</span>
                                            <p class="add_ttrr">
                                                {{-- {{ $detail['status'] }} --}}
                                                @if($detail['status'] == "ACTIVE")
                                                    ATIVO
                                                @endif

                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                            @if($detail->status=="ACTIVE")
                                                @if($detail->added_by=="P" && $detail->added_by_id == Auth::id())
                                                    <a href="{{route('prof-log-book.edit',[$detail->id])}}" class="acpt editbtn" style="margin: 2px;" title="Edit">@lang('site.edit')</a>
                                                    <a href="javascript:void(0)" onclick="deleteCategory('{{route('prof-log-book.destroy',[$detail->id])}}')" title="Delete" class="rjct delbtn"> @lang('site.delete')</a>


                                                   <!--  @if($detail->status=="ACTIVE")
                                                        <a href="{{ route('prof.logbook.status', ['id'=>$detail->id]) }}" onclick="return confirm('Are you really want to Inactive this logbook tools?')" class="acpt" title="Inactive">Inactive</a>
                                                    @else
                                                        <a href="{{ route('prof.logbook.status', ['id'=>$detail->id]) }}" title="Active" onclick="return confirm('Are you really want to Active this logbook tools?')" class="acpt rjct">Active</a>
                                                    @endif -->
                                                @else
                                                N.A
                                                @endif
                                               {{--  <a href="{{route('prof-log-book.show', [$detail->id])}}" class="acpt viewbtn" style="margin: 2px;" title="View Question">@lang('site.view')</a> --}}
                                                <!-- <a href="javascript:;" class="show_assign acpt assginbtn" data-title="{{$detail->question}}" data-id="{{ $detail->id }}" style="margin: 2px;">@lang('site.assign')</a>
                                                <a href="{{route('prof.logbook.user.assigne.list', ['logbook_master_id'=>@$detail->id])}}" class="acpt view_asgi_btn" style="margin: 2px;">@lang('site.view_assign')</a> -->
                                            @else

                                               <!--  <a href="{{ route('prof.logbook.status', ['id'=>$detail->id]) }}" title="Active" onclick="return confirm('Are you really want to Active this logbook tools?')" class="acpt rjct">Active</a> -->
                                            @endif
                                        </div>
                                    </div>
                                @endforeach

                        </div>
                        @else
                            <div class="one_row small_screen31">
                                <center><span><h3 class="error">Oops! não encontrado.</h3></span></center>
                            </div>
                        @endif
                    </div>
                </div>


            </form>


            </div>
        </div>
    </div>



<!--assigne Modal -->
<div class="modal fade" id="assignModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Assign User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="myform" method="post" action="{{ route('prof.logbook.user.assigne') }}">
                {{ csrf_field() }}
                <div class="modal-body">
                    <h5>Question: <span class="tool-title"></span></h5>

                        <label for="exampleInputEmail1">Select User</label>
                        <select name="user_id[]" class="required form-control newdrop required chosen-select" multiple="true" required="">
                            <option value="">Select</option>
                            @foreach($all_paid_users as $row)
                                <option value="{{$row->user_id}}">{{ @$row->userDetails->nick_name ? @$row->userDetails->nick_name : @$row->userDetails->name }}</option>
                            @endforeach
                        </select>
                    <!-- <input type="hidden" name="log_book_master_id" class="form_id"> --> {{-- this calass form id get form id --}}
                </div>

                <div class="modal-footer">
                    <button type="submit" value="submit" class="btn btn-primary">Save</button>
                </div>

            </form>
        </div>
    </div>
</div>



<!-- backup -->
<!-- <div class="modal fade" id="assignModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Assign User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="myform" method="post" action="{{ route('prof.logbook.user.assigne') }}">
                {{ csrf_field() }}
                <div class="modal-body">
                    <h5>Question: <span class="tool-title"></span></h5>

                        <label for="exampleInputEmail1">Select User</label>
                        <select name="user_id[]" class="required form-control newdrop required chosen-select" multiple="true" required="">
                            <option value="">Select</option>
                            @foreach($all_paid_users as $row)
                                <option value="{{$row->user_id}}">{{@$row->userDetails->name}}</option>
                            @endforeach
                        </select>
                    <input type="hidden" name="log_book_master_id" class="form_id"> {{-- this calass form id get form id --}}
                </div>

                <div class="modal-footer">
                    <button type="submit" value="submit" class="btn btn-primary">Save</button>
                </div>

            </form>
        </div>
    </div>
</div> -->


</section>


@endsection
@section('footer')
@include('includes.footer')



<script>
    $('#user').click(function(){
        if($(this).prop('checked') == true) {
            $('.users').prop('checked', true);
        } else {
            $('.users').prop('checked', false);
        }
    });

    $('body').on('click', '.users', function(){
        if($(this).prop('checked') == true) {
            if ($('.users:checked').length == $('.users').length) {
                $('#user').prop("indeterminate", false);
                $('#user').prop("checked", true);
            } else {
                if($('.users:checked').length == 0) {
                    $('#user').prop("checked", false);
                    $('#user').prop("indeterminate", false);
                } else {
                    $('#user').prop("indeterminate", true);
                }
            }
        } else {
            if ($('.users:checked').length == $('.users').length) {
               $('#user').prop("checked", true);
            } else {
                if($('.users:checked').length == 0) {
                    $('#user').prop("checked", false);
                    $('#user').prop("indeterminate", false);
                } else {
                    $('#user').prop("indeterminate", true);
                }
            }
        }
    });

</script>



<script>

$(document).ready(function(){
    $('.show_assign').click(function(){
        var title = $(this).data('title');
        var id = $(this).data('id');
        $('.tool-title').html(title);
        $('.form_id').val(id);
        $('#assignModal').modal('show');
    });
});

$(".chosen-select").chosen();

$(document).ready(function(){
        $("#myform").validate();
    });
</script>

<form method="post" id="destroy">
    {{ csrf_field() }}
    {{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
    var confirm = window.confirm('@lang('site.confrm_del_import_tool')');
    if(confirm){
        $("#destroy").attr('action',val);
        $("#destroy").submit();
    }
 }
</script>
<style>
    .error{
        color: red !important;
    }
</style>
  <script>
    $(document).ready(function(){
        $("#myform").validate();
    });
</script>

<style type="text/css">
                  .make-buty{
                    margin-top: 25px;
                    width: 100%;
                    float: left;
                  }
                  .make-buty label{
                    margin-top: 12px;
                    font-weight: 600;
                    font-size: 17px;
                  }
                  .make-buty .col-lg-3{
                    padding-right: 0px !important;
                  }

              </style>
@endsection
