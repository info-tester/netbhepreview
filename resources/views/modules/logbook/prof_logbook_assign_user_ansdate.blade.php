@extends('layouts.app')
@section('title')
  {{-- @lang('site.my_availability') --}}
  Logbook
@endsection
@section('style')

<link href='{{ URL::to('public/frontend/packages/core/main.css') }}' rel='stylesheet' />
<link href='{{ URL::to('public/frontend/packages/daygrid/main.css') }}' rel='stylesheet' />
<link href='{{ URL::to('public/frontend/packages/timegrid/main.css') }}' rel='stylesheet' />
<link href='{{ URL::to('public/frontend/packages/list/main.css') }}' rel='stylesheet' />

@include('includes.style')
<style>
.time_slot_span{
    background: #1781d2;
    padding: 2px 7px;
    border: solid 1px #475dfb;
    border-radius: 12px;
    color: #fff;
    margin: 0 15px 10px 0;
    float: left;
    cursor: pointer;

}

}
.ui-datepicker{
	z-index:2 !important;
	}
.form-group {
	 width: 100% !important;
}
</style>
@endsection
@section('scripts')
@include('includes.scripts')

<script src='{{ URL::to('public/frontend/packages/core/main.js') }}'></script>
<script src='{{ URL::to('public/frontend/packages/interaction/main.js') }}'></script>
<script src='{{ URL::to('public/frontend/packages/daygrid/main.js') }}'></script>
<script src='{{ URL::to('public/frontend/packages/timegrid/main.js') }}'></script>
<script src='{{ URL::to('public/frontend/packages/list/main.js') }}'></script>
<script src="{{ URL::to('public/frontend/js/moment.min.js') }}"></script>
<style type="text/css">
    .fc-available {
    background: #4f4f4e !important;
    color: #fff !important;
    }
    .fc-more-popover .fc-event-container{
        height: 167px;
        overflow-y: scroll;
    }
</style>



@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        {{-- <h2>@lang('client_site.availability')</h2> --}}
        <h2>@lang('site.assigned_users')</h2>

        @include('includes.professional_tab_menu')

        <div class="bokcntnt-bdy no-margin-top">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp

            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
            {{-- @include('includes.user_sidebar') --}}
            @include('includes.professional_sidebar')

            <div class="dshbrd-rghtcntn">

                <div class="mainDiv">
                    @if(sizeof(@$logbookdata)>0)
                        <div class="buyer_table">
                            <div class="table-responsive">
                                <div class="table">
                                    <div class="one_row1 hidden-sm-down only_shawo">

                                        <div class="cell1 tab_head_sheet">@lang('site.user_name')</div>

                                        <div class="cell1 tab_head_sheet">Answer Date</div>

                                        <div class="cell1 tab_head_sheet">@lang('site.action')</div>
                                    </div>
                                    <!--row 1-->
                                    @foreach(@$logbookdata as $ql)
                                        <div class="one_row1 small_screen31">

                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">user_id</span>
                                                <p class="add_ttrr">{{ @$ql->getUserData->nick_name ? @$ql->getUserData->nick_name : @$ql->getUserData->name }}</p>
                                            </div>

                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">assign_date</span>
                                                <p class="add_ttrr">{{ Date::parse(@$ql->ans_date)->format('m/d/Y (D)')}}</p>
                                            </div>


                                            <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                                <span class="W55_1">@lang('client_site.action')</span>

                                               <a href="{{route('prof.logbook.user.assigne.queansdate',[$ql->id])}}" class=""  style="margin: 2px;"><i class="fa fa-eye"></i> @lang('site.view')</a>

                                            </div>
                                        </div>
                                    @endforeach
                                    {{-- {{ $avl->->onEachSide(5)->links() }} --}}
                                </div>
                            </div>
                        </div>

                        <div class="pbdy-box" id="dwn4">
                            <h3>@lang('client_site.calendar')</h3>
                            <div class="right_dash" style="width:100%;">
                                <div class="main-form">
                                    <div class="avll">
                                        <p><img src="{{URL::to('public/frontend/images/avl.png')}}"> @lang('client_site.available')</p>
                                        <p><img src="{{URL::to('public/frontend/images/unvl.png')}}"> @lang('client_site.not_available')</p>
                                    </div>
                                    <div id='wrap'>
                                        <div id='calendar'></div>
                                        <div style='clear:both'></div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    @endif
                </div>
            </div>
        </div>
    </div>









<!--View logbook question answer  Modal start -->
<div class="modal fade" id="view_logbook_que_answer_Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Log book question and answer</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <p><strong>Question :</strong> <span class="question-title"></span> ?</p>
                <p><strong>Answer :</strong> <span class="log_answer"></span> </p>
            </div>
        </div>
    </div>
</div>
<!--View logbook question answer Modal end -->

</section>
@endsection
@section('footer')
@include('includes.footer')


<script>
$(document).ready(function(){

    $('.logbook_que_answer_btn').click(function(){
        var log_question = $(this).data('title');
        var log_answ = $(this).data('logbookanswer');
        $('.question-title').html(log_question);
        $('.log_answer').html(log_answ);
        $('#view_logbook_que_answer_Modal').modal('show');
    });

});
</script>

<script>
    var availiabilityDays = <?php echo $avlDay; ?>;

    var availiabilityTimes = <?php echo $avlDay; ?>;
    //var availiabilityTimes1 = <?php echo $avlDay; ?>;

    // var availiabilityTimes = < ?php echo $avlTime; ?>;
     var availiabilityTimes1 = <?php echo $avlTime1; ?>;
    document.addEventListener('DOMContentLoaded', function() {
    var Calendar = FullCalendar.Calendar;
    var Draggable = FullCalendarInteraction.Draggable


    var events = [];
    var timeS;
    var detailsurl ="https://www.netbhe.com/prof-logbook-user-qus-ansdate/"
    availiabilityTimes1.forEach(function(item1, index1) {
        events.push({
            'title': item1.ans_date,
            'start': item1.ans_date,
            'end': item1.ans_date,
            'url': detailsurl+item1.id


            /*'title': item1.from_time+'-'+item1.to_time,
            'start': item1.date+'T'+item1.from_time,
            'end': item1+'T'+item1.to_time*/
        });
    })

    var calendarEl = document.getElementById('calendar');
    var calendar;
    var i = 1;
    var event = [];
    var ardt = [];

    calendar = new Calendar(calendarEl, {
      eventLimit: true, // for all non-TimeGrid views
      locale: 'pt-br',
      plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list' ],
      header: {
        left: 'prev,next today',
        center: 'title',
        right: ''
      },
      views: {
        timeGrid: {
            eventLimit: 6 // adjust to 6 only for timeGridWeek/timeGridDay
        }
    },
      /*slotDuration: '01:00:00',
      slotLabelInterval: '00:30:00',
      eventSources: [event],*/
      events: events,
      selectable: true,
      firstDay: 6,
      select: function(info) {
        // checkDate(info.startStr);
      },


    eventClick: function(event) {
        if (event.url) {
            window.open(event.url, "_blank");
            return false;
        }
    },


      dayRender: function(event) {
        const day = moment(event.date).format('YYYY-MM-DD');
        var todayDate = new Date();
        if ($.inArray(day, availiabilityDays) > -1) {
            var time = availiabilityTimes[day];
            //var time = '21.10.00';
            $.each(time, function(ind, val) {
               /* $(event.el).append('<label class="date_click" data-time="'+val+'" data-date="'+moment(event.date).format('YYYY-MM-DD')+'" onclick="callMe('+val+');">' + val + '</label>');
                $('.fc-day-top').addClass('avl-date');*/
                 todayDate.setHours(0,0,0,0);
                if(event.date >= todayDate){
                    $(event.el).css('background-color', '#60F76F');
                }
                else{
                    $(event.el).css('background-color', 'rgb(236, 205, 205)');
                }
            });
        }
        else{
           $(event.el).css('background-color', 'rgb(236, 205, 205)');
        }
      },
      eventRender: function(info) {
        // console.log('event', info);
      },
    });

    calendar.render();
    });
    function callMe(val){
    alert(val);
    }

</script>
<script>
    $(function() {
        $("#datepicker11").datepicker({dateFormat: "yy-mm-dd",
           defaultDate: new Date(),
           maxDate:new Date(),
           onClose: function( selectedDate ) {
           //$( "#datepicker1").datepicker( "option", "minDate", selectedDate );
          }
        });
    });
</script>
<script>
    $(function() {
        $("#datepicker").datepicker({dateFormat: "dd-mm-yy",
            defaultDate: new Date(),
            showButtonPanel: true,
            showMonthAfterYear: true,
            showWeek: true,
            showAnim: "drop",
            constrainInput: true,
            yearRange: "-90:",
            minDate:new Date(),
            onClose: function( selectedDate ) {
               $( "#datepicker1").datepicker( "option", "minDate", selectedDate );
            }
        });
        $("#datepicker1").datepicker({dateFormat: "dd-mm-yy",
            defaultDate: new Date(),
            showButtonPanel: true,
            showMonthAfterYear: true,
            showWeek: true,
            showAnim: "drop",
            constrainInput: true,
            yearRange: "-90:",
            onClose: function( selectedDate ) {
                $( "#datepicker" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>



@endsection
