@extends('layouts.app')
@section('title')
    {{-- @lang('site.add_new_blog') --}}
    Coaching tools
@endsection
@section('style')
@include('includes.style')
<style type="text/css">
    div.mce-edit-area {
        background:
        #FFF;
        filter: none;
        min-height: 317px;
    }
    .chck_eml_rd{
        color: red;
        font-weight: bold;
        font-size: 14px;
    }
    
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.edit_question')</h2>
        {{-- <h2>@lang('site.add_new_blog')</h2> --}}
        @include('includes.professional_tab_menu')
        <div class="bokcntnt-bdy no-margin-top">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">
                    
                    <form id="myform" method="post" action="{{ route('form.question.update',['form_id'=>$form->id,'id'=>@$details->id]) }}">
                        @csrf
                        <div class="form_body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
                                    <div class="your-mail">
                                        <label for="exampleInputEmail1" class="personal-label">@lang('site.question') *</label>
                                        <input type="text" name="field_name" class="personal-type required" id="title" placeholder="Question Name" value="{{ @$details->field_name }}">
                                    </div>
                                </div>
                                
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('site.type') *</label>
                                        <select name="formtype" class="personal-type newdrop required formType">
                                            <option value="">{{__('site.question_select')}}</option>
                                            <option value="TEXTBOX" {{ @$details->field_type=='TEXTBOX'?'selected':'' }}>{{__('site.textbox')}}</option>
                                            <option value="TEXTAREA" {{ @$details->field_type=='TEXTAREA'?'selected':'' }}>{{__('site.textarea')}}</option>
                                            <option value="SINGLESELECT" {{ @$details->field_type=='SINGLESELECT'?'selected':'' }}>{{__('site.single_select')}}</option>
                                            <option value="MULTISELECT" {{ @$details->field_type=='MULTISELECT'?'selected':'' }}>{{__('site.multi_select')}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                
                                
                            </div>
                                
                                @php
											$is_add = 'YES';
											$total_data = 1;
											@endphp
                                <div id="fields">
@if(@$details->field_type=="SINGLESELECT" || @$details->field_type=="MULTISELECT")
													@php
														$is_add = 'NO';
														$data = json_decode(@$details->field_values);
														$total_data = count($data);
														// $data = array_reverse($data);
														// dd($data)
													@endphp
													@foreach($data as $key=>$row)
														@if($key==$total_data-1)
											<div class="" id="fieldRow{{ $key }}"><div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
														<div class="your-mail">
															<label for="exampleInputEmail1" class="personal-label">Option *</label>
														<input type="text" name="field_value[]" class="personal-type required" id="field_value{{ $key }}" placeholder="Eneter Select Value" value="{{ $row }}">
														</div>
													</div>
													<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
														<button type="button" class="btn btn-success add-button addsingleselect" title="Add"><i class="fa fa-plus" aria-hidden="true"></i></button>
													</div></div></div><div class="clearfix"></div>
														@else
														<div class="" id="fieldRow{{ $key }}"><div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<div class="your-mail">
													<label for="exampleInputEmail1" class="personal-label">Option *</label>
													<input type="text" name="field_value[]" class="personal-type required" id="field_value{{ $key }}" placeholder="Eneter Select Value" value="{{ $row }}">
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<button type="button" class="btn btn-danger add-button removesingleselect" title="Remove" data-id="{{ $key }}"><i class="fa fa-minus" aria-hidden="true"></i></button>
											</div></div></div><div class="clearfix"></div>
														@endif
													@endforeach
												@endif
                                </div>            
                                <input type="hidden" class="countvalues" value="{{ $total_data }}">     <input type="hidden" class="total_value" value="{{ $total_data }}">            
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
                                        <div class="submit-login add_btnm">
                                            <input value="{{__('site.save_question')}}" type="submit" class="login_submitt">
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                        <div class="submit-login add_btnm">
                                            <input value="{{__('site.complete_question')}}" type="submit" class="login_submitt" name="complete">
                                        </div>
                                    </div>
                                </div>
                                
                            
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
<style type="text/css">
.add-button {
    margin-top: 38px;
}
    .upldd {
    display: block;
    width: auto;
    border-radius: 4px;
    text-align: center;
    background: #9caca9;
    cursor: pointer;
    overflow: hidden;
    padding: 10px 15px;
    font-size: 15px;
    color: #fff;
    cursor: pointer;
    float: left;
    margin-top: 15px;
    font-family: 'Poppins', sans-serif;
    position: relative;
}
.upldd input {
    position: absolute;
    font-size: 50px;
    opacity: 0;
    left: 0;
    top: 0;
    width: 200px;
    cursor: pointer;
}
.upldd:hover{
    background: #1781d2;
}

</style>
<script>
    $(document).ready(function(){
        $("#myform").validate();
    });
</script>
<style>
    .error{
        color: red !important;
    }
</style>
<script>
	$(document).ready(function(){
		$('.formType').change(function(){
			var is_add = '{{ $is_add }}';
			$('#fields').show();
			// if(is_add=='NO'){
			// 	return false;
			// }else{
			// 	return true;
			// }
			var type = $(this).val();
			var count = $('.countvalues').val();
			var singleselect = '<div class="" id="fieldRow'+count+'"><div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">\
												<div class="your-mail">\
													<label for="exampleInputEmail1" class="personal-label">Option *</label>\
													<input type="text" name="field_value[]" class=" required personal-type" id="field_value'+count+'" placeholder="Eneter Select Value">\
												</div>\
											</div>\
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">\
												<button type="button" class="btn btn-success add-button addsingleselect" title="Add"><i class="fa fa-plus" aria-hidden="true"></i></button>\
											</div></div></div><div class="clearfix"></div>';
			if(type=='SINGLESELECT' || type=='MULTISELECT'){
				if(is_add=='YES'){
					$('#fields').html('');
					var newcount = parseInt(count)+1;
					$('.countvalues').val(newcount);
					$('#fields').html(singleselect);
				}
			}else{
				$('#fields').hide();
			}
		});
		$('body').on('click','.addsingleselect',function(){
		var count = $('.countvalues').val();
        var total_value = $('.total_value').val();
		total_value = parseInt(total_value)-1;
		var singleselect = '<div class="" id="fieldRow'+count+'"><div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">\
												<div class="your-mail">\
													<label for="exampleInputEmail1" class="personal-label">Option *</label>\
													<input type="text" name="field_value[]" class="required personal-type" id="field_value'+count+'" placeholder="Eneter Select Value">\
												</div>\
											</div>\
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">\
												<button type="button" class="btn btn-danger add-button removesingleselect" title="Remove" data-id="'+count+'"><i class="fa fa-minus" aria-hidden="true"></i></button>\
											</div></div></div><div class="clearfix"></div>';
		$('#fieldRow'+total_value).before(singleselect);							
		});
		$('body').on('click','.removesingleselect',function(){
			var id = $(this).data('id');
			$('#fieldRow'+id).remove();
		});
	});
</script>
@endsection