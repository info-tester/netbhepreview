@extends('layouts.app')
@section('title')
    {{-- @lang('site.add_new_blog') --}}
    Coaching tools
@endsection
@section('style')
@include('includes.style')
<style type="text/css">
    div.mce-edit-area {
        background:
        #FFF;
        filter: none;
        min-height: 317px;
    }
    .chck_eml_rd{
        color: red;
        font-weight: bold;
        font-size: 14px;
    }
    
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>{{-- @lang('site.add_new_blog') --}} @lang('site.add_new_question')</h2>

        @include('includes.professional_tab_menu')
        <div class="bokcntnt-bdy no-margin-top">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">
                    
                    <form id="myform" method="post" action="{{ route('form.question.store',['form_id'=>$details->id]) }}">
                        @csrf
                        <div class="form_body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
                                    <div class="your-mail">
                                        <label for="exampleInputEmail1" class="personal-label">@lang('site.question') *</label>
                                        <input type="text" name="field_name" class="personal-type required" id="title" placeholder="@lang('site.question')" >
                                    </div>
                                </div>
                                
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('site.type') *</label>
                                        <select name="formtype" class="personal-type newdrop required formType">
                                            <option value="">{{__('site.question_select')}}</option>
                                            <option value="TEXTBOX">{{__('site.textbox')}}</option>
                                            <option value="TEXTAREA">{{__('site.textarea')}}</option>
                                            <option value="SINGLESELECT">{{__('site.single_select')}}</option>
                                            <option value="MULTISELECT">{{__('site.multi_select')}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                
                                
                            </div>
                                <input type="hidden" class="countvalues" value="1">
                                <div id="fields">

                                </div>                                     
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                        <div class="submit-login add_btnm">
                                            <input value="@lang('site.save')" type="submit" class="login_submitt">
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                        <div class="submit-login add_btnm">
                                            <input value="@lang('site.completa')" type="submit" class="login_submitt" name="complete">
                                        </div>
                                    </div>
                                </div>
                                
                            
                        </div>
                    </form>




                    <div class="buyer_table">
                                <div class="table-responsive">
                                    @if(@$questions->isNotEmpty())
                                    <div class="table">
                                        <div class="one_row1 hidden-sm-down only_shawo">
                                            <div class="cell1 tab_head_sheet">#</div>
                                            <div class="cell1 tab_head_sheet">@lang('site.question_type')</div>
                                            <div class="cell1 tab_head_sheet">@lang('site.question_name')</div>
                                            <div class="cell1 tab_head_sheet">@lang('site.action')</div>
                                        </div>
                                        <!--row 1-->                                
                                            @foreach(@$questions as $key=>$data)                        
                                                <div class="one_row1 small_screen31">
                                                    <div class="cell1 tab_head_sheet_1">
                                                        <span class="W55_1">@lang('site.no')</span>
                                                        <p class="add_ttrr">{{$key+1}}</p>
                                                    </div>

                                                    <div class="cell1 tab_head_sheet_1">
                                                        <span class="W55_1">Question Type</span>
                                                        <p class="add_ttrr">
                                                            @if($data['field_type']=='TEXTBOX')
                                                            {{__('site.textbox')}}
                                                            @elseif($data['field_type']=='TEXTAREA')
                                                            {{__('site.textarea')}}
                                                            @elseif($data['field_type']=='SINGLESELECT')
                                                            {{__('site.single_select')}}
                                                            @elseif($data['field_type']=='MULTISELECT')
                                                            {{__('site.multi_select')}}
                                                            @endif
                                                        {{-- {{ $data['field_type'] }} --}}
                                                        </p>
                                                    </div>
                                                    <div class="cell1 tab_head_sheet_1">
                                                        <span class="W55_1">Question Name</span>
                                                        <p class="add_ttrr">{{ @$data['field_name'] }}</p>
                                                    </div>
                                                    
                                                   
                                                    <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                                        @if($details->added_by=="P" && $details->added_by_id == Auth::id())
                                                            <a href="{{route('form.question.edit',['form_id'=>$details->id,'id'=>$data['id']])}}" class="rjct">@lang('site.edit')</a>
                                                            {{-- <a href="{{route('professional-form.show', ['id'=>@$blg->id])}}" class="acpt">@lang('site.view')</a> --}}
                                                            <a onclick="return confirm(`@lang('client_site.delete_question')`)" href="{{route('form.question.delete',['form_id'=>$details->id,'id'=>$data['id']])}}" class="rjct">@lang('site.delete')</a>
                                                        @else
                                                        N.A
                                                        @endif
                                                    </div>
                                                </div>                                    
                                            @endforeach
                                        
                                    </div>
                                    {{-- @else
                                        <div class="one_row small_screen31">
                                            <center><span><h3 class="error">OOPS! not found.</h3></span></center>
                                        </div> --}}
                                    @endif
                                </div>
                            </div>



                    
                    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
<style type="text/css">
.add-button {
    margin-top: 38px;
}
    .upldd {
    display: block;
    width: auto;
    border-radius: 4px;
    text-align: center;
    background: #9caca9;
    cursor: pointer;
    overflow: hidden;
    padding: 10px 15px;
    font-size: 15px;
    color: #fff;
    cursor: pointer;
    float: left;
    margin-top: 15px;
    font-family: 'Poppins', sans-serif;
    position: relative;
}
.upldd input {
    position: absolute;
    font-size: 50px;
    opacity: 0;
    left: 0;
    top: 0;
    width: 200px;
    cursor: pointer;
}
.upldd:hover{
    background: #1781d2;
}

</style>
<script>
    $(document).ready(function(){
        $("#myform").validate();
    });
</script>
<style>
    .error{
        color: red !important;
    }
</style>
<script>
	$(document).ready(function(){
		$('.formType').change(function(){
			var type = $(this).val();
			var count = $('.countvalues').val();
			var singleselect = '<div class=" plus_row" id="fieldRow'+count+'"><div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">\
												<div class="form-group">\
													<label for="exampleInputEmail1" class="personal-label">Option *</label>\
													<input type="text" name="field_value[]" class="personal-type required" id="field_value'+count+'" placeholder="Eneter Select Value">\
												</div>\
											</div>\
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">\
												<button type="button" class="btn btn-success add-button addsingleselect" title="Add"><i class="fa fa-plus" aria-hidden="true"></i></button>\
											</div></div></div><div class="clearfix"></div>';
			if(type=='SINGLESELECT' || type=='MULTISELECT'){
				$('#fields').html('');
				var newcount = parseInt(count)+1;
				$('.countvalues').val(newcount);
				$('#fields').html(singleselect);
			}
		});
		$('body').on('click','.addsingleselect',function(){
		var count = $('.countvalues').val();
			var singleselect = '<div class="" id="fieldRow'+count+'"><div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">\
												<div class="form-group">\
													<label for="exampleInputEmail1" class="personal-label">Option *</label>\
													<input type="text" name="field_value[]" class="personal-type required" id="field_value'+count+'" placeholder="Eneter Select Value">\
												</div>\
											</div>\
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">\
												<button type="button" class="btn btn-danger add-button removesingleselect" title="Remove" data-id="'+count+'"><i class="fa fa-minus" aria-hidden="true"></i></button>\
											</div></div></div><div class="clearfix"></div>';
			$('#fieldRow1').before(singleselect);								
		});
		$('body').on('click','.removesingleselect',function(){
			var id = $(this).data('id');
			$('#fieldRow'+id).remove();
		});
	});
</script>
@endsection