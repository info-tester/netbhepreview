@extends('layouts.app')
@section('title')
    @lang('site.post') @lang('site.details')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.post') @lang('site.details')</h2>
        <div class="bokcntnt-bdy">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="blog-right details-right">
                    <div class="blog-box">
                        
                        <div class="blog-dtls">
                            <h4><a href="javascript::void(0);">{{ @$blog->title }}</a></h4>
                            <ul class="blog-post">
                                <li><img src="{{ URL::to('public/frontend/images/post-1.png') }}"> @lang('site.author') : {{ @$blog->postedBy->name }}</li>
                                <li> {{ date('D-Y', strtotime(@$blog->created_at)) }}</li>
                                <li>{{ @$blog->blogCategoryName->name }}</li>
                            </ul>

                            @if(@$blog->you_tube_video)
                                <div class="">
                                    <iframe id="videoObject" width="100%" height="300" src="https://www.youtube.com/embed/{{@$blog->you_tube_video}}" frameborder="2" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    
                                </div>
                            @else
                            <div class="blog-image">
                                <img src="{{ URL::to('storage/app/public/uploads/blog_image').'/'.@$blog->image }}" alt="">
                            </div>
                            @endif
                            <div class="prev-next">
                                <ul>
                                    <li>@lang('site.share_on') :</li>
                                    <li><a href="#"><img src="{{ URL::to('public/frontend/images/social1.png') }}" alt=""></a></li>
                                    <li><a href="#"><img src="{{ URL::to('public/frontend/images/social2.png') }}" alt=""></a></li>
                                    <li><a href="#"><img src="{{ URL::to('public/frontend/images/social3.png') }}" alt=""></a></li>
                                    <li><a href="#"><img src="{{ URL::to('public/frontend/images/social4.png') }}" alt=""></a></li>
                                </ul>
                                <a href="#">{{ count(@$blog->blogComments) }} @lang('site.comments')</a>
                            </div>
                            <h6>{{ @$blog->title }}</h6>
                            <p>{!! @$blog->desc !!}</p>
                            
                            
                        </div>
                    </div>
                    
                </div>
            </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
    <script>
        $(document).ready(function(){
            $('#myForm').validate();     
        });
    </script>

@endsection