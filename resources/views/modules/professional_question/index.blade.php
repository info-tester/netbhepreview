@extends('layouts.app')
@section('title')
    {{-- @lang('site.my_blogs') --}}
    Coaching tools
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2> Coaching tools</h2>
        

        <div class="frmfld">
            <a class="btn btn-success" href="#">Form tools</a>
        </div>


        <div class="bokcntnt-bdy">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
                // dd($user);
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>

            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">

                <div class="frmfld" style="float: right;">
                    <a class="btn btn-success" href="{{route('professional-form.create')}}">Add new question</a>
                </div>


                <?php /*
                <form name="myForm" id="myForm" method="post" action="{{route('my.blog.search')}}">
                    @csrf
                    <div class="from-field">
                        
                        <div class="frmfld">
                            <div class="form-group">
                                <label class="search_label">@lang('site.select') @lang('site.category')</label>
                                <select class="dashboard-type dashboard_select" name="cat_id" id="cat_id">
                                    <option value="">@lang('site.select') @lang('site.category')</option>
                                    @foreach(@$category as $cat)
                                        <option value="{{@$cat->id}}" @if(@$cat->id==@$key['cat_id']) selected @endif>{{@$cat->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="frmfld">
                            <div class="form-group">
                                <label class="search_label">@lang('site.select') @lang('site.status')</label>
                                <select class="dashboard-type dashboard_select" name="status" id="status">
                                    <option value="">@lang('site.select') @lang('site.status')</option>
                                    <option value="A" @if(@$key['status']=='A') selected @endif>@lang('site.active')</option>
                                    <option value="I" @if(@$key['status']=='I') selected @endif>@lang('site.inactive') </option>
                                </select>
                            </div>
                        </div>
                        <div class="frmfld">
                            <button class="banner_subb fmSbm">@lang('site.filter')</button>
                            
                        </div>

                        <div class="frmfld">
                            <a class="btn btn-success" href="{{route('add.blog.post')}}">+@lang('site.add_new_post')</a>
                            
                        </div>
                    </div>
                </form>

                */?>
                <div class="buyer_table">
                    <div class="table-responsive">
                        @if(@$questions->isNotEmpty())
                        <div class="table">
                            <div class="one_row1 hidden-sm-down only_shawo">
                                <div class="cell1 tab_head_sheet">#</div>
                                <div class="cell1 tab_head_sheet">Question Type</div>
                                <div class="cell1 tab_head_sheet">Question Name</div>
                                <div class="cell1 tab_head_sheet">Action{{-- @lang('site.action') --}}</div>
                            </div>
                            <!--row 1-->                                
                                @foreach(@$questions as $key=>$data)                        
                                    <div class="one_row1 small_screen31">
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.no')</span>
                                            <p class="add_ttrr">{{$key+1}}</p>
                                        </div>

                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">Question Type</span>
                                            <p class="add_ttrr">
                                            {{ $data['field_type'] }}
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">Question Name</span>
                                            <p class="add_ttrr">{{ @$data['field_name'] }}</p>
                                        </div>
                                        
                                       
                                        <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                            @if($form_data->added_by=="P" && $form_data->added_by_id == Auth::id())
                                                <a href="{{route('form.question.edit',['qid',$data->id])}}" class="rjct">@lang('site.edit')</a>
                                                {{-- <a href="{{route('professional-form.show', ['id'=>@$blg->id])}}" class="acpt">@lang('site.view')</a> --}}
                                                <a href="{{route('remove.blog.post', ['id'=>@$blg->id])}}" class="rjct">@lang('site.delete')</a>
                                            @endif
                                        </div>
                                    </div>                                    
                                @endforeach
                            
                        </div>
                        @else
                            <div class="one_row small_screen31">
                                <center><span><h3 class="error">OOPS! @lang('site.blog_post_not_found').</h3></span></center>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')


@endsection