@extends('layouts.app')
@section('title', (@$user->nick_name  ? @$user->nick_name : @$user->name).' public profile')

@section('style')
	@include('includes.style')
    <link href='{{ URL::to('public/frontend/packages/core/main.css') }}' rel='stylesheet' />
    <link href='{{ URL::to('public/frontend/packages/daygrid/main.css') }}' rel='stylesheet' />
    <link href='{{ URL::to('public/frontend/packages/timegrid/main.css') }}' rel='stylesheet' />
    <link href='{{ URL::to('public/frontend/packages/list/main.css') }}' rel='stylesheet' />
@endsection

@section('scripts')
	@include('includes.scripts')
    <script src='{{ URL::to('public/frontend/packages/core/main.js') }}'></script>
<script src='{{ URL::to('public/frontend/packages/interaction/main.js') }}'></script>
<script src='{{ URL::to('public/frontend/packages/daygrid/main.js') }}'></script>
<script src='{{ URL::to('public/frontend/packages/timegrid/main.js') }}'></script>
<script src='{{ URL::to('public/frontend/packages/list/main.js') }}'></script>

<script src="{{ URL::to('public/frontend/js/moment.min.js') }}"></script>
<style type="text/css">
    .fc-available {
        background: #4f4f4e !important;
        color: #fff !important;
    }
</style>
    <script>
$(document).ready(function() {
    $("#owl-demo-1").owlCarousel({
        margin: 25,
        nav: true,
        loop: true,
        responsive: {
          0: {
            items: 1
          },
          768: {
            items:2
          },
          1078: {
            items:3
          },
          1200: {
            items:4
          },
        }
      });
    })
</script> 
 
<script>
    var availiabilityDays = [1,2,3,4,5,6,7];
    var availiabilityTimes = <?php echo $avlTime; ?>;
  document.addEventListener('DOMContentLoaded', function() {
    var Calendar = FullCalendar.Calendar;
    var Draggable = FullCalendarInteraction.Draggable

    /* initialize the external events
    -----------------------------------------------------------------*/

    /*var containerEl = document.getElementById('external-events-list');
    new Draggable(containerEl, {
      itemSelector: '.fc-event',
      eventData: function(eventEl) {
        return {
          title: eventEl.innerText.trim()
        }
      }
    });*/

    //// the individual way to do it
    // var containerEl = document.getElementById('external-events-list');
    // var eventEls = Array.prototype.slice.call(
    //   containerEl.querySelectorAll('.fc-event')
    // );
    // eventEls.forEach(function(eventEl) {
    //   new Draggable(eventEl, {
    //     eventData: {
    //       title: eventEl.innerText.trim(),
    //     }
    //   });
    // });

    /* initialize the calendar
    -----------------------------------------------------------------*/

    var calendarEl = document.getElementById('calendar');
    var calendar;
    var i = 1;
    // var events = [
    //     {
    //         allDay: false,
    //         start: moment('2019-05-29').format(),
    //         end: moment('2019-05-29').format(),
    //         title: 'Sample Item'
    //     },
    //     {
    //         allDay: false,
    //         start: moment('2019-05-30 12:00:00').format(),
    //         end: moment('2019-05-30 13:00:00').format(),
    //         title: 'This is a sample event.',
    //         className: 'revent'
    //     },
    //     {
    //         id: i,
    //         allDay: false,
    //         start: moment('2019-05-30 4:00:00').format(),
    //         end: moment('2019-05-30 5:00:00').format(),
    //         title: '<img src="images/unavailable.png" class="unvimg u">',
    //         className: 'cross_event'
    //     }
    //   ];

     // var events = [];
     //    for (var i = 1; i <= 41; i++) {
     //        // if(result[i] != undefined) {
     //        //   result[i].forEach(function(item, index){
     //        //     if(item != undefined) {
     //        //     item.forEach(function(item1, index1){
     //        //       if(item1.availiability != undefined){
     //        //         console.log(item1)

     //                events.push("Ok")
     //          //     }

     //          //   })
     //          // }

     //          // })
     //        // }
     //    }
    calendar = new Calendar(calendarEl, {
      plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list' ],
      header: {
        left: 'prev,next today',
        center: 'title',
        right: ''
      },
      slotDuration: '01:00:00',
      slotLabelInterval: '00:30:00',
      eventSources: [event],
      selectable: true,
      select: function(info) {
        
      },
     
      dayRender: function(event) {
        const day = moment(event.date).weekday() + 1;
        if ($.inArray(day, availiabilityDays) > -1) {
            // console.log(day);
            // $(event.el).html('ok')
            // console.log(event.el);
            
            var time = availiabilityTimes[day];
            $.each(time, function(ind, val) {
               
                $(event.el).append('<img src="{{ URL::to('public/frontend/images/avl.png') }}" class="unvl">');

                $(event.el).append('<label>' + val + '</label>')
            })
        }
        else{
            
            $(event.el).append('<img src="{{ URL::to('public/frontend/images/unvl.png') }}" class="unvl">');
        }
        /*var day = moment(info.date).format('YYYY-MM-DD');
        if(day=='2019-05-01') {
            console.log(day);
            $(info.el).append('<img src="images/unavailable.png" class="unvl">');
        }*/
      },
      eventRender: function(info) {
          console.log(info);
        // $(info.el).addClass('hr-event');
        // if( ! info.event._def.allDay) {
        //     if(info.view.type == 'timeGridDay') {
        //         if($(info.el).hasClass('cross_event')) {
        //             var e = $(info.el).find('.fc-title');
        //             $(info.el).find('.fc-time').remove();
        //             e.html(e.text());
        //         } else {
        //             var e = $(info.el).find('.fc-title');
        //             e.html(e.text());
        //         }
        //     } else if(info.view.type == 'dayGridMonth') {
        //         if($(info.el).hasClass('cross_event')) {
        //             console.log('found');
        //             $(info.el).html('');
        //         } 
        //     }
        // }
      },
     /* eventClick: function(info) {
        if( ! info.event._def.allDay) {
            var e = $(info.el).find('img.unvimg');
            if(e.hasClass('u')) {
                e.attr('src', 'images/available.png').removeClass('u').addClass('a');
            } else {
                e.attr('src', 'images/unavailable.png').removeClass('a').addClass('u');
            }
        }
      }*/
    });
    // new Draggable(calendarEl, {
    //   itemSelector: '.item-class',
    //   eventData: function(eventEl) {
    //     return {
    //       title: eventEl.innerText,
    //       duration: '02:00',
    //       date: '2019-10-07'
    //     };
    //   }
    // });
    calendar.render();
  });

</script> 
 
@endsection

@section('header')
	@include('includes.header')
@endsection
@section('content')

    <section class="prfile-bnr-sec">
    <div class="prfil-info">
    
        <div class="container">
        <div class="row rwmrgn" style="display:block;">
        
        <div class="porflabt">
        <div class="prlpic"><img src="{{ @$user->profile_pic ? URL::to('storage/app/public/uploads/profile_pic').'/'.@$user->profile_pic: URL::to('public/frontend/images/no_img.png') }}"></div>
        
        <div class="prfldtl">
            <h5>{{ @$user->nick_name ? @$user->nick_name : @$user->name }}</h5>
            @if(@$user->crp_no!=null || @$user->crp_no!="")
                <p>{{ @$user->crp_no }}</p>
            @endif

                <ul>
                    <li><img src="{{ URL::to('public/frontend/images/pstr.png') }}"></li>
                    <li><img src="{{ URL::to('public/frontend/images/pstr.png') }}"></li>
                    <li><img src="{{ URL::to('public/frontend/images/pstr.png') }}"></li>
                    <li><img src="{{ URL::to('public/frontend/images/pstr.png') }}"></li>
                    <li><img src="{{ URL::to('public/frontend/images/pstr.png') }}"></li>
                    <li>(35)</li>
                </ul>
        </div>
        </div>
        
        <div class="rghtinoo">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                @php
                    $vrr=0; 
                @endphp
                @foreach(@$user->userDetails as $us)
                @if($vrr>=2)
                    @break
                @endif
                @if($us->level==0)
                    <li class="breadcrumb-item"><a href="javascript:void(0);">{{ @$us->categoryName->name }}</a></li>
                    @php
                        $vrr++; 
                    @endphp
                @else
                    <li class="breadcrumb-item active rtd" aria-current="page"><i class="fa fa-angle-right" aria-hidden="true"></i> {{ @$us->categoryName->name }}</li>
                    @php
                        $vrr++; 
                    @endphp
                @endif
                @endforeach
                
                
              </ol>
              
              
              
            </nav>
            
            
            
            <div class="jbinfo">
                <ul>
                    <li>
                        <span>Fees</span>
                        <p>R${{ @$user->rate_price }} / {{ @$user->rate=="M" ? "Minute": "Hour" }}</p>
                    </li>
                    <li>
                        <span>Speaks </span>
                        <p>English, Spanish</p>
                    </li>
                    
                    <li>
                        <span>City</span>
                        <p>Kolkata</p>
                    </li>
                </ul>
            </div>
            
            
            
        </div>
        
        </div>
        </div>
    
    </div>
</section>


<div class="prfltab">
    
    <div class="container">
        <div class="row rwmrgn">
        
            <div class="lfttbvrsn">
                <ul>
                    <li>
                        <a href="javascript:void(0);" id="top">
                            <span><img src="{{URL::to('public/frontend/images/p1.png')}}"></span>
                            Personal Information
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" id="top1">
                            <span><img src="{{URL::to('public/frontend/images/p2.png')}}"></span>
                            Education
                        </a>
                    </li>
                     <li>
                        <a href="javascript:void(0);" id="top2">
                            <span><img src="{{URL::to('public/frontend/images/p3.png')}}"></span>
                            Experience
                        </a>
                    </li>
                    
                         <li>
                            <a href="javascript:void(0);" id="top3">
                                <span><img src="{{URL::to('public/frontend/images/p4.png')}}"></span>
                                Reviews
                            </a>
                        </li>
                    
                     <li>
                        <a href="javascript:void(0);" id="top4">
                            <span><img src="{{URL::to('public/frontend/images/p5.png')}}"></span>
                            Calendar
                        </a>
                    </li>
                </ul>
            </div>
            
            
            <div class="rtsidebtn">
                <ul>
                    <li><a href="#" class="shre"><img src="{{URL::to('public/frontend/images/shre.png')}}"></a></li>
                    <li><a href="javascript:void(0);" class="addfvrt">Add to Favorite</a></li>
                    <!--<li><a href="#">Send a Message</a></li>-->
                    <li><a href="#">Booking Request</a></li>
                    
                </ul>
            </div>
            
            
        </div>
    </div>
    
</div>



<div class="pblcprfl-bdy">
    <div class="container">
        <div class="row rwmrgn">
        
            <div class="pbdy-box" id="dwn">
                <h3>About</h3>
                <p class="pra">{!! @$user->description !!}</p>
            </div>
            
            <div class="pbdy-box">
                <h3>Speciality</h3>
                <div class="skilsplty">
                    <ul>
                        {{-- {{ dd(@$user) }} --}}
                        @if(sizeof($user->userDetails)>0)
                            @foreach($user->userDetails as $uu)
                                <li>{{ @$uu->categoryName->name }}</li>
                            @endforeach
                        @else
                            <li>Skills Currently not enlisted</li>
                        @endif
                    </ul>
                </div>
            </div>
            
            
                <div class="pbdy-box" id="dwn3">
                    <h3>Reviews</h3>
                    
                    
                    <div class="boxed-list margin-bottom-60">

                        <ul class="boxed-list-ul">
                        <li>
                            <div class="boxed-list-item">
                                <!-- Content -->
                                <div class="item-content">
                                    <h4>Lorem ipsum dolor sit amet <span>Anonymous Employee</span></h4>
                                    
                                        <div class="item-details margin-top-10">
                                            <div class="star-rating" data-rating="5.0">
                                            <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>
                                            <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>
                                            <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>
                                            <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>
                                            <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>
                                            </div>
                                            <div class="detail-item"><i class="fa fa-calendar-o" aria-hidden="true"></i> March 2019</div>
                                        </div>
                                   
                                    <div class="item-description">
                                        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident,</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="boxed-list-item">
                                <!-- Content -->
                                <div class="item-content">
                                    <h4>Vero Eos et Accusamus Et Iusto Odio</span></h4>
                                    <div class="item-details margin-top-10">
                                        <div class="star-rating" data-rating="5.0">
                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>
                                        </div>
                                        <div class="detail-item"><i class="fa fa-calendar-o" aria-hidden="true"></i> April 2019</div>
                                    </div>
                                    <div class="item-description">
                                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae.</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>

                    

                </div>
                    
                    
                </div>
            
            
            
            <div class="pbdy-box" id="dwn1">
                <h3>Education</h3>
                @if(sizeof($user->userQualification)>0)
                @foreach(@$user->userQualification as $us)
                    
                        <div class="eductn-sec">
                            <h5>{{ @$us->degree }}</h5>
                            <li><img src="{{URL::to('public/frontend/images/e1.png')}}"> {{ @$us->university }}</li>
                            <li><img src="{{URL::to('public/frontend/images/e2.png')}}"> {{ @$us->from_month.' '.@$us->from_year }} - {{ @$us->pursuing =="Y" ? 'Till now': @$us->to_month.' '.@$us->to_year }}</li>
                            
                            {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In a dolor the  accumsan massa sed, consectetur turpis. Nunc facilisis, diam et efficitur tristique,blandit  consect etur turpis. Nunc facilisis, diam et efficitur tristique,blandit </p> --}}
                        
                        </div>
                   
                @endforeach
                 @else
                        <div class="eductn-sec">
                            <h5>Information Not Found</h5>
                        </div>
                    @endif
                
            
            </div>
            
            <div class="pbdy-box" id="dwn2">
                <h3>Experience - {{ @$user->experience  ? @$user->experience." Month(s)": "Not Found"}} </h3>
                @if(sizeof(@$user->userExperience)>0)
                    @foreach(@$user->userExperience as $us)
                    <div class="eductn-sec">
                        <h5>{{ @$us->role }}</h5>
                        <li><img src="{{URL::to('public/frontend/images/e1.png')}}"> {{ @$us->organization }}</li>
                        <li><img src="{{URL::to('public/frontend/images/e2.png')}}"> {{ @$us->from_month.' '.@$us->from_year }} - {{ @$us->pursuing =="Y" ? 'Till now': @$us->to_month.' '.@$us->to_year }}</li>
                        
                        <p>{!! @$us->description !!}</p>
                    </div>
                    @endforeach
                @else
                    <div class="eductn-sec">
                        <h5>Information Not Found</h5>
                    </div>
                @endif
            
            </div>
            
            
            
            <div class="pbdy-box" id="dwn4">
                <h3>Calendar</h3>
                
                <div class="right_dash" style="width:100%;">
            <div class="main-form">
            
                <div class="avll">
                    <p><img src="{{URL::to('public/frontend/images/avl.png')}}"> Available</p>
                    <p><img src="{{URL::to('public/frontend/images/unvl.png')}}"> Not Available</p>
                </div>
                
            <div id='wrap'>


            <div id='calendar'></div>
        
            <div style='clear:both'></div>
        
          </div>    
            
                
            </div>
        </div>
                
                
            </div>
        
        
        </div>
    </div>
</div>


<section class="guidance-area" style="border-bottom:1px solid #ccc;">
    <div class="container">
        <div class="row">
            <div class="page-h2">
                <h2>View Other Professionals</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vitae pharetra erat. Fusce quis suscipit leo. Nulla scelerisque erat in dolor laoreet fermentum.<br> Nam at efficitur tortor, vitae porttitor mi. Morbi sit amet velit nec leo imperdiet scelerisque.</p>
            </div>
            <div class="all-guidance">
                <div id="owl-demo-1" class="owl-carousel owl-theme">
                @foreach(@$simm as $us)
                <div class="item">
                  <div class="guiede-box">
                        <div class="guide-image"><img src="{{ URL::to('public/frontend/images/guide-image1.png') }}" alt=""></div>
                        <div class="guide-dtls">
                            <div class="guide-intro">
                                <h3>
                                    {{ @$us->nick_name ? @$us->nick_name : @$us->name }}
                                      
                                    <ul>
                                        <li><img src="{{ URL::to('public/frontend/images/star.png') }}" alt=""></li>
                                        <li><img src="{{ URL::to('public/frontend/images/star.png') }}" alt=""></li>
                                        <li><img src="{{ URL::to('public/frontend/images/star.png') }}" alt=""></li>
                                        <li><img src="{{ URL::to('public/frontend/images/star.png') }}" alt=""></li>
                                        <li><img src="{{ URL::to('public/frontend/images/star1.png') }}" alt=""></li>
                                        <li>(35)</li>
                                    </ul>
                                    
                                </h3>
                                @if(@Auth::guard('web')->user()->crp_no==null || @Auth::guard('web')->user()->crp_no=="")
                                <p>{{@Auth::guard('web')->user()->crp_no  }}</p>
                                @endif
                            </div>
                            <div class="guide-more">
                                <ul>
                                    <li>
                                        <span>Feees</span>
                                        <p>${{ @$us->rate_price }} / Hour</p>
                                    </li>
                                    <li>
                                        <span>Speaks </span>
                                        <p>English, Spanish</p>
                                    </li>
                                </ul>
                                <a class="guide-btn pull-left" href="#">Request A Session </a>
                                <a class="guide-btn pull-right" href="{{ route('pblc.pst',['slug'=>@$us->slug]) }}">View Profile </a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
               
              </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
	@include('includes.footer')
    <script src="{{ URL::to('public/frontend/js/jquery.com_ui_1.11.4_jquery-ui.js') }}"></script>

<script>
    $(function() {
        $( "#slider-range" ).slider({
            range: true,
            min: 0,
            max: 1000,
            values: [ 75, 300 ],
            slide: function( event, ui ) {
                $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $." + ui.values[ 1 ] );
            }
        });
        $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
            " - $" + $( "#slider-range" ).slider( "values", 1 ) );
    });
</script> 

<script>
    $("#top").click(function() {
      $("html, body").animate({ scrollTop: $('#dwn').offset().top }, "slow");
      return false;
    });

    $("#top1").click(function() {
      $("html, body").animate({ scrollTop: $('#dwn1').offset().top }, "slow");
      return false;
    });

    $("#top2").click(function() {
      $("html, body").animate({ scrollTop: $('#dwn2').offset().top }, "slow");
      return false;
    });

    $("#top3").click(function() {
      $("html, body").animate({ scrollTop: $('#dwn3').offset().top }, "slow");
      return false;
    });

    $("#top4").click(function() {
      $("html, body").animate({ scrollTop: $('#dwn4').offset().top }, "slow");
      return false;
    });
</script>
<script>
    $('.addfvrt').click(function(){
        var reqData = {
          'jsonrpc' : '2.0',
          '_token' : '{{csrf_token()}}',
          'params' : {
                'id' : {{ @$user->id }}
            }
        };
        $.ajax({
            url: "{{ route('add.favourite') }}",
            method: 'post',
            dataType: 'json',
            data: reqData,
            success: function(response){
                if(response.status==1) {
                    toastr.success('Successfully added to favourite list');
                }
                if(response.status==3){
                    localStorage.removeItem('slug');
                    localStorage.setItem('slug','{{ @$user->slug }}');
                    location.href = "{{ route('login') }}";
                }

                if(response.status==2){
                    toastr.info('You have been already added {{ @$user->nick_name ? @$user->nick_name : @$user->name }} into your favourite list');
                } 
            }, error: function(error) {
                console.error(error);
            }
        });
    });
</script>
@endsection