@extends('layouts.app')
@section('title', (@$user->nick_name  ? @$user->nick_name : @$user->name).' public profile')
@section('style')
@include('includes.style')
<link href='{{ URL::to('public/frontend/packages/core/main.css') }}' rel='stylesheet' />
<link href='{{ URL::to('public/frontend/packages/daygrid/main.css') }}' rel='stylesheet' />
<link href='{{ URL::to('public/frontend/packages/timegrid/main.css') }}' rel='stylesheet' />
<link href='{{ URL::to('public/frontend/packages/list/main.css') }}' rel='stylesheet' />
@endsection
@section('scripts')
@include('includes.scripts')
<script src='{{ URL::to('public/frontend/packages/core/main.js') }}'></script>
<script src='{{ URL::to('public/frontend/packages/interaction/main.js') }}'></script>
<script src='{{ URL::to('public/frontend/packages/daygrid/main.js') }}'></script>
<script src='{{ URL::to('public/frontend/packages/timegrid/main.js') }}'></script>
<script src='{{ URL::to('public/frontend/packages/list/main.js') }}'></script>
<script src="{{ URL::to('public/frontend/js/moment.min.js') }}"></script>
<style type="text/css">
    .fc-available {
    background: #4f4f4e !important;
    color: #fff !important;
    }
    .fc-more-popover .fc-event-container{
        height: 167px;
        overflow-y: scroll;
    }
    .fc-event, .fc-event-dot {
        background-color: #ddd !important;
        color: #000 !important;
        border: 1px solid transparent !important;
    }
    .fc-event:hover, .fc-event-dot:hover {
        background-color: #1781d2 !important;
        color: #fff !important;
        transition: background-color 300ms linear;
        transition: color 300ms linear;
        border: 1px solid #3788d8 !important;
    }
    .unavlbl_slot{
        background: url('http://localhost/netbhepreview/public/frontend/images/unavlbl_slot.png') no-repeat 50% 50%;
        background-size: 28px;
    }
</style>
<script>
    $(document).ready(function() {
        $("#owl-demo-1").owlCarousel({
            margin: 25,
            nav: true,
            loop: true,
            responsive: {
              0: {
                items: 1
              },
              768: {
                items:2
              },
              1078: {
                items:3
              },
              1200: {
                items:4
              },
            }
          });
        })
</script>
<script>
    var availiabilityDays = {!! $avlDay !!};
    var availiabilityTimes1 = {!! $avlTime1 !!};
    document.addEventListener('DOMContentLoaded', function() {
    var Calendar = FullCalendar.Calendar;
    var Draggable = FullCalendarInteraction.Draggable

    var events = [];
    var timeS;
    console.log(availiabilityTimes1);
    availiabilityTimes1.forEach(function(item, index) {
        events.push({
            'title': item.time_start+' - '+item.time_end,
            'start': item.slot_start,
            'end': item.slot_end,
            'name': item.datecls
            // 'start': item.date+'T'+item.from_time,
            // 'end': item+'T'+item.to_time
        });
    });


    // $(document).on("mousemove", ".fc-event-container", function(){
    //     console.log("Mouse Move");
    //     var date = $(this).find('.fc-day-grid-event').data('date');
    //     $('.'+date).hide();
    // });
    // $(document).off("mouseover").on("mouseenter", ".fc-event-container", function(e){
    //     console.log("Mouse Enter");
    //     var date = $(this).find('.fc-day-grid-event').data('date');
    //     $('.'+date).hide();
    // });
    // $(document).off("mouseout").on("mouseleave", ".fc-event-container", function(e){
    //     console.log("Mouse Leave");
    //     var date = $(this).find('.fc-day-grid-event').data('date');
    //     $('.'+date).show();
    // });

    $("body").on({
        mousemove: function (e) {
            console.log("MMove");
            var date = $(this).find('.fc-day-grid-event').data('date');
            $('.'+date).hide();
            if(!$(this).hasClass('on')) $(this).addClass('on');
        },
        mouseenter: function (e) {
            console.log("Mouse Enter");
            var date = $(this).find('.fc-day-grid-event').data('date');

            $('.on').each(function(i, ele){
                var dt = $(this).find('.fc-day-grid-event').data('date');
                console.log(dt);
                if(!$(ele).hasClass(date)){
                    $('.'+dt).show();
                    $(ele).removeClass('on');
                }
            });
            if(!$(this).hasClass('on')) $(this).addClass('on');
            $('.'+date).hide();
        },
    }, ".fc-event-container");

    $("body").on("mouseleave", ".fc-event-container", function(e){
        console.log("Mouse Leave");
        // $(this).removeClass('on');
        // if(!$(this).hasClass('on')){
        //     var date = $(this).find('.fc-day-grid-event').data('date');
        //     setTimeout(() => {
        //         $('.'+date).show();
        //     }, 1000);
        // }
    });

    // $(document).delegate('.fc-widget-content', 'mouseover', function(){
    //     var date = $(this).data('date');
    //     $('.date-'+date).css('visibility', 'visible');
    // });
    // $(document).delegate('.fc-widget-content', 'mouseout', function(){
    //     var date = $(this).data('date');
    //     $('.date-'+date).css('visibility', 'hidden');
    // });

    // $(document).delegate('.fc-event-container', 'mouseover', function(){
    //     var cls = $(this).find('.fc-day-grid-event').data('date')
    //     $('.'+cls).css('visibility', 'visible');
    //     console.log(cls);
    // });
    // $(document).delegate('.fc-event-container', 'mouseout', function(){
    //     var cls = $(this).find('.fc-day-grid-event').data('date')
    //     $('.'+cls).css('visibility', 'hidden');
    // });

    var calendarEl = document.getElementById('calendar');
    var calendar;
    var i = 1;
    var event = [];
    var ardt = [];

    calendar = new Calendar(calendarEl, {
        eventLimit: true, // for all non-TimeGrid views
        locale: 'pt-br',
        plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list' ],
        header: {
            left: 'prev,next today',
            center: 'title',
            right: ''
        },
        views: {
            timeGrid: {
                eventLimit: 6 // adjust to 6 only for timeGridWeek/timeGridDay
            }
        },
        events: events,
        eventRender: function(info) {
            // console.log(info.event.extendedProps.name);
            // console.log(info.el.parentNode.parentNode);
            // console.log($(event.el).parent());
            info.el.setAttribute('data-date', info.event.extendedProps.name);
            // if(!event.el.parentNode.classList.contains('dated')){
                // event.el.parentNode.classList.add('dated');
                // event.el.parentNode.classList.add(event.type);
            // }
        },
        selectable: true,
        firstDay: 6,
        select: function(info) {
            // checkDate(info.startStr);
        },
        click: function(info) {
            //console.log(info)
            checkDate(info.startStr);
        },
        eventClick: function(info) {
            checkDate(moment(info.event.start).format('YYYY-MM-DD'), info.event.title);
        },
        dayRender: function(event) {
            const day = moment(event.date).format('YYYY-MM-DD');
            var todayDate = new Date();
            if ($.inArray(day, availiabilityDays) > -1) {
                $(event.el).css('background-color', '#ddd');
                $(event.el).addClass(event.datecls);
            } else {
                $(event.el).css('background-color', '#a1c7e4');
                $(event.el).addClass('unavlbl_slot');
            }
        },
    });

    calendar.render();
    });
    function callMe(val){
    alert(val);
    }

</script>
<script>
    $(function() {
        $("#datepicker11").datepicker({dateFormat: "yy-mm-dd",
           defaultDate: new Date(),
           maxDate:new Date(),
           onClose: function( selectedDate ) {
           //$( "#datepicker1").datepicker( "option", "minDate", selectedDate );
          }
        });
    });
</script>
<script>
    $(function() {
        $("#datepicker").datepicker({dateFormat: "dd-mm-yy",
            defaultDate: new Date(),
            showButtonPanel: true,
            showMonthAfterYear: true,
            showWeek: true,
            showAnim: "drop",
            constrainInput: true,
            yearRange: "-90:",
            minDate:new Date(),
            onClose: function( selectedDate ) {
               $( "#datepicker1").datepicker( "option", "minDate", selectedDate );
            }
        });
        $("#datepicker1").datepicker({dateFormat: "dd-mm-yy",
            defaultDate: new Date(),
            showButtonPanel: true,
            showMonthAfterYear: true,
            showWeek: true,
            showAnim: "drop",
            constrainInput: true,
            yearRange: "-90:",
            onClose: function( selectedDate ) {
                $( "#datepicker" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
</script>
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')
<section class="prfile-bnr-sec">
    <div class="prfil-info">
        <div class="container">
            <div class="row rwmrgn" style="display:block;">
                <div class="porflabt">
                    <div class="prlpic"><img src="{{ @$user->profile_pic ? URL::to('storage/app/public/uploads/profile_pic').'/'.@$user->profile_pic: URL::to('public/frontend/images/no_img.png') }}" class="img-responsive img-fluid"></div>
                    <div class="prfldtl">
                        <h5>{{ @$user->nick_name ? @$user->nick_name : @$user->name }}</h5>
                        @if(@$user->introductory_video)
                        <a href="javascript:;" id="introduction_video"><i class="fa fa-film"></i> @lang('site.introductory_video')</a>
                        @endif
                        @if(@$user->crp_no!=null || @$user->crp_no!="")
                        <p>{{ @$user->crp_no }}</p>
                        @endif
                        <ul>
                            {!! getStars(@$user->avg_review) !!}
                            <li>({{ $user->total_review ?? 0 }})</li>
                            {{-- @if(@$user->total_review>0)
                                @for($i=0;$i<$user->avg_review;$i++)
                                <li><img src="{{ URL::to('public/frontend/images/star.png') }}" alt=""></li>
                                @endfor
                                @for($j=$user->avg_review;$j<5;$j++)
                                <li><img src="{{ URL::to('public/frontend/images/star1.png') }}" alt=""></li>
                                @endfor
                            @else
                            @for($j=0;$j<=4;$j++)
                            <li><img src="{{ URL::to('public/frontend/images/star1.png')}}" alt=""></li>
                            @endfor
                            @endif
                            @if(@$user->total_review>0)
                            <li>({{@$user->total_review}})</li>
                            @else
                            <li>(0)</li>
                            @endif --}}
                        </ul>
                    </div>
                </div>
                <div class="rghtinoo">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            @php
                            $vrr=0;
                            @endphp
                            @foreach(@$user->userDetails as $us)
                            @if($vrr>=2)
                            @break
                            @endif
                            @if($us->level==0)
                            <li class="breadcrumb-item"><a href="javascript:void(0);">{{ @$us->categoryName->name }}</a></li>
                            @php
                            $vrr++;
                            @endphp
                            @else
                            <li class="breadcrumb-item active" aria-current="page"><i class="fa fa-angle-right" aria-hidden="true"></i> {{ @$us->categoryName->name }}</li>
                            @php
                            $vrr++;
                            @endphp
                            @endif
                            @endforeach
                        </ol>
                    </nav>
                    <div class="jbinfo">
                        <ul>
                            <li>
                                <span>@lang('client_site.fees')</span>
                                @if(@$user->rate=="M")
                                    @php
                                        $time = $user->rate_price;
                                        $call_durations = $user->get_call_durations()->pluck('call_duration')->toArray();
                                    @endphp
                                    @if( count($user->get_call_durations()->pluck('call_duration_id')->toArray()) > 0)
                                        @foreach($call_durations as $call_dur)
                                            <p>R$ {{ number_format((float)$time*$call_dur, 2, '.', '') }} / {{$call_dur}} @lang("client_site.minutes") </p>
                                        @endforeach
                                    @else
                                        <p>R$ {{ number_format((float)$time*60, 2, '.', '') }} / 60 @lang("client_site.minutes") </p>
                                    @endif
                                @else
                                    @php
                                        $time = $user->rate_price/60;
                                        $call_durations = $user->get_call_durations()->pluck('call_duration')->toArray();
                                    @endphp
                                    @if( count($user->get_call_durations()->pluck('call_duration_id')->toArray()) > 0)
                                        @foreach($call_durations as $call_dur)
                                            <p>R$ {{ number_format((float)$time*$call_dur, 2, '.', '') }} / {{$call_dur}} @lang("client_site.minutes") </p>
                                        @endforeach
                                    @else
                                        <p>R$ {{ number_format((float)$time*60, 2, '.', '') }} / 60 @lang("client_site.minutes") </p>
                                    @endif
                                @endif
                            </li>
                            <li>
                                <span>@lang('client_site.speaks') </span>
                                <p>
                                    @php
                                    $j=1;
                                    @endphp
                                    @foreach(@$user->userLang as $u)
                                    {{ @$u->languageName->name}} @if(sizeof(@$user->userLang)> $j) , @endif
                                    @php
                                    $j++;
                                    @endphp
                                    @endforeach
                                </p>
                            </li>
                            <li>
                                <span>@lang('client_site.city')</span>
                                <p>{{@$user->city}}</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="prfltab mmt_rmtopp01">
    <div class="container">
        <div class="row rwmrgn">
            <div class="lfttbvrsn">
                <ul>
                    <li class="mmt_rmtopp02">
                        <a href="javascript:void(0);" id="top">
                        <span><img src="{{URL::to('public/frontend/images/p1.png')}}"></span>
                        <p>@lang('client_site.personal_information')</p>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" id="top1">
                        <span><img src="{{URL::to('public/frontend/images/p2.png')}}"></span>
                        <p>@lang('client_site.education')</p>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" id="top2">
                        <span><img src="{{URL::to('public/frontend/images/p3.png')}}"></span>
                        <p>@lang('client_site.experience')</p>
                        </a>
                    </li>

                    <div class="devideted"></div>
                    <li>
                        <a href="javascript:void(0);" id="top3">
                        <span><img src="{{URL::to('public/frontend/images/p4.png')}}"></span>
                        <p>@lang('client_site.reviews')</p>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" id="top4">
                        <span><img src="{{URL::to('public/frontend/images/p5.png')}}"></span>
                        <p>@lang('client_site.calendar')</p>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" id="top5">
                        <span><img src="{{URL::to('public/frontend/images/p6.png')}}"></span>
                        <p>@lang('client_site.products')</p>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="rtsidebtn ml-auto">
                <ul>
                    <li>
                        <div class="st-custom-button" data-network="sharethis" data-url="{{ url()->current() }}">
                            <a href="javascript:;" class="shre"><img src="{{URL::to('public/frontend/images/shre.png')}}"></a>
                        </div>
                    </li>
                    <li><a href="javascript:void(0);" class="addfvrt">@lang('client_site.add_to_favorite')</a></li>

                    <li><a class="ssnbtnslg" data-slug="{{ @$user->slug }}" href="javascript::void(0);">@lang('client_site.booking_request')</a></li>
                    <li><a href="{{route('compose.msg',['id'=>@$user->id])}}">{{__('site.send_a_message')}}</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="pblcprfl-bdy">
    <div class="container">
        <div class="row rwmrgn">
            <div class="pbdy-box" id="dwn">
                <h3>@lang('client_site.about_me')</h3>
                <p class="pra" style="white-space: pre-wrap;">{!! strip_tags(@$user->description, '<br>') !!}</p>
            </div>
            <div class="pbdy-box">
                <h3>@lang('client_site.speciality')</h3>
                <div class="skilsplty">
                    <ul>
                        {{-- {{ dd(@$user) }} --}}
                        @if(sizeof($user->userDetails)>0)
                        @foreach($user->userDetails as $uu)
                        <li>{{ @$uu->categoryName->name }}</li>
                        @endforeach
                        @else
                        <li>@lang('client_site.skills_currently_not_enlisted')</li>
                        @endif
                    </ul>
                </div>
            </div>
            <div class="pbdy-box" id="dwn1">
                <h3>@lang('client_site.education')</h3>
                @if(sizeof($user->userQualification)>0)
                @foreach(@$user->userQualification as $us)
                <div class="eductn-sec">
                    {{-- <h5>{{ @$us->degree }}</h5>
                    <li><img src="{{URL::to('public/frontend/images/e1.png')}}"> {{ @$us->university }}</li>
                    @if(@$us->from_year != 0)
                    <li><img src="{{URL::to('public/frontend/images/e2.png')}}"> {{ @$us->from_month.' - '.@$us->from_year }}
                        @endif --}}
                    <li>{{ @$us->degree }} - {{ @$us->university }}
                    @if(@$us->from_year != 0)- {{@$us->from_year }} @endif
                        {{-- - {{ @$us->pursuing =="Y" ? 'Till now': @$us->to_month.' - '.@$us->to_year }} --}}
                    </li>
                    {{--
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In a dolor the  accumsan massa sed, consectetur turpis. Nunc facilisis, diam et efficitur tristique,blandit  consect etur turpis. Nunc facilisis, diam et efficitur tristique,blandit </p>
                    --}}
                </div>
                @endforeach
                @else
                <div class="eductn-sec">
                    <h5>@lang('client_site.information_not_found')</h5>
                </div>
                @endif
            </div>
            <div class="pbdy-box" id="dwn2">
                <h3>@lang('client_site.experience')  {{ @$user->experience  ? ' - '.@$user->experience." Month(s)": ""}} </h3>
                @if(sizeof(@$user->getExperiences)>0)
                @foreach(@$user->getExperiences as $us)
                <div class="eductn-sec">
                    {{--
                    <h5>{{ @$us->role }}</h5>
                    <li><img src="{{URL::to('public/frontend/images/e1.png')}}"> {{ @$us->organization }}</li>
                    <li><img src="{{URL::to('public/frontend/images/e2.png')}}"> {{ @$us->from_month.' '.@$us->from_year }} - {{ @$us->pursuing =="Y" ? 'Till now': @$us->to_month.' '.@$us->to_year }}</li>
                    --}}
                    <p>{!! @$us->experience !!}</p>
                </div>
                @endforeach
                @else
                <div class="eductn-sec">
                    <h5>@lang('client_site.information_not_found')</h5>
                </div>
                @endif
            </div>
            <div class="pbdy-box">
                <h3>@lang('site.redemption_policy')</h3>
                <div class="eductn-sec">
                    @if(@$user->reschedule_time)
                    <p>@lang('site.redemption_policy_message_1'){{@$user->reschedule_time}}@lang('site.redemption_policy_message_2')</p>
                    @endif
                </div>

            </div>
            <div class="pbdy-box" id="dwn4">
                <h3>@lang('client_site.calendar')</h3>
                <div class="right_dash" style="width:100%;">
                    <div class="main-form">
                        <div class="avll">
                            <p><img src="{{URL::to('public/frontend/images/avl1.png')}}"> @lang('client_site.available')</p>
                            <p><img src="{{URL::to('public/frontend/images/unvl1.png')}}"> @lang('client_site.not_available')</p>
                        </div>
                        <div id='wrap'>
                            <div id='calendar'></div>
                            <div style='clear:both'></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pbdy-box comment_box" id="dwn3">
                <h3>@lang('client_site.reviews')</h3>
                <div class="boxed-list margin-bottom-60">
                    @if(sizeof(@$userReview)>0)
                    <ul class="boxed-list-ul">
                        @foreach(@$userReview as $ur)
                        <li>
                            <div class="boxed-list-item">
                                <!-- Content -->
                                <div class="item-content">
                                    <h4>
                                        {{-- {{@$ur->review_heading}} --}}
                                        {{-- <span> --}}
                                        {{-- {{@$ur->reviewedUserName->name}} --}}
                                        {{-- </span> --}}
                                        @php
                                        $splitName = explode(' ', $ur->reviewedUserName->name, 2);
                                        $first_name = $splitName[0];
                                        @endphp
                                        {{ @$ur->reviewedUserName->nick_name ? @$ur->reviewedUserName->nick_name : @$first_name}}
                                    </h4>
                                    <div class="item-details margin-top-10">
                                        <div class="star-rating" data-rating="{{@$ur->points}}">
                                            @if($ur->points>0)
                                            @for($i=0;$i<$ur->points;$i++)
                                                <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>
                                                @endfor
                                                @for($j=$ur->points;$j<5;$j++) <span class="star"><i class="fa fa-star-o"
                                                        aria-hidden="true"></i></span>
                                                    @endfor
                                                    @endif
                                                    @if($ur->points<=0) @for($k=0;$k<=4;$k++) <span class="star"><i
                                                            class="fa fa-star-o" aria-hidden="true"></i></span>
                                                        @endfor
                                                        @endif
                                        </div>
                                        <div class="detail-item"><i class="fa fa-calendar-o" aria-hidden="true"></i>
                                            {{date('d-m-Y', strtotime(@$ur->created_at))}}</div>
                                    </div>
                                    <div class="item-description">
                                        <p style="white-space: pre-wrap;">{!!strip_tags(@$ur->comments, '<br>')!!}</p>
                                    </div>
                                    @if(@$user->id == Auth::id())
                                    <div class="reply">
                                        @if(@$ur->reply)
                                            <span class="text-secondary">{{ @Auth::user()->nick_name ? @Auth::user()->nick_name : explode(" ",@Auth::user()->name)[0] }} : <span> {{@$ur->reply}}
                                        @else
                                            <button class="btn btn-primary reply_btn" type="button" data-id="{{$ur->id}}">{{__('client_site.public_reply')}}</button>
                                            <div class="reply_form reply_form_{{$ur->id}}" style="display:none;">
                                                <form action="{{ route('reply.comment',[$ur->id]) }}" method="post">
                                                    @csrf
                                                    <textarea type="text" name="reply" class="form-control"></textarea>
                                                    <input type="submit" value="{{__('client_site.public_post')}}" class="btn btn-primary">
                                                </form>
                                            </div>
                                        @endif
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                    @else
                    <div class="eductn-sec">
                        <h5>@lang('client_site.review_information_not_found')</h5>
                    </div>
                    @endif
                </div>
            </div>
            <div class="pbdy-box" id="dwn5">
                <h3>@lang('client_site.products')</h3>
                @if(count($products)>0)
                <div class="right_dash" style="width:100%;">
                    <section class="product-area px-4">
                        <div class="container">
                            <div class="row">
                                <div id="owl-demo-6" class="owl-carousel owl-theme">
                                    @php
                                    $cartProduct=getAllCart();
                                    @endphp
                                    @foreach(@$products as $product)
                                    <div class="item">
                                        <div class="product-box">
                                            <div class="product-cover-image image_resize">
                                                <a href="{{route('product.details',@$product->slug)}}">
                                                    <img src="{{ URL::to('storage/app/public/uploads/product_cover_images').'/'.@$product->cover_image }}">
                                                </a>
                                                </div>
                                            <div class="product-dtls">
                                                <div class="product-intro">
                                                    <a href="{{route('product.details',@$product->slug)}}">
                                                        <h3>
                                                            @if(strlen(@$product->title) < 50)
                                                            {{ @$product->title }}
                                                            @else {{substr(@$product->title, 0, 47 ) . '...'}}
                                                            @endif
                                                        </h3>
                                                    </a>
                                                    <p><i class="fa fa-tag" aria-hidden="true"></i> {{ @$product->category->category_name }} <i class="fa fa-angle-right" aria-hidden="true"></i> {{ @$pro->subCategory->name }}</p>
                                                    <p><i class="fa fa-user" aria-hidden="true"></i> {{ @$product->professional->name }}</p>
                                                </div>
                                                <div class="rrt_stll01">
                                                    @php $month=[ "0", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ] @endphp
                                                    <p class="small_dates"><strong>@lang('client_site.sale_starts_on'): </strong>
                                                        {{date('d',strtotime(@$product->purchase_start_date))}}
                                                        {{$month[(int)date('m',strtotime(@$product->purchase_start_date))]}}
                                                        {{date('Y',strtotime(@$product->purchase_start_date))}}
                                                    </p>
                                                    <p class="small_dates">
                                                        <strong>@lang('client_site.sale_ends_on'): </strong>
                                                        @if(@$product->purchase_end_date)
                                                            {{date('d',strtotime(@$product->purchase_end_date))}}
                                                            {{$month[(int)date('m',strtotime(@$product->purchase_end_date))]}}
                                                            {{date('Y',strtotime(@$product->purchase_end_date))}}
                                                        @else
                                                            @lang('client_site.not_specified')
                                                        @endif
                                                    </p>
                                                    <p class="ttyu01 small_dates">
                                                        <strong>@lang('client_site.available_from'): </strong>
                                                        @if(@$product->course_start_date)
                                                            {{date('d',strtotime(@$product->course_start_date))}}
                                                            {{$month[(int)date('m',strtotime(@$product->course_start_date))]}}
                                                            {{date('Y',strtotime(@$product->course_start_date))}}
                                                        @else
                                                            @lang('client_site.not_specified')
                                                        @endif
                                                    </p>
                                                </div>
                                                @if(@$product->description)
                                                    <div class="product-more more_det">
                                                        <p class="pr-desc" data-desc="{{$product->description}}"></p>
                                                    </div>
                                                @endif
                                                <div class="price-and-more price-100">
                                                    @if(@$product->price)
                                                        <div class="price-lefts">
                                                            <span>@lang('client_site.fees')</span>
                                                            @if(@$product->discounted_price > 0)
                                                            <p><del>R${{@$product->price}}</del>&nbsp;
                                                                {{-- {{ ((@$product->price-@$product->discounted_price)/@$product->price)*100 }}% de desconto --}}
                                                                <?php  $a = ((@$product->price-@$product->discounted_price)/@$product->price)*100;
                                                                    echo ceil($a)
                                                                ?>% de desconto
                                                            </p>
                                                            <p style="font-size: 1rem;">R${{@$product->discounted_price}}</p>
                                                            @else
                                                            <p style="font-size: 1rem;">R${{@$product->price}}</p>
                                                            @endif
                                                        </div>
                                                    @endif
                                                    {{-- <a class="product-btn pull-right pull-right-1" href="{{route('product.details',@$product->slug)}}">@lang('client_site.view_more')</a> --}}
                                                    @if(@$product->professional_id != @auth()->user()->id)
                                                        @if(in_array(@$product->id , $orderedProducts))
                                                            <p class="text-secondary" style="font-size:1rem;">@lang('client_site.already_ordered')</p>
                                                        @else
                                                            @if(date('Y-m-d') >= date('Y-m-d', strtotime(@$product->purchase_start_date)))
                                                                @if(@$product->purchase_end_date && date('Y-m-d') >= date('Y-m-d', strtotime(@$product->purchase_end_date)))
                                                                    <p class="text-secondary" style="font-size:1rem;">@lang('client_site.sale_over')</p>
                                                                @else
                                                                <a href="javascript:void(0);" class="product-btn pull-left pull-right-1 addToCart cartClass{{$product->id}}" data-product="{{$product->id}}" @if(@in_array($product->id, @$cartProduct)) style="display: none" @endif> @lang('client_site.add_to_cart')</a>
                                                                <a href="{{route('product.cart')}}" class="product-btn pull-left pull-right-1 GoToCart{{$product->id}}" data-product="{{$product->id}}" @if(!@in_array($product->id, @$cartProduct)) style="display: none" @endif> @lang('client_site.go_to_cart')</a>
                                                                <a class="product-btn pull-left buyNow" href="javascript:void(0);" data-product="{{$product->id}}">{{__('site.buy_now')}}</a>
                                                                @endif
                                                            @else
                                                                <p class="text-secondary" style="font-size:1rem;">
                                                                    <span style="font-size:0.7rem; text-align:center;">@lang('client_site.available_from')</span><br>
                                                                    @php $month=[ "0", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ] @endphp
                                                                    {{date('d',strtotime(@$product->purchase_start_date))}}
                                                                    {{$month[(int)date('m',strtotime(@$product->purchase_start_date))]}}
                                                                    {{date('Y',strtotime(@$product->purchase_start_date))}}
                                                                </p>
                                                            @endif
                                                        @endif
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                @else
                <div class="eductn-sec">
                    <h5>@lang('client_site.product_not_found')</h5>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
<section class="guidance-area" style="border-bottom:1px solid #ccc;">
    <div class="container">
        <div class="row">
            <div class="page-h2">
                <h2>{{ $profPage->title }}</h2>
                <p style="white-space: pre-wrap;">{!! $profPage->description !!}</p>
            </div>
            <div class="all-guidance">
                <div id="owl-demo-1" class="owl-carousel owl-theme">
                    @foreach(@$simm as $us)
                    <div class="item">
                        <div class="guiede-box">
                            <div class="guide-image"><img src="{{ URL::to('storage/app/public/uploads/profile_pic/'.$us->profile_pic) }}" alt=""></div>
                            <div class="guide-dtls">
                                <div class="guide-intro">
                                    <h3>
                                        {{ @$us->nick_name ? @$us->nick_name : @$us->name }}
                                        <ul>
                                            {!! getStars($us->avg_review) !!}
                                            <li>({{ $us->total_review ?? 0 }})</li>
                                        </ul>
                                    </h3>
                                    <p>{!! @$us->specializationName->name ? $us->specializationName->name . ', ' . $us->crp_no : '&nbsp;' !!}</p>
                                    {{-- <p>{!!@$us->free_session_number>0 ? __('client_site.teacher_free_session') : '&nbsp;'!!}</p> --}}
                                </div>
                                <div class="guide-more">
                                    <ul>
                                        <li>
                                            <span>@lang('client_site.fees')</span>
                                            <p>R${{ @$us->rate_price }} / {{  @$us->rate=="M" ? __('client_site.minute') : __('client_site.hour') }}</p>
                                        </li>
                                        <li>
                                            <span>@lang('client_site.speaks') </span>
                                            <p>
                                                @php
                                                $j=1;
                                                @endphp
                                                @foreach(@$us->userLang as $u)
                                                {{ @$u->languageName->name }}{{ sizeof(@$us->userLang)>$j ? ', ' : '' }}
                                                @php
                                                $j++;
                                                @endphp
                                                @endforeach
                                            </p>
                                        </li>
                                    </ul>
                                    <a class="guide-btn pull-left" href="#">@lang('client_site.request_a_session') </a>
                                    <a class="guide-btn pull-right" href="{{ route('pblc.pst',['slug'=>@$us->slug]) }}">@lang('client_site.view_profile') </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Return to Top -->
<a href="javascript:" id="return-to-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
@if(@$user->introductory_video)
<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body" style="padding: 1rem;" id="yt-player">
                {{-- @if($user->introductory_video_type=='Y')
                <iframe frameborder="0" height="315" id="ytplayer" type="text/html" width="100%" src="https://www.youtube.com/embed/{{$user->introductory_video}}?rel=0&amp;autoplay=1&amp;modestbranding=1"></iframe>
                @elseif($user->introductory_video_type=='V')
                <video width="100%" height="315" controls>
                    <source src="{{ URL::to('storage/app/public/uploads/introductory_video').'/'.$user->introductory_video }}"
                        type="video/mp4">
                    Your browser does not support the video tag.
                </video>
                @elseif($user->introductory_video_type=='A')
                <audio style="width: 100%;" controls>
                    <source src="{{ URL::to('storage/app/public/uploads/introductory_video').'/'.$user->introductory_video }}"
                        type="audio/mpeg">
                    Your browser does not support the Audio tag.
                </audio>

                @endif --}}
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
@endif
@endsection
@section('footer')
@include('includes.footer')
<style type="text/css">
    #return-to-top i {
    color: #fff;
    margin: 0;
    position: relative;
    font-size: 19px;
    line-height: 40px;
    -webkit-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    -ms-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    transition: all 0.3s ease;
    }
    #return-to-top {
    position: fixed;
    bottom: 80px;
    right: 4%;
    background: #0095da;
    width: 40px;
    height: 40px;
    display: block;
    text-decoration: none;
    display: none;
    -webkit-transition: all 0.3s linear;
    -moz-transition: all 0.3s ease;
    -ms-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    transition: all 0.3s ease;
    text-align: center;
    z-index: 9999999;
    }
</style>
<script src="{{ URL::to('public/frontend/js/jquery.com_ui_1.11.4_jquery-ui.js') }}"></script>
<script>
    $('.reply_btn').click(function(){
        var id = $(this).data('id');
        $('.reply_form_'+id).toggle();
    });
    $('#introduction_video').click(function(){
        var video_type ='{{@$user->introductory_video_type ? @$user->introductory_video_type : ''}}';
        if(video_type=='Y'){
            $('#myModal .modal-body').html('<iframe frameborder="0" height="315" id="ytplayer" type="text/html" width="100%" src="https://www.youtube.com/embed/{{$user->introductory_video}}?rel=0&amp;autoplay=1&amp;modestbranding=1"></iframe>')
        }

       if(video_type=='V'){
            $('#myModal .modal-body').html(`<video width="100%" height="315" controls> <source src="{{ URL::to('storage/app/public/uploads/introductory_video').'/'.$user->introductory_video }}" type="video/mp4">
                Your browser does not support the video tag.
            </video>`);
        }

        else if(video_type=='A'){
            $('#myModal .modal-body').html(`<audio style="width: 100%;" controls>
                <source src="{{ URL::to('storage/app/public/uploads/introductory_video').'/'.$user->introductory_video }}"
                    type="audio/mpeg">
                Your browser does not support the Audio tag.
            </audio>`);
        }






    $('#myModal').modal('show');
    });
    $("#myModal").on('hidden.bs.modal', function (e) {
        console.log('close');
        $('#myModal .modal-body').empty();
    // $("#myModal iframe").attr("src", $("#myModal iframe").attr("src"));
    });
    var limit = 150;
    var productCount = "{{count(@$products)}}";
    function decodeEntities(encodedString) {
        var div = document.createElement('div');
        div.innerHTML = encodedString;
        return div.textContent;
    }
    $('#prod-desc').text(decodeEntities($('#prod-desc').attr('data-desc')));
    $('.pr-desc').each(function(){
        var str = decodeEntities($(this).attr('data-desc'));
        if(str.length > limit){
            str = str.substring(0,limit) + "...";
        }
        $(this).text(str);
    });
    $(window).scroll(function(){

            if ($(this).scrollTop() > 120) {
            // alert('hi');

            $('.top_head').css('background','#000');

            }
            else
            {
                 $('.top_head').css('background','rgba(0,0,0,0.4)');
            }
        });
</script>
<script>
    // ===== Scroll to Top ====
    $(window).scroll(function() {
    if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
        $('#return-to-top').fadeIn(200);    // Fade in the arrow
    } else {
        $('#return-to-top').fadeOut(200);   // Else fade out the arrow
    }
    });
    $('#return-to-top').click(function() {      // When arrow is clicked
    $('body,html').animate({
        scrollTop : 0                       // Scroll to top of body
    }, 500);
    });
</script>
<script>
    $(function() {
        $( "#slider-range" ).slider({
            range: true,
            min: 0,
            max: 1000,
            values: [ 75, 300 ],
            slide: function( event, ui ) {
                $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $." + ui.values[ 1 ] );
            }
        });
        $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
            " - $" + $( "#slider-range" ).slider( "values", 1 ) );
    });
</script>
<script>
    $("#top").click(function() {
      $("html, body").animate({ scrollTop: $('#dwn').offset().top }, "slow");
      return false;
    });

    $("#top1").click(function() {
      $("html, body").animate({ scrollTop: $('#dwn1').offset().top }, "slow");
      return false;
    });

    $("#top2").click(function() {
      $("html, body").animate({ scrollTop: $('#dwn2').offset().top }, "slow");
      return false;
    });

    $("#top3").click(function() {
      $("html, body").animate({ scrollTop: $('#dwn3').offset().top }, "slow");
      return false;
    });

    $("#top4").click(function() {
      $("html, body").animate({ scrollTop: $('#dwn4').offset().top }, "slow");
      return false;
    });

    $("#top5").click(function() {
      $("html, body").animate({ scrollTop: $('#dwn5').offset().top }, "slow");
      return false;
    });
</script>
<script>
    $('.addfvrt').click(function(){
        var reqData = {
          'jsonrpc' : '2.0',
          '_token' : '{{csrf_token()}}',
          'params' : {
                'id' : {{ @$user->id }}
            }
        };
        $.ajax({
            url: "{{ route('add.favourite') }}",
            method: 'post',
            dataType: 'json',
            data: reqData,
            success: function(response){
                if(response.status==1) {
                    toastr.success('@lang('client_site.successfully_added_to_favourite_list')');
                }
                if(response.status==3){
                    localStorage.removeItem('slug');
                    localStorage.setItem('slug','{{ @$user->slug }}');
                    location.href = "{{ route('login') }}";
                }

                if(response.status==2){
                    toastr.info('@lang('client_site.you_have_been_allready_added')' + '{{ @$user->nick_name ? @$user->nick_name : @$user->name }}' + '@lang('client_site.into_your_favourite_list')');
                }
            }, error: function(error) {
                console.error(error);
            }
        });
    });
    function checkDate(date, time){
        console.log(time)
        var varDate = new Date(date); //dd-mm-YYYY
        var today = new Date();
        x = today;
        today.setHours(0,0,0,0);
        // if(varDate >= today) {
            var reqData = {
              'jsonrpc' : '2.0',
              '_token' : '{{csrf_token()}}',
              'params' : {
                    'date' : date,
                    'time' : time,
                    'id' : {{ @$user->id }}
                }
            };
            console.log(reqData)
            $.ajax({
                url: "{{ route('set.date') }}",
                method: 'post',
                dataType: 'json',
                data: reqData,
                success: function(response){
                    if(response.status==1){
                        localStorage.removeItem('bookSlug');
                        localStorage.setItem('bookSlug', "{{ @$user->slug }}");
                        location.href="{{ route('login') }}";
                    }
                    if(response.status==0){
                        swal('@lang('client_site.currently_time_slot_is_not_avaliable_for_this_day')',{icon:"info"});
                    }
                }, error: function(error) {
                    swal('@lang('client_site.internal_server_error')',{icon:"warning"});
                }
            });
        // }
        // else{
        //     swal('@lang('client_site.currently_time_slot_is_not_avaliable_for_this_day')',{icon:"error"});
        //     {{-- swal('@lang('client_site.please_selct_a_date_that_is_greater_than_or_equal_to')'+x,{icon:"error"}); --}}
        // }
    }
    $('.ssnbtnslg').click(function(){
        if($(this).data('slug')!=""){
            localStorage.removeItem('bookSlug');
            localStorage.setItem('bookSlug', $(this).data('slug'));
            location.href="{{ route('login') }}"
        }
    });
</script>
<script>
    $(document).ready(function(){
        // $('.fc-event-container').each(function(i, elem){
        //     $(elem).addClass($(elem).find('.fc-day-grid-event').data('date'));
        // });
        // var onInterval = setInterval(function(){
        //     // $('.fc-event-container').each(function(i, ele){
        //     //     if(!$(ele).hasClass('on')){
        //     //         var date = $(this).find('.fc-day-grid-event').data('date');
        //     //         $('.'+date).show();
        //     //     }
        //     // });
        //     $('.on').removeClass('on');
        // }, 2000);

        $(".fc-more").addClass('more_slots');
        $('.fc-content-skeleton').each(function(i, elem){
            $(elem).find('tbody').find("tr:first-child").addClass("first_tr");
        });
        $('.fc-event-container').each(function(i, elem){
            if($(elem).parent().hasClass("first_tr")){
                var date = $(elem).find('.fc-day-grid-event').data('date');
                $(elem).append(`<div class="solot `+date+`" data-date="`+date+`"></div>`);
            }
        });
            $('body').on('click', '.addToCart', function() {
            // $('.addToCart').click(function(){
                var productId = $(this).data('product');
                console.log(productId)
                var reqData = {
                    'jsonrpc': '2.0',
                    '_token': '{{csrf_token()}}',
                    'params': {
                        productId: productId,
                    }
                };
                $.ajax({
                    url: '{{ route('product.add.to.cart') }}',
                    type: 'post',
                    dataType: 'json',
                    data: reqData,
                })
                .done(function(response) {
                    console.log(response);
                    $('.cou_cart').text(response.result.cart.length);
                    $('.cartClass'+productId).css('display','none');
                    $('.GoToCart'+productId).css('display','block');
                    console.log($(this));
                })
                .fail(function(error) {
                    console.log("error", error);
                })
                .always(function() {
                    console.log("complete");
                })
            })
            $('.buyNow').click(function(){
                var productId = $(this).data('product');
                console.log(productId)
                var reqData = {
                    'jsonrpc': '2.0',
                    '_token': '{{csrf_token()}}',
                    'params': {
                        productId: productId,
                    }
                };
                $.ajax({
                    url: '{{ route('product.add.to.cart') }}',
			        type: 'post',
			        dataType: 'json',
			        data: reqData,
                })
                .done(function(response) {
                    window.location.href = '{{route('product.order.store')}} ';
                })
                .fail(function(error) {
                    console.log("error", error);
                })
                .always(function() {
                    console.log("complete");
                })
            })
        })

</script>
@endsection
