@extends('layouts.app')
@section('title')
@lang('site.booking_successfull')
@endsection
@section('style')
<style>
    .time_slot_span{
        background: #1781d2;
        padding: 2px 7px;
        border: solid 1px #475dfb;
        border-radius: 12px;
        color: #fff;
        margin: 0 15px 10px 0;
        float: left;
        cursor: pointer;
    }
    .ui-datepicker{
        z-index: 2 !important;
    }
    .form-group {
        width: 100% !important;
    }
    .fc-more-popover .fc-event-container{
        height: 167px;
        overflow-y: scroll;
    }
    .fc-day-grid-event{
        cursor: pointer;
    }
    .fc-event-container{
        color: #000;
    }
    /*.fc-event, .fc-event-dot {
        background-color: #ddd !important;
        color: #000 !important;
        border: 1px solid transparent !important;
    }*/
   /* .fc-event:hover, .fc-event-dot:hover {
        background-color: #1781d2 !important;
       color: #fff !important;
        transition: background-color 300ms linear;
        transition: color 300ms linear;
        border: 1px solid #3788d8 !important;
    }*/
    .fc-event, .fc-event:hover {
        color: #000 !important;
    }
    .unavlbl_slot{
        background: url('http://localhost/netbhepreview/public/frontend/images/unavlbl_slot.png') no-repeat 50% 50%;
        background-size: 28px;
    }

</style>
@include('includes.style')
<link href='{{ URL::to('public/frontend/packages/core/main.css') }}' rel='stylesheet' />
<link href='{{ URL::to('public/frontend/packages/daygrid/main.css') }}' rel='stylesheet' />
<link href='{{ URL::to('public/frontend/packages/timegrid/main.css') }}' rel='stylesheet' />
<link href='{{ URL::to('public/frontend/packages/list/main.css') }}' rel='stylesheet' />
<link href="{{ URL::to('public/frontend/css/calender.css') }}" rel="stylesheet" type="text/css">


<style>
.fc-event, .fc-event-dot {
    background-color: #ddd !important;
    color: #000 !important;
    border: 1px solid transparent !important;
}
.fc-event:hover, .fc-event-dot:hover {
    background-color: #1781d2 !important;
    color: #fff !important;
    transition: background-color 300ms linear;
    transition: color 300ms linear;
    border: 1px solid #3788d8 !important;
}
</style>


@endsection
@section('scripts')
@include('includes.scripts')
<script src='{{ URL::to('public/frontend/packages/core/main.js') }}'></script>
<script src='{{ URL::to('public/frontend/packages/interaction/main.js') }}'></script>
<script src='{{ URL::to('public/frontend/packages/daygrid/main.js') }}'></script>
<script src='{{ URL::to('public/frontend/packages/timegrid/main.js') }}'></script>
<script src='{{ URL::to('public/frontend/packages/list/main.js') }}'></script>
<script src="{{ URL::to('public/frontend/js/moment.min.js') }}"></script>
<script src="{{ URL::to('public/frontend/js/jquery-ui.js') }}"></script>
<script>
    $(function() {
        $("#datepicker11").datepicker({dateFormat: "yy-mm-dd",
            defaultDate: new Date(),
            maxDate:new Date(),
            onClose: function( selectedDate ) {
            //$( "#datepicker1").datepicker( "option", "minDate", selectedDate );
            }
        });
    });
</script>
<script>
    var availiabilityDays = <?php echo $avlDay; ?>;
    var availiabilityTimes1 = <?php echo $avlTime1; ?>;
    console.log(availiabilityTimes1, availiabilityDays);
    document.addEventListener('DOMContentLoaded', function() {
    var Calendar = FullCalendar.Calendar;
    var Draggable = FullCalendarInteraction.Draggable

    var events = [];
    var timeS;

    let now = new Date('{{ date("Y-m-d") }}T{{ date("H:i:s") }}')
    availiabilityTimes1.forEach(function(item, index) {
        events.push({
            'title': item.time_start+' - '+item.time_end,
            'start': item.slot_start,
            'end': item.slot_end
        });
    })

    var calendarEl = document.getElementById('calendar');
    var calendar;
    var i = 1;
    var event = [];
    var ardt = [];

    calendar = new Calendar(calendarEl, {
        eventLimit: true, // for all non-TimeGrid views
        locale: 'pt-br',
        plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list' ],
        header: {
            left: 'prev,next today',
            center: 'title',
            right: ''
        },
        views: {
            timeGrid: {
                eventLimit: 6 // adjust to 6 only for timeGridWeek/timeGridDay
            }
        },
        events: events,
        selectable: true,
        firstDay: 6,
        select: function(info) {
            // checkDate(info.startStr);
        },
        click: function(info) {
            //console.log(info)
            checkDate(info.startStr);
        },
        eventClick: function(info) {
            checkDate(moment(info.event.start).format('YYYY-MM-DD'), info.event.title);
        },
        dayRender: function(event) {
            const day = moment(event.date).format('YYYY-MM-DD');
            var todayDate = new Date();
            if ($.inArray(day, availiabilityDays) > -1) {
                $(event.el).css('background-color', '#ddd');
            } else {
                $(event.el).css('background-color', '#a1c7e4');
                $(event.el).addClass('unavlbl_slot');
            }
        },
    });

    calendar.render();
    });
    function callMe(val){
    alert(val);
    }

</script>
<script>
   $(function() {
       $("#datepicker").datepicker({dateFormat: "dd-mm-yy",
       monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho',
            'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            monthNamesShort: [ "Janeiro", "Fevereiro", "Março", "Abril",
                   "Maio", "Junho", "Julho", "Agosto", "Setembro",
                   "Outubro", "Novembro", "Dezembro" ],

            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
           defaultDate: new Date(),
        //    showButtonPanel: true,
           showMonthAfterYear: true,
           showWeek: true,
           showAnim: "drop",
           constrainInput: true,
           yearRange: "-90:",
           minDate:new Date(),
           onClose: function( selectedDate ) {
              $( "#datepicker1").datepicker( "option", "minDate", selectedDate );
           }
       });
       $("#datepicker1").datepicker({dateFormat: "dd-mm-yy",
       monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho',
    'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
    monthNamesShort: [ "Janeiro", "Fevereiro", "Março", "Abril",
    "Maio", "Junho", "Julho", "Agosto", "Setembro",
    "Outubro", "Novembro", "Dezembro" ],

    dayNames: ['Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo'],
    dayNamesShort: ['Se', 'Te', 'Qu', 'Qu', 'Se', 'Sá', 'Do'],
    dayNamesMin: ['Se', 'Te', 'Qu', 'Qu', 'Se', 'Sá', 'Do'],
           defaultDate: new Date(),
        //    showButtonPanel: true,
           showMonthAfterYear: true,
           showWeek: true,
           showAnim: "drop",
           constrainInput: true,
           yearRange: "-90:",
           onClose: function( selectedDate ) {
               $( "#datepicker" ).datepicker( "option", "maxDate", selectedDate );
           }
       });
   });
</script>
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')
<div class="bkng-hstrybdy cus-book">
   <div class="container">
      <h2>{{ $bookingPage->title }}</h2>
      <p style="white-space: pre-wrap;">{!! $bookingPage->description !!}</p>
      @if(!@Auth::user()->timezone_id)
      <span>
         <h3 class="alert alert-info">@lang('site.for_booking_you_need')</h3>
      </span>
      @endif
      {{-- @if(@$profile>0)
      <span>
         <h3 class="alert alert-info">For booking you need to update your profile first e.g (CFP number, Country, City etc) Please Fill up this form to continue your booking.</h3>
      </span>
      @endif --}}
   </div>
</div>
<div class="all_logo_area message_compose">
   <div class="container">
      <div class="row">
         <div class="booking-div">
            <div class="head-book">
               <span><img src="{{ @$user->profile_pic ? URL::to('storage/app/public/uploads/profile_pic').'/'.@$user->profile_pic: URL::to('public/frontend/images/no_img.png')}}"></span>
               <div class="des-big">
                  <h4>{{ @$user->nick_name ? @$user->nick_name : @$user->name }}</h4>
                  <p>{!! @$user->specializationName->name ? $user->specializationName->name . ', ' . $user->crp_no : '&nbsp;' !!}</p>
                  {{-- <p>@lang('site.education_guidance_advisory')</p> --}}
               </div>
               <div class="des-small">
                <h5> @lang('site.fees')</h5>
                @if(@$user->rate=="M")
                    @php
                        $time = $user->rate_price/60;
                        $call_durations = $user->get_call_durations()->pluck('call_duration')->toArray();
                    @endphp
                    @if( count($user->get_call_durations()->pluck('call_duration_id')->toArray()) > 0)
                        @foreach($call_durations as $call_dur)
                            <p>R$ {{ number_format((float)$time*$call_dur, 2, '.', '') }} / {{$call_dur}} @lang("client_site.minutes") </p>
                        @endforeach
                    @else
                        <p>R$ {{ number_format((float)$time*60, 2, '.', '') }} / 60 @lang("client_site.minutes") </p>
                    @endif
                @else
                    @php
                        $time = $user->rate_price/60;
                        $call_durations = $user->get_call_durations()->pluck('call_duration')->toArray();
                    @endphp
                    @if( count($user->get_call_durations()->pluck('call_duration_id')->toArray()) > 0)
                        @foreach($call_durations as $call_dur)
                            <p>R$ {{ number_format((float)$time*$call_dur, 2, '.', '') }} / {{$call_dur}} @lang("client_site.minutes") </p>
                        @endforeach
                    @else
                        <p>R$ {{ number_format((float)$time*60, 2, '.', '') }} / 60 @lang("client_site.minutes") </p>
                    @endif
                @endif
               </div>
            </div>
            <div class="book-from">
               <form name="myForm" id="myform" action="{{ route('store.booking',['slug'=>@$user->slug]) }}" method="post">
                  @csrf
                  <div class="row">
                     <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="form-group">
                           <label class="personal-label">@lang('site.category') *</label>
                           <select class="personal-type required personal-select" name="category" id="category" @if(sizeof($parent)<=0) disabled @endif @if(!@Auth::user()->timezone_id) disabled @endif>
                           <option value="">@lang('site.select')</option>
                           @foreach($category as $cat)
                           @if(in_array(@$cat->id,$parent))
                           <option value="{{ @$cat->id }}">{{ @$cat->name }}</option>
                           @endif
                           @endforeach
                           </select>
                        </div>
                     </div>
                     <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="form-group">
                           <label class="personal-label">@lang('site.sub') @lang('site.category')</label>
                           <select class="personal-type personal-select" name="sub_category" id="sub_category" @if(sizeof(@$child)<=0) disabled @endif @if(!@Auth::user()->timezone_id) disabled @endif>
                           <option value="">@lang('site.select') @lang('site.sub') @lang('site.category')</option>
                           </select>
                        </div>
                     </div>
                     {{-- @dump(@Auth::user()->timezone_id) --}}
                     <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="form-group">
                           <label class="personal-label"> @lang('site.date') *</label>
                           <input type="text" autocomplete="off" name="date" class="dashboard-type required datepicker slt_date" id="datepicker" placeholder="@lang('site.date')" value="{{ $date }}">
                           {{-- <input type="text" id="datepicker" onchange="checkDate();" name="date" class="dashboard-type required" readonly value="{{@$date}}" placeholder="@lang('site.select_date')" @if(!@Auth::user()->timezone_id) disabled @endif> --}}
                           <img class="pstn" src="{{ URL::to('public/frontend/images/clndr.png') }}">
                        </div>
                     </div>
                     <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label class="personal-label">{{__('site.booking_type')}} *</label>
                            <select class="personal-type required personal-select" name="booking_type" id="booking_type" @if(sizeof(@$child)<=0) disabled @endif @if(!@Auth::user()->timezone_id) disabled @endif>
                                <option value="">{{__('site.booking_type_select')}}</option>
                                <option value="V">{{__('site.booking_type_video')}}</option>
                                <option value="C">{{__('site.booking_type_chat')}}</option>
                            </select>
                        </div>
                    </div>
                     <div class="col-md-12 time_slot time_radio">
                        @if (count($fromTotime ?? []) > 0)
                            <label class="personal-label">@lang('site.time_slot') *</label>
                        @endif
                        @foreach($fromTotime as $i => $val)
                        <div class="checkbox-group_time">
                           <input type="radio" value="{{ $val['slot_start'] }}-{{ $val['slot_end'] }}" name="time_slot" id="time_slot_id_{{ $i }}" @if($val['from_time'].'-'.$val['to_time'] == @$time) checked @endif>
                           <label for="time_slot_id_{{ $i }}" id="time_slot_{{ $i }}" class="time_slot_span">
                           <span class="check find_chek"></span>
                           <span class="box W25 boxx">{{ $val['slot_start'] }}-{{ $val['slot_end'] }}</span>
                           </label>
                        </div>
                        @endforeach
                     </div>
                     <input type="hidden" name="user_id" id="user_id" value="{{ @$user->id }}">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                           <label class="personal-label">@lang('site.message') *</label>
                           <textarea class="personal-type99 required" name="msg" id="msg" placeholder="@lang('site.type_your_message_here')" @if(!@Auth::user()->timezone_id) disabled @endif></textarea>
                        </div>
                        <h3><span id="perr" class="error"></span></h3>
                     </div>
                     <div class="col-sm-12">
                        <button type="submit" class="login_submitt" @if(!@Auth::user()->timezone_id) disabled @endif>@lang('site.send') @lang('site.request')</button>
                     </div>
                  </div>
               </form>
            </div>
            <div class="clearfix"></div>
            <div id="dwn4">
               <h3>@lang('client_site.calendar')</h3>
               <div class="right_dash" style="width:100%;">
                  <div class="main-form">
                     <div class="avll">
                        <p><img src="{{URL::to('public/frontend/images/avl1.png')}}"> @lang('client_site.available')</p>
                        <p><img src="{{URL::to('public/frontend/images/unvl1.png')}}"> @lang('client_site.not_available')</p>
                     </div>
                     <div id='wrap'>
                        <div id='calendar'></div>
                        <div style='clear:both'></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   </section>
</div>
@endsection
@section('footer')
@include('includes.footer')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/moment.js"></script>
<script>
    $('.timepicker').timepicker({
        timeFormat: 'H:mm',
        interval: 15,
        minTime: '00',
        maxTime: '11:00pm',
        startTime: '09:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });

    $('.slt_date').bind("change load", function(event) {
        var reqData = {
            'jsonrpc' : '2.0',
            '_token' : '{{ csrf_token() }}',
            'params' : {
                'date' : $(this).val(),
                'user_id' : $('#user_id').val()
            }
        };
        $.ajax({
            url: "{{ route('front.get.date') }}",
            method: 'post',
            dataType: 'json',
            data: reqData,
            success: function(response){
                if(response.status==1) {
                    let value = {
                        interval: '00:'+30+':00',
                        startTime: '00:00:00',
                        endTime: '24:00:00'
                    };
                    // var intervals = [];
                    var i=0, html="", result = response.result;
                    var outputFormat = "HH:mm";
                    var html = '<label class="personal-label">@lang('site.time_slot') *</label>';
                    if(response.result.length==0) {
                        html += '<label style="color:red;">@lang('site.no_time_slot_availabile_this_date')</label>';
                    }
                    for(;i<response.result.length;i++) {
                        var slot = result[i].slot_start + '-' + result[i].slot_end;
                        html += '<div class="checkbox-group_time">\
                               <input type="radio" value="' + slot + '" name="time_slot" id="time_slot_id_'+i+'">\
                               <label for="time_slot_id_'+i+'" id="time_slot_'+ i +'" class="time_slot_span">\
                                   <span class="check find_chek"></span>\
                                   <span class="box W25 boxx">' + slot + '</span>\
                               </label>\
                           </div>';
                    }
                    $('.time_slot').html(html);
                }
            }, error: function(error) {
                console.error(error);
            }
        });
    });

   $('#myform').validate();
   $('#myform1').validate();
       $('#category').change(function(){
           if($('#category').val()!=""){
               var reqData = {
                 'jsonrpc' : '2.0',
                 '_token' : '{{csrf_token()}}',
                 'params' : {
                       'cat' : $('#category').val()
                   }
               };
               $.ajax({
                   url: "{{ route('front.get.subcat') }}",
                   method: 'post',
                   dataType: 'json',
                   data: reqData,
                   success: function(response){
                       if(response.status==1) {
                           var i=0, html="";
                           html = '<option value="">@lang('site.select_sub_category')</option>';
                           for(;i<response.result.length;i++){
                               html+='<option value='+response.result[i].id+'>'+response.result[i].name+'</option>';
                           }
                           $('#sub_category').html(html);
                           $('#sub_category').addClass('valid');
                       }
                       else{

                       }
                   }, error: function(error) {
                       console.error(error);
                   }
               });
           }
       });
       function checkDate(date, time){
        console.log(time)
        var varDate = new Date(date); //dd-mm-YYYY
        var today = new Date();
        x = today;
        today.setHours(0,0,0,0);
        // if(varDate >= today) {
            var reqData = {
              'jsonrpc' : '2.0',
              '_token' : '{{csrf_token()}}',
              'params' : {
                    'date' : date,
                    'time' : time,
                    'id' : {{ @$user->id }}
                }
            };
            console.log(reqData)
            $.ajax({
                url: "{{ route('set.date') }}",
                method: 'post',
                dataType: 'json',
                data: reqData,
                success: function(response){
                    if(response.status==1){
                        localStorage.removeItem('bookSlug');
                        localStorage.setItem('bookSlug', "{{ @$user->slug }}");
                        location.href="{{ route('login') }}";
                    }
                    if(response.status==0){
                        swal('@lang('client_site.currently_time_slot_is_not_avaliable_for_this_day')',{icon:"info"});
                    }
                }, error: function(error) {
                    swal('@lang('client_site.internal_server_error')',{icon:"warning"});
                }
            });
        // }
        // else{
        //     swal('@lang('client_site.currently_time_slot_is_not_avaliable_for_this_day')',{icon:"error"});
        //     {{-- swal('@lang('client_site.please_selct_a_date_that_is_greater_than_or_equal_to')'+x,{icon:"error"}); --}}
        // }
    }

       // function checkDate(){
       //     if($('#datepicker').val()!="" && $('#timepckr').val() !="" && $('#duration').val()!=""){
       //         var reqData = {
       //           'jsonrpc' : '2.0',
       //           '_token' : '{{csrf_token()}}',
       //           'params' : {
       //                 'date' :$('#datepicker').val(),
       //                 'start_time' :$('#timepckr').val(),
       //                 // 'duration':$('#duration').val(),
       //                 'user_id' : {{ @$user->id }}
       //             }
       //         };
       //         $.ajax({
       //             url: "{{ route('checkDate') }}",
       //             method: 'post',
       //             dataType: 'json',
       //             data: reqData,
       //             success: function(response){
       //                 if(response.status==1) {
       //                    $('#perr').text('{{ @$user->name }} @lang('site.is_currently_not')');
       //                    $('#datepicker').val("");
       //                    $('#timepckr').val("");
       //                    // $('#duration').val("");
       //                 }
       //                 else{
       //                     $('#perr').text('');
       //                 }
       //             }, error: function(error) {
       //                 console.error(error);
       //             }
       //         });
       //     }
       // }
       $('#country').change(function(){
           if($('#country').val()!=""){
               var reqData = {
                 'jsonrpc' : '2.0',
                 '_token' : '{{csrf_token()}}',
                 'params' : {
                       'cn' : $('#country').val()
                   }
               };
               $.ajax({
                   url: "{{ route('front.get.state') }}",
                   method: 'post',
                   dataType: 'json',
                   data: reqData,
                   success: function(response){
                       if(response.status==1) {
                           var i=0, html="";
                           html = '<option value="">Selecionar Estado</option>';
                           for(;i<response.result.length;i++){
                               html+='<option value='+response.result[i].id+'>'+response.result[i].name+'</option>';
                           }
                           $('#state').html(html);
                           $('#state').addClass('valid');
                       }
                       else{

                       }
                   }, error: function(error) {
                       console.error(error);
                   }
               });
           }
       });
</script>
@endsection

