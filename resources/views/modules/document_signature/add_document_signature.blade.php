@extends('layouts.app')
@section('title')
@if(@$template)
Edit
@else
@lang('site.add_new_blog')
@endif
@endsection
@section('style')
@include('includes.style')
<style type="text/css">
    div.mce-edit-area {
        background:
            #FFF;
        filter: none;
        min-height: 317px;
    }

    .chck_eml_rd {
        color: red;
        font-weight: bold;
        font-size: 14px;
    }
</style>
@endsection
@section('scripts')
@include('includes.scripts')
<script>
    $(document).ready(function(){
         $('#myForm').on('submit', function() {
            var editorContent = tinyMCE.get('short_description').getContent();
            //alert(editorContent);
            if(editorContent == '') {
                $('.desc_error').html('Por favor escreva alguma descrição.');
                $('.desc_error').css('color', 'red');
                return false;
            } else {
                $('.desc_error').html('');
                return true;
            }
        });
      });
</script>
<script>
    tinyMCE.init({
            mode : "specific_textareas",
            editor_selector : "short_description",
            height: '320px',
            plugins: [
              'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
              'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
              'save table contextmenu directionality emoticons template paste textcolor'
            ],
            relative_urls : false,
            remove_script_host : false,
            convert_urls : true,
            toolbar: 'insertfile undo redo | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor emoticons',
            images_upload_url: '{{ URL::to('storage/app/public/uploads/content_image/') }}',
            images_upload_handler: function(blobInfo, success, failure) {
                var formD = new FormData();
                formD.append('file', blobInfo.blob(), blobInfo.filename());
                formD.append( "_token", '{{csrf_token()}}');
                $.ajax({
                    url: '{{ route('artical.img.upload') }}',
                    data: formD,
                    type: 'POST',
                    contentType: false,
                    cache: false,
                    processData:false,
                    dataType: 'JSON',
                    success: function(jsn) {
                        if(jsn.status == 'ERROR') {
                            failure(jsn.error);
                        } else if(jsn.status == 'SUCCESS') {
                            success(jsn.location);
                        }
                    }
                });
            },
        });
</script>
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.new_template')</h2>
        <div class="bokcntnt-bdy">
            @php
            $user = Auth::guard('web')->user();
            $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0) <center>
                <p class="alert alert-info">@lang('client_site.please_complete_your_profile')
                    @lang('site.by_entering_your_educational_information'), <a
                        href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p>
                </center>
                @endif
                <div class="mobile_filter">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                    <p>@lang('site.menu')</p>
                </div>
                @include('includes.professional_sidebar')
                <div class="dshbrd-rghtcntn">
                    <div class="dash_form_box">
                        <form action="{{ @$template? route('document.edit.success',['id'=>@$template->id]) : route('document.add.success') }}" method="post" name="myForm" id="myForm"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="form_body">

                                <div class="row">

                                    <div class="col-lg-12 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">@lang('site.title')</label>
                                            <input type="text" placeholder="@lang('site.placeholder_title')"
                                                class="required personal-type" value="{{ old('title',@$template->title) }}" name="title">
                                        </div>
                                    </div>                                  
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">@lang('site.document_content')</label>
                                            <textarea class="required short_description personal-type99"
                                                placeholder="@lang('site.document_content_placeholder')" name="content"
                                                id="short_description">{{ old('content',@$template->description) }}</textarea>
                                            <label class="desc_error" style="color: red"></label>
                                        </div>
                                    </div>
                                    {{-- <div class="col-lg-12">
                                        <div class="your-mail">
                                            <label>@lang('site.document_content')</label>
                                            <p>
                                                <code style="white-space: pre-wrap;">__signature__</code>
                                            </p>
                                        </div>
                                    </div> --}}
                                    <div class="col-sm-12">
                                        <button type="submit" class="login_submitt">@if(@$template) @lang('site.update') @else @lang('site.save') @endif</button>
                                    </div>


                                </div>
                            </div>
                        </form>

                    </div>
                </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
<script>
    $(document).ready(function(){
        $('#myForm').validate();
    });
</script>
<style type="text/css">
    #mceu_21 {
        float: left !important;
    }
</style>
@endsection