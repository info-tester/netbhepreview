@extends('layouts.app')
@section('title')
@lang('site.post') @lang('site.details')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.post') @lang('site.details')</h2>
        <div class="bokcntnt-bdy">
            @php
            // $user = Auth::guard('web')->user();
            // $user = $user->load('userQualification');
            @endphp
            {{-- @if(sizeof($user->userQualification)<=0) <center> --}}
                <p class="alert alert-info">@lang('client_site.please_complete_your_profile')
                    @lang('site.by_entering_your_educational_information'), <a
                        href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p>
                </center>
                {{-- @endif --}}
                <div class="mobile_filter">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                    <p>@lang('site.menu')</p>
                </div>
                @include('includes.professional_sidebar')
                <div class="dshbrd-rghtcntn">
                    <div class="dash_form_box">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="blog-right details-right">
                                <div class="blog-box">

                                    <div class="blog-dtls">
                                        <h6>{{ @$template->title }}</h6>
                                        <p>{!! @$template->content !!}</p>
                                        <p style="text-align: center;">
                                            <span>
                                                <span style="vertical-align: inherit;">
                                                    <span style="vertical-align: inherit;">
                                                        <img src="{{URL::to('storage/app/public/uploads/signature'.'/'.@Auth::user()->signature)}}" alt=""
                                                            width="100" height="100" />
                                                        {{-- {!!@$image!!} --}}
                                                    </span>
                                                </span>
                                            </span>
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
<script>
    $(document).ready(function(){
            $('#myForm').validate();
        });
</script>

@endsection
