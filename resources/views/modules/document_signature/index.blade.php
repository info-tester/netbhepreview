@extends('layouts.app')
@section('title')
@lang('site.my_blogs')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
<script>
    $(document).ready(function(){
        $('#myForm').validate();
    });
    function send(id){
        console.log(id);
        $('#template_id').val(id);
        $('#myModal').modal('show');
    } 
</script>
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.template')</h2>

        <div class="bokcntnt-bdy">
            @php
            $user = Auth::guard('web')->user();
            $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0) <center>
                <p class="alert alert-info">@lang('client_site.please_complete_your_profile')
                    @lang('site.by_entering_your_educational_information'), <a
                        href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p>
                </center>
                @endif
                <div class="mobile_filter">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                    <p>@lang('site.menu')</p>

                </div>
                @include('includes.professional_sidebar')
                <div class="dshbrd-rghtcntn">
                    {{-- <form name="myForm" id="myForm" method="post" action="{{route('my.blog.search')}}"> --}}
                        {{-- @csrf --}}
                        <div class="from-field">

                            

                            @if(auth()->user()->sell != 'PS')
                                <div class="frmfld">
                                    <a class="btn btn-success" href="{{route('document.add')}}">+ @lang('site.Add_template')</a>
                                </div>
                            @endif
                        </div>
                    {{-- </form> --}}
                    <div class="buyer_table">
                        <div class="table-responsive">
                            @if(sizeof(@$data)>0)
                            <div class="table">
                                <div class="one_row1 hidden-sm-down only_shawo">
                                    <div class="cell1 tab_head_sheet">@lang('site.no').</div>
                                    <div class="cell1 tab_head_sheet">@lang('site.title')</div>
                                    <div class="cell1 tab_head_sheet">@lang('site.created_by')</div>
                                    {{-- <div class="cell1 tab_head_sheet">posted on</div> --}}
                                    <div class="cell1 tab_head_sheet">@lang('site.status')</div>
                                    <div class="cell1 tab_head_sheet">@lang('site.action')</div>
                                </div>
                                <!--row 1-->
                                @php
                                $i=1;
                                @endphp
                                @foreach(@$data as $item)
                                <div class="one_row1 small_screen31">
                                    <div class="cell1 tab_head_sheet_1">
                                        <span class="W55_1">@lang('site.no')</span>
                                        <p class="add_ttrr">{{@$i}}</p>
                                    </div>

                                    <div class="cell1 tab_head_sheet_1">
                                        <span class="W55_1">@lang('site.topic')</span>
                                        <p class="add_ttrr">{{ @$item->title }}</p>
                                    </div>
                                    <div class="cell1 tab_head_sheet_1">
                                        <span class="W55_1">@lang('site.post_name')</span>
                                        <p class="add_ttrr">@if(@$item->created_by=="A") @lang('site.admin') @else @lang('client_site.professional') @endif</p>
                                    </div>
                                    <div class="cell1 tab_head_sheet_1">
                                        <span class="W55_1">@lang('site.status')</span>
                                        <p class="add_ttrr">@if(@$item->status=="A") @lang('site.active') @else
                                            @lang('site.inactive') @endif</p>
                                    </div>
                                    <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                        <a href="{{route('document.view', ['id'=>@$item->id])}}" class="acpt">@lang('site.view')</a>
                                        <a href="{{route('document.copy', ['id'=>@$item->id])}}" class="acpt" onclick="return confirm('@lang('site.copy_message')');">@lang('site.copy')</a>
                                        @if($item->user_id==Auth::guard('web')->user()->id)
                                        <a href="{{route('document.edit',['id'=>@$item->id])}}" class="rjct">@lang('site.edit')</a>
                                        @endif
                                        @if($item->user_id==Auth::guard('web')->user()->id)
                                        <a href="{{route('document.delete', ['id'=>@$item->id])}}" class="rjct" >@lang('site.delete')</a>
                                        @endif
                                        @if($item->status=='A')
                                        <a href="javascript:void(0)" class="acpt" onclick="send({{@$item->id}})">@lang('site.send')</a>
                                        @endif
                                    </div>
                                </div>
                                @php
                                $i++;
                                @endphp
                                @endforeach

                            </div>
                            @else
                            <div class="one_row small_screen31">
                                <center><span>
                                        <h3 class="error">OOPS! @lang('site.blog_post_not_found').</h3>
                                    </span></center>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
        </div>
    </div>
</section>
<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">@lang('client_site.message')</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row" style="margin:0px !important;">
                    <div class="right-user main_Message_List">
                        <div class="main-infor massage_boxMin">
                            <div class="edit-froms massage_boxcontent">
                                <div class="inbox_tabBox top-edit-menus">
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div id="home" class="tab-pane active">
                                            <form action="{{route('document.send')}}" method="post" name="myForm" id="myForm" enctype="multipart/form-data">
                                                @csrf
                                                <input type="hidden" name="template_id" id="template_id">
                                                <div class="form_body">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                                            <div class="form-group" style="width: 100%">
                                                                <label class="personal-label">@lang('client_site.user')</label>
                                                                <select name="user_id" class="personal-type personal-select" required>
                                                                    <option value="">@lang('site.select_user')</option>
                                                                    @foreach (@$userlist as $item)
                                                                    <option value="{{@$item->userDetails->id}}" >{{ @$item->userDetails->nick_name ? @$item->userDetails->nick_name : @$item->userDetails->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        {{-- <div class="col-lg-6 col-md-6 col-sm-12">
                                                            <div class="form-group">
                                                                <label class="personal-label">User</label>
                                                                <select name="gender" class="@if ($user->status == 'A') required @endif personal-type personal-select">
                                                                    <option value="">select Option</option>
                                                                    @foreach (@$userlist as $item)
                                                                    <option value="{{@$item->id}}" >{{ @$item->nick_name ? @$item->nick_name : @$item->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div> --}}
                                                        <div class="col-sm-12">
                                                            <button type="submit" class="login_submitt">@lang('site.send')</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="login_submitt"
                    data-dismiss="modal">@lang('site.close')</button>
            </div>

        </div>
    </div>
</div>
@endsection
@section('footer')
@include('includes.footer')
<script>
    
</script>
@endsection