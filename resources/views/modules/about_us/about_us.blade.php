@extends('layouts.app')
@section('title')
  @lang('site.about_us')
@endsection
@section('style')
    @include('includes.style')
@endsection
@section('scripts')
    @include('includes.scripts')
@endsection
@section('header')
    @include('includes.header')
@endsection
@section('content')
    <div class="login-body about-body">
    <div class="container">
        <div class="row">
            <div class="abut_page">
        <h2>@lang('site.about_us')</h2>
        <p>{!! @$about->short_description !!}</p>

        
        <div class="abut_sec">
        
        <div class="abut_contnt">
            <div class="abutpic">
                <span><img src="{{URL::to('public/frontend/images/vsn.png')}}" alt=""></span>
            </div>
            
            <h4>@lang('site.our_vision')</h4>
            <p>{!! @$about->our_vision !!}</p>
            
        </div>
        
        <div class="abut_contnt">
            <div class="abutpic">
                <span><img src="{{URL::to('public/frontend/images/msn.png')}}" alt=""></span>
            </div>
            
            <h4>@lang('site.our_mission')</h4>
            <p>{!! @$about->our_mission !!} </p>
            
        </div>
        
        <div class="abut_contnt">
            <div class="abutpic">
                <span><img src="{{URL::to('public/frontend/images/tm.png')}}" alt=""></span>
            </div>
            
            <h4>@lang('site.our_team')</h4>
            <p>{!! @$about->our_team !!}</p>
            
        </div>
        
        </div>
        
        
    </div>
    
            <div class="bitt abthstry">
                <img src="{{URL::to('storage/app/public/uploads/about_us').'/'.@$about->image}}" alt="">
                <h5>@lang('site.our_history')</h5>
                <p>{!! @$about->long_description !!}</p>
                
                
            
            </div>
        </div>
    </div>
</div> 
@endsection
@section('footer')
    @include('includes.footer')
@endsection