@extends('layouts.app')
@section('title')
    {{-- @lang('site.edit') @lang('site.blog')-@lang('site.post') --}}
    Edit Form
@endsection
@section('style')
@include('includes.style')
<style type="text/css">
    .chck_eml_rd{
        color: red;
        font-weight: bold;
        font-size: 14px;
    }
</style>
@endsection
@section('scripts')
@include('includes.scripts')

@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>
            @lang('site.edit_form')
            {{-- @lang('site.edit') @lang('site.blog')-@lang('site.post') --}}
        </h2>
        @include('includes.professional_tab_menu')
        <div class="bokcntnt-bdy no-margin-top">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.professional_sidebar')


            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">
                    <form id="myform" method="post" action="{{ route('professional-360evaluation.update',['form_id'=>@$details->id]) }}">
                    {{-- <form id="myform" method="post" action="{{ route('professional-form.store') }}"> --}}
                        <input name="_method" type="hidden" value="PUT">
                        {{ csrf_field() }}
                        <div class="form_body">

                            <div class="row">


                                <div class="col-lg-12 col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="personal-label" for="exampleInputEmail1">Title *</label>
                                    <input type="text" name="title" class="personal-type required" id="title" placeholder="Title" value="{{ @$details->title }}">
                                    </div>
                                </div>

                                
                                {{-- answer field --}}

                                <div class="col-lg-12 col-md-6 col-sm-6 col-xs-12" id="fields">
                                    @php
                                        $totalanswer = count($details->get360Details);
                                    @endphp
                                    @foreach($details->get360Details as $key=>$val)
                                    @if(($key+1)==($totalanswer))
                                    <div class="col-md-12 plus_row" id="fieldRow0">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Answer *</label>
                                                    <input type="text" name="answer[]" class="form-control required" id="field_value0" placeholder="Enter your answer" value="{{ $val['answer'] }}">
                                                    <input type="color" name="color_box[]" class="form-control required" id="field_value0" placeholder="Enter your answer" value="{{ $val['color_box'] }}">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <button type="button" class="btn btn-success add-button addsingleselect" title="Add"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    @else
                                    <div class="col-md-12 plus_row" id="fieldRow{{ $key+1 }}">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="your-mail">
                                                    <label for="exampleInputEmail1">Answer *</label>
                                                    <input type="text" name="answer[]" class="form-control required" id="field_value0" placeholder="Enter your answer" value="{{ $val['answer'] }}">
                                                    <input type="color" name="color_box[]" class="form-control required" id="field_value0" placeholder="Enter your answer" value="{{ $val['color_box'] }}">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <button type="button" class="btn btn-danger add-button addsingleselect" title="Delete"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    @endif
                                    @endforeach
                                </div>
                                
                                <input type="hidden" class="countvalues" value="1">
                                <div class="clearfix"></div>
                                <br>

                                {{-- ============= --}}



                                            
                                            
                                <div class="clearfix"></div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="submit-login add_btnm">
                                        <input value="Atualizar" type="submit" class="login_submitt">
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>


        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
   <script>
    $(document).ready(function(){
        $("#myform").validate();
    });
</script>


<script>
    $(document).ready(function(){       
        $('body').on('click','.addsingleselect',function(){
        var count = $('.countvalues').val();
            var singleselect = '<div class="col-md-12" id="fieldRow'+count+'"><div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">\
                                                <div class="your-mail">\
                                                    <label for="exampleInputEmail1">Answer *</label>\
                                                    <input type="text" name="answer[]" class="form-control required" id="field_value'+count+'" placeholder="Enter your answer">\
                                                    <input type="color" name="color_box[]" class="form-control required" id="field_value0" placeholder="Enter your answer">\
                                                </div>\
                                            </div>\
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">\
                                                <button type="button" class="btn btn-danger add-button removesingleselect" title="Remove" data-id="'+count+'"><i class="fa fa-minus" aria-hidden="true"></i></button>\
                                            </div></div></div><div class="clearfix"></div>';
            $('#fieldRow0').before(singleselect);                               
        });
        $('body').on('click','.removesingleselect',function(){
            var id = $(this).data('id');
            $('#fieldRow'+id).remove();
        });
    });
</script>

<style>
    .error{
        color: red !important;
    }
</style>

@endsection