@extends('layouts.app')
@section('title')
    @lang('site.post') @lang('site.details')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.360evaluation_view')</h2>

       @include('includes.professional_tab_menu')

        <div class="bokcntnt-bdy no-margin-top">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">

                <?php /* ?>
                <div class="dash_form_box">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="blog-right details-right">
                            <div class="blog-box">                        
                                <div class="blog-dtls">                            
                                    <div class="from-field">
                                        <div class="weicome manage-designs">

                                            <div class="col-md-12 view-page coach_tools">
                                                <h3><strong>Title<b>:</b> </strong> {{ $details->title }}</h3>                                    
                                                <p><strong>Added by <b>:</b></strong> {{ $details['added_by']=='A'?'Admin':$details->getUserData->name }}</p>
                                                <p><strong>Answar<b>:</b> </strong> 

                                                    @foreach($details->get360Details as $key => $rw)
                                                    <div class="clearfix"></div>
                                                       <b>{{ $key+1 }} .</b> {{ $rw->answer }}
                                                    @endforeach
                                                    <div class="clearfix"></div>
                                                </p>
                                            </div>
                                            
                                        </div>                       
                                    </div>
                                    
                                </div>
                            </div>                    
                        </div>
                    </div>                    
                </div>

                <?php */?>



                <div class="buyer_table">
                    <div class="table-responsive add_rm003">
                        
                            <h3><strong>@lang('site.title')<b>:</b> </strong> {{ $details->title }}</h3>                                    
                            <p><strong>@lang('site.Added_by') <b>:</b></strong> {{ $details['added_by']=='A'?'Admin': ( @$details->getUserData->nick_name ? $details->getUserData->nick_name : $details->getUserData->name ) }}</p>
                            <p><strong>@lang('site.answer')<b>:</b> </strong> 

                            <div class="container add_rm001">
                                <div id="chart" class="add_rm002"></div>
                                
                                <div class="static">
                                <h1>@lang('site.answer_list') : </h1>
                                <ul>
                                    @foreach(@$details->get360Details as $answered_details)
                                        <li><span style="background:{{ @$answered_details->color_box }};"></span> <p>{{ @$answered_details->answer }}</p></li>                                        
                                    @endforeach

                                   {{--  <li><span style="background:#2298c3;"></span> <p>Question two</p></li>
                                    <li><span style="background:#22c33d;"></span> <p>Question three</p></li>
                                    <li><span style="background:#a722c3;"></span> <p>Simply dummy four</p></li>
                                    <li><span style="background:#0acce3;"></span> <p>Question five</p></li>
                                    <li><span style="background:#dd1717;"></span> <p>Question six</p></li> --}}
                                </ul>
                                </div>

                            </div>
                            
                    </div>
                </div>





                {{-- <div class="container">
                    <div id="chart"></div>
                </div> --}}

            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')




<script src="https://code.highcharts.com/highcharts.src.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>

<script src="https://code.highcharts.com/modules/exporting.js"></script>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>


 <script>
        'use strict';
        var plotData = [];
        //var testVar = [];

        

        $(document).ready(() => {
            //var plotData = [8, 7, 6, 5];

           

            var plotData =  <?= $get_all_anss ?> ;
        

            // plotData = [{
            //         y: 8,
            //         name: "Point2",
            //         color: "#07c400cc"
            //     },
            //     {
            //         y: 3,
            //         name: "Point2",
            //         color: "#c42eb7cc"
            //     },
            //     {
            //         y: 2,
            //         name: "Point2",
            //         color: "#4135c6cc"
            //     },
            //     {
            //         y: 4,
            //         name: "Point2",
            //         color: "#e07204cc"
            //     },
            //     {
            //         y: 4,
            //         name: "Point2",
            //         color: "#000"
            //     }
            // ];

            plotData = plotData;


           

            var scaleSize = 10;
            var interval = plotData.length;
            var chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'chart',
                    polar: true,
                    height: 400,
                    width: 400,
                    events: {
                        click: function (e) {
                            console.log(e);
                            var y = e.yAxis[0].value;
                            var x = e.xAxis[0].value;
                            var xSegment = parseInt(x / (360 / interval));
                            var ySegment = Math.ceil(e.yAxis[0].value);
                            if (plotData[xSegment] !== null) {
                                plotData[xSegment].y = ySegment;
                                chart.update({
                                    series: [{
                                        data: plotData,
                                    }]
                                });
                                output(plotData);
                            }
                        }
                    }
                },

                title: {
                    text: 'Responder roda'
                },

                pane: {
                    startAngle: 0,
                    endAngle: 360
                },

                xAxis: {
                    tickInterval: 360 / interval,
                    min: 0,
                    max: 360,
                    allowDecimals: false, 

                    labels: {
                        formatter: function () {
                            return '';
                        }
                    }
                    //categories: ['Jan', 'Dec']
                },


                

                yAxis: {
                    min: 0,
                    gridLineColor: '#197F07',
                    max: scaleSize,
                    tickAmount: scaleSize + 1,
                    tickPixelInterval: 50
                },

                plotOptions: {
                    series: {
                        pointStart: 0,
                        pointInterval: 360 / interval
                    },
                    column: {
                        pointPadding: 0,
                        groupPadding: 0,
                    }
                },

                series: [{
                    type: 'column',
                    name: 'Column',
                    data: plotData,
                    pointPlacement: 'between',
                    zoneAxis: 'x',
                    colors: ['#adada9', 'adada0'],
                    zones: [{
                        value: 0,
                        fillColor: '#adada0'
                    }],
                    events: {
                        click: function (e) {
                            var index = parseInt(e.point.x / (360 / interval));
                            console.log(index);
                            var x1 = e.chartX,
                                y1 = e.chartY,
                                x2 = 201,
                                y2 = 199,
                                distanceOfCenterFromEdge = 127;
                            var dist = Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 -
                                y1), 2));
                            var val = String(dist / distanceOfCenterFromEdge * 10);
                            var valMatch = val.match(/(.*\.)([0-9]{1}).*/);
                            var valCeil = Math.ceil(valMatch[1] + valMatch[2]);
                            if (valCeil > 10) {
                                return;
                            }
                            plotData[index].y = valCeil;
                            chart.update({
                                series: [{
                                    data: plotData,
                                }]
                            });
                            output(plotData);
                           
                        }
                    }
                }]
            });
        });

        function output(data) {
            console.log(data);
            //testVar = data;
        }

    </script>



@endsection