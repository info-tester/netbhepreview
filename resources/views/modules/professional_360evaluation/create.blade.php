@extends('layouts.app')
@section('title')
    {{-- @lang('site.add_new_blog') --}}
    360degree evaluation
@endsection
@section('style')
@include('includes.style')
<style type="text/css">
    div.mce-edit-area {
        background:
        #FFF;
        filter: none;
        min-height: 317px;
    }
    .chck_eml_rd{
        color: red;
        font-weight: bold;
        font-size: 14px;
    }

</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.add_new_360degree')</h2>

         @include('includes.professional_tab_menu')
        <div class="bokcntnt-bdy">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">

                    <form id="myform" method="post" action="{{ route('professional-360evaluation.store') }}">
                        @csrf
                        <div class="form_body">
                            <div class="row">



                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
                                    <div class="your-mail">
                                        <label for="exampleInputEmail1" class="personal-label">@lang('site.title') *</label>
                                        <input type="text" name="title" class="personal-type required" id="title" placeholder="@lang('site.title')" >
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
                                    <div id="fields">
                                        <div class="col-md-12 plus_row" id="fieldRow0">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="your-mail">
                                                        <label for="exampleInputEmail1">@lang('site.answer') *</label>
                                                        <input type="text" name="answer[]" class="personal-type required" id="field_value0" placeholder="@lang('site.answer')">
                                                        <div class="clearfix"></div>
                                                        <label for="exampleInputEmail1">@lang('site.chooce_your_answer_color')</label>
                                                        <div class="clearfix"></div>
                                                        <input type="color" name="color_box[]" class="personal-type required" id="field_value0" value="#ffffff" placeholder="@lang('site.answer')">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <button type="button" class="btn btn-success add-button addsingleselect" title="Add"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <input type="hidden" class="countvalues" value="1">


                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="submit-login add_btnm">
                                        <input value="@lang('site.add')" type="submit" class="login_submitt">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</section>




@endsection
@section('footer')
@include('includes.footer')
<style type="text/css">
    .upldd {
    display: block;
    width: auto;
    border-radius: 4px;
    text-align: center;
    background: #9caca9;
    cursor: pointer;
    overflow: hidden;
    padding: 10px 15px;
    font-size: 15px;
    color: #fff;
    cursor: pointer;
    float: left;
    margin-top: 15px;
    font-family: 'Poppins', sans-serif;
    position: relative;
}
.upldd input {
    position: absolute;
    font-size: 50px;
    opacity: 0;
    left: 0;
    top: 0;
    width: 200px;
    cursor: pointer;
}
.upldd:hover{
    background: #1781d2;
}

</style>
<script>
    $(document).ready(function(){
        $("#myform").validate();
    });
</script>
<script>
    $(document).ready(function(){
        $('body').on('click','.addsingleselect',function(){
        var count = $('.countvalues').val();
            var singleselect = '<div class="col-md-12" id="fieldRow'+count+'"><div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">\
                                                <div class="your-mail">\
                                                    <label for="exampleInputEmail1">@lang('site.answer') *</label>\
                                                    <input type="text" name="answer[]" class="form-control required" id="field_value'+count+'" placeholder="@lang('site.answer')">\
                                                     <label for="exampleInputEmail1">Choose your answer color</label>\
                                                        <div class="clearfix"></div>\
                                                   <input type="color" name="color_box[]" class="form-control required" id="field_value0" value="#ffffff" placeholder="@lang('site.answer')">\
                                                </div>\
                                            </div>\
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">\
                                                <button type="button" class="btn btn-danger add-button removesingleselect" title="Remove" data-id="'+count+'"><i class="fa fa-minus" aria-hidden="true"></i></button>\
                                            </div></div></div><div class="clearfix"></div>';
            $('#fieldRow0').before(singleselect);
        });
        $('body').on('click','.removesingleselect',function(){
            var id = $(this).data('id');
            $('#fieldRow'+id).remove();
        });
    });
</script>






<style>
    .error{
        color: red !important;
    }
</style>
@endsection
