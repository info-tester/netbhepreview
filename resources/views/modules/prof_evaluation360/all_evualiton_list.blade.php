@extends('layouts.app')
@section('title')
    @lang('site.post') @lang('site.details')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<?php
    $competencesname = competencesName();
    $competencesdesc = competencesDesc();
?>

<section class="bkng-hstrybdy">
    <div class="container">
        <h2> @lang('site.evaluation_360view')</h2>



        <div class="bokcntnt-bdy no-margin-top">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.user_sidebar')
            <div class="dshbrd-rghtcntn">



                 <div class="dash_form_box">

                    @foreach($all_evulation_shair_url->getEvulation360Url as $row)


                        {{-- @if(@$row->user_type == "C")
                            $typeuse ="Customer";
                        @elseif(@$row->user_type == "M")
                            $typeuse ="Manager";
                        @elseif(@$row->user_type == "O")
                            $typeuse ="Others";
                        @elseif(@$row->user_type == "P")
                            $typeuse ="Pairs";
                        @elseif(@$row->user_type == "S")
                            $typeuse ="Subordinates";
                        @elseif(@$row->user_type == "U")
                            $typeuse ="Users";
                        @else
                        $typeuse ="";
                        @endif --}}

                        @if(@$row->user_type == "C")
                            <p style="overflow-wrap: break-word"><strong>@lang('site.share_link_customer') :</strong> {{ route('pblc.evaluation',['id'=>encrypt(@$row->user_to_tools_id) ,'type'=>@$row->user_type]) }}</p>
                        @elseif(@$row->user_type == "M")
                            <p style="overflow-wrap: break-word"><strong>@lang('site.share_link_manager') :</strong> {{ route('pblc.evaluation',['id'=>encrypt(@$row->user_to_tools_id) ,'type'=>@$row->user_type]) }}</p>
                        @elseif(@$row->user_type == "O")
                            <p style="overflow-wrap: break-word"><strong>@lang('site.share_link_others') :</strong> {{ route('pblc.evaluation',['id'=>encrypt(@$row->user_to_tools_id) ,'type'=>@$row->user_type]) }}</p>
                        @elseif(@$row->user_type == "P")
                            <p style="overflow-wrap: break-word"><strong>@lang('site.share_link_pairs') :</strong> {{ route('pblc.evaluation',['id'=>encrypt(@$row->user_to_tools_id) ,'type'=>@$row->user_type]) }}</p>
                        @elseif(@$row->user_type == "S")
                            <p style="overflow-wrap: break-word"><strong>@lang('site.share_link_subordinates'):</strong> {{ route('pblc.evaluation',['id'=>encrypt(@$row->user_to_tools_id) ,'type'=>@$row->user_type]) }}</p>

                        @elseif(@$row->user_type == "U")
                            <p style="overflow-wrap: break-word"><strong>@lang('site.share_link_users'):</strong> {{ route('pblc.evaluation',['id'=>encrypt(@$row->user_to_tools_id) ,'type'=>@$row->user_type]) }}</p>
                        @endif

                        @endforeach
                    <?php /*<strong>Share link : {{ route('pblc.evaluation',['id'=>@$evaluations_user_link]) }}</strong>
                    */?>
                    </br></br>
                    <h5>@lang('site.submit_your_answer'):</h5>
                    <form id="myform" method="get" action="{{ route('user.evaluation360.score.post') }}">

                        @csrf
                        <div class="form_body">
                            <div class="row">
                                @foreach($all_evaluation_data as $key => $details1)

                                <div class="toolcompte">
                                        <h4><?= @$competencesname[@$details1[$key]['tool360_competences_id']]; ?></h4>

                                        <p><?= @$competencesdesc[@$details1[$key]['tool360_competences_id']]; ?></p>

                                    @foreach($details1 as $details)
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
                                        <div class="your-mail">

                                            <label for="exampleInputEmail1" class="competences_sel">{{@$details->tool360_competences}}</label>
                                            <select name="user_answer_point[]" class="required form-control newdrop required " style="margin-bottom: 10px;" required="">
                                                    <option value="">@lang('site.select')</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                            </select>
                                            <textarea cols="75" class="form-control" name="report_note[]" placeholder="@lang('site.report_item')" ></textarea>
                                            <input type="hidden" value="{{@$details->id}}" name="evaluation_user_answer_id[]">
                                            <input type="hidden" value="{{@$user_to_tools_id}}" name="user_to_tools_id">

                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                @endforeach


                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="submit-login add_btnm">
                                        <input value="@lang('site.submit')" type="submit" class="login_submitt">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>

                </div>



            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')


@endsection
