@extends('layouts.app')
@section('title')
    {{-- @lang('site.add_new_blog') --}}
    360degree evaluation
@endsection
@section('style')
@include('includes.style')
<style type="text/css">
    div.mce-edit-area {
        background:
        #FFF;
        filter: none;
        min-height: 317px;
    }
    .chck_eml_rd{
        color: red;
        font-weight: bold;
        font-size: 14px;
    }

    .tooltip {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted #ccc;
        color: #006080;
        opacity: 1 !important;
    }

    .tooltip .tooltiptext {
        visibility: hidden;
        position: absolute;
        width: 250px;
        background-color: #555;
        color: #fff;
        text-align: center;
        padding: 5px;
        border-radius: 6px;
        z-index: 1;
        opacity: 0;
        transition: opacity 0.3s;
        margin-left: 10px
    }

    .tooltip-right {
        top: 0px;
        left: 125%;
    }

    .tooltip-right::after {
        content: "";
        position: absolute;
        top: 10%;
        right: 100%;
        margin-top: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: transparent #555 transparent transparent;
    }

    .tooltip:hover .tooltiptext {
        visibility: visible;
        opacity: 1;
    }
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.360evaluation')</h2>

         @include('includes.professional_tab_menu')
        <div class="bokcntnt-bdy">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">
                    <form id="myform" method="post" action="{{ route('prof-evaluation360.store') }}">
                        @csrf
                        <div class="form_body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
                                    <div class="your-mail">
                                        <label for="exampleInputEmail1">
                                            @lang('site.Select_User')
                                            <div class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">{{__('site.select_user_360_tooltip')}}</span>
                                            </div>
                                        </label>
                                        <select name="user_id[]" class="required form-control newdrop required chosen-select" data-placeholder="Selecione uma Opção" multiple="true" required="">
                                            <option value="">Selecionar</option>
                                            @foreach($all_paid_users as $row)
                                                <option value="{{$row->user_id}}">{{@$row->userDetails->nick_name ? @$row->userDetails->nick_name : @$row->userDetails->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
                                    <div class="your-mail">
                                        <label for="exampleInputEmail1" class="personal-label">
                                            @lang('site.Select_the_category_of_evaluation'):
                                            <div class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">{{__('site.select_360_eval_category')}}</span>
                                            </div>
                                        </label>
                                        <select name="" id="cat" class="personal-type">
                                            <option value="">Selecionar</option>
                                            @foreach($categories as $cat )
                                                <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
                                    <div class="your-mail">
                                        <label for="exampleInputEmail1" class="personal-label">
                                            @lang('site.Select_the_type_of_evaluation'):
                                            <div class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">{{__('site.select_360_eval_type')}}</span>
                                            </div>
                                        </label>
                                        <select name="evaluation_type" id="evaluation_type" class="personal-type evaluation_type">
                                            <option value="">Selecionar</option>
                                            {{-- @foreach($all_type_evaluation as $evaluation )
                                                <option value="{{ $evaluation->id }}">{{ $evaluation->title }}</option>
                                            @endforeach --}}
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
                                    <div class="your-mail competences" id="competences">
                                        <label for="exampleInputEmail1" class="personal-label">
                                            @lang('site.Competences_to_be_assessed'):
                                            <div class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">{{__('site.select_360_eval_competences')}}</span>
                                            </div>
                                        </label>
                                        <div class="row" style="float:left;width: 100%">
                                            {{-- @foreach($all_competencesmaster as $competencescc )
                                                <div class="col-lg-6">
                                                    <input type="checkbox" id="" name="competences[]">
                                                    <input type="hidden" id="" name="competences_question[]">
                                                    <input type="hidden" id="" name="competences_id[]">
                                                    <label for="{{ $competencescc->title }}">{{ $competencescc->title }}</label>
                                                </div>
                                            @endforeach --}}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
                                    <div class="your-mail">
                                        <label for="exampleInputEmail1" class="personal-label">
                                            @lang('site.Reviewer_email_one')
                                            <div class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">{{__('site.360_reviewer_one_tooltip')}}</span>
                                            </div>
                                        </label>
                                        <input type="text" name="review_email" class="personal-type required" id="title" placeholder="{{__('site.reviewer_email')}}" value="{{ old('review_email') }}">
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
                                    <div class="your-mail">
                                        <label for="exampleInputEmail1" class="personal-label">
                                            @lang('site.Reviewer_email_two')
                                            <div class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">{{__('site.360_reviewer_two_tooltip')}}</span>
                                            </div>
                                        </label>
                                        <input type="text" name="evaluations_review_email_one" class="personal-type required" id="title" placeholder="{{__('site.reviewer_email')}}" value="{{ old('evaluations_review_email_one') }}">
                                    </div>
                                </div>

                                <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
                                    <div class="your-mail">
                                        <label for="exampleInputEmail1" class="personal-label">Link</label>
                                        <input type="text" name="user_link" class="personal-type required" id="title" placeholder="https://www.netbhe.com/devNet/" >
                                    </div>
                                </div> -->

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
                                    <div class="your-mail">
                                        <label for="exampleInputEmail1" class="personal-label">
                                            @lang('site.Deadline_for_evaluations')*
                                            <div class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">{{__('site.360_review_deadline')}}</span>
                                            </div>
                                        </label>
                                        <input type="text" autocomplete="off" name="evaluations_deadline" id="datepicker" class="personal-type required datepicker" placeholder="17/03/2020">

                                    </div>
                                </div>


                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="submit-login add_btnm">
                                        <input value="@lang('site.add')" type="submit" class="login_submitt">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('footer')
@include('includes.footer')
<style type="text/css">
    .upldd {
    display: block;
    width: auto;
    border-radius: 4px;
    text-align: center;
    background: #9caca9;
    cursor: pointer;
    overflow: hidden;
    padding: 10px 15px;
    font-size: 15px;
    color: #fff;
    cursor: pointer;
    float: left;
    margin-top: 15px;
    font-family: 'Poppins', sans-serif;
    position: relative;
}
.upldd input {
    position: absolute;
    font-size: 50px;
    opacity: 0;
    left: 0;
    top: 0;
    width: 200px;
    cursor: pointer;
}
.upldd:hover{
    background: #1781d2;
}
//
</style>
<script>
    var types = {!! json_encode($all_type_evaluation->toArray() ?? []) !!}
    $(document).ready(function() {
        $('#cat').change(function() {
            var val = $(this).val();
            var chosenOpts = '<option value="">Selecionar</option>';
            types.forEach(v => {
                if (v.evaluation_category_id == val) {
                    chosenOpts += '<option value="' + v.id + '">' + v.title + '</option>'
                }
            });
            $('#evaluation_type').html(chosenOpts);
        });

        $(".chosen-select").chosen();

        $("#myform").validate();
    });
</script>




<script>
    $(document).ready(function(){
        $('.evaluation_type').change(function(){
            if($(this).val() != ''){
                // var select = $(this).attr("id");
                var value = $(this).val();
                // var dependent = $(this).data('dependent');
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('dynamicdependent.fetchcompetences') }}",
                    method:"POST",
                    data:{value:value, _token:_token},
                    success:function(result)
                    {
                        $('#competences').html(result);
                    }
                });
            }
        });
        $('#evaluation_type').change(function(){
            $('#competences').val('');
        });

    });
</script>

<script>
    $(document).ready(function(){
        $('.datepicker').datepicker();
        $("#myform").validate();
    });
</script>



<style>
    .error{
        color: red !important;
    }
</style>
@endsection
