@extends('layouts.app')
@section('title')
  {{-- @lang('site.view_booked_user_details') --}}
  User submit answer
@endsection
@section('style')
@include('includes.style')
<style>
   .ui-timepicker-standard {
    position: absolute;
    z-index: 9999 !important;
}
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')
<section class="bkng-hstrybdy">
    <div class="container">
        {{-- <h2>@lang('client_site.class_details')</h2> --}}
        <h2>@lang('site.coaching_tools')</h2>
        @if(Auth::user()->is_professional=="Y")
            {{-- @include('includes.professional_tab_menu') --}}
        @endif
        <div class="bokcntnt-bdy">
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>

            @if(Auth::user()->is_professional=="Y")
                @include('includes.professional_sidebar')
            @else
                @include('includes.user_sidebar')
            @endif
            
            <div class="dshbrd-rghtcntn">
                <strong>@lang('site.share_link_list')</strong>
                <?php 
                //$ddmm = Crypt::encrypt(@$user_tools_id);
                //pr1($ddmm);
                $usertoolsid = encrypt(@$user_tools_id);
                ?>
                
                <p style="overflow-wrap: break-word"><strong>@lang('site.customer') : </strong> {{ route('pblc.evaluation',['id'=> $usertoolsid,'type'=>"C"]) }} </p>
                <p style="overflow-wrap: break-word"><strong>@lang('site.manager') : </strong> {{ route('pblc.evaluation',['id'=>$usertoolsid ,'type'=>"M"]) }}</p>
                <p style="overflow-wrap: break-word"><strong>@lang('site.others') : </strong>{{ route('pblc.evaluation',['id'=>@$usertoolsid ,'type'=>"O"]) }}</p>
                <p style="overflow-wrap: break-word"><strong>@lang('site.pairs') : </strong>{{ route('pblc.evaluation',['id'=>@$usertoolsid ,'type'=>"P"]) }}</p>
                <p style="overflow-wrap: break-word"><strong>@lang('site.subordinates') : </strong>{{ route('pblc.evaluation',['id'=>@$usertoolsid ,'type'=>"S"]) }}</p>
                <p style="overflow-wrap: break-word"><strong>@lang('site.users') : </strong>{{ route('pblc.evaluation',['id'=>@$usertoolsid ,'type'=>"U"]) }}</p>
                
                <strong>@lang('site.360evaluation_review')</strong>
                <div class="col-md-12 view-page coach_tools">
                    <ul class="competencestools">
                        @foreach(@$all_usertype_anspoint as $usertype_anspoint)                       
                            
                                <li>
                                    <strong style="text-transform: capitalize;">{{ $usertype_anspoint->user_type }} </strong>                            
                                    <span><b>:</b>{{ number_format($usertype_anspoint->avg_ans_point) }}</span>                                
                                </li>
                        @endforeach 
                    </ul>
                </div>

                

                <div class="buyer_table">
                    <div class="table-responsive">
                        @if(@$all_evaluation_data->isNotEmpty())
                        <div class="table">
                            <div class="one_row1 hidden-sm-down only_shawo">                                
                                <div class="cell1 tab_head_sheet">@lang('site.competences')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.User_point')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.Customer_point')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.Manager_point')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.Other_point')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.Pairs_point')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.Subordinates_point')</div>
                            </div>
                            <!--row 1-->                                
                                @foreach(@$all_evaluation_data as $ans_data)                        
                                    <div class="one_row1 small_screen31">

                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">Components</span>
                                            <p class="add_ttrr">
                                                {{ $ans_data->tool360_competences }}
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">User point</span>
                                            <p class="add_ttrr">
                                                
                                                <a href="javascript:;" class="show_comment" data-title="{{$ans_data->tool360_competences}}" data-repordcomment="{{ $ans_data->report_note }}" style="margin: 2px;">
                                                    {{ $ans_data->user_answer_point }}
                                                </a> 
                                            </p>
                                        </div>
                                        
                                        <div class="cell1 tab_head_sheet_1"> 
                                        
                                            <a href="javascript:;" class="show_comment" data-title="{{$ans_data->tool360_competences}}" data-repordcomment="{{ $ans_data->customer_report_note }}" style="margin: 2px;">
                                                {{ $ans_data->customer_answer_point ? $ans_data->customer_answer_point : 'NA' }} 
                                            </a>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">                                           
                                            <p class="add_ttrr">                                                
                                                <a href="javascript:;" class="show_comment" data-title="{{$ans_data->tool360_competences}}" data-repordcomment="{{ $ans_data->manager_report_note }}" style="margin: 2px;">
                                                    {{ $ans_data->manager_answer_point ? $ans_data->manager_answer_point :'NA' }}
                                                </a>
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">Other point</span>
                                            <p class="add_ttrr">                                                
                                                <a href="javascript:;" class="show_comment" data-title="{{$ans_data->tool360_competences}}" data-repordcomment="{{ $ans_data->others_report_note }}" style="margin: 2px;">
                                                    {{ $ans_data->others_answer_point ? $ans_data->others_answer_point : 'NA' }}
                                                </a>
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">Pairs point</span>
                                            <p class="add_ttrr">                                                
                                                <a href="javascript:;" class="show_comment" data-title="{{$ans_data->tool360_competences}}" data-repordcomment="{{ $ans_data->pairs_report_note }}" style="margin: 2px;">{{ $ans_data->pairs_answer_point ? $ans_data->pairs_answer_point : 'NA' }}</a>
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">Subordinates point</span>
                                            <p class="add_ttrr">                                                
                                                <a href="javascript:;" class="show_comment" data-title="{{$ans_data->tool360_competences}}" data-repordcomment="{{ $ans_data->subordinates_report_note }}" style="margin: 2px;">{{ $ans_data->subordinates_answer_point ? $ans_data->subordinates_answer_point : 'NA' }}</a>
                                            </p>
                                        </div>
                                       
                                        
                                    </div>                                    
                                @endforeach
                            
                        </div>
                        @else
                            <div class="one_row small_screen31">
                                <center><span><h3 class="error">OOPS! não encontrado.</h3></span></center>
                            </div>
                        @endif
                    </div>
                </div>





                
            </div>
        </div>
    </div>



<!--reports comments -->
<div class="modal fade" id="assignModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><span class="tool-title"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>


                <div class="modal-body">
                    <h5>@lang('site.comments'): </h5>

                    <span class="compontes-report-comment"></span>

                        
                </div>
            
                <div class="modal-footer">                
                </div>

            </form>
        </div>
    </div>
</div>




</section>
@endsection
@section('footer')
@include('includes.footer')
<link href="{{ URL::to('public/frontend/css/calender.css') }}" rel="stylesheet" type="text/css">
<script src="{{ URL::to('public/frontend/js/jquery-ui.js') }}"></script>

<script>
    
    $(document).ready(function(){
    $('.show_comment').click(function(){
        var title = $(this).data('title');
        var repordcomment = $(this).data('repordcomment');
        var id = $(this).data('id');
        $('.tool-title').html(title);
        $('.compontes-report-comment').html(repordcomment);
        
        $('#assignModal').modal('show');
    });
});

</script>

<style>
/*    .competencestools {
    float: left;
    width: 100%;
}

.competencestools li {
    float: left;
    width: 49%;
    margin-bottom: 5px;
    font-family: 'Poppins', sans-serif;
    color: #252424;
    font-size: 15px;
    border: 1px solid #dfdede;
    padding: 6px 15px;
    margin-right: 1%;
    background: #f9f9f9;
    line-height: 23px;
}

.competencestools li span {
    color: #1482cf;
    font-size: 17px;
    margin-left: 4px;
    width: 25%;
    float: left;
}
.competencestools li strong {
    font-weight: 500;
    width: 73%;
    float: left;
}
.competencestools li span b {
    float: left;
    margin-right: 10px;
    font-weight: 500;
    color: #000;
}*/
</style>

@endsection