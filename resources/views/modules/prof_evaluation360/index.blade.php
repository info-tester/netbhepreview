@extends('layouts.app')
@section('title')
    {{-- @lang('site.my_blogs') --}}
    360º evaluation
@endsection
@section('style')
@include('includes.style')

@endsection
@section('scripts')
@include('includes.scripts')
<script src="{{URL::to('public/js/chosen.jquery.min.js')}}"></script>
@endsection
@section('header')
@include('includes.professional_header')
<link rel="stylesheet" href="{{URL::to('public/css/chosen.css')}}">
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>Avaliação 360º</h2>
        

		{{-- <div class="like-tab">
        	<ul>
            	<li><a class="active" href="#">Form tools</a></li>
                <li><a href="#">Tab tow</a></li>
                <li><a href="#">Tab three</a></li>
                <li><a href="#">Tab four</a></li>
            </ul>
        </div> --}}

        @include('includes.professional_tab_menu')

        <div class="bokcntnt-bdy no-margin-top">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>

            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">

                <div class="" style="float: right;">
                    <a class="btn btn-success" href="{{route('professional-360evaluation.create')}}">+ @lang('site.add_new_360degree')</a>
                </div>


                
                <div class="buyer_table">
                    <div class="table-responsive">
                        @if(sizeof(@$all360data)>0)
                        <div class="table">
                            <div class="one_row1 hidden-sm-down only_shawo">
                                <div class="cell1 tab_head_sheet">@lang('site.title')</div>
                               
                                <div class="cell1 tab_head_sheet">{{-- Added By --}}@lang('site.added_by')</div>
                                {{-- <div class="cell1 tab_head_sheet">posted on</div> --}}
                                <div class="cell1 tab_head_sheet">@lang('site.status')</div>
                                <div class="cell1 tab_head_sheet">@lang('site.action'){{-- @lang('site.action') --}}</div>
                            </div>
                            <!--row 1-->   
                            <?php 
                            //pr1($all360data->toArray());
                            //die();
                            ?>

                                @foreach(@$all360data as $details)                        
                                    <div class="one_row1 small_screen31">
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.no')</span>
                                            <p class="add_ttrr">{{$details->title}}</p>
                                        </div>

                                        
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">added by</span>
                                            <p class="add_ttrr">{{ $details['added_by']=='A'?'Admin': ( @$details->getUserData->nick_name ? @$details->getUserData->nick_name : @$details->getUserData->name ) }}</p>
                                        </div>
                                        
                                        
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.status')</span>
                                            <p class="add_ttrr">
                                                {{ $details['status'] }}
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                            @if($details->added_by=="P" && $details->added_by_id == Auth::id())
                                                <a href="{{route('professional-360evaluation.edit',[$details->id])}}" class="acpt editbtn" style="margin: 2px;" title="Edit">@lang('site.edit')</a>                                                

                                                {{-- <a href="javascript:void(0)" onclick="deleteCategory('{{route('professional-form.destroy',[$detail->id])}}')" title="Delete"> <i class="fa fa-trash-o delet" aria-hidden="true"></i></a> --}}


                                                <a href="javascript:void(0)" onclick="deleteCategory('{{route('professional-360evaluation.destroy',[$details->id])}}')" title="Delete" class="rjct delbtn"> @lang('site.delete')</a>
                                                {{-- <a href="{{route('remove.blog.post', ['id'=>@$blg->id])}}" class="rjct delbtn" style="margin: 2px;">@lang('site.delete')</a> --}}
                                                {{--   <a href="{{route('view.blog.post', ['id'=>@$blg->id])}}" class="acpt">Assign</a> --}}                                                
                                            
                                                {{--   <a href="{{route('view.blog.post', ['id'=>@$blg->id])}}" class="acpt">Assign</a> --}}          
                                            @endif
                                            {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-title="{{$form_data->form_title}}" data-target="#exampleModalCenter">
                                              Assign
                                            </button> --}}
                                            <a href="{{route('professional-360evaluation.show', [$details->id])}}" class="acpt viewbtn" style="margin: 2px;" title="View Question">@lang('site.view')</a>
                                            <a href="javascript:;" class="show_assign acpt assginbtn" data-title="{{$details->title}}" data-id="{{ $details->id }}" style="margin: 2px;">@lang('site.assign')</a>
                                            
                                            {{-- <a href="{{route('prof.360asign.view', [$details->id])}}" class="acpt view_asgi_btn" style="margin: 2px;">@lang('site.view_assign')</a> --}}
                                            

                                            <a href="{{route('prof.360asign.userlist', [$details->id])}}" class="acpt view_asgi_btn" style="margin: 2px;">@lang('site.view_assign')</a>
                                        </div>
                                    </div>                                    
                                @endforeach
                            
                        </div>
                        @else
                            <div class="one_row small_screen31">
                                <center><span><h3 class="error">Oops! não encontrado.</h3></span></center>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

<!--assigne Modal -->
<div class="modal fade" id="assignModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">@lang('site.Assign_User')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="myform" method="post" action="{{ route('prof.360evaluation.asign') }}">
                {{ csrf_field() }}
                <div class="modal-body">
                    <h5>Título da ferramenta: <span class="tool-title"></span></h5>

                        <label for="exampleInputEmail1">@lang('site.Select_User')</label>
                        <select name="user_id[]" class="required form-control newdrop required chosen-select" multiple="true" required="">
                            <option value="">Select</option>
                            @foreach($all_paid_users as $row)
                                <option value="{{$row->user_id}}">{{@$row->userDetails->nick_name ? @$row->userDetails->nick_name :@$row->userDetails->name}}</option>
                            @endforeach
                        </select>
                    <input type="hidden" name="tool_360_master_id" class="form_id"> {{-- this calass form id get form id --}}
                </div>
            
                <div class="modal-footer">                
                    <button type="submit" value="submit" class="btn btn-primary">@lang('site.save')</button>
                </div>

            </form>
        </div>
    </div>
</div>

</section>




@endsection
@section('footer')
@include('includes.footer')

<script>

$(document).ready(function(){
    $('.show_assign').click(function(){
        var title = $(this).data('title');
        var id = $(this).data('id');
        $('.tool-title').html(title);
        $('.form_id').val(id);
        $('#assignModal').modal('show');
    });
});

$(".chosen-select").chosen();

$(document).ready(function(){
        $("#myform").validate();
    });
</script>

<form method="post" id="destroy">
    {{ csrf_field() }}
    {{method_field('delete')}}
</form>
<script>
 function deleteCategory(val){
    var confirm = window.confirm('Do you want to delete this question?');
    if(confirm){
        $("#destroy").attr('action',val);
        $("#destroy").submit();
    }
 }
</script>
<style>
    .error{
        color: red !important;
    }
</style>

@endsection