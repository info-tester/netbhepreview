@extends('layouts.app')
@section('title')
  @lang('site.my_availability')
@endsection
@section('style')
@include('includes.style')
<style>
.time_slot_span{
    background: #1781d2;
    padding: 2px 7px;
    border: solid 1px #475dfb;
    border-radius: 12px;
    color: #fff;
    margin: 0 15px 10px 0;
    float: left;
    cursor: pointer;

}

}
.ui-datepicker{
	z-index:2 !important;
	}
.form-group {
	 width: 100% !important;
}
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        {{-- <h2>@lang('client_site.availability')</h2> --}}
        <h2>@lang('site.assigned_users')</h2>

        @include('includes.professional_tab_menu')

        <div class="bokcntnt-bdy no-margin-top">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp

            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
            {{-- @include('includes.user_sidebar') --}}
            @include('includes.professional_sidebar')

            <div class="dshbrd-rghtcntn">

                <div class="" style="float: right;">
                    <a class="btn btn-success" href="{{ route('prof-evaluation360.create') }}"> @lang('site.Assign_User')  {{--  @lang('site.add_imported_tools') --}}</a>

                </div>

                <div class="mainDiv">
                    @if(sizeof(@$all_user_data)>0)
                        <div class="buyer_table">
                            <div class="table-responsive">
                                <div class="table">
                                    <div class="one_row1 hidden-sm-down only_shawo">

                                        <div class="cell1 tab_head_sheet">@lang('site.user_name')</div>
                                        <div class="cell1 tab_head_sheet">@lang('site.tool_type')</div>
                                        <div class="cell1 tab_head_sheet">@lang('site.assign_date')</div>
                                        <!-- <div class="cell1 tab_head_sheet">Link</div> -->
                                        <div class="cell1 tab_head_sheet">@lang('site.status')</div>
                                        <div class="cell1 tab_head_sheet">@lang('site.action')</div>
                                    </div>
                                    <!--row 1-->
                                    @foreach(@$all_user_data as $ql)
                                        <div class="one_row1 small_screen31">

                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('site.user_name')</span>
                                                <p class="add_ttrr">{{ @$ql->getprofessionalUserData->nick_name ? @$ql->getprofessionalUserData->nick_name : @$ql->getprofessionalUserData->name }}</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('site.tool_type')</span>
                                                <p class="add_ttrr">{{ @$ql->tool_type }}</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('site.assign_date')</span>
                                                <p class="add_ttrr">{{ Date::parse(@$ql->assign_date)->format('m/d/Y (D)') }}</p>
                                            </div>

                                           <!--  <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">link</span>


                                                @if($ql->getEvulation360Url)
                                                    @foreach($ql->getEvulation360Url as $value)
                                                        <p>{{ route('pblc.evaluation',['id'=>@$value->user_to_tools_id ,'type'=>@$value->user_type]) }}<p>
                                                    @endforeach
                                                @endif
                                            </div> -->

                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('site.status')</span>
                                                <p class="add_ttrr">
                                                    @if($ql->status=='COMPLETED')
                                                    @lang('site.tools_completed')
                                                    @elseif($ql->status=='INVITED')
                                                    @lang('site.tools_invited')
                                                    @else
                                                    --
                                                    @endif
                                                </p>
                                            </div>


                                            <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                                <span class="W55_1">@lang('client_site.action')</span>

                                                <a href="{{ route('user.evaluation360.score.listing',['id'=>@$ql->id]) }}" ><i class="fa fa-eye"></i> @lang('site.view')</a>
                                                <!-- @if($ql->status == "COMPLETED")
                                                    <a href="{{ route('prof.360asign.view',['id'=>@$ql->tool_id]) }}" ><i class="fa fa-eye"></i> View</a>
                                                @else
                                                N.A
                                                @endif -->
                                                {{-- <a href="#" class="rjct" style="background: #098005;"><i class="fa fa-pencil"></i> @lang('client_site.edit') </a> --}}
                                            </div>
                                        </div>
                                    @endforeach
                                    {{-- {{ $avl->->onEachSide(5)->links() }} --}}
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')




</script>
@endsection
