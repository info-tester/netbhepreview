@extends('layouts.app')
@section('title')
  {{-- @lang('site.view_booked_user_details') --}}
  User submit answer
@endsection
@section('style')
@include('includes.style')
<style>
   .ui-timepicker-standard {
    position: absolute;
    z-index: 9999 !important;
}
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')

<?php
    $competencesname = competencesName();
    $competencesdesc = competencesDesc();
?>
<section class="bkng-hstrybdy">
    <div class="container">
        {{-- <h2>@lang('client_site.class_details')</h2> --}}
        <h2>Avaliação 360º </h2>
        {{-- @include('includes.professional_tab_menu') --}}

        <div class="bokcntnt-bdy">


            <div class="dshbrd">

                <h4 class="notestl">@lang('site.evaluation_rate_note')</h4>
                <!-- <div class="col-md-12 publicevulation coach_tools">
                    @foreach(@$all_evaluation_data as $ans_data)
                            <p>{{ @$ans_data->tool360_competences }}:</p>
                            <h5>{{ @$ans_data->user_answer_point }}</h5>
                    @endforeach
                </div> -->


                <div class="dash_form_box">
                    <form id="myform" method="get" action="{{ route('user.evaluation360.public.score.post') }}">

                        @csrf
                        <div class="form_body">
                            <div class="row">
                                @foreach($all_evaluation_data as $key => $details1)
                                    <div class="toolcompte">
                                        <h4>{{ @$competencesname[@$details1[$key]['tool360_competences_id']] }}</h4>

                                        <p>{{ @$competencesdesc[@$details1[$key]['tool360_competences_id']] }}</p>
                                        @foreach($details1 as $details)
                                            <div class="col-lg-12 col-md-6 col-sm-12 col-xs-12">
                                                <div class="your-mail pubevlu">

                                                    <label for="exampleInputEmail1">{{@$details->tool360_competences}}</label>
                                                    <select name="user_answer_point[]" class="required form-control newdrop required "  required="">
                                                        <option value="">Select</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                    </select>
                                                    <textarea cols="75" class="form-control" name="report_note[]" placeholder="You can report a note about this item"></textarea>
                                                    <input type="hidden" value="{{@$details->id}}" name="evaluation_user_answer_id[]">
                                                    <input type="hidden" value="{{@$user_to_tools_id}}" name="user_to_tools_id">
                                                    <input type="hidden" value="{{@$usertype}}" name="usertype">
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endforeach
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="submit-login add_btnm">
                                        <input value="@lang('site.submit')" type="submit" class="login_submitt">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
<link href="{{ URL::to('public/frontend/css/calender.css') }}" rel="stylesheet" type="text/css">
<script src="{{ URL::to('public/frontend/js/jquery-ui.js') }}"></script>
<style>
    .pubevlu{
        margin-top: 15px;
    }
    .pubevlu select{
        margin-bottom: 10px;
    }
    .notestl{
        font-size: 19px;
        font-weight: 600;
    }
</style>

@endsection
