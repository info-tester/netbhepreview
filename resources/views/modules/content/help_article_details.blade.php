@extends('layouts.app')
@section('title')
    @lang('site.help_section')
@endsection
@section('style')
    @include('includes.style')
@endsection
@section('scripts')
    @include('includes.scripts')
@endsection
@section('header')
    @include('includes.header')
@endsection
@section('content')
    <div class="login-body about-body">
        <div class="container">
            <div class="abut_page help_main_div">
                <!-- <div class="row">
                    <div class="col-md-12 mb-3">
                        <h2>{{$article->title}}</h2>
                        <span class="back_btn_terms"><a href="{{ route('help.articles', @$article->getCategory->slug) }}" class="p text-secondary"> <i class="fa fa-angle-double-left"></i> Back </a></span>
                    </div>
                    @if(@$article->image)
                    <div class="col-lg-5 col-md-5">
                        <div class="text-center w-25">
                            <img src="{{url('/')}}/storage/app/public/uploads/content_images/{{@$article->image}}" alt="Article image" width="100%">
                        </div>
                    </div>
                    @endif
                    <div class="col-lg-7 col-md-7">
                        <p style="white-space: pre-wrap;">{!! $article->description !!}</p>
                    </div>
                    @if(@$article->video)
                    <div class="text-center">
                        <iframe style="width: 65%; height: 375px;" src="https://www.youtube.com/embed/{{ $article->video }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                    @endif
                </div> -->

                <h2>{{$article->title}}</h2>
                <span class="back_btn_terms"><a href="{{ route('help.articles', @$article->getCategory->slug) }}" class="p text-secondary"> <i class="fa fa-angle-double-left"></i> Back </a></span>
                
                <!-- @if(@$article->video)
                    <div class="help_article_video">
                        <iframe style="width: 65%; height: 375px;" src="https://www.youtube.com/embed/{{ $article->video }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                @endif
                @if(@$article->image)
                    <em class="help_article_img">
                        <img src="{{url('/')}}/storage/app/public/uploads/content_images/{{@$article->image}}" alt="Article image" width="100%">
                    </em>
                @endif
                {!! $article->description !!} -->
                <div class="row">
                    @if(@$article->video && @$article->image)
                        <div class="col-md-12 col-sm-12 text-center">
                            <iframe style="width: 100%; height: 375px;" src="https://www.youtube.com/embed/{{ $article->video }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                        <div class="col-md-6 col-sm-6 offset-md-3 col-sm-12 text-center">
                            <img src="{{url('/')}}/storage/app/public/uploads/content_images/{{@$article->image}}" alt="Article image" width="100%">
                        </div>
                    @elseif(@$article->video)
                        <div class="col-md-12 col-sm-12 text-center">
                            <iframe style="width: 65%; height: 375px;" src="https://www.youtube.com/embed/{{ $article->video }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    @elseif(@$article->image)
                        <div class="col-md-6 col-sm-6 offset-md-3 col-sm-12 text-center">
                            <img src="{{url('/')}}/storage/app/public/uploads/content_images/{{@$article->image}}" alt="Article image" width="100%">
                        </div>
                    @endif
                    <div class="col-md-12">
                        {!! $article->description !!}
                    </div>
                </div>

            </div>
        </div> 
    </div>
@endsection
@section('footer')
    @include('includes.footer')
@endsection