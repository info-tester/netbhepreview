@extends('layouts.app')
@section('title')
    @lang('site.terms_of_service')
@endsection
@section('style')
    @include('includes.style')
@endsection
@section('scripts')
    @include('includes.scripts')
@endsection
@section('header')
    @include('includes.header')
@endsection
@section('content')
    <div class="login-body about-body">
        <div class="container">
            <div class="row">
                <div class="abut_page row">
                    <div class="col-md-12 mb-3"><h2>{{$terms_title->title}}</h2></div>
                    @foreach($terms as $term)
                        <!-- <a href="{{route('subterms.of.services', ['slug' => @$term->slug])}}">
                            <h5>{{ $term->title }}</h5>
                        </a> -->
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="card term_card">
                                <a href="{{route('subterms.of.services', ['slug' => @$term->slug])}}">
                                    <p>{{ $term->title }}</p>
                                </a>
                            </div>
                        </div>
                        <!-- <p>{!! @$term->description !!}</p> -->
                    @endforeach
                </div>
            </div>
        </div> 
    </div>
@endsection
@section('footer')
    @include('includes.footer')
@endsection