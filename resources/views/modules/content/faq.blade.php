@extends('layouts.app')
@section('title')
  @lang('site.faq')
@endsection
@section('style')
    @include('includes.style')
    <style>
        .fq-tabs li a {
            cursor: pointer;
            display: block;
            padding: 8px 25px;
            color: #1781d2;
            font-size: 17px;
            /* border: 1px solid #e5e5e5; */
            position: relative;
            -webkit-transition: all 0.4s ease;
            -o-transition: all 0.4s ease;
            transition: all 0.4s ease;
            background: #fff;
            /* text-transform: uppercase; */
            font-weight: 400;
            font-family: 'Roboto', sans-serif;
        }
        .fq-tabs li a.active {
            background: #1781d2 !important;
            color: #fff !important;
        }
        .fq-tabs {
            margin-bottom: 20px !important;
            margin-top: 0 !important;
        }
        .yt-vdd {
            height: 360px;
            width: 70%;
            margin: 0 auto;
        }
        .abut_page .tab-content .col-lg-12 {
            padding: 0 !important;
        }
        .fq-tabs .nav-item {
            width: auto;
        }
        @media (max-width: 991px) {
            .yt-vdd {
                height: 320px;
                width: 80%;
            }
        }
        @media (max-width: 767px) {
            .fq-tabs .nav-item {
                width: 100%;
            }
            .yt-vdd {
                height: 300px;
            }
        }
        @media (max-width: 480px) {
            .yt-vdd {
                height: 320px;
                width: 100%;
            }
        }
    </style>
@endsection
@section('scripts')
    @include('includes.scripts')
    <script>
		$(function() {
			var Accordion = function(el, multiple) {
				this.el = el || {};
				this.multiple = multiple || false;

				// Variables privadas
				var links = this.el.find('.link');
				// Evento
				links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
			}

			Accordion.prototype.dropdown = function(e) {
				var $el = e.data.el;
					$this = $(this),
					$next = $this.next();

				$next.slideToggle();
				$this.parent().toggleClass('open');

				if (!e.data.multiple) {
					$el.find('.pm').not($next).slideUp().parent().removeClass('open');
				};
			}

			var accordion = new Accordion($('#accordion'), false);
			var accordion1 = new Accordion($('#accordion1'), false);
		});
	</script>
@endsection
@section('header')
    @include('includes.header')
@endsection
@section('content')
    <div class="login-body about-body">
        <div class="container">
            <div class="row">
                <div class="abut_page">
                    <ul class="nav nav-tabs justify-content-center fq-tabs" style="margin-top: 0 !important;">
                        <li class="nav-item">
                            <a class="nav-link active" href="#t1" id="tab_user" data-toggle="tab">{{ $faq->title }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#t2" id="tab_prof" data-toggle="tab">{{ $faq->heading_1 }}</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="t1" role="tabpanel" aria-labelledby="home-tab">
                            @if ($faq->heading_2)
                                @php
                                    preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $faq->heading_2, $match);
                                    $youtube_id_1 = @$match[1];
                                @endphp
                                <h2>{{ $faq->title }}</h2>
                                <div class="yt-vdd">
                                    <iframe style="width: 100%; height: 100%;" src="https://www.youtube.com/embed/{{ $youtube_id_1 }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                </div>
                            @endif
                            <div class="col-lg-12 col-md-12">
                                <ul id="accordion" class="accordion">
                                    @foreach ($faqUser as $fe)
                                        <li>
                                            <div class="link" id="link-{{ $fe->id }}">{{ $fe->title }}</div>
                                            <div class="pm" id="pm-{{ $fe->id }}">
                                                <div class="row">
                                                    @if(@$fe->video)
                                                        <div class="col-md-6">
                                                            <p style="white-space: pre-wrap;">{!! $fe->description !!}</p>
                                                        </div>
                                                        <div class="col-md-6">
                                                            @php
                                                                preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $fe->video, $match);
                                                                $video = @$match[1];
                                                            @endphp
                                                            <iframe style="width: 100%; height: 300px;" src="https://www.youtube.com/embed/{{ $video }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                                        </div>
                                                    @else
                                                        <div class="col-md-12">
                                                            <p style="white-space: pre-wrap;">{!! $fe->description !!}</p>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="t2" role="tabpanel" aria-labelledby="home-tab">
                            @if ($faq->heading_3)
                                @php
                                    preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $faq->heading_3, $match);
                                    $youtube_id_2 = @$match[1];
                                @endphp
                                <h2>{{ $faq->heading_1 }}</h2>
                                <div class="yt-vdd">
                                    <iframe style="width: 100%; height: 100%;" src="https://www.youtube.com/embed/{{ $youtube_id_2 }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                </div>
                            @endif
                            <div class="col-lg-12 col-md-12">
                                <ul id="accordion1" class="accordion">
                                    @foreach ($faqExpert as $fe)
                                        <li>
                                            <div class="link" id="link-{{ $fe->id }}">{{ $fe->title }}</div>
                                            <div class="pm" id="pm-{{ $fe->id }}">
                                                <div class="row">
                                                    @if(@$fe->video)
                                                        <div class="col-md-6">
                                                            <p style="white-space: pre-wrap;">{!! $fe->description !!}</p>
                                                        </div>
                                                        <div class="col-md-6">
                                                            @php
                                                                preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $fe->video, $match);
                                                                $video = @$match[1];
                                                            @endphp
                                                            <iframe style="width: 100%; height: 300px;" src="https://www.youtube.com/embed/{{ $video }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                                        </div>
                                                    @else
                                                        <div class="col-md-12">
                                                            <p style="white-space: pre-wrap;">{!! $fe->description !!}</p>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')
    @include('includes.footer')
    <script>
        var url = window.location.href;
        if(url.includes("/prof")){
            $("#tab_prof").trigger("click");
            $("#t1").css('display','none');
            $("#t2").css('display','block');
            var id = url.substring(url.lastIndexOf("/")+1, url.length);
            $("#pm-"+id).css('display','block');
            $("body,html").animate({
                scrollTop: $("#pm-"+id).offset().top - 40
            }, 800);
        } else if(url.includes("/user")){
            $("#tab_user").trigger("click");
            $("#t1").css('display','block');
            $("#t2").css('display','none');
            var id = url.substring(url.lastIndexOf("/")+1, url.length);
            $("#pm-"+id).css('display','block');
            $("body,html").animate({
                scrollTop: $("#pm-"+id).offset().top - 40
            }, 800);
        }
        $('#tab_prof').click(function(){
           $('#t2').show();
           $('#t1').hide();
        });
        $('#tab_user').click(function(){
           $('#t1').show();
           $('#t2').hide();
        });
    </script>
@endsection
