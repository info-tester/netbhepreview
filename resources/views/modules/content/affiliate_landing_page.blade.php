@extends('layouts.app')
@section('title')
	@lang('client_site.affiliate_landing_page')
@endsection
@section('style')
@include('includes.style')


      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <!--style-->
      <link href="{{ URL::to('public/frontend/css/style.css') }}" type="text/css" rel="stylesheet" media="all" />
      <link href="{{ URL::to('public/frontend/css/responsive.css') }}" type="text/css" rel="stylesheet" media="all" />
      <!--bootstrape-->
      <link href="{{ URL::to('public/frontend/css/bootstrap.min.css') }}" type="text/css" rel="stylesheet" media="all" />
      <!--font-awesome-->
      <link href="{{ URL::to('public/frontend/css/font-awesome.min.css') }}" type="text/css" rel="stylesheet" media="all" />
      <!--fonts-->
      <!--<link href="https://fonts.googleapis.com/css?family=Didact+Gothic" rel="stylesheet"> -->
      <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,900" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900&display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900&display=swap" rel="stylesheet">
      <!-- Owl Stylesheets -->
      <link rel="stylesheet" href="{{ URL::to('public/frontend/css/owl.carousel.min.css') }}">
      <link rel="stylesheet" href="{{ URL::to('public/frontend/css/owl.theme.default.min.css') }}">
      <!--price range (Calender/date picker)-->
      <link href="{{ URL::to('public/frontend/css/themes_smoothness_jquery-ui.css') }}" rel="stylesheet" type="text/css">
	  <style>
		  .bigger_icon {
			font-size: 3rem !important;
		  }
	  </style>
@endsection
@section('scripts')
@include('includes.scripts')
      <script type="text/javascript" src="{{ URL::to('public/frontend/js/jquery-3.2.1.js') }}"></script>
      <script type="text/javascript" src="{{ URL::to('public/frontend/js/bootstrap.min.js') }}"></script>
      <!-- Owl javascript -->
      <script src="{{ URL::to('public/frontend/js/jquery.min.js') }}"></script>
      <script src="{{ URL::to('public/frontend/js/owl.carousel.js') }}"></script>
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')

<div class="how_vidio_ban">
	@if(@$part1->image)
		<img src="{{URL::to('storage/app/public/uploads/content_images/'.@$part1->image)}}" alt="">
	@else
		<img src="{{ URL::to('public/frontend/images/how-ban.jpg') }}" alt="">
	@endif
	<div class="how_vid_inr">
		<div class="container">
			@php
				$class = "";
				if(@$part1->image_1 == "L") $class = "land8_ban_txt_left";
				else if(@$part1->image_1 == "R") $class = "land8_ban_txt_right";
			@endphp
			<div class="how_vid_tx {{@$class}}">
				<strong>{!! @$part1->title !!}</strong>
				<p>{!! @$part1->description !!}</p>
				<a href="{{ @$part2->button_link_7 ?? 'javascript:;' }}" class="view-all">{!! @$part1->short_description_1 !!}</a>
			</div>
		</div>
	</div>
</div>

<div class="tem_potencial_sec">
	<div class="container">
		<div class="tem_potencial_inr">
			<div class="page-h2">
				<h2>{!! @$part1->heading_1 !!}</h2>
			</div>
			<div class="tem_potencial_pan">
				<div class="row">
					<div class="col-md-4">
						<div class="tem_potencial_bx">
							@if(@$part1->image_2)
								<img src="{{URL::to('storage/app/public/uploads/content_images/'.@$part1->image_2)}}" style="height: 56px; width: 56px;" alt="">
							@else
								<i class="fa bigger_icon fa-briefcase" aria-hidden="true"></i>
							@endif
							<h4>{!! @$part1->heading_2 !!}</h4>
							<p>{!! @$part1->description_2 !!}</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="tem_potencial_bx">
							@if(@$part1->image_3)
								<img src="{{URL::to('storage/app/public/uploads/content_images/'.@$part1->image_3)}}" style="height: 56px; width: 56px;" alt="">
							@else
								<i class="fa bigger_icon fa-usd" aria-hidden="true"></i>
							@endif
							<h4>{!! @$part1->heading_3 !!}</h4>
							<p>{!! @$part1->description_3 !!}</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="tem_potencial_bx">
							@if(@$part1->image_4)
								<img src="{{URL::to('storage/app/public/uploads/content_images/'.@$part1->image_4)}}" style="height: 56px; width: 56px;" alt="">
							@else
								<i class="fa bigger_icon fa-bullhorn" aria-hidden="true"></i>
							@endif
							<h4>{!! @$part1->heading_4 !!}</h4>
							<p>{!! @$part1->description_4 !!}</p>
						</div>
					</div>
					<strong class="entao_cc">{!! @$part1->description_1 !!}</strong>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="faca_seu_sec">
	<div class="container">
		<div class="faca_seu_inr">
			<div class="row">
				<div class="col-sm-5">
					<div class="faca_seu_img">
						@if(@$part1->image_5)
							<img src="{{URL::to('storage/app/public/uploads/content_images/'.@$part1->image_5)}}" alt="">
						@else
							<img src="{{ URL::to('public/frontend/images/how_affiliate1.jpg') }}" alt="">
						@endif
					</div>
				</div>
				<div class="col-sm-7">
					<div class="faca_seu_tx">
						<h5>{!! @$part1->heading_5 !!}</h5>
						<p>{!! @$part1->description_5 !!}</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="que_voc_sec">
	<div class="container">
		<div class="que_voc_inr">
			<div class="row">
				<div class="col-md-6">
					<div class="que_voc_bx">
						<h5>{!! @$part1->heading_6 !!}</h5>
						<p>{!! @$part1->description_6 !!}</p>
					</div>
				</div>
				<div class="col-md-6">
					<div class="que_voc_bx que_voc_bg">
						<h5>{!! @$part1->heading_7 !!}</h5>
						<p>{!! @$part1->description_7 !!}</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="tem_potencial_sec acesse_solu_sec">
	<div class="container">
		<div class="tem_potencial_inr">
			<div class="page-h2">
				<h2>{!! @$part2->heading_1 !!}</h2>
			</div>
			<div class="tem_potencial_pan">
				<div class="row">
					<div class="col-md-4">
						<div class="tem_potencial_bx">
							@if(@$part2->image_2)
								<img src="{{URL::to('storage/app/public/uploads/content_images/'.@$part2->image_2)}}" style="height: 56px; width: 56px;" alt="">
							@else
								<i class="fa bigger_icon fa-video-camera" aria-hidden="true"></i>
							@endif
							<h4>{!! @$part2->heading_2 !!}</h4>
							<p>{!! @$part2->description_2 !!}</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="tem_potencial_bx">
							@if(@$part2->image_3)
								<img src="{{URL::to('storage/app/public/uploads/content_images/'.@$part2->image_3)}}" style="height: 56px; width: 56px;" alt="">
							@else
								<i class="fa bigger_icon fa-gift" aria-hidden="true"></i>
							@endif
							<h4>{!! @$part2->heading_3 !!}</h4>
							<p>{!! @$part2->description_3 !!}</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="tem_potencial_bx">
							@if(@$part2->image_4)
								<img src="{{URL::to('storage/app/public/uploads/content_images/'.@$part2->image_4)}}" style="height: 56px; width: 56px;" alt="">
							@else
								<i class="fa bigger_icon fa-percent" aria-hidden="true"></i>
							@endif
							<h4>{!! @$part2->heading_4 !!}</h4>
							<p>{!! @$part2->description_4 !!}</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="monte_seu_sec">
	<div class="container">
		<div class="monte_seu_inr">
			<div class="page-h2">
				<h2>{!! @$part2->title !!}</h2>
			</div>
			<a href="{{ @$part2->button_link_7 ?? 'javascript:;' }}" class="view-all">{!! @$part2->short_description_1 !!}</a>
		</div>
	</div>
</div>

@endsection
@section('footer')
@include('includes.footer')

<script src="{{ URL::to('public/frontend/js/jquery.com_ui_1.11.4_jquery-ui.js') }}"></script>

@endsection
