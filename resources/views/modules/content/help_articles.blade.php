@extends('layouts.app')
@section('title')
    @if(@$article->meta_title) 
        {{@$article->meta_title}}
    @else
        @lang('site.help_section')
    @endif
@endsection
@section('style')
    @include('includes.style')
@endsection
@section('scripts')
    @include('includes.scripts')
@endsection
@section('header')
    @include('includes.header')
@endsection
@section('help_section_meta')
    <meta name="description" content="{{ @$article->meta_description }}">
    <meta name="keyword" content="{{ @$article->meta_keyword }}">
@endsection
@section('content')
    <div class="login-body about-body">
        <div class="container">
            <div class="row">
                <div class="abut_page row">
                    <div class="col-md-12 mb-3">
                        <h2>{{$category->category}}</h2>
                        <span class="back_btn_terms"><a href="{{ route('help.section') }}" class="p text-secondary"> <i class="fa fa-angle-double-left"></i> Back </a></span>
                    </div>
                    <form action="{{ route('help.articles', @$category->slug) }}" method="post" class="col-md-12 row mb-3">
                        @csrf
                        <div class="col-md-11">
                            <label for="title">@lang('site.keyword')</label>
                            <input type="text" name="title" id="title" class="form-control" value="{{@$key['title']}}">
                        </div>
                        <div class="col-md-1 text-right" style="align-self: flex-end;">
                            <input type="submit" value="@lang('client_site.search')" class="btn btn-primary">
                        </div>
                    </form>
                    {{-- <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="card term_card">
                            <a href="{{ route('help.section', [@$category->slug]) }}">
                                <p>{{ $category->category }}</p>
                            </a>
                        </div>
                    </div> --}}

                    <div class="col-lg-12 col-md-12">
                        <ul id="help_article_list" class="help_article_list">
                            @foreach ($articles as $article)
                                <li>
                                    <a class="link" id="link-{{ $article->id }}" href="{{route('help.article.details', $article->slug)}}">{{ $article->title }}</a>
                                    {{-- <div class="pm" id="pm-{{ $article->id }}">
                                        <div class="row">
                                            @if(@$article->video)
                                                <div class="col-md-6">
                                                    <p style="white-space: pre-wrap;">{!! $article->description !!}</p>
                                                </div>
                                                <div class="col-md-6">
                                                    <iframe style="width: 100%; height: 100%;" width="80%" height="100%" src="https://www.youtube.com/embed/{{ $article->video }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                                </div>
                                            @else
                                                <div class="col-md-12">
                                                    <p style="white-space: pre-wrap;">{!! $article->description !!}</p>
                                                </div>
                                            @endif
                                        </div>
                                        @if(@$article->image)
                                        <div class="row">
                                            <img src="{{url('/')}}/storage/app/public/uploads/content_images/{{@$article->image}}" alt="Help Image">
                                        </div>
                                        @endif
                                    </div> --}}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div> 
    </div>
@endsection
@section('footer')
    @include('includes.footer')
@endsection