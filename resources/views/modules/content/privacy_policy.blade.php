@extends('layouts.app')
@section('title')
    @lang('site.privacy_policy')
@endsection
@section('style')
    @include('includes.style')
@endsection
@section('scripts')
    @include('includes.scripts')
@endsection
@section('header')
    @include('includes.header')
@endsection
@section('content')
    <div class="login-body about-body">
        <div class="container">
            <div class="row">
                <div class="abut_page">
                    <h2>{{ $privacyPolicy->title }}</h2>
                    <p>{!! @$privacyPolicy->description !!}</p>
                </div>
            </div>
        </div> 
    </div>
@endsection
@section('footer')
    @include('includes.footer')
@endsection