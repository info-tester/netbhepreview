@extends('layouts.app')
@section('title')
    @lang('site.terms_of_service')
@endsection
@section('style')
    @include('includes.style')
@endsection
@section('scripts')
    @include('includes.scripts')
@endsection
@section('header')
    @include('includes.header')
@endsection
@section('content')
    <div class="login-body about-body">
        <div class="container">
            <div class="row">
                <div class="abut_page">
                    <h2>
                        {{ $term->title }}
                    </h2>
                    <span class="back_btn_terms"><a href="{{ route('terms.of.services') }}" class="p text-secondary"> <i class="fa fa-angle-double-left"></i> Back </a></span>
                    <p>{!! @$term->description !!}</p>
                    {{-- @foreach($subterms as $subterm)
                        <h6>{{ $subterm->title }}</h6>
                        <p>{!! @$subterm->description !!}</p>
                    @endforeach --}}
                </div>
            </div>
        </div> 
    </div>
@endsection
@section('footer')
    @include('includes.footer')
@endsection