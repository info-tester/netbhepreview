@extends('layouts.app')
@section('title', 'Landing Pages')
@section('style')
@include('includes.style')


      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <!--style-->
      <link href="{{ URL::to('public/frontend/css/style.css') }}" type="text/css" rel="stylesheet" media="all" />
      <link href="{{ URL::to('public/frontend/css/responsive.css') }}" type="text/css" rel="stylesheet" media="all" />
      <!--bootstrape-->
      <link href="{{ URL::to('public/frontend/css/bootstrap.min.css') }}" type="text/css" rel="stylesheet" media="all" />
      <!--font-awesome-->
      <link href="{{ URL::to('public/frontend/css/font-awesome.min.css') }}" type="text/css" rel="stylesheet" media="all" />
      <!--fonts-->
      <!--<link href="https://fonts.googleapis.com/css?family=Didact+Gothic" rel="stylesheet"> -->
      <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,900" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900&display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900&display=swap" rel="stylesheet">
      <!-- Owl Stylesheets -->
      <link rel="stylesheet" href="{{ URL::to('public/frontend/css/owl.carousel.min.css') }}">
      <link rel="stylesheet" href="{{ URL::to('public/frontend/css/owl.theme.default.min.css') }}">
      <!--price range (Calender/date picker)-->
      <link href="{{ URL::to('public/frontend/css/themes_smoothness_jquery-ui.css') }}" rel="stylesheet" type="text/css">
      <style>
          p{
            white-space:pre-wrap
          }
      </style>

@endsection
@section('scripts')
@include('includes.scripts')
      <script type="text/javascript" src="{{ URL::to('public/frontend/js/jquery-3.2.1.js') }}"></script>
      <script type="text/javascript" src="{{ URL::to('public/frontend/js/bootstrap.min.js') }}"></script>
      <!-- Owl javascript -->
      <script src="{{ URL::to('public/frontend/js/jquery.min.js') }}"></script>
      <script src="{{ URL::to('public/frontend/js/owl.carousel.js') }}"></script>
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')

<div class="land8_ban_sec">
	<!-- <video autoplay muted loop id="myVideo">
	  <source src="images/ban_ve.mp4" type="video/mp4">
	</video> -->
    @if( @$part1->image)
	<img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$part1->image) }}" alt="">
    @else
	<img src="{{ URL::to('public/frontend/images/ban_1.jpg')}}" alt="">
    @endif
	<div class="land8_ban_inr">
		<div class="container">
			@php
		  		$class = "";
				if(@$part1->description_6 == 'L') $class = "land8_ban_txt_left";
				elseif(@$part1->description_6 == 'R') $class = "land8_ban_txt_right";
			@endphp
			<div class="land8_ban_txt {{@$class}}">
				<strong>{!! @$part1->title!!}</strong>
				<p>{!! @$part1->description!!}</p>
				<a href="{{ @$part1->button_link_7 ? $part1->button_link_7 :'javascript:;'}}" class="view-all" target="_blank">{!! @$part1->short_description_1 !!}</a>
				<span class="play_wi">
					<a href="{{@$part1->image_6?@$part1->image_6:'javascript:;'}}" class="fancybox-media play_vedi_cc" rel="media-gallery" @if(@$part1->image_6) target="_blank" @endif>
						<em><i class="fa fa-play-circle"></i></em>{{@$part1->heading_6?@$part1->heading_6:'Watch Video To Learn More'}}
					</a>
				</span>
			</div>
		</div>
	</div>
</div>

<section class="advanced_pl_sec">
	<div class="container">
		<div class="advanced_pl_inr">
			<div class="page-h2">
				<h2>{!!@$part2->title!!}</h2>
				<p>{!! @$part2->description!!}</p>
			</div>
			<div class="advanced_panel">
				<div class="row">
					<div class="col-md-4">
						<div class="advanced_bx">
							<div class="media">
                                @if(@$part2->image_1)
								<span><img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$part2->image_1) }}"></span>
                                @else
								<span><img src="{{ URL::to('public/frontend/images/advanced_bx1.svg')}}"></span>
                                @endif
								<div class="media-body">
									<h5>{!!@$part2->heading_1!!}</h5>
								</div>
							</div>
							<p>{!!@$part2->description_1!!}</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="advanced_bx">
							<div class="media">
                                @if(@$part2->image_2)
                                <span><img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$part2->image_2) }}"></span>
                                @else
								<span><img src="{{ URL::to('public/frontend/images/advanced_bx2.svg')}}"></span>
                                @endif
								<div class="media-body">
									<h5>{!!@$part2->heading_2!!}</h5>
								</div>
							</div>
							<p>{!!@$part2->description_2!!}</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="advanced_bx">
							<div class="media">
                                @if(@$part2->image_3)
                                <span><img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$part2->image_3) }}"></span>
                                @else
								<span><img src="{{ URL::to('public/frontend/images/advanced_bx3.svg')}}"></span>
                                @endif
								<div class="media-body">
									<h5>{!!@$part2->heading_3!!}</h5>
								</div>
							</div>
							<p>{!!@$part2->description_3!!}</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="advanced_bx">
							<div class="media">
                                @if(@$part2->image_4)
                                <span><img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$part2->image_4) }}"></span>
                                @else
								<span><img src="{{ URL::to('public/frontend/images/advanced_bx4.svg')}}"></span>
                                @endif
								<div class="media-body">
									<h5>{!!@$part2->heading_4!!}</h5>
								</div>
							</div>
							<p>{!!@$part2->description_4!!}</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="advanced_bx">
							<div class="media">
                                @if(@$part2->image_5)
                                <span><img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$part2->image_5) }}"></span>
                                @else
								<span><img src="{{ URL::to('public/frontend/images/advanced_bx5.svg')}}"></span>
                                @endif
								<div class="media-body">
									<h5>{!!@$part2->heading_5!!}</h5>
								</div>
							</div>
							<p>{!!@$part2->description_5!!}</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="advanced_bx">
							<div class="media">
                                @if(@$part2->image_6)
                                <span><img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$part2->image_6) }}"></span>
                                @else
								<span><img src="{{ URL::to('public/frontend/images/advanced_bx6.svg')}}"></span>
                                @endif
								<div class="media-body">
									<h5>{!!@$part2->heading_6!!}</h5>
								</div>
							</div>
							<p>{!!@$part2->description_6!!}</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="wasting_sec">
	<div class="container">
		<div class="wasting_inr">
			<div class="page-h2">
				<h2>{!!@$part1->heading_1!!}</h2>
				<p>{!!@$part1->description_1!!}</p>
			</div>
			<div class="wasting_panel">
				<div class="row">
					<div class="col-md-4">
						<div class="wasting_bx">
                            @if(@$part1->image_2)
							<em><img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$part1->image_2) }}" alt=""></em>
                            @else
							<em><img src="{{ URL::to('public/frontend/images/wasting1.svg')}}" alt=""></em>
                            @endif
							<h4>{!!@$part1->heading_2!!}</h4>
							{{-- <span>Move at the speed of advertising.</span> --}}
							<p>{!!@$part1->description_2!!}</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="wasting_bx">
                            @if(@$part1->image_3)
							<em><img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$part1->image_3) }}" alt=""></em>
                            @else
							<em><img src="{{ URL::to('public/frontend/images/wasting2.svg')}}" alt=""></em>
                            @endif
							<h4>{!!@$part1->heading_3!!}</h4>
							{{-- <span>Free yourself from production constraints.</span> --}}
							<p>{!!@$part1->description_3!!}</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="wasting_bx">
                            @if(@$part1->image_4)
							<em><img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$part1->image_4) }}" alt=""></em>
                            @else
							<em><img src="{{ URL::to('public/frontend/images/wasting3.svg')}}" alt=""></em>
                            @endif
							<h4>{!!@$part1->heading_4!!}</h4>
							{{-- <span>Increase your return on advertising spend.</span> --}}
							<p>{!!@$part1->description_4!!}</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="personalized_sec">
	<div class="container">
		<div class="page-h2">
			<h2>{!!@$part1->heading_5!!}</h2>
			<p>{!!@$part1->description_5!!}</p>
		</div>
		<div class="personalized_inr">
			<div class="personalized_img">
                @if(@$part1->image_5)
                <img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$part1->image_5) }}" alt="">
                @else
				<img src="{{ URL::to('public/frontend/images/videobg1.jpg')}}" alt="">
                @endif
			</div>
		</div>
	</div>
</section>
<section class="builder_sec">
	<div class="container">
		<div class="page-h2">
			<h2>{!!@$part3->title!!}</h2>
			<p>{!!@$part3->description!!}</p>
		</div>
		<div class="builder_inr">
			<div class="builder_panel">
				<div class="row">
					<div class="col-md-6">
						<div class="builder_img">
                            @if(@$part3->image_1)
							<img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$part3->image_1) }}" alt="">
                            @else
							<img src="{{ URL::to('public/frontend/images/builder1.jpg')}}" alt="">
                            @endif
						</div>
					</div>
					<div class="col-md-6">
						<div class="builder_tex">
							<h6>{!!@$part3->heading_1!!}</h6>
							<p>{!!@$part3->description_1!!}</p>
						</div>
					</div>
				</div>
			</div>
			<div class="builder_panel">
				<div class="row">
					<div class="col-md-6">
						<div class="builder_img">
                            @if(@$part3->image_2)
							<img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$part3->image_2) }}" alt="">
                            @else
							<img src="{{ URL::to('public/frontend/images/builder2.jpg')}}" alt="">
                            @endif
						</div>
					</div>
					<div class="col-md-6">
						<div class="builder_tex">
							<h6>{!!@$part3->heading_2!!}</h6>
							<p>{!!@$part3->description_2!!}</p>
						</div>
					</div>
				</div>
			</div>
			<div class="builder_panel">
				<div class="row">
					<div class="col-md-6">
						<div class="builder_img">
                            @if(@$part3->image_3)
							<img src="{{ URL::to('storage/app/public/uploads/content_images/' . @$part3->image_3) }}" alt="">
                            @else
							<img src="{{ URL::to('public/frontend/images/builder3.jpg')}}" alt="">
                            @endif
						</div>
					</div>
					<div class="col-md-6">
						<div class="builder_tex">
							<h6>{!!@$part3->heading_3!!}</h6>
							<p>{!!@$part3->description_3!!}</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="conversions_sec">
	<div class="container">
		<div class="page-h2">
			<h2>{!!@$part4->title!!}</h2>
			<p>{!!@$part4->description!!}</p>
		</div>
		<div class="conversions_inr">
			<div class="row">
				<div class="col-md-4">
					<div class="conversions_bx">
						<ul>
							<li><i class="fa fa-check"></i>{!!@$part4->heading_1!!}</li>
							<li><i class="fa fa-check"></i>{!!@$part4->description_1!!}</li>
							<li><i class="fa fa-check"></i>{!!@$part4->heading_2!!}</li>
							<li><i class="fa fa-check"></i>{!!@$part4->description_2!!}</li>
						</ul>
					</div>
				</div>
				<div class="col-md-4">
					<div class="conversions_bx">
						<ul>
							<li><i class="fa fa-check"></i>{!!@$part4->heading_3!!}</li>
							<li><i class="fa fa-check"></i>{!!@$part4->description_3!!}</li>
							<li><i class="fa fa-check"></i>{!!@$part4->heading_4!!}</li>
							<li><i class="fa fa-check"></i>{!!@$part4->description_4!!}</li>
						</ul>
					</div>
				</div>
				<div class="col-md-4">
					<div class="conversions_bx">
						<ul>
							<li><i class="fa fa-check"></i>{!!@$part4->heading_5!!}</li>
							<li><i class="fa fa-check"></i>{!!@$part4->description_5!!}</li>
							<li><i class="fa fa-check"></i>{!!@$part4->heading_6!!}</li>
							<li><i class="fa fa-check"></i>{!!@$part4->description_6!!}</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="get_start">
				<a href="{{@$part4->button_link_7?$part4->button_link_7:'javacript:;'}}" class="view-all" target="_blank">{{@$part4->short_description_1?$part4->short_description_1:'GET STARTED'}}</a>
			</div>
		</div>
	</div>
</section>

@endsection
@section('footer')
@include('includes.footer')

<script src="{{ URL::to('public/frontend/js/jquery.com_ui_1.11.4_jquery-ui.js') }}"></script>
<script src="{{ URL::to('public/frontend/js/js/fancyjs.js') }}"></script>
<script src="{{ URL::to('public/frontend/js/js/jquery.fancybox.js') }}"></script>
<script src="{{ URL::to('public/frontend/js/js/jquery.fancybox.pack.js') }}"></script>
<script src="{{ URL::to('public/frontend/js/js/jquery.mousewheel-3.0.6.pack.js') }}"></script>
<script src="{{ URL::to('public/frontend/js/js/jquery.fancybox-media.js') }}"></script>

@endsection
