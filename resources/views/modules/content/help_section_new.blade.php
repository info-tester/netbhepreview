@extends('layouts.app')
@section('title')
    {{-- @lang('site.edit') @lang('site.blog')-@lang('site.post') --}}
    {{$content->title}} | {{@$article->title}}
@endsection
@section('style')
@include('includes.style')
<style type="text/css">
    .chck_eml_rd{
        color: red;
        font-weight: bold;
        font-size: 14px;
    }
    li{
        white-space:unset !important;
    }
</style>
@endsection
@section('help_section_meta')
<meta name="description" content="{{ @$article->meta_description }}">
<meta name="keyword" content="{{ @$article->meta_keyword }}">
@endsection
@section('scripts')
@include('includes.scripts')

@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h1 class="help_heading">{{$content->title}}</h1>
        <div class="bokcntnt-bdy no-margin-top">
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>

            <div class="dshbrd-lftmnu">
                <div class="prflinfo-sec">
                </div>

                <div class="dashmnu">
                    <span class="one-ul">

                    </span>
                    <ul class="user-ul">
                        @foreach($categories as $i=>$cat)
                            <li class="article_dash" data-num="{{$i}}">
                                <a href="javascript:;">
                                    <!-- <img src="{{ URL::to('public/frontend/images/dsh1.jpg') }}"> -->
                                    {{ $cat->category }}&nbsp;
                                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                                </a>
                                @if(count($cat->getArticles) > 0)
                                <ul class="user-ul article_drop_dash" id="article_drop_dash_{{$i}}" @if(@$article->getCategory->id != $cat->id) style="display:none;" @endif >
                                    @foreach($cat->getArticles as $artcl)
                                    <li>
                                        <a href="{{ route('help.section', @$artcl->slug) }}" class="{{ @$article->id == $artcl->id ? 'active' : '' }}" @if(Request::segment(1)=="my-dashboard")style="color: #007bff;"@endif>
                                            <!-- <img src="{{ URL::to('public/frontend/images/dsh1.jpg') }}"> -->
                                            <!-- <i class="fa fa-minus" aria-hidden="true"></i> -->
                                            <i class="fa fa-circle" aria-hidden="true"></i>
                                            &nbsp;{{ $artcl->title }}
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>

            <div class="dshbrd-rghtcntn">
                <!-- <form action="{{route('help.section')}}" method="post" class="col-md-12 row mb-3">
                    @csrf -->
                    <div class="col-md-11">
                        <label for="title">@lang('site.keyword')</label>
                        <input type="text" name="title" id="title" class="form-control" value="{{@$key['title']}}" placeholder="@lang('client_site.search_for_articles')">
                        <div class="article_search_result" style="display:none;">
                        </div>

                        <div class="main_article">
                            @if(@$article)
                                <h4>{{@$article->title}}</h4>
                                <div class="row">
                                    @if(@$article->video && @$article->image)
                                        <div class="col-md-12 col-sm-12 text-center">
                                            <iframe style="width: 100%; height: 375px;" src="https://www.youtube.com/embed/{{ $article->video }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                        </div>
                                        <div class="col-md-6 col-sm-6 offset-md-3 col-sm-12 text-center">
                                            <img src="{{url('/')}}/storage/app/public/uploads/content_images/{{@$article->image}}" alt="Article image" width="100%">
                                        </div>
                                    @elseif(@$article->video)
                                        <div class="col-md-12 col-sm-12 text-center">
                                            <iframe style="width: 65%; height: 375px;" src="https://www.youtube.com/embed/{{ $article->video }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                        </div>
                                    @elseif(@$article->image)
                                        <div class="col-md-6 col-sm-6 offset-md-3 col-sm-12 text-center">
                                            <img src="{{url('/')}}/storage/app/public/uploads/content_images/{{@$article->image}}" alt="Article image" width="100%">
                                        </div>
                                    @endif
                                    <div class="col-md-12">
                                        {!! $article->description !!}
                                    </div>
                                </div>
                            @endif
                        </div>

                    </div>
                    <!-- <div class="col-md-1 text-right" style="align-self: flex-end;">
                        <input type="submit" value="@lang('client_site.search')" class="btn btn-primary">
                    </div> -->
                <!-- </form> -->
            </div>

        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
<script src="https://unpkg.com/@popperjs/core@2"></script>
<script>
    $(".article_dash").click(function(){
        console.log("Hit");
        var num = $(this).data('num');
        if($("#article_drop_dash_"+num).css('display') == "none"){
            $(".article_drop_dash").slideUp();
            $("#article_drop_dash_"+num).slideDown();
        }
    });
    // $(".dropdown_dash").click(function(){
    //     $(".dropdown_dash").slideUp();
    // });
    function delay(callback, ms) {
        var timer = 0;
        return function() {
            var context = this, args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function () {
            callback.apply(context, args);
            }, ms || 0);
        };
    }
    $('#title').keyup(delay(function (e) {
        console.log('Time elapsed!', this.value);
        var val = this.value;
        if(val == ""){
            $('.article_search_result').hide();
        } else {
            var formD = new FormData();
            formD.append( "_token", '{{csrf_token()}}');
            formD.append( "title", val);
            $.ajax({
                url: '{{route("fetch.help.articles")}}',
                data: formD,
                type: 'POST',
                contentType: false,
                cache: false,
                processData:false,
                dataType: 'JSON',
                success: function(jsn) {
                    console.log(jsn);
                    if(jsn.status == "success"){
                        var html = "";
                        console.log(jsn.categories.length);
                        if(jsn.categories.length == 0){
                            html = `<small class="text-secondary text-center">No results found</p>`;
                        } else {
                            $(jsn.categories).each(function(i, cat){
                                var arthtml = "";
                                $(cat.articles).each(function(i, art){
                                    arthtml += `<h4><a href="{{url('/')}}/central-de-ajuda/${art.slug}">${art.title}</a></h4>`;
                                });
                                html += `<div class="search_cat_res">
                                            <h3>${cat.category}</h3>
                                            ${arthtml}
                                        </div>`;
                            });
                        }
                        $('.article_search_result').html(html).show();
                    }
                }
            });
        }
    }));
</script>
@endsection
