@extends('layouts.app')
@section('title')
    @lang('site.help_section')
@endsection
@section('style')
    @include('includes.style')
@endsection
@section('scripts')
    @include('includes.scripts')
    <script>
		$(function() {
			var Accordion = function(el, multiple) {
				this.el = el || {};
				this.multiple = multiple || false;

				// Variables privadas
				var links = this.el.find('.link');
				// Evento
				links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
			}

			Accordion.prototype.dropdown = function(e) {
				var $el = e.data.el;
					$this = $(this),
					$next = $this.next();

				$next.slideToggle();
				$this.parent().toggleClass('open');

				if (!e.data.multiple) {
					$el.find('.pm').not($next).slideUp().parent().removeClass('open');
				};
			}

			var accordion = new Accordion($('#accordion'), false);
			var accordion1 = new Accordion($('#accordion1'), false);
		});
	</script>
@endsection
@section('header')
    @include('includes.header')
@endsection
@section('content')
    <div class="login-body about-body">
        <div class="container">
            <div class="row">
                <div class="abut_page row">
                    <div class="col-md-12 mb-3">
                        <h1 class="help_heading">{{$content->title}}</h1>
                        @if(@$articles)
                            <span class="back_btn_terms"><a href="{{ route('help.section') }}" class="p text-secondary"> <i class="fa fa-angle-double-left"></i> Back </a></span>
                        @endif
                    </div>
                    <!-- <form action="{{route('help.section')}}" method="post" class="col-md-12 row mb-3">
                        @csrf -->
                        <div class="col-md-12">
                            <label for="title">@lang('site.keyword')</label>
                            <input type="text" name="title" id="title" class="form-control" value="{{@$key['title']}}">
                            <div class="article_search_result" style="display:none;">
                            </div>
                        </div>
                        <!-- <div class="col-md-1 text-right" style="align-self: flex-end;">
                            <input type="submit" value="@lang('client_site.search')" class="btn btn-primary">
                        </div> -->
                    <!-- </form> -->
                    @if(@$categories)
                        @foreach($categories as $category)
                            <div class="col-lg-4 col-md-6 col-sm-12 pt-4">
                                <div class="card term_card">
                                    <a href="{{ route('help.articles', [@$category->slug]) }}">
                                        <p>{{ $category->category }}</p>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')
    @include('includes.footer')
    <script>
        function delay(callback, ms) {
            var timer = 0;
            return function() {
                var context = this, args = arguments;
                clearTimeout(timer);
                timer = setTimeout(function () {
                callback.apply(context, args);
                }, ms || 0);
            };
        }
        $('#title').keyup(delay(function (e) {
            console.log('Time elapsed!', this.value);
            var val = this.value;
            if(val == ""){
                $('.article_search_result').hide();
            } else {
                var formD = new FormData();
                formD.append( "_token", '{{csrf_token()}}');
                formD.append( "title", val);
                $.ajax({
                    url: '{{route("fetch.help.articles")}}',
                    data: formD,
                    type: 'POST',
                    contentType: false,
                    cache: false,
                    processData:false,
                    dataType: 'JSON',
                    success: function(jsn) {
                        console.log(jsn);
                        if(jsn.status == "success"){
                            var html = "";
                            console.log(jsn.categories.length);
                            if(jsn.categories.length == 0){
                                html = `<small class="text-secondary text-center">No results found</p>`;
                            } else {
                                $(jsn.categories).each(function(i, cat){
                                    var arthtml = "";
                                    $(cat.articles).each(function(i, art){
                                        arthtml += `<h4><a href="{{url('/')}}/central-de-ajuda/${art.slug}">${art.title}</a></h4>`;
                                    });
                                    html += `<div class="search_cat_res">
                                                <h3>${cat.category}</h3>
                                                ${arthtml}
                                            </div>`;
                                });
                            }
                            $('.article_search_result').html(html).show();
                        }
                    }
                });
            }
        }));
    </script>
@endsection
