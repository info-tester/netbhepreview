@extends('layouts.app')
@section('title')
  @lang('site.get_the_right_advice')
@endsection

@section('style')
	@include('includes.style')
@endsection

@section('scripts')
	@include('includes.scripts')
@endsection

@section('header')
	@include('includes.header')
@endsection


@section('content')
    <form action="{{ route('srch.pst') }}" method="post" name="myForm" id="myForm">
        @csrf
            <section class="search-filter">
                <div class="container">
                    <div class="row">
                        <div class="search-white">
                            <div class="form-group one-type">
                                <label class="search-label">@lang('client_site.keywords')</label>
                                <input type="text" placeholder="Digite suas palavras-chave" class="result-type" name="keyword" id="keyword" value="{{ @$key['keyword'] }}">
                            </div>
                            <div class="form-group one-type">
                                <label class="search-label">@lang('client_site.category')</label>

                                <select class="result-type result-select" name="category" id="category">
                                    <option value="">@lang('client_site.select_category')</option>
                                    @foreach($category as $cat)
                                        @if($cat->parent_id==0)
                                            <option value="{{ @$cat->id }}" @if(@$key['category']==@$cat->id) selected @endif>{{ @$cat->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group one-type">
                                <label class="search-label">@lang('client_site.sub_category')</label>
                                <!--<input type="text" placeholder="Type your keywords" class="result-type">-->
                                <select class="result-type result-select" name="subcategory" id="subcategory">
                                    <option value="">@lang('client_site.select_category_first')</option>
                                    @if(@$key['subcategory'])
                                        @foreach(@$category as $ct)
                                            @if($ct->parent_id!=0)
                                                <option value="{{ @$ct->id }}" @if(@$key['subcategory']==$ct->id) selected @endif>{{ @$ct->name }}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            {{-- <div class="form-group tow-type">
                                <label class="search-label">Experience Level</label>
                                <!--<input type="text" placeholder="Type your keywords" class="result-type">-->
                                <select class="result-type result-select" name="experience">
                                    <option>Select Experience</option>
                                        <option value="1" @if(@$key['experience']==1) selected @endif>Less than or 1 Year</option>
                                        <option value="3" @if(@$key['experience']==3) selected @endif>Less than or 1 to 3 Year</option>
                                        <option value="5" @if(@$key['experience']==5) selected @endif>Less than or 3 to 5 Year</option>
                                </select>
                            </div> --}}
                            <div class="form-group two-type">
                                <label class="search-label">@lang('client_site.hourly_minute_rate')</label>
                                <div class="slider_rnge">
                                    <div id="slider-range" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
                                        <div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 0%; width: 100%;"></div>
                                        <span tabindex="0" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 0%;"></span>
                                        <span tabindex="0" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 100%;"></span>
                                    </div>
                                    <span>
                                    <input type="text" class="price_numb" readonly id="amount" name="amount">
                                    </span>
                                </div>
                            </div>
                            <input type="hidden" name="max_price" id="max_price">
                            <input type="hidden" name="min_price" id="min_price">

                            <button class="submit-search12345 frm1" type="submit">@lang('client_site.search')</button>
                        </div>

                    </div>
                </div>
            </section>
    </form>
    <section class="search-body">
        <div class="container">
            <div class="row rwmrgn">
                <div class="srchpgi">
                    <form method="post" action="{{ route('srch.pst') }}">
                        @csrf
                        <select name="sort_by" id="sort_by" class="result-type srtby" style="width: 150px;">
                            <option>@lang('client_site.sort_by')</option>
                            <option value="A" @if(@$key['sort_by']=="A") selected @endif>@lang('client_site.assending')</option>
                            <option value="D" @if(@$key['sort_by']=="D") selected @endif>@lang('client_site.desending')</option>
                        </select>
                    </form>
                    {{ @$user->links('vendor.pagination.default') }}
                </div>
                @if(sizeof($user)>0)
                    <div class="all-search" style="width: 100%;">
                        <div class="row rwmrgn">
                            @foreach(@$user as $us)
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="guiede-box">
                                        <div class="guide-image"><img src="{{ @$us->profile_pic ? URL::to('storage/app/public/uploads/profile_pic').'/'.@$us->profile_pic: URL::to('public/frontend/images/no_img.png') }}" alt=""></div>
                                        <div class="guide-dtls" style="min-height: 1px;">
                                            <div class="guide-intro">
                                                <h3>
                                                    {{ @$us->nick_name ? @$us->nick_name : @$us->name }}
                                                    <ul>
                                                        {!! getStars($us->avg_review) !!}
                                                        <li>({{ $us->total_review ?? 0 }})</li>
                                                    </ul>
                                                </h3>
                                                <p>{!! @$us->specializationName->name ? $us->specializationName->name . ', ' . $us->crp_no : @$us->crp_no.'&nbsp;' !!}</p>
                                                {{-- <p>{!!@$us->free_session_number>0 ? __('client_site.teacher_free_session') : '&nbsp;'!!}</p> --}}
                                            </div>
                                            <div class="guide-more">
                                                <ul>
                                                    <li>
                                                        <span>Preço</span>
                                                        <p>R${{ @$us->rate_price }} / {{  @$us->rate=="M" ? __('client_site.minute') : __('client_site.hour') }}</p>
                                                    </li>
                                                    <li>
                                                        <span>@lang('client_site.language')</span>
                                                        <p>
                                                            @php
                                                                $j=1;
                                                            @endphp
                                                            @foreach(@$us->userLang as $u)
                                                                {{ @$u->languageName->name }}{{ sizeof(@$us->userLang)>$j ? ', ' : '' }}
                                                                @php
                                                                    $j++;
                                                                @endphp
                                                            @endforeach
                                                        </p>
                                                    </li>
                                                </ul>
                                                <a  data-slug="{{ @$us->slug }}" href="javascript:void(0);" class="guide-btn pull-left ssnbtnslg">@lang('client_site.request_a_session') </a>
                                                <a class="guide-btn pull-right" href="{{ route('pblc.pst',['slug'=>@$us->slug]) }}">@lang('client_site.view_profile') </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="float-right w-100">
                        {{ @$user->links('vendor.pagination.default') }}
                    </div>
                @else
                {{-- <div class="all-search"> --}}
                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                       <center> <h2><span class="error">@lang('client_site.search_result_not_found')</span></h2></center>
                    </div>
                {{-- </div> --}}
                @endif

            </div>
        </div>
    </section>
@endsection
@section('footer')
	@include('includes.footer')
        <script src="{{ URL::to('public/frontend/js/jquery.com_ui_1.11.4_jquery-ui.js') }}"></script>
        <script>
            $(function() {
                $( "#slider-range" ).slider({
                    range: true,
                    min: {{ @$minprice->rate_price }},
                    max: {{ @$maxprice->rate_price }},
                    values: [ {{ @$key['min_price'] ? @$key['min_price']: @$minprice->rate_price }}, {{ @$key['max_price'] ? $key['max_price']: @$maxprice->rate_price }} ],
                    slide: function( event, ui ) {
                        $( "#amount" ).val( "R$" + ui.values[ 0 ] + " - R$" + ui.values[ 1 ] );
                        $('#min_price').val(ui.values[ 0 ]);
                        $('#max_price').val(ui.values[ 1 ]);
                    }
                });
                $( "#amount" ).val( "R$" + $( "#slider-range" ).slider( "values", 0 ) +
                    " - R$" + $( "#slider-range" ).slider( "values", 1 ) );

                        $('#min_price').val($( "#slider-range" ).slider( "values", 0 ));
                        $('#max_price').val($( "#slider-range" ).slider( "values", 1 ));
            });

            $('.frm1').click(function(){
                $('#myForm').submit();
            });

            $('#category').change(function(){
                if($('#category').val()==""){
                    $('#subcategory').html("");
                }else{
                    var reqData = {
                      'jsonrpc' : '2.0',
                      '_token' : '{{csrf_token()}}',
                      'params' : {
                            'cat' : $('#category').val()
                        }
                    };
                    $.ajax({
                        url: "{{ route('fetch.subcat') }}",
                        method: 'post',
                        dataType: 'json',
                        data: reqData,
                        success: function(response){
                            if(response.status==1) {
                                var i=0, html="";
                                html = '<option value="">@lang('client_site.select_option')</option>';
                                for(;i<response.result.length;i++){
                                    html+='<option value='+response.result[i].id+'>'+response.result[i].name+'</option>';
                                }
                                $('#subcategory').html(html);
                                }
                        }, error: function(error) {
                            console.error(error);
                        }
                    });
                }
            });

            $('#sort_by').change(function(){
                $('form').submit();
            });

            $('.ssnbtnslg').click(function(){
                if($(this).data('slug')!=""){
                    localStorage.removeItem('bookSlug');
                    localStorage.setItem('bookSlug', $(this).data('slug'));
                    location.href="{{ route('login') }}"
                }
            });
        </script>
@endsection
