@extends('layouts.app')
@section('title')
  @lang('site.search_by_category')
@endsection

@section('style')
	@include('includes.style')
@endsection

@section('scripts')
	@include('includes.scripts')
@endsection

@section('header')
	@include('includes.header')
@endsection


@section('content')
    <div class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('client_site.browse_by_categories')</h2>
        @foreach(@$cat as $ct)   
        <div class="one-category">
            <div class="main-categori">
                <h4><a href="{{ route('srch.slug',['slug'=>@$ct->slug]) }}"><i class="fa fa-graduation-cap" aria-hidden="true"></i>{{ @$ct->name }}</a></h4>
            </div>
            <div class="all-sub">
                <ul>
                    @if(sizeof(@$ct->childCat)>0)
                        @foreach(@$ct->childCat as $cc)
                        <li><a href="{{ route('srch.slug',['slug'=>$cc->slug]) }}"><i class="fa fa-star" aria-hidden="true"></i>{{ @$cc->name }}</a></li>
                        @endforeach
                    @else
                        <li><p class="alert alert-info">@lang('client_site.no_subcategory_found')</p></center></li>
                    @endif
                </ul>
            </div>
        </div>
        @endforeach
    </div>
</div> 
@endsection
@section('footer')
	@include('includes.footer')
@endsection