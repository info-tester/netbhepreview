@extends('layouts.app')
@section('title')
    @lang('site.add_new_blog')
@endsection
@section('style')
@include('includes.style')
<style type="text/css">
    div.mce-edit-area {
        background:
        #FFF;
        filter: none;
        min-height: 317px;
    }
    .chck_eml_rd{
        color: red;
        font-weight: bold;
        font-size: 14px;
    }

    .tooltip {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted #ccc;
        color: #006080;
        opacity:1 !important;
    }
    .tooltip .tooltiptext {
        visibility: hidden;
        position: absolute;
        width: 250px;
        background-color: #555;
        color: #fff;
        text-align: center;
        padding: 5px;
        border-radius: 6px;
        z-index: 1;
        opacity: 0;
        transition: opacity 0.3s;
        margin-left:10px
    }
    .tooltip-right {
        top: 0px;
        left: 125%;
    }
    .tooltip-right::after {
        content: "";
        position: absolute;
        top: 10%;
        right: 100%;
        margin-top: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: transparent #555 transparent transparent;
    }
    .tooltip:hover .tooltiptext {
        visibility: visible;
        opacity: 1;
    }
    .tooltip_img i{
        margin-left: 10px !important;
        margin-top: 20px !important;
    }
    .tooltip_img .tooltiptext{
        margin-top: 15px !important;
    }

</style>
@endsection
@section('scripts')
@include('includes.scripts')
<script>
      $(document).ready(function(){
         $('#myForm').on('submit', function() {
            var editorContent = tinyMCE.get('short_description').getContent();
            //alert(editorContent);
            if(editorContent == '') {
                $('.desc_error').html('Por favor escreva alguma descrição.');
                $('.desc_error').css('color', 'red');
                return false;
            } else {
                $('.desc_error').html('');
                return true;
            }
        });
      });
   </script>
<script>
    tinyMCE.init({
            mode : "specific_textareas",
            editor_selector : "short_description",
            height: '320px',
            plugins: [
              'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
              'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
              'save table contextmenu directionality emoticons template textcolor'
            ],
            content_style:
        "body {font-family: Roboto; }",
            relative_urls : false,
            remove_script_host : false,
            convert_urls : true,
            toolbar: 'insertfile undo redo | styleselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor emoticons',
            images_upload_url: '{{ URL::to('storage/app/public/uploads/content_image/') }}',
            images_upload_handler: function(blobInfo, success, failure) {
                var formD = new FormData();
                formD.append('file', blobInfo.blob(), blobInfo.filename());
                formD.append( "_token", '{{csrf_token()}}');
                $.ajax({
                    url: '{{ route('artical.img.upload') }}',
                    data: formD,
                    type: 'POST',
                    contentType: false,
                    cache: false,
                    processData:false,
                    dataType: 'JSON',
                    success: function(jsn) {
                        if(jsn.status == 'ERROR') {
                            failure(jsn.error);
                        } else if(jsn.status == 'SUCCESS') {
                            success(jsn.location);
                        }
                    }
                });
            },
        });
</script>
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.add_new_blog')</h2>
        <div class="bokcntnt-bdy">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">
                    <form action="{{ route('store.blog.post') }}" method="post" name="myForm" id="myForm" enctype="multipart/form-data">
                                @csrf
                        <div class="form_body">

                            <div class="row">

                                <div class="col-lg-12 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('site.post_title')
                                            <small class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">@lang('site.tooltip_blog_title')</span>
                                            </small>
                                        </label>

                                        <input type="text" placeholder="Digite o título da postagem" class="required personal-type" value="{{ old('title') }}" name="title">
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('site.category')
                                            <small class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">@lang('site.tooltip_category')</span>
                                            </small>
                                        </label>
                                        <select name="cat_id" class="required personal-type personal-select">

                                            <option value="">@lang('site.select') @lang('site.category')</option>
                                            @foreach(@$category as $ln)
                                                <option value="{{ @$ln->id }}">{{ @$ln->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">Link de vídeo do YouTube
                                            <small class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">@lang('site.tooltip_youtube_link') </span>
                                            </small>
                                        </label>
                                        <input type="url" class="personal-type" placeholder="Link de vídeo do YouTube" name="you_tube_video" id="youtube">
                                        <span id="ytlInfo" class="chck_eml_rd"></span>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-6 col-sm-12">
                                    <div class="form-group" style="margin-top: 22px">
                                        <label class="personal-label">@lang('site.upload_picture') * (Tamanho recomendado para a imagem 1280 * 720px)
                                            <small class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">@lang('site.tooltip_upload_picture')</span>
                                            </small>
                                        </label>
                                            <input type="file" class="personal-type required" id="file" placeholder="Image" name="image" accept="image/*">
                                        {{-- <div class="upldd" >
                                             @lang('site.upload_picture') <i class="fa fa-cloud-upload" aria-hidden="true"></i>
                                             <input type="file" name="image">
                                        </div> --}}
                                        <div class="clearfix"></div>
                                        <div class="image_error"></div>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('site.description')
                                            <small class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">@lang('site.tooltip_blog_description') </span>
                                            </small>
                                        </label>
                                        <textarea class="required short_description personal-type99" placeholder="Enter some text that describe your blog post.." name="desc" id="short_description">{{ old('desc') }}</textarea>
                                        <label class="desc_error" style="color: red"></label>
                                    </div>
                                </div>

                                    <div class="col-sm-12">
                                        <button type="submit" class="login_submitt">@lang('site.blog_post_save')</button>
                                    </div>


                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
<style type="text/css">
    .upldd {
    display: block;
    width: auto;
    border-radius: 4px;
    text-align: center;
    background: #9caca9;
    cursor: pointer;
    overflow: hidden;
    padding: 10px 15px;
    font-size: 15px;
    color: #fff;
    cursor: pointer;
    float: left;
    margin-top: 15px;
    font-family: 'Poppins', sans-serif;
    position: relative;
}
.upldd input {
    position: absolute;
    font-size: 50px;
    opacity: 0;
    left: 0;
    top: 0;
    width: 200px;
    cursor: pointer;
}
.upldd:hover{
    background: #1781d2;
}

</style>
    <script>
        var flag = 0;

        $(document).ready(function(){
            $('#myForm').validate({
                rules:{},
			    submitHandler: function(form) {
                    if(flag == 1) return false;
                    else form.submit();
                    // else console.log("Submit");
                }
            });
        });

        function ytVidId(url) {
            var p = /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
            return (url.match(p)) ? RegExp.$1 : false;
        }

        $('#youtube').bind("change", function() {
            var url = $(this).val();
            if (ytVidId(url) !== false) {
                $('#ytlInfo').html('');
            } else {
                $("#ytlInfo").html('Link do youtube inválido.');
                $('#youtube').val('');
            }
        });

        $('#file').on("change", function() {
            const validImageTypes = ['image/jpeg', 'image/jpg', 'image/png'];
            var mimetype = $('#file')[0].files[0]['type'];
            var size = $('#file')[0].files[0]['size'];
            var name = $('#file')[0].files[0]['name'];
            console.log(mimetype);
            if (!validImageTypes.includes(mimetype)) {
                $('.image_error').html(`<label class="error">${name} não é um arquivo de imagem válido.</label>`);
                $('#file').val('');
                flag = 1;
            } else {
                $('.image_error').html('');
                flag = 0;
            }
        });

    </script>
<style type="text/css">
    #mceu_21{
        float: left !important;
    }
</style>
@endsection
