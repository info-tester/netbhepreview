@extends('layouts.app')
@section('title')
    @lang('site.edit') @lang('site.blog')-@lang('site.post')
@endsection
@section('style')
@include('includes.style')
<style type="text/css">
    .chck_eml_rd{
        color: red;
        font-weight: bold;
        font-size: 14px;
    }

    .tooltip {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted #ccc;
        color: #006080;
        opacity:1 !important;
    }
    .tooltip .tooltiptext {
        visibility: hidden;
        position: absolute;
        width: 250px;
        background-color: #555;
        color: #fff;
        text-align: center;
        padding: 5px;
        border-radius: 6px;
        z-index: 1;
        opacity: 0;
        transition: opacity 0.3s;
        margin-left:10px
    }
    .tooltip-right {
        top: 0px;
        left: 125%;
    }
    .tooltip-right::after {
        content: "";
        position: absolute;
        top: 10%;
        right: 100%;
        margin-top: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: transparent #555 transparent transparent;
    }
    .tooltip:hover .tooltiptext {
        visibility: visible;
        opacity: 1;
    }
    .tooltip_img i{
        margin-left: 10px !important;
        margin-top: 20px !important;
    }
    .tooltip_img .tooltiptext{
        margin-top: 15px !important;
    }

</style>
@endsection
@section('scripts')
@include('includes.scripts')
<script>
    tinyMCE.init({
        mode : "specific_textareas",
        editor_selector : "short_description",
        height: '320px',
        plugins: [
          'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
          'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking media',
          'save table contextmenu directionality emoticons template textcolor'
        ],
        relative_urls : false,
        remove_script_host : false,
        convert_urls : true,
        content_style:
        "body {font-family: Roboto; }",
        toolbar: 'insertfile undo redo | styleselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor emoticons',
        images_upload_url: '{{ URL::to('storage/app/public/uploads/content_image/') }}',
        images_upload_handler: function(blobInfo, success, failure) {
            var formD = new FormData();
            formD.append('file', blobInfo.blob(), blobInfo.filename());
            formD.append( "_token", '{{csrf_token()}}');
            $.ajax({
                url: '{{ route('artical.img.upload') }}',
                data: formD,
                type: 'POST',
                contentType: false,
                cache: false,
                processData:false,
                dataType: 'JSON',
                success: function(jsn) {
                    if(jsn.status == 'ERROR') {
                        failure(jsn.error);
                    } else if(jsn.status == 'SUCCESS') {
                        success(jsn.location);
                    }
                }
            });
        },
    });
</script>
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.edit') @lang('site.blog')-@lang('site.post')</h2>
        <div class="bokcntnt-bdy">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">
                    <form action="{{ route('update.blog.post', ['id'=>@$blg->id]) }}" method="post" name="myForm" id="myForm" enctype="multipart/form-data">
                        @csrf
                        <div class="form_body">

                            <div class="row">

                                <div class="col-lg-12 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('site.post_title')</label>
                                        <input type="text" placeholder="Enter post title" class="required personal-type" value="{{ @$blg->title }}" name="title">
                                    </div>
                                </div>


                                <div class="col-lg-12 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">
                                            @lang('site.category')
                                            <div class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">{{__('site.cont_temp_category_tooltip')}}</span>
                                            </div>
                                        </label>
                                        <select name="cat_id" class="required personal-type personal-select">

                                            <option value="">@lang('site.select') @lang('site.category')</option>
                                            @foreach(@$category as $ln)
                                                <option value="{{ @$ln->id }}" @if(@$ln->id==@$blg->cat_id) selected @endif>{{ @$ln->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">Link de vídeo do YouTube</label>
                                        <input type="url" class="personal-type" placeholder="Link de vídeo do YouTube" id="youtube" name="you_tube_video" value="{{ @$blg->you_tube_video_path }}">
                                         <span id="ytlInfo" class="chck_eml_rd"></span>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('site.upload_picture') * (Tamanho recomendado para a imagem 1280 * 720)</label>

                                        <input type="file" name="image" class="personal-type " accept="image/*">
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('site.existing_picture')</label>

                                        <img src="{{URL::to('storage/app/public/uploads/blog_image').'/'.@$blg->image}}" width="60">
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label" for="exampleInputEmail1">
                                            @lang('site.description')
                                            <div class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">{{__('site.cont_temp_desc_tooltip')}}</span>
                                            </div>
                                        </label>
                                        <textarea class="required short_description personal-type99" placeholder="Enter some text that describe your blog post.." name="desc">{{ @$blg->desc }}</textarea>
                                    </div>
                                </div>

                                    <div class="col-sm-12">
                                        <button type="submit" class="login_submitt">@lang('site.blog_post_save')</button>
                                    </div>


                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
    <script>
        $(document).ready(function(){
            $('#myForm').validate();
        });
        function ytVidId(url) {
            var p = /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
            return (url.match(p)) ? RegExp.$1 : false;
        }

        $('#youtube').bind("change", function() {
            var url = $(this).val();
            if (ytVidId(url) !== false) {
                $('#ytlInfo').html('');
            } else {
                $("#ytlInfo").html('Link do youtube inválido.');
                $('#youtube').val('');
            }
        });
    </script>

@endsection
