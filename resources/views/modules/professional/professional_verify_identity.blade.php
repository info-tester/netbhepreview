@extends('layouts.app')
@section('title')
  @lang('client_site.verify_identity')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('client_site.verify_identity_heading')</h2>
        <div class="bokcntnt-bdy">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile'), <a href="{{ route('professional_qualification') }}">@lang('client_site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">
                    <h5>@lang('client_site.verify_identity_text')</h5>
                    <div class="clearfix"></div>
                    <hr/>
                    <form name="myForm" id="myForm" method="post" action="{{route('professional.verify.identity')}}" enctype="multipart/form-data">
                    @csrf
                        <div class="form_body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.company_data')</label>
                                        <input type="file" class="personal-type" id="company_data" name="company_data">
                                        <label for="" class="error company_data_error" style="display: none;">@lang('client_site.image_doc_or_pdf_allowed')</label>
                                        <div class="clearfix"></div>
                                        @lang('site.existing_file')
                                        <div class="field company_data">
                                            {{@$user->company_data}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.passport')</label>
                                        <input type="file" class="personal-type" id="passport" name="passport">
                                        <label for="" class="error passport_error" style="display: none;">@lang('client_site.image_doc_or_pdf_allowed')</label>
                                        <div class="clearfix"></div>
                                        @lang('site.existing_file')
                                        <div class="field passport">
                                            {{@$user->passport}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.cpf')</label>
                                        <input type="file" class="personal-type" id="cpf" name="cpf">
                                        <label for="" class="error cpf_error" style="display: none;">@lang('client_site.image_doc_or_pdf_allowed')</label>
                                        <div class="clearfix"></div>
                                        @lang('site.existing_file')
                                        <div class="field cpf">
                                            {{@$user->cpf}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.proof_of_residence')</label>
                                        <input type="file" class="personal-type" id="proof_of_residence" name="proof_of_residence">
                                        <label for="" class="error proof_of_residence_error" style="display: none;">@lang('client_site.image_doc_or_pdf_allowed')</label>
                                        <div class="clearfix"></div>
                                        @lang('site.existing_file')
                                        <div class="field proof_of_residence">
                                            {{@$user->proof_of_residence}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <button type="button" class="login_submitt" id="save_identity">@lang('client_site.save')</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')

<script>
    var all_empty = "{{@$all_empty}}";
    var flag = 0;
    $(document).ready(function(){
        $('#save_identity').click(function(){
            flag = 0;
            $('.error').hide();
            if(all_empty == "true" && $('#company_data').val() == "" && $('#passport').val() == "" && $('#cpf').val() == "" && $('#proof_of_residence').val() == ""){
                toastr.error("@lang('client_site.atleast_one_identity')");
            } else {
                const validImageTypes = ['jpeg', 'jpg', 'png', 'doc', 'docx', 'pdf'];
                var company_data_type = passport_type = cpf_type = proof_of_residence_type = "";
                if($('#company_data').val() != "") {
                    var file = $('#company_data')[0].files[0];
                    var fileName = file.name;
                    company_data_type = fileName.split('.').pop();
                    console.log(company_data_type);
                    if (!validImageTypes.includes(company_data_type)) {
                        $('.company_data_error').show();
                        flag = 1;
                    }
                }
                if($('#passport').val() != "") {
                    var file = $('#passport')[0].files[0];
                    var fileName = file.name;
                    passport_type = fileName.split('.').pop();
                    console.log(passport_type);
                    if (!validImageTypes.includes(passport_type)) {
                        $('.passport_error').show();
                        flag = 1;
                    }
                }
                if($('#cpf').val() != "") {
                    var file = $('#cpf')[0].files[0];
                    var fileName = file.name;
                    cpf_type = fileName.split('.').pop();
                    console.log(cpf_type);
                    if (!validImageTypes.includes(cpf_type)) {
                        $('.cpf_error').show();
                        flag = 1;
                    }
                }
                if($('#proof_of_residence').val() != "") {
                    var file = $('#proof_of_residence')[0].files[0];
                    var fileName = file.name;
                    proof_of_residence_type = fileName.split('.').pop();
                    console.log(proof_of_residence_type);
                    if (!validImageTypes.includes(proof_of_residence_type)) {
                        $('.proof_of_residence_error').show();
                        flag = 1;
                    }
                }
                
                if(flag == 0){
                    console.log("Submitting");
                    $('#myForm').submit();
                }
            }
        });
        $('.field').each(function(i, elem){
            console.log("Looping");
            console.log($(elem)[0]);
            var text = $(elem).text().trim();
            var ext = text.split('.').pop();
            // console.log(ext == 'jpg');
            var html = "";
            if(ext == 'jpg' || ext == 'jpeg' || ext == 'png'){
                html = `<i class="fa fa-picture-o" aria-hidden="true"></i>
                <a target="_blank" href="{{ URL::to('storage/app/public/uploads/identity') }}/${text}">${text}</a>`;
            } else if(ext == 'doc' || ext == 'docx'){
                html = `<i class="fa fa-file-text-o" aria-hidden="true"></i>
                <a href="{{ URL::to('storage/app/public/uploads/identity/') }}/${text}">${text}</a>`;
            } else if(ext == 'pdf'){
                html = `<i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                <a href="{{ URL::to('storage/app/public/uploads/identity/') }}/${text}">${text}</a>`;
            }
            $(elem).html(html);
        });
    });
</script>
@endsection
