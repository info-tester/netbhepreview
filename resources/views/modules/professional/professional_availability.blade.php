@extends('layouts.app')
@section('title')
  @lang('site.my_availability')
@endsection
@section('style')
@include('includes.style')
<style>

 .tooltip {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted #ccc;
        color: #006080;
        opacity:1 !important;
        z-index: 0 !important;
    }
    .tooltip .tooltiptext {
        visibility: hidden;
        position: absolute;
        width: 250px;
        background-color: #555;
        color: #fff;
        text-align: center;
        padding: 5px;
        border-radius: 6px;
        z-index: 1;
        opacity: 0;
        transition: opacity 0.3s;
        margin-left:10px
    }
    .tooltip-right {
        top: 0px;
        left: 125%;
    }
    .tooltip-right::after {
        content: "";
        position: absolute;
        top: 10%;
        right: 100%;
        margin-top: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: transparent #555 transparent transparent;
    }
    .tooltip:hover .tooltiptext {
        visibility: visible;
        opacity: 1;
    }
    .tooltip_img i{
        margin-left: 10px !important;
        margin-top: 20px !important;
    }
    .tooltip_img .tooltiptext{
        margin-top: 15px !important;
    }
.time_slot_span{
    background: #1781d2;
    padding: 2px 7px;
    border: solid 1px #475dfb;
    border-radius: 12px;
    color: #fff;
    margin: 0 15px 10px 0;
    float: left;
    cursor: pointer;

}

}
.ui-datepicker{
	z-index:2 !important;
	}
.form-group {
	 width: 100% !important;
}
.monthDatePicker{
    position: absolute;
    /* top: 290px !important; */
    left: 511.2px;
    z-index: 1;
    display: block;
}
.monthDatePicker .ui-datepicker-calendar{
    display: none !important;
}
.monthDatePicker .ui-widget-content{
    margin: 0 !important;
}
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('client_site.availability')</h2>
        <div class="bokcntnt-bdy">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile')<a href="{{ route('professional_qualification') }}">@lang('client_site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
            @include('includes.professional_sidebar')

            <div class="dshbrd-rghtcntn">
                @if(auth()->user()->sell != 'PS')
                <form action="{{route('submit_user_availability')}}" method="post" id="avl-form">
                    @csrf
                    <input type="text" id="create_copy" value="create" hidden>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="personal-label">@lang('site.time_period')
                                    <small class="tooltip">
                                        <i class="fa fa-question-circle" aria-hidden="true"></i>
                                        <span class="tooltiptext tooltip-right">@lang('site.time_period_tooltip')</span>
                                    </small>
                                </label>
                                <select id="period" name="period" class="form-control required">
                                    <option value="D" id="date_period">@lang('site.date_wise')</option>
                                    <option value="M">@lang('site.monthly')</option>
                                    <option value="W">@lang('site.weekly')</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <button type="button" class="login_submitt ml-1 create_schedule" id="toggle_create_copy">@lang('site.copy_schedule')</button>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="create_schedule">
                        <div class="col-md-6 col_hide" id="col_D">
                            <div class="form-group">
                                <label class="personal-label">@lang('site.date')
                                    <small class="tooltip">
                                        <i class="fa fa-question-circle" aria-hidden="true"></i>
                                        <span class="tooltiptext tooltip-right">@lang('site.tooltip_data')</span>
                                    </small>
                                </label>
                                <input type="text" autocomplete="off" name="date" class="form-control datepicker required" placeholder="@lang('site.date')">
                            </div>
                        </div>
                        <div class="col-md-6 col_hide" id="col_M" style="display:none;">
                            <div class="form-group">
                                <label class="personal-label">@lang('site.month')
                                    <small class="tooltip">
                                        <i class="fa fa-question-circle" aria-hidden="true"></i>
                                        <span class="tooltiptext tooltip-right">@lang('site.tooltip_data')</span>
                                    </small>
                                </label>
                                <input type="text" autocomplete="off" name="month" class="form-control monthpicker required" placeholder="@lang('site.month')">
                            </div>
                        </div>
                        <div class="col-md-6 col_hide" id="col_W" style="display:none;">
                            <div class="form-group">
                                <label class="personal-label">@lang('site.week')
                                    <small class="tooltip">
                                        <i class="fa fa-question-circle" aria-hidden="true"></i>
                                        <span class="tooltiptext tooltip-right">@lang('site.tooltip_data')</span>
                                    </small>
                                </label>
                                <input type="text" id="weeknumber" name="weeknumber" hidden>
                                <input type="text" autocomplete="off" name="week" class="form-control weekpicker required" data-idchng="weeknumber" placeholder="@lang('site.date')">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="personal-label">@lang('site.interval')
                                    <small class="tooltip">
                                                        <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                        <span class="tooltiptext tooltip-right">@lang('site.tooltip_time_intervl')</span>
                                    </small>
                                </label>
                                <select id="interval" name="interval" class="form-control required">
                                    <option value="">@lang('site.time_interval')</option>
                                    @php
                                        $time = $user->rate_price/60;
                                        $call_durations = $user->get_call_durations()->pluck('call_duration')->toArray();
                                    @endphp
                                    @if( count($user->get_call_durations()->pluck('call_duration_id')->toArray()) > 0)
                                        @foreach($call_durations as $call_dur)
                                            <option value="{{$call_dur}}">{{$call_dur}} Minutos</option>
                                        @endforeach
                                    @else
                                        <option value="60">60 Minutos</option>
                                    @endif

                                </select>

                            </div>
                        </div>
                        <div class="col-md-12 time_slot">

                        </div>
                    </div>
                    <div class="row" id="copy_schedule" style="display:none;">
                        <div class="col-md-12 row row_hide" id="row_M">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="personal-label">@lang('site.copy_month_from')
                                        <small class="tooltip">
                                            <i class="fa fa-question-circle" aria-hidden="true"></i>
                                            <span class="tooltiptext tooltip-right">@lang('site.copy_month_from_tooltip')</span>
                                        </small>
                                    </label>
                                    <input type="text" autocomplete="off" name="copy_month_from" class="form-control monthpicker required" placeholder="@lang('site.copy_month_from')">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="personal-label">@lang('site.copy_month_to')
                                        <small class="tooltip">
                                            <i class="fa fa-question-circle" aria-hidden="true"></i>
                                            <span class="tooltiptext tooltip-right">@lang('site.copy_month_to_tooltip')</span>
                                        </small>
                                    </label>
                                    <input type="text" autocomplete="off" name="copy_month_to" class="form-control monthpicker required" placeholder="@lang('site.copy_month_to')">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 row row_hide" id="row_W" style="display:none;">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="personal-label">@lang('site.copy_week_from')
                                        <small class="tooltip">
                                            <i class="fa fa-question-circle" aria-hidden="true"></i>
                                            <span class="tooltiptext tooltip-right">@lang('site.copy_week_from_tooltip')</span>
                                        </small>
                                    </label>
                                    <input type="text" id="copy_weeknumber_from" name="copy_weeknumber_from" hidden>
                                    <input type="text" autocomplete="off" name="copy_week_from" id="copy_week_from" data-idchng="copy_weeknumber_from" class="form-control weekpicker required" placeholder="@lang('site.copy_week_from')">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="personal-label">@lang('site.copy_week_to')
                                        <small class="tooltip">
                                            <i class="fa fa-question-circle" aria-hidden="true"></i>
                                            <span class="tooltiptext tooltip-right">@lang('site.copy_week_to_tooltip')</span>
                                        </small>
                                    </label>
                                    <input type="text" id="copy_weeknumber_to" name="copy_weeknumber_to" hidden>
                                    <input type="text" autocomplete="off" name="copy_week_to" id="copy_week_to" data-idchng="copy_weeknumber_to" class="form-control weekpicker required" placeholder="@lang('site.copy_week_to')">
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="button" class="login_submitt _avl-sub">@lang('site.submit')</button>
                </form>
                @endif

                <div class="row" style="float: left;width: 100%">
                    <div class="col-md-12">
                    <strong>O valor será cobrado automaticamente sobre o preço definido pelo profissional quantos aos minutos.</strong>
                    </div>
                </div>
                {{--@dump($avl)--}}
                <div class="mainDiv">
                    @if(sizeof(@$avl)>0)
                        <div class="buyer_table">
                            @if(auth()->user()->sell != 'PS')
                            <a class="btn rjct mb-2" id="del_avl" href="javascript:;">{{ __('site.delete_all') }}</a>
                            @endif
                            <div class="table-responsive">
                                <div class="table">
                                    <div class="one_row1 hidden-sm-down only_shawo">
                                        <div class="cell1 tab_head_sheet">@lang('client_site.day')</div>

                                        <div class="cell1 tab_head_sheet">@lang('client_site.from_time')</div>
                                        <div class="cell1 tab_head_sheet">@lang('client_site.to_time')</div>

                                        <div class="cell1 tab_head_sheet">@lang('client_site.action')</div>
                                    </div>
                                    <!--row 1-->
                                    @php
                                        $day = [
                                            '1'     =>  'domingo',
                                            '2'     =>  'Segunda-feira',
                                            '3'     =>  'terça-feira',
                                            '4'     =>  'quarta-feira',
                                            '5'     =>  'quinta-feira',
                                            '6'     =>  'sexta-feira',
                                            '7'     =>  'sábado'
                                        ];
                                    @endphp
                                    @foreach(@$avl as $ql)
                                    <div class="one_row1 small_screen31">
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('client_site.day')</span>
                                            <p class="add_ttrr">
                                                {{-- {{ Date::parse(@$ql->date)->format('d/m/Y (D)')}} --}}
                                                {{ Date::parse(toUserTime(@$ql->slot_start))->format('d/m/Y (D)')}}
                                            </p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('client_site.from_time')</span>
                                            <p class="add_ttrr">{{ toUserTime(@$ql->slot_start, 'H:i') }}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('client_site.to_time')</span>
                                            <p class="add_ttrr">{{ toUserTime(@$ql->slot_end, 'H:i') }}</p>
                                        </div>

                                        <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                            <span class="W55_1">@lang('client_site.action')</span>

                                            <a href="{{ route('delete_avl',['id'=>@$ql->id]) }}" onclick="return confirm('Você tem certeza ?')" class="rjct"><i class="fa fa-trash"></i> @lang('client_site.delete') </a>

                                            {{-- <a href="#" class="rjct" style="background: #098005;"><i class="fa fa-pencil"></i> @lang('client_site.edit') </a> --}}
                                        </div>
                                    </div>
                                    @endforeach
                                    {{-- {{ $avl->->onEachSide(5)->links() }} --}}
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/moment.js"></script>

<script>
    $(document).ready(function(){
        $('#myForm').validate();
        $('.datepicker').datepicker({
            dateFormat: "dd-mm-yy",
            monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho',
            'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
            showButtonPanel: true,
            defaultDate: new Date()
        });
        $('.timepicker').timepicker({
            timeFormat: 'H:mm',
            interval: 15,
            minTime: '00',
            maxTime: '11:00pm',
            startTime: '09:00',
            dynamic: false,
            dropdown: true,
            scrollbar: true
        });
        $('.monthpicker').on('change focus keyup', function(){
            if(!$('#ui-datepicker-div').hasClass('monthDatePicker')) $('#ui-datepicker-div').addClass('monthDatePicker');
        });
        $('.datepicker').on('change focus keyup', function(){
            $('#ui-datepicker-div').removeClass('monthDatePicker');
        });
        $('.weekpicker').on('change focus keyup', function(){
            $('#ui-datepicker-div').removeClass('monthDatePicker');
        });
        $('.monthpicker').datepicker( {
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'MM yy',
            monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho',
            'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            monthNamesShort: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho',
            'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            onClose: function(dateText, inst) {
                $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
            }
        });
        // moment.locale('en', {
        //     week: { dow: 1 } // Monday is the first day of the week
        // });
        $(".weekpicker").datepicker({
            format: 'MM-DD-YYYY',
            monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho',
            'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
            showButtonPanel: true,
            defaultDate: new Date()
        });
        $('.weekpicker').on('change', function (e) {
            var value = $(this).val();
            var weeknumber = $(this).data('idchng');
            var firstDate = moment(value, "MM-DD-YYYY").day(0).format("MM-DD-YYYY");
            var lastDate =  moment(value, "MM-DD-YYYY").day(6).format("MM-DD-YYYY");
            $(this).val(firstDate + " - " + lastDate);
            $("#"+weeknumber).val(getWeekNumber(new Date(lastDate)));
        });
        $('#period').on('change', function(){
            var create_copy = $('#create_copy').val();
            if(create_copy == 'copy'){
                $('.row_hide').hide();
                $('#row_'+$(this).val()).show();
            } else {
                $('.col_hide').hide();
                $('#col_'+$(this).val()).show();
            }
        });
        $('#del_avl').click(function(e){
            swal({
                title: "Você tem certeza?",
                text: "@lang('client_site.delete_all_availability')",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                buttons: ['Cancel', 'Delete']
            })
            .then((willDelete) => {
                if (willDelete) {
                    console.log("delete");
                    window.location.href = "{{ route('delete_avl.all') }}";
                } else {
                    return false;
                }
            });
        })
        $('._avl-sub').click(function() {
            var period = $('#period').val();
            var create_copy = $('#create_copy').val();
            var my_url = "";
            if(create_copy == 'copy'){
                my_url = "{{ route('user_availability_copy.ajax.check') }}";
            } else {
                my_url = "{{ route('user_availability_update.ajax.check') }}";
            }
            if ($('#avl-form').valid()) {
                $.ajax({
                    url: my_url,
                    type: 'post',
                    data: $('#avl-form').serialize(),
                    dataType: 'JSON',
                    success: function (res) {
                        if (res.error && res.error.message) {
                            swal('Erro!', res.error.message, 'error');
                        } else if (res.result) {
                            console.log(res);
                            $('#avl-form').submit();
                        }
                    }
                });
            }
        });
        $('#interval').change(function(event) {
            var interval = $(this).val();
            var period = $('#period').val();
            if(interval != null) {
                if(period == 'D' && $('.datepicker').val() == ''){
                    toastr.warning('@lang('site.please_select_date')');
                } else if(period == 'M' && $('.monthpicker').val() == '') {
                    toastr.warning('@lang('site.please_select_date')');
                } else if(period == 'W' && $('.weekpicker').val() == '') {
                    toastr.warning('@lang('site.please_select_date')');
                } else {
                    /*var reqData = {
                        _token: '@csrf_token()',
                        params: {
                            date: $('.datepicker').val(),
                            interval: interval
                        }
                    }
                    $.ajax({
                        url: '',
                        method: 'post',
                        data: reqData,
                        success: function(response) {
                            console.log(response);
                        }
                    })*/
                    if(interval == 60) {
                        interval1 = '01:00:00'
                    } else if(interval == 90) {
                        interval1 = '01:30:00'
                    } else {
                        interval1 = '00:'+interval+':00';
                    }
                    let value = {
                      interval: interval1,
                      startTime: '00:00:00',
                      endTime: '24:00:00'
                    };

                    var inputDataFormat = "HH:mm:ss";
                    var outputFormat = "HH:mm";

                    var tmp = moment(value.interval, inputDataFormat);
                    var dif = tmp - moment().startOf("day");

                    var startIntervalTime = moment(value.startTime, inputDataFormat).add(-dif, "ms");
                    var endIntervalTime = moment(value.startTime, inputDataFormat);
                    var finishTime = moment(value.endTime, inputDataFormat);

                    console.log(finishTime);
                    // console.log(startIntervalTime.format(outputFormat));
                    var html = '<h4>@lang('site.choose_your_time_slot')</h4>';
                    var intervals = [];
                    var i=0;
                    while(startIntervalTime < finishTime && endIntervalTime <= finishTime) {
                        var format = startIntervalTime.format(outputFormat) + " - " + endIntervalTime.format(outputFormat);
                        if(i != 0) {
                            html += '<div class="checkbox-group_time">\
                                    <input type="checkbox" name="time_slot[]" id="time_slot_id_'+i+'" value="' + format + '">\
                                    <label for="time_slot_id_'+i+'" id="time_slot_'+ i +'" class="time_slot_span">\
                                        <span class="check find_chek"></span>\
                                        <span class="box W25 boxx">' + format + '</span>\
                                    </label>\
                                </div>';
                        }
                        // intervals.push(format);
                        startIntervalTime.add(dif, "ms");
                        endIntervalTime.add(dif, "ms");
                        i++;
                    }
                    $('.time_slot').html(html);
                }
            }
        });

        $('#toggle_create_copy').click(function(){
            if($(this).hasClass('create_schedule')){
                // copy mode
                $(this).removeClass('create_schedule');
                $(this).addClass('copy_schedule');
                $(this).text("@lang('site.create_schedule')");
                $('#create_copy').val('copy');
                $('#create_schedule').hide();
                $('#copy_schedule').show();
                $('#period').val('M');
                $('#date_period').hide();
                $('#avl-form').get(0).setAttribute('action', "{{route('submit_user_availability_copy')}}");

            } else if($(this).hasClass('copy_schedule')){
                // create mode
                $(this).removeClass('copy_schedule');
                $(this).addClass('create_schedule');
                $(this).text("@lang('site.copy_schedule')");
                $('#create_copy').val('create');
                $('#copy_schedule').hide();
                $('#create_schedule').show();
                $('#period').val('D');
                $('#date_period').show();
                $('#avl-form').get(0).setAttribute('action', "{{route('submit_user_availability')}}");
            }
        });

    });
    function getWeekNumber(d) {
        // Copy date so don't modify original
        d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
        // Set to nearest Thursday: current date + 4 - current day number
        // Make Sunday's day number 7
        d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay()||7));
        // Get first day of year
        var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
        // Calculate full weeks to nearest Thursday
        var weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7);
        // Return array of year and week number
        return [d.getUTCFullYear(), weekNo];
    }

// for time interval






</script>
@endsection
