@extends('layouts.app')
@section('title')
  @lang('site.my_qualification')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<style>
    .error{
        color:red!important;
    }

     .tooltip {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted #ccc;
        color: #006080;
        opacity:1 !important;
    }
    .tooltip .tooltiptext {
        visibility: hidden;
        position: absolute;
        width: 250px;
        background-color: #555;
        color: #fff;
        text-align: center;
        padding: 5px;
        border-radius: 6px;
        z-index: 1;
        opacity: 0;
        transition: opacity 0.3s;
        margin-left:10px
    }
    .tooltip-right {
        top: 0px;
        left: 125%;
    }
    .tooltip-right::after {
        content: "";
        position: absolute;
        top: 10%;
        right: 100%;
        margin-top: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: transparent #555 transparent transparent;
    }
    .tooltip:hover .tooltiptext {
        visibility: visible;
        opacity: 1;
    }
    .tooltip_img i{
        margin-left: 10px !important;
        margin-top: 20px !important;
    }
    .tooltip_img .tooltiptext{
        margin-top: 15px !important;
    }
</style>
<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('client_site.qualification')</h2>
        <div class="bokcntnt-bdy">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0 && $user->sell!='PS')
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile')</p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
           <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">
                    <form name="myForm" id="myForm" action="{{ !@$update->id ? route('add_professional_qualification') : route('my_qualification_update',['id'=>@$update->id]) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form_body">
                            <div class="row">

                                <div class="col-lg-4 col-md-5 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.degree')
                                            <small class="tooltip">
                                                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                    <span class="tooltiptext tooltip-right">@lang('site.tooltip_degree')</span>
                                    </small>
                                        </label>
                                        <input type="text" placeholder="@lang('client_site.degree')" name="degree" id="degree" class="required personal-type" value="{{ @$update->degree }}">
                                    </div>
                                </div>
                                <div class="col-lg-8 col-md-7 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.university')
                                            <small class="tooltip">
                                                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                    <span class="tooltiptext tooltip-right">@lang('site.tooltip_university')</span>
                                    </small>
                                        </label>
                                        <input type="text" name="university" id="university" placeholder="@lang('client_site.university')" class="required personal-type" value="{{ @$update->university }}">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.from')</label>
                                        <div class="lftdrp">
                                            <select class="personal-type personal-select" name="from_month" id="from_month">
                                                <option value="">@lang('client_site.month')</option>
                                                <option value="1" @if(@$update->from_month==1) selected @endif>@lang('client_site.january')</option>
                                                <option value="2" @if(@$update->from_month==2) selected @endif>@lang('client_site.february')</option>
                                                <option value="3" @if(@$update->from_month==3) selected @endif>@lang('client_site.march')</option>
                                                <option value="4" @if(@$update->from_month==4) selected @endif>@lang('client_site.april')</option>
                                                <option value="5" @if(@$update->from_month==5) selected @endif>@lang('client_site.may')</option>
                                                <option value="6" @if(@$update->from_month==6) selected @endif>@lang('client_site.june')</option>
                                                <option value="7" @if(@$update->from_month==7) selected @endif>@lang('client_site.july')</option>
                                                <option value="8" @if(@$update->from_month==8) selected @endif>@lang('client_site.august')</option>
                                                <option value="9" @if(@$update->from_month==9) selected @endif>@lang('client_site.september')</option>
                                                <option value="10" @if(@$update->from_month==10) selected @endif>@lang('client_site.october')</option>
                                                <option value="11" @if(@$update->from_month==11) selected @endif>@lang('client_site.november')</option>
                                                <option value="12" @if(@$update->from_month==12) selected @endif>@lang('client_site.december')</option>
                                            </select>
                                        </div>
                                        @php
                                            $year = date('Y');
                                        @endphp
                                        <div class="ritdrp">
                                            <select class="personal-type personal-select required" name="from_year" id="from_year">
                                                <option value="">@lang('client_site.year')</option>
                                                @for($i=1970; $i<=$year; $i++)
                                                    <option value="{{ $i }}" @if(@$update->from_year==$i) selected @endif>{{ $i }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <div id="err_date" class="error"></div>
                                    </div>
                                </div>
                                {{-- <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.to')</label>
                                        <div class="lftdrp">
                                            <select class="personal-type personal-select" name="to_month" id="to_month">
                                                <option value="">@lang('client_site.month')</option>
                                                <option value="1" @if(@$update->to_month==1) selected @endif>@lang('client_site.january')</option>
                                                <option value="2" @if(@$update->to_month==2) selected @endif>@lang('client_site.february')</option>
                                                <option value="3" @if(@$update->to_month==3) selected @endif>@lang('client_site.march')</option>
                                                <option value="4" @if(@$update->to_month==4) selected @endif>@lang('client_site.april')</option>
                                                <option value="5" @if(@$update->to_month==5) selected @endif>@lang('client_site.may')</option>
                                                <option value="6" @if(@$update->to_month==6) selected @endif>@lang('client_site.june')</option>
                                                <option value="7" @if(@$update->to_month==7) selected @endif>@lang('client_site.july')</option>
                                                <option value="8" @if(@$update->to_month==8) selected @endif>@lang('client_site.august')</option>
                                                <option value="9" @if(@$update->to_month==9) selected @endif>@lang('client_site.september')</option>
                                                <option value="10" @if(@$update->to_month==10) selected @endif>@lang('client_site.october')</option>
                                                <option value="11" @if(@$update->to_month==11) selected @endif>@lang('client_site.november')</option>
                                                <option value="12" @if(@$update->to_month==12) selected @endif>@lang('client_site.december')</option>
                                            </select>
                                        </div>
                                        <div class="ritdrp">
                                            <select class="personal-type personal-select" name="to_year" id="to_year">
                                                <option value="">@lang('client_site.year')</option>
                                                @for($i=1970; $i<=$year+5; $i++)
                                                    <option value="{{ $i }}" @if(@$update->to_year==$i) selected @endif>{{ $i }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group check_group tilchk">
                                        <div class="checkbox-group">
                                            <input id="checkiz" name="pursuing" id="pursuing" type="checkbox"  @if(@$update->pursuing=="Y") checked @endif>
                                            <label for="checkiz" class="Fs16">
                                            <span class="check find_chek"></span>
                                            <span class="box W25 boxx"></span>
                                            @lang('client_site.till_now')
                                            </label>
                                        </div>
                                    </div>
                                </div> --}}
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 mt-3">
                                    <div class="form-group check_group">
                                        <img id="certificate" src="@if(@$update->attachment){{URL::to('storage/app/public/uploads/user_qualifiaction/'.$update->attachment)}}@endif" width="100%"/>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group check_group tilchk">
                                        <!-- label class="personal-label">Upload</label> -->
                                        <div class="upldd">@lang('client_site.upload_resume') <i class="fa fa-cloud-upload" aria-hidden="true"></i>
                                            <input type="file" name="file" id="file" class="@if(!@$update->id)required @endif">
                                        </div>
                                    </div>
                                    <label for="file" generated="true" class="error" style="color: red !important"></label>
                                </div>
                                {{-- <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.curriculum')</label>
                                        <input type="text" placeholder="@lang('client_site.curriculum')" name="curriculum" id="curriculumcxxzc" class="personal-type" value="{{ @$update->curriculum }}">
                                    </div>
                                </div> --}}
                            </div>
                            <div class="col-sm-12">
                                <p class="error" id="p_error"></p>
                            </div>
                            <div class="col-sm-12">
                                <button type="submit" class="login_submitt">@lang('client_site.save')</button>
                            </div>
                                @if(sizeof($quali)>0)
                                    <div class="buyer_table">
                                        <div class="table-responsive">
                                            <div class="table">
                                                <div class="one_row1 hidden-sm-down only_shawo">
                                                    <div class="cell1 tab_head_sheet">@lang('client_site.degree')</div>
                                                    <div class="cell1 tab_head_sheet">@lang('client_site.university')</div>
                                                    <div class="cell1 tab_head_sheet">@lang('client_site.experience')</div>
                                                    {{-- <div class="cell1 tab_head_sheet">To Date</div> --}}
                                                    <div class="cell1 tab_head_sheet">@lang('client_site.action')</div>
                                                </div>
                                                <!--row 1-->
                                                @php
                                                    $month = [
                                                        '1'     =>  __('client_site.january'),
                                                        '2'     =>  __('client_site.february'),
                                                        '3'     =>  __('client_site.march'),
                                                        '4'     =>  __('client_site.april'),
                                                        '5'     =>  __('client_site.may'),
                                                        '6'     =>  __('client_site.june'),
                                                        '7'     =>  __('client_site.july'),
                                                        '8'     =>  __('client_site.august'),
                                                        '9'     =>  __('client_site.september'),
                                                        '10'    =>  __('client_site.october'),
                                                        '11'    =>  __('client_site.november'),
                                                        '12'    =>  __('client_site.december')
                                                    ];
                                                    $dates = array();
                                                @endphp
                                                @foreach(@$quali as $ql)
                                                @php
                                                    array_push( $dates, date('Y-m-d', strtotime($ql->from_year.'-'.$ql->from_month.'-01')) )
                                                @endphp
                                                <div class="one_row1 small_screen31">
                                                    <div class="cell1 tab_head_sheet_1">
                                                        <span class="W55_1">@lang('client_site.degree')</span>
                                                        <p class="add_ttrr">{{ @$ql->degree }}</p>
                                                    </div>
                                                    <div class="cell1 tab_head_sheet_1">
                                                        <span class="W55_1">@lang('client_site.university')</span>
                                                        <p class="add_ttrr">{{ @$ql->university }}</p>
                                                    </div>
                                                    <div class="cell1 tab_head_sheet_1">
                                                        <span class="W55_1">@lang('client_site.experience')</span>
                                                        <p class="add_ttrr">
                                                            @if($ql->pursuing!="Y")
                                                            {{ @$month[@$ql->from_month].' '.@$ql->from_year }}
                                                            {{-- - {{ @$month[@$ql->to_month].' '.@$ql->to_year }} --}}
                                                            @else
                                                                @lang('client_site.pursuing')
                                                            @endif
                                                        </p>
                                                    </div>
                                                    {{-- <div class="cell1 tab_head_sheet_1">
                                                        <span class="W55_1">To Date</span>
                                                        <p class="add_ttrr">{{ @$month[@$ql->to_month].' '.@$ql->to_year }}</p>
                                                    </div> --}}
                                                    <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                                        <span class="W55_1">@lang('client_site.action')</span>
                                                        <a href="{{ route('my_qualification',['id'=>@$ql->id]) }}" class="acpt">@lang('client_site.edit')</a>
                                                        <a href="{{ route('delete_qualification',['id'=>@$ql->id]) }}" onclick="return confirm(`@lang('client_site.are_you_sure')`)" class="rjct">@lang('client_site.delete') </a>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
<style type="text/css">
    .upldd {
        display: block;
        width: auto;
        border-radius: 4px;
        text-align: center;
        background: #9caca9;
        cursor: pointer;
        overflow: hidden;
        padding: 10px 15px;
        font-size: 15px;
        color: #fff;
        cursor: pointer;
        float: left;
        margin-top: 15px;
        font-family: 'Poppins', sans-serif;
        position: relative;
    }
    .upldd input {
        position: absolute;
        font-size: 50px;
        opacity: 0;
        left: 0;
        top: 0;
        width: 200px;
        cursor: pointer;
    }
    .upldd:hover{
        background: #1781d2;
    }
</style>
<script>
    var dates = {!! json_encode(@$dates) !!};
    // console.log(dates);
    $(function() {
            $("#datepicker").datepicker({dateFormat: "dd-mm-yy",
               defaultDate: new Date(),
               onClose: function( selectedDate ) {
               $( "#datepicker1").datepicker( "option", "minDate", selectedDate );
              }
            });
        $("#datepicker1").datepicker({dateFormat: "dd-mm-yy",
         defaultDate: new Date(),
              onClose: function( selectedDate ) {
              $( "#datepicker" ).datepicker( "option", "maxDate", selectedDate );
            }
         });

        });

    $(document).ready(function(){
        $('#myForm').validate({
            rules: {},
            submitHandler: function(form) {
                var flag = 0;
                var current_date = new Date( $('#from_year').val() + '-' + $('#from_month').val() + '-01' );
                $(dates).each(function(i, date){
                    date = new Date(date);
                    console.log(current_date.getMonth() == date.getMonth() && current_date.getFullYear() == date.getFullYear());
                    if(current_date.getMonth() == date.getMonth() && current_date.getFullYear() == date.getFullYear()){
                        flag = 1;
                    }
                });
                if(flag == 1){
                    $('#err_date').text("@lang('site.this_date_already_exists')");
                    return false;
                } else {
                    form.submit();
                }
                // return false;
            }
        });
        // $('#myForm').submit(function(){
        //     if($('#checkiz').prop('checked')==false){
        //         if(('#from_year').val()!=""){
        //             if($('#from_year').val()>=$('#to_year').val()){
        //                 $('#p_error').html('@lang('client_site.from_month_or_from')');

        //                 return false;

        //             }
        //             else{
        //                 return true;
        //             }
        //         }
        //     }
        // });
        $('#checkiz').click(function(){
            if($('#checkiz').prop('checked')==true){
                $('#to_month').prop('disabled', 'disabled');
                $('#to_year').prop('disabled', 'disabled');
            }
            else{
                $('#to_month').prop('disabled', false);
                $('#to_year').prop('disabled', false);
            }
        });
    });
    function readURL(input) {
        if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('.uploaded_ppc').show();
            $('#certificate').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
        }
    }
    $("#file").change(function() {
        readURL(this);
    });
</script>
@endsection
