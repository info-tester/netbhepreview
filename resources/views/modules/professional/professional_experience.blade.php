@extends('layouts.app')
@section('title')
  @lang('site.my_experience')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('client_site.experience')</h2>
        <div class="bokcntnt-bdy">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile'), <a href="{{ route('professional_qualification') }}">@lang('client_site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">
                    @if(!@$update->id)
                        <form name="myForm" id="myForm" method="post" action="{{ route('add.professional.experience')}}">

                    @else
                        <form name="myForm" id="myForm" method="post" action="{{ route('update.professional.experience',['id'=>@$update->id])}}">
                    @endif
                        @csrf
                        <div class="form_body">
                            <div class="row">
                                {{-- <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.organization')</label>
                                        <input type="text" placeholder="@lang('client_site.organization')" class="required personal-type" name="organization" id="organization" value="{{ @$update->organization }}">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.role')</label>
                                        <input type="text" placeholder="@lang('client_site.role')" class="required personal-type" name="role" id="role" value="{{ @$update->role }}">
                                    </div>
                                </div> --}}
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.description')</label>
                                        <textarea class="required personal-type99" placeholder="Digite algumas palavras sobre o seu trabalho." name="description" id="description">{{ @$update->description }}</textarea>
                                    </div>
                                </div>
                                {{-- <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.from')</label>
                                        <div class="lftdrp">
                                            <select class="personal-type personal-select" name="from_month" id="from_month">
                                                <option value="">@lang('client_site.month')</option>
                                                <option value="1" @if(@$update->from_month==1) selected @endif>@lang('client_site.january')</option>
                                                <option value="2" @if(@$update->from_month==2) selected @endif>@lang('client_site.february')</option>
                                                <option value="3" @if(@$update->from_month==3) selected @endif>@lang('client_site.march')</option>
                                                <option value="4" @if(@$update->from_month==4) selected @endif>@lang('client_site.april')</option>
                                                <option value="5" @if(@$update->from_month==5) selected @endif>@lang('client_site.may')</option>
                                                <option value="6" @if(@$update->from_month==6) selected @endif>@lang('client_site.june')</option>
                                                <option value="7" @if(@$update->from_month==7) selected @endif>@lang('client_site.july')</option>
                                                <option value="8" @if(@$update->from_month==8) selected @endif>@lang('client_site.august')</option>
                                                <option value="9" @if(@$update->from_month==9) selected @endif>@lang('client_site.september')</option>
                                                <option value="10" @if(@$update->from_month==10) selected @endif>@lang('client_site.october')</option>
                                                <option value="11" @if(@$update->from_month==11) selected @endif>@lang('client_site.november')</option>
                                                <option value="12" @if(@$update->from_month==12) selected @endif>@lang('client_site.december')</option>
                                            </select>
                                        </div>
                                        @php
                                            $year = date('Y');
                                        @endphp
                                        <div class="ritdrp">
                                            <select class=" personal-type personal-select" name="from_year" id="from_year">
                                                <option value="">@lang('client_site.year')</option>
                                                @for($i=1970; $i<=$year; $i++)
                                                    <option value="{{ $i }}" @if(@$update->from_year==$i) selected @endif>{{ $i }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">To</label>
                                        <div class="lftdrp">
                                            <select class=" personal-type personal-select" name="to_month" id="to_month">
                                                <option value="">@lang('client_site.month')</option>
                                                <option value="1" @if(@$update->to_month==1) selected @endif>@lang('client_site.january')</option>
                                                <option value="2" @if(@$update->to_month==2) selected @endif>@lang('client_site.february')</option>
                                                <option value="3" @if(@$update->to_month==3) selected @endif>@lang('client_site.march')</option>
                                                <option value="4" @if(@$update->to_month==4) selected @endif>@lang('client_site.april')</option>
                                                <option value="5" @if(@$update->to_month==5) selected @endif>@lang('client_site.may')</option>
                                                <option value="6" @if(@$update->to_month==6) selected @endif>@lang('client_site.june')</option>
                                                <option value="7" @if(@$update->to_month==7) selected @endif>@lang('client_site.july')</option>
                                                <option value="8" @if(@$update->to_month==8) selected @endif>@lang('client_site.august')</option>
                                                <option value="9" @if(@$update->to_month==9) selected @endif>@lang('client_site.september')</option>
                                                <option value="10" @if(@$update->to_month==10) selected @endif>@lang('client_site.october')</option>
                                                <option value="11" @if(@$update->to_month==11) selected @endif>@lang('client_site.november')</option>
                                                <option value="12" @if(@$update->to_month==12) selected @endif>@lang('client_site.december')</option>
                                            </select>
                                        </div>
                                        <div class="ritdrp">
                                            <select class=" personal-type personal-select" name="to_year" id="to_year">
                                                <option value="">@lang('client_site.year')</option>
                                                @for($i=1970; $i<=$year; $i++)
                                                    <option value="{{ $i }}" @if(@$update->to_year==$i) selected @endif>{{ $i }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group check_group tilchk">
                                        <div class="checkbox-group">
                                            <input id="checkiz" name="pursuing" id="pursuing" type="checkbox"  @if(@$update->pursuing=="Y") checked @endif>
                                            <label for="checkiz" class="Fs16">
                                            <span class="check find_chek"></span>
                                            <span class="box W25 boxx"></span>
                                            @lang('client_site.till_now')
                                            </label>
                                        </div>
                                    </div>
                                </div> --}}

                                <div class="col-sm-12">
                                    <p class="error" id="p_error"></p>
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="login_submitt">@lang('client_site.save')</button>
                                </div>
                                @if(sizeof($exp)>0)
                                    <div class="buyer_table">
                                        <div class="table-responsive">
                                            <div class="table">
                                                <div class="one_row1 hidden-sm-down only_shawo">
                                                    {{-- <div class="cell1 tab_head_sheet">@lang('client_site.organization')</div> --}}
                                                    <div class="cell1 tab_head_sheet">@lang('client_site.description')</div>
                                                    {{-- <div class="cell1 tab_head_sheet"> @lang('client_site.experience')</div> --}}
                                                    {{-- <div class="cell1 tab_head_sheet">To Experience</div> --}}
                                                    <div class="cell1 tab_head_sheet">@lang('client_site.action')</div>
                                                </div>
                                                 @php
                                                    $month = [
                                                        '1'     =>  'January',
                                                        '2'     =>  'Februaryy',
                                                        '3'     =>  'March',
                                                        '4'     =>  'April',
                                                        '5'     =>  'May',
                                                        '6'     =>  'June',
                                                        '7'     =>  'July',
                                                        '8'     =>  'August',
                                                        '9'     =>  'September',
                                                        '10'    =>  'October',
                                                        '11'    =>  'November',
                                                        '12'    =>  'December'
                                                    ];
                                                @endphp
                                                @foreach(@$exp as $ex)
                                                <!--row 1-->
                                                <div class="one_row1 small_screen31">
                                                    {{-- <div class="cell1 tab_head_sheet_1">
                                                        <span class="W55_1">@lang('client_site.organization')</span>
                                                        <p class="add_ttrr">{{ @$ex->organization }}</p>
                                                    </div> --}}
                                                    <div class="cell1 tab_head_sheet_1">
                                                        <span class="W55_1">@lang('client_site.description')</span>
                                                        <p class="add_ttrr">{{ @$ex->description }}
                                                        </p>
                                                    </div>
                                                    {{-- <div class="cell1 tab_head_sheet_1">
                                                        <span class="W55_1">@lang('client_site.experience')</span>
                                                        <p class="add_ttrr">
                                                            @if(@$ex->pursuing=="N" && @$ex->from_year!=null)
                                                            {{ @$month[@$ex->from_month].' '.@$ex->from_year  }} - {{ @$month[@$ex->to_month].' '.@$ex->to_year  }}
                                                            @elseif(@$ex->from_year==null)
                                                                ━
                                                            @else
                                                                @lang('client_site.pursuing')
                                                            @endif
                                                        </p>
                                                    </div> --}}
                                                    {{-- <div class="cell1 tab_head_sheet_1">
                                                        <span class="W55_1">To Experience</span>
                                                        <p class="add_ttrr"></p>
                                                    </div> --}}
                                                    <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                                        <span class="W55_1">@lang('client_site.action')</span>
                                                        <a href="{{ route('edit.professional.experience',['id'=>@$ex->id]) }}" class="acpt">@lang('client_site.edit')</a>
                                                        <a href="{{ route('delete.experience',['id'=>@$ex->id]) }}" onclick="return confirm(@lang('client_site.are_you_sure'))" class="rjct">@lang('client_site.delete') </a>
                                                    </div>
                                                </div>
                                                <!--row 1-->
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')

<script>

    $(function() {
            $("#datepicker").datepicker({dateFormat: "dd-mm-yy",
               defaultDate: new Date(),
               onClose: function( selectedDate ) {
               $( "#datepicker1").datepicker( "option", "minDate", selectedDate );
              }
            });
        $("#datepicker1").datepicker({dateFormat: "dd-mm-yy",
         defaultDate: new Date(),
              onClose: function( selectedDate ) {
              $( "#datepicker" ).datepicker( "option", "maxDate", selectedDate );
            }
         });

        });
var flag = 0;
    $(document).ready(function(){
        $('#myForm').validate();
        $('#myForm').submit(function(){
            if($('#checkiz').prop('checked')==false){
                if($('#from_year').val()!=""){
                    if($('#from_year').val()>=$('#to_year').val()){
                        $('#p_error').html("@lang('client_site.from_month_or_from')");
                        return false;
                    }
                    else{
                        return true;
                    }
                }
            }
        });
        $('#checkiz').click(function(){
            if($('#checkiz').prop('checked')==true){
                $('#to_month').prop('disabled', 'disabled');
                $('#to_year').prop('disabled', 'disabled');
            }
            else{
                $('#to_month').prop('disabled', false);
                $('#to_year').prop('disabled', false);
            }
        });
    });
</script>
@endsection
