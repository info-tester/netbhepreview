@extends('layouts.app')
@section('title')
    @lang('site.welcome_to_dashboard')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('client_site.dashboard')</h2>

        <div class="bokcntnt-bdy">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0 && $user->sell!='PS')
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile')<a href="{{ route('professional_qualification') }}">@lang('client_site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>

            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="d-flex banner-side">
                    <div class="banner_side_info">
                        <span><img src="{{ URL::to('public/frontend/images/bannerimg2.png') }}"></span>
                        @if(auth()->user()->is_join_affiliate=='N')
                        <p >{{__('site.aff_join_text')}}, <a href="{{ route('request.affiliate.aprove') }}">{{__('site.click_link')}}</a></p>
                        @else
                        <p >{{__('site.aff_after_join_text')}}, <a href="{{route('affiliate.products.search.filter')}}">{{__('site.click_link')}}</a></p>
                        @endif
                        {{-- <p> You can join our affiliate program to</p> --}}
                    </div>
                    <div class="banner_side_info">
                        <span><img src="{{ URL::to('public/frontend/images/bannerimg1.png') }}"></span>
                        @if(auth()->user()->is_video_affiliate=='N')
                        <p>{{__('site.video_aff_join_text')}}, <a href="{{ route('request.video.affiliate.aprove') }}">{{__('site.click_link')}}</a></p>
                        @else
                        <p>{{__('site.video_aff_after_join_text')}}, <a href="{{route('video.affiliate.earning.list')}}">{{__('site.click_link')}}</a></p>
                        @endif
                        {{-- <p> You can join our affiliate program to</p> --}}
                    </div>
                </div>
                {{-- @if(auth()->user()->is_join_affiliate=='N')
                <center><p class="alert alert-info">You can join our affiliate program to , <a href="{{ route('request.affiliate.aprove') }}">Click this link</a></p></center>
                @else
                <center><p class="alert alert-info">Your account associated with our affiliate program to view product click this link , <a href="{{route('affiliate.products.search.filter')}}">Click this link</a></p></center>
                @endif
                @if(auth()->user()->is_video_affiliate=='N')
                <center><p class="alert alert-info">You can join our video affiliate program to , <a href="{{ route('request.video.affiliate.aprove') }}">Click this link</a></p></center>
                @else
                <center><p class="alert alert-info">Your account associated with our video affiliate program view earning, <a href="{{route('video.affiliate.earning.list')}}">Click this link</a></p></center>
                @endif --}}
                 <div class="from-field">
                    <div class="weicome">
                        <h3>{{ $dashBoardPage->title }} {{ @Auth::guard('web')->user()->nick_name ? @Auth::guard('web')->user()->nick_name : @Auth::guard('web')->user()->name }}</h3>
                        <p style="white-space: pre-wrap;">{!! $dashBoardPage->description !!}</p>
                    </div>
                    <div class="dash_main">
                        <ul>
                            <li>
                                <span><img src="{{ URL::to('public/frontend/images/dashboard_icon1.png') }}" alt=""></span>
                                <h4>@lang('client_site.net_earning')</h4>
                                <h3>R$ {{ @$netEarning }}</h3>
                            </li>
                            <li>
                                <span><img src="{{ URL::to('public/frontend/images/dashboard_icon2.png') }}" alt=""></span>
                                <h4>@lang('client_site.paid')</h4>
                                <h3>R$ {{ @$paid }}</h3>
                            </li>
                            <li>
                                <span><img src="{{ URL::to('public/frontend/images/dashboard_icon3.png') }}" alt=""></span>
                                <h4>@lang('client_site.due')</h4>
                                <h3>R$ {{ @$due }}</h3>
                            </li>
                            <li>
                                <span><img src="{{ URL::to('public/frontend/images/dashboard_icon4.png') }}" alt=""></span>
                                <h4>@lang('client_site.classes')</h4>
                                <h3>{{ @$classes }}</h3>
                            </li>
                        </ul>
                    </div>
                </div>
                <form id="myForm" name="myForm" action="{{ route('srch.prof.dash.my.booking', ['type' => @$key['type']]) }}" method="get">
                    @csrf
                    <div class="from-field">
                        <div class="frmfld">
                            <div class="form-group">
                                <label class="search_label">@lang('client_site.from_date')</label>
                                <input type="text" id="datepicker" name="from_date" class="dashboard-type" placeholder="@lang('client_site.select_date')" value="{{ @$key['from_date'] }}">
                                <img class="pstn" src="{{ URL::to('public/frontend/images/clndr.png') }}">
                            </div>
                        </div>
                        <div class="frmfld">
                            <div class="form-group">
                                <label class="search_label">@lang('site.to_date')</label>
                                <input type="text" id="datepicker1" name="to_date" class="dashboard-type" placeholder="@lang('client_site.select_date')" value="{{ @$key['to_date'] }}">
                                <img class="pstn" src="{{ URL::to('public/frontend/images/clndr.png') }}">
                            </div>
                        </div>
                        <div class="frmfld">
                            <div class="form-group">
                                <label class="search_label">@lang('client_site.select_status')</label>
                                <select class="dashboard-type dashboard_select" name="status">
                                    <option value="">@lang('client_site.awaiting_approval')</option>
                                    <option value="AA" @if(@$key['status']=="AA") selected @endif>@lang('client_site.select_status') </option>
                                    <option value="A" @if(@$key['status']=="A") selected @endif>@lang('client_site.approved') </option>
                                    <option value="R" @if(@$key['status']=="R") selected @endif>@lang('client_site.rejected') </option>
                                </select>
                            </div>
                        </div>
                        <div class="frmfld">
                            <div class="form-group">
                                <label class="search_label">@lang('client_site.type')</label>
                                <select class="dashboard-type dashboard_select required" name="type">
                                    <option value="">@lang('client_site.select_type')</option>
                                    <option value="UC" @if(@$key['type']=="UC") selected @endif>@lang('client_site.upcomming_classes')</option>
                                    <option value="PC" @if(@$key['type']=="PC") selected @endif>@lang('client_site.past_classes') </option>
                                </select>
                            </div>
                        </div>
                        <div class="frmfld">
                            <button class="banner_subb srch rtyy">@lang('client_site.filter')</button>
                        </div>
                    </div>
                </form>
            @if(sizeof(@$booking)>0)
                <div class="buyer_table">
                    <div class="table-responsive">
                        <div class="table for_btn_nn60">
                            <div class="one_row1 hidden-sm-down only_shawo">
                                <div class="cell1 tab_head_sheet">@lang('client_site.id')</div>
                                <!-- <div class="cell1 tab_head_sheet">@lang('client_site.professional')</div> -->
                                <div class="cell1 tab_head_sheet">@lang('client_site.date_time')</div>
                                <div class="cell1 tab_head_sheet">@lang('client_site.duration')</div>
                                <div class="cell1 tab_head_sheet">@lang('client_site.topic')</div>
                                <div class="cell1 tab_head_sheet">{{__('site.payment_type_1')}}<br>{{__('site.payment_type_2')}}</div>
                                <div class="cell1 tab_head_sheet">{{__('site.payment_status_1')}}<br>{{__('site.payment_status_2')}}</div>
                                <div class="cell1 tab_head_sheet">@lang('client_site.status')</div>
                                <div class="cell1 tab_head_sheet">@lang('client_site.action')</div>
                            </div>
                            <!--row 1-->
                                {{-- @dd($booking) --}}
                                @foreach(@$booking as $bk)
                                    @if(@$bk->order_status == 'AA' || @$bk->order_status == 'R')
                                        <div class="one_row1 small_screen31">
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('client_site.id')</span>
                                                <p class="add_ttrr">{{ @$bk->id }}</p>
                                            </div>
                                            <!-- <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('client_site.professional')</span>
                                                <p class="add_ttrr">{{ @$bk->userDetails->name }}</p>
                                            </div> -->
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('client_site.date_time')</span>
                                                <p class="add_ttrr">{{ @$bk->date }}, {{ date('H:i a' ,strtotime(@$bk->start_time)) }}</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('client_site.duration')</span>
                                                <p class="add_ttrr">{{ @$bk->duration }} @lang('client_site.minutes')</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('client_site.topic')</span>
                                                <p class="add_ttrr">{{ @$bk->parentCatDetails->name }} <br>{{ @$bk->childCatDetails->name }}</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">{{__('site.payment_type')}}</span>
                                                <p class="add_ttrr">
                                                    @if(@$bk->payment_type == 'C') {{__('site.payment_method_card')}}
                                                    @elseif(@$bk->payment_type == 'BA') {{__('site.payment_method_bank')}}
                                                    @elseif(@$bk->payment_type == 'P') Paypal
                                                    @elseif(@$bk->payment_type == 'S') Stripe
                                                    @elseif(@$bk->amount == 0){{__('site.free_session')}}
                                                    @elseif(@$bk->wallet == @$bk->sub_total)  {{__('site.wallet')}}
                                                    @endif
                                                </p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('client_site.payment_status')</span>
                                                <p class="add_ttrr">
                                                    @if(@$bk->payment_status == 'I') {{__('site.Payment_initiated')}} @elseif(@$bk->payment_status == 'P')
                                                    {{__('site.Payment_paid')}}
                                                    @elseif(@$bk->payment_status == 'F') {{__('site.Payment_failed')}} @elseif(@$bk->payment_status == 'PR')
                                                    {{__('site.Payment_processing')}}
                                                    @endif
                                                </p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('client_site.status')</span>
                                                <p class="add_ttrr">
                                                    @if(@$bk->order_status=="AA")
                                                        @lang('client_site.awaiting_approval')
                                                    @else
                                                        @lang('client_site.reject')
                                                    @endif

                                                </p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                                <span class="W55_1">@lang('client_site.action')</span>
                                                @if(@$bk->order_status=="R")
                                                    <strong>@lang('client_site.rejected_by')
                                                        @if(@$bk->order_cancelled_by=="U")
                                                        {{ @$bk->userDetails->nick_name ? @$bk->userDetails->nick_name : @$bk->userDetails->name }}
                                                        @else
                                                            {{ @Auth::guard('web')->user()->nick_name ? @Auth::guard('web')->user()->nick_name : @Auth::guard('web')->user()->name }}
                                                        @endif</strong>
                                                @elseif(@$bk->order_status=="AA")
                                                    <a href="{{ route('prof.acc.req', ['token'=>@$bk->token_no]) }}" class="acpt">@lang('client_site.accept')</a>

                                                    <a href="{{ route('prof.my.cancel.req',['token'=>@$bk->token_no]) }}" class="rjct">@lang('client_site.reject') </a>
                                                @endif
                                            </div>
                                        </div>
                                    @elseif(@$bk->order_status == 'A' && @$key['type'] == 'UC')
                                    <div class="one_row1 small_screen31">
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('client_site.id')</span>
                                                <p class="add_ttrr">{{ @$bk->id }}</p>
                                            </div>
                                           <!--  <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('client_site.professional')</span>
                                                <p class="add_ttrr">{{ @$bk->userDetails->name }}</p>
                                            </div> -->
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('client_site.date_time')</span>
                                                <p class="add_ttrr">{{ @$bk->date}}, {{ toUserTime(@$bk->start_time,'H:i a') }}</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('client_site.duration')</span>
                                                <p class="add_ttrr">{{ @$bk->duration }} @lang('client_site.minutes')</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('client_site.topic')</span>
                                                <p class="add_ttrr">{{ @$bk->parentCatDetails->name }} <br>{{ @$bk->childCatDetails->name }}</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">{{__('site.payment_type')}}</span>
                                                <p class="add_ttrr">
                                                    @if(@$bk->payment_type == 'C') {{__('site.payment_method_card')}}
                                                    @elseif(@$bk->payment_type == 'BA') {{__('site.payment_method_bank')}}
                                                    @elseif(@$bk->payment_type == 'P') Paypal
                                                    @elseif(@$bk->payment_type == 'S') Stripe
                                                    @elseif(@$bk->amount == 0) {{__('site.free_session')}}
                                                    @elseif(@$bk->wallet == @$bk->sub_total)  {{__('site.wallet')}}
                                                    @endif
                                                </p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('client_site.payment_status')</span>
                                                <p class="add_ttrr">
                                                    @if(@$bk->payment_status == 'I') {{__('site.Payment_initiated')}} @elseif(@$bk->payment_status == 'P')
                                                    {{__('site.Payment_paid')}}
                                                    @elseif(@$bk->payment_status == 'F') {{__('site.Payment_failed')}} @elseif(@$bk->payment_status == 'PR')
                                                    {{__('site.Payment_processing')}}
                                                    @endif
                                                </p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('client_site.status')</span>
                                                <p class="add_ttrr">@lang('client_site.approved')</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                                <span class="W55_1">@lang('client_site.action')</span>
                                                    <a href="{{ route('view.booked.user.details', ['id' => $bk->id]) }}" class="rjct">@lang('client_site.view') </a>

                                                    @if(auth()->user()->sell != 'PS')
                                                        <a href="{{ route('compose.msg',['id'=>@$bk->user_id]) }}" class="rjct">@lang('client_site.message') </a>
                                                        @if(@$bk->payment_status == 'P' && @$bk->booking_type=="C")
                                                            @if(@$bk->getMsgMaster)
                                                                @if(@$bk->getMsgMaster->status == 'OPEN')
                                                                    <a href="javascript:;" class="rjct chat_btn" data-bkid="{{@$bk->id}}" data-token="{{@$bk->token_no}}" data-userid="{{@$bk->userDetails->id}}" data-username="{{@$bk->userDetails->nick_name ? @$bk->userDetails->nick_name : @$bk->userDetails->name}}">@lang('client_site.chat') </a>
                                                                @endif
                                                            @else
                                                                <a href="javascript:;" class="rjct chat_btn" data-bkid="{{@$bk->id}}" data-token="{{@$bk->token_no}}" data-userid="{{@$bk->userDetails->id}}" data-username="{{@$bk->userDetails->nick_name ? @$bk->userDetails->nick_name : @$bk->userDetails->name}}">@lang('client_site.chat') </a>
                                                            @endif
                                                        @endif
                                                        @if(@$bk->booking_type=="V")
                                                            @if(@$bk->video_status=="I" && @$bk->payment_status == 'P')
                                                                <button class="rjct videoCallStart" style="border: none;" data-token="{{ @$bk->token_no }}" data-id="{{ @$bk->user_id }}" data-dur="{{ @$bk->duration-@$bk->completed_call }}" data-completed_call="{{ @$bk->completed_call }}" type="button">@lang('client_site.video_call') </button>
                                                            @endif
                                                        @endif
                                                    @endif
                                                    @php
                                                    $startTime =date('Y-m-d H:i:s', strtotime(@$bk->date.@$bk->start_time));
                                                    @endphp
                                                    @if((toUTCTime(date('Y-m-d H:i:s', strtotime("+24 hours"))) < $startTime) && @$bk->order_status == 'A' && $bk->payment_status=='P')
                                                    {{-- <a href="{{ route('booking.cancel', ['token' => @$bk->token_no,'id'=>@$bk->id]) }}" class="rjct" onclick="return confirm(`@lang('client_site.cancel_order')`)">booking cancel</a> --}}
                                                    <a href="javascript:;" class="rjct cancelbtn" data-token="{{@$bk->token_no}}" data-id="{{@$bk->id}}">{{__('site.booking_cancel')}}</a>
                                                    @endif
                                                    {{-- <a href="{{ route('booking.cancel', ['token' => @$bk->token_no,'id'=>@$bk->id]) }}" class="rjct">booking cancel</a> --}}
                                            </div>
                                        </div>
                                        @elseif(@$bk->order_status == 'C' && @$key['type'] == 'UC')
                                    <div class="one_row1 small_screen31">
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('client_site.id')</span>
                                                <p class="add_ttrr">{{ @$bk->id }}</p>
                                            </div>
                                           <!--  <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('client_site.professional')</span>
                                                <p class="add_ttrr">{{ @$bk->userDetails->name }}</p>
                                            </div> -->
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('client_site.date_time')</span>
                                                <p class="add_ttrr">{{ @$bk->date}}, {{ toUserTime(@$bk->start_time,'H:i a') }}</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('client_site.duration')</span>
                                                <p class="add_ttrr">{{ @$bk->duration }} @lang('client_site.minutes')</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('client_site.topic')</span>
                                                <p class="add_ttrr">{{ @$bk->parentCatDetails->name }} <br>{{ @$bk->childCatDetails->name }}</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">{{__('site.payment_type')}}</span>
                                                <p class="add_ttrr">
                                                    @if(@$bk->payment_type == 'C') {{__('site.payment_method_card')}}
                                                    @elseif(@$bk->payment_type == 'BA') {{__('site.payment_method_bank')}}
                                                    @elseif(@$bk->payment_type == 'S') Stripe
                                                    @elseif(@$bk->payment_type == 'P') Paypal
                                                    @elseif(@$bk->amount == 0) {{__('site.free_session')}}
                                                    @elseif(@$bk->wallet == @$bk->sub_total)  {{__('site.wallet')}}
                                                    @endif
                                                </p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('client_site.payment_status')</span>
                                                <p class="add_ttrr">
                                                    @if(@$bk->payment_status == 'I') {{__('site.Payment_initiated')}} @elseif(@$bk->payment_status == 'P')
                                                    {{__('site.Payment_paid')}}
                                                    @elseif(@$bk->payment_status == 'F') {{__('site.Payment_failed')}} @elseif(@$bk->payment_status == 'PR')
                                                    {{__('site.Payment_processing')}}
                                                    @endif
                                                </p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('client_site.status')</span>
                                                @if(@$bk->order_status == 'C')
                                                <p class="add_ttrr">@lang('site.order_cancel')</p>
                                                @endif
                                            </div>
                                            <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                                <span class="W55_1">@lang('client_site.action')</span>
                                                    <a href="{{ route('view.booked.user.details', ['id' => $bk->id]) }}" class="rjct">@lang('client_site.view') </a>
                                            </div>
                                        </div>

                                    @elseif(@$bk->video_status == 'C' && @$key['type'] == 'PC')
                                    <div class="one_row1 small_screen31">
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('client_site.id')</span>
                                                <p class="add_ttrr">{{ @$bk->id }}</p>
                                            </div>
                                            <!-- <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('client_site.professional')</span>
                                                <p class="add_ttrr">{{ @$bk->userDetails->name }}</p>
                                            </div> -->
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('client_site.date_time')</span>
                                                <p class="add_ttrr">{{ @$bk->date }}, {{ date('H:i a' ,strtotime(@$bk->start_time)) }}</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('client_site.duration')</span>
                                                <p class="add_ttrr">{{ @$bk->duration }} @lang('client_site.minutes')</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('client_site.topic')</span>
                                                <p class="add_ttrr">{{ @$bk->parentCatDetails->name }} <br>{{ @$bk->childCatDetails->name }}</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">{{__('site.payment_type')}}</span>
                                                <p class="add_ttrr">
                                                    @if(@$bk->payment_type == 'C') {{__('site.payment_method_card')}}
                                                    @elseif(@$bk->payment_type == 'BA') {{__('site.payment_method_bank')}}
                                                    @elseif(@$bk->payment_type == 'S') Stripe
                                                    @elseif(@$bk->payment_type == 'P') Paypal
                                                    @elseif(@$bk->amount == 0) {{__('site.free_session')}}
                                                    @elseif(@$bk->wallet == @$bk->sub_total)  {{__('site.wallet')}}
                                                    @endif
                                                </p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('client_site.payment_status')</span>
                                                <p class="add_ttrr">
                                                    @if(@$bk->payment_status == 'I') {{__('site.Payment_initiated')}} @elseif(@$bk->payment_status == 'P')
                                                    {{__('site.Payment_paid')}}
                                                    @elseif(@$bk->payment_status == 'F') {{__('site.Payment_failed')}} @elseif(@$bk->payment_status == 'PR')
                                                    {{__('site.Payment_processing')}}
                                                    @endif
                                                </p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('client_site.status')</span>
                                                <p class="add_ttrr">@lang('client_site.completed')</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                                <span class="W55_1">@lang('client_site.action')</span>
                                                    <a href="{{route('view.booked.user.details',['id'=>@$bk->id])}}" class="rjct">@lang('client_site.view') </a>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                        </div>
                    </div>
                </div>
            @else
                <center>
                    <h3 class="error">@lang('client_site.booking_details_not_found')</h3>
                </center>
            @endif
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalcancel">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
              <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title">{{__('site.booking_cancel')}} </h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form name="myForm3" id="myForm3" method="post">
                    @csrf
                    <input type="hidden" name="token_no" id="token_no">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group" style="width: 100%;">
                                    <label>{{__('site.cancellation_reason')}}</label>
                                    <input type="text" class="required form-control" name="reason" value="" >
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <input type="submit" value="Cancel" class="btn btn-primary">
                                </div>
                            </div>
                      </div>
                    </div>
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('site.close')</button>
            </div>

          </div>
        </div>
  </div>
</section>

@endsection
@section('footer')
@include('includes.footer')
    <script>

    function setVl(tn){
        $('#token_no').val(tn);
    }
    $('#myForm2').validate();
    $('.timepicker').timepicker({
        timeFormat: 'H:mm',
        interval: 15,
        minTime: '00',
        maxTime: '11:00pm',
        startTime: '09:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
    $(function() {
        // $('.parent_loader').show();
        $("#date").datepicker({dateFormat: "dd-mm-yy",
             monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho',
            'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],

            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],

           defaultDate: new Date(),
           onClose: function( selectedDate ) {

          }
        });

        $("#datepicker").datepicker({dateFormat: "dd-mm-yy",
             monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho',
            'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],

           defaultDate: new Date(),
           onClose: function( selectedDate ) {
           $( "#datepicker1").datepicker( "option", "minDate", selectedDate );
          }
        });
    $("#datepicker1").datepicker({dateFormat: "dd-mm-yy",
         monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho',
            'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
     defaultDate: new Date(),
          onClose: function( selectedDate ) {
          $( "#datepicker" ).datepicker( "option", "maxDate", selectedDate );
        }
     });

    });
</script>
<script>
    $('#srch').click(function(){
        $('#myForm').submit();
    });
    $('.sbmt').click(function(){
        if($('#date').val()!="" && $('#time').val()!=""){
            $('.parent_loader').show();
            setTimeout(function(){ $('#myForm2').submit(); }, 3000);
        }
        else{
             $('#myForm2').submit();
        }

    });
    $(document).ready(function(){
        $('#myForm').validate({
            rules:{
                phone_no:{
                    digits:true,
                    maxlength:10,
                    minlength:10
                }
            }
        });

        $('#myForm1').validate({
            rules:{
                old_password:{
                    required:true
                },
                new_password:{
                    required:true,
                    minlength:8,
                    maxlength:8
                },
                confirm_password:{
                    required:true,
                    minlength:8,
                    maxlength:8,
                    equalTo:"#new_password"
                }
            }
        });
        $('#myForm3').validate({});
    });

    $('#phone_no').change(function(){
        if($('#phone_no').val()!=""){
            var reqData = {
              'jsonrpc' : '2.0',
              '_token' : '{{csrf_token()}}',
              'params' : {
                    'no' : $('#phone_no').val()
                }
            };
            $.ajax({
                url: "{{ route('chk.mobile') }}",
                method: 'post',
                dataType: 'json',
                data: reqData,
                success: function(response){
                    if(response.status==1) {
                        $('#phone_no').val("");
                        $('#ph_error').text('@lang('client_site.phone_number_allready_exist')');
                    }

                }, error: function(error) {
                    toastr.info('@lang('client_site.Try_again_after_sometime')');
                }
            });
        }
    });
    function copyLink(text){
        var input = document.createElement('input');
        input.setAttribute('value', text);
        document.body.appendChild(input);
        input.select();
        var result = document.execCommand('copy');
        document.body.removeChild(input);
        toastr.success("Link Copied");
        return result;
    }
    $('.cancelbtn').click(function(){
        var id= $(this).data('id');
        var token= $(this).data('token');
        $('#modalcancel').modal('show');
        var url= '{{route('home')}}/booking-cancel/'+token+'/'+id;
        $('#myForm3').attr('action', url)
    });
</script>

@endsection
