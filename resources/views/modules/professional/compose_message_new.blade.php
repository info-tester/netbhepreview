@extends('layouts.app')
@section('title')
@lang('site.send') @lang('site.message')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')
<div class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('client_site.message_compose')</h2>
    </div>
</div>
<div class="all_logo_area message_compose">
    <div class="container">
        <div class="row">
            <div class="message_tab">
                <div class="col-md-12">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link " href="{{ route('message') }}">@lang('client_site.messagebox')</a>
                        </li>
                        {{-- <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#Sent">Sent (215)</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#Trash">Trash (5)</a>
                        </li> --}}
                    </ul>
                    {{--  <a class="compose_btn" href="#"><i class="fa fa-pencil" aria-hidden="true"></i> compose</a> --}}
                    <div class="card msgcnt">
                        <!-- Tab panes -->

                        <div class="tab-content msg_list_pa">
                            <div role="tabpanel" class="tab-pane active" id="Inbox">
                                <div class="inbox_msg">
                                    <form id="user_chat_form" name="user_chat_form" method="post">

                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <div class="form-group">
                                                    <label class="personal-label">@lang('client_site.name')</label>
                                                    <select class="personal-type personal-select" name="recipents" id="recipents" @if(@$user)disabled @endif >
                                                        <option value="">
                                                            @lang('client_site.select_your_recipents')
                                                        </option>
                                                        @if(@$user)
                                                        <option value="{{@$user->id}}" selected>{{@$user->nick_name ? @$user->nick_name : @$user->name}}</option>
                                                        @endif

                                                        @foreach(@$recipents as $rr)
                                                        <option value="{{@$rr->userDetails->id}}" >
                                                            {{ @$rr->userDetails->nick_name ? @$rr->userDetails->nick_name : (@$rr->userDetails->name ?? '') }}
                                                        </option>
                                                        @endforeach
                                                        @foreach (@$userlist as $userlists)
                                                        <option value="{{@$userlists->profDetails->id}}">
                                                            {{@$userlists->profDetails->nick_name ? @$userlists->profDetails->nick_name : @$userlists->profDetails->name}}
                                                        </option>  
                                                        @endforeach
                                                        @foreach (@$allReceiver as $allReceivers)
                                                        <option value="{{@$allReceivers->senderDetails->id}}">
                                                            {{@$allReceivers->senderDetails->nick_name ? @$allReceivers->senderDetails->nick_name : @$allReceivers->senderDetails->name}}
                                                        </option>  
                                                        @endforeach
                                                    </select>
                                                    <label id="nameerr" class="error"></label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>

                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div class="form-group">
                                                    <!--<label class="personal-label">About Me</label>-->
                                                    <textarea class="emltxtarea" placeholder="" id="msg"
                                                        name="msg"></textarea>
                                                    <label id="msgerr" class="error"></label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="attach_file">
                                                    <label class="myLabel">
                                                        <input name="chat_attachment" id="chat_attachment" type="file">
                                                        <span><i class="fa fa-paperclip" aria-hidden="true"></i>
                                                            @lang('client_site.attach_files')</span>
                                                    </label>

                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <button type="button"
                                                    class="login_submitt sndms">@lang('client_site.send_message')</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
</div>
@endsection
@section('footer')
@include('includes.footer')
@endsection