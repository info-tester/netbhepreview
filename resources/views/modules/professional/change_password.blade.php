@extends('layouts.app')
@section('title')
  @lang('site.change_password')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('client_site.change_password')</h2>
        <div class="bokcntnt-bdy">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile'), <a href="{{ route('professional_qualification') }}">@lang('client_site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">
                    <form id="myForm1" name="myForm1" method="post" action="{{ route('my.profile') }}">
                        @csrf
                        <div class="form_body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4>@lang('client_site.change_password')</h4>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.old_password')</label>
                                        <input type="Password" name="old_password" id="old_password" placeholder="@lang('site.enter_old_password')" class="personal-type">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.new_password')</label>
                                        <input type="Password" placeholder="@lang('site.enter_new_password')" id="new_password" name="new_password" class="personal-type">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.confirm_password')</label>
                                        <input type="Password" id="confirm_password" placeholder="@lang('site.enter_confirm_password')" name="confirm_password" class="personal-type">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="login_submitt">@lang('client_site.change_password')</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')

<script>
    $(document).ready(function(){
        $(".chosen-select").chosen();
        $('#myForm1').validate({
            rules:{
                old_password:{
                    required:true
                },
                new_password:{
                    required:true,
                    minlength:8,
                    // maxlength:8
                },
                confirm_password:{
                    required:true,
                    minlength:8,
                    // maxlength:8,
                    equalTo:"#new_password"
                }
            }
        });
    });
</script>
@endsection