@extends('layouts.app')

@section('title')

  @lang('site.my_account')

@endsection

@section('style')

@include('includes.style')

@endsection

@section('scripts')

@include('includes.scripts')

<link rel="stylesheet" href="{{URL::to('public/css/chosen.css')}}">

<style>

    .email_error{

        color: red !important;

    }

     .tooltip {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted #ccc;
        color: #006080;
        opacity:1 !important;
    }
    .tooltip .tooltiptext {
        visibility: hidden;
        position: absolute;
        width: 250px;
        background-color: #555;
        color: #fff;
        text-align: center;
        padding: 5px;
        border-radius: 6px;
        z-index: 1;
        opacity: 0;
        transition: opacity 0.3s;
        margin-left:10px
    }
    .tooltip-right {
        top: 0px;
        left: 125%;
    }
    .tooltip-right::after {
        content: "";
        position: absolute;
        top: 10%;
        right: 100%;
        margin-top: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: transparent #555 transparent transparent;
    }
    .tooltip:hover .tooltiptext {
        visibility: visible;
        opacity: 1;
    }
    .tooltip_img i{
        margin-left: 10px !important;
        margin-top: 20px !important;
    }
    .tooltip_img .tooltiptext{
        margin-top: 15px !important;
    }

</style>

<style type="text/css">

    .agreement-sec {

        height: 50vh;

        overflow-y: scroll;

        border: 1px solid #ced4da;

        padding: 15px;

    }



    .signCanv {

        border: 1px solid #ced4da;

        width: 300px;

        height: 160px;

        float: left;

    }



    .clearButton,

    .submitButton {

        float: left;

        margin-top: 10px;

    }



    .clearButton {

        margin-right: 10px;

    }



    .clearButton a {

        color: #fff;

    }



    p.error {

        background: #fff;

        padding: 0;

        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol" !important;

        font-size: 1rem !important;

        font-weight: 400 !important;

        line-height: 1.5 !important;

    }



    @media (max-width: 480px) {

        .signCanv {

            width: 250px;

        }

    }

</style>

    <link href="{{ URL::to('public/frontend/css/calender.css') }}" rel="stylesheet" type="text/css">

    <script src="{{ URL::to('public/frontend/js/jquery-ui.js') }}"></script>

    <script>

    $(function() {

            $("#datepicker11").datepicker({dateFormat: "dd-mm-yy",



                 monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho',

            'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],

            monthNamesShort: [ "Janeiro", "Fevereiro", "Março", "Abril",

                   "Maio", "Junho", "Julho", "Agosto", "Setembro",

                   "Outubro", "Novembro", "Dezembro" ],



            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],

            dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],

            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],



                defaultDate: new Date(),

                changeMonth: true,

                changeYear: true,

                showButtonPanel: true,

                showMonthAfterYear: true,

                showWeek: true,

                showAnim: "drop",

                constrainInput: true,

                yearRange: "-90:",

                maxDate:new Date(),

                onClose: function( selectedDate ) {

                    //$( "#datepicker1").datepicker( "option", "minDate", selectedDate );

                }

            });



        });



    </script>

@endsection

@section('header')



@include('includes.professional_header')

@endsection

@section('content')



<section class="bkng-hstrybdy">

    <div class="container">

        <h2>Video de Apresentação</h2>

        <div class="bokcntnt-bdy">

            @php

                $user = Auth::guard('web')->user();

                $user = $user->load('userQualification');

            @endphp

            @if(sizeof($user->userQualification)<=0)

                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile'), <a href="{{ route('professional_qualification') }}">@lang('client_site.click_here')</a></p></center>

            @endif

            <div class="mobile_filter">

                <i class="fa fa-bars" aria-hidden="true"></i>

                <p>@lang('client_site.menu')</p>

            </div>

            @include('includes.professional_sidebar')

            <div class="dshbrd-rghtcntn">

                <div class="dash_form_box">

                    <form action="{{route('professional.profile.introductory.video.submit')}}" method="post" name="myForm" id="myForm" enctype="multipart/form-data">

                                @csrf

                        <div class="form_body">



                            <div class="row">

                                <div class="col-lg-12 col-md-12 col-sm-12">

                                    <div class="form-group">

                                        <label class="personal-label">Vídeo de apresentação
                                            <small class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">@lang('site.tooltip_video_introductory')</span>
                                            </small>

                                        </label>

                                        <input type="radio" class="introductory_video" id="youtube" name="introductory_video" value="Y"> Vídeo YouTube

                                        <input type="radio" class="introductory_video" id="video" name="introductory_video" value="V"> Video File

                                        <input type="radio" class="introductory_video" id="audio" name="introductory_video" value="A"> Áudio file

                                    </div>

                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 into" id="youtube_file" style="display: none">

                                    <label class="personal-label">@lang('site.introductory_video') [Adicione o link URL do seu vídeo do Youtube.]</label>

                                    <div class="form-group w-50">

                                        <input type="url" name="intro_video" id="intro_video" class="personal-type"

                                            placeholder="Embed an introductory video url">

                                        <label class="intro_video_error error error1" style="color: red"></label>

                                    </div>

                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 into" id="video_file" style="display: none">

                                    <div class="form-group" style="margin-top: 22px; overflow: hidden;">

                                        <label class="personal-label">Video [Adicionar somente Mp4 e Tam  Max 20 MB]</label>

                                        <input type="file" class="personal-type required" id="product_file" placeholder="File" name="video_file">

                                        <label id="product_file_error" style="color:red;"></label>

                                    </div>

                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 into" id="audio_file" style="display: none">

                                    <div class="form-group" style="margin-top: 22px; overflow: hidden;">

                                        <label class="personal-label">Áudio [Adicionar somente Mp3 e Tam  Max 20 MB]</label>

                                        <input type="file" class="personal-type" id="product_file2" placeholder="File" name="audio_file">

                                        <label id="product_file_error" class="" style="color:red;"></label>

                                    </div>

                                </div>

                                {{-- <input type="radio" id="male" name="gender" value="male">

                                <label for="male">Male</label><br>

                                <input type="radio" id="female" name="gender" value="female">

                                <label for="female">Female</label><br>

                                <input type="radio" id="other" name="gender" value="other">

                                <label for="other">Other</label> --}}







                                <div class="col-sm-12">

                                    <button type="submit" class="login_submitt" id="update">@lang('client_site.save')</button>

                                </div>





                            </div>

                        </div>

                    </form>

                    </div>

                    @if($user->introductory_video_type=='Y')

                    <label class="personal-label">@lang('site.introductory_video')</label>

                    <iframe width="420" height="315" src="https://www.youtube.com/embed/{{$user->introductory_video}}">

                    </iframe>

                    @elseif($user->introductory_video_type=='V')

                    <label class="personal-label">@lang('site.introductory_video')</label>

                        <video width="420" height="315" controls>

                            <source src="{{ URL::to('storage/app/public/uploads/introductory_video').'/'.$user->introductory_video }}" type="video/mp4">

                            Your browser does not support the video tag.

                        </video>

                    @elseif($user->introductory_video_type=='A')

                    <label class="personal-label">Introductory Audio</label>

                        <audio width="420" height="315" controls>

                            <source src="{{ URL::to('storage/app/public/uploads/introductory_video').'/'.$user->introductory_video }}" type="audio/mpeg">

                            Your browser does not support the Audio tag.

                        </audio>

                    @endif

                    @if(@$user->introductory_video==null)

                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">

                        <center>

                            <h2 style="background: none !important"><span class="error">Nenhum vídeo adicionado</span></h2>

                        </center>

                    </div>

                    @endif



            </div>

        </div>

    </div>

</section>

<div class="modal fade" id="crop-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"

    aria-hidden="true">

    <div class="modal-dialog" role="document">

        <div class="modal-content">

            {{-- <div class="modal-header">

                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                    <span aria-hidden="true">&times;</span>

                </button>

            </div> --}}

            <div class="modal-body">

                <div class="upload-demo" style="width: 100%;">

                </div>

            </div>

            <div class="modal-footer">

                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>

                <button type="button" class="btn btn-primary crop-result">@lang('client_site.save')</button>

            </div>

        </div>

    </div>

</div>

@endsection

@section('footer')

    @include('includes.footer')

    <link rel="stylesheet" href="{{ URL::to('public/frontend/css/croppie.min.css') }}">

    <style type="text/css">

        .upldd {

            display: block;

            width: auto;

            border-radius: 4px;

            text-align: center;

            background: #9caca9;

            cursor: pointer;

            overflow: hidden;

            padding: 10px 15px;

            font-size: 15px;

            color: #fff;

            cursor: pointer;

            float: left;

            margin-top: 15px;

            font-family: 'Poppins', sans-serif;

            position: relative;

        }

        .upldd input {

            position: absolute;

            font-size: 50px;

            opacity: 0;

            left: 0;

            top: 0;

            width: 200px;

            cursor: pointer;

        }

        .upldd:hover{

            background: #1781d2;

        }

    </style>



    <script src="{{ URL::to('public/frontend/js/bank-account-validator.min.js') }}"></script>

    <script src="{{ URL::to('public/frontend/js/croppie.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('public/frontend/js/numeric-1.2.6.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('public/frontend/js/bezier.js') }}"></script>

    <script type="text/javascript" src="{{ asset('public/frontend/js/jquery.signaturepad.js') }}"></script>

    <script type="text/javascript" src="{{ asset('public/frontend/js/html2canvas.js') }}"></script>

    <script type="text/javascript" src="{{ asset('public/frontend/js/json2.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('public/frontend/js/jquery-ui.min.js') }}"></script>

    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.js"></script>

    <script>

        $(document).ready(function() {

            $('#youtube').click(function(){

                console.log($(this).val());

                $('.into').css('display', 'none');

                $('#youtube_file').css('display', 'block');

            })

            $('#video').click(function(){

                console.log($(this).val());

                $('.into').css('display', 'none');

                $('#video_file').css('display', 'block');

            })

            $('#audio').click(function(){

                console.log($(this).val());

                console.log($('input[name="introductory_video"]:checked').val());

                $('.into').css('display', 'none');

                $('#audio_file').css('display', 'block');

            })

        });

        function ytVidId(url) {

        var p = /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;

        return (url.match(p)) ? RegExp.$1 : false;

    }



    $('#intro_video').bind("change", function() {

        var url = $(this).val();

        if (ytVidId(url) !== false) {

            $('.intro_video_error').html('');

        } else {

            $('#intro_video').val('');
            validator.resetForm();
            $(".intro_video_error").html('Link do youtube inválido.');
            $(".intro_video_error").show();

        }

    });

    $.validator.addMethod('filesize', function (value, element, arg) {

        if(element.files[0].size<=arg){

            return true;

        }else{

            return false;

        }

    });

    var validator = $('#myForm').validate({

            rules: {

                introductory_video:"required",

                intro_video:{

                    required: function(){

                       if($('input[name="introductory_video"]:checked').val()=='Y'){

                           return true

                       }else{

                           return false

                       }

                   },

                },

                video_file:{

                    required: function(){

                       if($('input[name="introductory_video"]:checked').val()=='V'){

                           return true

                       }else{

                           return false

                       }

                   },

                   extension: "mpeg|mp4",

                   filesize: 20971520,

                },

                audio_file:{

                    required: function(){

                       if($('input[name="introductory_video"]:checked').val()=='A'){

                           return true

                       }else{

                           return false

                       }

                   },

                   extension: "mp3",

                   filesize: 20971520,

                },

            },

            errorPlacement: function(error, element)

            {

                if ( element.is(":radio") )

                {

                    error.appendTo( element.parents('.form-group') );

                }

                else

                { // This is the default behavior

                    error.insertAfter( element );

                }

            },

            messages: {

            video_file:{

                filesize:" file size must be less than 20 MB.",

                extension:"Envie apenas arquivo de vídeo .mp4.",

            },

            audio_file:{

                filesize:" file size must be less than 20 MB.",

                extension:"Faça upload apenas de arquivos de áudio .mp3.",

            }

        },

        submitHandler: function(form) {

            form.submit();

        }

        });

    </script>







@endsection

