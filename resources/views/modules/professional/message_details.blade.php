@extends('layouts.app')
@section('title')
  @lang('client_site.message_details')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')
    <div class="bkng-hstrybdy">
        <div class="container">
            <h2>@lang('client_site.message_details')</h2>
        </div>
    </div>
    <div class="all_logo_area message_compose">
        <div class="container">
            <div class="row">
                <div class="message_tab">
                    <div class="col-md-12">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('message') }}">@lang('client_site.chatbox')</a>
                            </li>
                            {{-- <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#Sent">Sent (215)</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#Trash">Trash (5)</a>
                            </li> --}}
                        </ul>
                       {{--  <a class="compose_btn" href="{{ route('compose') }}"><i class="fa fa-pencil" aria-hidden="true"></i> compose</a> --}}
                        <div class="card msgcnt">
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="Inbox">
                                    <div class="inbox_msg">
                                        @foreach(@$msg as $ms)
                                        @if(@$ms->receiver_id==@Auth::guard('web')->user()->id)
                                            <div class="one_msg">
                                                <div class="msg_image">
                                                   {{--  <img src="{{ URL::to('public/frontend/images/msg_1.png') }}" alt=""> --}}
                                                    <img src="{{ @$ms->receiverDetails->profile_pic ? URL::to('storage/app/public/uploads/profile_pic').'/'.@$ms->receiverDetails->profile_pic: URL::to('public/frontend/images/no_img.png')}}">
                                                </div>
                                                <div class="msg_details">
                                                    <div class="top_msg">
                                                        <p>{{ @$ms->receiverDetails->nick_name ? @$ms->receiverDetails->nick_name : @$ms->receiverDetails->name }}</p>
                                                        <div class="right-options">
                                                            <div class="mail_date">
                                                                <img src="{{URL::to('public/frontend/images/mail_calendar.png')}}" alt=""> {{ date('d-m-Y h:i a', strtotime(@$ms->created_at)) }}
                                                            </div>
                                                            @if($ms->is_reply==null)
                                                            <ul>

                                                                <li class="reply_msg">
                                                                    <a href="{{route('user.reply',['id'=>@$ms->id])}}">
                                                                    <i class="fa fa-reply" aria-hidden="true"></i>
                                                                    @lang('client_site.reply')
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="below_msg">
                                                            {!! @$ms->msg !!}
                                                        @if(@$ms->attachment!=null || @$ms->attachment!="")
                                                            <div class="last_doc">
                                                                <p> <a href="{{URL::to('storage/app/public/uploads/chat_attachment').'/'.@$ms->attachment}}" download><img src="{{ URL::to('public/frontend/images/mail_icon_2.png') }}" alt=""> {{ @$ms->attachment }}</a></p>
                                                            </div>
                                                        @endif
                                                        @if(@$ms->is_reply!=null)
                                                            <div class="last_doc">
                                                                <div class="below_msg">
                                                                    <p> You Replied: <strong>{!! @$ms->is_reply!!}</strong></p>
                                                                    @if(@$ms->is_reply_attachment!=null)
                                                                        <div class="last_doc">
                                                                            <p> <a href="{{URL::to('storage/app/public/uploads/chat_attachment').'/'.@$ms->is_reply_attachment}}" download><img src="{{ URL::to('public/frontend/images/mail_icon_2.png') }}" alt=""> {{ @$ms->is_reply_attachment }}</a></p>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        @if(@$ms->sender_id==@Auth::guard('web')->user()->id)
                                            <div class="one_msg">
                                                <div class="msg_image">
                                                    <img src="{{ @$ms->profile_pic ? URL::to('storage/app/public/uploads/profile_pic').'/'.@$ms->profile_pic: URL::to('public/frontend/images/no_img.png')}}">
                                                </div>
                                                <div class="msg_details">
                                                    <div class="top_msg">
                                                        <p>{{ @Auth::guard('web')->user()->nick_name ? @Auth::guard('web')->user()->nick_name : @Auth::guard('web')->user()->name }}</p>
                                                        <div class="right-options right-options1">
                                                            <div class="mail_date">
                                                                <img src="{{ URL::to('public/frontend/images/mail_calendar.png') }}" alt=""> {{ date('d-m-Y h:i a', strtotime(@$ms->created_at)) }}
                                                            </div>
                                                            <ul>
                                                                {{-- <li>
                                                                    <img src="{{ URL::to('public/frontend/images/mail_icon_1.png') }}" alt="">
                                                                </li>
                                                                <li>
                                                                    <img src="{{ URL::to('public/frontend/images/mail_icon_2.png') }}" alt="">
                                                                </li> --}}
                                                                {{-- <li>
                                                                    <a href="#">
                                                                    <img src="{{ URL::to('public/frontend/images/mail_icon_3.png') }}" alt="">
                                                                    </a>
                                                                </li> --}}
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="below_msg">
                                                            {!! @$ms->msg !!}
                                                        @if(@$ms->is_reply!=null)
                                                            <div class="last_doc">
                                                                <div class="below_msg">
                                                                    <p> @lang('client_site.you_replied') {!! @$ms->is_reply!!}</p>
                                                                    @if(@$ms->is_reply_attachment!=null)
                                                                        <div class="last_doc">
                                                                            <p> <a href="{{URL::to('storage/app/public/uploads/chat_attachment').'/'.@$ms->is_reply_attachment}}" download><img src="{{ URL::to('public/frontend/images/mail_icon_2.png') }}" alt=""> {{ @$ms->is_reply_attachment }}</a></p>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </div>

                                                    <div class="below_msg">
                                                            
                                                        @if(@$ms->attachment!=null || @$ms->attachment!="")
                                                            <div class="last_doc">
                                                                <p> <a href="{{URL::to('storage/app/public/uploads/chat_attachment').'/'.@$ms->attachment}}" download><img src="{{ URL::to('public/frontend/images/mail_icon_2.png') }}" alt=""> {{ @$ms->attachment }}</a></p>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="col-md-12 top_right_list last_pagenation">
                    <div class="pagination">
                        <a href="#"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Previous</a>
                        <a href="#">1</a>
                        <a href="#" class="active">2</a>
                        <a href="#">3</a>
                        <a href="#">4</a>
                        <a href="#">next <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                    </div>
                </div> --}}
            </div>
        </div>
        </section>
    </div>
@endsection
@section('footer')
@include('includes.footer')
@endsection