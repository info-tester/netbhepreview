@extends('layouts.app')
@section('title')
    @lang('site.my_messages')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')
    <div class="bkng-hstrybdy">
        <div class="container">
            <h2>@lang('client_site.message')</h2>
            <h6 style="text-align: center;">@lang('site.message_for_user_compose')</h6>            
        </div>
    </div>
    <div class="all_logo_area message_compose">
        <div class="container">
            <div class="row">
                <div class="message_tab">
                    <div class="col-md-12">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#Inbox">@lang('client_site.inbox')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#Sent">@lang('client_site.sent')</a>
                            </li>
                            {{-- <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#Trash">Trash (5)</a>
                            </li> --}}
                        </ul>
                        @if(auth()->user()->sell != 'PS')
                            <a class="compose_btn" href="{{ route('compose') }}"><i class="fa fa-pencil" aria-hidden="true"></i> @lang('client_site.compose')</a>
                        @endif
                        <div class="card msgcnt">
                            <div class="tab-content msg_list_pa">
                                
                                <div role="tabpanel" class="tab-pane active" id="Inbox">
                                    @if(sizeof(@$receiver)>0)
                                    <div class="inbox_msg">
                                        @foreach(@$receiver as $rv)
                                            <div class="one_msg un_read">
                                                <div class="msg_image">
                                                    <img src="{{ @$rv->receiverDetails->profile_pic ? URL::to('storage/app/public/uploads/profile_pic').'/'.@$rv->receiverDetails->profile_pic: URL::to('public/frontend/images/no_img.png') }}">
                                                </div>
                                                <div class="msg_details">
                                                    <div class="top_msg">
                                                        <p>{{ @$rv->receiverDetails->nick_name ? @$rv->receiverDetails->nick_name : @$rv->receiverDetails->name }}</p>
                                                        <div class="right-options">
                                                            <div class="mail_date">
                                                                <img src="{{ URL::to('public/frontend/images/mail_calendar.png') }}" alt=""> {{ date('d-m-Y', strtotime(@$rv->created_at)) }}
                                                            </div>
                                                            <ul>
                                                                {{-- <li>
                                                                    <img src="{{ ('public/frontend/images/mail_icon_1.png') }}" alt="">
                                                                </li>
                                                                <li>
                                                                    <img src="{{ ('public/frontend/images/mail_icon_2.png') }}" alt="">
                                                                </li> --}}
                                                                <li>
                                                                    <a href="javascript::void(0);" class="delReq" data-deltoken="{{@$rv->token_no}}">
                                                                    <img src="{{ ('public/frontend/images/mail_icon_3.png') }}" alt="">
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <p style="white-space: pre-wrap;"> {!! strip_tags(@$rv->msg,"<br>") !!}</p>
                                                    <a class="vw" href="{{ route('viewMessage', ['token'=>@$rv->token_no]) }}"><i class="fa fa-eye" aria-hidden="true"></i> @lang('client_site.view')</a>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    @else
                                    <div class="inbox_msg">
                                        <center>
                                            <p class="error"><h3>@lang('client_site.your_inbox_is_currently_empty')</h3></p>
                                        </center>
                                    </div>
                                    @endif
                                </div>
                                <div role="tabpanel" class="tab-pane" id="Sent">
                                    @if(sizeof(@$sender)>0)
                                    <div class="inbox_msg">
                                        @foreach(@$sender as $snd)
                                            <div class="one_msg un_read">
                                                <div class="msg_image">
                                                    <img src="{{ @$snd->senderDetails->profile_pic ? URL::to('storage/app/public/uploads/profile_pic').'/'.@$snd->senderDetails->profile_pic: URL::to('public/frontend/images/no_img.png') }}">
                                                </div>
                                                <div class="msg_details">
                                                    <div class="top_msg">
                                                        <p>{{ @$snd->senderDetails->name }}</p>
                                                        <div class="right-options">
                                                            <div class="mail_date">
                                                                <img src="{{ URL::to('public/frontend/images/callender.png') }}" alt=""> {{ date('d-m-Y', strtotime(@$snd->created_at)) }}
                                                            </div>
                                                            <ul>
                                                                {{-- <li>
                                                                    <img src="{{ URL::to('public/frontend/images/mail_icon_1.png') }}" alt="">
                                                                </li>
                                                                <li>
                                                                    <img src="{{ URL::to('public/frontend/images/mail_icon_2.png') }}" alt="">
                                                                </li> --}}
                                                                {{-- <li>
                                                                    <a href="javascript::void(0);" class="delReq" data-deltoken="{{@$snd->token_no}}">
                                                                    <img src="{{ URL::to('public/frontend/images/mail_icon_3.png') }}" alt="">
                                                                    </a>
                                                                </li> --}}
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <p style="white-space: pre-wrap;">{!! strip_tags(@$snd->msg, '<br>') !!}</p>
                                                    <a class="vw" href="{{ route('viewMessage', ['token'=>@$snd->token_no]) }}"><i class="fa fa-eye" aria-hidden="true"></i> @lang('client_site.view')</a>                      
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    @else
                                    <div class="inbox_msg">
                                        <center>
                                            <p class="error"><h3>@lang('client_site.your_inbox_is_currently_empty')</h3></p>
                                        </center>
                                    </div>
                                    @endif
                                </div>
                                <div role="tabpanel" class="tab-pane" id="Trash">
                                    <div class="inbox_msg">
                                        <div class="one_msg un_read">
                                            <div class="msg_image">
                                                <img src="images/msg_1.png" alt="">
                                            </div>
                                            <div class="msg_details">
                                                <div class="top_msg">
                                                    <p>Mikka acabangon</p>
                                                    <div class="right-options">
                                                        <div class="mail_date">
                                                            <img src="images/callender.png" alt=""> 08/07/2017, 12:10:58 PM
                                                        </div>
                                                        <ul>
                                                            <li>
                                                                <img src="images/mail_icon_1.png" alt="">
                                                            </li>
                                                            <li>
                                                                <img src="images/mail_icon_2.png" alt="">
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                <img src="images/mail_icon_3.png" alt="">
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of type and scrambled lorem Ipsum is simply dummy text.</p>
                                                <a class="vw" href="message-details.html"><i class="fa fa-eye" aria-hidden="true"></i> View</a>                            
                                            </div>
                                        </div>
                                        <div class="one_msg read">
                                            <div class="msg_image">
                                                <img src="images/msg_2.png" alt="">
                                            </div>
                                            <div class="msg_details">
                                                <div class="top_msg">
                                                    <p>Mikka acabangon</p>
                                                    <div class="right-options">
                                                        <div class="mail_date">
                                                            <img src="images/callender.png" alt=""> 08/07/2017, 12:10:58 PM
                                                        </div>
                                                        <ul>
                                                            <li>
                                                                <img src="images/mail_icon_1.png" alt="">
                                                            </li>
                                                            <li>
                                                                <img src="images/mail_icon_2.png" alt="">
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                <img src="images/mail_icon_3.png" alt="">
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of type and scrambled lorem Ipsum is simply dummy text.</p>
                                                <a class="vw" href="message-details.html"><i class="fa fa-eye" aria-hidden="true"></i> View</a>                             
                                            </div>
                                        </div>
                                        <div class="one_msg un_read">
                                            <div class="msg_image">
                                                <img src="images/msg_1.png" alt="">
                                            </div>
                                            <div class="msg_details">
                                                <div class="top_msg">
                                                    <p>Mikka acabangon</p>
                                                    <div class="right-options">
                                                        <div class="mail_date">
                                                            <img src="images/callender.png" alt=""> 08/07/2017, 12:10:58 PM
                                                        </div>
                                                        <ul>
                                                            <li>
                                                                <img src="images/mail_icon_1.png" alt="">
                                                            </li>
                                                            <li>
                                                                <img src="images/mail_icon_2.png" alt="">
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                <img src="images/mail_icon_3.png" alt="">
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of type and scrambled lorem Ipsum is simply dummy text.</p>
                                                <a class="vw" href="message-details.html"><i class="fa fa-eye" aria-hidden="true"></i> View</a>                               
                                            </div>
                                        </div>
                                        <div class="one_msg read">
                                            <div class="msg_image">
                                                <img src="images/msg_2.png" alt="">
                                            </div>
                                            <div class="msg_details">
                                                <div class="top_msg">
                                                    <p>Mikka acabangon</p>
                                                    <div class="right-options">
                                                        <div class="mail_date">
                                                            <img src="images/callender.png" alt=""> 08/07/2017, 12:10:58 PM
                                                        </div>
                                                        <ul>
                                                            <li>
                                                                <img src="images/mail_icon_2.png" alt="">
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                <img src="images/mail_icon_3.png" alt="">
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of type and scrambled lorem Ipsum is simply dummy text.</p>
                                                <a class="vw" href="message-details.html"><i class="fa fa-eye" aria-hidden="true"></i> View</a>                                      
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="col-md-12 top_right_list last_pagenation">
                    <div class="pagination">
                        <a href="#"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Previous</a>
                        <a href="#">1</a>
                        <a href="#" class="active">2</a>
                        <a href="#">3</a>
                        <a href="#">4</a>
                        <a href="#">next <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                    </div>
                </div> --}}
            </div>
        </div>
        </section>
    </div>
@endsection
@section('footer')
@include('includes.footer')
<script>
    $('.delReq').click(function(){
        if($(this).data('deltoken')!=null || $(this).data('deltoken')!=undefined){
            swal({
                title: "@lang('client_site.delete_conversation')",
                text: "@lang('client_site.do_you_want_to_delete_this_conversation')",
                icon: "info",
                buttons: true,
                dangerMode: true,
              })
              .then((willDelete) => {
                if (willDelete) {
                  location.href="{{url('delete-conversation')}}"+'/'+$(this).data('deltoken');
                } else {
                  return false;
                }
            });
        }
    });
</script>
@endsection