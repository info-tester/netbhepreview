@extends('layouts.app')
@section('title')
  @lang('site.view_booked_user_details')
@endsection
@section('style')
@include('includes.style')
<style>
   .ui-timepicker-standard {
    position: absolute;
    z-index: 9999 !important;
}
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')
<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('client_site.class_details')</h2>
        <div class="bokcntnt-bdy">
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="col-md-12 view-page">
                	<p><strong>@lang('client_site.user_id') :</strong> {{ @$booking->user_id }}</p>
                    <p><strong>@lang('client_site.user_name') :</strong>{{ @$booking->userDetails->nick_name ? @$booking->userDetails->nick_name : @$booking->userDetails->name }}</p>
                    <p><strong>@lang('client_site.date_time') :</strong> {{ toUserTime(@$booking->date.' '.@$booking->start_time,'Y-m-d, h:i a') }}</p>
                    <p><strong>@lang('client_site.category') :</strong> {{@$booking->parentCatDetails ?  @$booking->parentCatDetails->name: 'N.A' }} / {{ @$booking->childCatDetails ? @$booking->childCatDetails->name: 'N.A' }}</p>
                    <p><strong>@lang('client_site.amount') :</strong>${{ @$booking->amount }}</p>
                    <p><strong>@lang('site.booking_type') :</strong> @if(@$booking->booking_type=='C') @lang('site.booking_type_chat') @else @lang('site.booking_type_video') @endif</p>
                    @if(@$booking->video_status=='I' && @$booking->order_status!='C')
                    <p><strong>Status :</strong>@lang('client_site.initiated') </p>
                    @elseif(@$booking->video_status=='C' && @$booking->order_status!='C')
                    <p><strong>Status :</strong>@lang('client_site.complete') </p>
                    @elseif(@$booking->order_status=='C')
                    <p><strong>Status :</strong>@lang('site.order_cancel') </p>
                    @endif
                    @if(@$booking->order_status == 'C')
                    <p><strong>{{__('site.booking_cancelled_by')}} :</strong> @if(@$booking->order_cancelled_by=='P'){{__('site.professional')}} @else{{__('site.User_point')}} @endif</p>
                    <p><strong>{{__('site.cancellation_reason')}} :</strong> {{@$booking->cancellation_reason??'--'}}</p>
                    @endif
                    <p><strong>@lang('client_site.message') :</strong>{{ @$booking->msg }}</p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
<link href="{{ URL::to('public/frontend/css/calender.css') }}" rel="stylesheet" type="text/css">
<script src="{{ URL::to('public/frontend/js/jquery-ui.js') }}"></script>


<script>

    function setVl(tn){
        $('#token_no').val(tn);
    }
    $('#myForm2').validate();
    $('.timepicker').timepicker({
        timeFormat: 'H:mm',
        interval: 15,
        minTime: '00',
        maxTime: '11:00pm',
        startTime: '09:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
$(function() {
        // $('.parent_loader').show();
        $("#date").datepicker({dateFormat: "dd-mm-yy",
           defaultDate: new Date(),
           onClose: function( selectedDate ) {

          }
        });

        $("#datepicker").datepicker({dateFormat: "dd-mm-yy",
           defaultDate: new Date(),
           onClose: function( selectedDate ) {
           $( "#datepicker1").datepicker( "option", "minDate", selectedDate );
          }
        });
    $("#datepicker1").datepicker({dateFormat: "dd-mm-yy",
     defaultDate: new Date(),
          onClose: function( selectedDate ) {
          $( "#datepicker" ).datepicker( "option", "maxDate", selectedDate );
        }
     });

    });
</script>
<script>
    $('#srch').click(function(){
        $('#myForm').submit();
    });
    $('.sbmt').click(function(){
        if($('#date').val()!="" && $('#time').val()!=""){
            $('.parent_loader').show();
            setTimeout(function(){ $('#myForm2').submit(); }, 3000);
        }
        else{
             $('#myForm2').submit();
        }

    });
    $(document).ready(function(){
        $('#myForm').validate({
            rules:{
                phone_no:{
                    digits:true,
                    maxlength:10,
                    minlength:10
                }
            }
        });

        $('#myForm1').validate({
            rules:{
                old_password:{
                    required:true
                },
                new_password:{
                    required:true,
                    minlength:8,
                    maxlength:8
                },
                confirm_password:{
                    required:true,
                    minlength:8,
                    maxlength:8,
                    equalTo:"#new_password"
                }
            }
        });
    });

    $('#phone_no').change(function(){
        if($('#phone_no').val()!=""){
            var reqData = {
              'jsonrpc' : '2.0',
              '_token' : '{{csrf_token()}}',
              'params' : {
                    'no' : $('#phone_no').val()
                }
            };
            $.ajax({
                url: "{{ route('chk.mobile') }}",
                method: 'post',
                dataType: 'json',
                data: reqData,
                success: function(response){
                    if(response.status==1) {
                        $('#phone_no').val("");
                        $('#ph_error').text(@lang('client_site.phone_number_allready_exist'));
                    }

                }, error: function(error) {
                    toastr.info(@lang('client_site.Try_again_after_sometime'));
                }
            });
        }
    });


</script>

@endsection
