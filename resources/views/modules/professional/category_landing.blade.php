@extends('layouts.app')
@section('title')
    @lang('site.my_messages')
@endsection
@section('style')
@include('includes.style')
<style>
    #videoModal .modal-body video{
        width:100%;
        height: auto;
    }
    .readmore_btn{
        box-shadow: 0 3px 30px 0 rgb(0 0 0 / 12%);
    }
    #landing_preloader {
        position: fixed;
        left: 0;
        top: 0;
        z-index: 9999999;
        display: flex;
        width: 100%;
        align-items: center;
        justify-content: center;
        height: 100%;
        overflow: visible;
        background: #fff;
    }
    .u-loading {
        width: 128px;
        height: 128px;
        display: block;
    }

    .u-loading_symbol {
        background-color: #d1d1d1;
        padding: 8px;
        animation: loading  2s infinite;
        border-radius: 5px;
        width: 150px;
        min-height: 62px;
    }

    .u-loading_symbol img {
        display: block;
        max-width: 100%;
        width: 100%;
        height: 100%;
        object-fit: contain;
        animation: loading-icon  2s infinite;
    }
    @keyframes loading {
        0% {
        transform: perspective(250px) rotateX(0deg) rotateY(0deg);
        }

        15% {
            background-color: #d1d1d1;
        }

        16% {
            background-color: #d1d1d1;
        }

        50% {
        transform: perspective(250px) rotateX(180deg) rotateY(0deg);
            background-color: #d1d1d1;
        }

        65% {
            background-color: #d1d1d1;

        }

        66% {
                background-color: #d1d1d1;
        }

        100% {
        transform: perspective(250px) rotateX(180deg) rotateY(0deg);
        }
    }

    @keyframes loading-icon {
        0% {
        transform: perspective(250px) rotateX(0deg) rotateY(0deg);
        }

        15% {
            transform: perspective(250px) rotateX(0deg) rotateY(0deg);
        }

        16% {
            transform: perspective(250px) rotateX(180deg) rotateY(0deg);
        }

        50% {
        transform: perspective(250px) rotateX(180deg) rotateY(0deg);
        }

        65% {
            transform: perspective(250px) rotateX(180deg) rotateY(0deg);
        }

        66% {
                transform: perspective(250px) rotateX(180deg) rotateY(0deg);
        }

        100% {
        transform: perspective(250px) rotateX(180deg) rotateY(0deg);
        }
    }
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
	@include('includes.header')
@endsection
@section('content')
    <div id="landing_preloader" class="u-loading">
        <div class="u-loading_symbol">
            <img src="{{ URL::to('public/frontend/images/loader-logo.png') }}" alt="loader-logo">
        </div>
    </div>
    <!--wrapper start-->
    <div class="wrapper">
        <section class="feature_banner">
            @if(@$detail->banner_image)
                <img src="{{URL::to('storage/app/public/uploads/category_landing_images').'/'.@$detail->banner_image}}" alt="">
            @else
                <img src="{{ URL::to('public/frontend/images/category-banner.png')}}">
            @endif
            <div class="category_banner_cont">
                <div class="container">
                    <div class="row">
                    <div class="col-lg-12 col-md-12">
                        @php
                            $alignment = "";
                            if(@$detail->alignment == 'L'){
                                $alignment = "category_banner_text_left";
                            } else if(@$detail->alignment == 'R') {
                                $alignment = "category_banner_text_right";
                            } else {
                                $alignment = "";
                            }
                            $btn_class = "btnclr";
                        @endphp
                        <div class="category_banner_text {{@$alignment}}">
                            @if(@$detail->banner_heading)
                                <h2 style="color: {{@$detail->banner_heading_color}} !important;">{{@$detail->banner_heading}}</h2>
                            @else
                                <h2 style="color: {{@$detail->banner_heading_color}} !important;">The biggest platform for <span class="dis-block">your growth</span></h2>
                            @endif
                            <p style="color: {{@$detail->banner_desc_color}} !important;">{{@$detail->banner_desc ?? 'Make every decision about publishing, pricing, and ting your education business while crafting amazing learning experiences for your audience. printing and typesetting'}} </p>
                            <a href="{{@$detail->banner_button_link ?? 'javascript:;'}}" target="_blank" class="get_btn2 btnclr">{{@$detail->banner_button_text ?? 'Get Free Trial'}}</a>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="category-feature-section">
            <div class="container">
                <div class="row">
                <div class="col-lg-10 mx-auto col-12">
                    <div class="category-heading">
                    <h2>{{@$detail->whyus_section_heading ?? 'Netbhe, the platform you deserve'}}</h2>
                    <p>{{@$detail->whyus_section_desc ?? "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever"}}</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="box-effect-two selected" style="visibility: visible; animation-name: fadeInUp;">
                    <div class="icon-box">
                        <div class="img-effect mb-4">
                            @if(@$detail->whyus_image_1)
                                <img src="{{URL::to('storage/app/public/uploads/category_landing_images').'/'.@$detail->whyus_image_1}}" alt="">
                            @else
                                <img src="{{ URL::to('public/frontend/images/cate1.png')}}" >
                            @endif
                        </div>
                        <h5 >{{@$detail->whyus_heading_1 ?? 'Online booking'}}</h5>
                        <p class="">{{@$detail->whyus_desc_1 ?? 'Lorem Ipsum is simply dummy text of the printing and setting in Lorem Ipsum has text ever.'}}</p>
                        <a href="{{@$detail->whyus_btn_link_1 ?? 'javascript:;'}}" target="_blank" class="whyus_btns">
                            {{@$detail->whyus_btn_text_1 ?? 'Read More'}} 
                            <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                            <!-- <img src="{{ URL::to('public/frontend/images/arrwblue.png')}}" class="hovera"> <img src="{{ URL::to('public/frontend/images/arrb.png')}}" class="hovern"> -->
                        </a>
                    </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="box-effect-two " style="visibility: visible; animation-name: fadeInUp;">
                    <div class="icon-box">
                        <div class="img-effect mb-4">
                            @if(@$detail->whyus_image_2)
                                <img src="{{URL::to('storage/app/public/uploads/category_landing_images').'/'.@$detail->whyus_image_2}}" alt="">
                            @else
                                <img src="{{ URL::to('public/frontend/images/cate2.png')}}">
                            @endif
                        </div>
                        <h5 >{{@$detail->whyus_heading_2 ?? 'Detailed Reports'}}</h5>
                        <p class="">{{@$detail->whyus_desc_2 ?? 'Lorem Ipsum is simply dummy text of the printing and setting in Lorem Ipsum has text ever.'}}</p>
                        <a href="{{@$detail->whyus_btn_link_2 ?? 'javascript:;'}}" target="_blank" class="whyus_btns">
                            {{@$detail->whyus_btn_text_2 ?? 'Read More'}}
                            <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                            <!-- <img src="{{ URL::to('public/frontend/images/arrwblue.png')}}" class="hovera"> <img src="{{ URL::to('public/frontend/images/arrb.png')}}" class="hovern"> -->
                        </a>
                    </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="box-effect-two " style="visibility: visible; animation-name: fadeInUp;">
                    <div class="icon-box">
                        <div class="img-effect mb-4">
                            @if(@$detail->whyus_image_3)
                                <img src="{{URL::to('storage/app/public/uploads/category_landing_images').'/'.@$detail->whyus_image_3}}" alt="">
                            @else
                                <img src="{{ URL::to('public/frontend/images/cate32.png')}}" >
                            @endif
                        </div>
                        <h5 >{{@$detail->whyus_heading_3 ?? 'Successful marketing'}}</h5>
                        <p class="">{{@$detail->whyus_desc_3 ?? 'Lorem Ipsum is simply dummy text of the printing and setting in Lorem Ipsum has text ever.'}}</p>
                        <a href="{{@$detail->whyus_btn_link_3 ?? 'javascript:;'}}" target="_blank" class="whyus_btns">
                            {{@$detail->whyus_btn_text_3 ?? 'Read More'}}
                            <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                            <!-- <img src="{{ URL::to('public/frontend/images/arrwblue.png')}}" class="hovera"> <img src="{{ URL::to('public/frontend/images/arrb.png')}}" class="hovern"> -->
                        </a>
                    </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="box-effect-two " style="visibility: visible; animation-name: fadeInUp;">
                    <div class="icon-box">
                        <div class="img-effect mb-4">
                            @if(@$detail->whyus_image_4)
                                <img src="{{URL::to('storage/app/public/uploads/category_landing_images').'/'.@$detail->whyus_image_4}}" alt="">
                            @else
                                <img src="{{ URL::to('public/frontend/images/cate4.png')}}" >
                            @endif
                        </div>
                        <h5 >{{@$detail->whyus_heading_4 ?? 'Online booking'}}</h5>
                        <p class="">{{@$detail->whyus_desc_4 ?? 'Lorem Ipsum is simply dummy text of the printing and setting in Lorem Ipsum has text ever.'}}</p>
                        <a href="{{@$detail->whyus_btn_link_4 ?? 'javascript:;'}}" target="_blank" class="whyus_btns">
                            {{@$detail->whyus_btn_text_3 ?? 'Read More'}}
                            <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                            <!-- <img src="{{ URL::to('public/frontend/images/arrwblue.png')}}" class="hovera"> <img src="{{ URL::to('public/frontend/images/arrb.png')}}" class="hovern"> -->
                        </a>
                    </div>
                    </div>
                </div>

                </div>
            </div>
        </section>

        <section class="category-what deeper-category">
            <div class="container">
                <div class="row align-items-center">
                <div class="col-lg-11 mx-auto col-12">
                    <div class="category-heading">
                    @if(@$detail->whatwedo_heading)
                        <h2>{{@$detail->whatwedo_heading}}</h2>
                    @else
                        <h2>What Netbhe can do for you? <span class="dis-block">attract new clients and build lasting customer relationships?</span> </h2>
                    @endif
                    <p>{{@$detail->whatwedo_desc ?? "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever"}}</p>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="cate-what-cont">
                    <ul>
                        <li>{{@$detail->whatwedo_point_1 ?? 'Manage all your daily operations online - it’s easy and secure'}}</li>
                        <li>{{@$detail->whatwedo_point_2 ?? 'Access Shore from anywhere using your PC, tablet or smartphone'}}</li>
                        <li>{{@$detail->whatwedo_point_3 ?? 'Keep track of all customer data and offer a great service'}}</li>
                    </ul>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="cate-what-img">
                        @if(@$detail->whatwedo_image)
                            <img src="{{URL::to('storage/app/public/uploads/category_landing_images').'/'.@$detail->whatwedo_image}}" alt="">
                        @else
                            <img src="{{ URL::to('public/frontend/images/what-img1.png')}}">
                        @endif
                    </div>
                </div>
                </div>
            </div>
        </section>
        @if(@$detail->hide_testimonal != 'Y')
            <section class="category-testimonials">
                <div class="container">
                    <div class="row">
                    <div class="col-12">
                        <div class="col-lg-9 mx-auto col-12">
                        <div class="category-heading">
                        <h2>{{@$detail->testimonial_section_heading ?? 'How Clients React'}}</h2>
                        <p>{{@$detail->testimonial_section_desc ?? "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever"}}</p>
                        </div>
                    </div>
                    <div id="catergory-testimonials" class="owl-carousel owl-theme">
                        <div class="item">
                            <div class="row align-items-center">
                            <div class="col-md-6">
                                <div class="testimonials-left-panel">
                                <img src="{{ URL::to('public/frontend/images/qut.png')}}">
                                <div class="testm-details">
                                    <div class="testm-name">
                                    <h6>{{@$detail->testimonial_name_1 ?? 'Jancy Hanck'}}</h6>
                                    <p>{{@$detail->testimonial_profession_1 ?? 'Business Advisor'}}</p>
                                    </div>
                                    <p>{{@$detail->testimonial_desc_1 ?? "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."}}</p>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="shortt-v">
                                <div class="vedio_bx">
                                    @if(@$detail->testimonial_file_1)
                                        @if(@$detail->testimonial_filetype_1 == 'I')
                                            <img src="{{URL::to('storage/app/public/uploads/category_landing_images').'/'.@$detail->testimonial_file_1}}" alt="">
                                        @elseif(@$detail->testimonial_filetype_1 == 'V')
                                            <video src="{{URL::to('storage/app/public/uploads/category_landing_images').'/'.@$detail->testimonial_file_1}}"></video>
                                        @endif
                                    @else
                                        <!-- <video src="{{URL::to('storage/app/public/uploads/category_landing_images').'/'.'5 Minute Product Demo.mp4'}}"></video> -->
                                        <img src="{{ URL::to('public/frontend/images/test-video.png')}}" alt="">
                                    @endif
                                    @if(@$detail->testimonial_filetype_1 == 'V')
                                        <div class="playIcon">
                                            <div class="circle_box">
                                                <div class="circle" style="animation-delay: 0s"></div>
                                                <div class="circle" style="animation-delay: 1s"></div>
                                            </div>      
                                            <a data-toggle="modal" data-target="#videoModal" class="vdoModal" data-file="{{@$detail->testimonial_file_1}}">
                                            <!-- <a data-toggle="modal" data-target="#videoModal" class="vdoModal" data-file="5 Minute Product Demo.mp4"> -->
                                                <img src="{{ URL::to('public/frontend/images/paly.png')}}" alt="" rel="">
                                            </a>
                                        </div>
                                    @endif
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        @if(@$detail->testimonial_name_2)
                        <div class="item">
                            <div class="row align-items-center">
                                <div class="col-md-6">
                                    <div class="testimonials-left-panel">
                                    <img src="{{ URL::to('public/frontend/images/qut.png')}}">
                                    <div class="testm-details">
                                        <div class="testm-name">
                                        <h6>{{@$detail->testimonial_name_2 ?? 'Jancy Hanck'}}</h6>
                                        <p>{{@$detail->testimonial_profession_2 ?? 'Business Advisor'}}</p>
                                        </div>
                                        <p>{{@$detail->testimonial_desc_2 ?? "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."}}</p>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="shortt-v">
                                    <div class="vedio_bx">
                                        @if(@$detail->testimonial_file_2)
                                            @if(@$detail->testimonial_filetype_2 == 'I')
                                                <img src="{{URL::to('storage/app/public/uploads/category_landing_images').'/'.@$detail->testimonial_file_2}}" alt="">
                                            @elseif(@$detail->testimonial_filetype_2 == 'V')
                                                <video src="{{URL::to('storage/app/public/uploads/category_landing_images').'/'.@$detail->testimonial_file_2}}"></video>
                                            @endif
                                        @else
                                            <!-- <video src="{{URL::to('storage/app/public/uploads/category_landing_images').'/'.'earth.mp4'}}"></video> -->
                                            <img src="{{ URL::to('public/frontend/images/categori4.jpg')}}" alt="">
                                        @endif
                                        @if(@$detail->testimonial_filetype_2 == 'V')
                                            <div class="playIcon">
                                                <div class="circle_box">
                                                    <div class="circle" style="animation-delay: 0s"></div>
                                                    <div class="circle" style="animation-delay: 1s"></div>
                                                </div>      
                                                <a data-toggle="modal" data-target="#videoModal" class="vdoModal" data-file="{{@$detail->testimonial_file_2}}">
                                                <!-- <a data-toggle="modal" data-target="#videoModal" class="vdoModal" data-file="earth.mp4"> -->
                                                    <img src="{{ URL::to('public/frontend/images/paly.png')}}" alt="" rel="">
                                                </a>
                                            </div>
                                        @endif
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        @if(@$detail->testimonial_name_3)
                        <div class="item">
                            <div class="row align-items-center">
                            <div class="col-md-6">
                                <div class="testimonials-left-panel">
                                <img src="{{ URL::to('public/frontend/images/qut.png')}}">
                                <div class="testm-details">
                                    <div class="testm-name">
                                    <h6>{{@$detail->testimonial_name_3 ?? 'Jancy Hanck'}}</h6>
                                    <p>{{@$detail->testimonial_profession_3 ?? 'Business Advisor'}}</p>
                                    </div>
                                    <p>{{@$detail->testimonial_desc_3 ?? "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."}}</p>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="shortt-v">
                                <div class="vedio_bx">
                                    @if(@$detail->testimonial_file_3)
                                        @if(@$detail->testimonial_filetype_3 == 'I')
                                            <img src="{{URL::to('storage/app/public/uploads/category_landing_images').'/'.@$detail->testimonial_file_3}}" alt="">
                                        @elseif(@$detail->testimonial_filetype_3 == 'V')
                                            <video src="{{URL::to('storage/app/public/uploads/category_landing_images').'/'.@$detail->testimonial_file_3}}"></video>
                                        @endif
                                    @else
                                        <img src="{{ URL::to('public/frontend/images/test-video.png')}}" alt="">
                                    @endif
                                    @if(@$detail->testimonial_filetype_3 == 'V')
                                        <div class="playIcon">
                                            <div class="circle_box">
                                                <div class="circle" style="animation-delay: 0s"></div>
                                                <div class="circle" style="animation-delay: 1s"></div>
                                            </div>      
                                            <a data-toggle="modal" data-target="#videoModal" class="vdoModal" data-target="testimonial_file_3">
                                                <img src="{{ URL::to('public/frontend/images/paly.png')}}" alt="" rel="">
                                            </a>
                                        </div>
                                    @endif
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        @endif
                    </div>
                    </div>
                    </div>
                </div> 
            </section>
        @endif
        <div class="modal fade" id="videoModal">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                    <button type="button" class="close closemodal" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <iframe width="100%" height="415px" src="https://www.youtube.com/embed/cIRxRM5M8Ds?enablejsapi=1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen class="modal-testimonail-video"></iframe>
                    </div>
                </div>
            </div>
        </div>

        <section class="grey-category {{ @$detail->hide_testimonal != 'Y' ? 'deeper-category' : '' }}">
            <div class="container">
                <div class="row align-items-center no-gutters">
                    <div class="col-md-6">
                        <div class="cat_feature_img1">
                            @if(@$detail->content_image_1)
                                <img src="{{URL::to('storage/app/public/uploads/category_landing_images').'/'.@$detail->content_image_1}}" alt="">
                            @else
                                <img src="{{ URL::to('public/frontend/images/categort-img1.png')}}">
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="category-feature-heading">
                        <h3>{{@$detail->content_heading_1 ?? 'Your time is valuable. Let Shore simplify your appointment scheduling.'}}</h3>
                        @if(@$detail->content_desc_1)
                            <p>{{@$detail->content_desc_1}}</p>
                        @else
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever when an unknown printer took a galley of tspecimen book.</p>
                            <ul>
                                <li><img src="{{ URL::to('public/frontend/images/tickbu.png')}}"> Boost SEO ranking   </li>
                                
                                <li><img src="{{ URL::to('public/frontend/images/tickbu.png')}}"> Visual Reviews   </li>
                                
                                <li><img src="{{ URL::to('public/frontend/images/tickbu.png')}}"> Marketing  </li>
                                <li><img src="{{ URL::to('public/frontend/images/tickbu.png')}}"> Marketing   </li>
                                <li><img src="{{ URL::to('public/frontend/images/tickbu.png')}}"> Boost SEO ranking   </li>
                                <li><img src="{{ URL::to('public/frontend/images/tickbu.png')}}"> Visual Reviews    </li>
                            </ul>
                        @endif
                        <a href="{{@$detail->content_btn_link_1 ?? 'javascript:;'}}" target="_blank" class="readmore_btn btnclr">{{@$detail->content_btn_text_1 ?? 'Read More'}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="white-category {{ @$detail->hide_testimonal != 'Y' ? '' : 'deeper-category' }}">
          <div class="container">
            <div class="row align-items-center ">
                <div class="col-md-6">
                    <div class="category-feature-heading">
                    <h3>{{@$detail->content_heading_2 ?? 'Give your sales and client satisfaction a boost'}}</h3>
                    @if(@$detail->content_desc_2)
                        <p>{{@$detail->content_desc_2}}</p>
                    @else
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever when an unknown printer took a galley of tspecimen book.</p>
                        <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    @endif
                    <a href="{{@$detail->content_btn_link_2 ?? 'javascript:;'}}" target="_blank" class="readmore_btn btnclr">{{@$detail->content_btn_text_2 ?? 'Read More'}}</a>
                    </div>
                </div>
                <div class="col-md-6">
                    @if(@$detail->content_image_2)
                        <div class="cat_feature_img2">
                            <img src="{{URL::to('storage/app/public/uploads/category_landing_images').'/'.@$detail->content_image_2}}" alt="">
                        </div>
                    @else
                        <div class="cat_feature_img2">
                            <img src="{{ URL::to('public/frontend/images/categort-img2.png')}}">
                            <div class="ab-cat-img">
                                <img src="{{ URL::to('public/frontend/images/content-al.png')}}">
                            </div>
                        </div>
                    @endif
                </div>
              
            </div>
          </div>
        </section>


        <section class="grey-category {{ @$detail->hide_testimonal != 'Y' ? 'deeper-category' : '' }}">
          <div class="container">
            <div class="row align-items-center no-gutters">
              <div class="col-md-6">

               <div class="cat_feature_img3">
                    @if(@$detail->content_image_3)
                        <img src="{{URL::to('storage/app/public/uploads/category_landing_images').'/'.@$detail->content_image_3}}" alt="">
                    @else
                        <img src="{{ URL::to('public/frontend/images/categort-img3.png')}}">
                    @endif
                </div>
              </div>
              <div class="col-md-6">
                <div class="category-feature-heading">
                    <h3>{{@$detail->content_heading_3 ?? 'Your time is valuable. Let Shore simplify your appointment scheduling.'}}</h3>
                    @if(@$detail->content_desc_3)
                        <p>{{@$detail->content_desc_3}}</p>
                    @else
                        <ul class="extra-uls">
                            <li><img src="{{ URL::to('public/frontend/images/tickbu.png')}}"> Lorem Ipsum is simply dummy text of the printing and typesetting ever when an unknown printer took a galley of tspecimen book.   </li>
                            <li><img src="{{ URL::to('public/frontend/images/tickbu.png')}}">  It has survived not only five centuries, but also the leap into lectronic  </li>
                            <li><img src="{{ URL::to('public/frontend/images/tickbu.png')}}"> ever when an unknown printer took a galley of tspecimen book.  </li>
                            <li><img src="{{ URL::to('public/frontend/images/tickbu.png')}}">  It has survived not only five centuries, but also the leap into electronic essentially unchanged.   </li>
                        </ul>
                    @endif
                    <a href="{{@$detail->content_btn_link_3 ?? 'javascript:;'}}" target="_blank" class="readmore_btn btnclr">{{@$detail->content_btn_text_3 ?? 'Read More'}}</a>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section class="white-category {{ @$detail->hide_testimonal != 'Y' ? '' : 'deeper-category' }}">
          <div class="container">
            <div class="row align-items-center no-gutters">
              <div class="col-md-6">
                <div class="category-feature-heading">
                    <h3>{{@$detail->content_heading_4 ?? 'Turn heads with a great-looking professional website'}}</h4>

                    @if(@$detail->content_desc_4)
                        <p>{{@$detail->content_desc_4}}</p>
                    @else
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 

                        <span> when an unknown printer took a galley of type </span> and scrambled it to make a type specimen book. It has survived not only five centuries, Lorem Ipsum is simply dummy text of the printing and dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,</p>
                    @endif
                    <a href="{{@$detail->content_btn_link_4 ?? 'javascript:;'}}" target="_blank" class="readmore_btn btnclr">{{@$detail->content_btn_text_4 ?? 'Read More'}}</a>
                </div>
              </div>
              <div class="col-md-6">
                <div class="cat_feature_img4">
                    @if(@$detail->content_image_4)
                        <img src="{{URL::to('storage/app/public/uploads/category_landing_images').'/'.@$detail->content_image_4}}" alt="">
                    @else
                        <img src="{{ URL::to('public/frontend/images/categort-img4.png')}}">
                    @endif
               </div>
              </div>
              
            </div>
          </div>

       </section>

        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal_product">
            
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close btn_modal" data-dismiss="modal">&times;</button>

                </div>

            <div class="modal-body">
                <p>Course Preview</p>
                <h4>2021 Complete Python Bootcamp From Zero to Hero in Python</h4>

                <div class="video_les">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/jnovz-qihrU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                </div>


                <div class="smap_video">
                    <h4>Free Sample Videos:</h4>
                    <div class="video-smaple_list">
                        <div class="video_details">
                        <div class="p-reative">
                            <img src="{{ URL::to('public/frontend/images/thumb-1.jpg')}}">
                            <span class="over_audio"> <i class="fa fa-volume-up" aria-hidden="true"></i></span>
                        </div>

                        <p>Course Introduction</p>
                        </div>
                        <span>03:45</span>

                    </div>

                    <div class="video-smaple_list active">
                        <div class="video_details">
                        <img src="{{ URL::to('public/frontend/images/thumb-1.jpg')}}">
                        <p>Course Introduction</p>
                        </div>

                        <span>03:45</span>
                    </div>

                    <div class="video-smaple_list">
                        <div class="video_details">
                        <img src="{{ URL::to('public/frontend/images/thumb-1.jpg')}}">
                        <p>Course Introduction</p>
                        </div>
                        <span>03:45</span>

                    </div>
                    <div class="video-smaple_list">
                        <div class="video_details">
                        <img src="{{ URL::to('public/frontend/images/thumb-1.jpg')}}">
                        <p>Course Introduction</p>
                        </div>
                        <span>03:45</span>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')
@include('includes.footer')
<!-- Owl javascript -->
<script src="{{ URL::to('public/frontend/js/owl.carousel.js')}}"></script>
<script>
    $(document).ready(function() {
        setTimeout(function() {
            $('#landing_preloader').fadeOut('slow', function() {
                $(this).remove();
            });
        }, 2000);
        $("#catergory-testimonials").owlCarousel({
            margin: 25,
            nav: true,
            loop: true,
            responsive: {
            0: {
            items: 1
            },
            768: {
            items:1
            },
            1078: {
            items:1
            },
            1200: {
            items:1
            },
            }
        });
        $("#videoModal").on('hidden.bs.modal', function (e) {
            $("#videoModal iframe").attr("src", $("#videoModal iframe").attr("src"));
        });
        $('.vdoModal').click(function(){
            var file = $(this).data('file');
            $('#videoModal').find('.modal-body').empty();
            var html = `<video autoplay controls loop src="{{URL::to('storage/app/public/uploads/category_landing_images')}}/${file}"></video>`;
            $('#videoModal').find('.modal-body').html(html);
        });
        $("#videoModal").on("hidden.bs.modal", function () {
            // put your default event here
            $('#videoModal').find('.modal-body').find('video').trigger('pause');
        });
        var pageclr = "{{@$detail->page_color}}";
        var rgb = hexToRgb(pageclr);
        var rgba = [rgb.slice(0, rgb.length-1), `, 0.2`, rgb.slice(rgb.length-1)].join('');
        rgba = rgba.replace('rgb','rgba');
        $('.deeper-category').css('background', rgba);
        $('.whyus_btns').css('color', `${pageclr}`).hover(function(){
            $(this).css('color', 'black');
        }, function(){
            $(this).css('color', pageclr);
        });
        $('body').append('<style>.box-effect-two:before{background-color: ' + pageclr + ' !important;}</style>');
        $('body').append('<style>.cat_feature_img1 img{border-color: ' + pageclr + ' !important;}</style>');
        $('body').append('<style>.cate-what-cont ul li:before{background-color: ' + pageclr + ' !important;}</style>');

        var btnclr = "{{@$detail->button_color}}";
        var btntxtclr = "{{@$detail->button_text_color}}";
        $('.btnclr').css('background', btnclr).css('color', btntxtclr).hover(function(){
            $(this).css('background', 'black');
            $(this).css('color', 'white');
        }, function(){
            $(this).css('background', btnclr);
            $(this).css('color', btntxtclr);
        });
    });
    let hexToRgb= c=> `rgb(${c.match(/\w\w/g).map(x=>+`0x${x}`)})`;
</script>
@endsection