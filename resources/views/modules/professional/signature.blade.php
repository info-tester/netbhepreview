@extends('layouts.app')
@section('title')
  @lang('site.document_Signature')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
<link rel="stylesheet" href="{{URL::to('public/css/chosen.css')}}">
<style>
    .email_error{
        color: red !important;
    }
</style>
<style type="text/css">
    .agreement-sec {
        height: 50vh;
        overflow-y: scroll;
        border: 1px solid #ced4da;
        padding: 15px;
    }

    .signCanv {
        border: 1px solid #ced4da;
        width: 300px;
        height: 160px;
        float: left;
    }

    .clearButton,
    .submitButton {
        float: left;
        margin-top: 10px;
    }

    .clearButton {
        margin-right: 10px;
    }

    .clearButton a {
        color: #fff;
    }

    p.error {
        background: #fff;
        padding: 0;
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol" !important;
        font-size: 1rem !important;
        font-weight: 400 !important;
        line-height: 1.5 !important;
    }

    @media (max-width: 480px) {
        .signCanv {
            width: 250px;
        }
    }
</style>
    <link href="{{ URL::to('public/frontend/css/calender.css') }}" rel="stylesheet" type="text/css">
    <script src="{{ URL::to('public/frontend/js/jquery-ui.js') }}"></script>
    <script>
    $(function() {
            $("#datepicker11").datepicker({dateFormat: "dd-mm-yy",

                 monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho',
            'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            monthNamesShort: [ "Janeiro", "Fevereiro", "Março", "Abril",
                   "Maio", "Junho", "Julho", "Agosto", "Setembro",
                   "Outubro", "Novembro", "Dezembro" ],

            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],

                defaultDate: new Date(),
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                showMonthAfterYear: true,
                showWeek: true,
                showAnim: "drop",
                constrainInput: true,
                yearRange: "-90:",
                maxDate:new Date(),
                onClose: function( selectedDate ) {
                    //$( "#datepicker1").datepicker( "option", "minDate", selectedDate );
                }
            });

        });

    </script>
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.your_Signature')</h2>
        <div class="bokcntnt-bdy">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile'), <a href="{{ route('professional_qualification') }}">@lang('client_site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">
                    <form id="myForm" method="post" action="{{ route('upload.signature') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form_body">

                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group" style="margin-top: 21px">
                                        <div class="text-center">
                                            <div class="upldd">
                                                {{__('site.upload_signature')}} <i class="fa fa-cloud-upload" aria-hidden="true"></i>
                                                <input type="file"
                                                    accept="image/*" onchange="loadFileSignature(this)" id="signature" name="signature" class="nameExistErrClass">
                                            </div>
                                        </div>
                                        <img id="output2" style="width: 100px;margin-top: 16px;" @if(@Auth::guard('web')->user()->signature)
                                        src="{{ URL::to('storage/app/public/uploads/signature/'.Auth::guard('web')->user()->signature) }}" @endif>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group" style="margin-top: 21px">
                                        {{__('site.signature_upload_white_paper')}}<br/>
                                        {{__('site.signature_upload_white_paper_1')}}<br/>
                                        {{__('site.signature_upload_white_paper_2')}}<br/>
                                        {{__('site.signature_upload_white_paper_3')}}

                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <input type="hidden" name="signature2" id="signatureoutput">
                                    <div class="form-group" id="signArea">
                                        <h5>{{__('site.enter_digital_signature')}}</h5>
                                        <canvas class="signCanv"></canvas>
                                        <div style="width: 100%; float: left;"></div>
                                        <button class="clearButton btn btn-danger" type="button"><a href="#clear">{{__('site.clear_signature')}}</a></button>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group" style="margin-top: 21px">
                                        {{__('site.digital_signature_upload')}}

                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="login_submitt">@lang('client_site.save')</button>
                                    <!-- <a href="{{ route('public.profile.preview', ['self' => 'self', 'slug' => auth()->user()->slug]) }}" class="login_submitt ml-3" target="_blank">@lang('site.preview')</a> -->
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
<link rel="stylesheet" href="{{ URL::to('public/frontend/css/croppie.min.css') }}">
    <style type="text/css">
        .upldd {
            display: block;
            width: auto;
            border-radius: 4px;
            text-align: center;
            background: #9caca9;
            cursor: pointer;
            overflow: hidden;
            padding: 10px 15px;
            font-size: 15px;
            color: #fff;
            cursor: pointer;
            float: left;
            margin-top: 15px;
            font-family: 'Poppins', sans-serif;
            position: relative;
        }
        .upldd input {
            position: absolute;
            font-size: 50px;
            opacity: 0;
            left: 0;
            top: 0;
            width: 200px;
            cursor: pointer;
        }
        .upldd:hover{
            background: #1781d2;
        }
    </style>

    <script src="{{ URL::to('public/frontend/js/bank-account-validator.min.js') }}"></script>
    <script src="{{ URL::to('public/frontend/js/croppie.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/frontend/js/numeric-1.2.6.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/frontend/js/bezier.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/frontend/js/jquery.signaturepad.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/frontend/js/html2canvas.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/frontend/js/json2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/frontend/js/jquery-ui.min.js') }}"></script>
    <script>
        let uploadCrop = $('.upload-demo').croppie({
            viewport: {
                width: 260,
                height: 160,
                type: 'square'
            },
            boundary: {
                width: 300,
                height: 300
            },
            enableExif: true,
        });
        var loadFile = function(input) {
            if (input.files && input.files[0]) {
	            var reader = new FileReader();
	            reader.onload = function (e) {
	            	uploadCrop.croppie('bind', {
	            		url: e.target.result
	            	}).then(function(){
                        $('#crop-modal').modal('show');
	            	});
	            }

	            reader.readAsDataURL(input.files[0]);
	        }
        };
        var loadFileSignature = function(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var result =e.target.result
                    var output = document.getElementById('output2');
                    output.src = result;
                }
                reader.readAsDataURL(input.files[0]);
            }
        };
        $('#crop-modal').on('shown.bs.modal', function() {
            $('.upload-demo').croppie('bind');
        });
        $('.crop-result').click(function() {
            uploadCrop.croppie('result', {
                type: 'base64',
                size: 'original',
            })
            .then(result => {
                var output = document.getElementById('output');
                output.src = result;
                $('#profile_img_').val(result);
                $('#crop-modal').modal('hide');
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $(".chosen").chosen();

            let sig = $('#signArea').signaturePad({
                drawOnly: true,
                drawBezierCurves: true,
                lineTop: 155,
                validateFields: false,
            });

            $('#myForm').validate({
                submitHandler: function(form) {
                    console.log("Validating form");
                    var flag = 0;
                    if (sig.validateForm()) {
                        console.log('test');
                        $('#signatureoutput').val(sig.getSignatureImage());
                        $('#signature').val('');
                    }
                    if($('#signatureoutput').val() == "" && $('#signature').val() == ""){
                        flag = 1;
                        toastr.error("Please upload file or sign on the signature pad.");
                    }
                    if(flag == 0){
                        console.log("Validated");
                        console.log($('#signature')[0].files[0]);
                        // return false;
                        form.submit();
                    }
                },
            });
            jQuery.validator.addMethod("dimention", function (value, element, param) {
                var width = $(element).data('imageWidth');
                var height = $(element).data('imageHeight');
                if(width < param[0] && height < param[1]){
                    return true;
                }else if(width==undefined){
                    return true;
                }else{
                    return false;
                }
            }, 'O tamanho da imagem deve ser 300 x 200');

            jQuery.validator.addClassRules("nameExistErrClass", {
                dimention:[1300, 1200]
            });
            $('#signature').change(function() {
                $('#signature').removeData('imageWidth');
                $('#signature').removeData('imageHeight');
                var file = this.files[0];
                var tmpImg = new Image();
                tmpImg.src=window.URL.createObjectURL( file );
                tmpImg.onload = function() {
                    width = tmpImg.naturalWidth,
                    height = tmpImg.naturalHeight;
                    $('#signature').data('imageWidth', width);
                    $('#signature').data('imageHeight', height);
                }
            });
        });
        // $(document).ready(function () {
        //     let sig = $('#signArea').signaturePad({
        //         drawOnly: true,
        //         drawBezierCurves: true,
        //         lineTop: 155,
        //         validateFields: false,
        //     });
        //     $('#update').click(function() {
        //         if (sig.validateForm()) {
        //             console.log('test');
        //                 $('#signatureoutput').val(sig.getSignatureImage());
        //                 $('#signature').val('');
        //         //         if ($('#myForm').valid()) {
        //         //             // $('#myForm').submit();
        //         //             console.log("validated");
        //         //         }
        //         }
        //     });
		// });
    </script>

@endsection
