@extends('layouts.app')
@section('title')
  @lang('site.my_experience')
@endsection
@section('style')
@include('includes.style')
<style>
    .chosen-search-input{
        text-overflow: ellipsis;
    }
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('client_site.experience')</h2>
        <div class="bokcntnt-bdy">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile'), <a href="{{ route('professional_qualification') }}">@lang('client_site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">
                    <form name="myForm" id="myForm" method="post" action="{{ route('store.professional.experience')}}">
                        @csrf
                        <div class="form_body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">Escolha uma das opções abaixo</label>
                                        <p class="add_ttrr">Aqui você pode escolher uma das especialidades que você achar que são relacionadas a sua especialização e caso você não encontre nenhuma de sua área você pode adicionar.</p>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <p class="add_ttrr text-center">Lista de Especialidades</p>
                                        <hr>
                                        <h6>Escolha uma das especiadades</h6>
                                        <br>
                                        <select name="experiences[]" class="required form-control newdrop required chosen-select" multiple="true" required="" 
                                            placeholder="Selecione dentre as opções as suas especialidades profissionais. Você pode adicionar mais de uma"
                                            data-placeholder="Selecione dentre as opções as suas especialidades profissionais. Você pode adicionar mais de uma"
                                        >
                                            <option value="">Select</option>
                                            @foreach($experiences as $exp)
                                                <option value="{{$exp->id}}" @if(in_array($exp->id, $my_exps)) selected @endif>{{@$exp->experience}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="login_submitt">@lang('site.update')</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="dash_form_box">
                    <form name="myForm" id="myForm" method="post" action="{{ route('add.new.experience')}}">
                        @csrf
                        <div class="form_body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.add_new_exp')</label>
                                        <input type="text" name="experience" class="form-control newdrop">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="login_submitt">@lang('client_site.save')</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')

<script>
    $(document).ready(function(){
        $(".chosen-select").chosen();
        $('#myForm').validate();
        // $('.chosen-search-input').attr('value', 'Selecione dentre as opções as suas especialidades profissionais. Você pode adicionar mais de uma')
    });
</script>
@endsection
