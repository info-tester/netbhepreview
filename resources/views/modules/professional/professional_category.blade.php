@extends('layouts.app')
@section('title')
  @lang('site.my_account')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
<link rel="stylesheet" href="{{URL::to('public/css/chosen.css')}}">
<style>

     .tooltip {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted #ccc;
        color: #006080;
        opacity:1 !important;
    }
    .tooltip .tooltiptext {
        visibility: hidden;
        position: absolute;
        width: 250px;
        background-color: #555;
        color: #fff;
        text-align: center;
        padding: 5px;
        border-radius: 6px;
        z-index: 1;
        opacity: 0;
        transition: opacity 0.3s;
        margin-left:10px
    }
    .tooltip-right {
        top: 0px;
        left: 125%;
    }
    .tooltip-right::after {
        content: "";
        position: absolute;
        top: 10%;
        right: 100%;
        margin-top: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: transparent #555 transparent transparent;
    }
    .tooltip:hover .tooltiptext {
        visibility: visible;
        opacity: 1;
    }
    .tooltip_img i{
        margin-left: 10px !important;
        margin-top: 20px !important;
    }
    .tooltip_img .tooltiptext{
        margin-top: 15px !important;
    }
    .email_error{
        color: red !important;
    }
    .rmv-bttn {
        background: red;
        font-size: 15px;
        font-weight: 400;
        padding: 9px 15px;
        margin-top: 2rem;
        height: 48px;
    }
    @media (max-width: 991px) {
        .rmv-bttn {
            margin-top: 0 !important;
            height: 41px;
        }
    }


</style>
@endsection
@section('header')

@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('client_site.my_account')</h2>
        <div class="bokcntnt-bdy">
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">
                    <form action="{{ route('professional.change.category') }}" method="post" name="myForm" id="myForm">
                        @csrf
                        <div class="form_body">
                            @foreach($userCategories as $k => $category)
                                <div class="row entry-row" id="row-{{ $category->id }}">
                                    <div class="col-lg-5 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">@lang('client_site.category')
                                                <small class="tooltip">
                                                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                    <span class="tooltiptext tooltip-right">@lang('site.tooltip_category')</span>
                                                </small>
                                            </label>
                                            <select name="category[{{ $k }}]" id="category-{{ $k }}" data-id="{{ $k }}" class="required personal-type personal-select cat-select">
                                                <option value="">@lang('client_site.select_option')</option>
                                                @foreach($categories as $cn)
                                                    <option value="{{ $cn->id }}" @if($category->categoryName->parent->id == $cn->id) selected @endif>{{ $cn->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-5 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">@lang('client_site.subcategory')
                                                <small class="tooltip">
                                                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                    <span class="tooltiptext tooltip-right">@lang('site.tooltip_subcategory')</span>
                                                </small> 
                                            </label>
                                            <select name="sub_category[{{ $k }}]" id="sub_category-{{ $k }}" data-id="{{ $k }}" class="required personal-type personal-select">
                                                <option value="">@lang('client_site.select_option')</option>
                                                @php
                                                    $sub = [];
                                                    foreach ($categories as $ccc) {
                                                        if ($category->parent_id == $ccc->id) {
                                                            $sub = $ccc->childCat;
                                                        }
                                                    }
                                                @endphp
                                                @foreach($sub as $cnn)
                                                    <option value="{{ $cnn->id }}" @if($category->categoryName->id == $cnn->id) selected @endif>{{ $cnn->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @if(auth()->user()->sell != 'PS')
                                    <div class="col-lg-2 col-md-6 col-sm-12">
                                        <button type="submit" class="login_submitt rmv-bttn remove-action" data-id="{{ $category->id }}">@lang('site.remove')</button>
                                    </div>
                                    @endif
                                </div>
                            @endforeach

                            <div class="can-add-here"></div>

                            @if(auth()->user()->sell != 'PS')
                            <div class="row">
                                <div class="col-sm-12">
                                    <button type="button" class="login_submitt mr-3 btn btn-success add-new">@lang('client_site.add')</button>
                                    <button type="submit" class="login_submitt">@lang('client_site.save')</button>
                                </div>
                            </div>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
    @include('includes.footer')

    <script>
        let c = {{ count($userCategories ?? []) + 999 }}
        $(document).ready(function() {
            $("body").delegate(".remove-action", "click", function(e){
                const id = $(this).data('id');
                $('#row-' + id).remove();
                if ($('.entry-row').length == 0) {
                    $('.can-add-here').append(createAddMoreHTML());
                }
            });
            $('.add-new').click(function() {
                $('.can-add-here').append(createAddMoreHTML());
            });
            $('body').on('change', '.cat-select', function() {
                const val = $(this).val();
                const id = $(this).data('id');
                $('#sub_category-' + id).html("<option value=\"\">@lang('site.select_sub_category')</option>");
                if(val != ""){
                    const reqData = {
                        jsonrpc : 2.0,
                        _token : '{{ csrf_token() }}',
                        params : {
                            'cat' : val
                        }
                    };
                    $.ajax({
                        url: "{{ route('get.subcat') }}",
                        method: 'post',
                        dataType: 'json',
                        data: reqData,
                        success: function(response){
                            if(response.status==1) {
                                var i=0, html="";
                                html = "<option value=\"\">@lang('site.select_sub_category')</option>";
                                response.result.forEach(v => {
                                    html += '<option value="' + v.id + '">' + v.name + '</option>';
                                });
                                $('#sub_category-' + id).html(html);
                            } else {

                            }
                        }, error: function(error) {
                            console.error(error, error.responseText);
                        }
                    });
                }
            });
            $('#myForm').validate();
        });
        function createAddMoreHTML() {
            c += 1;
            return `<div class="row entry-row" id="row-${c}">
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <div class="form-group">
                        <label class="personal-label">@lang('client_site.category')</label>
                        <select name="category[${c}]" id="category-${c}" data-id="${c}" class="required personal-type personal-select cat-select">
                            <option value="">@lang('client_site.select_option')</option>
                            @foreach($categories as $cn)
                                <option value="{{ $cn->id }}">{{ $cn->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <div class="form-group">
                        <label class="personal-label">@lang('client_site.subcategory')</label>
                        <select name="sub_category[${c}]" id="sub_category-${c}" data-id="${c}" class="required personal-type personal-select">
                            <option value="">@lang('client_site.select_option')</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-12">
                    <button type="submit" class="login_submitt rmv-bttn remove-action" data-id="${c}">@lang('site.remove')</button>
                </div>
            </div>`;
        }
    </script>
@endsection
