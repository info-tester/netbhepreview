@extends('layouts.app')
@section('title')
  @lang('site.my_account')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
<link rel="stylesheet" href="{{URL::to('public/css/chosen.css')}}">
<style>
    .email_error{
        color: red !important;
    }
    .chosen-container-multi .chosen-choices{
        padding: 2px 20px 2px 20px !important;
    }
</style>
<style type="text/css">
    .agreement-sec {
        height: 50vh;
        overflow-y: scroll;
        border: 1px solid #ced4da;
        padding: 15px;
    }

    .signCanv {
        border: 1px solid #ced4da;
        width: 300px;
        height: 160px;
        float: left;
    }

    .clearButton,
    .submitButton {
        float: left;
        margin-top: 10px;
    }

    .clearButton {
        margin-right: 10px;
    }

    .clearButton a {
        color: #fff;
    }

    p.error {
        background: #fff;
        padding: 0;
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol" !important;
        font-size: 1rem !important;
        font-weight: 400 !important;
        line-height: 1.5 !important;
    }
    .tooltip {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted #ccc;
        color: #006080;
        opacity:1 !important;
    }
    .tooltip .tooltiptext {
        visibility: hidden;
        position: absolute;
        width: 250px;
        background-color: #555;
        color: #fff;
        text-align: center;
        padding: 5px;
        border-radius: 6px;
        z-index: 1;
        opacity: 0;
        transition: opacity 0.3s;
        margin-left:10px
    }
    .tooltip-right {
        top: 0px;
        left: 125%;
    }
    .tooltip-right::after {
        content: "";
        position: absolute;
        top: 10%;
        right: 100%;
        margin-top: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: transparent #555 transparent transparent;
    }
    .tooltip:hover .tooltiptext {
        visibility: visible;
        opacity: 1;
    }
    .tooltip_img i{
        margin-left: 10px !important;
        margin-top: 20px !important;
    }
    .tooltip_img .tooltiptext{
        margin-top: 15px !important;
    }

    @media (max-width: 480px) {
        .signCanv {
            width: 250px;
        }
    }
</style>
    <link href="{{ URL::to('public/frontend/css/calender.css') }}" rel="stylesheet" type="text/css">
    <script src="{{ URL::to('public/frontend/js/jquery-ui.js') }}"></script>
    <script>
    $(function() {
            $("#datepicker11").datepicker({dateFormat: "dd-mm-yy",

                 monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho',
            'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            monthNamesShort: [ "Janeiro", "Fevereiro", "Março", "Abril",
                   "Maio", "Junho", "Julho", "Agosto", "Setembro",
                   "Outubro", "Novembro", "Dezembro" ],

            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],

                defaultDate: new Date(),
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                showMonthAfterYear: true,
                showWeek: true,
                showAnim: "drop",
                constrainInput: true,
                yearRange: "-90:",
                maxDate:new Date(),
                onClose: function( selectedDate ) {
                    //$( "#datepicker1").datepicker( "option", "minDate", selectedDate );
                }
            });

        });

    </script>
@endsection
@section('header')

@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('client_site.my_account')</h2>
        <div class="bokcntnt-bdy">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp

            @if(sizeof($user->userQualification)<=0 && $user->sell!='PS')
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile'), <a href="{{ route('professional_qualification') }}">@lang('client_site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">
                    <form action="{{ route('my.profile') }}" method="post" name="myForm" id="myForm" enctype="multipart/form-data">
                                @csrf
                        <div class="form_body">

                            <div class="row">
                                    @php
                                        @$name = explode(' ', @Auth::guard('web')->user()->name);
                                    @endphp
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.first_name')</label>
                                        <input type="text" placeholder="@lang('site.enter_first_name')" class="@if ($user->status == 'A') required @endif personal-type" value="{{ old('first_name',@$name[0]) }}" name="first_name">
                                    </div>
                                </div>
                                {{-- {{ dd(@$name) }} --}}
                                    @php
                                     $nc = "";
                                    @endphp
                                @for($i=1;$i<sizeof($name);$i++)

                                     @if($i>1)
                                        @php
                                            $nc .= ' '.@$name[$i];
                                        @endphp
                                     @else
                                        @php
                                            $nc .= @$name[$i];
                                        @endphp
                                     @endif

                                @endfor
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.last_name')</label>
                                        <input type="text" placeholder="@lang('site.enter_last_name')" class="@if ($user->status == 'A') required @endif personal-type" value="{{ old('last_name',@$nc) }}" name="last_name">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">
                                            @lang('client_site.nick_name')
                                            <div class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">@lang('client_site.tooltip_text')</span>
                                            </div>
                                        </label>
                                        @if(@Auth::guard('web')->user()->nick_name)
                                            <!-- <input type="text" id="nick_name" placeholder="@lang('site.enter_nick_name')" class="@if ($user->status == 'A') required @endif personal-type" value="{{ old('nick_name',@Auth::guard('web')->user()->nick_name) }}" name="nick_name" readonly> -->
                                            <label class="personal-label">{{@Auth::guard('web')->user()->nick_name}}</label>
                                        @else
                                            <input type="text" id="nick_name" placeholder="@lang('site.enter_nick_name')" class="@if ($user->status == 'A') required @endif personal-type" value="{{ old('nick_name') }}" name="nick_name">
                                        @endif
                                    </div>
                                    <label class="error" id="nick_error"></label>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.email')</label>
                                        <input type="text" placeholder="@lang('client_site.enter_email')" readonly class="personal-type" value="{{ old('email',@Auth::guard('web')->user()->email) }}" name="email">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.area_code')</label>
                                        <input type="text" placeholder="@lang('site.enter_area_code')" class="validate personal-type" value="{{ old('area_code',@Auth::guard('web')->user()->area_code ? @Auth::guard('web')->user()->area_code: '') }}" name="area_code">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.phone')</label>
                                        <input type="text" placeholder="@lang('site.enter_ph_no')" id="phone_no" class="@if ($user->status == 'A') required @endif personal-type" value="{{ old('phone_no',@Auth::guard('web')->user()->mobile) }}" name="phone_no">
                                        {{-- <label class="personal-label" id="extraLavelPhone" @if(auth()->user()->country_id == 30) style="display:block" @else style="display:none" @endif>please add your state code with your phone number</label> --}}
                                    </div>
                                    <label class="error" id="ph_error"></label>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.dob')</label>
                                        <input type="text" id="datepicker11" class="@if ($user->status == 'A') required @endif personal-type" value="{{ old('dob',date('d-m-Y', strtotime( @Auth::guard('web')->user()->dob )))  }}" name="dob" placeholder="@lang('site.enter_dob')" autocomplete="off" readonly>
                                    </div>
                                </div>

                                {{-- @if(auth()->user()->country_id == 30) --}}
                                <div class="col-lg-4 col-md-6 col-sm-12" id="cpf_no" @if(auth()->user()->country_id == 30) style="display:block" @else style="display:none" @endif>
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.cpf_no')</label>
                                        <input type="text" placeholder="@lang('site.enter_cpf_no')" class="validate @if ($user->status == 'A') required @endif personal-type" value="{{ old('cpf_no',@Auth::guard('web')->user()->cpf_no) }}" name="cpf_no" id="cpf_no_val">
                                    </div>
                                </div>
                                {{-- @endif --}}

                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.country')</label>
                                        <select name="country" class="@if ($user->status == 'A') required @endif personal-type personal-select" id="country">
                                            <option value="">@lang('client_site.select_country')</option>
                                            @foreach(@$country as $cn)
                                                <option value="{{@$cn->id}}" @if(@Auth::guard('web')->user()->country_id==$cn->id || old('country') == $cn->id) selected @endif>{{@$cn->name}}</option>
                                                {{-- @if(@$cn->id==30)
                                                @endif --}}
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.state')</label>
                                        <select name="state" id="state" class="@if ($user->status == 'A') required @endif personal-type personal-select">
                                            <option value="">@lang('client_site.select_state')</option>
                                            @if(@Auth::guard('web')->user()->state_id)
                                                @foreach(@$state as $cn)
                                                    @if(@Auth::guard('web')->user()->country_id==$cn->country_id)
                                                        <option value="{{@$cn->id}}"  @if(@Auth::guard('web')->user()->state_id==$cn->id || old('state') == $cn->id) selected @endif>{{@$cn->name}}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.city')</label>
                                        <input type="text" placeholder="@lang('site.enter_city')" class="validate @if ($user->status == 'A') required @endif personal-type" value="{{ old('city',@Auth::guard('web')->user()->city) }}" name="city">
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.street_name')</label>
                                        <input type="text" placeholder="@lang('site.enter_street')" class="@if ($user->status == 'A') required @endif personal-type" value="{{ old('street_name',@Auth::guard('web')->user()->street_name) }}" name="street_name">
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.street_number')</label>
                                        <input type="text" placeholder="@lang('site.enter_street_no')" class="@if ($user->status == 'A') required @endif personal-type" value="{{ old('street_number',@Auth::guard('web')->user()->street_number) }}" name="street_number">
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.address_complement')</label>
                                        <input type="text" placeholder="@lang('site.enter_address_complement')" class="@if ($user->status == 'A') required @endif personal-type" value="{{ old('complement',@Auth::guard('web')->user()->complement) }}" name="complement">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.district_name')</label>
                                        <input type="text" placeholder="@lang('site.enter_district')" class="@if ($user->status == 'A') required @endif personal-type" value="{{ old('district',@Auth::guard('web')->user()->district) }}" name="district">
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.zipcode')</label>
                                        <input type="text" placeholder="@lang('site.enter_zip')" class="@if ($user->status == 'A') required @endif personal-type" value="{{ old('zipcode',@Auth::guard('web')->user()->zipcode) }}" name="zipcode">
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.language')</label>
                                        <select name="language[]" id="language" class="@if ($user->status == 'A') required @endif personal-type personal-select chosen" multiple="true" data-placeholder="Selecione alguns idiomas">
                                            @foreach(@$lang as $ln)
                                                <option value="{{ @$ln->id }}"@if(sizeof($lang_id)>0)  @if(in_array(@$ln->id, $lang_id)) selected @endif @endif>{{ @$ln->name }}</option>
                                            @endforeach
                                        </select>
                                        <p id="lang" class="error"></p>
                                    </div>
                                </div>

                                {{-- <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.rate_per_time')</label>
                                        <select name="rate" class="@if ($user->status == 'A') required @endif personal-type personal-select">
                                            <option value="">@lang('client_site.select')</option>
                                            <option value="H" @if(@Auth::guard('web')->user()->rate=="H" || old('rate') == "H") selected @endif>@lang('client_site.hourly')</option>
                                            <option value="M" @if(@Auth::guard('web')->user()->rate=="M" || old('rate') == "M") selected @endif>@lang('client_site.minute')</option>
                                        </select>
                                    </div>
                                </div> --}}

                                <input type="text" name="rate" value="H" hidden>

                                <div class="col-lg-4 col-md-6 col-sm-12" id="service_fees_div" @if(@Auth::guard('web')->user()->sell == 'PS') style="display:none;" @endif >
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.consultant_fees') (@lang('client_site.hourly'))<small class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">@lang('client_site.tooltip_consultant_fees')</span>
                                        </small>
                                    </label>

                                        <input type="text" name="rate_price" id="rate_price" class="personal-type @if ($user->status == 'A') required @endif number" placeholder="@lang('site.enter_tax_consultant')" value="{{ old('rate_price',@Auth::guard('web')->user()->rate_price>0 ?  @Auth::guard('web')->user()->rate_price:'') }}">
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6 col-sm-12" id="interval_div" @if(@Auth::guard('web')->user()->sell == 'PS') style="display:none;" @endif >
                                    <div class="form-group">
                                        <label class="personal-label">@lang('site.time_interval')<small class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">@lang('site.tooltip_time_interval')</span>
                                        </small>
                                    </label>

                                        <select name="call_duration[]" class="required form-control newdrop required chosen-select" multiple="true" data-placeholder="Escolha uma das opções">
                                            <!-- <option value="">@lang('client_site.select')</option> -->
                                            @if( count(@$call_durations) > 0 )
                                                @if( count($user->get_call_durations()->pluck('call_duration_id')->toArray()) > 0)
                                                    @foreach($call_durations as $call_dur)
                                                        <option value="{{$call_dur->id}}" @if(in_array($call_dur->id, $user->get_call_durations()->pluck('call_duration_id')->toArray())) selected @endif> {{$call_dur->call_duration}} @lang('client_site.minutes')</option>
                                                    @endforeach
                                                @else
                                                    @foreach($call_durations as $call_dur)
                                                        <option value="{{$call_dur->id}}" @if($call_dur->call_duration == "60") selected @endif> {{$call_dur->call_duration}} @lang('client_site.minutes')</option>
                                                    @endforeach
                                                @endif
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('site.sell_what')<small class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">@lang('site.tooltip_sell_what')</span>
                                        </small>
                                    </label>


                                        <select name="sell" id="sell" class="@if ($user->status == 'A') required @endif personal-type personal-select">
                                            <!-- <option value="">@lang('client_site.select')</option> -->
                                            <option value="VS" @if(@Auth::guard('web')->user()->sell == 'VS') selected @endif>@lang('client_site.video_only')</option>
                                            <option value="PS" @if(@Auth::guard('web')->user()->sell == 'PS') selected @endif>@lang('client_site.product_sell_only')</option>
                                            <option value="BOTH" @if(@Auth::guard('web')->user()->sell == 'BOTH' || @Auth::guard('web')->user()->sell == null) selected @endif>@lang('client_site.both_of_above')</option>
                                        </select>
                                    </div>
                                </div>

                                {{-- <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.cRP_no') Nome da Especialidade</label>

                                        <input type="text" name="crp_no" id="crp_noxcxc" class="personal-type" placeholder="@lang('site.enter_crp')" value="{{ old('crp_no',@Auth::guard('web')->user()->crp_no) }}">
                                    </div>
                                </div> --}}
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.gender')</label>
                                        <select name="gender" class="@if ($user->status == 'A') required @endif personal-type personal-select">
                                            <option value="">@lang('client_site.select_Gender')</option>
                                            <option value="M" @if(@Auth::guard('web')->user()->gender=="M" || old('gender') == "M") selected @endif>@lang('client_site.male')</option>
                                            <option value="F" @if(@Auth::guard('web')->user()->gender=="F" || old('gender') == "F") selected @endif>@lang('client_site.female')</option>
                                            <option value="B" @if(@Auth::guard('web')->user()->gender=="B" || old('gender') == "B") selected @endif>@lang('client_site.both')</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.time_zone')</label>
                                        <select class="@if ($user->status == 'A') required @endif personal-type personal-select" name="time_zone">
                                            <option value="">@lang('client_site.select_timezone')</option>
                                            @foreach($time as $tm)
                                                <option value="{{ @$tm->timezone_id }}" @if(@$tm->timezone_id==Auth::guard('web')->user()->timezone_id || @$tm->timezone_id==old('time_zone')) selected @endif>{{ @$tm->timezone_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group" style="margin-top: 21px">
                                        <div class="upldd" >
                                            @lang('client_site.upload_picture') <i class="fa fa-cloud-upload" aria-hidden="true"></i>
                                            <input type="file" id="imgInp" accept="image/*" onchange="loadFile(this)">
                                        </div>
                                        <div class="tooltip tooltip_img">
                                            <i class="fa fa-question-circle" aria-hidden="true"></i>
                                            <span class="tooltiptext tooltip-right">@lang('client_site.tooltip_text_image')</span>
                                        </div>
                                        <input type="hidden" name="profile_img" id="profile_img_">
                                        {{-- <label class="recc-sizeinfo"><i class="fa-info-circle fa"></i> Resolução de imagem recomendada 360 x 230</label> --}}
                                        <img id="output" style="width: 100px;margin-top: 16px;" @if(@Auth::guard('web')->user()->profile_pic)  src="{{ URL::to('storage/app/public/uploads/profile_pic/'.Auth::guard('web')->user()->profile_pic) }}" @endif>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.about_me')</label>
                                        <!-- <textarea class="@if ($user->status == 'A') required @endif personal-type99" placeholder="@lang('client_site.enter_some_text_that_describe_yourself')" name="description">{{ @Auth::guard('web')->user()->description }}</textarea> -->
                                        <textarea class="@if ($user->status == 'A') required @endif personal-type99"
                                                    placeholder="Qual sua formação? Qual sua especialização? Metodologia e abordagem de trabalho que você usa? Adicione dados interessante e relevante que você gostaria de ressaltar sobre você e seus serviços."
                                                    name="description">{{ old('description',@Auth::guard('web')->user()->description) }}</textarea>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label"> @lang('site.active_profile') <small class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">@lang('site.tooltip_active_profile')</span>
                                            </small>
                                        </label>

                                        <select name="profile_active" class="required personal-type personal-select">
                                            <option value="Y" @if(@Auth::guard('web')->user()->profile_active=="Y" || old('profile_active') == "Y") selected @endif>Sim</option>
                                            <option value="N" @if(@Auth::guard('web')->user()->profile_active=="N" || old('profile_active') == "N") selected @endif>Não</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12" id="reschedule_div" @if(@Auth::guard('web')->user()->sell == 'PS') style="display:none;" @endif >
                                    <div class="form-group">
                                        <label class="personal-label">@lang('site.reschedule')
                                            <small class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">@lang('site.tooltip_reschedule')</span>
                                            </small>

                                        </label>
                                            <select name="reschedule_time" class="@if ($user->reschedule_time== 'A') required @endif personal-type personal-select">
                                            <option value="">@lang('site.select_reschedule_time')</option>
                                            <option value="3" @if(@Auth::guard('web')->user()->reschedule_time=="3" || old('reschedule_time') == "3") selected @endif>3 {{__('site.hours')}}</option>
                                            <option value="6" @if(@Auth::guard('web')->user()->reschedule_time=="6" || old('reschedule_time') == "6") selected @endif>6 {{__('site.hours')}}</option>
                                            <option value="12" @if(@Auth::guard('web')->user()->reschedule_time=="12" || old('reschedule_time') == "12") selected @endif>12 {{__('site.hours')}}</option>
                                            <option value="24" @if(@Auth::guard('web')->user()->reschedule_time=="24" || old('reschedule_time') == "24") selected @endif>24 {{__('site.hours')}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label class="personal-label"> @lang('site.specialized_in')
                                                    <small class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">@lang('site.tooltip_specialized_in')</span>
                                            </small>
                                                </label>

                                                <select name="professional_specialty_id" id="professional_specialty_id" class="@if ($user->status == 'A') required @endif personal-type personal-select">
                                                    <option value="">@lang('site.choose_specializion')</option>
                                                    @foreach ($specialties as $specialty)
                                                        <option value="{{ $specialty->id }}" @if(@Auth::guard('web')->user()->professional_specialty_id == $specialty->id ) selected @endif>{{ $specialty->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12" id="sp-info" style="display: none;">
                                            <div class="form-group">
                                                <label class="personal-label" id="sp-info-label"> Especialidade de atuação

                                                </label>

                                                <input type="text" name="crp_no" id="sp-info-value" class="personal-type" placeholder="@lang('site.enter_crp')" value="{{ old('crp_no',@Auth::guard('web')->user()->crp_no) }}" @if ($user->status == 'A') required @endif>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">{{__('site.no_of_installment_for_section')}}
                                            <div class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">{{__('site.no_of_installment_for_section_info')}}</span>
                                            </div>
                                        </label>
                                        <select name="no_of_installment" class="personal-type personal-select">
                                            <option value="1" @if(@Auth::guard('web')->user()->no_of_installment==1) selected @endif>--{{__('site.no_installment')}}--</option>
                                            @for ($i = 2; $i <= 12; $i++)
                                            <option value="{{$i}}" @if(@Auth::guard('web')->user()->no_of_installment==$i) selected @endif>{{$i}}</option>
                                            @endfor

                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">{{__('site.installment_charge_given_by_you')}}
                                            <div class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">{{__('site.installment_charge_given_by_you_info')}}</span>
                                            </div>
                                        </label>
                                        <select name="installment_charge" class="personal-type personal-select">
                                            <option value="N" @if(@Auth::guard('web')->user()->installment_charge=='N') selected @endif>{{__('site.no')}}</option>
                                            <option value="Y" @if(@Auth::guard('web')->user()->installment_charge=='Y') selected @endif>{{__('site.yes')}}</option>

                                        </select>
                                    </div>
                                </div>
                                @if($user->is_credit_card_added=='Y')
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.credit_card')
                                            <div class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">This is your card details. You can delete your card data click on delete button</span>
                                            </div>
                                        </label><p style="font-size:18px !important">{{@$user->userCard->first_six}}******{{@$user->userCard->last_four}}
                                        <a href="{{route('credit.card.delete')}}" onclick="return confirm(`@lang('client_site.delete_credit_card_details')`);"><i class="fa fa-trash-o delet" aria-hidden="true"></i></a>
                                        </p>
                                    </div>
                                </div>
                                @endif

                                <div class="col-sm-12">
                                    <button type="submit" class="login_submitt" id="update">@lang('client_site.save')</button>
                                    <a href="{{ route('public.profile.preview', ['self' => 'self', 'slug' => auth()->user()->slug]) }}" class="login_submitt ml-3" target="_blank">@lang('site.preview')</a>
                                </div>

                            </div>
                        </div>
                    </form>
                    @if(auth()->user()->country_id == 30)
                        {{-- @if(!Auth::guard('web')->user()->bankAccount) --}}
                        <div class="form_body">
                            <div class="row">
                                @if(!@Auth::guard('web')->user()->bankAccount->accountCheckNumber)
                                    <div class="col-sm-12">
                                        <h4>
                                            @lang('client_site.add_bank_account')
                                            <small class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">@lang('client_site.tooltip_add_bank_account')</span>
                                            </small>
                                        </h4>
                                    </div>

                                @else
                                    <div class="col-sm-12">
                                        <h4>@lang('client_site.update_bank_account')</h4>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <form id="myForm111" name="myForm111" method="post" action="{{ route('set.bank') }}">
                            @csrf
                            <div class="form_body">
                                <div class="row">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.bank_name')</label>

                                        <select class="required personal-type personal-select" name="bank_name" id="bank_name">
                                            <option value="">@lang('client_site.select_your_bank')</option>
                                            @foreach(@$bankList as $bl)
                                                <option value="{{@$bl->bank_name}}" @if(@$bl->bank_name==@$bank->bankName || old('bank_name')==@$bl->bank_name ) selected @endif>{{@$bl->bank_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.account_number')</label>
                                        <input type="text" placeholder="@lang('site.enter_account_number')" id="account_number" name="account_number" class="required personal-type" value="{{ old('account_number',@$bank->accountNumber) }}">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.agency_number')</label>
                                        <input type="text" id="agency_number" placeholder="@lang('site.enter_agency_number')" name="agency_number" class="required personal-type" value="{{ old('agency_number',@$bank->agencyNumber) }}">
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.bank_number')</label>
                                        <select class="required personal-type personal-select" name="bank_number" id="bank_number">
                                            <option value="">@lang('client_site.select_your_bank_number')</option>
                                            @foreach(@$bankList as $bl)
                                                <option value="{{@$bl->bank_number}}" @if(@$bl->bank_number==@$bank->bank_number || old('bank_number')==@$bank->bank_number ) selected @endif>{{'('.@$bl->bank_number.') - '.@$bl->bank_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.account_check_number')</label>
                                        <input type="text" id="account_check_number" placeholder="@lang('site.enter_account_check_number')" name="account_check_number" class="required personal-type" value="{{ old('account_check_number',@$bank->accountCheckNumber) }}">
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('site.account_type')</label>
                                        <select name="account_type" class="required personal-type personal-select">
                                            <option value="">@lang('site.account_type')</option>
                                            <option @if (@$bank->account_type == 'SAVING') selected="" @endif value="SAVING">Poupança</option>
                                            <option @if (@$bank->account_type == 'CHECKING') selected="" @endif value="CHECKING">Conta corrente</option>

                                        </select>
                                        {{-- <input type="text" id="account_check_number" placeholder="@lang('site.enter_account_check_number')" name="account_check_number" class="required personal-type" value="{{@$bank->accountCheckNumber}}"> --}}
                                    </div>
                                </div>

                                    <div class="col-sm-12">
                                        <button type="submit" id="saveBank" class="login_submitt">{{@$bank->accountCheckNumber ? "Atualizar conta bancária": "Adicionar conta bancária"}}</button>
                                    </div>

                                </div>
                            </div>
                        </form>
                        {{-- @endif --}}
                    @else
                        <div class="form_body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4>@lang('client_site.add_paypal_address')</h4>
                                </div>

                            </div>
                        </div>
                        <form action="{{route('update.paypal.address')}}" method="post" id="paypal_form">
                            @csrf
                            <div class="form_body">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">@lang('client_site.paypal_address')<small class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">@lang('client_site.tooltip_add_paypal_address')</span>
                                            </small>
                                        </label>

                                            <input type="text" placeholder="@lang('site.enter_paypal_address')" id="paypal_address" name="paypal_address" class="required personal-type" value="{{ old('paypal_address', auth()->user()->paypal_address) }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <button type="submit" class="login_submitt">@lang('client_site.save')</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    @endif
                </div>

            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="crop-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {{-- <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div> --}}
            <div class="modal-body">
                <div class="upload-demo" style="width: 100%;">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-primary crop-result">@lang('client_site.save')</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer')
    @include('includes.footer')
    <link rel="stylesheet" href="{{ URL::to('public/frontend/css/croppie.min.css') }}">
    <style type="text/css">
        .upldd {
            display: block;
            width: auto;
            border-radius: 4px;
            text-align: center;
            background: #9caca9;
            cursor: pointer;
            overflow: hidden;
            padding: 10px 15px;
            font-size: 15px;
            color: #fff;
            cursor: pointer;
            float: left;
            margin-top: 15px;
            font-family: 'Poppins', sans-serif;
            position: relative;
        }
        .upldd input {
            position: absolute;
            font-size: 50px;
            opacity: 0;
            left: 0;
            top: 0;
            width: 200px;
            cursor: pointer;
        }
        .upldd:hover{
            background: #1781d2;
        }
    </style>

    <script src="{{ URL::to('public/frontend/js/bank-account-validator.min.js') }}"></script>
    <script src="{{ URL::to('public/frontend/js/croppie.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/frontend/js/numeric-1.2.6.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/frontend/js/bezier.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/frontend/js/jquery.signaturepad.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/frontend/js/html2canvas.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/frontend/js/json2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/frontend/js/jquery-ui.min.js') }}"></script>
    <script>
        $('#sell').change(function(){
            if($(this).val() == 'PS'){
                $('#interval_div').hide();
                $('#service_fees_div').hide();
                $('#reschedule_div').hide();
            } else {
                $('#interval_div').show();
                $('#service_fees_div').show();
                $('#reschedule_div').show();
            }
        });
        $('#nick_name').keyup(function(){
            var reqData = {
                'jsonrpc' : '2.0',
                '_token' : '{{csrf_token()}}',
                'params' : {
                    'name' : $('#nick_name').val()
                }
            };
            $.ajax({
                url: "{{ route('chk.nick.name') }}",
                method: 'post',
                dataType: 'json',
                data: reqData,
                success: function(response){
                    console.log(response);
                    if(response.status==1) {
                        $('#nick_name').val("");
                        $('#nick_error').text('@lang('client_site.nick_name_allready_exist')');
                    } else if(response.status==0) {
                        $('#nick_error').text('');
                    }
                }, error: function(error) {
                    toastr.info('@lang('client_site.Try_again_after_sometime')');
                }
            });
        });
        let uploadCrop = $('.upload-demo').croppie({
            viewport: {
                width: 260,
                height: 160,
                type: 'square'
            },
            boundary: {
                width: 300,
                height: 300
            },
            enableExif: true,
        });
        var loadFile = function(input) {
            if (input.files && input.files[0]) {
	            var reader = new FileReader();
	            reader.onload = function (e) {
	            	uploadCrop.croppie('bind', {
	            		url: e.target.result
	            	}).then(function(){
                        $('#crop-modal').modal('show');
	            	});
	            }

	            reader.readAsDataURL(input.files[0]);
	        }
        };
        var loadFileSignature = function(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var result =e.target.result
                    var output = document.getElementById('output2');
                    output.src = result;
                }
                reader.readAsDataURL(input.files[0]);
            }
        };
        $('#crop-modal').on('shown.bs.modal', function() {
            $('.upload-demo').croppie('bind');
        });
        $('.crop-result').click(function() {
            uploadCrop.croppie('result', {
                type: 'base64',
                size: 'original',
            })
            .then(result => {
                var output = document.getElementById('output');
                output.src = result;
                $('#profile_img_').val(result);
                $('#crop-modal').modal('hide');
            });
        });
    </script>
    <script>
        const specialities = {!! json_encode($specialties->toArray() ?? []) !!};
        var phonemax = phonemin = 9;
        if("{{auth()->user()->country_id}}" != 30){
            phonemax = 8;
            phonemin = 10;
        }
        console.log(specialities);
        function setSpeicaltyInfo(val) {
            if (val == '') {
                $('#sp-info').hide();
            } else {
                $('#sp-info').show();
                let name = '';
                specialities.forEach(v => {
                    if (v.id == val) {
                        name = v.entry_info_message;
                        return false;
                    }
                });
                $('#sp-info-value').attr('placeholder', name);
                $('#sp-info-label').html(name +' <small class="tooltip">'+
                                                '<i class="fa fa-question-circle" aria-hidden="true"></i>'+
                                                '<span class="tooltiptext tooltip-right">Insira aqui a sua especialidade profissional como Life Coaching para Coaches e CRP para Psicólogos.</span>'+
                                                '</small>');
            }
        }
        $(document).ready(function() {
            // var call_duration = "{{$user->call_duration}}";
            $('.chosen-select').chosen();
            // if(call_duration == 'BOTH'){

            // }
            $('#professional_specialty_id').change(function() {
                const val = $(this).val();
                setSpeicaltyInfo(val);
            });
            setSpeicaltyInfo($('#professional_specialty_id').val());
            $(".chosen").chosen();
            /*
            $('#saveBank').click(function() {
                Moip.BankAccount.validate({
                    bankNumber: $("#bank_number" ).val(),
                    agencyNumber: $("#agency_number" ).val(),
                    agencyCheckNumber: $("#agency_check_number" ).val(),
                    accountNumber: $("#account_number" ).val(),
                    accountCheckNumber: 0,
                    valid: function () {
                        alert ( "Valid bank account" );
                    },
                    invalid: function ( data )  {
                        var errors = "Invalid bank account: \n" ;
                        for( i  in  data . errors ) {
                            errors  +=  data . errors [ i ] . description  +  "-"  +  data . errors [ i ] . code  +  ") \n" ;
                        }
                        alert ( errors ) ;
                    }
                });
            });
            */
            $('#myForm111').validate({
                rules:{
                    // bank_number:{
                    //     digits:true,
                    //     minlength:3,
                    //     maxlength:3
                    // },
                    account_number:{
                        digits:true,
                        minlength:1,
                        maxlength:12
                    },
                    account_check_number:{
                        digits:true,
                        minlength:1,
                        maxlength:1
                    },
                    agency_number:{
                        digits:true,
                        // minlength:5,
                        // maxlength:5
                    }

                }
            });
            jQuery.validator.addMethod("dimention", function (value, element, param) {
                var width = $(element).data('imageWidth');
                var height = $(element).data('imageHeight');
                if(width < param[0] && height < param[1]){
                    return true;
                }else if(width==undefined){
                    return true;
                }else{
                    return false;
                }
            }, 'O tamanho da imagem deve ser 300 x 200');

            jQuery.validator.addClassRules("nameExistErrClass", {
                dimention:[1300, 1200]
            });
            $('#signature').change(function() {
                $('#signature').removeData('imageWidth');
                $('#signature').removeData('imageHeight');
                var file = this.files[0];
                var tmpImg = new Image();
                tmpImg.src=window.URL.createObjectURL( file );
                tmpImg.onload = function() {
                    width = tmpImg.naturalWidth,
                    height = tmpImg.naturalHeight;
                    $('#signature').data('imageWidth', width);
                    $('#signature').data('imageHeight', height);
                }
            });
            jQuery.validator.addMethod("alphanumeric", function(value, element) {
                return this.optional(element) || /^[a-zA-Z0-9]*$/.test(value);
            }, "Insira um caractere alfanumérico válido.");
            // jQuery.validator.addMethod("alphanumericspace", function(value, element) {
            //     return this.optional(element) || /^[A-Z0-9 ]*[A-Z0-9][A-Z0-9 ]*$/.test(value);
            // },"Adicione caracteres alfanuméricos ou espaços.");
            jQuery.validator.addMethod("alphanumberspace", function(value, element) {
                return this.optional(element) || /^[A-Z0-9 ]*$/.test(value);
            },"Adicione caracteres alfanuméricos ou espaços.");
            $('#myForm').validate({
                rules:{
                    phone_no:{
                        digits:true,
                        minlength:function(){
                            if($('#country').val()==30){
                                return 9;
                            }else{
                                return 8;
                            }
                        },
                        maxlength:function(){
                            if($('#country').val()==30){
                                return 9;

                            }else{
                                return 10;
                            }
                        }
                    },
                    zipcode:{
                        // digits:true,
                        @if ($user->status == 'A') required: true, @endif
                        minlength:8,
                        maxlength:10
                    },
                    area_code:{
                        digits:true,
                        minlength:2,
                        maxlength:2,
                        required:function(){
                            if($('#country').val()==30){
                                return true;
                            }else{
                                return false;
                            }
                        }
                    },
                    street_number:{
                        alphanumberspace:true,
                        // minlength:3,
                        // maxlength:3
                    }
                }
            });
        });

        $('#phone_no').change(function(){
            if($('#phone_no').val()!=""){
                var reqData = {
                  'jsonrpc' : '2.0',
                  '_token' : '{{csrf_token()}}',
                  'params' : {
                        'no' : $('#phone_no').val()
                    }
                };
                $.ajax({
                    url: "{{ route('chk.mobile') }}",
                    method: 'post',
                    dataType: 'json',
                    data: reqData,
                    success: function(response){
                        if(response.status==1) {
                            $('#phone_no').val("");
                            $('#ph_error').text('@lang('client_site.phone_number_allready_exist')');
                        }

                    }, error: function(error) {
                        toastr.info('@lang('client_site.Try_again_after_sometime')');
                    }
                });
            }
        });
        @if ($user->status == 'A')
            $('#myForm').submit(function(){
                if($('#language').val()==""){
                    $('#lang').text('@lang('client_site.please_select_your_language')');
                    return false;
                }
                else{
                    $('#lang').text("");
                    return true;
                }
            });
        @endif

        $('#country').change(function(){
            if($('#country').val()!=""){
                var reqData = {
                  'jsonrpc' : '2.0',
                  '_token' : '{{csrf_token()}}',
                  'params' : {
                        'cn' : $('#country').val()
                    }
                };
                $.ajax({
                    url: "{{ route('front.get.state') }}",
                    method: 'post',
                    dataType: 'json',
                    data: reqData,
                    success: function(response){
                        if($('#country').val()==30){
                            $('#cpf_no').css('display','block');
                            $('#extraLavelPhone').css('display','block');

                            $('#cpf_no_val').val('');
                        }else{
                            $('#cpf_no').css('display','none');
                            $('#extraLavelPhone').css('display','none');
                            $('#cpf_no_val').val("{{@Auth::guard('web')->user()->cpf_no}}");
                        }
                        if(response.status==1) {
                            var i=0, html="";
                            html = '<option value="">@lang('client_site.select_state')</option>';
                            for(;i<response.result.length;i++){
                                html+='<option value='+response.result[i].id+'>'+response.result[i].name+'</option>';
                            }
                            $('#state').html(html);
                            $('#state').addClass('valid');
                        }
                        else{

                        }
                    }, error: function(error) {
                        console.error(error);
                    }
                });
            }
        });
        $(document).ready(function () {
            let sig = $('#signArea').signaturePad({
                drawOnly: true,
                drawBezierCurves: true,
                lineTop: 155,
                validateFields: false,
            });
            $('#update').click(function() {
                if (sig.validateForm()) {
                    console.log('test')
                    if ($('#myForm').valid()) {
                        $('#signatureoutput').val(sig.getSignatureImage());
                        $('#signature').val('');
                        $('#myForm').submit();
                    }
                }
            });
		});
        $(document).ready(function () {
            jQuery.validator.addMethod("emailonly", function(value, element) {
                return this.optional(element) || /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+$/.test(value.toLowerCase());
            }, "Please enter a valid email.");
            $('#paypal_form').validate({
                rules:{
                    paypal_address:{
                        email:true,
                        emailonly:true,
                    },
                }
            });
        });

    </script>

@endsection
