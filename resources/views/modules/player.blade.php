@extends('layouts.app')
@section('title')
  @lang('site.my_account')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')
      @if(@$my_video)
      <video
          id="video_player"
          class="video-js vjs-big-play-centered"
          data-setup="{}"
        >
        <source src="{{URL::to('storage/app/public/uploads/product_files').'/'.@$my_video.'#t=1'}}" type="video/mp4" />
        <!-- <source src="MY_VIDEO.webm" type="video/webm" /> -->
        <p class="vjs-no-js">
          To view this video please enable JavaScript, and consider upgrading to a
          web browser that
          <a href="https://videojs.com/html5-video-support/" target="_blank"
            >supports HTML5 video</a
          >
        </p>
      </video>
      @else
        <p class="text-center text-secondary">Oops! Something went wrong.</p>
      @endif
@endsection
@section('footer')
@include('includes.footer')
<script src="https://vjs.zencdn.net/7.10.2/video.min.js"></script>
<script>
  var video = videojs('#video_player',{
    autoplay: 'muted',
    controls: true,
    poster: "{{URL::to('storage/app/public/uploads/product_files').'/'.@$my_video.'#t=1'}}",
    aspectRatio: '16:9',
    // responsive: true
  });
  video.on("pause", function () {
    localStorage['{{@$my_video}}_paused_at'] = video.currentTime();
  });
  if(localStorage.getItem('{{@$my_video}}_paused_at')){
    video.currentTime(localStorage.getItem('{{@$my_video}}_paused_at'));
  }
  console.log("{{Request::segment(1)}}");
  window.addEventListener("beforeunload", function (e) {
    // var confirmationMessage = "\o/";
    localStorage['{{@$my_video}}_paused_at'] = video.currentTime();
    // (e || window.event).returnValue = confirmationMessage; //Gecko + IE
    // return confirmationMessage;                            //Webkit, Safari, Chrome
  });
</script>
@endsection