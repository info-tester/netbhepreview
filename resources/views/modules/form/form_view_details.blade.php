@extends('layouts.app')
@section('title')
  @lang('site.my_availability')
@endsection
@section('style')
@include('includes.style')
<style>
.time_slot_span{
    background: #1781d2;
    padding: 2px 7px;
    border: solid 1px #475dfb;
    border-radius: 12px;
    color: #fff;
    margin: 0 15px 10px 0;
    float: left;
    cursor: pointer;

}

}
.ui-datepicker{
	z-index:2 !important;
	}
.form-group {
	 width: 100% !important; 
}
</style>
@endsection
@section('scripts')
@include('includes.scripts')
<script src="{{URL::to('public/js/chosen.jquery.min.js')}}"></script>
@endsection
@section('header')
<link rel="stylesheet" href="{{URL::to('public/css/chosen.css')}}">
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
       {{--  <h2>@lang('client_site.availability')</h2> --}}
        <h2>@lang('site.coaching_tools')</h2>
        <div class="bokcntnt-bdy">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>Coaching Tools</p>
                {{-- <p>@lang('client_site.menu')</p> --}}
            </div>
            @include('includes.user_sidebar')
            
            <div class="dshbrd-rghtcntn">
                <div class="mainDiv">


                    <div class="from-field">
                        <div class="weicome">
                            <a href="javascript:;" class="view_tools_help acpt assginbtn pull-right" data-title="{{$form_data->form_title}}" data-helpdesc="{{ $form_data->help_tools_view }}" style="margin: 2px;">@lang('site.view_tool_help')</a>
                            <h3><strong>@lang('site.form_name'): </strong>{{ @$form_data->form_title }}</h3>
                            <strong>@lang('site.category'): {{ @$form_data->category->form_category }}</strong>
                            <p><strong>@lang('site.description') : </strong> <?= @$form_data->form_dsc ?></p>
                        </div>                       
                    </div>
                   
                    @if($data->tool_type != "Imported Tools")

                        <div class="buyer_table">
                            <div class="">

                                <form action="{{ route('user.view.form.post') }}" method="post" name="myForm" id="myForm" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form_body">
                                        <div class="row">
                                            @foreach($data->getFormData as $key => $value)

                                                {{-- @if($value->field_type!='MULTISELECT')
                                                    @php
                                                    break
                                                    @endphp
                                                @endif --}}
                                                @if($value->field_type == "TEXTBOX")
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="personal-label">{{ $value->field_name }}</label>
                                                            <input type="text" placeholder="{{ $value->field_name }}" class="personal-type required"  name="textfield[]" required="" id="valueField{{ $key }}">
                                                            <input type="hidden" name="question[]" value="{{ $value->field_name }}">
                                                            <input type="hidden" name="form_details_id[]" value="{{ $value->id }}">
                                                        </div>
                                                    </div>
                                                @endif

                                                @if($value->field_type == "TEXTAREA")
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="personal-label">{{ $value->field_name }}</label>
                                                            <textarea class="required personal-type99" placeholder="{{ $value->field_name }}" name="textfield[]" class="form-control required" required="" id="valueField{{ $key }}"></textarea>
                                                            <input type="hidden" name="question[]" value="{{ $value->field_name }}">
                                                            <input type="hidden" name="form_details_id[]" value="{{ $value->id }}">
                                                        </div>
                                                    </div>
                                                @endif 

                                                @if($value->field_type == "SINGLESELECT")
                                                    <?php $selectvalue = json_decode($value->field_values); ?>
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="personal-label">{{ $value->field_name }}</label>
                                                            <select name="textfield[]" class="required personal-type personal-select" id="valueField{{ $key }}">
                                                                <option value="">Select</option>
                                                                @foreach($selectvalue as $select_value)
                                                                    <option value="{{@$select_value}}">{{@$select_value}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="question[]" value="{{ $value->field_name }}">
                                                    <input type="hidden" name="form_details_id[]" value="{{ $value->id }}">
                                                @endif


                                                @if($value->field_type == "MULTISELECT")
                                                    <?php $multi_selectvalue = json_decode($value->field_values); ?>
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="personal-label">{{ $value->field_name }}</label>
                                                           
                                                            <select name="textfield[{{$key}}][]" class="required personal-type personal-select chosen-select " multiple="true" id="valueField{{ $key }}">
                                                                
                                                                @foreach($multi_selectvalue as $multi_select_value)
                                                                    <option value="{{@$multi_select_value}}">{{@$multi_select_value}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="question[]" value="{{ $value->field_name }}">
                                                    <input type="hidden" name="form_details_id[]" value="{{ $value->id }}">
                                                @endif       

                                            @endforeach

                                            <div class="col-sm-12">
                                                <input type="hidden" name="user_to_tools_id" value="{{ $data->id }}">
                                                <input type="hidden" name="professional_id" value="{{ $data->professional_id }}">
                                                <button type="submit" class="login_submitt btnCP">@lang('site.submit')</button>
                                            </div>
                                        </div>
                                    </div>        
                                </form>

                                
                            </div>
                        </div>
                    @endif    

                    
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="view_tool_help_Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Título: <span class="tool-title"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                
                <div class="modal-body">
                    <span class="tool-help-desc-view"></span>
                </div>           
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function(){
        $("#myForm").validate();
        $('.view_tools_help').click(function(){
            var title = $(this).data('title'); 
            var help_desc = $(this).data('helpdesc'); 
            $('.tool-title').html(title);
            $('.tool-help-desc-view').html(help_desc);       
            $('#view_tool_help_Modal').modal('show');
        });

        $('.view_importtools_help').click(function(){          
            $('#view_toolimport_help_Modal').modal('show');
        });
    });

    $(".chosen-select").chosen();
    
</script>
<style>
    .error{
        color: red !important;
    }
</style>
@endsection
@section('footer')
@include('includes.footer')

</script>
@endsection