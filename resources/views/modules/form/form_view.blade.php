@extends('layouts.app')
@section('title')
  @lang('site.my_availability')
@endsection
@section('style')
@include('includes.style')
<style>
.time_slot_span{
    background: #1781d2;
    padding: 2px 7px;
    border: solid 1px #475dfb;
    border-radius: 12px;
    color: #fff;
    margin: 0 15px 10px 0;
    float: left;
    cursor: pointer;

}

}
.ui-datepicker{
	z-index:2 !important;
}
.form-group {
	width: 100% !important;
}
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        {{-- <h2>@lang('client_site.availability')</h2> --}}
        <h2>@lang('site.coaching_tools')</h2>
        <div class="bokcntnt-bdy">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp

            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
            @include('includes.user_sidebar')

            <div class="dshbrd-rghtcntn">

                <div class="mainDiv">
                    @if(sizeof(@$formdata)>0)
                        <div class="buyer_table">
                            <div class="table-responsive">
                                <div class="table">
                                    <div class="one_row1 hidden-sm-down only_shawo">

                                        <div class="cell1 tab_head_sheet">@lang('site.professional_name')</div>
                                        <div class="cell1 tab_head_sheet">@lang('site.tool_type')</div>
                                        <div class="cell1 tab_head_sheet">@lang('site.assign_date')</div>

                                        <div class="cell1 tab_head_sheet">@lang('site.status')</div>
                                        <div class="cell1 tab_head_sheet">@lang('site.action')</div>
                                    </div>
                                    <!--row 1-->
                                    @foreach(@$formdata as $ql)
                                        <div class="one_row1 small_screen31">

                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('site.professional_name')</span>
                                                <p class="add_ttrr">{{ @$ql->getUserData->nick_name ? @$ql->getUserData->nick_name : @$ql->getUserData->name }}</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">


                                                <span class="W55_1">@lang('site.tool_type')</span>
                                                <p class="add_ttrr">


                                                @if(@$ql->tool_type == "FORM")
                                                    @lang('site.form_tools')
                                                @elseif(@$ql->tool_type == "Evaluation360")
                                                    @lang('site.evaluation360')
                                                @elseif(@$ql->tool_type == "Imported Tools")
                                                    @lang('site.imported_tools')
                                                @elseif(@$ql->tool_type == "Log Book")
                                                    @lang('site.logbook')
                                                @elseif(@$ql->tool_type == "Contract Template")
                                                    @lang('site.contract_template')
                                                @elseif(@$ql->tool_type == "Content Templates")
                                                    @lang('site.content_template')
                                                @elseif(@$ql->tool_type == "SMART Goals")
                                                    @lang('site.smart_goals')
                                                @elseif(@$ql->tool_type == "360 evaluation")
                                                    @lang('site.360tools')
                                                @endif



                                                </p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('site.assign_date')</span>
                                                @if(@$ql->getContractTemplatesdata->contract_start_date)
                                                    @php
                                                        $cont_start = strtotime(@$ql->getContractTemplatesdata->contract_start_date);
                                                        $cont_end = strtotime(@$ql->getContractTemplatesdata->contract_end_date);
                                                    @endphp
                                                    <p class="add_ttrr">{{ date('m/d/Y', $cont_start) }} (@lang('site.' . date('D', $cont_start))) - {{ date('m/d/Y', $cont_end) }} (@lang('site.' . date('D', $cont_end)))</p>
                                                @else
                                                    @php
                                                        $assgn_date = strtotime(@$ql->assign_date);
                                                    @endphp
                                                    <p class="add_ttrr">{{ date('m/d/Y', $assgn_date) }} (@lang('site.' . date('D', $assgn_date)))</p>
                                                @endif
                                            </div>

                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">@lang('site.status')</span>
                                                <p class="add_ttrr">@lang('site.' . @$ql->status)</p>
                                            </div>


                                            <div class="cell1 tab_head_sheet_1" style="min-width: 122px;">
                                                <span class="W55_1">@lang('client_site.action')</span>

                                                @if($ql->tool_type == "Imported Tools")
                                                    {{-- <a href="{{ route('user.view.form.details',['id'=>@$ql->id]) }}" >View</a> --}}
                                                    <a href="{{ route('user.importing.answer.show',['id'=>@$ql->id]) }}" >@lang('site.view')</a>

                                                    @if($ql->status == "INVITED")
                                                        |<a href="{{ route('user.importing.assigne.view',['id'=>@$ql->id]) }}" >@lang('site.answer_submit')</a>

                                                        {{-- | <a href="{{ route('professional.importing.update.status',['id'=>@$ql->id]) }}" onclick="return confirm(`@lang('client_site.complete_tools')`);" >Completed</a> --}}
                                                    @endif

                                                    @if(@$ql->getImportedToolsFeedback)
                                                        |<a href="javascript:;" class="view_feedback_btn" data-title="{{ @$ql->getUserData->nick_name ? @$ql->getUserData->nick_name : @$ql->getUserData->name }}" data-userfeedback="{{ @$ql->getImportedToolsFeedback->user_feedback }}" data-proffeedback="{{ @$ql->getImportedToolsFeedback->professional_feedback }}" style="margin: 2px;">@lang('site.view_feedback')</a>
                                                    @else
                                                        |<a href="javascript:;" class="show_assign" data-title="{{$ql->tool_type}}" data-id="{{ $ql->id }}" style="margin: 2px;">@lang('site.feedback')</a>
                                                    @endif
                                                    {{-- |<a href="javascript:;" class="view_tools_help" data-title="{{@$ql->getImportingToolsdata->title}}" data-helpdesc="{{ @$ql->getImportingToolsdata->help_tools_view }}" style="margin: 2px;">View tool help</a> --}}

                                                @elseif($ql->tool_type == "360 evaluation")
                                                    @if($ql->status == "INVITED")
                                                        <a href="{{ route('user.360evaluation',['id'=>@$ql->id]) }}" >@lang('site.submit_answer_score')</a>
                                                    @elseif($ql->status =="COMPLETED")
                                                        <a href="{{ route('user.360evaluation.view',['id'=>@$ql->id]) }}" >@lang('site.view')</a>
                                                    @endif

                                                @elseif($ql->tool_type == "SMART Goals")
                                                    @if($ql->status == "INVITED")
                                                        <?php
                                                            $nowtime = date('Y-m-d');
                                                            $smar_enddate = date('Y-m-d', strtotime(@$ql->getSmartGoalsData->end_date));
                                                        ?>
                                                        @if($nowtime < $smar_enddate)
                                                            <a href="{{ route('user.smart.goal.view',['id'=>@$ql->id]) }}" >@lang('site.submit_your_goal')</a>
                                                        @else
                                                            <a href="{{ route('user.smart.goal.view',['id'=>@$ql->id]) }}" >@lang('site.view')</a>
                                                        @endif
                                                    @endif

                                                @elseif($ql->tool_type == "Content Templates")
                                                    @if($ql->status == "INVITED")
                                                        <a href="{{ route('user.content.temp.view',['id'=>@$ql->id]) }}" >@lang('site.view')</a>
                                                    @endif


                                                @elseif($ql->tool_type == "Contract Template")
                                                    <a href="{{ route('user.contract.temp.view',['id'=>@$ql->id]) }}" >@lang('site.view')</a>
                                                    @if($ql->status == "INVITED")
                                                        | <a href="{{ route('user.contract.temp.update.status',['id'=>@$ql->id]) }}" onclick="return confirm(`@lang('client_site.accept_contract')`);" >@lang('site.accept_contract')</a>
                                                    @endif


                                                @elseif($ql->tool_type == "Log Book")

                                                   <!--  @if($ql->status == "INVITED")
                                                        <a href="javascript:;" class="logbook_answer_btn" data-title="{{@$ql->getlogbookDetails->question}}" data-id="{{ $ql->id }}" style="margin: 2px;">Submit Answer</a>
                                                    @else
                                                        <a href="{{ route('user.logbook.view',['id'=>@$ql->id]) }}" >View</a>
                                                        {{-- | <a href="{{ route('user.loogbook.answer.submit',['id'=>@$ql->id]) }}">Submit Answer</a> --}}
                                                    @endif -->
                                                    <a href="{{ route('user.logbook.all.ansdate') }}" >@lang('site.view')</a> |
                                                    <!-- <a href="{{ route('user.logbook.view',['id'=>@$ql->id]) }}" >View</a> | -->

                                                    <a href="{{route('user.logbook.question.list',[$ql->id])}}" class="" style="margin: 2px;" title="Edit">@lang('site.answer_submit')</a>

                                                @elseif($ql->tool_type == "Evaluation360")

                                                        <a href="{{ route('user.evaluation360.quelist',['id'=>@$ql->id]) }}" ></i>@lang('site.share_and_preview')</a> |

                                                        <a href="{{ route('user.evaluation360.score.listing',['id'=>@$ql->id]) }}" >@lang('site.view')</a>

                                                @else
                                                    @if($ql->status == "INVITED")
                                                        <a href="{{ route('user.view.form.details',['id'=>@$ql->id]) }}" >@lang('site.submit_form')</a>
                                                    @elseif($ql->status =="COMPLETED")
                                                        <a href="{{ route('user.view.form.submit.details',['id'=>@$ql->id]) }}" > @lang('site.view')</a>
                                                    @endif

                                                    @if(@$ql->getImportedToolsFeedback)
                                                        |<a href="javascript:;" class="view_feedback_btn" data-title="{{ $ql->getUserData->nick_name ? $ql->getUserData->nick_name : $ql->getUserData->name}}" data-userfeedback="{{ @$ql->getImportedToolsFeedback->user_feedback }}" data-proffeedback="{{ @$ql->getImportedToolsFeedback->professional_feedback }}" style="margin: 2px;">@lang('site.view_feedback')</a>
                                                    @else
                                                        |<a href="javascript:;" class="show_formbtn" data-title="{{$ql->tool_type}}" data-id="{{ $ql->id }}" style="margin: 2px;">@lang('site.feedback')</a>
                                                    @endif


                                                @endif
                                                    {{-- <a href="#" class="rjct" style="background: #098005;"><i class="fa fa-pencil"></i> @lang('client_site.edit') </a> --}}
                                            </div>
                                        </div>
                                    @endforeach
                                    {{-- {{ $avl->->onEachSide(5)->links() }} --}}

                                    {{-- {{ $formdata->links() }} --}}
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>



<!--View tool help Modal start -->
<div class="modal fade" id="view_tool_help_Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">@lang('site.tool_title'): <span class="tool-title"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <span class="tool-help-desc-view"></span>
            </div>
        </div>
    </div>
</div>
<!--View tool help Modal end -->


<!--insert imp[orted tools feedback Modal start -->
<div class="modal fade" id="assignModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">{{-- <span class="tool-type"></span> : Feedback --}} @lang('site.what_did_you_learn_from_these_answers')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="myform" method="post" action="{{ route('user.importing.tools.feedback') }}">
                {{ csrf_field() }}
                <div class="modal-body">
                    {{-- <h5>Tool Title: <span class="tool-type"></span></h5> --}}

                        <label for="exampleInputEmail1" class="personal-label">@lang('site.feedback')</label>
                        <textarea name="user_feedback" class="required personal-type99" rows="7"></textarea>

                    <input type="hidden" name="user_to_tools_id" class="user_to_tools_id"> {{-- this calass form id get form id --}}
                </div>

                <div class="modal-footer">
                    <button type="submit" value="submit" class="btn btn-primary">@lang('site.submit')</button>
                </div>

            </form>
        </div>
    </div>
</div>
<!--View tool help Modal end -->



<!--View feedback both (user and professional) Modal start -->
<div class="modal fade" id="view_both_feedback_Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">@lang('site.what_did_you_learn_from_these_answers'){{-- Feedback <span class="tool-title"></span> --}}</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <p>@lang('site.user') : <span class="user-feedback"></span> </p>
                <p>@lang('site.professional'): <span class="prof-feedback"></span></p>
            </div>
        </div>
    </div>
</div>
<!--View feedback Modal end -->




<!--insert imported tools feedback Modal start -->
<div class="modal fade" id="assignformModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">@lang('site.what_did_you_learn_from_these_answers'){{-- What did you learn from these answers? --}}{{-- <span class="tool-type"></span> : Feedback --}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="myform" method="post" action="{{ route('user.form.tools.feedback') }}">
                {{ csrf_field() }}
                <div class="modal-body">
                    {{-- <h5>Tool Title: <span class="tool-type"></span></h5> --}}

                        <label for="exampleInputEmail1" class="personal-label">@lang('site.feedback')</label>
                        <textarea name="user_feedback" class="required personal-type99" rows="7"></textarea>

                    <input type="hidden" name="user_to_tools_id" class="user_to_tools_id"> {{-- this calass form id get form id --}}
                </div>

                <div class="modal-footer">
                    <button type="submit" value="submit" class="btn btn-primary">@lang('site.submit')</button>
                </div>

            </form>
        </div>
    </div>
</div>
<!--View tool help Modal end -->


<!--insert loogbook question- answer Modal start -->
<div class="modal fade" id="loogbook_question_Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">@lang('site.question') : <span class="loogbook_question"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="myform" method="post" action="{{ route('user.loogbook.answer.submit') }}">
                {{ csrf_field() }}
                <div class="modal-body">
                    {{-- <h5>Tool Title: <span class="tool-type"></span></h5> --}}

                        <label for="exampleInputEmail1" class="personal-label">@lang('site.type_your_answer')</label>
                        <textarea name="answer" class="required personal-type99" rows="7"></textarea>

                    <input type="hidden" name="user_to_tools_id" class="user_to_tools_id"> {{-- this calass form id get form id --}}
                    <input type="hidden" name="loogbook_question" class="logquestion"> {{-- this calass form question get form id --}}
                </div>

                <div class="modal-footer">
                    <button type="submit" value="submit" class="btn btn-primary">@lang('site.submit')</button>
                </div>

            </form>
        </div>
    </div>
</div>
<!--insert loogbook question- answer Modal end-->







</section>
@endsection
@section('footer')
@include('includes.footer')



<script>
$(document).ready(function(){

    $('.view_tools_help').click(function(){
        var title = $(this).data('title');
        var help_desc = $(this).data('helpdesc');
        $('.tool-title').html(title);
        $('.tool-help-desc-view').html(help_desc);
        $('#view_tool_help_Modal').modal('show');
    });

});
</script>


<script>

$(document).ready(function(){
    $('.show_assign').click(function(){
        var title = $(this).data('title');
        var id = $(this).data('id');
        $('.tool-type').html(title);
        $('.user_to_tools_id').val(id);
        $('#assignModal').modal('show');
    });

    $('.view_feedback_btn').click(function(){
        var title = $(this).data('title');
        var user_feed = $(this).data('userfeedback');
        var prof_feed = $(this).data('proffeedback');

        $('.tool-title').html(title);
        $('.user-feedback').html(user_feed);
        $('.prof-feedback').html(prof_feed);
        $('#view_both_feedback_Modal').modal('show');
    });

    // for form modules

    $('.show_formbtn').click(function(){
        var title = $(this).data('title');
        var id = $(this).data('id');
        $('.tool-type').html(title);
        $('.user_to_tools_id').val(id);
        $('#assignformModal').modal('show');
    });

    // for loogbook modules

    $('.logbook_answer_btn').click(function(){
        var title = $(this).data('title');
        var id = $(this).data('id');
        $('.loogbook_question').html(title);
        $('.user_to_tools_id').val(id);
        $('.logquestion').val(title);
        $('#loogbook_question_Modal').modal('show');
    });

});

$(".chosen-select").chosen();

$(document).ready(function(){
    $("#myform").validate();
});
</script>


@endsection
