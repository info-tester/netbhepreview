@extends('layouts.app')
@section('title')
  {{-- @lang('site.view_booked_user_details') --}}
  User submit answer
@endsection
@section('style')
@include('includes.style')
<style>
   .ui-timepicker-standard {
    position: absolute;
    z-index: 9999 !important;
}
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')
<section class="bkng-hstrybdy">
    <div class="container">
        {{-- <h2>@lang('client_site.class_details')</h2> --}}
        <h2>Coaching tools</h2>
        {{-- @include('includes.professional_tab_menu') --}}
        
        <div class="bokcntnt-bdy">
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
            @include('includes.user_sidebar')
            <div class="dshbrd-rghtcntn">

                <div class="from-field">
                    <div class="weicome manage-designs">                       


                        <h3><strong>Form name <b>:</b> </strong> {{ $form_data->form_title }}</h3>
                        <p><strong>Category <b>:</b></strong> {{ $form_data->category->form_category }}</p>
                        <p><strong>Description <b>:</b></strong> <?= $form_data->form_dsc ?></p>
                        

                        {{-- <h3>{{ @$form_data->form_title }}</h3>
                        <strong>{{ @$form_data->category->form_category }}</strong>
                        <p>{{ @$form_data->form_title }}</p> --}}
                    </div>                       
                </div>
                 

                <div class="col-md-12 view-page coach_tools full-w">

                    @foreach(@$answerdata as $ans_data)                       
                        @if(@$ans_data->getFormDetailsData->field_type == "MULTISELECT")
                            <p>
                                <strong>{{ $ans_data->question }}:</strong>
                            </p>
                            <p>   
                                @php 
                                    $multi_ans_value = json_decode($ans_data->answer); 
                                @endphp
                                @foreach($multi_ans_value as $answers)
                                    {{ $answers }}
                                    @if(!$loop->last)
                                        ,
                                    @endif    
                                @endforeach    
                            </p>
                        @else
                            <p><strong>{{ $ans_data->question }}:</strong></p>
                            <p>{{ $ans_data->answer }}</p>
                        @endif
                    @endforeach    

                	{{-- <p><strong>@lang('client_site.user_id') :</strong> {{ @$booking->user_id }}</p>
                    <p><strong>@lang('client_site.user_name') :</strong>{{ @$booking->userDetails->nick_name ? @$booking->userDetails->nick_name : @$booking->userDetails->name }}</p>
                    <p><strong>@lang('client_site.date_time') :</strong> {{ @$booking->date }}, {{ date('H:i a' ,strtotime(@$booking->start_time)) }}</p>
                    <p><strong>@lang('client_site.category') :</strong> {{@$booking->parentCatDetails ?  @$booking->parentCatDetails->name: 'N.A' }} / {{ @$booking->childCatDetails ? @$booking->childCatDetails->name: 'N.A' }}</p>
                    <p><strong>@lang('client_site.amount') :</strong>${{ @$booking->amount }}</p>
                   @if(@$booking->video_status!='C')
                        <p><strong>Status :</strong>@lang('client_site.initiated') </p>
                    @else
                        <p><strong>Status :</strong>@lang('client_site.complete') </p>
                    @endif --}}

                </div>

                
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
<link href="{{ URL::to('public/frontend/css/calender.css') }}" rel="stylesheet" type="text/css">
<script src="{{ URL::to('public/frontend/js/jquery-ui.js') }}"></script>


@endsection