@extends('layouts.app')
@section('title')
  @lang('site.my_account')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
<link rel="stylesheet" href="{{URL::to('public/css/chosen.css')}}">
<style>
    .email_error{
        color: red !important;
    }
    .chosen-container-multi .chosen-choices{
        padding: 2px 20px 2px 20px !important;
    }
</style>
<style type="text/css">
    .agreement-sec {
        height: 50vh;
        overflow-y: scroll;
        border: 1px solid #ced4da;
        padding: 15px;
    }

    .signCanv {
        border: 1px solid #ced4da;
        width: 300px;
        height: 160px;
        float: left;
    }

    .clearButton,
    .submitButton {
        float: left;
        margin-top: 10px;
    }

    .clearButton {
        margin-right: 10px;
    }

    .clearButton a {
        color: #fff;
    }

    p.error {
        background: #fff;
        padding: 0;
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol" !important;
        font-size: 1rem !important;
        font-weight: 400 !important;
        line-height: 1.5 !important;
    }
    .tooltip {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted #ccc;
        color: #006080;
        opacity:1 !important;
    }
    .tooltip .tooltiptext {
        visibility: hidden;
        position: absolute;
        width: 250px;
        background-color: #555;
        color: #fff;
        text-align: center;
        padding: 5px;
        border-radius: 6px;
        z-index: 1;
        opacity: 0;
        transition: opacity 0.3s;
        margin-left:10px
    }
    .tooltip-right {
        top: 0px;
        left: 125%;
    }
    .tooltip-right::after {
        content: "";
        position: absolute;
        top: 10%;
        right: 100%;
        margin-top: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: transparent #555 transparent transparent;
    }
    .tooltip:hover .tooltiptext {
        visibility: visible;
        opacity: 1;
    }
    .tooltip_img i{
        margin-left: 10px !important;
        margin-top: 20px !important;
    }
    .tooltip_img .tooltiptext{
        margin-top: 15px !important;
    }

    @media (max-width: 480px) {
        .signCanv {
            width: 250px;
        }
    }
</style>
    <link href="{{ URL::to('public/frontend/css/calender.css') }}" rel="stylesheet" type="text/css">
    <script src="{{ URL::to('public/frontend/js/jquery-ui.js') }}"></script>
    <script>
    $(function() {
            $("#datepicker11").datepicker({dateFormat: "dd-mm-yy",

                 monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho',
            'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            monthNamesShort: [ "Janeiro", "Fevereiro", "Março", "Abril",
                   "Maio", "Junho", "Julho", "Agosto", "Setembro",
                   "Outubro", "Novembro", "Dezembro" ],

            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],

                defaultDate: new Date(),
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                showMonthAfterYear: true,
                showWeek: true,
                showAnim: "drop",
                constrainInput: true,
                yearRange: "-90:",
                maxDate:new Date(),
                onClose: function( selectedDate ) {
                    //$( "#datepicker1").datepicker( "option", "minDate", selectedDate );
                }
            });

        });

    </script>
@endsection
@section('header')

@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>{{__('site.my_video_affiliate_account_create')}}</h2>
        <div class="bokcntnt-bdy">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp

            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('client_site.menu')</p>
            </div>
            @if($user->is_professional== 'Y')
            @include('includes.professional_sidebar')
            @else
            @include('includes.user_sidebar')
            @endif
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">
                    <form action="{{ route('submit.video.affiliate') }}" method="post" name="myForm" id="myForm" enctype="multipart/form-data">
                                @csrf
                        <div class="form_body">

                            <div class="row">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.country')</label>
                                        <select name="country" class="required personal-type personal-select" id="country">
                                            <option value="">@lang('client_site.select_country')</option>
                                            @foreach(@$country as $cn)
                                                <option value="{{@$cn->id}}" @if(@Auth::guard('web')->user()->country_id==$cn->id || old('country') == $cn->id) selected @endif>{{@$cn->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @php
                                @$name = explode(' ', @Auth::guard('web')->user()->name);
                                @endphp
                                <div class="col-lg-4 col-md-6 col-sm-12 acshow" @if(auth()->user()->country_id != 30) style="display: none" @endif>
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.first_name')</label>
                                        <input type="text" placeholder="@lang('site.enter_first_name')" class="@if ($user->status == 'A') required @endif personal-type" value="{{ old('first_name',@$name[0]) }}" name="first_name">
                                    </div>
                                </div>
                                @php
                                    $nc = "";
                                @endphp
                                @for($i=1;$i<sizeof($name);$i++)
                                    @if($i>1)
                                        @php
                                            $nc .= ' '.@$name[$i];
                                        @endphp
                                    @else
                                        @php
                                            $nc .= @$name[$i];
                                        @endphp
                                    @endif

                                @endfor
                                <div class="col-lg-4 col-md-6 col-sm-12 acshow"    @if(auth()->user()->country_id != 30) style="display: none" @endif>
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.last_name')</label>
                                        <input type="text" placeholder="@lang('site.enter_last_name')" class="@if ($user->status == 'A') required @endif personal-type" value="{{ old('last_name',@$nc) }}" name="last_name">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 acshow" @if(auth()->user()->country_id != 30) style="display: none" @endif>
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.email')</label>
                                        <input type="text" placeholder="@lang('client_site.enter_email')" readonly class="personal-type" value="{{ old('email',@Auth::guard('web')->user()->email) }}" name="email">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 acshow" @if(auth()->user()->country_id != 30) style="display: none" @endif>
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.area_code')</label>
                                        <input type="text" placeholder="@lang('site.enter_area_code')" class="validate @if ($user->status == 'A') required @endif personal-type" value="{{ old('area_code',@Auth::guard('web')->user()->area_code >0 ? @Auth::guard('web')->user()->area_code: '') }}" name="area_code">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 acshow" @if(auth()->user()->country_id != 30) style="display: none" @endif>
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.phone')</label>
                                        <input type="text" placeholder="@lang('site.enter_ph_no')" id="phone_no" class="@if ($user->status == 'A') required @endif personal-type" value="{{ old('phone_no',@Auth::guard('web')->user()->mobile) }}" name="phone_no">
                                    </div>
                                    <label class="error" id="ph_error"></label>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 acshow" @if(auth()->user()->country_id != 30) style="display: none" @endif>
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.dob')</label>
                                        <input type="text" id="datepicker11" class="@if ($user->status == 'A') required @endif personal-type" value="{{ old('dob',date('d-m-Y', strtotime( @Auth::guard('web')->user()->dob )))  }}" name="dob" placeholder="@lang('site.enter_dob')" autocomplete="off" readonly>
                                    </div>
                                </div>




                                <div class="col-lg-4 col-md-6 col-sm-12 acshow" @if(auth()->user()->country_id != 30) style="display: none" @endif>
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.state')</label>
                                        <select name="state" id="state" class="@if ($user->status == 'A') required @endif personal-type personal-select">
                                            <option value="">@lang('client_site.select_state')</option>
                                            @if(@Auth::guard('web')->user()->state_id)
                                                @foreach(@$state as $cn)
                                                    @if(@Auth::guard('web')->user()->country_id==$cn->country_id)
                                                        <option value="{{@$cn->id}}"  @if(@Auth::guard('web')->user()->state_id==$cn->id || old('state') == $cn->id) selected @endif>{{@$cn->name}}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 acshow" @if(auth()->user()->country_id != 30) style="display: none" @endif>
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.city')</label>
                                        <input type="text" placeholder="@lang('site.enter_city')" class="validate @if ($user->status == 'A') required @endif personal-type" value="{{ old('city',@Auth::guard('web')->user()->city) }}" name="city">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 acshow" @if(auth()->user()->country_id != 30) style="display: none" @endif>
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.cpf_no')</label>
                                        <input type="text" placeholder="@lang('site.enter_cpf_no')" class="validate  required personal-type" value="{{ old('cpf_no',@Auth::guard('web')->user()->cpf_no) }}" name="cpf_no">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 acshow" @if(auth()->user()->country_id != 30) style="display: none" @endif>
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.street_name')</label>
                                        <input type="text" placeholder="@lang('site.enter_street')" class="@if ($user->status == 'A') required @endif personal-type" value="{{ old('street_name',@Auth::guard('web')->user()->street_name) }}" name="street_name">
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6 col-sm-12 acshow" @if(auth()->user()->country_id != 30) style="display: none" @endif>
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.street_number')</label>
                                        <input type="text" placeholder="@lang('site.enter_street_no')" class="@if ($user->status == 'A') required @endif personal-type" value="{{ old('street_number',@Auth::guard('web')->user()->street_number) }}" name="street_number">
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6 col-sm-12 acshow" @if(auth()->user()->country_id != 30) style="display: none" @endif >
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.address_complement')</label>
                                        <input type="text" placeholder="@lang('site.enter_address_complement')" class="@if ($user->status == 'A') required @endif personal-type" value="{{ old('complement',@Auth::guard('web')->user()->complement) }}" name="complement">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 acshow" @if(auth()->user()->country_id != 30) style="display: none" @endif>
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.district_name')</label>
                                        <input type="text" placeholder="@lang('site.enter_district')" class="@if ($user->status == 'A') required @endif personal-type" value="{{ old('district',@Auth::guard('web')->user()->district) }}" name="district">
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6 col-sm-12 acshow" @if(auth()->user()->country_id != 30) style="display: none" @endif>
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.zipcode')</label>
                                        <input type="text" placeholder="@lang('site.enter_zip')" class="@if ($user->status == 'A') required @endif personal-type" value="{{ old('zipcode',@Auth::guard('web')->user()->zipcode) }}" name="zipcode">
                                    </div>
                                </div>

                            </div>
                            <div class="row" >
                                <div class="col-sm-12 papalshow" @if(auth()->user()->country_id == 30 || auth()->user()->country_id == 0) style="display: none" @endif>
                                    {{-- <h4>
                                        @lang('client_site.add_bank_account')
                                        <small class="tooltip">
                                            <i class="fa fa-question-circle" aria-hidden="true"></i>
                                            <span class="tooltiptext tooltip-right">@lang('client_site.tooltip_add_bank_account')</span>
                                        </small>
                                    </h4> --}}
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 papalshow" @if(auth()->user()->country_id == 30 || auth()->user()->country_id == 0) style="display: none" @endif>
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.paypal_address')<small class="tooltip">
                                            <i class="fa fa-question-circle" aria-hidden="true"></i>
                                            <span class="tooltiptext tooltip-right">@lang('client_site.tooltip_add_paypal_address')</span>
                                        </small>
                                    </label>

                                        <input type="text" placeholder="@lang('site.enter_paypal_address')" id="paypal_address" name="paypal_address" class="required personal-type" value="{{ old('paypal_address', auth()->user()->paypal_address) }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 acshow" @if(auth()->user()->country_id != 30) style="display: none" @endif>
                                    <h4>
                                        @lang('client_site.add_bank_account')
                                        <small class="tooltip">
                                            <i class="fa fa-question-circle" aria-hidden="true"></i>
                                            <span class="tooltiptext tooltip-right">@lang('client_site.tooltip_add_bank_account')</span>
                                        </small>
                                    </h4>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 acshow" @if(auth()->user()->country_id != 30) style="display: none" @endif>
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.bank_name')</label>

                                        <select class="required personal-type personal-select" name="bank_name" id="bank_name">
                                            <option value="">@lang('client_site.select_your_bank')</option>
                                            @foreach(@$bankList as $bl)
                                                <option value="{{@$bl->bank_name}}" @if(@$bl->bank_name==@$bank->bankName || old('bank_name')==@$bl->bank_name ) selected @endif>{{@$bl->bank_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 acshow" @if(auth()->user()->country_id != 30) style="display: none" @endif>
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.account_number')</label>
                                        <input type="text" placeholder="@lang('site.enter_account_number')" id="account_number" name="account_number" class="required personal-type" value="{{ old('account_number',@$bank->accountNumber) }}">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 acshow" @if(auth()->user()->country_id != 30) style="display: none" @endif>
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.agency_number')</label>
                                        <input type="text" id="agency_number" placeholder="@lang('site.enter_agency_number')" name="agency_number" class="required personal-type" value="{{ old('agency_number',@$bank->agencyNumber) }}">
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6 col-sm-12 acshow" @if(auth()->user()->country_id != 30) style="display: none" @endif>
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.bank_number')</label>
                                        <select class="required personal-type personal-select" name="bank_number" id="bank_number">
                                            <option value="">@lang('client_site.select_your_bank_number')</option>
                                            @foreach(@$bankList as $bl)
                                                <option value="{{@$bl->bank_number}}" @if(@$bl->bank_number==@$bank->bank_number || old('bank_number')==@$bank->bank_number ) selected @endif>{{'('.@$bl->bank_number.') - '.@$bl->bank_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6 col-sm-12 acshow" @if(auth()->user()->country_id != 30) style="display: none" @endif>
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.account_check_number')</label>
                                        <input type="text" id="account_check_number" placeholder="@lang('site.enter_account_check_number')" name="account_check_number" class="required personal-type" value="{{ old('account_check_number',@$bank->accountCheckNumber) }}">
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6 col-sm-12 acshow" @if(auth()->user()->country_id != 30) style="display: none" @endif>
                                    <div class="form-group">
                                        <label class="personal-label">@lang('site.account_type')</label>
                                        <select name="account_type" class="required personal-type personal-select">
                                            <option value="">@lang('site.account_type')</option>
                                            <option @if (@$bank->account_type == 'SAVING') selected="" @endif value="SAVING">Poupança</option>
                                            <option @if (@$bank->account_type == 'CHECKING') selected="" @endif value="CHECKING">Conta corrente</option>

                                        </select>
                                        {{-- <input type="text" id="account_check_number" placeholder="@lang('site.enter_account_check_number')" name="account_check_number" class="required personal-type" value="{{@$bank->accountCheckNumber}}"> --}}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6 show-div" @if(auth()->user()->country_id == 0) style="display: none" @endif>
                                    <input type="checkbox" name="terms_condiditon" class=" required">
                                    <span>
                                        {{__('site.aff_video_accept')}} <a href="{{route('terms.of.services')}}" target="_blank">{{__('site.terms_of_service')}}</a>
                                    </span>
                                    <br>
                                    <label id ="terms_condiditon-error"for="terms_condiditon" class="error" style="display: none">{{__('site.terms_accept')}} </label>
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="login_submitt" id="update">@lang('client_site.save')</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    {{-- @if(auth()->user()->country_id == 30)
                        <div class="form_body">
                            <div class="row">
                                @if(!@Auth::guard('web')->user()->bankAccount->accountCheckNumber)
                                    <div class="col-sm-12">
                                        <h4>
                                            @lang('client_site.add_bank_account')
                                            <small class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">@lang('client_site.tooltip_add_bank_account')</span>
                                            </small>
                                        </h4>
                                    </div>
                                @else
                                    <div class="col-sm-12">
                                        <h4>@lang('client_site.update_bank_account')</h4>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <form id="myForm111" name="myForm111" method="post" action="{{ route('set.bank') }}">
                            @csrf
                            <div class="form_body">
                                <div class="row">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.bank_name')</label>

                                        <select class="required personal-type personal-select" name="bank_name" id="bank_name">
                                            <option value="">@lang('client_site.select_your_bank')</option>
                                            @foreach(@$bankList as $bl)
                                                <option value="{{@$bl->bank_name}}" @if(@$bl->bank_name==@$bank->bankName || old('bank_name')==@$bl->bank_name ) selected @endif>{{@$bl->bank_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.account_number')</label>
                                        <input type="text" placeholder="@lang('site.enter_account_number')" id="account_number" name="account_number" class="required personal-type" value="{{ old('account_number',@$bank->accountNumber) }}">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.agency_number')</label>
                                        <input type="text" id="agency_number" placeholder="@lang('site.enter_agency_number')" name="agency_number" class="required personal-type" value="{{ old('agency_number',@$bank->agencyNumber) }}">
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.bank_number')</label>
                                        <select class="required personal-type personal-select" name="bank_number" id="bank_number">
                                            <option value="">@lang('client_site.select_your_bank_number')</option>
                                            @foreach(@$bankList as $bl)
                                                <option value="{{@$bl->bank_number}}" @if(@$bl->bank_number==@$bank->bank_number || old('bank_number')==@$bank->bank_number ) selected @endif>{{'('.@$bl->bank_number.') - '.@$bl->bank_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('client_site.account_check_number')</label>
                                        <input type="text" id="account_check_number" placeholder="@lang('site.enter_account_check_number')" name="account_check_number" class="required personal-type" value="{{ old('account_check_number',@$bank->accountCheckNumber) }}">
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="personal-label">@lang('site.account_type')</label>
                                        <select name="account_type" class="required personal-type personal-select">
                                            <option value="">@lang('site.account_type')</option>
                                            <option @if (@$bank->account_type == 'SAVING') selected="" @endif value="SAVING">Poupança</option>
                                            <option @if (@$bank->account_type == 'CHECKING') selected="" @endif value="CHECKING">Conta corrente</option>

                                        </select>
                                    </div>
                                </div>

                                    <div class="col-sm-12">
                                        <button type="submit" id="saveBank" class="login_submitt">{{@$bank->accountCheckNumber ? "Atualizar conta bancária": "Adicionar conta bancária"}}</button>
                                    </div>

                                </div>
                            </div>
                        </form>
                    @else
                        <div class="form_body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4>@lang('client_site.add_paypal_address')</h4>
                                </div>

                            </div>
                        </div>
                        <form action="{{route('update.paypal.address')}}" method="post" id="paypal_form">
                            @csrf
                            <div class="form_body">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="personal-label">@lang('client_site.paypal_address')<small class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">@lang('client_site.tooltip_add_paypal_address')</span>
                                            </small>
                                        </label>

                                            <input type="text" placeholder="@lang('site.enter_paypal_address')" id="paypal_address" name="paypal_address" class="required personal-type" value="{{ old('paypal_address', auth()->user()->paypal_address) }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <button type="submit" class="login_submitt">@lang('client_site.save')</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    @endif --}}
                </div>

            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
    @include('includes.footer')
    <link rel="stylesheet" href="{{ URL::to('public/frontend/css/croppie.min.css') }}">
    <style type="text/css">
        .upldd {
            display: block;
            width: auto;
            border-radius: 4px;
            text-align: center;
            background: #9caca9;
            cursor: pointer;
            overflow: hidden;
            padding: 10px 15px;
            font-size: 15px;
            color: #fff;
            cursor: pointer;
            float: left;
            margin-top: 15px;
            font-family: 'Poppins', sans-serif;
            position: relative;
        }
        .upldd input {
            position: absolute;
            font-size: 50px;
            opacity: 0;
            left: 0;
            top: 0;
            width: 200px;
            cursor: pointer;
        }
        .upldd:hover{
            background: #1781d2;
        }
    </style>

    <script src="{{ URL::to('public/frontend/js/bank-account-validator.min.js') }}"></script>
    <script src="{{ URL::to('public/frontend/js/croppie.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/frontend/js/numeric-1.2.6.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/frontend/js/bezier.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/frontend/js/jquery.signaturepad.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/frontend/js/html2canvas.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/frontend/js/json2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/frontend/js/jquery-ui.min.js') }}"></script>

    <script>

        $(document).ready(function() {
            $('#myForm111').validate({
                rules:{
                    // bank_number:{
                    //     digits:true,
                    //     minlength:3,
                    //     maxlength:3
                    // },
                    account_number:{
                        digits:true,
                        minlength:1,
                        maxlength:12
                    },
                    account_check_number:{
                        digits:true,
                        minlength:1,
                        maxlength:1
                    },
                    agency_number:{
                        digits:true,
                        // minlength:5,
                        // maxlength:5
                    }

                }
            });
            jQuery.validator.addMethod("dimention", function (value, element, param) {
                var width = $(element).data('imageWidth');
                var height = $(element).data('imageHeight');
                if(width < param[0] && height < param[1]){
                    return true;
                }else if(width==undefined){
                    return true;
                }else{
                    return false;
                }
            }, 'O tamanho da imagem deve ser 300 x 200');

            jQuery.validator.addClassRules("nameExistErrClass", {
                dimention:[1300, 1200]
            });
            $('#signature').change(function() {
                $('#signature').removeData('imageWidth');
                $('#signature').removeData('imageHeight');
                var file = this.files[0];
                var tmpImg = new Image();
                tmpImg.src=window.URL.createObjectURL( file );
                tmpImg.onload = function() {
                    width = tmpImg.naturalWidth,
                    height = tmpImg.naturalHeight;
                    $('#signature').data('imageWidth', width);
                    $('#signature').data('imageHeight', height);
                }
            });
            jQuery.validator.addMethod("alphanumeric", function(value, element) {
                return this.optional(element) || /^[a-zA-Z0-9]*$/.test(value);
            }, "Insira um caractere alfanumérico válido.");
            // jQuery.validator.addMethod("alphanumericspace", function(value, element) {
            //     return this.optional(element) || /^[A-Z0-9 ]*[A-Z0-9][A-Z0-9 ]*$/.test(value);
            // },"Adicione caracteres alfanuméricos ou espaços.");
            jQuery.validator.addMethod("alphanumberspace", function(value, element) {
                return this.optional(element) || /^[A-Z0-9 ]*$/.test(value);
            },"Adicione caracteres alfanuméricos ou espaços.");
            $('#myForm').validate({
                rules:{
                    phone_no:{
                        digits:true,
                        minlength:9,
                        maxlength:9
                    },
                    zipcode:{
                        // digits:true,
                        required: true,
                        minlength:4,
                        maxlength:10
                    },
                    area_code:{
                        digits:true,
                        minlength:2,
                        maxlength:2
                    },
                    street_number:{
                        alphanumberspace:true,
                        // minlength:3,
                        // maxlength:3
                    },
                    // bank_number:{
                    //     digits:true,
                    //     minlength:3,
                    //     maxlength:3
                    // },
                    account_number:{
                        digits:true,
                        minlength:1,
                        maxlength:12
                    },
                    account_check_number:{
                        digits:true,
                        minlength:1,
                        maxlength:1
                    },
                    agency_number:{
                        digits:true,
                        // minlength:5,
                        // maxlength:5
                    }
                }
            });
        });

        $('#phone_no').change(function(){
            if($('#phone_no').val()!=""){
                var reqData = {
                  'jsonrpc' : '2.0',
                  '_token' : '{{csrf_token()}}',
                  'params' : {
                        'no' : $('#phone_no').val()
                    }
                };
                $.ajax({
                    url: "{{ route('chk.mobile') }}",
                    method: 'post',
                    dataType: 'json',
                    data: reqData,
                    success: function(response){
                        if(response.status==1) {
                            $('#phone_no').val("");
                            $('#ph_error').text('@lang('client_site.phone_number_allready_exist')');
                        }

                    }, error: function(error) {
                        toastr.info('@lang('client_site.Try_again_after_sometime')');
                    }
                });
            }
        });


        $('#country').change(function(){
            if($('#country').val()!=""){
                if($('#country').val()==30){
                    $('.acshow').css('display','block');
                    $('.show-div').css('display','block');
                    $('.papalshow').css('display','none');
                }else{
                    $('.acshow').css('display','none');
                    $('.papalshow').css('display','block');
                    $('.show-div').css('display','block');
                }

                var reqData = {
                  'jsonrpc' : '2.0',
                  '_token' : '{{csrf_token()}}',
                  'params' : {
                        'cn' : $('#country').val()
                    }
                };
                $.ajax({
                    url: "{{ route('front.get.state') }}",
                    method: 'post',
                    dataType: 'json',
                    data: reqData,
                    success: function(response){
                        if(response.status==1) {
                            var i=0, html="";
                            html = '<option value="">@lang('client_site.select_state')</option>';
                            for(;i<response.result.length;i++){
                                html+='<option value='+response.result[i].id+'>'+response.result[i].name+'</option>';
                            }
                            $('#state').html(html);
                            $('#state').addClass('valid');
                        }
                        else{

                        }
                    }, error: function(error) {
                        console.error(error);
                    }
                });
            }else{
                $('.acshow').css('display','none');
                $('.papalshow').css('display','none');
                $('.show-div').css('display','none');
            }
        });
        $(document).ready(function () {
            jQuery.validator.addMethod("emailonly", function(value, element) {
                return this.optional(element) || /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+$/.test(value.toLowerCase());
            }, "Please enter a valid email.");
            $('#paypal_form').validate({
                rules:{
                    paypal_address:{
                        email:true,
                        emailonly:true,
                    },
                }
            });
        });

    </script>

@endsection
