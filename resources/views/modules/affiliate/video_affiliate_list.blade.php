@extends('layouts.app')
@section('title')

@lang('client_site.video_affiliate_program')
@endsection
@section('style')
@include('includes.style')
<style>
    .shere_li{
        margin-right: 7px !important;
        background: none !important;
        padding: 0px !important;
        border-radius: 0px !important;
        box-shadow: none !important;
        margin-bottom: 0px !important;
    }
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')
<section class="bkng-hstrybdy">
    <div class="container custom_container">
        <h2>@lang('client_site.video_affiliate_program')</h2>
        <div class="bokcntnt-bdy">
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.user_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="from-field">
                    <div class="weicome">
                        {{-- <a href="{{route('video.affiliate.share.link',['slug'=>auth()->user()->slug])}}">
                            <img src="{{ URL::to('public/frontend/images/details-blog.png') }}" alt="" >
                        </a>
                        @php
                        $code='<a href="'.route('video.affiliate.share.link',['slug'=>auth()->user()->slug]).'">';
                        $code=$code.'<img src="'.URL::to('public/frontend/images/details-blog.png') .'" alt="" style="width: 100%"></a>';
                        @endphp
                        <textarea style="width: 100%">{!!$code!!}
                        </textarea>
                        <a href="javascript:;" onclick="copyLink()">Copy Code</a> --}}
                        {{-- <h3>{{ @$dashBoardPage->title }} {{ @Auth::guard('web')->user()->nick_name ? @Auth::guard('web')->user()->nick_name : @Auth::guard('web')->user()->name }}</h3>
                        <p style="white-space: pre-wrap;">{!! @$dashBoardPage->description !!}</p> --}}
                    </div>
                    <div class="dash_main">
                        {{-- <center><p class="alert alert-info">Your account associated with our video affiliate program  , <a href="javascript:;" onclick="copyLink('{{route('video.affiliate.share.link',['slug'=>auth()->user()->slug])}}')">copy link</a></p></center> --}}
                        {{-- <a href="javascript:;" onclick="copyLink('{{route('video.affiliate.share.link',['slug'=>auth()->user()->slug])}}')">
                            <img src="{{ URL::to('public/frontend/images/details-blog.png') }}" alt="" style="width: 100%">
                        </a> --}}

                        <ul>
                            <li>
                                <span><img src="{{ URL::to('public/frontend/images/dashboard_icon1.png') }}" alt=""></span>
                                <h4>{{__('client_site.video_affiliate_total_earning')}}</h4>
                                <h3>R$ {{auth()->user()->video_aff_earning}}</h3>
                            </li>
                            <li>
                                <span><img src="{{ URL::to('public/frontend/images/dashboard_icon2.png') }}" alt=""></span>
                                <h4>{{__('client_site.video_affiliate_total_paid')}}</h4>
                                <h3>R$  {{auth()->user()->video_aff_paid}}</h3>
                            </li>
                            <li>
                                <span><img src="{{ URL::to('public/frontend/images/dashboard_icon3.png') }}" alt=""></span>
                                <h4>{{__('client_site.video_affiliate_total_due')}}</h4>
                                <h3>R$  {{auth()->user()->video_aff_earning-auth()->user()->video_aff_paid}}</h3>
                            </li>
                            <li>
                                <span><img src="{{ URL::to('public/frontend/images/dashboard_icon4.png') }}" alt=""></span>
                                <h4>{{__('client_site.video_affiliate_no_of_sign_up')}}</h4>
                                <h3>{{@$totalShare}}</h3>
                            </li>
                            <li>
                                <span><img src="{{ URL::to('public/frontend/images/dashboard_icon4.png') }}" alt=""></span>
                                <h4>{{__('client_site.video_affiliate_no_of_clicks')}}</h4>
                                <h3>{{auth()->user()->aff_link_click}}</h3>
                            </li>
                        </ul>
                        <div class="aff-link">
                            <a class="btn btn-success" href="{{route('video.affiliate.all.payment.list')}}">{{__('client_site.video_affiliate_payment_btn')}}</a>
                            <a class="btn btn-success" href="{{route('video.affiliate.all.earning.list')}}">{{__('client_site.video_affiliate_earning_btn')}}</a>
                        </div>
                    </div>
                    <div class="dash_main">
                        {{-- <h4>Referral URL with Banner You can share this image along with link in social media or in your web site: </h4> --}}
                        @php
                        $code='<a href="'.route('video.affiliate.share.link',['slug'=>auth()->user()->v_aff_slug]).'">';
                        $code=$code.'<img src="'.URL::to('storage/app/public/uploads/banner/banner1.png') .'" alt=""></a>';
                        $code2='<a href="'.route('video.affiliate.share.link',['slug'=>auth()->user()->v_aff_slug]).'">';
                        $code2=$code2.'<img src="'.URL::to('storage/app/public/uploads/banner/banner2.png') .'" alt=""></a>';
                        $code3='<a href="'.route('video.affiliate.share.link',['slug'=>auth()->user()->v_aff_slug]).'">';
                        $code3=$code3.'<img src="'.URL::to('storage/app/public/uploads/banner/banner3.png') .'" alt=""></a>';
                        $code4='<a href="'.route('video.affiliate.share.link',['slug'=>auth()->user()->v_aff_slug]).'">';
                        $code4=$code4.'<img src="'.URL::to('storage/app/public/uploads/banner/banner4.png') .'" alt=""></a>';
                        $code5='<a href="'.route('video.affiliate.share.link',['slug'=>auth()->user()->v_aff_slug]).'">';
                        $code5=$code5.'<img src="'.URL::to('storage/app/public/uploads/banner/banner5.png') .'" alt=""></a>';
                        $code6='<a href="'.route('video.affiliate.share.link',['slug'=>auth()->user()->v_aff_slug]).'">';
                        $code6=$code6.'<img src="'.URL::to('storage/app/public/uploads/banner/banner6.png') .'" alt=""></a>';
                        @endphp
                        <div class="form_body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                    <label class="personal-label"> @lang('client_site.referral_url_with_banner') : </label>
                                    <h6>@lang('client_site.referral_url_with_banner_text') </h6>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12"> @lang('client_site.banner_1') ( Tamanho de banner recomendado 300 × 250 )<br>
                                <a href="{{route('video.affiliate.share.link',['slug'=>auth()->user()->v_aff_slug])}}">
                                    <img src="{{ URL::to('storage/app/public/uploads/banner/banner1.png') }}" alt="" style="max-width: 100%">
                                </a>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                    <label class="personal-label"> Code :  &nbsp;&nbsp;&nbsp;<a href="javascript:;" onclick="copyCode('{{@$code}}')" class="rjct">@lang('client_site.copy_code') </a></label>
                                        <textarea class="short_description personal-type99" cols="100" rows="3"> {{@$code}}</textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    @lang('client_site.banner_2') ( Tamanho de banner recomendado 728 × 90 )<br>
                                <a href="{{route('video.affiliate.share.link',['slug'=>auth()->user()->v_aff_slug])}}">
                                    <img src="{{ URL::to('storage/app/public/uploads/banner/banner2.png') }}" alt="" style="max-width: 100%">
                                </a>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                    <label class="personal-label"> Code : &nbsp;&nbsp;&nbsp;<a href="javascript:;" onclick="copyCode('{{@$code2}}')" class="rjct">@lang('client_site.copy_code') </a></label>
                                        <textarea class="short_description personal-type99" cols="100" rows="3"> {{@$code2}}</textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    @lang('client_site.banner_3') ( Tamanho de banner recomendado 970 × 250)<br>
                                <a href="{{route('video.affiliate.share.link',['slug'=>auth()->user()->v_aff_slug])}}">
                                    <img src="{{ URL::to('storage/app/public/uploads/banner/banner3.png') }}" alt="" style="max-width: 100%">
                                </a>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                    <label class="personal-label"> Code : &nbsp;&nbsp;&nbsp;<a href="javascript:;" onclick="copyCode('{{@$code3}}')" class="rjct">@lang('client_site.copy_code') </a>  </label>
                                        <textarea class="short_description personal-type99" cols="100" rows="3"> {{@$code3}}</textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    @lang('client_site.banner_4') ( Tamanho de banner recomendado 300 × 600)<br>
                                <a href="{{route('video.affiliate.share.link',['slug'=>auth()->user()->v_aff_slug])}}">
                                    <img src="{{ URL::to('storage/app/public/uploads/banner/banner4.png') }}" alt="" style="max-width: 100%">
                                </a>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                    <label class="personal-label"> Code : &nbsp;&nbsp;&nbsp;<a href="javascript:;" onclick="copyCode('{{@$code4}}')" class="rjct">@lang('client_site.copy_code') </a>  </label>
                                        <textarea class="short_description personal-type99" cols="100" rows="3"> {{@$code4}}</textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    @lang('client_site.banner_5') ( Tamanho de banner recomendado 200 × 200)<br>
                                <a href="{{route('video.affiliate.share.link',['slug'=>auth()->user()->v_aff_slug])}}">
                                    <img src="{{ URL::to('storage/app/public/uploads/banner/banner5.png') }}" alt="" style="max-width: 100%">
                                </a>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                    <label class="personal-label"> Code : &nbsp;&nbsp;&nbsp;<a href="javascript:;" onclick="copyCode('{{@$code5}}')" class="rjct">@lang('client_site.copy_code') </a>  </label>
                                        <textarea class="short_description personal-type99" cols="100" rows="3"> {{@$code5}}</textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    @lang('client_site.banner_6') ( Tamanho de banner recomendado 970 × 90)<br>
                                <a href="{{route('video.affiliate.share.link',['slug'=>auth()->user()->v_aff_slug])}}">
                                    <img src="{{ URL::to('storage/app/public/uploads/banner/banner6.png') }}" alt="" style="max-width: 100%">
                                </a>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                    <label class="personal-label"> @lang('client_site.code') : &nbsp;&nbsp;&nbsp;<a href="javascript:;" onclick="copyCode('{{@$code6}}')" class="rjct">@lang('client_site.copy_code') </a>  </label>
                                        <textarea class="short_description personal-type99" cols="100" rows="3"> {{@$code6}}</textarea>
                                    </div>
                                </div>



                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                    <label class="personal-label"> @lang('client_site.referral_url') :  &nbsp;&nbsp;&nbsp;<a href="javascript:;" onclick="copyLink('{{route('video.affiliate.share.link',['slug'=>auth()->user()->v_aff_slug])}}')" class="rjct">@lang('site.copy_link')</a></label>

                                        {{-- <input type="text" class="personal-type" value="{{route('video.affiliate.share.link',['slug'=>auth()->user()->slug])}}" style="width:100%;"> --}}
                                        <h5>{{route('video.affiliate.share.link',['slug'=>auth()->user()->v_aff_slug])}}</h5>
                                    </div>
                                    <ul class="fot-social">
                                        <li class="shere_li">@lang('site.link_to_share'):</li>
                                        <li class="shere_li"><a href="https://www.facebook.com/sharer.php?u={{route('video.affiliate.share.link',['slug'=>auth()->user()->v_aff_slug])}}"  target="_blank"><img src="{{ URL::to('public/frontend/images/social1.png') }}" alt=""></a></li>
                                        <li class="shere_li" ><a href="https://www.linkedin.com/shareArticle?url={{route('video.affiliate.share.link',['slug'=>auth()->user()->v_aff_slug])}}"  target="_blank"><img src="{{ URL::to('public/frontend/images/social6.png') }}" alt=""></a></li>
                                        <li class="shere_li" ><a href="https://api.whatsapp.com/send?text={{route('video.affiliate.share.link',['slug'=>auth()->user()->v_aff_slug])}}" target="_blank"><img src="{{ URL::to('public/frontend/images/whatsapp.png') }}" alt=""></a></li>
                                        <li class="shere_li"><a href="https://twitter.com/share?url={{route('video.affiliate.share.link',['slug'=>auth()->user()->v_aff_slug])}}"  target="_blank"><img src="{{ URL::to('public/frontend/images/social2.png') }}" alt=""></a></li>
                                    </ul>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>
                {{-- <div class="buyer_table">
                    <div class="table-responsive">
                    @if(sizeof(@$earnings)>0)
                        <div class="table">
                            <div class="one_row1 hidden-sm-down only_shawo">
                                <!-- <div class="cell1 tab_head_sheet">@lang('site.id').</div> -->
                                <div class="cell1 tab_head_sheet">#</div>
                                <div class="cell1 tab_head_sheet">@lang('client_site.video_affiliate_order_no</div>
                                <div class="cell1 tab_head_sheet">@lang('client_site.user_name')</div>
                                <div class="cell1 tab_head_sheet">@lang('client_site.amount')</div>
                            </div>
                            <!--row 1-->
                                @foreach(@$earnings as $k=>$row)

                                 <div class="one_row1 small_screen31">
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">#</span>
                                            <p class="add_ttrr">{{(@$k+1)}}</p>
                                        </div>

                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('site.category')</span>
                                            <p class="add_ttrr">{{@$row->bookingDetails->token_no}}</p>
                                        </div>

                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('client_site.user_name')</span>
                                            <p class="add_ttrr">{{ @$row->userDetails->nick_name ?? @$row->userDetails->name }}</p>
                                        </div>

                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('client_site.earning')</span>
                                            <p class="add_ttrr">{{ @$row->amount }}</p>
                                        </div>

                                    </div>

                                @endforeach



                        </div>
                    @else
                        <center>
                            <h3 class="error">@lang('site.product_not_found') !</h3>
                        </center>
                    @endif

                    </div>
                </div> --}}
            </div>
        </div>
    </div>

</section>
@endsection
@section('footer')
@include('includes.footer')
<link href="{{ URL::to('public/frontend/css/calender.css') }}" rel="stylesheet" type="text/css">
<script src="{{ URL::to('public/frontend/js/jquery-ui.js') }}"></script>
<script src="{{ URL::to('public/frontend/js/moment.min.js') }}"></script>
<script>
    $('.aff_rem').click(function(){
        var thisaff = $(this);
        var productId = $(this).data('product');
        $.ajax({
            url: "{{ url('/') }}/affiliate-products-remove-list/"+productId,
            type: 'get',
            dataType: 'json',
        })
        .done(function(response) {
            console.log(response);
            if(response.status == 'success'){
                // toastr.success(response.message);
                location.reload();
            } else {
                toastr.error(response.message);
            }
        })
        .fail(function(error) {
            console.log(response);
        });
    });

    function copyCode(text){
        var input = document.createElement('input');
        input.setAttribute('value', text);
        document.body.appendChild(input);
        input.select();
        var result = document.execCommand('copy');
        document.body.removeChild(input);
        toastr.success("Code Copied");
        return result;
    }
    function copyLink(text){
        var input = document.createElement('input');
        input.setAttribute('value', text);
        document.body.appendChild(input);
        input.select();
        var result = document.execCommand('copy');
        document.body.removeChild(input);
        toastr.success("Link Copied");
        return result;
    }
</script>

@endsection
