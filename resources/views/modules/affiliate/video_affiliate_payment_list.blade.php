@extends('layouts.app')
@section('title')
{{__('client_site.video_affiliate_payment')}}
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')
<section class="bkng-hstrybdy">
    <div class="container custom_container">
        <h2>{{__('client_site.video_affiliate_payment')}}</h2>
        <div class="bokcntnt-bdy">
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.user_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="from-field">
                    <div class="weicome">
                        {{-- <a href="{{route('video.affiliate.share.link',['slug'=>auth()->user()->slug])}}">
                            <img src="{{ URL::to('public/frontend/images/details-blog.png') }}" alt="" >
                        </a>
                        @php
                        $code='<a href="'.route('video.affiliate.share.link',['slug'=>auth()->user()->slug]).'">';
                        $code=$code.'<img src="'.URL::to('public/frontend/images/details-blog.png') .'" alt="" style="width: 100%"></a>';
                        @endphp
                        <textarea style="width: 100%">{!!$code!!}
                        </textarea>
                        <a href="javascript:;" onclick="copyLink()">Copy Code</a> --}}
                        {{-- <h3>{{ @$dashBoardPage->title }} {{ @Auth::guard('web')->user()->nick_name ? @Auth::guard('web')->user()->nick_name : @Auth::guard('web')->user()->name }}</h3>
                        <p style="white-space: pre-wrap;">{!! @$dashBoardPage->description !!}</p> --}}
                    </div>
                    {{-- <div class="dash_main">
                        <h4>Referral URL with Banner You can share this image along with link in social media or in your web site: </h4>
                        @php
                        $code='<a href="'.route('video.affiliate.share.link',['slug'=>auth()->user()->slug]).'">';
                        $code=$code.'<img src="'.URL::to('public/frontend/images/categori1.jpg') .'" alt=""></a>';
                        @endphp
                        <div class="form_body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                <a href="{{route('video.affiliate.share.link',['slug'=>auth()->user()->slug])}}">
                                    <img src="{{ URL::to('public/frontend/images/categori1.jpg') }}" alt="" style="max-width: 100%">
                                </a>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                    <label class="personal-label"> Code :  &nbsp;&nbsp;&nbsp;<a href="javascript:;" onclick="copyCode('{{@$code}}')" class="rjct">Copy Code</a></label>
                                        <textarea class="short_description personal-type99" cols="100" rows="3"> {{@$code}}</textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                    <label class="personal-label"> Referral URL :  &nbsp;&nbsp;&nbsp;<a href="javascript:;" onclick="copyLink('{{route('video.affiliate.share.link',['slug'=>auth()->user()->slug])}}')" class="rjct">Copy Url</a></label>

                                        <input type="text" class="personal-type" value="{{route('video.affiliate.share.link',['slug'=>auth()->user()->slug])}}" style="width:100%;">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div> --}}
                    <div class="dash_main">
                        <ul>
                            <li>
                                <span><img src="{{ URL::to('public/frontend/images/dashboard_icon1.png') }}" alt=""></span>
                                <h4>{{__('client_site.video_affiliate_total_earning')}}</h4>
                                <h3>R$ {{auth()->user()->video_aff_earning}}</h3>
                            </li>
                            <li>
                                <span><img src="{{ URL::to('public/frontend/images/dashboard_icon2.png') }}" alt=""></span>
                                <h4>{{__('client_site.video_affiliate_total_paid')}}</h4>
                                <h3>R$  {{auth()->user()->video_aff_paid}}</h3>
                            </li>
                            <li>
                                <span><img src="{{ URL::to('public/frontend/images/dashboard_icon3.png') }}" alt=""></span>
                                <h4>{{__('client_site.video_affiliate_total_due')}}</h4>
                                <h3>R$  {{auth()->user()->video_aff_earning-auth()->user()->video_aff_paid}}</h3>
                            </li>
                            <li>
                                <span><img src="{{ URL::to('public/frontend/images/dashboard_icon4.png') }}" alt=""></span>
                                <h4>{{__('client_site.video_affiliate_no_of_sign_up')}}</h4>
                                <h3>{{@$totalShare}}</h3>
                            </li>
                            <li>
                                <span><img src="{{ URL::to('public/frontend/images/dashboard_icon4.png') }}" alt=""></span>
                                <h4>{{__('client_site.video_affiliate_no_of_clicks')}}</h4>
                                <h3>{{auth()->user()->aff_link_click}}</h3>
                            </li>
                            <div class="aff-link">
                                {{-- <a class="btn btn-success" href="{{route('video.affiliate.all.payment.list')}}">My Video Affiliate Payments</a> --}}
                                <a class="btn btn-success" href="{{route('video.affiliate.earning.list')}}">{{__('client_site.video_affiliate_program_btn')}}</a>
                                <a class="btn btn-success" href="{{route('video.affiliate.all.earning.list')}}">{{__('client_site.video_affiliate_earning_btn')}}</a>
                            </div>
                        </ul>
                    </div>
                </div>
                <div class="buyer_table">
                    <div class="table-responsive">
                    @if(sizeof(@$payment)>0)
                        <div class="table">
                            <div class="one_row1 hidden-sm-down only_shawo">
                                <div class="cell1 tab_head_sheet">{{__('client_site.video_affiliate_payment_date')}}</div>
                                <div class="cell1 tab_head_sheet">{{__('client_site.video_affiliate_amount')}}</div>
                                <div class="cell1 tab_head_sheet">{{__('client_site.video_affiliate_note')}}</div>
                            </div>
                            <!--row 1-->
                                @foreach(@$payment as $k=>$row)

                                 <div class="one_row1 small_screen31">
                                    <div class="cell1 tab_head_sheet_1">
                                        <span class="W55_1">{{__('client_site.video_affiliate_payment_date')}}</span>
                                        <p class="add_ttrr">{{ toUserTime($row->created_at,'d-m-Y') }}</p>
                                    </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">{{__('client_site.video_affiliate_amount')}}</span>
                                            <p class="add_ttrr">{{@$row->amount}}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">{{__('client_site.video_affiliate_note')}}</span>
                                            <p class="add_ttrr">{{ @$row->note }}</p>
                                        </div>
                                    </div>

                                @endforeach



                        </div>
                    @else
                        <center>
                            <h3 class="error">{{__('client_site.video_affiliate_no_payment_found')}} </h3>
                        </center>
                    @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
@endsection
@section('footer')
@include('includes.footer')
<link href="{{ URL::to('public/frontend/css/calender.css') }}" rel="stylesheet" type="text/css">
<script src="{{ URL::to('public/frontend/js/jquery-ui.js') }}"></script>
<script src="{{ URL::to('public/frontend/js/moment.min.js') }}"></script>
@endsection
