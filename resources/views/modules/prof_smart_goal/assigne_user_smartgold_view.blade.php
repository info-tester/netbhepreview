@extends('layouts.app')
@section('title')
    {{-- @lang('site.edit') @lang('site.blog')-@lang('site.post') --}}
    Smart Goal
@endsection
@section('style')
@include('includes.style')
<style type="text/css">
    .chck_eml_rd{
        color: red;
        font-weight: bold;
        font-size: 14px;
    }
</style>
@endsection
@section('scripts')
@include('includes.scripts')

@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>
            Objetivos SMART
            {{-- @lang('site.edit_form') --}}
            {{-- @lang('site.edit') @lang('site.blog')-@lang('site.post') --}}
        </h2>
        @include('includes.professional_tab_menu')
        <div class="bokcntnt-bdy no-margin-top">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.user_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">
                    <div class="mainDiv">
                        @if(sizeof(@$all_smart_goal_details)>0)
                            <div class="buyer_table">
                                <div class="table-responsive">
                                    <div class="table">
                                        <div class="one_row1 hidden-sm-down only_shawo">
                                            <div class="cell1 tab_head_sheet">Nome do usuário</div>
                                            <div class="cell1 tab_head_sheet">Ponto de Metas</div>
                                            <div class="cell1 tab_head_sheet">Data</div>
                                        </div>
                                        <!--row 1-->
                                        @foreach(@$all_smart_goal_details as $goal_details)
                                        <div class="one_row1 small_screen31">

                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">User name</span>
                                                <p class="add_ttrr">{{ @$goal_details->getUserData->nick_name ? @$goal_details->getUserData->nick_name : @$goal_details->getUserData->name }}</p>
                                            </div>

                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">goal_point</span>
                                                <p class="add_ttrr">{{ @$goal_details->goal_point }}</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">assign_date</span>
                                                <p class="add_ttrr">{{ date('m/d/Y (D)', strtotime(@$goal_details->created_at)) }}</p>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
   <script>
    $(document).ready(function(){
        $('.datepicker').datepicker();
        $("#myform").validate();
    });


    $(function() {
        $('#reminderdate').hide();
        $('#dayofweek').hide();
        $('#remindertype').change(function(){
            if($('#remindertype').val() == 'one time') {
                $('#reminderdate').show();
                $('#dayofweek').hide();
            }else if($('#remindertype').val() == 'weekly') {
                $('#dayofweek').show();
                $('#reminderdate').hide();
            }else {
                $('#reminderdate').hide();
                $('#dayofweek').hide();
            }
        });
    });

</script>


<style>
    .error{
        color: red !important;
    }
</style>

@endsection
