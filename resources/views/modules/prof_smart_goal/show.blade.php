@extends('layouts.app')
@section('title')
    @lang('site.post') @lang('site.details')
@endsection
@section('style')
@include('includes.style')
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>@lang('site.smart_Goal_View')</h2>

       @include('includes.professional_tab_menu')

        <div class="bokcntnt-bdy no-margin-top">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="blog-right details-right">
                    <div class="blog-box">

                        <div class="blog-dtls">

                            <div class="col-md-12 view-page coach_tools">
                                <p>
                                    <strong>@lang('site.Goal_title'):</strong>
                                    {{ $details->goal_title }}
                                </p>
                                <p>
                                    <strong>@lang('site.Goals_Point'):</strong>
                                    {{ $details->goal_point }}
                                </p>
                                <p>
                                    <strong>@lang('site.Current_point'):</strong>
                                    {{ $details->current_point }}
                                </p>
                                <p>
                                    <strong>@lang('site.Unit_measure'):</strong>
                                    {{ $details->unit_of_measure }}
                                </p>
                                <p>
                                    <strong>@lang('site.Start_date'):</strong>
                                    {{ $details->start_date }}
                                </p>
                                <p>
                                    <strong>@lang('site.end_date'):</strong>
                                    {{ $details->end_date }}
                                </p>
                                <p>
                                    <strong>@lang('site.reminder'):</strong>
                                    @if ($details->reminder == 'one time') @lang('site.reminder_one_time')
                                    @elseif ($details->reminder == 'weekly') @lang('site.reminder_weekly')
                                    @elseif ($details->reminder == 'daily') @lang('site.reminder_daily')
                                    @endif
                                </p>
                                @if($details->reminder == "one time")
                                    <p>
                                        <strong>@lang('site.reminder_date'):</strong>
                                        {{-- {{ date('Y-m-d', strtotime($request->reminder_date)) }}; --}}
                                        {{ $details->reminder_date }}
                                    </p>
                                @elseif($details->reminder == "weekly")
                                    <p>
                                        <strong>Day of week:</strong>
                                        {{ $details->day_of_week }}
                                    </p>
                                @endif
                            </div>

                        </div>
                    </div>

                </div>
            </div>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')
    <script>
        $(document).ready(function(){
            $('#myForm').validate();
        });
    </script>

@endsection
