@extends('layouts.app')
@section('title')
    {{-- @lang('site.add_new_blog') --}}
     Smart Goal
@endsection
@section('style')
@include('includes.style')
<style type="text/css">
    div.mce-edit-area {
        background:
        #FFF;
        filter: none;
        min-height: 317px;
    }
    .chck_eml_rd{
        color: red;
        font-weight: bold;
        font-size: 14px;
    }
    .tooltip {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted #ccc;
        color: #006080;
        opacity: 1 !important;
    }

    .tooltip .tooltiptext {
        visibility: hidden;
        position: absolute;
        width: 250px;
        background-color: #555;
        color: #fff;
        text-align: center;
        padding: 5px;
        border-radius: 6px;
        z-index: 1;
        opacity: 0;
        transition: opacity 0.3s;
        margin-left: 10px
    }

    .tooltip-right {
        top: 0px;
        left: 125%;
    }

    .tooltip-right::after {
        content: "";
        position: absolute;
        top: 10%;
        right: 100%;
        margin-top: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: transparent #555 transparent transparent;
    }

    .tooltip:hover .tooltiptext {
        visibility: visible;
        opacity: 1;
    }
</style>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection
@section('header')
@include('includes.professional_header')
@endsection
@section('content')

<section class="bkng-hstrybdy">
    <div class="container">
        <h2>Objetivos SMART</h2>

         @include('includes.professional_tab_menu')
        <div class="bokcntnt-bdy">
            @php
                $user = Auth::guard('web')->user();
                $user = $user->load('userQualification');
            @endphp
            @if(sizeof($user->userQualification)<=0)
                <center><p class="alert alert-info">@lang('client_site.please_complete_your_profile') @lang('site.by_entering_your_educational_information'), <a href="{{ route('professional_qualification') }}">@lang('site.click_here')</a></p></center>
            @endif
            <div class="mobile_filter">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>@lang('site.menu')</p>
            </div>
            @include('includes.professional_sidebar')
            <div class="dshbrd-rghtcntn">
                <div class="dash_form_box">

                    <form id="myform" method="post" action="{{ route('prof-smartgoal-tools.store') }}">
                        @csrf
                        <div class="form_body">
                            <div class="row">

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
                                    <div class="your-mail">
                                        <label for="exampleInputEmail1" class="personal-label">@lang('site.title') *</label>
                                        <input type="text" name="goal_title" class="personal-type required" id="goal_title" placeholder="@lang('site.title')" >
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
                                    <div class="your-mail">
                                        <label for="exampleInputEmail1" class="personal-label">
                                            @lang('site.goal_point') (Apenas números)*
                                            <div class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">{{__('site.goal_tooltip')}}</span>
                                            </div>
                                        </label>
                                        <input type="text" name="goal_point" id="goal_point" class="personal-type required digits" id="goal_point" placeholder="@lang('site.goal_point')" >
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
                                    <div class="your-mail">
                                        <label for="exampleInputEmail1" class="personal-label">
                                            @lang('site.current_point') (Apenas números)*
                                            <div class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">{{__('site.current_goal_tooltip')}}</span>
                                            </div>
                                        </label>
                                        <input type="text" name="current_point" id="current_point" class="personal-type required digits" id="current_point" placeholder="@lang('site.current_point')" >
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
                                    <div class="your-mail">
                                        <label for="exampleInputEmail1" class="personal-label">
                                            @lang('site.unit_of_measurement')  *
                                            <div class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">{{__('site.goal_unit_tooltip')}}</span>
                                            </div>
                                        </label>
                                        <input type="text" name="unit_of_measure" class="personal-type required" id="unit_of_measure" placeholder="@lang('site.unit_of_measurement')" >
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom:15px">
                                    <div class="your-mail">
                                        <label for="exampleInputEmail1" class="personal-label">@lang('site.Start_date')*</label>
                                        <input type="text" autocomplete="off" name="start_date" id="datepicker" class="personal-type fdte  required" placeholder="@lang('site.Start_date')">
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom:15px">
                                    <div class="your-mail">
                                        <label for="exampleInputEmail1" class="personal-label">@lang('site.end_date')*</label>
                                        <input type="text" autocomplete="off" name="end_date" id="datepicker1"  class="personal-type sdte  required" placeholder="@lang('site.end_date')">
                                        {{-- <input type="text" name="unit_of_measure" class="personal-type required" id="unit_of_measure" placeholder="Unit of measurement" > --}}
                                    </div>
                                </div>


                                {{-- <div class="col-lg-12 col-md-12 col-sm-12 todte">
                                    <label class="search_label">Date *</label>
                                    <div class="form-group">
                                        <input type="text" id="datepicker" class="dashboard-type fdte required" placeholder="From Date" name="from_date" readonly="">
                                        <img class="pstn" src="public/assets/images/clndr.png">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" id="datepicker1" class="dashboard-type sdte required" placeholder="To Date" name="to_date" readonly="">
                                        <img class="pstn" src="public/assets/images/clndr.png">
                                    </div>
                                </div> --}}


                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
                                    <div class="your-mail">
                                        <label for="exampleInputEmail1" class="personal-label">
                                            @lang('site.note')  *
                                            <div class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">{{__('site.goal_observation_tooltip')}}</span>
                                            </div>
                                        </label>
                                        <textarea name="note" class="personal-type required" id="note" placeholder="@lang('site.note_placeholder')"></textarea>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="your-mail">
                                        <label class="personal-label">
                                            @lang('site.reminder')
                                            <div class="tooltip">
                                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                <span class="tooltiptext tooltip-right">{{__('site.goal_reminder_tooltip')}}</span>
                                            </div>
                                        </label>
                                        <select class="personal-type personal-select" name="reminder" id="remindertype">
                                            <option value="">Selecionar</option>
                                            <option value="one time">@lang('site.reminder_one_time')</option>
                                            <option value="weekly">@lang('site.reminder_weekly')</option>
                                            <option value="daily">@lang('site.reminder_daily')</option>
                                        </select>

                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px" id="reminderdate">
                                    <div class="your-mail">
                                        <label for="exampleInputEmail1" class="personal-label">@lang('site.reminder_date') *</label>
                                        <input type="text" name="reminder_date" class="personal-type datepicker" id="reminder_date" placeholder="@lang('site.reminder_date')" >
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px" id="dayofweek">
                                    <div class="your-mail">
                                        <label for="exampleInputEmail1" class="personal-label">Dia da semana *</label>

                                        <select class="personal-type personal-select" name="day_of_week" id="day_of_week">
                                            <option value="">Selecionar</option>
                                            <option value="Monday">Segunda-feira</option>
                                            <option value="Tuesday">terça</option>
                                            <option value="Wednesday">Quarta-feira</option>
                                            <option value="Thursday">Quinta-feira</option>
                                            <option value="Friday">Sexta-feira</option>
                                            <option value="Saturday">sábado</option>
                                            <option value="Sunday">domingo</option>
                                        </select>
                                        {{-- <input type="text" name="day_of_week" class="personal-type" id="day_of_week" placeholder="Day of week" > --}}
                                    </div>
                                </div>


                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="submit-login add_btnm">
                                        <input value="Adicionar" type="submit" class="login_submitt">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('footer')
@include('includes.footer')

<style type="text/css">
</style>
<script>
    // (function($) {
    // $.fn.inputFilter = function(inputFilter) {
    //     return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
    //     if (inputFilter(this.value)) {
    //         this.oldValue = this.value;
    //         this.oldSelectionStart = this.selectionStart;
    //         this.oldSelectionEnd = this.selectionEnd;
    //     } else if (this.hasOwnProperty("oldValue")) {
    //         this.value = this.oldValue;
    //         this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
    //     } else {
    //         this.value = "";
    //     }
    //     });
    // };
    // }(jQuery));
    $(document).ready(function(){
        $('.datepicker').datepicker();
        $("#myform").validate({
            ignore: [],
            errorPlacement: function (error, element)
	        {
                error.insertAfter(element);
	        }
        });
        $('#goal_point').keyup(function(){
            $("#goal_point").inputFilter(function(value) {
            return /^\d*$/.test(value);
            });
        });
        $('#current_point').keyup(function(){
            $("#current_point").inputFilter(function(value) {
            return /^\d*$/.test(value);
            });
        });
    });

    $(function() {
        $('#reminderdate').hide();
        $('#dayofweek').hide();
        $('#remindertype').change(function(){
            if($('#remindertype').val() == 'one time') {
                $('#reminderdate').show();
                $('#dayofweek').hide();
            }else if($('#remindertype').val() == 'weekly') {
                $('#dayofweek').show();
                $('#reminderdate').hide();
            }else {
                $('#reminderdate').hide();
                $('#dayofweek').hide();
            }
        });


        $("#datepicker").datepicker({
            dateFormat: "dd-mm-yy",
            monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho',
            'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
            defaultDate: new Date(),
            onClose: function( selectedDate ) {
                $( "#datepicker1").datepicker( "option", "minDate", selectedDate );
            }
        });
        $("#datepicker1").datepicker({
            dateFormat: "dd-mm-yy",
            monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho',
            'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
            defaultDate: new Date(),
            onClose: function( selectedDate ) {
                $( "#datepicker" ).datepicker( "option", "maxDate", selectedDate );
            }
        });

    });


</script>
<style>
    .error{
        color: red !important;
    }
</style>
@endsection
