<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="Description" content="Enter your description here"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
<link rel="stylesheet" href="assets/css/style.css">
<title>Title</title>
<link href="https://vjs.zencdn.net/7.10.2/video-js.css" rel="stylesheet" />
<style>
  body{
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  #jsplayer_container{
    width: 640px;
    height: auto;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    -webkit-transform: translate(-50%, -50%);
    -moz-transform: translate(-50%, -50%);
    -o-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
  }
</style>

  <!-- If you'd like to support IE8 (for Video.js versions prior to v7) -->
  <script src="https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
</head>
<body>
    <div id="jsplayer_container">
      <video
          id="video_player"
          class="video-js vjs-big-play-centered"
          data-setup="{}"
        >
        <source src="{{URL::to('public/frontend/sample_vdo_3.mp4')}}" type="video/mp4" />
        <!-- <source src="MY_VIDEO.webm" type="video/webm" /> -->
        <p class="vjs-no-js">
          To view this video please enable JavaScript, and consider upgrading to a
          web browser that
          <a href="https://videojs.com/html5-video-support/" target="_blank"
            >supports HTML5 video</a
          >
        </p>
      </video>
    </div>
  <!-- <video id="my_video_1" class="video-js vjs-big-play-centered" controls preload="auto" width="640" height="268" 
  data-setup='{ }' muted>
    <source src="http://vjs.zencdn.net/v/oceans.mp4" type='video/mp4'>
  </video>
   <button type="button" id="click">Submit</button> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://vjs.zencdn.net/7.10.2/video.min.js"></script>
<script>
    // var player = videojs('my-video', {
    //   autoplay: true,
    //   setCurrentTime: "200",
    // });
    
    // $('#click').click(function(){
    //   var dom = document.getElementById('my-video');
    //   videojs(dom, {}, function(){
    //     this.on('loadedmetadata', function(){
    //       this.currentTime(200);
    //       this.play();
    //       console.log(this.currentTime());
    //     });
    //   });
    // });

    // $(document).ready(function()
    // {
    //     var timecode = 120;
    //     var initdone = false;

    //     // wait for video metadata to load, then set time 
    //     video.on("loadedmetadata", function(){
    //         video.currentTime(timecode);
    //     });

    //     // iPhone/iPad need to play first, then set the time
    //     // events: https://www.w3.org/TR/html5/embedded-content-0.html#mediaevents
    //     video.on("canplaythrough", function(){
    //         if(!initdone)
    //         {
    //             video.currentTime(timecode);
    //             initdone = true;
    //         }
    //     });

    // }); // END ready


    // var currPlayer = videojs("my_video_1")
    // var player = currPlayer

    // console.log("Start");
    // console.log(player);
    // currPlayer.currentTime(5000)
    // currPlayer.play()

    // currPlayer.on('ended', function(){
    //   // Play the endPlayer
    //   console.dir("Done");
    //   player.pause()
    //   player.src({ 			
    //   src:'https://d2zihajmogu5jn.cloudfront.net/bipbop-advanced/bipbop_16x9_variant.m3u8',
    //   type:'application/x-mpegURL'})
    // //  player.setAttribute('src','https://d2zihajmogu5jn.cloudfront.net/bipbop-advanced/bipbop_16x9_variant.m3u8')
    // //  player.setAttribute('type','application/x-mpegURL')

    //   console.log(player.currentSrc())
      
    //   player.load()
    //   player.currentTime = 10000
    //   player.play()
    // });
</script>
<script>
  var video = videojs('#video_player',{
    autoplay: 'muted',
    controls: true,
    poster: "https://picsum.photos/800/450",
    aspectRatio: '16:9'
  });
  video.on("pause", function () {
    localStorage['video_paused_at'] = video.currentTime();
  });
  if(localStorage.getItem('video_paused_at')){
    video.currentTime(localStorage.getItem('video_paused_at'));
  }

  window.addEventListener("beforeunload", function (e) {
    var confirmationMessage = "\o/";
    localStorage['video_paused_at'] = video.currentTime();
    (e || window.event).returnValue = confirmationMessage; //Gecko + IE
    return confirmationMessage;                            //Webkit, Safari, Chrome
  });
</script>
</body>
</html>