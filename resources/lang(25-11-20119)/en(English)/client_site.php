<?php
//@lang('client_site.@@@@@')
return [
	// home page
	'search'					=> 'Search',
	'how_it_works'				=> 'How it works',
	'browse_by_category'		=> 'Browse By Category',
	'view_all'					=> 'View All',
	'become_an_expert_now_!'	=> 'Become an Expert now !',
	'get_started'				=> 'Get Started',
	'what_makes_us_different'	=> 'What Makes Us Different',
	'learn_more'				=> 'Learn more',
	'find_instant_guidance'		=> 'Find Instant Guidance',
	'speaks'					=> 'Speaks',
	'request_a_session'			=> 'Request A Session',
	'view_profile'				=> 'View Profile',
	'testimonials'				=> 'Testimonials',
	'posted_on'					=> 'Posted on :',

	// payment page
	'my_payments'					=> 'My Payments',
	'menu'							=> 'Menu',
	'order_id'						=> 'Order Id',
	'payment_status'				=> 'Payment Status',
	'already_setteled'				=> 'Already Setteled',
	'under_settelement'				=> 'Under Settelement',
	'filter'						=> 'Filter',
	'user_name'						=> 'User name',
	'date'							=> 'Date',
	'amount'						=> 'Amount',
	'commission'					=> 'Commission',
	'net'							=> 'Net',
	'earning'						=> 'Earning',
	'payment'						=> 'Payment',
	'status'						=> 'Status',
	'commision'						=> 'Commision',
	'payment_details_not_found'	=> 'Payment details not found !',
	'balance_transfered'	=> 'Balance Transfered',

	// professional->compose_message
	'message_compose'				=> 'Message Compose',
	'messagebox'					=> 'Messagebox',
	'name'							=> 'Name',
	'select_your_recipents'			=> 'Select your Recipents',
	'attach_files'					=> 'Attach Files',
	'send_message'					=> 'send message',

	// professional->message
	'message'						=> 'Message',
	'inbox'							=> 'Inbox',
	'sent'							=> 'Sent',
	'compose'						=> 'compose',
	'your_inbox_is_currently_empty' => 'Your inbox is currently empty.',
	'view'							=> 'View',
	'delete_conversation'			=> 'Delete Conversation',
	'do_you_want_to_delete_this_conversation'=> 'Do you want to delete this conversation ?',

	// professional->message->message_details
	'message_details'				=> 'Message Details',
	'chatbox'						=> 'Chatbox',
	'reply'							=> 'Reply',
	'you_replied'					=> 'You Replied:',

	// professional->message->professional_as_user_upcomming_list
	'upcomming_class'				=> 'Upcomming Class',
	'professional'					=> 'Professional',
	'from_date'						=> 'From Date',
	'to_date'						=> 'To Date',
	'filter'						=> 'Filter',
	'user'							=> 'User',
	'date_time'						=> 'Date/Time',
	'duration'						=> 'Duration',
	'minutes'						=> 'Minutes',
	'minute'						=> 'Minute',
	'topic'							=> 'Topic',
	'action'						=> 'Action',
	'message'						=> 'Message',
	'booking_details_not_found'		=> 'Booking details not found !',
	'reschedule_appoinment'			=> 'Reschedule Appoinment',
	'time'							=> 'Time',
	'submit'						=> 'Submit',
	'close'							=> 'Close',

	// professional->message->professional_availability
	'availability'					=> 'Availability',
	'please_complete_your_profile'  => 'Please complete your profile by entering your educational information,',
	'click_here'					=> 'click here',
	'standard_hours'				=> 'Standard hours',
	'add'							=> 'Add',
	'day'							=> 'Day',
	'monday'						=> 'Monday',
	'tuesday'						=> 'Tuesday',
	'wednesday'						=> 'Wednesday',
	'thursday'						=> 'Thursday',
	'friday'						=> 'Friday',
	'saturday'						=> 'Saturday',
	'sunday'						=> 'Sunday',
	'from_time'						=> 'From Time',
	'to_time'						=> 'To Time',
	'delete'						=> 'Delete',
	'successfully_updated'			=> 'Successfully Updated',
	'from_time_can'					=> 'From time cant be greater than to time.',
	'already_same_time_is'			=>'Already same time is alloted for this day.',

	// professional->message->professional_availability
	'my_booking'					=> 'My Booking',
	'select_status'					=> 'Select Status',
	'awaiting_approval'				=> 'Awaiting Approval',
	'approved'						=> 'Approved',
	'rejected'						=> 'Rejected',
	'type'							=> 'Type',
	'select_type'					=> 'Select Type',
	'upcomming_classes'				=> 'Upcomming Classes',
	'past_classes'					=> 'Past Classes',
	'id'							=> 'ID',
	'rejected_by'					=> 'Rejected by',
	'accept'						=> 'Accept',
	'reject'						=> 'Reject',
	'video_call'					=> 'Video Call',
	'completed'						=> 'Completed',
	'reschedule_appoinment'			=> 'Reschedule Appoinment',

	// professional->message->professional_dashboard
	'dashboard'						=> 'Dashboard',
	'welcome'						=> 'Welcome',
	'paid'							=> 'Paid',
	'due'							=> 'Due',
	'classes'						=> 'Classes',
	'net_earning'						=> 'Net Earning',

	// professional->message->professional_experience
	'experience'					=> 'Experience',
	'organization'					=> 'Organization',
	'role'							=> 'Role',
	'description'					=> 'Description',
	'from'							=> 'From',
	'month'							=> 'Month',
	'january'						=> 'January',
	'february'						=> 'February',
	'march'							=> 'March',
	'april'							=> 'April',
	'may'							=> 'May',
	'june'							=> 'June',
	'july'							=> 'July',
	'august'						=> 'August',
	'september'						=> 'September',
	'october'						=> 'October',
	'november'						=> 'November',
	'december'						=> 'December',
	'year'							=> 'Year',
	'till_now'						=> 'Till Now',
	'save'							=> 'Save',
	'pursuing'						=> 'Pursuing',
	'edit'							=> 'Edit',
	'are_you_sure'				=> 'Are you sure ?',

	// professional->message->professional_profile
	'my_account'					=> 'My Account',
	'first_name'					=> 'First Name',
	'last_name'						=>'Last Name',
	'email'							=> 'Email',
	'area_code'						=> 'Area Code',
	'Phone'							=> 'Phone',
	'dob'							=> 'DOB',
	'cpf_no'						=> 'CPF No.',
	'country'						=> 'Country',
	'select_country'				=> 'Select Country',
	'state'							=> 'State',
	'select_state'					=> 'Select State',
	'city'							=> 'City',
	'street_name'					=> 'Street Name',
	'street_number'					=> 'Street Number',
	'address_complement'			=> 'Address Complement',
	'district_name'					=> 'District Name',
	'zipcode'						=> 'Zipcode',
	'language'						=> 'Language',
	'rate_per_time'					=> 'Rate Per Time',
	'select'						=> 'Select',
	'hourly'						=> 'Hourly',
	'consultant_fees'				=> 'Consultant Fees',
	'cRP_no'						=> 'CRP No',
	'gender'						=> 'Gender',
	'select_Gender'					=> 'Select Gender',
	'male'							=> 'Male',
	'Female'						=> 'Female',
	'both'							=> 'Both',
	'time_zone'						=> 'Time Zone',
	'select_timezone'				=> 'Select Timezone',
	'upload_picture'				=> 'Upload Picture',
	'about_me'						=> 'About Me',
	'speciality'					=> 'Speciality',
	'skills_currently_not_enlisted'	=> 'Skills Currently not enlisted',
	'save'							=> 'Save',
	'add_bank_account'				=> 'Add Bank Account',
	'update_bank_account'			=> 'Update Bank Account',
	'bank_name'						=> 'Bank name',
	'select_your_bank'				=> 'Select your Bank',
	'account_number'				=> 'Account number',
	'agency_number'					=> 'Agency Number',
	'bank_number'					=> 'Bank Number',
	'select_your_bank_number'		=> 'Select your Bank number',
	'account_check_number'			=> 'Account Check Number',
	'change_password'				=> 'Change Password',
	'old_password'					=> 'Old Password',
	'new_password'					=> 'New Password',
	'confirm_password'				=> 'Confirm Password',
	'change_password'				=> 'Change Password',

	// professional->message->professional_qualification
	'qualification'					=> 'Qualification',
	'please_complete_your_profile'					=> 'Please complete your profile by fill-out this informations.',
	'degree' 						=> 'Degree',
	'university'					=> 'University',
	'to'							=> 'To',
	'Experience'					=> 'experience',
	'from_month_or_from'			=> 'From month or From Year can t be greater than or equal To year',

	// professional->message->professional_upcomming_list
	'phone_number_allready_exist' 	=> 'Phone number allready exist.',
	'Try_again_after_sometime'		=> 'Try again after sometime',
	'please_select_your_language'		=> 'Please select your language',

	// professional->message->reply
	'reply_message'					=> 'Reply Message',
	'messagebox'					=> 'Messagebox',
	'recipents_name'				=> 'Recipents Name',
	'select_your_recipents'			=> 'Select your Recipents',
	'attach_files'					=> 'Attach Files',
	'send_message'					=> 'Send message',

	// professional->message->view_booked_user_details
	'class_details'					=> 'Class Details',
	'user_id'						=> 'User Id',
	'user_name'						=> 'User Name',
	'date_&_time'					=> 'Date & Time',
	'category'						=> 'Category',
	'amount'						=> 'Amount',
	'initiated'						=> 'Initiated',
	'complete'						=> 'Complete',
	'try_again_after_sometime'  	=> 'Try again after sometime',

	// public_profile->public_profile
	'public_profile'				=> 'public profile',
	'fees'							=> 'Fees',
	'hour'							=> 'Hour',
	'speaks'						=> 'Speaks',
	'personal_information'			=> 'Personal Information',
	'education'						=> 'Education',
	'reviews'						=> 'Reviews',
	'calendar'						=> 'Calendar',
	'add_to_favorite'				=> 'Add to Favorite',
	'booking_request'				=> 'Booking Request',
	'review_information_not_found'  => 'Review Information Not Found',
	'information_not_found'			=> 'Information Not Found',
	'available'						=> 'Available',
	'not_available'					=> 'Not Available',
	'view_other_professionals'		=> 'View Other Professionals',
	'request_a_session'				=> 'Request A Session',
	'view_profile'					=> 'View Profile',
	'successfully_added_to_favourite_list' => 'Successfully added to favourite list',
	'you_have_been_allready_added'	=> 'You have been allready added',
	'into_your_favourite_list'		=> 'into your favourite list',
	'currently_time_slot_is_not_avaliable_for_this_day' => 'Currently time slot is not avaliable for this day.',
	'please_selct_a_date_that_is_greater_than_or_equal_to' => 'Please selct a date that is greater than or equal to :',
	'error'							=> 'error',
	'internal_server_error'			=> 'internal server error.',

	//search->browse_search
	'browse_by_categories'			=> 'Browse by Categories',
	'no_subcategory_found'		=> 'No Subcategory found !!',

	//search->browse_search
	'keywords'						=> 'Keywords',
	'category'						=> 'Category',
	'select_category'				=> 'Select Category',
	'sub_category'					=> 'Sub Category',
	'hourly_minute_rate'			=> 'Hourly / Minute Rate',
	'sort_by'						=> 'Sort By',
	'request_a_session'				=> 'Request A Session',
	'view_profile'					=> 'View Profile',
	'select_option'					=> 'Select Option',
	'select_category_first'			=> 'Select Category First',
	'desending'						=> 'Desending',
	'assending'						=> 'Assending',
	'search_result_not_found'		=> 'Search Result Not Found!!',


	//videocall->videocall
	'currently_you_dont_have_any_live_video_conference'	=> 'OOPS! Currently you don t have any live video conference',

	//user->user_review
	'review'						=> 'Review',
	'menu'							=> 'Menu',
	'give_rating'					=> 'Give Rating :',
	'review_heading'				=> 'Review Heading',
	'review_comments'				=> 'Review Comments',
	'submit'						=> 'Submit',
	'please_select_at_least_one_review' => 'Please select at least one review.',

	//user->user_upcomming_classes
			//all are same
	
	//user->view_user_booked_details
	'ratting'						=> 'Ratting :',
	'professional_name'				=> 'Professional Name :',
];