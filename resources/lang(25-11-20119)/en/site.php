<?php

return [
		'find_a_professional'           => 'Encontre um profissional',
		'home'                          => 'Home',
		'become_an_expert'              => 'Torne-se um especialista',
		'login'                         => 'Entrar',
		'signup'                        => 'Cadastre-se',
		'signout'                       => 'Sair',
		'dashboard'                   	=> 'Painel',
		'edit_profile'                  => 'Editar perfil',
		'my_messages'                   => 'Minhas mensagens',
		'my_conference'                 => 'Minhas sessões',
		'log_out'                       => 'Sair',
		'change_password'               => 'Alterar senha',
		'usuário'                       => 'Usuário',
		'professional'                  => 'Professional',
		'become_professional'           => 'Torne-se profissional',
		'favourite_expert'              => 'Especialista favorito',
		'coming_classes'                => 'Próximas sessões',
		'past_classes'                  => 'Sessões  anteriores',
		'my_experts'					=> 'Meus especialistas',
		'payment'						=> 'Pagamentos',
		'personal_info_and_Education'   => 'Informações pessoais e Profissional',
		'availability'                 	=> 'Disponibilidade',
		'experience'                    => 'Experiência',
		'my_blogs'  					=> 'Meus blogs',
		'my_users'                      => 'Meus usuários',
		'withdrawal_request'            => 'Pedido de retirada',
		'about_us'                      => 'Sobre nós',
		'our_vision'                    => 'Nossa visão',
		'our_mission'                   => 'Nossa missão',
		'our_team'		                => 'Nossa equipe',
		'our_history'                   => 'Nossa história',
		'about_netbhe'                  => 'Sobre o Netbhe',
		'read_more'	                    => 'Leia mais',
		'quick_links'                   => 'Links rápidos',
		'how_it_works'                  => 'Como funciona',
		'experts'                       => 'Especialistas',
		'become_an_expert'              => 'Torne-se um especialista',
		'terms_of_service'              => 'Termos de Serviço',
		'privacy_policy'                => 'Política de Privacidade',
		'blog'                          => 'Blog',
		'get_in_touch'					=> 'Entre em contato',
		'email' 						=> 'Email', 
										// => 'contato@netbhe.com',
		'all_rights_reserved'           => 'Todos os direitos reservados',
		'add_new_blog'                  => 'Adicionar novo blog',
		'click_here'                    => 'Clique aqui',
		'menu'                          => 'Menu',
		'post_title'                    => 'Título da postagem',
		'category'                      => 'Categoria',
		'upload_picture'                => 'Carregar imagem',
		'description'                   => 'Descrição',
		'select'                        => 'Selecionar',
		'salvar'                        => 'Salvar',
		'status'                        => 'Status',
		'filter'                        => 'Filtro',
		'add_new_post'                  => 'Adicionar nova postagem',
		'no'                            => 'Não',
		'topic'                         => 'Tópico',
		'post_name'                     => 'Nome da postagem',
		'action'						=> 'Ação',
		'add'							=> 'Adicionar',
		'edit'                          => 'Editar',
		'post'                          => 'Postagem',
		'existing_picture'              => 'Imagem existente',
		'details'						=> 'Detalhes',
		'author'                        => 'Autor',
		'share_on'                      => 'Compartilhar em',
		'comments'                      => 'Comentários',
		'education_guidance_advisory'   => 'Orientação educacional',
		'sub'    						=> 'Sub',
		'start'							=> 'Começar ',
		'time' 							=> 'Tempo ',
		'date'                          => 'Data',
		'duration'                      => 'Duração',
		'minute'                        => 'Minuto',
		'hour'                          => 'Hora',
		'message'                       => 'Mensagem',
		'send'                          => 'Enviar',
		'request'                       => ' Solicitação ',
		'fees'                          => 'Preço',
		'review_your_booking_request'   => 'Revise sua solicitação de reserva',
		'session'                       => 'Sessão',
		'slot'                          => 'Slot',
		'token'                         => 'Token',
		'gender'                        => 'Gênero',
		'country'                      	=> 'País',
		'State'                        	=> 'Estado',
		'City'                         	=> 'Cidade',
		'Area_code'                     => 'Código de área',
		'street_name'					=> 'Nome da rua',
		'street_number'                 => 'Número da rua',
		'address_complement'            => 'Endereço complementar',
		'district_name'					=> 'Nome do distrito',
		'zipcode'                       => 'CEP',
		'accept_cards'                  => 'Cartões aceitos',
		'accepted_cards'                => 'Cartões Aceitos',
		'name_on_card'					=> 'Nome no cartão',
		'credit_card_number'           	=> 'Número do cartão',
		'back' 							=> 'Voltar',
		'continue_to_pay'               => 'Prossiga para pagar',
		'proceed_to_pay'               	=> 'Finalizar Compra',
		'card_cvc'               		=> 'CVC do cartão',
		'card_no'               		=> 'Número do cartão de 16 dígitos',
		'card_name'               		=> 'Nome do cartão',
		'credit_card_number'            => 'Número do cartão de crédito',
		'request_a_session'				=> 'Solicitar uma sessão',
		'view_profile'                  => 'Visualizar perfil',
		'speak'                         => 'Falar',
		'checkout'                      => 'Verificar',
		'expert'						=> 'Especialista ',
		'male'                          => 'Masculino',
		'female'                        => 'Feminino',
		'both'                          => 'Prefiro não dizer',
		'payment'                       => 'Pagamento',
		'payments'                      => 'Pagamentos',
		'month'							=> 'Mês',
		'year'                          => 'Ano',
		'categories'					=> 'Categorias',
		'submit'						=> 'Enviar',
		'popular_post'                  => 'Postagem popular',
		'blog_post_not_found'           => 'Postagem no blog não encontrada',

		//'book_an_expert'	            => 'Agende um especialista',
		'book_an_expert'	            => 'Agendar um profissional é simples e rápido!',

		'active'                        => 'Ativo',
		'inactive'                      => 'Inativo',
		'view'                          => 'Ver',
		'delete'            	        => 'Excluir',
		'testimonials'                  => 'Depoimentos',
		'my_booking'                    => 'Minha reserva',
		'awaiting_approval'	        	=> 'Aguardando aprovação',
		'approved'                      => 'Aprovado',
		'approve'                      	=> 'Aprovar',
		'rejected'                      => 'Rejeitado',
		'reject'                      	=> 'Rejeita',
		'testimonials'                  => 'Depoimentos',
		'reschedule'	                => 'reagendar',
		'awaiting_rescheduled_approval' => 'Aguardando aprovação reprogramada',
		'rescheduled_approval' 			=> 'Aprovação reagendada',
		'rescheduled_rejected' 			=> 'Reprogramado rejeitado',
		'type'                          => ' Tipo ',
		'booking_cancelled_by'          => 'Reserva cancelada por',
		'review'                        => 'Ver',
		'booking_details_not_found'     => 'Detalhes da reserva não encontrados',
		'appoinment'                    => 'Compromisso ',
		'close'                         => 'Fechar',
		'my_account'                    => 'Minha conta',
		'first'                         => 'Primeiro',
		'Name'                          => 'Nome',
		'last'                          => 'Último',
		'zona'                          => 'Zona',
		'Timezone'                      => 'Fuso horário',
		'about_me'                      => 'Sobre mim',
		'old_password'                  => 'Senha antiga',
		'new_password'                  => 'Nova senha',
		'confirm_password'              => 'Confirmar senha',
		'my_professionals'              => 'Meus profissionais',
		'remove'                        => 'Remover',
		'become_a_professional'         => 'Torne-se um profissional',
		'Pursuing '                     => 'Prosseguir',
		'faq'                           => 'Perguntas Frequentes',
		'china_office'					=> 'Escritório na China',
		'phone'							=> 'telefone',
		'user'							=> 'Usuário',
		'upcoming_classes'				=> 'Classess sessões',
		'save'                          => 'Salvar',
		'awaiting_approval'             => 'Aguardando aprovação',
		'dob'                           => 'Data de Nascimento',
		'cpf_no'                        => 'Nº do CPF',
		'id'                            => 'ID',
		'empty_favorite'				=> 'Sua lista de profissionais está vazia!',
		'enter_first_name'				=> 'entrar primeiro nome',
		'enter_last_name'				=> 'entrar sobrenome',
		'enter_area_code'				=> 'entrar código de área',
		'enter_ph_no'					=> 'entrar telefone',
		'enter_dob'						=> 'entrar Data de Nascimento',
		'enter_cpf_no'					=> 'entrar Nº do CPF',
		'enter_city'					=> 'entrar cidade',
		'enter_street'					=> 'entrar nome da rua',
		'enter_street_no'				=> 'entrar número da rua',
		'enter_address_complement'		=> 'entrar endereço complementar',
		'enter_district'				=> 'entrar nome do distrito',
		'enter_zip'						=> 'entrar CEP',
		'enter_tax_consultant'			=> 'entrar taxas de atendimento',
		'enter_crp'						=> 'entrar Nº CRP',
		'enter_account_number'			=> 'entrar Número da conta',
		'enter_agency_number'			=> 'entrar Número da agência',
		'enter_account_check_number'	=> 'entrar Número de verificação da conta',
		'enter_old_password'			=> 'entrar senha antiga',
		'enter_new_password'			=> 'entrar nova senha',
		'enter_confirm_password'		=> 'entrar confirmar senha',
		'account_type'					=> 'Tipo de conta',
		'select_date'					=> 'Selecione a data',
		'select_start_time'				=> 'Selecione Hora de início',
		'type_your_message_here'		=> 'Digite aqui sua mensagem',
		'your'							=> 'Seu',
		'select_sub_category'			=> 'Selecionar subcategoria',
		'is_currently_not'				=> 'não está disponível no momento nesta data.',
		'for_booking_you_need'			=> 'Para fazer a reserva, você precisa atualizar seu perfil primeiro, por exemplo (número de telefone, fuso horário etc.).',

		// 19-11-2019

		'welcome_to_home_page'			=> 'Welcome to home page',
		'book_now'						=> 'Book Now',
		'booking_successfull'			=> 'Booking successfull',
		'my_availability'				=> 'My Availability',
		'welcome_to_dashboard'			=> 'Welcome to Dashboard',
		'my_experience'					=> 'My Experience',
		'my_qualification'				=> 'My Qualification',
		'view_booked_user_details'		=> 'View Booked User Details',
		'search_by_category'			=> 'Search by Category',
		'get_the_right_advice'			=> 'Get the right advice',
		'live_video_conference'			=> 'Live video conference',
		'preview_my_camera'				=> 'Preview My Camera',
		'to_date'						=> 'To Date',
		'by_entering_your_educational_information' => 'By entering your educational information',
		'no_time_slot_availabile_this_date' 	   => 'No time slot availabile this date',
		'choose_your_time_slot' 	   	=> 'Choose your time slot',
		'time_slot' 	   				=> 'Time slot',
		'please_select_date' 	   		=> 'Please select date',
		'select_time_slot' 	   			=> 'Select Time Slot',
		'time_interval' 	   			=> 'Time Interval',
		'could_not_initiate_video' 	   	=> 'Could not initiate video',
		'please_try_again_later' 	   	=> 'Please try again later',
		'video_conference_is_over' 	   	=> 'Video conference is over',
		'could_not_connect_to_twilio' 	=> 'Could not connect to Twilio',
];