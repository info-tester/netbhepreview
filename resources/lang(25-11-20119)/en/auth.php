<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    // 'failed' => 'Invalid email or password.',
    // 'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    
    'failed' => 'E-mail ou senha inválidos.',
    'throttle' => 'Muitas tentativas de login. Por favor, tente novamente em: segundos segundos.',

];
