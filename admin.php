<?php


Route::group(['namespace' => 'Admin'], function() {

    // For Dashboard
    Route::get('/home', 'HomeController@index')->name('admin.dashboard');

    // For category managment
    Route::get('categories', 'Modules\Category\CategoryController@index')->name('admin.categories');

    Route::post('categories', 'Modules\Category\CategoryController@index')->name('admin.filter.categories');

    Route::get('add-category', 'Modules\Category\CategoryController@add')->name('admin.add.categories');

    Route::post('store-category', 'Modules\Category\CategoryController@add')->name('admin.store.categories');

    Route::get('category/{id}/status', 'Modules\Category\CategoryController@status')->name('admin.category.status');

    Route::get('category/{cid}/delete', 'Modules\Category\CategoryController@delete')->name('admin.category.delete');

    Route::get('category/{eid}/edit', 'Modules\Category\CategoryController@update')->name('admin.category.edit');

    Route::post('category/{eid}/update', 'Modules\Category\CategoryController@update')->name('admin.category.update');

    Route::get('category-homepage/{id}', 'Modules\Category\CategoryController@categoryShowHomepage')->name('admin.category.homepage');

    Route::get('category-search-show/{id}', 'Modules\Category\CategoryController@showinCategorySearch')->name('admin.category.search.show');


    // For Unit
   //  Route::resource('unit', 'UnitController', ['only' => ['index', 'create', 'edit', 'destroy', 'update', 'store']]);
   //  Route::get('unit-status/{id}', 'UnitController@status')->name('admin.unit-status');

    // For Customer
    Route::resource('subadmin','Modules\Subadmin\SubadminController',['only' => ['index','show','destroy']]);

    Route::get('subadmin-status/{id}', 'Modules\Subadmin\SubadminController@status')->name('admin.subadmin.status');

    Route::get('add-subadmin', 'Modules\Subadmin\SubadminController@addSubAdmin')->name('admin.subadmin.add');

    Route::post('store-subadmin', 'Modules\Subadmin\SubadminController@addSubAdmin')->name('admin.subadmin.store');

    Route::get('edit-subadmin/{id}', 'Modules\Subadmin\SubadminController@updateSubAdmin')->name('admin.subadmin.edit');

    Route::post('update-subadmin/{id}', 'Modules\Subadmin\SubadminController@updateSubAdmin')->name('admin.subadmin.update');

    // Route::get('subadmin-reject/{id}', 'Modules\Customer\CustomerController@reject')->name('admin.subadmin.reject');

    // For Astrologer
    Route::get('professional/inactive', 'Modules\Professional\ProfessionalController@inactiveProfessionalsIndex')->name('professional.inactive');
    Route::post('professional/inactive', 'Modules\Professional\ProfessionalController@inactiveProfessionalsIndex')->name('professional.inactive');
    Route::resource('professional','Modules\Professional\ProfessionalController',['only' => ['index','show','destroy', 'edit', 'update','bankAccount']]);
    Route::get('professional/edit-commission/{id}','Modules\Professional\ProfessionalController@professionalCommission')->name('admin.professional.commission');
    Route::post('professional/edit-commission/{id}','Modules\Professional\ProfessionalController@professionalCommission')->name('admin.professional.commission');

    Route::any('professional/{id}/edit-bank-account','Modules\Professional\ProfessionalController@bankAccount')->name('admin.professional.edit.bank.account');

    Route::get('professional/category/edit/{id}', 'Modules\Professional\ProfessionalController@editProfessionalCategory')->name('professional.category.edit');
    Route::post('professional/category/add/{id}', 'Modules\Professional\ProfessionalController@addProfessionalCategory')->name('professional.category.add');
    Route::get('professional/category/delete/{id}/{uid}', 'Modules\Professional\ProfessionalController@deleteProfessionalCategory')->name('professional.category.delete');

    Route::get('professional/experience/edit/{id}', 'Modules\Professional\ProfessionalController@editProfessionalExperience')->name('professional.experience.edit');
    Route::post('professional/experience/add/{id}', 'Modules\Professional\ProfessionalController@addProfessionalExperience')->name('professional.experience.add');
    Route::get('professional/experience/delete/{id}/{uid}', 'Modules\Professional\ProfessionalController@deleteProfessionalExperience')->name('professional.experience.delete');

    Route::get('professional/qualification/edit/{id}', 'Modules\Professional\ProfessionalController@editProfessionalQualification')->name('professional.qualification.edit');
    Route::get('professional/qualification/add/{id}', 'Modules\Professional\ProfessionalController@addProfessionalQualification')->name('professional.qualification.add');
    Route::post('professional/qualification/add/{id}', 'Modules\Professional\ProfessionalController@addProfessionalQualification')->name('professional.qualification.add');
    Route::get('professional/qualification/update/{id}', 'Modules\Professional\ProfessionalController@updateProfessionalQualification')->name('professional.qualification.update');
    Route::post('professional/qualification/update/{id}', 'Modules\Professional\ProfessionalController@updateProfessionalQualification')->name('professional.qualification.update');
    Route::get('professional/qualification/delete/{id}', 'Modules\Professional\ProfessionalController@deleteProfessionalQualification')->name('professional.qualification.delete');


    Route::get('accsept-request/{id}', 'Modules\Professional\ProfessionalController@upgradeUser')->name('upgradeUser');

    Route::get('user-status/{id}/{status}', 'Modules\Professional\ProfessionalController@status')->name('admin.professional.status');

    Route::post('prof-status/account', 'Modules\Professional\ProfessionalController@createProfessionalAccount')->name('admin.professional.create.account');

    Route::get('user-reject/{id}', 'Modules\Professional\ProfessionalController@reject')->name('admin.professional.reject');

    Route::get('user-view-reject/{id}', 'Modules\Professional\ProfessionalController@reject1')->name('admin.professional.reject1');
    Route::post('check-email', 'Modules\Professional\ProfessionalController@check_email')->name('admin.chk.email');
    Route::post('check-email-change', 'Modules\Professional\ProfessionalController@checkEmailChange')->name('admin.check.email.change');

    Route::post('check-mobile-change', 'Modules\Professional\ProfessionalController@checkMobileChange')->name('admin.check.mobile.change');

    Route::post('check-nick-name', 'Modules\Professional\ProfessionalController@checkNickName')->name('admin.chk.nick.name');

    Route::get('user-home/{id}', 'Modules\Professional\ProfessionalController@home')->name('admin.professional.home');

    Route::get('user-recommended/{id}', 'Modules\Professional\ProfessionalController@recommended')->name('admin.professional.recommended');

    Route::get('user-review/{id}', 'Modules\Professional\ProfessionalController@review')->name('admin.professional.review');

    Route::get('user-review-delete/{id}', 'Modules\Professional\ProfessionalController@reviewDelete')->name('admin.professional.review.delete');

    Route::post('user-review', 'Modules\Professional\ProfessionalController@reviewStore')->name('admin.professional.review.store');

    Route::get('user-remainder/{id}', 'Modules\Professional\ProfessionalController@sendRemainder')->name('admin.professional.remainder');

    Route::get('approve-user/{id}', 'Modules\Professional\ProfessionalController@approveUser')->name('admin.user.approve');

    // for professional profile edit..........
    Route::post('get-state', 'Modules\Professional\ProfessionalController@getState')->name('admin.get.state');

    Route::get('user-free-membership/{id}', 'AstrologerController@membership')->name('admin.professional.membership');
   //  // astrologer payout.................
    Route::post('payout-user', 'AstrologerController@payout_astrologer')->name('professional.payout');

    Route::get('payouts', 'AstrologerController@payouts')->name('payouts');

    Route::get('payouts/filter', 'AstrologerController@payouts')->name('payouts.filter');
    // astrologer payout.................

    // For Membership Plan
   //  Route::get('membership', 'MemberShipController@plan')->name('admin.membership');
   //  Route::post('membership-update', 'MemberShipController@update')->name('admin.membership.update');

    // For Product
    // Route::get('product', 'ProductController@index')->name('admin.product');
    // Route::get('product-details/{id}', 'ProductController@view')->name('admin.product.details');
    // Route::get('product-status/{id}', 'ProductController@status')->name('admin.product.status');
    // For tiny-mcs image upload...

    Route::post('admin-article-image-upload', 'Modules\Blog\BlogController@imgUpload')->name('admin.artical.img.upload');

    // For Blog
    Route::get('blog', 'Modules\Blog\BlogController@index')->name('admin.blog.index');
    Route::get('blog-add', 'Modules\Blog\BlogController@create')->name('admin.blog.create');
    Route::post('blog-store', 'Modules\Blog\BlogController@store')->name('admin.blog.store');
    Route::get('blog-delete/{id}', 'Modules\Blog\BlogController@destroy')->name('admin.blog.destroy');
    Route::get('blog-edit/{id}', 'Modules\Blog\BlogController@edit')->name('admin.blog.edit');
    Route::post('blog-update', 'Modules\Blog\BlogController@update')->name('admin.blog.update');
    Route::get('blog-details/{id}', 'Modules\Blog\BlogController@view')->name('admin.blog.details');
    Route::get('blog-status/{id}', 'Modules\Blog\BlogController@status')->name('admin.blog.status');
    Route::get('blog-contact-show/{id}', 'Modules\Blog\BlogController@blogContactShow')->name('admin.blog.contact.show');
    Route::get('blog/blog-contact-list/{id}', 'Modules\Blog\BlogController@blogContactList')->name('admin.blog.contact.list');

    // For Blog Category
    Route::get('blog-category', 'Modules\Blog\BlogCategoryController@index')->name('admin.blog.category.index');
    Route::get('blog-category-add', 'Modules\Blog\BlogCategoryController@create')->name('admin.blog.category.create');
    Route::post('blog-category-store', 'Modules\Blog\BlogCategoryController@store')->name('admin.blog.category.store');
    Route::get('blog-category-delete/{id}', 'Modules\Blog\BlogCategoryController@destroy')->name('admin.blog.category.destroy');
    Route::get('blog-category-edit/{id}', 'Modules\Blog\BlogCategoryController@edit')->name('admin.blog.category.edit');
    Route::post('blog-category-update', 'Modules\Blog\BlogCategoryController@update')->name('admin.blog.category.update');
    // Route::get('blog-details/{id}', 'BlogController@view')->name('admin.blog.details');
    Route::get('blog-category-status/{id}', 'Modules\Blog\BlogCategoryController@status')->name('admin.blog.category.status');

    // Login
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('admin.login');
    Route::get('/', 'Auth\LoginController@showLoginForm')->name('admin.login');
    Route::post('login', 'Auth\LoginController@login')->name('admin.post.login');

    Route::post('logout', 'Auth\LoginController@logout');

    Route::get('logout', 'Auth\LoginController@logout')->name('admin.logout');

    // Register
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('admin.register');
    Route::post('register', 'Auth\RegisterController@register');

    // Passwords
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('admin.password.reset');

    // Change Password
    Route::get('change-password', 'HomeController@changePassword')->name('admin.change.password');
    Route::post('update-password', 'HomeController@updatePassword')->name('admin.update.password');

    // For Order Management
    Route::get('order', 'Modules\Order\OrderController@index')->name('admin.order');
    Route::get('orders/{userId}', 'Modules\Order\OrderController@index')->name('admin.user.orders');

    Route::get('order/{id}', 'Modules\Order\OrderController@view')->name('admin.order.details');

    Route::post('make-transfer', 'Modules\Order\OrderController@makeTransfer')->name('admin.make.transfer');
    Route::post('make-transfer-product', 'Modules\Order\OrderController@makeTransferProduct')->name('admin.make.transfer.product');
    Route::post('make-multiple-transfer', 'Modules\Order\OrderController@makeMultipleTransfer')->name('admin.make.multiple.transfer');
    Route::post('make-transfer-bank', 'Modules\Order\OrderController@makeTransferBank')->name('admin.make.transfer.bank');
    Route::post('make-transfer-bank-product', 'Modules\Order\OrderController@makeTransferBankProduct')->name('admin.make.transfer.bank.product');
    Route::post('make-transfer-paypal', 'Modules\Order\OrderController@makeTransferPaypal')->name('admin.make.transfer.paypal');
    Route::post('make-transfer-affiliate', 'Modules\Order\OrderController@makeTransferAffiliate')->name('admin.make.transfer.affiliate');
    Route::post('make-transfer-bank-product-affiliate', 'Modules\Order\OrderController@makeTransferBankProductAffiliate')->name('admin.make.transfer.bank.product.affiliate');
    Route::post('make-transfer-paypal-affiliate', 'Modules\Order\OrderController@makeTransferPaypalAffiliate')->name('admin.make.transfer.paypal.affiliate');

 //video call charge status

    Route::get('video-call-charge/{id}', 'Modules\Order\OrderController@updateStatus')->name('admin.order.video.callcharge');

 //video call charge status end

    Route::get('newsletter', 'Modules\Content\ContentController@newsLetter')->name('admin.newsLetter');
    Route::post('newsletter', 'Modules\Content\ContentController@newsLetter');
    //csv dowanload
    Route::get('newsletter-download', 'Modules\Content\ContentController@download')->name('admin.newsLetter.download');

    Route::get('update-about-us', 'Modules\AboutUs\AboutUsController@index')->name('admin.about_us');

    Route::post('store-about-us', 'Modules\AboutUs\AboutUsController@index')->name('admin.about_us.store');
    Route::get('how-it-works/update', 'Modules\Content\ContentController@updateHowItWorks')->name('admin.how.it.works.update');
    Route::post('how-it-works/update', 'Modules\Content\ContentController@updateHowItWorks')->name('admin.how.it.works.update');

    Route::get('home-expert-section/update', 'Modules\Content\ContentController@updateHomeExpert')->name('admin.home.expert.section');
    Route::post('home-expert-section/update', 'Modules\Content\ContentController@updateHomeExpert');

    Route::get('other-page-contents/update', 'Modules\Content\ContentController@otherPageContent')->name('other.page.contents');
    Route::post('other-page-contents/update', 'Modules\Content\ContentController@otherPageContent');

    Route::get('meta-tag-management/', 'Modules\Content\ContentController@metaTagManager')->name('meta.tag.manage');
    Route::get('meta-tag-management/edit/{id}', 'Modules\Content\ContentController@metaTagEditor')->name('meta.tag.edit');
    Route::post('meta-tag-management/edit/{id}', 'Modules\Content\ContentController@metaTagEditor');

    Route::get('landing-page-management', 'Modules\Content\ContentController@manageLandingPage')->name('landing.page.edit');
    Route::post('landing-page-management/{section?}', 'Modules\Content\ContentController@manageLandingPage')->name('landing.page.update');

    Route::get('home-banner-management', 'Modules\Content\ContentController@manageHomeBanner')->name('home.banner.management');
    Route::get('home-banner-management/add', 'Modules\Content\ContentController@addHomeBanner')->name('home.banner.add');
    Route::post('home-banner-management/add', 'Modules\Content\ContentController@addHomeBanner');
    Route::get('home-banner-management/edit/{id}', 'Modules\Content\ContentController@editHomeBanner')->name('home.banner.edit');
    Route::post('home-banner-management/edit/{id}', 'Modules\Content\ContentController@editHomeBanner');
    Route::get('home-banner-management/delete/{id}', 'Modules\Content\ContentController@deleteHomeBanner')->name('home.banner.delete');

    Route::get('terms-of-services', 'Modules\Content\ContentController@indexTermsOfServices')->name('admin.terms.of.services');
    Route::post('terms-of-services', 'Modules\Content\ContentController@indexTermsOfServices')->name('admin.terms.of.services');

    Route::get('add-terms-of-services', 'Modules\Content\ContentController@storeTermsOfServices')->name('admin.terms.of.services.add');
    Route::get('edit-terms-of-services/{id}', 'Modules\Content\ContentController@storeTermsOfServices')->name('admin.terms.of.services.edit');
    Route::post('store-terms-of-services/{id?}', 'Modules\Content\ContentController@storeTermsOfServices')->name('admin.terms.of.services.store');
    Route::get('delete-terms-of-services/{id}', 'Modules\Content\ContentController@deleteTermsOfServices')->name('admin.terms.of.services.delete');

    Route::get('add-subterms-of-services/{tid}', 'Modules\Content\ContentController@storeSubtermsOfServices')->name('admin.subterms.of.services.add');
    Route::get('edit-subterms-of-services/{tid}/{sid}', 'Modules\Content\ContentController@storeSubtermsOfServices')->name('admin.subterms.of.services.edit');
    Route::post('store-subterms-of-services/{tid}/{sid?}', 'Modules\Content\ContentController@storeSubtermsOfServices')->name('admin.subterms.of.services.store');
   //  Route::get('delete-subterms-of-services/{id}', 'Modules\Content\ContentController@deleteSubtermsOfServices')->name('admin.subterms.of.services.delete');

    Route::get('privacy-policy/update', 'Modules\Content\ContentController@updatePrivacyPolicy')->name('admin.privacy.policy.update');
    Route::post('privacy-policy/update', 'Modules\Content\ContentController@updatePrivacyPolicy')->name('admin.privacy.policy.update');

    Route::get('faq/update', 'Modules\Content\ContentController@updateFaq')->name('admin.faq.update');
    Route::post('faq/update', 'Modules\Content\ContentController@updateFaq');
    Route::post('faq/create', 'Modules\Content\ContentController@createFaq')->name('admin.faq.create');
    Route::get('faq/edit/{id}', 'Modules\Content\ContentController@editFaq')->name('admin.faq.edit');
    Route::post('faq/edit/{id}', 'Modules\Content\ContentController@editFaq');
    Route::get('faq/delete/{id}', 'Modules\Content\ContentController@deleteFaq')->name('admin.faq.delete');
    Route::get('faq/move/{id}/{dir}', 'Modules\Content\ContentController@moveFaq')->name('admin.faq.move');
    Route::post('faq/image-upload', 'Modules\Content\ContentController@imageUploadFaq')->name('admin.faq.image.upload');
    Route::get('faq/user', 'Modules\Content\ContentController@userFaq')->name('admin.faq.update.user');
    Route::get('faq/expert', 'Modules\Content\ContentController@expertFaq')->name('admin.faq.update.expert');

    Route::get('help/categories', 'Modules\Content\ContentController@helpCategoriesIndex')->name('admin.help.contents');
     Route::get('help/show-articles', 'Modules\Content\ContentController@helpShowArticles')->name('admin.help.show.articles');
    Route::get('help/category/add/{id?}', 'Modules\Content\ContentController@helpCategorystore')->name('admin.help.category.add');
    Route::post('help/category/store/{id?}', 'Modules\Content\ContentController@helpCategorystore')->name('admin.help.category.store');
    Route::get('help/category/status/{id?}', 'Modules\Content\ContentController@helpCategorystatus')->name('admin.help.category.status');
    Route::get('help/category/delete/{id}', 'Modules\Content\ContentController@helpCategorydelete')->name('admin.help.category.delete');

    Route::get('help/articles/manage/{id}', 'Modules\Content\ContentController@helpArticlemanage')->name('admin.help.articles.manage');
    Route::post('help/articles/manage/{id}', 'Modules\Content\ContentController@helpArticlemanage')->name('admin.help.articles.manage');
    Route::get('help/articles/add/{id?}', 'Modules\Content\ContentController@helpArticlestore')->name('admin.help.articles.add');
    Route::post('help/articles/store/{id?}', 'Modules\Content\ContentController@helpArticlestore')->name('admin.help.articles.store');
    Route::get('help/articles/status/{id?}', 'Modules\Content\ContentController@helpArticlestatus')->name('admin.help.articles.status');
    Route::get('help/articles/delete/{id?}', 'Modules\Content\ContentController@helpArticledelete')->name('admin.help.articles.delete');

    Route::get('footer-management', 'Modules\Content\ContentController@updateFooterContent')->name('admin.footer.management.update');
    Route::post('footer-management', 'Modules\Content\ContentController@updateFooterContent')->name('admin.footer.management.update');
    Route::get('refer-content-management', 'Modules\Content\ContentController@referContentManagement')->name('admin.refer.content');
    Route::post('refer-content-update', 'Modules\Content\ContentController@referContentUpdate')->name('admin.refer.content.update');

   Route::get('product-landing-page','Modules\Content\ContentController@productLandingPage')->name('admin.product.landing.page');

   Route::post('product-landing-page/{section?}','Modules\Content\ContentController@productLandingPage')->name('admin.product.landing.page.update');

   Route::get('affiliate-landing-page','Modules\Content\ContentController@affiliateLandingPage')->name('admin.affiliate.landing.page');
   Route::post('affiliate-landing-page-update/{section}','Modules\Content\ContentController@affiliateLandingPage')->name('admin.affiliate.landing.page.update');

   Route::get('landing-page-landing','Modules\Content\ContentController@professionalLandingPage')->name('admin.professional.landing.page');
   Route::post('landing-page-landing-update/{section}','Modules\Content\ContentController@professionalLandingPage')->name('admin.professional.landing.page.update');

    // For Review Management
   //  Route::get('review', 'ReviewController@index')->name('admin.review');
   //  Route::get('review/{id}', 'ReviewController@index')->name('admin.review1');
   //  Route::get('review-delete/{id}', 'ReviewController@destroy')->name('admin.review.destroy');

    // For Coupon Management
    // Route::get('coupon', 'CouponController@index')->name('admin.coupon');
    // Route::get('coupon-create', 'CouponController@create')->name('admin.coupon.create');
    // Route::post('coupon-store', 'CouponController@store')->name('admin.coupon.store');
    // Route::get('coupon-edit/{id}', 'CouponController@edit')->name('admin.coupon.edit');
    // Route::post('coupon-update', 'CouponController@update')->name('admin.coupon.update');
    // Route::get('coupon-delete/{id}', 'CouponController@delete')->name('admin.coupon.delete');
    // Route::get('coupon-status/{id}', 'CouponController@status')->name('admin.coupon.status');

    // For Coupon Management
    Route::get('coupon', 'Modules\Coupon\CouponController@index')->name('admin.coupon');
    Route::get('coupon-create', 'Modules\Coupon\CouponController@create')->name('admin.coupon.create');
    Route::post('coupon-store', 'Modules\Coupon\CouponController@store')->name('admin.coupon.store');
    Route::get('coupon-edit/{id}', 'Modules\Coupon\CouponController@edit')->name('admin.coupon.edit');
    Route::post('coupon-update/{id}', 'Modules\Coupon\CouponController@update')->name('admin.coupon.update');
    Route::get('coupon-delete/{id}', 'Modules\Coupon\CouponController@delete')->name('admin.coupon.delete');
    Route::get('coupon-status/{id}', 'Modules\Coupon\CouponController@status')->name('admin.coupon.status');

    // for product section content
    Route::get('product-section/update', 'Modules\Content\ContentController@updateProductsSection')->name('admin.product.section.update');
    Route::post('product-section/update', 'Modules\Content\ContentController@updateProductsSection')->name('admin.product.section.update');

    // for content
   //  Route::get('content', 'ContentController@index')->name('admin.content.index');
   //  Route::get('home-page-content', 'ContentController@index1')->name('admin.content.index1');

   //  Route::get('home-page-header-content', 'ContentController@header')->name('admin.content.header');
   //  Route::post('save-page-header-content', 'ContentController@header')->name('admin.content.header.save');

   //  Route::get('content-edit/{id}', 'ContentController@edit')->name('admin.content.edit');
   //  Route::post('content-update/{id}', 'ContentController@update')->name('admin.content.update');
   //  Route::post('home-page-content-update', 'ContentController@update1')->name('admin.content.update1');
   //  Route::get('home-page-content-slider', 'HomeImageSliderController@index')->name('admin.content.slider.index');
   //  Route::get('home-page-content-slider-create', 'HomeImageSliderController@create')->name('admin.content.slider.create');
   //  Route::post('home-page-content-slider-store', 'HomeImageSliderController@store')->name('admin.content.slider.store');
   //  Route::get('home-page-content-slider-edit/{id}', 'HomeImageSliderController@edit')->name('admin.content.slider.edit');
   //  Route::post('home-page-content-slider-update/{id}', 'HomeImageSliderController@update')->name('admin.content.slider.update');
   //  Route::get('home-page-content-slider-delete/{id}', 'HomeImageSliderController@delete')->name('admin.content.slider.delete');
   //  Route::get('home-page-content-slider-status/{id}', 'HomeImageSliderController@status')->name('admin.content.slider.status');

    // for testimonial
    Route::get('testimonial', 'Modules\Testimonial\TestimonialController@index')->name('admin.testimonial.index');
    Route::get('testimonial-create', 'Modules\Testimonial\TestimonialController@create')->name('admin.testimonial.create');
    Route::post('testimonial-store', 'Modules\Testimonial\TestimonialController@store')->name('admin.testimonial.store');
    Route::get('testimonial-edit/{id}', 'Modules\Testimonial\TestimonialController@edit')->name('admin.testimonial.edit');
    Route::post('testimonial-update/{id}', 'Modules\Testimonial\TestimonialController@update')->name('admin.testimonial.update');
    Route::get('testimonial-delete/{id}', 'Modules\Testimonial\TestimonialController@delete')->name('admin.testimonial.delete');
    Route::get('testimonial-status/{id}', 'Modules\Testimonial\TestimonialController@status')->name('admin.testimonial.status');



    // Verify
    // Route::get('email/resend', 'Auth\VerificationController@resend')->name('admin.verification.resend');
    // Route::get('email/verify', 'Auth\VerificationController@show')->name('admin.verification.notice');
    // Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('admin.verification.verify');

   //  Route::get('users-membership', 'UserMembershipController@index')->name('admin.users.membership');

    Route::post('users-recharge', 'UserMembershipController@recharge')->name('cust.reacharge');


    // for currency converter
   //  Route::get('currency-converter', 'CurrencyConverterController@index')->name('admin.currency.converter');

    // for showing transaction details
   //  Route::get('all-transactions', 'TransactionController@index')->name('admin.transaction.details');
   //  Route::get('transactions/filter', 'TransactionController@index')->name('admin.transaction.filter');

   //  Route::get('transactions/{txnid}/status', 'TransactionController@status_cancel')->name('transaction.status.cancel');

   //  Route::get('transactions/{txnid1}/success', 'TransactionController@status_success')->name('transaction.status.success');

    // for testimonial section

    Route::get('testimonials', 'Modules\Testimonial\TestimonialController@view')->name('admin.testimonial');
    Route::post('testimonials', 'Modules\Testimonial\TestimonialController@view');

    Route::get('add-testimonial', 'Modules\Testimonial\TestimonialController@add')->name('admin.add.testimonial');

    Route::post('store-testimonial', 'Modules\Testimonial\TestimonialController@add')->name('admin.store.testimonial');

    Route::get('edit-testimonial/{id}', 'Modules\Testimonial\TestimonialController@edit')->name('admin.edit.testimonial');

    Route::post('update-testimonial/{id}', 'Modules\Testimonial\TestimonialController@edit')->name('admin.update.testimonial');

    Route::get('delete-testimonial/{id}', 'Modules\Testimonial\TestimonialController@delete')->name('admin.delete.testimonial');

    //For custom form
    Route::get('form/manage', 'Modules\Form\FormController@manage')->name('admin.form.manage');
    Route::get('form/add', 'Modules\Form\FormController@add')->name('admin.form.add');
    Route::post('form/add', 'Modules\Form\FormController@create')->name('admin.form.create');
    Route::get('form/edit/{id}', 'Modules\Form\FormController@edit')->name('admin.form.edit');
    Route::post('form/edit/{id}', 'Modules\Form\FormController@update')->name('admin.form.update');
    Route::get('form/delete/{id}', 'Modules\Form\FormController@delete')->name('admin.form.delete');
    Route::get('form/active/{id}', 'Modules\Form\FormController@statusChange')->name('admin.form.active');
    Route::get('form/details/manage/{id}', 'Modules\Form\FormController@FormDetailsManage')->name('admin.form.details.manage');
    Route::get('form/details/add/{id}', 'Modules\Form\FormController@FormDetailsAdd')->name('admin.form.details.add');
    Route::post('form/details/add/{id}', 'Modules\Form\FormController@FormDetailsCreate')->name('admin.form.details.create');
    Route::get('form/details/edit/{form}/{id}', 'Modules\Form\FormController@FormDetailsEdit')->name('admin.form.details.edit');
    Route::post('form/details/edit/{form}', 'Modules\Form\FormController@FormDetailsUpdate')->name('admin.form.details.update');
    Route::get('form/details/delete/{id}', 'Modules\Form\FormController@FormDetailsDelete')->name('admin.form.details.delete');
    Route::get('form/category/manage', 'Modules\Form\FormController@categoryManage')->name('admin.form.category.manage');
    Route::get('form/category/add', 'Modules\Form\FormController@categoryAdd')->name('admin.form.category.add');
    Route::post('form/category/add', 'Modules\Form\FormController@categoryCreate')->name('admin.form.category.create');
    Route::get('form/category/delete/{id}', 'Modules\Form\FormController@categoryDelete')->name('admin.form.category.delete');
    Route::get('form/category/edit/{id}', 'Modules\Form\FormController@categoryEdit')->name('admin.form.category.edit');
    Route::post('form/category/update/{id}', 'Modules\Form\FormController@categoryUpdate')->name('admin.form.category.update');
    Route::get('form/category/delete/{id}', 'Modules\Form\FormController@categoryDelete')->name('admin.form.category.delete');
    Route::get('imported/manage', 'Modules\Form\FormController@importedManage')->name('admin.imported.manage');
    Route::get('imported/add', 'Modules\Form\FormController@importedAdd')->name('admin.imported.add');
    Route::post('imported/add', 'Modules\Form\FormController@importedCreate')->name('admin.imported.create');
    Route::get('imported/edit/{id}', 'Modules\Form\FormController@importedEdit')->name('admin.imported.edit');
    Route::post('imported/edit/{id}', 'Modules\Form\FormController@importedUpdate')->name('admin.imported.update');
    Route::get('imported/delete/{id}', 'Modules\Form\FormController@importedDelete')->name('admin.imported.delete');

    //23.3.20 form tools
    Route::get('formtools-forproff/{id}', 'Modules\Form\FormController@formToolShowProff')->name('admin.formtools.forproff');
    //imported tools
    Route::get('importedtools-forproff/{id}', 'Modules\Form\FormController@importedToolShowProff')->name('admin.importedtools.forproff');

    // Tool 360 Evaluation Start

    Route::resource('tool-360-evaluation', 'Modules\Tool360Evaluation\Tool360EvaluationController');

    Route::get('tools360-forproff/{id}', 'Modules\Tool360Evaluation\Tool360EvaluationController@tool360ShowProff')->name('admin.tools360.forproff');
    // Tool 360 Evaluation End

    // Tool ContentTemplate 20.3.20 Start
    Route::resource('tool-content-template', 'Modules\ContentTemplate\ContentTemplateController');
    Route::get('contenttools-forproff/{id}', 'Modules\ContentTemplate\ContentTemplateController@contentToolShowProff')->name('admin.contenttools.forproff');
    // Content toos status change 19.3.20
    //Route::get('tool-content-template-status/{id}', 'Modules\ContentTemplate\ContentTemplateController@contenttempStatusUpdate')->name('admin.contenttemp.status');
    // Tool ContentTemplate End

    // Tool ContractTemplate 20.3.20 Start
    Route::resource('tool-contract-template', 'Modules\ContractTemplate\ContractTemplateController');
    Route::get('contracttools-forproff/{id}', 'Modules\ContractTemplate\ContractTemplateController@contractToolShowProff')->name('admin.contracttools.forproff');
    // Tool ContentTemplate End

    //Tool CertificateTemplate 05.07.2021 start
     Route::resource('tool-certificate-template', 'Modules\CertificateTemplate\CertificateTemplateController');
     Route::get('certificatetools-forproff/{id}', 'Modules\CertificateTemplate\CertificateTemplateController@certificateToolShowProff')->name('admin.certificatetools.forproff');

    //

    // Tool logbook 20.3.20 Start
    Route::resource('tool-logbook-tools', 'Modules\Logbook\LogbookController');
    Route::get('logbooktools-forproff/{id}', 'Modules\Logbook\LogbookController@logbookToolShowProff')->name('admin.logbooktools.forproff');
    // Tool ContentTemplate End

    // Evaluation 360 Start 26.3.20
    Route::resource('evaluation360', 'Modules\Evaluation360\Evaluation360Controller');
    // Evaluation 360 end 26.3.20

    // CompetencesMaster Start 13.4.20
    Route::resource('competences-master', 'Modules\CompetencesMaster\CompetencesMasterController');

    //CompetencesMasterQuestion
    Route::get('competences-master-question/{id}', 'Modules\CompetencesMaster\CompetencesMasterController@competencesQuestion')->name('admin.competences.question');
    Route::post('competences-update-question/{id}', 'Modules\CompetencesMaster\CompetencesMasterController@competencesQuestionUpdate')->name('admin.competences.question.update');

    // for question delete
    Route::post('competences-question-delete/{id}', 'Modules\CompetencesMaster\CompetencesMasterController@competencesQuestionDelete')->name('admin.competences.questiondel');

    Route::get('competences-add-question/{id}', 'Modules\CompetencesMaster\CompetencesMasterController@competencesQuestionAdd')->name('admin.competences.question.add');
    Route::post('competences-post-question', 'Modules\CompetencesMaster\CompetencesMasterController@competencesQuestionPost')->name('admin.competences.question.added');
    Route::post('load-types', 'Modules\CompetencesMaster\CompetencesMasterController@fetchEvalCatTypes')->name('load.types');
    Route::post('load-specialities', 'Modules\CompetencesMaster\CompetencesMasterController@fetchEvalCatSpeciality')->name('load.specialities');
    Route::post('load-type-specialities', 'Modules\CompetencesMaster\CompetencesMasterController@fetchEvalTypeSpeciality')->name('load.type.specialities');


    Route::resource('evaluation-type', 'Modules\EvaluationType\EvaluationTypeController');

    Route::get('evaluation-type/{id}/delete', 'Modules\EvaluationType\EvaluationTypeController@delete')->name('evaluation-type.delete');

    Route::get('evaluation-category', 'Modules\EvaluationType\EvaluationTypeController@evaluationCategory')->name('evaluation.category');
    Route::get('evaluation-category/add', 'Modules\EvaluationType\EvaluationTypeController@addEvaluationCategory')->name('evaluation.category.add');
    Route::post('evaluation-category/add', 'Modules\EvaluationType\EvaluationTypeController@addEvaluationCategory');
    Route::get('evaluation-category/edit/{id}', 'Modules\EvaluationType\EvaluationTypeController@editEvaluationCategory')->name('evaluation.category.edit');
    Route::post('evaluation-category/edit/{id}', 'Modules\EvaluationType\EvaluationTypeController@editEvaluationCategory');
    Route::get('evaluation-category/delete/{id}', 'Modules\EvaluationType\EvaluationTypeController@deleteEvaluationCategory')->name('evaluation.category.delete');
    Route::post('fetch-evaluation-category-speciality', 'Modules\EvaluationType\EvaluationTypeController@fetchEvalCats')->name('dynamic.fetchevalcats');


    // CompetencesMaster  end 13.4.20

    Route::get('mail-template', 'Modules\MailTemplate\MailTemplateController@index')->name('admin.mail.template');
    Route::get('mail-template/edit/{id}', 'Modules\MailTemplate\MailTemplateController@edit')->name('admin.mail.template.edit');
    Route::post('mail-template/edit/{id}', 'Modules\MailTemplate\MailTemplateController@edit');

    //Professional Specialties
    Route::get('specialties', 'Modules\Specialty\SpecialtyController@index')->name('admin.specialty');
    Route::get('specialties/add', 'Modules\Specialty\SpecialtyController@add')->name('admin.specialty.add');
    Route::post('specialties/add', 'Modules\Specialty\SpecialtyController@add');
    Route::get('specialties/edit/{id}', 'Modules\Specialty\SpecialtyController@edit')->name('admin.specialty.edit');
    Route::post('specialties/edit/{id}', 'Modules\Specialty\SpecialtyController@edit');
    Route::get('specialties/delete/{id}', 'Modules\Specialty\SpecialtyController@delete')->name('admin.specialty.delete');

     //Professional Expirienecs
     Route::get('experience', 'Modules\Experience\ExperienceController@index')->name('admin.experiences');
     Route::post('experience', 'Modules\Experience\ExperienceController@index')->name('admin.experiences');
     Route::get('experience/add', 'Modules\Experience\ExperienceController@store')->name('admin.experience.add');
     Route::post('experience/add', 'Modules\Experience\ExperienceController@store');
     Route::get('experience/edit/{id}', 'Modules\Experience\ExperienceController@store')->name('admin.experience.edit');
     Route::post('experience/edit/{id}', 'Modules\Experience\ExperienceController@store');
     Route::get('experience/delete/{id}', 'Modules\Experience\ExperienceController@delete')->name('admin.experience.delete');


    Route::get('refer-discount', 'Modules\Referral\ReferralDiscountController@index')->name('admin.refer.discount');
    Route::get('refer-discount/edit/{id}', 'Modules\Referral\ReferralDiscountController@edit')->name('admin.refer.discount.edit');
    Route::post('refer-discount/update/{id}', 'Modules\Referral\ReferralDiscountController@update')->name('admin.refer.discount.update');
    Route::get('payment-reject/{slug}', 'Modules\Order\OrderController@rejectPayment')->name('admin.payment.reject');
    Route::get('payment-approve/{slug}', 'Modules\Order\OrderController@approvePayment')->name('admin.payment.approve');


    Route::get('account', 'Modules\Account\BankAccountController@index')->name('admin.account');
    Route::get('account/edit/{id}', 'Modules\Account\BankAccountController@edit')->name('admin.account.edit');
    Route::post('account/update/{id}', 'Modules\Account\BankAccountController@update')->name('admin.account.update');

    Route::get('document', 'Modules\DocumentsSignature\DocumentsSignatureController@index')->name('admin.document.index');
    Route::get('document/add', 'Modules\DocumentsSignature\DocumentsSignatureController@add')->name('admin.document.add');
    Route::post('document/add/success', 'Modules\DocumentsSignature\DocumentsSignatureController@addSuccess')->name('admin.document.add.success');
    Route::get('document/edit/{id}', 'Modules\DocumentsSignature\DocumentsSignatureController@edit')->name('admin.document.edit');
    Route::post('document/edit/{id}/success', 'Modules\DocumentsSignature\DocumentsSignatureController@editSuccess')->name('admin.document.edit.success');
    Route::get('document/status/{id}', 'Modules\DocumentsSignature\DocumentsSignatureController@statusChange')->name('admin.document.status');
    Route::get('document/delete/{id}', 'Modules\DocumentsSignature\DocumentsSignatureController@delete')->name('admin.document.delete');

    Route::get('commission', 'Modules\Commission\CommissionController@index')->name('admin.commission');
    Route::post('commission-edit/{id}', 'Modules\Commission\CommissionController@editCommission')->name('admin.commission.edit');
    Route::get('category-commission/{id}', 'Modules\Commission\CommissionController@categoryCommissionEdit')->name('category.commission.edit');
    Route::post('category-commission/{id}', 'Modules\Commission\CommissionController@categoryCommissionEdit')->name('category.commission.edit');

    Route::get('product-category', 'Modules\Product\ProductController@index')->name('admin.product.category');
    Route::get('product-category/add', 'Modules\Product\ProductController@addCategoryView')->name('admin.product.category.add.view');
    Route::post('product-category/add', 'Modules\Product\ProductController@addCategory')->name('admin.product.category.add');
    Route::get('product-category/edit/{id}', 'Modules\Product\ProductController@editCategoryView')->name('admin.product.category.edit.view');
    Route::post('product-category/edit/{id}', 'Modules\Product\ProductController@editProductCategory')->name('admin.product.category.edit');
    Route::get('product-subcategory/store/{pid}/{id?}', 'Modules\Product\ProductController@storeSubcategoryView')->name('admin.product.subcategory.store.view');
    Route::post('product-subcategory/store', 'Modules\Product\ProductController@storeProductSubcategory')->name('admin.product.subcategory.store');
    Route::get('product-category/delete/{id}', 'Modules\Product\ProductController@deleteProductCategory')->name('admin.product.category.delete');
    Route::get('product-subcategory/delete/{id}', 'Modules\Product\ProductController@deleteProductSubcategory')->name('admin.product.subcategory.delete');
    Route::get('product-category/status/{id}', 'Modules\Product\ProductController@statusChangeProductCategory')->name('admin.product.category.status.change');
    Route::get('product-category/view/{id}', 'Modules\Product\ProductController@viewProductCategory')->name('admin.product.category.view');
    Route::get('product-subcategory/status/{id}', 'Modules\Product\ProductController@statusChangeProductSubcategory')->name('admin.product.subcategory.status.change');

    Route::get('product', 'Modules\Product\ProductController@productList')->name('admin.product.list');
    Route::post('product', 'Modules\Product\ProductController@productList')->name('admin.product.list.search');

    // Route::get('product-category-search', 'Modules\Product\ProductController@productCategory')->name('admin.product.category.search');

    Route::post('product-category', 'Modules\Product\ProductController@index')->name('admin.product.category.search');


    Route::get('product/details/{id}', 'Modules\Product\ProductController@viewProduct')->name('admin.product.view');

    Route::get('product/chapter-details/{id}', 'Modules\Product\ProductController@viewChapter')->name('admin.product.chapter.view');

    Route::get('product/lesson-details/{id}', 'Modules\Product\ProductController@viewLesson')->name('admin.product.lesson.view');

    Route::get('product/lesson-details/details/{id}', 'Modules\Product\ProductController@viewLessonDetails')->name('admin.lesson.view');

    Route::get('product/lesson-details/details/lesson-files/{id}', 'Modules\Product\ProductController@viewLessonFileDetails')->name('admin.lesson.files.view');

    Route::get('product/lesson-details/details/slides/{id}', 'Modules\Product\ProductController@viewLessonSlideDetails')->name('admin.lesson.slides.view');

    Route::get('product/lesson-details/details/question-answer/{id}','Modules\Product\ProductController@viewLessonQA')->name('admin.lesson.qa');




    Route::get('product/delete/{id}', 'Modules\Product\ProductController@deleteProduct')->name('admin.product.delete');
    Route::get('product/status/{id}', 'Modules\Product\ProductController@statusChangeProduct')->name('admin.product.status.change');
    Route::get('product/showinhomepage/{id}', 'Modules\Product\ProductController@showProductInHomePage')->name('admin.product.showinhomepage');

    Route::get('order-product', 'Modules\Product\ProductController@OrderProductList')->name('admin.order.product');
    Route::post('order-product', 'Modules\Product\ProductController@OrderProductList')->name('admin.order.product.search');
    Route::get('order-product-view/{token}', 'Modules\Product\ProductController@viewOrderDetails')->name('admin.order.product.view');
    Route::get('payment-reject-product/{slug}', 'Modules\Order\OrderController@rejectProductOrderPayment')->name('admin.payment.reject.product');
    Route::get('payment-approve-product/{slug}', 'Modules\Order\OrderController@approveProductOrderPayment')->name('admin.payment.approve.product');
    Route::any('order-cancel-product', 'Modules\Product\ProductController@OrderProductCancelList')->name('admin.order.cancel.product.search');


    Route::get('bank-test/{user_id}/{id}/{token}', 'Modules\Professional\ProfessionalController@createBankAccount');

    Route::get('reviews','Modules\Reviews\ReviewController@manageReveiws')->name('admin.manage.reviews');

    Route::post('reviews','Modules\Reviews\ReviewController@manageReveiws')->name('admin.manage.reviews');

    Route::get('review-delete/{id?}','Modules\Reviews\ReviewController@deleteReview')->name('admin.review.delete');

    Route::get('affiliate','Modules\Affiliate\AffiliateController@manageAffiliate')->name('admin.manage.affiliate');
    Route::post('affiliate','Modules\Affiliate\AffiliateController@manageAffiliate')->name('admin.manage.affiliate');
    Route::get('affiliated-products/{id}','Modules\Affiliate\AffiliateController@viewAffiliatedProducts')->name('admin.manage.affiliated.product');
    Route::get('affiliated-product-details/{id}','Modules\Affiliate\AffiliateController@viewAffiliatedProductDetails')->name('admin.affiliated.product.details');

    Route::get('affiliate/blockuser/{id}', 'Modules\Affiliate\AffiliateController@blockUserAffiliation')->name('admin.affiliate.blockuser');
    Route::get('affiliate-earnings/{id}','Modules\Affiliate\AffiliateController@viewAffiliateEarnings')->name('admin.affiliate.earnings');

    Route::get('affiliate-product-purchase-details/{id}/{pro_id}','Modules\Affiliate\AffiliateController@affiliateProductPurchaseDetail')->name('admin.affiliate.product.purchase.details');

    Route::get('manage-language','Modules\Language\LanguageController@index')->name('manage.language');
    Route::get('manage-language/create','Modules\Language\LanguageController@show')->name('manage.language.add');
    Route::post('manage-language/create/store','Modules\Language\LanguageController@store')->name('manage.language.add.store');
    Route::get('manage-language/edit/{id}','Modules\Language\LanguageController@show')->name('manage.languages.edit');
    Route::post('manage-language/edit/update/{id}','Modules\Language\LanguageController@update')->name('manage.languages.edit.update');

    Route::any('video-affiliate','Modules\Affiliate\AffiliateController@manageVideoAffiliate')->name('admin.manage.video.affiliate');
    Route::get('video-affiliate-commission','Modules\Affiliate\AffiliateController@affiliateCommission')->name('admin.manage.video.affiliate.commission');
    Route::post('video-affiliate-commission-edit/{id}', 'Modules\Affiliate\AffiliateController@editCommission')->name('admin.manage.video.affiliate.commission.edit');
    Route::get('video-affiliate-earning-list/{id}','Modules\Affiliate\AffiliateController@manageVideoAffiliateEarning')->name('admin.manage.video.affiliate.earning');
    Route::post('video-affiliate-payment','Modules\Affiliate\AffiliateController@videoAffiliatePayment')->name('admin.manage.video.affiliate.payment');
    Route::any('video-affiliate-banner/{id}','Modules\Affiliate\AffiliateController@videoAffiliateBanner')->name('admin.manage.video.affiliate.banner');
    Route::any('video-affiliate-banner-list','Modules\Affiliate\AffiliateController@videoAffiliateBannerList')->name('admin.manage.video.affiliate.banner.list');

    Route::any('landing-page-list','Modules\LandingPage\LandingPageController@index')->name('admin.landing.page.list');
    Route::get('landing-page-price','Modules\LandingPage\LandingPageController@landingPagePrice')->name('admin.landing.page.price');
    Route::post('landing-page-price','Modules\LandingPage\LandingPageController@landingPagePriceUpdate')->name('admin.landing.page.price.update');
    Route::get('landing-page/leads/{id}','Modules\LandingPage\LandingPageController@viewLeads')->name('admin.landing.page.leads');
    Route::any('landing-page-payment','Modules\LandingPage\LandingPageController@viewPayments')->name('admin.landing.page.payment');
    Route::get('landing-page/{status}/{id}','Modules\LandingPage\LandingPageController@landingStatus')->name('admin.landing.page.status');

    Route::any('manage-category-landing','Modules\Category\CategoryLandingController@index')->name('admin.category.landing.index');
    Route::get('category-landing-add','Modules\Category\CategoryLandingController@categoryLandingAddEdit')->name('admin.category.landing.add');
    Route::Post('category-landing-save','Modules\Category\CategoryLandingController@categoryLandingAddEditSave')->name('admin.category.landing.add.save');
    Route::get('category-landing-edit/{id}','Modules\Category\CategoryLandingController@categoryLandingAddEdit')->name('admin.category.landing.edit');
    Route::Post('category-landing-update/{id}','Modules\Category\CategoryLandingController@categoryLandingAddEditSave')->name('admin.category.landing.add.update');
    Route::get('manage-category-landing/status/{id}','Modules\Category\CategoryLandingController@changeStatus')->name('admin.category.landing.status');
    Route::any('category-landing-delete/{id}','Modules\Category\CategoryLandingController@delete')->name('admin.category.landing.delete');
    Route::any('category-landing-show-footer/{id}','Modules\Category\CategoryLandingController@showInFooter')->name('admin.category.landing.show.footer');

});
