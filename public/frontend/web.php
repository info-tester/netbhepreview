<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::post('update-timezone', 'HomeController@updateTimezone')->name('update.timezone');

Route::group(['middleware' => ['set.timezone']], function () {

    Auth::routes();
    Route::any('/ware-card', 'HomeController@wireCard');
    // Route::get('/', function(){ return view("modules.maintenance"); })->name('home');
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('refer/{id}', 'HomeController@refer')->name('refer');
    Route::get('refer', 'HomeController@referUser')->name('refer.page');
    Route::get('refer-list', 'HomeController@referUserList')->name('refer.page.list');
    Route::get('logout', 'Auth\LoginController@logout')->name('logout');

    Route::get('public-profile/{slug}/free-session/{id}', 'HomeController@freeSession')->name('freeSession.invite');
    Route::get('invite/free-session', 'HomeController@inviteFreeSession')->name('invite.page');
    // for expert registration........
    Route::get('expert-landing', 'Auth\RegisterController@showExpertLandingPage')->name('exp.landing');
    Route::get('be-an-expert', 'Auth\RegisterController@showExpertRegistrationForm')->name('exp.register');

    // for social login........
    Route::get('oauth/{fb}/login', 'Auth\RegisterController@redirect')->name('social.login');

    Route::get('oauth/successfull', 'Auth\RegisterController@redirect')->name('oauth.successfull');

    Route::get('oauth/{prvdr}/callback', 'Auth\RegisterController@callback');

    // for email duplicate checking while registering user....
    Route::post('check-email', 'Auth\RegisterController@check_email')->name('chk.email');
    // For successpage after registration.....
    Route::get('success', 'Auth\RegisterController@successLoad')->name('front.success');

    // for fetching sub category while registering user..........
    Route::post('get-subcat', 'Auth\RegisterController@getSubcat')->name('get.subcat');


    //for products
    Route::get('products', 'Modules\Professional\ProfessionalProductsController@index')->name('professional.products');
    Route::post('product-search', 'Modules\Professional\ProfessionalProductsController@index')->name('search.product');
    Route::get('store-product/{id?}', 'Modules\Professional\ProfessionalProductsController@store')->name('store.product');
    Route::post('store-product/{id?}', 'Modules\Professional\ProfessionalProductsController@store')->name('store.product');

    Route::get('product-file/{id}', 'Modules\Professional\ProfessionalProductsController@productFileView')->name('product.file.view');
    Route::get('product-file-upload/{id}', 'Modules\Professional\ProfessionalProductsController@productFileUploadView')->name('product.file.upload.view');
    Route::post('product-file-upload/{id}', 'Modules\Professional\ProfessionalProductsController@productFileUpload')->name('product.file.upload');
    Route::get('product-file-delete/{id}', 'Modules\Professional\ProfessionalProductsController@productFileDelete')->name('product.file.delete');
    Route::get('change-status-product/{id}', 'Modules\Professional\ProfessionalProductsController@changeProductStatus')->name('changestatus.product');
    Route::get('delete-product/{id}', 'Modules\Professional\ProfessionalProductsController@deleteProduct')->name('delete.product');
    Route::get('product-details/{slug}', 'Modules\Professional\ProfessionalProductsController@productDetails')->name('product.details');
    Route::get('my-sales', 'Modules\Professional\ProfessionalProductsController@professionalOrders')->name('professional.orders');
    Route::post('my-sales', 'Modules\Professional\ProfessionalProductsController@professionalOrders')->name('professional.orders.search');
    Route::get('view-order/{id}', 'Modules\Professional\ProfessionalProductsController@viewProfessionalOrder')->name('view.professional.order');
    Route::post('upload-product-file/{id}', 'Modules\Professional\ProfessionalProductsController@uploadProductFiles')->name('upload.product.files');

    //professional add coupons
    Route::get('my-coupons', 'Modules\Professional\ProfessionalProductsController@professionalCouponList')->name('professional.coupons');
    Route::get('add-coupon/{id?}', 'Modules\Professional\ProfessionalProductsController@professionalAddCoupon')->name('professional.add.coupon');
    Route::post('store-coupon', 'Modules\Professional\ProfessionalProductsController@storeCoupon')->name('store.coupon');
    Route::get('delete-coupon/{id}', 'Modules\Professional\ProfessionalProductsController@deleteCoupon')->name('delete.coupon');
    Route::post('check-coupon-product', 'Modules\ProductOrder\ProductOrderController@checkCoupon')->name('check.coupon.product');
    Route::post('add-payment-method-product/{slug}', 'Modules\ProductOrder\ProductOrderController@addPaymentCoupon')->name('add.pamentmethod.coupon.product');
    Route::post('upload-payment-document-product/{slug}', 'Modules\ProductOrder\ProductOrderController@uploadDocument')->name('upload.payment.document.product');


    // for products - user
    Route::get('my-purchases', 'Modules\User\UserProfileController@myProductOrders')->name('user.products');
    Route::post('my-purchases', 'Modules\User\UserProfileController@myProductOrders')->name('user.products.search');
    Route::get('order-details/{id}', 'Modules\User\UserProfileController@userProductOrderDetails')->name('order.details');


    // for professional profile edit..........
    Route::post('get-state', 'Modules\User\UserProfileController@getState')->name('front.get.state');


    Route::post('get-city', 'Modules\Professional\ProfessionalProfileController@getCity')->name('front.get.city');

    Route::get('edit-profile', 'Modules\Professional\ProfessionalProfileController@index')->name('professional.profile');
    Route::get('introductory-video', 'Modules\Professional\ProfessionalProfileController@introductoryVideo')->name('professional.profile.introductory.video');
    Route::post('introductory-video', 'Modules\Professional\ProfessionalProfileController@introductoryVideoSubmit')->name('professional.profile.introductory.video.submit');

    Route::get('profile-category', 'Modules\Professional\ProfessionalProfileController@profileCategory')->name('professional.change.category');
    Route::post('profile-category', 'Modules\Professional\ProfessionalProfileController@profileCategory');

    Route::post('set-bank', 'Modules\Professional\ProfessionalProfileController@addBank')->name('set.bank');

    Route::post('check-mobile', 'Modules\Professional\ProfessionalProfileController@checkMobile')->name('chk.mobile');


    Route::post('update-professional-account', 'Modules\Professional\ProfessionalProfileController@index')->name('my.profile');

    // for loading professionals dashboard..............
    Route::get('dashboard', 'Modules\Professional\ProfessionalProfileController@load_professional_dashboard')->name('load_professional_dashboard');

    Route::get('qualification', 'Modules\Professional\ProfessionalProfileController@professional_qualification')->name('professional_qualification');

    Route::post('add-qualification', 'Modules\Professional\ProfessionalProfileController@professional_qualification')->name('add_professional_qualification');

    Route::get('qualification/{id}', 'Modules\Professional\ProfessionalProfileController@update_professional_qualification')->name('my_qualification');

    Route::post('update-qualification/{id}', 'Modules\Professional\ProfessionalProfileController@update_professional_qualification')->name('my_qualification_update');

    Route::get('remove-qualification/{pid}', 'Modules\Professional\ProfessionalProfileController@delete_qualification')->name('delete_qualification');

    // For professional experience

    Route::get('my-experience', 'Modules\Professional\ProfessionalProfileController@professionalExperience')->name('professional.exp');

    Route::post('add-experience', 'Modules\Professional\ProfessionalProfileController@professionalExperience')->name('add.professional.experience');

    Route::get('edit-experience/{id}', 'Modules\Professional\ProfessionalProfileController@edit_professional_experience')->name('edit.professional.experience');

    Route::post('update-experience/{id}', 'Modules\Professional\ProfessionalProfileController@edit_professional_experience')->name('update.professional.experience');

    Route::get('delete-experience/{id}', 'Modules\Professional\ProfessionalProfileController@delete_experience')->name('delete.experience');

    // For Verifiying user email..........
    Route::get('check-mail/{vcode}', 'Auth\RegisterController@verifyMail')->name('check.email.verify');

    // For User Avaliability...................

    Route::get('availability', 'Modules\Professional\ProfessionalProfileController@user_availability')->name('user_availability');

    Route::post('availability', 'Modules\Professional\ProfessionalProfileController@user_availability')->name('submit_user_availability');

    Route::post('availability-update', 'Modules\Professional\ProfessionalProfileController@user_availability')->name('user_availability_update');
    Route::post('availability-update/ajax-check', 'Modules\Professional\ProfessionalProfileController@userAvailabilityAjaxCheck')->name('user_availability_update.ajax.check');

    Route::post('availability-update', 'Modules\Professional\ProfessionalProfileController@user_availability_insert')->name('user.insert.avl');

    Route::get('availability-delete/{id}', 'Modules\Professional\ProfessionalProfileController@delete_avl')->name('delete_avl');
    Route::get('availability-delete-all', 'Modules\Professional\ProfessionalProfileController@deleteAvlAll')->name('delete_avl.all');

    // For search..................

    Route::get('consultant/{slug}', 'Modules\Search\SearchController@index')->name('srch.slug');

    Route::post('consultant', 'Modules\Search\SearchController@index')->name('srch.pst');

    Route::get('consultant', 'Modules\Search\SearchController@index')->name('srch.pst');

    Route::post('fetch-subcat', 'Modules\Search\SearchController@getSubcat')->name('fetch.subcat');

    Route::get('public-profile/{slug}', 'Modules\Professional\PublicProfileController@index')->name('pblc.pst');
    Route::get('public-profile-preview/{slug}/{self}', 'Modules\Professional\PublicProfileController@index')->name('public.profile.preview');
    // Add to favourite
    Route::post('add-tp-favourite', 'Modules\Professional\PublicProfileController@addFavourite')->name('add.favourite');

    Route::get('reschedule-approve/{id}', 'Modules\Professional\PublicProfileController@rescheduleApprove')->name('reschedule.approve');
    Route::get('reschedule-reject/{id}', 'Modules\Professional\PublicProfileController@rescheduleReject')->name('reschedule.reject');

    Route::get('browse-categories', 'Modules\Search\SearchController@browseCat')->name('browse.categories');



    // For User Profile Managment

    Route::get('my-dashboard', 'Modules\User\UserProfileController@load_user_dashboard')->name('load_user_dashboard');

    Route::get('edit-my-profile', 'Modules\User\UserProfileController@index')->name('user.profile');


    Route::post('update-user-account', 'Modules\User\UserProfileController@index')->name('user.update.profile');

    // For Forget Password

    Route::get('forget-password', 'Auth\RegisterController@forgetPass')->name('forget.pass');

    Route::post('send-password-link', 'Auth\RegisterController@forgetPass')->name('send.pass.link');

    Route::get('reset-password/{code}', 'Auth\RegisterController@resetPass')->name('resetPass');

    Route::post('update-password', 'Auth\RegisterController@updatePass')->name('updatePass');

    Route::get('became-professional', 'Modules\User\UserProfileController@becameProfessional')->name('be.professional');

    Route::post('store-professional', 'Modules\User\UserProfileController@becameProfessional')->name('store.professional');

    // for fetching sub category while registering user..........
    Route::post('front-get-subcat', 'Modules\User\UserProfileController@getSubcat')->name('front.get.subcat');

    Route::get('rescheduled-time-list', 'Modules\User\UserProfileController@rescheduledTimeList')->name('rescheduled.time.list');

    // For Favourite List

    Route::get('favourite-expert', 'Modules\User\UserProfileController@favouriteList')->name('favourite.list');

    Route::get('remove-favourite/{id}', 'Modules\User\UserProfileController@removeFav')->name('remove.my.favourite');


    // For Booking Managment

    Route::get('booking/{slug}', 'Modules\Booking\BookingController@index')->name('book.now');

    Route::post('pay-now', 'Modules\Booking\BookingController@createCustomer')->name('pay.now');

    Route::post('complete-profile', 'Modules\Booking\BookingController@completeProfile')->name('cmplt.user.profile');

    Route::post('store-booking/{slug}', 'Modules\Booking\BookingController@storeBooking')->name('store.booking');

    Route::post('chcek-booking', 'Modules\Booking\BookingController@checkDate')->name('checkDate');

    Route::get('successfully-booked/{token}', 'Modules\Booking\BookingController@bookingSuccess')->name('booking.sces');
    Route::post('front-get-date', 'Modules\Booking\BookingController@getDate')->name('front.get.date');


    // For user booking

    Route::get('my-bookings/{type}', 'Modules\User\UserProfileController@bookingRequest')->name('user.my.booking');

    Route::get('my-bookings/filter/{type}', 'Modules\User\UserProfileController@bookingRequest')->name('srch.user.my.booking');

    Route::get('my-bookings/filter/{type}', 'Modules\User\UserProfileController@bookingRequest')->name('srch.user.my.booking');

    Route::get('cancel-my-booking/{token}', 'Modules\User\UserProfileController@cancelBookingRequest')->name('user.cancel.req');

    Route::post('reschedeule-my-booking', 'Modules\User\UserProfileController@rescheduleBookingRequest')->name('user.booking.reschedule');

    Route::get('view-booked-details/{id}', 'Modules\User\UserProfileController@viewBookedDetails')->name('user.booking.details1');

    // For professional booking from Dashboard
    Route::get('dashboard-bookings/{type}', 'Modules\Professional\ProfessionalProfileController@load_professional_dashboard')->name('prof.dash.my.booking');

    Route::get('dashboard-bookings/filter/{type}', 'Modules\Professional\ProfessionalProfileController@load_professional_dashboard')->name('srch.prof.dash.my.booking');

    Route::get('dashboard-bookings/filter/{type}', 'Modules\Professional\ProfessionalProfileController@load_professional_dashboard')->name('srch.prof.dash.my.booking');

    // For professional booking

    Route::get('bookings/{type}', 'Modules\Professional\ProfessionalProfileController@bookingRequest')->name('prof.my.booking');

    Route::get('bookings/filter/{type}', 'Modules\Professional\ProfessionalProfileController@bookingRequest')->name('srch.prof.my.booking');

    Route::get('bookings/filter/{type}', 'Modules\Professional\ProfessionalProfileController@bookingRequest')->name('srch.prof.my.booking');

    Route::get('cancel-booking/{token}', 'Modules\Professional\ProfessionalProfileController@cancelBookingRequest')->name('prof.my.cancel.req');

    Route::get('accsept-booking/{token}', 'Modules\Professional\ProfessionalProfileController@accseptBookingRequest')->name('prof.acc.req');

    Route::get('view-booking-details/{id}', 'Modules\Professional\ProfessionalProfileController@viewBookedUserDetails')->name('view.booked.user.details');



    Route::get('professional-upcomming-class', 'Modules\Professional\ProfessionalProfileController@upcommingClass')->name('professional.upcomming.class');
    Route::post('professional-upcomming-class', 'Modules\Professional\ProfessionalProfileController@upcommingClass')->name('professional.upcomming.class');
    Route::get('professional-as-user-upcomming-class', 'Modules\Professional\ProfessionalProfileController@upcommingClass1')->name('professional.as.user.upcomming.class');
    Route::post('professional-as-user-upcomming-class', 'Modules\Professional\ProfessionalProfileController@upcommingClass1')->name('professional.as.user.upcomming.class');

    // For messaging servicce..................

    Route::get('message', 'Modules\Professional\ProfessionalMessageController@index')->name('message');

    Route::get('compose-message', 'Modules\Professional\ProfessionalMessageController@compose')->name('compose');

    Route::get('compose-message/{id}', 'Modules\Professional\ProfessionalMessageController@compose')->name('compose.msg');

    Route::get('delete-conversation/{token}', 'Modules\Professional\ProfessionalMessageController@deleteMessage')->name('delete_message');

    Route::get('message/{token}/details', 'Modules\Professional\ProfessionalMessageController@viewMessage')->name('viewMessage');

    Route::post('insrt-msg-prf', 'Modules\Professional\ProfessionalMessageController@insertMsg')->name('insrt.msg.prf');

    Route::post('insrt-atch', 'Modules\Professional\ProfessionalMessageController@insertAtch')->name('insrt.atch');

    Route::get('reply/{id}', 'Modules\Professional\ProfessionalMessageController@reply')->name('user.reply');

    Route::post('insrt-msg-reply', 'Modules\Professional\ProfessionalMessageController@insertReplyMsg')->name('insrt.msg.reply');

    //user upcomming class
    // Route::get('user-past-class', 'Modules\User\UserProfileController@pastClass')->name('user.past.class');

    // Route::post('user-past-class', 'Modules\User\UserProfileController@pastClass')->name('user.past.filter.class');

    Route::get('user-upcomming-class', 'Modules\User\UserProfileController@upcommingList')->name('user.upcomming.class');

    Route::post('user-upcomming-class', 'Modules\User\UserProfileController@upcommingList')->name('user.upcomming.class');
    Route::get('review/{booking_id}/{professional_id}', 'Modules\User\UserProfileController@review')->name('user.review');
    Route::post('user-post-review', 'Modules\User\UserProfileController@postReview')->name('user.post.review');

    // For video conference

    Route::get('video-call', 'Modules\VideoCall\VideoCallController@index')->name('my.video');

    Route::post('update-video-status', 'Modules\VideoCall\VideoCallController@updateStatus')->name('update.video.status');
    Route::post('update-video-initiated', 'Modules\VideoCall\VideoCallController@updateVideoInitiated')->name('update.video.initiated');

    Route::post('update-call-time', 'Modules\VideoCall\VideoCallController@updateCallTime')->name('update.call.time');

    Route::get('get-twilio-token', 'Modules\VideoCall\VideoCallController@getTwilioToken')->name('get.twilio.token');
    Route::post('video-call-booking-details', 'Modules\VideoCall\VideoCallController@getBookingDetails')->name('video.booking.details');


    // For blog managment.....

    Route::get('my-blog', 'Modules\Blog\BlogController@index')->name('my.blog');

    Route::post('my-blog', 'Modules\Blog\BlogController@index')->name('my.blog.search');

    Route::get('add-blog-post', 'Modules\Blog\BlogController@add')->name('add.blog.post');

    Route::post('store-blog-post', 'Modules\Blog\BlogController@add')->name('store.blog.post');

    Route::get('view-blog-post/{id}', 'Modules\Blog\BlogController@view')->name('view.blog.post');

    Route::get('remove-blog-post/{id}', 'Modules\Blog\BlogController@delete')->name('remove.blog.post');

    Route::get('edit-blog-post/{id}', 'Modules\Blog\BlogController@edit')->name('edit.blog.post');

    Route::post('edit-blog-post/{id}', 'Modules\Blog\BlogController@edit')->name('update.blog.post');

    Route::post('article-image-upload', 'Modules\Blog\BlogController@imgUpload')->name('artical.img.upload');

    Route::get('blog', 'Modules\FrontendBlog\FrontendBlogController@index')->name('blog.frontend');

    Route::get('blog/{slug}', 'Modules\FrontendBlog\FrontendBlogController@index')->name('blog.frontend.slug');

    Route::get('blog-details/{slug}', 'Modules\FrontendBlog\FrontendBlogController@details')->name('blog.frontend.details');

    Route::post('publish-comment', 'Modules\FrontendBlog\FrontendBlogController@comments')->name('publish.comment');

    // For inserting email on newsletter
    Route::post('news-letter', 'Modules\Newsletter\NewsletterController@insert')->name('news_letter');

    // For About Us
    Route::get('about-us', 'Modules\AboutUs\AboutUsController@index')->name('frontend.about');

    //For content management
    Route::get('terms-of-services', 'Modules\Content\ContentController@termsOfservices')->name('terms.of.services');
    Route::get('privacy-policy', 'Modules\Content\ContentController@privacyPolicy')->name('privacy.policy');
    Route::get('faq', 'Modules\Content\ContentController@faq')->name('faq');

    // Route::get('paytest', 'Modules\TestPayment\PaymentController@index');

    // Route::get('create-customer', 'Modules\TestPayment\PaymentController@customer');

    // Route::post('create-transparent', 'Modules\TestPayment\PaymentController@index')->name('create_trans');

    // Route::get('create-order', 'Modules\TestPayment\PaymentController@order')->name('create_order');

    // Route::get('add-bank', 'Modules\TestPayment\PaymentController@addBankAccount');

    // Route::post('create-bank', 'Modules\TestPayment\PaymentController@addBankAccount')->name('create_ac');


    // For form Toots start
    Route::get('user-view-form', 'Modules\Form\FormController@userViewForm')->name('user.view.form');
    Route::get('user-view-form-details/{id}', 'Modules\Form\FormController@userViewFormDetails')->name('user.view.form.details');
    Route::post('user-view-form-post', 'Modules\Form\FormController@userViewFormPost')->name('user.view.form.post');
    Route::get('user-view-form-submit-details/{id}', 'Modules\Form\FormController@userViewFormSubmitDetails')->name('user.view.form.submit.details');

    // For form Toots end


    // for form Tools start for professional
    Route::get('professional-view-form', 'Modules\Form\FormController@professionalViewForm')->name('professional.view.form');

    Route::resource('professional-form', 'Modules\ProfessionalForm\ProfessionalFormController');
    Route::get('professional-form-question/create/{id}', 'Modules\ProfessionalForm\ProfessionalQuestionController@create')->name('form.question.create');
    Route::post('professional-form-question/create/{id}', 'Modules\ProfessionalForm\ProfessionalQuestionController@store')->name('form.question.store');
    Route::get('professional-form-question/manage/{id}', 'Modules\ProfessionalForm\ProfessionalQuestionController@index')->name('form.question.manage');
    Route::get('professional-form-question/edit/{form}/{id}', 'Modules\ProfessionalForm\ProfessionalQuestionController@edit')->name('form.question.edit');
    Route::post('professional-form-question/edit/{form}/{id}', 'Modules\ProfessionalForm\ProfessionalQuestionController@update')->name('form.question.update');
    Route::get('professional-form-question/delete/{form}/{id}', 'Modules\ProfessionalForm\ProfessionalQuestionController@delete')->name('form.question.delete');

    Route::post('professional-user-assigne', 'Modules\ProfessionalForm\ProfessionalFormController@professionalUserAssigne')->name('professional-user-assigne');

    //14.3.20

    Route::get('prof-view-alluserform-feedback/{id}', 'Modules\ProfessionalForm\ProfessionalFormController@viewAllUserFeedback')->name('prof.view.alluserform.feedback');
    Route::post('prof-formfeedback-post-touser', 'Modules\ProfessionalForm\ProfessionalFormController@postFeedbackTouser')->name('prof.formfeedback.post.touser');

    //for users 14.3.20
    Route::post('user-form-tools-feedback', 'Modules\ProfessionalForm\ProfessionalFormController@userFormToolsFeedback')->name('user.form.tools.feedback');

    // form toos status change 17.3.20
    Route::get('professional-form-status/{id}', 'Modules\ProfessionalForm\ProfessionalFormController@formStatusUpdate')->name('professional.formtools.status');

    // for form Tools end for professional

    //for 360degree-evaluation test start

    // professional

    Route::get('360degree-evaluation', 'Modules\ProfessionalForm\ProfessionalFormController@get360degreeEvaluation')->name('professional-360degree-evaluation');
    Route::resource('professional-360evaluation', 'Modules\Professional360Evaluation\Professional360EvaluationController');

    Route::post('prof-360evaluation-asign', 'Modules\Professional360Evaluation\Professional360EvaluationController@professional360eEvuUserAssigne')->name('prof.360evaluation.asign');
    Route::get('prof-360asign-view/{id}', 'Modules\Professional360Evaluation\Professional360EvaluationController@professional360eEvuUserAssigneView')->name('prof.360asign.view');

    Route::get('prof-360asign-userlist/{id}', 'Modules\Professional360Evaluation\Professional360EvaluationController@professional360eEvuUserAssigneUserList')->name('prof.360asign.userlist');

    //18.3.20 status update
    Route::get('prof-360toools-status/{id}', 'Modules\Professional360Evaluation\Professional360EvaluationController@tools360StatusUpdate')->name('prof.tools360.status');

    //Route::post('dynamic_dependent/fetchcompetences', 'Modules\Professional360Evaluation\Professional360EvaluationController@fetchCompetencesAll')->name('dynamicdependent.fetchcompetences');

    //users

    Route::get('user-360evaluation/{id}', 'Modules\User360Evaluation\User360EvaluationController@getUser360Evaluation')->name('user.360evaluation');

    Route::post('user-360evaluation-post/', 'Modules\User360Evaluation\User360EvaluationController@getUser360EvaluationPost')->name('user.360evaluation.post');
    Route::get('user-360evaluation-view/{id}', 'Modules\User360Evaluation\User360EvaluationController@getUser360EvaluationView')->name('user.360evaluation.view');
    //for 360degree-evaluation test end

    //17.3.20
    //for evaluation360 test start
    Route::resource('prof-evaluation360', 'Modules\ProfEvaluation360\ProfEvaluation360Controller');
    Route::post('dynamic_dependent/fetchcompetences', 'Modules\ProfEvaluation360\ProfEvaluation360Controller@fetchCompetencesAll')->name('dynamicdependent.fetchcompetences');

    Route::get('user-evaluation360-assign-userlist/', 'Modules\ProfEvaluation360\ProfEvaluation360Controller@profEvu360AssigneUserList')->name('user.evaluation360.assign.userlist');
    //26.3.20 user start
    Route::get('user-evaluation360-list/{id}', 'Modules\ProfEvaluation360\ProfEvaluation360Controller@getUserEvaluation360QuesList')->name('user.evaluation360.quelist');
    Route::get('user-evaluation360-scorepost/', 'Modules\ProfEvaluation360\ProfEvaluation360Controller@userEvaluation360ScorePost')->name('user.evaluation360.score.post');
    Route::get('user-evaluation360-scorelisting/{id}', 'Modules\ProfEvaluation360\ProfEvaluation360Controller@userEvaluation360ScoreListing')->name('user.evaluation360.score.listing');

    //public evaluation360
    Route::get('user-public-valuation360/{id}/{type}', 'Modules\Professional\PublicProfileController@userpPublicEvaluation360')->name('pblc.evaluation');
    Route::get('user-evaluation360-publicscorepost/', 'Modules\Professional\PublicProfileController@userEvaluation360ScorePost')->name('user.evaluation360.public.score.post');
    //Route::get('public-profile/{slug}', 'Modules\Professional\PublicProfileController@index')->name('pblc.pst');

    //26.3.20 user end


    //for evaluation360 test start

    // Importing tools start

    Route::resource('professional-importing-tools', 'Modules\ImportingTools\ImportingToolsController');
    Route::post('professional-importing-tools-assigne', 'Modules\ImportingTools\ImportingToolsController@professionalImportingToolsAssigne')->name('professional.importing.assigne');

    Route::get('professional-importing-tools-view', 'Modules\ImportingTools\ImportingToolsController@professionalImportingToolsView')->name('professional.importing.assigne.view');

    Route::get('professional-importing-tools-update-status/{id}', 'Modules\ImportingTools\ImportingToolsController@updateStatus')->name('professional.importing.update.status');

    Route::get('professional-importing-tools-duplicate/{id}', 'Modules\ImportingTools\ImportingToolsController@duplicateImportedTools')->name('professional.importing.duplicate.tools');

    Route::get('prof-importing-tools-view-details/{id}', 'Modules\ImportingTools\ImportingToolsController@professionalImportingToolsViewDeatils')->name('prof.importing.assigne.view.details');

    Route::get('prof-view-alluser-feedback/{id}', 'Modules\ImportingTools\ImportingToolsController@viewAllUserFeedback')->name('prof.view.alluser.feedback');

    Route::post('prof-feedback-post-touser', 'Modules\ImportingTools\ImportingToolsController@postFeedbackTouser')->name('prof.feedback.post.touser');
    //17.3.20
    Route::get('professional-importedtool-status/{id}', 'Modules\ImportingTools\ImportingToolsController@importedStatusUpdate')->name('professional.importedtools.status');

    // for user 10.3.20
    Route::get('user-importing-tools-view/{id}', 'Modules\ImportingTools\ImportingToolsController@userImportingToolsView')->name('user.importing.assigne.view');
    Route::post('user-importing-tools-update/{id}', 'Modules\ImportingTools\ImportingToolsController@userImportingToolsUpdate')->name('user.importing.tools.update');
    Route::get('user-importing-tools-ans-show/{id}', 'Modules\ImportingTools\ImportingToolsController@userImportingToolsAnsShow')->name('user.importing.answer.show');
    Route::post('user-importing-tools-feedback', 'Modules\ImportingTools\ImportingToolsController@userImportingToolsFeedback')->name('user.importing.tools.feedback');

    // Importing tools end

    // SMART Goals start 3.2.20

    // professional
    Route::resource('prof-smartgoal-tools', 'Modules\ProfSmartGoal\ProfSmartGoalController');
    Route::post('prof-smartgoal-user-assigne', 'Modules\ProfSmartGoal\ProfSmartGoalController@profSmartGoalUserAssigne')->name('prof.smartgoal.user.assigne');
    Route::get('prof-smartgoal-user-assigne-view/{id}', 'Modules\ProfSmartGoal\ProfSmartGoalController@profSmartGoalUserAssigneView')->name('prof.smartgoal.user.assigne.view');

    //18.3.20 status update
    Route::get('prof-smartgoal-status/{id}', 'Modules\ProfSmartGoal\ProfSmartGoalController@profSmartGoalStatusUpdate')->name('prof.smartgoal.status');

    //users
    Route::get('user-smartgoal-tools-view/{id}', 'Modules\UserSmartGoal\UserSmartGoalController@userSmartGoalView')->name('user.smart.goal.view');
    Route::post('user-smartgoal-tools-post/{id}', 'Modules\UserSmartGoal\UserSmartGoalController@userSmartGoalPost')->name('user.smart.goal.post');
    // SMART Goals End


    //Content templates start 6.3.20

    Route::resource('user-content-temp', 'Modules\UserContentTemp\UserContentTempController');
    Route::post('prof-content-temp-user-assigne', 'Modules\UserContentTemp\UserContentTempController@profContentTempUserAssigne')->name('prof.content.temp.user.assigne');

    Route::get('prof-contenttemp-userassigne-list/{id}', 'Modules\UserContentTemp\UserContentTempController@profContentTempUserAssigneList')->name('prof.content.temp.user.assigne.list');

    Route::get('user-content-temp-view/{id}', 'Modules\UserContentTemp\UserContentTempController@userContentTempView')->name('user.content.temp.view');

    // Content toos status change 19.3.20
    Route::get('prof-contenttemp-status/{id}', 'Modules\UserContentTemp\UserContentTempController@contenttempStatusUpdate')->name('prof.contenttemp.status');
    //Content templates end 6.3.20

    //Contract Template start 6.3.20

    Route::resource('prof-contract-temp', 'Modules\ProfContractTemp\ProfContractTempController');
    Route::post('prof-contract-user-assigne', 'Modules\ProfContractTemp\ProfContractTempController@profContractTempUserAssigne')->name('prof.contract.temp.user.assigne');

    Route::get('prof-contract-assigne-list/{id}', 'Modules\ProfContractTemp\ProfContractTempController@profContractTempUserAssigneList')->name('prof.contract.temp.user.assigne.list');

    // Contract toos status change 19.3.20
    Route::get('prof-contract-status/{id}', 'Modules\ProfContractTemp\ProfContractTempController@contractStatusUpdate')->name('prof.contract.status');

    // contract for user start 14.3.20

    Route::get('user-contract-temp-view/{id}', 'Modules\ProfContractTemp\ProfContractTempController@userContractTempView')->name('user.contract.temp.view');
    Route::get('user-contract-temp-accept/{id}', 'Modules\ProfContractTemp\ProfContractTempController@userContractTempAcceptStatus')->name('user.contract.temp.update.status');

    // contract for user end 14.3.20

    //Contract Template end 6.3.20


    //Log book start 14.3.20

    Route::resource('prof-log-book', 'Modules\LogBook\LogBookController');
    Route::post('prof-contract-temp-user-assigne', 'Modules\LogBook\LogBookController@profLogbookUserAssigne')->name('prof.logbook.user.assigne');

    Route::get('prof-logbook-assigne-list/{id}', 'Modules\LogBook\LogBookController@profLogbookUserAssigneList')->name('prof.logbook.user.assigne.list');

    //Route::get('prof-logbook-assigne-list/{id}', 'Modules\LogBook\LogBookController@profLogbookUserAssigneList')->name('prof.logbook.user.assigne.list');

    //25.3.20
    Route::get('prof-logbook-assigne-userlist/', 'Modules\LogBook\LogBookController@profLogbookUserAssigneList')->name('prof.logbook.user.assigne.list');
    Route::get('prof-logbook-assigne-user-ansdate/{id}', 'Modules\LogBook\LogBookController@logbookUserAssigneAnsDate')->name('prof.logbook.user.assigne.ansdate');
    Route::get('prof-logbook-user-qus-ansdate/{id}', 'Modules\LogBook\LogBookController@logbookUserAssigneQuestionAns')->name('prof.logbook.user.assigne.queansdate');


    // logbook toos status change 19.3.20
    Route::get('prof-logbook-status/{id}', 'Modules\LogBook\LogBookController@logbookStatusUpdate')->name('prof.logbook.status');

    //user 16.3.20logbook
    Route::get('user-logbook-view/{id}', 'Modules\LogBook\LogBookController@userLogbookView')->name('user.logbook.view');
    Route::post('user-logbook-answ-post/', 'Modules\LogBook\LogBookController@userLogbookAnswerPost')->name('user.loogbook.answer.submit');
    Route::get('user-logbook-questionlist/{id}', 'Modules\LogBook\LogBookController@userLogbookQuestionList')->name('user.logbook.question.list');

    //25.3.20
    Route::get('user-logbook-all-ansdate/', 'Modules\LogBook\LogBookController@logbookUsergetAllAnsDate')->name('user.logbook.all.ansdate');
    Route::get('user-logbook-qus-ansdetails/{id}', 'Modules\LogBook\LogBookController@logbookUserQuestionAnsDetails')->name('user.logbook.queansdate.details');

    //Log book end 14.3.20
    Route::get('payments', 'Modules\Payment\PaymentController@myPayments')->name('my.payments');
    Route::post('booking-date', 'HomeController@setDate')->name('set.date');
    Route::post('make-payment', 'Modules\TestPayment\PaymentController@payment')->name('make-payment');

    Route::post('check-coupon', 'Modules\Booking\BookingController@checkCoupon')->name('check.coupon');
    Route::post('add-payment-method/{slug}', 'Modules\Booking\BookingController@addPaymentCoupon')->name('add.pamentmethod.coupon');
    Route::post('upload-payment-document/{slug}', 'Modules\Booking\BookingController@uploadDocument')->name('upload.payment.document');



    Route::get('document', 'Modules\DocumentSignature\DocumentSignatureController@index')->name('document');
    Route::get('document/add', 'Modules\DocumentSignature\DocumentSignatureController@add')->name('document.add');
    Route::post('document/add/success', 'Modules\DocumentSignature\DocumentSignatureController@addSuccess')->name('document.add.success');
    Route::get('document/edit/{id}', 'Modules\DocumentSignature\DocumentSignatureController@edit')->name('document.edit');
    Route::post('document/edit/{id}/success', 'Modules\DocumentSignature\DocumentSignatureController@editSuccess')->name('document.edit.success');
    Route::get('document/delete/{id}', 'Modules\DocumentSignature\DocumentSignatureController@delete')->name('document.delete');
    Route::get('document/view/{id}', 'Modules\DocumentSignature\DocumentSignatureController@view')->name('document.view');
    Route::post('document/send', 'Modules\DocumentSignature\DocumentSignatureController@send')->name('document.send');
    Route::get('document/copy/{id}', 'Modules\DocumentSignature\DocumentSignatureController@copy')->name('document.copy');

    //Product Order
    Route::any('product-order/store', 'Modules\ProductOrder\ProductOrderController@storeProductOrder')->name('product.order.store');
    Route::get('product-order-sccess/{token}', 'Modules\ProductOrder\ProductOrderController@OrderSuccess')->name('product.order.store.success');
    Route::post('add-payment-method-product/{token}', 'Modules\ProductOrder\ProductOrderController@addPaymentCoupon')->name('product.order.add.pamentmethod');
    Route::post('complete-profile-user', 'Modules\ProductOrder\ProductOrderController@completeProfile')->name('product.cmplt.user.profile');
    Route::get('all-product', 'Modules\Professional\ProfessionalProductsController@search')->name('all.product.search');
    Route::post('all-product', 'Modules\Professional\ProfessionalProductsController@search')->name('all.product.search.filter');
    Route::post('upload-payment-document-product/{slug}', 'Modules\ProductOrder\ProductOrderController@uploadDocument')->name('upload.payment.document.product');


});


//Clear configurations:
Route::get('/config-clear', function () {
    $status = Artisan::call('config:clear');
    return '<h1>Configurations cleared</h1>';
});


//Clear configurations:
Route::get('/route-cache', function () {
    $status = Artisan::call('route:cache');
    return '<h1>Route cache cleared</h1>';
});

//Clear configurations:
Route::get('/route-clear', function () {
    $status = Artisan::call('route:clear');
    return '<h1>Route  cleared</h1>';
});

//Clear cache:
Route::get('/cache-clear', function () {
    $status = Artisan::call('cache:clear');
    return '<h1>Cache cleared</h1>';
});

//Clear configuration cache:
Route::get('/config-cache', function () {
    $status = Artisan::call('config:cache');
    return '<h1>Configurations cache cleared</h1>';
});

Route::get('/php-info', function () {
    // echo phpinfo();
    date_default_timezone_set("America/Sao_Paulo");
    dd(date("h:i a"));
});


// Corn time update

Route::get('daily-date-update', 'Modules\CronDateUpdate\CronDateUpdateController@dailyDateUpdate');

Route::get('weekly-date-update', 'Modules\CronDateUpdate\CronDateUpdateController@weeklyDateUpdate');

Route::get('user-bookingdata', 'Modules\CronDateUpdate\CronDateUpdateController@userBookingMail');
Route::get('professional-bookingdata', 'Modules\CronDateUpdate\CronDateUpdateController@professionalBookingMail');

Route::get('message-pusher','Modules\Pusher\PusherController@message')->name('message.pusher');
Route::post('send-ajax', 'Modules\Pusher\PusherController@sendajax')->name('send.ajax');
Route::post('add-user', 'Modules\Pusher\PusherController@adduser')->name('add.user');
Route::post('remove-user', 'Modules\Pusher\PusherController@removeuser')->name('remove.user');
Route::post('message-search', 'Modules\Pusher\PusherController@searchajax')->name('message.search');
Route::post('user-message', 'Modules\Pusher\PusherController@usermessage')->name('user.message');
Route::post('typing-ajax', 'Modules\Pusher\PusherController@typingajax')->name('typing.ajax');
Route::get('receive', 'Modules\Pusher\PusherController@sendApi')->name('receive');
Route::post('get-message-master', 'Modules\Pusher\PusherController@getmessagemaster')->name('get.message.master');
Route::post('close-chat', 'Modules\Pusher\PusherController@closeChat')->name('close.chat');


Route::get('chats', 'Modules\Pusher\PusherController@messagesPage')->name('chats');
