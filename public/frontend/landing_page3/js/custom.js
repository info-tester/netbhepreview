	$('.carousel').carousel({
	  interval: 5000,
   	  pause: "false"
	})


$(document).ready(function() {
    var owl = $('.produt_slid .owl-carousel');
    owl.owlCarousel({
        nav: true,
        autoplay: true,
        loop: false,
        dots: false,
        responsive: {
            0: {
                items: 1,
                margin: 0
            },
            400: {
                items: 1,
                margin: 0
            },
            480: {
                items: 1,
                margin: 15
            },
            575: {
                items: 1,
                margin: 20
            },
            768: {
                items: 2,
                margin: 30
            },
            992: {
                items: 3,
                margin: 30
            },
            1199: {
                items: 3,
                margin: 30
            },
            1499: {
                items: 3,
                margin: 30
            }
        }
    })
  })
