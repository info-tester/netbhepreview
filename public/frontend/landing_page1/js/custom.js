	$('.carousel').carousel({
	  interval: 5000,
   	  pause: "false"
	})


	var owl = $('.pro_sec_sli .owl-carousel');
	owl.owlCarousel({
		margin: 0,
		nav: true,
		autoplay: true,
		loop: false,
		responsive: {
			0: {
				items: 1
			},
				575: {
				items: 1
			},
				768: {
				items: 2
			},
			991: {
				items: 3
			},
			1000: {
				items: 3
			}
		}
	});