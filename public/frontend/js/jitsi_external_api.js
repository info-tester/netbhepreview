! function(e, t) {
    "object" == typeof exports && "object" == typeof module ? module.exports = t() : "function" == typeof define && define.amd ? define([], t) : "object" == typeof exports ? exports.JitsiMeetExternalAPI = t() : e.JitsiMeetExternalAPI = t()
}(window, function() {
    return function(e) {
        var t = {};

        function n(i) {
            if (t[i]) return t[i].exports;
            var s = t[i] = {
                i: i,
                l: !1,
                exports: {}
            };
            return e[i].call(s.exports, s, s.exports, n), s.l = !0, s.exports
        }
        return n.m = e, n.c = t, n.d = function(e, t, i) {
            n.o(e, t) || Object.defineProperty(e, t, {
                enumerable: !0,
                get: i
            })
        }, n.r = function(e) {
            "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, {
                value: "Module"
            }), Object.defineProperty(e, "__esModule", {
                value: !0
            })
        }, n.t = function(e, t) {
            if (1 & t && (e = n(e)), 8 & t) return e;
            if (4 & t && "object" == typeof e && e && e.__esModule) return e;
            var i = Object.create(null);
            if (n.r(i), Object.defineProperty(i, "default", {
                    enumerable: !0,
                    value: e
                }), 2 & t && "string" != typeof e)
                for (var s in e) n.d(i, s, function(t) {
                    return e[t]
                }.bind(null, s));
            return i
        }, n.n = function(e) {
            var t = e && e.__esModule ? function() {
                return e.default
            } : function() {
                return e
            };
            return n.d(t, "a", t), t
        }, n.o = function(e, t) {
            return Object.prototype.hasOwnProperty.call(e, t)
        }, n.p = "/libs/", n(n.s = 9)
    }([function(e, t, n) {
        "use strict";
        (function(e) {
            n.d(t, "a", function() {
                return r
            }), n.d(t, "b", function() {
                return o
            }), n.d(t, "c", function() {
                return a
            }), n.d(t, "d", function() {
                return c
            }), n.d(t, "e", function() {
                return l
            }), n.d(t, "f", function() {
                return u
            }), n.d(t, "g", function() {
                return h
            }), n.d(t, "h", function() {
                return p
            });
            var i = n(2);
            const s = n.n(i).a.getLogger(e);

            function r(e) {
                return e.sendRequest({
                    type: "devices",
                    name: "getAvailableDevices"
                }).catch(e => (s.error(e), {}))
            }

            function o(e) {
                return e.sendRequest({
                    type: "devices",
                    name: "getCurrentDevices"
                }).catch(e => (s.error(e), {}))
            }

            function a(e, t) {
                return e.sendRequest({
                    deviceType: t,
                    type: "devices",
                    name: "isDeviceChangeAvailable"
                })
            }

            function c(e) {
                return e.sendRequest({
                    type: "devices",
                    name: "isDeviceListAvailable"
                })
            }

            function l(e) {
                return e.sendRequest({
                    type: "devices",
                    name: "isMultipleAudioInputSupported"
                })
            }

            function u(e, t, n) {
                return d(e, {
                    id: n,
                    kind: "audioinput",
                    label: t
                })
            }

            function h(e, t, n) {
                return d(e, {
                    id: n,
                    kind: "audiooutput",
                    label: t
                })
            }

            function d(e, t) {
                return e.sendRequest({
                    type: "devices",
                    name: "setDevice",
                    device: t
                })
            }

            function p(e, t, n) {
                return d(e, {
                    id: n,
                    kind: "videoinput",
                    label: t
                })
            }
        }).call(this, "modules/API/external/functions.js")
    }, function(e, t, n) {
        "use strict";
        var i = n(2);
        const s = {},
            r = {
                disableCallerInfo: !0
            };
        t.a = function(e) {
            const t = "ReactNative" === navigator.product ? r : s;
            return Object(i.getLogger)(e, void 0, t)
        }("features/base/util")
    }, function(e, t, n) {
        var i = n(5),
            s = n(11),
            r = {},
            o = [],
            a = i.levels.TRACE;
        e.exports = {
            addGlobalTransport: function(e) {
                i.addGlobalTransport(e)
            },
            removeGlobalTransport: function(e) {
                i.removeGlobalTransport(e)
            },
            setGlobalOptions: function(e) {
                i.setGlobalOptions(e)
            },
            getLogger: function(e, t, n) {
                var s = new i(a, e, t, n);
                return e ? (r[e] = r[e] || [], r[e].push(s)) : o.push(s), s
            },
            setLogLevelById: function(e, t) {
                for (var n = t ? r[t] || [] : o, i = 0; i < n.length; i++) n[i].setLevel(e)
            },
            setLogLevel: function(e) {
                a = e;
                for (var t = 0; t < o.length; t++) o[t].setLevel(e);
                for (var n in r) {
                    var i = r[n] || [];
                    for (t = 0; t < i.length; t++) i[t].setLevel(e)
                }
            },
            levels: i.levels,
            LogCollector: s
        }
    }, function(e, t, n) {
        "use strict";
        n.d(t, "a", function() {
            return d
        });
        var i = n(1);
        const s = "org.jitsi.meet:",
            r = "(//[^/?#]+)",
            o = "([^?#]*)",
            a = "^([a-z][a-z0-9\\.\\+-]*:)";

        function c(e) {
            const t = new RegExp(`${a}+`, "gi"),
                n = t.exec(e);
            if (n) {
                let i = n[n.length - 1].toLowerCase();
                "http:" !== i && "https:" !== i && (i = "https:"), (e = e.substring(t.lastIndex)).startsWith("//") && (e = i + e)
            }
            return e
        }

        function l(e = {}) {
            const t = [];
            for (const n in e) try {
                t.push(`${n}=${encodeURIComponent(JSON.stringify(e[n]))}`)
            } catch (e) {
                i.a.warn(`Error encoding ${n}: ${e}`)
            }
            return t
        }

        function u(e) {
            const t = {
                toString: h
            };
            let n, i, s;
            if (e = e.replace(/\s/g, ""), (i = (n = new RegExp(a, "gi")).exec(e)) && (t.protocol = i[1].toLowerCase(), e = e.substring(n.lastIndex)), i = (n = new RegExp(`^${r}`, "gi")).exec(e)) {
                let s = i[1].substring(2);
                e = e.substring(n.lastIndex);
                const r = s.indexOf("@"); - 1 !== r && (s = s.substring(r + 1)), t.host = s;
                const o = s.lastIndexOf(":"); - 1 !== o && (t.port = s.substring(o + 1), s = s.substring(0, o)), t.hostname = s
            }
            if ((i = (n = new RegExp(`^${o}`, "gi")).exec(e)) && (s = i[1], e = e.substring(n.lastIndex)), s ? s.startsWith("/") || (s = `/${s}`) : s = "/", t.pathname = s, e.startsWith("?")) {
                let n = e.indexOf("#", 1); - 1 === n && (n = e.length), t.search = e.substring(0, n), e = e.substring(n)
            } else t.search = "";
            return t.hash = e.startsWith("#") ? e : "", t
        }

        function h(e) {
            const {
                hash: t,
                host: n,
                pathname: i,
                protocol: s,
                search: r
            } = e || this;
            let o = "";
            return s && (o += s), n && (o += `//${n}`), o += i || "/", r && (o += r), t && (o += t), o
        }

        function d(e) {
            let t;
            const n = u(c(t = e.serverURL && e.room ? new URL(e.room, e.serverURL).toString() : e.room ? e.room : e.url || ""));
            if (!n.protocol) {
                let t = e.protocol || e.scheme;
                t && (t.endsWith(":") || (t += ":"), n.protocol = t)
            }
            let {
                pathname: i
            } = n;
            if (!n.host) {
                const t = e.domain || e.host || e.hostname;
                if (t) {
                    const {
                        host: e,
                        hostname: r,
                        pathname: o,
                        port: a
                    } = u(c(`${s}//${t}`));
                    e && (n.host = e, n.hostname = r, n.port = a), "/" === i && "/" !== o && (i = o)
                }
            }
            const r = e.roomName || e.room;
            !r || !n.pathname.endsWith("/") && n.pathname.endsWith(`/${r}`) || (i.endsWith("/") || (i += "/"), i += r), n.pathname = i;
            const {
                jwt: o
            } = e;
            if (o) {
                let {
                    search: e
                } = n; - 1 === e.indexOf("?jwt=") && -1 === e.indexOf("&jwt=") && (e.startsWith("?") || (e = `?${e}`), 1 === e.length || (e += "&"), e += `jwt=${o}`, n.search = e)
            }
            let {
                hash: a
            } = n;
            for (const t of["config", "interfaceConfig", "devices"]) {
                const n = l(e[`${t}Overwrite`] || e[t] || e[`${t}Override`]);
                if (n.length) {
                    let e = `${t}.${n.join(` & $ {
                        t
                    }.
                    `)}`;
                    a.length ? e = `&${e}` : a = "#", a += e
                }
            }
            return n.hash = a, n.toString() || void 0
        }
    }, function(e, t, n) {
        "use strict";
        var i = n(1);
        n(3);
        const s = function(e, t = !1, n = "hash") {
            const s = "search" === n ? e.search : e.hash,
                r = {},
                o = s && s.substr(1).split("&") || [];
            if ("hash" === n && 1 === o.length) {
                const e = o[0];
                if (e.startsWith("/") && 1 === e.split("&").length) return r
            }
            return o.forEach(e => {
                const n = e.split("="),
                    s = n[0];
                if (!s) return;
                let o;
                try {
                    if (o = n[1], !t) {
                        const e = decodeURIComponent(o).replace(/\\&/, "&");
                        o = "undefined" === e ? void 0 : JSON.parse(e)
                    }
                } catch (e) {
                    return void
                    function(e, t = "") {
                        i.a.error(t, e), window.onerror && window.onerror(t, null, null, null, e)
                    }(e, `Failed to parse URL parameter value: ${String(o)}`)
                }
                r[s] = o
            }), r
        }(window.location).jitsi_meet_external_api_id;
        var r = n(7),
            o = n.n(r);

        function a(e, t) {
            if (null == e) return {};
            var n, i, s = function(e, t) {
                if (null == e) return {};
                var n, i, s = {},
                    r = Object.keys(e);
                for (i = 0; i < r.length; i++) n = r[i], t.indexOf(n) >= 0 || (s[n] = e[n]);
                return s
            }(e, t);
            if (Object.getOwnPropertySymbols) {
                var r = Object.getOwnPropertySymbols(e);
                for (i = 0; i < r.length; i++) n = r[i], t.indexOf(n) >= 0 || Object.prototype.propertyIsEnumerable.call(e, n) && (s[n] = e[n])
            }
            return s
        }

        function c(e, t, n) {
            return t in e ? Object.defineProperty(e, t, {
                value: n,
                enumerable: !0,
                configurable: !0,
                writable: !0
            }) : e[t] = n, e
        }
        const l = {
                window: window.opener || window.parent
            },
            u = ["avatar-url", "display-name", "email", "toggle-audio", "toggle-chat", "toggle-film-strip", "toggle-share-screen", "toggle-video", "video-hangup"],
            h = ["display-name-change", "incoming-message", "outgoing-message", "participant-joined", "participant-left", "video-conference-joined", "video-conference-left", "video-ready-to-close"],
            d = "message";
        class p {
            constructor({
                enableLegacyFormat: e,
                postisOptions: t
            } = {}) {
                this.postis = o()(function(e) {
                    for (var t = 1; t < arguments.length; t++) {
                        var n = null != arguments[t] ? arguments[t] : {},
                            i = Object.keys(n);
                        "function" == typeof Object.getOwnPropertySymbols && (i = i.concat(Object.getOwnPropertySymbols(n).filter(function(e) {
                            return Object.getOwnPropertyDescriptor(n, e).enumerable
                        }))), i.forEach(function(t) {
                            c(e, t, n[t])
                        })
                    }
                    return e
                }({}, l, t)), this._enableLegacyFormat = e, this._enableLegacyFormat && u.forEach(e => this.postis.listen(e, t => this._legacyMessageReceivedCallback(e, t))), this._receiveCallback = (() => {}), this.postis.listen(d, e => this._receiveCallback(e))
            }
            _legacyMessageReceivedCallback(e, t = {}) {
                this._receiveCallback({
                    data: {
                        name: e,
                        data: t
                    }
                })
            }
            _sendLegacyMessage(e) {
                let {
                    name: t
                } = e, n = a(e, ["name"]);
                t && -1 !== h.indexOf(t) && this.postis.send({
                    method: t,
                    params: n
                })
            }
            dispose() {
                this.postis.destroy()
            }
            send(e) {
                this.postis.send({
                    method: d,
                    params: e
                }), this._enableLegacyFormat && this._sendLegacyMessage(e.data || {})
            }
            setReceiveCallback(e) {
                this._receiveCallback = e
            }
        }
        const f = "event",
            g = "request",
            m = "response";
        class v {
            constructor({
                backend: e
            } = {}) {
                this._listeners = new Map, this._requestID = 0, this._responseHandlers = new Map, this._unprocessedMessages = new Set, this.addListener = this.on, e && this.setBackend(e)
            }
            _disposeBackend() {
                this._backend && (this._backend.dispose(), this._backend = null)
            }
            _onMessageReceived(e) {
                if (e.type === m) {
                    const t = this._responseHandlers.get(e.id);
                    t && (t(e), this._responseHandlers.delete(e.id))
                } else e.type === g ? this.emit("request", e.data, (t, n) => {
                    this._backend.send({
                        type: m,
                        error: n,
                        id: e.id,
                        result: t
                    })
                }) : this.emit("event", e.data)
            }
            dispose() {
                this._responseHandlers.clear(), this._unprocessedMessages.clear(), this.removeAllListeners(), this._disposeBackend()
            }
            emit(e, ...t) {
                const n = this._listeners.get(e);
                let i = !1;
                return n && n.size && n.forEach(e => {
                    i = e(...t) || i
                }), i || this._unprocessedMessages.add(t), i
            }
            on(e, t) {
                let n = this._listeners.get(e);
                return n || (n = new Set, this._listeners.set(e, n)), n.add(t), this._unprocessedMessages.forEach(e => {
                    t(...e) && this._unprocessedMessages.delete(e)
                }), this
            }
            removeAllListeners(e) {
                return e ? this._listeners.delete(e) : this._listeners.clear(), this
            }
            removeListener(e, t) {
                const n = this._listeners.get(e);
                return n && n.delete(t), this
            }
            sendEvent(e = {}) {
                this._backend && this._backend.send({
                    type: f,
                    data: e
                })
            }
            sendRequest(e) {
                if (!this._backend) return Promise.reject(new Error("No transport backend defined!"));
                this._requestID++;
                const t = this._requestID;
                return new Promise((n, i) => {
                    this._responseHandlers.set(t, ({
                        error: e,
                        result: t
                    }) => {
                        void 0 !== t ? n(t) : i(void 0 !== e ? e : new Error("Unexpected response format!"))
                    }), this._backend.send({
                        type: g,
                        data: e,
                        id: t
                    })
                })
            }
            setBackend(e) {
                this._disposeBackend(), this._backend = e, this._backend.setReceiveCallback(this._onMessageReceived.bind(this))
            }
        }
        n.d(t, "a", function() {
            return p
        }), n.d(t, "b", function() {
            return v
        });
        const _ = {};
        let b;
        "number" == typeof s && (_.scope = `jitsi_meet_external_api_${s}`), (window.JitsiMeetJS || (window.JitsiMeetJS = {}), window.JitsiMeetJS.app || (window.JitsiMeetJS.app = {}), window.JitsiMeetJS.app).setExternalTransportBackend = (e => b.setBackend(e))
    }, function(e, t) {
        var n = {
            trace: 0,
            debug: 1,
            info: 2,
            log: 3,
            warn: 4,
            error: 5
        };
        o.consoleTransport = console;
        var i = [o.consoleTransport];
        o.addGlobalTransport = function(e) {
            -1 === i.indexOf(e) && i.push(e)
        }, o.removeGlobalTransport = function(e) {
            var t = i.indexOf(e); - 1 !== t && i.splice(t, 1)
        };
        var s = {};

        function r() {
            var e = arguments[0],
                t = arguments[1],
                r = Array.prototype.slice.call(arguments, 2);
            if (!(n[t] < e.level))
                for (var o = !(e.options.disableCallerInfo || s.disableCallerInfo) && function() {
                        var e = {
                                methodName: "",
                                fileLocation: "",
                                line: null,
                                column: null
                            },
                            t = new Error,
                            n = t.stack ? t.stack.split("\n") : [];
                        if (!n || n.length < 1) return e;
                        var i = null;
                        return n[3] && (i = n[3].match(/\s*at\s*(.+?)\s*\((\S*)\s*:(\d*)\s*:(\d*)\)/)), !i || i.length <= 4 ? (0 === n[2].indexOf("log@") ? e.methodName = n[3].substr(0, n[3].indexOf("@")) : e.methodName = n[2].substr(0, n[2].indexOf("@")), e) : (e.methodName = i[1], e.fileLocation = i[2], e.line = i[3], e.column = i[4], e)
                    }(), a = i.concat(e.transports), c = 0; c < a.length; c++) {
                    var l = a[c],
                        u = l[t];
                    if (u && "function" == typeof u) {
                        var h = [];
                        e.id && h.push("[" + e.id + "]"), o && o.methodName.length > 1 && h.push("<" + o.methodName + ">: ");
                        var d = h.concat(r);
                        u.bind(l).apply(l, d)
                    }
                }
        }

        function o(e, t, i, s) {
            this.id = t, this.options = s || {}, this.transports = i, this.transports || (this.transports = []), this.level = n[e];
            for (var o = Object.keys(n), a = 0; a < o.length; a++) this[o[a]] = r.bind(null, this, o[a])
        }
        o.setGlobalOptions = function(e) {
            s = e || {}
        }, o.prototype.setLevel = function(e) {
            this.level = n[e]
        }, e.exports = o, o.levels = {
            TRACE: "trace",
            DEBUG: "debug",
            INFO: "info",
            LOG: "log",
            WARN: "warn",
            ERROR: "error"
        }
    }, function(e, t) {
        function n() {
            this._events = this._events || {}, this._maxListeners = this._maxListeners || void 0
        }

        function i(e) {
            return "function" == typeof e
        }

        function s(e) {
            return "object" == typeof e && null !== e
        }

        function r(e) {
            return void 0 === e
        }
        e.exports = n, n.EventEmitter = n, n.prototype._events = void 0, n.prototype._maxListeners = void 0, n.defaultMaxListeners = 10, n.prototype.setMaxListeners = function(e) {
            if ("number" != typeof e || e < 0 || isNaN(e)) throw TypeError("n must be a positive number");
            return this._maxListeners = e, this
        }, n.prototype.emit = function(e) {
            var t, n, o, a, c, l;
            if (this._events || (this._events = {}), "error" === e && (!this._events.error || s(this._events.error) && !this._events.error.length)) {
                if ((t = arguments[1]) instanceof Error) throw t;
                var u = new Error('Uncaught, unspecified "error" event. (' + t + ")");
                throw u.context = t, u
            }
            if (r(n = this._events[e])) return !1;
            if (i(n)) switch (arguments.length) {
                case 1:
                    n.call(this);
                    break;
                case 2:
                    n.call(this, arguments[1]);
                    break;
                case 3:
                    n.call(this, arguments[1], arguments[2]);
                    break;
                default:
                    a = Array.prototype.slice.call(arguments, 1), n.apply(this, a)
            } else if (s(n))
                for (a = Array.prototype.slice.call(arguments, 1), o = (l = n.slice()).length, c = 0; c < o; c++) l[c].apply(this, a);
            return !0
        }, n.prototype.addListener = function(e, t) {
            var o;
            if (!i(t)) throw TypeError("listener must be a function");
            return this._events || (this._events = {}), this._events.newListener && this.emit("newListener", e, i(t.listener) ? t.listener : t), this._events[e] ? s(this._events[e]) ? this._events[e].push(t) : this._events[e] = [this._events[e], t] : this._events[e] = t, s(this._events[e]) && !this._events[e].warned && (o = r(this._maxListeners) ? n.defaultMaxListeners : this._maxListeners) && o > 0 && this._events[e].length > o && (this._events[e].warned = !0, console.error("(node) warning: possible EventEmitter memory leak detected. %d listeners added. Use emitter.setMaxListeners() to increase limit.", this._events[e].length), "function" == typeof console.trace && console.trace()), this
        }, n.prototype.on = n.prototype.addListener, n.prototype.once = function(e, t) {
            if (!i(t)) throw TypeError("listener must be a function");
            var n = !1;

            function s() {
                this.removeListener(e, s), n || (n = !0, t.apply(this, arguments))
            }
            return s.listener = t, this.on(e, s), this
        }, n.prototype.removeListener = function(e, t) {
            var n, r, o, a;
            if (!i(t)) throw TypeError("listener must be a function");
            if (!this._events || !this._events[e]) return this;
            if (o = (n = this._events[e]).length, r = -1, n === t || i(n.listener) && n.listener === t) delete this._events[e], this._events.removeListener && this.emit("removeListener", e, t);
            else if (s(n)) {
                for (a = o; a-- > 0;)
                    if (n[a] === t || n[a].listener && n[a].listener === t) {
                        r = a;
                        break
                    }
                if (r < 0) return this;
                1 === n.length ? (n.length = 0, delete this._events[e]) : n.splice(r, 1), this._events.removeListener && this.emit("removeListener", e, t)
            }
            return this
        }, n.prototype.removeAllListeners = function(e) {
            var t, n;
            if (!this._events) return this;
            if (!this._events.removeListener) return 0 === arguments.length ? this._events = {} : this._events[e] && delete this._events[e], this;
            if (0 === arguments.length) {
                for (t in this._events) "removeListener" !== t && this.removeAllListeners(t);
                return this.removeAllListeners("removeListener"), this._events = {}, this
            }
            if (i(n = this._events[e])) this.removeListener(e, n);
            else if (n)
                for (; n.length;) this.removeListener(e, n[n.length - 1]);
            return delete this._events[e], this
        }, n.prototype.listeners = function(e) {
            return this._events && this._events[e] ? i(this._events[e]) ? [this._events[e]] : this._events[e].slice() : []
        }, n.prototype.listenerCount = function(e) {
            if (this._events) {
                var t = this._events[e];
                if (i(t)) return 1;
                if (t) return t.length
            }
            return 0
        }, n.listenerCount = function(e, t) {
            return e.listenerCount(t)
        }
    }, function(e, t) {
        e.exports = function(e) {
            var t, n = e.scope,
                i = e.window,
                s = e.windowForEventListening || window,
                r = {},
                o = [],
                a = {},
                c = !1,
                l = function(e) {
                    var t;
                    try {
                        t = JSON.parse(e.data)
                    } catch (e) {
                        return
                    }
                    if (t.postis && t.scope === n) {
                        var i = r[t.method];
                        if (i)
                            for (var s = 0; s < i.length; s++) i[s].call(null, t.params);
                        else a[t.method] = a[t.method] || [], a[t.method].push(t.params)
                    }
                };
            s.addEventListener("message", l, !1);
            var u = {
                    listen: function(e, t) {
                        r[e] = r[e] || [], r[e].push(t);
                        var n = a[e];
                        if (n)
                            for (var i = r[e], s = 0; s < i.length; s++)
                                for (var o = 0; o < n.length; o++) i[s].call(null, n[o]);
                        delete a[e]
                    },
                    send: function(e) {
                        var t = e.method;
                        (c || "__ready__" === e.method) && i && "function" == typeof i.postMessage ? i.postMessage(JSON.stringify({
                            postis: !0,
                            scope: n,
                            method: t,
                            params: e.params
                        }), "*") : o.push(e)
                    },
                    ready: function(e) {
                        c ? e() : setTimeout(function() {
                            u.ready(e)
                        }, 50)
                    },
                    destroy: function(e) {
                        clearInterval(t), c = !1, s && "function" == typeof s.removeEventListener && s.removeEventListener("message", l), e && e()
                    }
                },
                h = +new Date + Math.random() + "";
            return t = setInterval(function() {
                u.send({
                    method: "__ready__",
                    params: h
                })
            }, 50), u.listen("__ready__", function(e) {
                if (e === h) {
                    clearInterval(t), c = !0;
                    for (var n = 0; n < o.length; n++) u.send(o[n]);
                    o = []
                } else u.send({
                    method: "__ready__",
                    params: e
                })
            }), u
        }
    }, function(e) {
        e.exports = {
            "google-auth": {
                matchPatterns: {
                    url: "accounts.google.com"
                },
                target: "electron"
            },
            "dropbox-auth": {
                matchPatterns: {
                    url: "dropbox.com/oauth2/authorize"
                },
                target: "electron"
            }
        }
    }, function(e, t, n) {
        e.exports = n(10).default
    }, function(e, t, n) {
        "use strict";
        n.r(t),
            function(e) {
                n.d(t, "default", function() {
                    return b
                });
                var i = n(6),
                    s = n.n(i),
                    r = n(3),
                    o = n(4),
                    a = n(8),
                    c = n(0);

                function l(e, t) {
                    if (null == e) return {};
                    var n, i, s = function(e, t) {
                        if (null == e) return {};
                        var n, i, s = {},
                            r = Object.keys(e);
                        for (i = 0; i < r.length; i++) n = r[i], t.indexOf(n) >= 0 || (s[n] = e[n]);
                        return s
                    }(e, t);
                    if (Object.getOwnPropertySymbols) {
                        var r = Object.getOwnPropertySymbols(e);
                        for (i = 0; i < r.length; i++) n = r[i], t.indexOf(n) >= 0 || Object.prototype.propertyIsEnumerable.call(e, n) && (s[n] = e[n])
                    }
                    return s
                }

                function u(e, t, n) {
                    return t in e ? Object.defineProperty(e, t, {
                        value: n,
                        enumerable: !0,
                        configurable: !0,
                        writable: !0
                    }) : e[t] = n, e
                }
                const h = n(2).getLogger(e),
                    d = ["css/all.css", "libs/alwaysontop.min.js"],
                    p = {
                        avatarUrl: "avatar-url",
                        displayName: "display-name",
                        email: "email",
                        hangup: "video-hangup",
                        password: "password",
                        subject: "subject",
                        submitFeedback: "submit-feedback",
                        toggleAudio: "toggle-audio",
                        toggleChat: "toggle-chat",
                        toggleFilmStrip: "toggle-film-strip",
                        toggleShareScreen: "toggle-share-screen",
                        toggleTileView: "toggle-tile-view",
                        toggleVideo: "toggle-video"
                    },
                    f = {
                        "avatar-changed": "avatarChanged",
                        "audio-availability-changed": "audioAvailabilityChanged",
                        "audio-mute-status-changed": "audioMuteStatusChanged",
                        "camera-error": "cameraError",
                        "device-list-changed": "deviceListChanged",
                        "display-name-change": "displayNameChange",
                        "email-change": "emailChange",
                        "feedback-submitted": "feedbackSubmitted",
                        "feedback-prompt-displayed": "feedbackPromptDisplayed",
                        "filmstrip-display-changed": "filmstripDisplayChanged",
                        "incoming-message": "incomingMessage",
                        "mic-error": "micError",
                        "outgoing-message": "outgoingMessage",
                        "participant-joined": "participantJoined",
                        "participant-kicked-out": "participantKickedOut",
                        "participant-left": "participantLeft",
                        "password-required": "passwordRequired",
                        "proxy-connection-event": "proxyConnectionEvent",
                        "video-ready-to-close": "readyToClose",
                        "video-conference-joined": "videoConferenceJoined",
                        "video-conference-left": "videoConferenceLeft",
                        "video-availability-changed": "videoAvailabilityChanged",
                        "video-mute-status-changed": "videoMuteStatusChanged",
                        "screen-sharing-status-changed": "screenSharingStatusChanged",
                        "dominant-speaker-changed": "dominantSpeakerChanged",
                        "subject-change": "subjectChange",
                        "suspend-detected": "suspendDetected",
                        "tile-view-changed": "tileViewChanged"
                    };
                let g = 0;

                function m(e, t) {
                    e._numberOfParticipants += t
                }

                function v(e, t = {}) {
                    return Object(r.a)(function(e) {
                        for (var t = 1; t < arguments.length; t++) {
                            var n = null != arguments[t] ? arguments[t] : {},
                                i = Object.keys(n);
                            "function" == typeof Object.getOwnPropertySymbols && (i = i.concat(Object.getOwnPropertySymbols(n).filter(function(e) {
                                return Object.getOwnPropertyDescriptor(n, e).enumerable
                            }))), i.forEach(function(t) {
                                u(e, t, n[t])
                            })
                        }
                        return e
                    }({}, t, {
                        url: `${t.noSSL?"http":"https"}://${e}/#jitsi_meet_external_api_id=${g}`
                    }))
                }

                function _(e) {
                    let t;
                    return "string" == typeof e && null !== String(e).match(/([0-9]*\.?[0-9]+)(em|pt|px|%)$/) ? t = e : "number" == typeof e && (t = `${e}px`), t
                }
                class b extends s.a {
                    constructor(e, ...t) {
                        super();
                        const {
                            roomName: n = "",
                            width: i = "100%",
                            height: s = "100%",
                            parentNode: r = document.body,
                            configOverwrite: a = {},
                            interfaceConfigOverwrite: c = {},
                            noSSL: l = !1,
                            jwt: u,
                            onload: h,
                            invitees: d,
                            devices: p
                        } = function(e) {
                            if (!e.length) return {};
                            switch (typeof e[0]) {
                                case "string":
                                case void 0:
                                    {
                                        const [t, n, i, s, r, o, a, c, l] = e;
                                        return {
                                            roomName: t,
                                            width: n,
                                            height: i,
                                            parentNode: s,
                                            configOverwrite: r,
                                            interfaceConfigOverwrite: o,
                                            noSSL: a,
                                            jwt: c,
                                            onload: l
                                        }
                                    }
                                case "object":
                                    return e[0];
                                default:
                                    throw new Error("Can't parse the arguments!")
                            }
                        }(t);
                        this._parentNode = r, this._url = v(e, {
                            configOverwrite: a,
                            interfaceConfigOverwrite: c,
                            jwt: u,
                            noSSL: l,
                            roomName: n,
                            devices: p
                        }), this._createIFrame(s, i, h), this._transport = new o.b({
                            backend: new o.a({
                                postisOptions: {
                                    scope: `jitsi_meet_external_api_${g}`,
                                    window: this._frame.contentWindow
                                }
                            })
                        }), Array.isArray(d) && d.length > 0 && this.invite(d), this._isLargeVideoVisible = !0, this._numberOfParticipants = 0, this._participants = {}, this._myUserID = void 0, this._onStageParticipant = void 0, this._setupListeners(), g++
                    }
                    _createIFrame(e, t, n) {
                        const i = `jitsiConferenceFrame${g}`;
                        this._frame = document.createElement("iframe"), this._frame.allow = "camera; microphone", this._frame.src = this._url, this._frame.name = i, this._frame.id = i, this._setSize(e, t), this._frame.setAttribute("allowFullScreen", "true"), this._frame.style.border = 0, n && (this._frame.onload = n), this._frame = this._parentNode.appendChild(this._frame)
                    }
                    _getAlwaysOnTopResources() {
                        const e = this._frame.contentWindow,
                            t = e.document;
                        let n = "";
                        const i = t.querySelector("base");
                        if (i && i.href) n = i.href;
                        else {
                            const {
                                protocol: t,
                                host: i
                            } = e.location;
                            n = `${t}//${i}`
                        }
                        return d.map(e => new URL(e, n).href)
                    }
                    _getOnStageParticipant() {
                        return this._onStageParticipant
                    }
                    _getLargeVideo() {
                        const e = this.getIFrame();
                        if (this._isLargeVideoVisible && e && e.contentWindow && e.contentWindow.document) return e.contentWindow.document.getElementById("largeVideo")
                    }
                    _getParticipantVideo(e) {
                        const t = this.getIFrame();
                        if (t && t.contentWindow && t.contentWindow.document) return void 0 === e || e === this._myUserID ? t.contentWindow.document.getElementById("localVideo_container") : t.contentWindow.document.querySelector(`#participant_${e} video`)
                    }
                    _setSize(e, t) {
                        const n = _(e),
                            i = _(t);
                        void 0 !== n && (this._frame.style.height = n), void 0 !== i && (this._frame.style.width = i)
                    }
                    _setupListeners() {
                        this._transport.on("event", e => {
                            let {
                                name: t
                            } = e, n = l(e, ["name"]);
                            const i = n.id;
                            switch (t) {
                                case "video-conference-joined":
                                    this._myUserID = i, this._participants[i] = {
                                        avatarURL: n.avatarURL
                                    };
                                case "participant-joined":
                                    this._participants[i] = this._participants[i] || {}, this._participants[i].displayName = n.displayName, this._participants[i].formattedDisplayName = n.formattedDisplayName, m(this, 1);
                                    break;
                                case "participant-left":
                                    m(this, -1), delete this._participants[i];
                                    break;
                                case "display-name-change":
                                    {
                                        const e = this._participants[i];
                                        e && (e.displayName = n.displayname, e.formattedDisplayName = n.formattedDisplayName);
                                        break
                                    }
                                case "email-change":
                                    {
                                        const e = this._participants[i];
                                        e && (e.email = n.email);
                                        break
                                    }
                                case "avatar-changed":
                                    {
                                        const e = this._participants[i];
                                        e && (e.avatarURL = n.avatarURL);
                                        break
                                    }
                                case "on-stage-participant-changed":
                                    this._onStageParticipant = i, this.emit("largeVideoChanged");
                                    break;
                                case "large-video-visibility-changed":
                                    this._isLargeVideoVisible = n.isVisible, this.emit("largeVideoChanged");
                                    break;
                                case "video-conference-left":
                                    m(this, -1), delete this._participants[this._myUserID]
                            }
                            const s = f[t];
                            return !!s && (this.emit(s, n), !0)
                        })
                    }
                    addEventListener(e, t) {
                        this.on(e, t)
                    }
                    addEventListeners(e) {
                        for (const t in e) this.addEventListener(t, e[t])
                    }
                    dispose() {
                        this.emit("_willDispose"), this._transport.dispose(), this.removeAllListeners(), this._frame && this._frame.parentNode && this._frame.parentNode.removeChild(this._frame)
                    }
                    executeCommand(e, ...t) {
                        e in p ? this._transport.sendEvent({
                            data: t,
                            name: p[e]
                        }) : h.error("Not supported command name.")
                    }
                    executeCommands(e) {
                        for (const t in e) this.executeCommand(t, e[t])
                    }
                    getAvailableDevices() {
                        return Object(c.a)(this._transport)
                    }
                    getCurrentDevices() {
                        return Object(c.b)(this._transport)
                    }
                    isAudioAvailable() {
                        return this._transport.sendRequest({
                            name: "is-audio-available"
                        })
                    }
                    isDeviceChangeAvailable(e) {
                        return Object(c.c)(this._transport, e)
                    }
                    isDeviceListAvailable() {
                        return Object(c.d)(this._transport)
                    }
                    isMultipleAudioInputSupported() {
                        return Object(c.e)(this._transport)
                    }
                    invite(e) {
                        return Array.isArray(e) && 0 !== e.length ? this._transport.sendRequest({
                            name: "invite",
                            invitees: e
                        }) : Promise.reject(new TypeError("Invalid Argument"))
                    }
                    isAudioMuted() {
                        return this._transport.sendRequest({
                            name: "is-audio-muted"
                        })
                    }
                    getAvatarURL(e) {
                        const {
                            avatarURL: t
                        } = this._participants[e] || {};
                        return t
                    }
                    getDisplayName(e) {
                        const {
                            displayName: t
                        } = this._participants[e] || {};
                        return t
                    }
                    getEmail(e) {
                        const {
                            email: t
                        } = this._participants[e] || {};
                        return t
                    }
                    _getFormattedDisplayName(e) {
                        const {
                            formattedDisplayName: t
                        } = this._participants[e] || {};
                        return t
                    }
                    getIFrame() {
                        return this._frame
                    }
                    getNumberOfParticipants() {
                        return this._numberOfParticipants
                    }
                    isVideoAvailable() {
                        return this._transport.sendRequest({
                            name: "is-video-available"
                        })
                    }
                    isVideoMuted() {
                        return this._transport.sendRequest({
                            name: "is-video-muted"
                        })
                    }
                    removeEventListener(e) {
                        this.removeAllListeners(e)
                    }
                    removeEventListeners(e) {
                        e.forEach(e => this.removeEventListener(e))
                    }
                    sendProxyConnectionEvent(e) {
                        this._transport.sendEvent({
                            data: [e],
                            name: "proxy-connection-event"
                        })
                    }
                    setAudioInputDevice(e, t) {
                        return Object(c.f)(this._transport, e, t)
                    }
                    setAudioOutputDevice(e, t) {
                        return Object(c.g)(this._transport, e, t)
                    }
                    setVideoInputDevice(e, t) {
                        return Object(c.h)(this._transport, e, t)
                    }
                    _getElectronPopupsConfig() {
                        return Promise.resolve(a)
                    }
                }
            }.call(this, "modules/API/external/external_api.js")
    }, function(e, t, n) {
        var i = n(5);

        function s(e, t) {
            this.logStorage = e, this.stringifyObjects = !(!t || !t.stringifyObjects) && t.stringifyObjects, this.storeInterval = t && t.storeInterval ? t.storeInterval : 3e4, this.maxEntryLength = t && t.maxEntryLength ? t.maxEntryLength : 1e4, Object.keys(i.levels).forEach(function(e) {
                this[i.levels[e]] = function(e) {
                    this._log.apply(this, arguments)
                }.bind(this, e)
            }.bind(this)), this.storeLogsIntervalID = null, this.queue = [], this.totalLen = 0, this.outputCache = []
        }
        s.prototype.stringify = function(e) {
            try {
                return JSON.stringify(e)
            } catch (e) {
                return "[object with circular refs?]"
            }
        }, s.prototype.formatLogMessage = function(e) {
            for (var t = "", n = 1, s = arguments.length; n < s; n++) {
                var r = arguments[n];
                !this.stringifyObjects && e !== i.levels.ERROR || "object" != typeof r || (r = this.stringify(r)), t += r, n != s - 1 && (t += " ")
            }
            return t.length ? t : null
        }, s.prototype._log = function() {
            var e = this.formatLogMessage.apply(this, arguments);
            if (e) {
                var t = this.queue.length ? this.queue[this.queue.length - 1] : void 0;
                ("object" == typeof t ? t.text : t) == e ? "object" == typeof t ? t.count += 1 : this.queue[this.queue.length - 1] = {
                    text: e,
                    count: 2
                } : (this.queue.push(e), this.totalLen += e.length)
            }
            this.totalLen >= this.maxEntryLength && this._flush(!0, !0)
        }, s.prototype.start = function() {
            this._reschedulePublishInterval()
        }, s.prototype._reschedulePublishInterval = function() {
            this.storeLogsIntervalID && (window.clearTimeout(this.storeLogsIntervalID), this.storeLogsIntervalID = null), this.storeLogsIntervalID = window.setTimeout(this._flush.bind(this, !1, !0), this.storeInterval)
        }, s.prototype.flush = function() {
            this._flush(!1, !0)
        }, s.prototype._flush = function(e, t) {
            this.totalLen > 0 && (this.logStorage.isReady() || e) && (this.logStorage.isReady() ? (this.outputCache.length && (this.outputCache.forEach(function(e) {
                this.logStorage.storeLogs(e)
            }.bind(this)), this.outputCache = []), this.logStorage.storeLogs(this.queue)) : this.outputCache.push(this.queue), this.queue = [], this.totalLen = 0), t && this._reschedulePublishInterval()
        }, s.prototype.stop = function() {
            this._flush(!1, !1)
        }, e.exports = s
    }])
});
//# sourceMappingURL=external_api.min.map