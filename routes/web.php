<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::post('update-timezone', 'HomeController@updateTimezone')->name('update.timezone');
Route::get('imagick-test', 'HomeController@imagickTest')->name('imagick');
Route::post('imagick-test', 'HomeController@imagickTest')->name('imagick.save');
Route::get('check-cookie/{name}', 'Modules\Professional\ProfessionalProductsController@checkCookie')->name('check.cookie');
Route::post('whatsapp-twilio-get', 'HomeController@whatsappTwilioGet')->name('whatsapp.twilio.get');
Route::post('whatsapp-twilio-post', 'HomeController@whatsappTwilioPost')->name('whatsapp.twilio.post');

Route::get('whatsapp-twilio-post-test', 'HomeController@whatsappTwilioTest');


Route::get('audio-test', function(){
    return view('Audiojs');
});


Route::group(['middleware' => ['set.timezone']], function () {


    Auth::routes();
    Route::get('registre-se', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('registre-se',  'Auth\RegisterController@register');
    Route::post('verify-cpf',  'Auth\RegisterController@verifyCPFNO')->name('verify.cpf');
    Route::post('verify-cnpj',  'Auth\RegisterController@verifyCNPJ')->name('verify.cnpj');

    Route::get('/log-me-in/{id}/{val}', 'Auth\LoginController@logMeIn')->name('log-me-in');
    Route::any('/ware-card', 'HomeController@wireCard');
    // Route::get('/', function(){ return view("modules.maintenance"); })->name('home');
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('refer/{id}', 'HomeController@refer')->name('refer');
    Route::get('refer', 'HomeController@referUser')->name('refer.page');
    Route::get('refer-list', 'HomeController@referUserList')->name('refer.page.list');
    Route::get('logout', 'Auth\LoginController@logout')->name('logout');

    Route::get('public-profile/{slug}/free-session/{id}', 'HomeController@freeSession')->name('freeSession.invite');
    Route::get('invite/free-session', 'HomeController@inviteFreeSession')->name('invite.page');
    // for expert registration........
    // Route::get('expert-landing', 'Auth\RegisterController@showExpertLandingPage')->name('exp.landing');
    Route::get('para-profissionais', 'Auth\RegisterController@showExpertLandingPage')->name('exp.landing');
    Route::get('seja-um-profissional', 'Auth\RegisterController@showExpertRegistrationForm')->name('exp.register');
    Route::get('landing-pages','Modules\Content\ContentController@LandingPage')->name('landing.pages');
    Route::get('produto-landing-page','Modules\Content\ContentController@productLanding')->name('product.landing.page');

    // for social login........
    Route::get('oauth/{fb}/login', 'Auth\RegisterController@redirect')->name('social.login');

    Route::get('oauth/successfull', 'Auth\RegisterController@redirect')->name('oauth.successfull');

    Route::get('oauth/{prvdr}/callback', 'Auth\RegisterController@callback');

    // for email duplicate checking while registering user....
    Route::post('check-email', 'Auth\RegisterController@check_email')->name('chk.email');
    // For successpage after registration.....
    Route::get('success', 'Auth\RegisterController@successLoad')->name('front.success');

    // for fetching sub category while registering user..........
    Route::post('get-subcat', 'Auth\RegisterController@getSubcat')->name('get.subcat');

    //for product landing
    //for product reveiw
    Route::get('rate-review-product/{slug?}','Modules\Professional\ProfessionalProductsController@rateReveiw')->name('rate.review.product');
    Route::post('add-rate-review-product','Modules\Professional\ProfessionalProductsController@addRateReveiw')->name('add.rate.review.product');
    //for products
    Route::get('products', 'Modules\Professional\ProfessionalProductsController@index')->name('professional.products');
    Route::get('product-search', 'Modules\Professional\ProfessionalProductsController@index')->name('search.product');
    Route::post('product-search', 'Modules\Professional\ProfessionalProductsController@index')->name('search.product');
    Route::get('store-product/{id?}', 'Modules\Professional\ProfessionalProductsController@store')->name('store.product');
    Route::post('store-product/{id?}', 'Modules\Professional\ProfessionalProductsController@store')->name('store.product');
    Route::get('get-product-subcategories/{id?}', 'Modules\Professional\ProfessionalProductsController@getSubcategories')->name('get.product.subcategories');

    Route::get('edit-course/{id}', 'Modules\Professional\ProfessionalProductsController@editCourseChapters')->name('edit.course.chapters');
    Route::post('add-chapter', 'Modules\Professional\ProfessionalProductsController@addChapter')->name('add.chapter');
    Route::post('update-chapter', 'Modules\Professional\ProfessionalProductsController@updateChapter')->name('update.chapter');
    Route::post('chapter-text-img-upload', 'Modules\Professional\ProfessionalProductsController@textImgUpload')->name('chapter.text.img.upload');
    Route::post('check-chapter-title', 'Modules\Professional\ProfessionalProductsController@checkChapterTitle')->name('check.chapter.title');
    Route::get('delete-chapter/{id?}', 'Modules\Professional\ProfessionalProductsController@deleteChapter')->name('delete.chapter');

    Route::post('save-lesson', 'Modules\Professional\ProfessionalProductsController@saveLesson')->name('save.lesson');
    Route::get('get-lessons/{id}', 'Modules\Professional\ProfessionalProductsController@getLessons')->name('get.lessons');
    Route::get('get-preview-lessons/{id}', 'Modules\Professional\ProfessionalProductsController@getPreviewLessons')->name('get.preview.lessons');
    Route::post('find-chapters', 'Modules\Professional\ProfessionalProductsController@findChapters')->name('find.chapters');
    Route::get('get-lesson-details/{id}', 'Modules\Professional\ProfessionalProductsController@getLessonDetails')->name('lesson.details');

    Route::get('delete-lesson/{id}/{ajax?}', 'Modules\Professional\ProfessionalProductsController@deleteLesson')->name('delete.lesson');
    Route::get('delete-attachment/{id?}', 'Modules\Professional\ProfessionalProductsController@deleteAttachment')->name('delete.attachment');
    Route::get('delete-download/{id?}', 'Modules\Professional\ProfessionalProductsController@deleteDownload')->name('delete.download');
    Route::get('delete-question/{id?}', 'Modules\Professional\ProfessionalProductsController@deleteQuestion')->name('delete.question');
    Route::get('delete-slide/{id?}', 'Modules\Professional\ProfessionalProductsController@deleteSlide')->name('delete.slide');


    Route::post('upload-downloads', 'Modules\Professional\ProfessionalProductsController@uploadDownloads')->name('upload.downloads');
    Route::post('upload-audio', 'Modules\Professional\ProfessionalProductsController@uploadAudio')->name('upload.audio');
    Route::post('upload-video', 'Modules\Professional\ProfessionalProductsController@uploadVideo')->name('upload.video');
    Route::post('upload-attachments', 'Modules\Professional\ProfessionalProductsController@uploadAttachemnts')->name('upload.attachments');
    Route::post('upload-slide', 'Modules\Professional\ProfessionalProductsController@uploadSlide')->name('upload.slide');
    Route::get('delete-main-video/{id}', 'Modules\Professional\ProfessionalProductsController@deleteMainVideo')->name('delete. main.video');

    Route::get('preview-course/{id}', 'Modules\Professional\ProfessionalProductsController@previewLesson')->name('preview.lesson');
    Route::get('view-course/{id}/{master_id?}', 'Modules\Professional\ProfessionalProductsController@viewCourse')->name('view.course');
    Route::get('view-free-lessons/{slug}', 'Modules\Professional\ProfessionalProductsController@viewFreeLessons')->name('view.free.lessons');

    Route::post('post-lesson-question', 'Modules\Professional\ProfessionalProductsController@postLessonQuestion')->name('post.lesson.question');
    Route::post('post-lesson-answer', 'Modules\Professional\ProfessionalProductsController@postLessonAnswer')->name('post.lesson.answer');
    Route::get('delete-lesson-question/{id}', 'Modules\Professional\ProfessionalProductsController@deleteLessonQuestion')->name('delete.lesson.question');
    Route::get('delete-lesson-answer/{id}', 'Modules\Professional\ProfessionalProductsController@deleteLessonAnswer')->name('delete.lesson.answer');
    Route::post('user-course-progress', 'Modules\Professional\ProfessionalProductsController@userCourseProgress')->name('user.course.progress');
    Route::post('user-quiz-progress', 'Modules\Professional\ProfessionalProductsController@userQuizProgress')->name('user.quiz.progress');
    Route::get('delete-quiz-progress/{id}', 'Modules\Professional\ProfessionalProductsController@deleteQuizProgress')->name('delete.quiz.progress');


    Route::get('product-file/{id}', 'Modules\Professional\ProfessionalProductsController@productFileView')->name('product.file.view');
    Route::get('product-file-upload/{id}', 'Modules\Professional\ProfessionalProductsController@productFileUploadView')->name('product.file.upload.view');
    Route::post('product-file-upload/{id}', 'Modules\Professional\ProfessionalProductsController@productFileUpload')->name('product.file.upload');
    Route::get('product-file-delete/{id}', 'Modules\Professional\ProfessionalProductsController@productFileDelete')->name('product.file.delete');
    Route::get('change-status-product/{id}', 'Modules\Professional\ProfessionalProductsController@changeProductStatus')->name('changestatus.product');
    Route::get('delete-product/{id}', 'Modules\Professional\ProfessionalProductsController@deleteProduct')->name('delete.product');
    Route::get('product-details/{slug}/{affcode?}', 'Modules\Professional\ProfessionalProductsController@productDetails')->name('product.details');
    Route::post('get-preview-video','Modules\Professional\ProfessionalProductsController@getVideo')->name('product.preview.video');
    Route::post('add-to-wishlist','Modules\Professional\ProfessionalProductsController@addWishlist')->name('add.to.wishlist');
    Route::post('remove-from-wishlist','Modules\Professional\ProfessionalProductsController@removeWishlist')->name('remove.from.wishlist');
    Route::post('search-keyword','Modules\Professional\ProfessionalProductsController@searchReview')->name('search.review');
    Route::post('search-rate','Modules\Professional\ProfessionalProductsController@searchRate')->name('search.rate');
    Route::get('my-sales', 'Modules\Professional\ProfessionalProductsController@professionalOrders')->name('professional.orders');
    Route::post('my-sales', 'Modules\Professional\ProfessionalProductsController@professionalOrders')->name('professional.orders.search');
    Route::get('view-order/{id?}/{prod_id?}', 'Modules\Professional\ProfessionalProductsController@viewProfessionalOrder')->name('view.professional.order');
    Route::post('upload-product-file/{id}', 'Modules\Professional\ProfessionalProductsController@uploadProductFiles')->name('upload.product.files');

    Route::post('quiz-import', 'Modules\Professional\ProfessionalProductsController@import')->name('quiz.import');

    Route::get('assign-product-certificate-template/{id?}','Modules\Professional\ProfessionalProductsController@assignProductTemplate')->name('assign.product.certificate.template');
    Route::get('to-assign-product-certificate-template/{tid}/{pid}','Modules\Professional\ProfessionalProductsController@toAssignProductTemplate')->name('to.assign.product.certificate.template');

    Route::post('allow-download-certificate','Modules\Professional\ProfessionalProductsController@allowDownload')->name('allow.download.certificate');


    //professional add coupons
    Route::get('my-coupons', 'Modules\Professional\ProfessionalProductsController@professionalCouponList')->name('professional.coupons');
    Route::get('add-coupon/{id?}', 'Modules\Professional\ProfessionalProductsController@professionalAddCoupon')->name('professional.add.coupon');
    Route::post('store-coupon', 'Modules\Professional\ProfessionalProductsController@storeCoupon')->name('store.coupon');
    Route::get('delete-coupon/{id}', 'Modules\Professional\ProfessionalProductsController@deleteCoupon')->name('delete.coupon');
    Route::post('check-coupon-product', 'Modules\ProductOrder\ProductOrderController@checkCoupon')->name('check.coupon.product');
    Route::post('add-payment-method-product/{slug}', 'Modules\ProductOrder\ProductOrderController@addPaymentCoupon')->name('add.pamentmethod.coupon.product');
    Route::post('upload-payment-document-product/{slug}', 'Modules\ProductOrder\ProductOrderController@uploadDocument')->name('upload.payment.document.product');
    Route::get('remove-payment-method-product/{slug}', 'Modules\ProductOrder\ProductOrderController@removePaymentMethod')->name('remove.payment.method.product');
    //change sidebar

    Route::post('change-sidebar','Modules\User\UserProfileController@changeSidebar')->name('change.sidebar');

    // for products - user
    Route::get('my-purchases', 'Modules\User\UserProfileController@myProductOrders')->name('user.products');
    Route::post('my-purchases', 'Modules\User\UserProfileController@myProductOrders')->name('user.products.search');
    Route::get('order-details/{id}/{prod_id}', 'Modules\User\UserProfileController@userProductOrderDetails')->name('order.details');
    Route::get('order-bill/{id}', 'Modules\User\UserProfileController@userProductOrderBill')->name('order.bill');
    Route::get('view-cerificate/{order_id?}','Modules\User\UserProfileController@viewCertificate')->name('view.certificate');

    // Route::get('certificate-download/{order_id?}','Modules\User\UserProfileController@downloadCertificate')->name('certificate.download');
    Route::post('certificate-download','Modules\User\UserProfileController@downloadCertificate')->name('certificate.download');

    Route::get('view-invoice/{pro_id}/{order_id}','Modules\User\UserProfileController@viewInvoice')->name('view.invoice');

    // for professional profile edit..........
    Route::post('get-state', 'Modules\User\UserProfileController@getState')->name('front.get.state');


    Route::post('get-city', 'Modules\Professional\ProfessionalProfileController@getCity')->name('front.get.city');

    Route::get('edit-profile', 'Modules\Professional\ProfessionalProfileController@index')->name('professional.profile');
    Route::get('verify-identity', 'Modules\Professional\ProfessionalProfileController@verifyIdentity')->name('professional.verify.identity');
    Route::post('verify-identity', 'Modules\Professional\ProfessionalProfileController@verifyIdentity')->name('professional.verify.identity');

    Route::get('change-professional-password', 'Modules\Professional\ProfessionalProfileController@changPassword')->name('change.professional.password');
    Route::get('upload-signature', 'Modules\Professional\ProfessionalProfileController@uploadSign')->name('upload.signature');
    Route::post('upload-signature', 'Modules\Professional\ProfessionalProfileController@uploadSign')->name('upload.signature');
    Route::get('introductory-video', 'Modules\Professional\ProfessionalProfileController@introductoryVideo')->name('professional.profile.introductory.video');
    Route::post('introductory-video', 'Modules\Professional\ProfessionalProfileController@introductoryVideoSubmit')->name('professional.profile.introductory.video.submit');
    Route::post('update-paypal-address', 'Modules\Professional\ProfessionalProfileController@updatePaypalAddress')->name('update.paypal.address');

    Route::get('profile-category', 'Modules\Professional\ProfessionalProfileController@profileCategory')->name('professional.change.category');
    Route::post('profile-category', 'Modules\Professional\ProfessionalProfileController@profileCategory');

    Route::post('set-bank', 'Modules\Professional\ProfessionalProfileController@addBank')->name('set.bank');

    Route::post('check-mobile', 'Modules\Professional\ProfessionalProfileController@checkMobile')->name('chk.mobile');
    Route::post('check-nick-name', 'Modules\Professional\ProfessionalProfileController@checkNickName')->name('chk.nick.name');


    Route::post('update-professional-account', 'Modules\Professional\ProfessionalProfileController@index')->name('my.profile');

    // for loading professionals dashboard..............
    Route::get('dashboard', 'Modules\Professional\ProfessionalProfileController@load_professional_dashboard')->name('load_professional_dashboard');

    Route::get('qualification', 'Modules\Professional\ProfessionalProfileController@professional_qualification')->name('professional_qualification');

    Route::post('add-qualification', 'Modules\Professional\ProfessionalProfileController@professional_qualification')->name('add_professional_qualification');

    Route::get('qualification/{id}', 'Modules\Professional\ProfessionalProfileController@update_professional_qualification')->name('my_qualification');

    Route::post('update-qualification/{id}', 'Modules\Professional\ProfessionalProfileController@update_professional_qualification')->name('my_qualification_update');

    Route::get('remove-qualification/{pid}', 'Modules\Professional\ProfessionalProfileController@delete_qualification')->name('delete_qualification');

    // For professional experience

    // Route::get('my-experience', 'Modules\Professional\ProfessionalProfileController@professionalExperience')->name('professional.exp');

    // Route::post('add-experience', 'Modules\Professional\ProfessionalProfileController@professionalExperience')->name('add.professional.experience');

    // Route::get('edit-experience/{id}', 'Modules\Professional\ProfessionalProfileController@edit_professional_experience')->name('edit.professional.experience');

    // Route::post('update-experience/{id}', 'Modules\Professional\ProfessionalProfileController@edit_professional_experience')->name('update.professional.experience');

    // Route::get('delete-experience/{id}', 'Modules\Professional\ProfessionalProfileController@delete_experience')->name('delete.experience');


    Route::get('my-experience', 'Modules\Professional\ProfessionalProfileController@professionalExperienceNew')->name('professional.exp');
    Route::post('store-professional-experience', 'Modules\Professional\ProfessionalProfileController@professionalExperienceNew')->name('store.professional.experience');
    Route::post('add-new-experience', 'Modules\Professional\ProfessionalProfileController@professionalAddNewExperience')->name('add.new.experience');



    // For Verifiying user email..........
    Route::get('check-mail/{vcode}', 'Auth\RegisterController@verifyMail')->name('check.email.verify');

    // For User Avaliability...................

    Route::get('availability', 'Modules\Professional\ProfessionalProfileController@user_availability')->name('user_availability');

    Route::post('availability', 'Modules\Professional\ProfessionalProfileController@user_availability')->name('submit_user_availability');

    Route::post('availability-update', 'Modules\Professional\ProfessionalProfileController@user_availability')->name('user_availability_update');
    Route::post('availability-update/ajax-check', 'Modules\Professional\ProfessionalProfileController@userAvailabilityAjaxCheck')->name('user_availability_update.ajax.check');
    Route::post('availability-copy', 'Modules\Professional\ProfessionalProfileController@copySchedule')->name('submit_user_availability_copy');
    Route::post('availability-copy/ajax-check', 'Modules\Professional\ProfessionalProfileController@copyScheduleAjaxCheck')->name('user_availability_copy.ajax.check');

    Route::post('availability-update', 'Modules\Professional\ProfessionalProfileController@user_availability_insert')->name('user.insert.avl');

    Route::get('availability-delete/{id}', 'Modules\Professional\ProfessionalProfileController@delete_avl')->name('delete_avl');
    Route::get('availability-delete-all', 'Modules\Professional\ProfessionalProfileController@deleteAvlAll')->name('delete_avl.all');

    Route::post('set-month-schedule', 'Modules\Professional\ProfessionalProfileController@setMonthSchedule')->name('set.month.schedule');
    Route::post('set-week-schedule', 'Modules\Professional\ProfessionalProfileController@setWeekSchedule')->name('set.week.schedule');

    //for certificate template

    Route::get('professional-certificate-template','Modules\ProfCertificateTemp\ProfCertificateTempController@index')->name('index.certificate.template');
    Route::get('professional-certificate-template-create','Modules\ProfCertificateTemp\ProfCertificateTempController@create')->name('create.certificate.template');

    Route::get('professional-certificate-template-view/{id?}/{pid?}','Modules\ProfCertificateTemp\ProfCertificateTempController@viewProductCertificateTemplate')->name('view.product.certificate.template');

    Route::get('professional-certificate-template-show/{id?}','Modules\ProfCertificateTemp\ProfCertificateTempController@show')->name('show.certificate.template');
    Route::post('professional-certificate-template-store','Modules\ProfCertificateTemp\ProfCertificateTempController@store')->name('store.certificate.template');

    Route::get('professional-certificate-template-edit/{id?}','Modules\ProfCertificateTemp\ProfCertificateTempController@edit')->name('edit.certificate.template');

    Route::post('professional-certificate-template-update/{id?}','Modules\ProfCertificateTemp\ProfCertificateTempController@update')->name('update.certificate.template');

    Route::get('professional-certificate-template-delete/{id?}','Modules\ProfCertificateTemp\ProfCertificateTempController@destroyCertTemp')->name('delete.certificate.template');

    Route::get('reset-certificate-background/{id}','Modules\ProfCertificateTemp\ProfCertificateTempController@resetBackgroundImg')->name('reset.certificate.background');

    Route::post('prof-certificate-user-assigne', 'Modules\ProfCertificateTemp\ProfCertificateTempController@profCertificateTempUserAssigne')->name('prof.certificate.temp.user.assigne');

    Route::get('prof-certificate-assigne-list/{id}', 'Modules\ProfCertificateTemp\ProfCertificateTempController@profCertificateTempUserAssigneList')->name('prof.certificate.temp.user.assigne.list');

    Route::get('prof-certificate-status/{id}', 'Modules\ProfCertificateTemp\ProfCertificateTempController@certifiacteStatusUpdate')->name('prof.certificate.status');

    Route::get('certificate-copy/{id}','Modules\ProfCertificateTemp\ProfCertificateTempController@certificateCopy')->name('certificate.copy');

    Route::get('preview-template/{ord_id}','Modules\ProfCertificateTemp\ProfCertificateTempController@previewTemplate')->name('preview.template.certificate');

    Route::post('update-certificate-signature','Modules\ProfCertificateTemp\ProfCertificateTempController@updateSignature')->name('update.certificate.signature');


    // Landing Page
    Route::get('landing-page-template/{id}','Modules\LandingPage\LandingPageController@loadTemplate')->name('landing.page.template');

    Route::get('landing-page/list','Modules\LandingPage\LandingPageController@index')->name('list.landing.page.templates');
    Route::get('landing-page/create','Modules\LandingPage\LandingPageController@create')->name('create.landing.page.template');
    Route::get('landing-page/edit/{id}','Modules\LandingPage\LandingPageController@create')->name('edit.landing.page.template');
    Route::post('landing-page/store/{id?}','Modules\LandingPage\LandingPageController@store')->name('store.landing.page.template');
    Route::post('landing-page/single-video-upload/{id?}','Modules\LandingPage\LandingPageController@singleVideoUpload')->name('store.landing.page.single.video');

    Route::get('landing-page/delete/{id}','Modules\LandingPage\LandingPageController@delete')->name('delete.landing.page.template');
    Route::post('landing-page/check-name','Modules\LandingPage\LandingPageController@checkName')->name('check.template.name');

    Route::post('landing-page/multiple-content','Modules\LandingPage\LandingPageController@updateMultiContent')->name('store.landing.page.multiple.content');
    Route::get('landing-page/multiple-content-delete/{tid}/{num}','Modules\LandingPage\LandingPageController@deleteMultiContent')->name('remove.landing.page.multiple.content');
    Route::get('landing-page/all-multiple-content-delete/{tid}','Modules\LandingPage\LandingPageController@deleteAllMultiContent')->name('remove.all.landing.page.multiple.content');

    Route::post('landing-page/product-details','Modules\LandingPage\LandingPageController@storeProductDetails')->name('store.landing.page.product.details');
    Route::get('landing-page/delete-product/{id}','Modules\LandingPage\LandingPageController@deleteProductDetails')->name('delete.landing.page.product.details');
    Route::get('landing-page/delete-product-image/{id}','Modules\LandingPage\LandingPageController@deleteProductImage')->name('delete.landing.page.product.image');

    Route::get('landing-page/reset-landing-logo/{id}','Modules\LandingPage\LandingPageController@resetLandingLogo')->name('reset.landing.logo');

    Route::get('/l/{prof_slug}/{slug}','Modules\LandingPage\LandingPageController@landingPage')->name('landing.page');

    Route::post('landing-page/lead-form','Modules\LandingPage\LandingPageController@saveLead')->name('landing.page.save.lead');
    Route::post('landing-page/save-whyus','Modules\LandingPage\LandingPageController@saveWhyUs')->name('landing.page.save.whyus');
    Route::get('landing-page/delete-whyus/{id}','Modules\LandingPage\LandingPageController@deleteWhyUs')->name('landing.page.delete.whyus');

    Route::post('landing-page/save-faq','Modules\LandingPage\LandingPageController@saveFaq')->name('landing.page.save.faq');
    Route::get('landing-page/delete-faq/{id}','Modules\LandingPage\LandingPageController@deleteFaq')->name('landing.page.delete.faq');
    Route::get('landing-page/publish/{id}','Modules\LandingPage\LandingPageController@publishPage')->name('landing.page.publish');
    Route::get('landing-page/unpublish/{id}','Modules\LandingPage\LandingPageController@unpublishPage')->name('landing.page.unpublish');

    Route::get('landing-page-Payment','Modules\LandingPage\LandingPagePaymentController@landingPaymentCreate')->name('landing.page.payment.create');
    Route::get('landing-page-Payment/{token}','Modules\LandingPage\LandingPagePaymentController@storeSuccess')->name('landing.page.payment.store.view');
    Route::post('landing-page-Payment-type/{token}','Modules\LandingPage\LandingPagePaymentController@addPaymentCoupon')->name('landing.page.payment.type');
    Route::post('landing-complete-profile', 'Modules\LandingPage\LandingPagePaymentController@completeProfile')->name('landing.user.profile');
    Route::post('landing-payment-create-stripe', 'Modules\LandingPage\LandingPagePaymentController@paymentCreate')->name('landing.payment.stripe.create');
    Route::post('landing-payment-response-stripe', 'Modules\LandingPage\LandingPagePaymentController@paymentSuccessHandlerStripe')->name('landing.payment.stripe');
    Route::get('landing-remove-payment-method/{slug}', 'Modules\LandingPage\LandingPagePaymentController@removePaymentMethod')->name('landing.remove.payment.method');
    Route::post('paypal/landing', 'Modules\LandingPage\LandingPagePaymentController@postPaymentWithpaypal')->name('paypal.landing.post');
    Route::get('paypal/landing/status', 'Modules\LandingPage\LandingPagePaymentController@getPaymentStatus')->name('paypal.landing.status');

    Route::get('landing-page/leads/{id}', 'Modules\LandingPage\LandingPageController@viewLeads')->name('landing.page.leads');
    Route::get('landing-page/leads-export/{id}', 'Modules\LandingPage\LandingPageController@viewLeadsExport')->name('landing.page.leads.exports');

    Route::get('landing-page/payment', 'Modules\LandingPage\LandingPageController@viewPayments')->name('landing.page.payment');

    Route::get('categoria-landing-page', 'Modules\Professional\ProfessionalCategoryLandingController@index')->name('category.landing.page');
    Route::get('categoria-landing-page/{slug}', 'Modules\Professional\ProfessionalCategoryLandingController@index')->name('category.landing.page.f');


    // For search..................

    Route::get('consultant/{slug}', 'Modules\Search\SearchController@index')->name('srch.slug');

    Route::post('consultant', 'Modules\Search\SearchController@index')->name('srch.pst');

    Route::get('consultant', 'Modules\Search\SearchController@index')->name('srch.pst');

    Route::get('categorias-profissionais', 'Modules\Search\SearchController@consultantCategories')->name('consultant.categories');

    Route::post('fetch-subcat', 'Modules\Search\SearchController@getSubcat')->name('fetch.subcat');

    Route::get('profile/{slug}', 'Modules\Professional\PublicProfileController@index')->name('pblc.pst');
    Route::get('public-profile-preview/{slug}/{self}', 'Modules\Professional\PublicProfileController@index')->name('public.profile.preview');
    Route::post('reply-comment/{id}', 'Modules\Professional\PublicProfileController@replyComment')->name('reply.comment');

    Route::get('copy-month-schedule/{query_date}/{copy_date}', 'Modules\Professional\PublicProfileController@copyMonthSchedule')->name('copy.month.schedule');
    Route::get('copy-week-schedule/{query_date}/{copy_date}', 'Modules\Professional\PublicProfileController@copyWeekSchedule')->name('copy.week.schedule');


    // Add to favourite
    Route::post('add-tp-favourite', 'Modules\Professional\PublicProfileController@addFavourite')->name('add.favourite');

    Route::get('reschedule-approve/{id}', 'Modules\Professional\PublicProfileController@rescheduleApprove')->name('reschedule.approve');
    Route::get('reschedule-reject/{id}', 'Modules\Professional\PublicProfileController@rescheduleReject')->name('reschedule.reject');

    Route::get('browse-categories', 'Modules\Search\SearchController@browseCat')->name('browse.categories');



    // For User Profile Managment

    Route::get('my-dashboard', 'Modules\User\UserProfileController@load_user_dashboard')->name('load_user_dashboard');

    Route::get('edit-my-profile', 'Modules\User\UserProfileController@index')->name('user.profile');

    Route::get('change-password', 'Modules\User\UserProfileController@changePassword')->name('user.change.password');

    Route::post('update-user-account', 'Modules\User\UserProfileController@index')->name('user.update.profile');

    // For Forget Password

    Route::get('forget-password', 'Auth\RegisterController@forgetPass')->name('forget.pass');

    Route::post('send-password-link', 'Auth\RegisterController@forgetPass')->name('send.pass.link');

    Route::get('reset-password/{code}', 'Auth\RegisterController@resetPass')->name('resetPass');

    Route::post('update-password', 'Auth\RegisterController@updatePass')->name('updatePass');

    Route::get('became-professional', 'Modules\User\UserProfileController@becameProfessional')->name('be.professional');

    Route::post('store-professional', 'Modules\User\UserProfileController@becameProfessional')->name('store.professional');

    // for fetching sub category while registering user..........
    Route::post('front-get-subcat', 'Modules\User\UserProfileController@getSubcat')->name('front.get.subcat');

    Route::get('rescheduled-time-list', 'Modules\User\UserProfileController@rescheduledTimeList')->name('rescheduled.time.list');

    // For Favourite List

    Route::get('favourite-expert', 'Modules\User\UserProfileController@favouriteList')->name('favourite.list');

    Route::get('remove-favourite/{id}', 'Modules\User\UserProfileController@removeFav')->name('remove.my.favourite');


    // For Booking Managment

    Route::get('booking/{slug}', 'Modules\Booking\BookingController@index')->name('book.now');

    Route::post('pay-now', 'Modules\Booking\BookingController@createCustomer')->name('pay.now');

    Route::post('complete-profile', 'Modules\Booking\BookingController@completeProfile')->name('cmplt.user.profile');

    Route::post('store-booking/{slug}', 'Modules\Booking\BookingController@storeBooking')->name('store.booking');

    Route::post('chcek-booking', 'Modules\Booking\BookingController@checkDate')->name('checkDate');

    Route::get('successfully-booked/{token}', 'Modules\Booking\BookingController@bookingSuccess')->name('booking.sces');
    Route::post('front-get-date', 'Modules\Booking\BookingController@getDate')->name('front.get.date');

    Route::post('booking-cancel/{token}/{id}', 'Modules\Booking\BookingController@bookingCancel')->name('booking.cancel');


    // For user booking

    Route::get('my-bookings/{type}', 'Modules\User\UserProfileController@bookingRequest')->name('user.my.booking');

    Route::get('my-bookings/filter/{type}', 'Modules\User\UserProfileController@bookingRequest')->name('srch.user.my.booking');

    Route::get('my-bookings/filter/{type}', 'Modules\User\UserProfileController@bookingRequest')->name('srch.user.my.booking');

    Route::get('cancel-my-booking/{token}', 'Modules\User\UserProfileController@cancelBookingRequest')->name('user.cancel.req');

    Route::post('reschedeule-my-booking', 'Modules\User\UserProfileController@rescheduleBookingRequest')->name('user.booking.reschedule');

    Route::get('view-booked-details/{id}', 'Modules\User\UserProfileController@viewBookedDetails')->name('user.booking.details1');

    // For professional booking from Dashboard
    Route::get('dashboard-bookings/{type}', 'Modules\Professional\ProfessionalProfileController@load_professional_dashboard')->name('prof.dash.my.booking');

    Route::get('dashboard-bookings/filter/{type}', 'Modules\Professional\ProfessionalProfileController@load_professional_dashboard')->name('srch.prof.dash.my.booking');

    Route::get('dashboard-bookings/filter/{type}', 'Modules\Professional\ProfessionalProfileController@load_professional_dashboard')->name('srch.prof.dash.my.booking');

    // For professional booking

    Route::get('bookings/{type}', 'Modules\Professional\ProfessionalProfileController@bookingRequest')->name('prof.my.booking');

    Route::get('bookings/filter/{type}', 'Modules\Professional\ProfessionalProfileController@bookingRequest')->name('srch.prof.my.booking');

    Route::get('bookings/filter/{type}', 'Modules\Professional\ProfessionalProfileController@bookingRequest')->name('srch.prof.my.booking');

    Route::get('cancel-booking/{token}', 'Modules\Professional\ProfessionalProfileController@cancelBookingRequest')->name('prof.my.cancel.req');

    Route::get('accsept-booking/{token}', 'Modules\Professional\ProfessionalProfileController@accseptBookingRequest')->name('prof.acc.req');

    Route::get('view-booking-details/{id}', 'Modules\Professional\ProfessionalProfileController@viewBookedUserDetails')->name('view.booked.user.details');



    Route::get('professional-upcomming-class', 'Modules\Professional\ProfessionalProfileController@upcommingClass')->name('professional.upcomming.class');
    Route::post('professional-upcomming-class', 'Modules\Professional\ProfessionalProfileController@upcommingClass')->name('professional.upcomming.class');
    Route::get('professional-as-user-upcomming-class', 'Modules\Professional\ProfessionalProfileController@upcommingClass1')->name('professional.as.user.upcomming.class');
    Route::post('professional-as-user-upcomming-class', 'Modules\Professional\ProfessionalProfileController@upcommingClass1')->name('professional.as.user.upcomming.class');

    // For messaging servicce..................

    Route::get('message', 'Modules\Professional\ProfessionalMessageController@index')->name('message');

    Route::get('compose-message', 'Modules\Professional\ProfessionalMessageController@compose')->name('compose');

    Route::get('compose-message/{id}', 'Modules\Professional\ProfessionalMessageController@compose')->name('compose.msg');

    Route::get('delete-conversation/{token}', 'Modules\Professional\ProfessionalMessageController@deleteMessage')->name('delete_message');

    Route::get('message/{token}/details', 'Modules\Professional\ProfessionalMessageController@viewMessage')->name('viewMessage');

    Route::post('insrt-msg-prf', 'Modules\Professional\ProfessionalMessageController@insertMsg')->name('insrt.msg.prf');

    Route::post('insrt-atch', 'Modules\Professional\ProfessionalMessageController@insertAtch')->name('insrt.atch');

    Route::get('reply/{id}', 'Modules\Professional\ProfessionalMessageController@reply')->name('user.reply');

    Route::post('insrt-msg-reply', 'Modules\Professional\ProfessionalMessageController@insertReplyMsg')->name('insrt.msg.reply');

    //user upcomming class
    // Route::get('user-past-class', 'Modules\User\UserProfileController@pastClass')->name('user.past.class');

    // Route::post('user-past-class', 'Modules\User\UserProfileController@pastClass')->name('user.past.filter.class');

    Route::get('user-upcomming-class', 'Modules\User\UserProfileController@upcommingList')->name('user.upcomming.class');

    Route::post('user-upcomming-class', 'Modules\User\UserProfileController@upcommingList')->name('user.upcomming.class');
    Route::get('review/{booking_id}/{professional_id}', 'Modules\User\UserProfileController@review')->name('user.review');
    Route::post('user-post-review', 'Modules\User\UserProfileController@postReview')->name('user.post.review');

    // For video conference

    Route::get('video-call', 'Modules\VideoCall\VideoCallController@index')->name('my.video');

    Route::post('update-video-status', 'Modules\VideoCall\VideoCallController@updateStatus')->name('update.video.status');
    Route::post('update-video-initiated', 'Modules\VideoCall\VideoCallController@updateVideoInitiated')->name('update.video.initiated');

    Route::post('update-call-time', 'Modules\VideoCall\VideoCallController@updateCallTime')->name('update.call.time');

    Route::get('get-twilio-token', 'Modules\VideoCall\VideoCallController@getTwilioToken')->name('get.twilio.token');
    Route::post('video-call-booking-details', 'Modules\VideoCall\VideoCallController@getBookingDetails')->name('video.booking.details');


    // For blog managment.....

    Route::get('my-blog', 'Modules\Blog\BlogController@index')->name('my.blog');

    Route::post('my-blog', 'Modules\Blog\BlogController@index')->name('my.blog.search');

    Route::get('add-blog-post', 'Modules\Blog\BlogController@add')->name('add.blog.post');

    Route::post('store-blog-post', 'Modules\Blog\BlogController@add')->name('store.blog.post');

    Route::get('view-blog-post/{id}', 'Modules\Blog\BlogController@view')->name('view.blog.post');

    Route::get('remove-blog-post/{id}', 'Modules\Blog\BlogController@delete')->name('remove.blog.post');

    Route::get('edit-blog-post/{id}', 'Modules\Blog\BlogController@edit')->name('edit.blog.post');

    Route::post('edit-blog-post/{id}', 'Modules\Blog\BlogController@edit')->name('update.blog.post');

    Route::post('article-image-upload', 'Modules\Blog\BlogController@imgUpload')->name('artical.img.upload');

    Route::get('blog', 'Modules\FrontendBlog\FrontendBlogController@index')->name('blog.frontend');

    Route::get('blog/{slug}', 'Modules\FrontendBlog\FrontendBlogController@index')->name('blog.frontend.slug');

    Route::get('blog-details/{slug}', 'Modules\FrontendBlog\FrontendBlogController@details')->name('blog.frontend.details');

    Route::post('publish-comment', 'Modules\FrontendBlog\FrontendBlogController@comments')->name('publish.comment');
    Route::post('blog-contact', 'Modules\FrontendBlog\FrontendBlogController@BlogContact')->name('blog.contact');

    // For inserting email on newsletter
    Route::post('news-letter', 'Modules\Newsletter\NewsletterController@insert')->name('news_letter');

    // For About Us
    Route::get('sobre-nós', 'Modules\AboutUs\AboutUsController@index')->name('frontend.about');

    //For content management
    Route::get('termos-e-politicas', 'Modules\Content\ContentController@termsOfservices')->name('terms.of.services');
    Route::get('termos-e-politicas/{slug}', 'Modules\Content\ContentController@subtermsOfservices')->name('subterms.of.services');
    Route::get('privacy-policy', 'Modules\Content\ContentController@privacyPolicy')->name('privacy.policy');
    Route::get('faq/{for?}/{id?}', 'Modules\Content\ContentController@faq')->name('faq');

    Route::get('central-de-ajuda/{slug?}', 'Modules\Content\ContentController@helpSection')->name('help.section');
    // Route::get('help-articles/{slug?}', 'Modules\Content\ContentController@helpSection')->name('help.section');
    // Route::post('central-de-ajuda', 'Modules\Content\ContentController@helpSection')->name('help.section');
    Route::get('help-categories/{slug}', 'Modules\Content\ContentController@helpArticles')->name('help.articles');
    // Route::post('help-articles/{slug}', 'Modules\Content\ContentController@helpArticles')->name('help.articles');
    Route::get('help-article-details/{slug?}', 'Modules\Content\ContentController@helpArticleDetails')->name('help.article.details');

    Route::post('fetch-help-articles', 'Modules\Content\ContentController@fetchArticleSearch')->name('fetch.help.articles');

    // help_article_details

    // Route::get('paytest', 'Modules\TestPayment\PaymentController@index');

    // Route::get('create-customer', 'Modules\TestPayment\PaymentController@customer');

    // Route::post('create-transparent', 'Modules\TestPayment\PaymentController@index')->name('create_trans');

    // Route::get('create-order', 'Modules\TestPayment\PaymentController@order')->name('create_order');

    // Route::get('add-bank', 'Modules\TestPayment\PaymentController@addBankAccount');

    // Route::post('create-bank', 'Modules\TestPayment\PaymentController@addBankAccount')->name('create_ac');


    // For form Toots start
    Route::get('user-view-form', 'Modules\Form\FormController@userViewForm')->name('user.view.form');
    Route::get('user-view-form-details/{id}', 'Modules\Form\FormController@userViewFormDetails')->name('user.view.form.details');
    Route::post('user-view-form-post', 'Modules\Form\FormController@userViewFormPost')->name('user.view.form.post');
    Route::get('user-view-form-submit-details/{id}', 'Modules\Form\FormController@userViewFormSubmitDetails')->name('user.view.form.submit.details');

    // For form Toots end


    // for form Tools start for professional
    Route::get('professional-view-form', 'Modules\Form\FormController@professionalViewForm')->name('professional.view.form');

    Route::resource('professional-form', 'Modules\ProfessionalForm\ProfessionalFormController');
    Route::get('professional-form-question/create/{id}', 'Modules\ProfessionalForm\ProfessionalQuestionController@create')->name('form.question.create');
    Route::post('professional-form-question/create/{id}', 'Modules\ProfessionalForm\ProfessionalQuestionController@store')->name('form.question.store');
    Route::get('professional-form-question/manage/{id}', 'Modules\ProfessionalForm\ProfessionalQuestionController@index')->name('form.question.manage');
    Route::get('professional-form-question/edit/{form}/{id}', 'Modules\ProfessionalForm\ProfessionalQuestionController@edit')->name('form.question.edit');
    Route::post('professional-form-question/edit/{form}/{id}', 'Modules\ProfessionalForm\ProfessionalQuestionController@update')->name('form.question.update');
    Route::get('professional-form-question/delete/{form}/{id}', 'Modules\ProfessionalForm\ProfessionalQuestionController@delete')->name('form.question.delete');

    Route::post('professional-user-assigne', 'Modules\ProfessionalForm\ProfessionalFormController@professionalUserAssigne')->name('professional-user-assigne');

    //14.3.20

    Route::get('prof-view-alluserform-feedback/{id}', 'Modules\ProfessionalForm\ProfessionalFormController@viewAllUserFeedback')->name('prof.view.alluserform.feedback');
    Route::post('prof-formfeedback-post-touser', 'Modules\ProfessionalForm\ProfessionalFormController@postFeedbackTouser')->name('prof.formfeedback.post.touser');

    //for users 14.3.20
    Route::post('user-form-tools-feedback', 'Modules\ProfessionalForm\ProfessionalFormController@userFormToolsFeedback')->name('user.form.tools.feedback');

    // form toos status change 17.3.20
    Route::get('professional-form-status/{id}', 'Modules\ProfessionalForm\ProfessionalFormController@formStatusUpdate')->name('professional.formtools.status');

    // for form Tools end for professional

    //for 360degree-evaluation test start

    // professional

    Route::get('360degree-evaluation', 'Modules\ProfessionalForm\ProfessionalFormController@get360degreeEvaluation')->name('professional-360degree-evaluation');
    Route::resource('professional-360evaluation', 'Modules\Professional360Evaluation\Professional360EvaluationController');

    Route::post('prof-360evaluation-asign', 'Modules\Professional360Evaluation\Professional360EvaluationController@professional360eEvuUserAssigne')->name('prof.360evaluation.asign');
    Route::get('prof-360asign-view/{id}', 'Modules\Professional360Evaluation\Professional360EvaluationController@professional360eEvuUserAssigneView')->name('prof.360asign.view');

    Route::get('prof-360asign-userlist/{id}', 'Modules\Professional360Evaluation\Professional360EvaluationController@professional360eEvuUserAssigneUserList')->name('prof.360asign.userlist');

    //18.3.20 status update
    Route::get('prof-360toools-status/{id}', 'Modules\Professional360Evaluation\Professional360EvaluationController@tools360StatusUpdate')->name('prof.tools360.status');

    //Route::post('dynamic_dependent/fetchcompetences', 'Modules\Professional360Evaluation\Professional360EvaluationController@fetchCompetencesAll')->name('dynamicdependent.fetchcompetences');

    //users

    Route::get('user-360evaluation/{id}', 'Modules\User360Evaluation\User360EvaluationController@getUser360Evaluation')->name('user.360evaluation');

    Route::post('user-360evaluation-post/', 'Modules\User360Evaluation\User360EvaluationController@getUser360EvaluationPost')->name('user.360evaluation.post');
    Route::get('user-360evaluation-view/{id}', 'Modules\User360Evaluation\User360EvaluationController@getUser360EvaluationView')->name('user.360evaluation.view');
    //for 360degree-evaluation test end

    //17.3.20
    //for evaluation360 test start
    Route::resource('prof-evaluation360', 'Modules\ProfEvaluation360\ProfEvaluation360Controller');
    Route::post('dynamic_dependent/fetchcompetences', 'Modules\ProfEvaluation360\ProfEvaluation360Controller@fetchCompetencesAll')->name('dynamicdependent.fetchcompetences');

    Route::get('user-evaluation360-assign-userlist/', 'Modules\ProfEvaluation360\ProfEvaluation360Controller@profEvu360AssigneUserList')->name('user.evaluation360.assign.userlist');
    //26.3.20 user start
    Route::get('user-evaluation360-list/{id}', 'Modules\ProfEvaluation360\ProfEvaluation360Controller@getUserEvaluation360QuesList')->name('user.evaluation360.quelist');
    Route::get('user-evaluation360-scorepost/', 'Modules\ProfEvaluation360\ProfEvaluation360Controller@userEvaluation360ScorePost')->name('user.evaluation360.score.post');
    Route::get('user-evaluation360-scorelisting/{id}', 'Modules\ProfEvaluation360\ProfEvaluation360Controller@userEvaluation360ScoreListing')->name('user.evaluation360.score.listing');

    //public evaluation360
    Route::get('user-public-valuation360/{id}/{type}', 'Modules\Professional\PublicProfileController@userpPublicEvaluation360')->name('pblc.evaluation');
    Route::get('user-evaluation360-publicscorepost/', 'Modules\Professional\PublicProfileController@userEvaluation360ScorePost')->name('user.evaluation360.public.score.post');
    //Route::get('public-profile/{slug}', 'Modules\Professional\PublicProfileController@index')->name('pblc.pst');

    //26.3.20 user end


    //for evaluation360 test start

    // Importing tools start

    Route::resource('professional-importing-tools', 'Modules\ImportingTools\ImportingToolsController');
    Route::post('professional-importing-tools-assigne', 'Modules\ImportingTools\ImportingToolsController@professionalImportingToolsAssigne')->name('professional.importing.assigne');

    Route::get('professional-importing-tools-view', 'Modules\ImportingTools\ImportingToolsController@professionalImportingToolsView')->name('professional.importing.assigne.view');

    Route::get('professional-importing-tools-update-status/{id}', 'Modules\ImportingTools\ImportingToolsController@updateStatus')->name('professional.importing.update.status');

    Route::get('professional-importing-tools-duplicate/{id}', 'Modules\ImportingTools\ImportingToolsController@duplicateImportedTools')->name('professional.importing.duplicate.tools');

    Route::get('prof-importing-tools-view-details/{id}', 'Modules\ImportingTools\ImportingToolsController@professionalImportingToolsViewDeatils')->name('prof.importing.assigne.view.details');

    Route::get('prof-view-alluser-feedback/{id}', 'Modules\ImportingTools\ImportingToolsController@viewAllUserFeedback')->name('prof.view.alluser.feedback');

    Route::post('prof-feedback-post-touser', 'Modules\ImportingTools\ImportingToolsController@postFeedbackTouser')->name('prof.feedback.post.touser');
    //17.3.20
    Route::get('professional-importedtool-status/{id}', 'Modules\ImportingTools\ImportingToolsController@importedStatusUpdate')->name('professional.importedtools.status');

    // for user 10.3.20
    Route::get('user-importing-tools-view/{id}', 'Modules\ImportingTools\ImportingToolsController@userImportingToolsView')->name('user.importing.assigne.view');
    Route::post('user-importing-tools-update/{id}', 'Modules\ImportingTools\ImportingToolsController@userImportingToolsUpdate')->name('user.importing.tools.update');
    Route::get('user-importing-tools-ans-show/{id}', 'Modules\ImportingTools\ImportingToolsController@userImportingToolsAnsShow')->name('user.importing.answer.show');
    Route::post('user-importing-tools-feedback', 'Modules\ImportingTools\ImportingToolsController@userImportingToolsFeedback')->name('user.importing.tools.feedback');

    // Importing tools end

    // SMART Goals start 3.2.20

    // professional
    Route::resource('prof-smartgoal-tools', 'Modules\ProfSmartGoal\ProfSmartGoalController');
    Route::post('prof-smartgoal-user-assigne', 'Modules\ProfSmartGoal\ProfSmartGoalController@profSmartGoalUserAssigne')->name('prof.smartgoal.user.assigne');
    Route::get('prof-smartgoal-user-assigne-view/{id}', 'Modules\ProfSmartGoal\ProfSmartGoalController@profSmartGoalUserAssigneView')->name('prof.smartgoal.user.assigne.view');

    //18.3.20 status update
    Route::get('prof-smartgoal-status/{id}', 'Modules\ProfSmartGoal\ProfSmartGoalController@profSmartGoalStatusUpdate')->name('prof.smartgoal.status');

    //users
    Route::get('user-smartgoal-tools-view/{id}', 'Modules\UserSmartGoal\UserSmartGoalController@userSmartGoalView')->name('user.smart.goal.view');
    Route::post('user-smartgoal-tools-post/{id}', 'Modules\UserSmartGoal\UserSmartGoalController@userSmartGoalPost')->name('user.smart.goal.post');
    // SMART Goals End


    //Content templates start 6.3.20

    Route::resource('user-content-temp', 'Modules\UserContentTemp\UserContentTempController');
    Route::post('prof-content-temp-user-assigne', 'Modules\UserContentTemp\UserContentTempController@profContentTempUserAssigne')->name('prof.content.temp.user.assigne');

    Route::get('prof-contenttemp-userassigne-list/{id}', 'Modules\UserContentTemp\UserContentTempController@profContentTempUserAssigneList')->name('prof.content.temp.user.assigne.list');

    Route::get('user-content-temp-view/{id}', 'Modules\UserContentTemp\UserContentTempController@userContentTempView')->name('user.content.temp.view');

    // Content toos status change 19.3.20
    Route::get('prof-contenttemp-status/{id}', 'Modules\UserContentTemp\UserContentTempController@contenttempStatusUpdate')->name('prof.contenttemp.status');
    //Content templates end 6.3.20

    //Contract Template start 6.3.20

    Route::resource('prof-contract-temp', 'Modules\ProfContractTemp\ProfContractTempController');
    Route::post('prof-contract-user-assigne', 'Modules\ProfContractTemp\ProfContractTempController@profContractTempUserAssigne')->name('prof.contract.temp.user.assigne');

    Route::get('prof-contract-assigne-list/{id}', 'Modules\ProfContractTemp\ProfContractTempController@profContractTempUserAssigneList')->name('prof.contract.temp.user.assigne.list');

    // Contract toos status change 19.3.20
    Route::get('prof-contract-status/{id}', 'Modules\ProfContractTemp\ProfContractTempController@contractStatusUpdate')->name('prof.contract.status');

    // contract for user start 14.3.20

    Route::get('user-contract-temp-view/{id}', 'Modules\ProfContractTemp\ProfContractTempController@userContractTempView')->name('user.contract.temp.view');
    Route::get('user-contract-temp-accept/{id}', 'Modules\ProfContractTemp\ProfContractTempController@userContractTempAcceptStatus')->name('user.contract.temp.update.status');

    // contract for user end 14.3.20

    //Contract Template end 6.3.20


    //Log book start 14.3.20

    Route::resource('prof-log-book', 'Modules\LogBook\LogBookController');
    Route::post('prof-contract-temp-user-assigne', 'Modules\LogBook\LogBookController@profLogbookUserAssigne')->name('prof.logbook.user.assigne');

    Route::get('prof-logbook-assigne-list/{id}', 'Modules\LogBook\LogBookController@profLogbookUserAssigneList')->name('prof.logbook.user.assigne.list');

    //Route::get('prof-logbook-assigne-list/{id}', 'Modules\LogBook\LogBookController@profLogbookUserAssigneList')->name('prof.logbook.user.assigne.list');

    //25.3.20
    Route::get('prof-logbook-assigne-userlist/', 'Modules\LogBook\LogBookController@profLogbookUserAssigneList')->name('prof.logbook.user.assigne.list');
    Route::get('prof-logbook-assigne-user-ansdate/{id}', 'Modules\LogBook\LogBookController@logbookUserAssigneAnsDate')->name('prof.logbook.user.assigne.ansdate');
    Route::get('prof-logbook-user-qus-ansdate/{id}', 'Modules\LogBook\LogBookController@logbookUserAssigneQuestionAns')->name('prof.logbook.user.assigne.queansdate');


    // logbook toos status change 19.3.20
    Route::get('prof-logbook-status/{id}', 'Modules\LogBook\LogBookController@logbookStatusUpdate')->name('prof.logbook.status');

    //user 16.3.20logbook
    Route::get('user-logbook-view/{id}', 'Modules\LogBook\LogBookController@userLogbookView')->name('user.logbook.view');
    Route::post('user-logbook-answ-post/', 'Modules\LogBook\LogBookController@userLogbookAnswerPost')->name('user.loogbook.answer.submit');
    Route::get('user-logbook-questionlist/{id}', 'Modules\LogBook\LogBookController@userLogbookQuestionList')->name('user.logbook.question.list');

    //25.3.20
    Route::get('user-logbook-all-ansdate/', 'Modules\LogBook\LogBookController@logbookUsergetAllAnsDate')->name('user.logbook.all.ansdate');
    Route::get('user-logbook-qus-ansdetails/{id}', 'Modules\LogBook\LogBookController@logbookUserQuestionAnsDetails')->name('user.logbook.queansdate.details');

    //Log book end 14.3.20
    Route::get('payments', 'Modules\Payment\PaymentController@myPayments')->name('my.payments');
    Route::post('booking-date', 'HomeController@setDate')->name('set.date');
    Route::post('make-payment', 'Modules\TestPayment\PaymentController@payment')->name('make-payment');

    Route::post('check-coupon', 'Modules\Booking\BookingController@checkCoupon')->name('check.coupon');
    Route::post('add-payment-method/{slug}', 'Modules\Booking\BookingController@addPaymentCoupon')->name('add.pamentmethod.coupon');
    Route::get('remove-payment-method/{slug}', 'Modules\Booking\BookingController@removePaymentMethod')->name('remove.pament.method');
    Route::post('upload-payment-document/{slug}', 'Modules\Booking\BookingController@uploadDocument')->name('upload.payment.document');



    Route::get('document', 'Modules\DocumentSignature\DocumentSignatureController@index')->name('document');
    Route::get('document/add', 'Modules\DocumentSignature\DocumentSignatureController@add')->name('document.add');
    Route::post('document/add/success', 'Modules\DocumentSignature\DocumentSignatureController@addSuccess')->name('document.add.success');
    Route::get('document/edit/{id}', 'Modules\DocumentSignature\DocumentSignatureController@edit')->name('document.edit');
    Route::post('document/edit/{id}/success', 'Modules\DocumentSignature\DocumentSignatureController@editSuccess')->name('document.edit.success');
    Route::get('document/delete/{id}', 'Modules\DocumentSignature\DocumentSignatureController@delete')->name('document.delete');
    Route::get('document/view/{id}', 'Modules\DocumentSignature\DocumentSignatureController@view')->name('document.view');
    Route::post('document/send', 'Modules\DocumentSignature\DocumentSignatureController@send')->name('document.send');
    Route::get('document/copy/{id}', 'Modules\DocumentSignature\DocumentSignatureController@copy')->name('document.copy');

    //Product Order
    Route::any('product-order/store', 'Modules\ProductOrder\ProductOrderController@storeProductOrder')->name('product.order.store');
    Route::any('product-order/store', 'Modules\ProductOrder\ProductOrderController@storeProductOrder')->name('product.order.store');
    Route::any('product-order-cancel/{token}/{id}', 'Modules\ProductOrder\ProductOrderController@productOrderCancel')->name('product.order.cancel');
    Route::any('product-order-cancel-reject/{token}/{id}', 'Modules\ProductOrder\ProductOrderController@requestProductCancelReject')->name('product.order.cancel.reject');
    Route::any('product-order-cancel-request/{token}/{id}', 'Modules\ProductOrder\ProductOrderController@requestProductCancel')->name('product.order.cancel.request');

    Route::get('product-order-sccess/{token}', 'Modules\ProductOrder\ProductOrderController@OrderSuccess')->name('product.order.store.success');
    Route::post('add-payment-method-product/{token}', 'Modules\ProductOrder\ProductOrderController@addPaymentCoupon')->name('product.order.add.pamentmethod');
    Route::post('complete-profile-user', 'Modules\ProductOrder\ProductOrderController@completeProfile')->name('product.cmplt.user.profile');
    Route::get('todos-os-produtos/{slug?}', 'Modules\Professional\ProfessionalProductsController@search')->name('all.product.search');
    Route::post('todos-os-produtos', 'Modules\Professional\ProfessionalProductsController@search')->name('all.product.search.filter');
    Route::post('search-cat-subcat','Modules\Professional\ProfessionalProductsController@searchCatSubcat')->name('search.cat.subcat');
    Route::post('search-cat-subcat-affiliate','Modules\Professional\ProfessionalProductsController@searchCatSubcatAffiliate')->name('search.cat.subcat.affiliate');

    Route::get('all-product-categories', 'Modules\Professional\ProfessionalProductsController@categorySearch')->name('all.product.category.search');
    Route::get('all-product-subcategories/{slug?}', 'Modules\Professional\ProfessionalProductsController@subCategorySearch')->name('all.product.subcategory.search');
    Route::post('upload-payment-document-product/{slug}', 'Modules\ProductOrder\ProductOrderController@uploadDocument')->name('upload.payment.document.product');

    Route::get('products-for-affiliation/{slug?}', 'Modules\Professional\ProfessionalProductsController@affiliateProducts')->name('affiliate.products.search');
    Route::post('products-for-affiliation', 'Modules\Professional\ProfessionalProductsController@affiliateProducts')->name('affiliate.products.search.filter');

    Route::get('my-affiliate-products', 'Modules\Professional\ProfessionalProductsController@userAffiliationLinksList')->name('user.affiliation.links.list');
    Route::get('my-affiliation-earnings', 'Modules\Professional\ProfessionalProductsController@userAffiliationEarnings')->name('user.affiliation.earnings');
    Route::post('affiliate-products-to-list', 'Modules\Professional\ProfessionalProductsController@affiliateProductsToList')->name('affiliate.products.to.list');
    Route::get('affiliate-products-remove-list/{id}', 'Modules\Professional\ProfessionalProductsController@affiliateProductsRemoveList')->name('affiliate.products.remove.list');

    Route::get('landing-page-programa-de-afiliados','Modules\Content\ContentController@affiliateLandingPage')->name('affiliate.landing.page');

    Route::post('show-profile-in-search', 'Modules\Professional\ProfessionalProfileController@showProfileInSearch')->name('search.show.profile');

    Route::post('add-to-cart', 'Modules\Cart\CartController@addToCart')->name('product.add.to.cart');
    Route::get('cart', 'Modules\Cart\CartController@index')->name('product.cart');
    Route::get('remove-cart/{id}', 'Modules\Cart\CartController@removeCart')->name('product.cart.remove');

    // for user wishlist

    Route::get('my-wishlist','Modules\User\UserProfileController@myWishlist')->name('user.wishlist');
    Route::get('card-delete', 'Modules\Professional\ProfessionalProfileController@deleteCreditCard')->name('credit.card.delete');

    Route::get('affiliate', 'Modules\Affiliate\AffiliateController@index')->name('request.affiliate.aprove');
    Route::post('affiliate', 'Modules\Affiliate\AffiliateController@index')->name('submit.affiliate');

    Route::get('video-affiliate', 'Modules\Affiliate\AffiliateController@videoAffiliate')->name('request.video.affiliate.aprove');
    Route::post('video-affiliate', 'Modules\Affiliate\AffiliateController@videoAffiliate')->name('submit.video.affiliate');
    Route::get('video-affiliate/{slug}', 'Modules\Affiliate\AffiliateController@videoAffiliateShareLink')->name('video.affiliate.share.link');
    Route::get('video-affiliate-program', 'Modules\Affiliate\AffiliateController@videoAffiliateEarning')->name('video.affiliate.earning.list');
    Route::get('video-affiliate-earning-list', 'Modules\Affiliate\AffiliateController@videoAffiliateEarningList')->name('video.affiliate.all.earning.list');
    Route::get('video-affiliate-payment-list', 'Modules\Affiliate\AffiliateController@videoAffiliatePaymentList')->name('video.affiliate.all.payment.list');

    Route::post('payment-create-stripe', 'Modules\Booking\BookingController@paymentCreate')->name('booking.payment.stripe.create');
    Route::post('payment-responce-stripe', 'Modules\Booking\BookingController@paymentSuccessHandlerStripe')->name('booking.payment.stripe');

    Route::post('product-payment-create-stripe', 'Modules\ProductOrder\ProductOrderController@paymentCreate')->name('product.payment.stripe.create');
    Route::post('product-payment-responce-stripe', 'Modules\ProductOrder\ProductOrderController@paymentSuccessHandlerStripe')->name('product.payment.stripe');

    // Route::get('payment-success/{id}', 'Modules\Booking\BookingController@paymentSuccessHandler')->name('booking.payment.success');

    // Route::get('paywithpaypal', array('as' => 'paywithpaypal','uses' => 'Modules\Payment\PaypalController@payWithPaypal'))->name('paypal.get');
    // Route::post('paypal', array('as' => 'paypal','uses' => 'Modules\Payment\PaypalController@postPaymentWithpaypal'))->name('paypal.post');
    // Route::get('paypal', array('as' => 'status','uses' => 'Modules\Payment\PaypalController@getPaymentStatus'))->name('paypal.status');

    // Route::get('paywithpaypal', 'Modules\Payment\PaypalController@payWithPaypal')->name('paywithpaypal');
    Route::post('paypal', 'Modules\ProductOrder\ProductOrderController@postPaymentWithpaypal')->name('paypal.post');
    Route::get('paypal/status', 'Modules\ProductOrder\ProductOrderController@getPaymentStatus')->name('paypal.status');
    Route::post('paypal/booking', 'Modules\Booking\BookingController@postPaymentWithpaypal')->name('paypal.booking.post');
    Route::get('paypal/booking/status', 'Modules\Booking\BookingController@getPaymentStatus')->name('paypal.booking.status');

    Route::get('product-payment-refund/{token}', 'Modules\ProductOrder\ProductOrderController@paymentRefundProduct')->name('product.payment.refund');

});


//Clear configurations:
Route::get('/config-clear', function () {
    $status = Artisan::call('config:clear');
    return '<h1>Configurations cleared</h1>';
});


//Clear configurations:
Route::get('/route-cache', function () {
    $status = Artisan::call('route:cache');
    return '<h1>Route cache cleared</h1>';
});

//Clear configurations:
Route::get('/route-clear', function () {
    $status = Artisan::call('route:clear');
    return '<h1>Route  cleared</h1>';
});

//Clear cache:
Route::get('/cache-clear', function () {
    $status = Artisan::call('cache:clear');
    return '<h1>Cache cleared</h1>';
});

//Clear configuration cache:
Route::get('/config-cache', function () {
    $status = Artisan::call('config:cache');
    return '<h1>Configurations cache cleared</h1>';
});

//optimize
Route::get('/optimize', function () {
    $status = Artisan::call('optimize');
    return '<h1>Optimized</h1>';
});

Route::get('/php-info', function () {
    // echo phpinfo();
    date_default_timezone_set("America/Sao_Paulo");
    dd(date("h:i a"));
});


// Corn time update

Route::get('daily-date-update', 'Modules\CronDateUpdate\CronDateUpdateController@dailyDateUpdate');

Route::get('weekly-date-update', 'Modules\CronDateUpdate\CronDateUpdateController@weeklyDateUpdate');

Route::get('user-bookingdata', 'Modules\CronDateUpdate\CronDateUpdateController@userBookingMail');
Route::get('professional-bookingdata', 'Modules\CronDateUpdate\CronDateUpdateController@professionalBookingMail');

Route::get('message-pusher','Modules\Pusher\PusherController@message')->name('message.pusher');
Route::post('send-ajax', 'Modules\Pusher\PusherController@sendajax')->name('send.ajax');
Route::post('add-user', 'Modules\Pusher\PusherController@adduser')->name('add.user');
Route::post('remove-user', 'Modules\Pusher\PusherController@removeuser')->name('remove.user');
Route::post('message-search', 'Modules\Pusher\PusherController@searchajax')->name('message.search');
Route::post('user-message', 'Modules\Pusher\PusherController@usermessage')->name('user.message');
Route::post('typing-ajax', 'Modules\Pusher\PusherController@typingajax')->name('typing.ajax');
Route::get('receive', 'Modules\Pusher\PusherController@sendApi')->name('receive');
Route::post('get-message-master', 'Modules\Pusher\PusherController@getmessagemaster')->name('get.message.master');
Route::post('close-chat', 'Modules\Pusher\PusherController@closeChat')->name('close.chat');


Route::get('chats', 'Modules\Pusher\PusherController@messagesPage')->name('chats');

Route::get('js-player/{url}', 'HomeController@jsPlayer')->name('js.player');
// Route::get('js-player', function () {
//     return view('JSplayer');
// });

