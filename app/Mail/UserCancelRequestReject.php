<?php

namespace App\Mail;

use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserCancelRequestReject extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $usermail    = null;
    public $professionalmail   = null;
    public $bookingdetails   = null;
    public $professionalData   = null;
    public $userData  = null;
    public function __construct($user_mail, $professional_mail, $bookingdata, $professionalData, $userData)
    {
        //
        $this->usermail             = $user_mail;
        $this->professionalmail     = $professional_mail;
        $this->bookingdetails     = $bookingdata;
        $this->professionalData = $professionalData;
        $this->userData = $userData;
    }

    /**
     * Parameters to replace
     *
     * @var array
     */
    private $params = [
        'name', 'link', 'amount', 'professionalname', 'product','order_id'
    ];


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $template = MailTemplate::find(34);
        $subject = $template->subject;
        $content = $template->content;
        $link = route('login');
        $replace = [
            'name' => @$this->userData->nick_name ? $this->userData->nick_name: @$this->userData->name,
            'link' => $link,
            'product' => $this->bookingdetails->product->title,
            'amount' => $this->bookingdetails->amount,
            'professionalname' => @$this->professionalData->nick_name ? @$this->professionalData->nick_name : @$this->professionalData->name,
            'order_id' =>@$this->bookingdetails->orderMaster->token_no
        ];
        foreach ($this->params as $param) {
            $subject = str_replace('__' . $param . '__', $replace[$param], $subject);
            $content = str_replace('__' . $param . '__', $replace[$param], $content);
        }
        return $this->view('mail.main_template.index')
            ->subject($subject)
            ->with(['content' => $content])
            ->to(@$this->usermail)
            ->from('support@netbhe.com.br');
    }
}
