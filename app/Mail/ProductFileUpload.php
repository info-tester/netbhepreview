<?php

namespace App\Mail;

use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProductFileUpload extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $userData   = null;
    public $orderData   = null;
    public $productDetails   = null;
    public $usermail    = null;
    public function __construct($userData, $orderData, $productDetails, $usermail)
    {
        //
        $this->usermail = $usermail;
        $this->orderData = $orderData;
        $this->userData = $userData;
        $this->productDetails = $productDetails;
    }

    /**
     * Parameters to replace
     *
     * @var array
     */
    private $params = [
        'name',  'product_title', 'order_date', 'amount',
    ];

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $template = MailTemplate::find(27);
        $subject = $template->subject;
        $content = $template->content;
        $link = route('login');
        $replace = [
            'name' => @$this->userData->nick_name ? @$this->userData->nick_name : @$this->userData->name,
            'product_title' => $this->productDetails->title,
            'order_date' => $this->orderData->order_date,
            'amount' => $this->orderData->amount,
        ];
        foreach ($this->params as $param) {
            $subject = str_replace('__' . $param . '__', $replace[$param], $subject);
            $content = str_replace('__' . $param . '__', $replace[$param], $content);
        }
        return $this->view('mail.main_template.index')
            ->subject($subject)
            ->with(['content' => $content])
            ->to(@$this->usermail)
            ->from('support@netbhe.com.br');
    }
}
