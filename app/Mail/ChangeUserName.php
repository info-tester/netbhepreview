<?php

namespace App\Mail;

use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ChangeUserName extends Mailable
{
    use Queueable, SerializesModels;

    public $user_data = [];

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data = [])
    {
        $this->user_data = $data;
    }

    /**
     * Parameters to replace
     *
     * @var array
     */
    private $params = [
        'name', 'email', 'phone'
    ];


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $template = MailTemplate::find(1);
        $subject = $template->subject;
        $content = $template->content;
        $replace = [
            'name' => $this->user_data['user']['name'], 'email' => $this->user_data['user']['new_email'], 'phone' => $this->user_data['user']['mobile']
        ];
        foreach ($this->params as $param) {
            str_replace('__' . $param . '__', $replace[$param], $subject);
            str_replace('__' . $param . '__', $replace[$param], $content);
        }
        $data['content'] = $content;
        return $this->view('mail.main_template.index', @$data)
            ->to($this->user_data['user']['email'])
            ->subject($subject)
            ->from('contato@netbhe.com.br', 'Netbhe.com.br');
    }
}
