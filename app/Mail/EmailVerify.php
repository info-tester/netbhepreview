<?php

namespace App\Mail;

use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailVerify extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $data    = [];
    public $vcode   = null;
    public function __construct($user = [], $code)
    {
        //
        $this->data     = $user;
        $this->vcode    = $code;
    }

    /**
     * Parameters to replace
     *
     * @var array
     */
    private $params = [
        'name', 'email', 'link'
    ];

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $template = MailTemplate::find(1);
        $subject = $template->subject;
        $content = $template->content;
        $link = route('check.email.verify', ["code" => $this->vcode]);
        $replace = [
            'name' => $this->data['name'], 'email' => $this->data['email'], 'link' => $link
        ];
        foreach ($this->params as $param) {
            $subject = str_replace('__' . $param . '__', $replace[$param], $subject);
            $content = str_replace('__' . $param . '__', $replace[$param], $content);
        }
        return $this->view('mail.main_template.index')
            ->subject($subject)
            ->with(['content' => $content])
            ->to(@$this->data['email'])
            ->from(env('FROM_EMAIL'));
    }
}
