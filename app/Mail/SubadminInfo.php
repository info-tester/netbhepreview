<?php

namespace App\Mail;

use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubadminInfo extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $var = [];
    public $is_new = null;
    public function __construct($request, $new = 'N')
    {
        //
        $this->var = $request;
        $this->is_new = $new;
    }

    /**
     * Parameters to replace
     *
     * @var array
     */
    private $params = [
        'name', 'email', 'password'
    ];

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if (@$this->is_new == 'Y') {
            $template = MailTemplate::find(11);
            $subject = $template->subject;
            $content = $template->content;
            $replace = [
                'name' => $this->var['name'],
                'email' => $this->var['email'],
                'password' => @$this->var['new_password'] ? $this->var['new_password'] : $this->var['password']
            ];
            foreach ($this->params as $param) {
                $subject = str_replace('__' . $param . '__', $replace[$param], $subject);
                $content = str_replace('__' . $param . '__', $replace[$param], $content);
            }
        } else {
            $template = MailTemplate::find(12);
            $subject = $template->subject;
            $content = $template->content;
            $replace = [
                'name' => $this->var['name'],
                'email' => $this->var['email'],
                'password' => @$this->var['new_password'] ? $this->var['new_password'] : $this->var['password']
            ];
            foreach ($this->params as $param) {
                $subject = str_replace('__' . $param . '__', $replace[$param], $subject);
                $content = str_replace('__' . $param . '__', $replace[$param], $content);
            }
        }
        return $this->view('mail.main_template.index')
            ->subject($subject)
            ->with(['content' => $content])
            ->to(@$this->var['email'])
            ->from("contato@netbhe.com.br");
    }
}
