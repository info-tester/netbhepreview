<?php

namespace App\Mail;

use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserBfAppointmentSt extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $usermail    = null;
    public $professionalmail   = null;
    public $bookingDetails   = null;
    public function __construct($user_mail, $professional_mail, $bookingdata)
    {
        //
        $this->usermail             = $user_mail;
        $this->professionalmail     = $professional_mail;
        $this->bookingDetails       = $bookingdata;
    }

    /**
     * Parameters to replace
     *
     * @var array
     */
    private $params = [
        'name', 'link', 'date', 'start', 'end', 'duration'
    ];

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $template = MailTemplate::find(15);
        $subject = $template->subject;
        $content = $template->content;
        $link = route('login');
        $replace = [
            'name' => @$this->bookingDetails->userDetails->nick_name ? @$this->bookingDetails->userDetails->nick_name : (@$this->bookingDetails->userDetails->name ?? ''),
            'link' => $link,
            'date' => date('d/m/Y', strtotime($this->bookingDetails->date)),
            'start' => $this->bookingDetails->start_time,
            'end' => $this->bookingDetails->end_time,
            'duration' => $this->bookingDetails->duration,
        ];
        foreach ($this->params as $param) {
            $subject = str_replace('__' . $param . '__', $replace[$param], $subject);
            $content = str_replace('__' . $param . '__', $replace[$param], $content);
        }
        return $this->view('mail.main_template.index')
            ->subject($subject)
            ->with(['content' => $content])
            ->to(@$this->user_mail)
            ->from('support@netbhe.com.br');
    }
}
