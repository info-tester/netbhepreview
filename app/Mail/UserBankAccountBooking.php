<?php

namespace App\Mail;

use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Booking;
use App\User;

class UserBankAccountBooking extends Mailable
{
    use Queueable, SerializesModels;

    public $user_data = [];
    public $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id,$email)
    {
        $this->id = $id;
        $this->email = $email;
    }

    /**
     * Parameters to replace
     *
     * @var array
     */
    private $params = [
        'token', 'link','name'
    ];


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $template = MailTemplate::find(20);
        $subject = $template->subject;
        $content = $template->content;
        $booking = Booking::where('id', $this->id)->first();
        $user = User::where('id', $booking->user_id)->first();
        $link = route('booking.sces', ['id' => $booking->token_no]);
        $replace = [
            'token' => $booking->token_no,
            'link' => $link,
            'name'=> @$user->nick_name ? @$user->nick_name: @$user->name
        ];
        foreach ($this->params as $param) {
            $subject = str_replace('__' . $param . '__', $replace[$param], $subject);
            $content = str_replace('__' . $param . '__', $replace[$param], $content);
        }
        $data['content'] = $content;
        return $this->view('mail.main_template.index', @$data)
            ->to($this->email)
            ->subject($subject)
            ->from(env('FROM_EMAIL'), 'Netbhe.com.br');
    }
}
