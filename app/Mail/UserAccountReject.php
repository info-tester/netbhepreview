<?php

namespace App\Mail;

use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserAccountReject extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $request;
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Parameters to replace
     *
     * @var array
     */
    private $params = [
        'name',  'link'
    ];

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $template = MailTemplate::find(14);
        $subject = $template->subject;
        $content = $template->content;
        $link = url('/');
        $replace = [
            'name' => @$this->request->nick_name ? @$this->request->nick_name : @$this->request->name,
            'link' => $link
        ];
        foreach ($this->params as $param) {
            $subject = str_replace('__' . $param . '__', $replace[$param], $subject);
            $content = str_replace('__' . $param . '__', $replace[$param], $content);
        }
        return $this->view('mail.main_template.index')
            ->subject($subject)
            ->with(['content' => $content])
            ->to($this->request->email)
            // ->bcc('infoware.solutions4@gmail.com')
            ->from('contato@netbhe.com.br', 'NetBhe');
    }
}
