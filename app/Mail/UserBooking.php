<?php

namespace App\Mail;

use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserBooking extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $usermail    = null;
    public $professionalmail   = null;
    public $bookingdetails   = null;
    public $userData   = null;
    public $professionalData   = null;
    public function __construct($user_mail, $professional_mail, $bookingdata, $userData, $professionalData)
    {
        //
        $this->usermail             = $user_mail;
        $this->professionalmail     = $professional_mail;
        $this->bookingdetails     = $bookingdata;
        $this->userData = $userData;
        $this->professionalData = $professionalData;
    }

    /**
     * Parameters to replace
     *
     * @var array
     */
    private $params = [
        'name', 'date', 'link', 'start', 'end', 'duration', 'amount', 'professionalname'
    ];

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $template = MailTemplate::find(16);
        $subject = $template->subject;
        $content = $template->content;
        $link = route('login');
        $replace = [
            'name' => @$this->userData->nick_name ? @$this->userData->nick_name : @$this->userData->name,
            // 'date' => date('d/m/Y', strtotime($this->bookingdetails->date)), toUserTimeZone(@$this->bookingdetails->date . ' ' . @$this->bookingdetails->start_time, 'd/m/Y', $this->userData->userTimeZone->timezone),
            'date' => toUserTimeZone(@$this->bookingdetails->date . ' ' . @$this->bookingdetails->start_time, 'd/m/Y', $this->userData->userTimeZone->timezone),
            'link' => $link,
            // 'start' => $this->bookingdetails->start_time,
            'start' => toUserTimeZone(@$this->bookingdetails->date . ' ' . $this->bookingdetails->start_time, 'H:i:s', $this->userData->userTimeZone->timezone),
            // 'end' => $this->bookingdetails->end_time,
            'end' => toUserTimeZone(@$this->bookingdetails->date . ' ' . $this->bookingdetails->end_time, 'H:i:s', $this->userData->userTimeZone->timezone),
            'duration' => $this->bookingdetails->duration,
            'amount' => $this->bookingdetails->amount,
            'professionalname' => @$this->professionalData->nick_name ? @$this->professionalData->nick_name : @$this->professionalData->name
        ];
        foreach ($this->params as $param) {
            $subject = str_replace('__' . $param . '__', $replace[$param], $subject);
            $content = str_replace('__' . $param . '__', $replace[$param], $content);
        }
        return $this->view('mail.main_template.index')
            ->subject($subject)
            ->with(['content' => $content])
            ->to(@$this->usermail)
            ->from('support@netbhe.com.br');
    }
}
