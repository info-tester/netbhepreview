<?php

namespace App\Mail;

use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProfessionalProductOrder extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $usermail    = null;
    public $professionalmail   = null;
    public $bookingdetails   = null;
    public $professionalData   = null;
    public $userData  = null;
    public function __construct($user_mail, $professional_mail, $bookingdata, $professionalData, $userData)
    {
        //
        $this->usermail             = $user_mail;
        $this->professionalmail     = $professional_mail;
        $this->bookingdetails     = $bookingdata;
        $this->professionalData = $professionalData;
        $this->userData = $userData;
    }

    /**
     * Parameters to replace
     *
     * @var array
     */
    private $params = [
        'name', 'link', 'amount', 'username', 'product'
    ];


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $template = MailTemplate::find(22);
        $subject = $template->subject;
        $content = $template->content;
        $link = route('login');
        $replace = [
            'name' => @$this->professionalData->nick_name ? @$this->professionalData->nick_name : @$this->professionalData->name,
            // 'date' => date('d/m/Y', strtotime($this->bookingdetails->date)),
            'link' => $link,
            'product' => $this->bookingdetails->product->title,
            // 'start' => $this->bookingdetails->start_time,
            // 'end' => $this->bookingdetails->end_time,
            // 'duration' => $this->bookingdetails->duration,
            'amount' => $this->bookingdetails->amount,
            'username' => @$this->userData->nick_name ? $this->userData->nick_name: @$this->userData->name
        ];
        foreach ($this->params as $param) {
            $subject = str_replace('__' . $param . '__', $replace[$param], $subject);
            $content = str_replace('__' . $param . '__', $replace[$param], $content);
        }
        return $this->view('mail.main_template.index')
            ->subject($subject)
            ->with(['content' => $content])
            ->to(@$this->professionalmail)
            ->from('support@netbhe.com.br');
    }
}
