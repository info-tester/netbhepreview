<?php

namespace App\Mail;

use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RescheduleApprove extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->data = $request;
    }

    private $data;

    /**
     * Parameters to replace
     *
     * @var array
     */
    private $params = [
        'name'
    ];

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $template = MailTemplate::find(9);
        $subject = $template->subject;
        $content = $template->content;
        $replace = [
            'name' => @$this->data->userDetails->nick_name ? @$this->data->userDetails->nick_name : @$this->data->userDetails->name
        ];
        foreach ($this->params as $param) {
            $subject = str_replace('__' . $param . '__', $replace[$param], $subject);
            $content = str_replace('__' . $param . '__', $replace[$param], $content);
        }
        return $this->view('mail.main_template.index')
            ->subject($subject)
            ->with(['content' => $content])
            ->to(@$this->data->userDetails->email)
            ->from('suporte@netbhe.com', 'Netbhe.com.br');
    }
}
