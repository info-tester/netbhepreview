<?php

namespace App\Mail;

use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForgetPassword extends Mailable
{
    use Queueable, SerializesModels;
    public $user = [];
    public $code = [];
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($usr, $cde)
    {
        //
        $this->user = $usr;
        $this->code = $cde;
    }

    /**
     * Parameters to replace
     *
     * @var array
     */
    private $params = [
        'name', 'link'
    ];

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $template = MailTemplate::find(3);
        $subject = $template->subject;
        $content = $template->content;
        $link = route('resetPass', ["code" => $this->code]);
        $replace = [
            'name' => @$this->user->nick_name ? @$this->user->nick_name : @$this->user->name,
            'link' => $link
        ];
        foreach ($this->params as $param) {
            $subject = str_replace('__' . $param . '__', $replace[$param], $subject);
            $content = str_replace('__' . $param . '__', $replace[$param], $content);
        }
        return $this->view('mail.main_template.index')
            ->subject($subject)
            ->with(['content' => $content])
            ->to($this->user->email)
            ->from(env('FROM_EMAIL'));
    }
}
