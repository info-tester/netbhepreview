<?php

namespace App\Mail;

use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FormAnswered extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $userMail    = null;
    public $professionalMail   = null;
    public function __construct($user_mail, $professional_mail)
    {
        //
        $this->userMail             = $user_mail;
        $this->professionalMail     = $professional_mail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $template = MailTemplate::find(5);
        $subject = $template->subject;
        $content = $template->content;
        return $this->view('mail.main_template.index')
            ->with(['content' => $content])
            ->subject($subject)
            ->to(@$this->userMail)
            ->from(env('FROM_EMAIL'), 'Netbhe.com.br');
    }
}
