<?php

namespace App\Mail;

use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\ProductOrder;

class BankAccountPaymentProduct extends Mailable
{
    use Queueable, SerializesModels;

    public $user_data = [];

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Parameters to replace
     *
     * @var array
     */
    private $params = [
        'token', 'link'
    ];


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $template = MailTemplate::find(24);
        $subject = $template->subject;
        $content = $template->content;
        $booking = ProductOrder::where('id', $this->id)->first();
        $link = route('admin.order.product.view', ['token' => $booking->token_no]);
        $replace = [
            'token' => $booking->token_no, 'link' => $link
        ];
        foreach ($this->params as $param) {
            $subject = str_replace('__' . $param . '__', $replace[$param], $subject);
            $content = str_replace('__' . $param . '__', $replace[$param], $content);
        }
        $data['content'] = $content;
        return $this->view('mail.main_template.index', @$data)
            ->to(env('ADMIN_MAIL'))
            ->subject($subject)
            ->from(env('FROM_EMAIL'), 'Netbhe.com.br');
    }
}
