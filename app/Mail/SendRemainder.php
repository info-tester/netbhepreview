<?php

namespace App\Mail;

use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendRemainder extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $data;
    public function __construct($user)
    {
        //
        $this->data = $user;
    }

    /**
     * Parameters to replace
     *
     * @var array
     */
    private $params = [
        'name'
    ];

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $template = MailTemplate::find(19);
        $subject = $template->subject;
        $content = $template->content;
        $replace = [
            'name' => @$this->data->nick_name ? @$this->data->nick_name : @$this->data->name,
        ];
        foreach ($this->params as $param) {
            $content = str_replace('__' . $param . '__', $replace[$param], $content);
        }
        return $this->view('mail.main_template.index')
            ->subject($subject)
            ->with(['content' => $content])
            ->to(@$this->data->email)
            ->from("contato@netbhe.com.br");
    }
}
