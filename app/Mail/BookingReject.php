<?php

namespace App\Mail;

use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;

class BookingReject extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $bookingdetails   = null;

    public function __construct($bookingdata)
    {
        //
        $this->bookingdetails     = $bookingdata;
    }

    /**
     * Parameters to replace
     *
     * @var array
     */
    private $params = [
        'name', 'date', 'start', 'end', 'duration', 'amount',
    ];


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $template = MailTemplate::find(21);
        $subject = $template->subject;
        $content = $template->content;
        $user = User::where('id', $this->bookingdetails->user_id)->first();
        $replace = [
            'name' => @$user->nick_name ? @$user->nick_name : @$user->name,
            // 'date' => date('d/m/Y', strtotime($this->bookingdetails->date)),
            // 'start' => $this->bookingdetails->start_time,
            // 'end' => $this->bookingdetails->end_time,
            'date' => toUserTimeZone(@$this->bookingdetails->date . ' ' . @$this->bookingdetails->start_time, 'd/m/Y', $user->userTimeZone->timezone),
            'start' => toUserTimeZone(@$this->bookingdetails->date . ' ' . $this->bookingdetails->start_time, 'H:i:s', $user->userTimeZone->timezone),
            'end' => toUserTimeZone(@$this->bookingdetails->date . ' ' . $this->bookingdetails->end_time, 'H:i:s', $user->userTimeZone->timezone),
            'duration' => $this->bookingdetails->duration,
            'amount' => $this->bookingdetails->amount,
        ];
        foreach ($this->params as $param) {
            $subject = str_replace('__' . $param . '__', $replace[$param], $subject);
            $content = str_replace('__' . $param . '__', $replace[$param], $content);
        }
        return $this->view('mail.main_template.index')
            ->subject($subject)
            ->with(['content' => $content])
            ->to(@$user->email)
            ->from('support@netbhe.com.br');
    }
}
