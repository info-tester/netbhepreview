<?php

namespace App\Mail;

use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\UserToTools;

class EvulationReviewUser extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $usermail    = null;
    public $professionalmail   = null;


    public function __construct($user_mail, $professional_mail)
    {
        //
        $this->usermail             = $user_mail;
        $this->professionalmail     = $professional_mail;
    }

    /**
     * Parameters to replace
     *
     * @var array
     */
    private $params = [
        'link'
    ];

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $template = MailTemplate::find(4);
        $subject = $template->subject;
        $content = $template->content;
        $link = route('pblc.evaluation', ['id' => encrypt(@$this->usermail['usertoolsid'] ?? ''), 'type' => "C"]);
        $replace = [
            'link' => $link
        ];
        foreach ($this->params as $param) {
            $subject = str_replace('__' . $param . '__', $replace[$param], $subject);
            $content = str_replace('__' . $param . '__', $replace[$param], $content);
        }
        return $this->view('mail.main_template.index')
            ->subject($subject)
            ->with(['content' => $content])
            ->to(@$this->usermail['email'])
            //->from(@$this->professionalmail);
            ->from('contato@netbhe.com.br', 'Netbhe.com.br');
    }
}
