<?php

namespace App\Mail;

use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProfessionalBfAppointmentSt extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $usermail    = null;
    public $professionalmail   = null;
    public $bookingdetails   = null;
    public function __construct($user_mail, $professional_mail,$bookingdata)
    {
        //
        $this->usermail             = $user_mail;
        $this->professionalmail     = $professional_mail;
        $this->bookingdetails     = $bookingdata;
    }

    /**
     * Parameters to replace
     *
     * @var array
     */
    private $params = [
        'name', 'link', 'date', 'start', 'end', 'duration'
    ];

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $template = MailTemplate::find(6);
        $subject = $template->subject;
        $content = $template->content;
        $link = route('login');
        $replace = [
            'name' => @$this->bookingdetails->profDetails->nick_name ? @$this->bookingdetails->profDetails->nick_name : (@$this->bookingdetails->profDetails->name ?? ''),
            'link' => $link,
            'date' => date('d/m/Y', strtotime($this->bookingDetails->date)),
            'start' => $this->bookingDetails->start_time,
            'end' => $this->bookingDetails->end_time,
            'duration' => $this->bookingDetails->duration,
        ];
        foreach ($this->params as $param) {
            $subject = str_replace('__' . $param . '__', $replace[$param], $subject);
            $content = str_replace('__' . $param . '__', $replace[$param], $content);
        }
        return $this->view('mail.user_booking.professional_before_appointment_start')
                    ->subject("Before appointment start")
                    ->with([
                        'data'  =>  @$this->bookingdetails,
                        //'code'  =>  @$this->vcode
                    ])
                    ->to(@$this->professionalmail)
                    ->from('support@netbhe.com.br');

    }
}
