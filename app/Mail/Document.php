<?php

namespace App\Mail;

use App\Models\DocumentSignature;
use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use PDF;


class Document extends Mailable
{
    use Queueable, SerializesModels;
    public $request = [];
    public $user = [];
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request,$user)
    {
        //
        $this->request = $request;
        $this->user = $user;
    }

    /**
     * Parameters to replace
     *
     * @var array
     */
    private $params = [
        'name'
    ];

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $template = MailTemplate::find(17);
        $subject = $template->subject;
        $content = $template->content;
        
        // $link = route('resetPass', ["code" => $this->code]);
        $replace = [
            'name' => @$this->user->nick_name ? @$this->user->nick_name : @$this->user->name
        ];
        foreach ($this->params as $param) {
            // $subject = str_replace('__' . $param . '__', $replace[$param], $subject);
            $content = str_replace('__' . $param . '__', $replace[$param], $content);
        }
        $documentTemplate = DocumentSignature::where('id', $this->request)->where('status', '!=', 'D')->first();
        $link = URL::to('storage/app/public/uploads/signature');
        $signature = '<img src="'.$link .'/' .@auth()->user()->signature.'" alt="" width="100" height="100" />';
        $template = $documentTemplate;
        $contentPdf = $template->description;
        $template['content'] = $contentPdf;
        // return $template;
        // dd($subject);
        $pdf = PDF::loadView('pdf.document_pdf', ['data' => $documentTemplate->description ,'image'=> $signature]);
        return $this->view('mail.main_template.index')
            ->subject(@$subject)
            ->with(['content' => @$content])
            ->to($this->user->email)
            ->attachData($pdf->output(), 'customer.pdf')
            ->from(env('FROM_EMAIL'));
    }
}
