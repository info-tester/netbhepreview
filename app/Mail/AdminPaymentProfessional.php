<?php

namespace App\Mail;

use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Booking;

class AdminPaymentProfessional extends Mailable
{
    use Queueable, SerializesModels;

    public $user_data = [];

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($profData,$note, $paymentAmount)
    {
        $this->profData = $profData;
        $this->note = $note;
        $this->paymentAmount = $paymentAmount;
    }

    /**
     * Parameters to replace
     *
     * @var array
     */
    private $params = [
        'note', 'name','amount'
    ];


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $template = MailTemplate::find(28);
        $subject = $template->subject;
        $content = $template->content;
        $replace = [
            'note' => $this->note,
            'name' => @$this->profData->nick_name ? @$this->profData->nick_name : @$this->profData->name,
            'amount'=> $this->paymentAmount->professional_amount
        ];
        foreach ($this->params as $param) {
            $subject = str_replace('__' . $param . '__', $replace[$param], $subject);
            $content = str_replace('__' . $param . '__', $replace[$param], $content);
        }
        $data['content'] = $content;
        return $this->view('mail.main_template.index', @$data)
            ->to($this->profData->email)
            ->subject($subject)
            ->from(env('FROM_EMAIL'), 'Netbhe.com.br');
    }
}
