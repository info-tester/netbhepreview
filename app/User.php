<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * method: userDetails.
     * relation : relation with user_to_category table on user_id.
     * author: Abhisek.
     */

    public function userDetails(){
    	return $this->hasMany('App\Models\UserToCategory', 'user_id', 'id');
    }

    /**
     * method: userExperience.
     * relation : relation with professional_to_experience table on user_id.
     * author: Abhisek.
     */

    public function userExperience(){
        return $this->hasMany('App\Models\ProfessionalToExperience', 'user_id', 'id');
    }

    /**
     * method: userQualification.
     * relation : relation with professional_to_experience table on user_id.
     * author: Abhisek.
     */

    public function userQualification(){
        return $this->hasMany('App\Models\ProfessionalToQualification', 'user_id', 'id');
    }

    /**
     * method: userAvailiability.
     * relation : relation with user_to_availiability table on user_id.
     * author: Abhisek.
     */

    public function userAvailiability(){
        return $this->hasMany('App\Models\UserAvailability', 'user_id', 'id');
    }

    /**
     * method: userLanguage.
     * relation : relation with language table on user_id.
     * author: Abhisek.
     */

    public function userLanguage(){
        return $this->hasOne('App\Models\Language', 'id', 'language_id');
    }

    /**
     * method: user_to_language.
     * relation : relation with language table on user_id.
     * author: Abhisek.
     */

    public function userLang(){
        return $this->hasMany('App\Models\UserToLanguage', 'user_id', 'id');
    }

    /**
     * method: user_timezone.
     * relation : relation with timezone tables.
     * author: Abhisek.
     */

    public function userTimeZone(){
        return $this->hasOne('App\Models\Timezone', 'timezone_id', 'timezone_id');
    }

    /**
     * method: countryName.
     * relation : relation with country tables.
     * author: Abhisek.
     */

    public function countryName(){
        return $this->hasOne('App\Models\Country', 'id', 'country_id');
    }

    /**
     * method: stateName.
     * relation : relation with state tables.
     * author: Abhisek.
     */

    public function stateName(){
        return $this->hasOne('App\Models\State', 'id', 'state_id');
    }

    /**
     * method: userReview.
     * relation : relation with review tables.
     * author: Abhisek.
     */

    public function userReview(){
        return $this->hasMany('App\Models\Review', 'professional_id', 'id');
    }

    /**
     * method: msgConversation.
     * relation : relation with message_details tables.
     * author: Soumen.
     */

    public function msgConversation(){
        return $this->hasMany('App\Models\MessageDetail', 'sender_id', 'id');
    }

    /**
     * method: msgConversation.
     * relation : relation with message_details tables.
     * author: Soumen.
     */

    public function bankAccount(){
        return $this->hasOne('App\Models\UserToBankAccount', 'user_id', 'id');
    }

    /**
     * method: UserToCard.
     * relation : relation with user_to_credit_card table on user_id.
     * author: Abhisek.
     */

    public function userCard(){
        return $this->hasOne('App\Models\UserToCard', 'user_id', 'id');
    }

    /**
     * method: hasProfessionalBooking.
     * relation : relation with booking table on professional_id.
     * author: Abhisek.
     */

    public function hasProfessionalBooking(){
        return $this->hasOne('App\Models\Booking', 'professional_id', 'id');
    }

    /**
     * method: hasUserBooking.
     * relation : relation with booking table on professional_id.
     * author: Abhisek.
     */

    public function hasUserBooking(){
        return $this->hasOne('App\Models\Booking', 'user_id', 'id');
    }

    public function specializationName() {
        return $this->hasOne('App\Models\ProfessionalSpecialty', 'id', 'professional_specialty_id');
    }

    /**
     * method: getExperiences.
     * relation : relation with professional_experiences table
     * author: Sayantani.
     */
    public function getExperiences()
    {
        return $this->belongsToMany('App\Models\Experience', 'professional_experiences');
    }

    /**
     * method: get_call_durations.
     * relation : relation with call_duration table
     * author: Sayantani.
     */
    public function get_call_durations()
    {
        return $this->belongsToMany('App\Models\CallDuration','user_call_duration');
    }

    /**
     * method: getExperiences.
     * relation : relation with professional_experiences table
     * author: Sayantani.
     */
    public function getMyCourses()
    {
        return $this->hasMany('App\Models\Product', 'professional_id', 'id');
    }
    /**
     * method: userAvailiability.
     * relation : relation with user_to_availiability table on user_id.
     * author: Abhisek.
     */

    public function userToAvailability(){
        return $this->hasOne('App\Models\UserAvailability', 'user_id', 'id')->where('slot_start','>=',date('Y-m-d H:i:s'))->orderBy('slot_start');
    }

}
