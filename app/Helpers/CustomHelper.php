<?php

use App\Admin;
use App\Models\Booking;
use App\Models\Cart;
use App\Models\Timezone;
use App\Models\UserToCategory;
use App\Models\UserAvailability;
use App\Models\MessageDetail;
use App\Models\PusherMessageMaster;
use App\Models\FormMaster;
use App\Models\UserContentTemplate;
use App\Models\DocumentSignature;
use App\Models\Product;
use App\Models\ProductOrderDetails;
use App\Models\CertificateTemplateMaster;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\URL;

function pr1($data)
{
    echo "<pre>";
    print_r($data);
    echo "</pre>";
}

function getAdminToken()
{
    $admin = Admin::where('user_type', 'A')->first();

    return $admin->access_token;
}

function getYouTubeIdFromURL($url)
{
    $url_string = parse_url($url, PHP_URL_QUERY);
    parse_str($url_string, $args);
    return isset($args['v']) ? $args['v'] : false;
}

function competencesName()
{
    $competencesname = App\Models\CompetencesMaster::pluck('title', 'id');
    return $competencesname;
}

function competencesDesc()
{
    $competencesdesc = App\Models\CompetencesMaster::pluck('description', 'id');
    return $competencesdesc;
}

/**
 * Determines if a admin has access to certain menus
 *
 * @param int $id Menu id
 * @return bool
 */
function adminHasAccessToMenu(int $id = 0)
{
    if (!auth()->guard('admin')->check()) {
        return false;
    }
    $access = auth()->guard('admin')->user()->access()->get();
    $admin = auth()->guard('admin')->user();
    if ($admin->user_type != 'S') {
        return true;
    }
    $mainMenus = $access->pluck('parent_access_id')->toArray();
    $mainMenus = array_unique($mainMenus);
    $subMenus = $access->pluck('access_id')->toArray();
    if (in_array($id, $mainMenus)) {
        return true;
    }
    if (in_array($id, $subMenus)) {
        return true;
    }
    return false;
}

/**
 * @param float $rating
 */
function getStars($rating = 0): string
{
    $rating = $rating > 5 ? 5 : $rating;
    $rating = $rating < 0 ? 0 : $rating;
    $whole = floor($rating);
    $fraction = $rating - $whole;
    if ($fraction >= 0.5) {
        $whole += 1;
    }
    $str = '';
    foreach (range(1, 5) as $s) {
        if ($s <= $whole) {
            $str .= '<li><img src="' . URL::to('public/frontend/images/star.png') . '" alt=""></li>';
        } else {
            $str .= '<li><img src="' . URL::to('public/frontend/images/star1.png') . '" alt=""></li>';
        }
    }
    return $str;
}

function getUsedTimeSlotsForDate($professional_id, $date): array
{
    $get = Booking::where('professional_id', $professional_id)
        ->where('date', '>=', date('Y-m-d', strtotime('-1 day ' . $date)))
        ->where('date', '<=', date('Y-m-d', strtotime('+1 day ' . $date)))
        ->select('date', 'start_time')->where('order_status', '!=', 'R')
        ->get()->toArray();
    $ret = array_map(function ($a) {
        return $a['date'] . ' ' . $a['start_time'];
    }, $get);
    return $ret;
}
function getUsedTimeSlotsForAllDates($professional_id): array
{
    $get = Booking::where('professional_id', $professional_id)->where('order_status', '!=', 'R')->where('order_status', '!=', 'C')
        ->where('date', '>=', date('Y-m-d', strtotime('-1 day ' . now(env('TIMEZONE'))->toDateString())))->select('date', 'start_time')->get()->toArray();
    $ret = array_map(function ($a) {
        return $a['date'] . ' ' . $a['start_time'];
    }, $get);
    return $ret;
}
/**
 * @param string $dateTime must be in Y-m-d H:i:s
 * @param string $format the valid format that needs to be returned
 */
function toUserTime($dateTime, $format = 'Y-m-d H:i:s')
{
    try {
        $minDiff = now(@session('timezone') ? session('timezone') : 'UTC')->utcOffset();
        // dd(session('timezone'));
        return date($format, strtotime($minDiff . ' minutes', strtotime($dateTime)));
    } catch (Exception $e) {
        return '';
    }
}
/**
 * @param string $dateTime must be in Y-m-d H:i:s
 * @param string $format the valid format that needs to be returned
 */
function toUTCTime($dateTime, $format = 'Y-m-d H:i:s')
{
    try {
        $minDiff = now(@session('timezone') ? session('timezone') : 'UTC')->utcOffset() * -1;
        return date($format, strtotime($minDiff . ' minutes', strtotime($dateTime)));
    } catch (Exception $e) {
        return '';
    }
}

/**
 * @param string $date
 * @param Collection $slots
 *
 * @return array
 */
function getUserTimeSlotsForDate($date, $slots): array
{
    $slotArr = array();
    foreach ($slots as $slot) {
        $userSlotStartTime = strtotime(toUserTime($slot->slot_start, 'Y-m-d'));
        if ($userSlotStartTime == strtotime($date)) {
            array_push($slotArr, $slot->toArray());
        }
    }
    return $slotArr;
}
/**
 * @param string $dateTime must be in Y-m-d H:i:s
 * @param string $format the valid format that needs to be returned
 */
function toUserTimeZone($dateTime, $format = 'Y-m-d H:i:s', $timezone_id)
{
    try {
        $minDiff = now(@$timezone_id ? $timezone_id : 'UTC')->utcOffset();
        // dd(session('timezone'));
        return date($format, strtotime($minDiff . ' minutes', strtotime($dateTime)));
    } catch (Exception $e) {
        return '';
    }
}
/**
 * @param string $dateTime must be in Y-m-d H:i:s
 * @param string $format the valid format that needs to be returned
 */
function getAllCart()
{
    if (@auth()->user()->id) {
        Cart::where('user_id', auth()->user()->id)->where('professional_id', auth()->user()->id)->delete();
        $cart= Cart::where('user_id', auth()->user()->id)->get();
        $arr= array();
        foreach (@$cart as $cc){
            array_push($arr, $cc->product_id);
        }
        return $arr;
    }
    $cart=Cart::where('cart_session_id', session()->get('cart_session_id'))->get();
    $arr = array();
    foreach (@$cart as $cc) {
        array_push($arr, $cc->product_id);
    }
    return $arr;
}
// function getAllCartProduct()
// {
//     if (@auth()->user()->id) {
//         $cart= Cart::where('user_id', auth()->user()->id)->get();
//         $arr= array();
//         foreach (@$cart as $cc){
//             array_push($arr, $cc->product_id);
//         }
//         return $arr;
//     }
//     $cart = Cart::where('cart_session_id', session()->get('cart_session_id'))->get();
//     $arr = array();
//     foreach (@$cart as $cc) {
//         array_push($arr, $cc->product_id);
//     }
//     return $arr;
// }
function allowed_menus(){
    $user = Auth::user();
    // add product, add certificate

    // Menus For Products
    $products = Product::where('professional_id', @$user->id)->where('status', '!=', 'D')->count();
    if(@$user->sell == 'VS' && @$products == 0) $data['has_products'] = 'N';
    else $data['has_products'] = 'Y';

    $certificates = CertificateTemplateMaster::where('status','A')->where('professional_id', Auth::id())->count();
    if(@$user->sell == 'VS' && @$certificates == 0) $data['has_certificates'] = 'N';
    else $data['has_certificates'] = 'Y';

    $product_orders = ProductOrderDetails::where('professional_id', @$user->id)->count();
    if(@$user->sell == 'VS' && @$product_orders == 0) $data['has_product_order'] = 'N';
    else $data['has_product_order'] = 'Y';



    // Menus For Video Call
    $has_category = UserToCategory::where('user_id', $user->id)->count();
    if(@$user->sell == 'PS' && @$has_category == 0) $data['has_category'] = 'N';
    else $data['has_category'] = 'Y';

    $avl = UserAvailability::where('user_id', @$user->id)->orderBy('date', 'DESC')->count();
    if(@$user->sell == 'PS' && @$avl == 0) $data['has_availability'] = 'N';
    else $data['has_availability'] = 'Y';

    if(@$user->sell == 'PS') $data['show_free_session'] = 'N';
    else $data['show_free_session'] = 'Y';

    $myBookings = Booking::where('professional_id', @$user->id)->whereIn('order_status',['A','C'])->count();
    if(@$user->sell == 'PS' && @$myBookings == 0) $data['my_bookings'] = 'N';
    else $data['my_bookings'] = 'Y';

    $pastBookings = Booking::where('professional_id', @$user->id)->where('order_status','C')->count();
    if(@$user->sell == 'PS' && @$pastBookings == 0) $data['past_bookings'] = 'N';
    else $data['past_bookings'] = 'Y';

    $all_bookings = Booking::where(function($q) use ($user){
        $q = $q->where('professional_id', @$user->id)->orWhere('user_id', @$user->id);
    })->whereIn('order_status',['A','C'])->pluck('id')->toArray();
    $chats = PusherMessageMaster::whereIn('booking_id', $all_bookings)->count();
    if(@$user->sell == 'PS' && @$chats == 0) $data['chats'] = 'N';
    else $data['chats'] = 'Y';

    $messages = MessageDetail::where('sender_id', $user->id)->where('receiver_id', $user->id)->count();
    if(@$user->sell == 'PS' && @$messages == 0) $data['messages'] = 'N';
    else $data['messages'] = 'Y';

    // $form_tools = FormMaster::where('status', '!=', 'DELETED')->where('shown_in_proff', 'Y')->where('added_by', 'P')->where('added_by_id', @$user->id)->count();
    // if(@$user->sell == 'PS' && @$form_tools == 0) $data['coaching_tools'] = 'N';
    // else $data['coaching_tools'] = 'Y';
    if(@$user->sell == 'PS') $data['coaching_tools'] = 'N';
    else $data['coaching_tools'] = 'Y';

    // $content_temp = UserContentTemplate::where('status', '!=', 'DELETED')->where('shown_in_proff', 'Y')->where('added_by', 'P')->where('added_by_id', @$user->id)->count();
    // if(@$user->sell == 'PS' && @$content_temp == 0) $data['content_temp'] = 'N';
    // else $data['content_temp'] = 'Y';
    if(@$user->sell == 'PS') $data['content_temp'] = 'N';
    else $data['content_temp'] = 'Y';

    $documentTemplate = DocumentSignature::where(['created_by' => 'P', 'user_id'=> @$user->id])->whereIn('status' , ['A','I'])->count();
    if(@$user->sell == 'PS' && @$documentTemplate == 0) $data['document'] = 'N';
    else $data['document'] = 'Y';

    return (object)$data;
}



function adminAccessMenu()
{
    if (!auth()->guard('admin')->check()) {
        return false;
    }
    $access = auth()->guard('admin')->user()->access()->get();
    $admin = auth()->guard('admin')->user();
    if ($admin->user_type != 'S') {
        return true;
    }
    $mainMenus = $access->pluck('parent_access_id')->toArray();
    $mainMenus = array_unique($mainMenus);
    return $mainMenus;
}


function adminAccessSubMenu()
{
    if (!auth()->guard('admin')->check()) {
        return false;
    }
    $access = auth()->guard('admin')->user()->access()->get();
    $admin = auth()->guard('admin')->user();
    if ($admin->user_type != 'S') {
        return true;
    }
    $subMenus = $access->pluck('access_id')->toArray();
    return $subMenus;
}
