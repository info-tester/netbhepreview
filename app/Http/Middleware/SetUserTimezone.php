<?php

namespace App\Http\Middleware;

use App\Models\Timezone;
use Closure;
use Session;

class SetUserTimezone
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session()->has('timezone')) {
            date_default_timezone_set(session('timezone'));
        } else {
            date_default_timezone_set(env('TIMEZONE', 'Asia/Kolkata'));
        }
        if(Session::get('cart_session_id')==null){
            session(['cart_session_id' => str_random(20) . '-' . time()]);
        }
        return $next($request);
    }
}
