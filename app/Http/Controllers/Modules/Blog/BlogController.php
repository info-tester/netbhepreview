<?php

namespace App\Http\Controllers\Modules\Blog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\BlogCategory;
use Auth;
use validate;
use Intervention\Image\Facades\Image;

class BlogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /*
    * Method: index
    * Description: Blog listing with search
    * Author: Abhishek
    */
    public function index(Request $request)
    {
        if (Auth::user()->is_professional != "Y") {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        $blogs = Blog::with('blogCategoryName', 'postedBy')
            ->where('posted_by', Auth::id())
            ->orderBy('id', 'desc');
        $key = [];
        if ($request->all()) {
            if ($request->keyword) {
                $blogs = $blogs->where('title', 'like', '%' . $request->keyword . '%');
            }

            if ($request->cat_id) {
                $blogs = $blogs->whereHas('blogCategoryName', function ($query) use ($request) {
                    $query->where('id', $request->cat_id);
                });
            }

            if ($request->status) {
                $blogs = $blogs->where('status', $request->status);
            }
        }
        $key = $request->all();
        $blogs = $blogs->orderBy('id', 'desc')->get();
        $category = BlogCategory::where('status', 'A')
            ->orderBy('name', 'asc')
            ->get();
        // dd($blogs[0]->desc);
        return view('modules.blog.blogs')->with([
            'category'     => @$category,
            'blogs'     => @$blogs,
            'key'          => @$key
        ]);
    }

    /**
     * Method     : add
     * Description: For adding particular post.
     * Author     : Abhisek
     */

    public function add(Request $request)
    {
        if (Auth::user()->is_professional != "Y") {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        if ($request->all()) {
            $request->validate([
                'title'     =>  'required',
                'cat_id'    =>  'required',
                'desc'      =>  'required',
                'image'     =>  'required'
            ], [
                'title.required'    => 'O título da publicação é obrigatório.',
                'cat_id.required'    => 'O campo Categoria é obrigatório.',
                'desc.required'    => 'A descrição é obrigatória.',
                'image.required'    => 'O campo de imagem é obrigatório.'
            ]);
            if (@$request->image != null) {
                list($width, $height) = getimagesize($request->image);

                $filename = time() . '_' . $request->image->getClientOriginalName();
                $destinationPath = "storage/app/public/uploads/blog_image/original/";
                $destinationPathResized = "storage/app/public/uploads/blog_image/";
                $request->image->move($destinationPath, $filename);
                $new['image'] = $filename;
                if (file_exists($destinationPath . $filename)) {
                    copy($destinationPath . $filename, $destinationPathResized . $filename);
                } else {
                    session()->flash('error', 'Não foi possível carregar a imagem.');
                    return redirect()->back();
                }
                $image = Image::make($destinationPathResized . $filename);
                if ($width / $height >= 16 / 9) {
                    $image->resize(($width / $height) * 720, 720);
                } else {
                    $image->resize(1280, ($height / $width) * 1280);
                }
                $image->crop(1280, 720);
                $image->save($destinationPathResized . $filename);
            }
            // insert youtube link
            if (@$request->you_tube_video != null) {
                // insert youtube link
                $url = urldecode(rawurldecode($request->you_tube_video));
                preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);
                if (count($matches) == 0) {
                    return redirect()->back()->with('error', 'Link do youtube inválido.');
                }
                $new['you_tube_video_path'] = $matches[0];
                $new['you_tube_video'] = $matches[1];
            }
            $new['cat_id']      = $request->cat_id;
            $new['title']       = $request->title;
            $new['posted_by']   = @Auth::id();
            $new['desc']        = $request->desc;
            $new['post_date']   = date('Y-m-d');
            $blog = Blog::create($new);

            $slug = str_slug($blog->title . '-' . $blog->id);
            $find_blg = Blog::where("slug", $slug)->first();
            if ($find_blg != null) {
                $slug = $slug . uniqid();
            }
            $blog->slug = $slug;
            $blog->save();

            session()->flash('success', \Lang::get('client_site.blog_post_created_successfully'));
            return redirect()->back();
        }
        $category = BlogCategory::where('status', 'A')
            ->orderBy('name', 'asc')
            ->get();
        return view('modules.blog.add_blog')->with([
            'category'  => @$category
        ]);
    }

    /**
     * Method     : edit
     * Description: For editing particular post.
     * Author     : Abhisek
     */

    public function edit($id, Request $request)
    {
        if (Auth::user()->is_professional != "Y") {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        $post = Blog::where('id', $id)->first();
        if ($post == null) {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        if ($request->all()) {
            $request->validate([
                'title'     =>  'required',
                'cat_id'    =>  'required',
                'desc'      =>  'required'
            ]);
            if (@$request->image != null) {

                list($width, $height) = getimagesize($request->image);

                $filename = time() . '_' . $request->image->getClientOriginalName();
                $destinationPath = "storage/app/public/uploads/blog_image/original/";
                $destinationPathResized = "storage/app/public/uploads/blog_image/";
                $request->image->move($destinationPath, $filename);
                $new['image'] = $filename;
                if (file_exists($destinationPath . $filename)) {
                    copy($destinationPath . $filename, $destinationPathResized . $filename);
                } else {
                    session()->flash('error', 'Não foi possível carregar a imagem.');
                    return redirect()->back();
                }
                $image = Image::make($destinationPathResized . $filename);
                if ($width / $height >= 16 / 9) {
                    $image->resize(($width / $height) * 720, 720);
                } else {
                    $image->resize(1280, ($height / $width) * 1280);
                }
                $image->crop(1280, 720);
                $image->save($destinationPathResized . $filename);

                if (@$post->image) {
                    if (file_exists($destinationPath . $post->image)) {
                        unlink($destinationPath . $post->image);
                    }
                }
                if (@$post->image) {
                    if (file_exists($destinationPathResized . $post->image)) {
                        unlink($destinationPathResized . $post->image);
                    }
                }
            }
            // insert youtube link
            if (@$request->you_tube_video != null) {
                // insert youtube link
                $url = urldecode(rawurldecode($request->you_tube_video));
                preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);
                if (count($matches) == 0) {
                    return redirect()->back()->with('error', 'Link do youtube inválido.');
                }
                $new['you_tube_video_path'] = $matches[0];
                $new['you_tube_video'] = $matches[1];
            }
            $new['cat_id']      = $request->cat_id;
            $new['title']       = $request->title;
            $new['posted_by']   = @Auth::id();
            $new['desc']        = $request->desc;
            $new['post_date']   = date('Y-m-d');
            $new['status']      = 'I';
            $blog = Blog::where('id', $post->id)->update($new);

            $slug = str_slug($post->title . '-' . $post->id);
            $find_blg = Blog::where("slug", $slug)->first();
            if ($find_blg != null) {
                $slug = $slug . uniqid();
            }
            $post->slug = $slug;
            $post->save();

            session()->flash('success', \Lang::get('client_site.blog_post_created_successfully'));
            return redirect()->back();
        }
        $category = BlogCategory::where('status', 'A')
            ->orderBy('name', 'asc')
            ->get();

        return view('modules.blog.edit_blog')->with([
            'blg'      => @$post,
            'category'  => @$category
        ]);
    }

    /**
     * Method     : deelete
     * Description: For removing a particular post.
     * Author     : Abhisek
     */

    public function delete($id)
    {
        if (Auth::user()->is_professional != "Y") {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        $blog = Blog::where([
            'id'        =>  $id,
            'posted_by' =>  @Auth::id()
        ])->first();
        if ($blog == null) {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        $blog = $blog->load('blogComments');

        if (count($blog->blogComments) > 0) {
            session()->flash('error', \Lang::get('client_site.this_post_can'));
            return redirect()->back();
        } else {
            $destinationPath = "storage/app/public/uploads/blog_image/";
            if (file_exists($destinationPath . @$blog->image)) {
                unlink($destinationPath . @$blog->image);
            }
            $isRemoved = Blog::where('id', $id)->delete();
            session()->flash('success', \Lang::get('client_site.blog_post_successfully_removed'));
            return redirect()->back();
        }
    }

    /**
     * Method     : imgUpload
     * Description: For insert and edit article image.
     * Author     : Abhisek
     */
    public function imgUpload(Request $request)
    {
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        $doc_name           = $request['file']->getClientOriginalName();
        $explode_doc_name   = explode('.', $doc_name);
        $extension          = end($explode_doc_name);
        $doc_name           = time() . str_random(6) . '.' . $extension;
        $newFilePath        = "storage/app/public/uploads/content_image/";
        $request['file']->move($newFilePath, $doc_name);
        $jsn['status'] = 'SUCCESS';
        $jsn['location'] = url('/') . '/storage/app/public/uploads/content_image/' . $doc_name;
        return response()->json($jsn);
    }

    /**
     * Method     : view
     * Description: For viewing particular post.
     * Author     : Abhisek
     */

    public function view($id)
    {
        if (Auth::user()->is_professional != "Y") {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }

        $blog = Blog::where([
            'id'        =>  $id,
            'posted_by' =>  @Auth::id()
        ])->first();

        if ($blog == null) {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }

        $blog = $blog->load('blogComments', 'blogCategoryName', 'postedBy');

        return view('modules.blog.view_blog')->with([
            'blog'  =>  @$blog
        ]);
    }
}
