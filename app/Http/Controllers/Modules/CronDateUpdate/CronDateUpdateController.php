<?php

namespace App\Http\Controllers\Modules\CronDateUpdate;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserToTools;
use App\Models\Booking;
use App\User;
use Mail;
use App\Mail\ReminderMail;
use App\Mail\ProfessionalBfAppointmentSt;
use App\Mail\UserBfAppointmentSt;

class CronDateUpdateController extends Controller
{

    public function dailyDateUpdate() {
    	$nowtime = date('Y-m-d H:i:s');
    	$all_daily_user_data = UserToTools::with('getprofessionalUserData')->where('reminder_type','daily')->get();

		if($all_daily_user_data){

	    	foreach ($all_daily_user_data as $value) {

	    		$professional = User::where('id',$value->professional_id)->first();
        		$user = User::where('id',$value->user_id)->first();

		        $usermail = $user->email;
		        $professionalmail = $professional->email;

	    		UserToTools::where('id', $value->id)->update(['reminder_date' => $nowtime]);
	    		Mail::queue(new ReminderMail($usermail,$professionalmail));
	    	}
	    }
		//mail('infoware.solutions5@gmail.com', 'nethbhe test', 'Test Mail');
    }

    public function weeklyDateUpdate() {

    	$nowtime = date('Y-m-d H:i:s');
    	$all_weekly_user_data = UserToTools::with('getprofessionalUserData')->where('reminder_type','weekly')->get();

    	if($all_weekly_user_data){

	    	foreach ($all_weekly_user_data as $value) {

	    		$professional = User::where('id',$value->professional_id)->first();
        		$user = User::where('id',$value->user_id)->first();

		        $usermail = $user->email;
		        $professionalmail = $professional->email;

	    		UserToTools::where('id', $value->id)->update(['reminder_date' => $nowtime]);
	    		Mail::send(new ProfessionalBfAppointmentSt($usermail,$professionalmail));
	    	}
	    }

		//mail('infoware.solutions5@gmail.com', 'nethbhe test', 'Test Mail');
    }

    // befour start class
    public function userBookingMail() {
    	$nowtime = date('Y-m-d H:i:s');
    	$todaudate = date('d-m-Y');


    	$all_daily_user_data = Booking::with(['profDetails','userDetails'])->where('date',$todaudate)->get();
		//pr1($all_daily_user_data->toArray());
    	//die();





		if($all_daily_user_data){

	    	foreach ($all_daily_user_data as $value) {

	    		$date = $value->start_time;

		    	//$date = date('h:i:s');
				$newdate = strtotime ( '-2 hour' , strtotime ( $date ) ) ;
				$newdate = date ('H:i:s' , $newdate );
				// pr1($newdate);
				// pr1($date);
				// die();


	    		$professional = User::where('id',$value->professional_id)->first();
        		$user = User::where('id',$value->user_id)->first();

		        $usermail = $user->email;
		        $professionalmail = $professional->email;

		        $bookingdata = $value;
	    		//UserToTools::where('id', $value->id)->update(['reminder_date' => $nowtime]);
	    		Mail::send(new UserBfAppointmentSt($usermail,$professionalmail ,$bookingdata));
	    	}
	    }
		//mail('infoware.solutions5@gmail.com', 'nethbhe test', 'Test Mail');
    }


    public function professionalBookingMail() {
    	$nowtime = date('Y-m-d H:i:s');
    	$todaudate = date('d-m-Y');


    	$all_daily_user_data = Booking::with(['profDetails','userDetails'])->where('date',$todaudate)->get();
		//pr1($all_daily_user_data->toArray());
    	//die();

		if($all_daily_user_data){

	    	foreach ($all_daily_user_data as $value) {

	    		$professional = User::where('id',$value->professional_id)->first();
        		$user = User::where('id',$value->user_id)->first();

		        $usermail = $user->email;
		        $professionalmail = $professional->email;

		        $bookingdata = $value;
	    		//UserToTools::where('id', $value->id)->update(['reminder_date' => $nowtime]);
	    		Mail::send(new ProfessionalBfAppointmentSt($usermail,$professionalmail ,$bookingdata));
	    	}
	    }
		//mail('infoware.solutions5@gmail.com', 'nethbhe test', 'Test Mail');
    }



    /*
    * Route: top-user-cron
    * Method: topUser
    * Description: Astrologer top user cron
    */
  //   public function topUser() {
  //   	$astrologers = User::where([
		// 		    		'user_type' => 'A',
		// 		    		'status' => 'A'
		// 		    	])
  //   					->where('top_user', '!=', 1)
		// 		    	->get();

		// // update astrologer with random value
		// if(sizeOf($astrologers) > 0) {
		// 	foreach ($astrologers as $val) {
		// 		User::where(['id' => $val->id])->update(['top_user' => rand(101, 99999999)]);
		// 	}
		// }
  //   }


}
