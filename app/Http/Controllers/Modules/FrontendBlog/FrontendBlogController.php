<?php

namespace App\Http\Controllers\Modules\FrontendBlog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\BlogCategory;
use App\Models\BlogComments;
use App\Models\BlogToContact;
use App\Models\Content;
use App\User;
use Auth;
use validate;

class FrontendBlogController extends Controller
{
    public function index($slug = null, Request $request)
    {

        $blogs = Blog::with('blogCategoryName', 'adminPostedBy', 'postedBy', 'blogComments')->where('status', 'A');
        $popularblogs = Blog::with('blogCategoryName', 'postedBy', 'adminPostedBy', 'blogComments')->where('status', 'A')->take(3)->orderBy('view_count', 'desc')->get();

        //pr1($popularblogs);
        //die();

        if (@$request->all() || $slug != null) {
            if (@$request->keyword) {
                $blogs = $blogs->where('title', 'like', '%' . $request->keyword . '%');
            }

            if (@$slug) {

                $blogs = $blogs->whereHas('blogCategoryName', function ($query) use ($slug) {
                    $query->where('slug', $slug);
                });
                $key['slug'] = $slug;
            }

            if (@$request->status) {
                $blogs = $blogs->whereIn('status', [$request->status]);
            }
            // $key = $request->all();
        }

        $blogs = $blogs->orderBy('id', 'desc')
            // ->get();
            ->paginate(10);
        $category = BlogCategory::where('status', 'A')
            ->orderBy('name', 'asc')
            ->get();

        $user = User::where([
            'is_professional' => 'Y',
            'status' =>    'A',
            'is_approved' => 'Y',
            'profile_active' =>  'Y',
        ]);
        if (Auth::id()) {
            $user = $user->where('id', '!=', Auth::id());
        }
        $user = $user->inRandomOrder()->limit(10)->get();

        $userIds = $user->pluck('id');
        $userOther = User::where([
            'is_professional' => 'Y',
            'is_approved' => 'Y',
            'status' => 'A',
            'profile_active' => 'Y',
        ]);
        if (Auth::id()) {
            $userOther = $userOther->where('id', '!=', Auth::id());
        }
        $userOther = $userOther->whereNotIn('id', $userIds)->inRandomOrder()->limit(10)->get();
        $nlContent = Content::where('id', 10)->first();
        return view('modules.frontendblog.frontendblogs')->with([
            'category' => @$category,
            'blogs' => @$blogs,
            'user' => @$user,
            'userOther' => @$userOther,
            'key' => @$key,
            'nlContent' => @$nlContent,
            'popularblogs' => @$popularblogs
        ]);
    }

    public function details($slug)
    {
        $blog = Blog::where('slug', $slug)->where('status', 'A')->first();
        //pr1($blog);
        //die();
        Blog::where('id', @$blog->id)->update(['view_count' => @$blog->view_count + 1]);


        $popularblogs = Blog::with('blogCategoryName', 'adminPostedBy','postedBy', 'blogComments')->where('status', 'A')->take(3)->orderBy('view_count', 'desc')->get();

        if ($blog == null) {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        $blog = $blog->load('blogCategoryName', 'adminPostedBy' ,'postedBy', 'blogComments');
        $user = User::where([
            'is_professional' => 'Y',
            'is_approved' => 'Y',
            'status' => 'A',
            'profile_active' => 'Y',
        ]);
        if (Auth::id()) {
            $user = $user->where('id', '!=', Auth::id());
        }
        $user = $user->inRandomOrder()->limit(10)->get();
        $userIds = $user->pluck('id');
        $userOther = User::where([
            'is_professional' => 'Y',
            'is_approved' => 'Y',
            'status' => 'A',
            'profile_active' => 'Y',
        ]);
        if (Auth::id()) {
            $userOther = $userOther->where('id', '!=', Auth::id());
        }
        $userOther = $userOther->whereNotIn('id', $userIds)->inRandomOrder()->limit(10)->get();
        $category = BlogCategory::where('status', 'A')
            ->orderBy('name', 'asc')
            ->get();
        $nlContent = Content::where('id', 10)->first();
        return view('modules.frontendblog.frontendblog_details')->with([
            'category' => @$category,
            'blog' => @$blog,
            'popularblogs' => @$popularblogs,
            'user' => @$user,
            'userOther' => @$userOther,
            'nlContent' => @$nlContent,
        ]);
    }

    public function comments(Request $request)
    {
        if ($request->all()) {
            $request->validate([
                'name'      =>  'required',
                'email'     =>  'required|email',
                'msg'       =>  'required'
            ]);
            $isCreated = BlogComments::create([
                'blog_id'   =>  $request->blog_id,
                'name'      =>  $request->name,
                'email'     =>  $request->email,
                'msg'       =>  $request->msg
            ]);

            if ($isCreated) {
                session()->flash('success', \Lang::get('client_site.comment_successfully_published'));
                return redirect()->back();
            } else {
                session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
                return redirect()->back();
            }
        }
    }
    public function BlogContact(Request $request)
    {
        if ($request->all()) {
            $request->validate([
                'name'      =>  'required',
                'email'     =>  'required|email',
            ]);
            $isCreated = BlogToContact::create([
                'blog_id'   =>  $request->blog_id,
                'name'      =>  $request->name,
                'email'     =>  $request->email,
            ]);

            if ($isCreated) {
                session()->flash('success', \Lang::get('client_site.contact_form_submitted'));
                return redirect()->back();
            } else {
                session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
                return redirect()->back();
            }
        }
    }
}
