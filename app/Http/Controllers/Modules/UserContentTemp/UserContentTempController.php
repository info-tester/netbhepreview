<?php

namespace App\Http\Controllers\Modules\UserContentTemp;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\UserContentTemplate;
use validate;
use App\User;
use Auth;
use Mail;
use DB;
use App\Models\Booking;
use App\Models\UserToTools;



class UserContentTempController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $data = array();
        $data['title'] = 'Manage Content templates';

        $nowtime = date('Y-m-d H:i:s');
        $tendaybef = date('Y-m-d H:i:s', strtotime("-3 days"));

        // $data['all_paid_users'] = Booking::select('user_id')->distinct()->with('userDetails')->where('professional_id',Auth::id())->where('payment_status','P')->get();

        $data['all_paid_users'] = Booking::select('user_id')->distinct()->with('userDetails')->where('professional_id', Auth::id())->where('payment_status', 'P')->whereBetween('created_at', [$tendaybef, $nowtime])->get();

        //$data['all_content_template'] = UserContentTemplate::/*where('status','ACTIVE')->*/orderBy('id','DESC')->get();

        $user_id = Auth::id();
        $admin_id = '1';

        $data['all_content_template'] = UserContentTemplate::where('shown_in_proff', 'Y')->where(function($q) use ($user_id, $admin_id) {
            $q = $q->where('status', '!=', 'DELETED')
                    ->where(['added_by' => 'P','added_by_id' => $user_id ])
                    ->orWhere(function($qq) use ($user_id, $admin_id) {
                        $qq = $qq->where(['added_by' => 'A','added_by_id' => $admin_id ])
                                ->whereHas('specialities', function( $query ) use ( $user_id ){
                                    $query = $query->where('professional_specialty_id', Auth::user()->professional_specialty_id);
                                });
                    });
            })
            // ->whereRaw(DB::raw("((added_by = 'P' AND added_by_id = '".$user_id."') OR (added_by = 'A' AND added_by_id = '".$admin_id."')) AND status != 'DELETED'"))
            ->orderBy('id', 'DESC')
            ->get();
            // dd($user_id);
            // dd($data['all_content_template']);

        // dd(UserContentTemplate::where('id', 12)->first()->speciality());
        return view('modules.user_content_temp.index')->with($data);
    }

    /*
    @method         => create
    @description    => Form create in form_master table
    @author         => munmun
    @date           => 03/03/2020
    */
    public function create(Request $request)
    {

        return view('modules.user_content_temp.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */




    public function store(Request $request)
    {

        //pr1($request->all());
        //die();
        $nowtime = date("Y-m-d H:i:s", time());

        $request->validate([
            'cont_subject'         => 'required',
            'cont_description'   => 'required',
        ]);



        $content_temp = new UserContentTemplate;

        $content_temp->cont_subject         = $request->cont_subject;
        $content_temp->cont_description     = $request->cont_description;
        $content_temp->cont_category        = $request->cont_category;
        //$content_temp->cont_video           = @$youtube_videoid;

        $content_temp->status               = 'ACTIVE';
        $content_temp->added_by             = 'P';
        $content_temp->added_by_id          = Auth::id();

        if ($request->cont_category == "video") {
            if ($request->cont_cat_video_img_audio) {
                $youtube_videoid = getYouTubeIdFromURL($request->cont_cat_video_img_audio);
                $content_temp->cont_cat_video_img_audio = $youtube_videoid;
            }
        }
        if ($request->cont_category == "image") {
            if (@$request->cont_cat_video_img_audio) {
                $attachment = time() . '_' . $request->cont_cat_video_img_audio->getClientOriginalName();
                $destinationPath = "storage/app/public/uploads/user_template_content/";
                $request->cont_cat_video_img_audio->move($destinationPath, $attachment);

                $content_temp->cont_cat_video_img_audio = $attachment;
            }
        }
        if ($request->cont_category == "audio") {
            if (@$request->cont_cat_audio) {
                $attachment = time() . '_' . $request->cont_cat_audio->getClientOriginalName();
                $destinationPath = "storage/app/public/uploads/user_template_content/audio/";
                $request->cont_cat_audio->move($destinationPath, $attachment);

                $content_temp->cont_cat_video_img_audio = $attachment;
            }
        }

        $content_temp->save();

        session()->flash('success', 'New content template inserted successfully');
        return redirect()->back();
    }


    /*
    @method         => edit
    @description    => Form edit view
    @author         => munmun
    @date           => 28/02/2020
    */
    public function edit($id)
    {

        $data = array();
        $data['details'] = UserContentTemplate::find($id);

        return view('modules.user_content_temp.edit')->with($data);
    }

    /*
    @method         => update
    @description    => Form update in form_master table
    @author         => munmun
    @date           => 28/02/2020
    */
    public function update($id, Request $request)
    {
        //pr1($id);
        //die();
        $request->validate([
            'cont_subject' => 'required',
            'cont_description' => 'required',
        ]);


        $update['cont_subject'] = $request->cont_subject;
        $update['cont_description'] = $request->cont_description;
        $update['cont_category'] = $request->cont_category;

        if ($request->cont_category == "video") {
            if ($request->cont_cat_video_img_audio) {
                $youtube_videoid = getYouTubeIdFromURL($request->cont_cat_video_img_audio);
                $update['cont_cat_video_img_audio'] = @$youtube_videoid;
            }
        }

        if ($request->cont_category == "image") {
            if (@$request->cont_cat_video_img_audio) {
                $attachment = time() . '_' . $request->cont_cat_video_img_audio->getClientOriginalName();
                $destinationPath = "storage/app/public/uploads/user_template_content/";
                $request->cont_cat_video_img_audio->move($destinationPath, $attachment);

                $update['cont_cat_video_img_audio'] = $attachment;
            }
        }
        if ($request->cont_category == "audio") {
            if (@$request->cont_cat_audio) {
                $attachment = time() . '_' . $request->cont_cat_audio->getClientOriginalName();
                $destinationPath = "storage/app/public/uploads/user_template_content/audio/";
                $request->cont_cat_audio->move($destinationPath, $attachment);

                $update['cont_cat_video_img_audio'] = $attachment;
            }
        }
        $det = UserContentTemplate::where('id', $id)->first();
        if(@$det){
            if (@$request->cont_cat_video_img_audio || @$request->cont_cat_audio) {
                @unlink('storage/app/public/uploads/user_template_content/'.@$det->cont_cat_video_img_audio);
                @unlink('storage/app/public/uploads/user_template_content/audio/'.@$det->cont_cat_video_img_audio);
            }
            $det->update($update);
        }



        session()->flash('success', 'Content template edited successfully.');
        return redirect()->back();
    }


    public function show($id)
    {
        //pr1($id);
        //die();

        $details = UserContentTemplate::find($id);
        // pr1($details->toArray());
        // die();
        return view('modules.user_content_temp.show', [
            'details'       => $details
            //'questions'  => $questions
            //'productImages' => $productImages,

        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     *  @param  int  $id
     *  @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //pr1($id);
        //die();

        UserContentTemplate::where('id', $id)->delete();
        session()->flash('success', 'Form deleted successfully.');
        return redirect()->back();
    }

    // for user assigne
    public function profContentTempUserAssigne(Request $request)
    {

        //pr1($request->all());
        //die();

        $data = $request->all();
        $nowtime = date('Y-m-d H:i:s');

        //$formdata = FormMaster::where('id',$request->form_id)->first();
        //pr1($data);
        //pr1($formdata->tool_type);
        //die();



        if ($data) {
            if ($request->user_id) {
                foreach ($request->user_id as $val) {

                    $insert = array();
                    $insert['user_id']          = $val;
                    $insert['professional_id']  = Auth::id();
                    $insert['tool_type']        = "Content Templates";
                    $insert['tool_id']          = $request->content_temp_id;
                    $insert['assign_date']      = $nowtime;
                    $insert['status']           = "INVITED";

                    UserToTools::create($insert);
                }

                session()->flash('success', \Lang::get('client_site.user_assigned_successfully'));
                return redirect()->back();

                // send mail to profession start
                //Mail::send(new FormAnswered($usermail,$professionalmail));
                // send mail to profession end
            } else {

                session()->flash('error', \Lang::get('site.Select_User'));
                return redirect()->back();
                //return redirect()->route('user.view.form');
            }
        }
    }

    //18.3.20
    public function profContentTempUserAssigneList($id)
    {

        $data['all_user_data'] = UserToTools::with('getprofessionalUserData')->where(['professional_id' => Auth::id(), 'tool_id' => $id, 'tool_type' => 'Content Templates'])->get();

        return view('modules.user_content_temp.assign_users_list')->with($data);
    }


    // ==============for user part===============

    public function userContentTempView($id)
    {

        $details = UserToTools::with(['getContentTemplatesdata'])->find($id);
        //pr1($details->toArray());
        //die();
        return view('modules.user_content_temp.view_assign_user', [
            'details'       => $details
        ]);
    }



    public function contenttempStatusUpdate($id)
    {

        //pr1($id);
        //die();

        $data = UserContentTemplate::find($id);

        if (@$data->status == 'ACTIVE') {
            UserContentTemplate::where('id', $data->id)->update(['status' => 'INACTIVE']);
            session()->flash('success', 'Content template inactive updated successfully.');
            return redirect()->back();
        }

        if (@$data->status == 'INACTIVE') {
            UserContentTemplate::where('id', $data->id)->update(['status' => 'ACTIVE']);
            session()->flash('success', 'Content template active updated successfully.');
            return redirect()->back();
        }
    }
}