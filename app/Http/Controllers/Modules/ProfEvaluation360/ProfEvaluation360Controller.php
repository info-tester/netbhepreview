<?php

namespace App\Http\Controllers\Modules\ProfEvaluation360;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tool360Master;
use App\Models\Tool360Details;
use App\Models\Tool360AnsweredDetail;
use App\Models\Tool360Answered;
use App\Models\Booking;


use App\Models\UserToTools;
use App\Models\CompetencesMaster;
use App\Models\TypeOfEvaluationMaster;
use App\Models\TypeEvaluationToCompetences;
use App\Models\Tools360EvaluationUserAnswer;
use App\Models\Tools360EvaluationUrlShare;
use App\Models\CompetencesQuestion;

use validate;
use App\User;
use Auth;
use Mail;
use App\Mail\FormAnswered;
use App\Mail\EvulationReviewUser;
use App\Models\ToolsEvaluationCategory;

class ProfEvaluation360Controller extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }




    public function index()
    {

        $data = array();
        $data['title'] = 'Manage 360Evaluation';

        $data['all360data'] = Tool360Master::with(['get360Details', 'getUserData'])->where('status', 'ACTIVE')->orderBy('id', 'DESC')->get();

        $data['all_paid_users'] = Booking::select('user_id')->distinct()->with('userDetails')->where('professional_id', Auth::id())->where('payment_status', 'P')->get();


        return view('modules.prof_evaluation360.index')->with($data);
    }

    /*
    @method         => create
    @description    => Form create in form_master table
    @author         => munmun
    @date           => 28/02/2020
    */
    public function create(Request $request)
    {

        $data = array();
        $nowtime = date('Y-m-d H:i:s');
        $tendaybef = date('Y-m-d H:i:s', strtotime("-3 days"));

        // $data['all_paid_users'] = Booking::select('user_id')->distinct()->with('userDetails')->where('professional_id',Auth::id())->where('payment_status','P')->get();

        $data['all_paid_users'] = Booking::select('user_id')->distinct()->with('userDetails')->where('professional_id', Auth::id())->where('payment_status', 'P')->whereBetween('created_at', [$tendaybef, $nowtime])->get();

        $data['all_competencesmaster'] = CompetencesMaster::whereHas('specialities', function( $query ) {
            $query = $query->where('professional_specialty_id', Auth::user()->professional_specialty_id);
        })->get();
        $data['all_type_evaluation'] = TypeOfEvaluationMaster::whereHas('specialities', function( $query ) {
            $query = $query->where('professional_specialty_id', Auth::user()->professional_specialty_id);
        })->get();
        //pr1($data['all_type_evaluation']);
        //die();
        $data['categories'] = ToolsEvaluationCategory::whereHas('specialities', function( $query ) {
            $query = $query->where('professional_specialty_id', Auth::user()->professional_specialty_id);
        })->get();
        return view('modules.prof_evaluation360.create')->with($data);
        //return view('admin.modules.product.create');
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $nowtime = date('Y-m-d H:i:s');
        $data = $request->all();

        // dd($request->all());
        $request->validate([
            'review_email'        => 'required|email',
            'evaluations_review_email_one'        => 'required|email',
            'competences'        => 'required'
        ]);
        // dd($request->all());

        //$dadd = CompetencesQuestion::where('title','Eyesight')->get();
        // $question = [];
        // foreach ($dadd as $key => $value) {
        //     $question[] = ['id' => $value->id,'question' => $value->question];
        // }
        // dd($question);
        // pr1($question->toArray());
        //pr1($dadd);
        //pr1($dadd->toArray());
        //die();

        //for send url in mail
        $emailone = $request->review_email;
        $emailtwo = $request->evaluations_review_email_one;
        $reviewmail = array($emailone, $emailtwo);



        // $emaione = $data->data
        // pr1($usertools);
        // die();
        //$usertype = array("C=customer", "M=manager", "O =others","P =pairs","S=subordinates","U =users");
        $usertype = array("C", "M", "O", "P", "S", "U");


        $request->validate([
            'evaluation_type'        => 'required'
        ]);


        if ($request->user_id) {
            foreach ($request->user_id as $val) {

                $usertools = UserToTools::latest()->first();

                $insert = array();
                $insert['user_id']              = $val;
                $insert['professional_id']      = Auth::id();
                $insert['tool_type']            = "Evaluation360";
                $insert['evaluations_review_email']         = $request->review_email;
                $insert['evaluations_review_email_one']     = $request->evaluations_review_email_one;
                $insert['evaluations_user_link']            = $usertools->id + 1;
                $insert['evaluations_deadline'] = $request->evaluations_deadline;
                $insert['assign_date']          = $nowtime;
                $insert['status']               = "INVITED";

                //for user to tools table insert
                $latest = UserToTools::create($insert);

                //for reviewmail send two users
                foreach ($reviewmail as $val) {
                    $usermail = [];
                    $usermail['email'] = $val;
                    $usermail['usertoolsid'] = $latest->id;
                    $professionalmail = Auth::user()->email;

                    try {
                        Mail::send(new EvulationReviewUser($usermail, $professionalmail));
                    } catch (Throwable $e) {
                        report($e);
                        return false;
                    }
                }
                //for insert Tools360EvaluationUrlShare
                foreach ($usertype as $val) {

                    $insert2 = array();
                    $insert2['user_to_tools_id']        = $latest->id;
                    $insert2['user_type']               = $val;
                    $insert2['evaluation_urlshare']     = $latest->id . '/' . $val;

                    Tools360EvaluationUrlShare::create($insert2);
                }
                //for insert Tools360EvaluationUserAnswer table
                foreach ($request->competences as $key => $val) {


                    $dadd = CompetencesQuestion::where('title', $val)->get();
                    // $question = [];
                    foreach ($dadd as  $datavalu) {
                        // $question[] = ['tool360_competences_master_id' => $value->tool360_competences_master_id,'question' => $value->question];

                        $insert1 = array();
                        $insert1['user_id']                 = $latest->user_id;
                        $insert1['professional_id']         = Auth::id();
                        $insert1['user_to_tools_id']        = $latest->id;
                        //$insert1['tool360_competences']     = $val;
                        $insert1['tool360_competences']     = $datavalu->question;
                        $insert1['tool360_competences_id']  = $datavalu->tool360_competences_master_id;
                        $insert1['tool360_evaluation_type'] = $request->evaluation_type;
                        //$insert1['status']                  = "INVITED";
                        Tools360EvaluationUserAnswer::create($insert1);
                    }


                    /*$insert1 = array();
                    $insert1['user_id']                 = $latest->user_id;
                    $insert1['professional_id']         = Auth::id();
                    $insert1['user_to_tools_id']        = $latest->id;
                    //$insert1['tool360_competences']     = $val;
                    $insert1['tool360_competences']     = $request->competences_question[$key];
                    $insert1['tool360_competences_id']  = $request->competences_id[$key];
                    $insert1['tool360_evaluation_type'] = $request->evaluation_type;
                    //$insert1['status']                  = "INVITED";
                    Tools360EvaluationUserAnswer::create($insert1);*/
                }
            }



            session()->flash('success', \Lang::get('client_site.user_assigned_successfully'));
            return redirect()->back();

            // send mail to profession start
            //Mail::send(new FormAnswered($usermail,$professionalmail));
            // send mail to profession end

        } else {

            session()->flash('error', \Lang::get('site.Select_User'));
            return redirect()->back();
            //return redirect()->route('user.view.form');
        }

        //return redirect(route('form.question.create',['form_id' => $last->id]));

    }


    /*
    @method         => edit
    @description    => Form edit view
    @author         => munmun
    @date           => 28/02/2020
    */
    public function edit($id)
    {

        // $data = array();
        // $data['details'] = Tool360Master::with('get360Details')->find($id);

        // return view('modules.professional_360evaluation.edit')->with($data);
    }

    /*
    @method         => update
    @description    => Form update in form_master table
    @author         => munmun
    @date           => 28/02/2020
    */
    public function update($id, Request $request)
    {
        //pr1($id);
        //die();
        // $request->validate([
        //     'title'            => 'required'
        // ]);

        // $update['title']     = $request->title;

        // Tool360Master::where('id',$id)->update($update);


        // // insert to Tool360Master details
        // if(@$request->answer){
        //     Tool360Details::where('id_360master',$id)->delete($id);
        //     foreach ($request->answer as $key => $val) {
        //         $insert['id_360master']   = $id;
        //         $insert['answer']         = $val;
        //         $insert['color_box']         = $request->color_box[$key];
        //         Tool360Details::create($insert);
        //     }
        //     Tool360Details::whereNull('answer')->delete();
        // }


        // session()->flash('success', 'Form edited successfully.');
        // return redirect()->back();
    }


    public function show($id)
    {

        //     $details = Tool360Master::with(['get360Details','getUserData']
        // )->find($id);
        //     //pr1($details->get360Details->toArray());
        //     //die();

        //     if($details->get360Details){
        //         $ans = [];
        //         foreach ($details->get360Details as $key => $value) {
        //             $ans[$key]['y']                   = 10;
        //             $ans[$key]['name']                = @$value->answer;
        //             $ans[$key]['color']               = @$value->color_box;
        //         }
        //          //dd($ans);
        //         $get_all_anss = json_encode($ans);
        //         //pr1($get_all_anss);
        //         //die();
        //     }

        //     return view('modules.professional_360evaluation.show', [
        //         'details'       => $details,
        //         'get_all_anss'  => $get_all_anss
        //         //'questions'  => $questions
        //         //'productImages' => $productImages,

        //     ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     *  @param  int  $id
     *  @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        // $data = UserToTools::where('tool_id', $id)->first();
        // if(@$data){
        //     //echo "not delete";
        //     session()->flash('error', 'Form not deleted because it is assigned user.');
        //     return redirect()->back();
        // } else {
        //     //echo "delete";
        //     FormMaster::where('id', $id)->delete();
        //     session()->flash('success', 'Form deleted successfully.');
        //     return redirect()->back();
        // }
    }



    // public function professional360eEvuUserAssigne(Request $request){

    //     $data = $request->all();
    //     $nowtime = date('Y-m-d H:i:s');

    //     //$formdata = FormMaster::where('id',$request->form_id)->first();
    //     //pr1($data);
    //     //pr1($formdata->tool_type);
    //     //die();/

    //     if($data){
    //         if($request->user_id){
    //             foreach ($request->user_id as $val) {

    //                 $insert = array();
    //                 $insert['user_id']          = $val;
    //                 $insert['professional_id']  = Auth::id();
    //                 $insert['tool_type']        = "360 evaluation";
    //                 $insert['tool_id']          = $request->tool_360_master_id;
    //                 $insert['assign_date']      = $nowtime;
    //                 $insert['status']           = "INVITED";

    //                 UserToTools::create($insert);
    //             }

    //             session()->flash('success', "User assigne successfully");
    //             return redirect()->back();

    //             // send mail to profession start
    //                 //Mail::send(new FormAnswered($usermail,$professionalmail));
    //             // send mail to profession end

    //         } else {

    //             session()->flash('error', "Select user");
    //             return redirect()->back();
    //             //return redirect()->route('user.view.form');
    //         }
    //     }
    // }

    // public function professional360eEvuUserAssigneView($id){


    //     $user_tool_data = UserToTools::where('tool_id',$id)->where('tool_type','360 evaluation')->first();

    //     if($user_tool_data){

    //         $user_tool_id = $user_tool_data->id;

    //         //pr($user_tool_id);
    //         //die();

    //         $all_answered_data = Tool360Answered::with(['getAnsweredDetail','getAnsweredDetail.getAnswerTitle'])->where('user_to_tools_id',$user_tool_id)->first();

    //         // pr1($all_answered_data);
    //         // die();
    //         if($all_answered_data){
    //             $ans = [];
    //             foreach ($all_answered_data->getAnsweredDetail as $key => $value) {
    //                 $ans[$key]['y']                   = $value->score;
    //                 $ans[$key]['name']                = @$value->getAnswerTitle->answer;
    //                 $ans[$key]['color']               = @$value->getAnswerTitle->color_box;
    //             }
    //              //dd($ans);
    //             $get_all_anss = json_encode($ans);
    //         }else{
    //             $get_all_anss = null;
    //         }
    //     }else{

    //         $get_all_anss = null;
    //         $all_answered_data =null;
    //     }

    //     //pr1($get_all_anss);
    //     //die();

    //     return view('modules.professional_360evaluation.view',compact('get_all_anss','all_answered_data'));
    // }

    //getting all assign user list
    public function profEvu360AssigneUserList()
    {

        $data['all_user_data'] = UserToTools::with(['getprofessionalUserData', 'getEvulation360Url'])->where('professional_id', Auth::id())->where('tool_type', 'Evaluation360')->orderBy('id', 'desc')->get();

        return view('modules.prof_evaluation360.user_assign_list')->with($data);
    }



    function fetchCompetencesAll(Request $request)
    {

        $all_competencesmaster = CompetencesMaster::whereHas('specialities', function( $query ) {
            $query = $query->where('professional_specialty_id', Auth::user()->professional_specialty_id);
        })->get();
        $all_type_evaluation = TypeOfEvaluationMaster::whereHas('specialities', function( $query ) {
            $query = $query->where('professional_specialty_id', Auth::user()->professional_specialty_id);
        })->get();

        // $all_competencesmaster_ids = CompetencesMaster::select('id')->get();

        $value = $request->get('value');
        // $data = TypeEvaluationToCompetences::where('evaluation_type_id', $value)->get();

        $all_competences_ids = TypeEvaluationToCompetences::select('competences_id')->where('evaluation_type_id', $value)->get();
        $id = [];
        foreach ($all_competences_ids as $row) {
            $id[] = $row->competences_id;
        }


        $output = '<div class="your-mail competences" id="competences">
                        <label for="exampleInputEmail1" class="personal-label">Select the type of evaluation:</label>
                        <div class="row" style="float:left;width: 100%">';
        foreach ($all_competencesmaster as $row) {
            if (@$id && in_array($row->id, $id)) {
                $output .= '<div class="col-lg-6"><input type="checkbox" id="' . $row->title . '" value="' . $row->title . '" name="competences[]" checked>
                    <input type="hidden" id="' . $row->id . '" value="' . $row->id . '" name="competences_id[]" >
                    <input type="hidden" id="' . $row->question . '" value="' . $row->question . '" name="competences_question[]">
                    <label for="' . $row->title . '">' . $row->title . '</label> </div>';
            } else {
                $output .= '<div class="col-lg-6"><input type="checkbox" value="' . $row->title . '" id="' . $row->title . '" name="competences[]">
                    <label for="' . $row->title . '">' . $row->title . '</label> </div>';
            }
        }

        $output .= '
            </div></div>
        ';
        echo $output;
    }



    //26.3.20 for user start
    public function getUserEvaluation360QuesList($id)
    {
        //pr1($id);
        //die();
        //->groupby('year','month')
        $data['user_to_tools_id'] = $id;
        $data1 = UserToTools::where('id', $id)->first();
        $data['all_evulation_shair_url'] = UserToTools::with('getEvulation360Url')->where('id', $id)->first();

        $data['evaluations_user_link'] = $data1->evaluations_user_link;
        $all_evaluation_data = Tools360EvaluationUserAnswer::where('user_to_tools_id', $id)->get();
        $tool360_competences_id = Tools360EvaluationUserAnswer::where('user_to_tools_id', $id)
            ->groupBy('tool360_competences_id')
            ->get();

        $data['all_evaluation_data'] = [];

        foreach ($tool360_competences_id as $key => $value) {
            // $data['all_evaluation_data'][$key]['val'] = $value;
            foreach ($all_evaluation_data as $key1 => $value1) {
                if ($value->tool360_competences_id === $value1->tool360_competences_id) {
                    $data['all_evaluation_data'][$key][] = $value1;
                }
            }
        }
        // dd($data,$tool360_competences_id);
        //pr1($data['all_evaluation_data']->toArray());
        //die();
        return view('modules.prof_evaluation360.all_evualiton_list')->with($data);
    }


    //evlution type user pint update
    public function userEvaluation360ScorePost(Request $request)
    {
        $data = $request->all();

        $alldetails = Tools360EvaluationUrlShare::where('user_to_tools_id', $request->user_to_tools_id)->where('user_type', "U")->first();

        //pr1($alldetails->status);
        //die();

        if (@$alldetails->status == "complete") {
            session()->flash('error', \Lang::get('site.Select_User'));
            return redirect()->back();
        } else {


            if ($data) {
                if ($request->user_answer_point) {

                    foreach ($request->user_answer_point as $key => $val) {
                        $insert = array();
                        $insert['user_answer_point']    = $val;
                        $insert['report_note']          = $request->report_note[$key];

                        $user_answer_id = $request->evaluation_user_answer_id[$key];
                        Tools360EvaluationUserAnswer::where('id', $user_answer_id)->update($insert);
                    }

                    //get avarage ans point start

                    $user_answer_point = Tools360EvaluationUserAnswer::where('user_to_tools_id', $request->user_to_tools_id)->get();
                    $user_avg_point  = $user_answer_point->avg('user_answer_point');

                    //get avarage ans point end

                    //if($request->usertype =="users"){
                    Tools360EvaluationUrlShare::where('user_to_tools_id', $request->user_to_tools_id)->where('user_type', "U")->update(['avg_ans_point' => $user_avg_point, 'status' => 'complete']);
                    //}

                    //get avg point to upadte user to tools table start

                    $evaluation_urlshare_point = Tools360EvaluationUrlShare::where('user_to_tools_id', $request->user_to_tools_id)->get();

                    $urlshare_point_avg_point  = $user_answer_point->avg('avg_ans_point');

                    UserToTools::where('id', $request->user_to_tools_id)->update(['evaluations_avg_point' => $urlshare_point_avg_point]);

                    //get avg point to upadte user to tools table end

                    session()->flash('success', \Lang::get('client_site.score_submited_successfully'));
                    return redirect()->back();
                } else {

                    session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
                    return redirect()->back();
                    //return redirect()->route('user.view.form');
                }
            }
        } // status complete

    }

    public function userEvaluation360ScoreListing($id)
    {
        $data['user_tools_id'] = $id;
        $data['all_evaluation_data'] = Tools360EvaluationUserAnswer::where('user_to_tools_id', $id)->get();
        $data['all_usertype_anspoint'] = Tools360EvaluationUrlShare::where('user_to_tools_id', $id)->get();
        //pr1($data['all_evaluation_data']->toArray());
        //die();
        return view('modules.prof_evaluation360.all_evualiton_scorelist_submited')->with($data);
    }

    // public function userpPublicEvaluation360()

    //26.3.20 for user end


}
