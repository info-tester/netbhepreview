<?php

namespace App\Http\Controllers\Modules\User360Evaluation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tool360Master;
use App\Models\Tool360Details;
use App\Models\Tool360AnsweredDetail;
use App\Models\Tool360Answered;
use App\Models\Booking;
use App\Models\UserToTools;
use validate;
use App\User;
use Auth;
use Mail;
use App\Mail\FormAnswered;


class User360EvaluationController extends Controller {


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    //for 360 Evaluation  

    public function getUser360Evaluation($id){
        //pr1($id);
        //die();

        //get tool 360 master id
        $usertool = UserToTools::find($id);
        $tool_id = $usertool->tool_id;

        //get question
        $get_question = Tool360Master::where('id',$tool_id)->first();
        
        //get all answer
        $get_all_ans = Tool360Details::where('id_360master',$tool_id)->get();
        $a = [];
        foreach ($get_all_ans as $key => $value) {
            $a[$key]['y']                   = 1;
            $a[$key]['name']                = $value->answer;
            $a[$key]['color']               = $value->color_box;
            $a[$key]['id']                  = $value->id;            
            $a[$key]['user_to_tools_id']    = $id;                     
            $a[$key]['question']            = $get_question->title;                     
        }
        // dd($a);

        $get_all_anss = json_encode($a);

        //pr1($get_all_anss);
        //die();

        return view('modules.360degree_evaluation.index',compact('get_all_anss','get_question','get_all_ans'));

    }

    //for 360 Evaluation answer table data insert 

    public function getUser360EvaluationPost(Request $request){

        $response = [
            'jsonrpc'   => '2.0'
        ];

        $nowtime = date('Y-m-d H:i:s');
        $data = $request->all();
        $all_ans_data = $data['params']['answer_data'];
        
        $last_inset_data = null;
        
        foreach ($all_ans_data as $key => $val) {                    
            
            if($key == 0) {
                $insert['user_to_tools_id']         = $val['user_to_tools_id']; 
                $insert['question']                 = $val['question']; 
                $last_inset_data = Tool360Answered::create($insert);

                UserToTools::where('id', $val['user_to_tools_id'])->update(['status' => 'COMPLETED','completion_date' => $nowtime]);
            }
            if(@$last_inset_data) {
                $insert1['tools_360_answered_id']    = $last_inset_data->id;
                $insert1['tools_360_details_id']     = $val['id'];                   
                $insert1['score']                    = $val['y'];
                Tool360AnsweredDetail::create($insert1);
            }
            
        }


        if(@$last_inset_data) {
            $response['success'] = \Lang::get('client_site.saved_successfully');            
        } else {
            $response['error']['message'] = \Lang::get('client_site.something_went_be_wrong');
        }
        return $response;

        //return $request->all();
    }


    //for 360 Evaluation view 

    public function getUser360EvaluationView($id){

       //$id =  $usertool->tool_id (UserToTools table)
        
        $all_answered_data = Tool360Answered::with(['getAnsweredDetail','getAnsweredDetail.getAnswerTitle'])->where('user_to_tools_id',$id)->first();
        
        //pr1($all_answered_data->toArray());
        //die();
        $ans = [];
        foreach ($all_answered_data->getAnsweredDetail as $key => $value) {
            $ans[$key]['y']                   = $value->score;
            $ans[$key]['name']                = @$value->getAnswerTitle->answer;
            $ans[$key]['color']               = @$value->getAnswerTitle->color_box;                                
        }
         //dd($a);

        $get_all_anss = json_encode($ans);

        //pr1($get_all_anss);
        //die();

        return view('modules.360degree_evaluation.view',compact('get_all_anss','all_answered_data'));

    }


}
