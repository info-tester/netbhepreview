<?php

namespace App\Http\Controllers\Modules\VideoCall;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\Commission;
use App\Models\Payment;
use App\Models\UserToCard;
use App\Models\VideoAffEarning;
use App\User;
use Moip\Moip;
use Moip\Auth\BasicAuth;
use Moip\Auth\OAuth;
use Illuminate\Support\Facades\Auth;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VideoGrant;

class VideoCallController extends Controller
{
    protected $adminAccessToken;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->adminAccessToken = getAdminToken();
    }

    public function index()
    {
        return view('modules.videocall.videocall');
    }

    /**
     * Method: getTwilioToken
     * Description: This method is used to get twilio token
     * Author: Sanjoy
     */
    public function getTwilioToken(Request $request)
    {
        /*dd(env('TWILIO_ACCOUNT_SID'),
            env('TWILIO_API_KEY_SID'),
            env('TWILIO_API_KEY_SECRET'));*/
        // $accountSid = 'AC2501d55bb6716833be88c712e3d2f57b';
        // $apiKeySid = 'SK54f0ee6fc2fdc424ad158506a86b671d';
        // $apiKeySecret = 'e9JrK9gKnWgCHVafUSHObTqh51yVTznf';

        // $accountSid = 'AC88a39701a22e318c93573d2e2447094f';
        // $apiKeySid = 'SKafcc55996bcc91d46e4a76839dd1b71c';
        // $apiKeySecret = 'TQZ9qGVQYRJiHMZ3bEkzchWQ8ktWee2Q';

        $accountSid = 'AC88a39701a22e318c93573d2e2447094f';
        $apiKeySid = 'SK563c99c3157a1e289dea61bf6253a153';
        $apiKeySecret = 'vpr2HGTLWNbc7RwsaGDVzqcewekRf5ne';

        $identity = Auth::user()->name . '(#' . Auth::user()->id . ')';

        // Create an Access Token
        $token = new AccessToken(
            $accountSid,
            $apiKeySid,
            $apiKeySecret,
            3600,
            $identity
        );
        // Grant access to Video
        $grant = new VideoGrant();
        $grant->setRoom($request->roomName);
        $token->addGrant($grant);

        // Serialize the token as a JWT
        $response = [
            'token'     => $token->toJWT(),
            'identity'  => $identity
        ];
        return response()->json($response, 200);
    }

    public function updateStatus(Request $request)
    {
        $response = [
            'jsonrpc'   => '2.0'
        ];
        if ($request->params['token']) {
            // $this->chargeUser($request->params['token']);
            $orderDetails= Booking::where('token_no', $request->params['token'])->with(['payment','profDetails','userDetails'])->first();


            if(@$orderDetails->profDetails->video_affiliate_id && $orderDetails->video_status!='C'){
                $affCommission=Commission::first();
                $ins['affiliate_id']=$orderDetails->profDetails->video_affiliate_id;
                $ins['user_id']=$orderDetails->professional_id;
                $ins['booking_id']=$orderDetails->id;
                $ins['type']='P';
                $ins['amount']=$orderDetails->payment->paymentDetails1->admin_commission*($affCommission->affiliate_commission_professional/100);
                VideoAffEarning::create($ins);
                User::where('id',$orderDetails->profDetails->video_affiliate_id)->increment('video_aff_earning', $ins['amount']);

            }
            if(@$orderDetails->userDetails->video_affiliate_id && $orderDetails->video_status!='C'){
                $affCommission=Commission::first();
                $ins['affiliate_id']=$orderDetails->userDetails->video_affiliate_id;
                $ins['user_id']=$orderDetails->user_id;
                $ins['booking_id']=$orderDetails->id;
                $ins['type']='S';
                $ins['amount']=$orderDetails->payment->paymentDetails1->admin_commission*($affCommission->affiliate_commission_user/100);
                VideoAffEarning::create($ins);
                User::where('id',$orderDetails->userDetails->video_affiliate_id)->increment('video_aff_earning', $ins['amount']);
            }
            $booking = Booking::where('token_no', $request->params['token'])
                ->update([
                    'video_status'  =>  'C'
                ]);
            $response['status'] = 1;
        }
        return response()->json($response);
    }

    /**
     * For charging user after video call completation.
     *
     * @method: chargeUser
     */
    protected function chargeUser($token)
    {
        $user_id = Booking::where('token_no', $token)->first();
        $cardDetails = UserToCard::where('user_id', $user_id->user_id)->first();
        $access_token = getAdminToken();

        $customer = User::where([
            'id'    =>  @$user_id->user_id
        ])->first();

        // In Here the Primary account is the Admin account and the secondar account is the professional account.
        $amount = bcmul(@$user_id->amount, 100); // calculating on cents...
        $split_amount = bcmul(@$user_id->amount / 2, 100); // calculating on cents...]
        $amount = (int) $amount;
        $split_amount = (int) $split_amount;

        if (env('PAYMENT_ENVIORNMENT') == 'live') {
            $moip = new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_PRODUCTION);
        } else if (env('PAYMENT_ENVIORNMENT') == 'sandbox') {
            $moip = new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_SANDBOX);
        }

        $order  =  $moip->orders()->setOwnId(uniqid())
            ->addItem("Consultation Fees", 1, @$user_id->parentCatDetails->name, @$amount)
            ->setShippingAmount(0)->setAddition(0)->setDiscount(0)
            ->setCustomerId(@$customer->customer_id)
            ->addReceiver(@$user_id->profDetails->professional_id, "SECONDARY", @$split_amount, 0, false)
            ->create();

        $UpdateBooking = Booking::where([
            'token_no'  =>  @$token
        ])->update([
            'moip_order_id'  =>  @$order->getId()
        ]);

        return $this->payment($order, $customer, $cardDetails, $user_id);
    }

    /**
     * For Deducting customer balance
     * @method: payment
     */

    public function payment($order, $user, $card, $userBooking)
    {
        if (env('PAYMENT_ENVIORNMENT') == 'live') {
            $url = Moip::ENDPOINT_PRODUCTION;
        } else if (env('PAYMENT_ENVIORNMENT') == 'sandbox') {
            $url = Moip::ENDPOINT_SANDBOX;
        }
        $data = array(
            'installmentCount' => 1,
            'statementDescriptor' => 'netbhe',
            'fundingInstrument' =>
            array(
                'method' => 'CREDIT_CARD',
                'creditCard' =>
                array(
                    'id' => @$card->card_id,
                    'store' => true,
                    'holder' =>
                    array(
                        'fullname' => @$card->card_name,
                        'birthdate' => @$user->dob,
                        'taxDocument' =>
                        array(
                            'type' => 'CPF',
                            'number' => @$user->cpf_no,
                        ),
                        'phone' =>
                        array(
                            'countryCode' => @$user->countryName->phonecode,
                            'areaCode' => @$user->area_code,
                            'number' => @$user->mobile,
                        ),
                        'billingAddress' =>
                        array(
                            'city' => @$user->city,
                            'district' => @$user->district,
                            'street' => @$user->street_name,
                            'streetNumber' => @$user->street_number,
                            'zipCode' => @$user->zipcode,
                            'state' => substr(@$user->stateName->name, 0, 2),
                            'country' => substr(@$user->countryName->name, 0, 3),
                        ),
                    ),
                ),
            ),
        );
        $crl = curl_init();
        $header = array();
        // $header[] = 'Content-length: 0';
        $header[] = 'Content-type: application/json';
        $header[] = 'Authorization: Bearer ' . $this->adminAccessToken;
        curl_setopt($crl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($crl, CURLOPT_URL, "{$url}/v2/orders/" . @$order->getId() . "/payments");
        curl_setopt($crl, CURLOPT_POST, 1);
        curl_setopt($crl, CURLOPT_POST, true);
        curl_setopt($crl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        $payment_output = curl_exec($crl);
        curl_close($crl);
        $payment_output = json_decode($payment_output);
        if (@$payment_output->id) {
            $payment = Payment::create([
                'token_no'              =>  @$userBooking->token_no,
                'order_id'              =>  @$order->getId(),
                'payment_id'            =>  @$payment_output->id,
                'user_id'               =>  @$userBooking->user_id,
                'professional_id'       =>  @$userBooking->professional_id,
                'professional_amount'   =>  @$userBooking->amount / 2,
                'admin_amount'          =>  @$userBooking->amount / 2,
                'total_amount'          =>  @$userBooking->amount,
                'wirecard_response'     =>  json_encode($payment_output),
                'balance_status'        =>  'R'
            ]);
            if (@$payment) {
                $UpdateBooking = Booking::where([
                    'token_no'        =>  @$userBooking->token_no
                ])->update([
                    'payment_status'  =>  "P"
                ]);
                return true;
            } else {
                $UpdateBooking = Booking::where([
                    'token_no'        =>  @$userBooking->token_no
                ])->update([
                    'payment_status'  =>  "F"
                ]);

                return false;
            }
        }
    }

    /**
     * Method: updateVideoInitiated
     * Description: This method is used to update video status to initiated
     * Author: Sanjoy
     */
    public function updateVideoInitiated(Request $request)
    {
        $response = [
            'jsonrpc'   => '2.0'
        ];
        if ($request->params['token']) {
            Booking::where('token_no', $request->params['token'])->update(['is_video_started'  =>  'Y']);
            $response['result']['status'] = 'success';
        }
        return response()->json($response);
    }

    /**
     * Method: updateVideoInitiated
     * Description: This method is used to update video status to initiated
     * Author: Sanjoy
     */
    public function updateCallTime(Request $request)
    {
        $response = [
            'jsonrpc'   => '2.0'
        ];
        // $data = Booking::where('token_no', $request->params['token'])->first();
        if ($request->params['token']) {
            if($request->params['user_type'] == 'C'){
                Booking::where('token_no', $request->params['token'])->increment('professional_call_time', 1);

            }
            if($request->params['user_type'] == 'P'){
                Booking::where('token_no', $request->params['token'])->increment('student_call_time', 1);
            }
            $data = Booking::where('token_no', $request->params['token'])->first();
            if(@$data->student_call_time > @$data->professional_call_time){
                Booking::where('token_no', $request->params['token'])->update(['completed_call'=> $data->professional_call_time]);
            }
            if(@$data->student_call_time < @$data->professional_call_time){
                Booking::where('token_no', $request->params['token'])->update(['completed_call'=> $data->student_call_time]);
            }
            if($data->student_call_time == $data->professional_call_time){
                Booking::where('token_no', $request->params['token'])->update(['completed_call'=> $data->student_call_time]);
            }

            // Booking::where('token_no', $request->params['token'])->increment('completed_call',1);
            $response['result']['status'] = 'success';
        }
        return response()->json($response);
    }

    public function getBookingDetails(Request $request)
    {
        if(@$request->booking_token){
            $booking = Booking::where('token_no', $request->booking_token)->first();
            if(@$booking){
                return response()->json([
                    'result' => [
                        'booking_id'     => (int)@$booking->id,
                        'token'          =>  $booking->token_no,
                        'user_id'        =>  $booking->userDetails->id,
                        'username'       =>  $booking->userDetails->name
                    ]
                ]);
            }
            return response()->json([
                'error' => [
                    'message' => \Lang::get('client_site.booking_details_not_found')
                ]
            ]);
        }
    }
}
