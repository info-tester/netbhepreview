<?php

namespace App\Http\Controllers\Modules\Payment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Models\PaymentDetails;
use Auth;

class PaymentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function myPayments(Request $request)
    {

        if (@Auth::user()->is_professional != "Y") {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }

        $payment = PaymentDetails::with('paymentDetails.userDetails')->where('professional_id', Auth::id())->where('is_refund','N');
        $key = [];
        if ($request->all()) {
            if ($request->token_no) {
                $payment = $payment->where('token_no', $request->token_no);
            }
            if ($request->order_id) {
                $payment = $payment->whereHas('paymentDetails', function ($q) use ($request) {
                    $q->where('pg_order_id', $request->order_id);
                });
            }
            if ($request->balance_status) {
                $payment = $payment->where('balance_status', $request->balance_status);
            }
        }
        $key = $request->all();
        $payment = $payment->orderby('id','desc')->get();
        return view('modules.payment.payment')->with([
            "payment" =>  @$payment,
            "key"     =>  @$key
        ]);
    }
}
