<?php

namespace App\Http\Controllers\Modules\Payment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Payment;
use Auth;
class PaymentController extends Controller
{
	 /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function myPayments(Request $request){

    if(@Auth::user()->is_professional!="Y"){
      session()->flash('error', 'Unauthorize access');
      return redirect()->back();
    }

    $payment = Payment::with('userDetails')->where('professional_id', Auth::id());
    $key = [];
    if($request->all()){
      if($request->token_no){
        $payment = $payment->where('token_no', $request->token_no);
      }
      if($request->order_id){
        $payment = $payment->where('order_id', $request->order_id);
      }
      if($request->balance_status){
        $payment = $payment->where('balance_status', $request->balance_status);
      }
    }
    $key = $request->all();
    $payment = $payment->get();
    return view('modules.payment.payment')->with([
      "payment" =>  @$payment,
      "key"     =>  @$key
    ]);
  }
}
