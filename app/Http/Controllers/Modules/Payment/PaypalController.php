<?php

namespace App\Http\Controllers\Modules\Payment;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

use App\Models\Category;
use App\User;
use App\Models\Booking;
use App\Models\UserAvailability;
use App\Models\ProductCategory;
use App\Models\Timezone;
use App\Models\UserToCard;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use Moip\Moip;
use Moip\Auth\BasicAuth;
use Moip\Auth\OAuth;
use Moip\Resource\Customer;

use App\Mail\UserBooking;
use App\Mail\ProfessionalBooking;
use App\Models\Content;
use App\Models\Payment as PaymentModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Models\Coupon;
use App\Models\CouponToProduct;
use App\Models\ReferralDiscount;
use Illuminate\Support\Facades\Storage;
use App\Models\AdminBankAccount;
use App\Mail\BankAccountPayment;
use App\Mail\BankAccountPaymentProduct;
use App\Models\Commission;
use App\Mail\UserBankAccountBooking;
use App\Models\Product;
use App\Models\ProductOrder;
use App\Mail\ProfessionalProductOrder;
use App\Mail\UserBankAccountProductOrder;
use App\Mail\UserProductOrder;
use App\Models\Cart;
use App\Models\PaymentDetails;
use App\Models\ProductOrderDetails;
use Stripe\PaymentIntent;
use Stripe\Refund;

class PaypalController extends Controller
{
    private $_api_context;
    private $_ordID;
    
    public function __construct()
    {
        $paypal_configuration = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_configuration['client_id'], $paypal_configuration['secret']));
        $this->_api_context->setConfig($paypal_configuration['settings']);
    }

    public function payWithPaypal()
    {
        return view('paywithpaypal');
    }

    public function postPaymentWithpaypal(Request $request)
    {
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        // $this->_ordID = $request->get('ordID');
        // dd($request->get('ordID'));
        Session::put('ordID',encrypt($request->get('ordID')));

        // $unique_payment_id = decrypt($request->ordId);
        // $payments = ProductOrder::where('id', $unique_payment_id)->where('user_id',  @Auth::id())->first();

        // $userBooking = ProductOrder::where('token_no', $payments->token_no)->first()->load('orderDetails','profDetails', 'productDetails', 'productCategoryDetails');
        // $referDiscount = ReferralDiscount::first();
        // $checkTeacher = User::where('id', $userBooking->professional_id)->first();

    	$item_1 = new Item();

        $item_1->setName($request->get('title'))
            ->setCurrency(env('PAYPAL_CURRENCY'))
            ->setQuantity(1)
            ->setPrice($request->get('amount'));

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency(env('PAYPAL_CURRENCY'))
            ->setTotal($request->get('amount'));

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Enter Your transaction description');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(route('paypal.status'))
            ->setCancelUrl(route('paypal.status'));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));            
        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                // Session::put('error','Connection timeout');
                session()->flash('error', \Lang::get('client_site.connection_timeout'));
                return Redirect::route('paywithpaypal');
            } else {
                // Session::put('error','Some error occur, sorry for inconvenient');
                session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
                return Redirect::route('paywithpaypal');
            }
        }

        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        
        Session::put('paypal_payment_id', $payment->getId());

        if(isset($redirect_url)) {  
            // dd($redirect_url);
            return Redirect::away($redirect_url);
        }

        // Session::put('error','Unknown error occurred');
        session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
    	return Redirect::route('paywithpaypal');
    }

    public function getPaymentStatus(Request $request)
    {        
        $payment_id = Session::get('paypal_payment_id');
        $ordID = decrypt(Session::get('ordID'));

        $unique_payment_id = $ordID;
        $payments = ProductOrder::where('id', $unique_payment_id)->where('user_id',  @Auth::id())->first();
        // dd($payments);
        $userBooking = ProductOrder::where('token_no', $payments->token_no)->first()->load('orderDetails','profDetails', 'productDetails', 'productCategoryDetails');
        $referDiscount = ReferralDiscount::first();
        $checkTeacher = User::where('id', $userBooking->professional_id)->first();

        Session::forget('paypal_payment_id');
        if (empty($request->input('PayerID')) || empty($request->input('token'))) {
            // Session::put('error','Payment failed');
            session()->flash('error', \Lang::get('site.Payment_failed'));
            return Redirect::route('paywithpaypal');
        }
        $payment = Payment::get($payment_id, $this->_api_context);        
        $execution = new PaymentExecution();
        $execution->setPayerId($request->input('PayerID'));        
        $result = $payment->execute($execution, $this->_api_context);
        $res_arr = $result->toArray();
        $res = json_decode($result);

        if ($result->getState() == 'approved') {         
            // session()->flash('success', \Lang::get('site.Payment_successfull'));
            if ($userBooking->sub_total == 0) {
                $payment = PaymentModel::create([
                    'token_no'              =>  @$userBooking->token_no,
                    'order_id'              =>  'ORD-' . @$userBooking->token_no,
                    'user_id'               =>  @$userBooking->user_id,
                    'professional_id'       =>  @$userBooking->professional_id,
                    'order_type'            =>  'P',
                    'balance_status'        =>  'R'
                ]);
                if (@$payment) {
                    $UpdateBooking = ProductOrder::where([
                        'token_no'        =>  @$userBooking->token_no
                    ])->update([
                        'payment_status'  =>  "P",
                        // 'order_status'  =>  "A"
                    ]);
                    // $referralStatus = User::where('id', @$userBooking->user_id)->first();
                    // if (@$referralStatus->is_paid_activity == 'N') {
                    //     User::where('id', @$userBooking->user_id)->update(['is_paid_activity' => 'C']);
                    //     User::where('id', @$referralStatus->referrer_id)->increment("benefit_count", 1);
                    // }
                    $userdata = User::where('id', Auth::id())->first();
                    $userdata1 = User::where('id', @$userBooking->professional_id)->first();
                    $professionalmail = $userdata1->email;
                    $usermail = $userdata->email;
                    $bookingdata = $userBooking;
    
                    // // for send mail to user
                    // Mail::send(new UserBooking($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));
    
                    // // // for send mail to professional
                    // Mail::send(new ProfessionalBooking($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                    // for send mail to admin for Bank account payment
                    Mail::send(new UserBankAccountProductOrder($bookingdata->id, $usermail));
    
                    Mail::send(new BankAccountPaymentProduct($bookingdata->id));
                    return true;
                } else {
                    $UpdateBooking = ProductOrder::where([
                        'token_no'        =>  @$userBooking->token_no
                    ])->update([
                        'payment_status'  =>  "F"
                    ]);
                    return false;
                }
            } else {
                $total_amount         =  @$userBooking->sub_total;
                $payment = PaymentModel::create([
                    'token_no'              =>  @$userBooking->token_no,
                    'pg_order_id'           =>  'ORD-' . @$userBooking->token_no,
                    'user_id'               =>  @$userBooking->user_id,
                    'total_amount'          =>  @$total_amount,
                    'order_type'            =>  'P',
                    'pg_payment_id'         =>  $res_arr['id'],
                    'payment_status'        =>  'P',
                    'payment_type'          =>  'P',
                    'pg_response'           =>  json_encode($res),
                ]);
                // dd($payment);
                if (@$payment) {
                    $html='';
                    Cart::where('user_id', Auth::user()->id)->delete();
                    foreach (@$userBooking->orderDetails  as $orderDetails) {
                        $add = [];
                        $add['payment_master_id'] = $payment->id;
                        $add['product_order_details_id'] = $orderDetails->id;
                        $add['professional_amount'] = $orderDetails->professional_amount;
                        $add['admin_commission'] = $orderDetails->admin_commission;
                        $add['affiliate_commission'] = $orderDetails->affiliate_commission;
                        $add['affiliate_id'] = $orderDetails->affiliate_id;
                        $add['professional_id'] = $orderDetails->professional_id;
                        $add['type'] = 'P';
                        $add['withdraw_by_wirecard'] = 'N';
                        $add['balance_status'] = 'R';
                        $add['affiliate_withdraw_by_wirecard'] = 'N';
                        $add['affiliate_balance_status'] = 'R';
                        PaymentDetails::create($add);
                        $userdata = User::where('id', Auth::id())->first();
                        $userdata1 = User::where('id', @$orderDetails->professional_id)->first();
                        $professionalmail = $userdata1->email;
                        $usermail = $userdata->email;
                        $bookingdata = $orderDetails;
    
                        // for send mail to user
                        $profName= @$userdata1->nick_name ? @$userdata1->nick_name : @$userdata1->name;
                        $html=$html.'<div><br /><strong>Product Name:</strong>'. $bookingdata->product->title;
                        $html=$html.'<br /><strong>Product Category:</strong>'. $bookingdata->product->category->category_name;
                        $html=$html.'<br /><strong>purchase Date:</strong>'. toUserTime($userBooking->order_date,'Y-m-d');
                        $html=$html.'<br /><strong>Professional Name:&nbsp;</strong>'.$profName ;
                        $html=$html.'<br /><strong>Amount:&nbsp;</strong>'.$bookingdata->amount.'<br /><br /></div>';
                        // Mail::send(new UserProductOrder($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));
    
                        // for send mail to professional
                        Mail::send(new ProfessionalProductOrder($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                    }
                    $userdata = User::where('id', Auth::id())->first();
                    $usermail = $userdata->email;
                    $product_html=$html;
                    // dd($product_html);
                    Mail::send(new UserProductOrder($usermail, $userdata, $product_html));
                    $UpdateBooking = ProductOrder::where([
                        'token_no'        =>  @$userBooking->token_no
                    ])->update([
                        'payment_status'  =>  "P",
                        // 'order_status'  =>  "A"
                    ]);
                    // $response['success']['message'] = 'Payment Successfully';
                    // return response()->json($response);
                    session()->flash('success', \Lang::get('site.Payment_successfull'));
                    return Redirect::route('product.order.store.success', @$userBooking->token_no);
                } else {
                    $UpdateBooking = ProductOrder::where([
                        'token_no'        =>  @$userBooking->token_no
                    ])->update([
                        'payment_status'  =>  "F"
                    ]);
                    // Session::put('error','Payment Not Complete');
                    session()->flash('error', \Lang::get('site.Payment_failed'));
                    return Redirect::route('product.order.store.success', @$userBooking->token_no);
                    // $response['error']['message'] = 'Payment Not Complete';
                    // return response()->json($response);
                }
            }
            return Redirect::route('product.order.store.success', @$userBooking->token_no);
		    // return Redirect::route('paywithpaypal');
        }

        // Session::put('error','Payment failed !!');
        session()->flash('error', \Lang::get('site.Payment_failed'));
        $UpdateBooking = ProductOrder::where([
            'token_no'        =>  @$userBooking->token_no
        ])->update([
            'payment_status'  =>  "F"
        ]);
        return Redirect::route('product.order.store.success', @$userBooking->token_no);
		// return Redirect::route('paywithpaypal');
    }
}