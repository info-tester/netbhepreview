<?php

namespace App\Http\Controllers\Modules\Pusher;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Pusher\Pusher;
use App\Models\PusherMessageMaster;
use App\Models\PusherMessageDetail;
use App\Models\BookingToUser;
use App\Models\Booking;
use Auth;
use App\User;
use Storage;
use Mail;
class PusherController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
    *   Method      : message
    *   Description : This is use to message page
    *   Author      : Pankaj
    *   Date        : 29-12-2020
    **/
    public function message(Request $request)
    {
        $message_master_id=ProjectToUser::where('user_id',Auth::id())->pluck('message_master_id');
        // $data['user']=MessageMaster::whereIn('id',$message_master_id)->with('')
        // dd($booking_id);
        $data['user'] = MessageDetail::orderBy('created_at','DESC')->with('getProject:id,title,slug','getProjectToUser.getUser:id,username,online_status')->whereIn('message_master_id',$message_master_id);
        $data['user'] = $data['user']->get()->unique('message_master_id');
        $data['key'] = $request->all();
        // dd($data['user']);
        return view('modules.message.message')->with($data);
    }

    /**
    *   Method      : searchAjax
    *   Description : This is use to searchAjax
    *   Author      : Pankaj
    *   Date        : 2021-Jan-04
    **/
    public function searchAjax(Request $request){
        $response = [
            'jsonrpc' => '2.0'
        ];
        $message_master_id=BookingToUser::where('user_id',Auth::id())->pluck('message_master_id');
        if(@$request->data['keyword']){
            $user_id=User::whereNested(function($q) use ($request){
                $q->where('name', 'like', '%' . $request->data['keyword'] . '%')
                ->orwhere('username', 'like', '%' . $request->data['keyword'] . '%');
            })->where('id','!=',Auth::id())->pluck('id');
            $booking_id=Booking::where('title', 'like', '%' . $request->data['keyword'] . '%')->pluck('id');
            $message_master_id=BookingToUser::whereNested(function($q) use ($booking_id,$user_id){
                $q->whereIn('booking_id',$booking_id);
                $q->orwhereIn('user_id',$user_id);
            })->whereIn('pusher_message_master_id',$message_master_id)->pluck('pusher_message_master_id');
            // dd($user_id);
        }
        $data['user'] = MessageDetail::orderBy('created_at','DESC')->with('getProject:id,title,slug','getBookingToUser.getUser:id,username,online_status')->whereIn('pusher_message_master_id',$message_master_id);             
        $result= $data['user']->get()->unique('pusher_message_master_id');  
        $newres=[];  
        foreach($result as $res){
            $newres[]=$res;
        }
        $response['result']['user']=$newres;
        // dd($response);               
        return response()->json($response);
    }

    /**
    *   Method      : sendAjax
    *   Description : This is use to sendAjax page
    *   Author      : Pankaj
    *   Date        : 29-12-2020
    **/
    public function sendAjax(Request $request)
    {
        $pusher = new Pusher(env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'), [
            'cluster' => env('PUSHER_APP_CLUSTER')
        ]);
        $senderId = auth()->user()->id;
        // return $senderId;
        if(@$request->message_master_id){
            $msgMstrID=$request->message_master_id;
        }
        if(@$request->booking_id){
            //    $chk= PusherMessageMaster::where('booking_id',$request->booking_id)->whereIn('user_id',[$request->user_id,$senderId])->first();
            $chk= PusherMessageMaster::where('booking_id',$request->booking_id)->first();
            if(!$chk){
                $msgMstr = PusherMessageMaster::create(['booking_id'=>$request->booking_id, 'socket_id'=>$request->socket_id]);
                $msgMstrID = $msgMstr->id;
            } else {
                $msgMstrID = $chk->id;
            }
            $booking = Booking::where('id', $request->booking_id)->first();
            $chk1= BookingToUser::where('pusher_message_master_id',$msgMstrID)->where('user_id', $booking->user_id)->where('is_removable','N')->first();
            if(!$chk1){
                $userData['booking_id']=(int) $request->booking_id;
                $userData['user_id']=(int) $booking->user_id;
                $userData['pusher_message_master_id']=$msgMstrID;
                BookingToUser::create($userData);
            }
            $chk2= BookingToUser::where('pusher_message_master_id',$msgMstrID)->where('user_id',$senderId)->where('is_removable','N')->first();
            if(!$chk2){
                $userData['booking_id']=(int) $request->booking_id;
                $userData['user_id']=$senderId;
                $userData['pusher_message_master_id']=$msgMstrID;
                BookingToUser::create($userData);
            }
        }
        // dd($msgMstrID);
        // dd($request);
        
        if($request->file){
            $file = $request->file;
            $filename = time() . '-' . rand(1000, 9999) .'_'. $file->getClientOriginalName();
            Storage::putFileAs('public/uploads/message_files', $file, $filename);
        }

        $msgmaster=PusherMessageMaster::where('id',$msgMstrID)->first();
        $request->booking_id=$msgmaster->booking_id;
        $booking=Booking::where('id',$request->booking_id)->first();
        $link="<a href='".route('view.booked.user.details',@$booking->id)."' target='_blank'>".@$booking->token_no."</a>";
        $to_id=BookingToUser::where('pusher_message_master_id',$msgMstrID)->where('user_id','!=',$senderId)->pluck('user_id');
        $new = array();
        foreach($to_id as $t){
            $new[]=(int)$t;
        }
        $user = User::where('id',$to_id[0])->first();
        $username= $user->name;
        $category = $booking->parentCatDetails->name;
        $subcategory = @$booking->childCatDetails ? $booking->childCatDetails->name : "";

        // $userCount=(count($to_id)-1);
        // if($userCount>0){
        //     $username.=' +'.$userCount;
        // }
        // $toDetails = User::find($userId);
        $response = $pusher->trigger('netbhe', 'receive-event', [
            'to_id'         =>  $new,
            'from'          =>  auth()->user()->name,
            'online_status' =>  'Y',
            'file'          =>  @$filename,
            'booking_id'    =>  (int)@$request->booking_id,
            'token'         =>  @$booking->token_no,
            'link'          =>  @$link,
            'message'       =>  @$request->message ? $request->message : null,
            'username'      =>  $username,
            'pusher_message_master_id' => (int)  @$msgMstrID,
            'image'         =>  auth()->user()->profile_pic ? url('storage/app/public/uploads/profile_pic/'.'/'.auth()->user()->profile_pic) : url('public/frontend/images/blank.png'),
        ], $request->socket_id);
        // dd($response);
            $createData['pusher_message_master_id']  =  @$msgMstrID;
            $createData['booking_id']         =  @$request->booking_id ? $request->booking_id : 0;
            $createData['user_id']            =  $senderId;
            $createData['message']            =  @$request->message ? $request->message : null;
            $createData['file']               =  @$filename;
            PusherMessageDetail::create($createData);
        if ($response) {                
            if($request->booking_id && !$request->message_master_id){
                return response()->json([
                    'result' => [
                        'message_master_id' => @$msgMstrID,
                        'booking_id'=>(int)@$request->booking_id,
                        'online_status'  =>  'Y',
                        'username'       =>  $username,
                        'category'       =>  $category,
                        'subcategory'    =>  $subcategory,
                        'token'          =>$booking->token_no
                    ]
                ]);
            }
            return response()->json([
                'result' => [
                    'code' => 200,
                    'file'=>@$filename
                ]
            ]);
        } else {
            return response()->json([
                'error' => [
                    'message' => 'Cound\'nt send message.'
                ]
            ]);
        }
    }
    
    /**
    *   Method      : typingApi
    *   Description : This is use to typingApi page
    *   Author      : Pankaj
    *   Date        : 29-12-2020
    **/
    public function typingApi(Request $request)
    {
        $pusher = new Pusher(env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'), [
            'cluster' => env('PUSHER_APP_CLUSTER')
        ]);
        $params = $request->params;
        $response = $pusher->trigger('netbhe', 'start-end-typing', [
            'from' => $params['from'],
            'typing' => $params['typing'],
        ], @$params['socket_id']);
        if ($response) {
            $response = array(
                'result' => 'SUCCESS'
            );
            return response()->json($response);
        } else {
            return response()->json([
                'error' => [
                    'status' => [
                        'message' => 'Error',
                        'meaning' => 'Cound\'nt send message.',
                        'code' => '-13456'
                    ]    
                ]
            ]);
        }
    }
    
    /**
    *   Method      : typingAjax
    *   Description : This is use to typingAjax page
    *   Author      : Pankaj
    *   Date        : 29-12-2020
    **/
    public function typingAjax(Request $request)
    {
        $pusher = new Pusher(env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'), [
            'cluster' => env('PUSHER_APP_CLUSTER')
        ]);
        $params = $request->all();
        $to_id=BookingToUser::where('pusher_message_master_id',$params['message_master_id'])->pluck('user_id');
        $response = $pusher->trigger('netbhe', 'start-end-typing', [
            'from_id'   =>  Auth::id(),
            'to_id' => $to_id,
            'from' => $params['from'],
            'typing' => $params['typing'],
            'message_master_id' => (int) @$params['message_master_id'],
        ], $params['socket_id']);
        if ($response) {
            return response()->json([
                'result' => [
                    'code' => 200
                ]
            ]);
        } else {
            return response()->json([
                'error' => [
                    'message' => 'Cound\'nt send message.'
                ]
            ]);
        }
    }

    public function userMessage(Request $request)
    {
        $params = $request->all();
        $user_id=auth()->user()->id;
        $response = [
            'jsonrpc' => '2.0'
        ];
        $response['result'] = array();
        $master = PusherMessageMaster::where('id', @$params['message_master_id'])->first();
        $results= PusherMessageDetail::with('getUser')->where('pusher_message_master_id',@$params['message_master_id'])->get();
        foreach($results as $result){
            $result->created_at = toUserTime($result->created_at);
            array_push($response['result'], $result);
        }
        $response['booking_id'] = $master->getBooking->id;
        $response['token'] = $master->getBooking->token_no;
        $response['username'] = ($user_id == $master->getBooking->userDetails->id) ? $master->getBooking->profDetails->name : $master->getBooking->userDetails->name;
        // return $response['result']['created_at'];
        // $to_id=BookingToUser::where('pusher_message_master_id',@$params['message_master_id'])->pluck('user_id');
        // $users = User::whereIn('id',$to_id)->where('status','A')->where('id','!=',$user_id)->pluck('username');
        // $response['username'] = '';
        // foreach ($users as $key => $value) {
        //     $response['username'] = $response['username'] . $key>1 ? ',' : ''. $value;
        // }
        return response()->json($response);
    }

    public function getMessageMaster(Request $request)
    {
        $params = $request->all();
        $user_id=auth()->user()->id;
        $response = [
            'jsonrpc' => '2.0'
        ];
        $response['result'] = PusherMessageMaster::where('booking_id', @$params['booking_id'])->with('getBooking')->first();
        if(@$response['result']){
            // $response['category'] = $response['result']->getBooking->parentCatDetails->name;
            $dt = $response['result']->getBooking->date . " " . $response['result']->getBooking->start_time;
            $response['category'] = toUserTime($dt, 'd M, Y h:i A');
            if($response['result']->getBooking->sub_category_id > 0){
                $response['subcategory'] = $response['result']->getBooking->childCatDetails->name;
            }
            $to_id=BookingToUser::where('pusher_message_master_id',@$response['result']->id)->where('user_id','!=',$user_id)->pluck('user_id');
            $response['username']="";
            if(count($to_id)>0){
                $users = User::whereIn('id', $to_id)->get();
                foreach($users as $user){
                    $response['username'] .= $user->name . ", ";
                }
                $response['username'] = substr($response['username'], 0, -2);
                $response['users'] = $users;
            }
        } else {
            $booking = Booking::where('id', @$params['booking_id'])->first();
            $response['username'] = ($user_id == $booking->profDetails->id) ? $booking->userDetails->name : $booking->profDetails->name;
            // $response['category'] = ($user_id == $booking->profDetails->id) ? toUserTime($booking->created_at) : $booking->parentCatDetails->name;
            $dt = $booking->date . " " . $booking->start_time;
            $response['category'] = toUserTime($booking->dt, 'd M, Y h:i A');
            if($booking->sub_category_id > 0){
                $response['subcategory'] = $booking->childCatDetails->name;
            }
        }
        // $userCount=(count($to_id)-1);
        // if($userCount>0){
        //     $response['username'].=' +'.$userCount;
        // }
        return response()->json($response);
    }

    public function addUser(Request $request)
    {
        $user_id=auth()->user()->id;
        $user=User::whereNested(function($q) use ($request){
                        $q->where('email',$request->username)
                        ->orwhere('username',$request->username);
                    })->where('status','A');
        if(auth()->user()->user_type=='SS'){
            $user=$user->where('user_type','SS');
        }else{
            $user=$user->where('user_type','SP');
        }
        $user=$user->first();
        if($user){
            $chk= ProjectToUser::where('message_master_id',$request->message_master_id)
                                ->where('user_id',$user->id)
                                ->first();
            if($chk){
                return response()->json([
                    'error' => ['message' => $request->username.' already added.']
                ]);  
            }
            $msgmaster=MessageMaster::where('id',$request->message_master_id)->first();
            $userData['message_master_id']=$request->message_master_id;
            $userData['booking_id']=$msgmaster->booking_id;
            $userData['user_id']=$user->id;
            $userData['is_removable']="Y";
            $userData['added_by']=$user_id;
            ProjectToUser::create($userData);

            $notificationData['sender_id'] = Auth::id();
            $notificationData['receiver_id'] = $user->id;
            $notificationData['notification_title'] = Auth::user()->username." has invited you for group chart.";
            $notificationData['notification_msg'] = Auth::user()->username." has invited you for group chart.";
            $notificationData['page_link'] = route('message');
            sendNotification($notificationData);

            $maildata['email']  = $user->email;
            $maildata['name']   = $user->name;
            $maildata['profile_link']  = route('provider.view.profile',Auth::user()->username);
            if(Auth::user()->user_type=='SS'){
                $maildata['profile_link']  = route('employer.view.profile',Auth::user()->username);
            }
            $maildata['btn_link']  = route('message');
            $maildata['username'] = Auth::user()->username;
            Mail::send(new JoinMessage($maildata));   

            return response()->json([
                'success' => ['message' => $request->username.' Sucessfully added.','username'=>$user->username]
            ]);  
        }
        return response()->json([
            'error' => ['message' => 'Username not found.']
        ]);  
    }

    public function removeUser(Request $request)
    {
        $user_id=auth()->user()->id;
        $user=User::where('username',$request->username)->orwhere('email',$request->username)->where('status','A')->first();
        if($user){
            $chk= ProjectToUser::where(['message_master_id'=>$request->message_master_id,'added_by'=>$user_id,'user_id'=>$user->id,'is_removable'=>'Y'])->first();
            if($chk){
                ProjectToUser::where('id',$chk->id)->delete();
                return response()->json([
                    'success' => ['message' => $request->username.' Sucessfully remove.']
                ]);  
            } 
        }
        return response()->json([
            'error' => ['message' => 'Username not found.']
        ]);  
    }

    public function messagesPage(){
        $data['user'] = Auth::user();
        $bookings_to_user = BookingToUser::where('user_id', Auth::id())->groupBy('booking_id')->pluck('booking_id');
        $data['bookings'] = Booking::where(function($query) use ($bookings_to_user){
            $query = $query->where(['professional_id'=> Auth::id(), 'booking_type' => 'C'])->orWhereIn('id', $bookings_to_user);
        })->where('order_status', 'A')
            ->with('userDetails', 'getMsgMaster')
            ->orderBy('id','desc')
            ->get();
        $data['bookings_to_user'] = $bookings_to_user;
        return view('modules.messages.messages')->with($data);
    }

    public function closeChat(Request $request){
        $pusher = new Pusher(env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'), [
            'cluster' => env('PUSHER_APP_CLUSTER')
        ]);
        $message_master = PusherMessageMaster::where('id', $request->id)->first();
        if(@$message_master){
            $message_master->status = "CLOSED";
            $message_master->save();
            $response = $pusher->trigger('netbhe', 'close-event', [
                'booking_id'         =>  $message_master->booking_id,
                'message_master_id'  =>  $message_master->id,
            ], $request->socket_id);
            if($response){
                return response()->json([
                    'booking_id'         =>  $message_master->booking_id,
                    'message_master_id'  =>  $message_master->id,
                    'status'             =>  'success',
                ]);
            } else {
                return response()->json([
                    'status' => 'failure',
                ]);
            }
        }
        return response()->json([
            'status' => 'failure',
        ]);
    }
}