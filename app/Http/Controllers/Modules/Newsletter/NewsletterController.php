<?php

namespace App\Http\Controllers\Modules\Newsletter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Newsletter;
class NewsletterController extends Controller
{
    /*
    *Method: emailvalidation
    *Description: for inserting news letter
    *Author: Abhisek
    */
    public function insert(Request $request)
    {
        
        $response = [
            'jsonrpc' => '2.0',
        ];
        if(@$request->params['email']){
            if (!filter_var(@$request->params['email'], FILTER_VALIDATE_EMAIL)) {
                $response['error']=1;
                return response()->json($response);
            }
            $usr = Newsletter::where('email', $request->params['email'])->first();
            if($usr==null){
                Newsletter::create(["email" => $request->params['email']]);
                $response['success']=1;
            } 
            else {
               $response['error']=0;  
            }
            return response()->json($response);
        }
    }

}
