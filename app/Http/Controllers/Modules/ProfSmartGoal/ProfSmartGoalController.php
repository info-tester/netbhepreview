<?php

namespace App\Http\Controllers\Modules\ProfSmartGoal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tool360Master;
use App\Models\Tool360Details;
use App\Models\ToolsSmartGoalMaster;
use App\Models\ToolsSmartGoalDetails;
use App\Models\Booking;
use App\Models\UserToTools;
use validate;
use App\User;
use Auth;
use Mail;
use App\Mail\FormAnswered;


class ProfSmartGoalController extends Controller {


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }


   

    public function index() {


        $data = array();
        $data['title'] = 'Manage Smart goal';

        $nowtime = date('Y-m-d H:i:s');
        $tendaybef =date('Y-m-d H:i:s', strtotime("-3 days"));

       // $data['all_smart_goal'] = ToolsSmartGoalMaster::with('getUserData')->/*where('status','ACTIVE')->*/orderBy('id','DESC')->get();

        // $data['all_paid_users'] = Booking::select('user_id')->distinct()->with('userDetails')->where('professional_id',Auth::id())->where('payment_status','P')->get();

        $data['all_paid_users'] = Booking::select('user_id')->distinct()->with('userDetails')->where('professional_id',Auth::id())->where('payment_status','P')->whereBetween('created_at', [$tendaybef ,$nowtime])->get();



        $user_id = Auth::id();
        $admin_id = '1';

        $data['all_smart_goal'] = ToolsSmartGoalMaster::where(function($q) use ($user_id, $admin_id) {
            $q = $q->where('status', '!=', 'DELETED')
                    ->where(['added_by' => 'P','added_by_id' => $user_id ])
                    ->orWhere(['added_by' => 'A','added_by_id' => $admin_id ]);
        })
        ->orderBy('id','DESC')->with(['getUserData'])
        ->get();


        
        return view('modules.prof_smart_goal.index')->with($data);
    }

    /*
    @method         => create
    @description    => Form create in form_master table
    @author         => munmun
    @date           => 03/03/2020
    */
    public function create(Request $request) {
       
        $data = array();
        //$data['title'] = 'Add Form';
        //$data['button'] = 'Add';
        //$data['category'] = FormCategory::where('cat_type', 'FORM')->get();
        return view('modules.prof_smart_goal.create');
        //return view('admin.modules.product.create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request) {

        $nowtime = date('Y-m-d H:i:s');  

        //Current date 2020-02-03

        //$fridayNextWeek = date('Y-m-d', strtotime($request->day_of_week.' next week'));
        //Outputs 2020-02-14
        //pr1($fridayNextWeek);
        //die();
        
        // to get day name wise date
        if($request->day_of_week){
            $nextdate = date('Y-m-d', strtotime('next '.$request->day_of_week)); 
        } else{
            $nextdate =NULL;
        }
        //pr1($nextFriday);
        //die();

        //Outputs 2020-02-07


        

        $request->validate([
            'goal_title'            => 'required',
            'goal_point'            => 'required|numeric',
            'current_point'         => 'required|numeric',
            'unit_of_measure'       => 'required',
            'start_date'            => 'required',
            'end_date'              => 'required'            
        ]);

        //pr1($request->all());
        //die();


        $insert['goal_title']                = $request->goal_title;      
        $insert['goal_point']                = $request->goal_point;      
        $insert['current_point']             = $request->current_point;      
        $insert['unit_of_measure']           = $request->unit_of_measure;
        $insert['note']                      = $request->note;      
        $insert['reminder']                  = $request->reminder;      
        $insert['day_of_week']               = @$nextdate;      
        $insert['reminder_date']             = date('Y-m-d', strtotime($request->reminder_date));      
        $insert['start_date']                = date('Y-m-d', strtotime($request->start_date));    
        $insert['end_date']                  = date('Y-m-d', strtotime($request->end_date));    
        $insert['status']                    = 'ACTIVE';
        $insert['added_by']                  = 'P';
        $insert['added_by_id']               = Auth::id();

        ToolsSmartGoalMaster::create($insert);

        session()->flash('success', \Lang::get('client_site.goal_saved'));

        return redirect()->back();
        //return redirect(route('form.question.create',['form_id' => $last->id]));

    }


    /*
    @method         => edit
    @description    => Form edit view
    @author         => munmun
    @date           => 28/02/2020
    */
    public function edit($id) {
       
        $data = array();
        $data['details'] = ToolsSmartGoalMaster::find($id);
         //pr1($data['details']);
         //die();
        return view('modules.prof_smart_goal.edit')->with($data);
    }

    /*
    @method         => update
    @description    => Form update in form_master table
    @author         => munmun
    @date           => 28/02/2020
    */
    public function update($id, Request $request) {
        //pr1($id);
        //die();
        $request->validate([
            'goal_title'            => 'required',
            'goal_point'            => 'required|numeric',
            'current_point'         => 'required|numeric',
            'unit_of_measure'       => 'required',
            'start_date'            => 'required',
            'end_date'              => 'required'            
        ]);

        //$update['title']     = $request->title;

        $update['goal_title']                = $request->goal_title;      
        $update['goal_point']                = $request->goal_point;      
        $update['current_point']             = $request->current_point;      
        $update['unit_of_measure']           = $request->unit_of_measure;      
        $update['note']                      = $request->note;      
        $update['reminder']                  = $request->reminder;      
        $update['day_of_week']               = $request->day_of_week;      
        $update['reminder_date']             = date('Y-m-d', strtotime($request->reminder_date));      
        $update['start_date']                = date('Y-m-d', strtotime($request->start_date));    
        $update['end_date']                  = date('Y-m-d', strtotime($request->end_date));

        ToolsSmartGoalMaster::where('id',$id)->update($update);


        session()->flash('success', \Lang::get('client_site.goal_saved'));
        return redirect()->back();
    }


    public function show($id) {
        //pr1($id);
        //die();
        
        $details = ToolsSmartGoalMaster::with(['getUserData']
    )->find($id);
        //pr1($details->toArray());
        //die();
        return view('modules.prof_smart_goal.show', [
            'details'       => $details
            //'questions'  => $questions
            //'productImages' => $productImages,

        ]);

    }

    /**
    * Remove the specified resource from storage.
    *
    *  @param  int  $id
    *  @return \Illuminate\Http\Response
    */
    public function destroy($id) {

        // $data = UserToTools::where('tool_id', $id)->first();
        // if(@$data){
        //     //echo "not delete";
        //     session()->flash('error', 'Form not deleted because it is assigned user.');
        //     return redirect()->back();
        // } else {
        //     //echo "delete";
        //     FormMaster::where('id', $id)->delete();
        //     session()->flash('success', 'Form deleted successfully.');
        //     return redirect()->back();
        // }
    }



    public function profSmartGoalUserAssigne(Request $request){

        $data = $request->all();
        //pr1($data);
        //die();
        $nowtime = date('Y-m-d H:i:s');


        $smart_goal_master = ToolsSmartGoalMaster::where('id',$request->tool_smaert_goals_master_id)->first();

        // for get reminder_date
        if($smart_goal_master->reminder == "One time"){
            $reminder_date = $smart_goal_master->reminder_date;
        } elseif ($smart_goal_master->reminder == "weekly") {
           $reminder_date = $smart_goal_master->day_of_week;
        } else{
             $reminder_date = $nowtime;
        }  
        //$formdata = FormMaster::where('id',$request->form_id)->first();
        //pr1($data);
        //pr1($formdata->tool_type);
        //die();/

        if($data){
            if($request->user_id){
                foreach ($request->user_id as $val) {
                    
                    $insert = array();
                    $insert['user_id']          = $val;
                    $insert['professional_id']  = Auth::id();
                    $insert['tool_type']        = "SMART Goals";
                    $insert['tool_id']          = $request->tool_smaert_goals_master_id;
                    $insert['assign_date']      = $nowtime;
                    $insert['reminder_type']    = $smart_goal_master->reminder;
                    $insert['reminder_date']    = $reminder_date;
                    $insert['status']           = "INVITED";
                    
                    UserToTools::create($insert);
                }

                session()->flash('success', \Lang::get('client_site.user_assigned_successfully'));
                return redirect()->back();    
               
                // send mail to profession start
                    //Mail::send(new FormAnswered($usermail,$professionalmail));
                // send mail to profession end

            } else {   
                
                session()->flash('error', \Lang::get('site.Select_User'));
                return redirect()->back();
                //return redirect()->route('user.view.form');
            }      
        }
    }

    public function professional360eEvuUserAssigneView($id){


        $user_tool_data = UserToTools::where('tool_id',$id)->where('tool_type','360 evaluation')->first();
        $user_tool_id = $user_tool_data->id;

        $all_answered_data = Tool360Answered::with(['getAnsweredDetail','getAnsweredDetail.getAnswerTitle'])->where('user_to_tools_id',$user_tool_id)->first();
        
        //pr1($all_answered_data);
        //die();

        $ans = [];
        foreach ($all_answered_data->getAnsweredDetail as $key => $value) {
            $ans[$key]['y']                   = $value->score;
            $ans[$key]['name']                = @$value->getAnswerTitle->answer;
            $ans[$key]['color']               = @$value->getAnswerTitle->color_box;                                
        }
         //dd($ans);

        $get_all_anss = json_encode($ans);

        //pr1($get_all_anss);
        //die();

        return view('modules.professional_360evaluation.view',compact('get_all_anss','all_answered_data'));

    }


    public function profSmartGoalUserAssigneView($id){

        // $id tools_smart_goal_master table id 

        $all_smart_goal_details = ToolsSmartGoalDetails::with('getUserData')->where('smart_goal_master_id',$id)->get();

        return view('modules.prof_smart_goal.assigne_user_smartgold_view',compact('all_smart_goal_details'));
        
    }

    //18.3.20 smart goal status change
    public function profSmartGoalStatusUpdate($id){
        //pr1($id);
        //die();
        $data = ToolsSmartGoalMaster::find($id);        
      
        if(@$data->status == 'ACTIVE') {
            ToolsSmartGoalMaster::where('id', $data->id)->update(['status' => 'INACTIVE']);
            session()->flash('success', \Lang::get('client_site.status_updated_successfully'));
            return redirect()->back();
        }

        if(@$data->status == 'INACTIVE') {
            ToolsSmartGoalMaster::where('id', $data->id)->update(['status' => 'ACTIVE']);
            session()->flash('success', \Lang::get('client_site.status_updated_successfully'));
            return redirect()->back();
        }
    }



}
