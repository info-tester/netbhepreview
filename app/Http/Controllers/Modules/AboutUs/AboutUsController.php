<?php

namespace App\Http\Controllers\Modules\AboutUs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Aboutus;
class AboutUsController extends Controller
{
    public function index(){
    	$about = Aboutus::first();
    	return view('modules.about_us.about_us')->with([
    		'about'	=>	@$about
    	]);
    }
}
