<?php

namespace App\Http\Controllers\Modules\TestPayment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Moip\Moip ; 
use Moip\Auth\BasicAuth ;
use Moip\Auth\OAuth;
use Moip\Resource\Customer;
class PaymentController extends Controller
{
	public $cust;
	protected $userData;
    public function index(Request $request){
    	if($request->all()){

    		try{
				$access_token = 'e7aeb891cc064d37b67959a042cd6789_v2';
				$moip = new Moip(new OAuth($access_token), Moip::ENDPOINT_SANDBOX);
				
				/* $account  =  $moip->accounts()->get("MPA-05915BFAA8F8"); 
							For finding an Transparent Account.
				*/

				// For Creating New ptofessional
				$account = $moip->accounts()
					->setName('Fulano')
				  ->setLastName('De Tal')
				  ->setEmail('fulano@email2.com')
				  ->setIdentityDocument('4737283560', 'SSP', '2015-06-23')
				  ->setBirthDate('1988-12-30')
				  ->setTaxDocument('16262131000')
				  ->setType('MERCHANT')
				  ->setTransparentAccount(true)
				  ->setPhone(11, 66778899, 55)
				  ->addAlternativePhone(11, 66448899, 55)
				  ->addAddress('Rua de teste', 123, 'Bairro', 'Sao Paulo', 'SP', '01234567', 'Apt. 23', 'BRA')
				  ->setCompanyName('Empresa Teste', 'Teste Empresa ME')
				  ->setCompanyOpeningDate('2011-01-01')
				  ->setCompanyPhone(11, 66558899, 55)
				  ->setCompanyTaxDocument('69086878000198')
				  ->setCompanyAddress('Rua de teste 2', 123, 'Bairro Teste', 'Sao Paulo', 'SP', '01234567', 'Apt. 23', 'BRA')
				  ->setCompanyMainActivity('82.91-1/00', 'Atividades de cobranças e informações cadastrais')
				  ->create();
				  dd($account);
				  //MPA-05915BFAA8F8
    		}
    		catch(\Exception $e){
	    		dd($e);
	    	}
    	}
    	return view('modules.payment.pay');
    }

    public function customer(){
    	$access_token = 'e7aeb891cc064d37b67959a042cd6789_v2';
		$moip = new Moip(new OAuth($access_token), Moip::ENDPOINT_SANDBOX);
		
		/*$customer  =  $moip->customers()->get('CUS-J0RUJHT4DED6');
			For finding a customer Account.
		*/

		// For Creating New customer
    	$customer = $moip->customers()
    				->setOwnId(uniqid())
					->setFullname('Rabindra')
					->setEmail('fulano@email.com')
					->setBirthDate('1988-12-30')
					->setTaxDocument('22222222222')
					->setPhone(11, 66778899)
					->addAddress('BILLING','Test Street', 123 , ' Bairro ' , ' Sao Paulo ' , ' SP ' , ' 01234567 ' , 8 ) -> addAddress ( 'SHIPPING' , 'SHIPPING Test Street' , 123 , 'SHIPPING Neighborhood' , 'Sao Paulo' , 'SP' , ' 01234567' , 8 ) ->create();
					dd($customer);
		dd($moip->customers()
    				->setOwnId(uniqid())
					->setFullname('Rabindra')
					->setEmail('fulano@email.com')
					->setBirthDate('1988-12-30')
					->setTaxDocument('22222222222')
					->setPhone(11, 66778899)
					->addAddress('BILLING','Test Street', 123 , ' Bairro ' , ' Sao Paulo ' , ' SP' , ' 01234567 ' , 8 ) -> addAddress ( 'SHIPPING' , 'SHIPPING Test Street' , 123 , 'SHIPPING Neighborhood' , 'Sao Paulo' , 'SP' , ' 01234567' , 8 ));
		// CUS-J0RUJHT4DED6
    }

    public function order(){
    	$access_token = 'e7aeb891cc064d37b67959a042cd6789_v2';
		$moip = new Moip(new OAuth($access_token), Moip::ENDPOINT_SANDBOX);

		$this->userData = $moip->customers()
    				->setOwnId(uniqid())
					->setFullname('Rabindra')
					->setEmail('fulano@email.com')
					->setBirthDate('1988-12-30')
					->setTaxDocument('22222222222')
					->setPhone(11, 66778899)
					->addAddress('BILLING','Test Street', 123 , ' Bairro ' , ' Sao Paulo ' , ' SP ' , ' 01234567 ' , 8 )->addAddress('SHIPPING','SHIPPING Test Street', 123,'SHIPPING Neighborhood','Sao Paulo','SP','01234567',8)->create();
		

    	$order  =  $moip->orders()->setOwnId(uniqid())->addItem("bike 1",1,"skul",10000)->addItem ("bike 2",1,"sku2",11000)->setShippingAmount(3000)->setAddition(1000)->setDiscount(5000)->setCustomer($this->userData)->create();
    	dd($order);
    	//ORD-0Z0ICBLBQN45 order number
    	
    }

    public function addCreditCard(){

    	$access_token = 'e7aeb891cc064d37b67959a042cd6789_v2';
		$moip = new Moip(new OAuth($access_token), Moip::ENDPOINT_SANDBOX);

		$this->userData = $moip->customers()
    				->setOwnId(uniqid())
					->setFullname('Rabindra')
					->setEmail('fulano@email.com')
					->setBirthDate('1988-12-30')
					->setTaxDocument('22222222222')
					->setPhone(11, 66778899)
					->addAddress('BILLING','Test Street', 123 , 'Bairro' , 'Sao Paulo' , 'SP' , '01234567' , 8)->addAddress('SHIPPING','SHIPPING Test Street', 123,'SHIPPING Neighborhood','Sao Paulo','SP','01234567',8)->create();
    	$card  =  $moip->customers()
    					->creditCard()
    					->setExpirationMonth('05')
    					->setExpirationYear(2018)
    					->setNumber('4012001037141112')
    					->setCVC('123')
    					->setFullName('Jose Portador da Silva' )
    					->setBirthDate('1988-12-30' )
    					->setTaxDocument ('CPF' , '33333333333' ) 
    					->setPhone('55' , '11' , '66778899' )
    					->create($this->userData->getId());
    	dd($card);
    }

    /**
    * Method: addBankAccount
    * Description: This method is used to add bank account for professional account.
    */

    public function addBankAccount(Request $request){
    	if($request->all()){

			$access_token = '2e517f2e35fd4f4dbcac05f35511b02c_v2';
			
			$moip = new Moip(new OAuth($access_token), Moip::ENDPOINT_SANDBOX.'/v2/accounts/MPA-429028392FF1/bankaccounts');
			
	    	$account_id  =  "MPA-429028392FF1" ;
			$BANK_ACCOUNT  = $moip->BankAccount()
								  ->setBankNumber('237')
								  ->setAgencyNumber ('12345')
								  ->setAgencyCheckNumber('0')
								  ->setAccountNumber('12345678902')
								  ->setAccountCheckNumber('7')
								  ->setType('CHECKING')
								  ->setHolder('Demo Moip','622.134.533-24','CPF')
								  ->create($account_id);
			dd($BANK_ACCOUNT);
    	}
    	return view('modules.payment.create_ac');
    }

    /**
    * Method: payment
    * Description: This method is used to pay an particular order.
    */

    public function payment(Request $request){
    	if($request->all()){
    		$access_token = 'e7aeb891cc064d37b67959a042cd6789_v2';
			$moip = new Moip(new OAuth($access_token), Moip::ENDPOINT_SANDBOX);

			$this->userData = $moip->customers()
	    				->setOwnId(uniqid())
						->setFullname('Rabindra')
						->setEmail('fulano@email.com')
						->setBirthDate('1988-12-30')
						->setTaxDocument('22222222222')
						->setPhone(11, 66778899)
						->addAddress('BILLING','Test Street', 123 , ' Bairro ' , ' Sao Paulo ' , ' SP ' , ' 01234567 ' , 8 )->addAddress('SHIPPING','SHIPPING Test Street', 123,'SHIPPING Neighborhood','Sao Paulo','SP','01234567',8)->create();
			
	    	$order  =  $moip->orders()->setOwnId(uniqid())->addItem("bike 1",1,"skul",10000)->addItem ("bike 2",1,"sku2",11000)->setShippingAmount(3000)->setAddition(1000)->setDiscount(5000)->setCustomer($this->userData)->create();

	    	try{
	    		$hash  = "OEnEFsBE/lNDnFM1y7ZR4mV4hL8rith6zJdZ88nrYh4pPR7/+A1pet4UeELSHevg2OKQ1GN8pZxcmZ6Uk3EU469EtoNsjBXJ3B32rvDNr93AnXt/2qa2PS+lRHNkeJAyAPNQ0SXTt9O4bc4KmUrNZw3pSh3GXqGn0/kR1rXW+U2QcjZvZ7aoL5HWnklq3RIXfNjBhbLeGFzlb3uSOF+bK2IXcXx702nWCCb9V8lLswVKUByz/uDfs7yBJtvoH2qbWkYfjh3aHmXRE6Da8yemm5ugY0c/a2ACftQf8858YsAwwCrc9zVsqs8Ud0TSAn+FIp/EWomDrvN/XA+DH4b5xA=="; 

	    	// dd($order);
	    	$holder  =  $moip->holders()->setFullname('Jose Silva')->setBirthDate ("1990-10-10")->setTaxDocument('622.134.533-24','CPF')->setPhone(11,66778899,55)->setAddress('BILLING','Avenida Faria Lima', '2927','Itaim' , 'Sao Paulo' , 'SP' , '01234000', 'Apt 101');
			$payment  =  $order ->payments()
	         ->setCreditCardHash($hash , $holder)
	         ->setInstallmentCount(1)
	         ->setStatementDescriptor("My Store");
	         // ->execute();
	         dd($payment);
	    	}
	    	catch(\Exception $e){
	    		dd($e->getMessage());
	    	}
    	}
    	return view('modules.payment.payment');
    }

    public function paynew(){

    	$data = array (
          'installmentCount' => 1,
          'statementDescriptor' => 'netbhe.com consulting charges.',
          'fundingInstrument' => 
          array (
            'method' => 'CREDIT_CARD',
            'creditCard' => 
            array (
              'id' => @$card->card_id,
              'store' => true,
              'holder' => 
              array (
                'fullname' => @$card->card_name,
                'birthdate' => @$user->dob,
                'taxDocument' => 
                array (
                  'type' => 'CPF',
                  'number' => @$user->cpf_no,
                ),
                'phone' => 
                array (
                  'countryCode' => @$user->countryName->phonecode,
                  'areaCode' => @$user->area_code,
                  'number' => @$user->mobile,
                ),
                'billingAddress' => 
                array (
                  'city' => @$user->city,
                  'district' => @$user->district,
                  'street' => @$user->street_name,
                  'streetNumber' => @$user->street_number,
                  'zipCode' => @$user->zipcode,
                  'state' => substr(@$user->stateName->name,0,2),
                  'country' => substr(@$user->countryName->name, 0,2),
                ),
              ),
            ),
          ),
        );
        $crl = curl_init();
        $header = array();
        $header[] = 'Content-length: 0';
        $header[] = 'Content-type: application/json';
        $header[] = 'Authorization: Bearer e7aeb891cc064d37b67959a042cd6789_v2';
        curl_setopt($crl, CURLOPT_URL,"https://sandbox.moip.com/v2/orders//payments");
		curl_setopt($crl, CURLOPT_POST, 1);
        curl_setopt($crl, CURLOPT_POST,true);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
		curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($crl, CURLOPT_HTTPHEADER,$header);
		$server_output = curl_exec($crl);
        curl_close($crl);
        return dd($server_output);
    }

    public function notification(){
    	$notification  =  $moip->notifications ()
     ->addEvent ( 'ORDER. *' )
     ->addEvent ( 'PAYMENT.AUTHORIZED' )
     ->addEvent ( 'PAYMENT.CANCELLED' )
     ->setTarget ( 'http: / /requestb.in/1dhjesw1' )
     ->create();
    }

    public function paymentAuthorized(){
    //   $js = 
    }
}
