<?php

namespace App\Http\Controllers\Modules\Professional360Evaluation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tool360Master;
use App\Models\Tool360Details;
use App\Models\Tool360AnsweredDetail;
use App\Models\Tool360Answered;
use App\Models\Booking;
use App\Models\UserToTools;

use App\Models\CompetencesMaster;
use App\Models\TypeOfEvaluationMaster;
use App\Models\TypeEvaluationToCompetences;


use validate;
use App\User;
use Auth;
use Mail;
use App\Mail\FormAnswered;


class Professional360EvaluationController extends Controller {


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }


   

   public function index()
    {

        $data = array();
        $data['title'] = 'Manage 360Evaluation';

        $nowtime = date('Y-m-d H:i:s');
        $tendaybef =date('Y-m-d H:i:s', strtotime("-3 days"));

    //     $data['all360data'] = Tool360Master::with(['get360Details','getUserData']
    // )/*->where('status','ACTIVE')*/->orderBy('id','DESC')->get();

        $user_id = Auth::id();
        $admin_id = '1';

        $data['all360data'] = Tool360Master::where('shown_in_proff', 'Y')->where(function($q) use ($user_id, $admin_id) {
            $q = $q->where('status', '!=', 'DELETED')
                    ->where(['added_by' => 'P','added_by_id' => $user_id ])
                    ->orWhere(['added_by' => 'A','added_by_id' => $admin_id ]);
        })
        ->orderBy('id','DESC')->with(['get360Details','getUserData'])
        ->get();

        $data['all_paid_users'] = Booking::select('user_id')->distinct()->with('userDetails')->where('professional_id',Auth::id())->where('payment_status','P')->get();

        $data['all_paid_users'] = Booking::select('user_id')->distinct()->with('userDetails')->where('professional_id',Auth::id())->where('payment_status','P')->whereBetween('created_at', [$tendaybef ,$nowtime])->get();
        
        return view('modules.professional_360evaluation.index')->with($data);
    }

    /*
    @method         => create
    @description    => Form create in form_master table
    @author         => munmun
    @date           => 28/02/2020
    */
    public function create(Request $request) {
       
        $data = array();
        //$data['title'] = 'Add Form';
        //$data['button'] = 'Add';
        $data['all_competencesmaster'] = CompetencesMaster::get();
        $data['all_type_evaluation'] = TypeOfEvaluationMaster::get();
        // pr1($data['all_type_evaluation']);
        // die();
        return view('modules.professional_360evaluation.create')->with($data);
        //return view('admin.modules.product.create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request) {  

         
       
        $request->validate([
            'title'            => 'required'            
        ]);

        $insert['title']                     = $request->title;      
        $insert['status']                    = 'ACTIVE';
        $insert['added_by']                  = 'P';
        $insert['added_by_id']               = Auth::id();
        $last = Tool360Master::create($insert);

        if($request->answer){

            foreach ($request->answer as $key => $value) {
                Tool360Details::create([
                    'id_360master'       =>  $last->id,
                    'answer'             =>  $value,
                    'color_box'          =>  $request->color_box[$key]
                ]);
            }
        }   

        session()->flash('success', \Lang::get('client_site.form_saved'));

        return redirect()->back();
        //return redirect(route('form.question.create',['form_id' => $last->id]));

    }


    /*
    @method         => edit
    @description    => Form edit view
    @author         => munmun
    @date           => 28/02/2020
    */
    public function edit($id) {
       
        $data = array();
        $data['details'] = Tool360Master::with('get360Details')->find($id);
         
        return view('modules.professional_360evaluation.edit')->with($data);
    }

    /*
    @method         => update
    @description    => Form update in form_master table
    @author         => munmun
    @date           => 28/02/2020
    */
    public function update($id, Request $request) {
        //pr1($id);
        //die();
        $request->validate([
            'title'            => 'required'            
        ]);

        $update['title']     = $request->title;

        Tool360Master::where('id',$id)->update($update);


        // insert to Tool360Master details
        if(@$request->answer){
            Tool360Details::where('id_360master',$id)->delete($id);
            foreach ($request->answer as $key => $val) {
                $insert['id_360master']   = $id; 
                $insert['answer']         = $val;                   
                $insert['color_box']         = $request->color_box[$key];                   
                Tool360Details::create($insert);
            }
            Tool360Details::whereNull('answer')->delete();
        }


        session()->flash('success', \Lang::get('client_site.form_saved'));
        return redirect()->back();
    }


    public function show($id) {
        
        $details = Tool360Master::with(['get360Details','getUserData']
    )->find($id);
        //pr1($details->get360Details->toArray());
        //die();

        if($details->get360Details){
            $ans = [];
            foreach ($details->get360Details as $key => $value) {
                $ans[$key]['y']                   = 10;
                $ans[$key]['name']                = @$value->answer;
                $ans[$key]['color']               = @$value->color_box;                                
            }
             //dd($ans);
            $get_all_anss = json_encode($ans);
            //pr1($get_all_anss);
            //die();
        }

        return view('modules.professional_360evaluation.show', [
            'details'       => $details,
            'get_all_anss'  => $get_all_anss
            //'questions'  => $questions
            //'productImages' => $productImages,

        ]);

    }

    /**
    * Remove the specified resource from storage.
    *
    *  @param  int  $id
    *  @return \Illuminate\Http\Response
    */
    public function destroy($id) {

        // $data = UserToTools::where('tool_id', $id)->first();
        // if(@$data){
        //     //echo "not delete";
        //     session()->flash('error', 'Form not deleted because it is assigned user.');
        //     return redirect()->back();
        // } else {
        //     //echo "delete";
        //     FormMaster::where('id', $id)->delete();
        //     session()->flash('success', 'Form deleted successfully.');
        //     return redirect()->back();
        // }
    }



    public function professional360eEvuUserAssigne(Request $request){

        $data = $request->all();
        $nowtime = date('Y-m-d H:i:s');

        //$formdata = FormMaster::where('id',$request->form_id)->first();
        //pr1($data);
        //pr1($formdata->tool_type);
        //die();/

        if($data){
            if($request->user_id){
                foreach ($request->user_id as $val) {
                    
                    $insert = array();
                    $insert['user_id']          = $val;
                    $insert['professional_id']  = Auth::id();
                    $insert['tool_type']        = "360 evaluation";
                    $insert['tool_id']          = $request->tool_360_master_id;
                    $insert['assign_date']      = $nowtime;
                    $insert['status']           = "INVITED";
                    
                    UserToTools::create($insert);
                }

                session()->flash('success', \Lang::get('client_site.user_assigned_successfully'));
                return redirect()->back();    
               
                // send mail to profession start
                    //Mail::send(new FormAnswered($usermail,$professionalmail));
                // send mail to profession end

            } else {   
                
                session()->flash('error', \Lang::get('site.Select_User'));
                return redirect()->back();
                //return redirect()->route('user.view.form');
            }      
        }
    }

    public function professional360eEvuUserAssigneView($id){


        $user_tool_data = UserToTools::where('tool_id',$id)->where('tool_type','360 evaluation')->first();
        
        if($user_tool_data){

            $user_tool_id = $user_tool_data->id;

            //pr($user_tool_id);
            //die();

            $all_answered_data = Tool360Answered::with(['getAnsweredDetail','getAnsweredDetail.getAnswerTitle'])->where('user_to_tools_id',$user_tool_id)->first();
            
            // pr1($all_answered_data);
            // die();
            if($all_answered_data){
                $ans = [];
                foreach ($all_answered_data->getAnsweredDetail as $key => $value) {
                    $ans[$key]['y']                   = $value->score;
                    $ans[$key]['name']                = @$value->getAnswerTitle->answer;
                    $ans[$key]['color']               = @$value->getAnswerTitle->color_box;                                
                }
                 //dd($ans);
                $get_all_anss = json_encode($ans);
            }else{
                $get_all_anss = null;
            }
        }else{

            $get_all_anss = null;
            $all_answered_data =null;
        }    

        //pr1($get_all_anss);
        //die();

        return view('modules.professional_360evaluation.view',compact('get_all_anss','all_answered_data'));
    }

    // getting all assign user list
    public function professional360eEvuUserAssigneUserList($id){

        $data ['all_user_data'] = UserToTools::with('getprofessionalUserData')->where('tool_id',$id)->where('tool_type','360 evaluation')->get();
        
        return view('modules.professional_360evaluation.user_assign_list')->with($data);
    }



    // function fetchCompetencesAll(Request $request) {

    //     $all_competencesmaster = CompetencesMaster::get();
    //     $all_type_evaluation = TypeOfEvaluationMaster::get();

    //     // $all_competencesmaster_ids = CompetencesMaster::select('id')->get();

    //     $value = $request->get('value');
    //     // $data = TypeEvaluationToCompetences::where('evaluation_type_id', $value)->get();

    //     $all_competences_ids = TypeEvaluationToCompetences::select('competences_id')->where('evaluation_type_id', $value)->get();
    //     $id = [];
    //     foreach ($all_competences_ids as $row) {
    //         $id[] = $row->competences_id;
    //     }
    //     // $id1 = implode(',', $id);
       
    //     $output = '<div class="your-mail competences" id="competences">
    //                     <label for="exampleInputEmail1" class="personal-label">Select the type of evaluation:</label>';
    //     foreach($all_competencesmaster as $row)
    //     {
    //         if(@$id && in_array($row->id, $id)) {
    //             $output .= '<input type="checkbox" id="'.$row->title.'" name="competences" checked>
    //                 <label for="'.$row->title.'">'.$row->title.'</label> ';
    //         } else {
    //             $output .= '<input type="checkbox" id="'.$row->title.'" name="competences">
    //                 <label for="'.$row->title.'">'.$row->title.'</label> ';
    //         }
    //     }

    //     $output .= '
    //         </div>
    //     ';
    //     echo $output;
    // }


    public function tools360StatusUpdate($id){
        //pr1($id);
        //die();
        $data = Tool360Master::find($id);        
      
        if(@$data->status == 'ACTIVE') {
            Tool360Master::where('id', $data->id)->update(['status' => 'INACTIVE']);
            session()->flash('success', \Lang::get('client_site.form_inactivated'));
            return redirect()->back();
        }

        if(@$data->status == 'INACTIVE') {
            Tool360Master::where('id', $data->id)->update(['status' => 'ACTIVE']);
            session()->flash('success', \Lang::get('client_site.form_activated'));
            return redirect()->back();
        }

    }



}
