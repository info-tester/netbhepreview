<?php

namespace App\Http\Controllers\Modules\ImportingTools;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserToTools;
use App\Models\UserFormAnswered;
use App\Models\FormMaster;
use App\Models\FormCategory;
use App\Models\Booking;
use App\Models\FormMasterDetails;
use App\Models\ImportedTool;
use App\Models\ImportedToolFeedback;
use validate;
use App\User;
use Auth;
use Mail;
use App\Mail\FormAnswered;


class ImportingToolsController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }


   

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request) { 

        $nowtime = date('Y-m-d H:i:s');
        $tendaybef =date('Y-m-d H:i:s', strtotime("-3 days"));

        $data['all_paid_users'] = Booking::select('user_id')->distinct()->with('userDetails')->where('professional_id',Auth::id())->where('payment_status','P')->whereBetween('created_at', [$tendaybef ,$nowtime])->get();

        // $data['importeds'] = ImportedTool::where('status', '!=', 'DELETED')->orderBy('id','DESC')->with(['user','category'])->get();
        // dd($data['details']);

        $user_id = Auth::id();
        $admin_id = '1';

        $data['importeds'] = ImportedTool::where('status', '!=', 'DELETED')->where('shown_in_proff', 'Y')->where(function($q) use ($user_id, $admin_id) {
            $q = $q->where('status', '!=', 'DELETED')
                    ->where(['added_by' => 'P','added_by_id' => $user_id ])
                    ->orWhere(function($qq) use ($user_id, $admin_id) {
                        $qq = $qq->where(['added_by' => 'A','added_by_id' => $admin_id ])
                                ->whereHas('specialities', function( $query ) use ( $user_id ){
                                    $query = $query->where('professional_specialty_id', Auth::user()->professional_specialty_id);
                                });
                    });
            })
            ->orderBy('id','DESC')->with(['user','category'])
            ->get();
        
        return view('modules.importing_tools.index')->with($data);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create(Request $request) {
       
        $data = array();
        //$data['title'] = 'Add Form';
        //$data['button'] = 'Add';

        //$data['category'] = FormCategory::where('cat_type', 'IMPORTED')->get(); 
        $data['category'] = FormCategory::get();       
        return view('modules.importing_tools.create')->with($data);
        //return view('admin.modules.product.create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request) {        
       
        $request->validate([
            'title'            => 'required',
            'cat_id'           => 'required',
            'desc'             => 'required'
        ]);
        $insert['title']                = $request->title;
        $insert['cat_id']               = $request->cat_id;
        $insert['description']          = $request->desc;
        $insert['help_tools_view']      = @$request->help_tools_view;
        //$insert['status']                    = 'Form';
        $insert['status']               = 'ACTIVE';
        $insert['added_by']             = 'P';
        $insert['added_by_id']          = Auth::id();
        ImportedTool::create($insert);
        session()->flash('success', \Lang::get('client_site.imported_tool_saved_successfully'));
        return redirect()->back();

    }


    //product Details Show

    public function show($id) {

        $details = ImportedTool::with(['category','user'])->find($id);

        //$questions = FormMasterDetails::where('form_master_id',$id)->get();
       

        //pr1($data->toArray());
        //die();
        return view('modules.importing_tools.show', [
            'details'       => $details
        ]);

    }



    /**
    * Show the form for editing a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function edit($id) {

        //pr1($id);
        //die();
        $data = array();
        //$data['title'] = 'Edit Form';
        //$data['button'] = 'Edit';

        //$data['category'] = FormCategory::where('cat_type', 'IMPORTED')->get();
        $data['category'] = FormCategory::get();
        $data['details'] = ImportedTool::where('id', $id)->first();
        //pr($data['details']->toArray());
        //die();
        return view('modules.importing_tools.edit')->with($data);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id) {
        
        //pr($id);
        //die();
        $request->validate([
            'title'            => 'required',
            'cat_id'           => 'required',
            'desc'             => 'required'
        ]);
        $insert['title']                = $request->title;
        $insert['cat_id']               = $request->cat_id;
        $insert['description']          = $request->desc;
        $insert['help_tools_view']      = @$request->help_tools_view;
        ImportedTool::where('id',$id)->update($insert);
        session()->flash('success', \Lang::get('client_site.imported_tool_saved_successfully'));
        return redirect()->back();

    }

    /**
    * Remove the specified resource from storage.
    *
    *  @param  int  $id
    *  @return \Illuminate\Http\Response
    */
    public function destroy($id) {

        $data = UserToTools::where('tool_id', $id)->first();
        if(@$data){
            //echo "not delete";
            session()->flash('error', \Lang::get('client_site.cannot_delete_imp_tool_assigned'));
            return redirect()->back();
        } else {
            //echo "delete";
            ImportedTool::where('id', $id)->delete();
            session()->flash('success', \Lang::get('client_site.form_deleted_successfully'));
            return redirect()->back();
        }

        

       
     
    }



    public function professionalImportingToolsAssigne(Request $request){

        $data = $request->all();
        $nowtime = date('Y-m-d H:i:s');

        //$formdata = FormMaster::where('id',$request->form_id)->first();
        //pr1($data);
        //pr1($formdata->tool_type);
        //die();



        if($data){
            if($request->user_id){
                foreach ($request->user_id as $val) {
                    
                    $insert = array();
                    $insert['user_id']          = $val;
                    $insert['professional_id']  = Auth::id();
                    $insert['tool_type']        = "Imported Tools";
                    $insert['tool_id']          = $request->import_tools_id;
                    $insert['assign_date']      = $nowtime;
                    $insert['status']           = "INVITED";
                    
                    UserToTools::create($insert);
                }

                session()->flash('success', \Lang::get('client_site.user_assigned_successfully'));
                return redirect()->back();    
               
                // send mail to profession start
                    //Mail::send(new FormAnswered($usermail,$professionalmail));
                // send mail to profession end
            } else {   
                
                session()->flash('error', \Lang::get('site.Select_User'));
                return redirect()->back();
                //return redirect()->route('user.view.form');
            }      
        }
    }




    /*for professional ImportingToolsView section start*/

    public function professionalImportingToolsView(Request $request){
        //pr1($request->form_id);
        //die();

        $formdata = UserToTools::with('getprofessionalUserData')->where(['professional_id' => Auth::id(),'tool_id' => $request->import_tools_id,'tool_type' => 'Imported Tools'])->get();
        //pr1($formdata->toArray());       
        //die();

        return view('modules.importing_tools.professional_form_view')->with([
            'formdata'   => @$formdata
        ]);
    }

    public function professionalImportingToolsViewDeatils($id){

        $data['details'] = UserToTools::with(['getUserData','getImportingToolsdata','getImportingToolsdata.category'])->find($id);

        return view('modules.importing_tools.user_submit_imporedtools_show')->with($data);        
    }

    /*for professional ImportingToolsView section end*/

    public function updateStatus($id){

        $nowtime = date('Y-m-d H:i:s');

        UserToTools::where('id', $id)->update(['status' => 'COMPLETED','completion_date' => $nowtime]);
                
        session()->flash('success', \Lang::get('client_site.status_updated_successfully'));
        return redirect()->route('user.view.form'); 

    }


    public function duplicateImportedTools($id){

        $data = ImportedTool::find($id);
        //pr1($data->toArray());
        //die();

        $insert['title']                = $data->title;
        $insert['cat_id']               = $data->cat_id;
        $insert['description']          = $data->description;
        //$insert['status']                    = 'Form';
        $insert['status']               = 'ACTIVE';
        $insert['added_by']             = 'P';
        $insert['added_by_id']          = Auth::id();
        ImportedTool::create($insert);
        session()->flash('success', \Lang::get('client_site.status_updated_successfully'));
        return redirect()->back();
    }

    public function viewAllUserFeedback($id){

       $professional_id = Auth::id();
       $data['allfeedback'] = ImportedToolFeedback::where(['imported_tool_id' => $id, 'professional_id' => $professional_id, 'tool_type' => "Imported Tools"])->with(['getUserData'])->get();
       //pr1($data['allfeedback']->toarray());
       //die();
       return view('modules.importing_tools.alluser_feedback')->with($data);
    }


    public function postFeedbackTouser(Request $request){

        $nowtime = date('Y-m-d H:i:s');
        $id = $request->imported_tool_feedback_id;

        $request->validate([            
            'professional_feedback'      => 'required'
        ]);      

        if($request->professional_feedback){
            ImportedToolFeedback::where('id', $id)->update([
                'status'                            => 'COMPLETED',
                'professional_feedback_date'        => $nowtime,
                'professional_feedback'             => $request->professional_feedback
            ]);

            session()->flash('success', \Lang::get('client_site.feedback_added_successfully'));
            return redirect()->back();
        } else {
            session()->flash('error', \Lang::get('client_site.enter_your_feedback'));
            return redirect()->back();
        }      

    }


    // for user start 10.3.20

    // for  view user assign importns tools
    public function userImportingToolsView($id){

        //$category = FormCategory::where('cat_type', 'IMPORTED')->get();
        $category = FormCategory::get();
        $user_tool = UserToTools::with(['getUserData','getImportingToolsdata'])->find($id);
        
        $details = ImportedTool::with(['category','user'])->where('id',$user_tool->tool_id)->first();
        
        return view('modules.importing_tools.user_edit')->with([            
            'details'   => @$details,
            'category'   => @$category,
            'user_tool'   => @$user_tool,

        ]);

    }

    public function userImportingToolsUpdate(Request $request,$id){
        // pr1($request->user_tools_id);
        // die();

        $request->validate([            
            'desc'             => 'required'
        ]);
       

        $nowtime = date('Y-m-d H:i:s');

        UserToTools::where('id', $request->user_tools_id)->update(['status' => 'COMPLETED','completion_date' => $nowtime,'user_imoprt_tools_ans_desc' => $request->desc]);
                
        session()->flash('success', \Lang::get('client_site.status_updated_successfully'));
        return redirect()->route('user.view.form'); 

    }


    public function userImportingToolsAnsShow($id){

        $data['details'] = UserToTools::with(['getUserData','getImportingToolsdata','getImportingToolsdata.category'])->find($id);
        
        //pr1($data['details']->toArray());
        //die();
        return view('modules.importing_tools.user_answer_show')->with($data);
    }


    public function userImportingToolsFeedback(Request $request){
        $nowtime = date('Y-m-d H:i:s');

        $request->validate([            
            'user_feedback'             => 'required'
        ]);

        $user_tools = UserToTools::where('id',$request->user_to_tools_id)->first();
        
        if($request->user_feedback){
            $insert['user_to_tools_id']         = $request->user_to_tools_id;
            $insert['imported_tool_id']         = $user_tools->tool_id;
            $insert['professional_id']          = $user_tools->professional_id;
            $insert['status']                   = 'INCOMPLETE';
            $insert['tool_type']                = 'Imported Tools';
            $insert['user_id']                  = $user_tools->user_id;
            $insert['user_feedback']            = $request->user_feedback;
            $insert['user_feedback_date']       = $nowtime;
            ImportedToolFeedback::create($insert);

            session()->flash('success', \Lang::get('client_site.feedback_added_successfully'));
            return redirect()->back();
        } else {
            session()->flash('error', \Lang::get('client_site.enter_your_feedback'));
            return redirect()->back();
        }      

    }


    // for user end


    public function importedStatusUpdate($id){
        
        $data = ImportedTool::find($id);        
      
        if(@$data->status == 'ACTIVE') {
            ImportedTool::where('id', $data->id)->update(['status' => 'INACTIVE']);
            session()->flash('success', \Lang::get('client_site.status_updated_successfully'));
            return redirect()->back();
        }

        if(@$data->status == 'INACTIVE') {
            ImportedTool::where('id', $data->id)->update(['status' => 'ACTIVE']);
            session()->flash('success', \Lang::get('client_site.status_updated_successfully'));
            return redirect()->back();
        }

    }



}
