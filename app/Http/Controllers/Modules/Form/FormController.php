<?php

namespace App\Http\Controllers\Modules\Form;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserToTools;
use App\Models\UserFormAnswered;
use App\Models\FormMaster;
use App\Models\FormMasterDetails;
use validate;
use App\User;
use Auth;
use Mail;
use App\Mail\FormAnswered;


class FormController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    // public function termsOfservices(){
    // 	$termsOfservices = Content::where('id', 3)->first();
    // 	return view('modules.content.terms_of_services')->with([
    // 		'termsOfservices' 	=> @$termsOfservices
    // 	]);
    // }

    public function userViewForm()
    {

        $formdata = UserToTools::with(['getUserData', 'getImportingToolsdata', 'getImportedToolsFeedback', 'getlogbookDetails', 'getContractTemplatesdata', 'getSmartGoalsData'])->where('user_id', Auth::id())->orderBy('id', 'desc')->get();
        //$formdata = UserToTools::with(['getUserData','getImportingToolsdata','getImportedToolsFeedback','getlogbookDetails'])->where('user_id',Auth::id())->orderBy('id', 'desc')->paginate(20);
        //pr1($formdata->toArray());
        //die();
        return view('modules.form.form_view')->with([
            'formdata'   => @$formdata
        ]);
    }

    public function userViewFormDetails($id)
    {

        $data = UserToTools::with('getFormData')->find($id);
        $form_data = FormMaster::with('category')->where('id', $data->tool_id)->first();

        return view('modules.form.form_view_details')->with([
            'data'          => @$data,
            'form_data'     => @$form_data
        ]);
    }

    public function userViewFormPost(Request $request)
    {

        $data = $request->all();
        $nowtime = date('Y-m-d H:i:s');

        $user_tools_data = UserToTools::where('id', $request->user_to_tools_id)->first();
        $professional = User::where('id', $user_tools_data->professional_id)->first();
        $user = User::where('id', $user_tools_data->user_id)->first();

        $usermail = $user->email;
        $professionalmail = $professional->email;

        if ($data) {
            foreach ($request->textfield as $key => $val) {
                $getform_details = FormMasterDetails::where('id', $request->form_details_id[$key])->first();
                // dd($val);
                $insert = array();
                $insert['user_to_tools_id'] = $request->user_to_tools_id;
                $insert['form_details_id'] = $request->form_details_id[$key];
                $insert['question'] = $request->question[$key];
                if ($getform_details->field_type == 'MULTISELECT') {
                    $multi_ans = array();
                    foreach ($val as $row) {
                        $multi_ans[] = $row;
                    }
                    // dd($multi_ans);
                    $insert['answer'] = json_encode($multi_ans);
                } else {
                    $insert['answer'] = $val;
                }

                UserFormAnswered::create($insert);
            }
            // dd($data);

            // send mail to profession start
            Mail::send(new FormAnswered($usermail, $professionalmail));
            // send mail to profession end

            UserToTools::where('id', $request->user_to_tools_id)->update(['status' => 'COMPLETED', 'completion_date' => $nowtime]);

            session()->flash('success', \Lang::get('client_site.answer_submit_successfully'));
            return redirect()->route('user.view.form');
        }
    }


    public function userViewFormSubmitDetails($id)
    {

        // for get form master id
        $user_tools_data = UserToTools::where('id', $id)->first();
        // for get form details
        $form_data = FormMaster::with('category')->where('id', $user_tools_data->tool_id)->first();

        //$form_data = FormMaster::with('category')->where('id',$data->tool_id)->first();
        $answerdata = UserFormAnswered::with('getFormDetailsData')->where('user_to_tools_id', $id)->get();

        return view('modules.form.view_user_submited_answer')->with([
            'answerdata'   => @$answerdata,
            'form_data'   => @$form_data,
            'user_tools_data'   => @$user_tools_data
        ]);
    }


    /*for professional section start*/

    public function professionalViewForm(Request $request)
    {

        $formdata = UserToTools::with('getprofessionalUserData')->where(['professional_id' => Auth::id(), 'tool_id' => $request->form_id])->get();

        return view('modules.form.professional_form_view')->with([
            'formdata'   => @$formdata
        ]);
    }

    /*for professional section end*/
}