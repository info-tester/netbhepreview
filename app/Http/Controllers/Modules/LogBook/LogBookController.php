<?php

namespace App\Http\Controllers\Modules\LogBook;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserToTools;
use App\Models\UserFormAnswered;
use App\Models\FormMaster;
use App\Models\FormCategory;
use App\Models\Booking;
use App\Models\FormMasterDetails;
use App\Models\ImportedTool;
use App\Models\ContractTemplatMaster;

use App\Models\UserLogBookAnswer;
use App\Models\LogBookMaster;
use App\Models\LogBookDate;
use validate;
use App\User;
use Auth;
use Mail;
use App\Mail\FormAnswered;


class LogBookController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }


   

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request) { 

        $nowtime = date('Y-m-d H:i:s');
        $tendaybef =date('Y-m-d H:i:s', strtotime("-3 days"));

        // $data['all_paid_users'] = Booking::select('user_id')->distinct()->with('userDetails')->where('professional_id',Auth::id())->where('payment_status','P')->get();

        $data['all_paid_users'] = Booking::select('user_id')->distinct()->with('userDetails')->where('professional_id',Auth::id())->where('payment_status','P')->whereBetween('created_at', [$tendaybef ,$nowtime])->get();

        $user_id = Auth::id();
        $admin_id = '1';

        $data['all_log_book'] = LogBookMaster::where('shown_in_proff', 'Y')->where(function($q) use ($user_id, $admin_id) {
            $q = $q->where('status', '!=', 'DELETED')
                    ->where(['added_by' => 'P','added_by_id' => $user_id ])
                    ->orWhere(['added_by' => 'A','added_by_id' => $admin_id ]);
        })
        ->orderBy('id','DESC')->with(['user'])
        ->get();
         //dd($data['details']);
        
        return view('modules.logbook.index')->with($data);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create(Request $request) {
       
        $data = array();               
        return view('modules.logbook.create');
        //return view('admin.modules.product.create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request) {        
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];

        $request->validate([                        
            'question'    => 'required'
        ]);

        // $insert['title']                = $request->title;        
        // $insert['contract_desc']        = $request->contract_desc;
        // //$insert['status']                    = 'Form';
        // $insert['status']               = 'ACTIVE';
        // $insert['added_by']             = 'P';
        // $insert['added_by_id']          = Auth::id();
        // ContractTemplatMaster::create($insert);

        if($request->question){
            foreach ($request->question as $key => $value) {
                LogBookMaster::create([                    
                    'question'           =>  $value,
                    'added_by'           =>  "P",
                    'added_by_id'        =>  Auth::id(),
                    'status'             =>  "ACTIVE"                    
                ]);
            }
        }   

        $jsn['status'] = "success";
        $jsn['message'] = "Logbook question added successfully.";
        return response()->json($jsn);
        // session()->flash('success', 'Logbook question added successfully.');
        // return redirect()->back();

    }


    // Details Show

    public function show($id) {

        // $details = ContractTemplatMaster::with(['user'])->find($id);
        // return view('modules.logbook.show', [
        //     'details'       => $details
        // ]);

    }



    /**
    * Show the form for editing a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function edit($id) {
        
        $data = array();        
        $data['details'] = LogBookMaster::where('id', $id)->first();        
        return view('modules.logbook.edit')->with($data);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id) {
        
        $request->validate([
            'question'            => 'required' 
        ]);

        $insert['question']                = $request->question;        
       
        LogBookMaster::where('id',$id)->update($insert);
        session()->flash('success', \Lang::get('client_site.question_updated_successfully'));
        return redirect()->back();

    }

    /**
    * Remove the specified resource from storage.
    *
    *  @param  int  $id
    *  @return \Illuminate\Http\Response
    */
    public function destroy($id) {

        $data = UserToTools::where('tool_id', $id)->where('tool_type', "Log Book")->first();
        if(@$data){
            //echo "not delete";
            session()->flash('error', \Lang::get('client_site.assigned_logbook_not_del'));
            return redirect()->back();
        } else {
            //echo "delete";
            LogBookMaster::where('id', $id)->delete();
            session()->flash('success', \Lang::get('client_site.logbook_deleted_successfully'));
            return redirect()->back();
        }

        

       
     
    }


    // for contract temp assign in user
    public function profLogbookUserAssigne(Request $request){

        $data = $request->all();
        //pr1($data);
        //die();
        $nowtime = date('Y-m-d H:i:s');

        if($data){
            if($request->user_id){

                if($request->allquestionid){

                    foreach ($request->user_id as $val) {
                        
                        $insert = array();
                        $insert['user_id']          = $val;
                        $insert['professional_id']  = Auth::id();
                        $insert['tool_type']        = "Log Book";
                        $insert['tool_id']          = $request->log_book_master_id;
                        $insert['assign_date']      = $nowtime;
                        $insert['status']           = "INVITED";
                        
                        $latest = UserToTools::create($insert);

                        // for insert into user_log_book_answered table

                        foreach ($request->allquestionid as $key => $row) {

                            $insert1 = array();
                            $insert1['user_to_tools_id']    = $latest->id;
                            $insert1['log_book_master_id']  = $row;
                            $insert1['question']            = $request->question_name[$key];                            
                            UserLogBookAnswer::create($insert1);
                        }


                        $userdata = User::where('id',$val)->first(); //for get user data

                        $userdata1 = User::where('id',Auth::id())->first(); //for get profession data
                        $usermail           = $userdata->email;
                        $professionalmail   = $userdata1->email;

                        //send mail to user from professional start
                            Mail::send(new FormAnswered($usermail,$professionalmail));
                        //send mail to user from professional end
                    }
                   
                    

                    session()->flash('success', \Lang::get('client_site.user_assigned_successfully'));
                    return redirect()->back(); 

                }else{
                    session()->flash('error', \Lang::get('client_site.question_select'));
                    return redirect()->back();
                }    


            } else {   
                
                session()->flash('error', \Lang::get('site.Select_User'));
                return redirect()->back();
                //return redirect()->route('user.view.form');
            }      
        }
    }


    public function profLogbookUserAssignebackup24_3_20(Request $request){

        $data = $request->all();
        pr1($data);
        die();
        $nowtime = date('Y-m-d H:i:s');

        if($data){
            if($request->user_id){
                foreach ($request->user_id as $val) {
                    
                    $insert = array();
                    $insert['user_id']          = $val;
                    $insert['professional_id']  = Auth::id();
                    $insert['tool_type']        = "Log Book";
                    $insert['tool_id']          = $request->log_book_master_id;
                    $insert['assign_date']      = $nowtime;
                    $insert['status']           = "INVITED";
                    
                    UserToTools::create($insert);
                }

                session()->flash('success', \Lang::get('client_site.user_assigned_successfully'));
                return redirect()->back();    
               
                // send mail to profession start
                    //Mail::send(new FormAnswered($usermail,$professionalmail));
                // send mail to profession end
            } else {   
                
                session()->flash('error', \Lang::get('site.Select_User'));
                return redirect()->back();
                //return redirect()->route('user.view.form');
            }      
        }
    }

    

    // list of logbook assigned user list 16.3.20
    public function profLogbookUserAssigneList(){
        
        $data['all_user_data'] = UserToTools::with(['getprofessionalUserData','getlogbookAnswerDetails'])->where(['professional_id' => Auth::id(),'tool_type' => 'Log Book'])->orderBy('id','DESC')->get();
        return view('modules.logbook.prof_logbook_assign_userlist')->with($data);
    }

    // public function profLogbookUserAssigneList($id){
        
    //     $data['all_user_data'] = UserToTools::with(['getprofessionalUserData','getlogbookAnswerDetails'])->where(['professional_id' => Auth::id(),'tool_id' => $id,'tool_type' => 'Log Book'])->get();
    //     return view('modules.logbook.prof_logbook_assign_userlist')->with($data);
    // }


//for User Logbook 16.3.20 start

    // for log book details
    public function userLogbookView($id){        

        $data['details'] = UserToTools::with(['getUserData','getlogbookDetails','getlogbookAnswerDetails'])->find($id);
        return view('modules.logbook.user_logbook_show')->with($data);
    }

    //question list 24.3.20
    public function userLogbookQuestionList($id){

        $user_id = Auth::id();

        // $admin_id = '1';
        // $data['all_questionlist'] = LogBookMaster::where('shown_in_proff', 'Y')->where(function($q) use ($user_id, $admin_id) {
        //     $q = $q->where('status', '!=', 'DELETED')
        //             ->where(['added_by' => 'P','added_by_id' => $user_id ])
        //             ->orWhere(['added_by' => 'A','added_by_id' => $admin_id ]);
        // })
        // ->orderBy('id','DESC')->with(['user'])
        // ->get();



        $data['all_questionlist'] = UserLogBookAnswer::where('user_to_tools_id',$id)->whereNull('log_book_date_id')->get();

        //pr1($data['all_questionlist']->toArray());
        //die();    

        $data['user_to_tools_id'] = $id;
        return view('modules.logbook.user_logbook_questionlist')->with($data);
    }

    //for log book answer submit
    public function userLogbookAnswerPost(Request $request) {       
        $alldata = $request->all();
        //pr1($alldata);
        //die();
        $nowtime = date('Y-m-d H:i:s');

        $request->validate([            
            'answer'             => 'required'
        ]);

        
        if($request->answer){

            $insert['user_id']      = Auth::id();          
            $insert['ans_date']     = date('Y-m-d', strtotime($request->ans_date));            
            $latest = LogBookDate::create($insert);
            
                foreach ($request->answer as $key => $val) {
                    $insert1['log_book_date_id']         = $latest->id;
                    $insert1['user_to_tools_id']         = $request->user_to_tools_id;
                    $insert1['log_book_master_id']       = $request->log_book_master_id[$key];                
                    $insert1['answer']                   = $val;           
                    $insert1['question']                 = $request->question[$key];           
                    UserLogBookAnswer::create($insert1);

                    //UserLogBookAnswer::where(['user_to_tools_id'=>$request->user_to_tools_id , 'log_book_master_id' => $request->log_book_master_id[$key]])->update($update);
                }
               
            //UserToTools::where('id', $request->user_to_tools_id)->update(['status' => 'COMPLETED','completion_date' => $nowtime]);

            session()->flash('success', \Lang::get('client_site.answer_added_successfully'));
            return redirect()->back();
        } else {
            session()->flash('error', \Lang::get('client_site.enter_your_answer'));
            return redirect()->back();
        }
    }


    public function userLogbookAnswerPostbackup_24_3_20(Request $request){
        
        $nowtime = date('Y-m-d H:i:s');

        $request->validate([            
            'answer'             => 'required'
        ]);

        $user_tools = UserToTools::where('id',$request->user_to_tools_id)->first();
        
        if($request->answer){
            $insert['user_to_tools_id']         = $request->user_to_tools_id;
            $insert['log_book_master_id']       = $user_tools->tool_id;
            /*$insert['professional_id']          = $user_tools->professional_id;           
            $insert['user_id']                  = $user_tools->user_id;*/
            $insert['answer']                   = $request->answer;           
            $insert['question']                 = $request->loogbook_question;           
            UserLogBookAnswer::create($insert);

            UserToTools::where('id', $request->user_to_tools_id)->update(['status' => 'COMPLETED','completion_date' => $nowtime]);

            session()->flash('success', \Lang::get('client_site.answer_added_successfully'));
            return redirect()->back();
        } else {
            session()->flash('error', \Lang::get('client_site.enter_your_answer'));
            return redirect()->back();
        }
    }


// for user logbook end 16.3.20



    public function logbookStatusUpdate($id){
        

        $data = LogBookMaster::find($id);        
      
        if(@$data->status == 'ACTIVE') {
            LogBookMaster::where('id', $data->id)->update(['status' => 'INACTIVE']);
            session()->flash('success', \Lang::get('client_site.logbook_inactivated'));
            return redirect()->back();
        }

        if(@$data->status == 'INACTIVE') {
            LogBookMaster::where('id', $data->id)->update(['status' => 'ACTIVE']);
            session()->flash('success', \Lang::get('client_site.logbook_activated'));
            return redirect()->back();
        }

    }


//25.3.20

    public function logbookUserAssigneAnsDate($userid){

        $data['logbookdata'] = LogBookDate::with('getUserData')->where('user_id',$userid)->get();

        $avlDay = LogBookDate::where('user_id', $userid)->pluck('ans_date')->toArray();
        $data['avlDay'] = json_encode($avlDay);

        $avlDay1 = LogBookDate::where('user_id', $userid)->get();
        $data['avlTime1'] = json_encode($avlDay1);

        //pr1($data['avlTime1']);
        //die();

        return view('modules.logbook.prof_logbook_assign_user_ansdate')->with($data);       
    }

    public function logbookUserAssigneQuestionAns($id){
        
        $data['logbook_date'] = LogBookDate::find($id);
        $data['logbook_que_ans_list'] = UserLogBookAnswer::where('log_book_date_id',$id)->get();
        
        return view('modules.logbook.view_user_submited_ques_answer')->with($data);       
    }



    // for user data 25.03.20

    public function logbookUsergetAllAnsDate(){

        $data['logbookdata'] = LogBookDate::with('getUserData')->where('user_id',Auth::id())->get();

        //pr1($data['logbookdata']->toArray());
        //die();

        $avlDay = LogBookDate::where('user_id', Auth::id())->pluck('ans_date')->toArray();
        $data['avlDay'] = json_encode($avlDay);

        $avlDay1 = LogBookDate::where('user_id', Auth::id())->get();
        $data['avlTime1'] = json_encode($avlDay1);

        return view('modules.logbook.user_logbook_all_ansdate')->with($data);
    }

    // get details userlogbook data
    public function logbookUserQuestionAnsDetails($id){
        
        $data['logbook_date'] = LogBookDate::find($id);
        $data['logbook_que_ans_list'] = UserLogBookAnswer::where('log_book_date_id',$id)->get();
        
        return view('modules.logbook.view_user_ques_answer_details')->with($data);       
    }

}
