<?php

namespace App\Http\Controllers\Modules\Affiliate;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use validate;
use App\User;
use App\Models\ProfessionalToQualification;
use App\Models\ProfessionalToExperience;
use App\Models\UserAvailability;
use App\Models\UserToLanguage;
use App\Models\UserToCard;
use App\Models\Language;
use App\Models\Timezone;
use App\Models\Booking;
use App\Models\Payment;
use Soumen\Agent\Agent;
use Soumen\Agnet\Services\Device;
use Soumen\Agent\Services\Platform;
use App\Models\Country;
use App\Models\State;
use App\Models\Bank;
use App\Models\Category;
use App\Models\City;
use App\Models\Content;
use App\Models\ProfessionalSpecialty;
use App\Models\UserToBankAccount;
use App\Models\UserToCategory;
use App\Models\CallDuration;
use App\Models\Experience;
use App\Models\PaymentDetails;
use App\Models\VideoAffEarning;
use App\Models\VideoAffPayment;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Moip\Moip;
use Moip\Auth\BasicAuth;
use Moip\Auth\OAuth;
use DateTime;
use DatePeriod;
use DateInterval;
use Cookie;


class AffiliateController extends Controller
{
    protected $adminAccessToken;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('videoAffiliateShareLink');

        $this->adminAccessToken = getAdminToken();
    }

    public function index(Request $request)
    {

        if(Auth::user()->is_join_affiliate=='Y'){
            if(Auth::user()->is_professional != "Y"){
                return redirect()->route('load_user_dashboard');
            }
            return redirect()->route('prof.dash.my.booking',['type'=>'UC']);
        }


        $country = Country::orderBy('name')->get();
        $state = State::orderBy('name')->get();
        $bank = UserToBankAccount::where('user_id', Auth::id())->first();
        $bankList = Bank::orderBy('bank_name')->get();

        if ($request->all()) {
            $request->validate([
                'country'    =>  "required",
            ]);
            if(Auth::user()->is_professional == "Y" && Auth::user()->is_approved=='Y'){
                $upd1=[];
                $upd1['is_join_affiliate']='Y';
                User::where('id', Auth::id())->update($upd1);
                session()->flash('success',  \Lang::get('client_site.joined_affiliate_program'));
                if(Auth::user()->is_professional != "Y"){
                    return redirect()->route('load_user_dashboard');
                }
                return redirect()->route('prof.dash.my.booking',['type'=>'UC']);
            }
            if($request->country!=30){
                $request->validate([
                    'paypal_address'    =>  "required",
                ]);
                $upd=[];
                $upd['paypal_address']=$request->paypal_address;
                $upd['country_id']=$request->country;
                $upd['is_join_affiliate']='Y';
                User::where('id', Auth::id())->update($upd);
                session()->flash('success', \Lang::get('client_site.joined_affiliate_program'));

                if(Auth::user()->is_professional != "Y"){
                    return redirect()->route('load_user_dashboard');
                }
                return redirect()->route('prof.dash.my.booking',['type'=>'UC']);
            }
            if($request->country==30){
                $request->validate([
                    'first_name'    =>  "required",
                    'last_name'     =>  "required",
                    'phone_no'      => 'required|unique:users,mobile,' . Auth::id(),
                    'city'          =>  "required",
                    'state'         =>  "required",
                    'dob'           =>  "required",
                    'street_number' =>  "required",
                    'street_name'   =>  "required",
                    'district'      =>  "required",
                    'zipcode'       =>  "required",
                    'area_code'     =>  "required",
                    'complement'    =>  "required",
                    'cpf_no'        =>  "required_if:country,=,30|unique:users,cpf_no," . Auth::id(),

                ]);
                if (@$request->cpf_no) {
                    $this->CPF_CNPJ = $request->cpf_no;
                    if (!$this->getNumeral()) {
                        session()->flash('error', \Lang::get('client_site.invalid_cpf_number'));
                        return redirect()->back()->withInput($request->input());
                    }
                }
                $data = [
                    'name'          =>  $request->first_name . ' ' . $request->last_name,
                    'mobile'        =>  $request->phone_no,
                    'country_id'    =>  $request->country,
                    'state_id'      =>  $request->state,
                    'city'          =>  $request->city,
                    'dob'           =>  @$request->dob ? date('Y-m-d', strtotime($request->dob)) : NULL,
                    'street_number' =>  $request->street_number,
                    'street_name'   =>  $request->street_name,
                    'complement'    =>  $request->complement,
                    'district'      =>  $request->district,
                    'area_code'     =>  $request->area_code,
                    'zipcode'       =>  $request->zipcode,
                    'cpf_no'        =>  @$request->cpf_no,

                ];
                User::where('id', Auth::id())->update($data);
                $request->validate(
                    [
                        'bank_name'                 =>  'required',
                        'account_number'            =>  'required',
                        'agency_number'             =>  'required',
                        'bank_number'               =>  'required',
                        'account_check_number'      =>  'required',
                        'account_type'              =>  'required',
                    ],
                    [
                        'bank_name.required' =>  'Nome do banco é obrigatório',
                        'account_number.required' =>  'Número da conta é obrigatório',
                        'agency_number.required' =>  'Número da agência é obrigatório',
                        'bank_number.required' =>  'Número do banco é obrigatório',
                        'account_check_number'      =>  'required',
                        'account_type.required' =>  'O tipo de conta é obrigatório',
                    ]
                );
                UserToBankAccount::where('user_id', Auth::id())->delete();
                $create = UserToBankAccount::create([
                    'user_id'                 =>  Auth::id(),
                    'bankName'                =>  $request->bank_name,
                    'accountNumber'           =>  $request->account_number,
                    'agencyNumber'            =>  $request->agency_number,
                    'bank_number'             =>  $request->bank_number,
                    'accountCheckNumber'      =>  @$request->account_check_number,
                    'account_type'            =>  $request->account_type
                ]);
                $User = User::find(Auth::id());
                try {
                    // creating wirecard account for professional
                    // $moip = new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_SANDBOX);
                    if (env('PAYMENT_ENVIORNMENT') == 'live') {
                        $moip = new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_PRODUCTION);
                    } else if (env('PAYMENT_ENVIORNMENT') == 'sandbox') {
                        $moip = new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_SANDBOX);
                    }
                    $name = explode(' ', @$User->name);
                    $nc = "";
                    for ($i = 1; $i < sizeof($name); $i++) {
                        if ($i > 1) {
                            $nc .= ' ' . @$name[$i];
                        } else {
                            $nc .= @$name[$i];
                        }
                    }
                    $account = $moip->accounts()
                        ->setName(@$name[0])
                        ->setLastName(@$nc)
                        ->setEmail(@$User->email)
                        ->setBirthDate(@$User->dob)
                        ->setTaxDocument(@$User->cpf_no)
                        ->setType('MERCHANT')
                        ->setTransparentAccount(true)
                        ->setPhone(@$User->area_code, @$User->mobile, @$User->countryName->phonecode)
                        ->addAlternativePhone(@$User->area_code, @$User->mobile, @$User->countryName->phonecode)
                        ->addAddress($User->street_name, @$User->street_number, @$User->district, @$User->city, substr(@$User->stateName->name, 0, 2), @$User->zipcode, @$User->complement, substr(@$User->countryName->name, 0, 3))
                        ->create();
                    if (!@$account->getId()) {
                        session()->flash('error', \Lang::get('client_site.wirecard_error'));
                        return redirect()->back();
                    }
                    // updating professional status.
                    // Now creating bank account.....
                    $ret = $this->createBankAccount(@$User->id, @$account->getId(), @$account->getAccessToken());
                    if ($ret == 0) {
                        session()->flash('error', \Lang::get('client_site.wirecard_error'));
                        return redirect()->back();
                    }
                    User::where('id', $User->id)->update([
                        'professional_id'               => @$account->getId(),
                        'professional_access_token'     => @$account->getAccessToken(),
                        'is_join_affiliate'             => 'Y'
                    ]);
                    session()->flash('success',  \Lang::get('client_site.joined_affiliate_program'));
                    if(Auth::user()->is_professional != "Y"){
                        return redirect()->route('load_user_dashboard');
                    }
                    return redirect()->route('prof.dash.my.booking',['type'=>'UC']);
                } catch (\Exception $e) {
                    // dd($e->getMessage());
                    session()->flash('error', \Lang::get('client_site.wirecard_error'));
                    return redirect()->back();
                }
            }
        }



        // if(Auth::user()->is_professional == "Y" && Auth::user()->is_approved=='Y'){
        //     $upd1=[];
        //     $upd1['is_join_affiliate']='Y';
        //     User::where('id', Auth::id())->update($upd1);
        //     session()->flash('success', \Lang::get('client_site.joined_affiliate_program'));
        //     if(Auth::user()->is_professional != "Y"){
        //         return redirect()->route('load_user_dashboard');
        //     }
        //     return redirect()->route('prof.dash.my.booking',['type'=>'UC']);
        // }
        if (Auth::user()->is_professional == "Y") {

            return view('modules.affiliate.affiliate_profile')->with([
                'country'   =>  @$country,
                'state'     =>  @$state,
                'bank'      =>  @$bank,
                'bankList'  =>  @$bankList,
            ]);
        } else {
            if (Auth::user()->is_professional == "N") {
                return view('modules.affiliate.affiliate_profile')->with([
                'country'   =>  @$country,
                'state'     =>  @$state,
                'bank'      =>  @$bank,
                'bankList'  =>  @$bankList,
                ]);
            }
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        return view('modules.affiliate.affiliate_profile');
    }
    /**
     *method: createbankaccount()
     *purpose: For creating bank account
     */

    public function createBankAccount($user_id, $id, $token)
    {
        $bank = UserToBankAccount::where('user_id', $user_id)->first();
        $user = User::where('id', $user_id)->first();
        if (env('PAYMENT_ENVIORNMENT') == 'live') {
            // $moip = new Moip(new OAuth($token), Moip::ENDPOINT_PRODUCTION . '/v2/accounts/' . $id . '/bankaccounts');
            $moip = new Moip(new OAuth($token), Moip::ENDPOINT_PRODUCTION);
        } else if (env('PAYMENT_ENVIORNMENT') == 'sandbox') {
            // $moip = new Moip(new OAuth($token), Moip::ENDPOINT_SANDBOX . '/v2/accounts/' . $id . '/bankaccounts');
            $moip = new Moip(new OAuth($token), Moip::ENDPOINT_SANDBOX);
        }
        $bankAccount = $moip->bankaccount()
            ->setBankNumber(@$bank->bank_number)
            ->setAgencyNumber(@$bank->agencyNumber)
            ->setAgencyCheckNumber(@$bank->agencyCheckNumber)
            ->setAccountNumber(@$bank->accountNumber)
            ->setAccountCheckNumber(@$bank->accountCheckNumber) //commented for client requirement
            ->setType(@$bank->account_type)
            ->setHolder(@$user->name, @$user->cpf_no, 'CPF')
            ->create($id);
        // dd($bankAccount);
        if (!@$bankAccount->getId()) {
            return 0;
        } else {
            UserToBankAccount::where('user_id', $user_id)->update([
                'account_id'        => @$bankAccount->getId(),
                'bank_number'       => @$bankAccount->getBankNumber()
            ]);
            return 1;
        }
    }

    /**
    ######################For Validating CPF NUMBER#########################################
     */

    public function verificaDigitos($digito1, $digito2, $ver, $ver2)
    {
        $num = $digito1 % 11;
        $digito1 = ($num < 2) ? 0 : 11 - $num;
        $num2 = $digito2 % 11;
        $digito2 = ($num2 < 2) ? 0 : 11 - $num2;
        if ($digito1 === (int) $this->numeral[$ver] && $digito2 === (int) $this->numeral[$ver2]) {
            return true;
        } else {
            return false;
        }
    }
    private function verificaCPF()
    {
        $num1 = ($this->numeral[0] * 10) + ($this->numeral[1] * 9) + ($this->numeral[2] * 8) + ($this->numeral[3] * 7) + ($this->numeral[4] * 6)
            + ($this->numeral[5] * 5) + ($this->numeral[6] * 4) + ($this->numeral[7] * 3) + ($this->numeral[8] * 2);
        $num2 = ($this->numeral[0] * 11) + ($this->numeral[1] * 10) + ($this->numeral[2] * 9) + ($this->numeral[3] * 8) + ($this->numeral[4] * 7)
            + ($this->numeral[5] * 6) + ($this->numeral[6] * 5) + ($this->numeral[7] * 4) + ($this->numeral[8] * 3) + ($this->numeral[9] * 2);
        return $this->verificaDigitos($num1, $num2, 9, 10);
    }
    private function verificaCNPJ()
    {
        $num1 = ($this->numeral[0] * 5) + ($this->numeral[1] * 4) + ($this->numeral[2] * 3) + ($this->numeral[3] * 2) + ($this->numeral[4] * 9) + ($this->numeral[5] * 8)
            + ($this->numeral[6] * 7) + ($this->numeral[7] * 6) + ($this->numeral[8] * 5) + ($this->numeral[9] * 4) + ($this->numeral[10] * 3) + ($this->numeral[11] * 2);
        $num2 = ($this->numeral[0] * 6) + ($this->numeral[1] * 5) + ($this->numeral[2] * 4) + ($this->numeral[3] * 3) + ($this->numeral[4] * 2) + ($this->numeral[5] * 9)
            + ($this->numeral[6] * 8) + ($this->numeral[7] * 7) + ($this->numeral[8] * 6) + ($this->numeral[9] * 5) + ($this->numeral[10] * 4) + ($this->numeral[11] * 3) + ($this->numeral[12] * 2);
        return $this->verificaDigitos($num1, $num2, 12, 13);
    }
    private function getNumeral()
    {
        $this->numeral = preg_replace("/[^0-9]/", "", $this->CPF_CNPJ);
        $strLen = strlen($this->numeral);
        $ret = false;
        switch ($strLen) {
            case 11:
                if ($this->verificaCPF()) {
                    $this->tipo = 'CPF';
                    $ret = true;
                }
                break;
            case 14:
                if ($this->verificaCNPJ()) {
                    $this->tipo = 'CNPJ';
                    $ret = true;
                }
                break;
            default:
                $ret = false;
                break;
        }
        return $ret;
    }
    public function __toString()
    {
        return $this->CPF_CNPJ;
    }
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     *method:  videoAffiliate
     *created By: Soumojit
     *Description: video affiliate create
     *Date: 10-SEP-2021
     */

    public function videoAffiliate(Request $request){


        if(Auth::user()->is_video_affiliate=='Y'){
            if(Auth::user()->is_professional != "Y"){
                return redirect()->route('load_user_dashboard');
            }
            return redirect()->route('prof.dash.my.booking',['type'=>'UC']);
        }


        $country = Country::orderBy('name')->get();
        $state = State::orderBy('name')->get();
        $bank = UserToBankAccount::where('user_id', Auth::id())->first();
        $bankList = Bank::orderBy('bank_name')->get();

        if ($request->all()) {
            if(Auth::user()->is_professional == "Y" && Auth::user()->is_approved=='Y'){
                $upd1=[];
                $upd1['is_video_affiliate']='Y';
                User::where('id', Auth::id())->update($upd1);
                session()->flash('success', \Lang::get('client_site.joined_affiliate_program'));
                if(Auth::user()->is_professional != "Y"){
                    return redirect()->route('load_user_dashboard');
                }
                return redirect()->route('prof.dash.my.booking',['type'=>'UC']);
            }
            $request->validate([
                'country'    =>  "required",
            ]);
            if($request->country!=30){
                $request->validate([
                    'paypal_address'    =>  "required",
                ]);
                $upd=[];
                $upd['paypal_address']=$request->paypal_address;
                $upd['country_id']=$request->country;
                $upd['is_video_affiliate']='Y';
                User::where('id', Auth::id())->update($upd);
                session()->flash('success', \Lang::get('client_site.joined_affiliate_program'));

                if(Auth::user()->is_professional != "Y"){
                    return redirect()->route('load_user_dashboard');
                }
                return redirect()->route('prof.dash.my.booking',['type'=>'UC']);
            }
            if($request->country==30){
                $request->validate([
                    'first_name'    =>  "required",
                    'last_name'     =>  "required",
                    'phone_no'      => 'required|unique:users,mobile,' . Auth::id(),
                    'city'          =>  "required",
                    'state'         =>  "required",
                    'dob'           =>  "required",
                    'street_number' =>  "required",
                    'street_name'   =>  "required",
                    'district'      =>  "required",
                    'zipcode'       =>  "required",
                    'area_code'     =>  "required",
                    'complement'    =>  "required",
                    'cpf_no'        =>  "required_if:country,=,30|unique:users,cpf_no," . Auth::id(),

                ]);
                if (@$request->cpf_no) {
                    $this->CPF_CNPJ = $request->cpf_no;
                    if (!$this->getNumeral()) {
                        session()->flash('error', \Lang::get('client_site.invalid_cpf_number'));
                        return redirect()->back()->withInput($request->input());
                    }
                }
                $data = [
                    'name'          =>  $request->first_name . ' ' . $request->last_name,
                    'mobile'        =>  $request->phone_no,
                    'country_id'    =>  $request->country,
                    'state_id'      =>  $request->state,
                    'city'          =>  $request->city,
                    'dob'           =>  @$request->dob ? date('Y-m-d', strtotime($request->dob)) : NULL,
                    'street_number' =>  $request->street_number,
                    'street_name'   =>  $request->street_name,
                    'complement'    =>  $request->complement,
                    'district'      =>  $request->district,
                    'area_code'     =>  $request->area_code,
                    'zipcode'       =>  $request->zipcode,
                    'cpf_no'        =>  @$request->cpf_no,

                ];
                User::where('id', Auth::id())->update($data);
                $request->validate(
                    [
                        'bank_name'                 =>  'required',
                        'account_number'            =>  'required',
                        'agency_number'             =>  'required',
                        'bank_number'               =>  'required',
                        'account_check_number'      =>  'required',
                        'account_type'              =>  'required',
                    ],
                    [
                        'bank_name.required' =>  'Nome do banco é obrigatório',
                        'account_number.required' =>  'Número da conta é obrigatório',
                        'agency_number.required' =>  'Número da agência é obrigatório',
                        'bank_number.required' =>  'Número do banco é obrigatório',
                        'account_check_number'      =>  'required',
                        'account_type.required' =>  'O tipo de conta é obrigatório',
                    ]
                );
                UserToBankAccount::where('user_id', Auth::id())->delete();
                $create = UserToBankAccount::create([
                    'user_id'                 =>  Auth::id(),
                    'bankName'                =>  $request->bank_name,
                    'accountNumber'           =>  $request->account_number,
                    'agencyNumber'            =>  $request->agency_number,
                    'bank_number'             =>  $request->bank_number,
                    'accountCheckNumber'      =>  @$request->account_check_number,
                    'account_type'            =>  $request->account_type
                ]);
                $User = User::find(Auth::id());
                try {
                    // creating wirecard account for professional
                    // $moip = new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_SANDBOX);
                    if (env('PAYMENT_ENVIORNMENT') == 'live') {
                        $moip = new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_PRODUCTION);
                    } else if (env('PAYMENT_ENVIORNMENT') == 'sandbox') {
                        $moip = new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_SANDBOX);
                    }
                    $name = explode(' ', @$User->name);
                    $nc = "";
                    for ($i = 1; $i < sizeof($name); $i++) {
                        if ($i > 1) {
                            $nc .= ' ' . @$name[$i];
                        } else {
                            $nc .= @$name[$i];
                        }
                    }
                    $account = $moip->accounts()
                        ->setName(@$name[0])
                        ->setLastName(@$nc)
                        ->setEmail(@$User->email)
                        ->setBirthDate(@$User->dob)
                        ->setTaxDocument(@$User->cpf_no)
                        ->setType('MERCHANT')
                        ->setTransparentAccount(true)
                        ->setPhone(@$User->area_code, @$User->mobile, @$User->countryName->phonecode)
                        ->addAlternativePhone(@$User->area_code, @$User->mobile, @$User->countryName->phonecode)
                        ->addAddress($User->street_name, @$User->street_number, @$User->district, @$User->city, substr(@$User->stateName->name, 0, 2), @$User->zipcode, @$User->complement, substr(@$User->countryName->name, 0, 3))
                        ->create();
                    if (!@$account->getId()) {
                        session()->flash('error', \Lang::get('client_site.wirecard_error'));
                        return redirect()->back();
                    }
                    // updating professional status.
                    // Now creating bank account.....
                    $ret = $this->createBankAccount(@$User->id, @$account->getId(), @$account->getAccessToken());
                    if ($ret == 0) {
                        session()->flash('error', \Lang::get('client_site.wirecard_error'));
                        return redirect()->back();
                    }
                    User::where('id', $User->id)->update([
                        'professional_id'               => @$account->getId(),
                        'professional_access_token'     => @$account->getAccessToken(),
                        'is_video_affiliate'             => 'Y'
                    ]);
                    session()->flash('success',  \Lang::get('client_site.joined_affiliate_program'));
                    if(Auth::user()->is_professional != "Y"){
                        return redirect()->route('load_user_dashboard');
                    }
                    return redirect()->route('prof.dash.my.booking',['type'=>'UC']);
                } catch (\Exception $e) {
                    // dd($e->getMessage());
                    session()->flash('error', \Lang::get('client_site.wirecard_error'));
                    return redirect()->back();
                }
            }
        }



        // if(Auth::user()->is_professional == "Y" && Auth::user()->is_approved=='Y'){
        //     $upd1=[];
        //     $upd1['is_video_affiliate']='Y';
        //     User::where('id', Auth::id())->update($upd1);
        //     session()->flash('success', \Lang::get('client_site.joined_affiliate_program'));
        //     if(Auth::user()->is_professional != "Y"){
        //         return redirect()->route('load_user_dashboard');
        //     }
        //     return redirect()->route('prof.dash.my.booking',['type'=>'UC']);
        // }
        if (Auth::user()->is_professional == "Y") {

            return view('modules.affiliate.video_affiliate_profile')->with([
                'country'   =>  @$country,
                'state'     =>  @$state,
                'bank'      =>  @$bank,
                'bankList'  =>  @$bankList,
            ]);
        } else {
            if (Auth::user()->is_professional == "N") {
                return view('modules.affiliate.video_affiliate_profile')->with([
                'country'   =>  @$country,
                'state'     =>  @$state,
                'bank'      =>  @$bank,
                'bankList'  =>  @$bankList,
                ]);
            }
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        return view('modules.affiliate.video_affiliate_profile');
    }
    /**
     *method:  videoAffiliateShareLink
     *created By: Soumojit
     *Description: video affiliate Share Link click by user and going to register page
     *Date: 10-SEP-2021
     */

    public function videoAffiliateShareLink(Request $request, $slug=null){
        $user = User::where('v_aff_slug',$slug)->first();
        if($user){
            Cookie::queue('video_affiliate_refer_link', $user->id);
            User::where('v_aff_slug',$slug)->increment('aff_link_click', 1);
            return redirect()->route('register');
        }
        return redirect()->route('register');
    }

    /**
     *method:  videoAffiliateEarning
     *created By: Soumojit
     *Description: earning show and show share link
     *Date: 10-SEP-2021
     */

    public function videoAffiliateEarning(Request $request){
        $data['earnings']=VideoAffEarning::where('affiliate_id',auth()->user()->id)->with(['userDetails','bookingDetails'])->get();
        $data['totalShare']=User::where('video_affiliate_id',auth()->user()->id)->count();
        return view('modules.affiliate.video_affiliate_list')->with($data);
    }
    /**
     *method:  videoAffiliateEarningList
     *created By: Soumojit
     *Description: earning order wish
     *Date: 11-SEP-2021
     */
    public function videoAffiliateEarningList(Request $request){
        $data['earnings']=VideoAffEarning::where('affiliate_id',auth()->user()->id)->with(['userDetails','bookingDetails'])->get();
        $data['totalShare']=User::where('video_affiliate_id',auth()->user()->id)->count();
        return view('modules.affiliate.video_affiliate_earning_list')->with($data);
    }
    /**
     *method:  videoAffiliatePaymentList
     *created By: Soumojit
     *Description: payment list
     *Date: 11-SEP-2021
     */
    public function videoAffiliatePaymentList(Request $request){
        $data['payment']=VideoAffPayment::where('affiliate_id',auth()->user()->id)->get();
        $data['totalShare']=User::where('video_affiliate_id',auth()->user()->id)->count();
        return view('modules.affiliate.video_affiliate_payment_list')->with($data);
    }
}
