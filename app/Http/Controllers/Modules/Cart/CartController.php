<?php

namespace App\Http\Controllers\Modules\Cart;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AffiliateProducts;
use App\Models\Cart;
use App\Models\Product;
use App\Models\ProductOrderDetails;
use Session;
use App\Models\Wishlist;
use Auth;
use Cookie;

class CartController extends Controller
{
    public function index(){


        // $ac= Cookie::get('affiliates');
        // return $ac;
        // $ac= json_decode($ac);
        // $is_available=array_search(11, array_column($ac, 'product_id'));
        // dd($is_available);
        // if($is_available!=false || $is_available===0){
        //     return $ac[$is_available]->product_id;
        // }
        // if($is_available==false && $is_available!=0){
        //     return $ac;
        // }
        // return $ac;

        if(@auth()->user()->id){
            $allCartData = Cart::with(['product'])->where('user_id', auth()->user()->id)->get();
        }
        else{
            $cart_id = Session::get('cart_session_id');
            $allCartData = Cart::with(['product'])->where('cart_session_id', $cart_id)->get();
        }
        $category= array();
        $product= array();
        foreach(@$allCartData as  $products){
            array_push($category,$products->product->category_id);
            // array_push($product,['product_id'=>$products->product->id,'user_id'=>$products->product->category_id]);
            array_push($product,$products->product->id);

        }
        // Cookie::queue('product',json_encode($product) );
        // return ( $ac[1]->user_id);
        // dd(array_search(47, array_column($ac, 'product_id')));
        // return json_decode($ac);

        $similarProduct = Product::whereIn('category_id',$category)->where(['status' => 'A', 'admin_status' => 'A'])->where('professional_id','!=', @auth()->user()->id)->whereNotIn('id',$product)->get();
        // return $similarProduct;
        $data['cartData'] = $allCartData;
        $data['similarProducts'] = $similarProduct;
        $data['orderedProducts'] = [];
        if(Auth::check()){
            $data['orderedProducts'] = ProductOrderDetails::whereHas('orderMaster', function($q){
                $q->where('user_id', Auth::id())->whereIn('payment_status',['P','PR']);
            })->pluck('product_id')->toArray();
        }
        $data['wishList'] = [];
        if(Auth::check()){
            $data['wishList'] = Wishlist::where('user_id', Auth::id())->pluck('product_id')->toArray();
        }

        return view('modules.products.product_cart')->with($data);
    }


    public function addToCart(Request $request)
    {
        $response = [
            'jsonrpc' => '2.0'
        ];
        $productDetails = Product::where('id', $request->params['productId'])->first();
        $response['result']['details'] = $productDetails;

        if ($productDetails) {
            $ins = [];
            $ac= Cookie::get('affiliates');
            if($ac!=null){
                $ac= json_decode($ac);
                $is_available=array_search($productDetails->id, array_column($ac, 'product_id'));
                if($is_available != false  || $is_available===0){
                    $aff = AffiliateProducts::where('product_id', $productDetails->id)->where('affiliate_code', $ac[$is_available]->code)->first();
                    $affUserId = @$aff->user_id;
                }
            }
            if (@auth()->user()->id) {
                $delete= Cart::where('user_id', auth()->user()->id)->where('product_id', $productDetails->id)->delete();

                $ins['user_id'] = auth()->user()->id;
                // $affiliate_id = @Session::get('affiliate_id');
                $ins['product_id'] = $productDetails->id;
                $ins['professional_id'] = $productDetails->professional_id;
                if($productDetails->discounted_price>0){
                    $ins['amount'] = $productDetails->discounted_price;
                    $ins['discount_amount'] = $productDetails->price;
                }else{
                    $ins['amount'] = $productDetails->price;
                    $ins['discount_amount'] = 0;
                }

                $ins['affiliate'] = @$affUserId ? $affUserId : 0;
                Cart::create($ins);
                $allCartData = Cart::where('user_id', auth()->user()->id)->get();
                // $html = '';
                // foreach (@$allCartData as $cart) {
                //     $html .= "<div class='shopcutBx_media'>
                //         <div class='media'>
                //         <em> <a href='javascript:;'> <img src='" . asset('storage/app/public/small_product_image') . '/' . @$cart->productdefault->image . "' alt='product'> </a></em>
                //         <div class='media-body'>
                //         <p><a href='javascript:;'>" . $cart->product->product_name . "</a></p>
                //         <b>$" . $cart->total_price . "</b>
                //         </div>
                //         </div>
                //         <a href='javascript:;' class='closecut'><i class='fa fa-times' aria-hidden='true'></i></a>
                //         </div>";
                // }
                // $html = $html . "<div class='total_cut'>
                //     <em>Total</em>
                //     <b>$" . $allCartData->sum('total_price') . "</b>
                //     </div>
                //     <div class='cutview_btn'>
                //     <ul>
                //     <li><a href='javascript:;' class='sign_btn'>View Cart</a></li>
                //     <li><a href='javascript:;' class='sign_btn'>Checkout</a></li>
                //     </ul>
                //     </div>";
                $response['result']['cart'] = $allCartData;
                // $response['result']['html'] = $html;
                return response()->json($response);
            } else {
                $cart_id = Session::get('cart_session_id');
                // $affiliate_id = @Session::get('affiliate_id');
                $delete = Cart::where('cart_session_id', $cart_id)->where('product_id', $productDetails->id)->delete();

                // $ins = [];
                $ins['cart_session_id'] = $cart_id;
                $ins['product_id'] = $productDetails->id;
                $ins['professional_id'] = $productDetails->professional_id;
                if ($productDetails->discounted_price > 0) {
                    $ins['amount'] = $productDetails->discounted_price;
                    $ins['discount_amount'] = $productDetails->price;
                } else {
                    $ins['amount'] = $productDetails->price;
                    $ins['discount_amount'] = 0;
                }
                $ins['affiliate'] = @$affUserId? $affUserId:0;
                Cart::create($ins);
                $allCartData = Cart::with(['product'])->where('cart_session_id', $cart_id)->get();

                // $html = '';
                // foreach (@$allCartData as $cart) {
                //     $html .= "<div class='shopcutBx_media'>
                //         <div class='media'>
                //         <em> <a href='javascript:;'> <img src='" . asset('storage/app/public/small_product_image') . '/' . @$cart->productdefault->image . "' alt='product'> </a></em>
                //         <div class='media-body'>
                //         <p><a href='javascript:;'>" . $cart->product->product_name . "</a></p>
                //         <b>$" . $cart->total_price . "</b>
                //         </div>
                //         </div>
                //         <a href='javascript:;' class='closecut'><i class='fa fa-times' aria-hidden='true'></i></a>
                //         </div>";
                // }
                // $html = $html . "<div class='total_cut'>
                //     <em>Total</em>
                //     <b>$" . $allCartData->sum('total_price') . "</b>
                //     </div>
                //     <div class='cutview_btn'>
                //     <ul>
                //     <li><a href='javascript:;' class='sign_btn'>View Cart</a></li>
                //     <li><a href='javascript:;' class='sign_btn'>Checkout</a></li>
                //     </ul>
                //     </div>";
                $response['result']['cart'] = $allCartData;
                // $response['result']['html'] = $html;
                return response()->json($response);
            }
        }
        $response['result']['error'] = \Lang::get('client_site.something_went_be_wrong');
        return response()->json($response);
    }

    public function removeCart($id){
        if (@auth()->user()->id) {
            $delete = Cart::where('user_id', auth()->user()->id)->where('product_id', $id)->delete();
        } else{
            $cart_id = Session::get('cart_session_id');
            $delete = Cart::where('cart_session_id', $cart_id)->where('product_id', $id)->delete();
        }
        if($delete){
            session()->flash('success', \Lang::get('client_site.product_removed_successfully'));
            return redirect()->route('product.cart');
        }
        session()->flash('error', \Lang::get('client_site.product_not_removed'));
        return redirect()->route('product.cart');
    }


    public function newCart($user_id = null, $cart_session_id = null)
    {
        $user_cart_master = Cart::where('user_id', $user_id)->first();
        $new_cart_master = Cart::where('cart_session_id', $cart_session_id)->get();

        foreach($new_cart_master as $cartData){
            $checkDelete = Cart::where('user_id', $user_id)->where('product_id',$cartData->product_id)->delete();
        }
        $upd=[];
        $upd['user_id']= $user_id;
        $upd['cart_session_id']=null;
        Cart::where('cart_session_id', $cart_session_id)->update($upd);
    }
}
