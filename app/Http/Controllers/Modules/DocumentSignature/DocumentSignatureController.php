<?php

namespace App\Http\Controllers\Modules\DocumentSignature;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\Document;
use App\Models\DocumentSignature;
use App\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use PDF;
use App\Models\Booking;

class DocumentSignatureController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * For manage Document Template
     * @method: index
     */
    public function index(){
        $userlist = Booking::where('professional_id', auth()->user()->id)->with('userDetails')->groupBy('user_id')->get();
        // return $userlist;
        $documentTemplate = DocumentSignature::where(['status'=>'A','created_by'=>'A'])
        ->orWhere(function ($query) {
            $query->where(['created_by' => 'P', 'user_id'=>auth()->user()->id])
            ->whereIn('status' , ['A','I']);
        })
        ->get();
        // return $documentTemplate;
        return view('modules.document_signature.index')->with(['data'=> @$documentTemplate,'userlist'=>$userlist]);
    }
    /**
     * For add Document Template view
     * @method: add
     */
    public function add(){
        return view('modules.document_signature.add_document_signature');
    }
    /**
     * For add Document Template
     * @method: addSuccess
     */
    public function addSuccess(Request $request){
        $request->validate([
            "title" =>  "required",
            "content" =>  "required",
        ]);
        $ins = [];
        $ins['user_id'] = auth()->user()->id;
        $ins['created_by'] = 'P';
        $ins['title'] = $request->title;
        $ins['description'] = $request->content;
        $ins['status'] = 'A';
        $addDocument = DocumentSignature::create($ins);
        if (@$addDocument) {            
            session()->flash("success", \Lang::get('client_site.document_added'));
            return redirect()->route('document');
        }
        session()->flash("error", \Lang::get('client_site.document_not_added'));
        return redirect()->back();
    }
    /**
     * For edit Document Template view
     * @method: edit
     */
    public function edit($id){
        $documentTemplate = DocumentSignature::where('id', $id)->where('user_id', auth()->user()->id)->where('status', '!=', 'D')->first();
        if (@$documentTemplate) {
            return view('modules.document_signature.add_document_signature')->with(['template' => $documentTemplate]);
        }
        return redirect()->back();
    }
    /**
     * For edit Document Template
     * @method: editSuccess
     */
    public function editSuccess($id, Request $request){
        $request->validate([
            "title" =>  "required",
            "content" =>  "required",
        ]);
        $upd= [];
        $upd['title'] = $request->title;
        $upd['description'] = $request->content;
        $updateDocument = DocumentSignature::where('id', $id)->where('user_id', auth()->user()->id)->update($upd);
        if (@$updateDocument) {
            session()->flash("success", \Lang::get('client_site.document_updated'));
            return redirect()->route('document');
        }
        session()->flash("error", \Lang::get('client_site.document_not_updated'));
        return redirect()->back();
    }

    /**
     * For edit Document Template
     * @method: editSuccess
     */
    public function send(Request $request)
    {
        $documentTemplate = DocumentSignature::where('id', $request->template_id)->where('status', '!=', 'D')->first();
        $user = User::where('id',$request->user_id)->first();
        // return $documentTemplate;
        // $link = URL::to('storage/app/public/uploads/signature/');
        // $signature = '<img src="' . $link . '/' . auth()->user()->signature . '" alt="" width="100" height="100" />';
        // $template = $documentTemplate;
        // $content = str_replace('___', $signature, $template->description);
        // $template['content'] = $content;
        // // return $template;
        // $pdf= PDF::loadView('pdf.document_pdf', ['data' => $ins['description'] = $template->content]);
        // $path_storage = 'abc'.'.pdf';
        // @unlink(storage_path('public/pdf/'. $path_storage));
        // Storage::put('public/pdf/'. $path_storage,$pdf->output());
        $success= Mail::send(new Document($request->template_id,$user));
        session()->flash("success", \Lang::get('client_site.send_document'));
        return redirect()->route('document');
        
        
    }
    /**
     * For view Document Template
     * @method: view
     */
    public function view($id)
    {
        $documentTemplate = DocumentSignature::where('id', $id)->where('status', '!=', 'D')->first();
        $link = URL::to('storage/app/public/uploads/signature/');
        $signature='<img src="'.$link.'/'. auth()->user()->signature.'" alt="" width="100" height="100" />';
        $template = $documentTemplate;
        $content= $template->description;
        $template['content'] = $content;
        // return $template;


        if (@$documentTemplate) {
            return view('modules.document_signature.view')->with(['template' => $documentTemplate]);
        }
        return redirect()->back();
    }
    /**
     * For view Document Template
     * @method: view
     */
    public function delete($id)
    {
      $deleteTemplate = DocumentSignature::where('id', $id)->where('user_id', auth()->user()->id)->update(['status'=>'D']);
      if(@$deleteTemplate){
            session()->flash("success", \Lang::get('client_site.document_deleted'));
            return redirect()->route('document');
      }
        session()->flash("error", \Lang::get('client_site.document_not_deleted'));
        return redirect()->back();
    }
    /**
     * For view Document Template
     * @method: view
     */
    public function copy($id)
    {
        $documentTemplate = DocumentSignature::where('id', $id)->where('status', '!=', 'D')->first();
        $ins = [];
        $ins['user_id'] = auth()->user()->id;
        $ins['created_by'] = 'P';
        $ins['title'] = $documentTemplate->title;
        $ins['description'] = $documentTemplate->content;
        $ins['status'] = 'A';
        $addDocument = DocumentSignature::create($ins);
        if (@$addDocument) {
            session()->flash("success", \Lang::get('client_site.document_copy'));
            return redirect()->route('document.edit',['id'=> $addDocument->id]);
        }
        session()->flash("error", \Lang::get('client_site.document_not_copy'));
        return redirect()->back();

    }
}
