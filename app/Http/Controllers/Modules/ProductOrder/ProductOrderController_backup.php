<?php

namespace App\Http\Controllers\Modules\ProductOrder;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\AdminProductOrderCancelRequest;
use App\Models\Category;
use App\User;
use App\Models\Booking;
use App\Models\UserAvailability;
use App\Models\ProductCategory;
use App\Models\Timezone;
use App\Models\UserToCard;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use Moip\Moip;
use Moip\Auth\BasicAuth;
use Moip\Auth\OAuth;
use Moip\Resource\Customer;

use App\Mail\UserBooking;
use App\Mail\ProfessionalBooking;
use App\Models\Content;
use App\Models\Payment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Models\Coupon;
use App\Models\CouponToProduct;
use App\Models\ReferralDiscount;
use Illuminate\Support\Facades\Storage;
use App\Models\AdminBankAccount;
use App\Mail\BankAccountPayment;
use App\Mail\BankAccountPaymentProduct;
use App\Models\Commission;
use App\Mail\UserBankAccountBooking;
use App\Models\Product;
use App\Models\ProductOrder;
use App\Mail\ProfessionalProductOrder;
use App\Mail\ProfessionalProductOrderCancel;
use App\Mail\ProfessionalProductOrderCancelRequest;
use App\Mail\UserBankAccountProductOrder;
use App\Mail\UserCancelRequestReject;
use App\Mail\UserProductOrder;
use App\Mail\UserProductOrderCancel;
use App\Models\Cart;
use App\Models\PaymentDetails;
use App\Models\ProductOrderDetails;
use Stripe\PaymentIntent;
use Stripe\Stripe;
use Stripe\Refund;


use Validator;
use URL;
use Session;
use Redirect;
use Input;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment as PaymentPaypal;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use PayPal\Api\RefundRequest;
use PayPal\Api\Sale;



class ProductOrderController extends Controller
{
    protected $CPF_CNPJ, $numeral, $tipo;
    protected $adminAccessToken;
    private $_api_context;
    private $_ordID;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->adminAccessToken = getAdminToken();
        $paypal_configuration = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_configuration['client_id'], $paypal_configuration['secret']));
        $this->_api_context->setConfig($paypal_configuration['settings']);
    }
    /**
     * For Order Product store
     * @method: storeProductOrder
     */

    public function storeProductOrder(Request $request)
    {
        $token = null;
        $is_product = Product::where('id', $request->product_id)->first();
        // if ($is_product == null ) {
        //     session()->flash('error', \Lang::get('client_site.unauthorize_access'));
        //     return redirect()->back();
        // }
        if (Auth::user()->id == @$is_product->professional_id) {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        if($is_product ){

        }
        else {
            $cartData = Cart::where('user_id', Auth::user()->id)->get();
            if($cartData->isNotEmpty()){

                $use_wallet_amount=0;
                $use_use='N';
                $sub_total =  $cartData->sum('amount');
                if(auth()->user()->wallet_balance>0){
                    $use_wallet_amount= (int)($sub_total);
                    if(auth()->user()->wallet_balance >= $use_wallet_amount){
                        $use_wallet_amount =$use_wallet_amount;
                    }else{
                        $use_wallet_amount= auth()->user()->wallet_balance;
                    }
                    $use_use='Y';
                }
                $is_created = ProductOrder::create([
                    'user_id'           =>  auth()->id(),
                    'token_no'          =>  $token = strtoupper(str_random(13)),
                    'order_date'        =>  toUTCTime(date('Y-m-d H:i:s')),
                    'amount'            =>  $cartData->sum('amount'),
                    'payment_status'    => "I",
                    'sub_total'         => $cartData->sum('amount'),
                    'total'             => $cartData->sum('amount'),
                    // 'installment_charge'    => $is_product->installment_charge
                    'minimum_instalment'    => 1,
                    'is_wallet_use'     => $use_use,
                    'wallet'            => $use_wallet_amount
                ]);
                $minimumInstalment = array();
                $installmentCharge = array();
                if (@$is_created) {
                    foreach($cartData as $cart){
                        $product    = Product::where('id', $cart->product_id)->with('category')->first();
                        $professional = User::where('id',$cart->professional_id)->where('professional_access_token','!=',null)->first();
                        $affiliate = User::where('id',$cart->affiliate)->where('professional_access_token','!=',null)->first();
                        $ins =[];
                        if($cart->affiliate != 0){

                            // $discount   = $product->category->commission;
                            // $discount   = $product->professional->product_commission;
                            if(@$product->professional->product_commission>0 && $product->professional->product_commission!=null){
                                $discount   = @$product->professional->product_commission;
                            }else{
                                $discount   = $product->category->commission;
                            }
                            $amount     = (@$cart->amount * @$discount) / 100;
                            $affiliate_amount    =  (@$amount*$product->affiliate_percentage)/ 100;
                            $professional_amount    =  @$amount-$affiliate_amount;
                            $admin_amount   =   @$cart->amount - @$amount;
                        }else{
                            // $discount   = $product->category->commission;
                            // $discount   = $product->professional->product_commission;
                            if(@$product->professional->product_commission>0 && $product->professional->product_commission!=null){
                                $discount   = @$product->professional->product_commission;
                            }else{
                                $discount   = $product->category->commission;
                            }
                            $amount     = (@$cart->amount * @$discount) / 100;
                            $professional_amount    =  @$amount;
                            $admin_amount   =   @$cart->amount - @$amount;
                            $affiliate_amount    =  0;
                        }

                        $ins['product_order_master_id']     = $is_created->id;
                        $ins['product_id']                  = $cart->product_id;
                        $ins['professional_id']             = $cart->professional_id;
                        $ins['amount']                      = $cart->amount;
                        $ins['professional_amount']         = $professional_amount;
                        $ins['admin_commission']            = $admin_amount;
                        $ins['affiliate_id']                = $cart->affiliate;
                        $ins['affiliate_commission']        = $affiliate_amount;
                        if($use_use == 'Y'){
                            $ins['affiliate_is_wirecard']       = 'N';
                            $ins['is_wirecard']                 = 'N';
                        }else{
                            $ins['affiliate_is_wirecard']       = $affiliate? 'Y': 'N';
                            $ins['is_wirecard']                 = $professional ? 'Y' : 'N';
                        }
                        $ord = ProductOrderDetails::create($ins);

                        array_push($minimumInstalment, $product->no_of_installment);
                        array_push($installmentCharge, $product->installment_charge);
                    }
                    $installmentCharge = array_unique($installmentCharge);
                    $minimumInstalment = min($minimumInstalment);
                    if(count($installmentCharge)==1){
                        ProductOrder::where('id',$is_created->id)->update([
                            'installment_charge'    => $installmentCharge[0],
                            'minimum_instalment'    => $minimumInstalment
                        ]);
                    }
                }
            }
            if (@$is_created) {
                // Cart::where('user_id', Auth::user()->id)->delete();
                // session()->flash('success', @Auth::user()->is_credit_card_added == "N" ? \Lang::get('client_site.your_booking_was_initiated') : \Lang::get('client_site.successful_booked_your_request'));
                return redirect()->route('product.order.store.success', ['token' => $token]);
            } else {
                session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
                return redirect()->back();
            }
        }
    }

    public function checkDate(Request $request)
    {
        $response = [
            'jsonrpc'   =>  '2.0'
        ];
        if ($request->params['date']) {
            $userDetails = User::where('id', $request->params['user_id'])->first();
            $userDetails = $userDetails->load('userTimeZone');

            date_default_timezone_set(@$userDetails->userTimeZone->timezone);
            $day = date('l', strtotime($request->params['date']));
            $days = [
                'Sunday'    =>  1,
                'Monday'    =>  2,
                'Tuesday'   =>  3,
                'Wednesday' =>  4,
                'Thursday'  =>  5,
                'Friday'    =>  6,
                'Saturday'  =>  7
            ];
            $duration = date('H:i', strtotime($request->params['start_time'] . ' +' . $request->params['duration'] . ' minutes'));

            $is_avlbl = UserAvailability::where([
                'user_id'       =>  @$request->params['user_id'],
                'day'           =>  @$days[@$day]
            ])
                ->where('from_time', '<=', $request->params['start_time'])
                ->where('to_time', '>=', $duration)
                ->first();

            if ($is_avlbl == null) {
                $response['status'] = 1;
            } else {
                $response['status'] = 0;
            }
        }
        return response()->json($response);
    }

    /**
     * For Order Product review page and payment order
     * @method: OrderSuccess
     */
    public function OrderSuccess($slug)
    {
        $authUser = Auth::guard('web')->user();
        $order = ProductOrder::where('token_no', $slug)
            ->where('user_id', @Auth::id())
            ->first();
        $profile = 0;
        $required = [
            0   => 'country_id',
            1   => 'state_id',
            2   => 'city',
            3   => 'dob',
            4   => 'street_number',
            5   => 'street_name',
            6   => 'complement',
            7   => 'district',
            8   => 'zipcode',
            9  => 'area_code'
        ];

        for ($i = 0; $i < sizeof($required); $i++) {
            $str = $required[@$i];
            if ($authUser->$str == null) {

                $profile++;
                break;
            } elseif ($authUser->$str <= 0 && !is_string($authUser->$str)) {
                $profile++;
                break;
            }
        }
        if ($order == null) {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        } else {

            if($order->amount != 0 && $order->payment_status == 'I' && $order->wallet == $order->sub_total ){
                $this->fullPaymentThroughWallet($slug);
            }
            if ($authUser->is_credit_card_added == 'Y' && $authUser->customer_id != '' && $order->moip_order_id == '' && $order->payment_type == 'C') {
                $this->chargeUser($slug);
            }
            // if ($order->amount == 0 && $order->payment_status == 'I') {
            //     $this->freeSessionPayment($slug);
            // }
            if ($order->payment_type == 'BA' && $order->payment_status == 'I') {
                $this->bankAccountPayment($slug);
            }
            if ($order->payment_type == 'P' && $order->payment_status == 'I') {
                $title = "";
                foreach (@$order->orderDetails as $key=>$productDetails){
                    $title .=  ($key==0 ? '' : ', ') . $productDetails->product->title;
                    $productDetails->is_wirecard = 'N';
                    $productDetails->save();
                }
                // $paydata['title'] = $title;
                // $paydata['amount'] = $order->amount;
                // dd($paydata);
                $paydata = new Request([
                    'title'     => $title,
                    'amount'    => $order->sub_total-$order->wallet,
                    'ordID'     => $order->id,
                ]);
                // app('App\Http\Controllers\Modules\Payment\PaypalController')->postPaymentWithpaypal($paydata);
                $this->postPaymentWithpaypal($paydata);
            }
            $order = ProductOrder::where('token_no', $slug)
                ->where('user_id', @Auth::id())
                ->first();
            $order = $order->load('orderDetails','profDetails', 'productDetails', 'productCategoryDetails');
            // return $order;
            $time = Timezone::get();
            $country = Country::orderBy('name')->get();
            $state = State::orderBy('name')->get();
            $accountInfo = AdminBankAccount::first();
            if (@$order->payment_status == 'F') {
                session()->flash('error', \Lang::get('site.Payment_failed'));
                return redirect()->route('load_user_dashboard');
            }
            return view('modules.product_order.order_success')->with([
                'order'   =>  @$order,
                'profile'   =>  @$profile,
                'time'      =>  @$time,
                'country'   =>  @$country,
                'state'     =>  @$state,
                'accountInfo' => @$accountInfo
            ]);
        }
    }


    /**
     * For Completing Customer Profile
     *
     * @method completeProfile
     */
    public function completeProfile(Request $request)
    {
        try {
            // if (@$request->is_profile > 0) {
            $request->validate([
                'name'              =>  "required",
                'phone_no'          => 'required|digits:9|unique:users,mobile,' . Auth::id(),
                'country'           =>  "required",
                'state'             =>  "required",
                'city'              =>  "required",
                'dob'               =>  "required",
                'street_number'     =>  "required",
                'street_name'       =>  "required",
                'complement'        =>  "required",
                'district'          =>  "required",
                'zipcode'           =>  "required",
                'area_code'         =>  "required",
                'cpf_no'            =>  "required|unique:users,cpf_no," . Auth::id()
            ]);
            $data = [
                'name'              =>  $request->name,
                'mobile'            =>  $request->phone_no,
                'country_id'        =>  $request->country,
                'state_id'          =>  $request->state,
                'city'              =>  $request->city,
                'dob'               =>  date('Y-m-d', strtotime($request->dob)),
                'street_number'     =>  $request->street_number,
                'street_name'       =>  $request->street_name,
                'complement'        =>  $request->complement,
                'district'          =>  $request->district,
                'area_code'         =>  $request->area_code,
                'zipcode'           =>  $request->zipcode,
                'cpf_no'            =>  $request->cpf_no
            ];
            $user = User::where('id', Auth::id())->update($data);

            $this->CPF_CNPJ = $request->cpf_no;
            if (!$this->getNumeral()) {
                session()->flash('error', \Lang::get('client_site.invalid_cpf_number'));
                return redirect()->back();
            }
            // }

            if (@$user) {
                $request->validate([
                    'card_name' => "required",
                    'card_cpf_no' => "required",
                    // 'card_dob' => "required",
                    'dob_year' => "required",
                    'dob_month' => "required",
                    'dob_day' => "required",
                    'card_phonecode' => "required",
                    'card_area_code' => "required|digits:2",
                    'card_mobile' => 'required|digits:9',
                ]);
                $dob = $request->dob_year . '-' . $request->dob_month . '-' . $request->dob_day;
                // dd($dob);
                $this->CPF_CNPJ = $request->card_cpf_no;
                if (!$this->getNumeral()) {
                    session()->flash('error', \Lang::get('client_site.invalid_cpf_number'));
                    return redirect()->back();
                }
                $updatedUser = User::where('id', Auth::id())->first();
                $moip = env('PAYMENT_ENVIORNMENT', 'sandbox') == 'live' ? new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_PRODUCTION) : new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_SANDBOX);
                if($updatedUser->customer_id==null){
                $customer = $moip->customers()
                    ->setOwnId(uniqid())
                    ->setFullname(@$updatedUser->name)
                    ->setEmail(@$updatedUser->email)
                    ->setBirthDate(@$updatedUser->dob)
                    ->setTaxDocument(@$updatedUser->cpf_no)
                    ->setPhone(@$updatedUser->area_code, @$updatedUser->mobile)
                    ->addAddress('BILLING', @$updatedUser->street_name, @$updatedUser->street_number, @$updatedUser->district, @$updatedUser->city, strtoupper(substr(@$updatedUser->stateName->name, 0, 2)), @$updatedUser->zipcode, @$updatedUser->complement)
                    ->addAddress('SHIPPING', @$updatedUser->street_name, @$updatedUser->street_number, @$updatedUser->district, @$updatedUser->city, substr(@$updatedUser->stateName->name, 0, 2), @$updatedUser->zipcode, @$updatedUser->complement)->create();
                }
                $c_id=$updatedUser->customer_id?$updatedUser->customer_id:@$customer->getId();
                $card = $moip->customers()->creditCard()
                    ->setExpirationMonth($request->expiry_month)
                    ->setExpirationYear($request->expiry_year)
                    ->setNumber($request->cardnumber)
                    ->setCVC($request->cvc)
                    ->setFullName($request->card_name)
                    ->setBirthDate($dob)
                    ->setTaxDocument('CPF', $request->card_cpf_no)
                    ->setPhone($request->card_phonecode, $request->card_area_code, $request->card_mobile)
                    ->create($c_id);

                if (!@$card->getId()) {
                    session()->flash('error', \Lang::get('client_site.internal_server_error'));
                    return redirect()->back();
                }
                $update = User::where('id', Auth::id())->update([
                    'customer_id' => $c_id,
                    'is_credit_card_added' => @$card->getStore() == true ? 'Y' : 'N'
                ]);
                $is_Created = UserToCard::create([
                    'user_id' => Auth::id(),
                    'card_name' => @$request->card_name,
                    'cpf_no' => @$request->card_cpf_no,
                    'phonecode' => @$request->card_phonecode,
                    'area_code' => @$request->card_area_code,
                    'mobile' => @$request->card_mobile,
                    'dob' => @$dob,
                    'card_id' => @$card->getId(),
                    'first_six' => @$card->getFirst6(),
                    'last_four' => @$card->getLast4(),
                    'card_brand' => @$card->getBrand(),
                    'cred_type' => "CREDIT_CARD",
                    'card_response' => json_encode($card),
                ]);
                if (@$is_Created) {
                    Booking::where('token_no', $request->order_no)->update([
                        'order_status'  =>  'A'
                    ]);
                    session()->flash('success', \Lang::get('client_site.now_your_profile_is'));
                    return redirect()->back();
                } else {
                    session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
                    return redirect()->back();
                }
                //}
                /* catch(\Exception $e){
                 session()->flash('error', 'Internal server error.');
                return redirect()->back();
            }*/
            } else {
                session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
                return redirect()->back();
            }
        } catch (\Moip\Exceptions\ValidationException $e) {
            session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
            return redirect()->back();
        } catch (Exceptions\UnexpectedException $e) {
            session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
            return redirect()->back();
        } catch (Exception $e) {
            session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
            return redirect()->back();
        }
    }

    /**
     * charge user after booking is complete
     *
     * @method: chargeUser
     */
    private function chargeUser($token) //herework
    {
        $user_id = ProductOrder::where('token_no', $token)->first()->load('orderDetails','profDetails', 'productDetails', 'productCategoryDetails');

        $cardDetails = UserToCard::where('user_id', $user_id->user_id)->first();
        $access_token = getAdminToken();

        $customer = User::where([
            'id' => @$user_id->user_id
        ])->first();
        // dd($user_id->sub_total);
        if ($user_id->sub_total == 0) {
            $UpdateBooking = ProductOrder::where(['token_no' => @$user_id->token_no])->update(['payment_status' => "P"]);
            $referralStatus = User::where('id', @$user_id->user_id)->first();
            if (@$referralStatus->is_paid_activity == 'N') {
                User::where('id', @$user_id->user_id)->update(['is_paid_activity' => 'C']);
                User::where('id', @$referralStatus->referrer_id)->increment("benefit_count", 1);
            }
            $userdata = User::where('id', Auth::id())->first();
            $userdata1 = User::where('id', @$user_id->professional_id)->first();
            $professionalmail = $userdata1->email;
            $usermail = $userdata->email;
            $bookingdata = $user_id;

            // for send mail to user
            Mail::send(new UserProductOrder($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));

            // for send mail to professional
            Mail::send(new ProfessionalProductOrder($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
            session()->flash('success', \Lang::get('client_site.product_ordered_successfully'));
            return redirect()->back();
        }

        // if($user_id->is_coupon == 'Y'){
        //     // In Here the Primary account is the Admin account and the secondary account is the professional account.
        //     $amount = bcmul(@$user_id->sub_total, 100); // calculating on cents...
        //     $split_amount = bcmul(@$user_id->amount / 2, 100); // calculating on cents...]
        //     $amount = (int) $amount;
        //     $split_amount = (int) $split_amount;
        // }else{
        //     // In Here the Primary account is the Admin account and the secondary account is the professional account.
        //     $amount = bcmul(@$user_id->amount, 100); // calculating on cents...
        //     $split_amount = bcmul(@$user_id->amount / 2, 100); // calculating on cents...]
        //     $amount = (int) $amount;
        //     $split_amount = (int) $split_amount;
        // }
        // $referDiscount = ReferralDiscount::first();
        // $checkTeacher = User::where('id', $user_id->professional_id)->first();

        // $productCategoryCommission = ProductCategory::first();

        // if (@$checkTeacher->is_paid_activity == 'N') {

        //     $discount = $user_id->productCategoryDetails->commission + $referDiscount->referrer_teacher_discount;
        //     $amount_professional = (@$user_id->amount * @$discount) / 100;
        //     $split_amount   =  bcmul(@$amount_professional, 100); // calculating on cents...
        //     $split_amount   =  (int) @$split_amount;
        // } elseif (@$checkTeacher->benefit_count > 0) {

        //     $discount = $user_id->productCategoryDetails->commission + $referDiscount->referred_teacher_discount;
        //     $amount_professional = (@$user_id->amount * @$discount) / 100;
        //     $split_amount   =  bcmul(@$amount_professional, 100); // calculating on cents...
        //     $split_amount   =  (int) @$split_amount;
        // } else {
        // $discount = $user_id->productCategoryDetails->commission;
        // if ($user_id->no_of_installment > 1 && $user_id->installment_charge == 'Y') {

        //     $i = $user_id->no_of_installment;
        //     $x2 = 4.50;
        //     $x3 = 5.00;
        //     $x4 = 5.50;
        //     $x5 = 6.50;
        //     $x6 = 7.50;
        //     $x7 = 8.50;
        //     $x8 = 9.50;
        //     $x9 = 10.50;
        //     $x10 = 11.50;
        //     $x11 = 12.00;
        //     $x12 = 12.50;
        //     if ($i == 2) {
        //         $x = $x2;
        //     }
        //     if ($i == 3) {
        //         $x = $x3;
        //     }
        //     if ($i == 4) {
        //         $x = $x4;
        //     }
        //     if ($i == 5) {
        //         $x = $x5;
        //     }
        //     if ($i == 6) {
        //         $x = $x6;
        //     }
        //     if ($i == 7) {
        //         $x = $x7;
        //     }
        //     if ($i == 8) {
        //         $x = $x8;
        //     }
        //     if ($i == 9) {
        //         $x = $x9;
        //     }
        //     if ($i == 10) {
        //         $x = $x10;
        //     }
        //     if ($i == 11) {
        //         $x = $x11;
        //     }
        //     if ($i == 12) {
        //         $x = $x12;
        //     }
        //     $discount = ($user_id->productCategoryDetails->commission - $x);
        // }



        // $amount_professional = (@$user_id->sub_total * @$discount) / 100;
        // $admin_amount  = @$user_id->sub_total - @$amount_professional;

        // if ($amount_professional < 1) {
        //     $amount_professional = 1;
        //     $admin_amount = @$user_id->sub_total - $amount_professional;
        // }
        // if ($admin_amount < 1) {
        //     $admin_amount = 1;
        //     $amount_professional = @$user_id->sub_total - $admin_amount;
        // }

        $amount =  bcmul((@$user_id->sub_total + @$user_id->extra_installment_charge - $user_id->wallet), 100); // calculating on cents...
        $amount =  (int) $amount;

        // $split_amount   =  bcmul(@$amount_professional, 100); // calculating on cents...
        // $split_amount   =  (int) @$split_amount;
        // dd($admin_amount);
        if (env('PAYMENT_ENVIORNMENT') == 'live') {
            $moip = new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_PRODUCTION);
        } else if (env('PAYMENT_ENVIORNMENT') == 'sandbox') {
            $moip = new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_SANDBOX);
        }
        $arr= array();
        $arrAffiliate= array();
        $order = $moip->orders()->setOwnId(uniqid())
            ->addItem("Consultation Fees", 1, @$user_id->productCategoryDetails->category_name, @$amount)
            ->setShippingAmount(0)->setAddition(0)->setDiscount(0)
            ->setCustomerId(@$customer->customer_id);
            // if( $user_id->is_wallet_use=='N'){
            //     foreach ($user_id->orderDetails  as $orderDetails) {
            //         if($orderDetails->is_wirecard == 'Y' && !in_array($orderDetails->professional_id, $arr)){
            //             $sum = ProductOrderDetails::where('product_order_master_id', $user_id->id)->where('professional_id', @$orderDetails->professional_id)->sum('professional_amount');
            //             $order = $order->addReceiver(@$orderDetails->profDetails->professional_id, "SECONDARY", bcmul(@$sum,100), 0, false);
            //             array_push($arr, @$orderDetails->professional_id);
            //         }
            //         if($orderDetails->affiliate_is_wirecard == 'Y' && !in_array($orderDetails->affiliate_id, $arrAffiliate)){
            //             $sum1 = ProductOrderDetails::where('product_order_master_id', $user_id->id)->where('affiliate_id', @$orderDetails->affiliate_id)->sum('affiliate_commission');
            //             $order = $order->addReceiver(@$orderDetails->affiliateDetails->professional_id, "SECONDARY", bcmul(@$sum1,100), 0, false);
            //             array_push($arrAffiliate, @$orderDetails->affiliate_id);
            //         }
            //     }
            // }

            // dd ($order);
            $order = $order->create();

        ProductOrder::where([
            'token_no' => @$token
        ])->update([
            'moip_order_id'  =>  @$order->getId()
        ]);

        $user_id = ProductOrder::where('token_no', $token)->first()->load('orderDetails', 'profDetails', 'productDetails', 'productCategoryDetails');
        return $this->payment($order, $customer, $cardDetails, $user_id);
    }

    /**
     * For Deducting customer balance
     * @method: payment
     */
    private function payment($order, $user, $card, $productOrder)
    {
        if ($productOrder->sub_total == 0) {
            $payment = Payment::create([
                'token_no'              =>  @$productOrder->token_no,
                'order_id'              =>  @$order->getId(),
                // 'payment_id'            =>  @$payment_output->id,
                'user_id'               =>  @$productOrder->user_id,
                'professional_id'       =>  @$productOrder->professional_id,
                // 'professional_amount'   =>  @$professional_amount,
                // 'admin_amount'          =>  @$admin_amount,
                // 'total_amount'          =>  @$total_amount,
                'balance_status'        =>  'R',
                'order_type'     =>  'P',
            ]);
            if (@$payment) {
                Cart::where('user_id', Auth::user()->id)->delete();
                $UpdateBooking = ProductOrder::where([
                    'token_no'        =>  @$productOrder->token_no
                ])->update([
                    'payment_status'  =>  "P"
                ]);
                $referralStatus = User::where('id', @$productOrder->user_id)->first();
                if (@$referralStatus->is_paid_activity == 'N') {
                    User::where('id', @$productOrder->user_id)->update(['is_paid_activity' => 'C']);
                    User::where('id', @$referralStatus->referrer_id)->increment("benefit_count", 1);
                }
                $userdata = User::where('id', Auth::id())->first();
                $userdata1 = User::where('id', @$productOrder->professional_id)->first();
                $professionalmail = $userdata1->email;
                $usermail = $userdata->email;
                $bookingdata = $productOrder;

                // for send mail to user
                Mail::send(new UserProductOrder($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));

                // for send mail to professional
                Mail::send(new ProfessionalProductOrder($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                session()->flash('success', \Lang::get('client_site.product_ordered_successfully'));
                return true;
            } else {
                $UpdateBooking = ProductOrder::where([
                    'token_no'        =>  @$productOrder->token_no
                ])->update([
                    'payment_status'  =>  "F"
                ]);

                return false;
            }
        } else {
            if (env('PAYMENT_ENVIORNMENT') == 'live') {
                $url = Moip::ENDPOINT_PRODUCTION;
            } else if (env('PAYMENT_ENVIORNMENT') == 'sandbox') {
                $url = Moip::ENDPOINT_SANDBOX;
            }
            $data = array(
                'installmentCount' => @$productOrder->no_of_installment,
                'statementDescriptor' => 'netbhe',
                'fundingInstrument' =>
                array(
                    'method' => 'CREDIT_CARD',
                    'creditCard' =>
                    array(
                        'id' => @$card->card_id,
                        'store' => true,
                        'holder' =>
                        array(
                            'fullname' => @$card->card_name,
                            'birthdate' => @$user->dob,
                            'taxDocument' =>
                            array(
                                'type' => 'CPF',
                                'number' => @$user->cpf_no,
                            ),
                            'phone' =>
                            array(
                                'countryCode' => @$user->countryName->phonecode,
                                'areaCode' => @$user->area_code,
                                'number' => @$user->mobile,
                            ),
                            'billingAddress' =>
                            array(
                                'city' => @$user->city,
                                'district' => @$user->district,
                                'street' => @$user->street_name,
                                'streetNumber' => @$user->street_number,
                                'zipCode' => @$user->zipcode,
                                'state' => substr(@$user->stateName->name, 0, 2),
                                'country' => substr(@$user->countryName->name, 0, 3),
                            ),
                        ),
                    ),
                ),
            );
            $crl = curl_init();
            $header = array();
            // $header[] = 'Content-length: 0';
            $header[] = 'Content-type: application/json';
            $header[] = 'Authorization: Bearer ' . $this->adminAccessToken;
            curl_setopt($crl, CURLOPT_HTTPHEADER, $header);
            curl_setopt($crl, CURLOPT_URL, "{$url}/v2/orders/" . @$order->getId() . "/payments");
            curl_setopt($crl, CURLOPT_POST, 1);
            curl_setopt($crl, CURLOPT_POST, true);
            curl_setopt($crl, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
            $payment_output = curl_exec($crl);
            curl_close($crl);
            $payment_output = json_decode($payment_output);
            if (@$payment_output->id) {

                // $referDiscount = ReferralDiscount::first();
                // $checkTeacher = User::where('id', $productOrder->professional_id)->first();

                // $productCategoryCommission = Commission::first();


                // if (@$checkTeacher->is_paid_activity == 'N') {
                //     $discount = $productOrder->productCategoryDetails->commission + $referDiscount->referrer_teacher_discount;
                //     $amount = (@$productOrder->amount * @$discount) / 100;
                //     $professional_amount   =  @$amount;
                //     $admin_amount         =   @$productOrder->amount - @$amount;
                //     User::where('id', @$checkTeacher->id)->update(['is_paid_activity' => 'C']);
                //     User::where('id', @$checkTeacher->referrer_id)->increment("benefit_count", 1);
                // } elseif (@$checkTeacher->benefit_count > 0) {
                //     $discount = $productOrder->productCategoryDetails->commission + $referDiscount->referred_teacher_discount;
                //     $amount = (@$productOrder->amount * @$discount) / 100;
                //     $professional_amount   =  @$amount;
                //     $admin_amount         =   @$productOrder->sub_total - @$amount;
                //     User::where('id', @$checkTeacher->id)->decrement("benefit_count", 1);
                // } else {
                // $discount = $productOrder->productCategoryDetails->commission;
                // if (@$productOrder->no_of_installment > 1 && @$productOrder->installment_charge == 'Y') {

                //     $i = @$productOrder->no_of_installment;
                //     $x2 = 4.50;
                //     $x3 = 5.00;
                //     $x4 = 5.50;
                //     $x5 = 6.50;
                //     $x6 = 7.50;
                //     $x7 = 8.50;
                //     $x8 = 9.50;
                //     $x9 = 10.50;
                //     $x10 = 11.50;
                //     $x11 = 12.00;
                //     $x12 = 12.50;
                //     if ($i == 2) {
                //         $x = $x2;
                //     }
                //     if ($i == 3) {
                //         $x = $x3;
                //     }
                //     if ($i == 4) {
                //         $x = $x4;
                //     }
                //     if ($i == 5) {
                //         $x = $x5;
                //     }
                //     if ($i == 6) {
                //         $x = $x6;
                //     }
                //     if ($i == 7) {
                //         $x = $x7;
                //     }
                //     if ($i == 8) {
                //         $x = $x8;
                //     }
                //     if ($i == 9) {
                //         $x = $x9;
                //     }
                //     if ($i == 10) {
                //         $x = $x10;
                //     }
                //     if ($i == 11) {
                //         $x = $x11;
                //     }
                //     if ($i == 12) {
                //         $x = $x12;
                //     }
                //     $discount = (@$productOrder->productCategoryDetails->commission - $x);
                // }

                // $amount = (@$productOrder->sub_total * @$discount) / 100;
                // $professional_amount   =  @$amount;
                // $admin_amount         =   @$productOrder->sub_total - @$amount;
                // // }
                // if ($professional_amount < 1) {
                //     $professional_amount = 1;
                //     $admin_amount = @$productOrder->sub_total - $professional_amount;
                // }
                // if ($admin_amount < 1) {
                //     $admin_amount = 1;
                //     $professional_amount = @$productOrder->sub_total - $admin_amount;
                // }
                $total_amount         =  @$productOrder->sub_total + @$productOrder->extra_installment_charge;
                if (@$payment_output->status == 'CANCELLED') {
                    $UpdateBooking = ProductOrder::where([
                        'token_no'        =>  @$productOrder->token_no
                    ])->update([
                        'payment_status'  =>  "F"
                    ]);

                    return false;
                }
                $payment = Payment::create([
                    'token_no'                  =>  @$productOrder->token_no,
                    'pg_order_id'               =>  @$order->getId(),
                    'pg_payment_id'             =>  @$payment_output->id,
                    'user_id'                   =>  @$productOrder->user_id,
                    'total_amount'              =>  @$total_amount,
                    'pg_response'               =>  json_encode($payment_output),
                    'payment_status'            =>  'P',
                    'payment_type'              =>  'C',
                    'order_type'                =>  'P',
                    'wallet'                    =>  @$productOrder->wallet,
                    'is_wallet_use'             =>  @$productOrder->is_wallet_use,
                ]);
                if (@$payment) {
                    $html='';
                    Cart::where('user_id', Auth::user()->id)->delete();
                    foreach ($productOrder->orderDetails  as $orderDetails) {
                        $add=[];
                        $add['payment_master_id'] = $payment->id;
                        $add['product_order_details_id'] = $orderDetails->id;
                        $add['professional_amount'] = $orderDetails->professional_amount;
                        $add['admin_commission'] = $orderDetails->admin_commission;
                        $add['affiliate_commission'] = $orderDetails->affiliate_commission;
                        $add['affiliate_id'] = $orderDetails->affiliate_id;
                        $add['professional_id'] = $orderDetails->professional_id;
                        $add['type'] = 'P';
                        $add['withdraw_by_wirecard'] = $orderDetails->is_wirecard;
                        $add['balance_status'] = 'R';
                        $add['affiliate_withdraw_by_wirecard'] = $orderDetails->affiliate_is_wirecard;
                        $add['affiliate_balance_status'] = 'R';
                        PaymentDetails::create($add);

                        $userdata = User::where('id', Auth::id())->first();
                        $userdata1 = User::where('id', @$orderDetails->professional_id)->first();
                        $professionalmail = $userdata1->email;
                        $usermail = $userdata->email;
                        $bookingdata = $orderDetails;
                        $profName= @$userdata1->nick_name ? @$userdata1->nick_name : @$userdata1->name;
                        $html=$html.'<div><br /><strong>Product Name:</strong>'. $bookingdata->product->title;
                        $html=$html.'<br /><strong>Product Category:</strong>'. $bookingdata->product->category->category_name;
                        $html=$html.'<br /><strong>purchase Date:</strong>'. toUserTime($productOrder->order_date,'Y-m-d');
                        $html=$html.'<br /><strong>Professional Name:&nbsp;</strong>'.$profName ;
                        $html=$html.'<br /><strong>Amount:&nbsp;</strong>'.$bookingdata->amount.'<br /><br /></div>';

                        // variable for each product anme and price
                        // for send mail to user
                        // Mail::send(new UserProductOrder($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));

                        // for send mail to professional
                        Mail::send(new ProfessionalProductOrder($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                        // whatsHere
                        // to professional for $orderDetails
                        if(@$userdata1->mobile && @$userdata1->country_code){

                            /* $msg = "Dear " . (@$userdata1->nick_name ?? @$userdata1->name) .
                            ", your product: " . (@$bookingdata->product->title) . " has been purchased by " .
                            (@$userdata->nick_name ?? @$userdata->name) .
                            ". Order Token: " . (@$productOrder->token_no); */

                            $msg = "Olá ". (@$userdata1->nick_name ?? @$userdata1->name) . "
                            , Obrigado por usar a Netbhe! Nós recebemos seu pedido: "
                            . (@$bookingdata->product->title) . " que foi adquirido por "
                            . (@$userdata->nick_name ?? @$userdata->name) . ". Esperamos que goste de seu produto! 😉
                            Token de pedido:" . (@$productOrder->token_no);

                            $mobile = "+".@$userdata1->country_code . @$userdata1->mobile;
                            // $mobile = "+554598290176";
                            // $mobile = "+918017096564";
                            // dd($msg);
                            $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                            // if($result['statusCode'] == 200){
                            //     dd("Success");
                            // } else {
                            //     dd("Error");
                            // }
                        }
                    }
                    $userdata = User::where('id', Auth::id())->first();
                    $usermail = $userdata->email;
                    $product_html=$html;
                    // dd($product_html);
                    User::where('id', $userdata->id)->update(['wallet_balance'=>($userdata->wallet_balance - @$productOrder->wallet)]);
                    Mail::send(new UserProductOrder($usermail, $userdata, $product_html));

                    // whatsHere
                    // your product has been purchased
                    // variable for all products together
                    $UpdateBooking = ProductOrder::where([
                        'token_no'        =>  @$productOrder->token_no
                    ])->update([
                        'payment_status'  =>  "P"
                    ]);
                    $referralStatus = User::where('id', @$productOrder->user_id)->first();
                    if (@$referralStatus->is_paid_activity == 'N') {
                        User::where('id', @$productOrder->user_id)->update(['is_paid_activity' => 'C']);
                        User::where('id', @$referralStatus->referrer_id)->increment("benefit_count", 1);
                    }
                    // $userdata = User::where('id', Auth::id())->first();
                    // $userdata1 = User::where('id', @$productOrder->professional_id)->first();
                    // $professionalmail = $userdata1->email;
                    // $usermail = $userdata->email;
                    // $bookingdata = $productOrder;

                    // // for send mail to user
                    // Mail::send(new UserProductOrder($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));

                    // // for send mail to professional
                    // Mail::send(new ProfessionalProductOrder($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                    session()->flash('success', \Lang::get('client_site.product_ordered_successfully'));
                    return true;
                } else {
                    $UpdateBooking = ProductOrder::where([
                        'token_no'        =>  @$productOrder->token_no
                    ])->update([
                        'payment_status'  =>  "F"
                    ]);

                    return false;
                }
            }
        }
    }

    /**
     *method:createCustomer()
     *purpose:For Creating Customer on WireCard
     *Author:@bhisek
     */
    public function createCustomer(Request $request)
    {
        try {
            if (env('PAYMENT_ENVIORNMENT') == 'live') {
                $moip = new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_PRODUCTION);
            } else if (env('PAYMENT_ENVIORNMENT') == 'sandbox') {
                $moip = new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_SANDBOX);
            }

            $customer = $moip->customers()
                ->setOwnId(uniqid())
                ->setFullname(Auth::guard('web')->user()->name)
                ->setEmail(Auth::guard('web')->user()->email)
                ->setBirthDate(Auth::guard('web')->user()->dob)
                ->setTaxDocument(Auth::guard('web')->user()->cpf_no)
                ->setPhone(Auth::guard('web')->user()->area_code, Auth::guard('web')->user()->mobile)
                ->addAddress('BILLING', Auth::guard('web')->user()->street_name, Auth::guard('web')->user()->street_number, Auth::guard('web')->user()->district, Auth::guard('web')->user()->city, strtoupper(substr(Auth::guard('web')->user()->stateName->name, 0, 2)), Auth::guard('web')->user()->zipcode, Auth::guard('web')->user()->complement)
                ->addAddress('SHIPPING', Auth::guard('web')->user()->street_name, Auth::guard('web')->user()->street_number, Auth::guard('web')->user()->district, Auth::guard('web')->user()->city, substr(Auth::guard('web')->user()->stateName->name, 0, 2), Auth::guard('web')->user()->zipcode, Auth::guard('web')->user()->complement)->create();
            # SAVE CUSTOMER CREADIT CARD
            $card  =  $moip->customers()->creditCard()
                ->setExpirationMonth($request->expiry_month)
                ->setExpirationYear($request->expiry_year)
                ->setNumber($request->cardnumber)
                ->setCVC($request->cvc)
                ->setFullName($request->card_name)
                ->setBirthDate(Auth::guard('web')->user()->dob)
                ->setTaxDocument('CPF', Auth::guard('web')->user()->cpf_no)
                ->setPhone(Auth::guard('web')->user()->countryName->phonecode, Auth::guard('web')->user()->area_code, Auth::guard('web')->user()->mobile)
                ->create($customer->getId());
            if (!@$card->getId()) {
                session()->flash('error', \Lang::get('client_site.internal_server_error'));
                return redirect()->back();
            }

            $update = User::where('id', Auth::id())->update([
                'customer_id'                       =>  @$customer->getId(),
                'is_credit_card_added'              =>  @$card->getStore() == true ? 'Y' : 'N'
            ]);
            $is_Created = UserToCard::create([
                'user_id'       =>  Auth::id(),
                'card_id'       =>  $card->getId(),
                'first_six'     =>  $card->getFirst6(),
                'last_four'     =>  $card->getLast4(),
                'card_brand'    =>  $card->getBrand(),
                'cred_type'     =>  "CREDIT_CARD"
            ]);
            if (@$is_Created) {
                // Booking::where('token_no', @$request->order_no)->update([
                //     'order_status'  =>  'A'
                // ]);
                session()->flash('success', \Lang::get('client_site.card_successfully_added'));
                return redirect()->back();
            } else {
                session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
                return redirect()->back();
            }
        } catch (\Exception $e) {
            session()->flash('error', \Lang::get('client_site.internal_server_error'));
            return redirect()->back();
        }
    }

    /*
    @Below Functions are used for validating a CPF number.
    */
    public function verificaDigitos($digito1, $digito2, $ver, $ver2)
    {
        $num = $digito1 % 11;
        $digito1 = ($num < 2) ? 0 : 11 - $num;
        $num2 = $digito2 % 11;
        $digito2 = ($num2 < 2) ? 0 : 11 - $num2;
        if ($digito1 === (int) $this->numeral[$ver] && $digito2 === (int) $this->numeral[$ver2]) {
            return true;
        } else {
            return false;
        }
    }
    private function verificaCPF()
    {
        $num1 = ($this->numeral[0] * 10) + ($this->numeral[1] * 9) + ($this->numeral[2] * 8) + ($this->numeral[3] * 7) + ($this->numeral[4] * 6)
            + ($this->numeral[5] * 5) + ($this->numeral[6] * 4) + ($this->numeral[7] * 3) + ($this->numeral[8] * 2);
        $num2 = ($this->numeral[0] * 11) + ($this->numeral[1] * 10) + ($this->numeral[2] * 9) + ($this->numeral[3] * 8) + ($this->numeral[4] * 7)
            + ($this->numeral[5] * 6) + ($this->numeral[6] * 5) + ($this->numeral[7] * 4) + ($this->numeral[8] * 3) + ($this->numeral[9] * 2);
        return $this->verificaDigitos($num1, $num2, 9, 10);
    }
    private function verificaCNPJ()
    {
        $num1 = ($this->numeral[0] * 5) + ($this->numeral[1] * 4) + ($this->numeral[2] * 3) + ($this->numeral[3] * 2) + ($this->numeral[4] * 9) + ($this->numeral[5] * 8)
            + ($this->numeral[6] * 7) + ($this->numeral[7] * 6) + ($this->numeral[8] * 5) + ($this->numeral[9] * 4) + ($this->numeral[10] * 3) + ($this->numeral[11] * 2);
        $num2 = ($this->numeral[0] * 6) + ($this->numeral[1] * 5) + ($this->numeral[2] * 4) + ($this->numeral[3] * 3) + ($this->numeral[4] * 2) + ($this->numeral[5] * 9)
            + ($this->numeral[6] * 8) + ($this->numeral[7] * 7) + ($this->numeral[8] * 6) + ($this->numeral[9] * 5) + ($this->numeral[10] * 4) + ($this->numeral[11] * 3) + ($this->numeral[12] * 2);
        return $this->verificaDigitos($num1, $num2, 12, 13);
    }
    private function getNumeral()
    {
        $this->numeral = preg_replace("/[^0-9]/", "", $this->CPF_CNPJ);
        $strLen = strlen($this->numeral);
        $ret = false;
        switch ($strLen) {
            case 11:
                if ($this->verificaCPF()) {
                    $this->tipo = 'CPF';
                    $ret = true;
                }
                break;
            case 14:
                if ($this->verificaCNPJ()) {
                    $this->tipo = 'CNPJ';
                    $ret = true;
                }
                break;
            default:
                $ret = false;
                break;
        }
        return $ret;
    }
    public function __toString()
    {
        return $this->CPF_CNPJ;
    }
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * For Free Session Payment Create
     * @method: freeSessionPayment
     */
    private function freeSessionPayment($token)
    {
        $userBooking = Booking::where('token_no', $token)->first();
        $payment = Payment::create([
            'token_no'              =>  @$userBooking->token_no,
            'order_id'              =>  'ORD-' . @$userBooking->token_no,
            // 'payment_id'            =>  @$payment_output->id,
            'user_id'               =>  @$userBooking->user_id,
            'professional_id'       =>  @$userBooking->professional_id,
            'professional_amount'   =>  @$userBooking->amount / 2,
            'admin_amount'          =>  @$userBooking->amount / 2,
            'total_amount'          =>  @$userBooking->amount,
            // 'wirecard_response'     =>  json_encode($payment_output),
            'balance_status'        =>  'W'
        ]);
        // Booking::where([
        //     'token_no' => @$token
        // ])->update([
        //     'moip_order_id'  =>  'ORD-' . @$userBooking->token_no
        // ]);
        if (@$payment) {
            $UpdateBooking = Booking::where([
                'token_no'        =>  @$userBooking->token_no
            ])->update([
                'payment_status'  =>  "P"
            ]);
            $userdata = User::where('id', Auth::id())->first();
            $userdata1 = User::where('id', @$userBooking->professional_id)->first();
            $professionalmail = $userdata1->email;
            $usermail = $userdata->email;
            $bookingdata = $userBooking;

            // // for send mail to user
            Mail::send(new UserBooking($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));

            // // for send mail to professional
            Mail::send(new ProfessionalBooking($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
            // $referralStatus= User::where('id', @$userBooking->user_id)->first();
            // if(@$referralStatus->is_paid_activity=='N'){
            //     User::where('id', @$userBooking->user_id)->update(['is_paid_activity'=>'C']);
            //     User::where('id', @$referralStatus->referrer_id)->increment("benefit_count", 1);
            // }
            return true;
        } else {
            $UpdateBooking = Booking::where([
                'token_no'        =>  @$userBooking->token_no
            ])->update([
                'payment_status'  =>  "F"
            ]);
            return false;
        }
    }
    /**
     * For Check Coupon Code
     * @method: checkCoupon
     */
    public function checkCoupon(Request $request)
    {
        $response = [
            "jsonrpc" => "2.0"
        ];
        if ($request->params['coupon']) {

            $coupon = Coupon::where('coupon_code', $request->params['coupon'])->where('coupon_status', 'A')->first();
            $dated = date_create(@$coupon->exp_date);
            if (@$coupon) {
                if ($dated < date('Y-m-d')) {
                    $response['status'] = 2;
                    $response['message'] = \Lang::get('client_site.coupon_expiredy');
                } else {
                    if ($coupon->added_by=='P') {
                        $coupon_to_product = CouponToProduct::where('coupon_id', $coupon->id)->whereIn('product_id', $request->params['product_id'])->first();
                        // return $request->params['product_id'];
                        if (@$coupon_to_product) {
                            $response['status'] = 1;
                            $response['message'] = \Lang::get('client_site.coupon_applied');
                        } else {
                            $response['status'] = 0;
                            $response['message'] = \Lang::get('client_site.coupon_not_valid');
                        }
                    } else {
                        $response['status'] = 1;
                        $response['message'] = \Lang::get('client_site.coupon_applied');
                    }
                }
            } else {
                $response['status'] = 0;
                $response['message'] = \Lang::get('client_site.coupon_not_valid');
            }
            return response()->json($response);
        }
    }
    /**
     * For Add Payment Method and Coupon Apply
     * @method: addPaymentCoupon
     */
    public function addPaymentCoupon($slug, Request $request)
    {
        $request->validate([
            "payment_method" =>  "required|in:C,BA,S,P",
        ]);
        $booking = ProductOrder::where('token_no', $slug)->where('user_id', @Auth::id())->first();
        // dd($booking);
        $coupon = Coupon::where('coupon_code', $request->couponCode)->with('couponToProduct')->where('coupon_status', 'A')->first();
        // $checkStudent = User::where('id', @Auth::id())->first();
        // $referDiscount = ReferralDiscount::first();
        // return $request;
        if (@$booking) {
            $upd = [];
            $upd['payment_type'] = $request->payment_method;
            // dd($upd['payment_type']);
            if (@$request->installment_select) {
                if (@$request->no_installment > 1 && @$request->no_installment < 13) {
                    $i = $request->no_installment;
                    $x2 = 4.50;
                    $x3 = 5.00;
                    $x4 = 5.50;
                    $x5 = 6.50;
                    $x6 = 7.50;
                    $x7 = 8.50;
                    $x8 = 9.50;
                    $x9 = 10.50;
                    $x10 = 11.50;
                    $x11 = 12.00;
                    $x12 = 12.50;
                    if ($i == 2) {
                        $x = $x2;
                    }
                    if ($i == 3) {
                        $x = $x3;
                    }
                    if ($i == 4) {
                        $x = $x4;
                    }
                    if ($i == 5) {
                        $x = $x5;
                    }
                    if ($i == 6) {
                        $x = $x6;
                    }
                    if ($i == 7) {
                        $x = $x7;
                    }
                    if ($i == 8) {
                        $x = $x8;
                    }
                    if ($i == 9) {
                        $x = $x9;
                    }
                    if ($i == 10) {
                        $x = $x10;
                    }
                    if ($i == 11) {
                        $x = $x11;
                    }
                    if ($i == 12) {
                        $x = $x12;
                    }
                    $down = (1 - ($x / 100) - (5.49 / 100));
                    $up = $booking->sub_total * ($x / 100);
                    $additional = ($up / $down);
                    if ($booking->installment_charge == 'Y') {
                        $additional = 0.00;
                    }

                    $upd['no_of_installment'] = @$request->no_installment;
                    $upd['extra_installment_charge'] = @$additional;
                }
            }
            // if (@$coupon && @$request->installment_select == null) {
            //     $subtotal = $booking->amount - (($booking->amount * $coupon->discount) / 100);
            //     $upd['sub_total'] = (int) $subtotal;
            //     $upd['coupon_id'] = $coupon->id;
            //     $upd['is_coupon'] = 'Y';
            // }
            $update = ProductOrder::where('id', $booking->id)->update($upd);
            $productData = ProductOrder::where('id', $booking->id)->with('orderDetails')->first();
            if (@$update) {
                foreach($productData->orderDetails  as $cart){
                    if ($booking->installment_charge == 'Y'&& @$request->no_installment > 1 && @$request->no_installment < 13) {
                        $product    = Product::where('id', $cart->product_id)->with('category')->first();
                        // $discount   = $product->category->commission;
                        // $discount   = $product->professional->product_commission;
                        if(@$product->professional->product_commission>0 && $product->professional->product_commission!=null){
                            $discount   = @$product->professional->product_commission;
                        }else{
                            $discount   = $product->category->commission;
                        }
                        $amount     = (@$cart->amount * @$discount-$x) / 100;
                        $professional_amount    =  @$amount;
                        $admin_amount   =   @$cart->amount - @$amount;
                        $update1 = [];
                        $update1['professional_amount']         = $professional_amount;
                        $update1['admin_commission']            = $admin_amount;
                        ProductOrderDetails::where('id', $cart->id)->update($update1);
                    }
                    if(@$coupon && @$request->installment_select == null){
                        if($coupon->added_by=='A'){
                            $product    = Product::where('id', $cart->product_id)->with('category')->first();
                            // $discount   = $product->category->commission;
                            // $discount   = $product->professional->product_commission;
                            if(@$product->professional->product_commission>0 && $product->professional->product_commission!=null){
                                $discount   = @$product->professional->product_commission;
                            }else{
                                $discount   = $product->category->commission;
                            }
                            $product_amount = @$cart->amount - ((@$cart->amount * @$coupon->discount) / 100);

                            if($cart->affiliate_id != 0){
                                $amount     = (@$product_amount * @$discount) / 100;
                                $affiliate_amount    =  (@$amount*$product->affiliate_percentage)/ 100;
                                $professional_amount    =  @$amount-$affiliate_amount;
                                $admin_amount   =   @$product_amount - @$amount;
                            }else{
                                $amount     = (@$product_amount * @$discount) / 100;
                                $professional_amount    =  @$amount;
                                $admin_amount   =   @$product_amount - @$amount;
                                $affiliate_amount    =  0;
                            }
                            $update2 = [];
                            $update2['amount']                      = $product_amount;
                            $update2['professional_amount']         = $professional_amount;
                            $update2['admin_commission']            = $admin_amount;
                            $update2['affiliate_commission']        = $affiliate_amount;
                            ProductOrderDetails::where('id', $cart->id)->update($update2);
                        }
                        if($coupon->added_by=='P' && @$coupon->couponToProduct->product_id == $cart->product_id){
                            $product    = Product::where('id', $cart->product_id)->with('category')->first();
                            // $discount   = $product->category->commission;
                            // $discount   = $product->professional->product_commission;
                            if(@$product->professional->product_commission>0 && $product->professional->product_commission!=null){
                                $discount   = @$product->professional->product_commission;
                            }else{
                                $discount   = $product->category->commission;
                            }
                            $product_amount= @$cart->amount - ((@$cart->amount * @$coupon->discount) / 100);

                            if($cart->affiliate_id != 0){
                                $amount     = (@$product_amount * @$discount) / 100;
                                $affiliate_amount    =  (@$amount*$product->affiliate_percentage)/ 100;
                                $professional_amount    =  @$amount-$affiliate_amount;
                                $admin_amount   =   @$product_amount - @$amount;
                            }else{
                                $amount     = (@$product_amount * @$discount) / 100;
                                $professional_amount    =  @$amount;
                                $admin_amount   =   @$product_amount - @$amount;
                                $affiliate_amount    =  0;
                            }
                            $update3 = [];
                            $update3['amount']                      = $product_amount;
                            $update3['professional_amount']         = $professional_amount;
                            $update3['admin_commission']            = $admin_amount;
                            $update3['affiliate_commission']        = $affiliate_amount;
                            ProductOrderDetails::where('id', $cart->id)->update($update3);
                        }
                    }
                }
                $productAll = ProductOrderDetails::where('product_order_master_id', $booking->id)->get();
                $upd1=[];
                if (@$coupon && @$request->installment_select == null) {
                    $subtotal = $productAll->sum('amount');
                    $upd1['sub_total'] = $subtotal;
                    $upd1['coupon_id'] = $coupon->id;
                    $upd1['is_coupon'] = 'Y';
                }
                $update = ProductOrder::where('id', $booking->id)->update($upd1);
                return redirect()->back();
            }
        }
        return redirect()->back();
    }
    /**
     * For Free Session Payment Create
     * @method: freeSessionPayment
     */
    private function bankAccountPayment($token) //herework
    {
        $userBooking = ProductOrder::where('token_no', $token)->first()->load('orderDetails','profDetails', 'productDetails', 'productCategoryDetails');
        $referDiscount = ReferralDiscount::first();
        $checkTeacher = User::where('id', $userBooking->professional_id)->first();

        // $productCategoryCommission = Commission::first();

        if ($userBooking->sub_total == 0) {
            $payment = Payment::create([
                'token_no'              =>  @$userBooking->token_no,
                'order_id'              =>  'ORD-' . @$userBooking->token_no,
                // 'payment_id'            =>  @$payment_output->id,
                'user_id'               =>  @$userBooking->user_id,
                'professional_id'       =>  @$userBooking->professional_id,
                // 'professional_amount'   =>  @$professional_amount,
                // 'admin_amount'          =>  @$admin_amount,
                // 'total_amount'          =>  @$total_amount,
                // 'wirecard_response'     =>  json_encode($payment_output),
                'order_type'     =>  'P',
                'balance_status'        =>  'R',
                'wallet'                =>  @$userBooking->wallet,
                'is_wallet_use'         =>  @$userBooking->is_wallet_use,
            ]);
            if (@$payment) {
                $UpdateBooking = ProductOrder::where([
                    'token_no'        =>  @$userBooking->token_no
                ])->update([
                    'payment_status'  =>  "PR",
                    // 'order_status'  =>  "A"
                ]);
                // $referralStatus = User::where('id', @$userBooking->user_id)->first();
                // if (@$referralStatus->is_paid_activity == 'N') {
                //     User::where('id', @$userBooking->user_id)->update(['is_paid_activity' => 'C']);
                //     User::where('id', @$referralStatus->referrer_id)->increment("benefit_count", 1);
                // }
                $userdata = User::where('id', Auth::id())->first();
                $userdata1 = User::where('id', @$userBooking->professional_id)->first();
                $professionalmail = $userdata1->email;
                $usermail = $userdata->email;
                $bookingdata = $userBooking;

                // // for send mail to user
                // Mail::send(new UserBooking($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));

                // // // for send mail to professional
                // Mail::send(new ProfessionalBooking($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                // for send mail to admin for Bank account payment
                Mail::send(new UserBankAccountProductOrder($bookingdata->id, $usermail));

                Mail::send(new BankAccountPaymentProduct($bookingdata->id));
                return true;
            } else {
                $UpdateBooking = Booking::where([
                    'token_no'        =>  @$userBooking->token_no
                ])->update([
                    'payment_status'  =>  "F"
                ]);
                return false;
            }
        } else {
            // if (@$checkTeacher->is_paid_activity == 'N') {
            //     $discount = $userBooking->productCategoryDetails->commission + $referDiscount->referrer_teacher_discount;
            //     $amount = (@$userBooking->amount * @$discount) / 100;
            //     $professional_amount   =  @$amount;
            //     $admin_amount         =   @$userBooking->sub_total - @$amount;
            //     User::where('id', @$checkTeacher->id)->update(['is_paid_activity' => 'C']);
            //     User::where('id', @$checkTeacher->referrer_id)->increment("benefit_count", 1);
            // } elseif (@$checkTeacher->benefit_count > 0) {
            //     $discount = $userBooking->productCategoryDetails->commission + $referDiscount->referred_teacher_discount;
            //     $amount = (@$userBooking->amount * @$discount) / 100;
            //     $professional_amount   =  @$amount;
            //     $admin_amount         =   @$userBooking->sub_total - @$amount;
            //     User::where('id', @$checkTeacher->id)->decrement("benefit_count", 1);
            // } else {
            // $discount = $userBooking->productCategoryDetails->commission;

            // $amount = (@$userBooking->sub_total * @$discount) / 100;
            // $professional_amount   =  @$amount;
            // $admin_amount         =   @$userBooking->sub_total - @$amount;

            // if ($professional_amount < 1) {
            //     $professional_amount = 1;
            //     $admin_amount = @$productOrder->sub_total - $professional_amount;
            // }
            // if ($admin_amount < 1) {
            //     $admin_amount = 1;
            //     $professional_amount = @$productOrder->sub_total - $admin_amount;
            // }
            // }
            $total_amount         =  @$userBooking->sub_total;
            // dd($checkTeacher->benefit_count);
            // $payment = Payment::create([
            //     'token_no'              =>  @$userBooking->token_no,
            //     'order_id'              =>  'ORD-' . @$userBooking->token_no,
            //     // 'payment_id'            =>  @$payment_output->id,
            //     'user_id'               =>  @$userBooking->user_id,
            //     'professional_id'       =>  @$userBooking->professional_id,
            //     'professional_amount'   =>  @$professional_amount,
            //     'admin_amount'          =>  @$admin_amount,
            //     'total_amount'          =>  @$total_amount,
            //     // 'wirecard_response'     =>  json_encode($payment_output),
            //     'order_type'     =>  'P',
            //     'balance_status'        =>  'R'
            // ]);


            $payment = Payment::create([
                'token_no'              =>  @$userBooking->token_no,
                'pg_order_id'              =>  'ORD-' . @$userBooking->token_no,
                'user_id'               =>  @$userBooking->user_id,
                'total_amount'          =>  @$total_amount,
                'payment_status'     =>  'PR',
                'payment_type'     =>  'BA',
                'order_type'     =>  'P',
                'wallet'                =>  @$userBooking->wallet,
                'is_wallet_use'         =>  @$userBooking->is_wallet_use,
            ]);
            if (@$payment) {
                $html='';
                Cart::where('user_id', Auth::user()->id)->delete();
                foreach (@$userBooking->orderDetails  as $orderDetails) {
                    $add = [];
                    $add['payment_master_id'] = $payment->id;
                    $add['product_order_details_id'] = $orderDetails->id;
                    $add['professional_amount'] = $orderDetails->professional_amount;
                    $add['admin_commission'] = $orderDetails->admin_commission;
                    $add['affiliate_commission'] = $orderDetails->affiliate_commission;
                    $add['affiliate_id'] = $orderDetails->affiliate_id;
                    $add['professional_id'] = $orderDetails->professional_id;
                    $add['type'] = 'P';
                    $add['withdraw_by_wirecard'] = 'N';
                    $add['balance_status'] = 'R';
                    $add['affiliate_withdraw_by_wirecard'] = 'N';
                    $add['affiliate_balance_status'] = 'R';
                    PaymentDetails::create($add);
                    $userdata = User::where('id', Auth::id())->first();
                    $userdata1 = User::where('id', @$orderDetails->professional_id)->first();
                    $professionalmail = $userdata1->email;
                    $usermail = $userdata->email;
                    $bookingdata = $orderDetails;

                    // for send mail to user
                    $profName= @$userdata1->nick_name ? @$userdata1->nick_name : @$userdata1->name;
                    $html=$html.'<div><br /><strong>Product Name:</strong>'. $bookingdata->product->title;
                    $html=$html.'<br /><strong>Product Category:</strong>'. $bookingdata->product->category->category_name;
                    $html=$html.'<br /><strong>purchase Date:</strong>'. toUserTime($userBooking->order_date,'Y-m-d');
                    $html=$html.'<br /><strong>Professional Name:&nbsp;</strong>'.$profName ;
                    $html=$html.'<br /><strong>Amount:&nbsp;</strong>'.$bookingdata->amount.'<br /><br /></div>';
                    // Mail::send(new UserProductOrder($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));

                    // for send mail to professional
                    Mail::send(new ProfessionalProductOrder($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                    // whatsHere
                    // to professional for $orderDetails
                    if(@$userdata1->mobile && @$userdata1->country_code){

                        /* $msg = "Dear " . (@$userdata1->nick_name ?? @$userdata1->name) .
                        ", your product: " . (@$bookingdata->product->title) . " has been purchased by " .
                        (@$userdata->nick_name ?? @$userdata->name) .
                        ". As it is paid through a bank account, currently it is under Admin Approval. Order Token: " . (@$userBooking->token_no); */

                        $msg = "Olá " . (@$userdata1->nick_name ?? @$userdata1->name) . ",
                        Obrigado por usar a Netbhe! Nós recebemos seu pedido: " . (@$bookingdata->product->title) .
                        " que foi adquirido por " . (@$userdata->nick_name ?? @$userdata->name) .
                        ". Como o pagamento foi por meio de transferência bancária, estamos aguardando a confirmação do pagamento.
                        Por favor, envie o comprovante pela plataforma.
                        Esperamos que goste de seu produto! 😉 Token de reserva:" . (@$userBooking->token_no);

                        $mobile = "+".@$userdata1->country_code . @$userdata1->mobile;
                        // $mobile = "+554598290176";
                        // $mobile = "+918017096564";
                        // dd($msg);
                        $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                        // if($result['statusCode'] == 200){
                        //     dd("Success");
                        // } else {
                        //     dd("Error");
                        // }
                    }
                }
                $userdata = User::where('id', Auth::id())->first();
                $usermail = $userdata->email;
                $product_html=$html;
                User::where('id', $userdata->id)->update(['wallet_balance'=>($userdata->wallet_balance - @$userBooking->wallet)]);
                // whatsHere
                // your product has been purchased
                // variable for all products together
                // dd($product_html);
                Mail::send(new UserProductOrder($usermail, $userdata, $product_html));
                $UpdateBooking = ProductOrder::where([
                    'token_no'        =>  @$userBooking->token_no
                ])->update([
                    'payment_status'  =>  "PR",
                    // 'order_status'  =>  "A"
                ]);
                // $referralStatus = User::where('id', @$userBooking->user_id)->first();
                // if (@$referralStatus->is_paid_activity == 'N') {
                //     User::where('id', @$userBooking->user_id)->update(['is_paid_activity' => 'C']);
                //     User::where('id', @$referralStatus->referrer_id)->increment("benefit_count", 1);
                // }
                // $userdata = User::where('id', Auth::id())->first();
                // $userdata1 = User::where('id', @$userBooking->professional_id)->first();
                // $professionalmail = $userdata1->email;
                // $usermail = $userdata->email;
                // $bookingdata = $userBooking;

                // // for send mail to user
                // Mail::send(new UserBooking($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));

                // // // for send mail to professional
                // Mail::send(new ProfessionalBooking($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                // for send mail to admin for Bank account payment
                // Mail::send(new UserBankAccountProductOrder($bookingdata->id, $usermail));

                // Mail::send(new BankAccountPaymentProduct($bookingdata->id));
                return true;
            } else {
                $UpdateBooking = Booking::where([
                    'token_no'        =>  @$userBooking->token_no
                ])->update([
                    'payment_status'  =>  "F"
                ]);
                return false;
            }
        }
    }
    /**
     * For upload Payment Document Bank Account Payment
     * @method: uploadDocument
     */
    public function uploadDocument($slug, Request $request)
    {
        $request->validate([
            "upload_file" =>  "required",
        ]);
        $booking = ProductOrder::where('token_no', $slug)->where('user_id', @Auth::id())->first();
        if (@$booking) {
            $upd = [];
            if (@$request->upload_file) {
                $upload_file = $request->upload_file;
                $filename = time() . '-' . rand(1000, 9999) . '.' . $upload_file->getClientOriginalExtension();
                Storage::putFileAs('public/Bank_Document', $upload_file, $filename);
                $upd['payment_document'] = $filename;
            }
            // dd($upd);
            $update = ProductOrder::where('id', $booking->id)->update($upd);
            if (@$update) {
                return redirect()->back();
            }
        }
        return redirect()->back();
    }

    /**
     * For Add Payment Method and Coupon Apply
     * @method: addPaymentCoupon
     */
    public function removePaymentMethod($slug, Request $request)
    {

        $booking = ProductOrder::where('token_no', $slug)->where('user_id', @Auth::id())->first();
        if (@$booking) {
            $upd = [];
            $upd['payment_type'] = null;
            $upd['no_of_installment'] = 1;
            $upd['extra_installment_charge'] = 0.00;
            $upd['sub_total'] = $booking->amount;
            $upd['coupon_id'] = null;
            $upd['is_coupon'] = 'N';
            $update = ProductOrder::where('id', $booking->id)->update($upd);
            $productData = ProductOrder::where('id', $booking->id)->with('orderDetails')->first();
            if (@$update) {
                foreach($productData->orderDetails  as $cart){
                    $cartData=Cart::where('user_id', Auth::user()->id)->where('product_id',$cart->product_id)->first();
                    $product    = Product::where('id', $cart->product_id)->with('category')->first();
                    // $discount   = $product->category->commission;
                    // $discount   = @$product->professional->product_commission;
                    if(@$product->professional->product_commission>0 && $product->professional->product_commission!=null){
                        $discount   = @$product->professional->product_commission;
                    }else{
                        $discount   = $product->category->commission;
                    }
                    // $amount     = (@$cartData->amount * @$discount) / 100;
                    // $professional_amount    =  @$amount;
                    // $admin_amount   =   @$cart->amount - @$amount;
                    if($cart->affiliate_id != 0){
                        $amount     = (@$cartData->amount * @$discount) / 100;
                        $affiliate_amount    =  (@$amount*$product->affiliate_percentage)/ 100;
                        $professional_amount    =  @$amount-$affiliate_amount;
                        $admin_amount   =   @$cartData->amount - @$amount;
                    }else{
                        $amount     = (@$cartData->amount * @$discount) / 100;
                        $affiliate_amount    =  0;
                        $professional_amount    =  @$amount;
                        $admin_amount   =   @$cartData->amount - @$amount;
                        $affiliate_amount    =  0;
                    }
                    $update = [];
                    $update['professional_amount']         = $professional_amount;
                    $update['admin_commission']            = $admin_amount;
                    $update['affiliate_commission']        = $affiliate_amount;
                    $update['amount']            = @$cartData->amount;
                    productOrderDetails::where('id', $cart->id)->update($update);
                }
                return redirect()->back();
            }
        }
        return redirect()->back();
    }
    /**
     * Method: paymentCreate
     * Description: This method is used for create payment by stripe
     * Author: Soumojit
     * date:24-JULY-2021
     */
    public function paymentCreate(Request $request)
    {
        $response = [
            'jsonrpc' => 2.0
        ];
        $unique_payment_id = decrypt($request->params['id']);
        $payments = ProductOrder::where('id', $unique_payment_id)->where('user_id', @auth()->user()->id)->first();
        if(@$payments){
            Stripe::setApiKey(config('services.stripe.secret'));
            $paymentIntent = PaymentIntent::create([
                'amount' => (int) bcmul(($payments->sub_total-$payments->wallet), 100),
                'currency' => 'usd',
                'description' => 'Product perched',
                'metadata' => [
                    'unique_payment_id' => encrypt($unique_payment_id),
                ],
                'receipt_email' => @auth()->user()->email,
            ]);
            $response['result']['clientSecret'] = $paymentIntent->client_secret;
            $response['result']['payment'] = $paymentIntent;
            return response()->json($response);
        }
        $response['error']['message'] = \Lang::get('message.error_in_Payment');
        return response()->json($response);
    }
    /**
     * Method: paymentSuccessHandlerStripe
     * Description: This method is used for after payment success by stripe
     * Author: Soumojit
     * date:24-JULY-2021
     */
    public function paymentSuccessHandlerStripe(Request $request) //herework
    {
        $response = [
            'jsonrpc' => 2.0
        ];
        $unique_payment_id = decrypt($request->params['id']);
        $payments = ProductOrder::where('id', $unique_payment_id)->where('user_id',  @Auth::id())->first();

        $userBooking = ProductOrder::where('token_no', $payments->token_no)->first()->load('orderDetails','profDetails', 'productDetails', 'productCategoryDetails');
        $referDiscount = ReferralDiscount::first();
        $checkTeacher = User::where('id', $userBooking->professional_id)->first();

        // $productCategoryCommission = Commission::first();

        if ($userBooking->sub_total == 0) {
            $payment = Payment::create([
                'token_no'              =>  @$userBooking->token_no,
                'order_id'              =>  'ORD-' . @$userBooking->token_no,
                'user_id'               =>  @$userBooking->user_id,
                'professional_id'       =>  @$userBooking->professional_id,
                'order_type'            =>  'P',
                'balance_status'        =>  'R',
                'wallet'                =>  @$userBooking->wallet,
                'is_wallet_use'         =>  @$userBooking->is_wallet_use,
            ]);
            if (@$payment) {
                $UpdateBooking = ProductOrder::where([
                    'token_no'        =>  @$userBooking->token_no
                ])->update([
                    'payment_status'  =>  "P",
                    // 'order_status'  =>  "A"
                ]);
                // $referralStatus = User::where('id', @$userBooking->user_id)->first();
                // if (@$referralStatus->is_paid_activity == 'N') {
                //     User::where('id', @$userBooking->user_id)->update(['is_paid_activity' => 'C']);
                //     User::where('id', @$referralStatus->referrer_id)->increment("benefit_count", 1);
                // }
                $userdata = User::where('id', Auth::id())->first();
                $userdata1 = User::where('id', @$userBooking->professional_id)->first();
                $professionalmail = $userdata1->email;
                $usermail = $userdata->email;
                $bookingdata = $userBooking;

                // // for send mail to user
                // Mail::send(new UserBooking($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));

                // // // for send mail to professional
                // Mail::send(new ProfessionalBooking($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                // for send mail to admin for Bank account payment
                Mail::send(new UserBankAccountProductOrder($bookingdata->id, $usermail));

                Mail::send(new BankAccountPaymentProduct($bookingdata->id));
                return true;
            } else {
                $UpdateBooking = Booking::where([
                    'token_no'        =>  @$userBooking->token_no
                ])->update([
                    'payment_status'  =>  "F"
                ]);
                return false;
            }
        } else {
            $total_amount         =  @$userBooking->sub_total;
            $payment = Payment::create([
                'token_no'              =>  @$userBooking->token_no,
                'pg_order_id'           =>  'ORD-' . @$userBooking->token_no,
                'user_id'               =>  @$userBooking->user_id,
                'total_amount'          =>  @$total_amount,
                'order_type'            =>  'P',
                'pg_payment_id'         =>  $request->params['result']['paymentIntent']['id'],
                'payment_status'        =>  'P',
                'payment_type'          =>  'S',
                'pg_response'           =>  json_encode($request->params['result']),
                'wallet'                =>  @$userBooking->wallet,
                'is_wallet_use'         =>  @$userBooking->is_wallet_use,
            ]);
            if (@$payment) {
                $html='';
                Cart::where('user_id', Auth::user()->id)->delete();
                foreach (@$userBooking->orderDetails  as $orderDetails) {
                    $add = [];
                    $add['payment_master_id'] = $payment->id;
                    $add['product_order_details_id'] = $orderDetails->id;
                    $add['professional_amount'] = $orderDetails->professional_amount;
                    $add['admin_commission'] = $orderDetails->admin_commission;
                    $add['affiliate_commission'] = $orderDetails->affiliate_commission;
                    $add['affiliate_id'] = $orderDetails->affiliate_id;
                    $add['professional_id'] = $orderDetails->professional_id;
                    $add['type'] = 'P';
                    $add['withdraw_by_wirecard'] = 'N';
                    $add['balance_status'] = 'R';
                    $add['affiliate_withdraw_by_wirecard'] = 'N';
                    $add['affiliate_balance_status'] = 'R';
                    PaymentDetails::create($add);
                    $userdata = User::where('id', Auth::id())->first();
                    $userdata1 = User::where('id', @$orderDetails->professional_id)->first();
                    $professionalmail = $userdata1->email;
                    $usermail = $userdata->email;
                    $bookingdata = $orderDetails;

                    // for send mail to user
                    $profName= @$userdata1->nick_name ? @$userdata1->nick_name : @$userdata1->name;
                    $html=$html.'<div><br /><strong>Product Name:</strong>'. $bookingdata->product->title;
                    $html=$html.'<br /><strong>Product Category:</strong>'. $bookingdata->product->category->category_name;
                    $html=$html.'<br /><strong>purchase Date:</strong>'. toUserTime($userBooking->order_date,'Y-m-d');
                    $html=$html.'<br /><strong>Professional Name:&nbsp;</strong>'.$profName ;
                    $html=$html.'<br /><strong>Amount:&nbsp;</strong>'.$bookingdata->amount.'<br /><br /></div>';
                    // Mail::send(new UserProductOrder($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));

                    // for send mail to professional
                    Mail::send(new ProfessionalProductOrder($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                    // whatsHere
                    // to professional for $orderDetails
                    if(@$userdata1->mobile && @$userdata1->country_code){

                        /* $msg = "Dear " . (@$userdata1->nick_name ?? @$userdata1->name) .
                        ", your product: " . (@$bookingdata->product->title) . " has been purchased by " .
                        (@$userdata->nick_name ?? @$userdata->name) .
                        ". Order Token: " . (@$userBooking->token_no); */

                        $msg = "Olá ". (@$userdata1->nick_name ?? @$userdata1->name) . ",
                        Obrigado por usar a Netbhe! Nós recebemos seu pedido: "
                        . (@$bookingdata->product->title) . " que foi adquirido por "
                        . (@$userdata->nick_name ?? @$userdata->name) . ". Esperamos que goste de seu produto! 😉
                        Token de pedido:" . (@$userBooking->token_no);

                        $mobile = "+".@$userdata1->country_code . @$userdata1->mobile;
                        // $mobile = "+554598290176";
                        // $mobile = "+918017096564";
                        // dd($msg);
                        $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                        // if($result['statusCode'] == 200){
                        //     dd("Success");
                        // } else {
                        //     dd("Error");
                        // }
                    }
                }
                $userdata = User::where('id', Auth::id())->first();
                $usermail = $userdata->email;
                $product_html=$html;
                // dd($product_html);
                User::where('id', $userdata->id)->update(['wallet_balance'=>($userdata->wallet_balance - @$userBooking->wallet)]);
                Mail::send(new UserProductOrder($usermail, $userdata, $product_html));
                // whatsHere
                // your product has been purchased
                // variable for all products together
                $UpdateBooking = ProductOrder::where([
                    'token_no'        =>  @$userBooking->token_no
                ])->update([
                    'payment_status'  =>  "P",
                    // 'order_status'  =>  "A"
                ]);
                $response['success']['message'] = \Lang::get('site.Payment_successfull');
                return response()->json($response);
            } else {
                $UpdateBooking = Booking::where([
                    'token_no'        =>  @$userBooking->token_no
                ])->update([
                    'payment_status'  =>  "F"
                ]);
                $response['error']['message'] = \Lang::get('client_site.payment_not_complete');
                return response()->json($response);
            }
        }
    }


    /**
     * Method: postPaymentWithpaypal
     * Description: This method is used for payment by paypal
     * Author: Sayantani
     * date:13-AUGUST-2021
     */
    public function postPaymentWithpaypal(Request $request)
    {
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        // $this->_ordID = $request->get('ordID');
        // dd($request->get('ordID'));
        Session::put('ordID',encrypt($request->get('ordID')));
        // dd($request->get('ordID'));
        $unique_payment_id = $request->get('ordID');
        // dd($unique_payment_id);
        $payments = ProductOrder::where('id', $unique_payment_id)->where('user_id',  @Auth::id())->first();

        $userBooking = ProductOrder::where('token_no', $payments->token_no)->first()->load('orderDetails','profDetails', 'productDetails', 'productCategoryDetails');
        $referDiscount = ReferralDiscount::first();
        $checkTeacher = User::where('id', $userBooking->professional_id)->first();

    	$item_1 = new Item();

        $item_1->setName($request->get('title'))
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setPrice($request->get('amount'));

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency('USD')
            ->setTotal($request->get('amount'));

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Enter Your transaction description');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(route('paypal.status'))
            ->setCancelUrl(route('paypal.status'));

        $payment = new PaymentPaypal();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                // Session::put('error','Connection timeout');
                session()->flash('error', \Lang::get('client_site.connection_timeout'));
                // return Redirect::route('paywithpaypal');
                return Redirect::route('product.order.store.success', @$userBooking->token_no);
            } else {
                // Session::put('error','Some error occur, sorry for inconvenient');
                session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
                // return Redirect::route('paywithpaypal');
                return Redirect::route('product.order.store.success', @$userBooking->token_no);
            }
        }

        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        Session::put('paypal_payment_id', $payment->getId());

        if(isset($redirect_url)) {
            // dd($redirect_url);
            // return Redirect::away($redirect_url);
            return redirect()->away($redirect_url)->send();
        }

        // Session::put('error','Unknown error occurred');
        session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
    	// return Redirect::route('paywithpaypal');
        return Redirect::route('product.order.store.success', @$userBooking->token_no);
    }

    /**
     * Method: getPaymentStatus
     * Description: This method is used after payment success by paypal
     * Author: Sayantani
     * date:13-AUGUST-2021
     */
    public function getPaymentStatus(Request $request)
    {
        $payment_id = Session::get('paypal_payment_id');
        $ordID = decrypt(Session::get('ordID'));

        $unique_payment_id = $ordID;
        $payments = ProductOrder::where('id', $unique_payment_id)->where('user_id',  @Auth::id())->first();
        // dd($payments);
        $userBooking = ProductOrder::where('token_no', $payments->token_no)->first()->load('orderDetails','profDetails', 'productDetails', 'productCategoryDetails');
        $referDiscount = ReferralDiscount::first();
        $checkTeacher = User::where('id', $userBooking->professional_id)->first();

        Session::forget('paypal_payment_id');
        if (empty($request->input('PayerID')) || empty($request->input('token'))) {
            // Session::put('error','Payment failed');
            session()->flash('error', \Lang::get('site.Payment_failed'));
            // return Redirect::route('paywithpaypal');
            return Redirect::route('product.order.store.success', @$userBooking->token_no);
        }
        $payment = PaymentPaypal::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId($request->input('PayerID'));
        $result = $payment->execute($execution, $this->_api_context);
        $res_arr = $result->toArray();
        $res = json_decode($result);

        if ($result->getState() == 'approved') {
            // session()->flash('success', \Lang::get('site.Payment_successfull'));
            if ($userBooking->sub_total == 0) {
                $payment = Payment::create([
                    'token_no'              =>  @$userBooking->token_no,
                    'order_id'              =>  'ORD-' . @$userBooking->token_no,
                    'user_id'               =>  @$userBooking->user_id,
                    'professional_id'       =>  @$userBooking->professional_id,
                    'order_type'            =>  'P',
                    'balance_status'        =>  'R',
                    'wallet'                =>  @$userBooking->wallet,
                    'is_wallet_use'         =>  @$userBooking->is_wallet_use,
                ]);
                if (@$payment) {
                    $UpdateBooking = ProductOrder::where([
                        'token_no'        =>  @$userBooking->token_no
                    ])->update([
                        'payment_status'  =>  "P",
                        // 'order_status'  =>  "A"
                    ]);
                    // $referralStatus = User::where('id', @$userBooking->user_id)->first();
                    // if (@$referralStatus->is_paid_activity == 'N') {
                    //     User::where('id', @$userBooking->user_id)->update(['is_paid_activity' => 'C']);
                    //     User::where('id', @$referralStatus->referrer_id)->increment("benefit_count", 1);
                    // }
                    $userdata = User::where('id', Auth::id())->first();
                    $userdata1 = User::where('id', @$userBooking->professional_id)->first();
                    $professionalmail = $userdata1->email;
                    $usermail = $userdata->email;
                    $bookingdata = $userBooking;

                    // // for send mail to user
                    // Mail::send(new UserBooking($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));

                    // // // for send mail to professional
                    // Mail::send(new ProfessionalBooking($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                    // for send mail to admin for Bank account payment
                    Mail::send(new UserBankAccountProductOrder($bookingdata->id, $usermail));

                    Mail::send(new BankAccountPaymentProduct($bookingdata->id));
                    return true;
                } else {
                    $UpdateBooking = ProductOrder::where([
                        'token_no'        =>  @$userBooking->token_no
                    ])->update([
                        'payment_status'  =>  "F"
                    ]);
                    return false;
                }
            } else {
                $total_amount         =  @$userBooking->sub_total;
                $payment = Payment::create([
                    'token_no'              =>  @$userBooking->token_no,
                    'pg_order_id'           =>  'ORD-' . @$userBooking->token_no,
                    'user_id'               =>  @$userBooking->user_id,
                    'total_amount'          =>  @$total_amount,
                    'order_type'            =>  'P',
                    'pg_payment_id'         =>  $res_arr['id'],
                    'payment_status'        =>  'P',
                    'payment_type'          =>  'P',
                    'pg_response'           =>  json_encode($res),
                    'wallet'                =>  @$userBooking->wallet,
                    'is_wallet_use'         =>  @$userBooking->is_wallet_use,
                ]);
                // dd($payment);
                if (@$payment) {
                    $html='';
                    Cart::where('user_id', Auth::user()->id)->delete();
                    foreach (@$userBooking->orderDetails  as $orderDetails) {
                        $add = [];
                        $add['payment_master_id'] = $payment->id;
                        $add['product_order_details_id'] = $orderDetails->id;
                        $add['professional_amount'] = $orderDetails->professional_amount;
                        $add['admin_commission'] = $orderDetails->admin_commission;
                        $add['affiliate_commission'] = $orderDetails->affiliate_commission;
                        $add['affiliate_id'] = $orderDetails->affiliate_id;
                        $add['professional_id'] = $orderDetails->professional_id;
                        $add['type'] = 'P';
                        $add['withdraw_by_wirecard'] = 'N';
                        $add['balance_status'] = 'R';
                        $add['affiliate_withdraw_by_wirecard'] = 'N';
                        $add['affiliate_balance_status'] = 'R';
                        PaymentDetails::create($add);
                        $userdata = User::where('id', Auth::id())->first();
                        $userdata1 = User::where('id', @$orderDetails->professional_id)->first();
                        $professionalmail = $userdata1->email;
                        $usermail = $userdata->email;
                        $bookingdata = $orderDetails;

                        // for send mail to user
                        $profName= @$userdata1->nick_name ? @$userdata1->nick_name : @$userdata1->name;
                        $html=$html.'<div><br /><strong>Product Name:</strong>'. $bookingdata->product->title;
                        $html=$html.'<br /><strong>Product Category:</strong>'. $bookingdata->product->category->category_name;
                        $html=$html.'<br /><strong>purchase Date:</strong>'. toUserTime($userBooking->order_date,'Y-m-d');
                        $html=$html.'<br /><strong>Professional Name:&nbsp;</strong>'.$profName ;
                        $html=$html.'<br /><strong>Amount:&nbsp;</strong>'.$bookingdata->amount.'<br /><br /></div>';
                        // Mail::send(new UserProductOrder($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));

                        // for send mail to professional
                        Mail::send(new ProfessionalProductOrder($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                        // whatsHere
                        // paypal professional
                        if(@$userdata1->mobile && @$userdata1->country_code){

                            /* $msg = "Dear " . (@$userdata1->nick_name ?? @$userdata1->name) .
                            ", your product: " . (@$bookingdata->product->title) . " has been purchased by " .
                            (@$userdata->nick_name ?? @$userdata->name) .
                            ". Order Token: " . (@$userBooking->token_no); */

                            $msg = "Olá ". (@$userdata1->nick_name ?? @$userdata1->name) . ",
                            Obrigado por usar a Netbhe! Nós recebemos seu pedido: "
                            . (@$bookingdata->product->title) . " que foi adquirido por "
                            . (@$userdata->nick_name ?? @$userdata->name) . ". Esperamos que goste de seu produto! 😉
                            Token de pedido:" . (@$userBooking->token_no);


                            $mobile = "+".@$userdata1->country_code . @$userdata1->mobile;
                            // $mobile = "+554598290176";
                            // $mobile = "+918017096564";
                            // dd($msg);
                            $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                            // if($result['statusCode'] == 200){
                            //     dd("Success");
                            // } else {
                            //     dd("Error");
                            // }
                        }
                    }
                    $userdata = User::where('id', Auth::id())->first();
                    $usermail = $userdata->email;
                    $product_html=$html;
                    // dd($product_html);
                    User::where('id', $userdata->id)->update(['wallet_balance'=>($userdata->wallet_balance - @$userBooking->wallet)]);
                    Mail::send(new UserProductOrder($usermail, $userdata, $product_html));
                    // whatsHere
                    // foreach product
                    // for user
                    $UpdateBooking = ProductOrder::where([
                        'token_no'        =>  @$userBooking->token_no
                    ])->update([
                        'payment_status'  =>  "P",
                        // 'order_status'  =>  "A"
                    ]);
                    // $response['success']['message'] = 'Payment Successfully';
                    // return response()->json($response);
                    session()->flash('success', \Lang::get('site.Payment_successfull'));
                    return Redirect::route('product.order.store.success', @$userBooking->token_no);
                } else {
                    $UpdateBooking = ProductOrder::where([
                        'token_no'        =>  @$userBooking->token_no
                    ])->update([
                        'payment_status'  =>  "F"
                    ]);
                    // Session::put('error','Payment Not Complete');
                    session()->flash('error', \Lang::get('site.Payment_failed'));
                    return Redirect::route('product.order.store.success', @$userBooking->token_no);
                    // $response['error']['message'] = 'Payment Not Complete';
                    // return response()->json($response);
                }
            }
            return Redirect::route('product.order.store.success', @$userBooking->token_no);
		    // return Redirect::route('paywithpaypal');
        }

        // Session::put('error','Payment failed !!');
        session()->flash('error', \Lang::get('site.Payment_failed'));
        $UpdateBooking = ProductOrder::where([
            'token_no'        =>  @$userBooking->token_no
        ])->update([
            'payment_status'  =>  "F"
        ]);
        return Redirect::route('product.order.store.success', @$userBooking->token_no);
		// return Redirect::route('paywithpaypal');
    }


    /**
     * Method:  requestProductCancel
     * Description: cancelation request by user
     * Author: Soumojit
     * date:05-OCT-2021
     */
    public function requestProductCancel(Request $request,$token=null,$id=null){

        $productOrder=ProductOrder::where('token_no',$token)->first();
        if($productOrder==null){
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        $productOrderDetails=ProductOrderDetails::where('id',$id)->with('orderMaster')->where('product_order_master_id',$productOrder->id)->first();
        if($productOrderDetails==null){
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        if($productOrderDetails->order_status=='C'){
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        if($productOrderDetails->is_cancel_responce=='A'){
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        if($productOrderDetails->is_cancel_responce=='R'){
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        if($productOrder->payment_status=='P'){
            $upd1=[];
            $upd1['order_cancelled_by']='U';
            $upd1['cancel_request']='Y';
            $upd1['cancelled_on']=date('Y-m-d');
            $upd1['cancellation_reason']=@$request->reason;
            ProductOrderDetails::where('id', $productOrderDetails->id)->update($upd1);

            $userdata = User::where('id', $productOrder->user_id)->first();
            $userdata1 = User::where('id', @$productOrderDetails->professional_id)->first();
            $professionalmail = $userdata1->email;
            $usermail = $userdata->email;
            $bookingdata = $productOrderDetails;
            Mail::send(new ProfessionalProductOrderCancelRequest($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));

            Mail::send(new AdminProductOrderCancelRequest($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));

            session()->flash('success', \Lang::get('site.cancelation_request_message'));
            return redirect()->back();
        }
        session()->flash('error', \Lang::get('client_site.unauthorize_access'));
        return redirect()->back();

    }
    /**
     * Method: productOrderCancel
     * Description:for cancel product Order
     * Author: Soumojit
     * date:04-OCT-2021
     */
    public function  productOrderCancel(Request $request,$token=null,$id=null){
        $productOrder=ProductOrder::where('token_no',$token)->first();
        if($productOrder==null){
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        $productOrderDetails=ProductOrderDetails::where('id',$id)->with('orderMaster')->where('product_order_master_id',$productOrder->id)->first();
        // return $request;
        // return $productOrder;
        if($productOrderDetails==null){
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        if($productOrderDetails->order_status=='C'){
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        if($productOrderDetails->is_cancel_responce=='A'){
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        if($productOrderDetails->is_cancel_responce=='R'){
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        if($productOrder->payment_status=='P'){
            $payment =Payment::where('token_no',$productOrder->token_no)->first();
            $paymentDetails = PaymentDetails::where('payment_master_id',$payment->id)->where('product_order_details_id',$productOrderDetails->id)->first();
            $numberOfProductPurches=PaymentDetails::where('payment_master_id',$payment->id)->where('is_refund','N')->count();
            $totalWalletRefund=PaymentDetails::where('payment_master_id',$payment->id)->sum('refund_amount_wallet');
            $walletReturn = (($payment->wallet- $totalWalletRefund)/$numberOfProductPurches);
            $cancelationcharge=10;
            if(@$payment->payment_type=='S' && $paymentDetails->is_refund=='N'){

                try{
                    $thisAmount=$productOrderDetails->amount-$walletReturn;
                    $amount=(int)($thisAmount-(($cancelationcharge*$thisAmount)/100));
                    Stripe::setApiKey(config('services.stripe.secret'));
                    $refund = Refund::create([
                        'payment_intent' => $payment->pg_payment_id,
                        'amount' => (int) bcmul( $amount, 100),
                    ]);
                    if ($refund->status == 'succeeded') {
                        $upd=[];
                        $upd['is_refund']='Y';
                        $upd['pg_refund_id']=@$refund->id;
                        $upd['refund_amount']=$amount;
                        $upd['refund_amount_wallet']=$walletReturn;
                        $upd['pg_refund_response']=json_encode($refund);
                        PaymentDetails::where('payment_master_id',$payment->id)->where('product_order_details_id',$productOrderDetails->id)->update($upd);
                        $upd1=[];
                        $upd1['order_status']='C';
                        $upd1['is_cancel_responce']='A';
                        if(@$request->all()){
                            $upd1['cancelled_on']=date('Y-m-d');
                            $upd1['cancellation_reason']=@$request->reason;
                            $upd1['order_cancelled_by']='P';
                            $upd1['is_cancel_responce']='N';
                        }
                        // $upd1['cancellation_reason']=@$request->reason;
                        ProductOrderDetails::where('id', $productOrderDetails->id)->update($upd1);

                        $userdata = User::where('id', $productOrder->user_id)->first();
                        $userdata1 = User::where('id', @$productOrderDetails->professional_id)->first();
                        User::where('id',$productOrder->user_id)->update(['wallet_balance'=>($userdata->wallet_balance+$walletReturn)]);
                        $professionalmail = $userdata1->email;
                        $usermail = $userdata->email;
                        $bookingdata = $productOrderDetails;

                        Mail::send(new UserProductOrderCancel($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                        Mail::send(new ProfessionalProductOrderCancel($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                        // whatsHere
                        // for cancellation
                        // $upd1['order_cancelled_by']
                        // else user has cancelled
                        if(@$upd1['order_cancelled_by'] == 'P'){
                            if(@$userdata->mobile && @$userdata->country_code){

                                /*$msg = "Dear " . (@$userdata->nick_name ?? @$userdata->name) .
                                ", your order for product: " . (@$bookingdata->product->title) . " has been cancelled by the author " .
                                (@$userdata1->nick_name ?? @$userdata1->name) .
                                ". Order Token: " . (@$bookingdata->orderMaster->token_no); */

                                $msg = "Olá " . (@$userdata->nick_name ?? @$userdata->name) . ",
                                vi aqui que seu pedido do produto: " . (@$bookingdata->product->title) .
                                " foi cancelado pelo autor " . (@$userdata1->nick_name ?? @$userdata1->name) .
                                ". Mas não tem problema, você ainda pode aproveitar outras ofertas no site da Netbhe.
                                Fico esperando sua visita! Até a próxima! 😉 Token de pedido:" . (@$bookingdata->orderMaster->token_no);

                                $mobile = "+".@$userdata->country_code . @$userdata->mobile;
                                // $mobile = "+554598290176";
                                // $mobile = "+918017096564";
                                // dd($msg);
                                $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                                // if($result['statusCode'] == 200){
                                //     dd("Success");
                                // } else {
                                //     dd("Error");
                                // }
                            }
                        } else {
                            if(@$userdata1->mobile && @$userdata1->country_code){

                                /* $msg = "Dear " . (@$userdata1->nick_name ?? @$userdata1->name) .
                                ", order for your product: " . (@$bookingdata->product->title) . " has been cancelled by " .
                                (@$userdata->nick_name ?? @$userdata->name) .
                                ". Order Token: " . (@$productOrder->token_no); */

                                $msg = "Olá " . (@$userdata1->nick_name ?? @$userdata1->name) . ",
                                vi aqui que seu pedido do produto: " . (@$bookingdata->product->title) .
                                " foi cancelado por " . (@$userdata->nick_name ?? @$userdata->name) .
                                ". Mas não tem problema, você ainda pode aproveitar outras ofertas no site da Netbhe.
                                Fico esperando sua visita! Até a próxima! 😉 Token de pedido:" . (@$productOrder->token_no);

                                $mobile = "+".@$userdata1->country_code . @$userdata1->mobile;
                                // $mobile = "+554598290176";
                                // $mobile = "+918017096564";
                                // dd($msg);
                                $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                                // if($result['statusCode'] == 200){
                                //     dd("Success");
                                // } else {
                                //     dd("Error");
                                // }
                            }
                        }
                        session()->flash('success', \Lang::get('site.product_order_cancel_success'));
                        return redirect()->back();
                    }else{
                        session()->flash('error', \Lang::get('client_site.unauthorize_access'));
                        return redirect()->back();
                    }
                }
                 catch (\Stripe\Exception\RateLimitException $e) {
                    session()->flash('error', \Lang::get('client_site.unauthorize_access'));
                    return redirect()->back();
                } catch (\Stripe\Exception\InvalidRequestException $e) {
                    session()->flash('error', \Lang::get('client_site.unauthorize_access'));
                    return redirect()->back();
                } catch (\Stripe\Exception\AuthenticationException $e) {
                    session()->flash('error', \Lang::get('client_site.unauthorize_access'));
                    return redirect()->back();
                } catch (\Stripe\Exception\ApiConnectionException $e) {
                    session()->flash('error', \Lang::get('client_site.unauthorize_access'));
                    return redirect()->back();
                } catch (\Stripe\Exception\ApiErrorException $e) {
                    session()->flash('error', \Lang::get('client_site.unauthorize_access'));
                    return redirect()->back();
                } catch (Exception $e) {
                    session()->flash('error', \Lang::get('client_site.unauthorize_access'));
                    return redirect()->back();
                }
            }
            if(@$payment->payment_type=='P' && $paymentDetails->is_refund=='N'){
                // dd("hello");
                $thisAmount=$productOrderDetails->amount-$walletReturn;
                $amount=(int)($thisAmount-(($cancelationcharge*$thisAmount)/100));
                $payments = PaymentPaypal::get($payment->pg_payment_id, $this->_api_context);
                $payments->getTransactions();
                $obj = $payments->toJSON();//I wanted to look into the object
                $paypal_obj = json_decode($obj);//I wanted to look into the object
                $transaction_id = $paypal_obj->transactions[0]->related_resources[0]->sale->id;
                $saleId=$transaction_id;
                // dd($paypal_obj);
                $amt = new Amount();
                $amt->setCurrency('USD')
                ->setTotal($amount);
                $refundRequest = new RefundRequest();
                $refundRequest->setAmount($amt);
                $sale = new Sale();
                $sale->setId($saleId);
                try {
                    $refundedSale = $sale->refundSale($refundRequest, $this->_api_context);

                } catch (Exception $ex) {
                    exit(1);
                }
                // dd($refundedSale);
                if( $refundedSale!=null){
                    if ($refundedSale->getState() == 'completed') {
                        $res_arr = $refundedSale->toArray();
                        $res = json_decode($refundedSale);
                        $upd=[];
                        $upd['is_refund']='Y';
                        $upd['pg_refund_id']=$res_arr['id'];
                        $upd['refund_amount']=$amount;
                        $upd['refund_amount_wallet']=$walletReturn;
                        $upd['pg_refund_response']=json_encode($res);
                        PaymentDetails::where('payment_master_id',$payment->id)->where('product_order_details_id',$productOrderDetails->id)->update($upd);
                        $upd1=[];
                        $upd1['order_status']='C';
                        $upd1['is_cancel_responce']='A';
                        if(@$request->all()){
                            $upd1['cancelled_on']=date('Y-m-d');
                            $upd1['cancellation_reason']=@$request->reason;
                            $upd1['order_cancelled_by']='P';
                            $upd1['is_cancel_responce']='N';
                        }
                        // $upd1['cancellation_reason']=@$request->reason;
                        ProductOrderDetails::where('id', $productOrderDetails->id)->update($upd1);
                        $userdata = User::where('id', $productOrder->user_id)->first();
                        $userdata1 = User::where('id', @$productOrderDetails->professional_id)->first();
                        User::where('id',$productOrder->user_id)->update(['wallet_balance'=>($userdata->wallet_balance+$walletReturn)]);
                        $professionalmail = $userdata1->email;
                        $usermail = $userdata->email;
                        $bookingdata = $productOrderDetails;

                        Mail::send(new UserProductOrderCancel($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                        Mail::send(new ProfessionalProductOrderCancel($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                        // whatsHere
                        // for cancellation
                        // $upd1['order_cancelled_by']
                        // else user has cancelled
                        if(@$upd1['order_cancelled_by'] == 'P'){
                            if(@$userdata->mobile && @$userdata->country_code){

                                /*$msg = "Dear " . (@$userdata->nick_name ?? @$userdata->name) .
                                ", your order for product: " . (@$bookingdata->product->title) . " has been cancelled by the author " .
                                (@$userdata1->nick_name ?? @$userdata1->name) .
                                ". Order Token: " . (@$bookingdata->orderMaster->token_no);
                                // $mobile = "+".Auth::user()->country_code . Auth::user()->mobile; */

                                $msg = "Olá " . (@$userdata->nick_name ?? @$userdata->name) . ",
                                vi aqui que seu pedido do produto: " . (@$bookingdata->product->title) .
                                " foi cancelado pelo autor " . (@$userdata1->nick_name ?? @$userdata1->name) .
                                ". Mas não tem problema, você ainda pode aproveitar outras ofertas no site da Netbhe.
                                Fico esperando sua visita! Até a próxima! 😉 Token de pedido:" . (@$bookingdata->orderMaster->token_no);

                                $mobile = "+".@$userdata->country_code . @$userdata->mobile;
                                // $mobile = "+554598290176";
                                // $mobile = "+918017096564";
                                // dd($msg);
                                $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                                // if($result['statusCode'] == 200){
                                //     dd("Success");
                                // } else {
                                //     dd("Error");
                                // }
                            }
                        } else {
                            if(@$userdata1->mobile && @$userdata1->country_code){

                                /* $msg = "Dear " . (@$userdata1->nick_name ?? @$userdata1->name) .
                                ", order for your product: " . (@$bookingdata->product->title) . " has been cancelled by " .
                                (@$userdata->nick_name ?? @$userdata->name) .
                                ". Order Token: " . (@$userBooking->token_no); */

                                $msg = "Olá " . (@$userdata1->nick_name ?? @$userdata1->name) . ",
                                vi aqui que seu pedido do produto: " . (@$bookingdata->product->title) .
                                " foi cancelado por " . (@$userdata->nick_name ?? @$userdata->name) .
                                ". Mas não tem problema, você ainda pode aproveitar outras ofertas no site da Netbhe.
                                Fico esperando sua visita! Até a próxima! 😉 Token de pedido:" . (@$userBooking->token_no);

                                $mobile = "+".@$userdata1->country_code . @$userdata1->mobile;
                                // $mobile = "+554598290176";
                                // $mobile = "+918017096564";
                                // dd($msg);
                                $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                                // if($result['statusCode'] == 200){
                                //     dd("Success");
                                // } else {
                                //     dd("Error");
                                // }
                            }
                        }
                        session()->flash('success', \Lang::get('site.product_order_cancel_success'));
                        return redirect()->back();
                    }
                }
            }

            if(@$payment->payment_type=='BA' && $paymentDetails->is_refund=='N'){
                $thisAmount=$productOrderDetails->amount-$walletReturn;
                $amount=(int)($thisAmount);
                $upd=[];
                $upd['is_refund']='Y';
                $upd['refund_amount']=$amount;
                $upd['refund_amount_wallet']=$walletReturn;
                PaymentDetails::where('payment_master_id',$payment->id)->where('product_order_details_id',$productOrderDetails->id)->update($upd);
                $upd1=[];
                $upd1['order_status']='C';
                $upd1['is_cancel_responce']='A';
                if(@$request->all()){
                    $upd1['cancelled_on']=date('Y-m-d');
                    $upd1['cancellation_reason']=@$request->reason;
                    $upd1['order_cancelled_by']='P';
                    $upd1['is_cancel_responce']='N';
                }
                $userdata=User::where('id',$productOrder->user_id)->first();
                User::where('id',$productOrder->user_id)->update(['wallet_balance'=>($userdata->wallet_balance+$amount+$walletReturn)]);
                ProductOrderDetails::where('id', $productOrderDetails->id)->update($upd1);
                $userdata = User::where('id', $productOrder->user_id)->first();
                $userdata1 = User::where('id', @$productOrderDetails->professional_id)->first();
                $professionalmail = $userdata1->email;
                $usermail = $userdata->email;
                $bookingdata = $productOrderDetails;
                Mail::send(new UserProductOrderCancel($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                Mail::send(new ProfessionalProductOrderCancel($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                // whatsHere
                // for cancellation
                // $upd1['order_cancelled_by']
                // else user has cancelled
                if(@$upd1['order_cancelled_by'] == 'P'){
                    if(@$userdata->mobile && @$userdata->country_code){

                        /* $msg = "Dear " . (@$userdata->nick_name ?? @$userdata->name) .
                        ", your order for product: " . (@$bookingdata->product->title) . " has been cancelled by the author " .
                        (@$userdata1->nick_name ?? @$userdata1->name) .
                        ". Order Token: " . (@$bookingdata->orderMaster->token_no); */

                        $msg = "Olá " . (@$userdata->nick_name ?? @$userdata->name) . ",
                        vi aqui que seu pedido do produto: " . (@$bookingdata->product->title) .
                        " foi cancelado pelo autor " . (@$userdata1->nick_name ?? @$userdata1->name) .
                        ". Mas não tem problema, você ainda pode aproveitar outras ofertas no site da Netbhe.
                        Fico esperando sua visita! Até a próxima! 😉 Token de pedido:" . (@$bookingdata->orderMaster->token_no);

                        $mobile = "+".@$userdata->country_code . @$userdata->mobile;
                        // $mobile = "+554598290176";
                        // $mobile = "+918017096564";
                        // dd($msg);
                        $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                        // if($result['statusCode'] == 200){
                        //     dd("Success");
                        // } else {
                        //     dd("Error");
                        // }
                    }
                } else {
                    if(@$userdata1->mobile && @$userdata1->country_code){

                        /* $msg = "Dear " . (@$userdata1->nick_name ?? @$userdata1->name) .
                        ", order for your product: " . (@$bookingdata->product->title) . " has been cancelled by " .
                        (@$userdata->nick_name ?? @$userdata->name) .
                        ". Order Token: " . (@$userBooking->token_no); */

                        $msg = "Olá " . (@$userdata1->nick_name ?? @$userdata1->name) . ",
                        vi aqui que seu pedido do produto: " . (@$bookingdata->product->title) .
                        " foi cancelado por " . (@$userdata->nick_name ?? @$userdata->name) .
                        ". Mas não tem problema, você ainda pode aproveitar outras ofertas no site da Netbhe.
                        Fico esperando sua visita! Até a próxima! 😉 Token de pedido:" . (@$userBooking->token_no);

                        $mobile = "+".@$userdata1->country_code . @$userdata1->mobile;
                        // $mobile = "+554598290176";
                        // $mobile = "+918017096564";
                        // dd($msg);
                        $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                        // if($result['statusCode'] == 200){
                        //     dd("Success");
                        // } else {
                        //     dd("Error");
                        // }
                    }
                }
                session()->flash('success', \Lang::get('site.product_order_cancel_success'));
                return redirect()->back();
            }

            if(@$payment->payment_type=='C' && $paymentDetails->is_refund=='N'){
                $thisAmount=$productOrderDetails->amount-$walletReturn;
                $amount=(int)($thisAmount-(($cancelationcharge*$thisAmount)/100));
                if (env('PAYMENT_ENVIORNMENT') == 'live') {
                    $url = Moip::ENDPOINT_PRODUCTION;
                } else if (env('PAYMENT_ENVIORNMENT') == 'sandbox') {
                    $url = Moip::ENDPOINT_SANDBOX;
                }
                $data = array(
                    'amount' =>  bcmul($amount,100),
                );
                // return $data;
                $crl = curl_init();
                $header = array();
                // $header[] = 'Content-length: 0';
                $header[] = 'Content-type: application/json';
                $header[] = 'Authorization: Bearer ' . $this->adminAccessToken;
                curl_setopt($crl, CURLOPT_HTTPHEADER, $header);
                curl_setopt($crl, CURLOPT_URL, "{$url}/v2/payments/".$payment->pg_payment_id."/refunds");
                curl_setopt($crl, CURLOPT_POST, 1);
                curl_setopt($crl, CURLOPT_POST, true);
                curl_setopt($crl, CURLOPT_POSTFIELDS, json_encode($data));
                curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
                $payment_output = curl_exec($crl);
                curl_close($crl);
                $payment_output = json_decode($payment_output);

                if (@$payment_output->id) {
                    if(@$payment_output->status=='COMPLETED'){

                        $upd=[];
                        $upd['is_refund']='Y';
                        $upd['pg_refund_id']=@$payment_output->id;
                        $upd['refund_amount']=$amount;
                        $upd['refund_amount_wallet']=$walletReturn;
                        $upd['pg_refund_response']=json_encode($payment_output);
                        PaymentDetails::where('payment_master_id',$payment->id)->where('product_order_details_id',$productOrderDetails->id)->update($upd);
                        $upd1=[];
                        $upd1['order_status']='C';
                        $upd1['is_cancel_responce']='A';
                        if(@$request->all()){
                            $upd1['cancelled_on']=date('Y-m-d');
                            $upd1['cancellation_reason']=@$request->reason;
                            $upd1['order_cancelled_by']='P';
                            $upd1['is_cancel_responce']='N';
                        }
                        ProductOrderDetails::where('id', $productOrderDetails->id)->update($upd1);
                        $userdata = User::where('id', $productOrder->user_id)->first();
                        $userdata1 = User::where('id', @$productOrderDetails->professional_id)->first();
                        User::where('id',$productOrder->user_id)->update(['wallet_balance'=>($userdata->wallet_balance+$walletReturn)]);
                        $professionalmail = $userdata1->email;
                        $usermail = $userdata->email;
                        $bookingdata = $productOrderDetails;

                        Mail::send(new UserProductOrderCancel($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                        Mail::send(new ProfessionalProductOrderCancel($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));

                        session()->flash('success', \Lang::get('site.product_order_cancel_success'));
                        return redirect()->back();
                    }
                    session()->flash('error', \Lang::get('client_site.unauthorize_access'));
                    return redirect()->back();
                }
                session()->flash('error', \Lang::get('client_site.unauthorize_access'));
                return redirect()->back();
            }
            if($productOrder->wallet == $productOrder->sub_total ){
                $thisAmount=$productOrderDetails->amount-$walletReturn;
                $amount=(int)($thisAmount);
                $upd=[];
                $upd['is_refund']='Y';
                $upd['refund_amount']=0;
                $upd['refund_amount_wallet']=$productOrderDetails->amount;
                PaymentDetails::where('payment_master_id',$payment->id)->where('product_order_details_id',$productOrderDetails->id)->update($upd);
                $upd1=[];
                $upd1['order_status']='C';
                $upd1['is_cancel_responce']='A';
                if(@$request->all()){
                    $upd1['cancelled_on']=date('Y-m-d');
                    $upd1['cancellation_reason']=@$request->reason;
                    $upd1['order_cancelled_by']='P';
                    $upd1['is_cancel_responce']='N';
                }
                $userdata=User::where('id',$productOrder->user_id)->first();
                User::where('id',$productOrder->user_id)->update(['wallet_balance'=>($userdata->wallet_balance+$productOrderDetails->amount)]);
                ProductOrderDetails::where('id', $productOrderDetails->id)->update($upd1);
                $userdata = User::where('id', $productOrder->user_id)->first();
                $userdata1 = User::where('id', @$productOrderDetails->professional_id)->first();
                $professionalmail = $userdata1->email;
                $usermail = $userdata->email;
                $bookingdata = $productOrderDetails;
                Mail::send(new UserProductOrderCancel($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                Mail::send(new ProfessionalProductOrderCancel($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                // whatsHere
                // for cancellation
                // $upd1['order_cancelled_by']
                // else user has cancelled
                if(@$upd1['order_cancelled_by'] == 'P'){
                    if(@$userdata->mobile && @$userdata->country_code){

                        /* $msg = "Dear " . (@$userdata->nick_name ?? @$userdata->name) .
                        ", your order for product: " . (@$bookingdata->product->title) . " has been cancelled by the author " .
                        (@$userdata1->nick_name ?? @$userdata1->name) .
                        ". Order Token: " . (@$bookingdata->orderMaster->token_no); */

                        $msg = "Olá " . (@$userdata->nick_name ?? @$userdata->name) . ",
                        vi aqui que seu pedido do produto: " . (@$bookingdata->product->title) .
                        " foi cancelado pelo autor " . (@$userdata1->nick_name ?? @$userdata1->name) .
                        ". Mas não tem problema, você ainda pode aproveitar outras ofertas no site da Netbhe.
                        Fico esperando sua visita! Até a próxima! 😉 Token de pedido:" . (@$bookingdata->orderMaster->token_no);

                        $mobile = "+".@$userdata->country_code . @$userdata->mobile;
                        // $mobile = "+554598290176";
                        // $mobile = "+918017096564";
                        // dd($msg);
                        $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                        // if($result['statusCode'] == 200){
                        //     dd("Success");
                        // } else {
                        //     dd("Error");
                        // }
                    }
                } else {
                    if(@$userdata1->mobile && @$userdata1->country_code){

                        /* $msg = "Dear " . (@$userdata1->nick_name ?? @$userdata1->name) .
                        ", order for your product: " . (@$bookingdata->product->title) . " has been cancelled by " .
                        (@$userdata->nick_name ?? @$userdata->name) .
                        ". Order Token: " . (@$userBooking->token_no);
                        // $mobile = "+".Auth::user()->country_code . Auth::user()->mobile; */

                        $msg = "Olá " . (@$userdata1->nick_name ?? @$userdata1->name) . ",
                        vi aqui que seu pedido do produto: " . (@$bookingdata->product->title) .
                        " foi cancelado por " . (@$userdata->nick_name ?? @$userdata->name) .
                        ". Mas não tem problema, você ainda pode aproveitar outras ofertas no site da Netbhe.
                        Fico esperando sua visita! Até a próxima! 😉 Token de pedido:" . (@$userBooking->token_no);

                        $mobile = "+".@$userdata1->country_code . @$userdata1->mobile;
                        // $mobile = "+554598290176";
                        // $mobile = "+918017096564";
                        // dd($msg);
                        $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                        // if($result['statusCode'] == 200){
                        //     dd("Success");
                        // } else {
                        //     dd("Error");
                        // }
                    }
                }
                session()->flash('success', \Lang::get('site.product_order_cancel_success'));
                return redirect()->back();
            }

            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        session()->flash('error', \Lang::get('client_site.unauthorize_access'));
        return redirect()->back();
    }



    /**
     * Method: requestProductCancelReject
     * Description:for cancel product Order reject
     * Author: Soumojit
     * date:05-OCT-2021
     */
    public function requestProductCancelReject(Request $request,$token=null,$id=null){

        $productOrder=ProductOrder::where('token_no',$token)->first();
        if($productOrder==null){
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        $productOrderDetails=ProductOrderDetails::where('id',$id)->with('orderMaster')->where('product_order_master_id',$productOrder->id)->first();
        if($productOrderDetails==null){
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        if($productOrderDetails->order_status=='C'){
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        if($productOrder->payment_status=='P'){
            $upd1=[];
            $upd1['is_cancel_responce']='R';
            ProductOrderDetails::where('id', $productOrderDetails->id)->update($upd1);

            $userdata = User::where('id', $productOrder->user_id)->first();
            $userdata1 = User::where('id', @$productOrderDetails->professional_id)->first();

            $professionalmail = $userdata1->email;
            $usermail = $userdata->email;

            $bookingdata = $productOrderDetails;
            Mail::send(new UserCancelRequestReject($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));

            session()->flash('success', \Lang::get('site.cancelation_request_reject'));
            return redirect()->back();
        }
        session()->flash('error', \Lang::get('client_site.unauthorize_access'));
        return redirect()->back();

    }

    /**
     * For Free Session Payment Create
     * @method: fullPaymentThroughWallet
     */
    private function fullPaymentThroughWallet($token) //herework
    {


        $userBooking = ProductOrder::where('token_no', $token)->first()->load('orderDetails','profDetails', 'productDetails', 'productCategoryDetails');
        $referDiscount = ReferralDiscount::first();
        $checkTeacher = User::where('id', $userBooking->professional_id)->first();

        if ($userBooking->sub_total == 0) {
            $payment = Payment::create([
                'token_no'              =>  @$userBooking->token_no,
                'order_id'              =>  'ORD-' . @$userBooking->token_no,
                'user_id'               =>  @$userBooking->user_id,
                'professional_id'       =>  @$userBooking->professional_id,
                'order_type'            =>  'P',
                'balance_status'        =>  'R',
                'wallet'                =>  @$userBooking->wallet,
                'is_wallet_use'         =>  @$userBooking->is_wallet_use,
            ]);
            if (@$payment) {
                $UpdateBooking = ProductOrder::where([
                    'token_no'        =>  @$userBooking->token_no
                ])->update([
                    'payment_status'  =>  "P",
                    // 'order_status'  =>  "A"
                ]);
                // $referralStatus = User::where('id', @$userBooking->user_id)->first();
                // if (@$referralStatus->is_paid_activity == 'N') {
                //     User::where('id', @$userBooking->user_id)->update(['is_paid_activity' => 'C']);
                //     User::where('id', @$referralStatus->referrer_id)->increment("benefit_count", 1);
                // }
                $userdata = User::where('id', Auth::id())->first();
                $userdata1 = User::where('id', @$userBooking->professional_id)->first();
                $professionalmail = $userdata1->email;
                $usermail = $userdata->email;
                $bookingdata = $userBooking;

                // // for send mail to user
                // Mail::send(new UserBooking($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));

                // // // for send mail to professional
                // Mail::send(new ProfessionalBooking($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                // for send mail to admin for Bank account payment
                Mail::send(new UserBankAccountProductOrder($bookingdata->id, $usermail));

                Mail::send(new BankAccountPaymentProduct($bookingdata->id));
                return true;
            } else {
                $UpdateBooking = Booking::where([
                    'token_no'        =>  @$userBooking->token_no
                ])->update([
                    'payment_status'  =>  "F"
                ]);
                return false;
            }
        } else {
            $total_amount         =  @$userBooking->sub_total;
            $payment = Payment::create([
                'token_no'              =>  @$userBooking->token_no,
                'pg_order_id'           =>  'ORD-' . @$userBooking->token_no,
                'user_id'               =>  @$userBooking->user_id,
                'total_amount'          =>  @$total_amount,
                'order_type'            =>  'P',
                'payment_status'        =>  'P',
                'wallet'                =>  @$userBooking->wallet,
                'is_wallet_use'         =>  @$userBooking->is_wallet_use,
            ]);
            if (@$payment) {
                $html='';
                Cart::where('user_id', Auth::user()->id)->delete();
                foreach (@$userBooking->orderDetails  as $orderDetails) {
                    $add = [];
                    $add['payment_master_id'] = $payment->id;
                    $add['product_order_details_id'] = $orderDetails->id;
                    $add['professional_amount'] = $orderDetails->professional_amount;
                    $add['admin_commission'] = $orderDetails->admin_commission;
                    $add['affiliate_commission'] = $orderDetails->affiliate_commission;
                    $add['affiliate_id'] = $orderDetails->affiliate_id;
                    $add['professional_id'] = $orderDetails->professional_id;
                    $add['type'] = 'P';
                    $add['withdraw_by_wirecard'] = 'N';
                    $add['balance_status'] = 'R';
                    $add['affiliate_withdraw_by_wirecard'] = 'N';
                    $add['affiliate_balance_status'] = 'R';
                    PaymentDetails::create($add);
                    $userdata = User::where('id', Auth::id())->first();
                    $userdata1 = User::where('id', @$orderDetails->professional_id)->first();
                    $professionalmail = $userdata1->email;
                    $usermail = $userdata->email;
                    $bookingdata = $orderDetails;

                    // for send mail to user
                    $profName= @$userdata1->nick_name ? @$userdata1->nick_name : @$userdata1->name;
                    $html=$html.'<div><br /><strong>Product Name:</strong>'. $bookingdata->product->title;
                    $html=$html.'<br /><strong>Product Category:</strong>'. $bookingdata->product->category->category_name;
                    $html=$html.'<br /><strong>purchase Date:</strong>'. toUserTime($userBooking->order_date,'Y-m-d');
                    $html=$html.'<br /><strong>Professional Name:&nbsp;</strong>'.$profName ;
                    $html=$html.'<br /><strong>Amount:&nbsp;</strong>'.$bookingdata->amount.'<br /><br /></div>';
                    // Mail::send(new UserProductOrder($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));

                    // for send mail to professional
                    Mail::send(new ProfessionalProductOrder($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                    // whatsHere
                    // for full amount payment from wallet
                    // to professional
                    if(@$userdata1->mobile && @$userdata1->country_code){

                        /* $msg = "Dear " . (@$userdata1->nick_name ?? @$userdata1->name) .
                        ", your product: " . (@$bookingdata->product->title) . " has been purchased by " .
                        (@$userdata->nick_name ?? @$userdata->name) .
                        ". Order Token: " . (@$userBooking->token_no); */

                        $msg = "Olá ". (@$userdata1->nick_name ?? @$userdata1->name) . ",
                        Obrigado por usar a Netbhe! Nós recebemos seu pedido: "
                        . (@$bookingdata->product->title) . " que foi adquirido por "
                        . (@$userdata->nick_name ?? @$userdata->name) . ". Esperamos que goste de seu produto! 😉
                        Token de pedido:" . (@$userBooking->token_no);

                        $mobile = "+".@$userdata1->country_code . @$userdata1->mobile;
                        // $mobile = "+554598290176";
                        // $mobile = "+918017096564";
                        // dd($msg);
                        $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                        // if($result['statusCode'] == 200){
                        //     dd("Success");
                        // } else {
                        //     dd("Error");
                        // }
                    }
                }
                $userdata = User::where('id', Auth::id())->first();
                $usermail = $userdata->email;
                $product_html=$html;
                // dd($product_html);
                User::where('id', $userdata->id)->update(['wallet_balance'=>($userdata->wallet_balance - @$userBooking->wallet)]);
                Mail::send(new UserProductOrder($usermail, $userdata, $product_html));
                // whatsHere
                // for full amount payment from wallet
                // foreach
                // to user
                $UpdateBooking = ProductOrder::where([
                    'token_no'        =>  @$userBooking->token_no
                ])->update([
                    'payment_status'  =>  "P",
                ]);
                return true;
                $response['success']['message'] = \Lang::get('site.Payment_successfull');
                return response()->json($response);
            } else {
                $UpdateBooking = ProductOrder::where([
                    'token_no'        =>  @$userBooking->token_no
                ])->update([
                    'payment_status'  =>  "F"
                ]);
                return false;
                $response['error']['message'] = \Lang::get('client_site.payment_not_complete');
                return response()->json($response);
            }
        }
    }
}
