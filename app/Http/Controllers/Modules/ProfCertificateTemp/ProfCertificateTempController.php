<?php

namespace App\Http\Controllers\Modules\ProfCertificateTemp;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserToTools;
use App\Models\UserToCertificate;
use App\Models\UserFormAnswered;
use App\Models\FormMaster;
use App\Models\FormCategory;
use App\Models\Booking;
use App\Models\Product;
use App\Models\FormMasterDetails;
use App\Models\ImportedTool;
use App\Models\CertificateTemplateMaster;
use App\Models\ContractTemplatMaster;
use App\Models\ProductToCertificate;
use App\Models\ProductOrderDetails;
use validate;
use Storage;
use App\User;
use Auth;
use Mail;
use App\Mail\FormAnswered;


class ProfCertificateTempController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }


   

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request) { 

        $nowtime = date('Y-m-d H:i:s');
        $tendaybef = date('Y-m-d H:i:s', strtotime("-3 days"));

        // $data['all_paid_users'] = Booking::select('user_id')->distinct()->with('userDetails')->where('professional_id',Auth::id())->where('payment_status','P')->get();
        $data['all_paid_users'] = User::get();
        // $data['all_paid_users'] = Booking::select('user_id')->distinct()->with('userDetails')->where('professional_id',Auth::id())->where('payment_status','P')->whereBetween('created_at', [$tendaybef ,$nowtime])->get();

        //$data['contract_templats'] = ContractTemplatMaster::where('status', '!=', 'DELETED')->orderBy('id','DESC')->with(['user'])->get();
        
        $user_id = Auth::id();
        $admin_id = '1';
        $data['templates'] = CertificateTemplateMaster::where('professional_id', Auth::id())->get();
        // ->orderBy('id','DESC')->get();
        foreach($data['templates'] as $template) {
            $ex = ProductToCertificate::where('tool_id', $template->id)->first();
            if(@$ex) $template->assigned = 'Y';
            else $template->assigned = 'N';
        }
        return view('modules.prof_certificate_temp.index')->with($data);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create(Request $request) {
       
        $data = $parameters = array();
        return view('modules.prof_certificate_temp.create')->with($data);
        //return view('admin.modules.product.create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request) {        
       
        // $request->validate([
        //     'name'              => 'required',
        //     'cert_head'         => 'required',
        //     'template_number'   => 'required',
        //     'cert_head'         => 'required',
        //     // 'above_stu_name'    => 'required',
        //     // 'below_stu_name'    => 'required',
        //     // 'above_course'      => 'required',
        //     // 'below_course'      => 'required',
        //     'issued_on'         => 'required',
        //     'expires_on'        => 'required',
        //     // 'cert_id'           => 'required',
        //     'primary_color'     => 'required',
        //     'secondary_color'   => 'required',
        //     'font_style'        => 'required'
        // ]);

        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        
        $insert['professional_id']  = Auth::id();
        $insert['name']             = @$request->params['name'];
        $insert['title']            = @$request->params['cert_head'];
        $insert['certificate_id']   = @$request->params['cert_id'];
        $insert['template_number']  = @$request->params['template_number'];
        $insert['issued_on']        = @$request->params['issued_on'];
        $insert['expires_on']       = @$request->params['expires_on'];
        $insert['expires_by']       = @$request->params['expires_by'];
        $insert['primary_color']    = @$request->params['primary_color'];
        $insert['secondary_color']  = @$request->params['secondary_color'];
        $insert['font']             = @$request->params['font_style'];
        $insert['description']      = @$request->params['note'];
        $insert['content']          = @$request->params['content'];
        $insert['status']           = 'A';

        if(@$request->params['template_number'] == 1){
            // $insert['above_stu_name']   = @$request->params['above_stu_name'];
            $insert['below_stu_name']   = @$request->params['below_stu_name'];
            $insert['above_course']     = @$request->params['above_course'];
            // $insert['below_course']     = @$request->params['below_course'];
            $insert['primary_color']    = '#3c3333';
            $insert['secondary_color']  = '#6c6c6c';
        } else if(@$request->params['template_number'] == 2){
            $insert['above_stu_name']   = @$request->params['above_stu_name'];
            // $insert['below_stu_name']   = @$request->params['below_stu_name'];
            $insert['above_course']     = @$request->params['above_course'];
            // $insert['below_course']     = @$request->params['below_course'];
            $insert['primary_color']    = '#1781d2';
            $insert['secondary_color']  = '#6c6c6c';
        } else if(@$request->params['template_number'] == 3){
            $insert['above_stu_name']   = @$request->params['above_stu_name'];
            // $insert['below_stu_name']   = @$request->params['below_stu_name'];
            $insert['above_course']     = @$request->params['above_course'];
            $insert['below_course']     = @$request->params['below_course'];
        } else if(@$request->params['template_number'] == 4){
            $insert['above_stu_name']   = @$request->params['above_stu_name'];
            // $insert['below_stu_name']   = @$request->params['below_stu_name'];
            $insert['above_course']     = @$request->params['above_course'];
            // $insert['below_course']     = @$request->params['below_course'];
        }

        $cr = CertificateTemplateMaster::create($insert);
        if(@$cr){
            $jsn['request'] = $request['params'];
            $jsn['request_1'] = $request;
            $jsn['template'] = $cr;
            $jsn['status'] = "success";
            $jsn['message'] = \Lang::get('client_site.cert_temp_saved_successfully');
        } else {
            $jsn['status'] = "error";
            $jsn['message'] = \Lang::get('client_site.something_went_be_wrong');
        }
        return response()->json($jsn);

    }

    /*
    * Method: updateSignature
    * Description: to update signature of certificate template
    * Author: Sayantani
    * Date: 04-Oct-2021
    */
    public function updateSignature(Request $request, $id) {
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        $cert = CertificateTemplateMaster::where('id',$id)->first();
        if(@$cert && @$request->content){
            $cert->content = @$request->content;
            $cert->save();
            $jsn['request'] = $request->all();
            $jsn['template'] = $cert;
            $jsn['status'] = "success";
        } else {
            $jsn['status'] = "error";
        }
        return response()->json($jsn);
    }

    /*
    * Method: previewTemplate
    * Description: to show template to professional
    * Author: Sayantani
    * Date: 28-July-2021
    */
    public function previewTemplate($ord_id){
        $order = ProductOrderDetails::with('orderMaster','product')->where('id',$ord_id)->first();
        $assigned_certificate = ProductToCertificate::where('product_id',$order->product->id)->first();
        $certificate_template = $assigned_certificate->getTool;
        $certificatedesc = $assigned_certificate->getTool->content;
        $prof_data = User::where('id',Auth::user()->id)->first();
        $user_data = User::where('id',$order->orderMaster->user_id)->first();
        $course = $order->product;
        $img = ""; 
        if(@$prof_data['signature'])
            $img = "<img src='" . url('storage/app/public/uploads/signature/' . @$prof_data['signature']) . "' height='50px'/>";
        else
            $img = @$prof_data['name'];

        $certificatedesc = str_replace('{{Nome do aluno}}', @$user_data['name'], $certificatedesc); 
        $certificatedesc = str_replace('{{Nome do curso}}', @$course->title, $certificatedesc);
        $certificatedesc = str_replace('{{Assinatura}}', $img, $certificatedesc);

        return view('modules.products.preview_certificate_template', [
            'template'          => $certificate_template,
            'certificatedesc'   => $certificatedesc,
            'order'             => $order
        ]);
    }

    public function viewProductCertificateTemplate($id, $pid=null) {
        
        $data['certificate'] = CertificateTemplateMaster::with(['user','getUsertoolsData'])->find($id);
        if($data['certificate']){
            if(@$pid){
                $course = Product::where('id', $pid)->first();
                if(@$course){
                    $img = "";
                    if(@$course->professional->signature)
                        $img = "<img src='" . url('storage/app/public/uploads/signature/' .  @$course->professional->signature) . "' height='50px'/>";
                    else
                        $img = @$course->professional->name;
                    
                    $data['certdata'] = [];
                    $data['certdata']['NAME']               = "<b>Student Name</b>";
                    $data['certdata']['COURSE']             = @$course->title;
                    $data['certdata']['DURATION']           = @$course->course_duration;
                    $data['certdata']['PROF']               = @$course->professional->name;
                    $data['certdata']['SIGN']               = $img;
                } else {
                    return redirect()->back()->with('error', \Lang::get('client_site.product_not_found'));
                }
            }
            return view('modules.prof_certificate_temp.view_certificate_temp')->with($data);
        } else {
            return redirect()->back()->with('error', \Lang::get('client_site.something_went_be_wrong'));
        }

    }

    /*
    * Method: show
    * Description: to show template to student
    * Author: Sayantani
    * Date: 28-July-2021
    */
    public function show($ord_id) {
        //
    }



    /**
    * Show the form for editing a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function edit($id) {
        $data = array();
        $data['template'] = CertificateTemplateMaster::where('id', $id)->first();
        if(!@$data['template']){
            session()->flash('error', \Lang::get('client_site.cert_temp_not_found'));
            return redirect()->back();
        }
        return view('modules.prof_certificate_temp.edit')->with($data);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id) {
        $cert = CertificateTemplateMaster::where('id',$id)->first();
        // $request->validate([
        //     'name'              => 'required',
        //     'cert_head'         => 'required',
        //     'template_number'   => 'required',
        //     'cert_head'         => 'required',
        //     // 'above_stu_name'    => 'required',
        //     // 'below_stu_name'    => 'required',
        //     // 'above_course'      => 'required',
        //     // 'below_course'      => 'required',
        //     'issued_on'         => 'required',
        //     'expires_on'        => 'required',
        //     // 'cert_id'           => 'required',
        //     'primary_color'     => 'required',
        //     'secondary_color'   => 'required',
        //     'font_style'        => 'required'
        // ]);

        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        
        $insert['professional_id']  = Auth::id();
        $insert['name']             = @$request->name;
        $insert['title']            = @$request->cert_head;
        $insert['certificate_id']   = @$request->cert_id;
        $insert['issued_on']        = @$request->issued_on;
        $insert['expires_on']       = @$request->expires_on;
        $insert['expires_by']       = @$request->expires_by;
        $insert['primary_color']    = @$request->primary_color;
        $insert['secondary_color']  = @$request->secondary_color;
        $insert['font']             = @$request->font_style;
        $insert['description']      = @$request->note;
        $insert['content']          = @$request->content;
        $insert['status']           = 'A';

        if(@$request->hasFile('background')){
            $image_name = time().'-'.rand(1000,9999).'.'.@$request->background->getClientOriginalExtension();
            $image = @$request->background;
            Storage::putFileAs('public/uploads/certificate_template', $image, $image_name);
            $insert['background_filename'] = $image_name;
        }

        if(@$request->template_number == 1){
            // $insert['above_stu_name']   = @$request->above_stu_name;
            $insert['below_stu_name']   = @$request->below_stu_name;
            $insert['above_course']     = @$request->above_course;
            // $insert['below_course']     = @$request->below_course;
        } else if(@$request->template_number == 2){
            $insert['above_stu_name']   = @$request->above_stu_name;
            // $insert['below_stu_name']   = @$request->below_stu_name;
            $insert['above_course']     = @$request->above_course;
            // $insert['below_course']     = @$request->below_course;
        } else if(@$request->template_number == 3){
            $insert['above_stu_name']   = @$request->above_stu_name;
            // $insert['below_stu_name']   = @$request->below_stu_name;
            $insert['above_course']     = @$request->above_course;
            $insert['below_course']     = @$request->below_course;
        } else if(@$request->template_number == 4){
            $insert['above_stu_name']   = @$request->above_stu_name;
            // $insert['below_stu_name']   = @$request->below_stu_name;
            $insert['above_course']     = @$request->above_course;
            // $insert['below_course']     = @$request->below_course;
        }

        if(@$cert){
            $cert->update($insert);
            $jsn['request'] = $request->all();
            $jsn['template'] = $cert;
            $jsn['status'] = "success";
            $jsn['message'] = \Lang::get('client_site.cert_temp_saved_successfully');
        } else {
            $jsn['status'] = "error";
            $jsn['message'] = \Lang::get('client_site.cert_temp_not_found');
            // session()->flash('error', \Lang::get('client_site.cert_temp_not_found'));
        }
        return response()->json($jsn);
    }

    /*
    * Method: destroyCertTemp
    * Description: to delete certificate template
    * Author: Sayantani
    * Date: 28-July-2021
    */
    public function destroyCertTemp($id) {
        $cert = CertificateTemplateMaster::where('id',$id)->first();
        if(@$cert){
            $prod_to_cert = ProductToCertificate::where('tool_id', $cert->id)->get();
            if(count($prod_to_cert) > 0){
                foreach($prod_to_cert as $p) { $p->delete(); }
            }
            $cert->delete();
            session()->flash('success', \Lang::get('client_site.cert_temp_deleted_successfully'));
            return redirect()->back();
        } else {
            session()->flash('error', \Lang::get('client_site.cert_temp_not_found'));
            return redirect()->back();
        }
    }
    /**
    * Remove the specified resource from storage.
    *
    *  @param  int  $id
    *  @return \Illuminate\Http\Response
    */
    public function destroy($id) {

        $data = UserToTools::where('tool_id', $id)->first();
        if(@$data){
            //echo "not delete";
            session()->flash('error', \Lang::get('client_site.assigned_contract_not_del'));
            return redirect()->back();
        } else {
            //echo "delete";
            ContractTemplatMaster::where('id', $id)->delete();
            session()->flash('success', \Lang::get('client_site.contract_temp_deleted'));
            return redirect()->back();
        }

        

       
     
    }


    // for contract temp assign in user
    public function profCertificateTempUserAssigne(Request $request){

        $data = $request->all();
        
        $nowtime = date('Y-m-d H:i:s');
        // dd($data);
        if($data){
            if($request->user_id){
                foreach ($request->user_id as $val) {
                    
                    $insert = array();
                    $insert['user_id']                  = $val;
                    $insert['professional_id']          = Auth::id();
                    $insert['tool_type']                = "Certificate Template";
                    $insert['tool_id']                  = $request->certificate_template_id;
                    $insert['course']                   = $request->course_name;
                    $insert['course_duration']          = $request->course_duration;
                    $insert['signature']                = $request->signature;
                    $insert['assign_date']              = $nowtime;
                    $insert['status']                   = "ACTIVE";
                    
                    
                    UserToCertificate::create($insert);
                }

                session()->flash('success', \Lang::get('client_site.user_assigned_successfully'));
                return redirect()->back();    
               
                // send mail to profession start
                    //Mail::send(new FormAnswered($usermail,$professionalmail));
                // send mail to profession end
            } else {   
                
                session()->flash('error', \Lang::get('site.Select_User'));
                return redirect()->back();
                //return redirect()->route('user.view.form');
            }      
        }
    }



//for professional contract 14.3.20 start

   public function profCertificateTempUserAssigneList($id){
        

        $data['all_user_data'] = UserToCertificate::with(['getprofessionalUserData','getCertificateTemplatesdata'])->where(['professional_id' => Auth::id(),'tool_id' => $id,'tool_type' => 'Certificate Template'])->get();
        
        // pr1($data['all_user_data']->toArray());
        // die();
       
        return view('modules.prof_certificate_temp.professional_certificate_view')->with($data);

   }



//for professional contract 14.3.20 end


    /*for professional ImportingToolsView section start*/

    // public function professionalImportingToolsView(Request $request){
    //     //pr1($request->form_id);
    //     //die();

    //     $formdata = UserToTools::with('getprofessionalUserData')->where(['professional_id' => Auth::id(),'tool_id' => $request->import_tools_id,'tool_type' => 'Imported Tools'])->get();
    //     //pr1($formdata->toArray());       
    //     //die();

    //     return view('modules.importing_tools.professional_form_view')->with([
    //         'formdata'   => @$formdata
    //     ]);
    // }

    /*for professional ImportingToolsView section end*/

    public function updateStatus($id){

        $nowtime = date('Y-m-d H:i:s');

        UserToTools::where('id', $id)->update(['status' => 'COMPLETED','completion_date' => $nowtime]);
                
        session()->flash('success', \Lang::get('client_site.status_updated_successfully'));
        return redirect()->route('user.view.form'); 

    }


// for user contract start 14.3.20

    public function userContractTempView($id){
        

        //$data['details'] = UserToTools::with(['getUserData','getContractTemplatesdata','getImportingToolsdata.category'])->find($id);
        $details = UserToTools::with(['getUserData','getContractTemplatesdata','getImportingToolsdata.category'])->find($id);
        

        $profdetails =User::where('id',$details->professional_id)->first();
        $userdetails = User::where('id',@$details->user_id)->first();

        $month = date('m (M)',strtotime($details->contract_end_date));
        $day = date('d (D)',strtotime($details->contract_end_date));
        $year = date('y',strtotime($details->contract_end_date));


        $profdata = [];
        $profdata['NAME']               = $userdetails['name'];

        $profdata['CPF']                = @$userdetails['cpf_no'];
        $profdata['PUBLIC PLACE']       = @$userdetails['street_name'];
        $profdata['NUMERO']             = @$userdetails['street_number'];
        $profdata['CITY']               = @$userdetails['city'];
        $profdata['CEP']                = @$userdetails['crp_no'];
        $profdata['NAME_COACH']         = @$profdetails['name'];        
        $profdata['CITY_COACH']         = @$profdetails['city'];        
        $profdata['PRICE']              = @$profdetails['rate_price'];

        $profdata['MONTH']              = @$month;
        $profdata['DAY']                = @$day;
        $profdata['YEAR']               = @$year;

        $profdata['INSTALLMENT']        = @$details['contact_temp_installment'];
        $profdata['NEIGHBORHOOD']       = @$details['contact_temp_neighborhood'];
        $profdata['ESTADO_CIVIL']       = @$details['contact_temp_marital_status'];
        
        //pr1($profdata);
        //die();

        //return view('modules.prof_contract_temp.user_contract_show')->with($data);

         return view('modules.prof_contract_temp.user_contract_show', [
            'details'       => $details,
            'profdata'       => $profdata,
        ]);

    }


    public function userContractTempAcceptStatus($id){
        $nowtime = date('Y-m-d H:i:s');

        UserToTools::where('id', $id)->update(['status' => 'COMPLETED','completion_date' => $nowtime]);
                
        session()->flash('success', \Lang::get('client_site.accept_contract_succesfully'));
        return redirect()->route('user.view.form'); 
    }

// for user contract end 14.3.20


    public function certifiacteStatusUpdate($id){        

        $data = CertificateTemplateMaster::find($id);        
      
        if(@$data->status == 'A') {
            CertificateTemplateMaster::where('id', $data->id)->update(['status' => 'I']);
            session()->flash('success', \Lang::get('client_site.cert_temp_inactivated'));
            return redirect()->back();
        }

        if(@$data->status == 'I') {
            CertificateTemplateMaster::where('id', $data->id)->update(['status' => 'A']);
            session()->flash('success', \Lang::get('client_site.cert_temp_activated'));
            return redirect()->back();
        }

    }

    public function certificateCopy($id){
        
        $temp = CertificateTemplateMaster::where('id',$id)->where('added_by','A')->first();
        $insert['title'] = $temp['title'];
        $insert['certificate_description'] = $temp['certificate_description'];
        $insert['status']    =   "ACTIVE";
        $insert['added_by'] = 'P';
        $insert['added_by_id']  =   Auth::user()->id;
        $insert['shown_in_proff'] = 'Y';
        $insert['copied_from'] = 'Y';

        $result = CertificateTemplateMaster::create($insert);

        if($result){
            session()->flash('success', \Lang::get('client_site.cert_temp_copied'));
            return redirect()->back();
        }
    }

    /*
    * Method: resetBackgroundImg
    * Description: to reset background of certificate template
    * Author: Sayantani
    * Date: 02-April-2021
    */
    public function resetBackgroundImg($id) {
        $cert_temp = CertificateTemplateMaster::where('id',$id)->first();
        if(@$cert_temp){
            @unlink(storage_path('app/public/uploads/certificate_template/'.@$cert_temp->background_filename));
            $cert_temp->background_filename = null;
            $cert_temp->save();
            $jsn['status'] = "success";
            $jsn['message'] = \Lang::get('client_site.background_reset_succesfully');
        } else {
            $jsn['status'] = "error";
            $jsn['message'] = \Lang::get('client_site.cert_temp_not_found');
        }
        return response()->json($jsn);
    }

}
