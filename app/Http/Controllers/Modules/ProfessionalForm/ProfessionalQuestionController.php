<?php

namespace App\Http\Controllers\Modules\ProfessionalForm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserToTools;
use App\Models\UserFormAnswered;
use App\Models\FormMaster;
use App\Models\FormCategory;
use App\Models\FormMasterDetails;
use validate;
use App\User;
use Auth;
use Mail;
use App\Mail\FormAnswered;

class ProfessionalQuestionController extends Controller
{
    //

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id=null,Request $request)
    {
        $data = array();
        $data['title'] = 'Manage Question';
        $data['questions'] = FormMasterDetails::where('form_master_id', $id)->orderBy('id', 'DESC')->get();
        $data['form_data'] = FormMaster::where('id', $id)->first();
        // dd($data['questions']);
        return view('modules.professional_question.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id=null, Request $request)
    {
        // dd($id);
        $data = array();
        $data['details'] = FormMaster::where('id', $id)->first();

        $data['questions'] = FormMasterDetails::where('form_master_id',$id)->get();
        
        return view('modules.professional_question.create')->with($data);
        //return view('admin.modules.product.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id=null,Request $request){


        $request->validate([
            'formtype'            => 'required',
            'field_name'          => 'required'
        ]);
        $insert['form_master_id']          = $id;
        $insert['field_type']              = $request->formtype;
        $insert['field_name']              = $request->field_name;
        $insert['status']                  = 'ACTIVE';
        if ($request->field_value) {
            $insert['field_values']        =  json_encode($request->field_value);
        }
        $inertdata = FormMasterDetails::create($insert);
        //if ($request->finised) {
        if ($inertdata) {
            $update = array();
            $update['status'] = 'ACTIVE';
            FormMaster::where('id', $id)->update($update);
            session()->flash('success', \Lang::get('client_site.question_saved'));
            return redirect()->back();
            // return redirect(route('admin.form.manage'));
        }
        session()->flash('success', \Lang::get('client_site.question_saved'));
        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($form_id = null, $id=null, Request $request)
    {
        // dd($id);
        $data = array();
        $data['form'] = FormMaster::where('id', $form_id)->first();
        $data['details'] = FormMasterDetails::where('id', $id)->first();
        return view('modules.professional_question.edit')->with($data);
        //return view('admin.modules.product.create');
    }
    /*
    @method         => FormDetailsCreate
    @description    => Form details update in form_master_details table
    @author         => Sanjoy Patra
    @date           => 22/02/2020
    */
    public function update($form_id=null,$id = null, Request $request)
    {
        $request->validate([
            'formtype'            => 'required',
            'field_name'          => 'required'
        ]);
        $insert['field_type']              = $request->formtype;
        $insert['field_name']              = $request->field_name;
        if ($request->field_value) {
            $insert['field_values']        =  json_encode($request->field_value);
        }
        FormMasterDetails::where('id',$id)->update($insert);
        if ($request->finised) {
            $update = array();
            $update['status'] = 'ACTIVE';
            FormMaster::where('id', $form_id)->update($update);
            session()->flash('success', \Lang::get('client_site.question_saved'));
            return redirect(route('form.question.manage', ['form_id' => $form_id]));
        }
        session()->flash('success', \Lang::get('client_site.question_saved'));
        return redirect(route('professional-form.show', [$form_id]));
    }

    /*
    @method         => FormDetailsCreate
    @description    => Form details update in form_master_details table
    @author         => Sanjoy Patra
    @date           => 22/02/2020
    */
    public function delete($form_id = null, $id = null)
    {
        FormMasterDetails::where('id', $id)->delete();
        session()->flash('success', \Lang::get('client_site.question_deleted'));
        return redirect(route('professional-form.show', [$form_id]));
    }
}