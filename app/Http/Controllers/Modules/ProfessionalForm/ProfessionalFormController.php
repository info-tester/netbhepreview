<?php

namespace App\Http\Controllers\Modules\ProfessionalForm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserToTools;
use App\Models\UserFormAnswered;
use App\Models\FormMaster;
use App\Models\FormCategory;
use App\Models\Booking;
use App\Models\FormMasterDetails;
use validate;
use App\User;
use Auth;
use Mail;
use DB;
use App\Mail\FormAnswered;

use App\Models\ImportedToolFeedback;


class ProfessionalFormController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }


   

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request) { 

        $nowtime = date('Y-m-d H:i:s');
        $tendaybef =date('Y-m-d H:i:s', strtotime("-3 days"));

        
        $data = array();
        $data['title'] = 'Manage Form';
        //$data['form_tools'] = FormMaster::where('status','!=', 'DELETED')->with(['category','user'])->orderBy('id','DESC')->get();

        $user_id = Auth::id();
        $admin_id = '1';

        // $arr = DB::select(`form_master_id`)
		// ->from(`professional_specialties`)
		// ->join(`form_to_speciality`, function($join) {
		// 	$join->on(`professional_specialties.id`, `=`, `form_to_speciality.professional_specialty_id`);
		// 	})
		// ->where(`professional_specialty_id`, `=`, Auth::user()->professional_specialty_id)
        // ->get()->toArray();

        // $arr = DB::select(`form_master_id`)->from(`form_to_speciality`)->where('professional_specialty_id', Auth::user()->professional_specialty_id);
        // dd($arr);

        $data['form_tools'] = FormMaster::where('status', '!=', 'DELETED')->where('shown_in_proff', 'Y')->where(function($q) use ($user_id, $admin_id) {
            $q = $q->where('status', '!=', 'DELETED')
                    ->where(['added_by' => 'P','added_by_id' => $user_id ])
                    ->orWhere(function($qq) use ($user_id, $admin_id) {
                        $qq = $qq->where(['added_by' => 'A','added_by_id' => $admin_id ])
                                ->whereHas('specialities', function( $query ) use ( $user_id ){
                                    $query = $query->where('professional_specialty_id', Auth::user()->professional_specialty_id);
                                });
                    });
            })
            ->orderBy('id','DESC')->with(['category','user'])
            ->get();
        // dd($data['form_tools']);

        //echo "sdfdsfd";
        //pr($data['blogs']->toArray());
        //die();
        
        //$users = User::query()->distinct()->get();


        $data['all_paid_users'] = Booking::select('user_id')->distinct()->with('userDetails')->where('professional_id',Auth::id())->where('payment_status','P')->whereBetween('created_at', [$tendaybef ,$nowtime])->get();
       
        //pr1($data['all_paid_users']->toArray());
        //die();

        return view('modules.professional_form.index')->with($data);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create(Request $request) {
       
        $data = array();
        //$data['title'] = 'Add Form';
        //$data['button'] = 'Add';
        //$data['category'] = FormCategory::where('cat_type', 'FORM')->get();
        $data['category'] = FormCategory::get();
        return view('modules.professional_form.create')->with($data);
        //return view('admin.modules.product.create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request) {        
       
        $request->validate([
            'title'            => 'required',
            'cat_id'           => 'required',
            'desc'             => 'required'
        ]);
        $insert['form_title']                = $request->title;
        $insert['form_cat_id']               = $request->cat_id;
        $insert['form_dsc']                  = nl2br($request->desc);
        $insert['help_tools_view']           = nl2br($request->help_tools_view);
        //$insert['status']                    = 'Form';
        $insert['status']                    = 'INCOMPLETE';
        $insert['added_by']                  = 'P';
        $insert['added_by_id']               = Auth::id();
        $last = FormMaster::create($insert);
        session()->flash('success', \Lang::get('client_site.form_saved'));

        // return redirect()->back();
        return redirect(route('form.question.create',['form_id' => $last->id]));

    }


    //product Details Show

    public function show($id) {

        $details = FormMaster::with(['category','user'])->find($id);

        $questions = FormMasterDetails::where('form_master_id',$id)->get();
       

        //pr1($data->toArray());
        //die();
        return view('modules.professional_form.show', [
            'details'       => $details,
            'questions'  => $questions
            //'productImages' => $productImages,

        ]);

    }



    /**
    * Show the form for editing a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function edit($id) {

        //pr1($id);
        //die();
        $data = array();
        //$data['title'] = 'Edit Form';
        //$data['button'] = 'Edit';

        //$data['category'] = FormCategory::where('cat_type', 'FORM')->get();
        $data['category'] = FormCategory::get();
        $data['details'] = FormMaster::where('id', $id)->first();
        //pr($data['details']->toArray());
        //die();
        return view('modules.professional_form.edit')->with($data);

        // return view('admin.modules.product.edit', [
        //     'product' => $product,
           
        // ]);

    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id) {
        
        //pr($id);
        //die();
        $request->validate([
            'title'            => 'required',
            'cat_id'           => 'required',
            'desc'             => 'required'
        ]);
        $insert['form_title']                = $request->title;
        $insert['form_cat_id']               = $request->cat_id;
        $insert['form_dsc']                  = nl2br($request->desc);
        $insert['help_tools_view']           = nl2br($request->help_tools_view);

        FormMaster::where('id',$id)->update($insert);
        session()->flash('success', \Lang::get('client_site.form_saved'));
        return redirect()->back();

    }

    /**
    * Remove the specified resource from storage.
    *
    *  @param  int  $id
    *  @return \Illuminate\Http\Response
    */
    public function destroy($id) {

        $data = UserToTools::where('tool_id', $id)->first();
        if(@$data){
            //echo "not delete";
            session()->flash('error', \Lang::get('client_site.assigned_form_not_del'));
            return redirect()->back();
        } else {
            //echo "delete";
            FormMaster::where('id', $id)->delete();
            session()->flash('success', \Lang::get('client_site.form_deleted'));
            return redirect()->back();
        }

        

       
     
    }



    public function professionalUserAssigne(Request $request){

        $data = $request->all();
        $nowtime = date('Y-m-d H:i:s');

        $formdata = FormMaster::where('id',$request->form_id)->first();
        //pr1($data);
        //pr1($formdata->tool_type);
        //die();



        if($data){
            if($request->user_id){
                foreach ($request->user_id as $val) {
                    
                    $insert = array();
                    $insert['user_id']          = $val;
                    $insert['professional_id']  = Auth::id();
                    //$insert['tool_type']        = $formdata->tool_type;
                    $insert['tool_type']        = "FORM";
                    $insert['tool_id']          = $request->form_id;
                    $insert['assign_date']      = $nowtime;
                    $insert['status']           = "INVITED";
                    
                    UserToTools::create($insert);
                }

                session()->flash('success', \Lang::get('client_site.user_assigned_successfully'));
                return redirect()->back();    
               
                // send mail to profession start
                    //Mail::send(new FormAnswered($usermail,$professionalmail));
                // send mail to profession end
            } else {   
                
                session()->flash('error', \Lang::get('site.Select_User'));
                return redirect()->back();
                //return redirect()->route('user.view.form');
            }      
        }
    }



    // 360degree evaluation start for test 27.2.20

    public function get360degreeEvaluation(){

        return view('modules.360degree_evaluation.index');

    }

    // 360degree evaluation end for test


    //14.3.20 for professional start feedback

    public function viewAllUserFeedback($id){

       $professional_id = Auth::id();
       $data['allfeedback'] = ImportedToolFeedback::where(['imported_tool_id' => $id, 'professional_id' => $professional_id ,'tool_type' => "Form"])->with(['getUserData'])->get();
       //pr1($data['allfeedback']->toarray());
       //pr1($professional_id);
       //die();
       return view('modules.professional_form.alluser_feedback')->with($data);
    }


    public function postFeedbackTouser(Request $request){

        //pr1($request->all());
        //die();

        $nowtime = date('Y-m-d H:i:s');
        $id = $request->imported_tool_feedback_id;

        $request->validate([            
            'professional_feedback'      => 'required'
        ]);      

        if($request->professional_feedback){
            ImportedToolFeedback::where('id', $id)->update([
                'status'                            => 'COMPLETED',
                'professional_feedback_date'        => $nowtime,
                'professional_feedback'             => $request->professional_feedback
            ]);

            session()->flash('success', \Lang::get('client_site.feedback_added_successfully'));
            return redirect()->back();
        } else {
            session()->flash('error', \Lang::get('client_site.enter_your_feedback'));
            return redirect()->back();
        }      

    }


    //14.3.20 for professional end feedback



    //14.3.20 for User start feedback

    public function userFormToolsFeedback(Request $request){

        //pr1($request->all());
        //die();
        $nowtime = date('Y-m-d H:i:s');

        $request->validate([            
            'user_feedback'             => 'required'
        ]);

        $user_tools = UserToTools::where('id',$request->user_to_tools_id)->first();
        
        if($request->user_feedback){
            $insert['user_to_tools_id']         = $request->user_to_tools_id;
            $insert['imported_tool_id']         = $user_tools->tool_id;
            $insert['professional_id']          = $user_tools->professional_id;
            $insert['status']                   = 'INCOMPLETE';
            $insert['tool_type']                = 'Form';
            $insert['user_id']                  = $user_tools->user_id;
            $insert['user_feedback']            = $request->user_feedback;
            $insert['user_feedback_date']       = $nowtime;
            ImportedToolFeedback::create($insert);

            session()->flash('success', \Lang::get('client_site.feedback_added_successfully'));
            return redirect()->back();
        } else {
            session()->flash('error', \Lang::get('client_site.enter_your_feedback'));
            return redirect()->back();
        }      

    }


    //14.3.20 for User end feedback

/*Form tools status update for professional 17.3.20*/

    public function formStatusUpdate($id){

        $data = FormMaster::find($id);        
      
        if(@$data->show_status == 'A') {
            FormMaster::where('id', $data->id)->update(['show_status' => 'I']);
            session()->flash('success', \Lang::get('client_site.form_inactivated'));
            return redirect()->back();
        }

        if(@$data->show_status == 'I') {
            FormMaster::where('id', $data->id)->update(['show_status' => 'A']);
            session()->flash('success', \Lang::get('client_site.form_activated'));
            return redirect()->back();
        }


    }


}
