<?php

namespace App\Http\Controllers\Modules\Professional;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use Auth;
use validate;
use App\User;
use App\Models\ProfessionalToQualification;
use App\Models\ProfessionalToExperience;
use App\Models\UserAvailability;
use App\Models\UserToLanguage;
use App\Models\UserToCard;
use App\Models\Language;
use App\Models\Timezone;
use App\Models\Booking;
use App\Models\Payment;
use Soumen\Agent\Agent;
use Soumen\Agnet\Services\Device;
use Soumen\Agent\Services\Platform;
use App\Models\Country;
use App\Models\State;
use App\Models\Bank;
use App\Models\Category;
use App\Models\CategoryLandingPageMaster;
use App\Models\City;
use App\Models\Content;
use App\Models\ProfessionalSpecialty;
use App\Models\UserToBankAccount;
use App\Models\UserToCategory;
use App\Models\CallDuration;
use App\Models\Experience;
use App\Models\PaymentDetails;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Moip\Moip;
use Moip\Auth\BasicAuth;
use Moip\Auth\OAuth;
use DateTime;
use DatePeriod;
use DateInterval;


class ProfessionalCategoryLandingController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     *method: index
     *created By: Sayantani
     *description: For showing category landing
     */
    public function index($slug = null)
    {
        $data = [];
        if(@$slug){
            $data['category'] = Category::where('slug', $slug)->first();
            $data['detail'] = CategoryLandingPageMaster::where('category_id', $data['category']->id)->first();
            if(!@$data['detail']){
                session()->flash('error', \Lang::get('client_site.not_found'));
                return redirect()->route('home');
            }
        }
        return view('modules.professional.category_landing')->with($data);
    }
}