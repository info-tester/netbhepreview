<?php

namespace App\Http\Controllers\Modules\Professional;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use Auth;
use validate;
use App\User;
use App\Models\ProfessionalToQualification;
use App\Models\ProfessionalToExperience;
use App\Models\UserAvailability;
use App\Models\UserToLanguage;
use App\Models\UserToCard;
use App\Models\Language;
use App\Models\Timezone;
use App\Models\Booking;
use App\Models\Payment;
use App\Models\Product;
use Soumen\Agent\Agent;
use Soumen\Agnet\Services\Device;
use Soumen\Agent\Services\Platform;
use App\Models\Country;
use App\Models\State;
use App\Models\Bank;
use App\Models\Category;
use App\Models\City;
use App\Models\Content;
use App\Models\ProfessionalSpecialty;
use App\Models\UserToBankAccount;
use App\Models\UserToCategory;
use App\Models\CallDuration;
use App\Models\Experience;
use App\Models\PaymentDetails;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Moip\Moip;
use Moip\Auth\BasicAuth;
use Moip\Auth\OAuth;
use DateTime;
use DatePeriod;
use DateInterval;


class ProfessionalProfileController extends Controller
{
    protected $CPF_CNPJ, $numeral, $tipo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *method: index
     *created By: Abhisek
     *description: For Professional Edit-profile
     */

    public function index(Request $request)
    {
        //pr($request->all());
        $country = Country::orderBy('name')->get();
        $state = State::orderBy('name')->get();
        $call_durations = CallDuration::get();

        if (Auth::user()->is_professional == "Y") {
            if ($request->all()) {
                if ($request->old_password) {

                    $request->validate([
                        'new_password'      =>  'required',
                        'old_password'      =>  'required',
                        'confirm_password'  =>  'required'
                    ]);
                    if (\Hash::check($request->old_password, Auth::user()->password)) {
                        $data = [
                            'password'  =>  \Hash::make($request->new_password)
                        ];
                        $user = User::where('id', Auth::id())->update($data);
                        session()->flash('success', \Lang::get('client_site.password_successfully_updated'));
                        return redirect()->back();
                    } else {
                        session()->flash('error', \Lang::get('client_site.wrong_password'));
                        return redirect()->back();
                    }
                } else {
                    if (Auth::user()->status == 'A') {
                        $request->validate([
                            'first_name'    =>  "required",
                            'last_name'     =>  "required",
                            'nick_name'     =>  "nullable|unique:users,nick_name,",
                            'phone_no'      => 'required|unique:users,mobile,' . Auth::id(),
                            'gender'        =>  "required",
                            'time_zone'     =>  "required",
                            'description'   =>  "required",
                            'rate'          =>  "required",
                            'rate_price'    =>  "required",
                            'language'      =>  "required",
                            'city'          =>  "required",
                            'professional_specialty_id' => "required",
                            'crp_no'        =>  "required|unique:users,mobile," . Auth::id(),
                            'gender'        =>  "required",
                            'description'   =>  "required",
                            'time_zone'     =>  "required",
                            'country'       =>  "required",
                            'state'         =>  "required",
                            'dob'           =>  "required",
                            'street_number' =>  "required",
                            'street_name'   =>  "required",
                            'complement'    =>  "required",
                            'district'      =>  "required",
                            'zipcode'       =>  "required",
                            'area_code'     =>  "nullable",
                            // 'cpf_no'        =>  "required_if:country,=,30|unique:users,cpf_no," . Auth::id(),
                            'reschedule_time'        =>  "required"
                        ]);
                        if(@$request->country==30){
                            $request->validate([
                                'cpf_no'        =>  "required_if:country,=,30|unique:users,cpf_no," . Auth::id(),
                            ]);
                        }else{
                            $request->cpf_no==null;
                        }

                    } else {
                        $request->validate([
                            'first_name'    =>  "nullable",
                            'last_name'     =>  "nullable",
                            'nick_name'     =>  "nullable|unique:users,nick_name,",
                            'phone_no'      => 'nullable|unique:users,mobile,' . Auth::id(),
                            'gender'        =>  "nullable",
                            'time_zone'     =>  "nullable",
                            'description'   =>  "nullable",
                            'rate'          =>  "nullable",
                            'rate_price'    =>  "nullable",
                            'language'      =>  "nullable",
                            'city'          =>  "nullable",
                            'professional_specialty_id' => "nullable",
                            'crp_no'        =>  "nullable|unique:users,mobile," . Auth::id(),
                            'gender'        =>  "nullable",
                            'description'   =>  "nullable",
                            'time_zone'     =>  "nullable",
                            'country'       =>  "nullable",
                            'state'         =>  "nullable",
                            'dob'           =>  "nullable",
                            'street_number' =>  "nullable",
                            'street_name'   =>  "nullable",
                            'complement'    =>  "nullable",
                            'district'      =>  "nullable",
                            'zipcode'       =>  "nullable",
                            'area_code'     =>  "nullable",
                            // 'cpf_no'        =>  "nullable|unique:users,cpf_no," . Auth::id()
                        ]);
                        if(@$request->country==30){
                            $request->validate([
                                'cpf_no'        =>  "nullable|unique:users,cpf_no," . Auth::id(),
                            ]);
                        }else{
                            $request->cpf_no==null;
                        }
                    }

                    if (@$request->cpf_no) {
                        $this->CPF_CNPJ = $request->cpf_no;
                        if (!$this->getNumeral()) {
                            session()->flash('error', \Lang::get('client_site.invalid_cpf_number'));
                            return redirect()->back()->withInput($request->input());
                        }
                    }
                    $data = [
                        'name'          =>  $request->first_name . ' ' . $request->last_name,
                        'mobile'        =>  $request->phone_no,
                        'gender'        =>  $request->gender,
                        'timezone_id'   =>  $request->time_zone,
                        'description'   =>  $request->description,
                        'rate'          =>  $request->rate,
                        'rate_price'    =>  $request->rate_price,
                        'professional_specialty_id' => $request->professional_specialty_id,
                        'crp_no'        =>  $request->crp_no,
                        'country_id'    =>  $request->country,
                        'state_id'      =>  $request->state,
                        'city'          =>  $request->city,
                        'dob'           =>  @$request->dob ? date('Y-m-d', strtotime($request->dob)) : NULL,
                        'street_number' =>  $request->street_number,
                        'street_name'   =>  $request->street_name,
                        'complement'    =>  $request->complement,
                        'district'      =>  $request->district,
                        'area_code'     =>  @$request->area_code,
                        'zipcode'       =>  $request->zipcode,
                        'cpf_no'        =>  @$request->cpf_no,
                        'sell'          =>  $request->sell,
                        'profile_active' =>  $request->profile_active,
                        'reschedule_time' =>  @$request->reschedule_time,
                        'no_of_installment' =>  @$request->no_of_installment,
                        'installment_charge' =>  @$request->installment_charge
                    ];

                    if(@$request->country){
                        $cntry = Country::where('id',@$request->country)->first();
                        $data['country_code'] = $cntry->phonecode;
                    }
                    $user = User::where('id', Auth::id())->first();
                    $user->update($data);

                    if(@$request->sell == 'VS'){
                        $products = Product::where('professional_id', @$user->id)->where('status', 'A')->get();
                        foreach($products as $prod){
                            $prod->status ='I';
                            $prod->save();
                        }
                    }

                    if(@$request->call_duration)
                        $user->get_call_durations()->sync($request->call_duration);
                    else{
                        $cdr = CallDuration::where("call_duration", "60")->first()->id;
                        $user->get_call_durations()->sync([$cdr]);
                    }

                    if (auth()->user()->timezone_id != 0) {
                        $userTimezoneDB = Timezone::where('timezone_id', auth()->user()->timezone_id)->first();
                        session(['timezone' => $userTimezoneDB->timezone ?? env('TIMEZONE')]);
                    }

                    if ($request->language) {
                        $this->userToLanguage($request->language);
                    }

                    $ext = [
                        0   =>  'png',
                        1   =>  'jpeg',
                        2   =>  'jpg',
                        3   =>  'gif',
                        4   =>  'png'
                    ];
                    if (@$request->nick_name) {
                        $nik = ucwords(strtolower(@$request->nick_name));
                        User::where('id', Auth::id())->update([
                            'nick_name'   =>  $nik,
                            'slug'   =>   str_slug(@$request->nick_name)
                        ]);
                    }
                    if (@$request->profile_img) {
                        $name = time() . uniqid() . '.png';
                        $path = "storage/app/public/uploads/profile_pic/";
                        $image = Image::make(file_get_contents($request->profile_img));
                        $image->save($path . $name);

                        $update['profile_pic'] = $name;
                        if (@Auth::user()->profile_pic) {
                            if (file_exists($path . '/' . @Auth::user()->profile_pic)) {
                                unlink('storage/app/public/uploads/profile_pic/' . @Auth::user()->profile_pic);
                            }
                        }
                        User::where('id', Auth::id())->update([
                            'profile_pic'   =>  @$name
                        ]);
                    }
                    // if (@$request->call_duration && count($request->call_duration)>0) {
                    //     $call_dur = null;
                    //     if(in_array('30', $request->call_duration) && in_array('60', $request->call_duration)){
                    //         $call_dur = "BOTH";
                    //     } else if(in_array('30', $request->call_duration)){
                    //         $call_dur = "30";
                    //     } else if(in_array('60', $request->call_duration)){
                    //         $call_dur = "60";
                    //     }
                    //     User::where('id', Auth::id())->update([
                    //         'call_duration'   =>  @$call_dur
                    //     ]);
                    // }
                    if (@$request->signature2) {
                        $filename = time() . '-' . rand(1000, 9999) . '.png';
                        $path = "storage/app/public/uploads/signature/";
                        $image = Image::make(@$request->signature2);
                        $image->save($path . $filename);
                        // @unlink(storage_path('app/public/uploads/signature/' . auth()->user()->signature));
                        User::where('id', Auth::id())->update([
                            'signature'   =>  @$filename
                        ]);
                    }
                    if (@$request->signature) {
                        $request->validate([
                            'signature'    =>  'image|mimes:jpeg,png,jpg',
                        ]);
                        $signature_pic = $request->signature;
                        $filename = time() . '-' . rand(1000, 9999) . '.' . $signature_pic->getClientOriginalExtension();
                        Storage::putFileAs('public/uploads/signature', $signature_pic, $filename);
                        @unlink(storage_path('app/public/uploads/signature/' . auth()->user()->signature));
                        User::where('id', Auth::id())->update([
                            'signature'   =>  @$filename
                        ]);
                    }
                    session()->flash('success', \Lang::get('client_site.profile_successfully_updated'));
                    return redirect()->back();
                }
            }

            $time = Timezone::get();
            $lang = Language::orderBy('name')->get();
            $lang_id = UserToLanguage::where('user_id', Auth::id())->pluck('language_id')->toArray();
            /*$country = Country::orderBy('name')->get();
            $state = State::orderBy('name')->get();*/
            $bank = UserToBankAccount::where('user_id', Auth::id())->first();
            $bankList = Bank::orderBy('bank_name')->get();
            $specialties = ProfessionalSpecialty::orderBy('name', 'ASC')->select('id', 'name', 'entry_info_message')->get();

            return view('modules.professional.professional_profile')->with([
                'time'      =>  @$time,
                'lang'      =>  @$lang,
                'lang_id'   =>  @$lang_id,
                'country'   =>  @$country,
                'state'     =>  @$state,
                'bank'      =>  @$bank,
                'bankList'  =>  @$bankList,
                'specialties'  =>  @$specialties,
                'call_durations'  =>  @$call_durations,
            ]);
        } else {
            if (Auth::user()->is_professional == "N") {
                $time = Timezone::get();
                $lang = Language::orderBy('name')->get();
                $lang_id = UserToLanguage::where('user_id', Auth::id())->pluck('language_id')->toArray();
                return view('modules.user.user_edit_profile')->with([
                    'time'      =>  @$time,
                    'lang'      =>  @$lang,
                    'lang_id'   =>  @$lang_id,
                    'country'   =>  @$country,
                    'state'     =>  @$state,
                ]);
            }
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
    }

    /**
     *method: userToLanguage
     *created By: Abhisek
     *description: For loading Professional Dashboard
     */

    public function userToLanguage($lang = [])
    {

        UserToLanguage::where('user_id', Auth::id())->delete();
        for ($i = 0; $i < sizeof($lang); $i++) {
            UserToLanguage::create([
                'user_id'       =>  Auth::id(),
                'language_id'   =>  $lang[$i]
            ]);
        }
        return 1;
    }




    /**
     *method: load_professional_dashboard
     *created By: Abhisek
     *description: For loading Professional Dashboard
     */

    public function load_professional_dashboard(Request $request, $type = null)
    {
        $key = [];
        $notSupported = 0;
        if (Auth::user()->is_professional != "Y") {
            if (Auth::user()->is_professional == "N") {
                session()->flash('error', \Lang::get('client_site.unauthorize_access'));
                return view('modules.user.user_dashboard');
            }
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        } else {
            if ($request->all() || $type != null) {

                $booking = Booking::where('id', '!=', 0);
                if (@$type) {
                    $url = \Request::segment(2);

                    if ($url == 'BR') {

                        session()->flash('error', \Lang::get('client_site.unauthorize_access'));
                        return redirect()->back();
                        $booking = Booking::with('userDetails', 'profDetails', 'parentCatDetails', 'getReview')
                            ->where([
                                'professional_id' => Auth::id()
                            ]);
                        $key['type'] = 'BR';
                    }
                    if ($url == 'UC') {

                        $plat = Platform::getDetails('name');
                        if ($plat->name == "Windows 8.1") {
                            $notSupported = 1;
                        }
                        $booking = Booking::with('userDetails', 'profDetails', 'parentCatDetails', 'getReview')
                            ->where([
                                'professional_id' => Auth::id(),
                            ])
                            ->whereIn('order_status',['A','C']);
                        $key['type'] = 'UC';
                    }
                    if ($url == 'PC') {
                        //dd(Auth::id());
                        $booking = Booking::with('userDetails', 'profDetails', 'parentCatDetails', 'getReview')
                            ->where([
                                'professional_id' => Auth::id(),
                                'video_status'  => 'C'
                            ]);
                        $key['type'] = 'PC';
                    }
                }
                if ($request->all()) {
                    if ($request->type == 'BR') {
                        session()->flash('error', \Lang::get('client_site.unauthorize_access'));
                        return redirect()->back();
                        $booking = Booking::with('userDetails', 'profDetails', 'parentCatDetails', 'getReview')
                            ->where([
                                'professional_id' => Auth::id(),
                            ]);
                    }
                    if ($request->type == 'UC') {
                        $booking = Booking::with('userDetails', 'profDetails', 'parentCatDetails', 'getReview')
                            ->where([
                                'professional_id' => Auth::id(),
                            ])
                            ->whereIn('order_status',['A','C']);
                    }
                    if ($request->type == 'PC') {
                        $booking = Booking::with('userDetails', 'profDetails', 'parentCatDetails', 'getReview')
                            ->where([
                                'professional_id' => Auth::id(),
                                'order_status'  => 'P'
                            ]);
                    }
                    if ($request->from_date || $request->to_date) {
                        // dd($request->from_date);
                        $from = $request->from_date ? $request->from_date : date('d-m-Y');
                        $to = $request->to_date ? $request->to_date : date('d-m-Y');
                        $booking = $booking->whereBetween('date', array($from, $to));
                    }
                    if ($request->status) {
                        $booking = $booking->where('order_status', $request->status);
                    }
                    @$key = $request->all();
                }
                $booking = $booking->orderBy('id', 'desc')->get();
            } else {
                $plat = Platform::getDetails('name');
                if ($plat->name == "Windows 8.1") {
                    $notSupported = 1;
                }

                $booking = Booking::with('userDetails', 'profDetails', 'parentCatDetails', 'getReview', 'getMsgMaster')
                    ->where([
                        'professional_id' => Auth::id(),
                        'order_status'  => 'A'
                    ])
                    ->get();
            }
            $netEarning = PaymentDetails::where('professional_id', Auth::id())->sum('professional_amount');
            $paid = PaymentDetails::where([
                'professional_id' => Auth::id(),
                'balance_status' => 'W'
            ])
                ->sum('professional_amount');
            $due = PaymentDetails::where([
                'professional_id' => Auth::id(),
                'balance_status' => 'R'
            ])
                ->sum('professional_amount');
            $classes = Booking::where('professional_id', Auth::id())->count();
            $dashBoardPage = Content::where('id', 13)->first();
            return view('modules.professional.professional_dashboard')->with([
                'booking'               =>  @$booking,
                'key'                   =>  @$key,
                'notSupported'          =>  @$notSupported,
                'netEarning'            =>  @$netEarning,
                'paid'                  =>  @$paid,
                'due'                   =>  @$due,
                'classes'               =>  @$classes,
                'dashBoardPage'         =>  @$dashBoardPage,
            ]);
        }
    }

    /**
     *method: professional_qualification
     *created By: Abhisek
     *description: For Professional qualification
     */

    public function professional_qualification(Request $request, $id = null)
    {

        if (Auth::user()->is_professional != "Y") {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        } else {
            if ($request->all()) {
                $request->validate([
                    'degree'        =>  'required',
                    'university'    =>  'required',
                    // 'from_month'    =>  'required',
                    // 'to_month'      =>  'required',
                    // 'from_year'     =>  'required',
                    'file'              =>  'required',
                    //'curriculum'        =>  'required'
                    // 'curriculum_file'   =>  'required'
                ], [
                    'file.required' =>  \Lang::get('client_site.please_upload_your')
                    // 'curriculum_file.required' =>  'Please upload your curriculum soft copy.'

                ]);

                $exists = ProfessionalToQualification::where('user_id', Auth::id())->where('from_year', $request->from_year)->where('from_month', $request->from_month)->first();
                if(@$exists){
                    session()->flash('error', \Lang::get('site.this_date_already_exists'));
                    return redirect()->back();
                }

                if ($request->from_year && $request->to_year || $request->from_year && $request->pursuing) {
                    if ($request->pursuing != "on") {
                        if ($request->to_year < $request->from_year) {
                            session()->flash('error', \Lang::get('client_site.to_year_can_not'));
                            return redirect()->back();
                        }
                    }
                    $data = [
                        // 'curriculum'        =>  $request->curriculum,
                        // 'curriculum_file'   =>  $request->curriculum_file,
                        'user_id'           =>  Auth::id(),
                        'degree'            =>  $request->degree,
                        'university'        =>  $request->university,
                        'from_month'        =>  $request->from_month,
                        // 'to_month'          =>  !$request->to_month ? date('m'): $request->to_month,
                        'from_year'         =>  $request->from_year,
                        // 'to_year'           =>  !$request->to_year ? date('Y'): $request->to_year,
                        // 'pursuing'          =>  $request->pursuing=="on" ? "Y": "N"
                    ];
                } else {
                    $data = [
                        'user_id'           =>  Auth::id(),
                        'degree'            =>  $request->degree,
                        'university'        =>  $request->university,
                        'from_year'         =>  @$request->from_year,
                        // 'curriculum'        =>  $request->curriculum
                        // 'curriculum_file'   =>  $request->curriculum_file
                    ];
                }
                $is_created = ProfessionalToQualification::create($data);

                if (@$is_created->id && @$request->file) {
                    $name = $is_created->id . '-' . time() . str_random(4) . '.' . $request->file->getClientOriginalExtension();
                    $path = "storage/app/public/uploads/user_qualifiaction";
                    $request->file->move($path, $name);

                    ProfessionalToQualification::where('id', $is_created->id)->update([
                        'attachment'    =>  $name
                    ]);
                }

                // if(@$is_created->id && @$request->curriculum_file){
                //     $name = $is_created->id.'-'.time().str_random(4).'.'.$request->curriculum_file->getClientOriginalExtension();
                //     $path = "storage/app/public/uploads/user_qualifiaction";
                //     $request->curriculum_file->move($path,$name);

                //     ProfessionalToQualification::where('id', $is_created->id)->update([
                //             'curriculum_file'    =>  $name
                //         ]);
                // }

                if ($is_created) {
                    session()->flash('success', \Lang::get('client_site.qualification_details'));
                    return redirect()->back();
                } else {
                    session()->flash('error', \Lang::get('client_site.unauthorize_access'));
                    return redirect()->back();
                }
            }
            // for loading view...
            $quali = ProfessionalToQualification::where('user_id', Auth::id())->get();

            return view('modules.professional.professional_qualification')->with([
                'quali' =>  @$quali
            ]);
        }
    }

    /**
     *method: update_professional_qualification
     *created By: Abhisek
     *description: For Updating Professional qualification
     */

    public function update_professional_qualification($id, Request $request)
    {
        if (Auth::user()->is_professional != "Y") {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }

        $quali = ProfessionalToQualification::where([
            'id'            =>  $id,
            'user_id'       =>  Auth::id()
        ])->first();
        if ($quali == null) {
            session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
            return redirect()->back();
        } else {
            if ($request->all()) {
                $request->validate([
                    'degree'        =>  'required',
                    'university'    =>  'required'
                    // 'from_month'    =>  'required',
                    // 'to_month'      =>  'required',
                    // 'from_year'     =>  'required'
                    // 'to_year'       =>  'required'
                ]);
                // if ($request->from_year && $request->to_year || $request->from_year && $request->pursuing) {
                if ($request->from_year && $request->from_month) {
                    // if ($request->pursuing != "on") {
                    //     if ($request->to_year < $request->from_year) {
                    //         session()->flash('error', \Lang::get('client_site.to_year_can_not'));
                    //         return redirect()->back();
                    //     }
                    // }
                    $data = [
                        'user_id'       =>  Auth::id(),
                        'degree'        =>  $request->degree,
                        'university'    =>  $request->university,
                        // 'curriculum'    =>  $request->curriculum,
                        'from_month'    =>  $request->from_month,
                        // 'to_month'      =>  !$request->to_month ? date('m') : $request->to_month,
                        'from_year'     =>  $request->from_year,
                        // 'to_year'       =>  !$request->to_year ? date('Y') : $request->to_year,
                        // 'pursuing'      =>  $request->pursuing == "on" ? "Y" : "N"
                    ];
                } else {
                    $data = [
                        'user_id'       =>  Auth::id(),
                        'degree'        =>  $request->degree,
                        // 'curriculum'    =>  $request->curriculum,
                        'university'    =>  $request->university,
                        'from_year'         =>  @$request->from_year,
                    ];
                }
                $is_update = ProfessionalToQualification::where('id', $id)->update($data);

                if (@$is_update && @$request->file) {
                    $name = $quali->id . '-' . time() . str_random(4) . '.' . $request->file->getClientOriginalExtension();

                    if (file_exists($quali->attachment)) {
                        unlink('storage/app/public/uploads/user_qualifiaction/' . @$quali->attachment);
                    }

                    $path = "storage/app/public/uploads/user_qualifiaction";
                    $request->file->move($path, $name);

                    ProfessionalToQualification::where('id', $quali->id)->update([
                        'attachment'    =>  $name
                    ]);
                }
                if ($is_update) {
                    session()->flash('success', \Lang::get('client_site.qualification_details_successfully_updated'));
                    return redirect()->route('professional_qualification');
                } else {
                    session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
                    return redirect()->route('professional_qualification');
                }
            }
            // dd($quali);
            $all_quali = ProfessionalToQualification::where('user_id', Auth::id())->get();
            return view('modules.professional.professional_qualification')->with([
                'quali'     =>  @$all_quali,
                'update'    =>  @$quali
            ]);
        }
    }

    /**
     *method: delete_qualification
     *created By: Abhisek
     *description: For Removing Professional qualification
     */

    public function delete_qualification($id)
    {
        if (Auth::user()->is_professional != "Y") {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        $quali = ProfessionalToQualification::where([
            'id'            =>  $id,
            'user_id'       =>  Auth::id()
        ])->first();
        if ($quali == null) {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        } else {
            ProfessionalToQualification::where([
                'id'            =>  $id,
                'user_id'       =>  Auth::id()
            ])->delete();
            session()->flash('success', \Lang::get('client_site.qualification_details_successfully_removed'));
            return redirect()->route('professional_qualification');
        }
    }

    /**
     *method: professional_experience
     *created By: Abhisek
     *description: For Inserting and loading Professional experience
     */

    public function professionalExperience(Request $request)
    {
        if (Auth::user()->is_professional != "Y") {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        if ($request->all()) {
            $request->validate([
                // 'organization'  =>  'required',
                // 'role'          =>  'required',
                'description'   =>  'required'/*,
                'from_month'    =>  'required',*/
                // 'to_month'      =>  'required',
                // 'from_year'     =>  'required'
                // 'to_year'       =>  'required'
            ], [
                'organization.required' => 'O campo de uma organização é obrigatório.',
                'role.required'         => 'O campo de função é obrigatório.',
                'description.required'  => 'O campo de descrição é obrigatório.'
            ]);
            if ($request->from_year && $request->to_year || $request->from_year && $request->pursuing) {
                $from = date('Y-m', strtotime($request->from_year . '-' . $request->from_month));
                if ($request->pursuing == "on") {
                    $to = date('Y-m');
                } else {
                    $to = date('Y-m', strtotime($request->to_year . '-' . $request->to_month));

                    if ($request->to_year < $request->from_year) {
                        session()->flash('error', \Lang::get('client_site.to_year_can_not'));
                        return redirect()->back();
                    }
                }

                $diff = strtotime($from) - strtotime($to);
                $getDay =  (abs(round($diff / 86400)));
                $getDay1 = (int) $getDay;
                $data = [
                    'user_id'           =>  Auth::id(),
                    // 'organization'      =>  $request->organization,
                    // 'role'              =>  $request->role,
                    'description'       =>  $request->description,
                    // 'from_month'        =>  $request->from_month,
                    // 'to_month'          =>  $request->pursuing != "on" ? $request->to_month : date('m'),
                    // 'from_year'         =>  $request->from_year,
                    // 'to_year'           =>  $request->pursuing != "on" ? $request->to_year : date('Y'),
                    // 'pursuing'          =>  $request->pursuing == "on" ? "Y" : "N",
                    'total_experience'  =>  @$getDay1
                ];
            } else {
                $data = [
                    'user_id'           =>  Auth::id(),
                    // 'organization'      =>  $request->organization,
                    // 'role'              =>  $request->role,
                    'description'       =>  $request->description
                ];
            }
            $is_created = ProfessionalToExperience::create($data);
            if ($is_created) {
                session()->flash('success', \Lang::get('client_site.experience_successfully_created'));
                return redirect()->back();
            } else {
                session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
                return redirect()->back();
            }
        }
        $exp = ProfessionalToExperience::where('user_id', Auth::id())->get();

        return view('modules.professional.professional_experience')->with([
            'exp'       =>  @$exp
        ]);
    }

    /**
     *method: edit_professional_experience
     *created By: Abhisek
     *description: For Updating  Professional experience
     */

    public function edit_professional_experience($id, Request $request)
    {
        if (Auth::user()->is_professional != "Y") {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        $exp = ProfessionalToExperience::where([
            'id'            =>  $id,
            'user_id'       =>  Auth::id()
        ])->first();


        if ($exp == null) {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        } else {
            if ($request->all()) {
                $request->validate([
                    'organization'  =>  'required',
                    'role'          =>  'required',
                    'description'   =>  'required'/*,
                    'from_month'    =>  'required',*/
                    // 'to_month'      =>  'required',
                    /*'from_year'     =>  'required'*/
                    // 'to_year'       =>  'required'
                ]);

                if ($request->from_year && $request->to_year || $request->from_year && $request->pursuing) {
                    $from = date('Y-m', strtotime($request->from_year . '-' . $request->from_month));

                    if ($request->pursuing != "on") {
                        if ($request->to_year < $request->from_year) {
                            session()->flash('error', \Lang::get('client_site.to_year_can_not'));
                            return redirect()->back();
                        }
                        $to = date('Y-m', strtotime($request->to_year . '-' . $request->to_month));
                    } else {
                        $to = date('Y-m');
                    }

                    // $diff = date_diff(date_create($from),date_create($to));
                    $diff = strtotime($from) - strtotime($to);
                    $getDay =  (abs(round($diff / 86400)));
                    $getDay1 = (int) $getDay;

                    $data = [
                        'user_id'       =>  Auth::id(),
                        'organization'  =>  $request->organization,
                        'role'          =>  $request->role,
                        'description'   =>  $request->description,
                        'from_month'    =>  $request->from_month,
                        'to_month'      =>  $request->pursuing != "on" ? $request->to_month : date('m'),
                        'from_year'     =>  $request->from_year,
                        'to_year'       =>  $request->pursuing != "on" ? $request->to_year : date('Y'),
                        'pursuing'      =>  $request->pursuing == "on" ? "Y" : "N",
                        'total_experience'  =>  $getDay1
                    ];
                } else {
                    $data = [
                        'user_id'       =>  Auth::id(),
                        'organization'  =>  $request->organization,
                        'role'          =>  $request->role,
                        'description'   =>  $request->description
                    ];
                }
                $is_update = ProfessionalToExperience::where([
                    'id'        =>  $id,
                    'user_id'   =>  Auth::id()
                ])->update($data);
                if ($is_update) {
                    session()->flash('success', \Lang::get('client_site.experience_details_successfully_updated'));
                    return redirect()->route('professional.exp');
                } else {
                    session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
                    return redirect()->route('professional.exp');
                }
            }
        }
        $allExp = ProfessionalToExperience::where([
            // 'id'            =>  $id,
            'user_id'       =>  Auth::id()
        ])->get();
        return view('modules.professional.professional_experience')->with([
            'exp'           =>  @$allExp,
            'update'        =>  @$exp
        ]);
    }

    /**
     *method: delete_experience
     *created By: Abhisek
     *description: For Removing Professional experience
     */

    public function delete_experience($id)
    {
        if (Auth::user()->is_professional != "Y") {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        $quali = ProfessionalToExperience::where([
            'id'            =>  $id,
            'user_id'       =>  Auth::id()
        ])->first();
        if ($quali == null) {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        } else {
            ProfessionalToExperience::where([
                'id'            =>  $id,
                'user_id'       =>  Auth::id()
            ])->delete();
            session()->flash('success', \Lang::get('client_site.experience_details_successfully_removed'));
            return redirect()->route('professional.exp');
        }
    }

    /**
     *method: user_availability
     *created By: Abhisek
     *description: Insert user availability
     */

    public function user_availability(Request $request)
    {
        if (Auth::user()->is_professional != "Y") {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        if ($request->all()) {
            if (!@$request->time_slot) {
                session()->flash('error', \Lang::get('client_site.please_select_at_least_one_time_slot'));
                return redirect()->back();
            }
            if($request->period == 'D'){
                if ($request->date == '' || $request->interval == '') {
                    session()->flash('error', \Lang::get('client_site.invalid_date_and_time_slot'));
                    return redirect()->back();
                }
                if (!$this->checkIfAvlAlreadyInserted($request->time_slot, date('Y-m-d', strtotime($request->date)))) {
                    return redirect()->back()->with('error', Lang::get('site.timeslot_already_used'));
                }
                foreach ($request->time_slot as $value) {
                    $time = explode(' - ', $value);
                    UserAvailability::create([
                        "user_id"   =>  Auth::id(),
                        "date"      =>  toUTCTime(date('Y-m-d', strtotime($request->date)) . ' ' . $time[0] . ':00', 'Y-m-d'),
                        "slot_start" =>  toUTCTime(date('Y-m-d', strtotime($request->date)) . ' ' . $time[0] . ':00'),
                        "slot_end" =>  toUTCTime(date('Y-m-d', strtotime($request->date)) . ' ' . $time[1] . ':00'),
                        "time_interval" => $request->interval
                    ]);
                }

                session()->flash('success', \Lang::get('client_site.time_table_successfully_updated'));
                return redirect()->back();
            } else if($request->period == 'M'){
                if ($request->month == '' || $request->interval == '') {
                    session()->flash('error', \Lang::get('client_site.invalid_date_and_time_slot'));
                    return redirect()->back();
                }
                $parts = explode(" ", $request->month);

                $monthList=[
                    'Janeiro'=>'January',
                    'Fevereiro'=>'February',
                    'Março'=>'March',
                    'Abril'=>'April',
                    'Maio'=>'May',
                    'Junho'=>'June',
                    'Julho'=>'July',
                    'Agosto'=>'August',
                    'Setembro'=>'September',
                    'Outubro'=>'October',
                    'Novembro'=>'November',
                    'Dezembro'=>'December'
                ];
                // dd($monthList[$parts[0]]);
                $request->month=$monthList[$parts[0]].$parts[1];
                // dd($date = date( 'Y-m-d', strtotime($request->month) ));
                $date = date( 'Y-m-d', strtotime($request->month) );
                $first_date = date('Y-m-01', strtotime($date));
                $last_date = date('Y-m-t', strtotime($date));

                $loop_date = strtotime($first_date);
                $last = strtotime($last_date);
                $dates = $array = []; $c = 0;
                while( $loop_date <= $last ) {
                    array_push($dates, date( 'Y-m-d', $loop_date ));
                    $c += 1;
                    // $date = date( 'Y-m-d', strtotime("+".$c." day", strtotime($copy_first_date)) );
                    if ($this->checkIfAvlAlreadyInserted($request->time_slot, date('Y-m-d', $loop_date))) {
                        foreach ($request->time_slot as $value) {
                            $time = explode(' - ', $value);
                            UserAvailability::create([
                                "user_id"   =>  Auth::id(),
                                "date"      =>  toUTCTime(date('Y-m-d', $loop_date) . ' ' . $time[0] . ':00', 'Y-m-d'),
                                "slot_start" =>  toUTCTime(date('Y-m-d', $loop_date) . ' ' . $time[0] . ':00'),
                                "slot_end" =>  toUTCTime(date('Y-m-d', $loop_date) . ' ' . $time[1] . ':00'),
                                "time_interval" => $request->interval
                            ]);
                        }
                    }
                    $loop_date = strtotime( '+1 day', $loop_date );
                }
                session()->flash('success', \Lang::get('client_site.time_table_successfully_updated'));
                return redirect()->back();
            } else if($request->period == 'W'){
                if ($request->week == '' || $request->weeknumber == '' || $request->interval == '') {
                    session()->flash('error', \Lang::get('client_site.invalid_date_and_time_slot'));
                    return redirect()->back();
                }
                $year = explode(',', $request->weeknumber)[0];
                $weeknum = explode(',', $request->weeknumber)[1];
                $timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $weeknum * 7 * 24 * 60 * 60 );
                $timestamp_for_sunday = $timestamp - 86400 * ( date( 'N', $timestamp ) );
                $first_date = date( 'Y-m-d', $timestamp_for_sunday );
                $last_date = date( 'Y-m-d', strtotime("+7 day", $timestamp_for_sunday) );

                $loop_date = strtotime($first_date);
                $last = strtotime($last_date);
                $dates = $array = []; $c = 0;
                while( $loop_date < $last ) {
                    array_push($dates, date( 'Y-m-d', $loop_date ));
                    $c += 1;
                    // $date = date( 'Y-m-d', strtotime("+".$c." day", strtotime($copy_first_date)) );
                    if ($this->checkIfAvlAlreadyInserted($request->time_slot, date('Y-m-d', $loop_date))) {
                        foreach ($request->time_slot as $value) {
                            $time = explode(' - ', $value);
                            UserAvailability::create([
                                "user_id"   =>  Auth::id(),
                                "date"      =>  toUTCTime(date('Y-m-d', $loop_date) . ' ' . $time[0] . ':00', 'Y-m-d'),
                                "slot_start" =>  toUTCTime(date('Y-m-d', $loop_date) . ' ' . $time[0] . ':00'),
                                "slot_end" =>  toUTCTime(date('Y-m-d', $loop_date) . ' ' . $time[1] . ':00'),
                                "time_interval" => $request->interval
                            ]);
                        }
                    }
                    $loop_date = strtotime( '+1 day', $loop_date );
                }
                session()->flash('success', \Lang::get('client_site.time_table_successfully_updated'));
                return redirect()->back();
            }
        }
        $avl = UserAvailability::where('user_id', Auth::id())->orderBy('date', 'DESC')->get();
        return view('modules.professional.professional_availability')->with([
            'avl'           =>  @$avl
        ]);
    }

    public function userAvailabilityAjaxCheck(Request $request)
    {
        $response = [
            'jsonrpc' => 2.0
        ];
        if (Auth::user()->is_professional != "Y") {
            $response['error'] = [
                'message' => Lang::get('client_site.unauthorize_access')
            ];
            return response()->json($response);
        }
        if ($request->all()) {
            if($request->period == 'D'){
                if ($request->date == '' || $request->interval == '') {
                    $response['error'] = [
                        'message' => Lang::get('client_site.invalid_date_and_time_slot'),
                        'request' => $request->all(),
                    ];
                    return response()->json($response);
                }
                if (!@$request->time_slot) {
                    $response['error'] = [
                        'message' => Lang::get('client_site.please_select_at_least_one_time_slot')
                    ];
                    return response()->json($response);
                }
                $curServerTime = strtotime((now(env('TIMEZONE'))->addHour('3')->toDateTimeString()));
                $reqDate = date('Y-m-d', strtotime($request->date));
                $inv = false;
                foreach ($request->time_slot as $sl) {
                    $time = explode(' - ', $sl);
                    $startTime = strtotime(toUTCTime(date('Y-m-d H:i:s', strtotime($reqDate . ' ' . ($time[0] ?? '00:00') . ':00'))));
                    if ($curServerTime >= $startTime) {
                        $inv = true;
                        break;
                    }
                }
                if ($inv) {
                    $response['error'] = [
                        'message' => Lang::get('O horário disponibilizado entre as sessões, deve respeitar o prazo selecionado de intervalo entre uma sessão e outra.')
                    ];
                    return response()->json($response);
                }
                if (!$this->checkIfAvlAlreadyInserted($request->time_slot, $reqDate)) {
                    $response['error'] = [
                        'message' => Lang::get('site.timeslot_already_used')
                    ];
                    return response()->json($response);
                } else {
                    $response['result'] = [
                        'code' => 'AVL',
                        'request' => $request->all()
                    ];
                    return response()->json($response);
                }
            } else if($request->period == 'M'){
                if ($request->month == '' || $request->interval == '') {
                    $response['error'] = [
                        'message' => Lang::get('client_site.invalid_date_and_time_slot'),
                        'request' => $request->all(),
                    ];
                    return response()->json($response);
                }
                if (!@$request->time_slot) {
                    $response['error'] = [
                        'message' => Lang::get('client_site.please_select_at_least_one_time_slot')
                    ];
                    return response()->json($response);
                }
                $parts = explode(" ", $request->month);

                $monthList=[
                    'Janeiro'=>'January',
                    'Fevereiro'=>'February',
                    'Março'=>'March',
                    'Abril'=>'April',
                    'Maio'=>'May',
                    'Junho'=>'June',
                    'Julho'=>'July',
                    'Agosto'=>'August',
                    'Setembro'=>'September',
                    'Outubro'=>'October',
                    'Novembro'=>'November',
                    'Dezembro'=>'December'
                ];
                // dd($monthList[$parts[0]]);
                $request->month=$monthList[$parts[0]].$parts[1];
                // dd($date = date( 'Y-m-d', strtotime($request->month) ));


                $date = date( 'Y-m-d', strtotime($request->month) );
                $first_date = date('Y-m-01', strtotime($date));
                $last_date = date('Y-m-t', strtotime($date));

                $curServerTime = strtotime((now(env('TIMEZONE'))->addHour('3')->toDateTimeString()));
                $reqDate = date('Y-m-d', strtotime($first_date));
                $inv = false;
                // foreach ($request->time_slot as $sl) {
                //     $time = explode(' - ', $sl);
                //     $startTime = strtotime(toUTCTime(date('Y-m-d H:i:s', strtotime($reqDate . ' ' . ($time[0] ?? '00:00') . ':00'))));
                //     if ($curServerTime >= $startTime) {
                //         $inv = true;
                //         break;
                //     }
                // }
                if ($inv) {
                    $response['error'] = [
                        'message' => Lang::get('O horário disponibilizado entre as sessões, deve respeitar o prazo selecionado de intervalo entre uma sessão e outra.'),
                    ];
                    return response()->json($response);
                } else {
                    $response['result'] = [
                        'code' => 'AVL'
                    ];
                    return response()->json($response);
                }
            } else if($request->period == 'W'){
                if ($request->week == '' || $request->weeknumber == '' || $request->interval == '') {
                    $response['error'] = [
                        'message' => Lang::get('client_site.invalid_date_and_time_slot'),
                    ];
                    return response()->json($response);
                }
                if (!@$request->time_slot) {
                    $response['error'] = [
                        'message' => Lang::get('client_site.please_select_at_least_one_time_slot')
                    ];
                    return response()->json($response);
                }
                $year = explode(',', $request->weeknumber)[0];
                $weeknum = explode(',', $request->weeknumber)[1];
                $timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $weeknum * 7 * 24 * 60 * 60 );
                $timestamp_for_sunday = $timestamp - 86400 * ( date( 'N', $timestamp ) );
                $first_date = date( 'Y-m-d', $timestamp_for_sunday );
                $last_date = date( 'Y-m-d', strtotime("+7 day", $timestamp_for_sunday) );

                $curServerTime = strtotime((now(env('TIMEZONE'))->addHour('3')->toDateTimeString()));
                $reqDate = date('Y-m-d', strtotime($first_date));
                $inv = false;
                foreach ($request->time_slot as $sl) {
                    $time = explode(' - ', $sl);
                    $startTime = strtotime(toUTCTime(date('Y-m-d H:i:s', strtotime($reqDate . ' ' . ($time[0] ?? '00:00') . ':00'))));
                    if ($curServerTime >= $startTime) {
                        $inv = true;
                        break;
                    }
                }
                if ($inv) {
                    $response['error'] = [
                        'message' => Lang::get('O horário disponibilizado entre as sessões, deve respeitar o prazo selecionado de intervalo entre uma sessão e outra.'),
                    ];
                    return response()->json($response);
                } else {
                    $response['result'] = [
                        'code' => 'AVL',
                    ];
                    // $response['request'] = $request->all();
                    // $response['first_date'] = $first_date;
                    return response()->json($response);
                }
            }

        }
    }

    /**
     * @param array $slots format [..., "H:i - H:i", ...]
     * @param string $date format "Y-m-d"
     */
    private function checkIfAvlAlreadyInserted($slots, $date)
    {
        $valid = true;
        foreach ($slots as $value) {
            $time = explode(' - ', $value);
            $startTime = toUTCTime(date('Y-m-d H:i:s', strtotime($date . ' ' . ($time[0] ?? '00:00') . ':00')));
            $endTime = toUTCTime(date('Y-m-d H:i:s', strtotime($date . ' ' . ($time[1] ?? '00:00') . ':00')));
            $e = UserAvailability::where('user_id', auth()->id())
                ->where(function ($q) use ($startTime, $endTime) {
                    $q->whereRaw('slot_start < "' . $startTime . ':00" AND slot_end > "' . $startTime . ':00"')
                        ->orWhereRaw('slot_start < "' . $endTime . ':00" AND slot_end > "' . $endTime . ':00"')
                        ->orWhereRaw('slot_start = "' . $startTime . ':00" AND slot_start < "' . $endTime . ':00"')
                        ->orWhereRaw('slot_end = "' . $endTime . ':00" AND slot_end > "' . $startTime . ':00"')
                        ->orWhereRaw('slot_start > "' . $startTime . ':00" AND slot_start < "' . $endTime . ':00"')
                        ->orWhereRaw('slot_end > "' . $startTime . ':00" AND slot_end < "' . $endTime . ':00"');
                })->exists();
            if ($e) {
                $valid = false;
                break;
            }
        }
        return $valid;
    }

    /**
     *   Method      : checkIfAvlByMins
     *   Description : This is used to check avaliability of professional looping in mins
     *   Author      : Sayantani
     *   Date        : 2021-JULY-01
     **/
    private function checkIfAvlByMins($slot, $date){
        $valid = true;
        $time = explode(' - ', $slot);
        $startTime = date('Y-m-d H:i:s', strtotime($date . ' ' . ($time[0] ?? '00:00') . ':00'));
        $endTime = date('Y-m-d H:i:s', strtotime($date . ' ' . ($time[1] ?? '00:00') . ':00'));

        $begin = new DateTime( $startTime );
        $end = new DateTime( $endTime );
        $times = $array = [];
        $interval = DateInterval::createFromDateString( '1 minute' );
        $period = new DatePeriod($begin, $interval, $end);
        $a = 0;

        foreach ( $period as $dt ) {
            $d = $dt->format("H:i");
            $st = date('Y-m-d H:i:s', strtotime($date . ' ' . $d . ':00'));
            $e = UserAvailability::where('user_id', auth()->id())->where('date', $date)->where(function ($q) use ($st, $date) {
                $q->whereRaw('slot_start < "' . $st . ':00" AND slot_end > "' . $st . ':00"');
            })->first();
            array_push($array, $e);
            // array_push($array, $date . ' ' . $d . ':00');
            if ($e) {
                $valid = false;
                // break;
            }
            $a+=1;
            $d = $dt->format("H:i");
            array_push($times, $d);
        }
        // dd($array);
        // $last = strtotime($endTime);
        // $times = $array = []; $c = 0;
        // while( $loop_time <= $last ) {
        //     dd(date('Y-m-d H:i:s', strtotime($date ." ".$loop_time)));
        //     $e = UserAvailability::where('user_id', auth()->id())->where(function ($q) use ($loop_time) {
        //         $q->whereRaw('slot_start < "' . $loop_time . ':00" AND slot_end > "' . $loop_time . ':00"');
        //     })->exists();
        //     $c += 1;
        //     $loop_time = strtotime( '+1 minute', $loop_time );
        //     if ($e) {
        //         $valid = false;
        //         dd("f");
        //         break;
        //     }
        // }
        // return date('Y-m-d H:i:s', strtotime($loop_time));
        return $valid;
    }

    /**
     *method: user_availability_insert
     *created By: Abhisek
     *description: For inserting user availiability using ajax
     */

    public function user_availability_insert(Request $request)
    {
        $respose = [
            'jsonrpc'   =>  '2.0'
        ];
        if (Auth::user()->is_professional != "Y") {
            $response['status'] =   0;
            return response()->json($response);
        }
        if (date('H:i:s', strtotime($request->params['from'])) > date('H:i:s', strtotime($request->params['to']))) {

            $response['status'] = 108;
            return response()->json($response);
        }

        if ($request->params['day'] && $request->params['from'] && $request->params['to']) {

            $chkDplct = UserAvailability::where("day", $request->params['day'])->Where("from_time", $request->params['from'])/*
                ->orWhere("to_time" , $request->params['to'])*/
                ->where('user_id', Auth::id())
                ->first();
            if ($chkDplct != null) {
                $response['status'] = 1024;
                return response()->json($response);
            }

            $chkDplct = UserAvailability::where("day", $request->params['day'])

                ->whereRaw("('" . $request->params['from'] . "' between `from_time` and `to_time`)")
                ->where('user_id', Auth::id())
                ->first();
            if ($chkDplct != null) {
                $response['status'] = 1024;
                return response()->json($response);
            }
            $data = [
                "user_id"   =>  Auth::id(),
                "day"       =>  $request->params['day'],
                "from_time" =>  $request->params['from'],
                "to_time"   =>  $request->params['to']
            ];
            UserAvailability::create($data);
            $all_avl = UserAvailability::where('user_id', Auth::id())->get();
            $response['success'] = 1;
            $response['all'] = $all_avl;
            return response()->json($response);
        }
    }

    /**
     *method: delete_avl
     *created By: Abhisek
     *description: For deleting user availiability
     */

    public function delete_avl($id)
    {
        if (Auth::user()->is_professional != "Y") {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        $del = UserAvailability::where('id', $id)->delete();
        if ($del) {
            session()->flash('success', \Lang::get('client_site.successfully_removed'));
            return redirect()->back();
        } else {
            session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
            return redirect()->back();
        }
    }

    public function deleteAvlAll()
    {
        if (Auth::user()->is_professional != "Y") {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        $del = UserAvailability::where('user_id', auth()->id())->delete();
        if ($del) {
            session()->flash('success', \Lang::get('client_site.successfully_removed'));
            return redirect()->back();
        } else {
            session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
            return redirect()->back();
        }
    }


    /**
     *method: checkMobile
     *created By: Abhisek
     *description: For mobile duplicate checking
     */

    public function checkMobile(Request $request)
    {
        $response = [
            'jsonrpc'   =>  '2.0'
        ];
        if ($request->params['no']) {
            $user = User::where('mobile', $request->params['no'])
                ->where('id', '!=', Auth::id())
                ->first();
            if ($user != null) {
                $response['status'] = 1;
            } else {
                $response['status'] = 0;
            }
        }
        return response()->json($response);
    }


    public function bookingRequest(Request $request, $type = null)
    {
        if (Auth::user()->is_professional != "Y") {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        $key = [];
        $notSupported = 0;
        $booking = Booking::where('id', '!=', 0);
        if (@$type) {
            $url = \Request::segment(2);
            if ($url == 'BR') {

                session()->flash('error', \Lang::get('client_site.unauthorize_access'));
                return redirect()->back();
                $booking = Booking::with('userDetails', 'profDetails', 'parentCatDetails', 'getReview')
                    ->where([
                        'professional_id' => Auth::id()
                    ]);
                $key['type'] = 'BR';
            }
            if ($url == 'UC') {

                $plat = Platform::getDetails('name');
                if ($plat->name == "Windows 8.1") {
                    $notSupported = 1;
                }
                $booking = Booking::with('userDetails', 'profDetails', 'parentCatDetails', 'getReview')
                    ->where([
                        'professional_id' => Auth::id(),
                    ])
                    ->whereIn('order_status',['A','C']);
                $key['type'] = 'UC';
            }
            if ($url == 'PC') {
                //dd(Auth::id());
                $booking = Booking::with('userDetails', 'profDetails', 'parentCatDetails', 'getReview')
                    ->where([
                        'professional_id' => Auth::id(),
                        'video_status'  => 'C'
                    ]);
                $key['type'] = 'PC';
            }
        }
        if ($request->all()) {
            if ($request->type == 'BR') {
                session()->flash('error', \Lang::get('client_site.unauthorize_access'));
                return redirect()->back();
                $booking = Booking::with('userDetails', 'profDetails', 'parentCatDetails', 'getReview')
                    ->where([
                        'professional_id' => Auth::id(),
                    ]);
            }
            if ($request->type == 'UC') {
                $booking = Booking::with('userDetails', 'profDetails', 'parentCatDetails', 'getReview')
                ->where([
                    'professional_id' => Auth::id(),
                ])
                ->whereIn('order_status',['A','C']);
            }
            if ($request->type == 'PC') {
                $booking = Booking::with('userDetails', 'profDetails', 'parentCatDetails', 'getReview')
                    ->where([
                        'professional_id' => Auth::id(),
                        'order_status'  => 'P'
                    ]);
            }
            if ($request->from_date || $request->to_date) {
                // dd($request->from_date);
                $from = $request->from_date ? $request->from_date : date('d-m-Y');
                $to = $request->to_date ? $request->to_date : date('d-m-Y');
                $booking = $booking->whereBetween('date', array($from, $to));
            }
            if ($request->status) {
                $booking = $booking->where('order_status', $request->status);
            }
            @$key = $request->all();
        }
        $booking = $booking->orderBy('id', 'desc')->get();
        return view('modules.professional.professional_booking_request')->with([
            'booking'               =>  @$booking,
            'key'                   =>  @$key,
            'notSupported'          =>  @$notSupported
        ]);
    }

    public function cancelBookingRequest($token)
    {
        if (Auth::user()->is_professional != "Y") {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        $booking = Booking::where('token_no', $token)->first();
        if ($booking == null) {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        } else {
            $booking->order_status = 'R';
            $booking->order_cancelled_by = 'P';
            $booking->save();
            session()->flash('success', \Lang::get('client_site.booking_cancelled_successfully'));
            return redirect()->back();
        }
    }

    public function accseptBookingRequest($token)
    {
        if (Auth::user()->is_professional != "Y") {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        $booking = Booking::where('token_no', $token)->where('order_status', 'AA')->first();
        if ($booking == null) {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        } else {
            $booking->order_status = 'A';
            $booking->save();
            session()->flash('success', \Lang::get('client_site.booking_has_been_accepted_successfully'));
            return redirect()->back();
        }
    }

    // public function rescheduleBookingRequest(Request $request){

    //     $booking = Booking::where('token_no', $request->token_no)->first();
    //     if($booking==null){
    //         session()->flash('error', 'Unauthorize access.');
    //         return redirect()->back();
    //     }
    //     else{
    //         $day = date('l', strtotime($request->date));
    //         $days = [
    //             'Sunday'    =>  1,
    //             'Monday'    =>  2,
    //             'Tuesday'   =>  3,
    //             'Wednesday' =>  4,
    //             'Thursday'  =>  5,
    //             'Friday'    =>  6,
    //             'Saturday'  =>  7
    //         ];
    //         $duration = date('H:i',strtotime($request->time . ' +'.$booking->duration.' minutes'));

    //         $is_avlbl = UserAvailability::where([
    //             'user_id'       =>  @$booking->professional_id,
    //             'day'           =>  @$days[@$day]
    //         ])
    //         ->where('from_time','<=',$request->time)
    //         ->where('to_time', '>=', $duration)
    //         ->first();

    //         if($is_avlbl==null){
    //             session()->flash('error', 'Time slot currently not available for rescheduling.');
    //             return redirect()->back();
    //         }
    //         else{

    //             $booking->reschedule_date       = date('Y-m-d', strtotime($request->date));
    //             $booking->reschedule_time       = $request->time;
    //             $booking->reschedule_status     = 'AA';
    //             $booking->reschedule_by_whom    = 'U';
    //             $booking->save();
    //             session()->flash('success', 'Booking rescheduled successfully, now waiting for expert approval.');
    //             return redirect()->back();
    //         }
    //     }

    // }

    /*
    * Method      : upcommingClass
    * Description : It is used to show upcomming classes and also search
    * Author      : Soumen
    * Date        : 02-AUG-2019
    */
    public function upcommingClass(Request $request)
    {
        if (Auth::user()->is_professional != "Y") {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        $upcommingList = Booking::with('userDetails', 'profDetails', 'parentCatDetails', 'childCatDetails')
            ->where([
                'order_status'      => 'A',
                'professional_id'   => Auth::id()
            ]);
        if (@$request->all()) {
            if (@$request->user) {
                $name = $request->user;
                $upcommingList = $upcommingList->whereHas('userDetails', function ($query) use ($name) {
                    $query->where('name', 'LIKE', '%' . $name . '%');
                });
            }
            if (@$request->from_date) {
                $upcommingList = $upcommingList->where('date', '>=', $request->from_date);
            }
            if (@$request->to_date) {
                $upcommingList = $upcommingList->where('date', '<=', $request->to_date);
            }
            $key = $request->all();
        }
        $upcommingList = $upcommingList->get();
        return view('modules.professional.professional_upcomming_list')->with([
            'upcommingList' => $upcommingList,
            'key'           => @$key
        ]);
    }

    public function upcommingClass1(Request $request)
    {
        if (Auth::user()->is_professional != "Y") {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        $upcommingList = Booking::with('userDetails', 'profDetails', 'parentCatDetails', 'childCatDetails')
            ->where([
                'order_status'  => 'A',
                'user_id'       => Auth::id()
            ]);
        if (@$request->all()) {
            if (@$request->professional) {
                $name = $request->professional;
                $upcommingList = $upcommingList->whereHas('profDetails', function ($query) use ($name) {
                    $query->where('name', 'LIKE', '%' . $name . '%');
                });
            }
            if (@$request->from_date) {
                $upcommingList = $upcommingList->where('date', '>=', $request->from_date);
            }
            if (@$request->to_date) {
                $upcommingList = $upcommingList->where('date', '<=', $request->to_date);
            }
            $key = $request->all();
        }
        $upcommingList = $upcommingList->get();
        return view('modules.professional.professional_as_user_upcomming_list')->with([
            'upcommingList' => $upcommingList,
            'key'           => @$key
        ]);
    }

    public function viewBookedUserDetails($id)
    {
        if (Auth::user()->is_professional != "Y") {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        $booking = Booking::with('userDetails', 'parentCatDetails', 'childCatDetails')->where('id', $id)->first();
        return view('modules.professional.view_booked_user_details')->with([
            'booking' => $booking
        ]);
    }

    /**
    ######################For Validating CPF NUMBER#########################################
     */

    public function verificaDigitos($digito1, $digito2, $ver, $ver2)
    {
        $num = $digito1 % 11;
        $digito1 = ($num < 2) ? 0 : 11 - $num;
        $num2 = $digito2 % 11;
        $digito2 = ($num2 < 2) ? 0 : 11 - $num2;
        if ($digito1 === (int) $this->numeral[$ver] && $digito2 === (int) $this->numeral[$ver2]) {
            return true;
        } else {
            return false;
        }
    }
    private function verificaCPF()
    {
        $num1 = ($this->numeral[0] * 10) + ($this->numeral[1] * 9) + ($this->numeral[2] * 8) + ($this->numeral[3] * 7) + ($this->numeral[4] * 6)
            + ($this->numeral[5] * 5) + ($this->numeral[6] * 4) + ($this->numeral[7] * 3) + ($this->numeral[8] * 2);
        $num2 = ($this->numeral[0] * 11) + ($this->numeral[1] * 10) + ($this->numeral[2] * 9) + ($this->numeral[3] * 8) + ($this->numeral[4] * 7)
            + ($this->numeral[5] * 6) + ($this->numeral[6] * 5) + ($this->numeral[7] * 4) + ($this->numeral[8] * 3) + ($this->numeral[9] * 2);
        return $this->verificaDigitos($num1, $num2, 9, 10);
    }
    private function verificaCNPJ()
    {
        $num1 = ($this->numeral[0] * 5) + ($this->numeral[1] * 4) + ($this->numeral[2] * 3) + ($this->numeral[3] * 2) + ($this->numeral[4] * 9) + ($this->numeral[5] * 8)
            + ($this->numeral[6] * 7) + ($this->numeral[7] * 6) + ($this->numeral[8] * 5) + ($this->numeral[9] * 4) + ($this->numeral[10] * 3) + ($this->numeral[11] * 2);
        $num2 = ($this->numeral[0] * 6) + ($this->numeral[1] * 5) + ($this->numeral[2] * 4) + ($this->numeral[3] * 3) + ($this->numeral[4] * 2) + ($this->numeral[5] * 9)
            + ($this->numeral[6] * 8) + ($this->numeral[7] * 7) + ($this->numeral[8] * 6) + ($this->numeral[9] * 5) + ($this->numeral[10] * 4) + ($this->numeral[11] * 3) + ($this->numeral[12] * 2);
        return $this->verificaDigitos($num1, $num2, 12, 13);
    }
    private function getNumeral()
    {
        $this->numeral = preg_replace("/[^0-9]/", "", $this->CPF_CNPJ);
        $strLen = strlen($this->numeral);
        $ret = false;
        switch ($strLen) {
            case 11:
                if ($this->verificaCPF()) {
                    $this->tipo = 'CPF';
                    $ret = true;
                }
                break;
            case 14:
                if ($this->verificaCNPJ()) {
                    $this->tipo = 'CNPJ';
                    $ret = true;
                }
                break;
            default:
                $ret = false;
                break;
        }
        return $ret;
    }
    public function __toString()
    {
        return $this->CPF_CNPJ;
    }
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * For Adding new bank account #######
     */
    public function addBank(Request $request)
    {
        if ($request->all()) {
            $request->validate(
                [
                    'bank_name'                 =>  'required',
                    'account_number'            =>  'required',
                    'agency_number'             =>  'required',
                    'bank_number'               =>  'required',
                    //'account_check_number'      =>  'required',
                    'account_type'              =>  'required',
                ],
                [
                    'bank_name.required' =>  'Nome do banco é obrigatório',
                    'account_number.required' =>  'Número da conta é obrigatório',
                    'agency_number.required' =>  'Número da agência é obrigatório',
                    'bank_number.required' =>  'Número do banco é obrigatório',
                    //'account_check_number'      =>  'required',
                    'account_type.required' =>  'O tipo de conta é obrigatório',
                ]
            );

            $user = User::where('id', Auth::id())->first();
            if ($user->professional_access_token != null) {
                $bank = UserToBankAccount::where('user_id', Auth::id())->first();
                $token = $user->professional_access_token;
                if (env('PAYMENT_ENVIORNMENT') == 'live') {
                    // $moip = new Moip(new OAuth($token), Moip::ENDPOINT_PRODUCTION . '/v2/accounts/' . $id . '/bankaccounts');
                    $moip = new Moip(new OAuth($token), Moip::ENDPOINT_PRODUCTION);
                } else if (env('PAYMENT_ENVIORNMENT') == 'sandbox') {
                    // $moip = new Moip(new OAuth($token), Moip::ENDPOINT_SANDBOX . '/v2/accounts/' . $id . '/bankaccounts');
                    $moip = new Moip(new OAuth($token), Moip::ENDPOINT_SANDBOX);
                }
                $bankAccount = $moip->bankaccount()
                    ->setBankNumber(@$request->bank_number)
                    ->setAgencyNumber(@$request->agency_number)
                    ->setAgencyCheckNumber(0)
                    ->setAccountNumber(@$request->account_number)
                    ->setAccountCheckNumber(@$request->account_check_number) //commented for client requirement
                    ->setType(@$request->account_type)
                    ->setHolder(@$user->name, @$user->cpf_no, 'CPF')
                    ->create($user->professional_id);
                if (!@$bankAccount->getId()) {
                    session()->flash('error', \Lang::get('client_site.wirecard_error'));
                    return redirect()->back()->withInput($request->input());
                }
                UserToBankAccount::where('user_id', Auth::id())->delete();
                $create = UserToBankAccount::create([
                    'user_id'                 =>  Auth::id(),
                    'bankName'                =>  $request->bank_name,
                    'accountNumber'           =>  $request->account_number,
                    'agencyNumber'            =>  $request->agency_number,
                    'bank_number'             =>  $request->bank_number,
                    'accountCheckNumber'      =>  @$request->account_check_number,
                    'account_type'            =>  $request->account_type,
                    'account_id'        => @$bankAccount->getId()
                ]);

                session()->flash('success', \Lang::get('client_site.bank_account_successfully_updated'));
                return redirect()->back();
            }
            UserToBankAccount::where('user_id', Auth::id())->delete();
            $create = UserToBankAccount::create([
                'user_id'                 =>  Auth::id(),
                'bankName'                =>  $request->bank_name,
                'accountNumber'           =>  $request->account_number,
                'agencyNumber'            =>  $request->agency_number,
                'bank_number'             =>  $request->bank_number,
                'accountCheckNumber'      =>  @$request->account_check_number,
                'account_type'            =>  $request->account_type
            ]);

            session()->flash('success', \Lang::get('client_site.bank_account_successfully_updated'));
            return redirect()->back();
        }
    }

    /**
     * Show and update the categories for the professional
     *
     * @param Request $request
     */
    public function profileCategory(Request $request)
    {
        $user = Auth::user();
        $categories = Category::where('parent_id', 0)->with('childCat')->get();
        if ($user->is_professional != "Y") {
            session()->flash('error', Lang::get('site.visit_not_allowed'));
            return redirect()->back();
        }
        if ($request->all()) {
            $request->validate([
                'category.*' => 'required',
                'sub_category.*' => 'required',
            ]);
            UserToCategory::where('user_id', $user->id)->delete();
            foreach ($request->category ?? [] as $key => $value) {
                $findCat = UserToCategory::where([
                    'category_id' => $value,
                    'user_id' => $user->id
                ])->exists();

                if (!$findCat) {
                    $userToCat = UserToCategory::create([
                        'user_id' => $user->id,
                        'category_id' => $value,
                        'level' => 0
                    ]);
                }

                $subCat = $request->sub_category[$key];
                if ($subCat) {
                    $findCat1 = UserToCategory::where([
                        'category_id'   =>  $subCat,
                        'user_id'       =>  $user->id
                    ])->first();

                    if ($findCat1 == null) {

                        $userToSubCat = UserToCategory::create([
                            'user_id' => $user->id,
                            'category_id' => $subCat,
                            'level' => 1,
                            'parent_id' => $value
                        ]);
                    }
                }
            }
            session()->flash('success', Lang::get('site.cat_updated_success'));
            return redirect()->back();
        }
        $userCategories = UserToCategory::with('categoryName.parent')->where('level', 1)->where('user_id', Auth::id())->get();
        return view('modules.professional.professional_category', compact('userCategories', 'categories'));
    }

    /**
     *method: introductoryVideo
     *created By: Abhisek
     *description: For Professional Edit-profile
     */

    public function introductoryVideo(Request $request)
    {
        $user = User::where('id', Auth::id())->first();
        return view('modules.professional.professional_introductory_video')->with(['user' => $user]);
    }
    public function introductoryVideoSubmit(Request $request)
    {
        $request->validate([
            'introductory_video' => 'required|in:Y,V,A',
        ]);
        $upd = [];
        $user = User::where('id', Auth::id())->first();
        if ($request->introductory_video == 'Y') {
            $request->validate([
                'intro_video' => 'required',
            ]);
            $upd['introductory_video_type'] = $request->introductory_video;
            if (@$request->intro_video) {
                $url = @$request->intro_video;
                parse_str(parse_url($url, PHP_URL_QUERY), $my_array_of_vars);
                $url_id = @$my_array_of_vars['v'];
                if ($url_id == null) {
                    $pieces = explode("/", $url);
                    $url_id = end($pieces);
                    if ($url_id == null) {
                        session()->flash('error', \Lang::get('client_site.wrong_url'));
                        return redirect()->back();
                    }
                }
                $upd['introductory_video'] = $url_id;
            }
            $update =  User::where('id', Auth::id())->update($upd);
            if (@$update) {
                if ($user->introductory_video_type == 'V' || $user->introductory_video_type == 'A') {
                    @unlink(storage_path('app/public/uploads/introductory_video/' . $user->introductory_video));
                }
                session()->flash('success', Lang::get('client_site.introductory_video_updated'));
                return redirect()->back();
            }
        }
        if ($request->introductory_video == 'V') {
            $request->validate([
                'video_file' => 'required',
            ]);
            $upd['introductory_video_type'] = $request->introductory_video;



            if (@$request->hasFile('video_file')) {
                $into_video_file = time() . '-' . rand(1000, 9999) . '.' . @$request->video_file->getClientOriginalExtension();
                $video_file = @$request->video_file;
                Storage::putFileAs('public/uploads/introductory_video', $video_file, $into_video_file);
                $upd['introductory_video'] = $into_video_file;
            }
            $update =  User::where('id', Auth::id())->update($upd);
            if (@$update) {
                if ($user->introductory_video_type == 'V' || $user->introductory_video_type == 'A') {
                    @unlink(storage_path('app/public/uploads/introductory_video/' . $user->introductory_video));
                }
                session()->flash('success', Lang::get('client_site.introductory_video_updated'));
                return redirect()->back();
            }
        }
        if ($request->introductory_video == 'A') {
            $request->validate([
                'audio_file' => 'required',
            ]);
            $upd['introductory_video_type'] = $request->introductory_video;


            if (@$request->hasFile('audio_file')) {
                $into_audio_file = time() . '-' . rand(1000, 9999) . '.' . @$request->audio_file->getClientOriginalExtension();
                $audio_file = @$request->audio_file;
                Storage::putFileAs('public/uploads/introductory_video', $audio_file, $into_audio_file);
                $upd['introductory_video'] = $into_audio_file;
            }
            $update =  User::where('id', Auth::id())->update($upd);
            if (@$update) {
                if ($user->introductory_video_type == 'V' || $user->introductory_video_type == 'A') {
                    @unlink(storage_path('app/public/uploads/introductory_video/' . $user->introductory_video));
                }
                session()->flash('success', Lang::get('client_site.introductory_video_updated'));
                return redirect()->back();
            }
        }
        // return $request;
        session()->flash('error', Lang::get('client_site.introductory_video_error'));
        return redirect()->back();
    }

    /**
     *method: showProfileInSearch
     *created By: Sayantani
     *description: For showing profile or not in search results
     */
    public function showProfileInSearch(Request $request)
    {
        $user = Auth::user();
        if (@$request->show) {
            $user->show_in_search = @$request->show;
            $user->save();
            if ($user->show_in_search == 'Y') {
                $response['message'] = "Shown profile succesfully.";
            } else {
                $response['message'] = "Hidden profile succesfully.";
            }
            return response()->json($response);
        }
    }

    /**
     *method: professionalExperienceNew
     *created By: Sayantani
     *description: For Inserting and loading Professional experience
     */
    public function professionalExperienceNew(Request $request)
    {
        if (Auth::user()->is_professional != "Y") {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        if ($request->all()) {
            $request->validate([
                'experiences' => 'required',
            ]);
            Auth::user()->getExperiences()->sync($request->experiences);
            return redirect()->back()->with('success', \Lang::get('client_site.experience_saved'));
        }
        $data['experiences'] = Experience::where('added_by', 'A')->orWhere(function ($q) {
            $q = $q->where('added_by', 'P')->where('user_id', Auth::id());
        })->get();
        $data['my_experiences'] = Auth::user()->getExperiences()->get();
        $data['my_exps'] = Auth::user()->getExperiences()->get()->pluck('id')->toArray();
        return view('modules.professional.professional_experience_new')->with($data);
    }

    /**
     *method: professionalAddNewExperience
     *created By: Sayantani
     *description: For adding a new experience
     */
    public function professionalAddNewExperience(Request $request)
    {
        $request->validate([
            'experience' => 'required',
        ]);
        $exists = Experience::whereRaw('UPPER(experience) = ?', strtoupper($request->experience))->first();
        if (@$exists) {
            return redirect()->back()->with('error', \Lang::get('client_site.experience_exists'));
        }
        $insert['experience'] = $request->experience;
        $insert['added_by'] = 'P';
        $insert['user_id'] = Auth::id();
        $new_exp = Experience::create($insert);
        Auth::user()->getExperiences()->attach($new_exp);
        return redirect()->back()->with('success', \Lang::get('client_site.experience_saved'));
    }

    public function changPassword()
    {
        return view('modules.professional.change_password');
    }

    public function uploadSign(Request $request)
    {
        if ($request->all()) {
            // dd($request->all());
            if (@$request->signature2) {
                $filename = time() . '-' . rand(1000, 9999) . '.png';
                $path = "storage/app/public/uploads/signature/";
                $image = Image::make(@$request->signature2);
                $image->save($path . $filename);
                @unlink(storage_path('app/public/uploads/signature/' . auth()->user()->signature));
                User::where('id', Auth::id())->update([
                    'signature'   =>  @$filename
                ]);
            }
            if (@$request->signature) {
                $request->validate([
                    'signature'    =>  'image|mimes:jpeg,png,jpg',
                ]);
                $signature_pic = $request->signature;
                $filename = time() . '-' . rand(1000, 9999) . '.' . $signature_pic->getClientOriginalExtension();
                Storage::putFileAs('public/uploads/signature', $signature_pic, $filename);
                @unlink(storage_path('app/public/uploads/signature/' . auth()->user()->signature));
                User::where('id', Auth::id())->update([
                    'signature'   =>  @$filename
                ]);
            }
            return redirect()->back()->with('success',  \Lang::get('client_site.signature_uploaded'));
        }
        return view('modules.professional.signature');
    }

    /**
     *method: checkNickName
     *created By: Sayantani
     *description: For nick name duplicate checking
     */

    public function checkNickName(Request $request)
    {
        $response = [
            'jsonrpc'   =>  '2.0'
        ];
        if ($request->params['name']) {
            $user = User::where('nick_name', $request->params['name'])
                ->where('id', '!=', Auth::id())
                ->first();
            if ($user != null) {
                $response['status'] = 1;
            } else {
                $response['status'] = 0;
            }
        }
        return response()->json($response);
    }

    /**
     *method: updatePaypalAddress
     *created By: Sayantani
     *description: For updating paypal address
     */

    public function updatePaypalAddress(Request $request)
    {
        $user = Auth::user();
        if(@$user){
            $user->paypal_address = $request->paypal_address;
            $user->save();
            return redirect()->back()->with('success', \Lang::get('client_site.paypal_email_address_saved'));
        }
        return redirect()->back()->with('error', \Lang::get('client_site.user_not_found'));
    }

    /**
     *   Method      : copySchedule
     *   Description : This is used to copy schedule of professional
     *   Author      : Sayantani
     *   Date        : 2021-JUNE-30
     **/
    public function copySchedule(Request $request)
    {
        if($request->period == 'M'){
            $first_date = date('Y-m-01', strtotime($request->copy_month_from));
            $last_date = date('Y-m-t', strtotime($request->copy_month_from));

            $first_copy_date = date('Y-m-01', strtotime($request->copy_month_to));
            $last_copy_date = date('Y-m-t', strtotime($request->copy_month_to));
            $m = date('m', strtotime($request->copy_month_to));
            // dd(['last_date' => date('d', strtotime($last_date)), 'last_copy_date' => date('d', strtotime($last_copy_date))]);
            $av = UserAvailability::where('user_id', Auth::id())
                                    ->where('slot_start', '>', now(env('TIMEZONE'))->addHour('3')->toDateTimeString())
                                    ->whereNotIn('slot_start', getUsedTimeSlotsForAllDates(Auth::id()))
                                    ->where('date', '>=', $first_date)->where('date', '<=', $last_date)->get();
            $flag = 0; $array = [];
            if(date('d', strtotime($last_date)) == 31 && date('d', strtotime($last_copy_date)) == 30){
                $flag = 1;
            }
            foreach ($av as $a) {
                $date = date('Y-'.$m.'-d', strtotime($a->date));
                $slot = [ date( 'H:i', strtotime($a->slot_start) ) . " - " . date( 'H:i', strtotime( $a->slot_end ) )];
                if ( $this->checkIfAvlAlreadyInserted($slot,  $date) && $this->checkIfAvlByMins($slot[0], $date) ) {
                    if($flag == 1){
                        if(date('d', strtotime($a->date)) != 31){
                            $create['date'] = date('Y-'.$m.'-d', strtotime($a->date));
                            $create['slot_start'] = date('Y-'.$m.'-d H:i:s', strtotime($a->slot_start));
                            $create['slot_end'] = date('Y-'.$m.'-d H:i:s', strtotime($a->slot_end));
                            $create['user_id'] = $a->user_id;
                            $create['time_interval'] = $a->time_interval;
                            $create['from_time'] = $a->from_time;
                            $create['to_time'] = $a->to_time;
                            $avll = UserAvailability::create($create);
                            array_push($array, $avll);
                        }
                    } else {
                        $create['date'] = date('Y-'.$m.'-d', strtotime($a->date));
                        $create['slot_start'] = date('Y-'.$m.'-d H:i:s', strtotime($a->slot_start));
                        $create['slot_end'] = date('Y-'.$m.'-d H:i:s', strtotime($a->slot_end));
                        $create['user_id'] = $a->user_id;
                        $create['time_interval'] = $a->time_interval;
                        $create['from_time'] = $a->from_time;
                        $create['to_time'] = $a->to_time;
                        $avll = UserAvailability::create($create);
                        array_push($array, $avll);
                    }
                }
            }
        } else if($request->period == 'W'){
            $from_year = explode(',', $request->copy_weeknumber_from)[0];
            $from_weeknum = explode(',', $request->copy_weeknumber_from)[1];

            $from_timestamp = mktime( 0, 0, 0, 1, 1,  $from_year ) + ( $from_weeknum * 7 * 24 * 60 * 60 );
            $from_timestamp_for_sunday = $from_timestamp - 86400 * ( date( 'N', $from_timestamp ) );
            $from_first_date = date( 'Y-m-d', $from_timestamp_for_sunday );
            $from_last_date = date( 'Y-m-d', strtotime("+7 day", $from_timestamp_for_sunday) );

            $to_year = explode(',', $request->copy_weeknumber_to)[0];
            $to_weeknum = explode(',', $request->copy_weeknumber_to)[1];

            $to_timestamp = mktime( 0, 0, 0, 1, 1,  $to_year ) + ( $to_weeknum * 7 * 24 * 60 * 60 );
            $to_timestamp_for_sunday = $to_timestamp - 86400 * ( date( 'N', $to_timestamp ) );
            $to_first_date = date( 'Y-m-d', $to_timestamp_for_sunday );
            $to_last_date = date( 'Y-m-d', strtotime("+7 day", $to_timestamp_for_sunday) );

            $loop_date = strtotime($from_first_date);
            $last = strtotime($from_last_date);
            $dates = $array = []; $c = 0;
            while( $loop_date < $last ) {
                $date = date( 'Y-m-d', strtotime("+".$c." day", strtotime($to_first_date)) );
                array_push($dates, date( 'Y-m-d', strtotime($date) ));
                $av = UserAvailability::where('user_id', Auth::id())
                                        ->where('slot_start', '>', now(env('TIMEZONE'))->addHour('3')->toDateTimeString())
                                        ->whereNotIn('slot_start', getUsedTimeSlotsForAllDates(Auth::id()))
                                        ->where('date', date( 'Y-m-d', $loop_date ))->get();
                foreach ($av as $a) {
                    $slot = [ date( 'H:i', strtotime($a->slot_start) ) . " - " . date( 'H:i', strtotime( $a->slot_end ) )];
                    // $slot = $a->slot_start . " - " . $a->slot_end;
                    // dd($slot);
                    // dd( $this->checkIfAvlByMins($slot[0],  date('Y-m-d', strtotime($date))) );
                    if ( $this->checkIfAvlAlreadyInserted($slot,  date('Y-m-d', strtotime($date))) && $this->checkIfAvlByMins($slot[0],  date('Y-m-d', strtotime($date))) ) {
                        $create['date'] = $date;
                        $create['slot_start'] = $date . " " . date('H:i:s', strtotime($a->slot_start));
                        $create['slot_end'] = $date . " " . date('H:i:s', strtotime($a->slot_end));
                        $create['user_id'] = $a->user_id;
                        $create['time_interval'] = $a->time_interval;
                        $create['from_time'] = $a->from_time;
                        $create['to_time'] = $a->to_time;
                        $avll = UserAvailability::create($create);
                        array_push($array, $avll);
                    }
                }
                $c += 1;
                $loop_date = strtotime( '+1 day', $loop_date );
            }
        }
        // dd($array);
        session()->flash('success', \Lang::get('client_site.time_table_successfully_updated'));
        return redirect()->back();
    }

    /**
     *   Method      : copyScheduleAjaxCheck
     *   Description : This is used to copy schedule of professional
     *   Author      : Sayantani
     *   Date        : 2021-JUNE-30
     **/
    public function copyScheduleAjaxCheck(Request $request)
    {
        if($request->period == 'M'){
            if ($request->copy_month_from == '' || $request->copy_month_to == '') {
                $response['error'] = [
                    'message' => Lang::get('client_site.invalid_monthly_slots'),
                    'request' => $request->all(),
                ];
                return response()->json($response);
            }
            $first_date = date('Y-m-01', strtotime($request->copy_month_from));
            $last_date = date('Y-m-t', strtotime($request->copy_month_from));
            $first_month = date('m', strtotime($request->copy_month_from));

            $first_copy_date = date('Y-m-01', strtotime($request->copy_month_to));
            $last_copy_date = date('Y-m-t', strtotime($request->copy_month_to));
            $first_copy_month = date('m', strtotime($request->copy_month_to));

            if($first_month >= $first_copy_month){
                $response['error'] = [
                    'message' => Lang::get('client_site.week_from_greater_than_to')
                ];
                return response()->json($response);
            }

            // check if copy month to start date is greater than current server time
            $curServerTime = strtotime((now(env('TIMEZONE'))->addHour('3')->toDateTimeString()));
            $reqDate = date('Y-m-d', strtotime($first_copy_date));
            $inv = false;
            $startTime = strtotime(toUTCTime(date('Y-m-d H:i:s', strtotime($reqDate . ' ' . '00:00:00'))));
            if ($curServerTime >= $startTime) {
                $inv = true;
            }
            if ($inv) {
                $response['error'] = [
                    'message' => Lang::get('O horário disponibilizado entre as sessões, deve respeitar o prazo selecionado de intervalo entre uma sessão e outra.'),
                ];
                return response()->json($response);
            }

            // check if any availabity is there to be added
            $av = UserAvailability::where('user_id', Auth::id())
                                    ->where('slot_start', '>', now(env('TIMEZONE'))->addHour('3')->toDateTimeString())
                                    ->whereNotIn('slot_start', getUsedTimeSlotsForAllDates(Auth::id()))
                                    ->where('date', '>=', $first_date)->where('date', '<=', $last_date)->get();
            if(count($av) == 0){
                $response['error'] = [
                    'message' => Lang::get('client_site.no_time_slots_this_month')
                ];
                return response()->json($response);
            }

            // all tests done - form valid
            $response['result'] = [
                'code' => 'AVL'
            ];
            return response()->json($response);

        } else if($request->period == 'W'){
            if ($request->copy_week_from == '' || $request->copy_week_to == '' || $request->copy_weeknumber_from == '' || $request->copy_weeknumber_to == '') {
                $response['error'] = [
                    'message' => Lang::get('client_site.invalid_monthly_slots'),
                    'request' => $request->all(),
                ];
                return response()->json($response);
            }
            $from_year = explode(',', $request->copy_weeknumber_from)[0];
            $from_weeknum = explode(',', $request->copy_weeknumber_from)[1];

            $from_timestamp = mktime( 0, 0, 0, 1, 1,  $from_year ) + ( $from_weeknum * 7 * 24 * 60 * 60 );
            $from_timestamp_for_sunday = $from_timestamp - 86400 * ( date( 'N', $from_timestamp ) );
            $from_first_date = date( 'Y-m-d', $from_timestamp_for_sunday );
            $from_last_date = date( 'Y-m-d', strtotime("+7 day", $from_timestamp_for_sunday) );

            $to_year = explode(',', $request->copy_weeknumber_to)[0];
            $to_weeknum = explode(',', $request->copy_weeknumber_to)[1];

            $to_timestamp = mktime( 0, 0, 0, 1, 1,  $to_year ) + ( $to_weeknum * 7 * 24 * 60 * 60 );
            $to_timestamp_for_sunday = $to_timestamp - 86400 * ( date( 'N', $to_timestamp ) );
            $to_first_date = date( 'Y-m-d', $to_timestamp_for_sunday );
            $to_last_date = date( 'Y-m-d', strtotime("+7 day", $to_timestamp_for_sunday) );

            // if($from_weeknum >= $to_weeknum){
            //     $response['error'] = [
            //         'message' => Lang::get('client_site.week_from_greater_than_to')
            //     ];
            //     return response()->json($response);
            // }

            // check if copy week to start date is greater than current server time
            $curServerTime = strtotime((now(env('TIMEZONE'))->addHour('3')->toDateTimeString()));
            $reqDate = date('Y-m-d', strtotime($to_first_date));
            $inv = false;
            $startTime = strtotime(toUTCTime(date('Y-m-d H:i:s', strtotime($reqDate . ' ' . '00:00:00'))));
            if ($curServerTime >= $startTime) {
                $inv = true;
            }
            if ($inv) {
                $response['error'] = [
                    'message' => Lang::get('O horário disponibilizado entre as sessões, deve respeitar o prazo selecionado de intervalo entre uma sessão e outra.'),
                ];
                return response()->json($response);
            }

            // check if any availabity is there to be added
            $av = UserAvailability::where('user_id', Auth::id())
                                    ->where('slot_start', '>', now(env('TIMEZONE'))->addHour('3')->toDateTimeString())
                                    ->whereNotIn('slot_start', getUsedTimeSlotsForAllDates(Auth::id()))
                                    ->where('date', '>=', $from_first_date)->where('date', '<=', $from_last_date)->get();
            if(count($av) == 0){
                $response['error'] = [
                    'message' => Lang::get('client_site.no_time_slots_this_week')
                ];
                return response()->json($response);
            }

            // all tests done - form valid
            $response['result'] = [
                'code' => 'AVL'
            ];
            return response()->json($response);
        }
    }

    /**
     *method: deleteCreditCard
     *created By: Soumojit
     *description: For delete credit card
     */

    public function deleteCreditCard(Request $request)
    {
        $userData= User::where('id', Auth::id())->first();
        $upd=[];
        // $upd['customer_id']=null;
        $upd['is_credit_card_added']='N';
        User::where('id', Auth::id())->update($upd);
        UserToCard::where('user_id',Auth::id())->delete();
        session()->flash('success', \Lang::get('client_site.card_deleted_succesfully'));
        return redirect()->back();
    }

    /**
     *method: verifyIdentity
     *created By: Sayantani
     *description: For uploading identity documents
     */
    public function verifyIdentity(Request $request)
    {
        $data = [];
        $data['user'] = Auth::user();
        $data['all_empty'] = "true";
        if(@$data['user']->company_data || @$data['user']->passport || @$data['user']->cpf || @$data['user']->proof_of_residence){
            $data['all_empty'] = "false";
        }
        if(@$request->all()){
            if(@$request->company_data){
                $file = $request->company_data;
                $filename = time() . '-' . rand(1000, 9999) . '.' . $file->getClientOriginalExtension();
                Storage::putFileAs('public/uploads/identity', $file, $filename);
                @unlink('storage/app/public/uploads/identity/' . auth()->user()->company_data);
                User::where('id', Auth::id())->update([
                    'company_data'   =>  @$filename
                ]);
            }
            if(@$request->passport){
                $file = $request->passport;
                $filename = time() . '-' . rand(1000, 9999) . '.' . $file->getClientOriginalExtension();
                Storage::putFileAs('public/uploads/identity', $file, $filename);
                @unlink('storage/app/public/uploads/identity/' . auth()->user()->passport);
                User::where('id', Auth::id())->update([
                    'passport'   =>  @$filename
                ]);
            }
            if(@$request->cpf){
                $file = $request->cpf;
                $filename = time() . '-' . rand(1000, 9999) . '.' . $file->getClientOriginalExtension();
                Storage::putFileAs('public/uploads/identity', $file, $filename);
                @unlink('storage/app/public/uploads/identity/' . auth()->user()->cpf);
                User::where('id', Auth::id())->update([
                    'cpf'   =>  @$filename
                ]);
            }
            if(@$request->proof_of_residence){
                $file = $request->proof_of_residence;
                $filename = time() . '-' . rand(1000, 9999) . '.' . $file->getClientOriginalExtension();
                Storage::putFileAs('public/uploads/identity', $file, $filename);
                @unlink('storage/app/public/uploads/identity/' . auth()->user()->proof_of_residence);
                User::where('id', Auth::id())->update([
                    'proof_of_residence'   =>  @$filename
                ]);
            }
            session()->flash('success', \Lang::get('client_site.files_uploaded_succesfully'));
            return redirect()->back();
        }
        return view('modules.professional.professional_verify_identity')->with($data);
    }
}
