<?php

namespace App\Http\Controllers\Modules\Professional;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\ForgetPassword;
use App\User;
use App\Models\Favourite;
use App\Models\Category;
use App\Models\UserAvailability;
use App\Models\Booking;
use App\Mail\RescheduleReject;
use App\Mail\RescheduleApprove;
use Auth;
use Mail;
use Session;
use DateTime;

use App\Models\UserToTools;
use App\Models\CompetencesMaster;
use App\Models\Content;
use App\Models\Review;
use App\Models\Product;
use App\Models\TypeOfEvaluationMaster;
use App\Models\TypeEvaluationToCompetences;
use App\Models\Tools360EvaluationUserAnswer;
use App\Models\Tools360EvaluationUrlShare;
use App\Models\ProductOrderDetails;
use Carbon\Carbon;

class PublicProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');

    // }

    public function index($slug = '', $self = '')
    {
        $user = User::where('slug', $slug);
        if ($self == 'self') {
            $user = $user->where('id', auth()->id());
        } else {
            $user = $user->where('id', '!=', auth()->id());
        }
        $user = $user->first();
        if ($user == null) {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }

        $user->load('userDetails.categoryName', 'userExperience', 'userQualification', 'userLang.languageName');
        $userReview = Review::where('professional_id', $user->id)
        ->with(['reviewedUserName'=>function($query){
            $query->select('id','name','nick_name');
        }
        ])->orderBy('id', 'DESC')->get();
        $av = UserAvailability::where('user_id', $user->id)->where('slot_start', '>', now(env('TIMEZONE'))->addMinutes('30')->toDateTimeString())->whereNotIn('slot_start', getUsedTimeSlotsForAllDates($user->id))->get();
        $avlDay = [];
        $avlDay1 = [];
        foreach ($av->toArray() as $a) {
            $avl = [];
            $st = '';
            $avl['datecls'] = "date-". $a['date'];
            foreach ($a as $k => $v) {
                if ($k == 'slot_start') {
                    $avl[$k] = toUserTime($v, 'Y-m-d\TH:i:s');
                    $avl['time_start'] = toUserTime($v, 'g:i A');
                    $st = toUserTime($v, 'Y-m-d');
                } else if ($k == 'slot_end') {
                    $avl[$k] = toUserTime($v, 'Y-m-d\TH:i:s');
                    $avl['time_end'] = toUserTime($v, 'g:i A');
                } else {
                    $avl[$k] = $v;
                }
            }
            array_push($avlDay1, $avl);
            if (!in_array($st, $avlDay)) {
                array_push($avlDay, $st);
            }
        }
        $simm = User::where([
            'is_professional' => 'Y',
            'status' => 'A',
            'is_approved' => 'Y',
            'profile_active' =>  'Y',
        ])->where('id', '!=', @$user->id)->with('specializationName','userLang.languageName')->inRandomOrder()->limit(50)->get();

        @$date = date('d-m-Y');
        @$fromTotime = "";
        if (session()->has('bookingDate')) {
            $date = session()->get('bookingDate');
            $time = session()->get('bookingTime');
            $fromTotime = UserAvailability::where([
                'user_id'       =>  @$user->id,
                'date'          =>  @$date
            ])->get();
            $booking_id = Booking::where(['professional_id' => @$user->id, 'date' =>  @date('d-m-Y', strtotime($date))])->first();
            session()->forget('bookingDate');
        } else {
            $fromTotime = UserAvailability::where([
                'user_id'       =>  @$user->id,
                'date'          =>  @$date
            ])->get();
        }

        $profPage = Content::where('id', 11)->first();
        $products = Product::
        select(
            'id','admin_status','status','professional_id',
            'show_in_home_page','category_id','title','slug',
            'price','discounted_price','description','cover_image',
            'purchase_start_date','purchase_end_date'
        )
        ->with([
            'category'=>function($query){
                $query->select('id','category_name','slug');
            },
            'professional'
        ])
        ->where('status', '=', 'A')->where('admin_status', 'A')->where('professional_id', $user->id)->inRandomOrder()->get();
        $orderedProducts = [];
        if(Auth::check()){
            $orderedProducts = ProductOrderDetails::whereHas('orderMaster', function($q){
                $q->where('user_id', Auth::id());
            })->pluck('product_id')->toArray();
        }
        return view('modules.public_profile.public_profile')->with([
            'user'          =>  @$user,
            'userReview'    => $userReview,
            'simm'          =>  @$simm,
            'fromTotime'    =>  @$fromTotime,
            'avlTime1'      => json_encode($avlDay1),
            'avlDay'        => json_encode($avlDay),
            'profPage'      => $profPage,
            'products'      => $products,
            'orderedProducts'=>$orderedProducts
        ]);
    }

    /*
    #method:addFavourite
    #purpose:For adding to favourite list
    #author:Abhisek
    */

    public function addFavourite(Request $request)
    {
        $response = [
            'jsonrpc'   =>  '2.0'
        ];
        if ($request->params['id'] && @Auth::id()) {
            $user = Favourite::where('professional_id', $request->params['id'])->where('user_id', Auth::id())->first();
            if ($user == null) {
                $is_created = Favourite::create([
                    'professional_id'       =>  $request->params['id'],
                    'user_id'               =>  Auth::id()
                ]);
                if ($is_created) {
                    $response['status'] =   1;
                } else {
                    $response['status'] = 0;
                }
            } else {
                $response['status'] = 2;
            }
        } else {
            $response['status'] = 3;
        }
        return response()->json($response);
    }

    /**
     *   Method      : rescheduleApprove
     *   Description : This is use for Approve reschedule date-time
     *   Author      : Pankaj
     *   Date        : 2019-NOV-12
     **/
    public function rescheduleApprove($id)
    {
        // dd($id);userDetails
        $reschedule_approve = Booking::where('id', $id)->first();
        $reschedule_approve = $reschedule_approve->load('userDetails');
        $start_time = substr($reschedule_approve->reschedule_time, 0, 5);
        $end_time = substr($reschedule_approve->reschedule_time, 6);
        $duration = date('H:i', (strtotime($end_time) - strtotime($start_time)));

        // dd($reschedule_approve,$start_time,$end_time,date('H:i', strtotime($duration.' minutes')),$duration);
        $reschedule['date'] = $reschedule_approve->reschedule_date;
        $reschedule['start_time'] = $start_time;
        $reschedule['end_time'] = $end_time;
        $reschedule['duration'] = $duration;
        $reschedule['reschedule_date'] = null;
        $reschedule['reschedule_time'] = null;
        $reschedule['reschedule_status'] = 'A';
        $booking = Booking::where('id', $id)->update($reschedule);
        // dd($booking);
        if (@$booking) {
            Mail::send(new RescheduleApprove($reschedule_approve));
        }

        Session::flash('success', \Lang::get('client_site.reschedule_approve_successfully'));
        return redirect()->back();
    }
    /**
     *   Method      : rescheduleReject
     *   Description : This is use for Reject reschedule date-time
     *   Author      : Pankaj
     *   Date        : 2019-NOV-12
     **/
    public function rescheduleReject($id)
    {
        // dd($id);
        $reschedule_approve = Booking::where('id', $id)->first();
        $reschedule_approve = $reschedule_approve->load('userDetails');
        $reschedule['reschedule_date'] = null;
        $reschedule['reschedule_time'] = null;
        $reschedule['reschedule_status'] = 'R';
        $booking = Booking::where('id', $id)->update($reschedule);
        if (@$booking) {
            Mail::send(new RescheduleReject($reschedule_approve));
        }
        Session::flash('success', \Lang::get('client_site.reschedule_reject_successfully'));
        return redirect()->back();
    }

    //27.3.20 for evulation 360 public view answer

    public function userpPublicEvaluation360($id, $type)
    {
        //$id1 = decrypt($id);
        //pr1($id1);
        //die();

        $id = decrypt($id);
        //pr1($id);
        //die();
        $data['usertype'] = $type;
        $data['user_to_tools_id'] = $id;
        //$data['all_evaluation_data'] = Tools360EvaluationUserAnswer::where('user_to_tools_id',$id)->get();


        $all_evaluation_data = Tools360EvaluationUserAnswer::where('user_to_tools_id', $id)->get();
        $tool360_competences_id = Tools360EvaluationUserAnswer::where('user_to_tools_id', $id)
            ->groupBy('tool360_competences_id')
            ->get();

        $data['all_evaluation_data'] = [];

        foreach ($tool360_competences_id as $key => $value) {
            // $data['all_evaluation_data'][$key]['val'] = $value;
            foreach ($all_evaluation_data as $key1 => $value1) {
                if ($value->tool360_competences_id === $value1->tool360_competences_id) {
                    $data['all_evaluation_data'][$key][] = $value1;
                }
            }
        }


        //pr1($data['all_evaluation_data']->toArray());
        //die();

        return view('modules.prof_evaluation360.public_scorelist')->with($data);
    }


    public function userEvaluation360ScorePost(Request $request)
    {
        $data = $request->all();
        //pr($data);
        //die();

        $alldetails = Tools360EvaluationUrlShare::where('user_to_tools_id', $request->user_to_tools_id)->where('user_type', $request->usertype)->first();

        //pr1($data);
        //die();

        if (@$alldetails->status == "complete") {
            session()->flash('error', "Already submited");
            return redirect()->back();
        } else {

            if ($data) {
                if ($request->user_answer_point) {

                    //update Tools360EvaluationUserAnswer table useranswer point start

                    foreach ($request->user_answer_point as $key => $val) {

                        $insert = array();

                        if ($request->usertype == "C") {
                            $insert['customer_answer_point']    = $val;
                            $insert['customer_report_note']     = $request->report_note[$key];
                        }

                        if ($request->usertype == "M") {
                            $insert['manager_answer_point']    = $val;
                            $insert['manager_report_note']    = $request->report_note[$key];
                        }

                        if ($request->usertype == "O") {
                            $insert['others_answer_point']    = $val;
                            $insert['others_report_note']     = $request->report_note[$key];
                        }

                        if ($request->usertype == "P") {
                            $insert['pairs_answer_point']    = $val;
                            $insert['pairs_report_note']     = $request->report_note[$key];
                        }

                        if ($request->usertype == "S") {
                            $insert['subordinates_answer_point']    = $val;
                            $insert['subordinates_report_note']     = $request->report_note[$key];
                        }

                        if ($request->usertype == "U") {
                            $insert['user_answer_point']    = $val;
                            $insert['report_note']     = $request->report_note[$key];
                        }

                        //$insert['report_note']          = $request->report_note[$key];

                        $user_answer_id = $request->evaluation_user_answer_id[$key];

                        Tools360EvaluationUserAnswer::where('id', $user_answer_id)->update($insert);
                    }

                    //update Tools360EvaluationUserAnswer table useranswer point end

                    //get avarage ans point start
                    $user_answer_point = Tools360EvaluationUserAnswer::where('user_to_tools_id', $request->user_to_tools_id)->get();

                    $customer_avg_point      = $user_answer_point->avg('customer_answer_point');
                    $manager_avg_point       = $user_answer_point->avg('manager_answer_point');
                    $others_avg_point        = $user_answer_point->avg('others_answer_point');
                    $pairs_avg_point         = $user_answer_point->avg('pairs_answer_point');
                    $subordinates_avg_point  = $user_answer_point->avg('subordinates_answer_point');
                    $user_avg_point  = $user_answer_point->avg('user_answer_point');

                    //get avarage ans point end

                    //update avg anser point in Tools360EvaluationUrlShare table start


                    if ($request->usertype == "C") {
                        Tools360EvaluationUrlShare::where('user_to_tools_id', $request->user_to_tools_id)->where('user_type', $request->usertype)->update(['avg_ans_point' => $customer_avg_point, 'status' => 'complete']);
                    }

                    if ($request->usertype == "M") {
                        Tools360EvaluationUrlShare::where('user_to_tools_id', $request->user_to_tools_id)->where('user_type', $request->usertype)->update(['avg_ans_point' => $manager_avg_point, 'status' => 'complete']);
                    }

                    if ($request->usertype == "O") {
                        Tools360EvaluationUrlShare::where('user_to_tools_id', $request->user_to_tools_id)->where('user_type', $request->usertype)->update(['avg_ans_point' => $others_avg_point, 'status' => 'complete']);
                    }

                    if ($request->usertype == "P") {
                        Tools360EvaluationUrlShare::where('user_to_tools_id', $request->user_to_tools_id)->where('user_type', $request->usertype)->update(['avg_ans_point' => $pairs_avg_point, 'status' => 'complete']);
                    }

                    if ($request->usertype == "S") {
                        Tools360EvaluationUrlShare::where('user_to_tools_id', $request->user_to_tools_id)->where('user_type', $request->usertype)->update(['avg_ans_point' => $subordinates_avg_point, 'status' => 'complete']);
                    }

                    if ($request->usertype == "U") {
                        Tools360EvaluationUrlShare::where('user_to_tools_id', $request->user_to_tools_id)->where('user_type', $request->usertype)->update(['avg_ans_point' => $user_avg_point, 'status' => 'complete']);
                    }

                    //update avg anser point in Tools360EvaluationUrlShare table end

                    //get avg point to upadte user to tools table start

                    $evaluation_urlshare_point = Tools360EvaluationUrlShare::where('user_to_tools_id', $request->user_to_tools_id)->get();

                    $urlshare_point_avg_point  = $user_answer_point->avg('avg_ans_point');

                    UserToTools::where('id', $request->user_to_tools_id)->update(['evaluations_avg_point' => $urlshare_point_avg_point]);

                    //get avg point to upadte user to tools table end


                    session()->flash('success', \Lang::get('client_site.score_submited_successfully'));
                    return redirect()->back();
                } else {

                    session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
                    return redirect()->back();
                    //return redirect()->route('user.view.form');
                }
            } // $data if end
        }
    }

    public function replyComment($id, Request $request)
    {
        // $userReview = Review::where('professional_id', $user->id)->with('reviewedUserName')->orderBy('id', 'DESC')->get();
        $review = Review::where('id', $id)->first();
        if(@$review){
            $review->reply = $request->reply;
            $review->save();
            return redirect()->back()->with('success', \Lang::get('client_site.posted_succesfully'));
        }
        return redirect()->back()->with('error', \Lang::get('client_site.not_found'));
    }

    /**
     *   Method      : copyMonthSchedule
     *   Description : This is used to copy month schedule of professional
     *   Author      : Sayantani
     *   Date        : 2021-JUNE-22
     **/
    public function copyMonthSchedule($query_date, $copy_date)
    {
        $user = User::find(19);
        $first_date = date('Y-m-01', strtotime($query_date));
        $last_date = date('Y-m-t', strtotime($query_date));

        $first_copy_date = date('Y-m-01', strtotime($copy_date));
        $last_copy_date = date('Y-m-t', strtotime($copy_date));
        // dd(['last_date' => date('d', strtotime($last_date)), 'last_copy_date' => date('d', strtotime($last_copy_date))]);
        $av = UserAvailability::where('user_id', $user->id)
                                ->where('slot_start', '>', now(env('TIMEZONE'))->addHour('3')->toDateTimeString())
                                ->whereNotIn('slot_start', getUsedTimeSlotsForAllDates($user->id))
                                ->where('date', '>=', $first_date)->where('date', '<=', $last_date)->get();
        // dd($av);
        $flag = 0; $array = [];
        if(date('d', strtotime($last_date)) == 31 && date('d', strtotime($last_copy_date)) == 30){
            $flag = 1;
        }
        foreach ($av as $a) {
            if($flag == 1){
                if(date('d', strtotime($a->date)) != 31){
                    $create['date'] = date('Y-09-d', strtotime($a->date));
                    $create['slot_start'] = date('Y-09-d H:i:s', strtotime($a->slot_start));
                    $create['slot_end'] = date('Y-09-d H:i:s', strtotime($a->slot_end));
                    $create['user_id'] = $a->user_id;
                    $create['time_interval'] = $a->time_interval;
                    $create['from_time'] = $a->from_time;
                    $create['to_time'] = $a->to_time;
                    $avll = UserAvailability::create($create);
                    array_push($array, $avll);
                }
            } else {
                $create['date'] = date('Y-09-d', strtotime($a->date));
                $create['slot_start'] = date('Y-09-d H:i:s', strtotime($a->slot_start));
                $create['slot_end'] = date('Y-09-d H:i:s', strtotime($a->slot_end));
                $create['user_id'] = $a->user_id;
                $create['time_interval'] = $a->time_interval;
                $create['from_time'] = $a->from_time;
                $create['to_time'] = $a->to_time;
                $avll = UserAvailability::create($create);
                array_push($array, $avll);
            }
        }
        dd($array);
    }

    /**
     *   Method      : copyWeekSchedule
     *   Description : This is used to copy week schedule of professional
     *   Author      : Sayantani
     *   Date        : 2021-JUNE-22
     **/
    public function copyWeekSchedule($query_date, $copy_date){
        $query_date = new DateTime($query_date);
        $copy_date = new DateTime($copy_date);
        $query_week = $query_date->format("W");
        $copy_week = $copy_date->format("W");
        // echo "Weeknummer: $copy_week";
        $year = 2021;

        // $timestamp_for_sunday = $timestamp - 86400 * ( date( 'N', $timestamp ) );
        // $date_for_sunday = date( 'Y-m-d', $timestamp_for_sunday );
        // dd($date_for_sunday);

        $timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $query_week * 7 * 24 * 60 * 60 );
        $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
        $first_date = date( 'Y-m-d', $timestamp_for_monday );
        $last_date = date( 'Y-m-d', strtotime("+7 day", $timestamp_for_monday) );

        $timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $copy_week * 7 * 24 * 60 * 60 );
        $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
        $copy_first_date = date( 'Y-m-d', $timestamp_for_monday );
        $copy_last_date = date( 'Y-m-d', strtotime("+7 day", $timestamp_for_monday) );

        // dd(['copy_first_date' => $copy_first_date, 'copy_last_date' => $copy_last_date]);

        // $av = UserAvailability::where('user_id', $user->id)
        //                         ->where('slot_start', '>', now(env('TIMEZONE'))->addHour('3')->toDateTimeString())
        //                         ->whereNotIn('slot_start', getUsedTimeSlotsForAllDates($user->id))
        //                         ->where('date', '>=', $first_date)->where('date', '<=', $last_date)->get();

        // foreach ($av as $a) {
        //     $create['date'] = date('Y-09-d', strtotime($a->date));
        //     $create['slot_start'] = date('Y-09-d H:i:s', strtotime($a->slot_start));
        //     $create['slot_end'] = date('Y-09-d H:i:s', strtotime($a->slot_end));
        //     $create['user_id'] = $a->user_id;
        //     $create['time_interval'] = $a->time_interval;
        //     $create['from_time'] = $a->from_time;
        //     $create['to_time'] = $a->to_time;
        //     $avll = UserAvailability::create($create);
        //     array_push($array, $avll);
        // }

        $loop_date = strtotime($first_date);
        $last = strtotime($last_date);
        $dates = $array = []; $c = 0;
        while( $loop_date <= $last ) {
            array_push($dates, date( 'Y-m-d', $loop_date ));
            $loop_date = strtotime( '+1 day', $loop_date );
            $c += 1;
            $date = date( 'Y-m-d', strtotime("+".$c." day", strtotime($copy_first_date)) );
            $av = UserAvailability::where('user_id', $user->id)
                                    ->where('slot_start', '>', now(env('TIMEZONE'))->addHour('3')->toDateTimeString())
                                    ->whereNotIn('slot_start', getUsedTimeSlotsForAllDates($user->id))
                                    ->where('date', $loop_date)->get();
            foreach ($av as $a) {
                $create['date'] = $date;
                $create['slot_start'] = $date . " " . date('H:i:s', strtotime($a->slot_start));
                $create['slot_end'] = $date . " " . date('H:i:s', strtotime($a->slot_end));
                $create['user_id'] = $a->user_id;
                $create['time_interval'] = $a->time_interval;
                $create['from_time'] = $a->from_time;
                $create['to_time'] = $a->to_time;
                $avll = UserAvailability::create($create);
                array_push($array, $avll);
            }
        }
        dd($array);
    }

}
