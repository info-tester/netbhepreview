<?php

namespace App\Http\Controllers\Modules\Professional;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\MessageDetail;
use App\User;
use Auth;
class ProfessionalMessageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    /*
		Method:index
		author:Abhisek
		Purpose:For loading receive and sended message
    */

    public function index($token = null){

    	$sender = MessageDetail::where('sender_id', Auth::id())
    				->where('is_new_send', 'Y')
    				->with('senderDetails')/*
                    ->where('is_trashed', 'N')*/
    				->groupBy('token_no')
    				->orderBy('id', 'DESC')
    				->get();

    	$receiver = MessageDetail::where('receiver_id', Auth::id())
    				->where('is_new_send', 'Y')
                    ->where('is_trashed', 'N')
                    ->where('trashed_by','!=', @Auth::id())
    				->with('receiverDetails')
    				->groupBy('token_no')
    				->orderBy('id', 'DESC')
    				->get();

		return view('modules.professional.message')->with([
			'receiver'	=> @$receiver,
			'sender'	=> @$sender
		]);
    }

    /*
		Method:compose
		author:Abhisek
		Purpose:For loading compose message
    */


    public function compose($id = null){
    	$recipents = Booking::where('professional_id', Auth::id())
    						->with('userDetails')
    						->groupBy('user_id')
    						->where('order_status', 'A')
                            ->get();
        $userlist = Booking::where('user_id', Auth::id())
        ->with('profDetails')
        ->groupBy('professional_id')
        ->where('order_status', 'A')
        ->get();
        $allReceiver = MessageDetail::where('sender_id', Auth::id())
        ->with('senderDetails')->groupBy('receiver_id')->get();
        // return $allReceiver;
        $data=[];
        foreach ($allReceiver as $receiver){
            $bookingAvailable = Booking::where('token_no', $receiver->token_no)->first();
            if(@$bookingAvailable==null){
                $data[] = $receiver;
            }
        }
        // return $data;
        // return $allReceiver;
        $user = User::where('id',@$id)->first();
    	// dd($recipents);
		return view('modules.professional.compose_message_new')->with([
			'recipents'	=> @$recipents,
            'uId'        =>  @$id,
            'user'      =>@$user,
            'userlist'  => @$userlist,
            'allReceiver'  => @$data
		]);
    }

    /*
		Method:insertMsg
		author:Abhisek
		Purpose:For inserting messages from compose message page using XHR.
    */

    public function insertMsg(Request $request){
    	$response = [
    		'jsonrpc'	=>	'2.0'
    	];

    	if($request->params['res']  && $request->params['msg']){
    		$recipents = Booking::Where('professional_id', Auth::id())
    						->Where('user_id', $request->params['res'])
    						->where('order_status', 'A')
    						->first();

    		if($recipents==null){
    			$recipents = Booking::Where('professional_id', $request->params['res'])
    						->Where('user_id', Auth::id())
    						->where('order_status', 'A')
    						->first();
            }

            if($recipents == null){

                $is_created = MessageDetail::create([
                    'sender_id'        =>    Auth::id(),
                    'receiver_id'    =>    $request->params['res'],
                    'msg'            =>    $request->params['msg'],
                    'subject'        =>    @$request->params['sub'],
                    'is_new_send'    =>    'Y',
                ]);
                MessageDetail::where('id', $is_created->id)->update(['token_no'=> $is_created->id]);
                if ($is_created) {
                    $response['status'] = 1;
                    $response['id'] = @$is_created->id;
                }
                return response()->json($response);
            }

    		$update = MessageDetail::where([
    			'sender_id'		=>	Auth::id(),
				'token_no'		=>	$recipents->token_no
    		])->update([
    			'is_new_send'	=>	'N'
    		]);

    		$is_created = MessageDetail::create([
    			'sender_id'		=>	Auth::id(),
    			'receiver_id'	=>	$request->params['res'],
    			'msg'			=>	$request->params['msg'],
    			'subject'		=>	@$request->params['sub'],
    			'is_new_send'	=>	'Y',
    			'token_no'		=>	$recipents->token_no
    		]);

    		if($is_created){
    			$response['status']=1;
                $response['id']=@$is_created->id;
    		}
    		return response()->json($response);
    	}
    }


    /*
        Method:insertReplyMsg
        author:Abhisek
        Purpose:For inserting messages from compose message page using XHR.
    */

    public function insertReplyMsg(Request $request){
        $response = [
            'jsonrpc'   =>  '2.0'
        ];

        if($request->params['res']  && $request->params['msg']){
            $update = MessageDetail::where([
                'id'            =>  $request->params['conv_id']
            ])->update([
                'is_reply'      =>  $request->params['msg']
            ]);

            if($update){
                $response['status']=1;
                $response['id']=$request->conv_id;
            }
            return response()->json($response);
        }
    }

    /*
        Method:insertAtch
        author:Abhisek
        Purpose:For inserting messages from compose message page using XHR.
    */

    public function insertAtch(Request $request){
        $response = [
            'jsonrpc'   =>  '2.0'
        ];

        if($request->conv_id  && $request->chat_attachment){
            $path = "storage/app/public/uploads/chat_attachment/";
            $name = str_random(8).'.'.$request->chat_attachment->getClientOriginalExtension();
            $request->chat_attachment->move($path, $name);
            if($request->type=="M"){
                $update = MessageDetail::where([
                    'id'   =>  $request->conv_id
                ])->update([
                    'attachment'     =>  $name
                ]);
            }
            if($request->type=="R"){
                $update = MessageDetail::where([
                    'id'   =>  $request->conv_id
                ])->update([
                    'is_reply_attachment'     =>  $name
                ]);
            }
            if($update){
                $response['status']=1;
            }
            return response()->json($response);
        }
    }

    /*
		Method:chatDetails
		author:Abhisek
		Purpose:For view chat details.
    */

     public function viewMessage($token = null){
     	$recipents = MessageDetail::where('token_no', $token)
    						->first();

    	if($recipents->sender_id == Auth::id() || $recipents->receiver_id == Auth::id()){
            $messages = MessageDetail::where('token_no', $recipents->token_no)
                ->with('senderDetails', 'receiverDetails')
                // ->where('is_trashed', 'N')
                ->get();
            return view('modules.professional.message_details')->with('msg', @$messages);
    	}
    	else{

            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
    	}
     }

     /*
        Method:chatDetails
        author:Abhisek
        Purpose:For view chat details.
    */

     public function deleteMessage($token = null){
        $recipents = MessageDetail::where('token_no', $token)
                            ->first();

        if($recipents==null){
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        else{
            $messages = MessageDetail::where('token_no', $recipents->token_no)
                    ->update([
                        'is_trashed'    =>  'Y',
                        'trashed_by'    =>  @Auth::id()
                    ]);
            session()->flash('success', \Lang::get('client_site.conversation_successfully_removed'));
            return redirect()->back();
        }
    }

    public function reply($id, Request $request){
        $msg = MessageDetail::with('receiverDetails')->where('id', $id)->first();
        if($msg==null){
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        return view('modules.professional.reply')->with([
            'msg' => @$msg
        ]);
    }
}
