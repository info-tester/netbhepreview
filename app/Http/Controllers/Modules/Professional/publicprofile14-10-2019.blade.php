@extends('layouts.app')
@section('title', @$user->name.' public profile')
@section('style')

	@include('includes.style')
    <link href='{{ URL::to('public/frontend/packages/core/main.css') }}' rel='stylesheet' />
    <link href='{{ URL::to('public/frontend/packages/daygrid/main.css') }}' rel='stylesheet' />
    <link href='{{ URL::to('public/frontend/packages/timegrid/main.css') }}' rel='stylesheet' />
    <link href='{{ URL::to('public/frontend/packages/list/main.css') }}' rel='stylesheet' />
@endsection

@section('scripts')
	@include('includes.scripts')
<script src='{{ URL::to('public/frontend/packages/core/main.js') }}'></script>
<script src='{{ URL::to('public/frontend/packages/interaction/main.js') }}'></script>
<script src='{{ URL::to('public/frontend/packages/daygrid/main.js') }}'></script>
<script src='{{ URL::to('public/frontend/packages/timegrid/main.js') }}'></script>
<script src='{{ URL::to('public/frontend/packages/list/main.js') }}'></script>

<script src="{{ URL::to('public/frontend/js/moment.min.js') }}"></script>
<style type="text/css">
    .fc-available {
        background: #4f4f4e !important;
        color: #fff !important;
    }
</style>
    <script>
$(document).ready(function() {
    $("#owl-demo-1").owlCarousel({
        margin: 25,
        nav: true,
        loop: true,
        responsive: {
          0: {
            items: 1
          },
          768: {
            items:2
          },
          1078: {
            items:3
          },
          1200: {
            items:4
          },
        }
      });
    })
</script> 
 
<script>
    var availiabilityDays = <?php echo $avlDay; ?>;
    var availiabilityTimes = <?php echo $avlTime; ?>;
  document.addEventListener('DOMContentLoaded', function() {
    var Calendar = FullCalendar.Calendar;
    var Draggable = FullCalendarInteraction.Draggable
   var newEvents = []; 
   var globalEvent = []; 

    var calendarEl = document.getElementById('calendar');
    var calendar;
    var i = 1;
    var j = 1;
    var event = [];
    var ardt = [];

    calendar = new Calendar(calendarEl, {
      plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list' ],
      header: {
        left: 'prev,next today',
        center: 'title',
        right: ''
      },
      eventClick: function(info) {
        //console.log(info)
        alert(1)
        // checkDate(info.startStr);
      },
      dayRender: function(event) {
        globalEvent = event;
        const day = moment(event.date).weekday() + 1;
        if ($.inArray(day, availiabilityDays) > -1) {
            var time = availiabilityTimes[day];
            
            $.each(time, function(ind, val) {
                $(event.el).append('<label onclick="callMe();" style="position:relative;">' + val + '</label>');
                $('.fc-day-top').addClass('avl-date');
                $(event.el).css('background-color', '#60F76F');
            });

            $.each(time, function(ind, val) {
                newEvents.push({ 
                    "id" : j,
                    "title": val,
                    "start": new Date()
                });
                j++;
            });
            console.log(newEvents);
        }
        else{
           
           $(event.el).css('background-color', 'rgb(236, 205, 205)');
        }
      },
      slotDuration: '01:00:00',
      slotLabelInterval: '00:30:00',
      eventSources: [event],
      selectable: true,
      events:newEvents,
      select: function(info) {
        //console.log(info)
        checkDate(info.startStr);        
      },
      click: function(info) {
        //console.log(info)
        checkDate(info.startStr);        
      },
      eventRender: function(info) {
        
      },
    });
    
    calendar.render();
  });
  function callMe(){
    alert(1);
  }

</script> 
     
@endsection

@section('header')
	@include('includes.header')
@endsection
@section('content')

    <section class="prfile-bnr-sec">
    <div class="prfil-info">
    
        <div class="container">
        <div class="row rwmrgn" style="display:block;">
        
        <div class="porflabt">
        <div class="prlpic"><img src="{{ @$user->profile_pic ? URL::to('storage/app/public/uploads/profile_pic').'/'.@$user->profile_pic: URL::to('public/frontend/images/no_img.png') }}" class="img-responsive img-fluid"></div>
        
        <div class="prfldtl">
            <h5>{{ @$user->name }}</h5>
            @if(@$user->crp_no!=null || @$user->crp_no!="")
                <p>{{ @$user->crp_no }}</p>
            @endif

                <ul>
                    @if(@$user->total_review>0)
                        @for($i=0;$i<$user->avg_review;$i++)
                            <li><img src="{{ URL::to('public/frontend/images/star.png') }}" alt=""></li>
                        @endfor
                        @for($j=$user->avg_review;$j<5;$j++)
                            <li><img src="{{ URL::to('public/frontend/images/star1.png') }}" alt=""></li>
                        @endfor
                    @else
                        @for($j=0;$j<=4;$j++)
                            <li><img src="{{ URL::to('public/frontend/images/star1.png')}}" alt=""></li>
                        @endfor
                    @endif
                    @if(@$user->total_review>0)
                        <li>({{@$user->total_review}})</li>
                    @else
                        <li>(0)</li>
                    @endif
                </ul>
        </div>
        </div>
        
        <div class="rghtinoo">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                @php
                    $vrr=0; 
                @endphp
                @foreach(@$user->userDetails as $us)
                @if($vrr>=2)
                    @break
                @endif
                @if($us->level==0)
                    <li class="breadcrumb-item"><a href="javascript:void(0);">{{ @$us->categoryName->name }}</a></li>
                    @php
                        $vrr++; 
                    @endphp
                @else
                    <li class="breadcrumb-item active" aria-current="page"><i class="fa fa-angle-right" aria-hidden="true"></i> {{ @$us->categoryName->name }}</li>
                    @php
                        $vrr++; 
                    @endphp
                @endif
                @endforeach
                
                
              </ol>
              
              
              
            </nav>
            
            
            
            <div class="jbinfo">
                <ul>
                    <li>
                        <span>Fees</span>
                        <p>R${{ @$user->rate_price }} / {{ @$user->rate=="M" ? "Minute": "Hour" }}</p>
                    </li>
                    <li>
                        <span>Speaks </span>
                        <p>English, Spanish</p>
                    </li>
                    
                    <li>
                        <span>City</span>
                        <p>Kolkata</p>
                    </li>
                </ul>
            </div>
            
            
            
        </div>
        
        </div>
        </div>
    
    </div>
</section>

<div class="prfltab">    
    <div class="container">
        <div class="row rwmrgn">
            <div class="lfttbvrsn">
                <ul>
                    <li>
                        <a href="javascript:void(0);" id="top">
                            <span><img src="{{URL::to('public/frontend/images/p1.png')}}"></span>
                            Personal Information
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" id="top1">
                            <span><img src="{{URL::to('public/frontend/images/p2.png')}}"></span>
                            Education
                        </a>
                    </li>
                     <li>
                        <a href="javascript:void(0);" id="top2">
                            <span><img src="{{URL::to('public/frontend/images/p3.png')}}"></span>
                            Experience
                        </a>
                    </li>
                    
                         <li>
                            <a href="javascript:void(0);" id="top3">
                                <span><img src="{{URL::to('public/frontend/images/p4.png')}}"></span>
                                Reviews
                            </a>
                        </li>
                    
                     <li>
                        <a href="javascript:void(0);" id="top4">
                            <span><img src="{{URL::to('public/frontend/images/p5.png')}}"></span>
                            Calendar
                        </a>
                    </li>
                </ul>
            </div>
            
            
            <div class="rtsidebtn">
                <ul>
                    <li><a href="#" class="shre"><img src="{{URL::to('public/frontend/images/shre.png')}}"></a></li>
                    <li><a href="javascript:void(0);" class="addfvrt">Add to Favorite</a></li>
                    <!--<li><a href="#">Send a Message</a></li>-->
                    <li><a class="ssnbtnslg" data-slug="{{ @$user->slug }}" href="javascript::void(0);">Booking Request</a></li>
                    
                </ul>
            </div>
            
            
        </div>
    </div>
    
</div>



<div class="pblcprfl-bdy">
    <div class="container">
        <div class="row rwmrgn">
        
            <div class="pbdy-box" id="dwn">
                <h3>About</h3>
                <p class="pra" style="white-space: pre-wrap;">{!! strip_tags(@$user->description, '<br>') !!}</p>
            </div>
            
            <div class="pbdy-box">
                <h3>Speciality</h3>
                <div class="skilsplty">
                    <ul>
                        {{-- {{ dd(@$user) }} --}}
                        @if(sizeof($user->userDetails)>0)
                            @foreach($user->userDetails as $uu)
                                <li>{{ @$uu->categoryName->name }}</li>
                            @endforeach
                        @else
                            <li>Skills Currently not enlisted</li>
                        @endif
                    </ul>
                </div>
            </div>
            
            
                <div class="pbdy-box" id="dwn3">
                    <h3>Reviews</h3>
                    
                    <div class="boxed-list margin-bottom-60">
                        @if(sizeof(@$user->userReview)>0)
                            <ul class="boxed-list-ul">
                                @foreach(@$user->userReview as $ur)
                                    <li>
                                        <div class="boxed-list-item">
                                            <!-- Content -->
                                            <div class="item-content">
                                                <h4>{{@$ur->review_heading}} <span>{{@$ur->reviewedUserName->name}}</span></h4>
                                                
                                                    <div class="item-details margin-top-10">
                                                        <div class="star-rating" data-rating="{{@$ur->points}}">
                                                            @if($ur->points>0)
                                                            
                                                                @for($i=0;$i<$ur->points;$i++)
                                                                    <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>
                                                                @endfor

                                                                @for($j=$ur->points;$j<5;$j++)

                                                                    <span class="star"><i class="fa fa-star-o" aria-hidden="true"></i></span>
                                                                @endfor
                                                            @endif
                                                        @if($ur->points<=0)
                                                            @for($k=0;$k<=4;$k++)
                                                                <span class="star"><i class="fa fa-star-o" aria-hidden="true"></i></span>
                                                            @endfor
                                                        @endif
                                                        
                                                        

                                                        </div>
                                                        <div class="detail-item"><i class="fa fa-calendar-o" aria-hidden="true"></i> {{date('d-m-Y', strtotime(@$ur->created_at))}}</div>
                                                    </div>
                                               
                                                <div class="item-description">
                                                    <p style="white-space: pre-wrap;">{!!strip_tags(@$ur->comments, '<br>')!!}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        @else
                            <div class="eductn-sec">
                                <h5>Review Information Not Found</h5>
                            </div>
                        @endif
                    

                </div>
                    
                    
                </div>
            
            
            
            <div class="pbdy-box" id="dwn1">
                <h3>Education</h3>
                @if(sizeof($user->userQualification)>0)
                @foreach(@$user->userQualification as $us)
                    
                        <div class="eductn-sec">
                            <h5>{{ @$us->degree }}</h5>
                            <li><img src="{{URL::to('public/frontend/images/e1.png')}}"> {{ @$us->university }}</li>
                            <li><img src="{{URL::to('public/frontend/images/e2.png')}}"> {{ @$us->from_month.' '.@$us->from_year }} - {{ @$us->pursuing =="Y" ? 'Till now': @$us->to_month.' '.@$us->to_year }}</li>
                            
                            {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In a dolor the  accumsan massa sed, consectetur turpis. Nunc facilisis, diam et efficitur tristique,blandit  consect etur turpis. Nunc facilisis, diam et efficitur tristique,blandit </p> --}}
                        
                        </div>
                   
                @endforeach
                 @else
                        <div class="eductn-sec">
                            <h5>Information Not Found</h5>
                        </div>
                    @endif
                
            
            </div>
            
            <div class="pbdy-box" id="dwn2">
                <h3>Experience - {{ @$user->experience  ? @$user->experience." Month(s)": "Not Found"}} </h3>
                @if(sizeof(@$user->userExperience)>0)
                    @foreach(@$user->userExperience as $us)
                    <div class="eductn-sec">
                        <h5>{{ @$us->role }}</h5>
                        <li><img src="{{URL::to('public/frontend/images/e1.png')}}"> {{ @$us->organization }}</li>
                        <li><img src="{{URL::to('public/frontend/images/e2.png')}}"> {{ @$us->from_month.' '.@$us->from_year }} - {{ @$us->pursuing =="Y" ? 'Till now': @$us->to_month.' '.@$us->to_year }}</li>
                        
                        <p>{!! @$us->description !!}</p>
                    </div>
                    @endforeach
                @else
                    <div class="eductn-sec">
                        <h5>Information Not Found</h5>
                    </div>
                @endif
            
            </div>
            
            
            
            <div class="pbdy-box" id="dwn4">
                <h3>Calendar</h3>
                
                <div class="right_dash" style="width:100%;">
            <div class="main-form">
            
                <div class="avll">
                    <p><img src="{{URL::to('public/frontend/images/avl.png')}}"> Available</p>
                    <p><img src="{{URL::to('public/frontend/images/unvl.png')}}"> Not Available</p>
                </div>
                
            <div id='wrap'>


            <div id='calendar'></div>
        
            <div style='clear:both'></div>
        
          </div>    
            
                
            </div>
        </div>
                
                
            </div>
        
        
        </div>
    </div>
</div>


<section class="guidance-area" style="border-bottom:1px solid #ccc;">
    <div class="container">
        <div class="row">
            <div class="page-h2">
                <h2>View Other Professionals</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vitae pharetra erat. Fusce quis suscipit leo. Nulla scelerisque erat in dolor laoreet fermentum.<br> Nam at efficitur tortor, vitae porttitor mi. Morbi sit amet velit nec leo imperdiet scelerisque.</p>
            </div>
            <div class="all-guidance">
                <div id="owl-demo-1" class="owl-carousel owl-theme">
                @foreach(@$simm as $us)
                <div class="item">
                  <div class="guiede-box">
                        <div class="guide-image"><img src="{{ URL::to('storage/app/public/uploads/profile_pic/'.$us->profile_pic) }}" alt=""></div>
                        <div class="guide-dtls">
                            <div class="guide-intro">
                                <h3>
                                    {{ @$us->name }}
                                      
                                    <ul>
                                        <li><img src="{{ URL::to('public/frontend/images/star.png') }}" alt=""></li>
                                        <li><img src="{{ URL::to('public/frontend/images/star.png') }}" alt=""></li>
                                        <li><img src="{{ URL::to('public/frontend/images/star.png') }}" alt=""></li>
                                        <li><img src="{{ URL::to('public/frontend/images/star.png') }}" alt=""></li>
                                        <li><img src="{{ URL::to('public/frontend/images/star1.png') }}" alt=""></li>
                                        <li>(35)</li>
                                    </ul>
                                    
                                </h3>
                                @if(@Auth::guard('web')->user()->crp_no==null || @Auth::guard('web')->user()->crp_no=="")
                                <p>{{@Auth::guard('web')->user()->crp_no  }}</p>
                                @endif
                            </div>
                            <div class="guide-more">
                                <ul>
                                    <li>
                                        <span>Feees</span>
                                        <p>${{ @$us->rate_price }} / Hour</p>
                                    </li>
                                    <li>
                                        <span>Speaks </span>
                                        <p>English, Spanish</p>
                                    </li>
                                </ul>
                                <a class="guide-btn pull-left" href="#">Request A Session </a>
                                <a class="guide-btn pull-right" href="{{ route('pblc.pst',['slug'=>@$us->slug]) }}">View Profile </a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
               
              </div>
            </div>
        </div>
    </div>
</section>
<!-- Return to Top -->
<a href="javascript:" id="return-to-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
@endsection
@section('footer')
	@include('includes.footer')
    <style type="text/css">
        #return-to-top i {
    color: #fff;
    margin: 0;
    position: relative;
    font-size: 19px;
    line-height: 40px;
    -webkit-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    -ms-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    transition: all 0.3s ease;
}
#return-to-top {

    position: fixed;
    bottom: 80px;
    right: 4%;
    background: #0095da;
    width: 40px;
    height: 40px;
    display: block;
    text-decoration: none;
    display: none;
    -webkit-transition: all 0.3s linear;
    -moz-transition: all 0.3s ease;
    -ms-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    transition: all 0.3s ease;
    text-align: center;
    z-index: 9999999;

}
    </style>
    <script src="{{ URL::to('public/frontend/js/jquery.com_ui_1.11.4_jquery-ui.js') }}"></script>
    <script>
        $(window).scroll(function(){

                if ($(this).scrollTop() > 120) {
                //alert('hi');  
                    
                $('.top_head').css('background','#000');
               
                } 
                else
                {
                     $('.top_head').css('background','rgba(0,0,0,0.4)');
                }
            });
    </script>
    <script>
            // ===== Scroll to Top ==== 
        $(window).scroll(function() {
            if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
                $('#return-to-top').fadeIn(200);    // Fade in the arrow
            } else {
                $('#return-to-top').fadeOut(200);   // Else fade out the arrow
            }
        });
        $('#return-to-top').click(function() {      // When arrow is clicked
            $('body,html').animate({
                scrollTop : 0                       // Scroll to top of body
            }, 500);
        });
    </script> 
<script>
    $(function() {
        $( "#slider-range" ).slider({
            range: true,
            min: 0,
            max: 1000,
            values: [ 75, 300 ],
            slide: function( event, ui ) {
                $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $." + ui.values[ 1 ] );
            }
        });
        $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
            " - $" + $( "#slider-range" ).slider( "values", 1 ) );
    });
</script> 

<script>
    $("#top").click(function() {
      $("html, body").animate({ scrollTop: $('#dwn').offset().top }, "slow");
      return false;
    });

    $("#top1").click(function() {
      $("html, body").animate({ scrollTop: $('#dwn1').offset().top }, "slow");
      return false;
    });

    $("#top2").click(function() {
      $("html, body").animate({ scrollTop: $('#dwn2').offset().top }, "slow");
      return false;
    });

    $("#top3").click(function() {
      $("html, body").animate({ scrollTop: $('#dwn3').offset().top }, "slow");
      return false;
    });

    $("#top4").click(function() {
      $("html, body").animate({ scrollTop: $('#dwn4').offset().top }, "slow");
      return false;
    });
</script>
<script>
    $('.addfvrt').click(function(){
        var reqData = {
          'jsonrpc' : '2.0',
          '_token' : '{{csrf_token()}}',
          'params' : {
                'id' : {{ @$user->id }}
            }
        };
        $.ajax({
            url: "{{ route('add.favourite') }}",
            method: 'post',
            dataType: 'json',
            data: reqData,
            success: function(response){
                if(response.status==1) {
                    toastr.success('Successfully added to favourite list');
                }
                if(response.status==3){
                    localStorage.removeItem('slug');
                    localStorage.setItem('slug','{{ @$user->slug }}');
                    location.href = "{{ route('login') }}";
                }

                if(response.status==2){
                    toastr.info('You have been allready added {{ @$user->name }} into your favourite list');
                } 
            }, error: function(error) {
                console.error(error);
            }
        });
    });
    function checkDate(date){
    
        var varDate = new Date(date); //dd-mm-YYYY
        var today = new Date();
        x = today;
        today.setHours(0,0,0,0);
        if(varDate >= today) {
            var reqData = {
              'jsonrpc' : '2.0',
              '_token' : '{{csrf_token()}}',
              'params' : {
                    'date' : date,
                    'id' : {{ @$user->id }}
                }
            };
            $.ajax({
                url: "{{ route('set.date') }}",
                method: 'post',
                dataType: 'json',
                data: reqData,
                success: function(response){
                    if(response.status==1){
                        localStorage.removeItem('bookSlug');
                        localStorage.setItem('bookSlug', "{{ @$user->slug }}");
                        location.href="{{ route('login') }}";
                    }
                    if(response.status==0){
                        swal("Currently time slot is not avaliable for this day.",{icon:"info"});
                    } 
                }, error: function(error) {
                    swal("internal server error.",{icon:"warning"});
                }
            });
        }
        else{
            swal('Please selct a date greater tahn or equal to : '+x,{icon:"error"});
        }
    }
    $('.ssnbtnslg').click(function(){
        if($(this).data('slug')!=""){
            localStorage.removeItem('bookSlug');
            localStorage.setItem('bookSlug', $(this).data('slug'));
            location.href="{{ route('login') }}"
        }
    });
</script>
@endsection