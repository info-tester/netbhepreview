<?php

namespace App\Http\Controllers\Modules\Professional;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Mail;
use App\Mail\ProductFileUpload;
use App\Mail\ProductAllowCertifiacte;
use App\Models\Cart;
use App\Models\ProductSubcategory;
use App\Models\ProductCategory;
use App\Models\ProductOrder;
use App\Models\ProductOrderDetails;
use App\Models\Product;
use App\Models\ProductToFile;
use App\Models\OrderFile;
use App\Models\Coupon;
use App\Models\CouponToProduct;
use App\Models\Chapter;
use App\Models\Lesson;
use App\Models\LessonFile;
use App\Models\Slide;
use App\Models\Quiz;
use App\Models\Wishlist;
use App\Models\Content;
use App\Models\QuizProgress;
use App\Models\CourseProgress;
use App\Models\Payment;

use App\Models\CertificateTemplateMaster;
use App\Models\ReviewProduct;
use App\Models\ProductToCertificate;
use App\Models\LessonQuestion;
use App\Models\LessonAnswer;
use App\Models\AffiliateProducts;

use App\User;
use Carbon\Carbon;
use Response;
use Storage;
use Image;
use File;
use DB;
use Auth;
use Session;
use Illuminate\Support\Facades\Blade;
use Imagick;
use Thumbnail;
use Vimeo\Vimeo;

class ProfessionalProductsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('search', 'productDetails', 'categorySearch','subCategorySearch','getVideo','addWishlist','productLanding','checkCookie','searchReview','searchCatSubcat','viewFreeLessons','findChapters');
    }

    /*
    * Method: index
    * Description: for loading Products
    * Author: Sayantani
    * Date: 15-DEC-2020
    */
    public function index(Request $request){
        $products = Product::with('category')->where('status', '!=', 'D')->where('professional_id', Auth::user()->id)->orderBy('created_at', 'DESC');
        $data['category'] = ProductCategory::where('status', '!=', 'D')->get();
        if(@$request->all()){
            if(@$request->keyword){
                $products = $products->where('title', 'like','%'.$request->keyword.'%')->orWhere('description', 'like','%'.$request->keyword.'%');
            }
            if(@$request->category){
                $products = $products->where('category_id',$request->category);
            }
            if(@$request->status){
                $products = $products->where('status',$request->status)->orWhere('admin_status',$request->status);
            }
            $data['key'] = $request->all();
        }
        $data['products'] = $products->paginate(10);
        // $msg = "Test Trial Sayantani Message";
        // $mobile = "+918017096564";
        // $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
        // dd($result);

        // if($result['statusCode'] == 200){
        //     dd("Success");
        // } else {
        //     dd($result);
        // }
        return view('modules.products.professional_products')->with($data);
    }

    /*
    * Method: store
    * Description: for add/update Products
    * Author: Sayantani
    * Date: 12-DEC-2020
    */
    public function store(Request $request, $id=null){
        $data['categories'] = ProductCategory::where('status', '!=', 'D')->get();
        $data['subcategories'] = null;
        if(@$request->all()){
            $create['professional_id'] = Auth::user()->id;
            $create['category_id'] = @$request->category;
            $create['subcategory_id'] = @$request->subcategory;
            $create['title'] = @$request->title;
            $create['price'] = @$request->price;
            $create['discounted_price'] = @$request->discounted_price ? @$request->discounted_price : 0.00;
            $create['content_available_days'] = @$request->content_available;
            $create['cancel_day'] = @$request->cancel_day;
            $create['purchase_start_date'] = @$request->purchase_start_date ? date('Y-m-d', strtotime(@$request->purchase_start_date)) : NULL;
            $create['purchase_end_date'] = @$request->purchase_end_date ? date('Y-m-d', strtotime(@$request->purchase_end_date)) : NULL;
            $create['course_start_date'] = @$request->course_start_date ? date('Y-m-d', strtotime(@$request->course_start_date)) : NULL;
            $create['affiliate_program'] = @$request->affiliate_program;
            $create['affiliate_percentage'] = @$request->affiliate_commission;
            $create['affiliate_text'] = @$request->affiliate_text;
            $create['admin_description'] = @$request->desc;
            $create['description'] = @$request->intro;
            $create['how_it_works'] = @$request->how_it_works;
            $create['can_help'] = @$request->can_help;
            $create['for_whom'] = @$request->for_whom;
            $create['benefits'] = @$request->benefits;
            $create['when_to_do'] = @$request->when_to_do;
            $create['previous_preparation'] = @$request->previous_preparation;
            $create['no_of_installment'] = @$request->no_of_installment;
            $create['installment_charge'] = @$request->installment_charge;
            $create['status'] = 'I';
            $create['sale_type'] = @$request->set_date_radio;

            if(@$request->hasFile('cover_image')){
                @unlink(storage_path('app/public/uploads/product_cover_images/'.@$old->cover_image));
                $cover_image_name = time().'-'.rand(1000,9999).'.'.@$request->cover_image->getClientOriginalExtension();
                $cover_image = @$request->cover_image;
                Storage::putFileAs('public/uploads/product_cover_images', $cover_image, $cover_image_name);
                $create['cover_image'] = $cover_image_name;
            }

            if(@$request->intro_video){
                $url = @$request->intro_video;
                parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
                $url_id = @$my_array_of_vars['v'];
                if($url_id == null){
                    $pieces = explode("/", $url);
                    $url_id = end($pieces);
                    if($url_id == null){
                        session()->flash('error', \Lang::get('client_site.wrong_url'));
                        return redirect()->back()->with($data);
                    }
                }
                $create['intro_video'] = $url_id;
            }
            if(@$request->ID){
                $product = Product::find($request->ID);
                $product->update($create);
                session()->flash('success', \Lang::get('client_site.product_updated_succesfully'));
            } else {
                $product = Product::create($create);
                $product->update(['slug' => str_slug($request->title . '-' . $product->id)]);

                session()->flash('success', \Lang::get('client_site.product_created_succesfully'));
            }
            return $product->id;
        } else {
            if(@$id){
                $product = Product::find($id);
                if(@$product){
                    $data['product'] = $product;
                    $data['subcategories'] = ProductSubcategory::where('category_id', $product->category_id)->where('status', '!=', 'D')->get();
                    return view('modules.products.edit_product')->with($data);
                }
            } else {
                return view('modules.products.add_product')->with($data);
            }
        }
    }

    /*
    * Method: productDetails
    * Description: for deleting Product
    * Author: Sayantani
    * Date: 14-DEC-2020
    */
    public function productDetails($slug, $affcode = null){
        $total_lessons = 0;

        $data['product'] = Product::with('category', 'professional','subCategory')->where('slug',$slug)->whereIn('status', ['A','I'])->first();
        if(!@$data['product']){
            session()->flash('error', \Lang::get('client_site.product_not_found'));
            return redirect()->back();
        }
        $auth_id = 0;
        $ordered_products = [];
        if(Auth::check()){
            $ordered_products = ProductOrderDetails::whereHas('orderMaster', function($q){
                $q->where('user_id', Auth::id())->where('payment_status','P');
            })->where('order_status','A')->pluck('product_id')->toArray();
            $auth_id = Auth::id();
        }

        if($data['product']->status == 'A' || in_array($data['product']->id, $ordered_products) || $data['product']->professional_id == @$auth_id){

            $data['has_certificate'] = ProductToCertificate::where('product_id', @$data['product']->id)->first();

            if(@$data['product']->admin_status != 'A'){
                if(Auth::id() != @$data['product']->professional_id){
                    session()->flash('error', \Lang::get('client_site.product_not_found'));
                    return redirect()->back();
                }
            }

            $token_no = ProductOrderDetails::with('orderMaster')->where('product_id', $data['product']->id)->whereHas('orderMaster', function($q){
                    $q->where('user_id', Auth::id());
                })->orderBy('id','desc')->first();


            $data['is_purchased'] = Payment::where('token_no',@$token_no->orderMaster->token_no)->where('payment_status','P')->where('user_id',@Auth::user()->id)->first();
            $levels = $noOfComplete = $percentage = 0;
            // $chapters = Chapter::where('course_id', $data['product']->id)->with('getLessons')->get();
            $data['chapters'] = Chapter::with('getLessons','getVideoLessons','getVideoLessons.getLessonFile')->where('course_id', @$data['product']->id)->where('set_lessons_status','P')->get();

            foreach($data['chapters'] as $i=>$chapter){
                $levels += $chapter->getLessons->count();

                // if(!@$first_free_video_lesson) {
                //     $first_free_video_lesson = Lesson::where('chapter_id',$chapter->id)->where('lesson_type','V')->where('is_free_preview','Y')->where('status','A')->first();
                // }
            }
            $chapterIDS = $data['chapters']->pluck('id')->toArray();
            // $first_free_video_lesson = Lesson::whereIn('chapter_id',$chapterIDS)->where('lesson_type','V')->where('is_free_preview','Y')->where('status','A')->first();
            $first_free_video_lesson = Lesson::whereIn('chapter_id',$chapterIDS)->where('lesson_type','V')->where('is_main_preview_video','Y')->where('status','A')->first();
            // dd($first_free_video_lesson);
            if(@$first_free_video_lesson){
                $data['my_lesson'] = $first_free_video_lesson;
                $data['lesson_video'] = LessonFile::where('lesson_id',@$first_free_video_lesson->id)->where('parent_id', 0)->where(function ($q) {
                            $q->where('filetype', 'Y')->orWhere('filetype', 'V');
                        })->first();

                if(@$data['lesson_video']){
                    $data['lesson_video_thumbnail'] = LessonFile::where('parent_id',@$data['lesson_video']->id)->where('filetype','T')->first();
                    // dd($data['lesson_video_thumbnail']);
                    $data['attachments'] = LessonFile::where('lesson_id',@$first_free_video_lesson->id)->where('filetype', 'D')->where('parent_id', $data['lesson_video']->id)->get();
                }
            }

            $total_lessons = $levels;
            $noOfComplete += CourseProgress::where('user_id', Auth::id())->where('product_id', @$data['product']->id)->where('status', 'C')->count();
            if( $levels > 0 ) $percentage = ceil(( (int)$noOfComplete / (int)$levels ) * 100) > 100 ? 100 : ceil(( (int)$noOfComplete / (int)$levels ) * 100);
            else $percentage = 0;
            $data['completion'] = $percentage;

            $data['professionalProductsCount'] = Product::where('status', '!=', 'D')->where('professional_id', $data['product']->professional->id)->count();

            $prduct_ids = Product::where('status', '!=', 'D')->where('professional_id', $data['product']->professional->id)->get()->pluck('id')->toArray();
            $reviews = ReviewProduct::whereIn('product_id', $prduct_ids)->get()->pluck('rate')->toArray();
            $data['averageRating'] = "0";
            if(count($reviews) > 0) $data['averageRating'] = array_sum($reviews) / count($reviews);

            $data['totalStudents'] = ProductOrderDetails::whereIn('product_id', $prduct_ids)->get()->count();
            $data['totalComments'] = ReviewProduct::whereIn('product_id', $prduct_ids)->where('review', '!=', null)->get()->count();

            if(Auth::user()){
                $data['wishlist'] = Wishlist::where('user_id',Auth::user()->id)->where('product_id',$data['product']->id)->first();
            }
            $data['product']['total_lessons'] = $total_lessons;
            $data['suggested'] = Product::where('category_id', $data['product']->category_id)->where('status', '!=', 'D')->where('admin_status', 'A')->get();
            if(Auth::check()){
                $data['me'] = Auth::user();
                $data['suggested'] = Product::where('category_id', $data['product']->category_id)->where('professional_id', '!=', Auth::id())->where('status', '!=', 'D')->where('admin_status', 'A')->get();
            }
            if (@auth()->user()->id) {
                $allCartData = Cart::with(['product'])->where('user_id', auth()->user()->id)->where('product_id', $data['product']->id)->first();
            } else {
                $cart_id = Session::get('cart_session_id');
                $allCartData = Cart::with(['product'])->where('cart_session_id', $cart_id)->where('product_id',$data['product']->id)->first();
            }
            $data['cartData'] = $allCartData;
            $data['orderedProducts'] = [];
            if(Auth::check()){
                $data['orderedProducts'] = ProductOrderDetails::whereHas('orderMaster', function($q){
                    $q->where('user_id', Auth::id())->whereNotIn('payment_status', ['I','F']);
                })->pluck('product_id')->toArray();
            }

            $data['reviews'] = ReviewProduct::with([
                'getCustomer'=>function($query){
                    $query->select('id','name','profile_pic');
                },])->where('product_id',$data['product']->id)->get();
            $data['prod_avg_rating'] = $data['product']->avg_rating ;


            $data['affcode'] = @$affcode ? @$affcode : null;
            $cookie = []; $obj = (object)[];
            if(@$affcode){
                $aff = AffiliateProducts::where('product_id', $data['product']->id)->where('affiliate_code', $affcode)->first();
                if(@$aff){
                    if((Auth::check() && Auth::id() != $data['product']->professional_id && Auth::id() !== $aff->user_id) || !Auth::check()){
                        $cookie1 = Cookie::get('affiliates');
                        if($cookie1){
                            $cookie = (array) json_decode($cookie1);
                            // dd($cookie);
                            foreach($cookie as $k=>$c){
                                if($c->product_id == $data['product']->id) unset($cookie[$k]);
                                // dd($c);
                            }
                            $cookie = array_values($cookie);
                            $obj->product_id = $data['product']->id;
                            $obj->code = $affcode;
                            array_push($cookie,$obj);
                        } else {
                            $cookie = [];
                            $obj->product_id = $data['product']->id;
                            $obj->code = $affcode;
                            array_push($cookie,$obj);
                        }
                        $cookie = json_encode($cookie);
                        $ck = Cookie::queue(Cookie::forever('affiliates', $cookie));
                    }
                }
            }

            $data['total_orders'] = ProductOrderDetails::where('product_id', $data['product']->id)->count();

            $count_rev = count($data['reviews']);
            if(@$count_rev > 0){

                // $five_star = ReviewProduct::with('getCustomer','getProducts')->where('product_id',$data['product']->id)->where('rate',5)->count();
                // $data['per_five_star'] = (($five_star/@$count_rev)*100);

                // $four_star = ReviewProduct::with('getCustomer','getProducts')->where('product_id',$data['product']->id)->where('rate',4)->count();
                // $data['per_four_star'] = (($four_star/@$count_rev)*100);

                // $three_star = ReviewProduct::with('getCustomer','getProducts')->where('product_id',$data['product']->id)->where('rate',3)->count();
                // $data['per_three_star'] = (($three_star/@$count_rev)*100);

                // $two_star = ReviewProduct::with('getCustomer','getProducts')->where('product_id',$data['product']->id)->where('rate',2)->count();
                // $data['per_two_star'] = (($two_star/@$count_rev)*100);

                // $one_star = ReviewProduct::with('getCustomer','getProducts')->where('product_id',$data['product']->id)->where('rate',1)->count();
                // $data['per_one_star'] = (($one_star/@$count_rev)*100);

            }
        } else {
            session()->flash('error', \Lang::get('client_site.product_not_found'));
            return redirect()->back();
        }

        return view('modules.products.product_details_new')->with($data);
    }

    /*
    * Method: changeProductStatus
    * Description: for changing product status by professional
    * Author: Sayantani
    * Date: 14-DEC-2020
    */
    public function changeProductStatus($id){
        $product = Product::find($id);

        if(@$product){
            if ($product->status == 'A') {
                $product->update(['status' => 'I']);
                session()->flash('success', \Lang::get('client_site.product_inactivated_succesfully'));
            } elseif ($product->status == 'I') {
                $product->update(['status' => 'A']);
                session()->flash('success', \Lang::get('client_site.product_activated_succesfully'));
            }
        } else {
            session()->flash('error', \Lang::get('client_site.product_not_found'));
        }
        return redirect()->back();
    }
    /*
    * Method: deleteProduct
    * Description: for deleting Product
    * Author: Sayantani
    * Date: 12-DEC-2020
    */
    public function deleteProduct($id){
        $product = Product::find($id);
        if(@$product){
            $product->status = 'D';
            $chapters = Chapter::where('course_id',$product->id)->get();
            foreach($chapters as $chapter){
                $this->deleteChapter($chapter->id);
            }
            $product->save();
            session()->flash('success', \Lang::get('client_site.product_deleted_succesfully'));
        } else {
            session()->flash('error', \Lang::get('client_site.product_not_found'));
        }
        return redirect()->back();
    }

    /**
     *method: search
     *created By: Abhisek
     *description: For Search
     */

    public function search(Request $request, $slug = null)
    {
        $product = $maxprice = "";
        $productCategory = ProductCategory::with('getSubCategories')->where('status','!=','D')->get();
        $productSubCategory = ProductSubcategory::where('status','!=','D')->get();
        if(@$slug){
            $category = ProductCategory::with('getSubCategories')->where('status','!=','D')->where('slug',$slug)->first();
            $product =  Product::where(['status' => 'A', 'admin_status' => 'A', 'category_id' => $category->id]);
            $maxprice = Product::select('price')->where(['status'=>'A','admin_status'=>'A', 'category_id' => $category->id])->orderBy('price', 'desc')->first();
        } else {
            $product = Product::where(['status' => 'A', 'admin_status' => 'A']);
            $maxprice = Product::select('price')->where(['status'=>'A','admin_status'=>'A'])->orderBy('price', 'desc')->first();
            // $minprice = Product::select('price')->where(['status' => 'A', 'admin_status' => 'A'])->orderBy('price')->first();
        }

        // if(@$slug){
        //     $subcategory = ProductSubcategory::where('status','!=','D')->where('slug',$slug)->first();
        //     $category = ProductCategory::where('status','!=','D')->where('id',$subcategory->category_id)->first();
        //     $product =  Product::where(['status' => 'A', 'admin_status' => 'A', 'category_id' => $category->id, 'subcategory_id' => $subcategory->id]);
        //     $maxprice = Product::select('price')->where(['status'=>'A','admin_status'=>'A', 'category_id' => $category->id])->orderBy('price', 'desc')->first();
        // } else {
        //     $product = Product::where(['status' => 'A', 'admin_status' => 'A']);
        //     $maxprice = Product::select('price')->where(['status'=>'A','admin_status'=>'A'])->orderBy('price', 'desc')->first();
        //     // $minprice = Product::select('price')->where(['status' => 'A', 'admin_status' => 'A'])->orderBy('price')->first();
        // }
        // dd($product->get());
        $minprice = (object)['price' => '0.00'];
        if (Auth::id()) {
            $product = $product->where('professional_id', '!=', Auth::id());
        }
        if(@$request->all()){
            if(@$request->category){
                $product=$product->where('category_id',$request->category);
            }
            if(@$request->sub_category){
                $product=$product->where('subcategory_id',$request->sub_category);
            }
            if ($request->max_price || $request->min_price) {
                $product = $product->whereBetween('price', [$request->min_price, $request->max_price+1]);
            }
            if ($request->sort_by) {
                $product = $product->orderBy('price', $request->sort_by == "A" ? 'asc' : 'desc');
            }
            if ($request->keyword) {
                $product = $product->where(function ($where) use ($request) {
                    $where->where('title', 'like', '%' . $request->keyword . '%')
                        ->orWhere('description', '%'.$request->keyword.'%');
                });
            }
            $key=$request->all();
        }

        // $cart_products = Cart::with(['product'])->where('user_id', auth()->user()->id)->pluck('product_id')->toArray();
        $ordered_products = [];
        if(Auth::check()){
            $ordered_products = ProductOrderDetails::whereHas('orderMaster', function($q){
                $q->where('user_id', Auth::id())->whereNotIn('payment_status', ['I','F']);
            })->pluck('product_id')->toArray();
        }
        $product =  $product
        ->select(
            'id','admin_status','status','professional_id',
            'show_in_home_page','category_id','title','slug',
            'price','discounted_price','description','cover_image',
            'purchase_start_date','purchase_end_date','course_start_date','subcategory_id'
        )
        ->with(['category','professional:id,name'])->paginate(12);

        return view('modules.products.product_search')->with([
            'allProductCategory' => $productCategory,
            'allProductSubCategory' => $productSubCategory,
            'key'            =>    @$key,
            'minprice'      =>  @$minprice,
            'maxprice'      =>  @$maxprice,
            'allProduct'    => @$product,
            'orderedProducts' => @$ordered_products
        ]);
    }
    /*
    * Method: categorySearch
    * Description: for showing orders on professional's products
    * Author: Sayantani
    * Date: 17-DEC-2020
    */
    public function categorySearch(Request $request)
    {
        $data['categories'] = ProductCategory::with('getSubCategories')->where('status','!=','D')->get();
        return view('modules.products.product_category_search')->with($data);
    }

    /*
    * Method: professionalOrders
    * Description: for showing orders on professional's products
    * Author: Sayantani
    * Date: 17-DEC-2020
    */
    public function professionalOrders(Request $request){
        // $data['orders'] = ProductOrder::with('userDetails', 'profDetails', 'productDetails', 'productCategoryDetails')->whereIn('payment_status',['P','PR','F'])->where('professional_id', Auth::user()->id);

        $data['orders'] = ProductOrderDetails::with('orderMaster','product','profDetails','orderMaster.userDetails','product.assignedTemplate')->where('professional_id',Auth::user()->id);

        // $data['orders'] = ProductOrder::with('orderDetails','userDetails','orderDetails.product')->whereIn('payment_status',['P','PR','F','I'])->whereHas('orderDetails', function($q) {
        //         $q = $q->where('professional_id', Auth::user()->id);
        // });



        if(@$request->all()){
            if(@$request->keyword){
                $data['orders'] = $data['orders']->with('product')->where( function ($where) use ($request){
                         $where->whereHas("product", function ($query) use ($request) {
                                $query->where("title", 'LIKE', '%' . $request->keyword . '%')->orWhere("description", 'LIKE', '%' . $request->keyword . '%');
                    })->orWhereHas("orderMaster.userDetails", function ($query) use ($request) {
                        $query->where("name", 'LIKE', '%' . $request->keyword . '%');
                    });
                    })->orWhereHas("orderMaster", function ($query) use ($request) {
                        $query->where("token_no", 'LIKE', '%' . $request->keyword . '%');
                    });
            }

            if(@$request->status){
                $data['orders'] = $data['orders']->whereHas("orderMaster", function($q) use ($request){
                    $q->where('payment_status', $request->status);
            });
            }

            $data['key'] = $request->all();
        }
        $data['orders'] = $data['orders']->orderBy('created_at', 'desc')->paginate(10);

        // foreach($data['orders'] as $order){
        //     $data['det'] = ProductOrderDetails::with('product')->where('product_order_master_id',$order->id)->whereHas('product', function($q){
        //         $q = $q->where('professional_id', Auth::id());
        //     })->get();
        //     $sum = 0;
        //     foreach($data['det'] as $order_det){
        //         $sum += (int)$order_det->amount;
        //     }
        //     $order['sum'] = $sum;
        // }


        foreach($data['orders'] as $k=>$order_det){
            $arr = (object)[];
            $levels = $noOfComplete = 0;
            $arr = $order_det;
            $chapters = Chapter::where('course_id', $order_det->product->id)->get();
            foreach($chapters as $chapter){
                $levels += Lesson::where('chapter_id', $chapter->id)->where('status', 'A')->count();
            }
            // try{
            //     $noOfComplete += CourseProgress::where('user_id', $order_det->orderMaster->user_id)->where('product_id', @$order_det->product->id)->where('status', 'C')->count();
            //     if( $levels > 0 ) $order_det->percentage = ceil(( (int)$noOfComplete / (int)$levels ) * 100);
            //     else $order_det->percentage = 0;
            // } catch(\Exception $e) {
            //     dd([$order_det, $e]);
            // }

            $noOfComplete = CourseProgress::where('user_id', $order_det->orderMaster->user_id)->where('product_id', @$order_det->product->id)->where('status', 'C')->count();
            if( $levels > 0 ) $order_det->percentage = ceil(( (int)$noOfComplete / (int)$levels ) * 100);
            else $order_det->percentage = 0;

        }
        // $data['category'] = ProductCategory::where('status', '!=', 'D')->get();
        return view('modules.products.professional_orders')->with($data);
    }

    /*
    * Method: viewProfessionalOrder
    * Description: for showing order to professional
    * Author: Sayantani
    * Date: 17-DEC-2020
    */
    public function viewProfessionalOrder($id,$prod_id){
        // $data['order'] = ProductOrder::with('userDetails', 'profDetails', 'productDetails', 'productCategoryDetails')->where('id', $id)->first();

        $data['order'] = ProductOrder::with('userDetails', 'profDetails', 'productDetails', 'productCategoryDetails')->where('id', $id)->first();
        $data['prod_order_det'] = ProductOrderDetails::with('product')->where('product_order_master_id',$id)->where('product_id',$prod_id)->first();
        $data['product_files'] = ProductOrderDetails::with('product')->where('product_order_master_id',$id)->whereHas('product', function($q){
            $q = $q->where('professional_id', Auth::id());
        })->get();
        $data['sum'] = 0;
        foreach($data['product_files'] as $order_det){
            $data['sum'] += (int)$order_det->amount;
        }
        // $data['order_files'] = OrderFile::where('order_id', $id)->where('status','!=','D')->get();
        if(count($data['product_files']) > 0){
            return view('modules.products.professional_view_order')->with($data);
        } else {
            session()->flash('error', \Lang::get('site.visit_not_allowed'));
            return redirect()->back();
        }
    }

    /*
    * Method: uploadProductFiles
    * Description: for uploading file for an order
    * Author: Sayantani
    * Date: 17-DEC-2020
    */
    public function uploadProductFiles(Request $request, $id){
        $order = ProductOrder::with('userDetails', 'profDetails', 'productDetails', 'productCategoryDetails')->where('id', $id)->first();
        $data['order'] = $order;

        $create = array();
        $create['order_id'] = $id;
        $create['caption'] = @$request->caption;
        // dd($request->product_file);
        // @unlink(storage_path('app/public/uploads/product_files/'.@$data['order']->product_file));
        if($request->hasFile('product_file')) {
            $product_file_name = time().'-'.rand(1000,9999).'.'.@$request->product_file->getClientOriginalExtension();
            $product_file = @$request->product_file;
            Storage::putFileAs('public/uploads/product_files', $product_file, $product_file_name);
            $create['file_name'] = $product_file_name;
            $create['file_type'] = 'file';
            $create['mime_type'] = @$request->product_file->getMimeType();
        } elseif(@$request->product_link) {
            $create['file_name'] = $request->product_link;
            $create['file_type'] = 'link';
        }
        $order_file = OrderFile::create($create);

        // for mail
        $userdata = User::where('id', $order->userDetails->id)->first();
        $orderData = $data['order'];
        $productDetails = $data['order']->productDetails;
        $usermail = $order->userDetails->email;

        // Mail::send(new ProductFileUpload($userdata, $orderData, $productDetails, $usermail));

        session()->flash('success', \Lang::get('client_site.files_uploaded_succesfully'));
        return redirect()->back()->with($data);
    }


    /*
    * Method: productFileView
    * Description: for loading Products
    * Author: Soumojit
    * Date: 15-DEC-2020
    */
    public function productFileView($id)
    {
        $product = Product::where('id',$id)->where('status', '!=', 'D')->where('professional_id', Auth::user()->id)->first();
        $fileList = ProductToFile::where('product_id',$id)->where('file_status','A')->get();
        $data['fileList'] = $fileList;
        $data['product'] = $product;
        return view('modules.products.product_file_view')->with($data);
    }
    /*
    * Method: productFileView
    * Description: for loading Products
    * Author: Soumojit
    * Date: 15-DEC-2020
    */
    public function productFileUploadView($id)
    {
        $product = Product::where('id', $id)->where('status', '!=', 'D')->where('professional_id', Auth::user()->id)->first();
        // $fileList = ProductToFile::where('product_id',$id)->get();
        // $data['fileList'] = $fileList;
        $data['product'] = $product;
        return view('modules.products.product_file_upload')->with($data);
    }
    /*
    * Method: productFileUpload
    * Description: for loading Products
    * Author: Soumojit
    * Date: 15-DEC-2020
    */
    public function productFileUpload($id,Request $request)
    {
        // $products = Product::with('category')->where('status', '!=', 'D')->where('admin_status', 'A')->where('professional_id', Auth::user()->id);
        $request->validate([
            "caption" =>  "required",
        ]);
        $products = Product::where('id',$id)->where('status', '!=', 'D')->where('professional_id', Auth::user()->id)->first();
        if($products==null){
            return redirect()->route('product.file.view', ['id' => $id])->with('error', \Lang::get('site.error_in_file_upload'));
        }
        $ins=[];
        $ins['caption'] =$request->caption;
        $ins['product_id'] = $request->id;
        $ins['file_status'] = 'A';
        if (@$request->product_file) {
            $product_file_name = time() . '-' . rand(1000, 9999) . '.' . @$request->product_file->getClientOriginalExtension();
            $product_file = @$request->product_file;
            Storage::putFileAs('public/uploads/product_files', $product_file, $product_file_name);
            $ins['product_file'] = $product_file_name;
            $ins['file_type'] = 'file';
            $ins['mime_type'] = @$request->product_file->getMimeType();
        } elseif(@$request->product_link) {
            $ins['product_file'] = $request->product_link;
            $ins['file_type'] = 'link';
        }
        $create = ProductToFile::create($ins);
        if(@$create){
            Product::where('id', $request->id)->update(['product_type' => 'N']);
            return redirect()->route('product.file.view', ['id' => $id])->with('success',\Lang::get('site.files_uploaded_succesfully'));
        }
        return redirect()->route('product.file.view',['id'=>$id])->with('error', \Lang::get('site.error_in_file_upload'));
    }
    /*
    * Method: productFileDelete
    * Description: for loading Products
    * Author: Soumojit
    * Date: 15-DEC-2020
    */
    public function productFileDelete($id)
    {

        $file = ProductToFile::where('id',$id)->where('file_status', '!=', 'D')->first();
        if($file==null){
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        $products = Product::where('id', $file->product_id)->where('status', '!=', 'D')->where('professional_id', Auth::user()->id)->first();
        if ($products  == null) {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        $delete = ProductToFile::where('id',$id)->update(['file_status'=>'D']);
        if(@$delete){
            return redirect()->route('product.file.view', ['id' => $file->product_id])->with('success', \Lang::get('site.file_deleted_succesfully'));
        }
        return redirect()->route('product.file.view',['id'=> $file->product_id])->with('error', \Lang::get('site.error_in_file_delete'));
    }
    /*
    * Method: professionalCouponList
    * Description: for showing all coupons added by the professional
    * Author: Sayantani
    * Date: 15-FEB-2020
    */
    public function professionalCouponList(){
        $data['coupons'] = Coupon::where('added_by', 'P')->where('professional_id', Auth::id())->with('couponToProduct')->orderBy('created_at', 'DESC')->get();
        return view('modules.products.professional_coupons')->with($data);
    }
    /*
    * Method: professionalAddCoupon
    * Description: for adding coupon by professional
    * Author: Sayantani
    * Date: 15-FEB-2020
    */
    public function professionalAddCoupon($id=null){
        $data['products'] = Product::with('category')->where('status', '!=', 'D')->where('professional_id', Auth::user()->id)->orderBy('created_at', 'DESC')->get();
        if(@$id) {
            $data['coupon'] = Coupon::where('id', $id)->first();
        }
        return view('modules.products.professional_add_coupon')->with($data);
    }
    /*
    * Method: storeCoupon
    * Description: for storing coupon by professional
    * Author: Sayantani
    * Date: 15-FEB-2020
    */
    public function storeCoupon(Request $request){
        $request->validate([
            "product" =>  "required",
            "code" => "required",
            "discount" => "required",
        ]);
        $coupon = [];
        $create_coupon['coupon_code'] = $request->code;
        $create_coupon['discount'] = $request->discount;
        $create_coupon['start_date'] = date('Y-m-d', strtotime($request->start_date));
        $create_coupon['exp_date'] = date('Y-m-d', strtotime($request->end_date));
        if(@$request->id){
            $coupon = Coupon::where('id', $request->id)->first();
            if(@$coupon) {
                $coupon->update($create_coupon);
                $coupon_to_product = CouponToProduct::where('coupon_id', $coupon->id)->first();
                $coupon_to_product->product_id = $request->product;
                $coupon_to_product->save();
            } else return redirect()->back()->with('error', \Lang::get('client_site.something_went_be_wrong'));
        } else {
            $create_coupon['added_by'] = 'P';
            $create_coupon['professional_id'] = Auth::id();
            $coupon = Coupon::create($create_coupon);

            $create_coupon_product['coupon_id'] = $coupon->id;
            $create_coupon_product['product_id'] = $request->product;
            CouponToProduct::create($create_coupon_product);
        }
        return redirect()->route('professional.coupons')->with('success', \Lang::get('site.coupon_added_succcesfully'));
    }
    /*
    * Method: deleteCoupon
    * Description: for deleteing coupon by professional
    * Author: Sayantani
    * Date: 17-FEB-2020
    */
    public function deleteCoupon($id){
        $coupon = Coupon::where('id', $id)->first();
        if(@$coupon) {
            $coupon->delete();
            return redirect()->back()->with('success', \Lang::get('client_site.coupon_deleted_succesfully'));
        } else {
            return redirect()->back()->with('error', \Lang::get('client_site.coupon_not_found'));
        }

    }

    /*
    * Method: editCourseChapters
    * Description: for editing course chapters
    * Author: Sayantani
    * Date: 26-May-2021
    */
    public function editCourseChapters($id){
        $data['product'] = Product::where('id',$id)->where('status', '!=', 'D')->first();
        if(@$data['product']){
            if(@$data['product']->professional_id == Auth::id()){
                $data['chapters'] = Chapter::where('course_id', $data['product']->id)->get();
                $chapter_IDS = Chapter::where('course_id', $data['product']->id)->pluck('id')->toArray();
                $free = Lesson::whereIn('chapter_id', $chapter_IDS)->where('is_main_preview_video', 'Y')->first();
                if(@$free) $data['free_lesson'] = $free->id;
                return view('modules.products.edit_chapters')->with($data);
            } else {
                return redirect()->back()->with('error', \Lang::get('client_site.unauthorize_access'));
            }
        } else {
            return redirect()->back()->with('error', \Lang::get('client_site.product_not_found'));
        }
    }

    /*
    * Method: addChapter
    * Description: for adding/editing course chapters
    * Author: Sayantani
    * Date: 27-May-2021
    */
    public function addChapter(Request $request, $id, $cid=null){
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];

        $jsn['product'] = Product::where('id',$request['params']['product_id'])->where('status', '!=', 'D')->first();
        $create['chapter_title'] = $request['params']['chapter_title'];
        $create['set_lessons_status'] = $request['params']['set_lessons_status'];

        if(@$request['params']['ID']){
            // $create['status'] = $request['params']['chapter_status'];
            $chapter = Chapter::where('course_id', $jsn['product']->id)->where('id', @$request['params']['ID'])->first();
            $chapter->update($create);
            $jsn['chapter'] = $chapter;
            $jsn['status'] = "success";
            $jsn['message'] = \Lang::get('client_site.chapter_created_successfully');
        } else{
            $create['course_id'] = $jsn['product']->id;
            $jsn['chapter'] = Chapter::create($create);
            $jsn['status'] = "success";
            $jsn['message'] = \Lang::get('client_site.chapter_updated_successfully');
        }
        $jsn['chapters'] = Chapter::where('course_id', $jsn['product']->id)->get();
        return response()->json($jsn);
    }

    /*
    * Method: deleteChapter
    * Description: for deleting a chapter
    * Author: Sayantani
    * Date: 27-May-2021
    */
    public function deleteChapter($id){
        $chapter = Chapter::where('id', $id)->first();
        if(@$chapter){
            $lessons = Lesson::where('chapter_id', $chapter->id)->get();
            foreach($lessons as $lesson){
                $this->deleteLesson($lesson->id);
                // if(@$lesson->description){
                //     if (strpos(@$lesson->description, '<img') !== false) {
                //         preg_match_all( '@src="([^"]+)"@' , $lesson->description, $match );
                //         $files = [];
                //         $srcs = array_pop($match);
                //         foreach($srcs as $src){
                //             array_push($files, basename($src));
                //         }
                //         // dd($files);
                //     }
                // }
                // $files = LessonFile::where('lesson_id', $lesson->id)->get();
                // foreach($files as $file){
                //     if($file->filetype == 'V'){
                //         @unlink(storage_path('app/public/lessons/type_video/'.@$file->filename));
                //     }
                //     if($file->filetype == 'T'){
                //         @unlink(storage_path('app/public/lessons/type_video/thumbnails/'.@$file->filename));
                //     }
                //     if($file->filetype == 'D' && $file->parent_id != 0){
                //         @unlink(storage_path('app/public/lessons/type_video/attachments/'.@$file->filename));
                //     }
                //     if($file->filetype == 'D' && $file->parent_id == 0){
                //         @unlink(storage_path('app/public/lessons/type_downloads/'.@$file->filename));
                //     }
                //     if($file->filetype == 'A'){
                //         @unlink(storage_path('app/public/lessons/type_audio/'.@$file->filename));
                //     }
                // }
            }
            $chapter->delete();
            return redirect()->back()->with('success', \Lang::get('client_site.chapter_deleted_succesfully'));
        } else {
            return redirect()->back()->with('error', \Lang::get('client_site.chapter_not_found'));
        }
    }

    /*
    * Method: saveLesson
    * Description: for editing course chapters
    * Author: Sayantani
    * Date: 26-May-2021
    */
    public function saveLesson(Request $request)
    {
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];

        $chapter = null;
        $lesson = null;
        $flag = 0;

        if(@$request->lesson_id){
            $lesson = Lesson::where('id', $request->lesson_id)->first();
            if(!@$lesson){
                $jsn['status'] = 'error';
                $jsn['message'] = \Lang::get('client_site.something_went_be_wrong');
                return response()->json($jsn);
            }
        }

        $update['lesson_title']     = $request->lesson_title;
        $update['duration']         = @$request->duration ? @$request->duration : 0;
        $update['chapter_id']       = $request->chapter_id;
        $update['lesson_type']      = $request->lesson_type;
        $update['multimedia_url']   = @$request->multimedia_url;

        if($update['duration'] == 0) $update['status'] = 'D';
        else $update['status'] = @$request->status;

        $update['is_allow_discussion'] = $request->is_allow_discussion;
        $update['is_prerequisite'] = $request->is_prerequisite;
        $update['is_free_preview'] = $request->is_free_preview;
        // $update['is_main_preview_video'] = $request->is_main_preview_video;

        $jsn['product'] = Product::where('id',$request->product_id)->where('status', '!=', 'D')->first();
        $jsn['chapters'] = Chapter::where('course_id', $jsn['product']->id)->get();

        if(!@$jsn['product']){
            $jsn['status'] = 'error';
            $jsn['message'] = \Lang::get('client_site.something_went_be_wrong');
            return response()->json($jsn);
        }

        $chapter = Chapter::where('id',@$request->chapter_id)->first();
        if(!@$chapter){
            $jsn['status'] = 'error';
            $jsn['message'] = \Lang::get('client_site.something_went_be_wrong');
            return response()->json($jsn);
        }

        if($request->is_main_preview_video == 'Y'){
            $chapter_IDS = Chapter::where('course_id', $jsn['product']->id)->pluck('id')->toArray();
            $lsns = Lesson::whereIn('chapter_id', $chapter_IDS)->where('is_main_preview_video', 'Y')->get();
            if(count($lsns)>0){
                foreach($lsns as $lsn){
                    $lsn->update(['is_main_preview_video' => 'N']);
                }
            }
            $lesson->update(['is_main_preview_video' => 'Y']);
        }

        if(@$request->lesson_type == 'V'){
            $flag = 0;
            if(@$request->lesson_id){
                $lesson->update($update);
            } else {
                $lesson = Lesson::create($update);
                // $next_level = (int) max($chapter->getAllLessons->pluck('level')->toArray()) +1;
                // $lesson->update(['level' => $next_level]);
                $chapter->no_of_lessons += 1;
                $chapter->save();
            }
            $lesson_file = null;
            $lesson_parent_id = LessonFile::where('lesson_id',$lesson->id)->where('filetype','V')->where('parent_id',0)->first();
            if(@$request->thumbnail){
                $lsn_thumb = LessonFile::where('lesson_id', $lesson->id)->where('parent_id', @$lesson_parent_id->id)->where('filetype', 'T')->first();
                if(@$lsn_thumb){
                    @unlink(storage_path('app\public\lessons\type_video\thumbnails\\'.$lsn_thumb->filename));
                    $lsn_thumb->delete();
                }

                $file = $request->thumbnail;
                $name = explode('.', $file->getClientOriginalName())[0];
                // $name = str_replace(' ', '_', $name);
                $name = $name.'.'.@$file->getClientOriginalExtension();
                $file_name = time().'-'.rand(1000,9999).'.'.@$file->getClientOriginalExtension();
                $mime_type = $file->getClientMimeType();
                $file = @$file;
                Storage::putFileAs('public/lessons/type_video/thumbnails', $file, $file_name);

                $thumb['lesson_id'] = $request->lesson_id;
                $thumb['filename'] = $file_name;
                $thumb['name'] = $name;
                $thumb['filetype'] = 'T';
                $thumb['mimetype'] = $mime_type;
                $thumb['parent_id'] = $lesson_parent_id->id;
                $jsn['thumbnail'] = LessonFile::create($thumb);
            }
            if(@$request->video_url){
                $url = @$request->video_url;
                parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
                $url_id = @$my_array_of_vars['v'];
                if($url_id == null){
                    $pieces = explode("/", $url);
                    $url_id = end($pieces);
                    if($url_id == null){
                        $flag = 1;
                    }
                }
                $exists = LessonFile::where('lesson_id', $lesson->id)->where('filetype','Y')->where('parent_id',0)->first();
                if(@$exists){
                    $exists->update(['filename' => $url_id]);
                } else {
                    $cr['lesson_id'] = $lesson->id;
                    $cr['filename'] = $url_id;
                    $cr['filetype'] = 'Y';
                    $cr['parent_id'] = 0;
                    LessonFile::create($cr);
                }
            }
            if(@$request->description){
                $update['description'] = $request->description;
            }
            if(@$request->attachment_names){
                foreach($request->attachment_names as $id=>$name){
                    $d = LessonFile::where('id',$id)->first();
                    $d->update(['name'=>$name]);
                }
            }
            $lesson->update($update);

            if($flag == 1){
                $jsn['main_video'] = LessonFile::where('lesson_id',$lesson->id)->where('parent_id',0)->where('filetype','V')->first();
                $jsn['attachments'] = LessonFile::where('lesson_id',$lesson->id)->where('parent_id',$jsn['main_video']->id)->where('filetype','D')->get();
            }

        }
        if(@$request->lesson_type == 'Q'){
            if(@$request->lesson_id){
                $lesson->update($update);
            } else {
                $lesson = Lesson::create($update);
                // $next_level = (int) max($chapter->getAllLessons->pluck('level')->toArray()) +1;
                // $lesson->update(['level' => $next_level]);
                $chapter->no_of_lessons += 1;
                $chapter->save();
            }
            if(@$request->questions){
                $jsn['questions'] = json_decode($request->questions);
                $questions = json_decode($request->questions);

                foreach($questions as $question){
                    $quiz['lesson_id'] = $lesson->id;
                    $quiz['question'] = @$question->question ? $question->question : null;
                    $quiz['answer_1'] = @$question->answer_1 ? $question->answer_1 : null;
                    $quiz['answer_2'] = @$question->answer_2 ? $question->answer_2 : null;
                    $quiz['answer_3'] = @$question->answer_3 ? $question->answer_3 : null;
                    $quiz['answer_4'] = @$question->answer_4 ? $question->answer_4 : null;
                    $quiz['answer_5'] = @$question->answer_5 ? $question->answer_5 : null;
                    $quiz['answer_6'] = @$question->answer_6 ? $question->answer_6 : null;
                    $quiz['explanation'] = @$question->explanation ? $question->explanation : null;
                    $quiz['correct_answer'] = @$question->correct;
                    if(@$question->id){
                        Quiz::where('id',@$question->id)->update($quiz);
                    } else {
                        Quiz::create($quiz);
                    }
                }
            }
        }
        if(@$request->lesson_type == 'M'){
            $update['multimedia_url'] = $request->source_url;

            if(@$request->lesson_id){
                $lesson->update($update);
            } else {
                $lesson = Lesson::create($update);
                // $next_level = (int) max($chapter->getAllLessons->pluck('level')->toArray()) +1;
                // $lesson->update(['level' => $next_level]);
                $chapter->no_of_lessons += 1;
                $chapter->save();
            }
        }
        if(@$request->lesson_type == 'T'){
            $update['description'] = $request->description;

            if(@$request->lesson_id){
                $lesson->update($update);
            } else {
                $lesson = Lesson::create($update);
                // $next_level = (int) max($chapter->getAllLessons->pluck('level')->toArray()) +1;
                // $lesson->update(['level' => $next_level]);
                $chapter->no_of_lessons += 1;
                $chapter->save();
            }
        }
        if(@$request->lesson_type == 'A'){
            $update['description'] = $request->description;

            if(@$request->lesson_id){
                $lesson->update($update);
            } else {
                $lesson = Lesson::create($update);
                // $next_level = (int) max($chapter->getAllLessons->pluck('level')->toArray()) +1;
                // $lesson->update(['level' => $next_level]);
                $chapter->no_of_lessons += 1;
                $chapter->save();
            }
            $jsn['lesson'] = $lesson;
        }
        if(@$request->lesson_type == 'D'){
            $update['description'] = $request->description;

            if(@$request->download_names){
                foreach($request->download_names as $id=>$name){
                    $d = LessonFile::where('id',$id)->first();
                    $d->update(['name'=>$name]);
                }
            }
            if(@$request->lesson_id){
                $lesson->update($update);
            } else {
                $lesson = Lesson::create($update);
                // $next_level = (int) max($chapter->getAllLessons->pluck('level')->toArray()) +1;
                // $lesson->update(['level' => $next_level]);
                $chapter->no_of_lessons += 1;
                $chapter->save();
            }
        }
        if(@$request->lesson_type == 'P'){
            if(@$request->lesson_id){
                $lesson->update($update);
            } else {
                $lesson = Lesson::create($update);
                // $next_level = (int) max($chapter->getAllLessons->pluck('level')->toArray()) +1;
                // $lesson->update(['level' => $next_level]);
                $chapter->no_of_lessons += 1;
                $chapter->save();
            }
            if(@$request->slide_descriptions){
                foreach($request->slide_descriptions as $id=>$description){
                    Slide::where('id',$id)->update(['description'=>$description]);
                }
            }
        }

        $this->calcChapterDuration($chapter->id);
        $this->calcCourseDuration($jsn['product']->id);

        // rearrange lesson levels
        $this->setLessonLevels($jsn['product']->id);
        $jsn['request'] = $request->all();
        $jsn['lesson'] = $lesson;

        if($flag == 1){
            $jsn['status'] = 'error';
            $jsn['message'] = \Lang::get('client_site.something_went_be_wrong');
        } else {
            $jsn['status'] = 'success';
            $jsn['message'] = \Lang::get('client_site.lesson_saved_successfully');
        }

        return response()->json($jsn);
    }

    /*
    * Method: getLessonDetails
    * Description: for editing course chapters
    * Author: Sayantani
    * Date: 26-May-2021
    */
    public function getLessonDetails($id)
    {
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];

        $jsn['lesson'] = Lesson::where('id',$id)->first();
        $jsn['chapter'] = $jsn['lesson']->getChapter;
        $jsn['discussion'] = LessonQuestion::with('getAnswers', 'getQuestioner')->where('lesson_id', $id)->get();

        foreach($jsn['discussion'] as $question){
            $question['asked_on'] = Carbon::parse($question->created_at)->diffForHumans();
            foreach($question->getAnswers as $answer){
                $answer['answered_by'] = $answer->getAnswerer;
                $answer['answered_on'] = Carbon::parse($answer->created_at)->diffForHumans();
            }
        }
        if($jsn['lesson']->lesson_type == 'V'){
            $jsn['main_video'] = LessonFile::where('lesson_id',$jsn['lesson']->id)->where('parent_id',0)->whereIn('filetype', ['V', 'Y','VM'])->first();

            if($jsn['main_video']->filetype=='VM'){
                $client_id="2ef22f42e842d25765973e874e2bf34ed51dd320";
                $client_secret="hUZI1huiNKGNckaSSlT1GI8MSkGV5NkYVjy4PYktt/PCGVvCafHD0l9I5OOPP8WYenyZ2slGZrxu46+vDDvH0tYopcFuWG4UEiYRscVVT11FjA02Ib6otDth44D0JVWo";
                $access_token="00cd84a77d6116059394a3bbf43b3f46";

                $client = new Vimeo($client_id, $client_secret, $access_token);

                $re =str_replace('https://player.vimeo.com/video', '/videos', $jsn['main_video']->filename);
                $ra=explode('?', $re );
                $code= $ra[0];
                $data1=$client->request($code);
                // $jsn['vimeo_status']=$data1;
                $check=@$data1['body']['status'];
                if(@$data1['body']['status']=='available'){
                    $jsn['vimeo_status']='A';
                }else{
                    $jsn['vimeo_status']='I';
                }
            }




            // $jsn['thumbnail'] = LessonFile::where('lesson_id',$jsn['lesson']->id)->where('parent_id',$jsn['main_video']->id)->where('filetype', 'T')->first();
            $jsn['thumbnail'] = LessonFile::where('lesson_id',$jsn['lesson']->id)->where('filetype', 'T')->first();
            $jsn['downloads'] = LessonFile::where('lesson_id',$jsn['lesson']->id)->where('filetype','D')->where('parent_id','!=',0)->get();
        }
        if($jsn['lesson']->lesson_type == 'Q'){
            $jsn['questions'] = Quiz::where('lesson_id',$id)->get();
            $jsn['noOfCorrect'] = QuizProgress::where('user_id', Auth::id())->where('lesson_id', $id)->where('is_correct', 'Y')->count();
            $jsn['allProgress'] = QuizProgress::where('user_id', Auth::id())->where('lesson_id', $id)->get();
        }
        if($jsn['lesson']->lesson_type == 'A'){
            $jsn['main_audio'] = LessonFile::where('lesson_id',$jsn['lesson']->id)->where('parent_id',0)->where('filetype','A')->first();
        }
        if($jsn['lesson']->lesson_type == 'D'){
            $jsn['downloads'] = LessonFile::where('lesson_id',$jsn['lesson']->id)->where('parent_id',0)->where('filetype','D')->get();
        }
        if($jsn['lesson']->lesson_type == 'P'){
            $jsn['slides'] = Slide::where('lesson_id',$jsn['lesson']->id)->get();
        }
        // $bought = ProductOrderDetails::where('product_id', $id)->whereHas('orderMaster', function($q){
        //     $q->where('user_id', Auth::id());
        // })->first();
        $jsn['courseProgress']['id'] = "";
        if(@$jsn['lesson']->getChapter->getProduct->professional_id != Auth::id()){
            $exists = CourseProgress::where('user_id', Auth::id())->where('product_id', $jsn['lesson']->getChapter->getProduct->id)->where('lesson_id', $jsn['lesson']->id)->first();
            $all = CourseProgress::where('user_id', Auth::id())->where('product_id', $jsn['lesson']->getChapter->getProduct->id)->get();
            if(count($all) > 0){
                foreach($all as $a){
                    $a->current_lesson = 'N';
                    $a->save();
                }
            }
            if(!@$exists){
                $create['user_id'] = Auth::id();
                $create['product_id'] = $jsn['lesson']->getChapter->getProduct->id;
                $create['lesson_id'] = $jsn['lesson']->id;
                $create['level'] = $jsn['lesson']->level;
                $create['status'] = 'V';
                $create['current_lesson'] = 'Y';
                $c = CourseProgress::create($create);
                $jsn['courseProgress'] = $c;
            } else {
                $exists->current_lesson = 'Y';
                $exists->save();
                $jsn['courseProgress'] = $exists;
            }

            $data['prequisite'] = 'N';
            $chapter_IDS = Chapter::where('course_id', $jsn['chapter']->course_id)->pluck('id')->toArray();
            $previous_prerequisite_lesson = Lesson::whereIn('chapter_id', @$chapter_IDS)->where('status','A')->where('level', '<', $jsn['lesson']->level)->where('is_prerequisite', 'Y')->get();
            if(count(@$previous_prerequisite_lesson)>0){
                foreach(@$previous_prerequisite_lesson as $prerequisite){
                    $exists = CourseProgress::where('user_id', Auth::id())->where('product_id', $jsn['lesson']->getChapter->getProduct->id)->where('lesson_id', $prerequisite->id)->where('status','C')->first();
                    if(!@$exists){
                        $jsn['prequisite'] = 'Y';
                        $jsn['prequisite_message'] = "A lição \"" . $prerequisite->lesson_title. "\" é um pré-requisito. Preencha-o antes de prosseguir.";
                        return response()->json($jsn);
                    }
                }
            }
        }

        return response()->json($jsn);
    }

    /*
    * Method: textImgUpload
    * Description: for image upload from text editors
    * Author: Sayantani
    * Date: 27-May-2021
    */
    public function textImgUpload(Request $request)
    {
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        $doc_name           = $request['file']->getClientOriginalName();
        $explode_doc_name   = explode('.', $doc_name);
        $extension          = end($explode_doc_name);
        $doc_name           = time() . str_random(6) . '.' . $extension;
        $newFilePath        = "storage/app/public/lessons/textarea_uploads/";
        $request['file']->move($newFilePath, $doc_name);
        $jsn['status'] = 'SUCCESS';
        $jsn['location'] = url('/') . '/storage/app/public/lessons/textarea_uploads/' . $doc_name;
        return response()->json($jsn);
    }

    /*
    * Method: checkChapterTitle
    * Description: for checking chapter title
    * Author: Sayantani
    * Date: 27-May-2021
    */
    public function checkChapterTitle(Request $request)
    {
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        if (@$request->id){
            $check = Chapter::where('chapter_title',$request['chapter_title'])->where('id','!=',$request['id'])->where('course_id', $request['product_id'])->first();
        } else {
            $check = Chapter::where('chapter_title',$request['chapter_title'])->where('course_id', $request['product_id'])->first();
        }
        if (!empty($check)) {
            $jsn['result'] = "false";
        }else{
            $jsn['result'] = "true";
        }
        return response()->json($jsn);

    }

    /*
    * Method: deleteLesson
    * Description: for deleting a lessons
    * Author: Sayantani
    * Date: 28-May-2021
    */
    public function deleteLesson($id, $ajax = null){
        $lesson = Lesson::where('id', $id)->first();
        if(@$lesson){
            if(@$lesson->lesson_type == 'V'){
                $lesson_files =  LessonFile::where('lesson_id',$id)->get();
                foreach($lesson_files as $lesson_file){
                    if($lesson_file->filetype == 'V')
                        @unlink(storage_path('app\public\lessons\type_video\\'.$lesson_file->filename));
                    if($lesson_file->filetype == 'T')
                        @unlink(storage_path('app\public\lessons\type_video\thumbnails\\'.$lesson_file->filename));
                    if($lesson_file->filetype == 'D')
                        @unlink(storage_path('app\public\lessons\type_video\attachments\\'.$lesson_file->filename));
                    $lesson_file->delete();
                }
            }
            if(@$lesson->lesson_type == 'Q'){
                $quizes = Quiz::where('lesson_id', $id)->get();
                foreach($quizes as $quiz){
                    $quiz->delete();
                }
            }
            if($lesson->lesson_type == 'A'){
                @unlink(storage_path('app\public\lessons\type_audio\\'.$lesson_file->filename));
                if(@$lesson->description){
                    if (strpos(@$lesson->description, '<img') !== false) {
                        preg_match_all( '@src="([^"]+)"@' , $lesson->description, $match );
                        $srcs = array_pop($match);
                        foreach($srcs as $src){
                            @unlink(storage_path('app\public\lessons\textarea_uploads\\'.basename(@$src)));
                        }
                    }
                }
            }
            if($lesson->lesson_type == 'D'){
                $lesson_files =  LessonFile::where('lesson_id',$id)->get();
                // dd($lesson_files);
                foreach($lesson_files as $lesson_file){
                    @unlink(storage_path('app\public\lessons\type_downloads\\'.$lesson_file->filename));
                    $lesson_file->delete();
                }
            }
            if(@$lesson->lesson_type == 'P'){
                $slides =  Slide::where('lesson_id',$id)->get();
                foreach($slides as $slide){
                    @unlink(storage_path('app\public\lessons\type_presentation\\'.$slide->filename));
                    @unlink(storage_path('app\public\lessons\type_presentation\audio\\'.$slide->audio));
                    if(@$slide->description){
                        if (strpos(@$lesson->description, '<img') !== false) {
                            preg_match_all( '@src="([^"]+)"@' , $lesson->description, $match );
                            $srcs = array_pop($match);
                            foreach($srcs as $src){
                                @unlink(storage_path('app\public\lessons\textarea_uploads\\'.basename(@$src)));
                            }
                        }
                    }
                    $slide->delete();
                }
            }
            if(@$lesson->description){
                if (strpos(@$lesson->description, '<img') !== false) {
                    preg_match_all( '@src="([^"]+)"@' , $lesson->description, $match );
                    $srcs = array_pop($match);
                    foreach($srcs as $src){
                        @unlink(storage_path('app\public\lessons\textarea_uploads\\'.basename(@$src)));
                    }
                }
            }

            $progresses = CourseProgress::where('product_id', $lesson->getChapter->getProduct->id)->where('lesson_id', $lesson->id)->get();
            foreach($progresses as $progress){
                $progress->delete();
            }

            $lesson->delete();
            $lesson->getChapter->no_of_lessons -= 1;
            $lesson->getChapter->save();
            $this->calcChapterDuration($lesson->getChapter->id);
            $this->calcCourseDuration($lesson->getChapter->getProduct->id);

            // rearrange lesson levels
            $this->setLessonLevels($lesson->getChapter->getProduct->id);
            if($ajax == "ajax"){
                $jsn['status'] = "success";
                $jsn['message'] = \Lang::get('client_site.lesson_deleted_succesfully');
                return response()->json($jsn);
            } else {
                return redirect()->back()->with('success', \Lang::get('client_site.lesson_deleted_succesfully'));
            }
        } else {
            if($ajax = "ajax"){
                $jsn['status'] = "error";
                $jsn['message'] = \Lang::get('client_site.chapter_deleted_succesfully');
                return response()->json($jsn);
            } else {
                return redirect()->back()->with('error', \Lang::get('client_site.lesson_not_found'));
            }
        }
    }

    /*
    * Method: deleteAttachment
    * Description: for deleting a download attachment
    * Author: Sayantani
    * Date: 28-May-2021
    */
    public function deleteAttachment($id){
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        $lesson_file = LessonFile::where('id', $id)->first();
        if(@$lesson_file){
            @unlink(storage_path('app\public\lessons\type_video\attachments\\'.$lesson_file->filename));
            $lesson_file->delete();
            $jsn['status'] = "success";
            $jsn['message'] = \Lang::get('client_site.attachment_deleted_successfully');
        } else {
            $jsn['status'] = "error";
            $jsn['message'] = \Lang::get('client_site.lesson_file_not_found');
        }
        return response()->json($jsn);
    }

    /*
    * Method: deleteDownload
    * Description: for deleting a download attachment
    * Author: Sayantani
    * Date: 03-June-2021
    */
    public function deleteDownload($id){
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        $lesson_file = LessonFile::where('id', $id)->first();
        if(@$lesson_file){
            @unlink(storage_path('app\public\lessons\type_downloads\\'.$lesson_file->filename));
            $lesson_file->delete();
            $jsn['status'] = "success";
            $jsn['message'] = \Lang::get('client_site.file_deleted_successfully');
        } else {
            $jsn['status'] = "error";
            $jsn['message'] = \Lang::get('client_site.lesson_file_not_found');
        }
        return response()->json($jsn);
    }

    /*
    * Method: getLessons
    * Description: for loading lessons for a chapter
    * Author: Sayantani
    * Date: 1-June-2021
    */
    public function getLessons($id){
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        $jsn['chapter'] = Chapter::where('id',$id)->first();
        $jsn['lessons'] = Lesson::where('chapter_id', $id)->get();
        foreach($jsn['lessons'] as $i=>$lesson){
            if($lesson->lesson_type == 'V'){
                $lesson->main_video = LessonFile::where('lesson_id',$lesson->id)->where('parent_id',0)->first();
            }
            if($lesson->lesson_type == 'Q'){
                $lesson->question_count = Quiz::where('lesson_id',$lesson->id)->get()->count();
            }
        }
        return response()->json($jsn);
    }

    /*
    * Method: getPreviewLessons
    * Description: for loading lessons for a chapter
    * Author: Sayantani
    * Date: 12-June-2021
    */
    public function getPreviewLessons($id){
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        $jsn['chapter'] = Chapter::where('id',$id)->first();
        $jsn['lessons'] = Lesson::where('chapter_id', $id)->where('status', 'A')->get();
        $jsn['chapter']->progress_count = 0;
        foreach($jsn['lessons'] as $i=>$lesson){
            if($lesson->lesson_type == 'V'){
                $lesson->main_video = LessonFile::where('lesson_id',$lesson->id)->where('parent_id',0)->first();
            }
            if($lesson->lesson_type == 'A'){
                $lesson->main_audio = LessonFile::where('lesson_id',$lesson->id)->where('parent_id',0)->first();
            }
            if($lesson->lesson_type == 'Q'){
                $lesson->question_count = Quiz::where('lesson_id',$lesson->id)->get()->count();
            }
            if($lesson->lesson_type == 'P'){
                $lesson->slide_count = Slide::where('lesson_id',$lesson->id)->get()->count();
            }
            $prog = CourseProgress::where('user_id', Auth::id())->where('product_id', $jsn['chapter']->course_id)->where('level', $lesson->level)->first();
            if(@$prog){
                $lesson->progress = true;
                $jsn['chapter']->progress_count += 1;
            } else {
                $lesson->progress = false;
            }
        }
        return response()->json($jsn);
    }

    /*
    * Method: deleteQuestion
    * Description: for deleting a quiz quetion
    * Author: Sayantani
    * Date: 28-May-2021
    */
    public function deleteQuestion($id){
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        $quiz = Quiz::where('id', $id)->first();
        if(@$quiz){
            $quiz->delete();
            $jsn['status'] = "success";
            $jsn['message'] = \Lang::get('client_site.question_deleted_successfully');
        } else {
            $jsn['status'] = "error";
            $jsn['message'] = \Lang::get('client_site.question_not_found');
        }
        return response()->json($jsn);

    }

    /*
    * Method: uploadDownloads
    * Description: for saving files for lesson type download
    * Author: Sayantani
    * Date: 28-May-2021
    */
    public function uploadDownloads(Request $request){
        // $request = json_decode($request);
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        $jsn['request'] = $request;
        if(@$request->file('file')){
            $file = $request->file;
            $name = explode('.', $file->getClientOriginalName())[0];
            // $name = str_replace(' ', '_', $name);
            $name = $name.'.'.@$file->getClientOriginalExtension();
            $file_name = time().'-'.rand(1000,9999).'.'.@$file->getClientOriginalExtension();
            $mime_type = $file->getClientMimeType();
            $file = @$file;
            Storage::putFileAs('public/lessons/type_downloads', $file, $file_name);

            $file_ins_dwnld['lesson_id'] = $request->lesson_id;
            $file_ins_dwnld['filename'] = $file_name;
            $file_ins_dwnld['name'] = $name;
            $file_ins_dwnld['filetype'] = 'D';
            $file_ins_dwnld['mimetype'] = $mime_type;
            $file_ins_dwnld['duration'] = @$request->duration;
            $file_ins_dwnld['parent_id'] = 0;

            $jsn['status'] = "success";
            $jsn['message'] = \Lang::get('client_site.file_uploaded_successfully');
            $jsn['attachment'] = LessonFile::create($file_ins_dwnld);
        }
        return response()->json($jsn);
    }

    /*
    * Method: uploadAudio
    * Description: for deleting a quiz quetion
    * Author: Sayantani
    * Date: 04-June-2021
    */
    public function uploadAudio(Request $request)
    {
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        $jsn['request'] = $request;
        $lesson = Lesson::where('id',$request->lesson_id)->first();
        if(@$request->file('file')){
            $exists = LessonFile::where('lesson_id',$lesson->id)->where('parent_id',0)->where('filetype','A')->first();
            if(@$exists){
                @unlink(storage_path('app\public\lessons\type_audio\\'.$exists->filename));
                $exists->delete();
            }

            $file_ins['lesson_id'] = $lesson->id;
            $file_ins['filetype'] = 'A';
            $file_ins['parent_id'] = 0;

            $name = explode('.', $request->file->getClientOriginalName())[0];
            $name = $name.'.'.@$request->file->getClientOriginalExtension();
            $file_name = time().'-'.rand(100000,999999).'.'.@$request->file->getClientOriginalExtension();;
            $mime_type = $request->file->getClientMimeType();

            $file = @$request->file;
            Storage::putFileAs('public/lessons/type_audio', $file, $file_name);

            $file_ins['filename'] = $file_name;
            $file_ins['name'] = $name;
            $file_ins['mimetype'] = $mime_type;
            $file_ins['duration'] = @$request->duration;
            $jsn['main_audio'] = LessonFile::create($file_ins);
            $jsn['status'] = "success";
            $jsn['message'] = \Lang::get('client_site.file_uploaded_successfully');
        } else {
            $jsn['status'] = "error";
            $jsn['message'] = \Lang::get('client_site.file_not_found');
        }
        return response()->json($jsn);
    }

    /*
    * Method: uploadVideo
    * Description: for deleting a quiz quetion
    * Author: Sayantani
    * Date: 04-June-2021
    */
    public function uploadVideo(Request $request)
    {
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        $jsn['request'] = $request->all();

        $lesson = Lesson::where('id',$request->lesson_id)->first();
        if(@$request->file('file')){

            if(@$request->videoType=='VM'){

                $exists = LessonFile::where('lesson_id',$lesson->id)->where('parent_id',0)->where('filetype','VM')->first();
                if(@$exists){
                    @unlink(storage_path('app\public\lessons\type_video\\'.$exists->filename));
                    $exists->delete();
                }
                // $client_id="890583e505438df25dc16cab818ab191afb1b62b";
                // $client_secret="/VNtuDjrT8O971OmhxzXwpLroknhiMj4C++pLAi4kfQU+053dBIEiH6QRAmqZicUboIiOVm0lAEph4twvjbQvRP2hpoLcHLpfQn8OfppAzaXikGaNSM5WRWMsijis+s+";
                // $access_token="2cb0116c9137ea355f06a3fd5ffa59f1";

                $client_id="2ef22f42e842d25765973e874e2bf34ed51dd320";
                $client_secret="hUZI1huiNKGNckaSSlT1GI8MSkGV5NkYVjy4PYktt/PCGVvCafHD0l9I5OOPP8WYenyZ2slGZrxu46+vDDvH0tYopcFuWG4UEiYRscVVT11FjA02Ib6otDth44D0JVWo";
                $access_token="00cd84a77d6116059394a3bbf43b3f46";

                $client = new Vimeo($client_id, $client_secret, $access_token);


                $file_ins['lesson_id'] = $lesson->id;
                $file_ins['filetype'] = 'VM';
                $file_ins['parent_id'] = 0;

                $name = explode('.', $request->file->getClientOriginalName())[0];
                $orgName=$name ;
                $name = $name.'.'.@$request->file->getClientOriginalExtension();
                $file_name = time().'-'.rand(100000,999999).'.'.@$request->file->getClientOriginalExtension();;
                $mime_type = $request->file->getClientMimeType();

                $file = @$request->file;
                // Storage::putFileAs('public/lessons/type_video', $file, $file_name);

                $file_name = $request->image;
                $file_name= $request->file('file');
                $uri = $client->upload($file_name, array(
                    "name" => "" .@$orgName,
                    "description" => "Video Upload"
                ));
                // $videoCode= str_replace('/videos/', '', $uri);

                $data1=$client->request($uri);
                $data2['video_embed_url'] = @$data1['body']['player_embed_url'];



                $file_ins['filename'] = $data2['video_embed_url'];
                $file_ins['name'] = $name;
                $file_ins['mimetype'] = $mime_type;
                $file_ins['duration'] = @$request->duration;
                $jsn['main_video'] = LessonFile::create($file_ins);
                $jsn['vimeo_status']='I';
                $jsn['status'] = "success";
                $jsn['message'] = \Lang::get('client_site.file_uploaded_successfully');

            }else{

                $exists = LessonFile::where('lesson_id',$lesson->id)->where('parent_id',0)->where('filetype','V')->first();
                if(@$exists){
                    @unlink(storage_path('app\public\lessons\type_video\\'.$exists->filename));
                    $exists->delete();
                }

                $file_ins['lesson_id'] = $lesson->id;
                $file_ins['filetype'] = 'V';
                $file_ins['parent_id'] = 0;

                $name = explode('.', $request->file->getClientOriginalName())[0];
                $name = $name.'.'.@$request->file->getClientOriginalExtension();
                $file_name = time().'-'.rand(100000,999999).'.'.@$request->file->getClientOriginalExtension();;
                $mime_type = $request->file->getClientMimeType();

                $file = @$request->file;
                Storage::putFileAs('public/lessons/type_video', $file, $file_name);

                $file_ins['filename'] = $file_name;
                $file_ins['name'] = $name;
                $file_ins['mimetype'] = $mime_type;
                $file_ins['duration'] = @$request->duration;
                $jsn['main_video'] = LessonFile::create($file_ins);
                $jsn['status'] = "success";
                $jsn['message'] = \Lang::get('client_site.file_uploaded_successfully');

            }
        } else {
            $jsn['status'] = "error";
            $jsn['message'] = \Lang::get('client_site.file_not_found');
        }
        return response()->json($jsn);
    }

    /*
    * Method: deleteMainVideo
    * Description: to delete main video of lesson
    * Author: Sayantani
    * Date: 04-Aug-2021
    */
    public function deleteMainVideo($id){
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];

        $lesson = Lesson::where('id',$id)->first();
        if(@$lesson){
            $main = LessonFile::where('lesson_id',$lesson->id)->where('parent_id',0)->whereIn('filetype',['V','Y','VM'])->first();
            if(@$main){
                if(@$main->filetype == 'V') @unlink(storage_path('app\public\lessons\type_video\\'.$main->filename));
                $thumbnail = LessonFile::where('lesson_id',$lesson->id)->where('parent_id',@$main->id)->where('filetype', 'T')->first();
                if(@$thumbnail) {
                    @unlink(storage_path('app\public\lessons\type_video\thumbnails\\'.$thumbnail->filename));
                    $thumbnail->delete();
                }
                if($main->filetype == 'VM'){
                    // $client_id="890583e505438df25dc16cab818ab191afb1b62b";
                    // $client_secret="/VNtuDjrT8O971OmhxzXwpLroknhiMj4C++pLAi4kfQU+053dBIEiH6QRAmqZicUboIiOVm0lAEph4twvjbQvRP2hpoLcHLpfQn8OfppAzaXikGaNSM5WRWMsijis+s+";
                    // $access_token="2cb0116c9137ea355f06a3fd5ffa59f1";

                    $client_id="2ef22f42e842d25765973e874e2bf34ed51dd320";
                    $client_secret="hUZI1huiNKGNckaSSlT1GI8MSkGV5NkYVjy4PYktt/PCGVvCafHD0l9I5OOPP8WYenyZ2slGZrxu46+vDDvH0tYopcFuWG4UEiYRscVVT11FjA02Ib6otDth44D0JVWo";
                    $access_token="00cd84a77d6116059394a3bbf43b3f46";


                    $client = new Vimeo($client_id, $client_secret, $access_token);
                    $videoCode= str_replace('https://player.vimeo.com/video/', '', $main->filename);
                    $videoCodeR=explode("?",$videoCode);
                    // $data1=$client->request('/videos/'.$videoCodeR[0]);
                    $client->request('/videos/'.$videoCodeR[0], [], 'DELETE');
                    // $data2['video_embed_url'] = @$data1['body']['player_embed_url'];
                    // dd($data1);

                }
                $main->delete();
                $lesson->status = 'D';
                $lesson->save();
                $jsn['status'] = "success";
                $jsn['message'] = \Lang::get('client_site.video_deleted_successfully');
            } else {
                $jsn['status'] = "error";
                $jsn['message'] = \Lang::get('client_site.video_not_found');
            }
        } else {
            $jsn['status'] = "error";
            $jsn['message'] = \Lang::get('client_site.lesson_not_found');
        }
        return response()->json($jsn);
    }

    /*
    * Method: uploadAttachemnts
    * Description: for deleting a quiz quetion
    * Author: Sayantani
    * Date: 04-June-2021
    */
    public function uploadAttachemnts(Request $request)
    {
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        $jsn['request'] = $request->all();
        $lesson = Lesson::where('id',$request->lesson_id)->first();
        $jsn['lesson'] = $lesson;
        $jsn['main_video'] = LessonFile::where('lesson_id',$lesson->id)->where('parent_id',0)->whereIn('filetype',['V','Y'])->first();
        if(@$request->file('file')){

            $file_ins['lesson_id'] = $lesson->id;
            $file_ins['filetype'] = 'D';
            $file_ins['parent_id'] = $jsn['main_video']->id;

            $name = explode('.', $request->file->getClientOriginalName())[0];
            $name = $name.'.'.@$request->file->getClientOriginalExtension();
            $file_name = time().'-'.rand(100000,999999).'.'.@$request->file->getClientOriginalExtension();;
            $mime_type = $request->file->getClientMimeType();

            $file = @$request->file;
            Storage::putFileAs('public/lessons/type_video/attachments', $file, $file_name);

            $file_ins['filename'] = $file_name;
            $file_ins['name'] = $name;
            $file_ins['mimetype'] = $mime_type;
            $file_ins['duration'] = @$request->duration;
            $jsn['attachment'] = LessonFile::create($file_ins);
            $jsn['status'] = "success";
            $jsn['message'] = \Lang::get('client_site.file_uploaded_successfully');
        } else {
            $jsn['status'] = "error";
            $jsn['message'] = \Lang::get('client_site.file_not_found');
        }
        return response()->json($jsn);
    }

    /*
    * Method: getSubcategories
    * Description: for getting all product subcategories
    * Author: Sayantani
    * Date: 07-June-2021
    */
    public function getSubcategories($id = null)
    {
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        if(@$id){
            $jsn['subcategories'] = ProductSubcategory::where('category_id',$id)->where('status', 'A')->get();
        }
        return response()->json($jsn);
    }


    /*
    * Method: uploadSlide
    * Description: for uploading an image slide
    * Author: Sayantani
    * Date: 04-June-2021
    */
    public function uploadSlide(Request $request)
    {
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        $jsn['request'] = $request;
        $lesson = Lesson::where('id',$request->lesson_id)->first();

        $file_ins['lesson_id'] = $lesson->id;
        $file_ins['description'] = @$request->description ? $request->description : NULL;

        if(@$request->file('file')){
            $name = explode('.', $request->file->getClientOriginalName())[0];
            $name = $name.'.'.@$request->file->getClientOriginalExtension();
            $file_name = time().'-'.rand(100000,999999).'.'.@$request->file->getClientOriginalExtension();
            $mime_type = $request->file->getClientMimeType();

            $file = @$request->file;
            Storage::putFileAs('public/lessons/type_presentation', $file, $file_name);

            $file_ins['filename'] = $file_name;
        }
        if(@$request->audio){
            $audio_name = time().'-'.rand(100000,999999).'.'.@$request->audio->getClientOriginalExtension();
            Storage::putFileAs('public/lessons/type_presentation/audio', $request->audio, $audio_name);
            $file_ins['audio'] = $audio_name;
            $file_ins['audio_duration'] = $request->audio_duration;
        }
        if(@$request->slide_id){
            $jsn['slide'] = Slide::where('id', $request->slide_id)->first();
            if(@$request->audio) @unlink(storage_path('app\public\lessons\type_presentation\audio\\'.@$jsn['slide']->audio));
            $jsn['slide']->update($file_ins);
            $jsn['status'] = "success";
            $jsn['message'] = \Lang::get('client_site.slide_updated_successfully');
        } else {
            $jsn['slide'] = Slide::create($file_ins);
            $jsn['status'] = "success";
            $jsn['message'] = \Lang::get('client_site.slide_created_successfully');
        }
        return response()->json($jsn);
    }

    /*
    * Method: deleteSlide
    * Description: for deleting a slide
    * Author: Sayantani
    * Date: 10-June-2021
    */
    public function deleteSlide($id)
    {
        $slide = Slide::where('id',$id)->first();
        if(@$slide){
            @unlink(storage_path('app\public\lessons\type_presentation\\'.@$slide->filename));
            @unlink(storage_path('app\public\lessons\type_presentation\audio\\'.@$slide->audio));
            if (strpos(@$slide->description, '<img') !== false) {
                preg_match_all( '@src="([^"]+)"@' , $lesson->description, $match );
                $srcs = array_pop($match);
                foreach($srcs as $src){
                    @unlink(storage_path('app\public\lessons\textarea_uploads\\'.basename(@$src)));
                }
            }
            $slide->delete();
            $jsn['status'] = "success";
            $jsn['message'] = \Lang::get('client_site.slide_deleted_successfully');
        } else {
            $jsn['status'] = "error";
            $jsn['message'] = \Lang::get('client_site.slide_not_found');
        }
        return response()->json($jsn);
    }

    /*
    * Method: previewLesson
    * Description: for previewing lesson as student
    * Author: Sayantani
    * Date: 10-June-2021
    */
    public function previewLesson($id)
    {
        $data['product'] = Product::where('id',$id)->where('status', '!=', 'D')->first();
        if(@$data['product']){
            if(Auth::id() != $data['product']->professional_id){
                session()->flash('error', \Lang::get('site.visit_not_allowed'));
                return redirect()->back();
            }
            $levels = [];
            $data['chapters'] = Chapter::where('course_id', $data['product']->id)->whereHas('getLessons', function($q) {
                $q = $q->where('status', '!=', 'D');
            })->get();
            foreach($data['chapters'] as $i=>$chapter){
                $chapter->progress_count = 0;
                if($chapter->getLessons()->count() == 0){
                    unset($data['chapters'][$i]);
                } else {
                    $lessons = Lesson::where('chapter_id', $chapter->id)->where('status', 'A')->get();
                    $chapter->total_lessons = $lessons->count();
                    $chapter->minLevel = min(Lesson::where('chapter_id', $chapter->id)->where('status', 'A')->pluck('level')->toArray());
                    $chapter->maxLevel = max(Lesson::where('chapter_id', $chapter->id)->where('status', 'A')->pluck('level')->toArray());
                    foreach($lessons as $lesson){
                        if($lesson->lesson_type == 'V'){
                            $lesson->main_video = LessonFile::where('lesson_id',$lesson->id)->where('parent_id',0)->first();
                        }
                        if($lesson->lesson_type == 'A'){
                            $lesson->main_audio = LessonFile::where('lesson_id',$lesson->id)->where('parent_id',0)->first();
                        }
                        if($lesson->lesson_type == 'Q'){
                            $lesson->question_count = Quiz::where('lesson_id',$lesson->id)->get()->count();
                        }
                        if($lesson->lesson_type == 'P'){
                            $lesson->slide_count = Slide::where('lesson_id',$lesson->id)->get()->count();
                        }
                        array_push($levels, $lesson->level);
                    }
                    $chapter->lessons = $lessons;
                }
            }
            array_values((array) $data['chapters']);
            $data['levels'] = $levels;
            return view('modules.products.preview_course')->with($data);
        } else {
            session()->flash('error', \Lang::get('site.product_not_found'));
            return redirect()->back();
        }
    }

    /*
    * Method: viewCourse
    * Description: for viewing course and course completion
    * Author: Sayantani
    * Date: 02-July-2021
    */
    public function viewCourse($id, $master_id = null)
    {
        $levels = [];
        $data['product'] = Product::where('id',$id)->where('status', '!=', 'D')->first();
        if(!@$data['product']){
            session()->flash('error', \Lang::get('site.product_not_found'));
            return redirect()->back();
        }
        // if(@$data['product']->professional_id == Auth::id()){
        //     session()->flash('error', \Lang::get('site.product_not_found'));
        //     return redirect()->back();
        // }
        $exists = ProductOrderDetails::where('product_id', $id)->whereHas('orderMaster', function($q){
            $q->where('user_id', Auth::id());
        })->first();
        // if(@$master_id) $data['master_id'] = $master_id;

        if(!@$exists && Auth::id() != $data['product']->professional_id){
            session()->flash('error', \Lang::get('site.visit_not_allowed'));
            return redirect()->back();
        }
        if(@$data['product']->professional_id != Auth::id()){
            $data['order'] = $exists;


            $date1 = date_create(date("Y-m-d"));
            $date2 = date_create($data['order']->orderMaster->order_date);

            if($date1 < date_create(date("Y-m-d", strtotime($data['product']->course_start_date)))){
                $str = \Lang::get('client_site.course_starts_on') . date("jS F, Y", strtotime($data['product']->course_start_date)) . \Lang::get('client_site.wait_till_course_start');
                session()->flash('error', $str);
                return redirect()->back();
            }
            $day_diff = date_diff($date1,$date2);
            $day_diff = (int)$day_diff->format("%a");
            if($day_diff > @$data['product']->content_available_days){
                session()->flash('error', \Lang::get('client_site.course_duration_over'));
                return redirect()->back();
            }
        }
        $data['chapters'] = Chapter::where('course_id', $data['product']->id)->whereHas('getLessons', function($q) {
            $q = $q->where('status', '!=', 'D');
        })->get();
        $maxMarks = $myMarks = 0;
        foreach($data['chapters'] as $i=>$chapter){
            $chapter->progress_count = 0;
            if($chapter->getLessons()->count() == 0){
                unset($data['chapters'][$i]);
            } else {
                $lessons = Lesson::where('chapter_id', $chapter->id)->where('status', 'A')->get();
                $chapter->total_lessons = $lessons->count();
                $maxMarks += $lessons->count();
                $chapter->minLevel = min(Lesson::where('chapter_id', $chapter->id)->where('status', 'A')->pluck('level')->toArray());
                $chapter->maxLevel = max(Lesson::where('chapter_id', $chapter->id)->where('status', 'A')->pluck('level')->toArray());
                $complete = true;
                foreach($lessons as $lesson){
                    $progress_exists = CourseProgress::where('user_id', Auth::id())->where('product_id', $id)->where('level', $lesson->level)->where('status','C')->first();
                    if(@$progress_exists){
                        $lesson->progress = true;
                        $myMarks += 1;
                        $chapter->progress_count += 1;
                    } else {
                        $lesson->progress = false;
                        $complete = false;
                    }
                    if($lesson->lesson_type == 'V'){
                        $lesson->main_video = LessonFile::where('lesson_id',$lesson->id)->where('parent_id',0)->first();
                    }
                    if($lesson->lesson_type == 'A'){
                        $lesson->main_audio = LessonFile::where('lesson_id',$lesson->id)->where('parent_id',0)->first();
                    }
                    if($lesson->lesson_type == 'Q'){
                        $lesson->question_count = Quiz::where('lesson_id',$lesson->id)->get()->count();
                    }
                    if($lesson->lesson_type == 'P'){
                        $lesson->slide_count = Slide::where('lesson_id',$lesson->id)->get()->count();
                    }
                    array_push($levels, $lesson->level);
                }
                $chapter->complete = $complete;
                $chapter->lessons = $lessons;
            }
        }
        $data['course_progress_percentage'] = $maxMarks > 0 ? floor(($myMarks/$maxMarks)*100) : 0;
        $data['last_visit_level'] = 0;
        $last_visit = CourseProgress::where('user_id', Auth::id())->where('product_id', $id)->where('current_lesson', 'Y')->first();
        if(@$last_visit) $data['last_visit_level'] = $last_visit->level;
        array_values((array) $data['chapters']);
        $data['levels'] = $levels;
        return view('modules.products.view_course')->with($data);
    }

    /*
    * Method: viewFreeLessons
    * Description: for viewing free lessons of the course
    * Author: Sayantani
    * Date: 05-Jan-2022
    */
    public function viewFreeLessons($slug)
    {
        $levels = [];
        $data['product'] = Product::where('slug',$slug)->where('status', '!=', 'D')->first();
        if(!@$data['product']){
            session()->flash('error', \Lang::get('site.product_not_found'));
            return redirect()->back();
        }
        // if(@$data['product']->professional_id == Auth::id()){
        //     session()->flash('error', \Lang::get('site.product_not_found'));
        //     return redirect()->back();
        // }
        $exists = ProductOrderDetails::where('product_id', $data['product']->id)->whereHas('orderMaster', function($q){
            $q->where('user_id', Auth::id());
        })->first();
        // if(@$master_id) $data['master_id'] = $master_id;

        // if(!@$exists && Auth::id() != $data['product']->professional_id){
        //     session()->flash('error', \Lang::get('site.visit_not_allowed'));
        //     return redirect()->back();
        // }
        if(@$data['product']->professional_id != Auth::id()){

        }
        $data['chapters'] = Chapter::where('course_id', $data['product']->id)->whereHas('getLessons', function($q) {
            $q = $q->where('status', '!=', 'D');
        })->get();
        $maxMarks = $myMarks = 0;
        foreach($data['chapters'] as $i=>$chapter){
            $chapter->progress_count = 0;
            if($chapter->getLessons()->count() == 0){
                unset($data['chapters'][$i]);
            } else {
                $lessons = Lesson::where('chapter_id', $chapter->id)->where('is_free_preview','Y')->where('status', 'A')->get();
                $chapter->total_lessons = $lessons->count();
                $maxMarks += $lessons->count();
                $chapter->minLevel = min(Lesson::where('chapter_id', $chapter->id)->where('status', 'A')->pluck('level')->toArray());
                $chapter->maxLevel = max(Lesson::where('chapter_id', $chapter->id)->where('status', 'A')->pluck('level')->toArray());
                $complete = true;
                foreach($lessons as $lesson){
                    $progress_exists = CourseProgress::where('user_id', Auth::id())->where('product_id', $data['product']->id)->where('level', $lesson->level)->where('status','C')->first();
                    if(@$progress_exists){
                        $lesson->progress = true;
                        $myMarks += 1;
                        $chapter->progress_count += 1;
                    } else {
                        $lesson->progress = false;
                        $complete = false;
                    }
                    if($lesson->lesson_type == 'V'){
                        $lesson->main_video = LessonFile::where('lesson_id',$lesson->id)->where('parent_id',0)->first();
                    }
                    if($lesson->lesson_type == 'A'){
                        $lesson->main_audio = LessonFile::where('lesson_id',$lesson->id)->where('parent_id',0)->first();
                    }
                    if($lesson->lesson_type == 'Q'){
                        $lesson->question_count = Quiz::where('lesson_id',$lesson->id)->get()->count();
                    }
                    if($lesson->lesson_type == 'P'){
                        $lesson->slide_count = Slide::where('lesson_id',$lesson->id)->get()->count();
                    }
                    array_push($levels, $lesson->level);
                }
                $chapter->complete = $complete;
                $chapter->lessons = $lessons;
            }
        }
        $data['course_progress_percentage'] = $maxMarks > 0 ? floor(($myMarks/$maxMarks)*100) : 0;
        $data['last_visit_level'] = 0;
        $last_visit = CourseProgress::where('user_id', Auth::id())->where('product_id', $data['product']->id)->where('current_lesson', 'Y')->first();
        if(@$last_visit) $data['last_visit_level'] = $last_visit->level;
        array_values((array) $data['chapters']);
        $data['levels'] = $levels;
        return view('modules.products.view_free_lessons')->with($data);
    }

    /*
    * Method: subCategorySearch
    * Description: search by sub category
    * Author: Puja
    * Date: 18-June-2021
    */

    public function subCategorySearch($slug){
        $data['category'] = ProductCategory::where('slug',$slug)->where('status', '!=', 'D')->first();
        $data['subcategory'] = ProductSubcategory::where('category_id',$data['category']->id)->where('status', '!=', 'D')->get();
        return view('modules.products.product_subcategory_search')->with($data);
    }


    public function import(Request $request){
        // Excel::import(new QuestionsImport, $request->file('file')->store('temp'));
        // return back();
        $jsn = [
                        "jsonrpc"   =>  "2.0"
        ];
        $file = $request->file('file');
        $handle = fopen($file, "r");
        $c = 0;
        if($file->getClientOriginalExtension() == "csv"){
            $status = "";
            while(($row = fgetcsv($handle, 1000, ",")) !== false)
            {
                if(($row[0] != null || $row[0] != "") && $c > 0){
                    $add['lesson_id']      = $request->lesson_id;
                    $add['question']       = $row[0];
                    $add['answer_1']       = $row[1];
                    $add['answer_2']       = $row[2];
                    $add['answer_3']       = $row[3];
                    $add['answer_4']       = $row[4];
                    $add['answer_5']       = $row[5];
                    $add['answer_6']       = $row[6];
                    $add['explanation']    = $row[7];
                    $add['correct_answer'] = $row[8];
                    Quiz::create($add);
                    $jsn['add'][$c] = $add;
                    $status = "success";
                }
                $c = $c + 1;
            }
            if($status == "success"){
                $jsn['status'] = "success";
                $jsn['message'] = \Lang::get('client_site.questions_added_successfully');
            } else {
                $jsn['status'] = "error";
                $jsn['message'] = \Lang::get('client_site.empty_imported_file');
            }
        } else {
            $jsn['status'] = "error";
            $jsn['message'] = \Lang::get('client_site.not_csv_file');
        }
        $jsn['format'] = $file->getClientOriginalExtension();
        return response()->json($jsn);

    }

    /*
    * Method: calcChapterDuration
    * Description: to calculate chapter duration
    * Author: Sayantani
    * Date: 22-June-2021
    */
    public function calcChapterDuration($id){
        $minutes = 0;
        $chapter = Chapter::find($id);
        $lessons = Lesson::where('chapter_id', $chapter->id)->where('status', 'A')->get();
        foreach($lessons as $lesson){
            $minutes += (int)$lesson->duration;
        }
        $h = floor ($minutes / 60);
        $m = $minutes - ($h * 60);
        $chapter->no_of_hours = $h;
        $chapter->no_of_minutes = $m;
        $chapter->save();
        return true;
    }

    /*
    * Method: calcCourseDuration
    * Description: to calculate course duration
    * Author: Sayantani
    * Date: 22-June-2021
    */
    public function calcCourseDuration($id){
        $minutes = 0; $duration = "";
        $product = Product::find($id);
        $chapters = Chapter::where('course_id', $product->id)->get();
        foreach($chapters as $chapter){
            $lessons = Lesson::where('chapter_id', $chapter->id)->where('status', 'A')->get();
            foreach($lessons as $lesson){
                $minutes += (int)$lesson->duration;
            }
        }

        $d = floor ($minutes / 1440);
        $h = floor (($minutes - $d * 1440) / 60);
        $m = $minutes - ($d * 1440) - ($h * 60);

        if($d != 0) $duration .= "{$d}d";
        if($h != 0) $duration .= " {$h}h";
        if($m != 0) $duration .= " {$m}m";

        $product->course_duration = $duration;
        $product->save();
        return true;
    }
    /*
    * Method: getVideo
    * Description: to get preview video
    * Author: Puja
    * Date: 23-June-2021
    */
    public function getVideo(Request $request){
        $response = [
            'jsonrpc'   =>  '2.0'
        ];

        if($request->params['id']){

            $lesson_file_video = LessonFile::where('id',$request->params['id'])->first();
            $lesson_video_type = $lesson_file_video->filetype;
            $lesson = Lesson::where('id',$lesson_file_video->lesson_id)->first();

            $video_file_attachment = LessonFile::where('parent_id',$lesson_file_video->id)->where('mimetype','like','%image%')->get();

            if ($lesson_file_video) {
                    $response['status'] =   1;
                    $response['lesson_file_video'] = $lesson_file_video;
                    $response['lesson'] = $lesson;
                    $response['lesson_video_type'] = $lesson_video_type;
                    $response['video_file_attachment'] = $video_file_attachment;
                } else {
                    $response['status'] = 0;
                }

        }
        return response()->json($response);
    }
    /*
    * Method: addWishlist
    * Description: to add product in wishlist
    * Author: Puja
    * Date: 23-June-2021
    */
    public function addWishlist(Request $request){

        $response = [
            'jsonrpc'   =>  '2.0'
        ];
        if ($request->params['product_id'] && @Auth::id()) {

            $product = Product::where('id',@$request->params['product_id'])->first();
            if(Auth::user()->id == $product->professional_id){
                $response['status'] = 4;
            }else{
                $user = Wishlist::where('product_id', $request->params['product_id'])->where('user_id', Auth::user()->id)->first();
                if ($user == null) {
                        $is_created = Wishlist::create([
                            'product_id'       =>  $request->params['product_id'],
                            'user_id'               =>  Auth::user()->id
                        ]);
                        if ($is_created) {
                            $response['status'] =   1;
                        } else {
                            $response['status'] = 0;
                        }
                } else {
                      $response['status'] = 2;
                }

            }

        } else {
            $response['status'] = 3;
        }
        return response()->json($response);
    }
    /*
    * Method:  removeWishlist
    * Description: to remove product from wishlist
    * Author: Puja
    * Date: 25-June-2021
    */
    public function removeWishlist(Request $request){

        $response = [
            'jsonrpc'   =>  '2.0'
        ];
        if ($request->params['product_id'] && @Auth::user()) {

            $user = Wishlist::where('product_id', $request->params['product_id'])->where('user_id', Auth::user()->id)->first();
            if ($user != null) {
                    $is_removed = Wishlist::where('id',$user->id)->delete();
                    if ($is_removed) {
                        $response['status'] =   1;
                    } else {
                        $response['status'] = 0;
                    }
            } else {
                  $response['status'] = 2;
            }
        } else {
            $response['status'] = 3;
        }
        return response()->json($response);

    }

    /*
    * Method:   findChapters
    * Description: to find chapters
    * Author: Puja
    * Date: 26-June-2021
    */
    public function findChapters(Request $request){

        $response = [
            'jsonrpc'   =>  '2.0'
        ];

        $product_id = @$request->params['product_id'];

        $key = @$request->params['key'];

        if(@$product_id && @$key){
            Product::where('id',@$product->id)->first();
            $chapters = Chapter::with('getLessons')->where('course_id',@$product_id)
            ->WhereHas('getLessons',function($q1) use($key){
                 $q1->where('lesson_title','like', '%' . $key . '%');

                        })

            ->get();
            if(@$chapters)
            {
                $response['status'] = 1;
                $response['chapters'] = $chapters;

            }else{

                $response['status'] = 0;
            }



        }
        return response()->json($response);

    }


    public function rateReveiw($slug){

        $data['product'] = Product::with('category', 'professional','subCategory')->where('slug',$slug)->where('status', '!=', 'D')->whereIn('status', ['A','I'])->where('admin_status', 'A')->first();
        if(!@$data['product']){
            session()->flash('error', \Lang::get('client_site.product_not_found'));
            return redirect()->back();
        }
        $ordered_products = ProductOrderDetails::whereHas('orderMaster', function($q){
            $q->where('user_id', Auth::id())->where('payment_status','P');
        })->where('order_status','A')->pluck('product_id')->toArray();

        if($data['product']->status == 'A' || in_array($data['product']->id, $ordered_products)){
            $data['chapters'] = Chapter::with('getLessons','getVideoLessons','getVideoLessons.getLessonFile')->where('course_id', @$data['product']->id)->where('set_lessons_status','P')->get();
            // $product = Product::where('slug',$slug)->first();
            $data['id'] = $data['product']->id;
            $data['prod_avg_rating'] = $data['product']->avg_rating ;


            $data['review']  = ReviewProduct::where('user_id',Auth::user()->id)->where('product_id',$data['product']->id)->first();
                if (@auth()->user()->id) {
                    $allCartData = Cart::with(['product'])->where('user_id', auth()->user()->id)->where('product_id', $data['product']->id)->first();
                } else {
                    $cart_id = Session::get('cart_session_id');
                    $allCartData = Cart::with(['product'])->where('cart_session_id', $cart_id)->where('product_id',$data['product']->id)->first();
                }
            $data['cartData'] = $allCartData;
            $data['orderedProducts'] = [];
                if(Auth::check()){
                    $data['orderedProducts'] = ProductOrderDetails::whereHas('orderMaster', function($q){
                        $q->where('user_id', Auth::id());
                    })->pluck('product_id')->toArray();
                }
                $prduct_ids = Product::where('status', '!=', 'D')->where('professional_id', $data['product']->professional->id)->get()->pluck('id')->toArray();
                $data['totalStudents'] = ProductOrderDetails::whereIn('product_id', $prduct_ids)->get()->count();
            return view('modules.products.rate_review_product')->with($data);
        } else {
            session()->flash('error', \Lang::get('client_site.product_not_found'));
            return redirect()->back();
        }
    }

    public function addrateReveiw(Request $request){

        $insert['product_id'] = @$request->product_id;
        $insert['rate'] = @$request->selected_star;
        $insert['review'] = @$request->review;
        $insert['user_id']    =Auth::user()->id;
        $data['product'] = Product::with('category', 'professional','subCategory')->where('id',$request->product_id)->where('status', '!=', 'D')->where('admin_status', 'A')->first();
        $data['chapters'] = Chapter::with('getLessons','getVideoLessons','getVideoLessons.getLessonFile')->where('course_id', @$data['product']->id)->where('set_lessons_status','P')->get();

        $previous_review = ReviewProduct::where('product_id',@$request->product_id)->where('user_id',Auth::user()->id)->first();

        if(@$previous_review){
            $rating_updated = ReviewProduct::where('id',@$previous_review->id)->update($insert);
        } else {
            $rating = ReviewProduct::create($insert);
        }


        if(@$rating){

            $data['review']  = ReviewProduct::where('user_id',Auth::user()->id)->where('product_id',$data['product']->id)->first();
            $total = ReviewProduct::where('product_id',$data['product']->id)->count();
            $avg = ReviewProduct::where('product_id',$data['product']->id)->avg('rate');

            Product::where('id',$data['product']->id)->update([

                'total_rating'=> $total,
                'avg_rating'  => $avg,

            ]);
            session()->flash('success', \Lang::get('client_site.placed_review_successfully'));
            return redirect()->back()->with($data);
        } else if(@$rating_updated) {
            $data['review']  = ReviewProduct::where('user_id',Auth::user()->id)->where('product_id',$data['product']->id)->first();
            $avg = ReviewProduct::where('product_id',$data['product']->id)->avg('rate');
            Product::where('id',$data['product']->id)->update([

                'avg_rating'  => $avg,

            ]);
            session()->flash('success', \Lang::get('client_site.review_updated_successfully'));
            return redirect()->back()->with($data);
        } else {
            session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
            return redirect()->back()->with($data);
        }


    }


    /*
    * Method:   postLessonQuestion
    * Description: to post a question for a lesson
    * Author: Sayantani
    * Date: 02-July-2021
    */
    public function postLessonQuestion(Request $request){
        $response = [
            'jsonrpc'   =>  '2.0'
        ];
        $lesson = Lesson::where('id',$request['params']['lesson_id'])->first();
        if(@$lesson){
            $question['user_id'] = Auth::id();
            $question['lesson_id'] = $lesson->id;
            $question['question'] = $request['params']['question'];
            $qs = LessonQuestion::create($question);

            $response['question'] = $qs;
            $response['status'] = "success";
            $response['message'] = "Question posted succesfully";
        } else {
            $response['status'] = "error";
            $response['message'] = "Something went wrong";
        }
        $response['request'] = $request->all();
        return response()->json($response);
    }

    /*
    * Method:   postLessonAnswer
    * Description: to post an answer for a lesson question
    * Author: Sayantani
    * Date: 02-July-2021
    */
    public function postLessonAnswer(Request $request){
        $response = [
            'jsonrpc'   =>  '2.0'
        ];
        $question = LessonQuestion::where('id',$request['params']['question_id'])->first();
        if(@$question){
            $answer['user_id'] = Auth::id();
            $answer['lesson_id'] = $question->lesson_id;
            $answer['question_id'] = $question->id;
            $answer['answers'] = $request['params']['answer'];
            $ans = LessonAnswer::create($answer);

            $question->tot_answers += 1;
            $question->save();

            $response['question'] = $question;
            $response['answer'] = $ans;
            $response['status'] = "success";
            $response['message'] = "Answer posted succesfully";
        } else {
            $response['status'] = "error";
            $response['message'] = "Something went wrong";
        }
        $response['request'] = $request->all();
        return response()->json($response);
    }

    /*
    * Method:   deleteLessonQuestion
    * Description: to delete a lesson question
    * Author: Sayantani
    * Date: 06-July-2021
    */
    public function deleteLessonQuestion($id){
        $response = [
            'jsonrpc'   =>  '2.0'
        ];
        $question = LessonQuestion::where('id',$id)->first();
        if(@$question){
            $answers = LessonAnswer::where('question_id',$id)->get();
            if(@$answers){
                foreach($answers as $answer){
                    $answer->delete();
                }
            }
            $response['status'] = "success";
            $response['message'] = "Question deleted succesfully";
            $question->delete();
        } else {
            $response['status'] = "error";
            $response['message'] = "Something went wrong";
        }
        return response()->json($response);
    }

    /*
    * Method:   deleteLessonAnswer
    * Description: to delete an answer for a lesson question
    * Author: Sayantani
    * Date: 06-July-2021
    */
    public function deleteLessonAnswer($id){
        $response = [
            'jsonrpc'   =>  '2.0'
        ];
        $answer = LessonAnswer::where('id',$id)->first();
        if(@$answer){
            $response['status'] = "success";
            $response['message'] = "Answer deleted succesfully";
            $answer->delete();
        } else {
            $response['status'] = "error";
            $response['message'] = "Something went wrong";
        }
        return response()->json($response);

    }

    /*
    * Method:   setLessonLevels
    * Description: to set levels of each lessons of the course
    * Author: Sayantani
    * Date: 02-July-2021
    */
    public function setLessonLevels($id){
        $product = Product::find($id);
        if(@$product){
            $chapters = Chapter::where('course_id', $product->id)->get();
            $level = 0;
            foreach($chapters as $chapter){
                $lessons = Lesson::where('chapter_id', $chapter->id)->get();
                foreach($lessons as $lesson){
                    // set lesson level
                    $lesson->level = $level;
                    $lesson->save();
                    // change levels in user progress table
                    CourseProgress::where('product_id', $id)->where('lesson_id', $lesson->id)->update(['level'=>$lesson->level]);
                    $level += 1;
                }
            }
        }

    }

    /*
    * Method:   userCourseProgress
    * Description: to set levels of each lessons of the course
    * Author: Sayantani
    * Date: 08-July-2021
    */
    public function userCourseProgress(Request $request){
        $response = [
            "jsonrpc"   =>  "2.0"
        ];
        $course = Product::where('id', $request['params']['course_id'])->first();
        if(@$course){
            $lesson = Lesson::where('id', $request['params']['lesson_id'])->first();
            $exists = CourseProgress::where('user_id', Auth::id())->where('product_id', $course->id)->where('level', $lesson->level)->first();
            if(@$exists && @$exists->status != 'C'){
                $exists->update(['status' => 'C']);
                $response['increment'] = true;
            }
            $chapter_complete = true;
            foreach($lesson->getChapter->getLessons as $lsn){
                $prog = CourseProgress::where('user_id', Auth::id())->where('product_id', $course->id)->where('lesson_id', $lsn->id)->where('status', 'C')->first();
                if(!@$prog) $chapter_complete = false;
            }
            $response['chapter'] = $lesson->getChapter;
            $response['chapter']['totalLessons'] = $lesson->getChapter->total_lessons;
            $response['chapter_complete'] = $chapter_complete;
            $response['level'] = $lesson->level;
            $response['status'] = "success";
            $response['message'] = "Progress updated succesfully";
        } else {
            $response['status'] = "error";
            $response['message'] = "Something went wrong";
        }
        return response()->json($response);
    }
    /*
    * Method:   assignProductTemplate
    * Description: to assign product with a certificate template
    * Author: Puja
    * Date: 08-07-2021
    */
    public function assignProductTemplate($id){

        $data['certificates'] = CertificateTemplateMaster::where('status','A')->where('professional_id', Auth::id())->get();
        $data['product'] = Product::where('id',$id)->first();
        $data['already_assigned_temp'] = ProductToCertificate::where('product_id',$data['product']->id)->first();
        return view('modules.products.product_certificate_template_assign')->with($data);
    }

    public function toAssignProductTemplate($tid,$pid){
        $already_assigned = ProductToCertificate::where('product_id',@$pid)->first();
        if(@$already_assigned != null){
            $already_assigned->delete();
        }
        $insert['tool_id'] =   @$tid;
        $insert['product_id']= @$pid;
        ProductToCertificate::create($insert);
        session()->flash('success', \Lang::get('client_site.cert_temp_assigned_successfully'));
        return redirect()->back();
    }

    public function allowDownload(Request $request){

        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        $order_id  =  @$request->params['order_id'];
        $order = ProductOrderDetails::with('profDetails', 'orderMaster', 'orderMaster.productCategoryDetails','orderMaster.userDetails','product')->where('id', @$order_id)->where('professional_id',Auth::user()->id)->first();
        $certificate_assign = ProductToCertificate::where('product_id',$order->product->id)->first();

        // $product_details = ProductOrderDetails::where('id',@$order_id)->where('product_id',@$product_id)->where('professional_id',Auth::user()->id)->where('is_allowed_certificate','N')->first();
        if(@$certificate_assign){
            $order_update = $order->update(['is_allowed_certificate' => 'Y']);
            if(@$order_update){
                $jsn['status'] = 1;
                $data['order'] = $order;
                $userData = User::where('id', @$order->orderMaster->user_id)->first();
                $orderData = $data['order'];
                $productDetails = $order->product;
                $usermail = $userData->email;
                // dd($userdata,$orderData, $productDetails, $usermail);
                Mail::send(new ProductAllowCertifiacte($userData, $orderData, $productDetails, $usermail));
            } else {
                $jsn['status'] = 0;
            }
        } else {
            $jsn['status'] = 2;
        }

        return response()->json($jsn);

    }

    /*
    * Method: userQuizProgress
    * Description: for saving user quiz progress
    * Author: Sayantani
    * Date: 09-July-2021
    */
    public function userQuizProgress(Request $request)
    {
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        $progress['user_answer'] = $request['params']['user_answer'];
        $progress['correct_answer'] = $request['params']['correct_answer'];
        $progress['is_correct'] = $request['params']['is_correct'];

        $exists = QuizProgress::where('user_id', Auth::id())->where('lesson_id', $request['params']['lesson_id'])->where('question_id', $request['params']['question_id'])->first();
        if(@$exists){
            $exists->update($progress);
            $jsn['progress'] = $exists;
        } else {
            $progress['user_id'] = Auth::id();
            $progress['lesson_id'] = $request['params']['lesson_id'];
            $progress['course_progress_id'] = $request['params']['course_progress_id'];
            $progress['question_id'] = $request['params']['question_id'];
            $jsn['progress'] = QuizProgress::create($progress);
        }
        $jsn['noOfCorrect'] = QuizProgress::where('user_id', Auth::id())->where('lesson_id', $request['params']['lesson_id'])->where('is_correct', 'Y')->count();
        $jsn['allProgress'] = QuizProgress::where('user_id', Auth::id())->where('lesson_id', $request['params']['lesson_id'])->get();
        return response()->json($jsn);
    }

    /*
    * Method: deleteQuizProgress
    * Description: for deleting quiz progess
    * Author: Sayantani
    * Date: 10-July-2021
    */
    public function deleteQuizProgress($id)
    {
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        $lesson = Lesson::where('id', $id)->first();
        $courseProgress = CourseProgress::where('user_id', Auth::id())->where('product_id', $lesson->getChapter->getProduct->id)->where('lesson_id', $id)->first();
        $progresses = QuizProgress::where('user_id', Auth::id())->where('lesson_id', $id)->get();
        if(@$courseProgress){
            $courseProgress->status = 'V';
            $courseProgress->save();
            $jsn['courseProgress'] = $courseProgress;
        }
        if(count($progresses) > 0){
            foreach($progresses as $progress){
                $progress-> delete();
            }
            $jsn['status'] = "success";
            $jsn['message'] = \Lang::get('client_site.quiz_progress_deleted');
        } else {
            $jsn['status'] = "error";
            $jsn['message'] = \Lang::get('client_site.quiz_was_not_taken');
        }
        return response()->json($jsn);

    }

    /*
    * Method: affiliateProducts
    * Description: for showing all affiliate products
    * Author: Sayantani
    * Date: 14-July-2021
    */
    public function affiliateProducts(Request $request, $slug = null)
    {
        if(Auth::user()->is_join_affiliate == 'N'){
            return redirect()->back()->with('error', \Lang::get('client_site.you_are_not_part_of_affiliate_program'));
        }
        $product = $maxprice = "";
        $productCategory = ProductCategory::where('status','!=','D')->get();
        $productSubCategory = ProductSubcategory::where('status','!=','D')->get();
        if(@$slug){
            $category = ProductCategory::where('status','!=','D')->where('slug',$slug)->first();
            $product =  Product::where(['status' => 'A', 'admin_status' => 'A', 'affiliate_program' => 'Y', 'category_id' => $category->id]);
            $maxprice = Product::select('price')->where(['status'=>'A','admin_status'=>'A', 'affiliate_program' => 'Y', 'category_id' => $category->id])->orderBy('price', 'desc')->first();
        } else {
            $product = Product::where(['status' => 'A', 'admin_status' => 'A', 'affiliate_program' => 'Y']);
            $maxprice = Product::select('price')->where(['status'=>'A','admin_status'=>'A', 'affiliate_program' => 'Y'])->orderBy('price', 'desc')->first();
        }

        $minprice = (object)['price' => '0.00'];
        if (Auth::id()) {
            $product = $product->where('professional_id', '!=', Auth::id());
        }
        if(@$request->all()){
            if(@$request->category){
                $product=$product->where('category_id',$request->category);
            }
            if(@$request->sub_category){
                $product=$product->where('subcategory_id',$request->sub_category);
            }
            if ($request->max_price || $request->min_price) {
                $product = $product->whereBetween('price', [$request->min_price, $request->max_price+1]);
            }
            if ($request->sort_by) {
                $product = $product->orderBy('price', $request->sort_by == "A" ? 'asc' : 'desc');
            }
            if ($request->keyword) {
                $product = $product->where(function ($where) use ($request) {
                    $where->where('title', 'like', '%' . $request->keyword . '%')
                        ->orWhere('description', '%'.$request->keyword.'%');
                });
            }
            $key=$request->all();
        }

        $ordered_products = [];
        if(Auth::check()){
            $ordered_products = ProductOrderDetails::whereHas('orderMaster', function($q){
                $q->where('user_id', Auth::id());
            })->pluck('product_id')->toArray();
        }
        $product =  $product->paginate(12);
        $myAffiliationList = AffiliateProducts::where('user_id', Auth::id())->pluck('product_id')->toArray();

        return view('modules.products.affiliate_products')->with([
            'allProductCategory' => $productCategory,
            'allProductSubCategory' => $productSubCategory,
            'key'            =>    @$key,
            'minprice'      =>  @$minprice,
            'maxprice'      =>  @$maxprice,
            'allProduct'    => @$product,
            'orderedProducts' => @$ordered_products,
            'myAffiliationList'=> @$myAffiliationList
        ]);
    }

    /*
    * Method: affiliateProductsToList
    * Description: to store the affiliated products
    * Author: Puja
    * Date: 14-July-2021
    */
    public function affiliateProductsToList(Request $request){

        $response = [
            'jsonrpc' => '2.0'
        ];
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 10; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        $insert['product_id'] = $request['params']['productId'];
        $insert['user_id'] = Auth::user()->id;
        $insert['affiliate_code'] = $randomString;

        $exists = AffiliateProducts::where('user_id', Auth::id())->where('product_id', $request['params']['productId'])->first();
        if(!@$exist){
            AffiliateProducts::create($insert);
            $response['status'] = 'success';
            $response['message'] = \Lang::get('site.added_to_affiliate_list');
        } else {
            $response['status'] = 'error';
            $response['message'] = \Lang::get('client_site.something_went_be_wrong');
        }
        return response()->json($response);

    }

    /*
    * Method: userAffiliationLinksList
    * Description: for showing all products authenticated user is an affiliate to.
    * Author: Sayantani
    * Date: 14-July-2021
    */
    public function userAffiliationLinksList()
    {
        $data['affiliate_products'] = AffiliateProducts::with('product')->where('user_id', Auth::id())->get();
        return view('modules.user.affiliate_links_list')->with($data);
    }

    /*
    * Method: checkCookie
    * Description: to check cookie value
    * Author: Sayantani
    * Date: 15-July-2021
    */
    public function checkCookie($name){
        $cookie = (array) json_decode(Cookie::get($name));
        dd($cookie);
    }

    /*
    * Method: affiliateProductsRemoveList
    * Description: to remove the affiliated products
    * Author: Sayantani
    * Date: 15-July-2021
    */
    public function affiliateProductsRemoveList($id){

        $response = [
            'jsonrpc' => '2.0'
        ];
        $aff = AffiliateProducts::where('user_id', Auth::id())->where('product_id', $id)->first();
        if(@$aff){
            $aff->delete();
            session()->flash('success', \Lang::get('site.removed_from_list'));
            $response['status'] = 'success';
            $response['message'] = \Lang::get('site.removed_from_list');
        } else {
            $response['request'] = $id;
            $response['status'] = 'error';
            $response['message'] = \Lang::get('client_site.something_went_be_wrong');
        }
        return response()->json($response);

    }

    /*
    * Method: userAffiliationEarnings
    * Description: to show affiliate user's earnings
    * Author: Sayantani
    * Date: 15-July-2021
    */
    public function userAffiliationEarnings(){
        $data['earnings'] = ProductOrderDetails::where('affiliate_id', Auth::id())->get();
        $due = ProductOrderDetails::with(['paymentProductDetails'])->whereHas('paymentProductDetails',function($a){
            $a->where('affiliate_balance_status','R');
        })->where('affiliate_id', Auth::id())->get();
        $pay= ProductOrderDetails::with(['paymentProductDetails'])->whereHas('paymentProductDetails',function($a){
            $a->where('affiliate_balance_status','W');
        })->where('affiliate_id', Auth::id())->get();
        $total= ProductOrderDetails::where('affiliate_id', Auth::id())->get();
        $data['due']=$due->sum('affiliate_commission');
        $data['paid']=$pay->sum('affiliate_commission');
        $data['totalEarning']=$total->sum('affiliate_commission');

        return view('modules.user.affiliate_earnings')->with($data);
    }

    public function previewTemplate($ord_id){
        $order = ProductOrderDetails::with('orderMaster','product')->where('id',$ord_id)->first();
        $assigned_certificate = ProductToCertificate::where('product_id',$order->product->id)->first();
        $certificate_template = $assigned_certificate->getTool;
        $certificatedesc = $assigned_certificate->getTool->content;
        $prof_data = User::where('id',Auth::user()->id)->first();
        $user_data = User::where('id',$order->orderMaster->user_id)->first();
        $course = $order->product;
        $img = "";
        if(@$prof_data['signature'])
            $img = "<img src='" . url('storage/app/public/uploads/signature/' . @$prof_data['signature']) . "' height='50px'/>";
        else
            $img = @$prof_data['nick_name'] ?? @$prof_data['name'];

        $certificatedesc = str_replace('{{Nome do aluno}}', @$user_data['name'], $certificatedesc);
        $certificatedesc = str_replace('{{Nome do curso}}', @$course->title, $certificatedesc);

        return view('modules.products.preview_certificate_template', [
            'template'          => $certificate_template,
            'certificatedesc'   => $certificatedesc,
            'order'             => $order
        ]);
    }

    public function searchReview(Request $request){
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];

        $keyword = $request['params']['key'];
        $rating = $request['params']['rating'];
        $data['review'] = ReviewProduct::with('getCustomer')->where('product_id',$request['params']['prod_id']);

        $data['review'] = $data['review']->where('review','LIKE','%'.$keyword.'%')
        ->orWhereHas("getCustomer", function ($query) use ($keyword) {
                        $query->where("name", 'LIKE', '%' .$keyword. '%');
                    });
        $data['review'] = $data['review']->get();

        $data['count'] = count($data['review']);

        if($data['review']){
            $jsn['review'] = $data['review'];
            $jsn['count'] = $data['count'];
            $jsn['status'] = 1;

        }else{
            $jsn['status'] = 0;
        }
        return response()->json($jsn);



    }

    public function searchRate(Request $request){
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];

        $rate = $request['params']['rating'];
        $prod_id = $request['params']['prod_id'];
        $data['rate'] = ReviewProduct::where('product_id',$prod_id);
        if($rate == 5 || $rate == 4 || $rate == 3 || $rate == 2 || $rate == 1){
            $data['rate'] = $data['rate']->where('rate',$rate);
        }
        if($rate == 0){
            $data['rate'] = $data['rate']->whereBetween('rate',[1,5]);
        }
        $data['rate'] = $data['rate']->get();
        $data['count'] = count($data['rate']);
        if($data['rate']){
            $jsn['rate'] = $data['rate'];
            $jsn['count'] = $data['count'];

            $jsn['status'] = 1;

        }else{
            $jsn['status'] = 0;
        }
        return response()->json($jsn);


    }
    public function searchCatSubcat(Request $request){
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];

        $cat_slug = $request['params']['cat'];
        $sub_cat_slug = $request['params']['scat'];

        $cat = ProductCategory::where('slug',$cat_slug)->first();
        $sub_cat = ProductSubcategory::where('category_id',$cat->id)->where('slug',$sub_cat_slug)->first();
        $products = Product::where('category_id',$cat->id)->where(['status' => 'A', 'admin_status' => 'A'])->where('subcategory_id',$sub_cat->id)->get();

        if(@$products){
            $jsn['result'] = $products;
            $jsn['status'] = 1;

        }else{
            $jsn['status'] = 0;
        }
        return response()->json($jsn);
    }
    public function searchCatSubcatAffiliate(Request $request){

        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];

        $cat_slug = $request['params']['cat'];
        $sub_cat_slug = $request['params']['scat'];

        $cat = ProductCategory::where('slug',$cat_slug)->first();
        $sub_cat = ProductSubcategory::where('category_id',$cat->id)->where('slug',$sub_cat_slug)->first();
        $products = Product::where('category_id',$cat->id)->where(['status' => 'A', 'admin_status' => 'A'])->where('subcategory_id',$sub_cat->id)->get();

        if(@$products){
            $jsn['result'] = $products;
            $jsn['status'] = 1;

        }else{
            $jsn['status'] = 0;
        }
        return response()->json($jsn);

    }

}
