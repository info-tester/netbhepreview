<?php

namespace App\Http\Controllers\Modules\Booking;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\User;
use App\Models\Booking;
use App\Models\UserAvailability;
use App\Models\Timezone;
use App\Models\UserToCard;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use Moip\Moip;
use Moip\Auth\BasicAuth;
use Moip\Auth\OAuth;
use Moip\Resource\Customer;

use App\Mail\UserBooking;
use App\Mail\ProfessionalBooking;
use App\Models\Content;
use App\Models\Payment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Models\Coupon;
use App\Models\ReferralDiscount;
use Illuminate\Support\Facades\Storage;
use App\Models\AdminBankAccount;
use App\Mail\BankAccountPayment;
use App\Models\Commission;
use App\Mail\UserBankAccountBooking;

class BookingController extends Controller
{
    protected $CPF_CNPJ, $numeral, $tipo;
    protected $adminAccessToken;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->adminAccessToken = getAdminToken();
    }


    /**
     *method: index
     *created By: Abhisek
     *description: For Search
     */

    public function index($slug = null)
    {
        $nowtime = date('Y-m-d');
        $user = User::with('userDetails.categoryName')->where('slug', $slug)->first();
        if(!@$user){
            session()->flash('error', \Lang::get('client_site.user_not_found'));
            return redirect()->back();
        }
        $parCat = $chldCat = [];
        $i = 0;
        $j = 0;
        $profile = 0;
        $required = [
            0   => 'country_id',
            1   => 'state_id',
            2   => 'city',
            3   => 'dob',
            4   => 'street_number',
            5   => 'street_name',
            6   => 'complement',
            7   => 'district',
            8   => 'zipcode',
            9  => 'area_code'
        ];
        $av = UserAvailability::where('user_id', $user->id)->where('slot_start', '>', now(env('TIMEZONE'))->addHour('3')->toDateTimeString())->whereNotIn('slot_start', getUsedTimeSlotsForAllDates($user->id))->get();
        $avlDay = [];
        $avlDay1 = [];
        foreach ($av->toArray() as $a) {
            $avl = [];
            $st = '';
            foreach ($a as $k => $v) {
                if ($k == 'slot_start') {
                    $avl[$k] = toUserTime($v, 'Y-m-d\TH:i:s');
                    $avl['time_start'] = toUserTime($v, 'g:i A');
                    $st = toUserTime($v, 'Y-m-d');
                } else if ($k == 'slot_end') {
                    $avl[$k] = toUserTime($v, 'Y-m-d\TH:i:s');
                    $avl['time_end'] = toUserTime($v, 'g:i A');
                } else {
                    $avl[$k] = $v;
                }
            }
            array_push($avlDay1, $avl);
            if (!in_array($st, $avlDay)) {
                array_push($avlDay, $st);
            }
        }
        for ($i = 0; $i < sizeof($required); $i++) {
            $str = $required[@$i];
            if (Auth::guard('web')->user()->$str == null) {
                $profile++;
                break;
            } elseif (Auth::guard('web')->user()->$str <= 0 && !is_string(Auth::guard('web')->user()->$str)) {
                $profile++;
                break;
            }
        }
        if ($user == null) {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        } else {
            if ($user->slug == Auth::user()->slug || $user->id == Auth::user()->id) {
                session()->flash('error', \Lang::get('client_site.unauthorize_access'));
                return redirect()->back();
            } else {
                foreach ($user->userDetails as $key) {
                    if ($key->level == 0) {
                        $parCat[$i] = $key->category_id;
                    }
                    if ($key->level > 0 || $key->level != "") {
                        $chldCat[$i] = $key->category_id;
                    }
                    $i++;
                }
                $category = Category::orderBy('name')->where('status', 'A')->get();
                $time = Timezone::get();
                $country = Country::orderBy('name')->get();
                $state = State::orderBy('name')->get();
                $city = City::orderBy('name')->get();

                $date = date('d-m-Y');
                if (session()->has('slotStart')) {
                    $date = date('Y-m-d', strtotime(session()->get('slotStart')));
                    session()->forget('slotStart');
                }
                $slots = UserAvailability::where('user_id', $user->id)->where('date', '>=', now(env('TIMEZONE'))->addHour('3')->toDateString())
                    ->where('slot_start', '>=', date('Y-m-d', strtotime('-1 day ' . $date)))
                    ->where('slot_start', '<=', date('Y-m-d', strtotime('+1 day ' . $date)))
                    ->where('slot_start', '>', now(env('TIMEZONE'))->addHour('3')->toDateTimeString())->orderBy('slot_start', 'ASC')
                    ->whereNotIn('slot_start', getUsedTimeSlotsForDate($user->id, date('Y-m-d', strtotime($date))))->get();
                $fromTotime = [];
                foreach (getUserTimeSlotsForDate($date, $slots) as $a) {
                    $avl = [];
                    foreach ($a as $k => $v) {
                        if ($k != 'slot_start' && $k != 'slot_end') {
                            $avl[$k] = $v;
                        } else {
                            $avl[$k] = toUserTime($v, 'H:i');
                        }
                    }
                    array_push($fromTotime, $avl);
                }
                $bookingPage = Content::where('id', 12)->first();
                return view('modules.booking.booking')->with([
                    'user'          =>  @$user,
                    'category'      =>  @$category,
                    'parent'        =>  @$parCat,
                    'child'         =>  @$chldCat,
                    'profile'       =>  @$profile,
                    'time'          =>  @$time,
                    'country'       =>  @$country,
                    'state'         =>  @$state,
                    'city'          =>  @$city,
                    'date'          =>  @date('d-m-Y', strtotime($date)),
                    'fromTotime'    =>  @$fromTotime,
                    'avlTime1'      => json_encode($avlDay1),
                    'avlDay'        => json_encode($avlDay),
                    'bookingPage'   =>  @$bookingPage,
                    // 'time'          =>  @$time
                ]);
            }
        }
    }

    public function getDate(Request $request)
    {
        $response = [
            'jsonrpc'   =>  '2.0'
        ];
        if ($request->params['date']) {
            $date = UserAvailability::where('user_id', $request->params['user_id'])->where('date', '>=', date('Y-m-d'))
                ->where('slot_start', '>=', date('Y-m-d', strtotime('-1 day ' . $request->params['date'])))
                ->where('slot_start', '<=', date('Y-m-d', strtotime('+1 day ' . $request->params['date'])))
                ->where('slot_start', '>', now(env('TIMEZONE'))->addHour('3')->toDateTimeString())
                ->orderBy('slot_start', 'ASC')
                ->whereNotIn('slot_start', getUsedTimeSlotsForDate($request->params['user_id'], date('Y-m-d', strtotime($request->params['date']))))->get();
            $avlDay1 = [];
            foreach (getUserTimeSlotsForDate($request->params['date'], $date) as $a) {
                $avl = [];
                foreach ($a as $k => $v) {
                    if ($k != 'slot_start' && $k != 'slot_end') {
                        $avl[$k] = $v;
                    } else {
                        $avl[$k] = toUserTime($v, 'H:i');
                    }
                }
                array_push($avlDay1, $avl);
            }
            $response['status'] = 1;
            $response['result'] = $avlDay1;
        }
        return response()->json($response);
    }


    /**
     * For fetching sub-category.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function getSubcat(Request $request)
    {
        $response = [
            'jsonrpc'   =>  '2.0'
        ];
        if ($request->params['cat']) {
            $cat = Category::where('parent_id', $request->params['cat'])->orderBy('name')->get();
            if ($cat != null) {
                $response['status'] = 1;
                $response['result'] = $cat;
            } else {
                $response['status'] = 0;
            }
        }
        return response()->json($response);
    }

    public function storeBooking($slug, Request $request)
    {
        $user = User::where('slug', $slug)->first();

        date_default_timezone_set(@$user->userTimeZone->timezone);
        $token = null;
        if ($user == null || Auth::user()->slug == $user->slug) {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        } else {
            $request->validate([
                'category'              =>  'required',
                'date'                  =>  'required',
                'time_slot'             =>  'required',
                'booking_type'             =>  'required',
                'msg'                   =>  'required'
            ], [
                'time_slot.required'    =>  'selecione um horário.'
            ]);

            $time_slot = explode("-", @$request->time_slot);
            $startTime = toUTCTime(date('Y-m-d', strtotime($request->date)) . ' ' . $time_slot[0] . ':00', 'H:i:s');
            $slotStartDate = toUTCTime(date('Y-m-d', strtotime($request->date)) . ' ' . $time_slot[0] . ':00', 'Y-m-d H:i:s');
            $endTime = toUTCTime(date('Y-m-d', strtotime($request->date)) . ' ' . $time_slot[1] . ':00', 'H:i:s');
            $bookingDate = date('Y-m-d', strtotime($slotStartDate));
            if (@$time_slot[1] && $time_slot[1] == '00:00') {
                $endTime_ = strtotime($time_slot[1]) + (24 * 60 * 60);
                $duration = abs($endTime_ - strtotime($time_slot[0])) / 60;
            } else {
                $duration = abs(strtotime($time_slot[1]) - strtotime($time_slot[0])) / 60;
            }
            $is_avlbl = UserAvailability::where(['user_id' =>  @$user->id, 'slot_start'  =>  $slotStartDate])
                ->first();
            if ($is_avlbl == null) {
                session()->flash('error', \Lang::get('client_site.currently_time_slot_is_not'));
                return redirect()->back();
            } else {
                $isAval = Booking::where([
                    'start_time'        =>  $startTime,
                    'end_time'          =>  $endTime,
                    'date'              =>  $bookingDate,
                    'professional_id'   =>  $user->id
                ])->where('order_status','!=','R')->first();

                if ($isAval != null) {
                    session()->flash('error', \Lang::get('client_site.currently_booking_is_not'));
                    return redirect()->back();
                }
                $is_booking_free = Booking::where([
                    'professional_id'   =>  $user->id,
                    'user_id'           =>  auth()->id(),
                    'free_session_id'   => $user->id,
                ])->first();
                // dd($user->free_session_id);
                if (@$is_booking_free) {
                    $request->session()->forget('free_session_id');
                }
                $free_session_id = @session()->get('free_session_id');
                if (@$user->free_session_number > 0 && @$free_session_id == $user->free_session_id) {
                    // dd($user->free_session_number);
                    $is_created = Booking::create([
                        'user_id'           =>  auth()->id(),
                        'token_no'          =>  $token = strtoupper(str_random(12)),
                        'professional_id'   =>  @$user->id,
                        'category_id'       =>  @$request->category ? $request->category : 0,
                        'sub_category_id'   =>  @$request->sub_category ? $request->sub_category : 0,
                        'date'              =>  $bookingDate,
                        'start_time'        =>  $startTime,
                        'end_time'          =>  $endTime,
                        'duration'          =>  @$duration,
                        'msg'               =>  @$request->msg,
                        'booking_type'      =>  @$request->booking_type,
                        'amount'            =>  0,
                        'order_status'      => "A",
                        'payment_status'    => "I",
                        'free_session_id'   => $user->id,
                    ]);
                    if ($is_created) {
                        User::where('id', $user->id)->decrement("free_session_number", 1);
                    }
                } else {
                    // dd($user->id);
                    $is_created = Booking::create([
                        'user_id'           =>  auth()->id(),
                        'token_no'          =>  $token = strtoupper(str_random(12)),
                        'professional_id'   =>  @$user->id,
                        'category_id'       =>  @$request->category ? $request->category : 0,
                        'sub_category_id'   =>  @$request->sub_category ? $request->sub_category : 0,
                        'date'              =>  $bookingDate,
                        'start_time'        =>  $startTime,
                        'end_time'          =>  $endTime,
                        'duration'          =>  @$duration,
                        'msg'               =>  @$request->msg,
                        'booking_type'      =>  @$request->booking_type,
                        'amount'            =>  $user->rate == 'H' ? round($user->rate_price / 60 * @$duration) : round($user->rate_price * @$duration),
                        'order_status'      =>  Auth::guard('web')->user()->is_credit_card_added == "N" ? "AA" : "A",
                        'payment_status'    => "I",
                        'sub_total'         => $user->rate == 'H' ? round($user->rate_price / 60 * @$duration) : round($user->rate_price * @$duration),
                        'installment_charge'    => @$user->installment_charge
                    ]);
                }
                if ($is_created) {
                    // $userdata = User::where('id', Auth::id())->first();
                    // $userdata1 = User::where('id', $user->id)->first();
                    // $professionalmail = $userdata1->email;
                    // $usermail = $userdata->email;
                    // $bookingdata = $is_created;

                    // // // for send mail to user
                    // Mail::send(new UserBooking($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));

                    // // // for send mail to professional
                    // Mail::send(new ProfessionalBooking($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));

                    session()->flash('success', @Auth::user()->is_credit_card_added == "N" ? \Lang::get('client_site.your_booking_was_initiated') : \Lang::get('client_site.successful_booked_your_request'));
                    return redirect()->route('booking.sces', ['token' => $token]);
                } else {
                    session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
                    return redirect()->back();
                }
            }
        }
    }

    public function checkDate(Request $request)
    {
        $response = [
            'jsonrpc'   =>  '2.0'
        ];
        if ($request->params['date']) {
            $userDetails = User::where('id', $request->params['user_id'])->first();
            $userDetails = $userDetails->load('userTimeZone');

            date_default_timezone_set(@$userDetails->userTimeZone->timezone);
            $day = date('l', strtotime($request->params['date']));
            $days = [
                'Sunday'    =>  1,
                'Monday'    =>  2,
                'Tuesday'   =>  3,
                'Wednesday' =>  4,
                'Thursday'  =>  5,
                'Friday'    =>  6,
                'Saturday'  =>  7
            ];
            $duration = date('H:i', strtotime($request->params['start_time'] . ' +' . $request->params['duration'] . ' minutes'));

            $is_avlbl = UserAvailability::where([
                'user_id'       =>  @$request->params['user_id'],
                'day'           =>  @$days[@$day]
            ])
                ->where('from_time', '<=', $request->params['start_time'])
                ->where('to_time', '>=', $duration)
                ->first();

            if ($is_avlbl == null) {
                $response['status'] = 1;
            } else {
                $response['status'] = 0;
            }
        }
        return response()->json($response);
    }

    public function bookingSuccess($slug)
    {
        $authUser = Auth::guard('web')->user();
        $booking = Booking::where('token_no', $slug)
            ->where('user_id', @Auth::id())
            ->first();
        $profile = 0;
        $required = [
            0   => 'country_id',
            1   => 'state_id',
            2   => 'city',
            3   => 'dob',
            4   => 'street_number',
            5   => 'street_name',
            6   => 'complement',
            7   => 'district',
            8   => 'zipcode',
            9  => 'area_code'
        ];

        for ($i = 0; $i < sizeof($required); $i++) {
            $str = $required[@$i];
            if ($authUser->$str == null) {

                $profile++;
                break;
            } elseif ($authUser->$str <= 0 && !is_string($authUser->$str)) {
                $profile++;
                break;
            }
        }
        if ($booking == null) {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        } else {
            if ($authUser->is_credit_card_added == 'Y' && $authUser->customer_id != '' && $booking->moip_order_id == '' && $booking->amount != 0 && $booking->payment_type == 'C') {
                $this->chargeUser($slug);
            }
            if ($booking->amount == 0 && $booking->payment_status == 'I') {
                $this->freeSessionPayment($slug);
            }
            if ($booking->payment_type == 'BA' && $booking->payment_status == 'I') {
                $this->bankAccountPayment($slug);
            }
            $booking = Booking::where('token_no', $slug)
                ->where('user_id', @Auth::id())
                ->first();
            $booking = $booking->load('parentCatDetails', 'childCatDetails', 'profDetails');
            $time = Timezone::get();
            $country = Country::orderBy('name')->get();
            $state = State::orderBy('name')->get();
            $accountInfo = AdminBankAccount::first();
            if (@$booking->payment_status == 'F') {
                session()->flash('error', 'payment failed');
                return redirect()->route('load_user_dashboard');
            }
            $curServerTime = strtotime((now(env('TIMEZONE'))->addHour('24')->toDateTimeString()));
            // $booking_time = date('Y-m-d H:i:s',strtotime(toUTCTime(date('Y-m-d H:i:s', strtotime($booking->date . ' ' . $booking->start_time)))));
            $booking_time = strtotime($booking->date . ' ' . $booking->start_time);
            $is_grater_24hrs = 0;
            if ($curServerTime < $booking_time) {
                $is_grater_24hrs = 1;
            }
            return view('modules.booking.booking_success')->with([
                'booking'   =>  @$booking,
                'profile'   =>  @$profile,
                'time'      =>  @$time,
                'country'   =>  @$country,
                'state'     =>  @$state,
                'accountInfo' => @$accountInfo,
                'is_grater_24hrs'=>@$is_grater_24hrs
            ]);
        }
    }


    /**
     * For Completing Customer Profile
     *
     * @method completeProfile
     * @author Abhisek
     */
    public function completeProfile(Request $request)
    {
        try {
            // if (@$request->is_profile > 0) {
            $request->validate([
                'name'              =>  "required",
                'phone_no'          => 'required|digits:9|unique:users,mobile,' . Auth::id(),
                'country'           =>  "required",
                'state'             =>  "required",
                'city'              =>  "required",
                'dob'               =>  "required",
                'street_number'     =>  "required",
                'street_name'       =>  "required",
                'complement'        =>  "required",
                'district'          =>  "required",
                'zipcode'           =>  "required",
                'area_code'         =>  "required",
                'cpf_no'            =>  "required|unique:users,cpf_no," . Auth::id()
            ]);
            $data = [
                'name'              =>  $request->name,
                'mobile'            =>  $request->phone_no,
                'country_id'        =>  $request->country,
                'state_id'          =>  $request->state,
                'city'              =>  $request->city,
                'dob'               =>  date('Y-m-d', strtotime($request->dob)),
                'street_number'     =>  $request->street_number,
                'street_name'       =>  $request->street_name,
                'complement'        =>  $request->complement,
                'district'          =>  $request->district,
                'area_code'         =>  $request->area_code,
                'zipcode'           =>  $request->zipcode,
                'cpf_no'            =>  $request->cpf_no
            ];
            $user = User::where('id', Auth::id())->update($data);

            $this->CPF_CNPJ = $request->cpf_no;
            if (!$this->getNumeral()) {
                session()->flash('error', \Lang::get('client_site.invalid_cpf_number'));
                return redirect()->back();
            }
            // }

            if (@$user || @$is_profile <= 0) {
                $request->validate([
                    'card_name' => "required",
                    'card_cpf_no' => "required",
                    // 'card_dob' => "required",
                    'dob_year' => "required",
                    'dob_month' => "required",
                    'dob_day' => "required",
                    'card_phonecode' => "required",
                    'card_area_code' => "required|digits:2",
                    'card_mobile' => 'required|digits:9',
                ]);
                $dob= $request->dob_year.'-'. $request->dob_month.'-'. $request->dob_day;
                // dd($dob);
                $this->CPF_CNPJ = $request->card_cpf_no;
                if (!$this->getNumeral()) {
                    session()->flash('error', \Lang::get('client_site.invalid_cpf_number'));
                    return redirect()->back();
                }
                $updatedUser = User::where('id', Auth::id())->first();
                $moip = env('PAYMENT_ENVIORNMENT', 'sandbox') == 'live' ? new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_PRODUCTION) : new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_SANDBOX);
                $customer = $moip->customers()
                    ->setOwnId(uniqid())
                    ->setFullname(@$updatedUser->name)
                    ->setEmail(@$updatedUser->email)
                    ->setBirthDate(@$updatedUser->dob)
                    ->setTaxDocument(@$updatedUser->cpf_no)
                    ->setPhone(@$updatedUser->area_code, @$updatedUser->mobile)
                    ->addAddress('BILLING', @$updatedUser->street_name, @$updatedUser->street_number, @$updatedUser->district, @$updatedUser->city, strtoupper(substr(@$updatedUser->stateName->name, 0, 2)), @$updatedUser->zipcode, @$updatedUser->complement)
                    ->addAddress('SHIPPING', @$updatedUser->street_name, @$updatedUser->street_number, @$updatedUser->district, @$updatedUser->city, substr(@$updatedUser->stateName->name, 0, 2), @$updatedUser->zipcode, @$updatedUser->complement)->create();

                $card = $moip->customers()->creditCard()
                    ->setExpirationMonth($request->expiry_month)
                    ->setExpirationYear($request->expiry_year)
                    ->setNumber($request->cardnumber)
                    ->setCVC($request->cvc)
                    ->setFullName($request->card_name)
                    ->setBirthDate($dob)
                    ->setTaxDocument('CPF', $request->card_cpf_no)
                    ->setPhone($request->card_phonecode, $request->card_area_code, $request->card_mobile)
                    ->create(@$customer->getId());

                if (!@$card->getId()) {
                    session()->flash('error', \Lang::get('client_site.internal_server_error'));
                    return redirect()->back();
                }
                $update = User::where('id', Auth::id())->update([
                    'customer_id' => @$customer->getId(),
                    'is_credit_card_added' => @$card->getStore() == true ? 'Y' : 'N'
                ]);
                $is_Created = UserToCard::create([
                    'user_id' => Auth::id(),
                    'card_name' => @$request->card_name,
                    'cpf_no' => @$request->card_cpf_no,
                    'phonecode' => @$request->card_phonecode,
                    'area_code' => @$request->card_area_code,
                    'mobile' => @$request->card_mobile,
                    'dob' => @$dob,
                    'card_id' => @$card->getId(),
                    'first_six' => @$card->getFirst6(),
                    'last_four' => @$card->getLast4(),
                    'card_brand' => @$card->getBrand(),
                    'cred_type' => "CREDIT_CARD",
                    'card_response' => json_encode($card),
                ]);
                if (@$is_Created) {
                    Booking::where('token_no', $request->order_no)->update([
                        'order_status'  =>  'A'
                    ]);
                    session()->flash('success', \Lang::get('client_site.now_your_profile_is'));
                    return redirect()->back();
                } else {
                    session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
                    return redirect()->back();
                }
                //}
                /* catch(\Exception $e){
                 session()->flash('error', 'Internal server error.');
                return redirect()->back();
            }*/
            } else {
                session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
                return redirect()->back();
            }
        } catch (\Moip\Exceptions\ValidationException $e) {
            session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
            return redirect()->back();
        } catch (Exceptions\UnexpectedException $e) {
            session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
            return redirect()->back();
        }
         catch (Exception $e) {
            session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
            return redirect()->back();
        }
    }

    /**
     * charge user after booking is complete
     *
     * @method: chargeUser
     */
    private function chargeUser($token)
    {
        $user_id = Booking::where('token_no', $token)->first()->load('parentCatDetails', 'childCatDetails', 'profDetails');

        $cardDetails = UserToCard::where('user_id', $user_id->user_id)->first();
        $access_token = getAdminToken();

        $customer = User::where([
            'id' => @$user_id->user_id
        ])->first();
        // if($user_id->is_coupon == 'Y'){
        //     // In Here the Primary account is the Admin account and the secondary account is the professional account.
        //     $amount = bcmul(@$user_id->sub_total, 100); // calculating on cents...
        //     $split_amount = bcmul(@$user_id->amount / 2, 100); // calculating on cents...]
        //     $amount = (int) $amount;
        //     $split_amount = (int) $split_amount;
        // }else{
        //     // In Here the Primary account is the Admin account and the secondary account is the professional account.
        //     $amount = bcmul(@$user_id->amount, 100); // calculating on cents...
        //     $split_amount = bcmul(@$user_id->amount / 2, 100); // calculating on cents...]
        //     $amount = (int) $amount;
        //     $split_amount = (int) $split_amount;
        // }
        $referDiscount = ReferralDiscount::first();
        $checkTeacher = User::where('id', $user_id->professional_id)->first();
        $professionalCommissionList = Commission::first();
        $professionalCommission=50;
        $x=0;
        if (@$user_id->no_of_installment > 1 && @$user_id->installment_charge == 'Y') {

            $i = @$user_id->no_of_installment;
            $x2 = 4.50;
            $x3 = 5.00;
            $x4 = 5.50;
            $x5 = 6.50;
            $x6 = 7.50;
            $x7 = 8.50;
            $x8 = 9.50;
            $x9 = 10.50;
            $x10 = 11.50;
            $x11 = 12.00;
            $x12 = 12.50;
            if ($i == 2) {
                $x = $x2;
            }
            if ($i == 3) {
                $x = $x3;
            }
            if ($i == 4) {
                $x = $x4;
            }
            if ($i == 5) {
                $x = $x5;
            }
            if ($i == 6) {
                $x = $x6;
            }
            if ($i == 7) {
                $x = $x7;
            }
            if ($i == 8) {
                $x = $x8;
            }
            if ($i == 9) {
                $x = $x9;
            }
            if ($i == 10) {
                $x = $x10;
            }
            if ($i == 11) {
                $x = $x11;
            }
            if ($i == 12) {
                $x = $x12;
            }
        }

        if($user_id->booking_type=='C'){
            $professionalCommission= $professionalCommissionList->chat_commission-$x;
        }else{
            $professionalCommission = $professionalCommissionList->commission-$x;
        }
        if (@$checkTeacher->is_paid_activity == 'N') {

            $discount = $professionalCommission + $referDiscount->referrer_teacher_discount;
            $amount_professional = (@$user_id->amount * @$discount) / 100;
            $split_amount   =  bcmul(@$amount_professional, 100); // calculating on cents...
            $split_amount   =  (int) @$split_amount;
        } elseif (@$checkTeacher->benefit_count > 0) {

            $discount = $professionalCommission + $referDiscount->referred_teacher_discount;
            $amount_professional = (@$user_id->amount * @$discount) / 100;
            $split_amount   =  bcmul(@$amount_professional, 100); // calculating on cents...
            $split_amount   =  (int) @$split_amount;
        } else {

            $discount = $professionalCommission;
            $amount_professional = (@$user_id->amount * @$discount) / 100;
            $split_amount   =  bcmul(@$amount_professional, 100); // calculating on cents...
            $split_amount   =  (int) @$split_amount;
        }
        $amount =  bcmul((@$user_id->sub_total+ @$user_id->extra_installment_charge), 100); // calculating on cents...
        $amount =  (int) $amount;

        if (env('PAYMENT_ENVIORNMENT') == 'live') {
            $moip = new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_PRODUCTION);
        } else if (env('PAYMENT_ENVIORNMENT') == 'sandbox') {
            $moip = new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_SANDBOX);
        }

        $order = $moip->orders()->setOwnId(uniqid())
            ->addItem("Consultation Fees", 1, @$user_id->parentCatDetails->name, @$amount)
            ->setShippingAmount(0)->setAddition(0)->setDiscount(0)
            ->setCustomerId(@$customer->customer_id)
            ->addReceiver(@$user_id->profDetails->professional_id, "SECONDARY", @$split_amount, 0, false)
            ->create();

        Booking::where([
            'token_no' => @$token
        ])->update([
            'moip_order_id'  =>  @$order->getId()
        ]);

        return $this->payment($order, $customer, $cardDetails, $user_id);
    }

    /**
     * For Deducting customer balance
     * @method: payment
     */
    private function payment($order, $user, $card, $userBooking)
    {
        if (env('PAYMENT_ENVIORNMENT') == 'live') {
            $url = Moip::ENDPOINT_PRODUCTION;
        } else if (env('PAYMENT_ENVIORNMENT') == 'sandbox') {
            $url = Moip::ENDPOINT_SANDBOX;
        }
        $data = array(
            'installmentCount' => $userBooking->no_of_installment,
            'statementDescriptor' => 'netbhe',
            'fundingInstrument' =>
            array(
                'method' => 'CREDIT_CARD',
                'creditCard' =>
                array(
                    'id' => @$card->card_id,
                    'store' => true,
                    'holder' =>
                    array(
                        'fullname' => @$card->card_name,
                        'birthdate' => @$user->dob,
                        'taxDocument' =>
                        array(
                            'type' => 'CPF',
                            'number' => @$user->cpf_no,
                        ),
                        'phone' =>
                        array(
                            'countryCode' => @$user->countryName->phonecode,
                            'areaCode' => @$user->area_code,
                            'number' => @$user->mobile,
                        ),
                        'billingAddress' =>
                        array(
                            'city' => @$user->city,
                            'district' => @$user->district,
                            'street' => @$user->street_name,
                            'streetNumber' => @$user->street_number,
                            'zipCode' => @$user->zipcode,
                            'state' => substr(@$user->stateName->name, 0, 2),
                            'country' => substr(@$user->countryName->name, 0, 3),
                        ),
                    ),
                ),
            ),
        );
        $crl = curl_init();
        $header = array();
        // $header[] = 'Content-length: 0';
        $header[] = 'Content-type: application/json';
        $header[] = 'Authorization: Bearer ' . $this->adminAccessToken;
        curl_setopt($crl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($crl, CURLOPT_URL, "{$url}/v2/orders/" . @$order->getId() . "/payments");
        curl_setopt($crl, CURLOPT_POST, 1);
        curl_setopt($crl, CURLOPT_POST, true);
        curl_setopt($crl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        $payment_output = curl_exec($crl);
        curl_close($crl);
        $payment_output = json_decode($payment_output);
        if (@$payment_output->id) {
            $referDiscount = ReferralDiscount::first();
            $checkTeacher = User::where('id', $userBooking->professional_id)->first();
            $professionalCommissionList = Commission::first();
            $professionalCommission = 50;
            $x = 0;
            if (@$userBooking->no_of_installment > 1 && @$userBooking->installment_charge == 'Y') {

                $i = @$userBooking->no_of_installment;
                $x2 = 4.50;
                $x3 = 5.00;
                $x4 = 5.50;
                $x5 = 6.50;
                $x6 = 7.50;
                $x7 = 8.50;
                $x8 = 9.50;
                $x9 = 10.50;
                $x10 = 11.50;
                $x11 = 12.00;
                $x12 = 12.50;
                if ($i == 2) {
                    $x = $x2;
                }
                if ($i == 3) {
                    $x = $x3;
                }
                if ($i == 4) {
                    $x = $x4;
                }
                if ($i == 5) {
                    $x = $x5;
                }
                if ($i == 6) {
                    $x = $x6;
                }
                if ($i == 7) {
                    $x = $x7;
                }
                if ($i == 8) {
                    $x = $x8;
                }
                if ($i == 9) {
                    $x = $x9;
                }
                if ($i == 10) {
                    $x = $x10;
                }
                if ($i == 11) {
                    $x = $x11;
                }
                if ($i == 12) {
                    $x = $x12;
                }
            }

            if ($userBooking->booking_type == 'C') {
                $professionalCommission = $professionalCommissionList->chat_commission-$x;
            } else {
                $professionalCommission = $professionalCommissionList->commission-$x;
            }

            if (@$checkTeacher->is_paid_activity == 'N') {
                $discount = $professionalCommission + $referDiscount->referrer_teacher_discount;
                $amount = (@$userBooking->amount * @$discount) / 100;
                $professional_amount   =  @$amount;
                $admin_amount         =   @$userBooking->sub_total - @$amount;
                User::where('id', @$checkTeacher->id)->update(['is_paid_activity' => 'C']);
                User::where('id', @$checkTeacher->referrer_id)->increment("benefit_count", 1);
            } elseif (@$checkTeacher->benefit_count > 0) {
                $discount = $professionalCommission + $referDiscount->referred_teacher_discount;
                $amount = (@$userBooking->amount * @$discount) / 100;
                $professional_amount   =  @$amount;
                $admin_amount         =   @$userBooking->sub_total - @$amount;
                User::where('id', @$checkTeacher->id)->decrement("benefit_count", 1);
            } else {
                $discount = $professionalCommission;
                $amount = (@$userBooking->amount * @$discount) / 100;
                $professional_amount   =  @$amount;
                $admin_amount         =   @$userBooking->sub_total - @$amount;
            }
            $total_amount         =  @$userBooking->sub_total+ @$userBooking->extra_installment_charge;
            if (@$payment_output->status == 'CANCELLED') {
                $UpdateBooking = Booking::where([
                    'token_no'        =>  @$userBooking->token_no
                ])->update([
                    'payment_status'  =>  "F"
                ]);

                return false;
            }
            $payment = Payment::create([
                'token_no'              =>  @$userBooking->token_no,
                'order_id'              =>  @$order->getId(),
                'payment_id'            =>  @$payment_output->id,
                'user_id'               =>  @$userBooking->user_id,
                'professional_id'       =>  @$userBooking->professional_id,
                'professional_amount'   =>  @$professional_amount,
                'admin_amount'          =>  @$admin_amount,
                'total_amount'          =>  @$total_amount,
                'wirecard_response'     =>  json_encode($payment_output),
                'balance_status'        =>  'R',
                'order_type'            =>  'B',
            ]);
            if (@$payment) {
                $UpdateBooking = Booking::where([
                    'token_no'        =>  @$userBooking->token_no
                ])->update([
                    'payment_status'  =>  "P"
                ]);
                $referralStatus = User::where('id', @$userBooking->user_id)->first();
                if (@$referralStatus->is_paid_activity == 'N') {
                    User::where('id', @$userBooking->user_id)->update(['is_paid_activity' => 'C']);
                    User::where('id', @$referralStatus->referrer_id)->increment("benefit_count", 1);
                }
                $userdata = User::where('id', Auth::id())->first();
                $userdata1 = User::where('id', @$userBooking->professional_id)->first();
                $professionalmail = $userdata1->email;
                $usermail = $userdata->email;
                $bookingdata = $userBooking;

                // // for send mail to user
                Mail::send(new UserBooking($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));

                // // for send mail to professional
                Mail::send(new ProfessionalBooking($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                return true;
            } else {
                $UpdateBooking = Booking::where([
                    'token_no'        =>  @$userBooking->token_no
                ])->update([
                    'payment_status'  =>  "F"
                ]);

                return false;
            }
        }
    }

    /**
     *method:createCustomer()
     *purpose:For Creating Customer on WireCard
     *Author:@bhisek
     */
    public function createCustomer(Request $request)
    {
        try {
            if (env('PAYMENT_ENVIORNMENT') == 'live') {
                $moip = new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_PRODUCTION);
            } else if (env('PAYMENT_ENVIORNMENT') == 'sandbox') {
                $moip = new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_SANDBOX);
            }

            $customer = $moip->customers()
                ->setOwnId(uniqid())
                ->setFullname(Auth::guard('web')->user()->name)
                ->setEmail(Auth::guard('web')->user()->email)
                ->setBirthDate(Auth::guard('web')->user()->dob)
                ->setTaxDocument(Auth::guard('web')->user()->cpf_no)
                ->setPhone(Auth::guard('web')->user()->area_code, Auth::guard('web')->user()->mobile)
                ->addAddress('BILLING', Auth::guard('web')->user()->street_name, Auth::guard('web')->user()->street_number, Auth::guard('web')->user()->district, Auth::guard('web')->user()->city, strtoupper(substr(Auth::guard('web')->user()->stateName->name, 0, 2)), Auth::guard('web')->user()->zipcode, Auth::guard('web')->user()->complement)
                ->addAddress('SHIPPING', Auth::guard('web')->user()->street_name, Auth::guard('web')->user()->street_number, Auth::guard('web')->user()->district, Auth::guard('web')->user()->city, substr(Auth::guard('web')->user()->stateName->name, 0, 2), Auth::guard('web')->user()->zipcode, Auth::guard('web')->user()->complement)->create();
            # SAVE CUSTOMER CREADIT CARD
            $card  =  $moip->customers()->creditCard()
                ->setExpirationMonth($request->expiry_month)
                ->setExpirationYear($request->expiry_year)
                ->setNumber($request->cardnumber)
                ->setCVC($request->cvc)
                ->setFullName($request->card_name)
                ->setBirthDate(Auth::guard('web')->user()->dob)
                ->setTaxDocument('CPF', Auth::guard('web')->user()->cpf_no)
                ->setPhone(Auth::guard('web')->user()->countryName->phonecode, Auth::guard('web')->user()->area_code, Auth::guard('web')->user()->mobile)
                ->create($customer->getId());
            if (!@$card->getId()) {
                session()->flash('error', 'Internal server error');
                return redirect()->back();
            }

            $update = User::where('id', Auth::id())->update([
                'customer_id'                       =>  @$customer->getId(),
                'is_credit_card_added'              =>  @$card->getStore() == true ? 'Y' : 'N'
            ]);
            $is_Created = UserToCard::create([
                'user_id'       =>  Auth::id(),
                'card_id'       =>  $card->getId(),
                'first_six'     =>  $card->getFirst6(),
                'last_four'     =>  $card->getLast4(),
                'card_brand'    =>  $card->getBrand(),
                'cred_type'     =>  "CREDIT_CARD"
            ]);
            if (@$is_Created) {
                Booking::where('token_no', @$request->order_no)->update([
                    'order_status'  =>  'A'
                ]);
                session()->flash('success', \Lang::get('client_site.card_successfully_added'));
                return redirect()->back();
            } else {
                session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
                return redirect()->back();
            }
        } catch (\Exception $e) {
            session()->flash('error', \Lang::get('client_site.internal_server_error'));
            return redirect()->back();
        }
    }

    /*
    @Below Functions are used for validating a CPF number.
    */
    public function verificaDigitos($digito1, $digito2, $ver, $ver2)
    {
        $num = $digito1 % 11;
        $digito1 = ($num < 2) ? 0 : 11 - $num;
        $num2 = $digito2 % 11;
        $digito2 = ($num2 < 2) ? 0 : 11 - $num2;
        if ($digito1 === (int) $this->numeral[$ver] && $digito2 === (int) $this->numeral[$ver2]) {
            return true;
        } else {
            return false;
        }
    }
    private function verificaCPF()
    {
        $num1 = ($this->numeral[0] * 10) + ($this->numeral[1] * 9) + ($this->numeral[2] * 8) + ($this->numeral[3] * 7) + ($this->numeral[4] * 6)
            + ($this->numeral[5] * 5) + ($this->numeral[6] * 4) + ($this->numeral[7] * 3) + ($this->numeral[8] * 2);
        $num2 = ($this->numeral[0] * 11) + ($this->numeral[1] * 10) + ($this->numeral[2] * 9) + ($this->numeral[3] * 8) + ($this->numeral[4] * 7)
            + ($this->numeral[5] * 6) + ($this->numeral[6] * 5) + ($this->numeral[7] * 4) + ($this->numeral[8] * 3) + ($this->numeral[9] * 2);
        return $this->verificaDigitos($num1, $num2, 9, 10);
    }
    private function verificaCNPJ()
    {
        $num1 = ($this->numeral[0] * 5) + ($this->numeral[1] * 4) + ($this->numeral[2] * 3) + ($this->numeral[3] * 2) + ($this->numeral[4] * 9) + ($this->numeral[5] * 8)
            + ($this->numeral[6] * 7) + ($this->numeral[7] * 6) + ($this->numeral[8] * 5) + ($this->numeral[9] * 4) + ($this->numeral[10] * 3) + ($this->numeral[11] * 2);
        $num2 = ($this->numeral[0] * 6) + ($this->numeral[1] * 5) + ($this->numeral[2] * 4) + ($this->numeral[3] * 3) + ($this->numeral[4] * 2) + ($this->numeral[5] * 9)
            + ($this->numeral[6] * 8) + ($this->numeral[7] * 7) + ($this->numeral[8] * 6) + ($this->numeral[9] * 5) + ($this->numeral[10] * 4) + ($this->numeral[11] * 3) + ($this->numeral[12] * 2);
        return $this->verificaDigitos($num1, $num2, 12, 13);
    }
    private function getNumeral()
    {
        $this->numeral = preg_replace("/[^0-9]/", "", $this->CPF_CNPJ);
        $strLen = strlen($this->numeral);
        $ret = false;
        switch ($strLen) {
            case 11:
                if ($this->verificaCPF()) {
                    $this->tipo = 'CPF';
                    $ret = true;
                }
                break;
            case 14:
                if ($this->verificaCNPJ()) {
                    $this->tipo = 'CNPJ';
                    $ret = true;
                }
                break;
            default:
                $ret = false;
                break;
        }
        return $ret;
    }
    public function __toString()
    {
        return $this->CPF_CNPJ;
    }
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * For Free Session Payment Create
     * @method: freeSessionPayment
     */
    private function freeSessionPayment($token)
    {
        $userBooking = Booking::where('token_no', $token)->first();
        $payment = Payment::create([
            'token_no'              =>  @$userBooking->token_no,
            'order_id'              =>  'ORD-' . @$userBooking->token_no,
            // 'payment_id'            =>  @$payment_output->id,
            'user_id'               =>  @$userBooking->user_id,
            'professional_id'       =>  @$userBooking->professional_id,
            'professional_amount'   =>  @$userBooking->amount / 2,
            'admin_amount'          =>  @$userBooking->amount / 2,
            'total_amount'          =>  @$userBooking->amount,
            // 'wirecard_response'     =>  json_encode($payment_output),
            'balance_status'        =>  'W'
        ]);
        // Booking::where([
        //     'token_no' => @$token
        // ])->update([
        //     'moip_order_id'  =>  'ORD-' . @$userBooking->token_no
        // ]);
        if (@$payment) {
            $UpdateBooking = Booking::where([
                'token_no'        =>  @$userBooking->token_no
            ])->update([
                'payment_status'  =>  "P"
            ]);
            $userdata = User::where('id', Auth::id())->first();
            $userdata1 = User::where('id', @$userBooking->professional_id)->first();
            $professionalmail = $userdata1->email;
            $usermail = $userdata->email;
            $bookingdata = $userBooking;

            // // for send mail to user
            Mail::send(new UserBooking($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));

            // // for send mail to professional
            Mail::send(new ProfessionalBooking($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
            // $referralStatus= User::where('id', @$userBooking->user_id)->first();
            // if(@$referralStatus->is_paid_activity=='N'){
            //     User::where('id', @$userBooking->user_id)->update(['is_paid_activity'=>'C']);
            //     User::where('id', @$referralStatus->referrer_id)->increment("benefit_count", 1);
            // }
            return true;
        } else {
            $UpdateBooking = Booking::where([
                'token_no'        =>  @$userBooking->token_no
            ])->update([
                'payment_status'  =>  "F"
            ]);
            return false;
        }
    }
    /**
     * For Check Coupon Code
     * @method: checkCoupon
     */
    public function checkCoupon(Request $request)
    {
        $response = [
            "jsonrpc" => "2.0"
        ];
        if ($request->params['coupon']) {

            $coupon = Coupon::where('coupon_code', $request->params['coupon'])->where('coupon_status', 'A')->first();
            $dated = date_create(@$coupon->exp_date);
            if (@$coupon && $coupon->added_by != 'P') {
                if ($dated < date('Y-m-d')) {
                    $response['status'] = 2;
                    $response['message'] = \Lang::get('client_site.coupon_expiredy');
                } else {
                    $response['status'] = 1;
                    $response['message'] = \Lang::get('client_site.coupon_applied');
                }
            } else {
                $response['status'] = 0;
                $response['message'] = \Lang::get('client_site.coupon_not_valid');
            }
            return response()->json($response);
        }
    }
    /**
     * For Add Payment Method and Coupon Apply
     * @method: addPaymentCoupon
     */
    public function addPaymentCoupon($slug, Request $request)
    {
        $request->validate([
            "payment_method" =>  "required|in:C,BA",
        ]);
        $booking = Booking::where('token_no', $slug)->where('user_id', @Auth::id())->first();
        $coupon = Coupon::where('coupon_code', $request->couponCode)->where('coupon_status', 'A')->first();
        $checkStudent = User::where('id', @Auth::id())->first();
        $referDiscount = ReferralDiscount::first();
        if (@$booking) {
            $upd = [];
            $upd['payment_type'] = $request->payment_method;
            if (@$request->installment_select) {
                if (@$request->no_installment > 1 && @$request->no_installment < 13) {
                    $i = $request->no_installment;
                    $x2 = 4.50;
                    $x3 = 5.00;
                    $x4 = 5.50;
                    $x5 = 6.50;
                    $x6 = 7.50;
                    $x7 = 8.50;
                    $x8 = 9.50;
                    $x9 = 10.50;
                    $x10 = 11.50;
                    $x11 = 12.00;
                    $x12 = 12.50;
                    if ($i == 2) {
                        $x = $x2;
                    }
                    if ($i == 3) {
                        $x = $x3;
                    }
                    if ($i == 4) {
                        $x = $x4;
                    }
                    if ($i == 5) {
                        $x = $x5;
                    }
                    if ($i == 6) {
                        $x = $x6;
                    }
                    if ($i == 7) {
                        $x = $x7;
                    }
                    if ($i == 8) {
                        $x = $x8;
                    }
                    if ($i == 9) {
                        $x = $x9;
                    }
                    if ($i == 10) {
                        $x = $x10;
                    }
                    if ($i == 11) {
                        $x = $x11;
                    }
                    if ($i == 12) {
                        $x = $x12;
                    }
                    $down = (1 - ($x / 100) - (5.49 / 100));
                    $up = $booking->sub_total * ($x / 100);
                    $additional = ($up / $down);
                    if ($booking->installment_charge == 'Y') {
                        $additional = 0.00;
                    }

                    $upd['no_of_installment'] = @$request->no_installment;
                    $upd['extra_installment_charge'] = @$additional;
                }
            }
            elseif (@$coupon&& @$request->installment_select == null) {
                $subtotal = $booking->amount - (($booking->amount * $coupon->discount) / 100);
                $upd['sub_total'] = (int) $subtotal;
                $upd['coupon_id'] = $coupon->id;
                $upd['is_coupon'] = 'Y';
            } else {
                if (@$checkStudent->is_paid_activity == 'N') {
                    $subtotal = $booking->amount - (($booking->amount * $referDiscount->referrer_student_discount) / 100);
                    $upd['sub_total'] = (int) $subtotal;
                } elseif (@$checkStudent->benefit_count > 0) {
                    $subtotal = $booking->amount - (($booking->amount * $referDiscount->referred_student_discount) / 100);
                    $upd['sub_total'] = (int) $subtotal;
                    User::where('id', @$checkStudent->id)->decrement("benefit_count", 1);
                }
            }
            // dd($upd);
            $update = Booking::where('id', $booking->id)->update($upd);
            if (@$update) {
                return redirect()->back();
            }
        }
        return redirect()->back();
    }
    /**
     * For Free Session Payment Create
     * @method: freeSessionPayment
     */
    private function bankAccountPayment($token)
    {
        $userBooking = Booking::where('token_no', $token)->first();
        $referDiscount = ReferralDiscount::first();
        $checkTeacher = User::where('id', $userBooking->professional_id)->first();
        $professionalCommissionList = Commission::first();
            $professionalCommission = 50;
            if ($userBooking->booking_type == 'C') {
                $professionalCommission = $professionalCommissionList->chat_commission;
            } else {
                $professionalCommission = $professionalCommissionList->commission;
            }

        if (@$checkTeacher->is_paid_activity == 'N') {
            $discount = $professionalCommission + $referDiscount->referrer_teacher_discount;
            $amount = (@$userBooking->amount * @$discount) / 100;
            $professional_amount   =  @$amount;
            $admin_amount         =   @$userBooking->sub_total - @$amount;
            User::where('id', @$checkTeacher->id)->update(['is_paid_activity' => 'C']);
            User::where('id', @$checkTeacher->referrer_id)->increment("benefit_count", 1);
        } elseif (@$checkTeacher->benefit_count > 0) {
            $discount = $professionalCommission + $referDiscount->referred_teacher_discount;
            $amount = (@$userBooking->amount * @$discount) / 100;
            $professional_amount   =  @$amount;
            $admin_amount         =   @$userBooking->sub_total - @$amount;
            User::where('id', @$checkTeacher->id)->decrement("benefit_count", 1);
        } else {
            $discount = $professionalCommission;
            $amount = (@$userBooking->amount * @$discount) / 100;
            $professional_amount   =  @$amount;
            $admin_amount         =   @$userBooking->sub_total - @$amount;
        }
        $total_amount         =  @$userBooking->sub_total;
        // dd($checkTeacher->benefit_count);
        $payment = Payment::create([
            'token_no'              =>  @$userBooking->token_no,
            'order_id'              =>  'ORD-' . @$userBooking->token_no,
            // 'payment_id'            =>  @$payment_output->id,
            'user_id'               =>  @$userBooking->user_id,
            'professional_id'       =>  @$userBooking->professional_id,
            'professional_amount'   =>  @$professional_amount,
            'admin_amount'          =>  @$admin_amount,
            'total_amount'          =>  @$total_amount,
            // 'wirecard_response'     =>  json_encode($payment_output),
            'balance_status'        =>  'R',
            'order_type'            =>  'B',
        ]);
        if (@$payment) {
            $UpdateBooking = Booking::where([
                'token_no'        =>  @$userBooking->token_no
            ])->update([
                'payment_status'  =>  "PR",
                'order_status'  =>  "A"
            ]);
            $referralStatus = User::where('id', @$userBooking->user_id)->first();
            if (@$referralStatus->is_paid_activity == 'N') {
                User::where('id', @$userBooking->user_id)->update(['is_paid_activity' => 'C']);
                User::where('id', @$referralStatus->referrer_id)->increment("benefit_count", 1);
            }
            $userdata = User::where('id', Auth::id())->first();
            $userdata1 = User::where('id', @$userBooking->professional_id)->first();
            $professionalmail = $userdata1->email;
            $usermail = $userdata->email;
            $bookingdata = $userBooking;

            // // for send mail to user
            // Mail::send(new UserBooking($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));

            // // // for send mail to professional
            // Mail::send(new ProfessionalBooking($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
            // // for send mail to admin for Bank account payment
            Mail::send(new UserBankAccountBooking($bookingdata->id,$usermail));

            Mail::send(new BankAccountPayment($bookingdata->id));
            return true;
        } else {
            $UpdateBooking = Booking::where([
                'token_no'        =>  @$userBooking->token_no
            ])->update([
                'payment_status'  =>  "F"
            ]);
            return false;
        }
    }
    /**
     * For upload Payment Document Bank Account Payment
     * @method: uploadDocument
     */
    public function uploadDocument($slug, Request $request)
    {
        $request->validate([
            "upload_file" =>  "required",
        ]);
        $booking = Booking::where('token_no', $slug)->where('user_id', @Auth::id())->first();
        if (@$booking) {
            $upd = [];
            if (@$request->upload_file) {
                $upload_file = $request->upload_file;
                $filename = time() . '-' . rand(1000, 9999) . '.' . $upload_file->getClientOriginalExtension();
                Storage::putFileAs('public/Bank_Document', $upload_file, $filename);
                $upd['payment_document'] = $filename;
            }
            // dd($upd);
            $update = Booking::where('id', $booking->id)->update($upd);
            if (@$update) {
                return redirect()->back();
            }
        }
        return redirect()->back();
    }
}
