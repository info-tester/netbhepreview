<?php

namespace App\Http\Controllers\Modules\Booking;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\User;
use App\Models\Booking;
use App\Models\UserAvailability;
use App\Models\Timezone;
use App\Models\UserToCard;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use Moip\Moip;
use Moip\Auth\BasicAuth;
use Moip\Auth\OAuth;
use Moip\Resource\Customer;

use App\Mail\UserBooking;
use App\Mail\ProfessionalBooking;
use App\Models\Content;
use App\Models\Payment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Models\Coupon;
use App\Models\ReferralDiscount;
use Illuminate\Support\Facades\Storage;
use App\Models\AdminBankAccount;
use App\Mail\BankAccountPayment;
use App\Models\Commission;
use App\Mail\UserBankAccountBooking;
use App\Models\PaymentDetails;
use Stripe\PaymentIntent;
use Stripe\Stripe;
use Stripe\Refund;

use Validator;
use URL;
use Session;
use Redirect;
use Input;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment as PaymentPaypal;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use PayPal\Api\RefundRequest;
use PayPal\Api\Sale;
use App\Mail\ProfessionalBookingCancel;
use App\Mail\UserBookingCancel;

class BookingController extends Controller
{
    protected $CPF_CNPJ, $numeral, $tipo;
    protected $adminAccessToken;
    private $_api_context;
    private $_ordID;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->adminAccessToken = getAdminToken();
        $paypal_configuration = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_configuration['client_id'], $paypal_configuration['secret']));
        $this->_api_context->setConfig($paypal_configuration['settings']);
    }


    /**
     *method: index
     *created By: Abhisek
     *description: For Search
     */

    public function index($slug = null)
    {
        $nowtime = date('Y-m-d');
        $user = User::with('userDetails.categoryName')->where('slug', $slug)->first();
        if (!@$user) {
            session()->flash('error', \Lang::get('client_site.user_not_found'));
            return redirect()->back();
        }
        $parCat = $chldCat = [];
        $i = 0;
        $j = 0;
        $profile = 0;
        $required = [
            0   => 'country_id',
            1   => 'state_id',
            2   => 'city',
            3   => 'dob',
            4   => 'street_number',
            5   => 'street_name',
            6   => 'complement',
            7   => 'district',
            8   => 'zipcode',
            9  => 'area_code'
        ];
        $av = UserAvailability::where('user_id', $user->id)->where('slot_start', '>', now(env('TIMEZONE'))->addMinutes('30')->toDateTimeString())->whereNotIn('slot_start', getUsedTimeSlotsForAllDates($user->id))->get();
        $avlDay = [];
        $avlDay1 = [];
        foreach ($av->toArray() as $a) {
            $avl = [];
            $st = '';
            foreach ($a as $k => $v) {
                if ($k == 'slot_start') {
                    $avl[$k] = toUserTime($v, 'Y-m-d\TH:i:s');
                    $avl['time_start'] = toUserTime($v, 'g:i A');
                    $st = toUserTime($v, 'Y-m-d');
                } else if ($k == 'slot_end') {
                    $avl[$k] = toUserTime($v, 'Y-m-d\TH:i:s');
                    $avl['time_end'] = toUserTime($v, 'g:i A');
                } else {
                    $avl[$k] = $v;
                }
            }
            array_push($avlDay1, $avl);
            if (!in_array($st, $avlDay)) {
                array_push($avlDay, $st);
            }
        }
        for ($i = 0; $i < sizeof($required); $i++) {
            $str = $required[@$i];
            if (Auth::guard('web')->user()->$str == null) {
                $profile++;
                break;
            } elseif (Auth::guard('web')->user()->$str <= 0 && !is_string(Auth::guard('web')->user()->$str)) {
                $profile++;
                break;
            }
        }
        if ($user == null) {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        } else {
            if ($user->slug == Auth::user()->slug || $user->id == Auth::user()->id) {
                session()->flash('error', \Lang::get('client_site.unauthorize_access'));
                return redirect()->back();
            } else {
                foreach ($user->userDetails as $key) {
                    if ($key->level == 0) {
                        $parCat[$i] = $key->category_id;
                    }
                    if ($key->level > 0 || $key->level != "") {
                        $chldCat[$i] = $key->category_id;
                    }
                    $i++;
                }
                $category = Category::orderBy('name')->where('status', 'A')->get();
                $time = Timezone::get();
                $country = Country::orderBy('name')->get();
                $state = State::orderBy('name')->get();
                $city = City::orderBy('name')->get();

                $date = date('d-m-Y');
                if (session()->has('slotStart')) {
                    $date = date('Y-m-d', strtotime(session()->get('slotStart')));
                    session()->forget('slotStart');
                }
                $slots = UserAvailability::where('user_id', $user->id)->where('date', '>=', now(env('TIMEZONE'))->addMinutes('30')->toDateString())
                    ->where('slot_start', '>=', date('Y-m-d', strtotime('-1 day ' . $date)))
                    ->where('slot_start', '<=', date('Y-m-d', strtotime('+1 day ' . $date)))
                    ->where('slot_start', '>', now(env('TIMEZONE'))->addMinutes('30')->toDateTimeString())->orderBy('slot_start', 'ASC')
                    ->whereNotIn('slot_start', getUsedTimeSlotsForDate($user->id, date('Y-m-d', strtotime($date))))->get();
                $fromTotime = [];
                foreach (getUserTimeSlotsForDate($date, $slots) as $a) {
                    $avl = [];
                    foreach ($a as $k => $v) {
                        if ($k != 'slot_start' && $k != 'slot_end') {
                            $avl[$k] = $v;
                        } else {
                            $avl[$k] = toUserTime($v, 'H:i');
                        }
                    }
                    array_push($fromTotime, $avl);
                }
                $bookingPage = Content::where('id', 12)->first();
                return view('modules.booking.booking')->with([
                    'user'          =>  @$user,
                    'category'      =>  @$category,
                    'parent'        =>  @$parCat,
                    'child'         =>  @$chldCat,
                    'profile'       =>  @$profile,
                    'time'          =>  @$time,
                    'country'       =>  @$country,
                    'state'         =>  @$state,
                    'city'          =>  @$city,
                    'date'          =>  @date('d-m-Y', strtotime($date)),
                    'fromTotime'    =>  @$fromTotime,
                    'avlTime1'      => json_encode($avlDay1),
                    'avlDay'        => json_encode($avlDay),
                    'bookingPage'   =>  @$bookingPage,
                    // 'time'          =>  @$time
                ]);
            }
        }
    }

    public function getDate(Request $request)
    {
        $response = [
            'jsonrpc'   =>  '2.0'
        ];
        if ($request->params['date']) {
            $date = UserAvailability::where('user_id', $request->params['user_id'])->where('date', '>=', date('Y-m-d'))
                ->where('slot_start', '>=', date('Y-m-d', strtotime('-1 day ' . $request->params['date'])))
                ->where('slot_start', '<=', date('Y-m-d', strtotime('+1 day ' . $request->params['date'])))
                ->where('slot_start', '>', now(env('TIMEZONE'))->addMinutes('30')->toDateTimeString())
                ->orderBy('slot_start', 'ASC')
                ->whereNotIn('slot_start', getUsedTimeSlotsForDate($request->params['user_id'], date('Y-m-d', strtotime($request->params['date']))))->get();
            $avlDay1 = [];
            foreach (getUserTimeSlotsForDate($request->params['date'], $date) as $a) {
                $avl = [];
                foreach ($a as $k => $v) {
                    if ($k != 'slot_start' && $k != 'slot_end') {
                        $avl[$k] = $v;
                    } else {
                        $avl[$k] = toUserTime($v, 'H:i');
                    }
                }
                array_push($avlDay1, $avl);
            }
            $response['status'] = 1;
            $response['result'] = $avlDay1;
        }
        return response()->json($response);
    }


    /**
     * For fetching sub-category.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function getSubcat(Request $request)
    {
        $response = [
            'jsonrpc'   =>  '2.0'
        ];
        if ($request->params['cat']) {
            $cat = Category::where('parent_id', $request->params['cat'])->orderBy('name')->get();
            if ($cat != null) {
                $response['status'] = 1;
                $response['result'] = $cat;
            } else {
                $response['status'] = 0;
            }
        }
        return response()->json($response);
    }

    public function storeBooking($slug, Request $request)
    {
        $user = User::where('slug', $slug)->first();

        date_default_timezone_set(@$user->userTimeZone->timezone);
        $token = null;
        if ($user == null || Auth::user()->slug == $user->slug) {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        } else {
            $request->validate([
                'category'              =>  'required',
                'date'                  =>  'required',
                'time_slot'             =>  'required',
                'booking_type'             =>  'required',
                'msg'                   =>  'required'
            ], [
                'time_slot.required'    =>  'selecione um horário.'
            ]);

            $time_slot = explode("-", @$request->time_slot);
            $startTime = toUTCTime(date('Y-m-d', strtotime($request->date)) . ' ' . $time_slot[0] . ':00', 'H:i:s');
            $slotStartDate = toUTCTime(date('Y-m-d', strtotime($request->date)) . ' ' . $time_slot[0] . ':00', 'Y-m-d H:i:s');
            $endTime = toUTCTime(date('Y-m-d', strtotime($request->date)) . ' ' . $time_slot[1] . ':00', 'H:i:s');
            $bookingDate = date('Y-m-d', strtotime($slotStartDate));
            if (@$time_slot[1] && $time_slot[1] == '00:00') {
                $endTime_ = strtotime($time_slot[1]) + (24 * 60 * 60);
                $duration = abs($endTime_ - strtotime($time_slot[0])) / 60;
            } else {
                $duration = abs(strtotime($time_slot[1]) - strtotime($time_slot[0])) / 60;
            }
            $is_avlbl = UserAvailability::where(['user_id' =>  @$user->id, 'slot_start'  =>  $slotStartDate])
                ->first();
            if ($is_avlbl == null) {
                session()->flash('error', \Lang::get('client_site.currently_time_slot_is_not'));
                return redirect()->back();
            } else {
                $isAval = Booking::where([
                    'start_time'        =>  $startTime,
                    'end_time'          =>  $endTime,
                    'date'              =>  $bookingDate,
                    'professional_id'   =>  $user->id
                ])->where('order_status', '!=', 'R')->first();

                if ($isAval != null) {
                    session()->flash('error', \Lang::get('client_site.currently_booking_is_not'));
                    return redirect()->back();
                }
                $is_booking_free = Booking::where([
                    'professional_id'   =>  $user->id,
                    'user_id'           =>  auth()->id(),
                    'free_session_id'   => $user->id,
                ])->get();
                // dd($user->free_session_id);
                if (@$is_booking_free->count()>=10) {
                    $request->session()->forget('free_session_id');
                }
                $free_session_id = @session()->get('free_session_id');
                if (@$user->free_session_number > 0 && @$free_session_id == $user->free_session_id) {
                    // dd($user->free_session_number);
                    $is_created = Booking::create([
                        'user_id'           =>  auth()->id(),
                        'token_no'          =>  $token = strtoupper(str_random(12)),
                        'professional_id'   =>  @$user->id,
                        'category_id'       =>  @$request->category ? $request->category : 0,
                        'sub_category_id'   =>  @$request->sub_category ? $request->sub_category : 0,
                        'date'              =>  $bookingDate,
                        'start_time'        =>  $startTime,
                        'end_time'          =>  $endTime,
                        'duration'          =>  @$duration,
                        'msg'               =>  @$request->msg,
                        'booking_type'      =>  @$request->booking_type,
                        'amount'            =>  0,
                        'order_status'      => "A",
                        'payment_status'    => "I",
                        'free_session_id'   => $user->id,
                    ]);
                    if ($is_created) {
                        User::where('id', $user->id)->decrement("free_session_number", 1);
                    }
                } else {
                    // dd($user->id);
                    $use_wallet_amount=0;
                    $use_use='N';
                    $sub_total = $user->rate == 'H' ? round($user->rate_price / 60 * @$duration) : round($user->rate_price * @$duration);
                    if(auth()->user()->wallet_balance>0){
                        $use_wallet_amount= (int)($sub_total);
                        if(auth()->user()->wallet_balance >= $use_wallet_amount){
                            $use_wallet_amount =$use_wallet_amount;
                        }else{
                            $use_wallet_amount= auth()->user()->wallet_balance;
                        }
                        $use_use='Y';
                    }
                    // return auth()->user()->wallet_balance;
                    $is_created = Booking::create([
                        'user_id'           =>  auth()->id(),
                        'token_no'          =>  $token = strtoupper(str_random(12)),
                        'professional_id'   =>  @$user->id,
                        'category_id'       =>  @$request->category ? $request->category : 0,
                        'sub_category_id'   =>  @$request->sub_category ? $request->sub_category : 0,
                        'date'              =>  $bookingDate,
                        'start_time'        =>  $startTime,
                        'end_time'          =>  $endTime,
                        'duration'          =>  @$duration,
                        'msg'               =>  @$request->msg,
                        'booking_type'      =>  @$request->booking_type,
                        'amount'            =>  $user->rate == 'H' ? round($user->rate_price / 60 * @$duration) : round($user->rate_price * @$duration),
                        'order_status'      =>  Auth::guard('web')->user()->is_credit_card_added == "N" ? "AA" : "A",
                        'payment_status'    => "I",
                        'sub_total'         => $sub_total,
                        'installment_charge'    => @$user->installment_charge,
                        'is_wallet_use'=>$use_use,
                        'wallet'=>$use_wallet_amount
                    ]);
                }
                if ($is_created) {
                    // $userdata = User::where('id', Auth::id())->first();
                    // $userdata1 = User::where('id', $user->id)->first();
                    // $professionalmail = $userdata1->email;
                    // $usermail = $userdata->email;
                    // $bookingdata = $is_created;

                    // // // for send mail to user
                    // Mail::send(new UserBooking($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));

                    // // // for send mail to professional
                    // Mail::send(new ProfessionalBooking($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                    $request->session()->forget('free_session_id');
                    session()->flash('success', @Auth::user()->is_credit_card_added == "N" ? \Lang::get('client_site.your_booking_was_initiated') : \Lang::get('client_site.successful_booked_your_request'));
                    return redirect()->route('booking.sces', ['token' => $token]);
                } else {
                    session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
                    return redirect()->back();
                }
            }
        }
    }

    public function checkDate(Request $request)
    {
        $response = [
            'jsonrpc'   =>  '2.0'
        ];
        if ($request->params['date']) {
            $userDetails = User::where('id', $request->params['user_id'])->first();
            $userDetails = $userDetails->load('userTimeZone');

            date_default_timezone_set(@$userDetails->userTimeZone->timezone);
            $day = date('l', strtotime($request->params['date']));
            $days = [
                'Sunday'    =>  1,
                'Monday'    =>  2,
                'Tuesday'   =>  3,
                'Wednesday' =>  4,
                'Thursday'  =>  5,
                'Friday'    =>  6,
                'Saturday'  =>  7
            ];
            $duration = date('H:i', strtotime($request->params['start_time'] . ' +' . $request->params['duration'] . ' minutes'));

            $is_avlbl = UserAvailability::where([
                'user_id'       =>  @$request->params['user_id'],
                'day'           =>  @$days[@$day]
            ])
                ->where('from_time', '<=', $request->params['start_time'])
                ->where('to_time', '>=', $duration)
                ->first();

            if ($is_avlbl == null) {
                $response['status'] = 1;
            } else {
                $response['status'] = 0;
            }
        }
        return response()->json($response);
    }

    public function bookingSuccess($slug)
    {
        $authUser = Auth::guard('web')->user();
        $booking = Booking::where('token_no', $slug)
            ->where('user_id', @Auth::id())
            ->first();
        $profile = 0;
        $required = [
            0   => 'country_id',
            1   => 'state_id',
            2   => 'city',
            3   => 'dob',
            4   => 'street_number',
            5   => 'street_name',
            6   => 'complement',
            7   => 'district',
            8   => 'zipcode',
            9  => 'area_code'
        ];

        for ($i = 0; $i < sizeof($required); $i++) {
            $str = $required[@$i];
            if ($authUser->$str == null) {

                $profile++;
                break;
            } elseif ($authUser->$str <= 0 && !is_string($authUser->$str)) {
                $profile++;
                break;
            }
        }
        if ($booking == null) {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        } else {
            if($booking->amount != 0 && $booking->payment_status == 'I' && $booking->wallet == $booking->sub_total ){
                $this->fullPaymentThroughWallet($slug);
            }
            if ($authUser->is_credit_card_added == 'Y' && $authUser->customer_id != '' && $booking->moip_order_id == '' && $booking->amount != 0 && $booking->payment_type == 'C') {
                $this->chargeUser($slug);
            }
            if ($booking->amount == 0 && $booking->payment_status == 'I' ) {
                $this->freeSessionPayment($slug);
            }
            if ($booking->payment_type == 'BA' && $booking->payment_status == 'I') {
                $this->bankAccountPayment($slug);
            }
            if ($booking->payment_type == 'P' && $booking->payment_status == 'I') {
                $title = "Booking";
                $paydata = new Request([
                    'title'     => $title,
                    'amount'    => $booking->sub_total- @$booking->wallet,
                    'ordID'     => $booking->id,
                ]);
                // dd('Heyya');
                // app('App\Http\Controllers\Modules\Payment\PaypalController')->postPaymentWithpaypal($paydata);
                $this->postPaymentWithpaypal($paydata);
            }
            $booking = Booking::where('token_no', $slug)
                ->where('user_id', @Auth::id())
                ->first();
            $booking = $booking->load('parentCatDetails', 'childCatDetails', 'profDetails');
            $time = Timezone::get();
            $country = Country::orderBy('name')->get();
            $state = State::orderBy('name')->get();
            $accountInfo = AdminBankAccount::first();
            if (@$booking->payment_status == 'F') {
                session()->flash('error', \Lang::get('site.Payment_failed'));
                return redirect()->route('load_user_dashboard');
            }
            $curServerTime = strtotime((now(env('TIMEZONE'))->addHour('24')->toDateTimeString()));
            // $booking_time = date('Y-m-d H:i:s',strtotime(toUTCTime(date('Y-m-d H:i:s', strtotime($booking->date . ' ' . $booking->start_time)))));
            $booking_time = strtotime($booking->date . ' ' . $booking->start_time);
            $is_grater_24hrs = 0;
            if ($curServerTime < $booking_time) {
                $is_grater_24hrs = 1;
            }
            return view('modules.booking.booking_success')->with([
                'booking'   =>  @$booking,
                'profile'   =>  @$profile,
                'time'      =>  @$time,
                'country'   =>  @$country,
                'state'     =>  @$state,
                'accountInfo' => @$accountInfo,
                'is_grater_24hrs' => @$is_grater_24hrs
            ]);
        }
    }


    /**
     * For Completing Customer Profile
     *
     * @method completeProfile
     * @author Abhisek
     */
    public function completeProfile(Request $request)
    {
        try {
            // if (@$request->is_profile > 0) {
            $request->validate([
                'name'              =>  "required",
                'phone_no'          => 'required|digits:9|unique:users,mobile,' . Auth::id(),
                'country'           =>  "required",
                'state'             =>  "required",
                'city'              =>  "required",
                'dob'               =>  "required",
                'street_number'     =>  "required",
                'street_name'       =>  "required",
                'complement'        =>  "required",
                'district'          =>  "required",
                'zipcode'           =>  "required",
                'area_code'         =>  "required",
                'cpf_no'            =>  "required|unique:users,cpf_no," . Auth::id()
            ]);
            $data = [
                'name'              =>  $request->name,
                'mobile'            =>  $request->phone_no,
                'country_id'        =>  $request->country,
                'state_id'          =>  $request->state,
                'city'              =>  $request->city,
                'dob'               =>  date('Y-m-d', strtotime($request->dob)),
                'street_number'     =>  $request->street_number,
                'street_name'       =>  $request->street_name,
                'complement'        =>  $request->complement,
                'district'          =>  $request->district,
                'area_code'         =>  $request->area_code,
                'zipcode'           =>  $request->zipcode,
                'cpf_no'            =>  $request->cpf_no
            ];
            $user = User::where('id', Auth::id())->update($data);

            $this->CPF_CNPJ = $request->cpf_no;
            if (!$this->getNumeral()) {
                session()->flash('error', \Lang::get('client_site.invalid_cpf_number'));
                return redirect()->back();
            }
            // }

            if (@$user || @$is_profile <= 0) {
                $request->validate([
                    'card_name' => "required",
                    'card_cpf_no' => "required",
                    // 'card_dob' => "required",
                    'dob_year' => "required",
                    'dob_month' => "required",
                    'dob_day' => "required",
                    'card_phonecode' => "required",
                    'card_area_code' => "required|digits:2",
                    'card_mobile' => 'required|digits:9',
                ]);
                $dob = $request->dob_year . '-' . $request->dob_month . '-' . $request->dob_day;
                // dd($dob);
                $this->CPF_CNPJ = $request->card_cpf_no;
                if (!$this->getNumeral()) {
                    session()->flash('error', \Lang::get('client_site.invalid_cpf_number'));
                    return redirect()->back();
                }
                $updatedUser = User::where('id', Auth::id())->first();
                $moip = env('PAYMENT_ENVIORNMENT', 'sandbox') == 'live' ? new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_PRODUCTION) : new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_SANDBOX);
                if($updatedUser->customer_id==null){
                $customer = $moip->customers()
                    ->setOwnId(uniqid())
                    ->setFullname(@$updatedUser->name)
                    ->setEmail(@$updatedUser->email)
                    ->setBirthDate(@$updatedUser->dob)
                    ->setTaxDocument(@$updatedUser->cpf_no)
                    ->setPhone(@$updatedUser->area_code, @$updatedUser->mobile)
                    ->addAddress('BILLING', @$updatedUser->street_name, @$updatedUser->street_number, @$updatedUser->district, @$updatedUser->city, strtoupper(substr(@$updatedUser->stateName->name, 0, 2)), @$updatedUser->zipcode, @$updatedUser->complement)
                    ->addAddress('SHIPPING', @$updatedUser->street_name, @$updatedUser->street_number, @$updatedUser->district, @$updatedUser->city, substr(@$updatedUser->stateName->name, 0, 2), @$updatedUser->zipcode, @$updatedUser->complement)->create();
                }
                $c_id=$updatedUser->customer_id?$updatedUser->customer_id:@$customer->getId();
                $card = $moip->customers()->creditCard()
                    ->setExpirationMonth($request->expiry_month)
                    ->setExpirationYear($request->expiry_year)
                    ->setNumber($request->cardnumber)
                    ->setCVC($request->cvc)
                    ->setFullName($request->card_name)
                    ->setBirthDate($dob)
                    ->setTaxDocument('CPF', $request->card_cpf_no)
                    ->setPhone($request->card_phonecode, $request->card_area_code, $request->card_mobile)
                    ->create(@$c_id);

                if (!@$card->getId()) {
                    session()->flash('error', \Lang::get('client_site.internal_server_error'));
                    return redirect()->back();
                }
                $update = User::where('id', Auth::id())->update([
                    'customer_id' => @$c_id,
                    'is_credit_card_added' => @$card->getStore() == true ? 'Y' : 'N'
                ]);
                $is_Created = UserToCard::create([
                    'user_id' => Auth::id(),
                    'card_name' => @$request->card_name,
                    'cpf_no' => @$request->card_cpf_no,
                    'phonecode' => @$request->card_phonecode,
                    'area_code' => @$request->card_area_code,
                    'mobile' => @$request->card_mobile,
                    'dob' => @$dob,
                    'card_id' => @$card->getId(),
                    'first_six' => @$card->getFirst6(),
                    'last_four' => @$card->getLast4(),
                    'card_brand' => @$card->getBrand(),
                    'cred_type' => "CREDIT_CARD",
                    'card_response' => json_encode($card),
                ]);
                if (@$is_Created) {
                    Booking::where('token_no', $request->order_no)->update([
                        'order_status'  =>  'A'
                    ]);
                    session()->flash('success', \Lang::get('client_site.now_your_profile_is'));
                    return redirect()->back();
                } else {
                    session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
                    return redirect()->back();
                }
                //}
                /* catch(\Exception $e){
                 session()->flash('error', 'Internal server error.');
                return redirect()->back();
            }*/
            } else {
                session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
                return redirect()->back();
            }
        } catch (\Moip\Exceptions\ValidationException $e) {
            session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
            return redirect()->back();
        } catch (Exceptions\UnexpectedException $e) {
            session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
            return redirect()->back();
        } catch (Exception $e) {
            session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
            return redirect()->back();
        }
    }

    /**
     * charge user after booking is complete
     *
     * @method: chargeUser
     */
    private function chargeUser($token)
    {
        $user_id = Booking::where('token_no', $token)->first()->load('parentCatDetails', 'childCatDetails', 'profDetails');

        $cardDetails = UserToCard::where('user_id', $user_id->user_id)->first();
        $access_token = getAdminToken();

        $customer = User::where([
            'id' => @$user_id->user_id
        ])->first();
        // if($user_id->is_coupon == 'Y'){
        //     // In Here the Primary account is the Admin account and the secondary account is the professional account.
        //     $amount = bcmul(@$user_id->sub_total, 100); // calculating on cents...
        //     $split_amount = bcmul(@$user_id->amount / 2, 100); // calculating on cents...]
        //     $amount = (int) $amount;
        //     $split_amount = (int) $split_amount;
        // }else{
        //     // In Here the Primary account is the Admin account and the secondary account is the professional account.
        //     $amount = bcmul(@$user_id->amount, 100); // calculating on cents...
        //     $split_amount = bcmul(@$user_id->amount / 2, 100); // calculating on cents...]
        //     $amount = (int) $amount;
        //     $split_amount = (int) $split_amount;
        // }
        $referDiscount = ReferralDiscount::first();
        $checkTeacher = User::where('id', $user_id->professional_id)->first();
        $professionalCommissionList = Commission::first();
        $professionalCommission = 50;
        $x = 0;
        if (@$user_id->no_of_installment > 1 && @$user_id->installment_charge == 'Y') {

            $i = @$user_id->no_of_installment;
            $x2 = 4.50;
            $x3 = 5.00;
            $x4 = 5.50;
            $x5 = 6.50;
            $x6 = 7.50;
            $x7 = 8.50;
            $x8 = 9.50;
            $x9 = 10.50;
            $x10 = 11.50;
            $x11 = 12.00;
            $x12 = 12.50;
            if ($i == 2) {
                $x = $x2;
            }
            if ($i == 3) {
                $x = $x3;
            }
            if ($i == 4) {
                $x = $x4;
            }
            if ($i == 5) {
                $x = $x5;
            }
            if ($i == 6) {
                $x = $x6;
            }
            if ($i == 7) {
                $x = $x7;
            }
            if ($i == 8) {
                $x = $x8;
            }
            if ($i == 9) {
                $x = $x9;
            }
            if ($i == 10) {
                $x = $x10;
            }
            if ($i == 11) {
                $x = $x11;
            }
            if ($i == 12) {
                $x = $x12;
            }
        }

        // if ($user_id->booking_type == 'C') {
        //     // $professionalCommission = $professionalCommissionList->chat_commission - $x;
        //     $professionalCommission = $checkTeacher->chat_commission - $x;
        // } else {
        //     // $professionalCommission = $professionalCommissionList->commission - $x;
        //     $professionalCommission = $checkTeacher->video_commission - $x;
        // }
        // dd($user_id->booking_type);
        if ($user_id->booking_type == 'C') {
            if(@$checkTeacher->chat_commission> 0 && $checkTeacher->chat_commission!==null){
                $commision=$checkTeacher->chat_commission - $x;
            }else{
                $commision=$professionalCommissionList->chat_commission- $x;
            }
            $professionalCommission = $commision;
        } else {
            if(@$checkTeacher->video_commission>0 && $checkTeacher->video_commission!==null){
                $commision=$checkTeacher->video_commission- $x;
            }else{
                $commision=$professionalCommissionList->commission- $x;
            }
            $professionalCommission = $commision;
        }
        if (@$checkTeacher->is_paid_activity == 'N') {

            $discount = $professionalCommission + $referDiscount->referrer_teacher_discount;
            $amount_professional = (@$user_id->amount * @$discount) / 100;
            $split_amount   =  bcmul(@$amount_professional, 100); // calculating on cents...
            $split_amount   =  (int) @$split_amount;
        } elseif (@$checkTeacher->benefit_count > 0) {

            $discount = $professionalCommission + $referDiscount->referred_teacher_discount;
            $amount_professional = (@$user_id->amount * @$discount) / 100;
            $split_amount   =  bcmul(@$amount_professional, 100); // calculating on cents...
            $split_amount   =  (int) @$split_amount;
        } else {

            $discount = $professionalCommission;
            $amount_professional = (@$user_id->amount * @$discount) / 100;
            $split_amount   =  bcmul(@$amount_professional, 100); // calculating on cents...
            $split_amount   =  (int) @$split_amount;
        }
        $amount =  bcmul((@$user_id->sub_total + @$user_id->extra_installment_charge - $user_id->wallet), 100); // calculating on cents...
        $amount =  (int) $amount;

        if (env('PAYMENT_ENVIORNMENT') == 'live') {
            $moip = new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_PRODUCTION);
        } else if (env('PAYMENT_ENVIORNMENT') == 'sandbox') {
            $moip = new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_SANDBOX);
        }

        $order = $moip->orders()->setOwnId(uniqid())
            ->addItem("Consultation Fees", 1, @$user_id->parentCatDetails->name, @$amount)
            ->setShippingAmount(0)->setAddition(0)->setDiscount(0)
            ->setCustomerId(@$customer->customer_id);
            if(@$user_id->profDetails->professional_access_token && $user_id->is_wallet_use=='N'){
                $order= $order->addReceiver(@$user_id->profDetails->professional_id, "SECONDARY", @$split_amount, 0, false);
            }
            $order=$order->create();
        Booking::where([
            'token_no' => @$token
        ])->update([
            'moip_order_id'  =>  @$order->getId()
        ]);

        return $this->payment($order, $customer, $cardDetails, $user_id);
    }

    /**
     * For Deducting customer balance
     * @method: payment
     */
    private function payment($order, $user, $card, $userBooking)
    {
        if (env('PAYMENT_ENVIORNMENT') == 'live') {
            $url = Moip::ENDPOINT_PRODUCTION;
        } else if (env('PAYMENT_ENVIORNMENT') == 'sandbox') {
            $url = Moip::ENDPOINT_SANDBOX;
        }
        $data = array(
            'installmentCount' => $userBooking->no_of_installment,
            'statementDescriptor' => 'netbhe',
            'fundingInstrument' =>
            array(
                'method' => 'CREDIT_CARD',
                'creditCard' =>
                array(
                    'id' => @$card->card_id,
                    'store' => true,
                    'holder' =>
                    array(
                        'fullname' => @$card->card_name,
                        'birthdate' => @$user->dob,
                        'taxDocument' =>
                        array(
                            'type' => 'CPF',
                            'number' => @$user->cpf_no,
                        ),
                        'phone' =>
                        array(
                            'countryCode' => @$user->countryName->phonecode,
                            'areaCode' => @$user->area_code,
                            'number' => @$user->mobile,
                        ),
                        'billingAddress' =>
                        array(
                            'city' => @$user->city,
                            'district' => @$user->district,
                            'street' => @$user->street_name,
                            'streetNumber' => @$user->street_number,
                            'zipCode' => @$user->zipcode,
                            'state' => substr(@$user->stateName->name, 0, 2),
                            'country' => substr(@$user->countryName->name, 0, 3),
                        ),
                    ),
                ),
            ),
        );
        $crl = curl_init();
        $header = array();
        // $header[] = 'Content-length: 0';
        $header[] = 'Content-type: application/json';
        $header[] = 'Authorization: Bearer ' . $this->adminAccessToken;
        curl_setopt($crl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($crl, CURLOPT_URL, "{$url}/v2/orders/" . @$order->getId() . "/payments");
        curl_setopt($crl, CURLOPT_POST, 1);
        curl_setopt($crl, CURLOPT_POST, true);
        curl_setopt($crl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        $payment_output = curl_exec($crl);
        curl_close($crl);
        $payment_output = json_decode($payment_output);
        if (@$payment_output->id) {
            $referDiscount = ReferralDiscount::first();
            $checkTeacher = User::where('id', $userBooking->professional_id)->first();
            $professionalCommissionList = Commission::first();
            $professionalCommission = 50;
            $x = 0;
            if (@$userBooking->no_of_installment > 1 && @$userBooking->installment_charge == 'Y') {

                $i = @$userBooking->no_of_installment;
                $x2 = 4.50;
                $x3 = 5.00;
                $x4 = 5.50;
                $x5 = 6.50;
                $x6 = 7.50;
                $x7 = 8.50;
                $x8 = 9.50;
                $x9 = 10.50;
                $x10 = 11.50;
                $x11 = 12.00;
                $x12 = 12.50;
                if ($i == 2) {
                    $x = $x2;
                }
                if ($i == 3) {
                    $x = $x3;
                }
                if ($i == 4) {
                    $x = $x4;
                }
                if ($i == 5) {
                    $x = $x5;
                }
                if ($i == 6) {
                    $x = $x6;
                }
                if ($i == 7) {
                    $x = $x7;
                }
                if ($i == 8) {
                    $x = $x8;
                }
                if ($i == 9) {
                    $x = $x9;
                }
                if ($i == 10) {
                    $x = $x10;
                }
                if ($i == 11) {
                    $x = $x11;
                }
                if ($i == 12) {
                    $x = $x12;
                }
            }

            // if ($userBooking->booking_type == 'C') {
            //     $professionalCommission = $professionalCommissionList->chat_commission - $x;
            // } else {
            //     $professionalCommission = $professionalCommissionList->commission - $x;
            // }
            if ($userBooking->booking_type == 'C') {
                if(@$checkTeacher->chat_commission>0 && $checkTeacher->chat_commission!==null){
                    $commision=$checkTeacher->chat_commission - $x;
                }else{
                    $commision=$professionalCommissionList->chat_commission- $x;
                }
                $professionalCommission = $commision;
            } else {
                if(@$checkTeacher->video_commission>0 && $checkTeacher->video_commission!==null){
                    $commision=$checkTeacher->video_commission- $x;
                }else{
                    $commision=$professionalCommissionList->commission- $x;
                }
                $professionalCommission = $commision;
            }

            if (@$checkTeacher->is_paid_activity == 'N') {
                $discount = $professionalCommission + $referDiscount->referrer_teacher_discount;
                $amount = (@$userBooking->amount * @$discount) / 100;
                $professional_amount   =  @$amount;
                $admin_amount         =   @$userBooking->sub_total - @$amount;
                User::where('id', @$checkTeacher->id)->update(['is_paid_activity' => 'C']);
                User::where('id', @$checkTeacher->referrer_id)->increment("benefit_count", 1);
            } elseif (@$checkTeacher->benefit_count > 0) {
                $discount = $professionalCommission + $referDiscount->referred_teacher_discount;
                $amount = (@$userBooking->amount * @$discount) / 100;
                $professional_amount   =  @$amount;
                $admin_amount         =   @$userBooking->sub_total - @$amount;
                User::where('id', @$checkTeacher->id)->decrement("benefit_count", 1);
            } else {
                $discount = $professionalCommission;
                $amount = (@$userBooking->amount * @$discount) / 100;
                $professional_amount   =  @$amount;
                $admin_amount         =   @$userBooking->sub_total - @$amount;
            }
            $total_amount         =  @$userBooking->sub_total + @$userBooking->extra_installment_charge -@$userBooking->wallet;
            if (@$payment_output->status == 'CANCELLED') {
                $UpdateBooking = Booking::where([
                    'token_no'        =>  @$userBooking->token_no
                ])->update([
                    'payment_status'  =>  "F"
                ]);

                return false;
            }
            // $payment = Payment::create([
            //     'token_no'              =>  @$userBooking->token_no,
            //     'order_id'              =>  @$order->getId(),
            //     'payment_id'            =>  @$payment_output->id,
            //     'user_id'               =>  @$userBooking->user_id,
            //     'professional_id'       =>  @$userBooking->professional_id,
            //     'professional_amount'   =>  @$professional_amount,
            //     'admin_amount'          =>  @$admin_amount,
            //     'total_amount'          =>  @$total_amount,
            //     'wirecard_response'     =>  json_encode($payment_output),
            //     'balance_status'        =>  'R',
            //     'order_type'            =>  'B',
            // ]);
            $payment = Payment::create([
                'token_no'              =>  @$userBooking->token_no,
                'pg_order_id'           =>  @$order->getId(),
                'pg_payment_id'         =>  @$payment_output->id,
                'user_id'               =>  @$userBooking->user_id,
                'total_amount'          =>  @$total_amount,
                'wallet'                =>  @$userBooking->wallet,
                'is_wallet_use'         =>  @$userBooking->is_wallet_use,
                'pg_response'           =>  json_encode($payment_output),
                'payment_status'        =>  'P',
                'payment_type'          =>  'C',
                'order_type'            =>  'B',
            ]);
            if (@$payment) {

                $add = [];
                $add['payment_master_id'] = $payment->id;
                $add['product_order_details_id'] = 0;
                $add['professional_amount'] = @$professional_amount;
                $add['admin_commission'] = @$admin_amount;
                $add['affiliate_commission'] = 0;
                $add['affiliate_id'] = 0;
                $add['professional_id'] = @$userBooking->professional_id;
                $add['type'] = 'P';
                if(@$userBooking->is_wallet_use ='Y'){
                    $add['withdraw_by_wirecard'] = 'N';
                }else{
                    $add['withdraw_by_wirecard'] = @$userBooking->profDetails->professional_access_token? 'Y':'N';
                }
                $add['balance_status'] = 'R';
                PaymentDetails::create($add);

                $UpdateBooking = Booking::where([
                    'token_no'        =>  @$userBooking->token_no
                ])->update([
                    'payment_status'  =>  "P"
                ]);
                $referralStatus = User::where('id', @$userBooking->user_id)->first();
                if (@$referralStatus->is_paid_activity == 'N') {
                    User::where('id', @$userBooking->user_id)->update(['is_paid_activity' => 'C']);
                    User::where('id', @$referralStatus->referrer_id)->increment("benefit_count", 1);
                }
                $userdata = User::where('id', Auth::id())->first();
                $userdata1 = User::where('id', @$userBooking->professional_id)->first();
                $professionalmail = $userdata1->email;
                $usermail = $userdata->email;
                $bookingdata = $userBooking;

                // // for send mail to user
                Mail::send(new UserBooking($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));

                // // for send mail to professional
                Mail::send(new ProfessionalBooking($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                User::where('id', $userdata->id)->update(['wallet_balance'=>($userdata->wallet_balance - @$userBooking->wallet)]);
                // whatsHere
                // has been purchased video call / chat
                if(@$userdata1->mobile && @$userdata1->country_code){
                    // dd("Getitng In");

                    /* $msg = "Dear " . (@$userdata1->nick_name ?? @$userdata1->name) .
                    ", you have a new session with " . (@$userdata->nick_name ?? @$userdata->name) .
                    ". Booking Token: " . (@$bookingdata->token_no); */

                    $msg = "Olá " . (@$userdata1->nick_name ?? @$userdata1->name) . ",
                    Obrigado por usar a Netbhe! você tem uma nova sessão com: " . (@$userdata->nick_name ?? @$userdata->name) .
                    ". Esperamos que aproveite sua sessão! 😉 Token de reserva: " . (@$bookingdata->token_no);

                    $mobile = "+".@$userdata1->country_code . @$userdata1->mobile;
                    // $mobile = "+554598290176";
                    // $mobile = "+918017096564";
                    // dd($msg);
                    $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                    // if($result['statusCode'] == 200){
                    //     dd("Success");
                    // } else {
                    //     dd($result);
                    // }
                }
                return true;
            } else {
                $UpdateBooking = Booking::where([
                    'token_no'        =>  @$userBooking->token_no
                ])->update([
                    'payment_status'  =>  "F"
                ]);

                return false;
            }
        }
    }

    /**
     *method:createCustomer()
     *purpose:For Creating Customer on WireCard
     *Author:@bhisek
     */
    public function createCustomer(Request $request)
    {
        try {
            if (env('PAYMENT_ENVIORNMENT') == 'live') {
                $moip = new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_PRODUCTION);
            } else if (env('PAYMENT_ENVIORNMENT') == 'sandbox') {
                $moip = new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_SANDBOX);
            }

            $customer = $moip->customers()
                ->setOwnId(uniqid())
                ->setFullname(Auth::guard('web')->user()->name)
                ->setEmail(Auth::guard('web')->user()->email)
                ->setBirthDate(Auth::guard('web')->user()->dob)
                ->setTaxDocument(Auth::guard('web')->user()->cpf_no)
                ->setPhone(Auth::guard('web')->user()->area_code, Auth::guard('web')->user()->mobile)
                ->addAddress('BILLING', Auth::guard('web')->user()->street_name, Auth::guard('web')->user()->street_number, Auth::guard('web')->user()->district, Auth::guard('web')->user()->city, strtoupper(substr(Auth::guard('web')->user()->stateName->name, 0, 2)), Auth::guard('web')->user()->zipcode, Auth::guard('web')->user()->complement)
                ->addAddress('SHIPPING', Auth::guard('web')->user()->street_name, Auth::guard('web')->user()->street_number, Auth::guard('web')->user()->district, Auth::guard('web')->user()->city, substr(Auth::guard('web')->user()->stateName->name, 0, 2), Auth::guard('web')->user()->zipcode, Auth::guard('web')->user()->complement)->create();
            # SAVE CUSTOMER CREADIT CARD
            $card  =  $moip->customers()->creditCard()
                ->setExpirationMonth($request->expiry_month)
                ->setExpirationYear($request->expiry_year)
                ->setNumber($request->cardnumber)
                ->setCVC($request->cvc)
                ->setFullName($request->card_name)
                ->setBirthDate(Auth::guard('web')->user()->dob)
                ->setTaxDocument('CPF', Auth::guard('web')->user()->cpf_no)
                ->setPhone(Auth::guard('web')->user()->countryName->phonecode, Auth::guard('web')->user()->area_code, Auth::guard('web')->user()->mobile)
                ->create($customer->getId());
            if (!@$card->getId()) {
                session()->flash('error', \Lang::get('client_site.internal_server_error'));
                return redirect()->back();
            }

            $update = User::where('id', Auth::id())->update([
                'customer_id'                       =>  @$customer->getId(),
                'is_credit_card_added'              =>  @$card->getStore() == true ? 'Y' : 'N'
            ]);
            $is_Created = UserToCard::create([
                'user_id'       =>  Auth::id(),
                'card_id'       =>  $card->getId(),
                'first_six'     =>  $card->getFirst6(),
                'last_four'     =>  $card->getLast4(),
                'card_brand'    =>  $card->getBrand(),
                'cred_type'     =>  "CREDIT_CARD"
            ]);
            if (@$is_Created) {
                Booking::where('token_no', @$request->order_no)->update([
                    'order_status'  =>  'A'
                ]);
                session()->flash('success', \Lang::get('client_site.card_successfully_added'));
                return redirect()->back();
            } else {
                session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
                return redirect()->back();
            }
        } catch (\Exception $e) {
            session()->flash('error', \Lang::get('client_site.internal_server_error'));
            return redirect()->back();
        }
    }

    /*
    @Below Functions are used for validating a CPF number.
    */
    public function verificaDigitos($digito1, $digito2, $ver, $ver2)
    {
        $num = $digito1 % 11;
        $digito1 = ($num < 2) ? 0 : 11 - $num;
        $num2 = $digito2 % 11;
        $digito2 = ($num2 < 2) ? 0 : 11 - $num2;
        if ($digito1 === (int) $this->numeral[$ver] && $digito2 === (int) $this->numeral[$ver2]) {
            return true;
        } else {
            return false;
        }
    }
    private function verificaCPF()
    {
        $num1 = ($this->numeral[0] * 10) + ($this->numeral[1] * 9) + ($this->numeral[2] * 8) + ($this->numeral[3] * 7) + ($this->numeral[4] * 6)
            + ($this->numeral[5] * 5) + ($this->numeral[6] * 4) + ($this->numeral[7] * 3) + ($this->numeral[8] * 2);
        $num2 = ($this->numeral[0] * 11) + ($this->numeral[1] * 10) + ($this->numeral[2] * 9) + ($this->numeral[3] * 8) + ($this->numeral[4] * 7)
            + ($this->numeral[5] * 6) + ($this->numeral[6] * 5) + ($this->numeral[7] * 4) + ($this->numeral[8] * 3) + ($this->numeral[9] * 2);
        return $this->verificaDigitos($num1, $num2, 9, 10);
    }
    private function verificaCNPJ()
    {
        $num1 = ($this->numeral[0] * 5) + ($this->numeral[1] * 4) + ($this->numeral[2] * 3) + ($this->numeral[3] * 2) + ($this->numeral[4] * 9) + ($this->numeral[5] * 8)
            + ($this->numeral[6] * 7) + ($this->numeral[7] * 6) + ($this->numeral[8] * 5) + ($this->numeral[9] * 4) + ($this->numeral[10] * 3) + ($this->numeral[11] * 2);
        $num2 = ($this->numeral[0] * 6) + ($this->numeral[1] * 5) + ($this->numeral[2] * 4) + ($this->numeral[3] * 3) + ($this->numeral[4] * 2) + ($this->numeral[5] * 9)
            + ($this->numeral[6] * 8) + ($this->numeral[7] * 7) + ($this->numeral[8] * 6) + ($this->numeral[9] * 5) + ($this->numeral[10] * 4) + ($this->numeral[11] * 3) + ($this->numeral[12] * 2);
        return $this->verificaDigitos($num1, $num2, 12, 13);
    }
    private function getNumeral()
    {
        $this->numeral = preg_replace("/[^0-9]/", "", $this->CPF_CNPJ);
        $strLen = strlen($this->numeral);
        $ret = false;
        switch ($strLen) {
            case 11:
                if ($this->verificaCPF()) {
                    $this->tipo = 'CPF';
                    $ret = true;
                }
                break;
            case 14:
                if ($this->verificaCNPJ()) {
                    $this->tipo = 'CNPJ';
                    $ret = true;
                }
                break;
            default:
                $ret = false;
                break;
        }
        return $ret;
    }
    public function __toString()
    {
        return $this->CPF_CNPJ;
    }
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * For Free Session Payment Create
     * @method: freeSessionPayment
     */
    private function freeSessionPayment($token)
    {
        $userBooking = Booking::where('token_no', $token)->first();
        // $payment = Payment::create([
        //     'token_no'              =>  @$userBooking->token_no,
        //     'order_id'              =>  'ORD-' . @$userBooking->token_no,
        //     // 'payment_id'            =>  @$payment_output->id,
        //     'user_id'               =>  @$userBooking->user_id,
        //     'professional_id'       =>  @$userBooking->professional_id,
        //     'professional_amount'   =>  @$userBooking->amount / 2,
        //     'admin_amount'          =>  @$userBooking->amount / 2,
        //     'total_amount'          =>  @$userBooking->amount,
        //     // 'wirecard_response'     =>  json_encode($payment_output),
        //     'balance_status'        =>  'W'
        // ]);
        // Booking::where([
        //     'token_no' => @$token
        // ])->update([
        //     'moip_order_id'  =>  'ORD-' . @$userBooking->token_no
        // ]);

        $payment = Payment::create([
            'token_no'              =>  @$userBooking->token_no,
            'pg_order_id'           =>  'ORD-' . @$userBooking->token_no,
            'user_id'               =>  @$userBooking->user_id,
            'total_amount'          =>  @$userBooking->amount,
            'payment_status'        =>  'P',
            // 'payment_type'          =>  'BA',
            'order_type'            =>  'B',
        ]);

        if (@$payment) {
            $add = [];
            $add['payment_master_id'] = $payment->id;
            $add['product_order_details_id'] = 0;
            $add['professional_amount'] = @$userBooking->amount / 2;
            $add['admin_commission'] = @$userBooking->amount / 2;
            $add['affiliate_commission'] = 0;
            $add['affiliate_id'] = 0;
            $add['professional_id'] = @$userBooking->professional_id;
            $add['type'] = 'P';
            $add['withdraw_by_wirecard'] = 'N';
            $add['balance_status'] = 'W';
            PaymentDetails::create($add);
            $UpdateBooking = Booking::where([
                'token_no'        =>  @$userBooking->token_no
            ])->update([
                'payment_status'  =>  "P"
            ]);
            $userdata = User::where('id', Auth::id())->first();
            $userdata1 = User::where('id', @$userBooking->professional_id)->first();
            $professionalmail = $userdata1->email;
            $usermail = $userdata->email;
            $bookingdata = $userBooking;

            // // for send mail to user
            Mail::send(new UserBooking($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));

            // // for send mail to professional
            Mail::send(new ProfessionalBooking($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
            // $referralStatus= User::where('id', @$userBooking->user_id)->first();
            // if(@$referralStatus->is_paid_activity=='N'){
            //     User::where('id', @$userBooking->user_id)->update(['is_paid_activity'=>'C']);
            //     User::where('id', @$referralStatus->referrer_id)->increment("benefit_count", 1);
            // }
            return true;
        } else {
            $UpdateBooking = Booking::where([
                'token_no'        =>  @$userBooking->token_no
            ])->update([
                'payment_status'  =>  "F"
            ]);
            return false;
        }
    }
    /**
     * For Check Coupon Code
     * @method: checkCoupon
     */
    public function checkCoupon(Request $request)
    {
        $response = [
            "jsonrpc" => "2.0"
        ];
        if ($request->params['coupon']) {

            $coupon = Coupon::where('coupon_code', $request->params['coupon'])->where('coupon_status', 'A')->first();
            $dated = date_create(@$coupon->exp_date);
            if (@$coupon && $coupon->added_by != 'P') {
                if ($dated < date('Y-m-d')) {
                    $response['status'] = 2;
                    $response['message'] = \Lang::get('client_site.coupon_expiredy');
                } else {
                    $response['status'] = 1;
                    $response['message'] = \Lang::get('client_site.coupon_applied');
                }
            } else {
                $response['status'] = 0;
                $response['message'] = \Lang::get('client_site.coupon_not_valid');
            }
            return response()->json($response);
        }
    }
    /**
     * For Add Payment Method and Coupon Apply
     * @method: addPaymentCoupon
     */
    public function addPaymentCoupon($slug, Request $request)
    {
        $request->validate([
            "payment_method" =>  "required|in:C,BA,S,P",
        ]);
        $booking = Booking::where('token_no', $slug)->where('user_id', @Auth::id())->first();
        $coupon = Coupon::where('coupon_code', $request->couponCode)->where('coupon_status', 'A')->first();
        $checkStudent = User::where('id', @Auth::id())->first();
        $referDiscount = ReferralDiscount::first();
        if (@$booking) {
            $upd = [];
            $upd['payment_type'] = $request->payment_method;
            if (@$request->installment_select) {
                if (@$request->no_installment > 1 && @$request->no_installment < 13) {
                    $i = $request->no_installment;
                    $x2 = 4.50;
                    $x3 = 5.00;
                    $x4 = 5.50;
                    $x5 = 6.50;
                    $x6 = 7.50;
                    $x7 = 8.50;
                    $x8 = 9.50;
                    $x9 = 10.50;
                    $x10 = 11.50;
                    $x11 = 12.00;
                    $x12 = 12.50;
                    if ($i == 2) {
                        $x = $x2;
                    }
                    if ($i == 3) {
                        $x = $x3;
                    }
                    if ($i == 4) {
                        $x = $x4;
                    }
                    if ($i == 5) {
                        $x = $x5;
                    }
                    if ($i == 6) {
                        $x = $x6;
                    }
                    if ($i == 7) {
                        $x = $x7;
                    }
                    if ($i == 8) {
                        $x = $x8;
                    }
                    if ($i == 9) {
                        $x = $x9;
                    }
                    if ($i == 10) {
                        $x = $x10;
                    }
                    if ($i == 11) {
                        $x = $x11;
                    }
                    if ($i == 12) {
                        $x = $x12;
                    }
                    $down = (1 - ($x / 100) - (5.49 / 100));
                    $up = $booking->sub_total * ($x / 100);
                    $additional = ($up / $down);
                    if ($booking->installment_charge == 'Y') {
                        $additional = 0.00;
                    }

                    $upd['no_of_installment'] = @$request->no_installment;
                    $upd['extra_installment_charge'] = @$additional;
                }
            } elseif (@$coupon && @$request->installment_select == null) {
                $subtotal = $booking->amount - (($booking->amount * $coupon->discount) / 100);
                $upd['sub_total'] = (int) $subtotal;
                $upd['coupon_id'] = $coupon->id;
                $upd['is_coupon'] = 'Y';
            } else {
                if (@$checkStudent->is_paid_activity == 'N') {
                    $subtotal = $booking->amount - (($booking->amount * $referDiscount->referrer_student_discount) / 100);
                    $upd['sub_total'] = (int) $subtotal;
                } elseif (@$checkStudent->benefit_count > 0) {
                    $subtotal = $booking->amount - (($booking->amount * $referDiscount->referred_student_discount) / 100);
                    $upd['sub_total'] = (int) $subtotal;
                    User::where('id', @$checkStudent->id)->decrement("benefit_count", 1);
                }
            }
            // dd($upd);
            $update = Booking::where('id', $booking->id)->update($upd);
            if (@$update) {
                return redirect()->back();
            }
        }
        return redirect()->back();
    }
    /**
     * For Free Session Payment Create
     * @method: freeSessionPayment
     */
    private function bankAccountPayment($token)
    {
        $userBooking = Booking::where('token_no', $token)->first();
        $referDiscount = ReferralDiscount::first();
        $checkTeacher = User::where('id', $userBooking->professional_id)->first();
        $professionalCommissionList = Commission::first();
        // dd ($checkTeacher->video_commission);
        $professionalCommission = 50;
        if ($userBooking->booking_type == 'C') {
            if(@$checkTeacher->chat_commission>0 && $checkTeacher->chat_commission!==null){
                $commision=$checkTeacher->chat_commission;
            }else{
                $commision=$professionalCommissionList->chat_commission;
            }
            $professionalCommission = $commision;

            // $professionalCommission = $professionalCommissionList->chat_commission;
        } else {
            if(@$checkTeacher->video_commission>0 && $checkTeacher->video_commission!==null){
                $commision=$checkTeacher->video_commission;
            }else{
                $commision=$professionalCommissionList->commission;
            }
            $professionalCommission = $commision;
        }

        if (@$checkTeacher->is_paid_activity == 'N') {
            $discount = $professionalCommission + $referDiscount->referrer_teacher_discount;
            $amount = (@$userBooking->amount * @$discount) / 100;
            $professional_amount   =  @$amount;
            $admin_amount         =   @$userBooking->sub_total - @$amount;
            User::where('id', @$checkTeacher->id)->update(['is_paid_activity' => 'C']);
            User::where('id', @$checkTeacher->referrer_id)->increment("benefit_count", 1);
        } elseif (@$checkTeacher->benefit_count > 0) {
            $discount = $professionalCommission + $referDiscount->referred_teacher_discount;
            $amount = (@$userBooking->amount * @$discount) / 100;
            $professional_amount   =  @$amount;
            $admin_amount         =   @$userBooking->sub_total - @$amount;
            User::where('id', @$checkTeacher->id)->decrement("benefit_count", 1);
        } else {
            $discount = $professionalCommission;
            $amount = (@$userBooking->amount * @$discount) / 100;
            $professional_amount   =  @$amount;
            $admin_amount         =   @$userBooking->sub_total - @$amount;
        }
        $total_amount         =  @$userBooking->sub_total;
        // dd($checkTeacher->benefit_count);
        // $payment = Payment::create([
        //     'token_no'              =>  @$userBooking->token_no,
        //     'order_id'              =>  'ORD-' . @$userBooking->token_no,
        //     // 'payment_id'            =>  @$payment_output->id,
        //     'user_id'               =>  @$userBooking->user_id,
        //     'professional_id'       =>  @$userBooking->professional_id,
        //     'professional_amount'   =>  @$professional_amount,
        //     'admin_amount'          =>  @$admin_amount,
        //     'total_amount'          =>  @$total_amount,
        //     // 'wirecard_response'     =>  json_encode($payment_output),
        //     'balance_status'        =>  'R',
        //     'order_type'            =>  'B',
        // ]);
        $payment = Payment::create([
            'token_no'              =>  @$userBooking->token_no,
            'pg_order_id'           =>  'ORD-' . @$userBooking->token_no,
            'user_id'               =>  @$userBooking->user_id,
            'total_amount'          =>  @$total_amount,
            'payment_status'        =>  'PR',
            'payment_type'          =>  'BA',
            'order_type'            =>  'B',
            'wallet'                =>  @$userBooking->wallet,
            'is_wallet_use'         =>  @$userBooking->is_wallet_use,
        ]);
        if (@$payment) {
            $add = [];
            $add['payment_master_id'] = $payment->id;
            $add['product_order_details_id'] = 0;
            $add['professional_amount'] = @$professional_amount;
            $add['admin_commission'] = @$admin_amount;
            $add['affiliate_commission'] = 0;
            $add['affiliate_id'] = 0;
            $add['professional_id'] = @$userBooking->professional_id;
            $add['type'] = 'P';
            $add['withdraw_by_wirecard'] = 'N';
            $add['balance_status'] = 'R';
            PaymentDetails::create($add);
            $UpdateBooking = Booking::where([
                'token_no'        =>  @$userBooking->token_no
            ])->update([
                'payment_status'  =>  "PR",
                'order_status'  =>  "A"
            ]);
            $referralStatus = User::where('id', @$userBooking->user_id)->first();
            if (@$referralStatus->is_paid_activity == 'N') {
                User::where('id', @$userBooking->user_id)->update(['is_paid_activity' => 'C']);
                User::where('id', @$referralStatus->referrer_id)->increment("benefit_count", 1);
            }
            $userdata = User::where('id', Auth::id())->first();
            $userdata1 = User::where('id', @$userBooking->professional_id)->first();
            $professionalmail = $userdata1->email;
            $usermail = $userdata->email;
            $bookingdata = $userBooking;
            User::where('id', $userdata->id)->update(['wallet_balance'=>($userdata->wallet_balance - @$userBooking->wallet)]);

            // // for send mail to user
            // Mail::send(new UserBooking($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));

            // // // for send mail to professional
            // Mail::send(new ProfessionalBooking($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
            // // for send mail to admin for Bank account payment
            Mail::send(new UserBankAccountBooking($bookingdata->id, $usermail));

            Mail::send(new BankAccountPayment($bookingdata->id));
            // whatsHere
            // dd(@$userdata1->mobile && @$userdata1->country_code);
            // booked but under approval from admin
            if(@$userdata1->mobile && @$userdata1->country_code){
                // dd("Getitng In");

                /* $msg = "Dear " . (@$userdata1->nick_name ?? @$userdata1->name) .
                ", you have a new session with " . (@$userdata->nick_name ?? @$userdata->name) .
                ". As it is paid through a bank account, currently it is under Admin approval. Booking Token: " . (@$bookingdata->token_no); */

                $msg = "Olá " . (@$userdata1->nick_name ?? @$userdata1->name) . ",
                Obrigado por usar a Netbhe!  você tem uma nova sessão com: " . (@$userdata->nick_name ?? @$userdata->name) .
                ". Como o pagamento foi por meio de transferência bancária, estamos aguardando a confirmação do pagamento.
                Por favor, envie o comprovante pela plataforma. Esperamos que aproveite sua sessão! 😉 Token de reserva: " . (@$bookingdata->token_no);

                $mobile = "+".@$userdata1->country_code . @$userdata1->mobile;
                // $mobile = "+554598290176";
                // $mobile = "+918017096564";
                // dd($msg);
                $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                // dd($result);
                // if($result['statusCode'] == 200){
                //     dd("Success");
                // } else {
                //     dd($result);
                // }
            }
            return true;
        } else {
            $UpdateBooking = Booking::where([
                'token_no'        =>  @$userBooking->token_no
            ])->update([
                'payment_status'  =>  "F"
            ]);
            return false;
        }
    }
    /**
     * For upload Payment Document Bank Account Payment
     * @method: uploadDocument
     */
    public function uploadDocument($slug, Request $request)
    {
        $request->validate([
            "upload_file" =>  "required",
        ]);
        $booking = Booking::where('token_no', $slug)->where('user_id', @Auth::id())->first();
        if (@$booking) {
            $upd = [];
            if (@$request->upload_file) {
                $upload_file = $request->upload_file;
                $filename = time() . '-' . rand(1000, 9999) . '.' . $upload_file->getClientOriginalExtension();
                Storage::putFileAs('public/Bank_Document', $upload_file, $filename);
                $upd['payment_document'] = $filename;
            }
            // dd($upd);
            $update = Booking::where('id', $booking->id)->update($upd);
            if (@$update) {
                return redirect()->back();
            }
        }
        return redirect()->back();
    }


    /**
     * For Add Payment Method and Coupon Apply
     * @method: addPaymentCoupon
     */
    public function removePaymentMethod($slug, Request $request)
    {
        $booking = Booking::where('token_no', $slug)->where('user_id', @Auth::id())->first();
        $coupon = Coupon::where('coupon_code', $request->couponCode)->where('coupon_status', 'A')->first();
        $checkStudent = User::where('id', @Auth::id())->first();
        $referDiscount = ReferralDiscount::first();
        if (@$booking) {
            $upd = [];
            $upd['payment_type'] = null;
            $upd['no_of_installment'] = 1;
            $upd['extra_installment_charge'] = 0.00;
            $upd['sub_total'] = (int) $booking->amount;
            $upd['coupon_id'] = null;
            $upd['is_coupon'] = 'N';
            // dd($upd);
            $update = Booking::where('id', $booking->id)->update($upd);
            if (@$update) {
                return redirect()->back();
            }
        }
        return redirect()->back();
    }

   /**
     * Method: paymentCreate
     * Description: This method is used for create payment by stripe
     * Author: Soumojit
     * date:23-JULY-2021
     */
    public function paymentCreate(Request $request)
    {
        $response = [
            'jsonrpc' => 2.0
        ];
        $unique_payment_id = decrypt($request->params['id']);
        $payments = Booking::where('id', $unique_payment_id)->where('user_id', @auth()->user()->id)->first();
        if(@$payments){
            Stripe::setApiKey(config('services.stripe.secret'));
            $paymentIntent = PaymentIntent::create([
                'amount' => (int) bcmul(($payments->sub_total-$payments->wallet), 100),
                'currency' => 'usd',
                'description' => 'Session perched',
                'metadata' => [
                    'unique_payment_id' => encrypt($unique_payment_id),
                ],
                'receipt_email' => @auth()->user()->email,
            ]);
            $response['result']['clientSecret'] = $paymentIntent->client_secret;
            $response['result']['payment'] = $paymentIntent;
            return response()->json($response);
        }
        $response['error']['message'] = \Lang::get('message.error_in_Payment');
        return response()->json($response);
    }
    /**
     * Method: paymentSuccessHandlerStripe
     * Description: This method is used for after payment success by stripe
     * Author: Soumojit
     * date:23-JULY-2021
     */
    public function paymentSuccessHandlerStripe(Request $request){
        $response = [
            'jsonrpc' => 2.0
        ];
        $unique_payment_id = decrypt($request->params['id']);
        $payments = Booking::where('id', $unique_payment_id)->where('user_id', @auth()->user()->id)->first();
        // return $request->params['result']['paymentIntent']['id'];
        $userBooking = Booking::where('token_no', $payments->token_no)->first();
        $referDiscount = ReferralDiscount::first();
        $checkTeacher = User::where('id', $userBooking->professional_id)->first();
        $professionalCommissionList = Commission::first();
        $professionalCommission = 50;
        // if ($userBooking->booking_type == 'C') {
        //     $professionalCommission = $professionalCommissionList->chat_commission;
        // } else {
        //     $professionalCommission = $professionalCommissionList->commission;
        // }
        if ($userBooking->booking_type == 'C') {
            if(@$checkTeacher->chat_commission>0 && $checkTeacher->chat_commission!==null){
                $commision=$checkTeacher->chat_commission;
            }else{
                $commision=$professionalCommissionList->chat_commission;
            }
            $professionalCommission = $commision;

            // $professionalCommission = $professionalCommissionList->chat_commission;
        } else {
            if(@$checkTeacher->video_commission>0 && $checkTeacher->video_commission!==null){
                $commision=$checkTeacher->video_commission;
            }else{
                $commision=$professionalCommissionList->commission;
            }
            $professionalCommission = $commision;
        }

        if (@$checkTeacher->is_paid_activity == 'N') {
            $discount = $professionalCommission + $referDiscount->referrer_teacher_discount;
            $amount = (@$userBooking->amount * @$discount) / 100;
            $professional_amount   =  @$amount;
            $admin_amount         =   @$userBooking->sub_total - @$amount;
            User::where('id', @$checkTeacher->id)->update(['is_paid_activity' => 'C']);
            User::where('id', @$checkTeacher->referrer_id)->increment("benefit_count", 1);
        } elseif (@$checkTeacher->benefit_count > 0) {
            $discount = $professionalCommission + $referDiscount->referred_teacher_discount;
            $amount = (@$userBooking->amount * @$discount) / 100;
            $professional_amount   =  @$amount;
            $admin_amount         =   @$userBooking->sub_total - @$amount;
            User::where('id', @$checkTeacher->id)->decrement("benefit_count", 1);
        } else {
            $discount = $professionalCommission;
            $amount = (@$userBooking->amount * @$discount) / 100;
            $professional_amount   =  @$amount;
            $admin_amount         =   @$userBooking->sub_total - @$amount;
        }
        $total_amount         =  @$userBooking->sub_total;
        $payment = Payment::create([
            'token_no'              =>  @$userBooking->token_no,
            'pg_order_id'           =>  'ORD-' . @$userBooking->token_no,
            'pg_payment_id'           =>  $request->params['result']['paymentIntent']['id'],
            'user_id'               =>  @$userBooking->user_id,
            'total_amount'          =>  @$total_amount,
            'payment_status'        =>  'P',
            'payment_type'          =>  'S',
            'pg_response'          =>  json_encode($request->params['result']),
            'order_type'            =>  'B',
            'wallet'                =>  @$userBooking->wallet,
            'is_wallet_use'         =>  @$userBooking->is_wallet_use,
        ]);
        if (@$payment) {
            $add = [];
            $add['payment_master_id'] = $payment->id;
            $add['product_order_details_id'] = 0;
            $add['professional_amount'] = @$professional_amount;
            $add['admin_commission'] = @$admin_amount;
            $add['affiliate_commission'] = 0;
            $add['affiliate_id'] = 0;
            $add['professional_id'] = @$userBooking->professional_id;
            $add['type'] = 'P';
            $add['withdraw_by_wirecard'] = 'N';
            $add['balance_status'] = 'R';
            PaymentDetails::create($add);
            $UpdateBooking = Booking::where([
                'token_no'        =>  @$userBooking->token_no
            ])->update([
                'payment_status'  =>  "P",
                'order_status'  =>  "A"
            ]);
            $referralStatus = User::where('id', @$userBooking->user_id)->first();
            if (@$referralStatus->is_paid_activity == 'N') {
                User::where('id', @$userBooking->user_id)->update(['is_paid_activity' => 'C']);
                User::where('id', @$referralStatus->referrer_id)->increment("benefit_count", 1);
            }
            $userdata = User::where('id', Auth::id())->first();
            $userdata1 = User::where('id', @$userBooking->professional_id)->first();
            $professionalmail = $userdata1->email;
            $usermail = $userdata->email;
            $bookingdata = $userBooking;
            User::where('id', $userdata->id)->update(['wallet_balance'=>($userdata->wallet_balance - @$userBooking->wallet)]);
            // Mail::send(new UserBankAccountBooking($bookingdata->id, $usermail));

            // Mail::send(new BankAccountPayment($bookingdata->id));

            Mail::send(new UserBooking($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));

            // // for send mail to professional
            Mail::send(new ProfessionalBooking($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
            // whatsHere
            // booking purchase video/chat
            if(@$userdata1->mobile && @$userdata1->country_code){
                // dd("Getitng In");

                /* $msg = "Dear " . (@$userdata1->nick_name ?? @$userdata1->name) .
                ", you have a new session with " . (@$userdata->nick_name ?? @$userdata->name) .
                ". Booking Token: " . (@$bookingdata->token_no); */

                $msg = "Olá " . (@$userdata1->nick_name ?? @$userdata1->name) . ",
                Obrigado por usar a Netbhe! você tem uma nova sessão com: " . (@$userdata->nick_name ?? @$userdata->name) .
                ". Esperamos que aproveite sua sessão! 😉 Token de reserva: " . (@$bookingdata->token_no);

                $mobile = "+".@$userdata1->country_code . @$userdata1->mobile;
                // $mobile = "+554598290176";
                // $mobile = "+918017096564";
                // dd($msg);
                $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                // if($result['statusCode'] == 200){
                //     dd("Success");
                // } else {
                //     dd($result);
                // }
            }
            $response['success']['message'] = \Lang::get('site.Payment_successfull');
            return response()->json($response);
        }
    }


    /**
     * Method: postPaymentWithpaypal
     * Description: This method is used for payment by paypal
     * Author: Sayantani
     * date:13-AUGUST-2021
     */
    public function postPaymentWithpaypal(Request $request)
    {
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        Session::put('ordID',encrypt($request->get('ordID')));
        // dd($request->get('ordID'));

        $unique_payment_id = $request->get('ordID');
        $payments = Booking::where('id', $unique_payment_id)->where('user_id', @auth()->user()->id)->first();

        $userBooking = Booking::where('token_no', $payments->token_no)->first();
        $referDiscount = ReferralDiscount::first();
        $checkTeacher = User::where('id', $userBooking->professional_id)->first();

    	$item_1 = new Item();

        $item_1->setName($request->get('title'))
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setPrice($request->get('amount'));

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency('USD')
            ->setTotal($request->get('amount'));

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Enter Your transaction description');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(route('paypal.booking.status'))
            ->setCancelUrl(route('paypal.booking.status'));

        $payment = new PaymentPaypal();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                // Session::put('error','Connection timeout');
                session()->flash('error', \Lang::get('client_site.connection_timeout'));
                // return Redirect::route('paywithpaypal');
                return Redirect::route('booking.sces', @$userBooking->token_no);
            } else {
                // Session::put('error','Some error occur, sorry for inconvenient');
                session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
                // return Redirect::route('paywithpaypal');
                return Redirect::route('booking.sces', @$userBooking->token_no);
            }
        }

        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        Session::put('paypal_payment_id', $payment->getId());

        if(isset($redirect_url)) {
            // dd($redirect_url);
            // return Redirect::away($redirect_url);
            return redirect()->away($redirect_url)->send();
        }

        // Session::put('error','Unknown error occurred');
        session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
    	// return Redirect::route('paywithpaypal');
        return Redirect::route('booking.sces', @$userBooking->token_no);
    }

    /**
     * Method: getPaymentStatus
     * Description: This method is used after payment success by paypal
     * Author: Sayantani
     * date:13-AUGUST-2021
     */
    public function getPaymentStatus(Request $request)
    {
        $payment_id = Session::get('paypal_payment_id');
        $ordID = decrypt(Session::get('ordID'));

        $unique_payment_id = $ordID;
        // dd($ordID);
        $payments = Booking::where('id', $unique_payment_id)->where('user_id', @auth()->user()->id)->first();

        $userBooking = Booking::where('token_no', $payments->token_no)->first();
        $referDiscount = ReferralDiscount::first();
        $checkTeacher = User::where('id', $userBooking->professional_id)->first();

        Session::forget('paypal_payment_id');
        if (empty($request->input('PayerID')) || empty($request->input('token'))) {
            // Session::put('error','Payment failed');
            session()->flash('error', \Lang::get('site.Payment_failed'));
            // return Redirect::route('paywithpaypal');
            return Redirect::route('booking.sces', @$userBooking->token_no);
        }
        $payment = PaymentPaypal::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId($request->input('PayerID'));
        $result = $payment->execute($execution, $this->_api_context);
        $res_arr = $result->toArray();
        $res = json_decode($result);

        if ($result->getState() == 'approved') {
            // session()->flash('success', \Lang::get('site.Payment_successfull'));
            $userBooking = Booking::where('token_no', $payments->token_no)->first();
            $referDiscount = ReferralDiscount::first();
            $checkTeacher = User::where('id', $userBooking->professional_id)->first();
            $professionalCommissionList = Commission::first();
            $professionalCommission = 50;
            // if ($userBooking->booking_type == 'C') {
            //     $professionalCommission = $professionalCommissionList->chat_commission;
            // } else {
            //     $professionalCommission = $professionalCommissionList->commission;
            // }
            if ($userBooking->booking_type == 'C') {
                if(@$checkTeacher->chat_commission>0 && $checkTeacher->chat_commission!==null){
                    $commision=$checkTeacher->chat_commission;
                }else{
                    $commision=$professionalCommissionList->chat_commission;
                }
                $professionalCommission = $commision;

            } else {
                if(@$checkTeacher->video_commission>0 && $checkTeacher->video_commission!==null){
                    $commision=$checkTeacher->video_commission;
                }else{
                    $commision=$professionalCommissionList->commission;
                }
                $professionalCommission = $commision;
            }

            if (@$checkTeacher->is_paid_activity == 'N') {
                $discount = $professionalCommission + $referDiscount->referrer_teacher_discount;
                $amount = (@$userBooking->amount * @$discount) / 100;
                $professional_amount   =  @$amount;
                $admin_amount         =   @$userBooking->sub_total - @$amount;
                User::where('id', @$checkTeacher->id)->update(['is_paid_activity' => 'C']);
                User::where('id', @$checkTeacher->referrer_id)->increment("benefit_count", 1);
            } elseif (@$checkTeacher->benefit_count > 0) {
                $discount = $professionalCommission + $referDiscount->referred_teacher_discount;
                $amount = (@$userBooking->amount * @$discount) / 100;
                $professional_amount   =  @$amount;
                $admin_amount         =   @$userBooking->sub_total - @$amount;
                User::where('id', @$checkTeacher->id)->decrement("benefit_count", 1);
            } else {
                $discount = $professionalCommission;
                $amount = (@$userBooking->amount * @$discount) / 100;
                $professional_amount   =  @$amount;
                $admin_amount         =   @$userBooking->sub_total - @$amount;
            }
            $total_amount         =  @$userBooking->sub_total;
            $payment = Payment::create([
                'token_no'              =>  @$userBooking->token_no,
                'pg_order_id'           =>  'ORD-' . @$userBooking->token_no,
                'pg_payment_id'         =>  $res_arr['id'],
                'user_id'               =>  @$userBooking->user_id,
                'total_amount'          =>  @$total_amount,
                'payment_status'        =>  'P',
                'payment_type'          =>  'P',
                'pg_response'           =>  json_encode($res),
                'order_type'            =>  'B',
                'wallet'                =>  @$userBooking->wallet,
                'is_wallet_use'         =>  @$userBooking->is_wallet_use,
            ]);
            if (@$payment) {
                // dd("Enterd");
                $add = [];
                $add['payment_master_id'] = $payment->id;
                $add['product_order_details_id'] = 0;
                $add['professional_amount'] = @$professional_amount;
                $add['admin_commission'] = @$admin_amount;
                $add['affiliate_commission'] = 0;
                $add['affiliate_id'] = 0;
                $add['professional_id'] = @$userBooking->professional_id;
                $add['type'] = 'P';
                $add['withdraw_by_wirecard'] = 'N';
                $add['balance_status'] = 'R';
                PaymentDetails::create($add);
                $UpdateBooking = Booking::where([
                    'token_no'        =>  @$userBooking->token_no
                ])->update([
                    'payment_status'  =>  "P",
                    'order_status'  =>  "A"
                ]);
                $referralStatus = User::where('id', @$userBooking->user_id)->first();
                if (@$referralStatus->is_paid_activity == 'N') {
                    User::where('id', @$userBooking->user_id)->update(['is_paid_activity' => 'C']);
                    User::where('id', @$referralStatus->referrer_id)->increment("benefit_count", 1);
                }
                $userdata = User::where('id', Auth::id())->first();
                $userdata1 = User::where('id', @$userBooking->professional_id)->first();
                $professionalmail = $userdata1->email;
                $usermail = $userdata->email;
                $bookingdata = $userBooking;
                // Mail::send(new UserBankAccountBooking($bookingdata->id, $usermail));

                // Mail::send(new BankAccountPayment($bookingdata->id));

                Mail::send(new UserBooking($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));

                // // for send mail to professional
                Mail::send(new ProfessionalBooking($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                User::where('id', $userdata->id)->update(['wallet_balance'=>($userdata->wallet_balance - @$userBooking->wallet)]);
                // whatsHere
                // dd($userdata);
                if(@$userdata1->mobile && @$userdata1->country_code){
                    // dd("Getitng In");

                    /* $msg = "Dear " . (@$userdata1->nick_name ?? @$userdata1->name) .
                    ", you have a new session with " . (@$userdata->nick_name ?? @$userdata->name) .
                    ". Booking Token: " . (@$bookingdata->token_no); */

                    $msg = "Olá " . (@$userdata1->nick_name ?? @$userdata1->name) . ",
                    Obrigado por usar a Netbhe! você tem uma nova sessão com: " . (@$userdata->nick_name ?? @$userdata->name) .
                    ". Esperamos que aproveite sua sessão! 😉 Token de reserva: " . (@$bookingdata->token_no);

                    $mobile = "+".@$userdata1->country_code . @$userdata1->mobile;
                    // $mobile = "+554598290176";
                    // $mobile = "+918017096564";
                    // dd($msg);
                    $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                    // if($result['statusCode'] == 200){
                    //     dd("Success");
                    // } else {
                    //     dd($result);
                    // }
                }
                // $response['success']['message'] = 'Payment Successfully';
                // return response()->json($response);
                session()->flash('success', \Lang::get('site.Payment_successfull'));
                return Redirect::route('booking.sces', @$userBooking->token_no);
            }
            return Redirect::route('booking.sces', @$userBooking->token_no);
		    // return Redirect::route('paywithpaypal');
        }

        // Session::put('error','Payment failed !!');
        session()->flash('error', \Lang::get('site.Payment_failed'));
        $UpdateBooking = ProductOrder::where([
            'token_no'        =>  @$userBooking->token_no
        ])->update([
            'payment_status'  =>  "F"
        ]);
        return Redirect::route('booking.sces', @$userBooking->token_no);
		// return Redirect::route('paywithpaypal');
    }

    /**
     * Method: bookingCancle
     * Description:for cancel booking and refund payment
     * Author: Soumojit
     * date:01-OCT-2021
     */
    public function bookingCancel(Request $request,$token=null,$id=null){
        $bookingData= Booking::where('token_no', $token)->where('id', $id)->first();

        if($bookingData==null){
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        if($bookingData->order_status=='C'){
            session()->flash('error', \Lang::get('site.alrady_order_cancel'));
            return redirect()->back();
        }
        // $from_time = toUTCTime(date('Y-m-d H:i:s',strtotime($request->date.$request->time)));
        // $end_time = toUTCTime(date('Y-m-d H:i:s',strtotime('+'.$request->duration.'minutes',strtotime($request->date.$request->time))));
        // dd($from_time,date('Y-m-d H:i:s', strtotime("+60 minutes")));
        // return $bookingData;
        $startTime =date('Y-m-d H:i:s', strtotime($bookingData->date.$bookingData->start_time));
        // return $startTime;
        // return toUTCTime(date('Y-m-d H:i:s', strtotime("+24 hours")));
        if( toUTCTime(date('Y-m-d H:i:s', strtotime("+24 hours"))) > $startTime){
            session()->flash('error', \Lang::get('site.booking_cancellation_time_over'));
            return redirect()->back();
        }
        if($bookingData->payment_status=='P'){
            $payment =Payment::where('token_no',$bookingData->token_no)->first();
            $paymentDetails = PaymentDetails::where('payment_master_id',$payment->id)->first();
            if(@$payment->payment_type=='C' && $paymentDetails->is_refund=='N'){
                if (env('PAYMENT_ENVIORNMENT') == 'live') {
                    $url = Moip::ENDPOINT_PRODUCTION;
                } else if (env('PAYMENT_ENVIORNMENT') == 'sandbox') {
                    $url = Moip::ENDPOINT_SANDBOX;
                }
                $crl = curl_init();
                $header = array();
                // $header[] = 'Content-length: 0';
                $header[] = 'Content-type: application/json';
                $header[] = 'Authorization: Bearer ' . $this->adminAccessToken;
                curl_setopt($crl, CURLOPT_HTTPHEADER, $header);
                curl_setopt($crl, CURLOPT_URL, "{$url}/v2/payments/".$payment->pg_payment_id."/refunds");
                curl_setopt($crl, CURLOPT_POST, 1);
                curl_setopt($crl, CURLOPT_POST, true);
                // curl_setopt($crl, CURLOPT_POSTFIELDS, json_encode($data));
                curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
                $payment_output = curl_exec($crl);
                curl_close($crl);
                $payment_output = json_decode($payment_output);

                if (@$payment_output->id) {
                    if(@$payment_output->status=='COMPLETED'){
                        $upd=[];
                        $upd['is_refund']='Y';
                        $upd['pg_refund_id']=@$payment_output->id;
                        $upd['refund_amount']=$payment->total_amount-$bookingData->wallet;
                        $upd['refund_amount_wallet']=$bookingData->wallet;
                        $upd['pg_refund_response']=json_encode($payment_output);
                        PaymentDetails::where('payment_master_id',$payment->id)->update($upd);
                        // $bookingData= Booking::where('token_no', $token)->where('id', $id)->first();
                        $upd1=[];
                        if($bookingData->user_id==auth()->user()->id){
                            $upd1['order_cancelled_by']='U';
                        }
                        if($bookingData->professional_id==auth()->user()->id){
                            $upd1['order_cancelled_by']='P';
                        }
                        $upd1['order_status']='C';
                        $upd1['cancellation_reason']=@$request->reason;
                        Booking::where('token_no', $token)->where('id', $id)->update($upd1);
                        $userdata = User::where('id', $bookingData->user_id)->first();
                        $userdata1 = User::where('id',$bookingData->professional_id)->first();

                        $professionalmail = $userdata1->email;
                        $usermail = $userdata->email;
                        $bookingdata = $bookingData;
                        User::where('id',$bookingData->user_id)->update(['wallet_balance'=>($userdata->wallet_balance+$bookingData->wallet)]);

                        Mail::send(new UserBookingCancel($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));
                        Mail::send(new ProfessionalBookingCancel($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                        // whatsHere
                        // $upd1['order_cancelled_by']
                        // your session request has been cancellled by profession
                        // you have succesfully cancelled booking
                        if(@$upd1['order_cancelled_by'] == 'P'){
                            if(@$userdata->mobile && @$userdata->country_code){

                                /* $msg = "Dear " . (@$userdata->nick_name ?? @$userdata->name) .
                                ", your session with: " . (@$userdata1->nick_name ?? @$userdata1->name) .
                                " has been cancelled by the professional. Booking Token: " . (@$bookingData->token_no); */

                                $msg = "Olá ". (@$userdata->nick_name ?? @$userdata->name) . ",
                                vi aqui que sua sessão com: " . (@$userdata1->nick_name ?? @$userdata1->name) .
                                " foi cancelada pelo autor " . (@$userdata1->nick_name ?? @$userdata1->name) .
                                ". Mas não tem problema, você ainda pode aproveitar outras ofertas no site da Netbhe.
                                Fico esperando sua visita! Até a próxima! 😉 Token de reserva: " . (@$bookingData->token_no);

                                $mobile = "+".@$userdata->country_code . @$userdata->mobile;
                                // $mobile = "+554598290176";
                                // $mobile = "+918017096564";
                                // dd($msg);
                                $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                                // if($result['statusCode'] == 200){
                                //     dd("Success");
                                // } else {
                                //     dd($result);
                                // }
                            }
                        } else {
                            if(@$userdata1->mobile && @$userdata1->country_code){

                                /* $msg = "Dear " . (@$userdata1->nick_name ?? @$userdata1->name) .
                                ", your session with: " . (@$userdata->nick_name ?? @$userdata->name) .
                                " has been cancelled. Booking Token: " . (@$bookingdata->token_no); */

                                $msg = "Olá ". (@$userdata1->nick_name ?? @$userdata1->name) . ",
                                vi aqui que sua sessão com: " . (@$userdata->nick_name ?? @$userdata->name) .
                                " foi cancelada pelo usuário " . (@$userdata->nick_name ?? @$userdata->name) .
                                ". Mas não tem problema, você ainda pode aproveitar outras ofertas no site da Netbhe.
                                Fico esperando sua visita! Até a próxima! 😉 Token de reserva: " . (@$bookingData->token_no);

                                $mobile = "+".@$userdata1->country_code . @$userdata1->mobile;
                                // $mobile = "+554598290176";
                                // $mobile = "+918017096564";
                                // dd($msg);
                                $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                                // if($result['statusCode'] == 200){
                                //     dd("Success");
                                // } else {
                                //     dd($result);
                                // }
                            }
                        }
                        session()->flash('success', \Lang::get('client_site.booking_cancelled_successfully'));
                        return redirect()->back();
                    }
                    session()->flash('error', \Lang::get('client_site.unauthorize_access'));
                    return redirect()->back();
                }
                session()->flash('error', \Lang::get('client_site.unauthorize_access'));
                return redirect()->back();
            }
            if(@$payment->payment_type=='S' && $paymentDetails->is_refund=='N'){
                try{
                    Stripe::setApiKey(config('services.stripe.secret'));
                    $refund = Refund::create([
                        'payment_intent' => $payment->pg_payment_id,
                        'amount' => (int) bcmul( $payment->total_amount-$bookingData->wallet, 100),
                    ]);
                    // return $refund;
                    if ($refund->status == 'succeeded') {
                        $upd=[];
                        $upd['is_refund']='Y';
                        $upd['pg_refund_id']=@$refund->id;
                        $upd['refund_amount']=$payment->total_amount-$bookingData->wallet;
                        $upd['refund_amount_wallet']=$bookingData->wallet;
                        $upd['pg_refund_response']=json_encode($refund);
                        PaymentDetails::where('payment_master_id',$payment->id)->update($upd);
                        $upd1=[];
                        if($bookingData->user_id==auth()->user()->id){
                            $upd1['order_cancelled_by']='U';
                        }
                        if($bookingData->professional_id==auth()->user()->id){
                            $upd1['order_cancelled_by']='P';
                        }
                        $upd1['order_status']='C';
                        $upd1['cancellation_reason']=@$request->reason;
                        Booking::where('token_no', $token)->where('id', $id)->update($upd1);
                        $userdata = User::where('id', $bookingData->user_id)->first();
                        $userdata1 = User::where('id',$bookingData->professional_id)->first();
                        $professionalmail = $userdata1->email;
                        $usermail = $userdata->email;
                        $bookingdata = $bookingData;
                        User::where('id',$bookingData->user_id)->update(['wallet_balance'=>($userdata->wallet_balance+$bookingData->wallet)]);

                        Mail::send(new UserBookingCancel($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));
                        Mail::send(new ProfessionalBookingCancel($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                        // whatsHere
                        // $upd1['order_cancelled_by']
                        // your session request has been cancellled by profession
                        // you have succesfully cancelled booking
                        if(@$upd1['order_cancelled_by'] == 'P'){
                            if(@$userdata->mobile && @$userdata->country_code){

                                /* $msg = "Dear " . (@$userdata->nick_name ?? @$userdata->name) .
                                ", your session with: " . (@$userdata1->nick_name ?? @$userdata1->name) .
                                " has been cancelled by the professional. Booking Token: " . (@$bookingData->token_no); */

                                $msg = "Olá ". (@$userdata->nick_name ?? @$userdata->name) . ",
                                vi aqui que sua sessão com: " . (@$userdata1->nick_name ?? @$userdata1->name) .
                                " foi cancelada pelo autor " . (@$userdata1->nick_name ?? @$userdata1->name) .
                                ". Mas não tem problema, você ainda pode aproveitar outras ofertas no site da Netbhe.
                                Fico esperando sua visita! Até a próxima! 😉 Token de reserva: " . (@$bookingData->token_no);

                                $mobile = "+".@$userdata->country_code . @$userdata->mobile;
                                // $mobile = "+554598290176";
                                // $mobile = "+918017096564";
                                // dd($msg);
                                $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                                // if($result['statusCode'] == 200){
                                //     dd("Success");
                                // } else {
                                //     dd($result);
                                // }
                            }
                        } else {
                            if(@$userdata1->mobile && @$userdata1->country_code){

                                /* $msg = "Dear " . (@$userdata1->nick_name ?? @$userdata1->name) .
                                ", your session with: " . (@$userdata->nick_name ?? @$userdata->name) .
                                " has been cancelled. Booking Token: " . (@$bookingdata->token_no); */

                                $msg = "Olá ". (@$userdata1->nick_name ?? @$userdata1->name) . ",
                                vi aqui que sua sessão com: " . (@$userdata->nick_name ?? @$userdata->name) .
                                " foi cancelada pelo usuário " . (@$userdata->nick_name ?? @$userdata->name) .
                                ". Mas não tem problema, você ainda pode aproveitar outras ofertas no site da Netbhe.
                                Fico esperando sua visita! Até a próxima! 😉 Token de reserva: " . (@$bookingData->token_no);

                                $mobile = "+".@$userdata1->country_code . @$userdata1->mobile;
                                // $mobile = "+554598290176";
                                // $mobile = "+918017096564";
                                // dd($msg);
                                $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                                // if($result['statusCode'] == 200){
                                //     dd("Success");
                                // } else {
                                //     dd($result);
                                // }
                            }
                        }
                        session()->flash('success', \Lang::get('client_site.booking_cancelled_successfully'));
                        return redirect()->back();
                    }else{
                        session()->flash('error', \Lang::get('client_site.unauthorize_access'));
                        return redirect()->back();
                    }
                }
                 catch (\Stripe\Exception\RateLimitException $e) {
                    session()->flash('error', \Lang::get('client_site.unauthorize_access'));
                    return redirect()->back();
                } catch (\Stripe\Exception\InvalidRequestException $e) {
                    session()->flash('error', \Lang::get('client_site.unauthorize_access'));
                    return redirect()->back();
                } catch (\Stripe\Exception\AuthenticationException $e) {
                    session()->flash('error', \Lang::get('client_site.unauthorize_access'));
                    return redirect()->back();
                } catch (\Stripe\Exception\ApiConnectionException $e) {
                    session()->flash('error', \Lang::get('client_site.unauthorize_access'));
                    return redirect()->back();
                } catch (\Stripe\Exception\ApiErrorException $e) {
                    session()->flash('error', \Lang::get('client_site.unauthorize_access'));
                    return redirect()->back();
                } catch (Exception $e) {
                    session()->flash('error', \Lang::get('client_site.unauthorize_access'));
                    return redirect()->back();
                }
            }
            if(@$payment->payment_type=='P' && $paymentDetails->is_refund=='N'){
                $payments = PaymentPaypal::get($payment->pg_payment_id, $this->_api_context);
                $payments->getTransactions();
                $obj = $payments->toJSON();//I wanted to look into the object
                $paypal_obj = json_decode($obj);//I wanted to look into the object
                $transaction_id = $paypal_obj->transactions[0]->related_resources[0]->sale->id;
                $saleId=$transaction_id;
                $refundRequest = new RefundRequest();
                $sale = new Sale();
                $sale->setId($saleId);
                try {
                    $refundedSale = $sale->refundSale($refundRequest, $this->_api_context);

                } catch (Exception $ex) {
                    exit(1);
                }
                // return $refundedSale;
                if( $refundedSale!=null){
                    if ($refundedSale->getState() == 'completed') {
                        $res_arr = $refundedSale->toArray();
                        $res = json_decode($refundedSale);
                        $upd=[];
                        $upd['is_refund']='Y';
                        $upd['pg_refund_id']=$res_arr['id'];
                        $upd['refund_amount']=$payment->total_amount-$bookingData->wallet;
                        $upd['refund_amount_wallet']=$bookingData->wallet;
                        $upd['pg_refund_response']=json_encode($res);
                        PaymentDetails::where('payment_master_id',$payment->id)->update($upd);
                        $upd1=[];
                        if($bookingData->user_id==auth()->user()->id){
                            $upd1['order_cancelled_by']='U';
                        }
                        if($bookingData->professional_id==auth()->user()->id){
                            $upd1['order_cancelled_by']='P';
                        }
                        $upd1['order_status']='C';
                        $upd1['cancellation_reason']=@$request->reason;
                        Booking::where('token_no', $token)->where('id', $id)->update($upd1);
                        $userdata = User::where('id', $bookingData->user_id)->first();
                        $userdata1 = User::where('id',$bookingData->professional_id)->first();
                        $professionalmail = $userdata1->email;
                        $usermail = $userdata->email;
                        $bookingdata = $bookingData;
                        User::where('id',$bookingData->user_id)->update(['wallet_balance'=>($userdata->wallet_balance+$bookingData->wallet)]);
                        Mail::send(new UserBookingCancel($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));
                        Mail::send(new ProfessionalBookingCancel($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                        // whatsHere
                        // $upd1['order_cancelled_by']
                        // your session request has been cancellled by profession
                        // you have succesfully cancelled booking
                        if(@$upd1['order_cancelled_by'] == 'P'){
                            if(@$userdata->mobile && @$userdata->country_code){

                                /* $msg = "Dear " . (@$userdata->nick_name ?? @$userdata->name) .
                                ", your session with: " . (@$userdata1->nick_name ?? @$userdata1->name) .
                                " has been cancelled by the professional. Booking Token: " . (@$bookingData->token_no); */

                                $msg = "Olá ". (@$userdata->nick_name ?? @$userdata->name) . ",
                                vi aqui que sua sessão com: " . (@$userdata1->nick_name ?? @$userdata1->name) .
                                " foi cancelada pelo autor " . (@$userdata1->nick_name ?? @$userdata1->name) .
                                ". Mas não tem problema, você ainda pode aproveitar outras ofertas no site da Netbhe.
                                Fico esperando sua visita! Até a próxima! 😉 Token de reserva: " . (@$bookingData->token_no);

                                $mobile = "+".@$userdata->country_code . @$userdata->mobile;
                                // $mobile = "+554598290176";
                                // $mobile = "+918017096564";
                                // dd($msg);
                                $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                                // if($result['statusCode'] == 200){
                                //     dd("Success");
                                // } else {
                                //     dd($result);
                                // }
                            }
                        } else {
                            if(@$userdata1->mobile && @$userdata1->country_code){

                                /* $msg = "Dear " . (@$userdata1->nick_name ?? @$userdata1->name) .
                                ", your session with: " . (@$userdata->nick_name ?? @$userdata->name) .
                                " has been cancelled. Booking Token: " . (@$bookingdata->token_no); */

                                $msg = "Olá ". (@$userdata1->nick_name ?? @$userdata1->name) . ",
                                vi aqui que sua sessão com: " . (@$userdata->nick_name ?? @$userdata->name) .
                                " foi cancelada pelo usuário " . (@$userdata->nick_name ?? @$userdata->name) .
                                ". Mas não tem problema, você ainda pode aproveitar outras ofertas no site da Netbhe.
                                Fico esperando sua visita! Até a próxima! 😉 Token de reserva: " . (@$bookingData->token_no);

                                $mobile = "+".@$userdata1->country_code . @$userdata1->mobile;
                                // $mobile = "+554598290176";
                                // $mobile = "+918017096564";
                                // dd($msg);
                                $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                                // if($result['statusCode'] == 200){
                                //     dd("Success");
                                // } else {
                                //     dd($result);
                                // }
                            }
                        }
                        session()->flash('success', \Lang::get('client_site.booking_cancelled_successfully'));
                        return redirect()->back();
                    }
                }
                // $payments = PaymentPaypal::get($payment->pg_payment_id, $this->_api_context);
                // $payments->getTransactions();
                // $obj = $payments->toJSON();//I wanted to look into the object
                // $paypal_obj = json_decode($obj);//I wanted to look into the object
                // $transaction_id = $paypal_obj->transactions[0]->related_resources[0]->sale->id;
                // dd ($paypal_obj);
                // return $payment;
            }
            if(@$payment->payment_type=='BA' && $paymentDetails->is_refund=='N'){
                $upd=[];
                $upd['is_refund']='Y';
                $upd['refund_amount']=$payment->total_amount-$bookingData->wallet;
                $upd['refund_amount_wallet']=$bookingData->wallet;
                PaymentDetails::where('payment_master_id',$payment->id)->update($upd);
                $upd1=[];
                if($bookingData->user_id==auth()->user()->id){
                    $upd1['order_cancelled_by']='U';
                }
                if($bookingData->professional_id==auth()->user()->id){
                    $upd1['order_cancelled_by']='P';
                }
                $upd1['order_status']='C';
                $upd1['cancellation_reason']=@$request->reason;
                $userdata=User::where('id',$bookingData->user_id)->first();
                User::where('id',$bookingData->user_id)->update(['wallet_balance'=>($userdata->wallet_balance+$payment->total_amount+$bookingData->wallet)]);
                Booking::where('token_no', $token)->where('id', $id)->update($upd1);

                $userdata = User::where('id', $bookingData->user_id)->first();
                $userdata1 = User::where('id',$bookingData->professional_id)->first();

                $professionalmail = $userdata1->email;
                $usermail = $userdata->email;
                $bookingdata = $bookingData;

                Mail::send(new UserBookingCancel($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));
                Mail::send(new ProfessionalBookingCancel($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));


                    // whatsHere
                    // $upd1['order_cancelled_by']
                    // your session request has been cancellled by profession
                    // you have succesfully cancelled booking
                    if(@$upd1['order_cancelled_by'] == 'P'){
                        if(@$userdata->mobile && @$userdata->country_code){

                            /* $msg = "Dear " . (@$userdata->nick_name ?? @$userdata->name) .
                            ", your session with: " . (@$userdata1->nick_name ?? @$userdata1->name) .
                            " has been cancelled by the professional. Booking Token: " . (@$bookingData->token_no); */

                            $msg = "Olá ". (@$userdata->nick_name ?? @$userdata->name) . ",
                            vi aqui que sua sessão com: " . (@$userdata1->nick_name ?? @$userdata1->name) .
                            " foi cancelada pelo autor " . (@$userdata1->nick_name ?? @$userdata1->name) .
                            ". Mas não tem problema, você ainda pode aproveitar outras ofertas no site da Netbhe.
                            Fico esperando sua visita! Até a próxima! 😉 Token de reserva: " . (@$bookingData->token_no);

                            $mobile = "+".@$userdata->country_code . @$userdata->mobile;
                            // $mobile = "+554598290176";
                            // $mobile = "+918017096564";
                            // dd($msg);
                            $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                            // if($result['statusCode'] == 200){
                            //     dd("Success");
                            // } else {
                            //     dd($result);
                            // }
                        }
                    } else {
                        if(@$userdata1->mobile && @$userdata1->country_code){

                            /* $msg = "Dear " . (@$userdata1->nick_name ?? @$userdata1->name) .
                            ", your session with: " . (@$userdata->nick_name ?? @$userdata->name) .
                            " has been cancelled. Booking Token: " . (@$bookingdata->token_no); */

                            $msg = "Olá ". (@$userdata1->nick_name ?? @$userdata1->name) . ",
                            vi aqui que sua sessão com: " . (@$userdata->nick_name ?? @$userdata->name) .
                            " foi cancelada pelo usuário " . (@$userdata->nick_name ?? @$userdata->name) .
                            ". Mas não tem problema, você ainda pode aproveitar outras ofertas no site da Netbhe.
                            Fico esperando sua visita! Até a próxima! 😉 Token de reserva: " . (@$bookingData->token_no);

                            $mobile = "+".@$userdata1->country_code . @$userdata1->mobile;
                            // $mobile = "+554598290176";
                            // $mobile = "+918017096564";
                            // dd($msg);
                            $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                            // if($result['statusCode'] == 200){
                            //     dd("Success");
                            // } else {
                            //     dd($result);
                            // }
                        }
                    }

                session()->flash('success', \Lang::get('client_site.booking_cancelled_successfully'));
                return redirect()->back();
            }
            if($bookingData->wallet == $bookingData->sub_total){
                $upd=[];
                $upd['is_refund']='Y';
                $upd['refund_amount']=0;
                $upd['refund_amount_wallet']=$bookingData->wallet;
                PaymentDetails::where('payment_master_id',$payment->id)->update($upd);
                $upd1=[];
                if($bookingData->user_id==auth()->user()->id){
                    $upd1['order_cancelled_by']='U';
                }
                if($bookingData->professional_id==auth()->user()->id){
                    $upd1['order_cancelled_by']='P';
                }
                $upd1['order_status']='C';
                $upd1['cancellation_reason']=@$request->reason;
                $userdata=User::where('id',$bookingData->user_id)->first();
                User::where('id',$bookingData->user_id)->update(['wallet_balance'=>($userdata->wallet_balance+$bookingData->wallet)]);
                Booking::where('token_no', $token)->where('id', $id)->update($upd1);

                $userdata = User::where('id', $bookingData->user_id)->first();
                $userdata1 = User::where('id',$bookingData->professional_id)->first();

                $professionalmail = $userdata1->email;
                $usermail = $userdata->email;
                $bookingdata = $bookingData;

                Mail::send(new UserBookingCancel($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));
                Mail::send(new ProfessionalBookingCancel($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                // whatsHere
                // $upd1['order_cancelled_by']
                // your session request has been cancellled by profession
                // you have succesfully cancelled booking
                if(@$upd1['order_cancelled_by'] == 'P'){
                    if(@$userdata->mobile && @$userdata->country_code){

                        /*$msg = "Dear " . (@$userdata->nick_name ?? @$userdata->name) .
                        ", your session with: " . (@$userdata1->nick_name ?? @$userdata1->name) .
                        " has been cancelled by the professional. Booking Token: " . (@$bookingData->token_no); */

                        $msg = "Olá ". (@$userdata->nick_name ?? @$userdata->name) . ",
                        vi aqui que sua sessão com: " . (@$userdata1->nick_name ?? @$userdata1->name) .
                        " foi cancelada pelo autor " . (@$userdata1->nick_name ?? @$userdata1->name) .
                        ". Mas não tem problema, você ainda pode aproveitar outras ofertas no site da Netbhe.
                        Fico esperando sua visita! Até a próxima! 😉 Token de reserva: " . (@$bookingData->token_no);

                        $mobile = "+".@$userdata->country_code . @$userdata->mobile;
                        // $mobile = "+554598290176";
                        // $mobile = "+918017096564";
                        // dd($msg);
                        $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                        // if($result['statusCode'] == 200){
                        //     dd("Success");
                        // } else {
                        //     dd($result);
                        // }
                    }
                } else {
                    if(@$userdata1->mobile && @$userdata1->country_code){

                        /* $msg = "Dear " . (@$userdata1->nick_name ?? @$userdata1->name) .
                        ", your session with: " . (@$userdata->nick_name ?? @$userdata->name) .
                        " has been cancelled. Booking Token: " . (@$bookingdata->token_no); */

                        $msg = "Olá ". (@$userdata1->nick_name ?? @$userdata1->name) . ",
                        vi aqui que sua sessão com: " . (@$userdata->nick_name ?? @$userdata->name) .
                        " foi cancelada pelo usuário " . (@$userdata->nick_name ?? @$userdata->name) .
                        ". Mas não tem problema, você ainda pode aproveitar outras ofertas no site da Netbhe.
                        Fico esperando sua visita! Até a próxima! 😉 Token de reserva: " . (@$bookingData->token_no);

                        $mobile = "+".@$userdata1->country_code . @$userdata1->mobile;
                        // $mobile = "+554598290176";
                        // $mobile = "+918017096564";
                        // dd($msg);
                        $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                        // if($result['statusCode'] == 200){
                        //     dd("Success");
                        // } else {
                        //     dd($result);
                        // }
                    }
                }
                session()->flash('success', \Lang::get('client_site.booking_cancelled_successfully'));
                return redirect()->back();
            }
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        session()->flash('error', \Lang::get('client_site.unauthorize_access'));
        return redirect()->back();

    }

     /**
     * For Free Session Payment Create
     * @method: freeSessionPayment
     */
    private function fullPaymentThroughWallet($token)
    {
        $userBooking = Booking::where('token_no', $token)->first();
        $referDiscount = ReferralDiscount::first();
        $checkTeacher = User::where('id', $userBooking->professional_id)->first();
        $professionalCommissionList = Commission::first();
        $professionalCommission = 50;
        if ($userBooking->booking_type == 'C') {
            if(@$checkTeacher->chat_commission>0 && $checkTeacher->chat_commission!==null){
                $commision=$checkTeacher->chat_commission;
            }else{
                $commision=$professionalCommissionList->chat_commission;
            }
            $professionalCommission = $commision;
        }else {
            if(@$checkTeacher->video_commission>0 && $checkTeacher->video_commission!==null){
                $commision=$checkTeacher->video_commission;
            }else{
                $commision=$professionalCommissionList->commission;
            }
            $professionalCommission = $commision;
        }
        if (@$checkTeacher->is_paid_activity == 'N') {
            $discount = $professionalCommission + $referDiscount->referrer_teacher_discount;
            $amount = (@$userBooking->amount * @$discount) / 100;
            $professional_amount   =  @$amount;
            $admin_amount         =   @$userBooking->sub_total - @$amount;
            User::where('id', @$checkTeacher->id)->update(['is_paid_activity' => 'C']);
            User::where('id', @$checkTeacher->referrer_id)->increment("benefit_count", 1);
        } elseif (@$checkTeacher->benefit_count > 0) {
            $discount = $professionalCommission + $referDiscount->referred_teacher_discount;
            $amount = (@$userBooking->amount * @$discount) / 100;
            $professional_amount   =  @$amount;
            $admin_amount         =   @$userBooking->sub_total - @$amount;
            User::where('id', @$checkTeacher->id)->decrement("benefit_count", 1);
        } else {
            $discount = $professionalCommission;
            $amount = (@$userBooking->amount * @$discount) / 100;
            $professional_amount   =  @$amount;
            $admin_amount         =   @$userBooking->sub_total - @$amount;
        }
        $total_amount         =  @$userBooking->sub_total;
        $payment = Payment::create([
            'token_no'              =>  @$userBooking->token_no,
            'pg_order_id'           =>  'ORD-' . @$userBooking->token_no,
            'user_id'               =>  @$userBooking->user_id,
            'total_amount'          =>  @$total_amount,
            'payment_status'        =>  'P',
            // 'payment_type'          =>  'S',
            'order_type'            =>  'B',
            'wallet'                =>  @$userBooking->wallet,
            'is_wallet_use'         =>  @$userBooking->is_wallet_use,
        ]);
        if (@$payment) {
            $add = [];
            $add['payment_master_id'] = $payment->id;
            $add['product_order_details_id'] = 0;
            $add['professional_amount'] = @$professional_amount;
            $add['admin_commission'] = @$admin_amount;
            $add['affiliate_commission'] = 0;
            $add['affiliate_id'] = 0;
            $add['professional_id'] = @$userBooking->professional_id;
            $add['type'] = 'P';
            $add['withdraw_by_wirecard'] = 'N';
            $add['balance_status'] = 'R';
            PaymentDetails::create($add);
            $UpdateBooking = Booking::where([
                'token_no'        =>  @$userBooking->token_no
            ])->update([
                'payment_status'  =>  "P",
                'order_status'  =>  "A"
            ]);
            $referralStatus = User::where('id', @$userBooking->user_id)->first();
            if (@$referralStatus->is_paid_activity == 'N') {
                User::where('id', @$userBooking->user_id)->update(['is_paid_activity' => 'C']);
                User::where('id', @$referralStatus->referrer_id)->increment("benefit_count", 1);
            }
            $userdata = User::where('id', Auth::id())->first();
            $userdata1 = User::where('id', @$userBooking->professional_id)->first();
            $professionalmail = $userdata1->email;
            $usermail = $userdata->email;
            $bookingdata = $userBooking;
            User::where('id', $userdata->id)->update(['wallet_balance'=>($userdata->wallet_balance - @$userBooking->wallet)]);
            // // for send mail to user
            Mail::send(new UserBooking($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));
            // // for send mail to professional
            Mail::send(new ProfessionalBooking($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
            // whatsHere
            // session booked fully by wallet
            // to user
            if(@$userdata1->mobile && @$userdata1->country_code){
                // dd("Getitng In");

                /* $msg = "Dear " . (@$userdata1->nick_name ?? @$userdata1->name) .
                ", you have a new session with " . (@$userdata->nick_name ?? @$userdata->name) .
                ". Booking Token: " . (@$bookingdata->token_no); */

                $msg = "Olá " . (@$userdata1->nick_name ?? @$userdata1->name) . ",
                Obrigado por usar a Netbhe! você tem uma nova sessão com: " . (@$userdata->nick_name ?? @$userdata->name) .
                ". Esperamos que aproveite sua sessão! 😉 Token de reserva: " . (@$bookingdata->token_no);

                $mobile = "+".@$userdata1->country_code . @$userdata1->mobile;
                // $mobile = "+554598290176";
                // $mobile = "+918017096564";
                // dd($msg);
                $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                // if($result['statusCode'] == 200){
                //     dd("Success");
                // } else {
                //     dd($result);
                // }
            }
            return true;
        } else {
            $UpdateBooking = Booking::where([
                'token_no'        =>  @$userBooking->token_no
            ])->update([
                'payment_status'  =>  "F"
            ]);
            return false;
        }
    }


}
