<?php

namespace App\Http\Controllers\Modules\UserSmartGoal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tool360Master;
use App\Models\Tool360Details;
use App\Models\ToolsSmartGoalMaster;
use App\Models\ToolsSmartGoalDetails;
use App\Models\Booking;
use App\Models\UserToTools;
use validate;
use App\User;
use Auth;
use Mail;
use App\Mail\FormAnswered;


class UserSmartGoalController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }




    public function userSmartGoalView($id)
    {


        $data = array();
        $data['title'] = 'Manage Smart goal';

        $data['details'] = UserToTools::with(['getUserData', 'getSmartGoalsData'])->where('id', $id)->orderBy('id', 'DESC')->first();
        // dd($data['details']);

        $data['all_smart_goal_details'] = ToolsSmartGoalDetails::where(['smart_goal_master_id' => $data['details']->tool_id, 'user_id' => Auth::id()])->get();

        return view('modules.user_smart_goal.goal_post')->with($data);
    }

    //insert  ToolsSmartGoalDetails goal point

    public function userSmartGoalPost(Request $request, $user_tool_id)
    {


        $data = UserToTools::with(['getUserData', 'getSmartGoalsData'])->where('id', $user_tool_id)->orderBy('id', 'DESC')->first();
        //pr1($data->toArray());
        //die();
        $request->validate([
            'goal_point'            => 'required|numeric'
        ]);

        $insert['smart_goal_master_id']   = $data->tool_id;
        $insert['user_id']                = $data->user_id;
        $insert['goal_point']             = $request->goal_point;

        ToolsSmartGoalDetails::create($insert);
        session()->flash('success', \Lang::get('client_site.goal_point_added'));

        //return redirect()->back();
        return redirect(route('user.view.form'));
    }
}