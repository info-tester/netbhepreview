<?php

namespace App\Http\Controllers\Modules\ProfContractTemp;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserToTools;
use App\Models\UserFormAnswered;
use App\Models\FormMaster;
use App\Models\FormCategory;
use App\Models\Booking;
use App\Models\FormMasterDetails;
use App\Models\ImportedTool;
use App\Models\ContractTemplatMaster;
use validate;
use App\User;
use Auth;
use Mail;
use App\Mail\FormAnswered;


class ProfContractTempController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }


   

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request) { 

        $nowtime = date('Y-m-d H:i:s');
        $tendaybef =date('Y-m-d H:i:s', strtotime("-3 days"));

        // $data['all_paid_users'] = Booking::select('user_id')->distinct()->with('userDetails')->where('professional_id',Auth::id())->where('payment_status','P')->get();

        $data['all_paid_users'] = Booking::select('user_id')->distinct()->with('userDetails')->where('professional_id',Auth::id())->where('payment_status','P')->whereBetween('created_at', [$tendaybef ,$nowtime])->get();

        //$data['contract_templats'] = ContractTemplatMaster::where('status', '!=', 'DELETED')->orderBy('id','DESC')->with(['user'])->get();
        
        $user_id = Auth::id();
        $admin_id = '1';

        $data['contract_templats'] = ContractTemplatMaster::where('shown_in_proff', 'Y')->where(function($q) use ($user_id, $admin_id) {
            $q = $q->where('status', '!=', 'DELETED')
                    ->where(['added_by' => 'P','added_by_id' => $user_id ])
                    ->orWhere(function($qq) use ($user_id, $admin_id) {
                        $qq = $qq->where(['added_by' => 'A','added_by_id' => $admin_id ])
                                ->whereHas('specialities', function( $query ) use ( $user_id ){
                                    $query = $query->where('professional_specialty_id', Auth::user()->professional_specialty_id);
                                });
                    });
        })
        ->orderBy('id','DESC')
        ->with(['user'])
        ->get();

        //pr1($data['contract_templats']);
        //die();
        
        return view('modules.prof_contract_temp.index')->with($data);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create(Request $request) {
       
        $data = array();               
        return view('modules.prof_contract_temp.create');
        //return view('admin.modules.product.create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request) {        
       
        $request->validate([
            'title'            => 'required',            
            'contract_desc'    => 'required'
        ]);
        $insert['title']                = $request->title;        
        $insert['contract_desc']        = $request->contract_desc;
        //$insert['status']                    = 'Form';
        $insert['status']               = 'ACTIVE';
        $insert['added_by']             = 'P';
        $insert['added_by_id']          = Auth::id();
        ContractTemplatMaster::create($insert);
        session()->flash('success', \Lang::get('client_site.contract_temp_saved'));
        return redirect()->back();

    }


    //product Details Show

    public function show(Request $request,$id) {

        $usertoolsid = $request->usertoolsid;

        $usertools_data = UserToTools::where('id',$usertoolsid)->first();

        //$usertools_data->contract_end_date;

        $month = date('m (M)',strtotime(@$usertools_data->contract_end_date));
        $day = date('d (D)',strtotime(@$usertools_data->contract_end_date));
        $year = date('y',strtotime(@$usertools_data->contract_end_date));

        

        $details = ContractTemplatMaster::with(['user','getUsertoolsData'])->find($id);
        $userdetails = User::where('id',@$usertools_data->user_id)->first();

        
        
        $profdetails = User::where('id',Auth::id())->first();


        $profdata = [];
        $profdata['NAME']               = $userdetails['name'];

        $profdata['CPF']                = @$userdetails['cpf_no'];
        $profdata['PUBLIC PLACE']       = @$userdetails['street_name'];
        $profdata['NUMERO']             = @$userdetails['street_number'];
        $profdata['CITY']               = @$userdetails['city'];
        $profdata['CEP']                = @$userdetails['crp_no'];
        $profdata['NAME_COACH']         = @$profdetails['name'];        
        $profdata['CITY_COACH']         = @$profdetails['city'];        
        $profdata['PRICE']              = @$profdetails['rate_price'];

        $profdata['MONTH']              = @$month;
        $profdata['DAY']                = @$day;
        $profdata['YEAR']               = @$year;
        
        //pr1($details->toArray());
        //die();
        return view('modules.prof_contract_temp.show', [
            'details'       => $details,
            'profdata'       => $profdata,
        ]);

    }



    /**
    * Show the form for editing a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function edit($id) {

        //pr1($id);
        //die();
        $data = array();
       
        
        $data['details'] = ContractTemplatMaster::where('id', $id)->first();
        
        return view('modules.prof_contract_temp.edit')->with($data);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id) {
        
        //pr($id);
        //die();
        $request->validate([
            'title'            => 'required',           
            'contract_desc'             => 'required'
        ]);
        $insert['title']                = $request->title;        
        $insert['contract_desc']          = $request->contract_desc;
        ContractTemplatMaster::where('id',$id)->update($insert);
        session()->flash('success', \Lang::get('client_site.contract_temp_saved'));
        return redirect()->back();

    }

    /**
    * Remove the specified resource from storage.
    *
    *  @param  int  $id
    *  @return \Illuminate\Http\Response
    */
    public function destroy($id) {

        $data = UserToTools::where('tool_id', $id)->first();
        if(@$data){
            //echo "not delete";
            session()->flash('error', \Lang::get('client_site.assigned_contract_not_del'));
            return redirect()->back();
        } else {
            //echo "delete";
            ContractTemplatMaster::where('id', $id)->delete();
            session()->flash('success', \Lang::get('client_site.contract_temp_deleted'));
            return redirect()->back();
        }

        

       
     
    }


    // for contract temp assign in user
    public function profContractTempUserAssigne(Request $request){

        $data = $request->all();
        
        $nowtime = date('Y-m-d H:i:s');

        if($data){
            if($request->user_id){
                foreach ($request->user_id as $val) {
                    
                    $insert = array();
                    $insert['user_id']                  = $val;
                    $insert['professional_id']          = Auth::id();
                    $insert['tool_type']                = "Contract Template";
                    $insert['tool_id']                  = $request->contract_template_id;
                    $insert['contract_start_date']      = date('Y-m-d H:i:s',strtotime($request->contract_start_date));
                    $insert['contract_end_date']        = date('Y-m-d H:i:s',strtotime($request->contract_end_date));
                    $insert['assign_date']              = $nowtime;
                    $insert['status']                   = "INVITED";
                    $insert['contact_temp_marital_status']    = $request->contact_temp_marital_status;
                    $insert['contact_temp_neighborhood']      = $request->contact_temp_neighborhood;
                    $insert['contact_temp_installment']       = $request->contact_temp_installment;
                    
                    UserToTools::create($insert);
                }

                session()->flash('success', \Lang::get('client_site.user_assigned_successfully'));
                return redirect()->back();    
               
                // send mail to profession start
                    //Mail::send(new FormAnswered($usermail,$professionalmail));
                // send mail to profession end
            } else {   
                
                session()->flash('error', \Lang::get('site.Select_User'));
                return redirect()->back();
                //return redirect()->route('user.view.form');
            }      
        }
    }



//for professional contract 14.3.20 start

   public function profContractTempUserAssigneList($id){


        $data['all_user_data'] = UserToTools::with(['getprofessionalUserData','getContractTemplatesdata'])->where(['professional_id' => Auth::id(),'tool_id' => $id,'tool_type' => 'Contract Template'])->get();
        
        //pr1($data['all_user_data']->toArray());
        //die();
       
        return view('modules.prof_contract_temp.professional_form_view')->with($data);

   }



//for professional contract 14.3.20 end


    /*for professional ImportingToolsView section start*/

    // public function professionalImportingToolsView(Request $request){
    //     //pr1($request->form_id);
    //     //die();

    //     $formdata = UserToTools::with('getprofessionalUserData')->where(['professional_id' => Auth::id(),'tool_id' => $request->import_tools_id,'tool_type' => 'Imported Tools'])->get();
    //     //pr1($formdata->toArray());       
    //     //die();

    //     return view('modules.importing_tools.professional_form_view')->with([
    //         'formdata'   => @$formdata
    //     ]);
    // }

    /*for professional ImportingToolsView section end*/

    public function updateStatus($id){

        $nowtime = date('Y-m-d H:i:s');

        UserToTools::where('id', $id)->update(['status' => 'COMPLETED','completion_date' => $nowtime]);
                
        session()->flash('success', \Lang::get('client_site.status_updated_successfully'));
        return redirect()->route('user.view.form'); 

    }


// for user contract start 14.3.20

    public function userContractTempView($id){
        

        //$data['details'] = UserToTools::with(['getUserData','getContractTemplatesdata','getImportingToolsdata.category'])->find($id);
        $details = UserToTools::with(['getUserData','getContractTemplatesdata','getImportingToolsdata.category'])->find($id);
        

        $profdetails =User::where('id',$details->professional_id)->first();
        $userdetails = User::where('id',@$details->user_id)->first();

        $month = date('m (M)',strtotime($details->contract_end_date));
        $day = date('d (D)',strtotime($details->contract_end_date));
        $year = date('y',strtotime($details->contract_end_date));


        $profdata = [];
        $profdata['NAME']               = $userdetails['name'];

        $profdata['CPF']                = @$userdetails['cpf_no'];
        $profdata['PUBLIC PLACE']       = @$userdetails['street_name'];
        $profdata['NUMERO']             = @$userdetails['street_number'];
        $profdata['CITY']               = @$userdetails['city'];
        $profdata['CEP']                = @$userdetails['crp_no'];
        $profdata['NAME_COACH']         = @$profdetails['name'];        
        $profdata['CITY_COACH']         = @$profdetails['city'];        
        $profdata['PRICE']              = @$profdetails['rate_price'];

        $profdata['MONTH']              = @$month;
        $profdata['DAY']                = @$day;
        $profdata['YEAR']               = @$year;

        $profdata['INSTALLMENT']        = @$details['contact_temp_installment'];
        $profdata['NEIGHBORHOOD']       = @$details['contact_temp_neighborhood'];
        $profdata['ESTADO_CIVIL']       = @$details['contact_temp_marital_status'];
        
        //pr1($profdata);
        //die();

        //return view('modules.prof_contract_temp.user_contract_show')->with($data);

         return view('modules.prof_contract_temp.user_contract_show', [
            'details'       => $details,
            'profdata'       => $profdata,
        ]);

    }


    public function userContractTempAcceptStatus($id){
        $nowtime = date('Y-m-d H:i:s');

        UserToTools::where('id', $id)->update(['status' => 'COMPLETED','completion_date' => $nowtime]);
                
        session()->flash('success', \Lang::get('client_site.accept_contract_succesfully'));
        return redirect()->route('user.view.form'); 
    }

// for user contract end 14.3.20


    public function contractStatusUpdate($id){        

        $data = ContractTemplatMaster::find($id);        
      
        if(@$data->status == 'ACTIVE') {
            ContractTemplatMaster::where('id', $data->id)->update(['status' => 'INACTIVE']);
            session()->flash('success', \Lang::get('client_site.contract_temp_inactivated'));
            return redirect()->back();
        }

        if(@$data->status == 'INACTIVE') {
            ContractTemplatMaster::where('id', $data->id)->update(['status' => 'ACTIVE']);
            session()->flash('success', \Lang::get('client_site.contract_temp_activated'));
            return redirect()->back();
        }

    }


}
