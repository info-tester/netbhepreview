<?php

namespace App\Http\Controllers\Modules\Search;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\User;
use Auth;

class SearchController extends Controller
{

    /**
     *method: index
     *created By: Abhisek
     *description: For Search
     */

    public function index($slug = null, Request $request)
    {
        $user = User::select(
            'id','name','slug','status','is_professional','is_approved','profile_active','professional_specialty_id',
            'crp_no','rate_price','rate_price','mobile','avg_review','total_review','rate','profile_pic','nick_name','upcoming_availed_date'
        )
        ->with('userDetails', 'userExperience', 'userLang.languageName', 'specializationName')->where([
            'status'                =>  'A',
            'is_professional'       =>  'Y',
            'is_approved'           =>  'Y',
            'profile_active'        =>  'Y',
        ]);
        $allPro=$user->with('userToAvailability')->get();
        foreach($allPro as $pro){
            if(@$pro->userToAvailability->slot_start){
                User::where('id',$pro->id)->update(['upcoming_availed_date'=>@$pro->userToAvailability->slot_start]);
            }else{
                User::where('id',$pro->id)->update(['upcoming_availed_date'=>date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s').'+ 365 days'))]);
            }
        }
        if (Auth::id()) {
            $user = $user->where('id', '!=', Auth::id());
        }
        // dd($request->all());
        $maxprice = User::select('rate_price')->where('is_professional', 'Y')->orderBy('rate_price', 'desc')->first();

        $minprice = User::select('rate_price')->where('is_professional', 'Y')->orderBy('rate_price')->first();
        $key = [];
        if ($slug != null) {
            $cat_id = Category::where('slug', $slug)->first();
            // dd($cat_id);
            $user = $user->whereHas('userDetails', function ($query) use ($cat_id) {
                $query->where('category_id', $cat_id->id);
            });
            $key['category'] = $cat_id->id;
            if ($cat_id->parent_id != 0) {
                $key['category'] = $cat_id->parent_id;
                $key['subcategory'] = $cat_id->id;
            }
        }
        if ($request->all() && $slug == null) {

            if ($request->category) {
                $user = $user->whereHas('userDetails', function ($query) use ($request) {
                    $query->where('category_id', $request->category);
                });
            }

            if ($request->subcategory) {
                $user = $user->whereHas('userDetails', function ($query) use ($request) {
                    $query->where('category_id', $request->subcategory);
                });
            }

            if ($request->keyword) {
                $user = $user->where(function ($where) use ($request) {
                    $where->where('name', 'like', '%' . $request->keyword . '%')
                        ->orWhere('email', $request->keyword)
                        ->orWhere('mobile', $request->keyword);
                });
            }

            if ($request->experience) {
                if ($request->experience == 1) {

                    $user = $user->whereHas('userExperience', function ($q) {
                        $q->whereBetween('total_experience', [0, 365]);
                    });
                }
                if ($request->experience == 3) {

                    $user = $user->whereHas('userExperience', function ($q) {
                        $q->whereBetween('total_experience', [365, 1095]);
                    });
                }
                if ($request->experience == 5) {

                    $user = $user->whereHas('userExperience', function ($q) {
                        $q->whereBetween('total_experience', [1095, 1825]);
                    });
                }
            }

            if ($request->max_price || $request->min_price) {
                $user = $user->whereBetween('rate_price', [$request->min_price, $request->max_price]);
            }

            if ($request->sort_by) {
                // dd($request->sort_by);
                $user = $user->orderBy('id', $request->sort_by == "A" ? 'asc' : 'desc');
            } else {
                // $user = $user->inRandomOrder();
            }

            $key = $request->all();
        }
        // return $user->with('userToAvailability')->orderBy('upcoming_availed_date','asc')->get();
        $user->with('userToAvailability')->orderBy('upcoming_availed_date','asc');
        $user = $user->paginate(12);
        $user = $user->appends($request->all());
        // $experience = User::where('status', 'A')->pluck('experience');
        // dd($experience);
        $category = Category::select('id','status','name','slug')
        ->where('status', 'A')->orderBy('name')->get();
        // dd($user);
        return view('modules.search.search')->with([
            'user'            =>    @$user,
            'category'        =>    @$category,
            // 'experience'    =>    @$experience,
            'key'            =>    @$key,
            'minprice'      =>  @$minprice,
            'maxprice'      =>  @$maxprice
        ]);
    }
    public function consultantCategories(){
        $data['categories'] = Category::select('id','name','image','slug','status','parent_id','shown_in_cat_search')
        ->where('status', 'A')->where('shown_in_cat_search', 'Y')->get();
        return view('modules.search.categories')->with($data);
    }
    /**
     * For fetching sub-category.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function getSubcat(Request $request)
    {
        $response = [
            'jsonrpc'   =>  '2.0'
        ];
        if ($request->params['cat']) {
            $cat = Category::where('parent_id', $request->params['cat'])->where('status', 'A')->get();
            if ($cat != null) {
                $response['status'] = 1;
                $response['result'] = $cat;
            } else {
                $response['status'] = 0;
            }
        }
        return response()->json($response);
    }

    public function browseCat()
    {
        $brwsCat = Category::select('id','name','slug','status','parent_id')
        ->with(['childCat'=>function($query){
            $query->select('id','name','parent_id','slug','status');
        }])->where([
            'status'        => 'A',
            'parent_id'     =>  0
        ])->get();
        // dd($brwsCat);
        return view('modules.search.browse_search')->with('cat', @$brwsCat);
    }
}
