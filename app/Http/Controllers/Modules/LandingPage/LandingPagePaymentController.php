<?php

namespace App\Http\Controllers\Modules\LandingPage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\User;
use App\Models\Booking;
use App\Models\UserAvailability;
use App\Models\Timezone;
use App\Models\UserToCard;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use Moip\Moip;
use Moip\Auth\BasicAuth;
use Moip\Auth\OAuth;
use Moip\Resource\Customer;

use App\Mail\UserBooking;
use App\Mail\ProfessionalBooking;
use App\Models\Content;
use App\Models\Payment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Models\Coupon;
use App\Models\ReferralDiscount;
use Illuminate\Support\Facades\Storage;
use App\Models\AdminBankAccount;
use App\Mail\BankAccountPayment;
use App\Models\Commission;
use App\Mail\UserBankAccountBooking;
use App\Models\LandingPageMaster;
use App\Models\PaymentDetails;
use Stripe\PaymentIntent;
use Stripe\Stripe;
use Stripe\Refund;

use Validator;
use URL;
use Session;
use Redirect;
use Input;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment as PaymentPaypal;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use App\Models\LandingPagePricing;
use App\Models\LandingPagePayment;

class LandingPagePaymentController extends Controller
{
    protected $CPF_CNPJ, $numeral, $tipo;
    protected $adminAccessToken;
    private $_api_context;
    private $_ordID;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->adminAccessToken = getAdminToken();
        $paypal_configuration = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_configuration['client_id'], $paypal_configuration['secret']));
        $this->_api_context->setConfig($paypal_configuration['settings']);
    }
    /**
     * method: landingPaymentCreate
     *created By: Soumojit
     *Description: Landing page payment create
     *Date: 15-SEP-2021
     */

    public function landingPaymentCreate(Request $request)
    {
        $price= LandingPagePricing::first();
        // return $request;
        $check =LandingPagePayment::where('user_id',auth()->id())->where('payment_for','!=','B')->where('payment_status','P')->where('is_use','N')->first();
        if(@$check && @$request->template_id==null){
            return redirect()->route('create.landing.page.template');
        }
        if((@$request->banding =='Y'&& @$request->template=='Y') ||(@$request->banding=='Y' && @$request->template_id) ||(@$request->template=='Y' && @$request->banding==null)){
            $ins=[];
            $ins['token_no']=$token = strtoupper(str_random(12));
            $ins['user_id']=auth()->id();
            if(@$request->banding =='Y' && @$request->template=='Y'){
                $ins['total_amount']=$price->price_per_page+$price->price_branding_free;
                $ins['payment_for']='BT';
                $ins['branding_fees']=$price->price_branding_free;
                $ins['page_fees']=$price->price_per_page;
            }
            if(@$request->banding=='Y' && @$request->template_id){
                $ins['total_amount']=$price->price_branding_free;
                $ins['payment_for']='B';
                $ins['template_id']=@$request->template_id;
                $ins['branding_fees']=$price->price_branding_free;
            }
            if(@$request->template=='Y' && @$request->banding==null){
                $ins['total_amount']=$price->price_per_page;
                $ins['payment_for']='T';
                $ins['page_fees']=$price->price_per_page;
            }
            $ins['payment_status']='I';
            $ins['is_use']='N';
            LandingPagePayment::create($ins);
            return redirect()->route('landing.page.payment.store.view', ['token' => $token]);
        }
        session()->flash('error', \Lang::get('client_site.unauthorize_access'));
        return redirect()->route('list.landing.page.templates');

    }
    /**
     * method:  storeSuccess
     *created By: Soumojit
     *Description: After create page show payment page
     *Date: 15-SEP-2021
     */

    public function storeSuccess($slug)
    {
        $authUser = Auth::guard('web')->user();
        $booking = LandingPagePayment::where('token_no', $slug)->where('user_id', @Auth::id())->first();
        $profile = 0;
        $required = [
            0   => 'country_id',
            1   => 'state_id',
            2   => 'city',
            3   => 'dob',
            4   => 'street_number',
            5   => 'street_name',
            6   => 'complement',
            7   => 'district',
            8   => 'zipcode',
            9  => 'area_code'
        ];

        for ($i = 0; $i < sizeof($required); $i++) {
            $str = $required[@$i];
            if ($authUser->$str == null) {

                $profile++;
                break;
            } elseif ($authUser->$str <= 0 && !is_string($authUser->$str)) {
                $profile++;
                break;
            }
        }
        if ($booking == null) {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        } else {
            if ($authUser->is_credit_card_added == 'Y' && $authUser->customer_id != '' && $booking->pg_order_id == '' && $booking->total_amount != 0 && $booking->payment_type == 'C') {
                $this->chargeUser($slug);
            }
            // if ($booking->amount == 0 && $booking->payment_status == 'I') {
            //     $this->freeSessionPayment($slug);
            // }
            // if ($booking->payment_type == 'BA' && $booking->payment_status == 'I') {
            //     $this->bankAccountPayment($slug);
            // }
            if ($booking->payment_type == 'P' && $booking->payment_status == 'I') {
                if($booking->payment_for=='B'){
                    $title = "Banding Purchase";
                }
                if($booking->payment_for=='BT'){
                    $title = "Template & Banding Purchase";
                }
                if($booking->payment_for=='T'){
                    $title = "Template Purchase";
                }
                $paydata = new Request([
                    'title'     => $title,
                    'amount'    => $booking->total_amount,
                    'ordID'     => $booking->id,
                ]);
                $this->postPaymentWithpaypal($paydata);
            }
            $booking = LandingPagePayment::where('token_no', $slug)->where('user_id', @Auth::id())->first();

            // $booking = $booking;
            $time = Timezone::get();
            $country = Country::orderBy('name')->get();
            $state = State::orderBy('name')->get();
            $accountInfo = AdminBankAccount::first();
            if (@$booking->payment_status == 'F') {
                session()->flash('error', \Lang::get('site.Payment_failed'));
                return redirect()->route('list.landing.page.templates');
            }
            if(@$booking->payment_status=='P'){
                if($booking->payment_for=='B'){
                    LandingPagePayment::where('id', $booking->id)->update(['is_use'=>'Y']);
                    LandingPageMaster::where('id', $booking->template_id)->update(['branding_fees'=>$booking->branding_fees ,'is_branding_free'=>'Y']);
                    session()->flash('success', \Lang::get('client_site.branding_purchased_successfully'));
                    return redirect()->route('list.landing.page.templates');
                }
                session()->flash('success', \Lang::get('client_site.branding_purchased_successfully'));
                return redirect()->route('create.landing.page.template');
            }
            return view('modules.landing_page.landing_payment')->with([
                'booking'   =>  @$booking,
                'profile'   =>  @$profile,
                'time'      =>  @$time,
                'country'   =>  @$country,
                'state'     =>  @$state,
                'accountInfo' => @$accountInfo,
            ]);
        }
    }


    /**
     *method: index
     *created By: Abhisek
     *description: For Search
     */

    public function index($slug = null)
    {
        $nowtime = date('Y-m-d');
        $user = User::with('userDetails.categoryName')->where('slug', $slug)->first();
        if (!@$user) {
            session()->flash('error', \Lang::get('client_site.user_not_found'));
            return redirect()->back();
        }
        $parCat = $chldCat = [];
        $i = 0;
        $j = 0;
        $profile = 0;
        $required = [
            0   => 'country_id',
            1   => 'state_id',
            2   => 'city',
            3   => 'dob',
            4   => 'street_number',
            5   => 'street_name',
            6   => 'complement',
            7   => 'district',
            8   => 'zipcode',
            9  => 'area_code'
        ];
        $av = UserAvailability::where('user_id', $user->id)->where('slot_start', '>', now(env('TIMEZONE'))->addMinutes('30')->toDateTimeString())->whereNotIn('slot_start', getUsedTimeSlotsForAllDates($user->id))->get();
        $avlDay = [];
        $avlDay1 = [];
        foreach ($av->toArray() as $a) {
            $avl = [];
            $st = '';
            foreach ($a as $k => $v) {
                if ($k == 'slot_start') {
                    $avl[$k] = toUserTime($v, 'Y-m-d\TH:i:s');
                    $avl['time_start'] = toUserTime($v, 'g:i A');
                    $st = toUserTime($v, 'Y-m-d');
                } else if ($k == 'slot_end') {
                    $avl[$k] = toUserTime($v, 'Y-m-d\TH:i:s');
                    $avl['time_end'] = toUserTime($v, 'g:i A');
                } else {
                    $avl[$k] = $v;
                }
            }
            array_push($avlDay1, $avl);
            if (!in_array($st, $avlDay)) {
                array_push($avlDay, $st);
            }
        }
        for ($i = 0; $i < sizeof($required); $i++) {
            $str = $required[@$i];
            if (Auth::guard('web')->user()->$str == null) {
                $profile++;
                break;
            } elseif (Auth::guard('web')->user()->$str <= 0 && !is_string(Auth::guard('web')->user()->$str)) {
                $profile++;
                break;
            }
        }
        if ($user == null) {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        } else {
            if ($user->slug == Auth::user()->slug || $user->id == Auth::user()->id) {
                session()->flash('error', \Lang::get('client_site.unauthorize_access'));
                return redirect()->back();
            } else {
                foreach ($user->userDetails as $key) {
                    if ($key->level == 0) {
                        $parCat[$i] = $key->category_id;
                    }
                    if ($key->level > 0 || $key->level != "") {
                        $chldCat[$i] = $key->category_id;
                    }
                    $i++;
                }
                $category = Category::orderBy('name')->where('status', 'A')->get();
                $time = Timezone::get();
                $country = Country::orderBy('name')->get();
                $state = State::orderBy('name')->get();
                $city = City::orderBy('name')->get();

                $date = date('d-m-Y');
                if (session()->has('slotStart')) {
                    $date = date('Y-m-d', strtotime(session()->get('slotStart')));
                    session()->forget('slotStart');
                }
                $slots = UserAvailability::where('user_id', $user->id)->where('date', '>=', now(env('TIMEZONE'))->addMinutes('30')->toDateString())
                    ->where('slot_start', '>=', date('Y-m-d', strtotime('-1 day ' . $date)))
                    ->where('slot_start', '<=', date('Y-m-d', strtotime('+1 day ' . $date)))
                    ->where('slot_start', '>', now(env('TIMEZONE'))->addMinutes('30')->toDateTimeString())->orderBy('slot_start', 'ASC')
                    ->whereNotIn('slot_start', getUsedTimeSlotsForDate($user->id, date('Y-m-d', strtotime($date))))->get();
                $fromTotime = [];
                foreach (getUserTimeSlotsForDate($date, $slots) as $a) {
                    $avl = [];
                    foreach ($a as $k => $v) {
                        if ($k != 'slot_start' && $k != 'slot_end') {
                            $avl[$k] = $v;
                        } else {
                            $avl[$k] = toUserTime($v, 'H:i');
                        }
                    }
                    array_push($fromTotime, $avl);
                }
                $bookingPage = Content::where('id', 12)->first();
                return view('modules.booking.booking')->with([
                    'user'          =>  @$user,
                    'category'      =>  @$category,
                    'parent'        =>  @$parCat,
                    'child'         =>  @$chldCat,
                    'profile'       =>  @$profile,
                    'time'          =>  @$time,
                    'country'       =>  @$country,
                    'state'         =>  @$state,
                    'city'          =>  @$city,
                    'date'          =>  @date('d-m-Y', strtotime($date)),
                    'fromTotime'    =>  @$fromTotime,
                    'avlTime1'      => json_encode($avlDay1),
                    'avlDay'        => json_encode($avlDay),
                    'bookingPage'   =>  @$bookingPage,
                    // 'time'          =>  @$time
                ]);
            }
        }
    }


    public function bookingSuccess($slug)
    {
        $authUser = Auth::guard('web')->user();
        $booking = Booking::where('token_no', $slug)
            ->where('user_id', @Auth::id())
            ->first();
        $profile = 0;
        $required = [
            0   => 'country_id',
            1   => 'state_id',
            2   => 'city',
            3   => 'dob',
            4   => 'street_number',
            5   => 'street_name',
            6   => 'complement',
            7   => 'district',
            8   => 'zipcode',
            9  => 'area_code'
        ];

        for ($i = 0; $i < sizeof($required); $i++) {
            $str = $required[@$i];
            if ($authUser->$str == null) {

                $profile++;
                break;
            } elseif ($authUser->$str <= 0 && !is_string($authUser->$str)) {
                $profile++;
                break;
            }
        }
        if ($booking == null) {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        } else {
            if ($authUser->is_credit_card_added == 'Y' && $authUser->customer_id != '' && $booking->moip_order_id == '' && $booking->amount != 0 && $booking->payment_type == 'C') {
                $this->chargeUser($slug);
            }
            if ($booking->amount == 0 && $booking->payment_status == 'I') {
                $this->freeSessionPayment($slug);
            }
            if ($booking->payment_type == 'BA' && $booking->payment_status == 'I') {
                $this->bankAccountPayment($slug);
            }
            if ($booking->payment_type == 'P' && $booking->payment_status == 'I') {
                $title = "Booking";
                $paydata = new Request([
                    'title'     => $title,
                    'amount'    => $booking->sub_total,
                    'ordID'     => $booking->id,
                ]);
                // dd('Heyya');
                // app('App\Http\Controllers\Modules\Payment\PaypalController')->postPaymentWithpaypal($paydata);
                $this->postPaymentWithpaypal($paydata);
            }
            $booking = Booking::where('token_no', $slug)
                ->where('user_id', @Auth::id())
                ->first();
            $booking = $booking->load('parentCatDetails', 'childCatDetails', 'profDetails');
            $time = Timezone::get();
            $country = Country::orderBy('name')->get();
            $state = State::orderBy('name')->get();
            $accountInfo = AdminBankAccount::first();
            if (@$booking->payment_status == 'F') {
                session()->flash('error', \Lang::get('site.Payment_failed'));
                return redirect()->route('load_user_dashboard');
            }
            $curServerTime = strtotime((now(env('TIMEZONE'))->addHour('24')->toDateTimeString()));
            // $booking_time = date('Y-m-d H:i:s',strtotime(toUTCTime(date('Y-m-d H:i:s', strtotime($booking->date . ' ' . $booking->start_time)))));
            $booking_time = strtotime($booking->date . ' ' . $booking->start_time);
            $is_grater_24hrs = 0;
            if ($curServerTime < $booking_time) {
                $is_grater_24hrs = 1;
            }
            return view('modules.booking.booking_success')->with([
                'booking'   =>  @$booking,
                'profile'   =>  @$profile,
                'time'      =>  @$time,
                'country'   =>  @$country,
                'state'     =>  @$state,
                'accountInfo' => @$accountInfo,
                'is_grater_24hrs' => @$is_grater_24hrs
            ]);
        }
    }


    /**
     * For Completing Customer Profile
     *
     * @method completeProfile
     * @author Abhisek
     */
    public function completeProfile(Request $request)
    {
        try {
            // if (@$request->is_profile > 0) {
            $request->validate([
                'name'              =>  "required",
                'phone_no'          => 'required|digits:9|unique:users,mobile,' . Auth::id(),
                'country'           =>  "required",
                'state'             =>  "required",
                'city'              =>  "required",
                'dob'               =>  "required",
                'street_number'     =>  "required",
                'street_name'       =>  "required",
                'complement'        =>  "required",
                'district'          =>  "required",
                'zipcode'           =>  "required",
                'area_code'         =>  "required",
                'cpf_no'            =>  "required|unique:users,cpf_no," . Auth::id()
            ]);
            $data = [
                'name'              =>  $request->name,
                'mobile'            =>  $request->phone_no,
                'country_id'        =>  $request->country,
                'state_id'          =>  $request->state,
                'city'              =>  $request->city,
                'dob'               =>  date('Y-m-d', strtotime($request->dob)),
                'street_number'     =>  $request->street_number,
                'street_name'       =>  $request->street_name,
                'complement'        =>  $request->complement,
                'district'          =>  $request->district,
                'area_code'         =>  $request->area_code,
                'zipcode'           =>  $request->zipcode,
                'cpf_no'            =>  $request->cpf_no
            ];
            $user = User::where('id', Auth::id())->update($data);

            $this->CPF_CNPJ = $request->cpf_no;
            if (!$this->getNumeral()) {
                session()->flash('error', \Lang::get('client_site.invalid_cpf_number'));
                return redirect()->back();
            }
            // }

            if (@$user || @$is_profile <= 0) {
                $request->validate([
                    'card_name' => "required",
                    'card_cpf_no' => "required",
                    // 'card_dob' => "required",
                    'dob_year' => "required",
                    'dob_month' => "required",
                    'dob_day' => "required",
                    'card_phonecode' => "required",
                    'card_area_code' => "required|digits:2",
                    'card_mobile' => 'required|digits:9',
                ]);
                $dob = $request->dob_year . '-' . $request->dob_month . '-' . $request->dob_day;
                // dd($dob);
                $this->CPF_CNPJ = $request->card_cpf_no;
                if (!$this->getNumeral()) {
                    session()->flash('error', \Lang::get('client_site.invalid_cpf_number'));
                    return redirect()->back();
                }
                $updatedUser = User::where('id', Auth::id())->first();
                $moip = env('PAYMENT_ENVIORNMENT', 'sandbox') == 'live' ? new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_PRODUCTION) : new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_SANDBOX);
                if($updatedUser->customer_id==null){
                $customer = $moip->customers()
                    ->setOwnId(uniqid())
                    ->setFullname(@$updatedUser->name)
                    ->setEmail(@$updatedUser->email)
                    ->setBirthDate(@$updatedUser->dob)
                    ->setTaxDocument(@$updatedUser->cpf_no)
                    ->setPhone(@$updatedUser->area_code, @$updatedUser->mobile)
                    ->addAddress('BILLING', @$updatedUser->street_name, @$updatedUser->street_number, @$updatedUser->district, @$updatedUser->city, strtoupper(substr(@$updatedUser->stateName->name, 0, 2)), @$updatedUser->zipcode, @$updatedUser->complement)
                    ->addAddress('SHIPPING', @$updatedUser->street_name, @$updatedUser->street_number, @$updatedUser->district, @$updatedUser->city, substr(@$updatedUser->stateName->name, 0, 2), @$updatedUser->zipcode, @$updatedUser->complement)->create();
                }
                $c_id=$updatedUser->customer_id?$updatedUser->customer_id:@$customer->getId();
                $card = $moip->customers()->creditCard()
                    ->setExpirationMonth($request->expiry_month)
                    ->setExpirationYear($request->expiry_year)
                    ->setNumber($request->cardnumber)
                    ->setCVC($request->cvc)
                    ->setFullName($request->card_name)
                    ->setBirthDate($dob)
                    ->setTaxDocument('CPF', $request->card_cpf_no)
                    ->setPhone($request->card_phonecode, $request->card_area_code, $request->card_mobile)
                    ->create(@$c_id);

                if (!@$card->getId()) {
                    session()->flash('error', \Lang::get('client_site.internal_server_error'));
                    return redirect()->back();
                }
                $update = User::where('id', Auth::id())->update([
                    'customer_id' => @$c_id,
                    'is_credit_card_added' => @$card->getStore() == true ? 'Y' : 'N'
                ]);
                $is_Created = UserToCard::create([
                    'user_id' => Auth::id(),
                    'card_name' => @$request->card_name,
                    'cpf_no' => @$request->card_cpf_no,
                    'phonecode' => @$request->card_phonecode,
                    'area_code' => @$request->card_area_code,
                    'mobile' => @$request->card_mobile,
                    'dob' => @$dob,
                    'card_id' => @$card->getId(),
                    'first_six' => @$card->getFirst6(),
                    'last_four' => @$card->getLast4(),
                    'card_brand' => @$card->getBrand(),
                    'cred_type' => "CREDIT_CARD",
                    'card_response' => json_encode($card),
                ]);
                if (@$is_Created) {
                    // Booking::where('token_no', $request->order_no)->update([
                    //     'order_status'  =>  'A'
                    // ]);
                    session()->flash('success', \Lang::get('client_site.now_your_profile_is'));
                    return redirect()->back();
                } else {
                    session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
                    return redirect()->back();
                }
                //}
                /* catch(\Exception $e){
                 session()->flash('error', 'Internal server error.');
                return redirect()->back();
            }*/
            } else {
                session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
                return redirect()->back();
            }
        } catch (\Moip\Exceptions\ValidationException $e) {
            session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
            return redirect()->back();
        } catch (Exceptions\UnexpectedException $e) {
            session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
            return redirect()->back();
        } catch (Exception $e) {
            session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
            return redirect()->back();
        }
    }

    /**
     * charge user after booking is complete
     *
     * @method: chargeUser
     */
    private function chargeUser($token)
    {
        $user_id = LandingPagePayment::where('token_no', $token)->first();

        $cardDetails = UserToCard::where('user_id', $user_id->user_id)->first();
        $access_token = getAdminToken();

        $customer = User::where([
            'id' => @$user_id->user_id
        ])->first();
        $amount =  bcmul((@$user_id->total_amount), 100); // calculating on cents...
        $amount =  (int) $amount;

        if (env('PAYMENT_ENVIORNMENT') == 'live') {
            $moip = new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_PRODUCTION);
        } else if (env('PAYMENT_ENVIORNMENT') == 'sandbox') {
            $moip = new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_SANDBOX);
        }

        $order = $moip->orders()->setOwnId(uniqid())
            ->addItem("Consultation Fees", 1, 'Landing Page', @$amount)
            ->setShippingAmount(0)->setAddition(0)->setDiscount(0)
            ->setCustomerId(@$customer->customer_id);
            $order=$order->create();

        return $this->payment($order, $customer, $cardDetails, $user_id);
    }

    /**
     * For Deducting customer balance
     * @method: payment
     */
    private function payment($order, $user, $card, $userBooking)
    {
        if (env('PAYMENT_ENVIORNMENT') == 'live') {
            $url = Moip::ENDPOINT_PRODUCTION;
        } else if (env('PAYMENT_ENVIORNMENT') == 'sandbox') {
            $url = Moip::ENDPOINT_SANDBOX;
        }
        $data = array(
            'installmentCount' => 1,
            'statementDescriptor' => 'netbhe',
            'fundingInstrument' =>
            array(
                'method' => 'CREDIT_CARD',
                'creditCard' =>
                array(
                    'id' => @$card->card_id,
                    'store' => true,
                    'holder' =>
                    array(
                        'fullname' => @$card->card_name,
                        'birthdate' => @$user->dob,
                        'taxDocument' =>
                        array(
                            'type' => 'CPF',
                            'number' => @$user->cpf_no,
                        ),
                        'phone' =>
                        array(
                            'countryCode' => @$user->countryName->phonecode,
                            'areaCode' => @$user->area_code,
                            'number' => @$user->mobile,
                        ),
                        'billingAddress' =>
                        array(
                            'city' => @$user->city,
                            'district' => @$user->district,
                            'street' => @$user->street_name,
                            'streetNumber' => @$user->street_number,
                            'zipCode' => @$user->zipcode,
                            'state' => substr(@$user->stateName->name, 0, 2),
                            'country' => substr(@$user->countryName->name, 0, 3),
                        ),
                    ),
                ),
            ),
        );
        $crl = curl_init();
        $header = array();
        // $header[] = 'Content-length: 0';
        $header[] = 'Content-type: application/json';
        $header[] = 'Authorization: Bearer ' . $this->adminAccessToken;
        curl_setopt($crl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($crl, CURLOPT_URL, "{$url}/v2/orders/" . @$order->getId() . "/payments");
        curl_setopt($crl, CURLOPT_POST, 1);
        curl_setopt($crl, CURLOPT_POST, true);
        curl_setopt($crl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        $payment_output = curl_exec($crl);
        curl_close($crl);
        $payment_output = json_decode($payment_output);
        // dd($payment_output);
        if (@$payment_output->id) {
            if (@$payment_output->status == 'CANCELLED') {
                $UpdateBooking = LandingPagePayment::where([
                    'token_no'        =>  @$userBooking->token_no

                ])->update([
                    'payment_status'  =>  "F",
                    'pg_response'          =>  json_encode($payment_output),
                    'pg_order_id'           =>  @$order->getId(),
                    'pg_payment_id'         =>  @$payment_output->id,
                ]);
                return false;
            }
            $UpdateBooking = LandingPagePayment::where([
                'token_no'        =>  @$userBooking->token_no
            ])->update([
                'payment_status'  =>  "P",
                'pg_response'          =>  json_encode($payment_output),
                'pg_order_id'           =>  @$order->getId(),
                'pg_payment_id'         =>  @$payment_output->id,
            ]);
            // dd(Auth::user());
            if(@Auth::user()->mobile && @Auth::user()->country_code){
                // dd("Getitng In");

                /* $msg = "Dear " . (@Auth::user()->nick_name ?? @Auth::user()->name) .
                ", you have succesfully purchased your landing page " .
                ". Purchase Token: " . (@$userBooking->token_no); */

                // template_11

                $msg = "Olá " . (@Auth::user()->nick_name ?? @Auth::user()->name) . ", Obrigado por usar a Netbhe! você adquiriu sua página de destino com sucesso. Esperamos que goste de seu produto! 😉 Token de compra:" . (@$userBooking->token_no);

                $mobile = "+".Auth::user()->country_code . Auth::user()->mobile;
                // $mobile = "+554598290176";
                // $mobile = "+918017096564";
                // dd($msg);
                // (@$userBooking->profDetails->nick_name ?? @$userBooking->profDetails->name) .
                $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                // if($result['statusCode'] == 200){
                //     dd("Success");
                // } else {
                //     dd("Error");
                // }
            }
            return true;
        }
        else {
            $UpdateBooking = LandingPagePayment::where([
                'token_no'        =>  @$userBooking->token_no
            ])->update([
                'payment_status'  =>  "F"
            ]);

            return false;
        }
    }

    /*
    @Below Functions are used for validating a CPF number.
    */
    public function verificaDigitos($digito1, $digito2, $ver, $ver2)
    {
        $num = $digito1 % 11;
        $digito1 = ($num < 2) ? 0 : 11 - $num;
        $num2 = $digito2 % 11;
        $digito2 = ($num2 < 2) ? 0 : 11 - $num2;
        if ($digito1 === (int) $this->numeral[$ver] && $digito2 === (int) $this->numeral[$ver2]) {
            return true;
        } else {
            return false;
        }
    }
    private function verificaCPF()
    {
        $num1 = ($this->numeral[0] * 10) + ($this->numeral[1] * 9) + ($this->numeral[2] * 8) + ($this->numeral[3] * 7) + ($this->numeral[4] * 6)
            + ($this->numeral[5] * 5) + ($this->numeral[6] * 4) + ($this->numeral[7] * 3) + ($this->numeral[8] * 2);
        $num2 = ($this->numeral[0] * 11) + ($this->numeral[1] * 10) + ($this->numeral[2] * 9) + ($this->numeral[3] * 8) + ($this->numeral[4] * 7)
            + ($this->numeral[5] * 6) + ($this->numeral[6] * 5) + ($this->numeral[7] * 4) + ($this->numeral[8] * 3) + ($this->numeral[9] * 2);
        return $this->verificaDigitos($num1, $num2, 9, 10);
    }
    private function verificaCNPJ()
    {
        $num1 = ($this->numeral[0] * 5) + ($this->numeral[1] * 4) + ($this->numeral[2] * 3) + ($this->numeral[3] * 2) + ($this->numeral[4] * 9) + ($this->numeral[5] * 8)
            + ($this->numeral[6] * 7) + ($this->numeral[7] * 6) + ($this->numeral[8] * 5) + ($this->numeral[9] * 4) + ($this->numeral[10] * 3) + ($this->numeral[11] * 2);
        $num2 = ($this->numeral[0] * 6) + ($this->numeral[1] * 5) + ($this->numeral[2] * 4) + ($this->numeral[3] * 3) + ($this->numeral[4] * 2) + ($this->numeral[5] * 9)
            + ($this->numeral[6] * 8) + ($this->numeral[7] * 7) + ($this->numeral[8] * 6) + ($this->numeral[9] * 5) + ($this->numeral[10] * 4) + ($this->numeral[11] * 3) + ($this->numeral[12] * 2);
        return $this->verificaDigitos($num1, $num2, 12, 13);
    }
    private function getNumeral()
    {
        $this->numeral = preg_replace("/[^0-9]/", "", $this->CPF_CNPJ);
        $strLen = strlen($this->numeral);
        $ret = false;
        switch ($strLen) {
            case 11:
                if ($this->verificaCPF()) {
                    $this->tipo = 'CPF';
                    $ret = true;
                }
                break;
            case 14:
                if ($this->verificaCNPJ()) {
                    $this->tipo = 'CNPJ';
                    $ret = true;
                }
                break;
            default:
                $ret = false;
                break;
        }
        return $ret;
    }
    public function __toString()
    {
        return $this->CPF_CNPJ;
    }
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * For Free Session Payment Create
     * @method: freeSessionPayment
     */
    private function freeSessionPayment($token)
    {
        $userBooking = Booking::where('token_no', $token)->first();
        // $payment = Payment::create([
        //     'token_no'              =>  @$userBooking->token_no,
        //     'order_id'              =>  'ORD-' . @$userBooking->token_no,
        //     // 'payment_id'            =>  @$payment_output->id,
        //     'user_id'               =>  @$userBooking->user_id,
        //     'professional_id'       =>  @$userBooking->professional_id,
        //     'professional_amount'   =>  @$userBooking->amount / 2,
        //     'admin_amount'          =>  @$userBooking->amount / 2,
        //     'total_amount'          =>  @$userBooking->amount,
        //     // 'wirecard_response'     =>  json_encode($payment_output),
        //     'balance_status'        =>  'W'
        // ]);
        // Booking::where([
        //     'token_no' => @$token
        // ])->update([
        //     'moip_order_id'  =>  'ORD-' . @$userBooking->token_no
        // ]);

        $payment = Payment::create([
            'token_no'              =>  @$userBooking->token_no,
            'pg_order_id'           =>  'ORD-' . @$userBooking->token_no,
            'user_id'               =>  @$userBooking->user_id,
            'total_amount'          =>  @$userBooking->amount,
            'payment_status'        =>  'P',
            // 'payment_type'          =>  'BA',
            'order_type'            =>  'B',
        ]);

        if (@$payment) {
            $add = [];
            $add['payment_master_id'] = $payment->id;
            $add['product_order_details_id'] = 0;
            $add['professional_amount'] = @$userBooking->amount / 2;
            $add['admin_commission'] = @$userBooking->amount / 2;
            $add['affiliate_commission'] = 0;
            $add['affiliate_id'] = 0;
            $add['professional_id'] = @$userBooking->professional_id;
            $add['type'] = 'P';
            $add['withdraw_by_wirecard'] = 'N';
            $add['balance_status'] = 'W';
            PaymentDetails::create($add);
            $UpdateBooking = Booking::where([
                'token_no'        =>  @$userBooking->token_no
            ])->update([
                'payment_status'  =>  "P"
            ]);
            $userdata = User::where('id', Auth::id())->first();
            $userdata1 = User::where('id', @$userBooking->professional_id)->first();
            $professionalmail = $userdata1->email;
            $usermail = $userdata->email;
            $bookingdata = $userBooking;

            // // for send mail to user
            Mail::send(new UserBooking($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));

            // // for send mail to professional
            Mail::send(new ProfessionalBooking($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
            // $referralStatus= User::where('id', @$userBooking->user_id)->first();
            // if(@$referralStatus->is_paid_activity=='N'){
            //     User::where('id', @$userBooking->user_id)->update(['is_paid_activity'=>'C']);
            //     User::where('id', @$referralStatus->referrer_id)->increment("benefit_count", 1);
            // }
            return true;
        } else {
            $UpdateBooking = Booking::where([
                'token_no'        =>  @$userBooking->token_no
            ])->update([
                'payment_status'  =>  "F"
            ]);
            return false;
        }
    }
    /**
     * For Check Coupon Code
     * @method: checkCoupon
     */
    public function checkCoupon(Request $request)
    {
        $response = [
            "jsonrpc" => "2.0"
        ];
        if ($request->params['coupon']) {

            $coupon = Coupon::where('coupon_code', $request->params['coupon'])->where('coupon_status', 'A')->first();
            $dated = date_create(@$coupon->exp_date);
            if (@$coupon && $coupon->added_by != 'P') {
                if ($dated < date('Y-m-d')) {
                    $response['status'] = 2;
                    $response['message'] = \Lang::get('client_site.coupon_expiredy');
                } else {
                    $response['status'] = 1;
                    $response['message'] = \Lang::get('client_site.coupon_applied');
                }
            } else {
                $response['status'] = 0;
                $response['message'] = \Lang::get('client_site.coupon_not_valid');
            }
            return response()->json($response);
        }
    }
    /**
     * method: addPaymentCoupon
     *created By: Soumojit
     *Description: For Add Payment Method
     *Date: 15-SEP-2021
     */
    public function addPaymentCoupon($slug, Request $request)
    {
        $request->validate([
            "payment_method" =>  "required|in:C,BA,S,P",
        ]);
        $booking = LandingPagePayment::where('token_no', $slug)->where('user_id', @Auth::id())->first();
        if (@$booking) {
            $upd = [];
            $upd['payment_type'] = $request->payment_method;
            $update =LandingPagePayment::where('id', $booking->id)->update($upd);
            if (@$update) {
                return redirect()->back();
            }
        }
        return redirect()->back();
    }
    /**
     * For Free Session Payment Create
     * @method: freeSessionPayment
     */
    private function bankAccountPayment($token)
    {
        $userBooking = Booking::where('token_no', $token)->first();
        $referDiscount = ReferralDiscount::first();
        $checkTeacher = User::where('id', $userBooking->professional_id)->first();
        $professionalCommissionList = Commission::first();
        $professionalCommission = 50;
        if ($userBooking->booking_type == 'C') {
            $professionalCommission = $professionalCommissionList->chat_commission;
        } else {
            $professionalCommission = $professionalCommissionList->commission;
        }

        if (@$checkTeacher->is_paid_activity == 'N') {
            $discount = $professionalCommission + $referDiscount->referrer_teacher_discount;
            $amount = (@$userBooking->amount * @$discount) / 100;
            $professional_amount   =  @$amount;
            $admin_amount         =   @$userBooking->sub_total - @$amount;
            User::where('id', @$checkTeacher->id)->update(['is_paid_activity' => 'C']);
            User::where('id', @$checkTeacher->referrer_id)->increment("benefit_count", 1);
        } elseif (@$checkTeacher->benefit_count > 0) {
            $discount = $professionalCommission + $referDiscount->referred_teacher_discount;
            $amount = (@$userBooking->amount * @$discount) / 100;
            $professional_amount   =  @$amount;
            $admin_amount         =   @$userBooking->sub_total - @$amount;
            User::where('id', @$checkTeacher->id)->decrement("benefit_count", 1);
        } else {
            $discount = $professionalCommission;
            $amount = (@$userBooking->amount * @$discount) / 100;
            $professional_amount   =  @$amount;
            $admin_amount         =   @$userBooking->sub_total - @$amount;
        }
        $total_amount         =  @$userBooking->sub_total;
        // dd($checkTeacher->benefit_count);
        // $payment = Payment::create([
        //     'token_no'              =>  @$userBooking->token_no,
        //     'order_id'              =>  'ORD-' . @$userBooking->token_no,
        //     // 'payment_id'            =>  @$payment_output->id,
        //     'user_id'               =>  @$userBooking->user_id,
        //     'professional_id'       =>  @$userBooking->professional_id,
        //     'professional_amount'   =>  @$professional_amount,
        //     'admin_amount'          =>  @$admin_amount,
        //     'total_amount'          =>  @$total_amount,
        //     // 'wirecard_response'     =>  json_encode($payment_output),
        //     'balance_status'        =>  'R',
        //     'order_type'            =>  'B',
        // ]);
        $payment = Payment::create([
            'token_no'              =>  @$userBooking->token_no,
            'pg_order_id'           =>  'ORD-' . @$userBooking->token_no,
            'user_id'               =>  @$userBooking->user_id,
            'total_amount'          =>  @$total_amount,
            'payment_status'        =>  'PR',
            'payment_type'          =>  'BA',
            'order_type'            =>  'B',
        ]);
        if (@$payment) {
            $add = [];
            $add['payment_master_id'] = $payment->id;
            $add['product_order_details_id'] = 0;
            $add['professional_amount'] = @$professional_amount;
            $add['admin_commission'] = @$admin_amount;
            $add['affiliate_commission'] = 0;
            $add['affiliate_id'] = 0;
            $add['professional_id'] = @$userBooking->professional_id;
            $add['type'] = 'P';
            $add['withdraw_by_wirecard'] = 'N';
            $add['balance_status'] = 'R';
            PaymentDetails::create($add);
            $UpdateBooking = Booking::where([
                'token_no'        =>  @$userBooking->token_no
            ])->update([
                'payment_status'  =>  "PR",
                'order_status'  =>  "A"
            ]);
            $referralStatus = User::where('id', @$userBooking->user_id)->first();
            if (@$referralStatus->is_paid_activity == 'N') {
                User::where('id', @$userBooking->user_id)->update(['is_paid_activity' => 'C']);
                User::where('id', @$referralStatus->referrer_id)->increment("benefit_count", 1);
            }
            $userdata = User::where('id', Auth::id())->first();
            $userdata1 = User::where('id', @$userBooking->professional_id)->first();
            $professionalmail = $userdata1->email;
            $usermail = $userdata->email;
            $bookingdata = $userBooking;

            // // for send mail to user
            // Mail::send(new UserBooking($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));

            // // // for send mail to professional
            // Mail::send(new ProfessionalBooking($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
            // // for send mail to admin for Bank account payment
            Mail::send(new UserBankAccountBooking($bookingdata->id, $usermail));

            Mail::send(new BankAccountPayment($bookingdata->id));
            return true;
        } else {
            $UpdateBooking = Booking::where([
                'token_no'        =>  @$userBooking->token_no
            ])->update([
                'payment_status'  =>  "F"
            ]);
            return false;
        }
    }
    /**
     * For upload Payment Document Bank Account Payment
     * @method: uploadDocument
     */
    public function uploadDocument($slug, Request $request)
    {
        $request->validate([
            "upload_file" =>  "required",
        ]);
        $booking = Booking::where('token_no', $slug)->where('user_id', @Auth::id())->first();
        if (@$booking) {
            $upd = [];
            if (@$request->upload_file) {
                $upload_file = $request->upload_file;
                $filename = time() . '-' . rand(1000, 9999) . '.' . $upload_file->getClientOriginalExtension();
                Storage::putFileAs('public/Bank_Document', $upload_file, $filename);
                $upd['payment_document'] = $filename;
            }
            // dd($upd);
            $update = Booking::where('id', $booking->id)->update($upd);
            if (@$update) {
                return redirect()->back();
            }
        }
        return redirect()->back();
    }


    /**
     * method: removePaymentMethod
     * Description: For remove Payment Method
     * Author: Soumojit
     * Date:16-SEPT-2021
     */
    public function removePaymentMethod($slug, Request $request)
    {
        $booking = LandingPagePayment::where('token_no', $slug)->where('user_id', @Auth::id())->first();
        if (@$booking) {
            $upd = [];
            $upd['payment_type'] = null;
            $update = LandingPagePayment::where('id', $booking->id)->update($upd);
            if (@$update) {
                return redirect()->back();
            }
        }
        return redirect()->back();
    }

   /**
     * Method: paymentCreate
     * Description: This method is used for create payment by stripe
     * Author: Soumojit
     * Date:16-SEPT-2021
     */
    public function paymentCreate(Request $request)
    {
        $response = [
            'jsonrpc' => 2.0
        ];
        $unique_payment_id = decrypt($request->params['id']);
        $payments = LandingPagePayment::where('id', $unique_payment_id)->where('user_id', @auth()->user()->id)->first();
        if(@$payments){
            Stripe::setApiKey(config('services.stripe.secret'));
            $paymentIntent = PaymentIntent::create([
                'amount' => (int) bcmul($payments->total_amount, 100),
                'currency' => 'usd',
                'description' => 'Session perched',
                'metadata' => [
                    'unique_payment_id' => encrypt($unique_payment_id),
                ],
                'receipt_email' => @auth()->user()->email,
            ]);
            $response['result']['clientSecret'] = $paymentIntent->client_secret;
            $response['result']['payment'] = $paymentIntent;
            return response()->json($response);
        }
        $response['error']['message'] = \Lang::get('message.error_in_Payment');
        return response()->json($response);
    }
    /**
     * Method: paymentSuccessHandlerStripe
     * Description: This method is used for after payment success by stripe
     * Author: Soumojit
     * Date:16-SEPT-2021
     */
    public function paymentSuccessHandlerStripe(Request $request){
        $response = [
            'jsonrpc' => 2.0
        ];
        $unique_payment_id = decrypt($request->params['id']);
        $payments = LandingPagePayment::where('id', $unique_payment_id)->where('user_id', @auth()->user()->id)->first();
        $userBooking =LandingPagePayment::where('token_no', $payments->token_no)->first();
        $UpdateBooking =LandingPagePayment::where([
            'token_no'        =>  @$userBooking->token_no
        ])->update([
            'payment_status'  =>  "P",
            'pg_response'          =>  json_encode($request->params['result']),
            'pg_order_id'           =>  'ORD-' . @$userBooking->token_no,
            'pg_payment_id'           =>  $request->params['result']['paymentIntent']['id'],
        ]);
        // dd(Auth::user());
        if(@Auth::user()->mobile && @Auth::user()->country_code){
            // dd("Getitng In");

            /* $msg = "Dear " . (@Auth::user()->nick_name ?? @Auth::user()->name) .
            ", you have succesfully purchased your landing page " .
            ". Purchase Token: " . (@$userBooking->token_no); */
            // template_11

            $msg = "Olá " . (@Auth::user()->nick_name ?? @Auth::user()->name) . ", Obrigado por usar a Netbhe! você adquiriu sua página de destino com sucesso. Esperamos que goste de seu produto! 😉 Token de compra:" . (@$userBooking->token_no);

            $mobile = "+".Auth::user()->country_code . Auth::user()->mobile;
            // $mobile = "+554598290176";
            // $mobile = "+918017096564";
            // dd($msg);
            // (@$userBooking->profDetails->nick_name ?? @$userBooking->profDetails->name) .
            $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
            // if($result['statusCode'] == 200){
            //     dd("Success");
            // } else {
            //     dd("Error");
            // }
        }
        $response['success']['message'] = \Lang::get('site.Payment_successfull');
        return response()->json($response);
    }


    /**
     * Method: postPaymentWithpaypal
     * Description: This method is used for payment by paypal
     * Author: Sayantani
     * date:13-AUGUST-2021
     */
    public function postPaymentWithpaypal(Request $request)
    {
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        Session::put('ordID',encrypt($request->get('ordID')));
        // dd($request->get('ordID'));

        $unique_payment_id = $request->get('ordID');
        // $payments = Booking::where('id', $unique_payment_id)->where('user_id', @auth()->user()->id)->first();

        // $userBooking = Booking::where('token_no', $payments->token_no)->first();
        $payments = LandingPagePayment::where('id', $unique_payment_id)->where('user_id', @auth()->user()->id)->first();

        $userBooking = LandingPagePayment::where('token_no', $payments->token_no)->first();
        // $referDiscount = ReferralDiscount::first();
        // $checkTeacher = User::where('id', $userBooking->professional_id)->first();

    	$item_1 = new Item();

        $item_1->setName($request->get('title'))
            ->setCurrency(env('PAYPAL_CURRENCY'))
            ->setQuantity(1)
            ->setPrice($request->get('amount'));

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency(env('PAYPAL_CURRENCY'))
            ->setTotal($request->get('amount'));

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Enter Your transaction description');

        $redirect_urls = new RedirectUrls();
        // $redirect_urls->setReturnUrl(route('paypal.booking.status'))
        //     ->setCancelUrl(route('paypal.booking.status'));
        $redirect_urls->setReturnUrl(route('paypal.landing.status'))
            ->setCancelUrl(route('paypal.landing.status'));

        $payment = new PaymentPaypal();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                // Session::put('error','Connection timeout');
                session()->flash('error', \Lang::get('client_site.connection_timeout'));
                // return Redirect::route('paywithpaypal');
                // return Redirect::route('booking.sces', @$userBooking->token_no);
                return Redirect::route('landing.page.payment.store.view', @$userBooking->token_no);
            } else {
                // Session::put('error','Some error occur, sorry for inconvenient');
                session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
                // return Redirect::route('paywithpaypal');
                // return Redirect::route('booking.sces', @$userBooking->token_no);
                return Redirect::route('landing.page.payment.store.view', @$userBooking->token_no);
            }
        }

        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        Session::put('paypal_payment_id', $payment->getId());

        if(isset($redirect_url)) {
            // dd($redirect_url);
            // return Redirect::away($redirect_url);
            return redirect()->away($redirect_url)->send();
        }

        // Session::put('error','Unknown error occurred');
        session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
    	// return Redirect::route('paywithpaypal');
        // return Redirect::route('booking.sces', @$userBooking->token_no);
        return Redirect::route('landing.page.payment.store.view', @$userBooking->token_no);
    }

    /**
     * Method: getPaymentStatus
     * Description: This method is used after payment success by paypal
     * Author: Sayantani
     * date:13-AUGUST-2021
     */
    public function getPaymentStatus1(Request $request)
    {
        $payment_id = Session::get('paypal_payment_id');
        $ordID = decrypt(Session::get('ordID'));

        $unique_payment_id = $ordID;
        // dd($ordID);
        $payments = Booking::where('id', $unique_payment_id)->where('user_id', @auth()->user()->id)->first();

        $userBooking = Booking::where('token_no', $payments->token_no)->first();
        $referDiscount = ReferralDiscount::first();
        $checkTeacher = User::where('id', $userBooking->professional_id)->first();

        Session::forget('paypal_payment_id');
        if (empty($request->input('PayerID')) || empty($request->input('token'))) {
            // Session::put('error','Payment failed');
            session()->flash('error', \Lang::get('site.Payment_failed'));
            // return Redirect::route('paywithpaypal');
            return Redirect::route('booking.sces', @$userBooking->token_no);
        }
        $payment = PaymentPaypal::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId($request->input('PayerID'));
        $result = $payment->execute($execution, $this->_api_context);
        $res_arr = $result->toArray();
        $res = json_decode($result);

        if ($result->getState() == 'approved') {
            // session()->flash('success', \Lang::get('site.Payment_successfull'));
            $userBooking = Booking::where('token_no', $payments->token_no)->first();
            $referDiscount = ReferralDiscount::first();
            $checkTeacher = User::where('id', $userBooking->professional_id)->first();
            $professionalCommissionList = Commission::first();
            $professionalCommission = 50;
            if ($userBooking->booking_type == 'C') {
                $professionalCommission = $professionalCommissionList->chat_commission;
            } else {
                $professionalCommission = $professionalCommissionList->commission;
            }

            if (@$checkTeacher->is_paid_activity == 'N') {
                $discount = $professionalCommission + $referDiscount->referrer_teacher_discount;
                $amount = (@$userBooking->amount * @$discount) / 100;
                $professional_amount   =  @$amount;
                $admin_amount         =   @$userBooking->sub_total - @$amount;
                User::where('id', @$checkTeacher->id)->update(['is_paid_activity' => 'C']);
                User::where('id', @$checkTeacher->referrer_id)->increment("benefit_count", 1);
            } elseif (@$checkTeacher->benefit_count > 0) {
                $discount = $professionalCommission + $referDiscount->referred_teacher_discount;
                $amount = (@$userBooking->amount * @$discount) / 100;
                $professional_amount   =  @$amount;
                $admin_amount         =   @$userBooking->sub_total - @$amount;
                User::where('id', @$checkTeacher->id)->decrement("benefit_count", 1);
            } else {
                $discount = $professionalCommission;
                $amount = (@$userBooking->amount * @$discount) / 100;
                $professional_amount   =  @$amount;
                $admin_amount         =   @$userBooking->sub_total - @$amount;
            }
            $total_amount         =  @$userBooking->sub_total;
            $payment = Payment::create([
                'token_no'              =>  @$userBooking->token_no,
                'pg_order_id'           =>  'ORD-' . @$userBooking->token_no,
                'pg_payment_id'         =>  $res_arr['id'],
                'user_id'               =>  @$userBooking->user_id,
                'total_amount'          =>  @$total_amount,
                'payment_status'        =>  'P',
                'payment_type'          =>  'P',
                'pg_response'           =>  json_encode($res),
                'order_type'            =>  'B',
            ]);
            if (@$payment) {
                // dd("Enterd");
                $add = [];
                $add['payment_master_id'] = $payment->id;
                $add['product_order_details_id'] = 0;
                $add['professional_amount'] = @$professional_amount;
                $add['admin_commission'] = @$admin_amount;
                $add['affiliate_commission'] = 0;
                $add['affiliate_id'] = 0;
                $add['professional_id'] = @$userBooking->professional_id;
                $add['type'] = 'P';
                $add['withdraw_by_wirecard'] = 'N';
                $add['balance_status'] = 'R';
                PaymentDetails::create($add);
                $UpdateBooking = Booking::where([
                    'token_no'        =>  @$userBooking->token_no
                ])->update([
                    'payment_status'  =>  "P",
                    'order_status'  =>  "A"
                ]);
                $referralStatus = User::where('id', @$userBooking->user_id)->first();
                if (@$referralStatus->is_paid_activity == 'N') {
                    User::where('id', @$userBooking->user_id)->update(['is_paid_activity' => 'C']);
                    User::where('id', @$referralStatus->referrer_id)->increment("benefit_count", 1);
                }
                $userdata = User::where('id', Auth::id())->first();
                $userdata1 = User::where('id', @$userBooking->professional_id)->first();
                $professionalmail = $userdata1->email;
                $usermail = $userdata->email;
                $bookingdata = $userBooking;
                // Mail::send(new UserBankAccountBooking($bookingdata->id, $usermail));

                // Mail::send(new BankAccountPayment($bookingdata->id));

                Mail::send(new UserBooking($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));

                // // for send mail to professional
                Mail::send(new ProfessionalBooking($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));
                // dd(Auth::user());
                if(@Auth::user()->mobile && @Auth::user()->country_code){
                    // dd("Getitng In");

                    /* $msg = "Dear " . (@Auth::user()->nick_name ?? @Auth::user()->name) .
                    ", you have succesfully purchased your landing page " .
                    ". Purchase Token: " . (@$userBooking->token_no); */
                    // template_11

                    $msg = "Olá " . (@Auth::user()->nick_name ?? @Auth::user()->name) . ", Obrigado por usar a Netbhe! você adquiriu sua página de destino com sucesso. Esperamos que goste de seu produto! 😉 Token de compra:" . (@$userBooking->token_no);

                    $mobile = "+".Auth::user()->country_code . Auth::user()->mobile;
                    // $mobile = "+554598290176";
                    // $mobile = "+918017096564";
                    // dd($msg);
                    // (@$userBooking->profDetails->nick_name ?? @$userBooking->profDetails->name) .
                    $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                    // if($result['statusCode'] == 200){
                    //     dd("Success");
                    // } else {
                    //     dd("Error");
                    // }
                }
                // $response['success']['message'] = 'Payment Successfully';
                // return response()->json($response);
                session()->flash('success', \Lang::get('site.Payment_successfull'));
                return Redirect::route('booking.sces', @$userBooking->token_no);
            }
            return Redirect::route('booking.sces', @$userBooking->token_no);
		    // return Redirect::route('paywithpaypal');
        }

        // Session::put('error','Payment failed !!');
        session()->flash('error', \Lang::get('site.Payment_failed'));
        $UpdateBooking = ProductOrder::where([
            'token_no'        =>  @$userBooking->token_no
        ])->update([
            'payment_status'  =>  "F"
        ]);
        return Redirect::route('booking.sces', @$userBooking->token_no);
		// return Redirect::route('paywithpaypal');
    }
    public function getPaymentStatus(Request $request)
    {
        $payment_id = Session::get('paypal_payment_id');
        $ordID = decrypt(Session::get('ordID'));

        $unique_payment_id = $ordID;
        // dd($ordID);
        $payments = LandingPagePayment::where('id', $unique_payment_id)->where('user_id', @auth()->user()->id)->first();

        $userBooking = LandingPagePayment::where('token_no', $payments->token_no)->first();

        Session::forget('paypal_payment_id');
        if (empty($request->input('PayerID')) || empty($request->input('token'))) {
            // Session::put('error','Payment failed');
            session()->flash('error', \Lang::get('site.Payment_failed'));
            // return Redirect::route('paywithpaypal');
            return Redirect::route('landing.page.payment.store.view', @$userBooking->token_no);
        }
        $payment = PaymentPaypal::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId($request->input('PayerID'));
        $result = $payment->execute($execution, $this->_api_context);
        $res_arr = $result->toArray();
        $res = json_decode($result);

        if ($result->getState() == 'approved') {

            // $total_amount         =  @$userBooking->sub_total;
            // $payment = Payment::create([
            //     'token_no'              =>  @$userBooking->token_no,
            //     'pg_order_id'           =>  'ORD-' . @$userBooking->token_no,
            //     'pg_payment_id'         =>  $res_arr['id'],
            //     'user_id'               =>  @$userBooking->user_id,
            //     'total_amount'          =>  @$total_amount,
            //     'payment_status'        =>  'P',
            //     'payment_type'          =>  'P',
            //     'pg_response'           =>  json_encode($res),
            //     'order_type'            =>  'B',
            // ]);
		    // return Redirect::route('paywithpaypal');
            $UpdateBooking = LandingPagePayment::where([
                'token_no'        =>  @$userBooking->token_no
            ])->update([
                'payment_status'  =>  "P",
                'pg_response'          =>  json_encode($res),
                'pg_order_id'           =>  'ORD-' . @$userBooking->token_no,
                'pg_payment_id'         =>  $res_arr['id'],
            ]);
            // dd(Auth::user());
            if(@Auth::user()->mobile && @Auth::user()->country_code){
                // dd("Getitng In");

                /* $msg = "Dear " . (@Auth::user()->nick_name ?? @Auth::user()->name) .
                ", you have succesfully purchased your landing page " .
                ". Purchase Token: " . (@$userBooking->token_no); */
                // template_11

                $msg = "Olá " . (@Auth::user()->nick_name ?? @Auth::user()->name) . ", Obrigado por usar a Netbhe! você adquiriu sua página de destino com sucesso. Esperamos que goste de seu produto! 😉 Token de compra:" . (@$userBooking->token_no);

                $mobile = "+".Auth::user()->country_code . Auth::user()->mobile;
                // $mobile = "+554598290176";
                // $mobile = "+918017096564";
                // dd($msg);
                // (@$userBooking->profDetails->nick_name ?? @$userBooking->profDetails->name) .
                $result = app('App\Http\Controllers\HomeController')->whatsappTwilioPost($mobile, $msg);
                // if($result['statusCode'] == 200){
                //     dd("Success");
                // } else {
                //     dd("Error");
                // }
            }
            session()->flash('success', \Lang::get('site.Payment_successfull'));
            return Redirect::route('landing.page.payment.store.view', @$userBooking->token_no);
        }

        // Session::put('error','Payment failed !!');
        session()->flash('error', \Lang::get('site.Payment_failed'));
        $UpdateBooking = LandingPagePayment::where([
            'token_no'        =>  @$userBooking->token_no
        ])->update([
            'payment_status'  =>  "F"
        ]);
        return Redirect::route('list.landing.page.templates');
    }

}
