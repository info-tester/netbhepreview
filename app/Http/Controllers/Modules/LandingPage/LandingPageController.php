<?php

namespace App\Http\Controllers\Modules\LandingPage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\LandingTemplate;
use App\Models\LandingPageMaster;
use App\Models\LandingPageContentDetail;
use App\Models\LandingPageProductDetail;
use App\Models\LandingPageWhyUs;
use App\Models\LandingPageFaq;
use App\Models\LandingPageLead;
use App\User;
use Exception;
use validator;
use Auth;
use Image;
use Illuminate\Support\Facades\Storage;
use App\Models\LandingPagePricing;
use App\Models\LandingPagePayment;
class LandingPageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('loadTemplate', 'landingPage', 'saveLead');
    }

    public function loadTemplate($id)
    {
        $data['template'] = LandingTemplate::where('id', $id)->first();
        return view('modules.landing_page.landing' . $id)->with($data);
    }

    public function index()
    {
        // $size = getimagesize("http://localhost/netbhepreview/storage/app/public/uploads/landing_page/1634981235-7163.jpg");
        // dd($size);
        $data['landing_pricing']=LandingPagePricing::first();
        $data['templates'] = LandingPageMaster::where('user_id', Auth::id())->where('status', '!=', 'D')->orderBy('id','desc')->get();
        $data['free']=1;
        if($data['landing_pricing']->free_landing_pages <= $data['templates']->count()){
            $data['free']=0;
        }
        $check =LandingPagePayment::where('user_id',auth()->id())->where('payment_for','!=','B')->where('payment_status','P')->where('is_use','N')->first();
        if(@$check){
            $data['free']=1;
        }
        return view('modules.landing_page.index')->with($data);
    }

    public function create($id=null)
    {
        $data['templates'] = LandingTemplate::get();
        if(@$id){
            $data['detail'] = LandingPageMaster::with('getContentDetails', 'getProductDetails', 'getWhyUs')->where('id', $id)->where('status', '!=', 'D')->first();
            if($data['detail']->user_id !== Auth::id()){
                return redirect()->back()->with('error', \Lang::get('client_site.unauthorize_access'));
            }
            if($data['detail']->page_status == 'B'){
                session()->flash('error', \Lang::get('client_site.template_is_blocked'));
                return redirect()->route('list.landing.page.templates');
            }
            $data['temp'] = LandingTemplate::where('id', $data['detail']->landing_template_id)->first();
        }
        if($id==null){
            $data['landing_pricing']=LandingPagePricing::first();
            $data['free']=1;
            $used_templates = LandingPageMaster::where('user_id', Auth::id())->orderBy('id','desc')->count();
            if($data['landing_pricing']->free_landing_pages <= $used_templates){
                $data['free']=0;
            }
            $check = LandingPagePayment::where('user_id',auth()->id())->where('payment_for','!=','B')->where('payment_status','P')->where('is_use','N')->first();
            if(@$check){
                $data['free']=1;
            }
            if($data['free']==0){
                session()->flash('error', \Lang::get('client_site.free_template_not_available'));
                return redirect()->route('list.landing.page.templates');
            }
        }
        return view('modules.landing_page.create')->with($data);
    }

    public function store(Request $request, $id=null)
    {
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        $insert['user_id']                  = Auth::id();

        if(@$id) {
            $landing_page = LandingPageMaster::where('id', $id)->first();

            $insert['link_facebook']            = @$request->link_facebook;
            $insert['link_linkedin']            = @$request->link_linkedin;
            $insert['link_twitter']             = @$request->link_twitter;
            $insert['link_instagram']           = @$request->link_instagram;
            $insert['link_pinterest']           = @$request->link_pinterest;
            $insert['link_youtube']             = @$request->link_youtube;
            $insert['link_tiktok']             = @$request->link_tiktok;

            $insert['header_line_1']            = @$request->header_line_1;
            $insert['header_line_2']            = @$request->header_line_2;
            $insert['header_button']            = @$request->header_button;
            $insert['header_button_text']       = @$request->header_button_text;
            $insert['header_button_type']       = @$request->header_button_type;
            $insert['header_button_link']       = @$request->header_button_link;
            $insert['lead_position']            = @$request->lead_position;
            $insert['lead_section_heading']     = @$request->lead_section_heading;
            $insert['lead_section_footer_submit_btn']     = @$request->lead_section_footer_submit_btn;
            $insert['lead_section_desc']        = @$request->lead_section_desc;
            $insert['lead_fname']               = @$request->lead_fname;
            $insert['lead_lname']               = @$request->lead_lname;
            $insert['lead_email']               = @$request->lead_email;
            $insert['lead_country_code']        = @$request->lead_country_code;
            $insert['lead_mobile']              = @$request->lead_mobile;
            $insert['lead_address']             = @$request->lead_address;
            $insert['lead_city']                = @$request->lead_city;
            $insert['lead_state']               = @$request->lead_state;
            $insert['lead_postal_code']         = @$request->lead_postal_code;
            $insert['lead_website']             = @$request->lead_website;
            $insert['lead_facebook']            = @$request->lead_facebook;
            $insert['lead_instagram']           = @$request->lead_instagram;
            $insert['lead_linkedin']            = @$request->lead_linkedin;
            $insert['lead_message']             = @$request->lead_message;
            $insert['lead_success_heading']         = @$request->lead_success_heading;
            $insert['lead_success_desc']            = @$request->lead_success_desc;
            $insert['lead_success_button']          = @$request->lead_success_button;
            $insert['lead_success_button_caption']  = @$request->lead_success_button_caption;
            $insert['lead_download_link']           = @$request->lead_download_link;
            $insert['lead_privacy_policy']      = @$request->lead_privacy_policy;
            $insert['header_reverse_counter']   = @$request->header_reverse;
            $insert['header_counter_date']      = date('Y-m-d',strtotime(@$request->header_counter_date));

            $insert['why_us_section']           = @$request->why_us_section;
            $insert['why_us_heading']           = @$request->why_us_heading;
            $insert['why_us_description']       = @$request->why_us_description;

            $insert['body_single_content']              = @$request->body_single_content;
            $insert['body_single_content_heading']      = @$request->body_single_content_heading;
            $insert['body_single_content_desc']         = @$request->body_single_content_desc;
            $insert['body_single_content_button']       = @$request->body_single_content_button;
            $insert['body_single_content_button_text']  = @$request->body_single_content_button_text;
            $insert['body_single_content_button_link']  = @$request->body_single_content_button_link;

            $insert['show_faq']                 = @$request->show_faq;
            $insert['faq_heading']              = @$request->faq_heading;
            $insert['faq_description']          = @$request->faq_description;

            $insert['color_primary']            = @$request->color_primary;
            $insert['color_secondary']          = @$request->color_secondary;
            $insert['color_tertiary']           = @$request->color_tertiary;

            $insert['body_single_video']        = @$request->single_video;
            $insert['body_single_video_heading'] = @$request->body_single_video_heading;
            $insert['body_single_video_desc']   = @$request->body_single_video_desc;
            $insert['image_style']              = @$request->image_style;

            $insert['product_section_heading']  = @$request->product_section_heading;
            $insert['product_section_desc']     = @$request->product_section_desc;
            $insert['product_slider']           = @$request->product_slider;

            if(@$request->hasFile('landing_logo')) {
                @unlink('storage/app/public/uploads/landing_page/'.@$landing_page->landing_logo);
                $image = @$request->landing_logo;
                $image_name = time().'-'.rand(1000,9999).'.'.@$image->getClientOriginalExtension();
                Storage::putFileAs('public/uploads/landing_page/', $image, $image_name);
                Storage::putFileAs('public/uploads/landing_page/', $image, 'thumb_'.$image_name);
                $insert['landing_logo'] = $image_name;
                $insert['landing_logo_thumbnail'] = 'thumb_'.$image_name;

                $thumbnailpath = storage_path('app/public/uploads/landing_page/thumb_'.$image_name);
                $this->createThumbnail($thumbnailpath, 150, 150);
                // if(@$request->hasFile('landing_logo_thumbnail')) {
                //     @unlink('storage/app/public/uploads/landing_page/'.@$landing_page->landing_logo_thumbnail);
                //     $thumb = @$request->landing_logo_thumbnail;
                //     $thumb_name = 'thumb_' . time().'-'.rand(1000,9999).'.'.@$image->getClientOriginalExtension();
                //     Storage::putFileAs('public/uploads/landing_page/', $image, $thumb_name);
                //     $insert['landing_logo_thumbnail'] = $thumb_name;
                // }
            }

            if(@$request->hasFile('header_image')){
                @unlink('storage/app/public/uploads/landing_page/'.@$landing_page->header_image);
                $image = @$request->header_image;
                $image_name = time().'-'.rand(1000,9999).'.'.@$image->getClientOriginalExtension();
                Storage::putFileAs('public/uploads/landing_page/', $image, $image_name);
                $insert['header_image'] = $image_name;
            }

            if(@$request->hasFile('single_file')){
                @unlink('storage/app/public/uploads/landing_page/'.@$landing_page->body_single_video_filename);
                $file = @$request->single_file;
                $file_name = time().'-'.rand(1000,9999).'.'.@$file->getClientOriginalExtension();
                Storage::putFileAs('public/uploads/landing_page/', $file, $file_name);
                $mime = $file->getMimeType();
                $insert['body_single_video_type'] = (strpos($file->getMimeType(), "video") !== false) ? "V" : "I";
                $insert['body_single_video_filename'] = $file_name;
            }

            if(@$request->hasFile('body_single_content_image')){
                @unlink('storage/app/public/uploads/landing_page/'.@$landing_page->body_single_content_image);
                $file = @$request->body_single_content_image;
                $file_name = time().'-'.rand(1000,9999).'.'.@$file->getClientOriginalExtension();
                Storage::putFileAs('public/uploads/landing_page/', $file, $file_name);
                $mime = $file->getMimeType();
                $insert['body_single_content_image'] = $file_name;
            }

            $landing_page->update($insert);
            $jsn['request'] = $request->all();
        } else {

            $check =LandingPagePayment::where('user_id',auth()->id())->where('payment_for','!=','B')->where('payment_status','P')->where('is_use','N')->first();
            if(@$check){
                if($check->payment_for=='T'){
                    $insert['page_fees']=$check->page_fees;
                    $insert['page_type']='P';
                }
                if($check->payment_for=='BT'){
                    $insert['page_fees']=$check->page_fees;
                    $insert['page_type']='P';
                    $insert['branding_fees']=$check->branding_fees;
                    $insert['is_branding_free']='Y';
                }
                // LandingPagePayment::where('user_id',auth()->id())->where('id',$check->id)->update(['is_use'=>'Y']);
            }
            $body_single_content = null;
            if(@$request->params['template_number'] == 1){
                $body_single_content = 'N';
            } else if(@$request->params['template_number'] == 2){
                $body_single_content = 'Y';
            }
            $insert['landing_template_id']      = @$request->params['template_number'];
            $insert['landing_title']            = @$request->params['template_name'];
            $insert['header_counter_date']      = date('Y-m-d', time() + 86400);
            $insert['color_primary']            = LandingTemplate::where('id', @$request->params['template_number'])->first()->color_primary;
            $insert['color_secondary']          = LandingTemplate::where('id', @$request->params['template_number'])->first()->color_secondary;
            $insert['color_tertiary']           = LandingTemplate::where('id', @$request->params['template_number'])->first()->color_tertiary;
            $insert['body_single_content']      = $body_single_content;
            if(@$request->params['template_number'] == 1){
                $insert['lead_fname'] = $insert['lead_lname'] = $insert['lead_email'] = $insert['lead_mobile'] = $insert['lead_facebook'] = $insert['lead_instagram'] = $insert['lead_linkedin'] = $insert['lead_website'] = $insert['lead_message'] = 'Y';
                $insert['lead_country_code'] = $insert['lead_address'] = $insert['lead_city'] = $insert['lead_state'] = $insert['lead_postal_code'] = 'N';
                $insert['lead_position'] = 'P';
            } else {
                $insert['lead_fname'] = $insert['lead_lname'] = $insert['lead_email'] = $insert['lead_message'] = 'Y';
                $insert['lead_country_code'] = $insert['lead_mobile'] = $insert['lead_address'] = $insert['lead_city'] = $insert['lead_state'] = $insert['lead_postal_code'] = $insert['lead_website'] = $insert['lead_facebook'] = $insert['lead_instagram'] = $insert['lead_linkedin'] = 'N';
                $insert['lead_position'] = 'F';
            }
            $landing_page = LandingPageMaster::create($insert);
            $slug = str_slug(@$request->params['template_name'])."-".$landing_page->id;
            $landing_page->update(['slug'=>$slug]);
            session()->flash('success', \Lang::get('client_site.cert_temp_saved_successfully'));
            if(@$check){
                LandingPagePayment::where('user_id',auth()->id())->where('id',$check->id)->update(['template_id'=>$landing_page->id,'is_use'=>'Y']);
            }
        }
        $jsn['template'] = $landing_page;
        $jsn['status'] = "success";
        $jsn['message'] = \Lang::get('client_site.landing_page_saved');
        return response()->json($jsn);
    }

    public function checkName(Request $request)
    {
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        $exists = LandingPageMaster::where('landing_title', strtolower(@$request->params['template_name']))->where('status', '!=', 'D')->first();
        if(@$exists){
            $jsn['status'] = "NA";
        } else {
            $jsn['status'] = "A";
        }
        return response()->json($jsn);
    }

    public function landingPage($prof_slug, $slug){
        $data['professional'] = User::where('slug', $prof_slug)->first();
        $data['detail'] = LandingPageMaster::with('getContentDetails')->where('slug', $slug)->where('status', '!=', 'D')->first();
        if($data['professional'] == null || $data['detail'] == null){
            session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
            return redirect()->route('list.landing.page.templates');
        }
        // if($data['detail']->page_status == 'B'){
        //     session()->flash('error', \Lang::get('client_site.template_is_blocked'));
        //     return redirect()->route('list.landing.page.templates');
        // }
        if($data['detail']->page_status != 'P' && Auth::id() != $data['detail']->user_id){
            session()->flash('error', \Lang::get('client_site.landing_page_not_found'));
            return redirect()->route('home');
        }
        return view('modules.landing_page.landing'.$data['detail']->landing_template_id)->with($data);
    }

    public function delete($id)
    {
        $template = LandingPageMaster::where('id', $id)->first();
        if(@$template){
            // @unlink('storage/app/public/uploads/landing_page/'.@$template->landing_logo);
            // @unlink('storage/app/public/uploads/landing_page/'.@$template->header_image);
            // @unlink('storage/app/public/uploads/landing_page/'.@$template->body_single_video_filename);
            // @unlink('storage/app/public/uploads/landing_page/'.@$template->body_single_content_image);
            // $template->delete();
            $template->status = 'D';
            $template->save();
            session()->flash('success', \Lang::get('client_site.landing_page_deleted'));
        } else {
            session()->flash('error', \Lang::get('client_site.landing_page_not_found'));
        }
        return redirect()->back();
    }

    public function updateMultiContent(Request $request)
    {
        $template = LandingPageMaster::where('id', $request->landing_page_master_id)->first();
        $contents = LandingPageContentDetail::where('landing_page_master_id', $request->landing_page_master_id)->get();

        $ins['landing_page_master_id'] = $request->landing_page_master_id;
        $ins['content_heading'] = $request->content_heading;
        $ins['content_desc'] = $request->content_desc;
        $ins['content_button'] = $request->content_button;
        $ins['content_button_text'] = $request->content_button_text;
        $ins['content_button_link'] = $request->content_button_link;
        $ins['image_style'] = $request->image_style;

        if(@$request->hasFile('content_file')){
            $file = @$request->content_file;
            $file_name = time().'-'.rand(1000,9999).'.'.@$file->getClientOriginalExtension();
            Storage::putFileAs('public/uploads/landing_page/', $file, $file_name);
            $mime = $file->getMimeType();
            $ins['content_file_type'] = "I";
            $ins['content_file'] = $file_name;
        }
        if(@$request->content_file_youtube_link!==null &&@$request->content_file_youtube_link!='null'){
            $url = @$request->content_file_youtube_link;
            parse_str(parse_url($url, PHP_URL_QUERY), $my_array_of_vars);
            $url_id = @$my_array_of_vars['v'];
            if ($url_id == null) {
                $pieces = explode("/", $url);
                $url_id = end($pieces);
            }
            $ins['content_file_type'] = "Y";
            $ins['content_file'] = $url_id;
        }
        $created = LandingPageContentDetail::create($ins);

        $template->body_single_content = 'N';
        $template->save();

        $jsn['request'] = $request->all();
        $jsn['content_detail'] = $created;
        $jsn['status'] = "success";
        $jsn['message'] = "Added succesfully";
        return response()->json($jsn);
    }

    public function deleteAllMultiContent($tid){
        $contents = LandingPageContentDetail::where('landing_page_master_id', $tid)->get();
        if(count($contents) > 0){
            foreach($contents as $cntnt) {
                $cntnt->delete();
            }
        }
        $jsn['status'] = "success";
        $jsn['message'] = "Deleted all succesfully";
        return response()->json($jsn);
    }

    public function deleteMultiContent($tid, $num){
        $contents = LandingPageContentDetail::where('landing_page_master_id', $tid)->get();
        if(count($contents) > 0){
            foreach($contents as $i=>$cntnt) {
                if($i == $num) $cntnt->delete();
            }
            $jsn['status'] = "success";
            $jsn['message'] = "Deleted succesfully";
        } else {
            $jsn['status'] = "error";
            $jsn['message'] = "Template contents empty";
        }
        return response()->json($jsn);
    }

    public function storeProductDetails(Request $request)
    {
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        $product = null;
        $ins['landing_page_master_id']      = $request->landing_page_master_id;
        $ins['product_heading']             = $request->product_heading;
        $ins['product_desc']                = $request->product_desc;
        $ins['product_price_show']          = $request->product_price_show;
        $ins['product_price']               = $request->product_price;
        $ins['product_button']              = $request->product_button;
        $ins['product_button_caption']      = $request->product_button_caption;
        $ins['product_button_url']          = $request->product_button_url;
        $ins['product_reverse_counter']     = $request->product_reverse_counter;
        $ins['product_exp_date']            = $request->product_exp_date;
        $ins['product_file_type']           = @$request->product_file_type;

        if(@$request->product_file_type == 'I' && @$request->hasFile('product_file')){
            $image = @$request->product_file;
            $image_name = time().'-'.rand(1000,9999).'.'.@$image->getClientOriginalExtension();
            Storage::putFileAs('public/uploads/landing_page/', $image, $image_name);
            $ins['product_file'] = $image_name;
        }
        if(@$request->product_file_type == 'Y' && @$request->product_youtube){
            $ins['product_file'] = @$request->product_youtube;
        }
        if(@$request->product_file_type == 'N'){
            $ins['product_file'] = null;
        }

        if(@$request->product_id){
            $product = LandingPageProductDetail::where('id', $request->product_id)->first();
            if(@$request->hasFile('product_file') || @$request->product_file_type != 'I') @unlink('storage/app/public/uploads/landing_page/'.@$product->product_file);
            $product->update($ins);
        } else {
            $product = LandingPageProductDetail::create($ins);
        }

        $jsn['serial'] = $request->serial;
        $jsn['product'] = $product;
        $jsn['status'] = "success";
        $jsn['message'] = "Product Details Saved Successfully";
        return response()->json($jsn);
    }

    public function deleteProductDetails($id)
    {
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        $product = LandingPageProductDetail::where('id', $id)->first();
        if(@$product){
            @unlink('storage/app/public/uploads/landing_page/'.@$product->product_file);
            $product->delete();
            $jsn['status'] = "success";
            $jsn['message'] = \Lang::get('client_site.product_deleted_succesfully');
        } else {
            $jsn['status'] = "error";
            $jsn['message'] = \Lang::get('client_site.product_not_found');
        }
        return response()->json($jsn);
    }

    public function deleteProductImage($id)
    {
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        $product = LandingPageProductDetail::where('id', $id)->first();
        if(@$product){
            @unlink('storage/app/public/uploads/landing_page/'.@$product->product_file);
            $product->update(['product_file' => null]);
            $jsn['product'] = $product;
            $jsn['status'] = "success";
            $jsn['message'] = \Lang::get('client_site.image_deleted_successfully');
        } else {
            $jsn['status'] = "error";
            $jsn['message'] = \Lang::get('client_site.something_went_be_wrong');
        }
        return response()->json($jsn);
    }

    public function saveLead(Request $request)
    {
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        $ins['fname'] = @$request->fname;
        $ins['lname'] = @$request->lname;
        $ins['email'] = @$request->email;
        $ins['country_code'] = @$request->country_code;
        $ins['mobile'] = @$request->mobile;
        $ins['address'] = @$request->address;
        $ins['city'] = @$request->city;
        $ins['state'] = @$request->state;
        $ins['postal_code'] = @$request->postal_code;
        $ins['website'] = @$request->website;
        $ins['message'] = @$request->message;
        $ins['facebook_link'] = @$request->facebook_link;
        $ins['instagram_link'] = @$request->instagram_link;
        $ins['linkedin_link'] = @$request->linkedin_link;
        $ins['landing_page_master_id'] = @$request->landing_page_master_id;

        $lead = LandingPageLead::create($ins);

        if(@$lead){
            $jsn['request'] = $request->all();
            $jsn['details'] = $lead;
            $jsn['status'] = "success";
            $jsn['message'] = "Lead saved succesfully";
        } else {
            $jsn['status'] = "error";
            $jsn['message'] = \Lang::get('client_site.something_went_be_wrong');
        }
        return response()->json($jsn);
    }

    public function saveWhyUs(Request $request){
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        $details = null;
        $ins['title'] = @$request->title;
        $ins['description'] = @$request->description;
        $ins['landing_page_master_id'] = @$request->landing_page_master_id;

        if(@$request->hasFile('file')){
            $image = @$request->file;
            $image_name = time().'-'.rand(1000,9999).'.'.@$image->getClientOriginalExtension();
            Storage::putFileAs('public/uploads/landing_page/', $image, $image_name);
            $ins['filetype'] = $image->getMimeType();
            $ins['filename'] = $image_name;
        }

        if(@$request->whyus_id){
            $details = LandingPageWhyUs::where('id', @$request->whyus_id)->first();
            if(@$request->hasFile('file')) {
                @unlink('storage/app/public/uploads/landing_page/'.@$details->filename);
            }
            $details->update($ins);
        } else {
            $details = LandingPageWhyUs::create($ins);
        }

        if(@$details){
            $jsn['request'] = $request->all();
            $jsn['details'] = $details;
            $jsn['status'] = "success";
            $jsn['message'] = \Lang::get('client_site.saved_successfully');
        } else {
            $jsn['status'] = "error";
            $jsn['message'] = \Lang::get('client_site.something_went_be_wrong');
        }
        return response()->json($jsn);
    }

    public function deleteWhyUs($id){
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        $detail = LandingPageWhyUs::where('id', $id)->first();
        if(@$detail){
            $detail->delete();
            $jsn['status'] = "success";
            $jsn['message'] = \Lang::get('client_site.section_deleted_successfully');
        } else {
            $jsn['status'] = "error";
            $jsn['message'] = \Lang::get('client_site.something_went_be_wrong');
        }
        return response()->json($jsn);
    }

    public function saveFaq(Request $request){
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        $ins['title'] = @$request->title;
        $ins['description'] = @$request->description;
        $ins['landing_page_master_id'] = @$request->landing_page_master_id;

        if(@$request->faq_id){
            $details = LandingPageFaq::where('id', @$request->faq_id)->first();
            $details->update($ins);
        } else {
            $details = LandingPageFaq::create($ins);
        }

        if(@$details){
            $jsn['request'] = $request->all();
            $jsn['details'] = $details;
            $jsn['status'] = "success";
            $jsn['message'] = \Lang::get('client_site.saved_successfully');
        } else {
            $jsn['status'] = "error";
            $jsn['message'] = \Lang::get('client_site.something_went_be_wrong');
        }
        return response()->json($jsn);
    }

    public function deleteFaq($id){
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        $detail = LandingPageFaq::where('id', $id)->first();
        if(@$detail){
            $detail->delete();
            $jsn['status'] = "success";
            $jsn['message'] = \Lang::get('client_site.faq_deleted_successfully');
        } else {
            $jsn['status'] = "error";
            $jsn['message'] = \Lang::get('client_site.something_went_be_wrong');
        }
        return response()->json($jsn);
    }

    public function resetLandingLogo($id){
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        $landing_page = LandingPageMaster::where('id', $id)->first();
        if(@$landing_page){
            @unlink('storage/app/public/uploads/landing_page/'.@$landing_page->landing_logo);
            @unlink('storage/app/public/uploads/landing_page/'.@$landing_page->landing_logo_thumbnail);

            $landing_page->landing_logo = null;
            $landing_page->landing_logo_thumbnail = null;
            $landing_page->save();
            $jsn['details'] = $landing_page;
            $jsn['status'] = "success";
            $jsn['message'] = \Lang::get('client_site.logo_reset_succesfully');
        } else {
            $jsn['status'] = "error";
            $jsn['message'] = \Lang::get('client_site.something_went_be_wrong');
        }
        return response()->json($jsn);
    }


    /**
     * method:   viewLeads
     *created By: Soumojit
     *Description:  view Leads page wish
     *Date: 17-SEP-2021
     */
    public function viewLeads($id=null){

        $data['landing'] = LandingPageMaster::where('id', $id)->where('user_id',auth()->user()->id)->first();
        if($data['landing']==null){
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->route('list.landing.page.templates');
        }
        $data['leads']=LandingPageLead::where('landing_page_master_id',$id)->orderBy('id','desc')->get();

        return view('modules.landing_page.view_leads')->with($data);
    }

    /**
     * method:   viewLeadsExport
     *created By: Soumojit
     *Description:  Export leads
     *Date: 17-SEP-2021
     */
    public function viewLeadsExport($id){
        $data['landing'] = LandingPageMaster::where('id', $id)->where('user_id',auth()->user()->id)->first();
        if($data['landing']==null){
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->route('list.landing.page.templates');
        }
        $data['leads']=LandingPageLead::where('landing_page_master_id',$id)->orderBy('id','desc')->get();

        $data1 = '';
        $data1 .='<table><tr>';
        if($data['landing']->lead_fname=='Y'){
            $data1 .='<th style="border:1px solid black;background-color:#1781d2">First Name</th>';
        }
        if($data['landing']->lead_lname=='Y'){
            $data1 .='<th style="border:1px solid black;background-color:#1781d2">Last Name</th>';
        }
        if($data['landing']->lead_email=='Y'){
            $data1 .='<th style="border:1px solid black;background-color:#1781d2">Email</th>';
        }
        if($data['landing']->lead_mobile=='Y'){
            $data1 .='<th style="border:1px solid black;background-color:#1781d2">Mobile</th>';
        }
        if($data['landing']->lead_message=='Y'){
            $data1 .='<th style="border:1px solid black;background-color:#1781d2">Message</th>';
        }
        if($data['landing']->lead_address=='Y'){
            $data1 .='<th style="border:1px solid black;background-color:#1781d2">Address</th>';
        }
        if($data['landing']->lead_country_code=='Y'){
            $data1 .='<th style="border:1px solid black;background-color:#1781d2">Country Code</th>';
        }
        if($data['landing']->lead_city=='Y'){
            $data1 .='<th style="border:1px solid black;background-color:#1781d2">City</th>';
        }
        if($data['landing']->lead_state=='Y'){
            $data1 .='<th style="border:1px solid black;background-color:#1781d2">State</th>';
        }
        if($data['landing']->lead_postal_code=='Y'){
            $data1 .='<th style="border:1px solid black;background-color:#1781d2">Postal Code</th>';
        }
        if($data['landing']->lead_website=='Y'){
            $data1 .='<th style="border:1px solid black;background-color:#1781d2">Website</th>';
        }
        if($data['landing']->lead_facebook=='Y'){
            $data1 .='<th style="border:1px solid black;background-color:#1781d2">Facebook Link</th>';
        }
        if($data['landing']->lead_instagram=='Y'){
            $data1 .='<th style="border:1px solid black;background-color:#1781d2">Instagram Link</th>';
        }
        if($data['landing']->lead_linkedin=='Y'){
            $data1 .='<th style="border:1px solid black;background-color:#1781d2">Linkedin Link</th>';
        }
        $data1 .='</tr>';
        foreach (@$data['leads'] as $value) {
            $data1 .= '<tr>';
            if($data['landing']->lead_fname=='Y'){
                $data1 .=' <td style="border:1px solid black;">'.@$value->fname??'-'.'</td>';
            }
            if($data['landing']->lead_lname=='Y'){
                $data1 .=' <td style="border:1px solid black;">'.@$value->lname??'-'.'</td>';
            }
            if($data['landing']->lead_email=='Y'){
                $data1 .=' <td style="border:1px solid black;">'.@$value->email??'-'.'</td>';
            }
            if($data['landing']->lead_mobile=='Y'){
                $data1 .=' <td style="border:1px solid black;">'.@$value->mobile??'-'.'</td>';
            }
            if($data['landing']->lead_message=='Y'){
                $data1 .=' <td style="border:1px solid black;">'.@$value->message??'-'.'</td>';
            }
            if($data['landing']->lead_address=='Y'){
                $data1 .=' <td style="border:1px solid black;">'.@$value->address??'-'.'</td>';
            }
            if($data['landing']->lead_country_code=='Y'){
                $data1 .=' <td style="border:1px solid black;">'.@$value->country_code??'-'.'</td>';
            }
            if($data['landing']->lead_city=='Y'){
                $data1 .=' <td style="border:1px solid black;">'.@$value->city??'-'.'</td>';
            }
            if($data['landing']->lead_state=='Y'){
                $data1 .=' <td style="border:1px solid black;">'.@$value->state??'-'.'</td>';
            }
            if($data['landing']->lead_postal_code=='Y'){
                $data1 .=' <td style="border:1px solid black;">'.@$value->postal_code??'-'.'</td>';
            }
            if($data['landing']->lead_website =='Y'){
                $data1 .=' <td style="border:1px solid black;">'.@$value->website??'-'.'</td>';
            }
            if($data['landing']->lead_facebook =='Y'){
                $data1 .=' <td style="border:1px solid black;">'.@$value->facebook_link??'-'.'</td>';
            }
            if($data['landing']->lead_instagram =='Y'){
                $data1 .=' <td style="border:1px solid black;">'.@$value->instagram_link??'-'.'</td>';
            }
            if($data['landing']->lead_linkedin =='Y'){
                $data1 .=' <td style="border:1px solid black;">'.@$value->linkedin_link??'-'.'</td>';
            }
            $data1 .= '</tr>';
        }
        $data1 .= '</table>';
        header("Content-Type: application/xls");
        header("Content-Disposition: attachment; filename=".$data['landing']->landing_title.".xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo $data1;
    }

    /**
     * method:   viewLeads
     *created By: Soumojit
     *Description:  view Leads page wish
     *Date: 17-SEP-2021
     */
    public function viewPayments(Request $request){

        $data['payments'] =LandingPagePayment::where('user_id',auth()->id())->with(['landingDetails','userDetails'])->where('payment_status','P')->orderBy('id','desc')->get();
        $data['templates'] =[];
        return view('modules.landing_page.landing_payment_view')->with($data);
    }

    public function publishPage($id){
        $landing = LandingPageMaster::where('id', $id)->first();
        if(Auth::id() != $landing->user_id){
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->route('list.landing.page.templates');
        }
        $landing->page_status = 'P';
        $landing->save();
        session()->flash('success', \Lang::get('client_site.landing_publish_succesfully'));
        return redirect()->route('list.landing.page.templates');
    }

    public function unpublishPage($id){
        $landing = LandingPageMaster::where('id', $id)->first();
        if(Auth::id() != $landing->user_id){
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->route('list.landing.page.templates');
        }
        $landing->page_status = 'UP';
        $landing->save();
        session()->flash('success', \Lang::get('client_site.landing_unpublish_succesfully'));
        return redirect()->route('list.landing.page.templates');
    }

    /**
     * Create a thumbnail of specified size
     *
     * @param string $path path of thumbnail
     * @param int $width
     * @param int $height
     */
    public function createThumbnail($path, $width, $height)
    {
        $img = Image::make($path)->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save($path);
    }
}
