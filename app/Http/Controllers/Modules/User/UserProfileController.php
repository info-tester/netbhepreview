<?php

namespace App\Http\Controllers\Modules\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use validate;
// use Storage;
use App\User;
use App\Models\Timezone;
use App\Models\UserToCategory;
use App\Models\Category;
use App\Models\Favourite;
use App\Models\Booking;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductOrder;
use App\Models\ProductToFile;
use App\Models\OrderFile;
use App\Models\UserAvailability;
use App\Models\Review;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\Chapter;
use App\Models\Lesson;
use App\Models\UserToTools;
use App\Models\CourseProgress;
use App\Models\FormMaster;
use App\Models\ProductOrderDetails;
use App\Models\ProductToCertificate;
use App\Models\CertificateTemplateMaster;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use PDF;
use App\Models\Wishlist;
use App\Models\Cart;
class UserProfileController extends Controller
{
    protected $CPF_CNPJ, $numeral, $tipo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *method: index
     *created By: Abhisek
     *description: For user Edit-profile
     */

    public function index(Request $request)
    {
        if (Auth::user()->is_professional == "N") {
            if ($request->all()) {
                $userdata = User::where('id', Auth::id())->first();
                if ($request->creditcard == "Y") {
                    $creditcard = "N";
                } else {
                    $creditcard = $userdata->is_credit_card_added;
                }
                if ($request->old_password) {

                    $request->validate([
                        'new_password'      =>  'required',
                        'old_password'      =>  'required',
                        'confirm_password'  =>  'required'
                    ]);
                    if (\Hash::check($request->old_password, Auth::user()->password)) {
                        $data = [
                            'password'  =>  \Hash::make($request->new_password)
                        ];
                        $user = User::where('id', Auth::id())->update($data);
                        session()->flash('success', \Lang::get('client_site.password_successfully_updated'));
                        return redirect()->back();
                    } else {
                        session()->flash('error', \Lang::get('client_site.wrong_password'));
                        return redirect()->back()->withInput($request->input());
                    }
                } else {
                    if (Auth::guard('web')->user()->is_professional != "Y") {
                        $request->validate(
                            [
                                'first_name'        =>  "nullable",
                                'last_name'         =>  "nullable",
                                'phone_no'          => 'nullable|unique:users,mobile,' . Auth::id(),
                                'gender'            =>  "nullable",
                                //'description'       =>  "nullable",
                                'time_zone'         =>  "nullable",
                                'country'           =>  "nullable",
                                'state'             =>  "nullable",
                                'city'              =>  "nullable",
                                'dob'               =>  "nullable",
                                'street_number'     =>  "nullable",
                                'street_name'       =>  "nullable",
                                'complement'        =>  "nullable",
                                'area_code'         =>  "nullable",
                                'district'          =>  "nullable",
                                'zipcode'           =>  "nullable",
                                'cpf_no'            =>  "nullable",
                            ],
                            [
                                'phone_no.digits' => 'Por favor insira um número de telefone válido.',
                                'phone_no.unique' => 'Este número de telefone já está registrado.',
                                // 'cpf_no.unique' => 'O número do CPF já está registrado.',
                            ]
                        );
                        if ($request->cpf_no && User::where('cpf_no', $request->cpf_no)->where('id', '!=', auth()->id())->exists()) {
                            session()->flash('error', 'O número do CPF já está registrado.');
                            return redirect()->back()->withInput($request->input());
                        }
                        if (@$request->cpf_no) {
                            $this->CPF_CNPJ = $request->cpf_no;
                            if (!$this->getNumeral()) {
                                session()->flash('error', \Lang::get('client_site.invalid_cpf_number'));
                                return redirect()->back()->withInput($request->input());
                            }
                        }
                        $data = [
                            'name'              =>  @$request->first_name . ' ' . @$request->last_name,
                            'mobile'            =>  @$request->phone_no,
                            'timezone_id'       =>  @$request->time_zone,
                            'description'       =>  @$request->description,
                            'country_id'        =>  @$request->country,
                            'state_id'          =>  @$request->state,
                            'gender'            =>  @$request->gender,
                            'city'              =>  @$request->city,
                            'dob'               =>  @$request->dob ? date('Y-m-d', strtotime($request->dob)) : NULL,
                            'street_number'     =>  @$request->street_number,
                            'street_name'       =>  @$request->street_name,
                            'complement'        =>  @$request->complement,
                            'area_code'         =>  @$request->area_code,
                            'district'          =>  @$request->district,
                            'zipcode'           =>  @$request->zipcode,
                            'cpf_no'            =>  @$request->cpf_no,
                            'is_credit_card_added'  =>  $creditcard,
                        ];
                        if(@$request->country){
                            $cntry = Country::where('id',@$request->country)->first();
                            $data['country_code'] = $cntry->phonecode;
                        }
                        $user = User::where('id', Auth::id())->update($data);
                        if (auth()->user()->timezone_id != 0) {
                            $userTimezoneDB = Timezone::where('timezone_id', auth()->user()->timezone_id)->first();
                            session(['timezone' => $userTimezoneDB->timezone ?? env('TIMEZONE')]);
                        }
                        $ext = [
                            0   =>  'png',
                            1   =>  'jpeg',
                            2   =>  'jpg',
                            3   =>  'gif',
                            4   =>  'png'
                        ];
                        if (@$request->profile_pic) {
                            if (!in_array($request->profile_pic->getClientOriginalExtension(), $ext)) {
                                session()->flash('error', \Lang::get('client_site.please_upload_an_image'));
                                return redirect()->back()->withInput($request->input());
                            }
                            list($width, $height) = getimagesize($request->profile_pic);
                            $name = time() . '_' . $request->profile_pic->getClientOriginalName();
                            $path = "storage/app/public/uploads/profile_pic/";

                            $request->profile_pic->move($path, $name);
                            $image = Image::make($path . $name);
                            if ($width / $height >= 36 / 23) {
                                $image->resize(($width / $height) * 230, 230);
                            } else {
                                $image->resize(360, ($height / $width) * 360);
                            }
                            $image->crop(360, 230);
                            $image->save($path . $name);

                            if (@Auth::user()->profile_pic) {
                                unlink('storage/app/public/uploads/profile_pic/' . @Auth::user()->profile_pic);
                            }
                            User::where('id', Auth::id())->update([
                                'profile_pic'   =>  @$name
                            ]);
                        }
                        session()->flash('success', \Lang::get('client_site.profile_successfully_updated'));
                        return redirect()->back();
                    } else {
                        $request->validate([
                            'first_name'        =>  "required",
                            'last_name'         =>  "required",
                            'phone_no'          => 'required|digits:8|unique:users,mobile,' . Auth::id(),
                            'gender'            =>  "required",
                            'description'       =>  "required"
                        ]);

                        $data = [
                            'name'              =>  $request->first_name . ' ' . $request->last_name,
                            'mobile'            =>  $request->phone_no,
                            'gender'            =>  $request->gender,
                            'description'       =>  $request->description
                        ];
                        $user = User::where('id', Auth::id())->update($data);
                        $ext = [
                            0   =>  'png',
                            1   =>  'jpeg',
                            2   =>  'jpg',
                            3   =>  'gif',
                            4   =>  'png'
                        ];
                        if (@$request->profile_pic) {
                            if (!in_array($request->profile_pic->getClientOriginalExtension(), $ext)) {
                                session()->flash('error', \Lang::get('client_site.please_upload_an_image'));
                                return redirect()->back()->withInput($request->input());
                            }

                            $name = time() . '_' . $request->profile_pic->getClientOriginalName();
                            $path = "storage/app/public/uploads/profile_pic";

                            $request->profile_pic->move($path, $name);

                            // $update['profile_pic'] = $name;

                            if (@Auth::user()->profile_pic) {
                                if (file_exists($path . '/' . @Auth::user()->profile_pic)) {
                                    unlink('storage/app/public/uploads/profile_pic/' . @Auth::user()->profile_pic);
                                }
                            }
                            User::where('id', Auth::id())->update([
                                'profile_pic'   =>  @$name
                            ]);
                        }




                        session()->flash('success', \Lang::get('client_site.profile_successfully_updated'));
                        return redirect()->back();
                    }
                }
            }
            $time = Timezone::get();
            $country = Country::orderBy('name')->get();
            $state = State::orderBy('name')->get();
            $city = City::orderBy('name')->get();
            return view('modules.user.user_edit_profile')->with([
                'time'      =>  @$time,
                'country'   =>  @$country,
                'state'     =>  @$state,
                'city'      =>  @$city
            ]);
        } else {

            return redirect()->route('professional.profile');
        }
    }

    /**
     *method:isValidCPF()
     *purpose:For Checking CPF number is valid or not
     *Author:@bhisek
     */
    public function isValidCPF($cpf)
    {
        $cpf = preg_replace("/[^0-9]/", "", $cpf);
        $digitoUm = 0;
        $digitoDois = 0;
        for ($i = 0, $x = 10; $i <= 8; $i++, $x--) {
            $digitoUm += $cpf[$i] * $x;
        }
        for ($i = 0, $x = 11; $i <= 9; $i++, $x--) {
            if (str_repeat($i, 11) == $cpf) {
                return false;
            }

            $digitoDois += $cpf[$i] * $x;
        }
        $calculoUm  = (($digitoUm % 11) < 2) ? 0 : 11 - ($digitoUm % 11);
        $calculoDois = (($digitoDois % 11) < 2) ? 0 : 11 - ($digitoDois % 11);
        if ($calculoUm <> $cpf[9] || $calculoDois <> $cpf[10]) {
            return false;
        } else {
            return true;
        }
    }


    /**
     *method: load_professional_dashboard
     *created By: Abhisek
     *description: For loading user Dashboard
     */

    public function getState(Request $request)
    {
        $response = [
            'jsonrpc'   =>  '2.0'
        ];
        if ($request->params['cn']) {
            $state = State::where('country_id', @$request->params['cn'])
                ->orderBy('name')
                ->get();
            $response['result'] = @$state;
            $response['status'] = 1;
        }

        return response()->json($response);
    }



    /**
     *method: load_professional_dashboard
     *created By: Abhisek
     *description: For loading user Dashboard
     */

    public function load_user_dashboard()
    {


        $formdata = UserToTools::with(['getUserData','getFormDetails','getContractTemplatesdata','getContentTemplatesdata','getTool360MasterDetails','getSmartGoalsData','getImportingToolsdata'])->where('user_id', Auth::id())->where('status', 'INVITED')->orderBy('id', 'DESC')->get();

        //$form_master_data = FormMaster::with('category')->where('id',$formdata->tool_id)->first();
        // if(Auth::user()->is_professional=="Y"){
        //     return redirect()->route("prof.dash.my.booking",['type'=>"UC"]);
        // }
        // else{
        $booking = Booking::with('profDetails', 'parentCatDetails', 'childCatDetails', 'getReview')->where([
            'user_id' => Auth::id(),
            'order_status'  => 'A'
        ])->orderBy('date', 'DESC')->get();
        return view('modules.user.user_dashboard')->with([
            'booking'            =>  @$booking,
            'formdata'           => @$formdata

        ]);
        // }
    }

    /**
     *method: becameProfessional
     *created By: Abhisek
     *description: For making an user to an Professional
     */

    public function becameProfessional(Request $request)
    {

        if (Auth::user()->is_professional == "Y") {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }

        if (Auth::user()->is_professional == "Y") {
            if (!session()->has('red1')) {
                session()->flash('error', \Lang::get('client_site.your_request_is_under_admin_approval'));
            }

            return view('modules.user.user_professional');
        } else {
            if ($request->all()) {
                // dd($request->all());
                $request->validate([
                    'sell'      =>  'required',
                ]);
                if(@$request->category && $request->sell != 'PS'){
                    if (sizeof($request->category) > 0) {
                        for ($i = 0; $i < sizeof($request->category); $i++) {
                            if (sizeof($request->category) > 0) {

                                if ($request->category[$i] != null || $request->category[$i] != "") {

                                    $findCat = UserToCategory::where([
                                        'category_id'   =>  @$request->category[$i],
                                        'user_id'       =>  @Auth::id()
                                    ])->first();

                                    if ($findCat == null) {
                                        $userToCat = UserToCategory::create([
                                            'user_id'       =>  Auth::id(),
                                            'category_id'   =>  $request->category[$i],
                                            'level'         =>  0
                                        ]);
                                    }
                                }
                            }
                        }
                    }
                    if (sizeof($request->sub_category) > 0) {
                        for ($i = 0; $i < sizeof($request->sub_category); $i++) {
                            if (sizeof($request->sub_category) > 0) {
                                if ($request->sub_category[$i] != null || $request->sub_category[$i] != "") {
                                    $findCat1 = UserToCategory::where([
                                        'category_id'   =>  @$request->sub_category[$i],
                                        'user_id'       =>  @Auth::id()
                                    ])->first();

                                    if ($findCat1 == null) {
                                        $parent_id = Category::where('id', @$request->sub_category[@$i])->first();

                                        $userToSubCat = UserToCategory::create([
                                            'user_id'           =>  Auth::id(),
                                            'category_id'       =>  $request->sub_category[$i],
                                            'level'             =>  1,
                                            'parent_id'         =>  @$parent_id->parent_id
                                        ]);
                                    }
                                }
                            }
                        }
                    }
                }
                $user = User::where([
                    'id'    =>  @Auth::id()
                ])->update([
                    'sell'                              => $request->sell,
                    'is_professional'                   => 'Y',
                    'is_approved'                       => 'N',
                    'initiate_professionalism'          => 'Y',
                    'status'                            => "AA"
                ]);
                session()->flash('success', \Lang::get('client_site.your_request_to_be_a_professional'));
                session()->flash('red1', '1');
                //return redirect()->back(); //change 13.4.20
                return redirect()->route('professional.profile');
            }
        }
        $cat = Category::where('parent_id', 0)->get();
        return view('modules.user.user_professional')->with([
            'cat'   =>  @$cat
        ]);
    }

    /**
     * For fetching sub-category.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function getSubcat(Request $request)
    {
        $response = [
            'jsonrpc'   =>  '2.0'
        ];
        if ($request->params['cat']) {
            $cat = Category::where('parent_id', $request->params['cat'])->where('status', 'A')->orderBy('name')->get();
            if ($cat != null) {
                $response['status'] = 1;
                $response['result'] = $cat;
            } else {
                $response['status'] = 0;
            }
        }
        return response()->json($response);
    }

    /**
     * For fetching favourite list.
     *
     * @param  array  $data
     * @return \App\User
     */

    public function favouriteList()
    {
        $fav = Favourite::with('userDetails')
            ->where('user_id', @Auth::id())
            ->get();
        return view('modules.user.user_favourite')->with([
            'fav'   =>  @$fav
        ]);
    }

    /**
     * For removing favourite list.
     *
     * @param  array  $data
     * @return \App\User
     */


    public function removeFav($id)
    {
        $fav = Favourite::with('userDetails')->where('professional_id', $id)
            ->first();
        if ($fav == null) {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        }
        session()->flash('success', \Lang::get('client_site.successfully_removed') . @$fav->userDetails->name . \Lang::get('client_site.from_your_favourite_list'));
        $fav->delete();
        return redirect()->back();
    }

    public function bookingRequest(Request $request, $type = null)
    {
        $key = [];
        $booking = Booking::where('id', '!=', 0);
        if (@$type) {
            $url = \Request::segment(2);
            if ($url == 'BR') {
                session()->flash('error', \Lang::get('client_site.unauthorize_access'));
                return redirect()->back();

                $booking = Booking::with('userDetails', 'profDetails', 'parentCatDetails', 'getReview')
                    ->where([
                        'user_id'       => Auth::id()
                    ]);
                $key['type'] = 'BR';
            }
            if ($url == 'UC') {
                $booking = Booking::with('userDetails', 'profDetails', 'parentCatDetails', 'getReview')
                    ->where([
                        'user_id'       => Auth::id(),
                    ])
                    ->whereIn('order_status',['A','C']);
                $key['type'] = 'UC';
            }
            if ($url == 'PC') {

                $booking = Booking::with('userDetails', 'profDetails', 'parentCatDetails', 'getReview')
                    ->where([
                        'user_id'       => Auth::id(),
                        'video_status'  => 'C'
                    ]);
                $key['type'] = 'PC';
            }
        }
        if ($request->all()) {
            if ($request->type == 'BR') {
                session()->flash('error', \Lang::get('client_site.unauthorize_access'));
                return redirect()->back();
                $booking = Booking::with('userDetails', 'profDetails', 'parentCatDetails', 'getReview')
                    ->where([
                        'user_id'       => Auth::id()
                    ]);
            }
            if ($request->type == 'UC') {
                $booking = Booking::with('userDetails', 'profDetails', 'parentCatDetails', 'getReview')
                    ->where([
                        'user_id'       => Auth::id(),
                    ])
                    ->whereIn('order_status',['A','C']);
            }
            if ($request->type == 'PC') {
                $booking = Booking::with('userDetails', 'profDetails', 'parentCatDetails', 'getReview')
                    ->where([
                        'user_id'       => Auth::id(),
                        'order_status'  => 'P'
                    ]);
            }
            if ($request->from_date || $request->to_date) {
                $from = $request->from_date ? $request->from_date : date('d-m-Y');
                $to = $request->to_date ? $request->to_date : date('d-m-Y');
                $booking = $booking->whereBetween('date', array($from, $to));
            }
            if ($request->status) {
                $booking = $booking->where('order_status', $request->status);
            }
            @$key = $request->all();
        }

        $booking = $booking->orderBy('id', 'desc')->get();
        return view('modules.user.user_booking_request')->with([
            'booking'   =>  @$booking,
            'key'       =>  @$key
        ]);
    }

    public function cancelBookingRequest($token)
    {

        $booking = Booking::where('token_no', $token)->first();
        if ($booking == null) {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        } else {
            $booking->order_status = 'R';
            $booking->order_cancelled_by = 'U';
            $booking->save();
            session()->flash('success', \Lang::get('client_site.booking_cancelled_successfully'));
            return redirect()->back();
        }
    }

    public function rescheduleBookingRequest(Request $request)
    {
        $booking = Booking::where('token_no', $request->token_no)->first();
        if ($booking == null) {
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->back();
        } else {
            $is_avlbl = UserAvailability::where([
                'user_id'       => @$booking->professional_id,
                'date'          => date('Y-m-d', strtotime($request->date)),
            ])->first();
            if ($is_avlbl == null) {
                session()->flash('error', \Lang::get('client_site.time_slot_currently_not_available_for_rescheduling'));
                return redirect()->back();
            } else {
                $times = explode('-', $request->time);
                $booking->date = date('Y-m-d', strtotime($request->date));
                $booking->start_time = toUTCTime($times[0] . ':00', 'H:i:s');
                $booking->end_time = toUTCTime($times[1] . ':00', 'H:i:s');
                $booking->reschedule_status     = 'A';
                $booking->reschedule_by_whom    = 'U';
                $booking->save();
                session()->flash('success', \Lang::get('client_site.booking_rescheduled_successfully'));
                return redirect()->back();
            }
        }
    }

    public function rescheduledTimeList(Request $request)
    {
        if ($request->date) {
            $list = UserAvailability::where('user_id', $request->user_id)
                ->where('slot_start', '!=', date('Y-m-d', strtotime($request->date)) . ' ' . $request->start)
                ->where('slot_start', '>=', date('Y-m-d', strtotime('-1 day ' . $request->date)))
                ->where('slot_start', '<=', date('Y-m-d', strtotime('+1 day ' . $request->date)))
                ->where('slot_start', '>', toUTCTime(date('Y-m-d H:i:s')))
                ->whereNotIn('slot_start', getUsedTimeSlotsForDate($request->user_id, date('Y-m-d', strtotime($request->date))))->get();
            $listArr = [];
            foreach (getUserTimeSlotsForDate($request->date, $list) as $a) {
                $avl = [];
                foreach ($a as $k => $v) {
                    if ($k == 'slot_start') {
                        $avl[$k] = toUserTime($v, 'H:i:s');
                    } else if ($k == 'slot_end') {
                        $avl[$k] = toUserTime($v, 'H:i:s');
                    } else {
                        $avl[$k] = $v;
                    }
                }
                array_push($listArr, $avl);
            }
            return response()->json($listArr);
        }
    }

    public function upcommingList(Request $request)
    {
        $upcommingList = Booking::with('userDetails', 'profDetails', 'parentCatDetails')
            ->where([
                'order_status'  => 'A',
                'user_id'       => Auth::id()
            ]);
        if (@$request->all()) {
            if (@$request->professional) {
                $name = $request->professional;
                $upcommingList = $upcommingList->whereHas('profDetails', function ($query) use ($name) {
                    $query->where('name', 'LIKE', '%' . $name . '%');
                });
            }
            if (@$request->from_date) {
                $upcommingList = $upcommingList->where('date', '>=', $request->from_date);
            }
            if (@$request->to_date) {
                $upcommingList = $upcommingList->where('date', '<=', $request->to_date);
            }
            $key = $request->all();
        }
        $upcommingList = $upcommingList->get();
        return view('modules.user.user_upcomming_classes')->with([
            'upcommingList' => $upcommingList,
            'key'           => @$key
        ]);
    }

    /*
    * Method      : review
    * Description : It is used to show review form
    * Author      : Soumen
    * Date        : 02-AUG-2019
    */
    public function review($booking_id, $professional_id)
    {
        $no_of_review = Review::where([
            'booking_id' => $booking_id,
            'user_id'    => Auth::id()
        ])->first();
        $book = Booking::with('profDetails')->where([
            'id' => $booking_id
        ])->first();
        if(@$book->video_status=='I'){
            Booking::where([
                'id' => $booking_id
            ])->update(['video_status' => 'C']);
        }

        return view('modules.user.user_review')->with([
            'booking_id'        => $booking_id,
            'professional_id'   => $professional_id,
            'no_of_review'      => $no_of_review,
            'book'              => $book
        ]);
    }

    public function postReview(Request $request)
    {
        Review::create([
            'user_id'           => Auth::id(),
            'professional_id'   => $request->professional_id,
            'booking_id'        => $request->booking_id,
            // 'review_heading'    => $request->review_heading,
            'comments'          => $request->review_comment,
            'points'            => $request->review_point
        ]);
        $user = User::where('id', $request->professional_id)->first();
        User::where('id', $request->professional_id)->update([
            'total_review'  => ($user->total_review ?? 0) + 1,
            'avg_review'    => (($user->avg_review ?? 0) + ($request->review_point ?? 0)) / (($user->total_review ?? 0) + 1)
        ]);
        session()->flash('success', \Lang::get('client_site.successfully_reviewed'));
        return redirect()->route('user.my.booking', ['type' => 'UC']);
    }

    public function viewBookedDetails($id)
    {
        $booked = Booking::with('getReview', 'profDetails', 'parentCatDetails', 'childCatDetails')->where('id', $id)->first();
        return view('modules.user.view_user_booked_details')->with([
            'booked' => $booked
        ]);
    }

    /*
    * Method      : pastClass
    * Description : It is used to show upcomming classes and also search
    * Author      : Soumen
    * Date        : 02-AUG-2019
    */
    public function pastClass(Request $request)
    {
        $pastClass = Booking::with('userDetails', 'profDetails', 'parentCatDetails')
            ->where([
                'order_status'  => 'C',
                'user_id'       => Auth::id()
            ]);
        if (@$request->all()) {
            if (@$request->professional) {
                $name = $request->professional;
                $pastClass = $pastClass->whereHas('profDetails', function ($query) use ($name) {
                    $query->where('name', 'LIKE', '%' . $name . '%');
                });
            }
            if (@$request->from_date) {
                $pastClass = $pastClass->where('date', '>=', $request->from_date);
            }
            if (@$request->to_date) {
                $pastClass = $pastClass->where('date', '<=', $request->to_date);
            }
            $key = $request->all();
        }
        $pastClass = $pastClass->get();
        return view('modules.user.user_past_classes')->with([
            'pastClass' => $pastClass,
            'key'           => @$key
        ]);
    }


    public function verificaDigitos($digito1, $digito2, $ver, $ver2)
    {
        $num = $digito1 % 11;
        $digito1 = ($num < 2) ? 0 : 11 - $num;
        $num2 = $digito2 % 11;
        $digito2 = ($num2 < 2) ? 0 : 11 - $num2;
        if ($digito1 === (int) $this->numeral[$ver] && $digito2 === (int) $this->numeral[$ver2]) {
            return true;
        } else {
            return false;
        }
    }
    private function verificaCPF()
    {
        $num1 = ($this->numeral[0] * 10) + ($this->numeral[1] * 9) + ($this->numeral[2] * 8) + ($this->numeral[3] * 7) + ($this->numeral[4] * 6)
            + ($this->numeral[5] * 5) + ($this->numeral[6] * 4) + ($this->numeral[7] * 3) + ($this->numeral[8] * 2);
        $num2 = ($this->numeral[0] * 11) + ($this->numeral[1] * 10) + ($this->numeral[2] * 9) + ($this->numeral[3] * 8) + ($this->numeral[4] * 7)
            + ($this->numeral[5] * 6) + ($this->numeral[6] * 5) + ($this->numeral[7] * 4) + ($this->numeral[8] * 3) + ($this->numeral[9] * 2);
        return $this->verificaDigitos($num1, $num2, 9, 10);
    }
    private function verificaCNPJ()
    {
        $num1 = ($this->numeral[0] * 5) + ($this->numeral[1] * 4) + ($this->numeral[2] * 3) + ($this->numeral[3] * 2) + ($this->numeral[4] * 9) + ($this->numeral[5] * 8)
            + ($this->numeral[6] * 7) + ($this->numeral[7] * 6) + ($this->numeral[8] * 5) + ($this->numeral[9] * 4) + ($this->numeral[10] * 3) + ($this->numeral[11] * 2);
        $num2 = ($this->numeral[0] * 6) + ($this->numeral[1] * 5) + ($this->numeral[2] * 4) + ($this->numeral[3] * 3) + ($this->numeral[4] * 2) + ($this->numeral[5] * 9)
            + ($this->numeral[6] * 8) + ($this->numeral[7] * 7) + ($this->numeral[8] * 6) + ($this->numeral[9] * 5) + ($this->numeral[10] * 4) + ($this->numeral[11] * 3) + ($this->numeral[12] * 2);
        return $this->verificaDigitos($num1, $num2, 12, 13);
    }
    private function getNumeral()
    {
        $this->numeral = preg_replace("/[^0-9]/", "", $this->CPF_CNPJ);
        $strLen = strlen($this->numeral);
        $ret = false;
        switch ($strLen) {
            case 11:
                if ($this->verificaCPF()) {
                    $this->tipo = 'CPF';
                    $ret = true;
                }
                break;
            case 14:
                if ($this->verificaCNPJ()) {
                    $this->tipo = 'CNPJ';
                    $ret = true;
                }
                break;
            default:
                $ret = false;
                break;
        }
        return $ret;
    }
    public function __toString()
    {
        return $this->CPF_CNPJ;
    }
    public function getTipo()
    {
        return $this->tipo;
    }

    /*
    * Method: myProductOrders
    * Description: for loading user Products
    * Author: Sayantani
    * Date: 15-DEC-2020
    */
    public function myProductOrders(Request $request){
        $data['orders'] = ProductOrderDetails::with('orderMaster.userDetails', 'orderMaster', 'product')
                                                ->whereHas("orderMaster", function ($query) use ($request) {
                                                    $query->whereIn('Payment_status',['P','PR','F','I'])->where('user_id', Auth::user()->id);
                                                });
        $orders = ProductOrderDetails::with('orderMaster.userDetails', 'orderMaster', 'product')
                                        ->whereHas("orderMaster", function ($query) use ($request) {
                                            $query->whereIn('Payment_status',['P','PR','F','I'])->where('user_id', Auth::user()->id);
                                        });

        if(@$request->all()){
            if(@$request->keyword) {
                $data['orders'] = $data['orders']->whereHas("product", function ($query) use ($request) {
                    $query->where("title", 'LIKE', '%' . $request->keyword . '%');
                });
            }
            if(@$request->status){
                $data['orders'] = $data['orders']->whereHas("orderMaster", function ($query) use ($request) {
                    $query->where('payment_status', $request->status);
                });
            }
            $data['key'] = $request->all();
        }

        $paidProducts = $orders->whereHas("orderMaster", function ($query) use ($request) {
                                            $query->where('payment_status', 'P');
                                        })->pluck('product_id')->toArray();

        // dd($paidProducts);
        $data['orders'] = $data['orders']->orderBy('created_at', 'desc')->get();
        foreach($data['orders'] as $i=>$order_det){
            if($order_det->orderMaster->payment_status == 'I' && in_array($order_det->product->id, $paidProducts)){
                unset($data['orders'][$i]);
            }
            $levels = $noOfComplete = 0;
            $chapters = Chapter::where('course_id', $order_det->product->id)->get();
            foreach($chapters as $chapter){
                $levels += Lesson::where('chapter_id', $chapter->id)->where('status', 'A')->count();
            }
            $noOfComplete += CourseProgress::where('user_id', Auth::id())->where('product_id', @$order_det->product->id)->where('status', 'C')->count();
            if( $levels > 0 ) $order_det->percentage = ceil(( (int)$noOfComplete / (int)$levels ) * 100) > 100 ? 100 : ceil(( (int)$noOfComplete / (int)$levels ) * 100);
            else $order_det->percentage = 0;
        }
        // dd($data['orders']->get());
        return view('modules.user.user_products')->with($data);
    }

    public function myProductOrders1(Request $request){
        $data['orders'] = ProductOrder::with('userDetails', 'orderDetails')->whereIn('Payment_status',['P','PR','F','I'])->where('user_id', Auth::user()->id);
        if(@$request->all()){

            if(@$request->keyword){
            $data['orders'] = $data['orders']->whereHas("orderDetails.product", function ($query) use ($request) {

                        $query->where("title", 'LIKE', '%' . $request->keyword . '%');
                    });

            }

            if(@$request->status){
                $data['orders'] = $data['orders']->where('payment_status', $request->status);
            }

            $data['key'] = $request->all();
        }
        $data['orders'] = $data['orders']->orderBy('created_at', 'desc')->get();

        foreach($data['orders'] as $i=>$order_master){
            $new_arr = array();
            foreach($order_master->orderDetails as $k=>$order_det){
                $arr = (object)[];
                $levels = $noOfComplete = 0;
                $arr = $order_det;
                $chapters = Chapter::where('course_id', $order_det->product->id)->get();
                foreach($chapters as $chapter){
                    $levels += Lesson::where('chapter_id', $chapter->id)->where('status', 'A')->count();
                }
                $noOfComplete += CourseProgress::where('user_id', Auth::id())->where('product_id', @$order_det->product->id)->where('status', 'C')->count();
                if( $levels > 0 ) $arr->percentage = ceil(( (int)$noOfComplete / (int)$levels ) * 100) > 100 ? 100 : ceil(( (int)$noOfComplete / (int)$levels ) * 100);
                else $arr->percentage = 0;
                $new_arr[$k] =$arr;
            }
            $order_master->orders_det = $new_arr;
        }
        // dd($order_master);
        // dd($data['orders']->get());
        return view('modules.user.user_products')->with($data);
    }

    /*
    * Method: userProductOrderDetails
    * Description: for loading user product details
    * Author: Sayantani
    * Date: 15-DEC-2020
    */
    public function userProductOrderDetails($id,$prod_id){

        // $data['order'] = ProductOrder::with('userDetails', 'orderDetails')->where('id', $id)->where('user_id', Auth::user()->id)->whereHas("orderDetails", function ($query) use($prod_id){
        //             $query->where('product_id',$prod_id);

        // })->first();
        $data['order'] = ProductOrderDetails::with('orderMaster','orderMaster.userDetails','product')->where('product_order_master_id',$id)->where('product_id',$prod_id)->first();

        // dd($data['order']);

        if($data['order']== null){
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->route('user.products');
        }
        return view('modules.user.view_user_order_details')->with($data);
    }

    /*
    * Method: userProductOrderBill
    * Description: for loading user order bill/ details
    * Author: Sayantani
    * Date: 06-JULY-2021
    */
    public function userProductOrderBill($id){
        $data['order'] = ProductOrderDetails::with('product')->where('id', $id)->first();
        return view('modules.user.view_user_order_bill')->with($data);
    }

    /*
    * Method: changePassword
    * Description: for changing password
    * Author: Sayantani
    * Date: 15-MAR-2021
    */
    public function changePassword(){
        if(Auth::user()->is_professional == "Y"){
            return view('modules.professional.change_password');
        }
        return view('modules.user.change_password');
    }

     /*
    * Method: viewCertificate
    * Description: for view certificate
    * Author: Puja
    * Date: 06-July-2021
    */

    public function viewCertificate($order_id){

        $order = ProductOrderDetails::with('orderMaster','product')->where('id',$order_id)->first();
        $assigned_certificate = ProductToCertificate::where('product_id',$order->product->id)->first();
        if(@$assigned_certificate){
            $certificate_template = $assigned_certificate->getTool;
            $certificatedesc = $assigned_certificate->getTool->content;
            $prof_data = User::where('id',$order->professional_id)->first();
            $user_data = User::where('id',Auth::user()->id)->first();
            $course = $order->product;
            $img = "";
            if(@$prof_data['signature'])
                $img = "<img src='" . url('storage/app/public/uploads/signature/' . @$prof_data['signature']) . "' height='50px'/>";
            else
                $img = @$prof_data['nick_name'] ?? @$prof_data['name'];

            $certificatedesc = str_replace('{{Nome do aluno}}', @$user_data['name'], $certificatedesc);
            $certificatedesc = str_replace('{{Nome do curso}}', @$course->title, $certificatedesc);
            $certificatedesc = str_replace('{{Assinatura}}', @$img, $certificatedesc);

            if($certificate_template->issued_on == 'Y'){
                $issued_on = "Issued On : " . date('d-m-Y');
                $issued = substr($certificatedesc, strpos($certificatedesc, "Issued On : "), 22 );
                $certificatedesc = str_replace($issued, $issued_on, $certificatedesc);
            }

            if($certificate_template->expires_on == 'Y'){
                $expires_on = "Expires On : " . date('d-m-Y', strtotime(date('Y-m-d') . " + ".$certificate_template->expires_by." year"));
                $expires = substr($certificatedesc, strpos($certificatedesc, "Expires On : "), 23 );
                $certificatedesc = str_replace($expires, $expires_on, $certificatedesc);
            }
            // dd($certificatedesc);
            return view('modules.user.view_certificate_template', [
                'template'          => $certificate_template,
                'certificatedesc'   => $certificatedesc,
                'order'             => $order
            ]);
        } else {
            session()->flash('error', \Lang::get('client_site.something_went_be_wrong'));
            return redirect()->back();
        }

    }

    /*
    * Method: downloadCertificate
    * Description: for download pdf of certificate
    * Author: Puja
    * Date: 07-July-2021
    */

    // public function downloadCertificate($order_id) {

    //     $order = ProductOrderDetails::with('orderMaster','product')->where('id',$order_id)->first();
    //     $assigned_certificate = ProductToCertificate::where('product_id',$order->product->id)->first();
    //     $certificate_template = $assigned_certificate->getTool;
    //     $certificatedesc = $assigned_certificate->getTool->content;
    //     $prof_data = User::where('id',$order->professional_id)->first();
    //     $user_data = User::where('id',Auth::user()->id)->first();
    //     $course = $order->product;
    //     $img = "";
    //     if(@$prof_data['signature'])
    //         $img = "<img src='" . url('storage/app/public/uploads/signature/' . @$prof_data['signature']) . "' height='50px'/>";
    //     else
    //         $img = @$prof_data['name'];

    //     $certificatedesc = str_replace('{{Student Name}}', @$user_data['name'], $certificatedesc);
    //     $certificatedesc = str_replace('{{Course Name}}', @$course->title, $certificatedesc);

    //         $certificatedesc = nl2br($certificatedesc);
    //         $data['certificatedesc'] = $certificatedesc;

    //         view()->share('desc',$data);
    //     $pdf = PDF::loadView('modules.user.certificate_template', $data);

    //     return $pdf->download('Certificate.pdf');
    //     // return view('modules.user.certificate_template', $data);
    // }

    public function downloadCertificate(Request $request) {
        $data['html'] = '';
        // $data['html'] = $request->html;
        $pdfPath = url('storage/app/public/certificate/');
        view()->share('desc',$data);
        $pdf = PDF::loadView('modules.user.demo', $data)
        ->setOptions(['dpi' => 110,'margin-bottom'=>0])
                        ->setPaper('a4', 'portrait')
                        ->save($pdfPath. '/' . $fileName);;
        dd($pdf);
        // $path = url('storage/app/public/certificate/');
        // $fileName =  'Certificate' . '.' . 'pdf' ;
        // $pdf->save($path . '/' . $fileName);
    // return $pdf->download($fileName);
        $file = $pdf->output();
        Storage::put('storage/app/public/pdfFilename.pdf', $file);
        // $jsn['status'] = "success";
        // $jsn['output'] = $pdf->output();
        // $jsn['message'] = "PDF saved succeesfully";
        // dd($pdf->output());
        // return response()->json($jsn);

        return $pdf->download('Certificate.pdf');
        // return view('modules.user.certificate_template', $data);
    }

    /*
    * Method: myWhishlist
    * Description: user can view wishlisted product from here
    * Author: Puja
    * Date: 13-July-2021
    */

    public function myWishlist(){

        $data['my_wishlist'] = Wishlist::with('products','products.professional','products.category','products.subCategory')->where('user_id',Auth::user()->id)->get();
        foreach($data['my_wishlist'] as $row){

            // if (@auth()->user()->id) {
            //     $allCartData = Cart::with(['product'])->where('user_id', auth()->user()->id)->where('product_id', $row->products->id)->first();

            // } else {
            //     $cart_id = Session::get('cart_session_id');
            //     $allCartData = Cart::with(['product'])->where('cart_session_id', $cart_id)->where('product_id',$row->products->id)->first();
            // }
            $allCartData = Cart::where('user_id',Auth::id())->pluck('product_id')->toArray();
            $data['cartData'] = $allCartData;



            $data['orderedProducts'] = [];
            if(Auth::check()){
                $data['orderedProducts'] = ProductOrderDetails::whereHas('orderMaster', function($q){
                    $q->where('user_id', Auth::id());
                })->pluck('product_id')->toArray();
            }
        }




        return view('modules.user.user_product_wishlist')->with($data);

    }


    public function viewInvoice($pro_id,$order_id)
    {
        $data['order'] = ProductOrderDetails::where('product_order_master_id',$order_id)->where('product_id',$pro_id)->first();

        return view('modules.user.view_invoice')->with($data);
    }

    public function changeSidebar(Request $request){
         $response = [
            'jsonrpc'   =>  '2.0'
        ];
        $user_id = $request['params']['user_id'];
        $user_type = $request['params']['user_type'];
        $menu_type_updated = User::where('id',$user_id)->update(['menu_type'=> $user_type]);
        if($menu_type_updated){
                $response['status'] = 1;
                $response['result'] = $menu_type_updated;
        }else{
                $response['status'] = 0;
        }
        return response()->json($response);
    }
}
