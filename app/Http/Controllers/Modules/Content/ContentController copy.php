<?php

namespace App\Http\Controllers\Modules\Content;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TermsOfService;
use App\Models\HelpCategory;
use App\Models\HelpArticle;
use App\Models\Content;
use App\Models\Faq;
use validate;
class ContentController extends Controller
{
    public function termsOfservices(){
    	$terms_title = Content::where('id', 3)->first();
        $terms = TermsOfService::where('parent_id', 0)->get();
        foreach($terms as $term){
            $term->subtermcount = TermsOfService::where('parent_id', $term->id)->count();
        }
        // dd($terms);
    	return view('modules.content.terms_of_services')->with([
    		'terms' 	    => @$terms,
    		'terms_title' 	=> @$terms_title
    	]);
    }

    public function subtermsOfservices($slug){
    	$terms_title = Content::where('id', 3)->first();
        $term = TermsOfService::where('slug', $slug)->where('parent_id', 0)->first();
        if(@$term){
            // $subterms = TermsOfService::where('parent_id', $term->id)->get();
            return view('modules.content.subterms_of_services')->with([
                'term' 	    => @$term,
                // 'subterms' 	    => @$subterms,
                // 'terms_title' 	=> @$terms_title
            ]);
        } else {
            return redirect()->back()->with('error', \Lang::get('client_site.terms_of_service_not_found'));
        }
    }

    public function privacyPolicy() {
        $privacyPolicy = Content::where('id', 4)->first();
        return view('modules.content.privacy_policy')->with([
            'privacyPolicy'   => @$privacyPolicy
        ]);
    }

    public function faq() {
        $faq = Content::where('id', 1)->first();
        $faqExpert = Faq::orderBy('display_order', 'ASC')->where('type', 'EXPERT')->get();
        $faqUser = Faq::orderBy('display_order', 'ASC')->where('type', 'USER')->get();
        return view('modules.content.faq')->with([
            'faq' => $faq,
            'faqExpert' => $faqExpert,
            'faqUser' => $faqUser,
        ]);
    }

    public function helpSection(Request $request, $slug=null) {
        $data['content'] = Content::where('id', 60)->first();
        $data['categories'] = HelpCategory::with('getArticles')->where('status', 'A')->get();
        if(@$request->all()){
            // $data['categories'] = $data['categories']->whereHas('getArticles', function($q) use ($request){
            //     $q = $q->where('title', 'like', '%'.$request->title.'%');
            // })->orWhere('category', 'like', '%'.$request->title.'%');
            $data['articles'] = HelpArticle::where('status', 'A')->where('title', 'like', '%'.$request->title.'%')->get();
            $data['key'] = $request->all();
        }
        if(@$slug){
            $data['article'] = HelpArticle::with('getCategory')->where('slug', @$slug)->where('status', 'A')->first();
            if(!@$data['article']){
                return redirect()->back()->with('error', \Lang::get('client_site.article_not_found'));
            }
            return view('modules.content.help_section_new')->with($data);
            // return view('modules.content.help_article_details')->with($data);
            // dd($data['article']);
        } else {
            return view('modules.content.help_section')->with($data);
        }
    }

    public function fetchArticleSearch(Request $request) {
        $response = [
            'jsonrpc'   =>  '2.0'
        ];
        if(@$request->title){
            // $response['articles'] = HelpArticle::with('getCategory')->where('status', 'A')->where('title', 'like', '%'.$request->title.'%')
            // ->whereHas('getCategory', function($q){
            //     $q = $q->where('status', 'A');
            // })->get();
            $response['categories'] = HelpCategory::where('status', 'A')->whereHas('getArticles', function($q) use ($request){
                $q = $q->where('title', 'like', '%'.$request->title.'%')->where('status', 'A');
            })->get();
            foreach($response['categories'] as $category){
                $category['articles'] = HelpArticle::where('help_category_id', $category->id)->where('status', 'A')->where('title', 'like', '%'.$request->title.'%')->get();
            }
        }
        if(@$response['categories']){
            $response['status'] = "success";
        } else {
            $response['status'] = "error";
        }
        return response()->json($response);

    }

    public function helpArticles(Request $request, $slug = null) {

        $data['content'] = Content::where('id', 60)->first();
        $data['categories'] = HelpCategory::with('getArticles')->where('status', 'A')->get();
        $data['category'] = HelpCategory::where('slug', @$slug)->first();
        $data['articles'] = HelpArticle::where('help_category_id', $data['category']->id)->where('status', 'A');

        // if(@$request->all()){
        //     $data['articles'] = $data['articles']->where('title', 'like', '%'.$request->title.'%');
        //     $data['key'] = $request->all();
        // }
        $data['articles'] = $data['articles']->get();
        return view('modules.content.help_section_category_new')->with($data);

    }

    public function helpArticleDetails(Request $request, $slug = null) {
        $data['article'] = HelpArticle::where('slug', @$slug)->where('status', 'A')->first();
        return view('modules.content.help_article_details')->with($data);
    }

    /*
    * Method: affiliateLandingPage
    * Description: to update affiliate landing page
    * Author: Sayantani
    * Date: 07-OCT-2021
    */
    public function affiliateLandingPage()
    {
        $data = [];
        $data['part1'] = Content::find(61);
        $data['part2'] = Content::find(62);
        return view('modules.content.affiliate_landing_page')->with($data);
    }
    /*
    * Method: LandingPage
    * Description: to view Landing Page
    * Author: Soumojit
    * Date: 12-OCT-2021
    */
    public function LandingPage()
    {
        $data = [];
        $data['part1'] = Content::where('id', 63)->first();
        $data['part2'] = Content::where('id', 64)->first();
        $data['part3'] = Content::where('id', 65)->first();
        $data['part4'] = Content::where('id', 66)->first();
        return view('modules.content.professional_landing_page')->with($data);
    }
}
