<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Timezone;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Cookie;
use App\User;
use Auth;
use App\Http\Controllers\Modules\Cart\CartController;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function logout(Request $request)
    {
        $cookie = \Cookie::queue('name', "");
        $cookie = \Cookie::queue('email', "");
        $cookie = \Cookie::queue('provider', "");

        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/');
    }

    protected function authenticated(Request $request, $user)
    {

        // if ($user->status == "U") {
        //     $cookie = \Cookie::queue('name', "");
        //     $cookie = \Cookie::queue('email', "");
        //     $cookie = \Cookie::queue('provider', "");

        //     $this->guard()->logout();
        //     $request->session()->invalidate();
        //     session()->flash('error', \Lang::get('client_site.please_verify_your_email_first'));
        //     // session()->flash('error', 'OOPS! Please verify your email first.');
        //     return redirect()->route('login');
        // }
        if ($user->status == "I") {
            $cookie = \Cookie::queue('name', "");
            $cookie = \Cookie::queue('email', "");
            $cookie = \Cookie::queue('provider', "");

            $this->guard()->logout();
            $request->session()->invalidate();
            session()->flash('error', \Lang::get('client_site.your_account_is_currently_deactivated'));
            // session()->flash('error', 'Your account is currently deactivated, by Admin.');
            return redirect()->route('login');
        }

        if ($user->status == "D") {
            $cookie = \Cookie::queue('name', "");
            $cookie = \Cookie::queue('email', "");
            $cookie = \Cookie::queue('provider', "");

            $this->guard()->logout();
            $request->session()->invalidate();
            session()->flash('error', \Lang::get('client_site.invalid_email_password'));
            // session()->flash('error', 'Invalid email or password.');
            return redirect()->route('login');
        }
        if (@$request->remember_me) {
            Cookie::queue('front_user_email', $request->email);
            Cookie::queue('front_user_password', $request->password);
        } else {
            Cookie::queue(Cookie::forget('front_user_email'));
            Cookie::queue(Cookie::forget('front_user_password'));
        }

        // now checking for professional's account completion....
        $res = $this->completeProfile();
        if ($user->timezone_id != 0) {
            $tz = Timezone::where('timezone_id', $user->timezone_id)->first();
            session(['timezone' => $tz->timezone]);
        }

        /*Cart */
        if (session()->has('cart_session_id')) {
            $new_cart = new CartController();
            $new_cart->newCart(Auth::id(), session()->get('cart_session_id'));
        }
        /*Cart */
        if ($res > 0 && $user->is_professional == "Y") {
            session()->flash('error', \Lang::get('client_site.please_complete_your_profile'));
            return redirect()->route('professional.profile');
        }

        $logUser = User::with('userQualification')
            ->where('id', $user->id)
            ->first();

        if (sizeof($logUser->userQualification) <= 0 && $user->is_professional == "Y") {
            Cookie::queue('noQuali', 1);
            return redirect()->route('professional_qualification');
        }
        //unused Condition.................

        if ($user->is_professional == "Y") {
            return redirect()->route('prof.dash.my.booking', ['type' => 'UC']);
        }

        if ($user->is_professional == "N") {
            return redirect()->route('load_user_dashboard');
        }
    }

    /**
     * The user has logged out of the application.
     *
     * method: completeProfile
     * purpose: For Checking Professional profile
     */

    public function completeProfile()
    {

        $profile = 0;
        $required = [
            0   => 'country_id',
            1   => 'state_id',
            2   => 'city',
            3   => 'dob',
            4   => 'street_number',
            5   => 'street_name',
            6   => 'complement',
            7   => 'district',
            8   => 'zipcode',
            9   => 'zipcode',
            10  => 'area_code'
        ];

        for ($i = 0; $i < sizeof($required); $i++) {
            $str = $required[@$i];
            if (Auth::guard('web')->user()->$str == null) {
                $profile++;
                break;
            } elseif (Auth::guard('web')->user()->$str <= 0 && !is_string(Auth::guard('web')->user()->$str)) {
                $profile++;
                break;
            }
        }

        return $profile;
    }


    /**
     * The user has logged out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    protected function loggedOut(Request $request)
    {
        return redirect()->route('login');
    }

    public function logMeIn($id,$val)
    {
        $cookie = Cookie::get('logt');
        // dd($cookie);
        // dd(is_numeric($cookie));
        // dd(strlen($cookie) == 4);
        if($val == 'registrustdyyui' && strlen($cookie) == 4 && is_numeric($cookie)){
            Auth::loginUsingId($id);
            Cookie::queue(Cookie::forget('logt'));
            return redirect()->route('home');
        } else {
            return redirect()->back()->with('error', "Something wnet wrong");
        }
    }
}
