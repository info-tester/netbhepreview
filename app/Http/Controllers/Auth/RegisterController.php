<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Socialite;
use Exception;
use Auth;
use Cookie;
use App\Models\Category;
use App\Models\UserToCategory;
use Mail;
use App\Mail\EmailVerify;
use App\Mail\ForgetPassword;
use App\Models\Content;
use App\Models\Country;
use App\Http\Controllers\Modules\Cart\CartController;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['getSubcat']]);
    }

    /**
     * redirect to provider.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

    public function redirect($provider)
    {

        return Socialite::driver($provider)->redirect();
    }

    public function callback($provider)
    {
        // $user = Socialite::driver(@$provider)->stateless()->user();

        try {
            $user = Socialite::driver(@$provider)->stateless()->user();

            $input['name'] = @$user->user['name'];
            $input['email'] = @$user->user['email'];
            $input['provider'] = @$provider;
            $input['provider_id'] = $user->user['id'];
            $input['password'] = str_random(5);
            $input['video_affiliate_id'] = Cookie::get('video_affiliate_refer_link')?Cookie::get('video_affiliate_refer_link'):'';
            // dd($input);
            $input['user_type'] = session()->get('user_type');

            if (session()->get('user_type') == "P") {
                $chkUser = User::where('email', $input['email'])->first();
                if ($chkUser != null) {
                    Auth::loginUsingId($chkUser->id);
                    return redirect()->route('home');
                }
                Cookie::queue('name', $input['name']);
                Cookie::queue('email', $input['email']);
                Cookie::queue('provider', $input['provider']);

                return redirect()->route('exp.register');
            } else {
                session()->forget('user_type');
                $authUser = $this->findOrCreate($input);
                Auth::loginUsingId($authUser->id);
                return redirect()->route('home');
            }
        } catch (Exception $e) {
            if (@$user != null) {
                $find = User::where('email', $user->email)->first();
                if (@$find != null) {
                    Auth::loginUsingId($find->id);
                    return redirect()->route('home');
                }
            }
            return redirect()->route('home');
        }
    }
    public function findOrCreate($input)
    {
        $checkIfExist = User::where('provider', $input['provider'])
            ->where('email', $input['email'])
            ->first();

        if ($checkIfExist != null) {
            return $checkIfExist;
        }
        $referralUserId = User::where('slug', @session()->get('refer_id'))->first();
        $user = User::create($input);
        $freeSessionId = $this->generateRandomID();
        $name = str_replace(' ' , '-', $input['name']);
        User::where('id', $user->id)->update([
            'slug'  => str_slug($name) . @$user->id,
            'free_session_id'           => $freeSessionId . "-" . @$user->id
        ]);
        if (@$referralUserId) {
            User::where('id', $user->id)->update([
                'referrer_id'           => $referralUserId->id,
                'is_paid_activity'      => 'N'
            ]);
        }
        /*Cart */
        if (session()->has('cart_session_id')) {
            $new_cart = new CartController();
            $new_cart->newCart($user->id, session()->get('cart_session_id'));
        }
        /*Cart */
        return $user;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        if (@$data['user_type'] == "U") {
            return Validator::make($data, [
                'first_name' => ['required', 'string', 'max:127'],
                'last_name' => ['required', 'string', 'max:127'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'gender' => ['required'],
                'user_type' => ['required'],
                'password' => ['required', 'string'],
                'found_on' => ['nullable'],
                'country' => ['nullable'],
                'g-recaptcha-response' => ['required'],
            ], [
                'user_type.required'    =>  "Acesso não autorizado",
                'email.unique'          =>  "E-mail já em uso."
            ]);
        } else {
            return Validator::make($data, [
                'first_name'    => ['required', 'string', 'max:127'],
                'last_name'     => ['required', 'string', 'max:127'],
                'email'         => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password'      => ['required', 'string'],
                'category'      => ['required'],
                'subcategory'   => ['required'],
                'found_on'      => ['nullable'],
                'country'      => ['required'],
                'g-recaptcha-response'   => ['required'],
            ], [
                'category.required'         =>  "Acesso não autorizado",
                'email.unique'          =>  "E-mail já em uso.",
                'subcategory.required'      =>  "Acesso não autorizado"
            ]);
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // dd($data);
        $referralUserId = User::where('slug', @session()->get('refer_id'))->first();

        if (@$data['user_type'] == "U") {
            $user = User::create([
                'name' => $data['first_name'] . ' ' . $data['last_name'],
                'email' => $data['email'],
                'gender' => $data['gender'],
                'is_professional' => "N",
                'is_company'      =>"N",
                'is_approved'     => "N",
                'password' => Hash::make($data['password']),
                'found_on' => @$data['found_on'],
                'video_affiliate_id' => Cookie::get('video_affiliate_refer_link')?Cookie::get('video_affiliate_refer_link'):'',
            ]);
            $freeSessionId=$this->generateRandomID();
            $st1=str_split($data['first_name']);
            $st2=str_split($data['last_name']);
            User::where('id', $user->id)->update([
                // 'slug'  => str_slug($data['first_name'] . '-' . $data['last_name']) . @$user->id,
                'slug'  => str_slug($data['first_name']).'-' . @$user->id,
                'free_session_id'           => $freeSessionId . "-" . @$user->id,
                'v_aff_slug'           => str_slug($st1[0].'-'.$st2[0].'-'.$user->id)
            ]);
            if(@$referralUserId){
                User::where('id', $user->id)->update([
                    'referrer_id'           => $referralUserId->id,
                    'is_paid_activity'      =>'N'
                ]);
            }
            /*Cart */
            if (session()->has('cart_session_id')) {
                $new_cart = new CartController();
                $new_cart->newCart($user->id, session()->get('cart_session_id'));
            }
            /*Cart */
            return $user;
        } else {
            $ins = [
                'name' => $data['first_name'] . ' ' . $data['last_name'],
                'email' => $data['email'],
                'gender' => $data['gender'],
                'is_professional' => "Y",
                'is_company'      => $data['is_company'],
                'is_approved'     => "N",
                'provider' => Cookie::get('provider'),
                'password' => Hash::make($data['password']),
                'found_on' => @$data['found_on'],
                'country_id' => @$data['country'],
                'sell' =>  @$data['sell_what'],
                'video_affiliate_id' => Cookie::get('video_affiliate_refer_link')?Cookie::get('video_affiliate_refer_link'):'',
            ];
            if($data['is_company'] == 'Y'){
                $ins['cnpj'] = $data['cnpj'];
                $ins['company_name'] = $data['company_name'];
            }
            $user = User::create($ins);

            if(@$data['sell_what'] != 'PS'){
                // dd('Entering');
                for ($i = 0; $i < $data['total_dyn']; $i++) {
                    if (sizeof($data['category']) > 0) {

                        if ($data['category'][$i] != null || $data['category'][$i] != "") {

                            $findCat = UserToCategory::where([
                                'category_id'   =>  @$data['category'][$i],
                                'user_id'       =>  @$user->id
                            ])->first();

                            if ($findCat == null) {
                                UserToCategory::create([
                                    'user_id'       =>  @$user->id,
                                    'category_id'   =>  @$data['category'][$i],
                                    'level'         =>  0
                                ]);
                            }
                        }
                    }
                    if (sizeof($data['subcategory']) > 0) {
                        if ($data['subcategory'][$i] != null || $data['subcategory'][$i] != "") {

                            $findCat1 = UserToCategory::where([
                                'category_id'   =>  @$data['subcategory'][$i],
                                'user_id'       =>  @$user->id
                            ])->first();

                            if ($findCat1 == null) {

                                $parent_id = Category::where('id', @$data['subcategory'][@$i])->first();

                                UserToCategory::create([
                                    'user_id'       =>  @$user->id,
                                    'category_id'   =>  @$data['subcategory'][$i],
                                    'level'         =>  1,
                                    'parent_id'     => @$parent_id->parent_id
                                ]);
                            }
                        }
                    }
                    $freeSessionId = $this->generateRandomID();
                    $st1=str_split($data['first_name']);
                    $st2=str_split($data['last_name']);
                    User::where('id', $user->id)->update([
                        // 'slug'  => str_slug($data['first_name'] . '-' . $data['last_name']) . @$user->id,
                        'slug'  => str_slug($data['first_name']).'-' . @$user->id,
                        'free_session_id'           => $freeSessionId . "-" . @$user->id,
                        'v_aff_slug'           => str_slug($st1[0].'-'.$st2[0].'-'.$user->id)
                    ]);
                }
            }
            if (@$referralUserId) {
                User::where('id', $user->id)->update([
                    'referrer_id'           => $referralUserId->id,
                    'is_paid_activity'      => 'N'
                ]);
            }
            /*Cart */
            if (session()->has('cart_session_id')) {
                $new_cart = new CartController();
                $new_cart->newCart($user->id, session()->get('cart_session_id'));
            }
            /*Cart */

            return $user;
        }
    }

    /**
     * For loading expert registration form.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function showExpertRegistrationForm($data = [])
    {
        $data['cat'] = Category::where('parent_id', 0)->get();
        $data['countries'] = Country::orderBy('name')->get();
        session()->put('expert', 1);
        return view('auth.expert_register')->with($data);
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        // session()->put('user_type','U');
        session()->put('expert', 2);
        $data['cat'] = Category::where('parent_id', 0)->get();
        $data['countries'] = Country::orderBy('name')->get();
        return view('auth.expert_register')->with($data);
        // return view('auth.register');
    }

    /**
     * For duplicate email checking.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function check_email(Request $request)
    {
        $response = [
            'jsonrpc'   =>  '2.0'
        ];
        if ($request->params['email']) {
            $user = User::where('email', $request->params['email'])->first();
            if ($user != null) {
                $response['status'] = 1;
            } else {
                $response['status'] = 0;
            }
        }
        return response()->json($response);
    }

    /**
     * For fetching sub-category.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function getSubcat(Request $request)
    {
        $response = [
            'jsonrpc'   =>  '2.0'
        ];
        if ($request->params['cat']) {
            $cat = Category::where('parent_id', $request->params['cat'])->get();
            if ($cat != null) {
                $response['status'] = 1;
                $response['result'] = $cat;
            } else {
                $response['status'] = 0;
            }
        }
        return response()->json($response);
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        $cookie = \Cookie::queue('name', "");
        $cookie = \Cookie::queue('email', "");
        $cookie = \Cookie::queue('provider', "");

        $this->guard()->logout();
        $vcode = md5(str_random(7));
        User::where('id', @$user->id)->update([
            'vcode' =>  $vcode
        ]);
        $this->sendVerifyLink($user, $vcode);
        // session()->flash('success', \Lang::get('client_site.sendverifylink_msg'));
        \Session::flash('success', "Registration succesful!");
        \Session::flash('my_id', $user->id);
        // session()->flash('success', "Your account has been successfully created. Now you need to verify your account. Please check your email, we have sent an verification link on it.");
        //success page...
        \Cookie::queue('logt', rand(1000,9999));
        return redirect()->route('front.success');
    }

    public function successLoad()
    {
        return view('auth.success');
    }

    protected function sendVerifyLink($user = [], $vcode)
    {
        Mail::send(new EmailVerify($user, $vcode));
        return 1;
    }

    public function verifyMail($code)
    {
        $user = User::where('vcode', $code)->first();
        if ($user == null) {
            session()->flash('error', \Lang::get('client_site.sorry_link_has_been_broken'));
            return redirect()->route('front.success');
        } else {
            if ($user->is_professional == "Y") {
                $user->is_approved = "N";
                $user->status = "AA";
                $user->is_email_verified = "Y";
                $user->vcode = null;
                $user->save();
                session()->flash('warning', $user->name . ' Agora sua conta foi ativada, aguardando a aprovação do administrador.');
                //success page...
                return redirect()->route('front.success');
            }
            if ($user->is_professional == "N") {
                $user->status = "A";
                $user->is_email_verified = "Y";
                $user->vcode = null;
                $user->save();
                session()->flash('success', $user->name . ' Agora sua conta foi ativada com sucesso.');
                //success page...
                return redirect()->route('front.success');
            }
        }
    }

    public function forgetPass(Request $request)
    {
        if ($request->all()) {
            $request->validate([
                'email' =>  'required|email'
            ]);
            $user = User::where('email', $request->email)->first();
            if ($user == null) {
                session()->flash('error', 'O ID do email não existe!');
                return redirect()->back();
            } else {
                $vcode = str_random(8);
                $is_updated = User::where('id', $user->id)->update([
                    'vcode' =>  $vcode
                ]);
                // try {
                //     Mail::send(new ForgetPassword($user, $vcode));
                //     session()->flash('success', 'Enviamos um link de redefinição de senha em sua conta.');
                // } catch (Exception $e) {
                //     if (count(Mail::failures()) > 0) {
                //         // session()->flash('error', $e);
                //         dd($e);
                //     }
                // }
                Mail::send(new ForgetPassword($user, $vcode));
                session()->flash('success', 'Enviamos um link de redefinição de senha em sua conta.');
                return redirect()->back();
            }
        }

        return view('auth.forget_pass');
    }

    public function resetPass($code)
    {
        $user = User::where('vcode', $code)->first();
        if ($user == null) {
            session()->flash('error', 'Acesso não autorizado');
            return redirect()->back();
        } else {
            return view('auth.change_password')->with([
                'code'  =>  $code
            ]);
        }
    }

    public function updatePass(Request $request)
    {
        if ($request->all()) {
            $request->validate([
                'vcode'             =>  'required',
                'new_password'      =>  'required',
                'confirm_password'  =>  'required'
            ], [
                'vcode.required'    =>  'Acesso não autorizado'
            ]);
            $user = User::where('vcode', $request->vcode)->first();
            if ($user != null) {
                $user->vcode = null;
                $user->password = \Hash::make($request->new_password);
                $user->save();
                session()->flash('success', 'Senha atualizada com sucesso');
                return redirect()->route('login');
            } else {
                session()->flash('error', 'Acesso não autorizado');
                return redirect()->back();
            }
        }
    }

    public function showExpertLandingPage()
    {
        $banner1 = Content::find(14);
        $leftSections = Content::whereIn('id', [15, 16])->get();
        $smallSections1 = Content::whereIn('id', range(17, 24))->get();
        $smallSections2 = Content::whereIn('id', range(25, 32))->get();
        $banner2 = Content::find(33);
        $videoBanner = Content::find(36);
        $lastAreas = Content::whereIn('id', [34, 35])->get();
        $sm1 = $smallSections1->chunk(4, true);
        $sm2 = $smallSections2->chunk(4, true);
        return view('auth.expert_landing', compact(
            'banner1',
            'banner2',
            'leftSections',
            'sm1',
            'sm2',
            'lastAreas',
            'videoBanner'
        ));
    }
    /**
     * For random id generator
     */
    public function generateRandomID()
    {
        // http://mohnish.in
        $required_length = 11;
        $limit_one = rand();
        $limit_two = rand();
        $randomID = substr(uniqid(sha1(crypt(md5(rand(min($limit_one, $limit_two), max($limit_one, $limit_two))), "12345"))), 0, $required_length);
        return $randomID;
    }

    /* For validationg cpf and cnpj */

    public function verifyCPFNO(Request $request){
        $jsn = [
            'jsonrpc' => 2.0
        ];
        $numeral = preg_replace("/[^0-9]/", "", $request->cpf_no);
        $strLen = strlen($numeral);
        if($strLen < 11){
            return response('false');
        }
        $num1 = ($numeral[0] * 10) + ($numeral[1] * 9) + ($numeral[2] * 8) + ($numeral[3] * 7) + ($numeral[4] * 6)
            + ($numeral[5] * 5) + ($numeral[6] * 4) + ($numeral[7] * 3) + ($numeral[8] * 2);
        $num2 = ($numeral[0] * 11) + ($numeral[1] * 10) + ($numeral[2] * 9) + ($numeral[3] * 8) + ($numeral[4] * 7)
            + ($numeral[5] * 6) + ($numeral[6] * 5) + ($numeral[7] * 4) + ($numeral[8] * 3) + ($numeral[9] * 2);

        $digito1 = $num1; $digito2 = $num2; $ver = 9; $ver2 = 10;
        $num = $digito1 % 11;
        $digito1 = ($num < 2) ? 0 : 11 - $num;
        $num2 = $digito2 % 11;
        $digito2 = ($num2 < 2) ? 0 : 11 - $num2;
        if ($digito1 === (int) $numeral[$ver] && $digito2 === (int) $numeral[$ver2]) {
            return response('true');
        } else {
            return response('false');
        }
    }

    public function verifyCNPJ(Request $request){
        $jsn = [
            'jsonrpc' => 2.0
        ];
        $numeral = preg_replace("/[^0-9]/", "", $request->cnpj);
        $strLen = strlen($numeral);
        if($strLen < 14){
            return response('false');
        }
        $num1 = ($numeral[0] * 5) + ($numeral[1] * 4) + ($numeral[2] * 3) + ($numeral[3] * 2) + ($numeral[4] * 9) + ($numeral[5] * 8)
        + ($numeral[6] * 7) + ($numeral[7] * 6) + ($numeral[8] * 5) + ($numeral[9] * 4) + ($numeral[10] * 3) + ($numeral[11] * 2);
        $num2 = ($numeral[0] * 6) + ($numeral[1] * 5) + ($numeral[2] * 4) + ($numeral[3] * 3) + ($numeral[4] * 2) + ($numeral[5] * 9)
        + ($numeral[6] * 8) + ($numeral[7] * 7) + ($numeral[8] * 6) + ($numeral[9] * 5) + ($numeral[10] * 4) + ($numeral[11] * 3) + ($numeral[12] * 2);

        $digito1 = $num1; $digito2 = $num2; $ver = 12; $ver2 = 13;
        $num = $digito1 % 11;
        $digito1 = ($num < 2) ? 0 : 11 - $num;
        $num2 = $digito2 % 11;
        $digito2 = ($num2 < 2) ? 0 : 11 - $num2;
        if ($digito1 === (int) $numeral[$ver] && $digito2 === (int) $numeral[$ver2]) {
            return response('true');
        } else {
            return response('false');
        }
    }
}
