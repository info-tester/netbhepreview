<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\User;
use App\Models\UserAvailability;
use App\Models\Testimonial;
use App\Models\Content;
use App\Models\HomeBanner;
use App\Models\Timezone;
use Auth;
use URL;
use App\Models\Booking;
use App\Models\Blog;
use App\Models\ReferContent;
use App\Models\Product;
use Storage;
use Imagick;
use Exception;
use Org_Heigl\Ghostscript\Ghostscript;
use Spatie\PdfToImage\Pdf as Pdf;
use Twilio\Rest\Client;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'setDate', 'updateTimezone', 'refer', 'freeSession', 'wireCard');
    }

    public function imagickTest(Request $request){
        // phpinfo();
        $data['images'] = array();
        if(@$request->all()){
            $pdf_file = $request->pdf_upload;
            $filename = time() . '-' . rand(1000, 9999) . '.' . $pdf_file->getClientOriginalExtension();
            Storage::putFileAs('public/documents/', $pdf_file, $filename);

            $path = storage_path() . "/app/public/documents/".$filename;
            $arr = [];
            $pd=new Pdf($path);
            // dd($this->countPages($pdf_file));
            $totalPages = $this->countPages($pdf_file);

            // Ghostscript::setGsPath("C:\Program Files\gs\gs9.54.0\bin\gswin64c.exe -o page_%03d.jpg -sDEVICE=jpeg");
            Ghostscript::setGsPath("C:\Program Files\gs\gs9.54.0\bin\gswin64c.exe");
            $pd->getNumberOfPages($totalPages);
            foreach (range(1, $totalPages) as $pageNumber) {
                $pageName = 'page_no'.'_'.$pageNumber;
                $savePath = storage_path() . "\app\public\documents\\".$pageName;
                $pd->setPage($pageNumber)->saveImage($savePath);
                array_push($arr, $savePath.'.jpeg');
            }
            $data['images'] = $arr;
            dd(['pd' => $pd, 'images' => $arr]);
        }
        return view('imagick')->with($data);
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // Now getting the categories...........
        $cat = Category::
        select('id','status','shown_in_home','name','slug','image')
        ->where([
            'status'        =>  "A",
            //'parent_id'     =>  0,
            'shown_in_home' =>  "Y",
        ])->orderBy('name')->get();
        $user = User::with('userLanguage', 'specializationName')->where([
            'status' =>  'A',
            'is_professional' =>  'Y',
            'is_approved' =>  'Y',
            'shown_in_home' =>  'Y',
            'profile_active' => 'Y',
        ]);
        // $allUser= User::where('nick_name','=',null)->get();
        // foreach($allUser as $user2){
        //     $st = explode(" ",$user2->name);
        //     $slug= str_slug($st[0].'-'.$user2->id);
        //     User::where('id',$user2->id)->update(['slug'=>$slug]);
        // }
        // $allUser= User::get();
        // foreach($allUser as $user2){
        //     $st = explode(" ",$user2->name);
        //     $st1=str_split($st[0]);
        //     $st2=str_split($st[1]);
        //     $slug=  str_slug($st1[0].'-'.$st2[0].'-'.$user2->id);
        //     User::where('id',$user2->id)->update(['v_aff_slug'=>$slug]);
        // }
        // $allUser= User::where('nick_name','!=',null)->get();
        // foreach($allUser as $user2){

        //     $slug=  str_slug($user2->nick_name);
        //     User::where('id',$user2->id)->update(['slug'=>$slug]);
        // }
        if (Auth::id()) {
            $user = $user->where('id', '!=', Auth::id());
        }
        $user = $user->inRandomOrder()->limit(50)->get();
        $prods = Product::select(
            'id','admin_status','status','professional_id',
            'show_in_home_page','category_id','title','slug',
            'price','discounted_price','description','cover_image',
            'purchase_start_date','purchase_end_date'
        )
        ->with([
            'category'=>function($query){
                $query->select('id','category_name','slug');
            }
        ])->where('show_in_home_page', 'Y')->where('professional_id', '!=', @Auth::id())->where('status', 'A')->where('admin_status', 'A')
        ->inRandomOrder()->limit(8)->get();
        // dd($user);
        $testi = Testimonial::orderBy('id', 'desc')->get();
        // return view('modules.home.home')->with([
        //     'cat'       =>  @$cat,
        //     'user'      =>  @$user,
        //     'testi'     =>  @$testi
        $content= Content::whereIn('id', [2,5,37,6,7,8,9])->orderBy('id','asc')->get();

        // $how_it_works = Content::where('id', 2)->first();
        // $meetNeeds = Content::where('id', 5)->first();
        // $productSection = Content::where('id', 37)->first();
        // $testiContent = Content::where('id', 6)->first();
        // $exp1 = Content::where("id", 7)->first();
        // $exp2 = Content::where("id", 8)->first();
        // $exp3 = Content::where("id", 9)->first();
        $banners = HomeBanner::inRandomOrder()->get();
        $blog = Blog::orderBy('id','DESC')
        ->with([
            'blogCategoryName' => function($query) {
                $query->select('id','name');
            },
            'postedBy'=>function($query) {
                $query->select('id','name');
            },
            'adminPostedBy'=>function($query) {
                $query->select('id','name');
            },
            'blogComments'=>function($query) {
                $query->select('id','name');
            },
        ])
        ->where('status', 'A')->take(3)->get();
        // return $blog;
        $products=array();
        $limit = 8;
        if(count($prods) > $limit){
            //select random products
            //generate an array of product ids
            $prod_id_arr = $product_id_array = array();
            foreach($prods as $prod){
                array_push($prod_id_arr, $prod->id);
            }

            //pick 5 random products
            for($i=0; $i < $limit; $i++){
                $key = array_rand($prod_id_arr);
                array_push($product_id_array, $prod_id_arr[$key]);
                unset($prod_id_arr[$key]);
            }
            foreach($product_id_array as $product_id){
                array_push($products, Product::find($product_id));
            }
        } else {
            $products = $prods;
        }
        return view('modules.home.home')->with([
            'cat'       =>  @$cat,
            'user'      =>  @$user,
            'testi'     =>  @$testi,
            // 'how_it_works' => $how_it_works,
            // 'meetNeeds' => $meetNeeds,
            // 'productSection' => $productSection,
            'products' => $products,
            // 'testiContent' => $testiContent,
            // 'exp1' => $exp1,
            // 'exp2' => $exp2,
            // 'exp3' => $exp3,
            'banners' => $banners,
            'blog' => $blog,
            'content'=>$content,
        ]);
    }

    /**
     * For setting booking date.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function setDate(Request $request)
    {
        $response = [
            'jsonrpc'   =>  '2.0'
        ];
        if ($request->params['date'] && $request->params['time']) {
            $startTimeArr = explode(' - ', $request->params['time']);
            $startTime = $startTimeArr[0];
            session()->put('slotStart', date('Y-m-d H:i:s', strtotime($request->params['date'] . ' ' . $startTime)));
            $response['status'] = 1;
        } else {
            $response['status'] = 0;
        }
        return response()->json($response);
    }

    public function updateTimezone(Request $request)
    {
        $response = [
            'jsonrpc' => 2.0
        ];
        $params = $request->params;
        $timezone_offset_minutes = $params['timezone_offset_minutes'];
        $timezone = timezone_name_from_abbr('', $timezone_offset_minutes * 60, false);
        $timezoneDB = Timezone::where('timezone', $timezone)->first();
        if ($timezoneDB == null) {
            $response['error']['message'] = 'A detecção automática do seu fuso horário falhou. Faça login e atualize seu fuso horário em seu perfil. Você verá os horários UTC por enquanto.';
            return response()->json($response);
        }
        if (auth()->check()) {
            $userTZId = auth()->user()->timezone_id;
            if ($userTZId == 0) {
                auth()->user()->update([
                    'timezone_id' => ($timezoneDB->timezone_id ?? 0)
                ]);
                session(['timezone' => $timezone]);
            } else {
                $userTimezoneDB = Timezone::where('timezone_id', $userTZId)->first();
                session(['timezone' => $userTimezoneDB->timezone ?? env('TIMEZONE')]);
            }
        } else {
            session(['timezone' => $timezone]);
        }
        $response['result']['status'] = 'redirect';
        return response()->json($response);
    }
    public function refer($id)
    {
        session(['refer_id' => $id]);
        $value = session()->get('refer_id');
        return redirect()->route('register');
    }
    public function referUser()
    {
        $totalRefer = User::where('referrer_id', @Auth::id())->count();
        $referBenefit   = User::where('referrer_id', @Auth::id())->where('is_paid_activity','C')->count();
        $referBenefitRemaining  = User::where('id', @Auth::id())->first();
        $content = ReferContent::first();
        $data=[
            'totalRefer'=> @$totalRefer,
            'referBenefit'=> @$referBenefit,
            'referBenefitRemaining'=> @$referBenefitRemaining->benefit_count,
            'content' => @$content
        ];
        // return $data;
        return view('modules.refer.refer')->with(@$data);
    }
    public function referUserList()
    {
        $totalRefer = User::where('referrer_id', @Auth::id())->orderBy('id','DESC')->get();
        $data=[
            'totalRefer'=> @$totalRefer,
        ];
        // return $data;
        return view('modules.refer.refer_list')->with(@$data);
    }
    public function freeSession($slug,$id)
    {
        session(['free_session_id' => $id]);
        $freeSession = User::where('free_session_id',$id)->first();
        if($freeSession->free_session_number>0){
            $is_booking_free = Booking::where([
                'professional_id'   =>  $freeSession->id,
                'user_id'           =>  @auth()->id(),
                'free_session_id'   => $freeSession->id,
            ])->get();
            if (@$is_booking_free->count()>=10) {
                session()->forget('free_session_id');
                session()->flash('error', \Lang::get('client_site.free_session_not_available_you'));
                // \Lang::get('client_site.free_session_not_available');
                return redirect()->route('pblc.pst', ['slug' => $slug]);
            }
            session()->flash('success', \Lang::get('client_site.free_session_available'));
            return redirect()->route('book.now', ['slug' => $slug]);
        }
        session()->forget('free_session_id');
        session()->flash('error', \Lang::get('client_site.free_session_not_available'));
        return redirect()->route('pblc.pst',['slug'=>$slug]);
    }
    public function inviteFreeSession()
    {

        // return $data;
        return view('modules.refer.free_session');
    }
    public function wireCard(Request $request)
    {

        dd($request->all());
    }
    public function jsPlayer($url)
    {
        // if(file_exists(URL::to('storage/app/public/uploads/product_files').'/'.$url))
            return view('modules.player')->with('my_video', $url);
        // else
        //     return redirect()->back()->with('error', 'File not found');
    }

    function countPages($path) {
        $pdf = file_get_contents($path);
        $number = preg_match_all("/\/Page\W/", $pdf, $dummy);
        return $number;
    }

    public function whatsappTwilioGet(){
        // dd(csrf_token());
        // return "<h1>Try</h1>";
        return "success";
    }

    public function whatsappTwilioPost($to, $from, $body){
        // dd("Hello");
        try{
            $sid    = "ACd5378cc213160eaea06dc83259d56406";
            $token  = "19cd8b52b286dce9a7a5cf7e4c37985a";
            $twilio = new Client($sid, $token);

            $message = $twilio->messages
                            ->create("whatsapp:".$to, // to
                                    array(
                                        "from" => "whatsapp:".$from,
                                        "body" => $body
                                    )
                            );
            $msg = $message->toArray();
            $msg['statusCode'] = 200;
            return $msg;
        } catch (Exception $ex) {
            $exception = [];
            $exception['object'] = $ex;
            $exception['statusCode'] = $ex->getStatusCode();
            return $exception;
        }
    }
}
