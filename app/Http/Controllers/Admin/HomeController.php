<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Auth;
use App\Admin;
use App\Models\Blog;
use App\Models\Category;
use App\Models\CategoryLandingPageMaster;
use App\Models\HelpArticle;
use App\Models\HelpCategory;
use App\Models\Product;
use App\Models\TermsOfService;
use Illuminate\Http\Response;
use App\User;

class HomeController extends Controller
{

    protected $redirectTo = '/admin/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }

    /**
     * Show the Admin dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        return view('admin.modules.dashboard.home')->with([
                    'blog'          => @$blog,
                    'testimonial'   => @$testimonial,
                    'customer'      => @$customer,
                    'astrologer'    => @$astrologer,
                    'order'         => @$order,
                    'today_order'   => @$today_order,
                    'week_order'    => @$week_order,
                    'month_order'   => @$month_order,
                ]);
    }


    /*
    *Method: changePassword
    *Description: For password change form
    *Author: Rakesh
    */
    public function changePassword()
    {
        // dd(1);
        return view('admin.modules.settings.settings');
    }

    /*
    *Method: updatePassword
    *Description: For update password
    *Author: Rakesh
    */
    public function updatePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'         => 'required',
            'password'     => 'nullable|confirmed',
        ]);

        if($request->id != Auth::guard('admin')->id()) {
            \Session::flash('error', 'Unauthrize access.');
            return redirect()->back();
        }
        if ($validator->passes()) {
            // password check
            if (@$request->password != null) {
                $update['password'] = \Hash::make($request->password);
            }

            $update['name'] = $request->name;

            // for update profile
            Admin::where('id', Auth::guard('admin')->id())->update($update);

            \Session::flash('success', 'Profile updated successfully.');
            return redirect()->back();
        } else {
            \Session::flash('validator_error', $validator->messages()->messages());
            return redirect()->back();
        }
    }


    public function array_to_xml(array $data, \SimpleXMLElement $xml) {
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                if (is_numeric($key)) {
                    $key = 'url'; //dealing with <0/>..<n/> issues
                }
                $subnode = $xml->addChild($key);
                $this->array_to_xml($value, $subnode);
            } else {
                $xml->addChild("$key", htmlspecialchars("$value"));
            }
        }
    }

    public function generateSitemap()
    {
        $allUrl=[];
        $inData=[];

        $date= date('Y-m-d').'T'.date('H:i:sP');

        //static
        $inData['loc']=route('home');
        $inData['priority']='1.0';
        $inData['lastmod']=$date;
        array_push($allUrl,$inData);

        $inData['loc']=route('register');
        $inData['priority']='0.8';
        $inData['lastmod']=$date;
        array_push($allUrl,$inData);

        $inData['loc']=route('forget.pass');
        $inData['priority']='0.8';
        $inData['lastmod']=$date;
        array_push($allUrl,$inData);

        $inData['loc']=route('blog.frontend');
        $inData['priority']='0.8';
        $inData['lastmod']=$date;
        array_push($allUrl,$inData);

        $inData['loc']=route('frontend.about');
        $inData['priority']='0.8';
        $inData['lastmod']=$date;
        array_push($allUrl,$inData);

        $inData['loc']=route('terms.of.services');
        $inData['priority']='0.8';
        $inData['lastmod']=$date;
        array_push($allUrl,$inData);

        $inData['loc']=route('help.section');
        $inData['priority']='0.8';
        $inData['lastmod']=$date;
        array_push($allUrl,$inData);

        $inData['loc']=route('faq');
        $inData['priority']='0.8';
        $inData['lastmod']=$date;
        array_push($allUrl,$inData);

        $inData['loc']=route('consultant.categories');
        $inData['priority']='0.8';
        $inData['lastmod']=$date;
        array_push($allUrl,$inData);

        $inData['loc']=route('srch.pst');
        $inData['priority']='0.8';
        $inData['lastmod']=$date;
        array_push($allUrl,$inData);

        $inData['loc']=route('affiliate.landing.page');
        $inData['priority']='0.8';
        $inData['lastmod']=$date;
        array_push($allUrl,$inData);

        $inData['loc']=route('all.product.search.filter');
        $inData['priority']='0.8';
        $inData['lastmod']=$date;
        array_push($allUrl,$inData);

        $inData['loc']=route('category.landing.page');
        $inData['priority']='0.8';
        $inData['lastmod']=$date;
        array_push($allUrl,$inData);

        $inData['loc']=route('exp.landing');
        $inData['priority']='0.8';
        $inData['lastmod']=$date;
        array_push($allUrl,$inData);

        $inData['loc']=route('landing.pages');
        $inData['priority']='0.8';
        $inData['lastmod']=$date;
        array_push($allUrl,$inData);

        $inData['loc']=route('browse.categories');
        $inData['priority']='0.8';
        $inData['lastmod']=$date;
        array_push($allUrl,$inData);

        $inData['loc']=route('product.cart');
        $inData['priority']='0.8';
        $inData['lastmod']=$date;
        array_push($allUrl,$inData);


        // dinamic UserData
        $allUser = User::where([
            'status'                =>  'A',
            'is_professional'       =>  'Y',
            'is_approved'           =>  'Y',
            'profile_active'        =>  'Y',
        ])->get();

        foreach($allUser as $user){
            $inData=[];
            $inData['loc']=route('pblc.pst',['slug'=>$user->slug]);
            $inData['priority']='0.8';
            $inData['lastmod']=$date;
            array_push($allUrl,$inData);
        }
        $data=$allUrl;

        // dinamic Product Data
        $allProduct  = Product::where(['status' => 'A', 'admin_status' => 'A'])
        ->select(
            'id','admin_status','status','professional_id',
            'show_in_home_page','category_id','title','slug',
            'price','discounted_price','description','cover_image',
            'purchase_start_date','purchase_end_date','course_start_date','subcategory_id'
        )
        ->get();
        foreach($allProduct as $product){
            $inData=[];
            $inData['loc']=route('product.details',['slug'=>$product->slug]);
            $inData['priority']='0.8';
            $inData['lastmod']=$date;
            array_push($allUrl,$inData);
        }

        // dinamic Blog Data
        $blogs = Blog::where('status', 'A')->get();
        foreach($blogs as $blog){
            $inData=[];
            $inData['loc']=route('blog.frontend.details',['slug'=>$blog->slug]);
            $inData['priority']='0.8';
            $inData['lastmod']=$date;
            array_push($allUrl,$inData);
        }


        // dinamic Category Data
        $allCategory= Category::where('status', 'A')->get();
        foreach($allCategory as $category){
            $detail =CategoryLandingPageMaster::where('category_id', $category->id)->first();
            if(@$detail){
                $inData=[];
                $inData['loc']=route('category.landing.page.f',['slug'=>$category->slug]);
                $inData['priority']='0.8';
                $inData['lastmod']=$date;
                array_push($allUrl,$inData);
            }
        }
        foreach($allCategory as $category){
            $inData=[];
            $inData['loc']=route('srch.slug',['slug'=>$category->slug]);
            $inData['priority']='0.8';
            $inData['lastmod']=$date;
            array_push($allUrl,$inData);
        }
        // dinamic terms and condition Data
        $terms = TermsOfService::where('parent_id', 0)->get();
        foreach($terms as $term){
            $inData=[];
            $inData['loc']=route('subterms.of.services',['slug'=>$term->slug]);
            $inData['priority']='0.8';
            $inData['lastmod']=$date;
            array_push($allUrl,$inData);
        }

        // dinamic HelpCategory Data
        $allHelpCategory = HelpCategory::with('getArticles')->where('status', 'A')->get();
        foreach($allHelpCategory as $helpCategory){
            $inData=[];
            $inData['loc']=route('help.articles',['slug'=>$helpCategory->slug]);
            $inData['priority']='0.8';
            $inData['lastmod']=$date;
            array_push($allUrl,$inData);
        }

        // dinamic  HelpArticle Data
        $allHelpArticle = HelpArticle::with('getCategory')->where('status', 'A')->get();
        foreach($allHelpArticle as $helpArticle){
            $inData=[];
            $inData['loc']=route('help.section',['slug'=>$helpArticle->slug]);
            $inData['priority']='0.8';
            $inData['lastmod']=$date;
            array_push($allUrl,$inData);
        }
        $data=$allUrl;

        $xml_template_info = new \SimpleXMLElement("<?xml version=\"1.0\" encoding=\"UTF-8\"?><urlset></urlset>");
        $this->array_to_xml($data, $xml_template_info);
        $xml_template_info->asXML(dirname(__FILE__,5) . "/sitemap.xml");
        header('Content-type: text/xml');
        die(readfile(dirname(__FILE__,5) . "/sitemap.xml"));
        //or return readfile(dirname(__FILE__) . "/sitemap.xml");
    }
}
