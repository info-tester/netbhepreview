<?php

namespace App\Http\Controllers\Admin\Modules\Professional;
// date_default_timezone_set('Asia/Kolkata');
use App\User;
use App\Models\Category;
use App\Models\UserToCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\UserAccountApproval;
use App\Mail\PayoutMail;
use App\Mail\UserAccountReject;
use App\Mail\Verification;
use App\Mail\ChangeUserName;
use Mail;
use Auth;
use Session;
use Validator;
use Moip\Moip;
use Moip\Auth\BasicAuth;
use Moip\Auth\OAuth;
use App\Mail\EmailVerify;
use App\Models\UserToBankAccount;
use App\Models\Booking;
use App\Models\Country;
use App\Models\State;
use App\Models\Language;
use App\Models\UserToLanguage;
use App\Models\Timezone;
use App\Models\Experience;
use App\Models\ProfessionalToQualification;
use App\Mail\SendRemainder;
use App\Models\Bank;
use App\Admin;

class ProfessionalController extends Controller
{

    protected $redirectTo = '/admin/login';

    protected $adminAccessToken;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');

        $this->adminAccessToken = getAdminToken();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!adminHasAccessToMenu(22)) {
            return redirect()->route('admin.dashboard');
        }
        $prof = User::with('userDetails.categoryName', 'userDetails.parentCategoryName')
            ->where('status', 'A')->where('is_approved', 'Y');
        $key = [];

        if (@$request->all()) {
            if (@$request->keyword) {
                $prof = $prof->where(function ($where) use ($request) {
                    $where->where('name', 'like', '%' . $request->keyword . '%')
                        ->orWhere('nick_name', 'like', '%' . $request->keyword . '%')
                        ->orWhere('email', $request->keyword)
                        ->orWhere('mobile', $request->keyword);
                });
            }

            if (@$request->category) {
                $prof = $prof->whereHas('userDetails', function ($q) use ($request) {
                    $q->where('category_id', $request->category);
                });
            }

            if (@$request->subcategory) {
                $prof = $prof->whereHas('userDetails', function ($q) use ($request) {
                    $q->where('category_id', $request->subcategory);
                });
            }

            if (@$request->email) {
                $prof = $prof->where('email', $request->email);
            }

            if (@$request->status) {
                $prof = $prof->where('status', $request->status);
            }

            if (@$request->country) {
                $prof = $prof->where('country_id', $request->country);
            }

            if (@$request->is_company) {
                $prof = $prof->where('is_company', 'Y');
            }

            $key = $request->all();
        }
        $prof = $prof->orderBy('id', 'desc')->get();
        $category = Category::where('status', 'A')->orderBy('name', 'asc')->get();
        $countries = Country::get();
        //$profile = $this->completeProfile();
        return view('admin.modules.user.professional', [
            'prof'     => @$prof,
            'category' => @$category,
            'countries' => @$countries,
            'key'      => @$key/*,
            'profile'  => @$profile*/
        ]);
    }

    /**
     *method: inactiveProfessionalsIndex
     *created By: Sayantani
     *description: For showing inactive/unverified users separately
     */
    public function inactiveProfessionalsIndex(Request $request)
    {
        if (!adminHasAccessToMenu(23)) {
            return redirect()->route('admin.dashboard');
        }
        $prof = User::with('userDetails.categoryName', 'userDetails.parentCategoryName')
            ->where(function ($where) use ($request) {
                $where->whereIn('status', ['AA', 'I', 'U'])->orWhere('is_approved', 'N');
            });
        $key = [];

        if (@$request->all()) {
            if (@$request->keyword) {
                $prof = $prof->where(function ($where) use ($request) {
                    $where->where('name', 'like', '%' . $request->keyword . '%')
                        ->orWhere('nick_name', 'like', '%' . $request->keyword . '%')
                        ->orWhere('email', $request->keyword)
                        ->orWhere('mobile', $request->keyword);
                });
            }

            if (@$request->category) {
                $prof = $prof->whereHas('userDetails', function ($q) use ($request) {
                    $q->where('category_id', $request->category);
                });
            }

            if (@$request->subcategory) {
                $prof = $prof->whereHas('userDetails', function ($q) use ($request) {
                    $q->where('category_id', $request->subcategory);
                });
            }

            if (@$request->email) {
                $prof = $prof->where('email', $request->email);
            }

            if (@$request->status) {
                $prof = $prof->where('status', $request->status);
            }

            if (@$request->country) {
                $prof = $prof->where('country_id', $request->country);
            }

            if (@$request->is_company) {
                $prof = $prof->where('is_company', 'Y');
            }

            $key = $request->all();
        }
        $prof = $prof->orderBy('id', 'desc')->get();
        $category = Category::where('status', 'A')->orderBy('name', 'asc')->get();
        $countries = Country::get();
        //$profile = $this->completeProfile();
        return view('admin.modules.user.professional_inactive', [
            'prof'     => @$prof,
            'category' => @$category,
            'countries' => @$countries,
            'key'      => @$key/*,
            'profile'  => @$profile*/
        ]);
    }

    /**
     * The user has logged out of the application.
     *
     * method: completeProfile
     * purpose: For Checking Professional profile
     */

    public function completeProfile()
    {
        $profile = 0;
        $required = [
            0   => 'country_id',
            1   => 'state_id',
            2   => 'city',
            3   => 'dob',
            4   => 'street_number',
            5   => 'street_name',
            6   => 'complement',
            7   => 'district',
            8   => 'zipcode',
            9   => 'zipcode',
            10  => 'area_code'
        ];

        for ($i = 0; $i < sizeof($required); $i++) {
            $str = $required[@$i];
            if (Auth::guard('web')->user()->$str == null) {
                $profile++;
                break;
            } elseif (Auth::guard('web')->user()->$str <= 0 && !is_string(Auth::guard('web')->user()->$str)) {
                $profile++;
                break;
            }
        }

        return $profile;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!adminHasAccessToMenu(3)) {
            return redirect()->route('admin.dashboard');
        }
        $user = User::with([
            'userDetails.categoryName',
            'userDetails.parentCategoryName',
            'userAvailiability',
            'userQualification',
            'userExperience',
            'msgConversation',
            'countryName',
            'stateName',
            'userLang.languageName',
            'bankAccount',
            'userCard'
        ])->find($id);
        $userCategories = UserToCategory::with('categoryName.parent')->where('level', 1)->where('user_id', Auth::id())->get();
        $user->userCategories = @$userCategories;
        if (@$user == null) {
            session()->flash('error', 'Unauthorized access.');
            return redirect()->back();
        }
        $profile = 0;
        $required = [
            0   => 'country_id',
            1   => 'state_id',
            2   => 'city',
            3   => 'dob',
            4   => 'street_number',
            5   => 'street_name',
            6   => 'complement',
            7   => 'district',
            8   => 'zipcode',
            9   => 'zipcode',
            10  => 'area_code'
        ];

        for ($i = 0; $i < sizeof($required); $i++) {
            $str = $required[@$i];
            if ($user->$str == null) {
                $profile++;
                break;
            } elseif ($user->$str <= 0 && !is_string($user->$str)) {
                $profile++;
                break;
            }
        }
        return view('admin.modules.user.professional_details')->with([
            'user'      => @$user,
            'profile'   => @$profile,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!adminHasAccessToMenu(3)) {
            return redirect()->route('admin.dashboard');
        }
        $user = User::with('userDetails.categoryName')
            ->find($id);
        // dd($user);
        if (@$user == null) {
            \Session::flash('error', 'Unauthorized access.');
            return redirect()->back();
        }

        $category   = Category::orderBy('name', 'asc')->get();
        $sub = UserToCategory::select('category_id')->where([
            'user_id'   => @$user->id,
            'level'     => 1
        ])->get();

        if ($user == null) {
            session()->flash('error', 'Unauthorize access');
            return redirect()->back();
        }
        $var = [];
        $i = 0;

        if (sizeof($sub) > 0) {
            foreach (@$sub as $ss) {

                $var[$i] = @$ss->category_id;
                $i++;
            }
        }
        $time = Timezone::get();
        // $lang_id = UserToLanguage::where('user_id',$user->language_id)->get();
        $lang_id = UserToLanguage::where('user_id', $user->id)->pluck('language_id')->toArray();

        return view('admin.modules.user.edit_professional')->with([
            'user'             => @$user,
            'time'             => @$time,
            'category'         => @$category,
            'sub'              => @$var,
            'country'          => Country::orderBy('name')->get(),
            'state'            => State::orderBy('name')->get(),
            'lang'             => Language::orderBy('name')->get(),
            'lang_id'          => $lang_id,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        if (!adminHasAccessToMenu(3)) {
            return redirect()->route('admin.dashboard');
        }
        // dd($request->all());
        $user = User::where([
            'id'                =>  $id,
            'is_professional'   =>  "Y"
        ])->first();

        if ($user == null) {
            session()->flash('error', 'Unauthorized access.');
            return redirect()->back();
        }

        $request->validate([
            'name'                  => 'required',
            'nick_name'             => 'nullable|unique:users,nick_name,',
            'email'                 => 'required|email|unique:users,email,' . $user->id,
            'crp_no'                => 'required|unique:users,crp_no,' . @$user->id,
            'mobile'                => 'required|unique:users,mobile,' . $user->id,
            'gender'                => 'required',
            // 'category'              => 'required',
            'status'                => 'required',
            'rate_price'            => 'required',
            'rate'                  => 'required',
            // 'profile_pic'           => 'nullable|image|mimes:jpeg,bmp,png,jpg'
            'profile_pic'           => 'nullable',
            'description'           => 'nullable'
        ]);
        $duplicateMobile = User::where('mobile', $request->mobile)->where('id', '!=', $id)->first();

        if ($user->is_professional != 'Y') {
            session()->flash('error', 'Unauthorized access.');
            return redirect()->back();
        }

        // For email verification
        if (@$request->is_email_verified == 'Y') {
            $update['is_email_verified'] = 'Y';
            $update['email_code'] = null;
        } elseif (@$request->is_email_verified == 'N') {
            $update['is_email_verified'] = 'N';
        }

        // For email and mobile no change
        if (@$request->email != $user->email || @$request->mobile != $user->mobile) {

            $reqData['user'] = $user;
            $reqData['new_email'] = $request->email;

            // send mail to old email
            Mail::send(new ChangeUserName($reqData));
        }

        // if image has or not
        $ext = [
            0   =>  'png',
            1   =>  'jpeg',
            2   =>  'jpg',
            3   =>  'gif',
            4   =>  'png'
        ];
        if (@$request->nick_name) {
            $update['nick_name']  = ucwords(strtolower(@$request->nick_name));
        }
        if (@$request->profile_pic) {
            if (!in_array($request->profile_pic->getClientOriginalExtension(), $ext)) {
                session()->flash('error', 'Please Upload an image');
                return redirect()->back();
            }
            $name = time() . '_' . $request->profile_pic->getClientOriginalName();
            $path = "storage/app/public/uploads/profile_pic";
            $request->profile_pic->move($path, $name);
            $update['profile_pic'] = $name;
            if (@$user->profile_pic) {
                if (file_exists($path . '/' . @$user->profile_pic)) {
                    unlink('storage/app/public/uploads/profile_pic/' . $user->profile_pic);
                }
            }
        }


        // send email to User
        if ($user->status == 'U' || $user->status == 'AA') {
            if (@$request->status == 'A') {
                Mail::send(new UserAccountApproval($user));
            }
        }

        $update['name']                 = $request->name;
        $update['crp_no']               = $request->crp_no;
        $update['email']                = $request->email;
        $update['mobile']               = $request->mobile;
        $update['gender']               = $request->gender;
        $update['status']               = $request->status;
        $update['rate_price']           = $request->rate_price;
        $update['rate']                 = $request->rate;

        $update['dob']                 = $request->dob;
        $update['cpf_no']              = $request->cpf_no;
        $update['country_id']          = $request->country;
        $update['state_id']          = $request->state;
        $update['city']                = $request->city;
        $update['street_name']         = $request->street_name;
        $update['street_number']       = $request->street_number;
        $update['complement']          = $request->complement;
        $update['district']            = $request->district;
        $update['zipcode']             = $request->zipcode;
        $update['area_code']           = $request->area_code;
        $update['profile_active']      = $request->profile_active;
        $update['timezone_id']         = $request->time_zone;
        $update['free_session_number'] = $request->free_session;
        $update['description']         = $request->description;

        User::where('id', $user->id)->update($update);
        UserToLanguage::where('user_id', $user->id)->delete();
        if(@$request->language){
            foreach ($request->language as $key => $value) {
                UserToLanguage::create([
                    'user_id'       =>  $user->id,
                    'language_id'   =>  $value
                ]);
            }
        }

        // $this->updateToCategory($user->id, $request->category, $request->subcategory);

        session()->flash("success", "Professional profile updated successfully.");
        return redirect()->back();
    }

    /**
     *method: load_professional_dashboard
     *created By: Abhisek
     *description: For loading user Dashboard
     */

    public function getState(Request $request)
    {
        $response = [
            'jsonrpc'   =>  '2.0'
        ];
        if ($request->params['cn']) {
            $state = State::where('country_id', @$request->params['cn'])
                ->orderBy('name')
                ->get();
            $response['result'] = @$state;
            $response['status'] = 1;
        }

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!adminHasAccessToMenu(3)) {
            return redirect()->route('admin.dashboard');
        }
        // dd($User);
        $user = User::where('id', $id)->delete();
        session()->flash('success', 'Professional deleted successfully.');
        return redirect()->back();
    }

    /*
    * Method: status
    * Description: Status changed of User
    * Author: Abhisek mirored From Vedicastroworld
    */
    public function status($id, $status)
    {
        if (!adminHasAccessToMenu(3)) {
            return redirect()->route('admin.dashboard');
        }
        $User = User::find($id);

        if (@$User == null) {
            session()->flash('error', 'Unauthorized access.');
            return redirect()->back();
        }
        if (@$status == 'Approve') {
            if ($User->is_professional == "Y" && @$User->professional_id == null) {
                if($User->country_id == 30){
                    if (env('PAYMENT_ENVIORNMENT') == 'live') {
                        $moip = new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_PRODUCTION);
                    } else if (env('PAYMENT_ENVIORNMENT') == 'sandbox') {
                        $moip = new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_SANDBOX);
                    }

                    $account = $moip->accounts()
                        ->setName(@$User->name)
                        ->setLastName(@$User->name)
                        ->setEmail(@$User->name)
                        ->setBirthDate(@$User->dob)
                        ->setTaxDocument(@$User->cpf_no)
                        ->setType('MERCHANT')
                        ->setTransparentAccount(true)
                        ->setPhone(@$User->area_code, @$User->mobile, @$User->countryName->phonecode)
                        ->addAlternativePhone(@$User->area_code, @$User->mobile, @$User->countryName->phonecode)
                        ->addAddress(@$User->street, @$User->street_number, @$User->district, @$User->city, substr(@$User->stateName->name, 0, 2), @$User->zipcode, @$User->complement, substr(@$User->countryName->name, 0, 3))
                        ->setCompanyPhone(@$User->area_code, @$User->countryName->mobile, @$User->countryName->phonecode)
                        ->create();

                    User::where('id', $User->id)->update([
                        'professional_id'               => @$account->getId(),
                        'professional_access_token'     => @$account->getAccessToken()
                    ]);
                }
            }

            // update status field to active
            User::where('id', $id)->update([
                'status'        => 'A',
                'is_approved'   => 'Y'
            ]);
            // send email to User
            Mail::send(new UserAccountApproval($User));

            session()->flash('success', 'Professionals account approved successfully.');
            return redirect()->back();
        } elseif (@$status == 'Active') {
            // update status field to inactive
            User::where('id', $id)->update(['status' => 'A']);

            session()->flash('success', 'Professional status updated successfully.');
            return redirect()->back();
        } elseif (@$status == 'Inactive') {
            // update status field to active
            User::where('id', $id)->update(['status' => 'I']);

            session()->flash('success', 'Professional status updated successfully.');
            return redirect()->back();
        } elseif (@$status == 'Resend') {
            // Send mail to Astrologer for verify email ID
            $vcode = md5(str_random(7));

            User::where('id', $id)->update([
                'vcode'        => $vcode,
                'is_email_verified' => 'N'
            ]);

            $reqData = User::find($id);

            Mail::send(new EmailVerify($reqData, $vcode));

            session()->flash('success', 'Email verification link sended successfully.');
            return redirect()->back();
        }
    }

    /*
    * Method: reject
    * Description: User remove from database
    * Author: Abhisek mirored From Vedicastroworld
    */
    public function reject($id)
    {
        if (!adminHasAccessToMenu(3)) {
            return redirect()->route('admin.dashboard');
        }
        $User = User::find($id);
        // User reject and send mail to user for reject users account
        // Mail send to user for reject mail
        Mail::send(new UserAccountReject($User));
        // delete from User
        User::destroy($id);



        session()->flash('success', 'Professional successfully rejected.');
        return redirect()->back();
    }

    /*
    * Method: reject1
    * Description: Professional remove from database
    * Author: Abhisek mirored From Vedicastroworld
    */
    public function reject1($id)
    {
        if (!adminHasAccessToMenu(3)) {
            return redirect()->route('admin.dashboard');
        }
        $User = User::find($id);
        // User reject and send mail to user for reject users account

        // delete from User
        User::destroy($id);

        // Mail send to user for reject mail
        Mail::send(new UserAccountReject($User));

        session()->flash('success', 'Professional successfully rejected.');
        return redirect()->route('professional.index');
    }



    /*
    * Method: updateToCategory
    * Description: update UserToCategory table based on user_id
    * Author: Abhisek mirored From Vedicastroworld
    */
    private function updateToCategory($id, $cat = [], $sub = [])
    {
        // old category id
        $old_category = UserToCategory::where('user_id', $id)->delete();
        $i = $j = 0;
        if (sizeof($cat) > 0) {
            for ($i = 0; $i < sizeof($cat); $i++) {
                if (@$cat[$i] != null || @$cat[$i] != "") {
                    UserToCategory::create([
                        'user_id'       =>  $id,
                        'category_id'   =>  $cat[$i],
                        'level'         =>  0
                    ]);
                }
            }
        }
        // dd($sub);
        if (sizeof($sub) > 0) {

            for ($j = 0; $j < sizeof($sub); $j++) {
                if (@$sub[$j] != null || @$sub[$j] != "") {
                    UserToCategory::create([
                        'user_id'       =>  $id,
                        'category_id'   =>  $sub[$j],
                        'level'         =>  1
                    ]);
                }
            }
        }
        return true;
    }

    public function check_email(Request $request)
    {
        $email = $request->email;
        if($email) {
            $user = Admin::where('email', $email)->first();
            if ($user != null) {
               return "false";
            } else {
               return "true";
            }
        }

    }

    /*
    * Route: admin.check.email.change
    * Method: checkEmailChange
    * Description: For email checking with out user_id
    * Author: Abhisek
    */
    public function checkEmailChange(Request $request)
    {
        if (@$request->params['email'] && $request->params['id']) {
            $response = [
                "jsonrpc"   =>  "2.0"
            ];

            $find_email = User::where('email', $request->params['email'])
                ->whereNotIn('id', [$request->params['id']])
                ->first();

            $find_email1 = User::where('alter_email', $request->params['email'])
                ->whereNotIn('id', [$request->params['id']])
                ->first();

            if (@$find_email == null && @$find_email1 == null) {
                $response['status'] = 1;
            } else {
                $response['status'] = 0;
            }
            return response()->json($response);
        }
    }

    /*
    * Route: admin.check.mobile.change
    * Method: checkMobileChange
    * Description: For mobile checking with out user_id
    * Author: Abhisek
    */
    public function checkMobileChange(Request $request)
    {
        if ($request->params['mobile']) {
            $response = [
                "jsonrpc"   =>  "2.0"
            ];

            $find_mobile = User::where('mobile', $request->params['mobile'])
                ->whereNotIn('id', [$request->params['id']])
                ->first();

            $find_mobile1 = User::where('alter_mobile', $request->params['mobile'])
                ->whereNotIn('id', [$request->params['id']])
                ->first();

            if (@$find_mobile == null && @$find_mobile1 == null) {
                $response['status'] = 1;
            } else {
                $response['status'] = 0;
            }
            return response()->json($response);
        }
    }

    /*
    * Method: home
    * Description: shown in home page of User
    * Author: Abhisek
    */
    public function home($id)
    {
        if (!adminHasAccessToMenu(3)) {
            return redirect()->route('admin.dashboard');
        }
        $User = User::find($id);

        if (@$User == null) {
            session()->flash('error', 'Unauthorized access.');
            return redirect()->back();
        }

        if (@$User->shown_in_home == 'Y') {
            // update status field to active
            User::where('id', $id)->update(['shown_in_home' => 'N']);

            session()->flash('success', 'Now Professional is not showned in homepage.');
            return redirect()->back();
        } else if (@$User->shown_in_home == 'N') {
            // update status field to active
            User::where('id', $id)->update(['shown_in_home' => 'Y']);

            session()->flash('success', 'Now Professional is showned in homepage.');
            return redirect()->back();
        }
    }

    /*
    * Method: recommended
    * Description: shown in recommended of User
    * Author: Abhisek
    */
    public function recommended($id)
    {
        if (!adminHasAccessToMenu(3)) {
            return redirect()->route('admin.dashboard');
        }
        $User = User::find($id);

        if (@$User == null) {
            session()->flash('error', 'Unauthorized access.');
            return redirect()->back();
        }

        if (@$User->is_recommended == 'Y') {
            // update status field to active
            User::where('id', $id)->update(['is_recommended' => 'N']);

            session()->flash('success', 'Astrologer not recommended updated successfully.');
            return redirect()->back();
        } else if (@$User->is_recommended == 'N') {
            // update status field to active
            User::where('id', $id)->update(['is_recommended' => 'Y']);

            session()->flash('success', 'Astrologer recommended updated successfully.');
            return redirect()->back();
        }
    }

    /*
    * Method: payout_astrologer
    * Description: For paying User
    * Author: Abhisek
    */

    public function payout_astrologer(Request $request)
    {
        $request->validate([
            "payout"    =>  "required",
            "notes"     =>  "required"
        ]);
        $user = User::where("id", $request->astro_id)->first();
        if ($user == null) {
            session()->flash("error", "Unauthorize access");
            return redirect()->back();
        }
        if ($request->payout > $user->wallet_balance) {
            session()->flash("error", "Payble balance can't be greater than wallet balance");
            return redirect()->back();
        }
        $user->wallet_balance = $cur_bal = $user->wallet_balance - $request->payout;
        $user->tot_paid = $request->payout + $user->tot_paid;
        $name = $user->name;
        $data = [
            "user_id"       =>  $user->id,
            "currency_id"   =>  $user->currency_id,
            "date"          =>  date('Y-m-d'),
            "amount"        =>  @$request->payout,
            "balance"       =>  $cur_bal,
            "description"   =>  @$request->notes,
            "type"          =>  "O",
            "category"      =>  "P"
        ];
        $is_created = UserToWallet::create($data);
        Mail::send(new PayoutMail($user, @$request->payout));
        $is_saved = $user->save();
        if ($is_created) {
            session()->flash("success", $name . " paid successfully.");

            return redirect()->back();
        } else {
            session()->flash("error", "Something went be wrong.");
            return redirect()->back();
        }
    }

    public function payouts(Request $request)
    {
        $user = UserToWallet::with('userDetails')
            ->where([
                "category"  =>  "P",
                "type"      =>  "O"
            ]);
        $key = [];
        if ($request->all()) {
            if ($request->name) {
                $user = $user->whereHas("userDetails", function ($query) use ($request) {
                    $query = $query->where("name", 'like', '%' . $request->name . '%');
                });
            }
            if ($request->amount) {
                $user = $user->where("amount", ">=", $request->amount);
            }
            if ($request->from_date || $request->to_date) {

                $to_date = $request->to_date ? date("Y-m-d", strtotime($request->to_date)) : date("Y-m-d");
                $from_date = $request->from_date ? date("Y-m-d", strtotime($request->from_date)) : date("Y-m-d");
                $user = $user->whereBetween("date", array($from_date, $to_date));
            }
        }
        $key = $request->all();
        $user = $user->orderBy("id", "desc")->get();
        // dd($user);
        $astrologer_name = User::where("user_type", "A")->orderBy("name")->get();
        return view("admin.modules.payouts.payouts")->with([
            "user"          =>  $user,
            "key"           =>  $key,
            "astro_name"    =>  $astrologer_name
        ]);
    }

    /*
    * Method: review
    * Description: shown review page
    * Author: Abhisek
    */
    public function review($id)
    {
        $User = User::find($id);

        if (@$User == null) {
            session()->flash('error', 'Unauthorized access.');
            return redirect()->back();
        }

        // $review = Review::where(['astrologer_id' => $id])->get();
        // $review = $review->load(['customerName']);

        return view('admin.modules.user.astrologer_review', [
            'astrologer_id'     => @$id,
            'astrologer_name'   => @$User->name/*,
            'review'            => @$review*/
        ]);
    }

    /*
    * Method: reviewDelete
    * Description: review delete
    * Author: Abhisek
    */
    public function reviewDelete($id)
    {
        // dd($id);
        $review = Review::find($id);

        if (@$review == null) {
            session()->flash('error', 'Unauthorized access.');
            return redirect()->back();
        }

        // find User data
        $astro = User::find($review->astrologer_id);

        // update User data
        $update['total_review_point'] = ($astro->total_review_point - $review->review_point);
        $update['total_review'] = ($astro->total_review - 1);
        if ($update['total_review'] == 0) {
            $update['avg_review'] = 0;
        } else {
            $update['avg_review'] = (($astro->total_review_point - $review->review_point) / ($astro->total_review - 1));
        }

        User::where('id', $astro->id)->update($update);

        Review::destroy($id);

        session()->flash("success", "Review successfully deleted.");

        return redirect()->back();
    }

    /*
    * Method: reviewStore
    * Description: review store
    * Author: Abhisek
    */
    public function reviewStore(Request $request)
    {
        // for validation
        $request->validate([
            "title"         =>  "required",
            "description"   =>  "required",
            "rateing"       =>  "required",
            "customer_name" =>  "required"
        ]);

        // Create new fack user
        $new['name'] = $request->customer_name;
        $new['user_type'] = 'C';
        $new['email'] = '';
        $new['password'] = '';

        $customer_id = User::create($new)->id;

        // update email id for unique email
        User::where('id', $customer_id)->update(['email' => $customer_id]);

        $data = [
            "astrologer_id" =>  $request->astrologer_id,
            "customer_id"   =>  $customer_id,
            // "order_id"      =>  $request->order_id,
            "date"          =>  date('Y-m-d'),
            "title"         =>  $request->title,
            "description"   =>  nl2br($request->description),
            "review_point"  =>  $request->rateing
        ];

        // for create new row
        Review::create($data);

        // find User data
        $astro = User::find($request->astrologer_id);

        // update User data
        $update['total_review_point'] = ($astro->total_review_point + $request->rateing);
        $update['total_review'] = ($astro->total_review + 1);
        $update['avg_review'] = (($astro->total_review_point + $request->rateing) / ($astro->total_review + 1));

        User::where('id', $astro->id)->update($update);

        session()->flash("success", "Review successfully posted");

        return redirect()->back();
    }

    /*
    * Method: membership
    * Description: free membership update
    * Author: Abhisek
    */
    public function membership($id)
    {
        // dd($id);
        $astro = User::find($id);

        if (@$astro == null) {
            session()->flash('error', 'Unauthorized access.');
            return redirect()->back();
        }

        // update User data
        $update['activation_date'] = date('Y-m-d');
        // $update['expiry_date'] = date('Y-m-d', strtotime('+1 year'));
        $update['subscription_status'] = 'A';
        if (@$astro->expiry_date) {
            $update['expiry_date'] = date('Y-m-d', strtotime('+1 year', strtotime($astro->expiry_date)));
        } else {
            $update['expiry_date'] = date('Y-m-d', strtotime('+1 year'));
        }

        User::where('id', $astro->id)->update($update);

        session()->flash("success", "Free membership successfully updated.");

        return redirect()->back();
    }

    /*
    * Method: upgradeUser
    * Description: for upgrade an User to Professional
    * Author: Abhisek
    */
    public function upgradeUser($id)
    {
        // dd($id);
        $user = User::find($id);

        if (@$user == null) {
            session()->flash('error', 'Unauthorized access.');
            return redirect()->back();
        } else {
            $is_update = User::where('id', $id)->update([
                'is_professional'                       =>  'Y',
                'initiate_professionalism'              =>  'Y',
                'is_approved'                           =>  'Y',
                'status'                                =>  'A',
                'became_professional'                   =>  'N',
                'user_type'                             =>  'P'
            ]);
            session()->flash('success', $user->name . ' is now an Professional.');
            return redirect()->back();
        }
    }

    public function createProfessionalAccount(Request $request)
    {
        if (!adminHasAccessToMenu(3)) {
            return redirect()->route('admin.dashboard');
        }
        $User = User::find($request->profid);
        if (@$User->is_professional == "Y") {
            if($User->country_id == 30){
                try {
                    if( $User->professional_access_token != null){
                        // creating wirecard account for professional
                    // $moip = new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_SANDBOX);
                    if (env('PAYMENT_ENVIORNMENT') == 'live') {
                        $moip = new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_PRODUCTION);
                    } else if (env('PAYMENT_ENVIORNMENT') == 'sandbox') {
                        $moip = new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_SANDBOX);
                    }
                    $name = explode(' ', @$User->name);
                    $nc = "";
                    for ($i = 1; $i < sizeof($name); $i++) {
                        if ($i > 1) {
                            $nc .= ' ' . @$name[$i];
                        } else {
                            $nc .= @$name[$i];
                        }
                    }
                    $account = $moip->accounts()
                        ->setName(@$name[0])
                        ->setLastName(@$nc)
                        ->setEmail(@$User->email)
                        ->setBirthDate(@$User->dob)
                        ->setTaxDocument(@$User->cpf_no)
                        ->setType('MERCHANT')
                        ->setTransparentAccount(true)
                        ->setPhone(@$User->area_code, @$User->mobile, @$User->countryName->phonecode)
                        ->addAlternativePhone(@$User->area_code, @$User->mobile, @$User->countryName->phonecode)
                        ->addAddress($User->street_name, @$User->street_number, @$User->district, @$User->city, substr(@$User->stateName->name, 0, 2), @$User->zipcode, @$User->complement, substr(@$User->countryName->name, 0, 3))
                        ->create();
                    if (!@$account->getId()) {
                        session()->flash('error', 'Getting an error from wirecard. Check professional information carefully and try again.');
                        return redirect()->back();
                    }
                    // updating professional status.
                    // Now creating bank account.....
                    $ret = $this->createBankAccount(@$User->id, @$account->getId(), @$account->getAccessToken());
                    if ($ret == 0) {
                        session()->flash('error', 'Getting an error from wirecard. Check professional information carefully and try again.');
                        return redirect()->back();
                    }
                    User::where('id', $User->id)->update([
                        'professional_id'               => @$account->getId(),
                        'professional_access_token'     => @$account->getAccessToken(),
                        'status'                        => 'A',
                        'is_approved'                   => 'Y'
                    ]);
                    }else{
                        User::where('id', $User->id)->update([
                            'status'                        => 'A',
                            'is_approved'                   => 'Y'
                        ]);
                    }
                    // send email to User
                    try {
                        Mail::send(new UserAccountApproval($User));
                    } catch (\Exception $e) {
                        session()->flash('error', 'Professional account approved successfully, currently we are unable to send the mail to professional email account due to SMTP server issue.');
                        return redirect()->back();
                    }
                    session()->flash('success', 'Professional account approved successfully.');
                    return redirect()->back();
                } catch (\Exception $e) {
                    // dd($e->getMessage());
                    session()->flash('error', 'Getting an error from wirecard. Check professionals Bank identification code, bank name, bank account number etc carefully and try again.');
                    return redirect()->back();
                }
            } else {
                User::where('id', $User->id)->update([
                    'status'                        => 'A',
                    'is_approved'                   => 'Y'
                ]);
                session()->flash('success', 'Professional account approved successfully.');
                return redirect()->back();
            }
        }
    }


    /**
     *method: createbankaccount()
     *purpose: For creating bank account
     */

    public function createBankAccount($user_id, $id, $token)
    {
        $bank = UserToBankAccount::where('user_id', $user_id)->first();
        $user = User::where('id', $user_id)->first();
        if (env('PAYMENT_ENVIORNMENT') == 'live') {
            // $moip = new Moip(new OAuth($token), Moip::ENDPOINT_PRODUCTION . '/v2/accounts/' . $id . '/bankaccounts');
            $moip = new Moip(new OAuth($token), Moip::ENDPOINT_PRODUCTION);
        } else if (env('PAYMENT_ENVIORNMENT') == 'sandbox') {
            // $moip = new Moip(new OAuth($token), Moip::ENDPOINT_SANDBOX . '/v2/accounts/' . $id . '/bankaccounts');
            $moip = new Moip(new OAuth($token), Moip::ENDPOINT_SANDBOX);
        }
        $bankAccount = $moip->bankaccount()
            ->setBankNumber(@$bank->bank_number)
            ->setAgencyNumber(@$bank->agencyNumber)
            ->setAgencyCheckNumber(@$bank->agencyCheckNumber)
            ->setAccountNumber(@$bank->accountNumber)
            ->setAccountCheckNumber(@$bank->accountCheckNumber) //commented for client requirement
            ->setType(@$bank->account_type)
            ->setHolder(@$user->name, @$user->cpf_no, 'CPF')
            ->create($id);
        // dd($bankAccount);
        if (!@$bankAccount->getId()) {
            return 0;
        } else {
            UserToBankAccount::where('user_id', $user_id)->update([
                'account_id'        => @$bankAccount->getId(),
                'bank_number'       => @$bankAccount->getBankNumber()
            ]);
            return 1;
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function sendRemainder($id)
    {
        if (!adminHasAccessToMenu(3)) {
            return redirect()->route('admin.dashboard');
        }
        $user = User::where('id',$id)->first();
        if (@$user == null) {
            session()->flash('error', 'Unauthorized access.');
            return redirect()->back();
        }
        Mail::send(new SendRemainder($user));
        session()->flash('success', 'Remainder Send User');
        return redirect()->back();
    }

    /*
    * Method: editProfessionalCategory
    * Description: for editing professional category
    * Author: Sayantani
    */
    public function editProfessionalCategory($id)
    {
        if (!adminHasAccessToMenu(3)) {
            return redirect()->route('admin.dashboard');
        }
        // dd($request->all());
        $user = User::where([
            'id'                =>  $id,
            'is_professional'   =>  "Y"
        ])->first();

        if ($user == null) {
            session()->flash('error', 'Unauthorized access.');
            return redirect()->back();
        }

        $categories = Category::where('parent_id', 0)->with('childCat')->get();
        $userCategories = UserToCategory::with('categoryName.parent')->where('level', 1)->where('user_id', $user->id)->get();

        return view('admin.modules.user.edit_prof_category')->with([
            'user'             => @$user,
            'userCategories'   => @$userCategories,
            'categories'       => @$categories,
        ]);
    }

    /*
    * Method: addProfessionalCategory
    * Description: for adding professional category
    * Author: Sayantani
    */
    public function addProfessionalCategory(Request $request)
    {
        if (!adminHasAccessToMenu(3)) {
            return redirect()->route('admin.dashboard');
        }
        // dd($request->all());
        $user = User::where([
            'id'                =>  $request->user_id,
            'is_professional'   =>  "Y"
        ])->first();

        if ($user == null) {
            session()->flash('error', 'Unauthorized access.');
            return redirect()->back();
        }

        $request->validate([
            'category'    => 'required',
            'subcategory' => 'required',
        ]);
        $exists = UserToCategory::where([
            'category_id' => $request->subcategory,
            'user_id' => $user->id
        ])->first();

        if($exists == null){
            $findCat = UserToCategory::where([
                'category_id' => $request->category,
                'user_id' => $user->id
            ])->exists();

            if (!$findCat) {
                $userToCat = UserToCategory::create([
                    'user_id' => $user->id,
                    'category_id' => $request->category,
                    'level' => 0
                ]);
            }

            $subCat = $request->subcategory;
            if ($subCat) {
                $findCat1 = UserToCategory::where([
                    'category_id'   =>  $subCat,
                    'user_id'       =>  $user->id
                ])->first();

                if ($findCat1 == null) {

                    $userToSubCat = UserToCategory::create([
                        'user_id' => $user->id,
                        'category_id' => $subCat,
                        'level' => 1,
                        'parent_id' => $request->category
                    ]);
                }
            }
        } else {
            return redirect()->back()->with('error', 'Record already exists.');
        }

        session()->flash("success", "Professional profile updated successfully.");
        return redirect()->back();
    }

    /*
    * Method: deleteProfessionalCategory
    * Description: for deleting professional category
    * Author: Sayantani
    */
    public function deleteProfessionalCategory($id, $uid)
    {
        $toDelCat = UserToCategory::where([
            'category_id' => $id,
            'user_id' => $uid
        ])->first();

        if(@$toDelCat){
            $parentChildenExists = UserToCategory::where('id', '!=', $toDelCat->id)
                                                    ->where('parent_id', $toDelCat->parent_id)
                                                    ->where('user_id', $uid)
            ->first();
            if(@$parentChildenExists){
                $toDelCat->delete();
            } else {
                $parentRecord = UserToCategory::where('category_id', $toDelCat->parent_id)
                                                ->where('user_id', $uid)
                ->first();
                @$parentRecord->delete();
                $toDelCat->delete();
            }
            return redirect()->back()->with('success', "Record deleted.");
        }
        return redirect()->back()->with('error', "Record not found.");
    }

    /*
    * Method: editProfessionalExperience
    * Description: for editing professional experience
    * Author: Sayantani
    */
    public function editProfessionalExperience($id)
    {
        if (!adminHasAccessToMenu(3)) {
            return redirect()->route('admin.dashboard');
        }
        // dd($request->all());
        $user = User::where([
            'id'                =>  $id,
            'is_professional'   =>  "Y"
        ])->first();

        if ($user == null) {
            session()->flash('error', 'Unauthorized access.');
            return redirect()->back();
        }

        $my_experiences = $user->getExperiences()->get();
        $experiences = Experience::get();

        return view('admin.modules.user.edit_prof_experience')->with([
            'user'             => @$user,
            'my_experiences'   => @$my_experiences,
            'experiences'      => @$experiences,
        ]);
    }

    /*
    * Method: addProfessionalExperience
    * Description: for adding professional experience
    * Author: Sayantani
    */
    public function addProfessionalExperience(Request $request)
    {
        if (!adminHasAccessToMenu(42)) {
            return redirect()->route('admin.dashboard');
        }
        // dd($request->all());
        $user = User::where([
            'id'                =>  $request->user_id,
            'is_professional'   =>  "Y"
        ])->first();

        if ($user == null) {
            session()->flash('error', 'Unauthorized access.');
            return redirect()->back();
        }

        $request->validate([
            'experience'    => 'required',
        ]);
        $flag = 0;
        $experiences = $user->getExperiences()->get();
        foreach($experiences as $exp){
            if($exp->id == $request->experience){
                $flag = 1;
            }
        }
        if($flag == 0){
            $user->getExperiences()->attach($request->experience);
            return redirect()->back()->with('success', "Experience added succesfully");
        } else {
            return redirect()->back()->with('error', "Experience already exists");
        }
    }

    /*
    * Method: deleteProfessionalExperience
    * Description: for deleting professional experience
    * Author: Sayantani
    */
    public function deleteProfessionalExperience($id,$uid)
    {
        $user = User::where('id', $uid)->first();
        $flag = 0;
        if(@$user){
            $experiences = $user->getExperiences()->get();
            foreach($experiences as $exp){
                if($exp->id == $id){
                    $flag = 1;
                }
            }
            if($flag == 1){
                $user->getExperiences()->detach($id);
                return redirect()->back()->with('success', "Experience removed succesfully");
            } else {
                return redirect()->back()->with('error', "Experience not found");
            }
        } else {
            return redirect()->back()->with('error', "Something went wrong");
        }
    }

    /*
    * Method: editProfessionalQualification
    * Description: for editing professional qualification
    * Author: Sayantani
    */
    public function editProfessionalQualification($id)
    {
        if (!adminHasAccessToMenu(3)) {
            return redirect()->route('admin.dashboard');
        }
        // dd($request->all());
        $user = User::where([
            'id'                =>  $id,
            'is_professional'   =>  "Y"
        ])->first();

        if ($user == null) {
            session()->flash('error', 'Unauthorized access.');
            return redirect()->back();
        }

        $qualifications = ProfessionalToQualification::where('user_id', $id)->get();

        return view('admin.modules.user.edit_prof_qualification')->with([
            'user'             => @$user,
            'qualifications'   => @$qualifications,
        ]);
    }

    /*
    * Method: addProfessionalQualification
    * Description: for adding professional qualification
    * Author: Sayantani
    */
    public function addProfessionalQualification(Request $request, $id)
    {
        if (!adminHasAccessToMenu(3)) {
            return redirect()->route('admin.dashboard');
        }
        $user = User::where([
            'id'                =>  $id,
            'is_professional'   =>  "Y"
        ])->first();

        if ($user == null) {
            session()->flash('error', 'Unauthorized access.');
            return redirect()->back();
        }

        if($request->all()){
            $request->validate([
                'user_id'           =>  'required',
                'degree'            =>  'required',
                'university'        =>  'required',
                'from_month'        =>  'required',
                'from_year'         =>  'required',
                'certificate'       =>  'required',
            ]);

            $exists = ProfessionalToQualification::where('user_id', $user->id)->where('from_year', $request->from_year)->where('from_month', $request->from_month)->first();
            if(@$exists){
                session()->flash("error", "Professional qualification already exists.");
                return redirect()->back();
            }
            $insert['user_id'] = $user->id;
            $insert['degree'] = $request->degree;
            $insert['university'] = $request->university;
            $insert['from_month'] = $request->from_month;
            $insert['from_year'] = $request->from_year;

            $qual = ProfessionalToQualification::create($insert);

            if (@$qual->id && @$request->certificate) {
                $name = $qual->id . '-' . time() . str_random(4) . '.' . $request->certificate->getClientOriginalExtension();
                $path = "storage/app/public/uploads/user_qualifiaction";
                $request->certificate->move($path, $name);

                $qual->update([
                    'attachment'    =>  $name
                ]);
            }

            session()->flash("success", "Professional qualification added successfully.");
            return redirect()->back();
        }

        return view('admin.modules.user.add_prof_qualification')->with([
            'user' => @$user,
        ]);
    }

    /*
    * Method: updateProfessionalQualification
    * Description: for adding professional qualification
    * Author: Sayantani
    */
    public function updateProfessionalQualification(Request $request, $id)
    {
        if (!adminHasAccessToMenu(3)) {
            return redirect()->route('admin.dashboard');
        }

        $qualification = ProfessionalToQualification::where('id', $id)->first();
        if($qualification == null){
            return redirect()->back()->with('error', 'Qualification record not found.');
        }

        $user = User::where([
            'id'                =>  $qualification->user_id,
            'is_professional'   =>  "Y"
        ])->first();
        if ($user == null) {
            session()->flash('error', 'Unauthorized access.');
            return redirect()->back();
        }

        if($request->all()){
            $request->validate([
                'user_id'           =>  'required',
                'degree'            =>  'required',
                'university'        =>  'required',
                'from_month'        =>  'required',
                'from_year'         =>  'required',
                'certificate'       =>  'nullable',
            ]);

            $update['user_id'] = $user->id;
            $update['degree'] = $request->degree;
            $update['university'] = $request->university;
            $update['from_month'] = $request->from_month;
            $update['from_year'] = $request->from_year;

            $qual = ProfessionalToQualification::where('id', $request->id)->first();
            if($qual == null){
                return redirect()->back()->with('error', 'Qualification record not found.');
            }
            $qual->update($update);
            if (@$qual->id && @$request->certificate) {
                if (file_exists($qual->attachment)) {
                    unlink('storage/app/public/uploads/user_qualifiaction/' . @$qual->attachment);
                }
                $name = $qual->id . '-' . time() . str_random(4) . '.' . $request->certificate->getClientOriginalExtension();
                $path = "storage/app/public/uploads/user_qualifiaction";
                $request->certificate->move($path, $name);

                $qual->update([
                    'attachment'    =>  $name
                ]);
            }

            session()->flash("success", "Professional qualification updated successfully.");
            return redirect()->back();
        }

        return view('admin.modules.user.update_prof_qualification')->with([
            'user' => @$user,
            'qualification' => @$qualification,
        ]);
    }

    /*
    * Method: deleteProfessionalQualification
    * Description: for deleting professional qualification
    * Author: Sayantani
    */
    public function deleteProfessionalQualification($id)
    {
        $record = ProfessionalToQualification::where('id', $id)->first();
        if(@$record){
            if (file_exists($record->attachment)) {
                unlink('storage/app/public/uploads/user_qualifiaction/' . @$record->attachment);
            }
            $record->delete();
            return redirect()->back()->with('success', "Qualifcation record deleted succesfully");
        }
        return redirect()->back()->with('success', "Qualifcation record not found");
    }

    /*
    * Method: checkNickName
    * Description: for nick name duplicate checking
    * Author: Sayantani
    */
    public function checkNickName(Request $request)
    {
        $response = [
            'jsonrpc'   =>  '2.0'
        ];
        if ($request->params['name']) {
            $user = User::where('nick_name', $request->params['name'])
                ->where('id', '!=', Auth::id())
                ->first();
            if ($user != null) {
                $response['status'] = 1;
            } else {
                $response['status'] = 0;
            }
        }
        return response()->json($response);
    }



    /**
     * description: or edit bank account of professional
     */
    public function bankAccount(Request $request,$id)
    {
        if (!adminHasAccessToMenu(3)) {
            return redirect()->route('admin.dashboard');
        }
        $user = User::with('userDetails.categoryName')->find($id);
        // dd($user);
        if (@$user == null) {
            \Session::flash('error', 'Unauthorized access.');
            return redirect()->back();
        }
        if ($request->all()) {
            $request->validate(
                [
                    'bank_name'                 =>  'required',
                    'account_number'            =>  'required',
                    'agency_number'             =>  'required',
                    'bank_number'               =>  'required',
                    'account_check_number'      =>  'required',
                    'account_type'              =>  'required',
                ],
                [
                    'bank_name.required' =>  'Nome do banco é obrigatório',
                    'account_number.required' =>  'Número da conta é obrigatório',
                    'agency_number.required' =>  'Número da agência é obrigatório',
                    'bank_number.required' =>  'Número do banco é obrigatório',
                    'account_check_number'      =>  'required',
                    'account_type.required' =>  'O tipo de conta é obrigatório',
                ]
            );
            // $user = User::where('id', Auth::id())->first();
            if ($user->professional_access_token != null) {
                $bank = UserToBankAccount::where('user_id', $user->id)->first();
                $token = $user->professional_access_token;
                if (env('PAYMENT_ENVIORNMENT') == 'live') {
                    // $moip = new Moip(new OAuth($token), Moip::ENDPOINT_PRODUCTION . '/v2/accounts/' . $id . '/bankaccounts');
                    $moip = new Moip(new OAuth($token), Moip::ENDPOINT_PRODUCTION);
                } else if (env('PAYMENT_ENVIORNMENT') == 'sandbox') {
                    // $moip = new Moip(new OAuth($token), Moip::ENDPOINT_SANDBOX . '/v2/accounts/' . $id . '/bankaccounts');
                    $moip = new Moip(new OAuth($token), Moip::ENDPOINT_SANDBOX);
                }
                $bankAccount = $moip->bankaccount()
                    ->setBankNumber(@$request->bank_number)
                    ->setAgencyNumber(@$request->agency_number)
                    ->setAgencyCheckNumber(0)
                    ->setAccountNumber(@$request->account_number)
                    ->setAccountCheckNumber(@$request->account_check_number) //commented for client requirement
                    ->setType(@$request->account_type)
                    ->setHolder(@$user->name, @$user->cpf_no, 'CPF')
                    ->create($user->professional_id);
                if (!@$bankAccount->getId()) {
                    session()->flash('error', 'Getting an error from wirecard. Check professional information carefully and try again.');
                    return redirect()->back();
                }
                UserToBankAccount::where('user_id', $user->id)->delete();
                $create = UserToBankAccount::create([
                    'user_id'                 =>  $user->id,
                    'bankName'                =>  $request->bank_name,
                    'accountNumber'           =>  $request->account_number,
                    'agencyNumber'            =>  $request->agency_number,
                    'bank_number'             =>  $request->bank_number,
                    'accountCheckNumber'      =>  @$request->account_check_number,
                    'account_type'            =>  $request->account_type,
                    'account_id'        => @$bankAccount->getId()
                ]);

                session()->flash('success', 'Bank Account Update successfully');
                return redirect()->route('admin.professional.edit.bank.account', ['id' => @$user->id]);
            }
            UserToBankAccount::where('user_id', $user->id)->delete();
            $create = UserToBankAccount::create([
                'user_id'                 =>  $user->id,
                'bankName'                =>  $request->bank_name,
                'accountNumber'           =>  $request->account_number,
                'agencyNumber'            =>  $request->agency_number,
                'bank_number'             =>  $request->bank_number,
                'accountCheckNumber'      =>  @$request->account_check_number,
                'account_type'            =>  $request->account_type
            ]);

            session()->flash('success', 'Bank Account Update successfully');
            return redirect()->route('admin.professional.edit.bank.account',['id'=> @$user->id]);
        }
        $bank = UserToBankAccount::where('user_id', @$user->id)->first();
        $bankList = Bank::orderBy('bank_name')->get();

        return view('admin.modules.user.edit_bank_account')->with([
            'bank'             => @$bank,
            'bankList'         => @$bankList,
            'user'             => @$user,
        ]);
    }

    /**
     *method: professionalCommission
     *created By: Sayantani
     *description: For professional commission for video and product
     */
    public function professionalCommission(Request $request, $id)
    {
        $data = array();
        $data['user'] = User::where('id', $id)->first();
        if(@$request->all()){
            $ins['video_commission'] = @$request->video_commission;
            $ins['chat_commission'] = @$request->chat_commission;
            $ins['product_commission'] = @$request->product_commission;
            $data['user']->update($ins);
            session()->flash('success', 'Commissions updated successfully');
        }
        return view('admin.modules.user.edit_professional_commission')->with($data);
    }

    /**
     *method: approveUser
     *created By: Sayantani
     *description: To approve a user
     */
    public function approveUser($id)
    {
        $user = User::where('id', $id)->first();
        if(@$user){
            $user->is_approved = 'Y';
            $user->save();
            Mail::send(new UserAccountApproval($user));
            session()->flash('success', 'User account approved successfully.');
        } else {
            session()->flash('error', 'User not found');
        }
        return redirect()->back();
    }

    /**
     *method: paypalAddress
     *created By: Sayantani
     *description: To update professional paypal address
     */
    public function paypalAddress(Request $request, $id){
        $user = User::where('id', $id)->first();
        if (@$request->all()) {
            $upd['paypal_address'] = @$request->paypal_address;
            $user->update($upd);
        }
        $data['user'] = $user;
        return view('admin.modules.user.edit_paypal')->with($data);
    }



    /**
     * method:  userExportListInactive
     *created By: Soumojit
     *Description:  Export Inactive user
     *Date: 14-APR-2021
     */
    public function userExportListInactive(Request $request){

        $prof = User::with('userDetails.categoryName', 'userDetails.parentCategoryName')
            ->where(function ($where) use ($request) {
                $where->whereIn('status', ['AA', 'I', 'U'])->orWhere('is_approved', 'N');
            });
        $key = [];

        if (@$request->all()) {
            if (@$request->keyword) {
                $prof = $prof->where(function ($where) use ($request) {
                    $where->where('name', 'like', '%' . $request->keyword . '%')
                        ->orWhere('nick_name', 'like', '%' . $request->keyword . '%')
                        ->orWhere('email', $request->keyword)
                        ->orWhere('mobile', $request->keyword);
                });
            }

            if (@$request->category) {
                $prof = $prof->whereHas('userDetails', function ($q) use ($request) {
                    $q->where('category_id', $request->category);
                });
            }

            if (@$request->subcategory) {
                $prof = $prof->whereHas('userDetails', function ($q) use ($request) {
                    $q->where('category_id', $request->subcategory);
                });
            }

            if (@$request->email) {
                $prof = $prof->where('email', $request->email);
            }

            if (@$request->status) {
                $prof = $prof->where('status', $request->status);
            }

            if (@$request->country) {
                $prof = $prof->where('country_id', $request->country);
            }

            if (@$request->is_company) {
                $prof = $prof->where('is_company', 'Y');
            }

            $key = $request->all();
        }
        $prof = $prof->orderBy('id', 'desc')->get();

        $data1 = '';
        $data1 .='<table><tr>';
        $data1 .='<th style="border:1px solid black;background-color:#1781d2">Name</th>';
        $data1 .='<th style="border:1px solid black;background-color:#1781d2">Mobile</th>';
        $data1 .='<th style="border:1px solid black;background-color:#1781d2">E-mail</th>';
        $data1 .='<th style="border:1px solid black;background-color:#1781d2">Category</th>';
        $data1 .='</tr>';
        foreach (@$prof  as $row) {
            $data1 .= '<tr>';
            if(@$row->nick_name){

                $data1 .=' <td style="border:1px solid black;">'. @$row->nick_name.'</td>';
            }else{

                $data1 .=' <td style="border:1px solid black;">'.@$row->name.'</td>';
            }
            if(@$row->mobile){

                $data1 .=' <td style="border:1px solid black;">'.' + '. @$row->country_code.' '.@$row->area_code.@$row->mobile .'</td>';
            }else{

                $data1 .=' <td style="border:1px solid black;"><center>N.A</center></td>';
            }
            $data1 .=' <td style="border:1px solid black;">'.@$row->email .'</td>';


            $data1 .='<td style="border:1px solid black;">';
            @$printedCat = [];
            $i=0;

            if(sizeof($row->userDetails)>0){
                foreach($row->userDetails as $rw){
                    if(@$rw->parentCategoryName!=null && !in_array(@$rw->parentCategoryName->id, @$printedCat)){
                        $printedCat[$i] = @$rw->parentCategoryName->id;
                        $data1 .=@$rw->parentCategoryName->name.'->'.@$rw->categoryName->name;

                        $data1 .='<br>';
                    }
                    else{

                        $data1 .='<br>';
                        $data1 .=@$rw->categoryName->parent_id>0 ? '->': '•';
                        $data1 .=@$rw->categoryName->name ;
                        @$printedCat[$i] = @$rw->categoryName->id;
                    }
                    $i++;
                }


            }
            else{

                $data1 .='<center>N.A</center>';
            }
            $data1 .='</td>';
            $data1 .= '</tr>';
        }
        $data1 .= '</table>';
        // return $data1;
        header("Content-Type: application/xls");
        header("Content-Disposition: attachment; filename=".rand(000000,999999).".xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo $data1;
    }
    /**
     * method:  userExportListActive
     *created By: Soumojit
     *Description:  Export Active user
     *Date: 14-APR-2021
     */
    public function userExportListActive(Request $request){

        if (!adminHasAccessToMenu(22)) {
            return redirect()->route('admin.dashboard');
        }
        $prof = User::with('userDetails.categoryName', 'userDetails.parentCategoryName')
            ->where('status', 'A')->where('is_approved', 'Y');
        $key = [];

        if (@$request->all()) {
            if (@$request->keyword) {
                $prof = $prof->where(function ($where) use ($request) {
                    $where->where('name', 'like', '%' . $request->keyword . '%')
                        ->orWhere('nick_name', 'like', '%' . $request->keyword . '%')
                        ->orWhere('email', $request->keyword)
                        ->orWhere('mobile', $request->keyword);
                });
            }

            if (@$request->category) {
                $prof = $prof->whereHas('userDetails', function ($q) use ($request) {
                    $q->where('category_id', $request->category);
                });
            }

            if (@$request->subcategory) {
                $prof = $prof->whereHas('userDetails', function ($q) use ($request) {
                    $q->where('category_id', $request->subcategory);
                });
            }

            if (@$request->email) {
                $prof = $prof->where('email', $request->email);
            }

            if (@$request->status) {
                $prof = $prof->where('status', $request->status);
            }

            if (@$request->country) {
                $prof = $prof->where('country_id', $request->country);
            }

            if (@$request->is_company) {
                $prof = $prof->where('is_company', 'Y');
            }

            $key = $request->all();
        }
        $prof = $prof->orderBy('id', 'desc')->get();

        $data1 = '';
        $data1 .='<table><tr>';
        $data1 .='<th style="border:1px solid black;background-color:#1781d2">Name</th>';
        $data1 .='<th style="border:1px solid black;background-color:#1781d2">Mobile</th>';
        $data1 .='<th style="border:1px solid black;background-color:#1781d2">E-mail</th>';
        $data1 .='<th style="border:1px solid black;background-color:#1781d2">Category</th>';
        $data1 .='</tr>';
        foreach (@$prof  as $row) {
            $data1 .= '<tr>';
            if(@$row->nick_name){

                $data1 .=' <td style="border:1px solid black;">'. @$row->nick_name.'</td>';
            }else{

                $data1 .=' <td style="border:1px solid black;">'.@$row->name.'</td>';
            }
            if(@$row->mobile){

                $data1 .=' <td style="border:1px solid black;">'.' + '. @$row->country_code.' '.@$row->area_code.@$row->mobile .'</td>';
            }else{

                $data1 .=' <td style="border:1px solid black;"><center>N.A</center></td>';
            }
            $data1 .=' <td style="border:1px solid black;">'.@$row->email .'</td>';


            $data1 .='<td style="border:1px solid black;">';
            @$printedCat = [];
            $i=0;

            if(sizeof($row->userDetails)>0){
                foreach($row->userDetails as $rw){
                    if(@$rw->parentCategoryName!=null && !in_array(@$rw->parentCategoryName->id, @$printedCat)){
                        $printedCat[$i] = @$rw->parentCategoryName->id;
                        $data1 .=@$rw->parentCategoryName->name.'->'.@$rw->categoryName->name;

                        $data1 .='<br>';
                    }
                    else{

                        $data1 .='<br>';
                        $data1 .=@$rw->categoryName->parent_id>0 ? '->': '•';
                        $data1 .=@$rw->categoryName->name ;
                        @$printedCat[$i] = @$rw->categoryName->id;
                    }
                    $i++;
                }


            }
            else{

                $data1 .='<center>N.A</center>';
            }
            $data1 .='</td>';
            $data1 .= '</tr>';
        }
        $data1 .= '</table>';
        // return $data1;
        header("Content-Type: application/xls");
        header("Content-Disposition: attachment; filename=".rand(000000,999999).".xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo $data1;
    }
}
