<?php

namespace App\Http\Controllers\Admin\Modules\Reviews;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\BlogCategory;
// use Session;
use Validator;
use Auth;
use App\User;
use App\Models\ReviewProduct;
use App\Models\Review;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use App\Models\Product;

class ReviewController extends Controller
{
    protected $redirectTo = '/admin/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }

    

    public function manageReveiws(Request  $request){

        $data['reviews'] = ReviewProduct::with('getProducts','getProducts.professional','getCustomer');

         if (@$request->all()) {
                
                
                if (@$request->user_name) {

                     $data['reviews'] = $data['reviews']->whereHas("getCustomer", function ($query) use ($request) {
                        $query->where("name", 'LIKE', '%' . $request->user_name . '%');
                    });
                }
                if (@$request->professional) {
                    $data['reviews'] = $data['reviews']->whereHas("getProducts.professional", function ($query) use ($request) {
                        $query->where('id', $request->professional);
                    });
                }
                
            }
            $keys = $request->all();

        $data['reviews'] = $data['reviews']->get();
        $data['expert'] = User::where("is_professional", "Y")->orderBy("name")->get();

         return view('admin.modules.review.manage_review')->with($data);

    }

    public function deleteReview($id){
        $review = ReviewProduct::where('id',$id)->first();

        $product = Product::where('id',@$review->product_id)->first();

        
        $result = $review->delete();

        if($result){

                $avg_rate = ReviewProduct::where('product_id',$product->id)->avg('rate');

                $total =  ReviewProduct::where('product_id',$product->id)->count();



                Product::where('id',$product->id)->update([

                    'avg_rating'=>$avg_rate,
                    'total_rating'=>$total,
            ]);

                session()->flash('success','Review deleted  successfully');
                return redirect()->back();


        }else{


            session()->flash('error', 'Something went wrong');
            return redirect()->back();
        }

    }
}
