<?php

namespace App\Http\Controllers\Admin\Modules\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AdminBankAccount;
class BankAccountController extends Controller
{
    //
    /**
     * For view refer discount
     * @method: index
     */
    public function index()
    {
        if (!adminHasAccessToMenu(17)) {
            return redirect()->route('admin.dashboard');
        }
        $bankAccount = AdminBankAccount::get();
        return view('admin.modules.bank_account.account')->with(['bankAccount'=> $bankAccount]);
    }
    /**
     * For view refer discount
     * @method: edit
     */
    public function edit($id)
    {
        if (!adminHasAccessToMenu(17)) {
            return redirect()->route('admin.dashboard');
        }
        $bankAccount = AdminBankAccount::where('id', $id)->first();
        return view('admin.modules.bank_account.edit_account')->with(['bankAccount' => $bankAccount]);
    }
    /**
     * For view refer discount
     * @method: update
     */
    public function update($id, Request $request)
    {
        if (!adminHasAccessToMenu(17)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            "bank_information" =>  "required",
            "instruction" =>  "required",
            "beneficiary" =>  "required",
            "agency" =>  "required",
            "current_account" => "required",
            "cnpj" =>  "required",
        ]);
        $updateDiscount = AdminBankAccount::where('id', $id)->update([
            'bank_information' => $request->bank_information,
            'instruction' => $request->instruction,
            'beneficiary' => $request->beneficiary,
            'agency' => $request->agency,
            'current_account' => $request->current_account,
            'cnpj' => $request->cnpj,
        ]);
        if (@$updateDiscount) {
            session()->flash("success", " Updated Information");
            return redirect()->route('admin.account');
        }
        session()->flash("error", "Information Not Updated");
        return redirect()->back();
    }
}
