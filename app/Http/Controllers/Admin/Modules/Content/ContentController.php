<?php

namespace App\Http\Controllers\Admin\Modules\Content;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Content;
use App\Models\Faq;
use App\Models\FooterContent;
use App\Models\HomeBanner;
use App\Models\MetaTag;
use App\Models\Newsletter;
use App\Models\TermsOfService;
use Exception;
use validator;
use App\Models\ReferContent;
use App\Models\HelpArticle;
use App\Models\HelpCategory;
use Illuminate\Support\Facades\Storage;

class ContentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }


    /**
     *@ Method 		: updateHowItWorks
     *@ Description 	: For update how it works content
     *@ Author 		: Soumen
     *@ Use By       : Soumen
     */

    public function updateHowItWorks(Request $request)
    {
        if (!adminHasAccessToMenu(40)) {
            return redirect()->route('admin.dashboard');
        }
        if ($request->all()) {
            $request->validate([
                "title"             =>  "required",
                "description"       =>  "required",
                "heading_1"         =>  "required",
                "description_1"     =>  "required",
                "heading_2"         =>  "required",
                "description_2"     =>  "required",
                "heading_3"         =>  "required",
                "description_3"     =>  "required",
                "heading_4"         =>  "required",
                "description_4"     =>  "required",
                "meet_title"        =>  "required",
                "meet_description"  =>  "required",
            ]);
            $updateArr = [
                "title"             =>  $request->title,
                "description"       =>  $request->description,
                "heading_1"         =>  $request->heading_1,
                "description_1"     =>  $request->description_1,
                "heading_2"         =>  $request->heading_2,
                "description_2"     =>  $request->description_2,
                "heading_3"         =>  $request->heading_3,
                "description_3"     =>  $request->description_3,
                "heading_4"         =>  $request->heading_4,
                "description_4"     =>  $request->description_4
            ];
            for ($i = 1; $i <= 4; $i++) {
                if ($request->hasFile('image_' . $i)) {
                    $file = $request->file('image_' . $i);
                    $fileName = time() . mt_rand(10000, 99999) . '.' . $file->extension();
                    $stored = $file->move('storage/app/public/uploads/content_images', $fileName);
                    $updateArr['image_' . $i] = $fileName;
                }
            }
            Content::where('id', 2)->update($updateArr);
            $updateArr2 = [
                "title"             =>  $request->meet_title,
                "description"       =>  $request->meet_description,
            ];
            Content::where('id', 5)->update($updateArr2);
            session()->flash("success", "Successfully updated contents.");
            return redirect()->back();
        }
        $howItWorks = Content::where("id", 2)->first();
        $meetNeeds = Content::where("id", 5)->first();
        return view("admin.modules.content.edit_how_it_works")->with(compact('meetNeeds', 'howItWorks'));
    }

    public function updateHomeExpert(Request $request)
    {
        if (!adminHasAccessToMenu(42)) {
            return redirect()->route('admin.dashboard');
        }
        if ($request->all()) {
            $request->validate([
                "title_1"             =>  "required",
                "description_1"       =>  "required",
                "title_2"             =>  "required",
                "description_2"       =>  "required",
                "heading_2_1"         =>  "required",
                "description_2_1"     =>  "required",
                "heading_2_2"         =>  "required",
                "description_2_2"     =>  "required",
                "heading_2_3"         =>  "required",
                "description_2_3"     =>  "required",
                "heading_2_4"         =>  "required",
                "description_2_4"     =>  "required",
                "title_3"             =>  "required",
                "description_3"       =>  "required",
            ]);
            $updateArr_1 = [
                "title"             =>  $request->title_1,
                "description"       =>  $request->description_1,
            ];
            Content::where("id", 7)->update($updateArr_1);
            $updateArr_2 = [
                "title"             =>  $request->title_2,
                "description"       =>  $request->description_2,
                "heading_1"         =>  $request->heading_2_1,
                "description_1"     =>  $request->description_2_1,
                "heading_2"         =>  $request->heading_2_2,
                "description_2"     =>  $request->description_2_2,
                "heading_3"         =>  $request->heading_2_3,
                "description_3"     =>  $request->description_2_3,
                "heading_4"         =>  $request->heading_2_4,
                "description_4"     =>  $request->description_2_4,
            ];
            if ($request->hasFile('image_2')) {
                $file = $request->file('image_2');
                $fileName = time() . mt_rand(10000, 99999) . '.' . $file->extension();
                $stored = $file->move('storage/app/public/uploads/content_images', $fileName);
                $updateArr_2['image'] = $fileName;
            }
            for ($i = 1; $i <= 4; $i++) {
                if ($request->hasFile('image_2_' . $i)) {
                    $file = $request->file('image_2_' . $i);
                    $fileName = time() . mt_rand(10000, 99999) . '.' . $file->extension();
                    $stored = $file->move('storage/app/public/uploads/content_images', $fileName);
                    $updateArr_2['image_' . $i] = $fileName;
                }
            }
            Content::where('id', 8)->update($updateArr_2);
            $updateArr_3 = [
                "title"             =>  $request->title_3,
                "description"       =>  $request->description_3,
            ];
            Content::where('id', 9)->update($updateArr_3);
            session()->flash("success", "Successfully updated contents.");
            return redirect()->back();
        }
        $exp1 = Content::where("id", 7)->first();
        $exp2 = Content::where("id", 8)->first();
        $exp3 = Content::where("id", 9)->first();
        return view("admin.modules.content.home_expert_section")->with(compact('exp1', 'exp2', 'exp3'));
    }

    /**
     *@ Method       : indexTermsOfServices
     *@ Description  : For showing terms of services content
     *@ Author       : Sayantani
     *@ Use By       : Sayantani
     */
    public function indexTermsOfServices(Request $request)
    {
        if (!adminHasAccessToMenu(48)) {
            return redirect()->route('admin.dashboard');
        }
        $terms_title = Content::where('id', 3)->first();
        $terms = TermsOfService::where('parent_id', 0)->get();
        if($request->has('title')){
            $terms_title->title = $request->title;
            $terms_title->save();
            session()->flash("success", "Title updated succesfully");
        }
        return view("admin.modules.content.terms_of_services.terms_or_services")->with([
            "terms"          =>  $terms,
            "terms_title"    =>  $terms_title
        ]);
    }

    /**
     *@ Method       : storeTermsOfServices
     *@ Description  : For adding terms of services content
     *@ Author       : Sayantani
     *@ Use By       : Sayantani
     */
    public function storeTermsOfServices(Request $request, $id=null)
    {
        if (!adminHasAccessToMenu(48)) {
            return redirect()->route('admin.dashboard');
        }
        $data = array();

        if ($request->all()) {
            $request->validate([
                "title"             =>  "required",
                "description"       =>  "required"
            ]);
            $ins['title'] = $request->title;
            $ins['description'] = $request->description;
            $arr = explode(" ",strtolower($request->title));
            $slug = implode("-", array_slice($arr, 0, 5, true));
            $ins['slug'] = $slug;

            if(@$id){
                $term = TermsOfService::where('id', $id)->first();
                $term->update($ins);
                session()->flash("success", "Successfully updated terms of services content");
            } else {
                $term = TermsOfService::create($ins);
                $term->slug = $term->slug."-".$term->id;
                $term->save();
                session()->flash("success", "Successfully added terms of services content");
            }
        }
        if(@$id) {
            $data['term'] = TermsOfService::where('id', $id)->first();
            $data['subterms'] = TermsOfService::where('parent_id', $id)->get();
        }
        return view("admin.modules.content.terms_of_services.store_terms_or_services")->with($data);
    }

    /**
     *@ Method       : deleteTermsOfServices
     *@ Description  : For adding terms of services content
     *@ Author       : Sayantani
     *@ Use By       : Sayantani
     */
    public function deleteTermsOfServices($id)
    {
        if (!adminHasAccessToMenu(48)) {
            return redirect()->route('admin.dashboard');
        }
        $term = TermsOfService::where('id', $id)->first();
        if(@$term){
            $term->delete();
            return redirect()->back()->with('success', 'Terms of service deleted succesfully.');
        } else {
            return redirect()->back()->with('error', 'Terms of service not found.');
        }
    }

    /**
     *@ Method       : storeSubtermsOfServices
     *@ Description  : For storing sub terms of services content
     *@ Author       : Sayantani
     *@ Use By       : Sayantani
     */
    public function storeSubtermsOfServices(Request $request, $tid, $sid=null)
    {
        if (!adminHasAccessToMenu(48)) {
            return redirect()->route('admin.dashboard');
        }
        $data = array();
        $data['term'] = TermsOfService::where('id', $tid)->first();
        if(@$data['term']){
            if ($request->all()) {
                $request->validate([
                    "title"             =>  "required",
                    "description"       =>  "required"
                ]);
                $ins['title'] = $request->title;
                $ins['description'] = $request->description;
                $ins['parent_id'] = $data['term']->id;

                if(@$sid){
                    $subterm = TermsOfService::where('id', $sid)->first();
                    $subterm->update($ins);
                    session()->flash("success", "Successfully updated sub term content");
                } else {
                    $subterm = TermsOfService::create($ins);
                    session()->flash("success", "Successfully added sub term content");
                }
                return redirect()->back();
            }
            if(@$sid) {
                $data['subterm'] = TermsOfService::where('id', $sid)->first();
            }
            return view("admin.modules.content.terms_of_services.store_subterms")->with($data);
        } else {
            return redirect()->back()->with('error', 'Terms of service not found.');
        }
    }

    /**
     *@ Method       : updatePrivacyPolicy
     *@ Description  : For update privacy policy content
     *@ Author       : Soumen
     *@ Use By       : Soumen
     */
    public function updatePrivacyPolicy(Request $request)
    {
        if (!adminHasAccessToMenu(49)) {
            return redirect()->route('admin.dashboard');
        }
        if ($request->all()) {
            $request->validate([
                "title"             =>  "required",
                "description"       =>  "required"
            ]);
            Content::where('id', 4)->update([
                "title"             =>  $request->title,
                "description"       =>  $request->description
            ]);
            session()->flash("success", "Successfully updated terms of services content");
        }
        $privacyPolicy = Content::where("id", 4)->first();
        return view("admin.modules.content.edit_privacy_policy")->with([
            "privacyPolicy"    =>  $privacyPolicy
        ]);
    }

    /**
     *@ Method       : updateFaq
     *@ Description  : For update FAQ content
     *@ Author       : pankaj
     *@ Use By       : pankaj
     */
    public function updateFaq(Request $request)
    {
        if (!adminHasAccessToMenu(50)) {
            return redirect()->route('admin.dashboard');
        }
        if ($request->all()) {
            $request->validate([
                "title"             =>  "required",
                // "description"       =>  "required",
                // "heading_2"         =>  "required",
                "heading_1"         =>  "required",
                // "heading_3"         =>  "required",
                // "description_1"     =>  "required",
            ]);
            Content::where('id', 1)->update([
                "title"             =>  $request->title,
                // "description"       =>  $request->description,
                "heading_2"         =>  $request->heading_2 ?? null,
                "heading_1"         =>  $request->heading_1,
                "heading_3"         =>  $request->heading_3 ?? null,
                // "description_1"     =>  $request->description_1,
            ]);
            session()->flash("success", "Successfully updated Faq content");
            return redirect()->back();
        }
        $faq = Content::where("id", 1)->first();
        $faqItemsU = Faq::where('type', 'USER')->orderBy('display_order', 'ASC')->get();
        $faqItemsE = Faq::where('type', 'EXPERT')->orderBy('display_order', 'ASC')->get();
        return view("admin.modules.content.edit_faq")->with(compact('faqItemsU', 'faqItemsE', 'faq'));
    }

    public function createFaq(Request $request)
    {
        if (!adminHasAccessToMenu(50)) {
            return redirect()->route('admin.dashboard');
        }
        if ($request->all()) {
            $request->validate([
                'type' => 'required|in:USER,EXPERT',
                'title' => 'required',
                'description' => 'required',
                'video' => 'nullable',
            ]);
            $position = Faq::where('type', $request->type)->max('display_order');
            $create = [
                'type' => $request->type,
                'title' => $request->title,
                'description' => $request->description,
                'display_order' => $position + 1,
            ];
            if(@$request->video){
                $url = @$request->video;
                parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
                $url_id = @$my_array_of_vars['v'];
                if($url_id == null){
                    $pieces = explode("/", $url);
                    $url_id = end($pieces);
                    if($url_id == null){
                        session()->flash('error', \Lang::get('client_site.wrong_url'));
                        return redirect()->back()->with($data);
                    }
                }
                $create['video'] = "https://www.youtube.com/watch?v=".$url_id;
            }
            Faq::create($create);
            session()->flash("success", "FAQ added successfully.");
            return redirect()->back();
        }
    }

    public function imageUploadFaq(Request $request)
    {
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $name = time() . uniqid() . '.' . $file->extension();
            $file->move(env('PATH_TO_FAQ_CONTENT_IMAGE'), $name);
            return response([
                'status' => 'SUCCESS',
                'location' => asset(env('PATH_TO_FAQ_CONTENT_IMAGE') . $name),
            ]);
        } else {
            return response([
                'status' => 'ERROR',
            ]);
        }
    }

    public function moveFaq($id, $dir)
    {
        try {
            $row = Faq::find($id);
            if ($row == null) {
                session()->flash('error', 'FAQ not found.');
                return redirect()->back();
            }
            if ($dir == 'UP') {
                $before = Faq::where('display_order', '<', $row->display_order)->where('type', $row->type)->orderBy('display_order', 'DESC')->first();
                if ($before == null) {
                    session()->flash('error', 'Cannot move any further in the upward direction.');
                    return redirect()->back();
                }
                $rowPos = $row->display_order;
                $row->update([
                    'display_order' => $before->display_order
                ]);
                $before->update([
                    'display_order' => $rowPos
                ]);
                session()->flash('success', 'Moved one level up!');
                return redirect()->back();
            }
            if ($dir == 'DOWN') {
                $after = Faq::where('display_order', '>', $row->display_order)->where('type', $row->type)->orderBy('display_order', 'ASC')->first();
                if ($after == null) {
                    session()->flash('error', 'Cannot move any further in the downward direction.');
                    return redirect()->back();
                }
                $rowPos = $row->display_order;
                $row->update([
                    'display_order' => $after->display_order
                ]);
                $after->update([
                    'display_order' => $rowPos
                ]);
                session()->flash('success', 'Moved one level down!');
                return redirect()->back();
            }
        } catch (Exception $e) {
            session()->flash('success', $e->getMessage());
            return redirect()->back();
        }
    }

    public function editFaq(Request $request, $id)
    {
        if (!adminHasAccessToMenu(50)) {
            return redirect()->route('admin.dashboard');
        }
        $faqEdit = Faq::find($id);
        if ($request->all()) {
            $request->validate([
                'type' => 'required|in:USER,EXPERT',
                'title' => 'required',
                'description' => 'required',
                'video' => 'nullable',
            ]);
            $update = [
                'type' => $request->type,
                'title' => $request->title,
                'description' => $request->description,
            ];
            if(@$request->video){
                $url = @$request->video;
                parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
                $url_id = @$my_array_of_vars['v'];
                if($url_id == null){
                    $pieces = explode("/", $url);
                    $url_id = end($pieces);
                    if($url_id == null){
                        session()->flash('error', \Lang::get('client_site.wrong_url'));
                        return redirect()->back()->with($data);
                    }
                }
                $update['video'] = "https://www.youtube.com/watch?v=".$url_id;
            } else {
                $update['video'] = null;
            }
            $faqEdit->update($update);
            session()->flash("success", "FAQ updated successfully.");
            return redirect()->route('admin.faq.update');
        }
        return view('admin.modules.content.edit_faq', compact('faqEdit'));
    }

    public function deleteFaq(Request $request, $id)
    {
        if (!adminHasAccessToMenu(50)) {
            return redirect()->route('admin.dashboard');
        }
        Faq::find($id)->delete();
        session()->flash("success", "FAQ deleted successfully.");
        return redirect()->route('admin.faq.update');
    }


    public function updateFooterContent(Request $request)
    {
        if (!adminHasAccessToMenu(52)) {
            return redirect()->route('admin.dashboard');
        }
        if ($request->all()) {
            $request->validate([
                "address"           =>  "required",
                "ph_no"             =>  "required",
                "whatsapp"          =>  "required",
                "email"             =>  "required",
                // "facebook"          =>  "required",
                // "twitter"           =>  "required",
                // "google_plus"       =>  "required",
                // "instagram"         =>  "required",
                // "linkedin"          =>  "required",
                // "youtube"           =>  "required"
            ]);
            FooterContent::where('id', 1)->update([
                "address"     =>  $request->address,
                "ph_no"       =>  $request->ph_no,
                "whatsapp"    =>  $request->whatsapp,
                "email"       =>  $request->email,
                "facebook"    =>  $request->facebook,
                "twitter"     =>  $request->twitter,
                "google_plus" =>  $request->google_plus,
                "instagram"   =>  $request->instagram,
                "linkedin"    =>  $request->linkedin,
                "youtube"     =>  $request->youtube
            ]);
            session()->flash("success", "Successfully updated footer content");
        }
        $footerContent = FooterContent::first();
        return view("admin.modules.content.edit_footer_content")->with([
            "footerContent"    =>  $footerContent
        ]);
    }


    public function newsLetter(Request $request)
    {
        if (!adminHasAccessToMenu(47)) {
            return redirect()->route('admin.dashboard');
        }
        if ($request->all()) {
            $request->validate([
                "description" => "required",
            ]);
            Content::where('id', 10)->update(['description' => $request->description]);
            session()->flash("success", "Successfully updated newsletter content.");
            return redirect()->back();
        }
        $nlContent = Content::where('id', 10)->first();
        $nl = Newsletter::orderBy('id', 'desc')->get();
        return view("admin.modules.newsletter.newsletter")->with(compact('nlContent', 'nl'));
    }

    public function otherPageContent(Request $request)
    {
        if (!adminHasAccessToMenu(44)) {
            return redirect()->route('admin.dashboard');
        }
        if ($request->all()) {
            $request->validate([
                "pp_title" => "required",
                "pp_description" => "required",
                "pb_title" => "required",
                "pb_description" => "required",
                "pd_title" => "required",
                "pd_description" => "required",
            ]);
            Content::find(11)->update([
                'title' => $request->pp_title,
                'description' => $request->pp_description,
            ]);
            Content::find(12)->update([
                'title' => $request->pb_title,
                'description' => $request->pb_description,
            ]);
            Content::find(13)->update([
                'title' => $request->pd_title,
                'description' => $request->pd_description,
            ]);
            session()->flash("success", "Successfully updated all contents.");
            return redirect()->back();
        }
        $profPage = Content::where('id', 11)->first();
        $bookingPage = Content::where('id', 12)->first();
        $dashBoardPage = Content::where('id', 13)->first();
        return view("admin.modules.other_page_content.index")->with(compact('profPage', 'bookingPage', 'dashBoardPage'));
    }


    /**
     * Method : download csv
     * Description : newsletter list download
     * Author : munmun
     * Date : 1-may-2020
     */
    public function download(Request $request)
    {
        // dd($request->all());

        // $user = User::where('user_type', 'C')
        //         ->whereIn('status', ['A', 'I', 'U', 'AA']);

        // if(@$request->keyword){
        //     $user = $user->where(function($q) use($request) {
        //         $q->where('fname', 'like', '%'.$request->keyword.'%')
        //         ->orWhere('lname', 'like', '%'.$request->keyword.'%')
        //         ->orWhere('email', 'like', '%'.$request->keyword.'%')
        //         ->orWhere('mobile', 'like', '%'.$request->keyword.'%')
        //         ->orWhere(DB::raw("CONCAT(`fname`, ' ', `lname`)"), 'LIKE', "%".$request->keyword."%");
        //     });
        // }
        // if(@$request->status){
        //     $user = $user->where('status', $request->status);
        // }

        $user = Newsletter::orderBy('id', 'desc')->get();
        //pr1($user->toArray());
        //die();
        // dd($transaction);
        // if(@$request->button == 'Download as excel') {
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=newsletter-list.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $column = array('Id', 'Email ID');
        // $column = array('Job Date', 'Job No', 'Driver Details', 'Booking Amount', 'Commission Percent',  'Admin Commission', 'Driver Payble', 'Payment Status');

        $callback = function () use ($user, $column) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $column);
            foreach ($user as $row) {
                // if(@$row->status == "U") {
                //     $status = 'Email Not Verified';
                // } elseif (@$row->status == "A") {
                //     $status = 'Active';
                // } elseif (@$row->status == "I") {
                //     $status = 'Inactive';
                // } elseif (@$row->status == "AA") {
                //     $status = 'Awaiting Approval';
                // }
                $csv = array(@$row->id, @$row->email);

                fputcsv($file, $csv);
            }
            fclose($file);
        };
        return \Response::stream($callback, 200, $headers);
        // }

        // session()->flash("success", "Unauthorize access.");
        return redirect()->back();
    }

    public function metaTagManager()
    {
        if (!adminHasAccessToMenu(46)) {
            return redirect()->route('admin.dashboard');
        }
        $metas = MetaTag::get();
        return view('admin.modules.content.meta_list', compact('metas'));
    }

    public function metaTagEditor(Request $request, $id)
    {
        $meta = MetaTag::find($id);
        if ($meta == null) {
            session()->flash('error', 'Meta content not found');
            return redirect()->back();
        }
        if ($request->all()) {
            $request->validate([
                'title' => 'required',
                'description' => 'required',
                'keyword' => 'required',
            ]);
            $meta->update([
                'title' => $request->title,
                'description' => $request->description,
                'keyword' => $request->keyword,
            ]);
            session()->flash('success', 'Meta content updated');
            return redirect()->back();
        }
        return view('admin.modules.content.meta_edit', compact('meta'));
    }

    public function manageLandingPage(Request $request, $section = 0)
    {
        if (!adminHasAccessToMenu(43)) {
            return redirect()->route('admin.dashboard');
        }
        if ($request->all()) {
            if ($section == 1) {
                $b1 = array(
                    'title' => $request->banner1_title,
                    'description' => $request->banner1_description,
                    'heading_4' => $request->banner1_heading_4,
                    'description_4' => $request->banner1_description_4,
                );
                if ($request->hasFile('banner1_image')) {
                    $file = $request->file('banner1_image');
                    $fileName = time() . mt_rand(10000, 99999) . 'b1.' . $file->extension();
                    $stored = $file->move('storage/app/public/uploads/content_images', $fileName);
                    $b1['image'] = $fileName;
                }
                Content::find(14)->update($b1);
            } else if ($section == 2 || $section == 3) {
                foreach ($request->left_sections_title as $key => $value) {
                    if ($key == $section) {
                        Content::find(($section == 2 ? 15 : 16))->update([
                            'title' => $request->left_sections_title[$key],
                            'heading_1' => $request->left_sections_heading_1[$key],
                            'description_1' => $request->left_sections_description_1[$key],
                            'heading_4' => $request->left_sections_heading_4[$key],
                            'description_4' => $request->left_sections_description_4[$key],
                        ]);
                    }
                }
            } else if ($section == 4) {
                $bv = array(
                    'title' => $request->v_title,
                    'description' => $request->v_description,
                    'image' => $request->v_image,
                    'heading_4' => $request->v_heading_4,
                    'description_4' => $request->v_description_4,
                );
                Content::find(36)->update($bv);
            } else if (in_array($section, range(5, 20))) {
                foreach ($request->small_sections_title as $key => $value) {
                    if ($key == $section) {
                        $sm = array(
                            'title' => $request->small_sections_title[$key],
                            'description' => $request->small_sections_description[$key],
                        );
                        if ($request->hasFile('small_sections_image_' . $key)) {
                            $file = $request->file('small_sections_image_' . $key);
                            $fileName = time() . mt_rand(10000, 99999) . $key . 'sm.' . $file->extension();
                            $stored = $file->move('storage/app/public/uploads/content_images', $fileName);
                            $sm['image'] = $fileName;
                        }
                        Content::find($key + 12)->update($sm);
                    }
                }
            } else if ($section == 21) {
                $b2 = array(
                    'title' => $request->banner2_title,
                    'description' => $request->banner2_description,
                    'heading_4' => $request->banner2_heading_4,
                    'description_4' => $request->banner2_description_4,
                );
                if ($request->hasFile('banner2_image')) {
                    $file = $request->file('banner2_image');
                    $fileName = time() . mt_rand(10000, 99999) . 'b2.' . $file->extension();
                    $stored = $file->move('storage/app/public/uploads/content_images', $fileName);
                    $b2['image'] = $fileName;
                }
                Content::find(33)->update($b2);
            } else if (in_array($section, [22, 23])) {
                foreach ($request->last_sections_title as $key => $value) {
                    $lss = array(
                        'title' => $request->last_sections_title[$key],
                        'heading_1' => $request->last_sections_heading_1[$key],
                        'description_1' => $request->last_sections_description_1[$key],
                    );
                    if ($request->hasFile('last_sections_image_1_' . $key)) {
                        $file = $request->file('last_sections_image_1_' . $key);
                        $fileName = time() . mt_rand(10000, 99999) . $key . 'last_sm.' . $file->extension();
                        $stored = $file->move('storage/app/public/uploads/content_images', $fileName);
                        $lss['image_1'] = $fileName;
                    }
                    if ($request->hasFile('last_sections_image_' . $key)) {
                        $file = $request->file('last_sections_image_' . $key);
                        $fileName = time() . mt_rand(10000, 99999) . $key . 'last_bg.' . $file->extension();
                        $stored = $file->move('storage/app/public/uploads/content_images', $fileName);
                        $lss['image'] = $fileName;
                    }
                    Content::find($key + 12)->update($lss);
                }
            }
            session()->flash('success', 'Landing page content updated');
            return redirect()->back();
        }
        $banner1 = Content::find(14);
        $leftSections = Content::whereIn('id', [15, 16])->get();
        $smallSections1 = Content::whereIn('id', range(17, 24))->get();
        $smallSections2 = Content::whereIn('id', range(25, 32))->get();
        $banner2 = Content::find(33);
        $videoBanner = Content::find(36);
        $lastAreas = Content::whereIn('id', [34, 35])->get();
        return view('admin.modules.content.landing_edit', compact(
            'banner1',
            'banner2',
            'leftSections',
            'smallSections1',
            'smallSections2',
            'videoBanner',
            'lastAreas'
        ));
    }

    public function manageHomeBanner()
    {
        if (!adminHasAccessToMenu(45)) {
            return redirect()->route('admin.dashboard');
        }
        $banners = HomeBanner::orderBy('id', 'DESC')->get();
        return view('admin.modules.content.banner_list', compact('banners'));
    }

    public function addHomeBanner(Request $request)
    {
        if (!adminHasAccessToMenu(45)) {
            return redirect()->route('admin.dashboard');
        }
        if ($request->all()) {
            $request->validate([
                'heading' => 'required',
                'description' => 'required',
                'media' => 'required',
                'text_color' => 'required',
            ]);
            $create = array(
                'heading' => $request->heading,
                'description' => $request->description,
                'text_color' => $request->text_color,
            );
            if ($request->hasFile('media')) {
                $file = $request->file('media');
                $fileName = time() . mt_rand(10000, 99999) . 'banner.' . $file->extension();
                $file->move('storage/app/public/uploads/content_images', $fileName);
                $create['media'] = $fileName;
            }
            HomeBanner::create($create);
            session()->flash('success', 'Banner content added successfully.');
            return redirect()->back();
        }
        return view('admin.modules.content.banner_edit');
    }

    public function editHomeBanner(Request $request, $id)
    {
        if (!adminHasAccessToMenu(45)) {
            return redirect()->route('admin.dashboard');
        }
        $banner = HomeBanner::find($id);
        if ($banner == null) {
            session()->flash('error', 'Banner content not found.');
            return redirect()->back();
        }
        if ($request->all()) {
            $request->validate([
                'alignment' => 'required',
                'heading' => 'required',
                'description' => 'required',
                // 'media' => 'required',
                'text_color' => 'required',
            ]);
            $update = array(
                'alignment' => $request->alignment,
                'heading' => $request->heading,
                'description' => $request->description,
                'text_color' => $request->text_color,
            );
            if ($request->hasFile('media')) {
                $file = $request->file('media');
                $fileName = time() . mt_rand(10000, 99999) . 'banner.' . $file->extension();
                $file->move('storage/app/public/uploads/content_images', $fileName);
                $update['media'] = $fileName;
            }
            $banner->update($update);
            session()->flash('success', 'Banner content updated successfully.');
            return redirect()->back();
        }
        return view('admin.modules.content.banner_edit', compact('banner'));
    }

    public function deleteHomeBanner($id)
    {
        if (!adminHasAccessToMenu(45)) {
            return redirect()->route('admin.dashboard');
        }
        $banner = HomeBanner::find($id);
        if ($banner == null) {
            session()->flash('error', 'Banner content not found.');
            return redirect()->back();
        }
        $banner->delete();
        session()->flash('success', 'Banner content deleted successfully.');
        return redirect()->back();
        return view('admin.modules.content.banner_edit', compact('banner'));
    }
    /**
     * For refer content Edit view
     * @method: referContentManagement
     */
    public function referContentManagement()
    {
        if (!adminHasAccessToMenu(53)) {
            return redirect()->route('admin.dashboard');
        }
        $content = ReferContent::first();
        return view('admin.modules.content.refer')->with(['data'=>@$content]);
    }
    /**
     * For refer content Update
     * @method: referContentUpdate
     */
    public function referContentUpdate(Request $request)
    {
        if (!adminHasAccessToMenu(53)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            'content1' => 'required',
            'content2' => 'required',
            'content3' => 'required',
            'process' => 'required',
        ]);
        $data = ReferContent::where('id', 1)->first();
        $upd=[];
        $upd['content_1'] = $request->content1;
        $upd['content_2'] = $request->content2;
        $upd['content_3'] = $request->content3;
        $upd['process_content'] = $request->process;
        if (@$request->image_1) {
            $request->validate([
                'image_1'    =>  'image|mimes:jpeg,png,jpg',
            ]);
            $image_1_pic = $request->image_1;
            $filename = time() . '-' . rand(1000, 9999) . '.' . $image_1_pic->getClientOriginalExtension();
            Storage::putFileAs('public/uploads/content_images', $image_1_pic, $filename);
            @unlink(storage_path('app/public/uploads/content_images/' . @$data->image_1));
            $upd['image_1'] = $filename;
        }
        if (@$request->image_2) {
            $request->validate([
                'image_2'    =>  'image|mimes:jpeg,png,jpg',
            ]);
            $image_2_pic = $request->image_2;
            $filename = time() . '-' . rand(1000, 9999) . '.' . $image_2_pic->getClientOriginalExtension();
            Storage::putFileAs('public/uploads/content_images', $image_2_pic, $filename);
            @unlink(storage_path('app/public/uploads/content_images/' . @$data->image_2));
            $upd['image_2'] = $filename;
        }
        if (@$request->image_3) {
            $request->validate([
                'image_3'    =>  'image|mimes:jpeg,png,jpg',
            ]);
            $image_3_pic = $request->image_3;
            $filename = time() . '-' . rand(1000, 9999) . '.' . $image_3_pic->getClientOriginalExtension();
            Storage::putFileAs('public/uploads/content_images', $image_3_pic, $filename);
            @unlink(storage_path('app/public/uploads/content_images/' . @$data->image_3));
            $upd['image_3'] = $filename;
        }
        if (@$request->modal_image) {
            $request->validate([
                'modal_image'    =>  'image|mimes:jpeg,png,jpg',
            ]);
            $modal_image_pic = $request->modal_image;
            $filename = time() . '-' . rand(1000, 9999) . '.' . $modal_image_pic->getClientOriginalExtension();
            Storage::putFileAs('public/uploads/content_images', $modal_image_pic, $filename);
            @unlink(storage_path('app/public/uploads/content_images/' . @$data->modal_image));
            $upd['modal_image'] = $filename;
        }
        $update = ReferContent::where('id',1)->update($upd);
        if(@$update){
            session()->flash('success', 'Content Update successfully.');
            return redirect()->back();
        }
        session()->flash('edit', 'Content not Update');
        return redirect()->back();
    }

    /**
     *@ Method       : userFaq
     *@ Description  : For view user FAQ content
     *@ Author       : soumojit
     *@ date         : 17-NOV-2020
     */
    public function userFaq(Request $request)
    {
        if (!adminHasAccessToMenu(50)) {
            return redirect()->route('admin.dashboard');
        }
        $faq = Content::where("id", 1)->first();
        $faqItemsU = Faq::where('type', 'USER')->orderBy('display_order', 'ASC')->get();
        $faqItemsE = Faq::where('type', 'EXPERT')->orderBy('display_order', 'ASC')->get();
        return view("admin.modules.content.user_faq")->with(compact('faqItemsU', 'faqItemsE', 'faq'));
    }
    /**
     *@ Method       : expertFaq
     *@ Description  : For view expert FAQ content
     *@ Author       : soumojit
     *@ date         : 17-NOV-2020
     */
    public function expertFaq(Request $request)
    {
        if (!adminHasAccessToMenu(50)) {
            return redirect()->route('admin.dashboard');
        }
        $faq = Content::where("id", 1)->first();
        $faqItemsU = Faq::where('type', 'USER')->orderBy('display_order', 'ASC')->get();
        $faqItemsE = Faq::where('type', 'EXPERT')->orderBy('display_order', 'ASC')->get();
        return view("admin.modules.content.expert_faq")->with(compact('faqItemsU', 'faqItemsE', 'faq'));
    }

    /*
    * Method: updateProductsSection
    * Description: to update product section content
    * Author: Sayantani
    * Date: 14-DEC-2020
    */
    public function updateProductsSection(Request $request)
    {
        if (!adminHasAccessToMenu(41)) {
            return redirect()->route('admin.dashboard');
        }
        if ($request->all()) {
            $update = [
                "title"             =>  $request->title,
                "description"       =>  $request->description,
            ];
            Content::where('id', 37)->update($update);
            session()->flash("success", "Successfully updated product section contents.");
            return redirect()->back();
        }
        $product_section = Content::where("id", 37)->first();
        return view("admin.modules.content.edit_product_section")->with(compact('product_section'));
    }

     /*
    * Method: productLandingPage
    * Description: to update product section content
    * Author: Sayantani
    * Date: 14-DEC-2020
    */
    public function productLandingPage(Request $request, $section = 0){
        if (!adminHasAccessToMenu(54)) {
            return redirect()->route('admin.dashboard');
        }

        if ($request->all()) {
            if ($section == 1) {
                $b1 = array(
                    'title' => $request->banner1_title,
                    'description' => $request->banner1_description,
                    'heading_4' => $request->banner1_heading_4,
                    'description_4' => $request->banner1_description_4,
                    'description_2' => $request->alignment,
                );
                if ($request->hasFile('banner1_image')) {
                    $file = $request->file('banner1_image');
                    $fileName = time() . mt_rand(10000, 99999) . 'b1.' . $file->extension();
                    $stored = $file->move('storage/app/public/uploads/content_images', $fileName);
                    $b1['image'] = $fileName;
                }

                 Content::find(38)->update($b1);
            } else if ($section == 2) {
                $b2 = array(
                    'heading_1' => $request->second_banner_title,
                    'description_1' => $request->second_banner_description,
                    'short_description_1' => $request->second_banner_short_description,
                );
                if ($request->hasFile('second_banner_image')) {
                    $file = $request->file('second_banner_image');
                    $fileName = time() . mt_rand(10000, 99999) . 'b2.' . $file->extension();
                    $stored = $file->move('storage/app/public/uploads/content_images', $fileName);
                    $b2['image_1'] = $fileName;
                }

                 Content::find(38)->update($b2);
            } else if ($section == 3) {
                $b3 = array(
                    'heading_2' => $request->course_type_title,
                );


                 Content::find(38)->update($b3);
            } else if (in_array($section, range(5, 11))) {

                foreach ($request->small_sections_title as $key => $value) {

                    if ($key == $section) {
                        $sm = array(
                            'title' => $request->small_sections_title[$key],

                        );
                        if ($request->hasFile('small_sections_image_' . $key)) {
                            $file = $request->file('small_sections_image_' . $key);
                            $fileName = time() . mt_rand(10000, 99999) . $key . 'sm.' . $file->extension();
                            $stored = $file->move('storage/app/public/uploads/content_images', $fileName);
                            $sm['image'] = $fileName;
                        }

                        Content::find($key + 34)->update($sm);
                    }
                }
            } else if ($section == 13) {

                $b4 = array(
                    'heading_3' => $request->third_banner_title,
                );

                if ($request->hasFile('third_banner_image')) {
                    $file = $request->file('third_banner_image');
                    $fileName = time() . mt_rand(10000, 99999) . 'b4.' . $file->extension();
                    $stored = $file->move('storage/app/public/uploads/content_images', $fileName);
                    $b4['image_3'] = $fileName;
                }


                 Content::find(38)->update($b4);
            } else if ($section == 14) {

                $b5 = array(
                    'heading_5' => $request->fourth_banner_title,
                );




                 Content::find(38)->update($b5);
            } else if ($section == 15) {

                $b6 = array(
                    'heading_6' => $request->fifth_banner_title,
                );

                if ($request->hasFile('fifth_banner_image')) {
                    $file = $request->file('fifth_banner_image');
                    $fileName = time() . mt_rand(10000, 99999) . 'b6.' . $file->extension();
                    $stored = $file->move('storage/app/public/uploads/content_images', $fileName);
                    $b6['image_6'] = $fileName;
                }


                 Content::find(38)->update($b6);
            } else if ($section == 16) {

                $b7 = array(
                    'heading_7' => $request->sixth_banner_title,
                    'description_7' => $request->sixth_banner_description,
                    'button_link_7' => $request->sixth_banner_button_url,
                );




                 Content::find(38)->update($b7);
            } else if (in_array($section, range(17, 23))) {

                foreach ($request->small_sections_title_2_ as $key => $value) {

                    if ($key == $section) {
                        $sm2 = array(
                            'title' => $request->small_sections_title_2_[$key],

                        );


                        Content::find($key + 29)->update($sm2);
                    }
                }
            } else if (in_array($section, range(24, 27))) {

                foreach ($request->small_sections_title_3_ as $key => $value) {

                    if ($key == $section) {
                        $sm3 = array(
                            'title' => $request->small_sections_title_3_[$key],
                            'description' => $request->small_sections_description_3_[$key],

                        );


                        Content::find($key + 29)->update($sm3);
                    }
                }
            } else if (in_array($section, range(28, 30))) {

                foreach ($request->small_sections_title_4_ as $key => $value) {

                    if ($key == $section) {
                        $sm4 = array(
                            'title' => $request->small_sections_title_4_[$key],
                            'description' => $request->small_sections_description_4_[$key],

                        );


                        Content::find($key + 29)->update($sm4);
                    }
                }
            }
            session()->flash('success', 'Product Landing page content updated');
            return redirect()->back();
        }
        $banner1 = Content::find(38);
        $smallSections1 = Content::whereIn('id', range(39, 45))->get();
        $smallSections2 = Content::whereIn('id', range(46, 52))->get();
        $smallSections3 = Content::whereIn('id', range(53, 56))->get();
        $smallSections4 = Content::whereIn('id',range(57,59))->get();
        return view("admin.modules.content.edit_product_landing_page",compact(
            'banner1',
            'smallSections1',
            'smallSections2',
            'smallSections3',
            'smallSections4'
        ));
    }

    /**
     *@ Method       : helpCategoriesIndex
     *@ Description  : For showing help categories index page
     *@ Author       : Sayantani
     *@ Use By       : Sayantani
     */
    public function helpCategoriesIndex(Request $request)
    {
        if (!adminHasAccessToMenu(51)) {
            return redirect()->route('admin.dashboard');
        }
        $data['content'] = Content::find(60);
        $data['categories'] = HelpCategory::where('status', '!=', 'D');
        if($request->all()){
            $request->validate([
                'main_title' => 'required'
            ]);
            $data['content']->update(['title' => $request->main_title]);

            if(@$request->keyword){
                $data['categories'] = $data['categories']->where('category', 'like', '%'.@$request->keyword.'%');
            }
            if(@$request->status){
                $data['categories'] = $data['categories']->where('status', @$request->status);
            }
            $data['key'] = $request->all();
        }
        $data['categories'] = $data['categories']->get();
        return view("admin.modules.content.help_section.index")->with($data);
    }

    public function  helpShowArticles(Request $request)
    {
        $data['helpCategory'] = HelpCategory::get();
        $data['help_cat'] = HelpCategory::get();
        if(@$data['helpCategory']){
            $data['articles'] = HelpArticle::orderBy('created_at','DESC');
            if($request->all()){
                if(@$request->keyword){
                    $data['articles'] = $data['articles']->where('title', 'like', '%'.@$request->keyword.'%');
                }
                if(@$request->status){
                    $data['articles'] = $data['articles']->where('status', @$request->status);
                }
                if(@$request->help_cat){
                    $data['articles'] = $data['articles']->where('help_category_id', @$request->help_cat);
                }
                $data['key'] = $request->all();
            }
            $data['articles'] = $data['articles']->get();
            return view("admin.modules.content.help_section.manage_articles")->with($data);
        } else {
            session()->flash('error', 'Help article not found');
            return redirect()->back();
        }
    }



    /**
     *@ Method       : helpCategoryStore
     *@ Description  : For storing help categories
     *@ Author       : Sayantani
     *@ Use By       : Sayantani
     */
    public function helpCategoryStore(Request $request, $id = null)
    {
        if (!adminHasAccessToMenu(51)) {
            return redirect()->route('admin.dashboard');
        }
        $data = array();
        if(@$request->all()){
            $ins = [
                "category"             =>  $request->category,
            ];
            if(@$request->ID){
                $helpCategory = HelpCategory::where('id', $id)->first();
                if(@$helpCategory){
                    $helpCategory->update($ins);
                    $str = preg_replace('/[-?]/', '', $request->category);
                    $arr = explode(" ",strtolower($str));
                    for ($i = 0; $i < count($arr); $i++) {
                        $arr[$i] = preg_replace('/[^a-z]/i', '', $arr[$i]);
                        if($arr[$i] == "") unset($arr[$i]);
                    }
                    $slug = implode("-", array_slice($arr, 0, 5, true));
                    $helpCategory->slug = $slug . "-" . $helpCategory->id;
                    $helpCategory->update();
                    session()->flash('success', 'Help category updated successfully');
                } else {
                    session()->flash('error', 'Help category not found');
                }
                return redirect()->back();
            } else {
                $created = HelpCategory::create($ins);
                $str = preg_replace('/[-?]/', '', $request->category);
                $arr = explode(" ",strtolower($str));
                for ($i = 0; $i < count($arr); $i++) {
                    $arr[$i] = preg_replace('/[^a-z]/i', '', $arr[$i]);
                    if($arr[$i] == "") unset($arr[$i]);
                }
                $slug = implode("-", array_slice($arr, 0, 5, true));
                $created->slug = $slug . "-" . $created->id;
                $created->save();
                session()->flash('success', 'Help category created successfully');
                return redirect()->back();
            }
        }
        if(@$id){
            $data['helpCategory'] = HelpCategory::where('id', $id)->first();
            $data['articles'] = HelpArticle::where('help_category_id', $id)->get();
        }
        return view("admin.modules.content.help_section.create")->with($data);
    }

    /**
     *@ Method       : helpCategorystatus
     *@ Description  : For changing status of help category
     *@ Author       : Sayantani
     *@ Use By       : Sayantani
     */
    public function helpCategorystatus(Request $request, $id = null)
    {
        $helpCategory = HelpCategory::where('id', $id)->first();
        if(@$helpCategory){
            if($helpCategory->status == 'A'){
                $helpCategory->status = 'I';
                $helpCategory->save();
                session()->flash('success', 'Help category inactivated successfully');
            } else if($helpCategory->status == 'I'){
                $helpCategory->status = 'A';
                $helpCategory->save();
                session()->flash('success', 'Help category activated successfully');
            }
        } else {
            session()->flash('error', 'Help category not found');
        }
        return redirect()->back();
    }

    /**
     *@ Method       : helpCategorydelete
     *@ Description  : For deleting help categorys
     *@ Author       : Sayantani
     *@ Use By       : Sayantani
     */
    public function helpCategorydelete($id)
    {
        $helpCategory = HelpCategory::where('id', $id)->first();
        if(@$helpCategory){
            $helpCategory->delete();
            session()->flash('success', 'Help category deleted successfully');
        } else {
            session()->flash('error', 'Help category not found');
        }
        return redirect()->back();
    }

    /**
     *@ Method       : helpCategorydetails
     *@ Description  : For showing articles under help category
     *@ Author       : Sayantani
     *@ Use By       : Sayantani
     */
    public function helpCategorydetails($id)
    {
        //
    }

    /**
     *@ Method       : helpArticlestore
     *@ Description  : For storing help categories
     *@ Author       : Sayantani
     *@ Use By       : Sayantani
     */
    public function helpArticlestore(Request $request, $id=null)
    {
        if (!adminHasAccessToMenu(51)) {
            return redirect()->route('admin.dashboard');
        }
        $data = array();
        // $data['helpCategory'] = HelpCategory::where('id', $hid)->first();
        $data['help_cat'] = HelpCategory::get();
        if(@$id){
            $data['article'] = HelpArticle::where('id', $id)->first();
        }
        if(@$request->all()){
            // dd($request->all());
            $ins = [
                // "help_category_id"  =>  $hid,
                "help_category_id"  =>  $request->help_cat,
                "title"             =>  $request->title,
                "description"       =>  $request->description,
                "meta_title"        =>  @$request->meta_title,
                "meta_keyword"      =>  @$request->meta_keyword,
                "meta_description"  =>  @$request->meta_description
            ];
            if(@$request->video){
                $url = @$request->video;
                parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
                $url_id = @$my_array_of_vars['v'];
                if($url_id == null){
                    $pieces = explode("/", $url);
                    $url_id = end($pieces);
                    if($url_id == null){
                        session()->flash('error', \Lang::get('client_site.wrong_url'));
                        return redirect()->back()->with($data);
                    }
                }
                $ins['video'] = $url_id;
            } else {
                $ins['video'] = null;
            }
            if(@$request->hasFile('image')){
                @unlink(storage_path('app/public/uploads/content_images/'.@$data['article']->image));
                $image_name = time().'-'.rand(1000,9999).'.'.@$request->image->getClientOriginalExtension();
                $image = @$request->image;
                Storage::putFileAs('public/uploads/content_images', $image, $image_name);
                $ins['image'] = $image_name;
            }
            if(@$request->ID){
                $helpArticle = HelpArticle::where('id', $id)->first();
                if(@$helpArticle){
                    $helpArticle->update($ins);
                    session()->flash('success', 'Help article updated successfully');
                } else {
                    session()->flash('error', 'Help article not found');
                }
                return redirect()->back();
            } else {
                $created = HelpArticle::create($ins);
                $str = preg_replace('/[-?]/', '', $request->title);
                $arr = explode(" ",strtolower($str));
                for ($i = 0; $i < count($arr); $i++){
                    $arr[$i] = preg_replace('/[^a-z]/i', '', $arr[$i]);
                    if($arr[$i] == "") unset($arr[$i]);
                }
                $slug = implode("-", array_slice($arr, 0, 5, true));
                $created->slug = $slug . "-" . $created->id;
                $created->save();
                session()->flash('success', 'Help article created successfully');
                return redirect()->route('admin.help.articles.add', $created->id);
            }
        }
        return view("admin.modules.content.help_section.create_article")->with($data);
    }

    /**
     *@ Method       : helpArticlestatus
     *@ Description  : For changing status of help category
     *@ Author       : Sayantani
     *@ Use By       : Sayantani
     */
    public function helpArticlestatus($id)
    {
        $helpArticle = HelpArticle::where('id', $id)->first();
        if(@$helpArticle){
            if($helpArticle->status == 'A'){
                $helpArticle->status = 'I';
                $helpArticle->save();
                session()->flash('success', 'Help article inactivated successfully');
            } else if($helpArticle->status == 'I'){
                $helpArticle->status = 'A';
                $helpArticle->save();
                session()->flash('success', 'Help article activated successfully');
            }
        } else {
            session()->flash('error', 'Help article not found');
        }
        return redirect()->back();
    }

    /**
     *@ Method       : helpArticledelete
     *@ Description  : For deleting help article
     *@ Author       : Sayantani
     *@ Use By       : Sayantani
     */
    public function helpArticledelete($id)
    {
        $helpArticle = HelpArticle::where('id', $id)->first();
        if(@$helpArticle){
            @unlink(storage_path('app/public/uploads/content_images/'.@$helpArticle->image));
            $helpArticle->delete();
            session()->flash('success', 'Help article deleted successfully');
        } else {
            session()->flash('error', 'Help article not found');
        }
        return redirect()->back();
    }

    /**
     *@ Method       : helpArticlemanage
     *@ Description  : For managing help articles under a category
     *@ Author       : Sayantani
     *@ Use By       : Sayantani
     */
    public function helpArticlemanage(Request $request, $id)
    {
        $data['helpCategory'] = HelpCategory::where('id', $id)->first();
        $data['help_cat'] = HelpCategory::get();
        if(@$data['helpCategory']){
            $data['articles'] = HelpArticle::where('help_category_id', $id);
            if($request->all()){
                if(@$request->keyword){
                    $data['articles'] = $data['articles']->where('title', 'like', '%'.@$request->keyword.'%');
                }
                if(@$request->status){
                    $data['articles'] = $data['articles']->where('status', @$request->status);
                }
                if(@$request->help_cat){
                    $data['articles'] = $data['articles']->where('help_category_id', @$request->help_cat);
                }
                $data['key'] = $request->all();
            }
            $data['articles'] = $data['articles']->get();
            return view("admin.modules.content.help_section.manage_articles")->with($data);
        } else {
            session()->flash('error', 'Help article not found');
            return redirect()->back();
        }
    }

    /*
    * Method: affiliateLandingPage
    * Description: to update affiliate landing page
    * Author: Sayantani
    * Date: 07-OCT-2021
    */
    public function affiliateLandingPage(Request $request, $section=null)
    {
        if (!adminHasAccessToMenu(65)) {
            return redirect()->route('admin.dashboard');
        }
        $data['part1'] = Content::where('id', 61)->first();
        $data['part2'] = Content::where('id', 62)->first();
        if(@$request->all() && @$section){
            if(@$section == 1){
                $ins1['title']                      = @$request->banner_title;
                $ins1['description']                = @$request->banner_description;
                $ins1['image_1']                    = @$request->alignment;
                $ins1['short_description_1']        = @$request->banner_button_text;
                $ins1['button_link_7']              = @$request->banner_button_link;
                if(@$request->hasFile('banner_image')){
                    @unlink(storage_path('app/public/uploads/content_images/'.@$data['part1']->image));
                    $image = @$request->banner_image;
                    $image_name = time().'-'.rand(1000,9999).'.'.@$image->getClientOriginalExtension();
                    Storage::putFileAs('public/uploads/content_images', $image, $image_name);
                    $ins1['image'] = $image_name;
                }
                $data['part1']->update($ins1);
            }

            if(@$section == 2){
                $ins1['heading_1']                  = @$request->for_whom_title;
                $ins1['description_1']              = @$request->for_whom_bottom_line;
                $ins1['heading_2']                  = @$request->for_whom_title_1;
                $ins1['description_2']              = @$request->for_whom_description_1;
                $ins1['heading_3']                  = @$request->for_whom_title_2;
                $ins1['description_3']              = @$request->for_whom_description_2;
                $ins1['heading_4']                  = @$request->for_whom_title_3;
                $ins1['description_4']              = @$request->for_whom_description_3;

                if(@$request->hasFile('for_whom_icon_1')){
                    @unlink(storage_path('app/public/uploads/content_images/'.@$data['part1']->image_2));
                    $image = @$request->for_whom_icon_1;
                    $image_name = time().'-'.rand(1000,9999).'.'.@$image->getClientOriginalExtension();
                    Storage::putFileAs('public/uploads/content_images', $image, $image_name);
                    $ins1['image_2'] = $image_name;
                }
                if(@$request->hasFile('for_whom_icon_2')){
                    @unlink(storage_path('app/public/uploads/content_images/'.@$data['part1']->image_3));
                    $image = @$request->for_whom_icon_2;
                    $image_name = time().'-'.rand(1000,9999).'.'.@$image->getClientOriginalExtension();
                    Storage::putFileAs('public/uploads/content_images', $image, $image_name);
                    $ins1['image_3'] = $image_name;
                }
                if(@$request->hasFile('for_whom_icon_3')){
                    @unlink(storage_path('app/public/uploads/content_images/'.@$data['part1']->image_4));
                    $image = @$request->for_whom_icon_3;
                    $image_name = time().'-'.rand(1000,9999).'.'.@$image->getClientOriginalExtension();
                    Storage::putFileAs('public/uploads/content_images', $image, $image_name);
                    $ins1['image_4'] = $image_name;
                }
                $data['part1']->update($ins1);
            }

            if(@$section == 3){
                $ins1['heading_5']                  = @$request->middle_banner_title;
                $ins1['description_5']              = @$request->middle_banner_description;
                $ins1['heading_6']                  = @$request->middle_banner_title_1;
                $ins1['description_6']              = @$request->middle_banner_description_1;
                $ins1['heading_7']                  = @$request->middle_banner_title_2;
                $ins1['description_7']              = @$request->middle_banner_description_2;

                if(@$request->hasFile('middle_banner_image')){
                    @unlink(storage_path('app/public/uploads/content_images/'.@$data['part1']->image_5));
                    $image = @$request->middle_banner_image;
                    $image_name = time().'-'.rand(1000,9999).'.'.@$image->getClientOriginalExtension();
                    Storage::putFileAs('public/uploads/content_images', $image, $image_name);
                    $ins1['image_5'] = $image_name;
                }
                $data['part1']->update($ins1);
            }

            if(@$section == 4){
                $ins2['heading_1']                  = @$request->how_it_works_title;
                $ins2['heading_2']                  = @$request->how_it_works_title_1;
                $ins2['description_2']              = @$request->how_it_works_description_1;
                $ins2['heading_3']                  = @$request->how_it_works_title_2;
                $ins2['description_3']              = @$request->how_it_works_description_2;
                $ins2['heading_4']                  = @$request->how_it_works_title_3;
                $ins2['description_4']              = @$request->how_it_works_description_3;

                if(@$request->hasFile('how_it_works_icon_1')){
                    @unlink(storage_path('app/public/uploads/content_images/'.@$data['part2']->image_2));
                    $image = @$request->how_it_works_icon_1;
                    $image_name = time().'-'.rand(1000,9999).'.'.@$image->getClientOriginalExtension();
                    Storage::putFileAs('public/uploads/content_images', $image, $image_name);
                    $ins2['image_2'] = $image_name;
                }
                if(@$request->hasFile('how_it_works_icon_2')){
                    @unlink(storage_path('app/public/uploads/content_images/'.@$data['part2']->image_3));
                    $image = @$request->how_it_works_icon_2;
                    $image_name = time().'-'.rand(1000,9999).'.'.@$image->getClientOriginalExtension();
                    Storage::putFileAs('public/uploads/content_images', $image, $image_name);
                    $ins2['image_3'] = $image_name;
                }
                if(@$request->hasFile('how_it_works_icon_3')){
                    @unlink(storage_path('app/public/uploads/content_images/'.@$data['part2']->image_4));
                    $image = @$request->how_it_works_icon_3;
                    $image_name = time().'-'.rand(1000,9999).'.'.@$image->getClientOriginalExtension();
                    Storage::putFileAs('public/uploads/content_images', $image, $image_name);
                    $ins2['image_4'] = $image_name;
                }
                $data['part2']->update($ins2);
            }

            if(@$section == 5){
                $ins2['title']                      = @$request->bottom_banner_title;
                $ins2['short_description_1']        = @$request->bottom_banner_button_text;
                $ins2['button_link_7']              = @$request->bottom_banner_button_link;
                $data['part2']->update($ins2);
            }

        }
        return view("admin.modules.content.edit_affiliate_landing_page")->with($data);
    }



    /*
    * Method: professionalLandingPage
    * Description: to update professional landing page
    * Author: Soumojit
    * Date: 11-OCT-2021
    */
    public function professionalLandingPage(Request $request, $section=null)
    {
        if (!adminHasAccessToMenu(66)) {
            return redirect()->route('admin.dashboard');
        }
        $data['part1'] = Content::where('id', 63)->first();
        $data['part2'] = Content::where('id', 64)->first();
        $data['part3'] = Content::where('id', 65)->first();
        $data['part4'] = Content::where('id', 66)->first();
        if(@$request->all() && @$section){
            if(@$section == 1){
                $ins1['title']                      = @$request->banner_title;
                $ins1['description']                = @$request->banner_description;
                $ins1['short_description_1']        = @$request->banner_button_text;
                $ins1['button_link_7']              = @$request->banner_button_link;
                $ins1['heading_6']                  = @$request->banner_video_button_text;
                $ins1['image_6']                    = @$request->banner_youtube_link;
                $ins1['description_6']              = @$request->alignment;

                if(@$request->hasFile('banner_image')){
                    @unlink(storage_path('app/public/uploads/content_images/'.@$data['part1']->image));
                    $image = @$request->banner_image;
                    $image_name = time().'-'.rand(1000,9999).'.'.@$image->getClientOriginalExtension();
                    Storage::putFileAs('public/uploads/content_images', $image, $image_name);
                    $ins1['image'] = $image_name;
                }
                $data['part1']->update($ins1);
            }

            if(@$section == 2){
                $ins2['title']                      = @$request->for_whom_title;
                $ins2['description']                = @$request->for_whom_bottom_line;

                $ins2['heading_1']                  = @$request->for_whom_title_1;
                $ins2['description_1']              = @$request->for_whom_description_1;
                $ins2['heading_2']                  = @$request->for_whom_title_2;
                $ins2['description_2']              = @$request->for_whom_description_2;
                $ins2['heading_3']                  = @$request->for_whom_title_3;
                $ins2['description_3']              = @$request->for_whom_description_3;
                $ins2['heading_4']                  = @$request->for_whom_title_4;
                $ins2['description_4']              = @$request->for_whom_description_4;
                $ins2['heading_5']                  = @$request->for_whom_title_5;
                $ins2['description_5']              = @$request->for_whom_description_5;
                $ins2['heading_6']                  = @$request->for_whom_title_6;
                $ins2['description_6']              = @$request->for_whom_description_6;

                if(@$request->hasFile('for_whom_icon_1')){
                    @unlink(storage_path('app/public/uploads/content_images/'.@$data['part2']->image_1));
                    $image = @$request->for_whom_icon_1;
                    $image_name = time().'-'.rand(1000,9999).'.'.@$image->getClientOriginalExtension();
                    Storage::putFileAs('public/uploads/content_images', $image, $image_name);
                    $ins2['image_1'] = $image_name;
                }
                if(@$request->hasFile('for_whom_icon_2')){
                    @unlink(storage_path('app/public/uploads/content_images/'.@$data['part2']->image_2));
                    $image = @$request->for_whom_icon_2;
                    $image_name = time().'-'.rand(1000,9999).'.'.@$image->getClientOriginalExtension();
                    Storage::putFileAs('public/uploads/content_images', $image, $image_name);
                    $ins2['image_2'] = $image_name;
                }
                if(@$request->hasFile('for_whom_icon_3')){
                    @unlink(storage_path('app/public/uploads/content_images/'.@$data['part2']->image_3));
                    $image = @$request->for_whom_icon_3;
                    $image_name = time().'-'.rand(1000,9999).'.'.@$image->getClientOriginalExtension();
                    Storage::putFileAs('public/uploads/content_images', $image, $image_name);
                    $ins2['image_3'] = $image_name;
                }
                if(@$request->hasFile('for_whom_icon_4')){
                    @unlink(storage_path('app/public/uploads/content_images/'.@$data['part2']->image_4));
                    $image = @$request->for_whom_icon_4;
                    $image_name = time().'-'.rand(1000,9999).'.'.@$image->getClientOriginalExtension();
                    Storage::putFileAs('public/uploads/content_images', $image, $image_name);
                    $ins2['image_4'] = $image_name;
                }
                if(@$request->hasFile('for_whom_icon_5')){
                    @unlink(storage_path('app/public/uploads/content_images/'.@$data['part2']->image_5));
                    $image = @$request->for_whom_icon_5;
                    $image_name = time().'-'.rand(1000,9999).'.'.@$image->getClientOriginalExtension();
                    Storage::putFileAs('public/uploads/content_images', $image, $image_name);
                    $ins2['image_5'] = $image_name;
                }
                if(@$request->hasFile('for_whom_icon_6')){
                    @unlink(storage_path('app/public/uploads/content_images/'.@$data['part2']->image_6));
                    $image = @$request->for_whom_icon_6;
                    $image_name = time().'-'.rand(1000,9999).'.'.@$image->getClientOriginalExtension();
                    Storage::putFileAs('public/uploads/content_images', $image, $image_name);
                    $ins2['image_6'] = $image_name;
                }

                $data['part2']->update($ins2);
            }
            if(@$section == 3){
                $ins2['heading_1']                  = @$request->how_it_works_title;
                $ins2['description_1']              = @$request->how_it_works_description;
                $ins2['heading_2']                  = @$request->how_it_works_title_1;
                $ins2['description_2']              = @$request->how_it_works_description_1;
                $ins2['heading_3']                  = @$request->how_it_works_title_2;
                $ins2['description_3']              = @$request->how_it_works_description_2;
                $ins2['heading_4']                  = @$request->how_it_works_title_3;
                $ins2['description_4']              = @$request->how_it_works_description_3;

                if(@$request->hasFile('how_it_works_icon_1')){
                    @unlink(storage_path('app/public/uploads/content_images/'.@$data['part2']->image_2));
                    $image = @$request->how_it_works_icon_1;
                    $image_name = time().'-'.rand(1000,9999).'.'.@$image->getClientOriginalExtension();
                    Storage::putFileAs('public/uploads/content_images', $image, $image_name);
                    $ins2['image_2'] = $image_name;
                }
                if(@$request->hasFile('how_it_works_icon_2')){
                    @unlink(storage_path('app/public/uploads/content_images/'.@$data['part2']->image_3));
                    $image = @$request->how_it_works_icon_2;
                    $image_name = time().'-'.rand(1000,9999).'.'.@$image->getClientOriginalExtension();
                    Storage::putFileAs('public/uploads/content_images', $image, $image_name);
                    $ins2['image_3'] = $image_name;
                }
                if(@$request->hasFile('how_it_works_icon_3')){
                    @unlink(storage_path('app/public/uploads/content_images/'.@$data['part2']->image_4));
                    $image = @$request->how_it_works_icon_3;
                    $image_name = time().'-'.rand(1000,9999).'.'.@$image->getClientOriginalExtension();
                    Storage::putFileAs('public/uploads/content_images', $image, $image_name);
                    $ins2['image_4'] = $image_name;
                }
                $data['part1']->update($ins2);
            }

            if(@$section == 4){
                $ins1['heading_5']                  = @$request->middle_banner_title;
                $ins1['description_5']              = @$request->middle_banner_description;

                if(@$request->hasFile('middle_banner_image')){
                    @unlink(storage_path('app/public/uploads/content_images/'.@$data['part1']->image_5));
                    $image = @$request->middle_banner_image;
                    $image_name = time().'-'.rand(1000,9999).'.'.@$image->getClientOriginalExtension();
                    Storage::putFileAs('public/uploads/content_images', $image, $image_name);
                    $ins1['image_5'] = $image_name;
                }
                $data['part1']->update($ins1);
            }
            if(@$section == 5){
                $ins3['title']                      = @$request->for_content_title;
                $ins3['description']                = @$request->for_content_bottom_line;

                $ins3['heading_1']                  = @$request->for_content_title_1;
                $ins3['description_1']              = @$request->for_content_description_1;
                $ins3['heading_2']                  = @$request->for_content_title_2;
                $ins3['description_2']              = @$request->for_content_description_2;
                $ins3['heading_3']                  = @$request->for_content_title_3;
                $ins3['description_3']              = @$request->for_content_description_3;

                if(@$request->hasFile('for_content_icon_1')){
                    @unlink(storage_path('app/public/uploads/content_images/'.@$data['part3']->image_1));
                    $image = @$request->for_content_icon_1;
                    $image_name = time().'-'.rand(1000,9999).'.'.@$image->getClientOriginalExtension();
                    Storage::putFileAs('public/uploads/content_images', $image, $image_name);
                    $ins3['image_1'] = $image_name;
                }
                if(@$request->hasFile('for_content_icon_2')){
                    @unlink(storage_path('app/public/uploads/content_images/'.@$data['part3']->image_2));
                    $image = @$request->for_content_icon_2;
                    $image_name = time().'-'.rand(1000,9999).'.'.@$image->getClientOriginalExtension();
                    Storage::putFileAs('public/uploads/content_images', $image, $image_name);
                    $ins3['image_2'] = $image_name;
                }
                if(@$request->hasFile('for_content_icon_3')){
                    @unlink(storage_path('app/public/uploads/content_images/'.@$data['part3']->image_3));
                    $image = @$request->for_content_icon_3;
                    $image_name = time().'-'.rand(1000,9999).'.'.@$image->getClientOriginalExtension();
                    Storage::putFileAs('public/uploads/content_images', $image, $image_name);
                    $ins3['image_3'] = $image_name;
                }

                $data['part3']->update($ins3);
            }

            if(@$section == 6){
                $ins4['title']                      = @$request->bottom_banner_title;
                $ins4['description']                = @$request->bottom_banner_description;
                $ins4['short_description_1']        = @$request->bottom_banner_button_text;
                $ins4['button_link_7']              = @$request->bottom_banner_button_link;

                $ins4['heading_1']                  = @$request->bottom_banner_list1;
                $ins4['description_1']              = @$request->bottom_banner_list2;
                $ins4['heading_2']                  = @$request->bottom_banner_list3;
                $ins4['description_2']              = @$request->bottom_banner_list4;
                $ins4['heading_3']                  = @$request->bottom_banner_list5;
                $ins4['description_3']              = @$request->bottom_banner_list6;
                $ins4['heading_4']                  = @$request->bottom_banner_list7;
                $ins4['description_4']              = @$request->bottom_banner_list8;
                $ins4['heading_5']                  = @$request->bottom_banner_list9;
                $ins4['description_5']              = @$request->bottom_banner_list10;
                $ins4['heading_6']                  = @$request->bottom_banner_list11;
                $ins4['description_6']              = @$request->bottom_banner_list12;

                $data['part4']->update($ins4);
            }
            session()->flash('success', 'Landing page updated ');
            return redirect()->back();
        }

        return view('admin.modules.content.edit_professional_landing_page')->with($data);
    }
}
