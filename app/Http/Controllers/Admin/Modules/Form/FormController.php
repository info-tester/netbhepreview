<?php

namespace App\Http\Controllers\Admin\Modules\Form;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FormMaster;
use App\Models\FormCategory;
use App\Models\FormMasterDetails;
use App\Models\ImportedTool;
use App\Models\ProfessionalSpecialty;
use Session;
use Validator;
use Auth;

class FormController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }

    /*
    @method         => manage
    @description    => Form manage
    @author         => Sanjoy Patra
    @date           => 21/02/2020
    */
    public function manage(Request $request)
    {
        if (!adminHasAccessToMenu(56)) {
            return redirect()->route('admin.dashboard');
        }
        $data = array();
        $data['title'] = 'Manage Form';
        $data['category'] = FormCategory::get();
        //$data['form'] = FormMaster::where('status','!=', 'DELETED')->with('category')->with('user')->orderBy('id','DESC')->get();
        // dd($data);

        $details = FormMaster::with('category', 'user');
        if (@$request->all()) {
            if (@$request->keyword) {
                $details = $details->where('form_title', 'like', '%' . $request->keyword . '%');
            }

            if (@$request->cat_id) {
                $details = $details->whereHas('category', function ($query) use ($request) {
                    $query->where('id', $request->cat_id);
                });
            }

            if (@$request->status) {
                $details = $details->whereIn('status', [$request->status]);
            }
            $data['key'] = $request->all();
        }

        $data['form'] = $details->where('status', '!=', 'DELETED')->orderBy('id', 'desc')
            ->get();

        return view('admin.modules.form.manage')->with($data);
    }
    /*
    @method         => add
    @description    => Form add view
    @author         => Sanjoy Patra
    @date           => 21/02/2020
    */
    public function add()
    {
        if (!adminHasAccessToMenu(56)) {
            return redirect()->route('admin.dashboard');
        }
        $data = array();
        $data['title'] = 'Add Form';
        $data['button'] = 'Add';
        //$data['category'] = FormCategory::where('cat_type', 'FORM')->get();
        $data['category'] = FormCategory::get();
        $data['specialities'] = ProfessionalSpecialty::orderBy('id', 'DESC')->get();
        return view('admin.modules.form.add_form')->with($data);
    }
    /*
    @method         => create
    @description    => Form create in form_master table
    @author         => Sanjoy Patra
    @date           => 21/02/2020
    */
    public function create(Request $request)
    {
        if (!adminHasAccessToMenu(56)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            'title'            => 'required',
            'cat_id'           => 'required',
            'desc'             => 'required',
            'speciality_ids'   => 'required|array|min:1',
        ]);
        $insert['form_title']                = $request->title;
        $insert['form_cat_id']               = $request->cat_id;
        $insert['form_dsc']                  = nl2br($request->desc);
        $insert['help_tools_view']           = nl2br($request->help_tools_view);
        $insert['tool_type']                 = 'Form';
        $insert['status']                    = 'INCOMPLETE';
        $insert['added_by']                  = 'A';
        $insert['added_by_id']               = Auth::guard('admin')->id();
        $last = FormMaster::create($insert);
        $last->specialities()->sync($request->speciality_ids);
        session()->flash('success', 'Form added successfully.');
        return redirect(route('admin.form.details.add', ['form_id' => $last->id]));
    }

    /*
    @method         => edit
    @description    => Form edit view
    @author         => Sanjoy Patra
    @date           => 21/02/2020
    */
    public function edit($id = null)
    {
        if (!adminHasAccessToMenu(56)) {
            return redirect()->route('admin.dashboard');
        }
        $data = array();
        $data['title'] = 'Edit Form';
        $data['button'] = 'Edit';
        //$data['category'] = FormCategory::where('cat_type', 'FORM')->get();
        $data['category'] = FormCategory::get();
        $data['details'] = FormMaster::where('id', $id)->first();
        $data['specialities'] = ProfessionalSpecialty::orderBy('id', 'DESC')->get();
        $related_spec = $data['details']->specialities()->get();
        $data['related_spec_ids'] = array();
        foreach($related_spec as $ar){
            array_push($data['related_spec_ids'], $ar->id);
        }

        return view('admin.modules.form.add_form')->with($data);
    }

    /*
    @method         => update
    @description    => Form update in form_master table
    @author         => Sanjoy Patra
    @date           => 21/02/2020
    */
    public function update($id = null, Request $request)
    {
        if (!adminHasAccessToMenu(56)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            'title'            => 'required',
            'cat_id'           => 'required',
            'desc'             => 'required',
        ]);
        $insert['form_title']                = $request->title;
        $insert['form_cat_id']               = $request->cat_id;
        $insert['form_dsc']                  = nl2br($request->desc);
        $form = FormMaster::where('id', $id)->first();
        $form->update($insert);
            
        $data = $form->specialities()->sync($request->speciality_ids);

        session()->flash('success', 'Form edited successfully.');
        return redirect()->back();
    }

    /*
    @method         => delete
    @description    => Form delete in form_master table just status change
    @author         => Sanjoy Patra
    @date           => 21/02/2020
    */
    public function delete($id = null)
    {
        if (!adminHasAccessToMenu(56)) {
            return redirect()->route('admin.dashboard');
        }
        if (!$id) {
            session()->flash('error', 'Something went wrong.');
            return redirect()->back();
        }
        $details = FormMaster::where('id', $id)->first();
        if (!$details) {
            session()->flash('error', 'Form not found.');
            return redirect()->back();
        }
        FormMaster::where('id', $id)->update(['status' => 'DELETED']);
        session()->flash('success', 'Form deleted successfully.');
        return redirect()->back();
    }

    /*
    @method         => FormDetailsAdd
    @description    => Form details add view
    @author         => Sanjoy Patra
    @date           => 21/02/2020
    */
    public function FormDetailsAdd($id = null)
    {
        $data = array();
        $data['title'] = 'Add Question';
        $data['details'] = FormMaster::where('id', $id)->first();
        return view('admin.modules.form.add_form_details')->with($data);
    }

    /*
    @method         => FormDetailsCreate
    @description    => Form details add in form_master_details table
    @author         => Sanjoy Patra
    @date           => 21/02/2020
    */
    public function FormDetailsCreate($id = null, Request $request)
    {
        // dd($request->all());
        $request->validate([
            'formtype'            => 'required',
            'field_name'          => 'required'
        ]);
        $insert['form_master_id']          = $id;
        $insert['field_type']              = $request->formtype;
        $insert['field_name']              = $request->field_name;
        $insert['status']                  = 'ACTIVE';
        if ($request->field_value) {
            $insert['field_values']        =  json_encode($request->field_value);
        }
        FormMasterDetails::create($insert);
        if ($request->finised) {
            $update = array();
            $update['status'] = 'ACTIVE';
            FormMaster::where('id', $id)->update($update);
            session()->flash('success', 'Question added successfully.');
            return redirect(route('admin.form.manage'));
        }
        session()->flash('success', 'Question added successfully.');
        return redirect()->back();
    }

    /*
    @method         => FormDetailsEdit
    @description    => Form details edit view
    @author         => Sanjoy Patra
    @date           => 22/02/2020
    */
    public function FormDetailsEdit($form_id = null, $id = null)
    {
        $data = array();
        $data['title'] = 'Edit Question';
        $data['form'] = FormMaster::where('id', $form_id)->first();
        $data['details'] = FormMasterDetails::where('id', $id)->first();
        return view('admin.modules.form.edit_form_details')->with($data);
    }

    /*
    @method         => FormDetailsCreate
    @description    => Form details update in form_master_details table
    @author         => Sanjoy Patra
    @date           => 22/02/2020
    */
    public function FormDetailsUpdate($id = null, Request $request)
    {
        $request->validate([
            'formtype'            => 'required',
            'field_name'          => 'required'
        ]);
        $insert['form_master_id']          = $id;
        $insert['field_type']              = $request->formtype;
        $insert['field_name']              = $request->field_name;
        $insert['status']                  = 'ACTIVE';
        if ($request->field_value) {
            $insert['field_values']        =  json_encode($request->field_value);
        }
        FormMasterDetails::where('form_master_id', $id)->delete();
        FormMasterDetails::create($insert);
        session()->flash('success', 'Question edited successfully.');
        return redirect(route('admin.form.details.manage', ['form_id' => $id]));
    }

    /*
    @method         => FormDetailsManage
    @description    => Form details manage
    @author         => Sanjoy Patra
    @date           => 22/02/2020
    */
    public function FormDetailsManage($id = null)
    {
        if (!$id) {
            session()->flash('error', 'Something went wrong.');
            return redirect()->back();
        }
        $details = FormMaster::where('id', $id)->first();
        if (!$details) {
            session()->flash('error', 'Form not found.');
            return redirect()->back();
        }
        $data = array();
        $data['title'] = 'Manage Questions';
        $data['form'] = $details;
        $data['form_fields'] = FormMasterDetails::where('status', '!=', 'DELETED')->where('form_master_id', $id)->get();
        return view('admin.modules.form.manage_field')->with($data);
    }

    /*
    @method         => categoryManage
    @description    => Form category manage
    @author         => Sanjoy Patra
    @date           => 22/02/2020
    */
    public function categoryManage()
    {
        if (!adminHasAccessToMenu(55)) {
            return redirect()->route('admin.dashboard');
        }
        $data = array();
        $data['title'] = 'Manage Form';
        $data['category'] = FormCategory::orderBy('id', 'DESC')->get();
        return view('admin.modules.form.manage_category')->with($data);
    }

    /*
    @method         => categoryAdd
    @description    => Form category add view
    @author         => Sanjoy Patra
    @date           => 22/02/2020
    */
    public function categoryAdd()
    {
        if (!adminHasAccessToMenu(55)) {
            return redirect()->route('admin.dashboard');
        }
        $data = array();
        $data['title'] = 'Add category';
        $data['button'] = 'Add';
        return view('admin.modules.form.add_form_category')->with($data);
    }

    /*
    @method         => categoryCreate
    @description    => Form category add in form_category table
    @author         => Sanjoy Patra
    @date           => 22/02/2020
    */
    public function categoryCreate(Request $request)
    {
        if (!adminHasAccessToMenu(55)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            'cat_name'            => 'required',
            //'cat_type'            => 'required'
        ]);
        $if_exists = FormCategory::where('form_category','like','%'.@$request->cat_name. '%')->first();
        $insert['form_category']  = $request->cat_name;
        //$insert['cat_type']       = $request->cat_type;
        if($if_exists){

            session()->flash('error', 'Category already exists.');
            return redirect()->back();

        }else{
            
            FormCategory::create($insert);
            session()->flash('success', 'Category added successfully.');
            return redirect()->back();

        }
        
    }

    /*
    @method         => categoryEdit
    @description    => Form category edit view
    @author         => Sanjoy Patra
    @date           => 22/02/2020
    */
    public function categoryEdit($id = null)
    {
        if (!adminHasAccessToMenu(55)) {
            return redirect()->route('admin.dashboard');
        }
        $data = array();
        $data['title'] = 'Edit category';
        $data['button'] = 'Edit';
        $data['details'] = FormCategory::where('id', $id)->first();
        return view('admin.modules.form.add_form_category')->with($data);
    }

    /*
    @method         => categoryUpdate
    @description    => Form category update in form_category table
    @author         => Sanjoy Patra
    @date           => 22/02/2020
    */
    public function categoryUpdate($id = null, Request $request)
    {
        if (!adminHasAccessToMenu(55)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            'cat_name'            => 'required',
            //'cat_type'            => 'required'
        ]);
        $insert['form_category']  = $request->cat_name;
        //$insert['cat_type']       = $request->cat_type;
        FormCategory::where('id', $id)->update($insert);
        session()->flash('success', 'Category edited successfully.');
        return redirect()->back();
    }

    /*
    @method         => categoryDelete
    @description    => Form category delete
    @author         => Sanjoy Patra
    @date           => 22/02/2020
    */
    public function categoryDelete($id = null)
    {
        if (!adminHasAccessToMenu(55)) {
            return redirect()->route('admin.dashboard');
        }
        if (!$id) {
            session()->flash('error', 'Something went wrong.');
            return redirect()->back();
        }
        $is_form_add_cat = FormMaster::where('form_cat_id', $id)->first();
        if ($is_form_add_cat) {
            session()->flash('error', 'Category not deleted because it is associted with form.');
            return redirect()->back();
        } else {
            FormCategory::where('id', $id)->delete();
            session()->flash('success', 'Category deleted successfully.');
            return redirect()->back();
        }
    }

    /*
    @method         => importedManage
    @description    => Manage imported
    @author         => Sanjoy Patra
    @date           => 24/02/2020
    */
    public function importedManage($id = null)
    {
        if (!adminHasAccessToMenu(57)) {
            return redirect()->route('admin.dashboard');
        }
        $data = array();
        $data['title'] = 'Manage Imported';
        $data['importeds'] = ImportedTool::where('status', '!=', 'DELETED')->orderBy('id', 'DESC')->with('user')->with('category')->get();
        // dd($data['details']);
        return view('admin.modules.form.manage_imported')->with($data);
    }
    /*
    @method         => importedCreate
    @description    => Add imported view
    @author         => Sanjoy Patra
    @date           => 24/02/2020
    */
    public function importedAdd()
    {
        if (!adminHasAccessToMenu(57)) {
            return redirect()->route('admin.dashboard');
        }
        $data = array();
        $data['title'] = 'Add Imported';
        $data['button'] = 'Add';
        //$data['category'] = FormCategory::where('cat_type', 'IMPORTED')->get();
        $data['category'] = FormCategory::get();
        $data['specialities'] = ProfessionalSpecialty::orderBy('id', 'DESC')->get();
        return view('admin.modules.form.add_imported')->with($data);
    }
    /*
    @method         => importedCreate
    @description    => Form category update in form_category table
    @author         => Sanjoy Patra
    @date           => 22/02/2020
    */
    public function importedCreate(Request $request)
    {
        if (!adminHasAccessToMenu(57)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            'title'           => 'required',
            'cat_id'          => 'required',
            'desc'            => 'required',
        ]);
        $insert['cat_id']           = $request->cat_id;
        $insert['added_by']         = 'A';
        $insert['added_by_id']      = Auth::guard('admin')->id();
        $insert['title']            = $request->title;
        $insert['description']      = $request->desc;
        $insert['help_tools_view']  = $request->help_tools_view;
        $insert['status']       = 'ACTIVE';
        $imported_tool = ImportedTool::create($insert);
        $imported_tool->specialities()->sync($request->speciality_ids);
        session()->flash('success', 'Imported added successfully.');
        return redirect()->back();
    }

    /*
    @method         => importedEdit
    @description    => Add imported view
    @author         => Sanjoy Patra
    @date           => 24/02/2020
    */
    public function importedEdit($id = null)
    {
        if (!adminHasAccessToMenu(57)) {
            return redirect()->route('admin.dashboard');
        }
        $data = array();
        $data['title'] = 'Edit Imported';
        $data['button'] = 'Edit';
        //$data['category'] = FormCategory::where('cat_type', 'IMPORTED')->get();
        $data['category'] = FormCategory::get();
        $data['details'] = ImportedTool::where('id', $id)->first();
        $data['specialities'] = ProfessionalSpecialty::orderBy('id', 'DESC')->get();
        $related_spec = $data['details']->specialities()->get();
        $data['related_spec_ids'] = array();
        foreach($related_spec as $ar){
            array_push($data['related_spec_ids'], $ar->id);
        }
        return view('admin.modules.form.add_imported')->with($data);
    }

    /*
    @method         => importedUpdate
    @description    => Form category update in form_category table
    @author         => Sanjoy Patra
    @date           => 22/02/2020
    */
    public function importedUpdate($id = null, Request $request)
    {
        if (!adminHasAccessToMenu(57)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            'title'           => 'required',
            'cat_id'          => 'required',
            'desc'            => 'required',
        ]);
        $insert['cat_id']           = $request->cat_id;
        $insert['title']            = $request->title;
        $insert['description']      = $request->desc;
        $insert['help_tools_view']  = $request->help_tools_view;
        $imported_tool = ImportedTool::where('id', $id)->first();
        $imported_tool->update($insert);
        $data = $imported_tool->specialities()->sync($request->speciality_ids);

        session()->flash('success', 'Imported edited successfully.');
        return redirect()->back();
    }

    /*
    @method         => importedEdit
    @description    => Delete imported
    @author         => Sanjoy Patra
    @date           => 24/02/2020
    */
    public function importedDelete($id = null)
    {
        if (!adminHasAccessToMenu(57)) {
            return redirect()->route('admin.dashboard');
        }
        ImportedTool::where('id', $id)->delete();
        session()->flash('success', 'Imported edited successfully.');
        return redirect()->back();
    }


    /*
    * Method: categoryShowHomepage
    * Description: shown in professional of form tools
    * Author: munmun
    */
    public function formToolShowProff($id)
    {

        $data = FormMaster::find($id);
        //pr1($data);
        //die();

        if (@$data->shown_in_proff == 'Y') {
            // update status field to active
            FormMaster::where('id', $id)->update(['shown_in_proff' => 'N']);
            session()->flash('success', 'Now Form tools is not showned in professional.');
            return redirect()->back();
        } else if (@$data->shown_in_proff == 'N') {
            // update status field to active
            FormMaster::where('id', $id)->update(['shown_in_proff' => 'Y']);
            session()->flash('success', 'Now tools is showned in professional.');
            return redirect()->back();
        }
    }


    /*
    * Method: tools for professional
    * Description: shown in professional of form tools
    * Author: munmun
    */
    public function importedToolShowProff($id)
    {

        $data = ImportedTool::find($id);
        //pr1($data);
        //die();

        if (@$data->shown_in_proff == 'Y') {
            // update status field to active
            ImportedTool::where('id', $id)->update(['shown_in_proff' => 'N']);
            session()->flash('success', 'Now Imported tools is not showned in professional.');
            return redirect()->back();
        } else if (@$data->shown_in_proff == 'N') {
            // update status field to active
            ImportedTool::where('id', $id)->update(['shown_in_proff' => 'Y']);
            session()->flash('success', 'Now Imported is showned in professional.');
            return redirect()->back();
        }
    }
    /*
    @method         => statusChange
    @description    => Form status change .
    @author         => Soumojit
    @date           => 28/10/2020
    */
    public function statusChange($id = null)
    {
        if (!$id) {
            session()->flash('error', 'Something went wrong.');
            return redirect()->back();
        }
        $details = FormMaster::where('id', $id)->first();
        if (!$details) {
            session()->flash('error', 'Form not found.');
            return redirect()->back();
        }
        FormMaster::where('id', $id)->update(['status' => 'ACTIVE']);
        session()->flash('success', 'Form Active successfully.');
        return redirect()->back();
    }
    /*
    @method         => FormDetailsDelete
    @description    => Form Details Delete .
    @author         => Soumojit
    @date           => 29/10/2020
    */
    public function FormDetailsDelete($id = null)
    {
        if (!$id) {
            session()->flash('error', 'Something went wrong.');
            return redirect()->back();
        }
        $details = FormMasterDetails::where('id', $id)->first();
        if (!$details) {
            session()->flash('error', 'Form not found.');
            return redirect()->back();
        }
        FormMasterDetails::where('id', $id)->update(['status' => 'DELETED']);
        session()->flash('success', 'Form Deleted successfully.');
        return redirect()->back();
    }
}
