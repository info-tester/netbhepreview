<?php

namespace App\Http\Controllers\Admin\Modules\Coupon;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Coupon;

class CouponController extends Controller
{
    /**
     * For view all Coupon
     * @method: index
     */
    public function index(){
        if (!adminHasAccessToMenu(21)) {
            return redirect()->route('admin.dashboard');
        }
        $allCoupon =Coupon::whereIn('coupon_status',['A','I'])->orderBy('id','desc')->get();
        return view('admin.modules.coupon.coupon')->with(['allCoupon'=>$allCoupon]);
    }
    /**
     * For view Add Coupon
     * @method: create
     */
    public function create(){
        if (!adminHasAccessToMenu(21)) {
            return redirect()->route('admin.dashboard');
        }
        return view('admin.modules.coupon.add_coupon');
    }
    /**
     * For Add Coupon
     * @method: store
     */
    public function store(Request $request){
        if (!adminHasAccessToMenu(21)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            "coupon_code"=>  "required|unique:coupons",
            "discount_amount"=>  "required",
            "start_date"=>  "required",
            "exp_date"=>  "required",
        ]);
        $addCoupon=Coupon::create([
            'coupon_code'=>$request->coupon_code,
            'discount'=>$request->discount_amount,
            'added_by'=>'A',
            'start_date'=> date("Y-m-d", strtotime($request->start_date)),
            'exp_date'=> date("Y-m-d", strtotime($request->exp_date)),
            'coupon_status'=>'A',
        ]);
        if(@$addCoupon){
            session()->flash("success", "Coupon Added");
            return redirect()->route('admin.coupon');
        }
        session()->flash("error", "Coupon Not Added");
        return redirect()->back();
    }
    /**
     * For view edit Coupon
     * @method: edit
     */
    public function edit($id)
    {
        if (!adminHasAccessToMenu(21)) {
            return redirect()->route('admin.dashboard');
        }
        $coupon = Coupon::where('id',$id)->first();
        return view('admin.modules.coupon.add_coupon')->with(['coupon'=>$coupon]);
    }
    /**
     * For Update Coupon
     * @method: update
     */
    public function update($id,Request $request)
    {
        if (!adminHasAccessToMenu(21)) {
            return redirect()->route('admin.dashboard');
        }
        $oldCouponCode = Coupon::where('id', $id)->first();
        $request->validate([
            "coupon_code" =>  "required|unique:coupons,coupon_code,". $oldCouponCode->id,
            "discount_amount" =>  "required",
            "start_date" =>  "required",
            "exp_date" =>  "required",
        ]);
        $updateCoupon = Coupon::where('id',$id)->update([
            'coupon_code' => $request->coupon_code,
            'discount' => $request->discount_amount,
            'start_date' => date("Y-m-d", strtotime($request->start_date)),
            'exp_date' => date("Y-m-d", strtotime($request->exp_date))
        ]);
        if (@$updateCoupon) {
            session()->flash("success", "Coupon Updated");
            return redirect()->route('admin.coupon');
        }
        session()->flash("error", "Coupon Not Updated");
        return redirect()->back();
    }
    /**
     * For Delete Coupon
     * @method: delete
     */
    public function delete($id)
    {
        if (!adminHasAccessToMenu(21)) {
            return redirect()->route('admin.dashboard');
        }
        $deleteCoupon = Coupon::where('id',$id)->update(['coupon_status'=>'D']);
        if (@$deleteCoupon) {
            session()->flash("success", "Coupon Delete");
            return redirect()->route('admin.coupon');
        }
        session()->flash("error", "Coupon Not Delete");
        return redirect()->back();
    }
    /**
     * For change Status
     * @method: status
     */
    public function status($id)
    {
        if (!adminHasAccessToMenu(21)) {
            return redirect()->route('admin.dashboard');
        }
        $coupon = Coupon::where('id', $id)->whereIn('coupon_status',['A','I'])->first();
        if($coupon->coupon_status=='A'){
            $status = 'I';
            $message = 'Inactive';
        }elseif($coupon->coupon_status == 'I'){
            $status = 'A';
            $message = 'Active';
        }
        $statusChangeCoupon = Coupon::where('id', $id)->update(['coupon_status' => @$status]);
        if (@$statusChangeCoupon) {
            session()->flash("success", "Coupon ". $message);
            return redirect()->route('admin.coupon');
        }
        session()->flash("error", "Coupon Status Not Change");
        return redirect()->back();
    }
}
