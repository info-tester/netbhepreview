<?php

namespace App\Http\Controllers\Admin\Modules\EvaluationType;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ToolsEvaluationCategory;
use App\Models\UserContentTemplate;
use App\Models\UserToTools;
use App\Models\TypeOfEvaluationMaster;
use App\Models\ProfessionalSpecialty;
use App\User;

use Session;
use Validator;
use Auth;



class EvaluationTypeController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }

    /*
    @method         => manage
    @description    => Form manage
    @author         => munmun
    @date           => 20/03/2020
    */



    public function index(Request $request)
    {
        if (!adminHasAccessToMenu(63)) {
            return redirect()->route('admin.dashboard');
        }
        $data['all_evalution_type'] = TypeOfEvaluationMaster::orderBy('id', 'DESC')->with('category')->get();
        // dd($data['details']);

        return view('admin.modules.evaluation_type.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (!adminHasAccessToMenu(63)) {
            return redirect()->route('admin.dashboard');
        }
        $data = array();
        $categories = ToolsEvaluationCategory::get();
        $specialities = ProfessionalSpecialty::orderBy('id', 'DESC')->get();
        return view('admin.modules.evaluation_type.create', compact('categories', 'specialities'));
        //return view('admin.modules.product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!adminHasAccessToMenu(63)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            'title' => 'required',
            'evaluation_category_id' => 'required',
        ]);
        $insert['title'] = $request->title;
        $insert['evaluation_category_id'] = $request->evaluation_category_id;

        $eval_type = TypeOfEvaluationMaster::create($insert);
        $eval_type->specialities()->sync($request->speciality_ids);

        session()->flash('success', 'Evaluation type added successfully.');
        return redirect()->back();
    }


    //product Details Show

    public function show($id)
    {
        if (!adminHasAccessToMenu(63)) {
            return redirect()->route('admin.dashboard');
        }
        $details = TypeOfEvaluationMaster::find($id);
        return view('admin.modules.evaluation_type.show', [
            'details'       => $details
        ]);
    }



    /**
     * Show the form for editing a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!adminHasAccessToMenu(63)) {
            return redirect()->route('admin.dashboard');
        }
        $details = TypeOfEvaluationMaster::where('id', $id)->first();
        $categories = ToolsEvaluationCategory::get();
        $specialities = ProfessionalSpecialty::orderBy('id', 'DESC')->get();
        $related_spec = $details->specialities()->get();
        $related_spec_ids = array();
        foreach($related_spec as $ar){
            array_push($related_spec_ids, $ar->id);
        }

        return view('admin.modules.evaluation_type.edit')->with(compact('details', 'categories', 'specialities', 'related_spec_ids'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!adminHasAccessToMenu(63)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            'title' => 'required',
            'evaluation_category_id' => 'required',
        ]);
        $insert['title'] = $request->title;
        $insert['evaluation_category_id'] = $request->evaluation_category_id;

        $eval_type = TypeOfEvaluationMaster::where('id', $id)->first();
        $eval_type->update($insert);
        $eval_type->specialities()->sync($request->speciality_ids);

        session()->flash('success', 'Evaluation type edited successfully.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     *  @param  int  $id
     *  @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $type = TypeOfEvaluationMaster::find($id);
        if ($type->toCompetenceMasters()->exists()) {
            session()->flash('error', 'One or more competences already exists for this evaluation type. You cannot delete this evaluation type.');
            return redirect()->route('evaluation-type.index');
        }
        $type->delete();
        session()->flash('success', 'Evaluation type has been deleted successfully.');
        return redirect()->route('evaluation-type.index');
    }

    public function evaluationCategory()
    {
        if (!adminHasAccessToMenu(62)) {
            return redirect()->route('admin.dashboard');
        }
        $categories = ToolsEvaluationCategory::get();
        return view('admin.modules.evaluation_type.category', compact('categories'));
    }

    public function addEvaluationCategory(Request $request)
    {
        if (!adminHasAccessToMenu(62)) {
            return redirect()->route('admin.dashboard');
        }
        $data['specialities'] = ProfessionalSpecialty::orderBy('id', 'DESC')->get();
        if (@$request->all()) {
            $request->validate([
                'name' => 'required'
            ]);
            $eval_cat = ToolsEvaluationCategory::create([
                'name' => $request->name
            ]);
            $eval_cat->specialities()->sync($request->speciality_ids);
            session()->flash('success', 'Evaluation category added successfully.');
            return redirect()->back();
        }
        return view('admin.modules.evaluation_type.category_edit')->with($data);
    }

    public function editEvaluationCategory(Request $request, $id)
    {
        if (!adminHasAccessToMenu(62)) {
            return redirect()->route('admin.dashboard');
        }
        $category = ToolsEvaluationCategory::find($id);
        $specialities = ProfessionalSpecialty::orderBy('id', 'DESC')->get();
        $related_spec = $category->specialities()->get();
        $related_spec_ids = array();
        foreach($related_spec as $ar){
            array_push($related_spec_ids, $ar->id);
        }
        if ($category == null) {
            session()->flash('error', 'Evaluation category does not exist.');
            return redirect()->back();
        }
        if (@$request->all()) {
            $request->validate([
                'name' => 'required'
            ]);
            $category->update([
                'name' => $request->name
            ]);
            $category->specialities()->sync($request->speciality_ids);
            session()->flash('success', 'Evaluation category updated successfully.');
            return redirect()->back();
        }
        return view('admin.modules.evaluation_type.category_edit', compact('category', 'specialities', 'related_spec_ids'));
    }

    public function deleteEvaluationCategory($id)
    {
        if (!adminHasAccessToMenu(62)) {
            return redirect()->route('admin.dashboard');
        }
        $category = ToolsEvaluationCategory::find($id);
        if ($category == null) {
            session()->flash('error', 'Evaluation category does not exist.');
            return redirect()->route('evaluation.category');
        }
        $typesExists = TypeOfEvaluationMaster::where('evaluation_category_id', $id)->exists();
        if ($typesExists) {
            session()->flash('error', 'One or more evaluation types already exists for this category. You cannot delete this category.');
            return redirect()->route('evaluation.category');
        }
        $category->delete();
        session()->flash('success', 'Evaluation Category deleted.');
        return redirect()->route('evaluation.category');
    }

    public function fetchEvalCats(Request $request){
        $specialities = $request->arr;
        $eval_cat_ids = array();
        
        foreach($specialities as $id){
            $speciality = ProfessionalSpecialty::where('id', $id)->first();
            $eval_cats = $speciality->evalCats()->get();
            foreach($eval_cats as $eval_cat){
                if(!in_array($eval_cat->id, $eval_cat_ids)){
                    array_push($eval_cat_ids, $eval_cat->id);
                }
            }
        }
        sort($eval_cat_ids);
        $eval_cats = array();
        foreach($eval_cat_ids as $id){
            $speciality = ToolsEvaluationCategory::where('id', $id)->first()->toArray();
            array_push($eval_cats, $speciality);
        }
        return $eval_cats;
    }
}
