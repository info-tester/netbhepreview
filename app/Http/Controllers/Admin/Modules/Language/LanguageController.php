<?php

namespace App\Http\Controllers\Admin\Modules\Language;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\Models\LogBookMaster;
use App\Models\Language;
use Auth;
use DB;



class LanguageController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }

    public function index(Request $request)
    {
        if (!adminHasAccessToMenu(15)) {
            return redirect()->route('admin.dashboard');
        }
        $data['languages'] = Language::orderBy('name', 'ASC')->get();

        return view('admin.modules.language.manage_language')->with($data);
    }

    public function store(Request $request)
    {
        if (!adminHasAccessToMenu(15)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            'language_name'    => 'required'
        ]);
        $lang = Language::where(DB::raw('lower(name)'), strtolower(@$request->language_name))->first();
        if(@$lang){
            session()->flash('error', 'Language Already Exists.');
        } else {
            $ins=[];
            $ins['name']                = $request->language_name;
            Language::create($ins);
            session()->flash('success', 'Language Added Successfully.');
        }
        return redirect()->back();
    }

    public function show($id=null)
    {


        $details = Language::find($id);
        if($id!=null){
            if($details==null){
                session()->flash('error', 'unauthorize access.');
                return redirect()->back();
            }
        }

        return view('admin.modules.language.add_edit', [
            'details'       => $details
        ]);

    }
    public function update(Request $request, $id)
    {
        if (!adminHasAccessToMenu(15)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            'language_name'    => 'required'
        ]);
        $details = Language::find($id);
            if($details==null){
                session()->flash('error', 'unauthorize access.');
                return redirect()->back();
            }

        $upd=[];
        $upd['name']                = $request->language_name;
        Language::where('id',$id)->update($upd);


        session()->flash('success', 'Language updated Successfully.');
        return redirect()->back();
    }
}
