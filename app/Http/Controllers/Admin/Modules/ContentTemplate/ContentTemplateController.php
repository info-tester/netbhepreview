<?php

namespace App\Http\Controllers\Admin\Modules\ContentTemplate;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\ProfessionalSpecialty;
use App\Models\UserContentTemplate;
use App\Models\UserToTools;
use App\User;

use Session;
use Validator;
use Auth;



class ContentTemplateController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }

    /*
    @method         => manage
    @description    => Form manage
    @author         => munmun
    @date           => 20/03/2020
    */



    public function index()
    {
        if (!adminHasAccessToMenu(59)) {
            return redirect()->route('admin.dashboard');
        }
        $data = array();
        $data['title'] = 'Manage Content templates';

        //$data['all_paid_users'] = Booking::select('user_id')->distinct()->with('userDetails')->where('professional_id',Auth::id())->where('payment_status','P')->get();

        $data['all_content_template'] = UserContentTemplate::with('getUserData')->/*where('status','ACTIVE')->*/orderBy('id', 'DESC')->get();

        return view('admin.modules.content_template.index')->with($data);
    }

    /*
    @method         => create
    @description    => Form create in form_master table
    @author         => munmun
   @date           => 20/03/2020
    */
    public function create(Request $request)
    {
        if (!adminHasAccessToMenu(59)) {
            return redirect()->route('admin.dashboard');
        }
        $data['specialities'] = ProfessionalSpecialty::orderBy('id', 'DESC')->get();
        return view('admin.modules.content_template.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */




    public function store(Request $request)
    {
        if (!adminHasAccessToMenu(59)) {
            return redirect()->route('admin.dashboard');
        }
        $nowtime = date("Y-m-d H:i:s", time());

        $request->validate([
            'cont_subject'         => 'required',
            'cont_description'   => 'required',
        ]);



        // $content_temp = new UserContentTemplate;

        $insert = array(); 

        $insert['cont_subject']         = $request->cont_subject;
        $insert['cont_description']     = $request->cont_description;
        $insert['cont_category']        = $request->cont_category;
        //$insert['cont_video']           = @$youtube_videoid;

        $insert['status']               = 'ACTIVE';
        $insert['added_by']             = 'A';
        $insert['added_by_id']          = Auth::guard('admin')->id();

        if ($request->cont_category == "video") {
            if ($request->cont_cat_video_img_audio) {
                $youtube_videoid = getYouTubeIdFromURL($request->cont_cat_video_img_audio);
                $insert['cont_cat_video_img_audio'] = $youtube_videoid;
            }
        }
        if ($request->cont_category == "image") {
            if (@$request->cont_cat_video_img_audio) {
                $attachment = time() . '_' . $request->cont_cat_video_img_audio->getClientOriginalName();
                $destinationPath = "storage/app/public/uploads/user_template_content/";
                $request->cont_cat_video_img_audio->move($destinationPath, $attachment);

                $insert['cont_cat_video_img_audio'] = $attachment;
            }
        }
        if ($request->cont_category == "audio") {
            if (@$request->cont_cat_video_img_audio) {
                $attachment = time() . '_' . $request->cont_cat_video_img_audio->getClientOriginalName();
                $destinationPath = "storage/app/public/uploads/user_template_content/audio/";
                $request->cont_cat_video_img_audio->move($destinationPath, $attachment);

                $insert['cont_cat_video_img_audio'] = $attachment;
            }
        }
        
        $content_temp = UserContentTemplate::create($insert);
        $content_temp->specialities()->sync($request->speciality_ids);

        session()->flash('success', 'New content template inserted successfully');
        return redirect()->back();
    }


    /*
    @method         => edit
    @description    => Form edit view
    @author         => munmun
    @date           => 20/03/2020
    */
    public function edit($id)
    {
        if (!adminHasAccessToMenu(59)) {
            return redirect()->route('admin.dashboard');
        }
        $data = array();
        $data['details'] = UserContentTemplate::find($id);
        $data['specialities'] = ProfessionalSpecialty::orderBy('id', 'DESC')->get();
        $related_spec = $data['details']->specialities()->get();
        $data['related_spec_ids'] = array();
        foreach($related_spec as $ar){
            array_push($data['related_spec_ids'], $ar->id);
        }

        return view('admin.modules.content_template.edit')->with($data);
    }

    /*
    @method         => update
    @description    => Form update in form_master table
    @author         => munmun
    @date           => 20/03/2020
    */
    public function update($id, Request $request)
    {
        if (!adminHasAccessToMenu(59)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            'cont_subject'         => 'required',
            'cont_description'   => 'required',
        ]);

        //$update['title']     = $request->title;

        $update['cont_subject']                = $request->cont_subject;
        $update['cont_description']            = $request->cont_description;
        $update['cont_category']               = $request->cont_category;

        if ($request->cont_category == "video") {
            if ($request->cont_cat_video_img_audio) {
                $youtube_videoid = getYouTubeIdFromURL($request->cont_cat_video_img_audio);
                $update['cont_cat_video_img_audio'] = @$youtube_videoid;
            }
        }

        if ($request->cont_category == "image") {
            if (@$request->cont_cat_video_img_audio) {
                $attachment = time() . '_' . $request->cont_cat_video_img_audio->getClientOriginalName();
                $destinationPath = "storage/app/public/uploads/user_template_content/";
                $request->cont_cat_video_img_audio->move($destinationPath, $attachment);

                $update['cont_cat_video_img_audio'] = $attachment;
            }
        }
        if ($request->cont_category == "audio") {
            if (@$request->cont_cat_video_img_audio) {
                $attachment = time() . '_' . $request->cont_cat_video_img_audio->getClientOriginalName();
                $destinationPath = "storage/app/public/uploads/user_template_content/audio/";
                $request->cont_cat_video_img_audio->move($destinationPath, $attachment);

                $update['cont_cat_video_img_audio'] = $attachment;
            }
        }
        $content_temp = UserContentTemplate::where('id', $id)->first();
        $content_temp->update($update);
        $content_temp->specialities()->sync($request->speciality_ids);


        session()->flash('success', 'Content template edited successfully.');
        return redirect()->back();
    }


    public function show($id)
    {
        if (!adminHasAccessToMenu(59)) {
            return redirect()->route('admin.dashboard');
        }
        $details = UserContentTemplate::find($id);
        //pr1($details->toArray());
        //die();
        return view('admin.modules.content_template.show', [
            'details'       => $details
            //'questions'  => $questions
            //'productImages' => $productImages,

        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     *  @param  int  $id
     *  @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!adminHasAccessToMenu(59)) {
            return redirect()->route('admin.dashboard');
        }
        $data = UserToTools::where('tool_id', $id)->where('tool_type', 'Content Templates')->first();

        if (@$data) {
            //echo "not delete";
            session()->flash('error', 'Content template tools not deleted because it is assigned user.');
            return redirect()->back();
        } else {
            //echo "delete";
            UserContentTemplate::where('id', $id)->delete();
            session()->flash('success', 'Content template deleted successfully.');
            return redirect()->back();
        }

        // UserContentTemplate::where('id', $id)->delete();
        // session()->flash('success', 'Form deleted successfully.');
        // return redirect()->back();

    }


    /*
    * Method: tools for professional show
    * Description: shown in professional of form tools
    * Author: munmun
    */
    public function contentToolShowProff($id)
    {
        if (!adminHasAccessToMenu(59)) {
            return redirect()->route('admin.dashboard');
        }
        $data = UserContentTemplate::find($id);
        //pr1($data);
        //die();

        if (@$data->shown_in_proff == 'Y') {
            // update status field to active
            UserContentTemplate::where('id', $id)->update(['shown_in_proff' => 'N']);
            session()->flash('success', 'Now content template tools is not showned in professional.');
            return redirect()->back();
        } else if (@$data->shown_in_proff == 'N') {
            // update status field to active
            UserContentTemplate::where('id', $id)->update(['shown_in_proff' => 'Y']);
            session()->flash('success', 'Now content template is showned in professional.');
            return redirect()->back();
        }
    }
}
