<?php

/**
 * @author Subir Gupta
 * @copyright Convergent Infoware Pvt. Ltd. 2020
 */

namespace App\Http\Controllers\Admin\Modules\MailTemplate;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MailTemplate;

class MailTemplateController extends Controller
{
    /**
     * Lists the email templates and searches through them
     *
     * @param Request $request
     */
    public function index(Request $request)
    {
        if (!adminHasAccessToMenu(20)) {
            return redirect()->route('admin.dashboard');
        }
        $templates = MailTemplate::get();
        return view('admin.modules.mail_template.index', [
            'templates' => $templates
        ]);
    }

    /**
     * Edit the email templates
     *
     * @param Request $request
     * @param string $id
     */
    public function edit(Request $request, $id)
    {
        if (!adminHasAccessToMenu(20)) {
            return redirect()->route('admin.dashboard');
        }
        $template = MailTemplate::find($id);
        if ($template == null) {
            session()->flash('error', 'Template not found.');
            return redirect()->back();
        }
        if ($request->all()) {
            $request->validate([
                'subject' => 'required',
                'content' => 'required',
            ]);
            MailTemplate::where('id', $id)->update([
                'subject' => $request->subject,
                'content' => $request->content,
            ]);
            session()->flash('success', 'Template updated successfully.');
            return redirect()->back();
        }
        return view('admin.modules.mail_template.edit', [
            'template' => $template
        ]);
    }
}
