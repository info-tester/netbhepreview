<?php

namespace App\Http\Controllers\Admin\Modules\Subadmin;

use App\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Mail;
use Session;
use App\Mail\EmailVerify;
use App\Mail\SubadminInfo;
use App\Models\AdminAccess;

class SubadminController extends Controller
{

    protected $redirectTo = '/admin/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Auth::guard('admin')->user()->user_type == "S") {
            session()->flash('error', 'Unauthorize access');
            return redirect()->back();
        }

        $subadmin = Admin::where('id', '!=', 1);
        $key = [];
        if (@$request->all()) {
            if (@$request->keyword) {

                $subadmin = $subadmin->where(function ($where) use ($request) {
                    $where->where('name', 'like', '%' . $request->keyword . '%')
                        ->orWhere('email', 'like', $request->keyword);
                });
            }

            if (@$request->keyword_status) {
                $subadmin = $subadmin->where('status', $request->keyword_status);
            }

            if (@$request->email) {
                $subadmin = $subadmin->where('email', $request->email);
            }
        }
        $subadmin = $subadmin->orderBy('id', 'desc')->get();
        $key = $request->all();
        // dd($customers);
        return view('admin.modules.user.subadmin', [
            'subadmin' => $subadmin,
            'key'       => @$key
        ]);
    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Admin  $user
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $customer)
    {
        if (Auth::guard('admin')->user()->user_type == "S") {
            session()->flash('error', 'Unauthorize access');
            return redirect()->back();
        }
        $user = Admin::find($customer->id);
        return view('admin.modules.user.subadmin_details', [
            'user' => $user,
        ]);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($sub)
    {
        // user account delete
        if (Auth::guard('admin')->user()->user_type == "S") {
            session()->flash('error', 'Unauthorize access');
            return redirect()->back();
        }
        Admin::where('id', $sub)->delete();
        \Session::flash('success', 'subadmin deleted successfully.');
        return redirect()->back();
    }

    /**
     *@ for status Update
     */
    public function status($id)
    {
        if (Auth::guard('admin')->user()->user_type == "S") {
            session()->flash('error', 'Unauthorize access');
            return redirect()->back();
        }
        $sub = Admin::find($id);

        if (@$sub->status == 'A') {
            Admin::where('id', $id)->update(['status' => 'I']);

            \Session::flash('success', 'subadmin status updated successfully.');
            return redirect()->back();
        } elseif (@$sub->status == 'I') {
            Admin::where('id', $id)->update(['status' => 'A']);

            \Session::flash('success', 'subadmin status updated successfully.');
            return redirect()->back();
        }
    }


    public function reject($id)
    {
        if (Auth::guard('admin')->user()->user_type == "S") {
            session()->flash('error', 'Unauthorize access');
            return redirect()->back();
        }
        $sub = Admin::find($id);

        // delete from Admin
        Admin::destroy($id);
        if (@$sub->email) {
            Mail::send(new UserAccountReject($sub));
        }
        // Mail::send(new UserAccountReject($buyer));

        session()->flash('success', 'subadmin successfully rejected.');
        return redirect()->back();
    }

    public function addSubAdmin(Request $request)
    {
        if (Auth::guard('admin')->user()->user_type == "S") {
            session()->flash('error', 'Unauthorize access');
            return redirect()->back();
        }
        if ($request->all()) {
            $request->validate([
                'name'              =>  'required',
                'email'             =>  'required|email|unique:admins',
                'new_password'      =>  'required',
                'confirm_password'  =>  'required'
            ]);
            $data = [
                'name'              =>  $request->name,
                'email'             =>  $request->email,
                'password'          =>  \Hash::make($request->new_password),
                'user_type'         =>  'S'
            ];
            // dd($request->access);
            if($request->access == null){
                session()->flash('error', 'Please add acceesses to subadmin.');
                return redirect()->back();
            }
            $is_created = Admin::create($data);
            foreach ($request->access as $a) {
                $vals = explode('-', $a);
                $is_created->access()->create([
                    'access_id' => $vals[0],
                    'parent_access_id' => $vals[1],
                ]);
            }
            if ($is_created) {
                Mail::send(new SubadminInfo($request->all(), 'Y'));
                session()->flash('success', 'Subadmin Created Successfully');
                return redirect()->back();
            } else {
                session()->flash('error', 'Something went be wrong');
                return redirect()->back();
            }
        }
        $accesses = AdminAccess::where('parent_id', 0)->with('accesses')->get();
        return view('admin.modules.user.add_subadmin', compact('accesses'));
    }

    public function updateSubAdmin(Request $request, $id)
    {
        if (Auth::guard('admin')->user()->user_type == "S") {
            session()->flash('error', 'Unauthorize access');
            return redirect()->back();
        }
        $usr = Admin::where('id', '!=', 1)->where([
            'id'            =>  $id,
            'user_type'     =>  "S"
        ])->with('access')->first();
        if ($usr == null) {
            session()->flash('error', 'Unauthorize access');
            return redirect()->back();
        }
        if ($request->all()) {
            $this->validate($request, [
                'name'              =>  'required',
                'access'            =>  'required|array|min:1',
                // 'email'             =>  'required|email|unique:admins,email,' . $id
            ], [
                'access.required'            =>  'Please give access to atleast one section to subadmin.'
            ]);
            if ($request->email != $usr->email || \Hash::check($request->new_password, $usr->password)) {
                Mail::send(new SubadminInfo($request->all()));
            }
            $data = [
                'name'              =>  $request->name,
                'email'             =>  $request->email,
                'password'          => $request->new_password ? \Hash::make($request->new_password) : $usr->password,
                'user_type'         =>  'S'
            ];
            $usr->update($data);
            // Access Update
            $usr->access()->delete();
            foreach ($request->access as $a) {
                $vals = explode('-', $a);
                $usr->access()->create([
                    'access_id' => $vals[0],
                    'parent_access_id' => $vals[1],
                ]);
            }
            session()->flash('success', 'Subadmin Updated Successfully');
            return redirect()->back();
        }
        $accesses = AdminAccess::where('parent_id', 0)->with('accesses')->get();
        return view('admin.modules.user.edit_subadmin')->with([
            'sub'   =>  @$usr,
            'accesses' => $accesses,
        ]);
    }
}
