<?php

namespace App\Http\Controllers\Admin\Modules\AboutUs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Aboutus;
use validate;

class AboutUsController extends Controller
{
    protected $redirectTo = '/admin/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }

    public function index(Request $request)
    {
        if (!adminHasAccessToMenu(38)) {
            return redirect()->route('admin.dashboard');
        }
        $about = Aboutus::first();

        if ($about == null) {
            session()->flash('error', "An error occured into database.");
            return redirect()->back();
        }

        if ($request->all()) {

            $request->validate([
                'meta_title'        =>    'required',
                'meta_keyword'        =>    'required',
                'meta_description'    =>    'required',
                'short_description'    =>    'required',
                'long_description'    =>    'required',
                'our_vision'        =>    'required',
                'our_mission'        =>    'required',
                'our_team'            =>    'required'
            ]);


            $isUpdate = Aboutus::where('id', 1)->update($request->except('_token', 'image'));

            if ($request->image) {
                $path = 'storage/app/public/uploads/about_us/';
                $name = time() . '_' . $request->image->getClientOriginalName();
                if (file_exists($path . @$about->image)) {
                    unlink($path . @$about->image);
                }
                $about->image = $name;
                $request->image->move($path, $name);
                $about->save();
                session()->flash('success', 'About us content successfully updated.');
                return redirect()->back();
            }

            if ($isUpdate) {
                session()->flash('success', 'About us content successfully updated.');
                return redirect()->back();
            } else {
                session()->flash('error', "Something wen't wrong.");
                return redirect()->back();
            }
        }
        return view('admin.modules.about_us.about_us')->with([
            'about_us'    =>    @$about
        ]);
    }
}
