<?php

namespace App\Http\Controllers\Admin\Modules\Category;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Support\Facades\Storage;
use validator;
use App\Models\CategoryLandingPageMaster;

class CategoryLandingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }


    /*
    * Method: index
    * Description: for manage landing category
    * Author: Soumojit
    *Date: 23-SEP-2021
    */

    public function index(Request $request)
    {
        if (!adminHasAccessToMenu(13)) {
            return redirect()->route('admin.dashboard');
        }
        $data['categoryLanding']= CategoryLandingPageMaster::with('categoryDetails');
        if($request->all()){
            if($request->category_id){
                $data['categoryLanding']=$data['categoryLanding']->where('category_id',$request->category_id);
            }
            $data['key']=$request->all();

        }
        $data['categoryLanding']=$data['categoryLanding']->orderby('id','desc')->get();
        $data['p_cat'] = Category::where("parent_id", 0)->orderBy('name')->get();
        return view('admin.modules.category.manage_category_landing')->with($data);

    }

    /*
    * Method: categoryLandingAddEdit
    * Description: for view new landing add and update old landing update
    * Author: Soumojit
    *Date: 23-SEP-2021
    */
    public function categoryLandingAddEdit(Request $request,$id=null)
    {
        if (!adminHasAccessToMenu(13)) {
            return redirect()->route('admin.dashboard');
        }
        if($id!=null){
            $check= CategoryLandingPageMaster::where('id',$id)->first();
            if(@$check==null){
                session()->flash('error', 'something went wrong');
                return redirect()->back();
            }
            $data['content'] = $check;
        }
        $data['categoryLanding']= CategoryLandingPageMaster::select('id','category_id')->get();
        $cat=array();
        foreach($data['categoryLanding'] as $key){
            array_push($cat,$key->category_id);
        }
        $data['cat'] = Category::where("parent_id", 0)->whereNotIn('id',$cat)->get();

        return view("admin.modules.category.add_category_landing")->with($data);
    }
    /*
    * Method: categoryLandingAddEditSave
    * Description: for save new landing and update old landing
    * Author: Soumojit
    *Date: 23-SEP-2021
    */
    public function categoryLandingAddEditSave(Request $request ,$id=null){
        if (!adminHasAccessToMenu(13)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            "page_color"              =>  "required",
            "button_color"            =>  "required",
            "appearance"              =>  "required",
            "banner_heading"              =>  "required",
            "banner_heading_color"      =>  "required",
            "banner_button_text"        =>  "required",
            "banner_button_link"  =>  "required",
            "banner_desc"             =>  "required",
            "banner_desc_color"       =>  "required",
            "whyus_section_heading"             =>  "required",
            "whyus_section_desc"             =>  "required",
            "whyus_heading_1"             =>  "required",
            "whyus_desc_1"             =>  "required",
            "whyus_btn_text_1"             =>  "required",
            "whyus_btn_link_1"             =>  "required",
            "whyus_heading_2"             =>  "required",
            "whyus_desc_2"             =>  "required",
            "whyus_btn_text_2"             =>  "required",
            "whyus_btn_link_2"             =>  "required",
            "whyus_heading_3"             =>  "required",
            "whyus_desc_3"             =>  "required",
            "whyus_btn_text_3"             =>  "required",
            "whyus_btn_link_3"             =>  "required",
            "whyus_heading_4"             =>  "required",
            "whyus_desc_4"             =>  "required",
            "whyus_btn_text_4"             =>  "required",
            "whyus_btn_link_4"             =>  "required",
            "whatwedo_heading"             =>  "required",
            "whatwedo_desc"             =>  "required",
            "whatwedo_point_1"             =>  "required",
            "whatwedo_point_2"             =>  "required",
            "whatwedo_point_3"             =>  "required",
            "content_heading_1"             =>  "required",
            "content_desc_1"             =>  "required",
            "content_btn_text_1"             =>  "required",
            "content_btn_link_1"             =>  "required",
            "content_heading_2"             =>  "required",
            "content_desc_2"             =>  "required",
            "content_btn_text_2"             =>  "required",
            "content_btn_link_2"             =>  "required",
            "content_heading_3"             =>  "required",
            "content_desc_3"             =>  "required",
            "content_btn_text_3"             =>  "required",
            "content_btn_link_3"             =>  "required",
            "content_heading_4"             =>  "required",
            "content_desc_4"             =>  "required",
            "content_btn_text_4"             =>  "required",
            "content_btn_link_4"             =>  "required",
            // "testimonial_section_heading"             =>  "required",
            // "testimonial_section_desc"             =>  "required",
            // "testimonial_name_1"             =>  "required",
            // "testimonial_profession_1"             =>  "required",
            // "testimonial_desc_1"             =>  "required",
        ]);

        $ins=[];
        if($id==null){
            $request->validate([
                "category"              =>  "required",
            ]);
            $ins['category_id']=@$request->category;
        }else{
            $check= CategoryLandingPageMaster::where('id',$id)->first();
            @$data= $check;
            if(@$check==null){
                session()->flash('error', 'something went wrong');
                return redirect()->back();
            }
        }


        /* appearance */
        $ins['page_color'] = @$request->page_color;
        $ins['button_color'] = @$request->button_color;
        $ins['button_text_color'] = @$request->button_text_color;
        $ins['appearance'] = @$request->appearance;

        /* banner */
        $ins['alignment'] = @$request->alignment;
        $ins['banner_heading']=@$request->banner_heading;
        $ins['banner_heading_color']=@$request->banner_heading_color;
        $ins['banner_button_text']=@$request->banner_button_text;
        $ins['banner_button_link']=@$request->banner_button_link;
        $ins['banner_desc']=@$request->banner_desc;
        $ins['banner_desc_color']=@$request->banner_desc_color;
        if (@$request->banner_image) {
            $request->validate([
                'banner_image'    =>  'image|mimes:jpeg,png,jpg',
            ]);
            $modal_image_pic = $request->banner_image;
            $filename = time() . '-' . rand(1000, 9999) . '.' . $modal_image_pic->getClientOriginalExtension();
            Storage::putFileAs('public/uploads/category_landing_images', $modal_image_pic, $filename);
            @unlink(storage_path('app/public/uploads/category_landing_images/' . @$data->modal_image));
            $ins['banner_image'] = $filename;
        }

        /*banner */
        /* whyus_section */
        $ins['whyus_section_heading']=@$request->whyus_section_heading;
        $ins['whyus_section_desc']=@$request->whyus_section_desc;
        /* whyus_section */


        /* whyus_section 1 */
        $ins['whyus_heading_1']=@$request->whyus_heading_1;
        $ins['whyus_btn_text_1']=@$request->whyus_btn_text_1;
        $ins['whyus_btn_link_1']=@$request->whyus_btn_link_1;
        $ins['whyus_desc_1']=@$request->whyus_desc_1;
        if (@$request->whyus_image_1) {
            $request->validate([
                'whyus_image_1'    =>  'image|mimes:jpeg,png,jpg',
            ]);
            $modal_image_pic = $request->whyus_image_1;
            $filename = time() . '-' . rand(1000, 9999) . '.' . $modal_image_pic->getClientOriginalExtension();
            Storage::putFileAs('public/uploads/category_landing_images', $modal_image_pic, $filename);
            @unlink(storage_path('app/public/uploads/category_landing_images/' . @$data->whyus_image_1));
            $ins['whyus_image_1'] = $filename;
        }

        /*whyus_section 1 */

        /* whyus_section 2 */
        $ins['whyus_heading_2']=@$request->whyus_heading_2;
        $ins['whyus_btn_text_2']=@$request->whyus_btn_text_2;
        $ins['whyus_btn_link_2']=@$request->whyus_btn_link_2;
        $ins['whyus_desc_2']=@$request->whyus_desc_2;
        if (@$request->whyus_image_2) {
            $request->validate([
                'whyus_image_2'    =>  'image|mimes:jpeg,png,jpg',
            ]);
            $modal_image_pic = $request->whyus_image_2;
            $filename = time() . '-' . rand(1000, 9999) . '.' . $modal_image_pic->getClientOriginalExtension();
            Storage::putFileAs('public/uploads/category_landing_images', $modal_image_pic, $filename);
            @unlink(storage_path('app/public/uploads/category_landing_images/' . @$data->whyus_image_2));
            $ins['whyus_image_2'] = $filename;
        }

        /*whyus_section 2 */

        /* whyus_section 3 */
        $ins['whyus_heading_3']=@$request->whyus_heading_3;
        $ins['whyus_btn_text_3']=@$request->whyus_btn_text_3;
        $ins['whyus_btn_link_3']=@$request->whyus_btn_link_3;
        $ins['whyus_desc_3']=@$request->whyus_desc_3;
        if (@$request->whyus_image_3) {
            $request->validate([
                'whyus_image_3'    =>  'image|mimes:jpeg,png,jpg',
            ]);
            $modal_image_pic = $request->whyus_image_3;
            $filename = time() . '-' . rand(1000, 9999) . '.' . $modal_image_pic->getClientOriginalExtension();
            Storage::putFileAs('public/uploads/category_landing_images', $modal_image_pic, $filename);
            @unlink(storage_path('app/public/uploads/category_landing_images/' . @$data->whyus_image_3));
            $ins['whyus_image_3'] = $filename;
        }

        /*whyus_section 3 */

        /* whyus_section 4 */
        $ins['whyus_heading_4']=@$request->whyus_heading_4;
        $ins['whyus_btn_text_4']=@$request->whyus_btn_text_4;
        $ins['whyus_btn_link_4']=@$request->whyus_btn_link_4;
        $ins['whyus_desc_4']=@$request->whyus_desc_4;
        if (@$request->whyus_image_4) {
            $request->validate([
                'whyus_image_4'    =>  'image|mimes:jpeg,png,jpg',
            ]);
            $modal_image_pic = $request->whyus_image_4;
            $filename = time() . '-' . rand(1000, 9999) . '.' . $modal_image_pic->getClientOriginalExtension();
            Storage::putFileAs('public/uploads/category_landing_images', $modal_image_pic, $filename);
            @unlink(storage_path('app/public/uploads/category_landing_images/' . @$data->whyus_image_4));
            $ins['whyus_image_4'] = $filename;
        }

        /*whyus_section 4 */

        /* What We Do */
        $ins['whatwedo_heading']=@$request->whatwedo_heading;
        $ins['whatwedo_desc']=@$request->whatwedo_desc;
        $ins['whatwedo_point_1']=@$request->whatwedo_point_1;
        $ins['whatwedo_point_2']=@$request->whatwedo_point_2;
        $ins['whatwedo_point_3']=@$request->whatwedo_point_3;
        if (@$request->whatwedo_image) {
            $request->validate([
                'whatwedo_image'    =>  'image|mimes:jpeg,png,jpg',
            ]);
            $modal_image_pic = $request->whatwedo_image;
            $filename = time() . '-' . rand(1000, 9999) . '.' . $modal_image_pic->getClientOriginalExtension();
            Storage::putFileAs('public/uploads/category_landing_images', $modal_image_pic, $filename);
            @unlink(storage_path('app/public/uploads/category_landing_images/' . @$data->whatwedo_image));
            $ins['whatwedo_image'] = $filename;
        }

        /*What We Do */



        /* content 1 */
        $ins['content_heading_1']=@$request->content_heading_1;
        $ins['content_desc_1']=@$request->content_desc_1;
        $ins['content_btn_text_1']=@$request->content_btn_text_1;
        $ins['content_btn_link_1']=@$request->content_btn_link_1;
        if (@$request->content_image_1) {
            $request->validate([
                'content_image_1'    =>  'image|mimes:jpeg,png,jpg',
            ]);
            $modal_image_pic = $request->content_image_1;
            $filename = time() . '-' . rand(1000, 9999) . '.' . $modal_image_pic->getClientOriginalExtension();
            Storage::putFileAs('public/uploads/category_landing_images', $modal_image_pic, $filename);
            @unlink(storage_path('app/public/uploads/category_landing_images/' . @$data->content_image_1));
            $ins['content_image_1'] = $filename;
        }

        /*content 1 */

        /* content 2 */
        $ins['content_heading_2']=@$request->content_heading_2;
        $ins['content_desc_2']=@$request->content_desc_2;
        $ins['content_btn_text_2']=@$request->content_btn_text_2;
        $ins['content_btn_link_2']=@$request->content_btn_link_2;
        if (@$request->content_image_2) {
            $request->validate([
                'content_image_2'    =>  'image|mimes:jpeg,png,jpg',
            ]);
            $modal_image_pic = $request->content_image_2;
            $filename = time() . '-' . rand(1000, 9999) . '.' . $modal_image_pic->getClientOriginalExtension();
            Storage::putFileAs('public/uploads/category_landing_images', $modal_image_pic, $filename);
            @unlink(storage_path('app/public/uploads/category_landing_images/' . @$data->content_image_2));
            $ins['content_image_2'] = $filename;
        }

        /*content 2 */

        /* content 3 */
        $ins['content_heading_3']=@$request->content_heading_3;
        $ins['content_desc_3']=@$request->content_desc_3;
        $ins['content_btn_text_3']=@$request->content_btn_text_3;
        $ins['content_btn_link_3']=@$request->content_btn_link_3;
        if (@$request->content_image_3) {
            $request->validate([
                'content_image_3'    =>  'image|mimes:jpeg,png,jpg',
            ]);
            $modal_image_pic = $request->content_image_3;
            $filename = time() . '-' . rand(1000, 9999) . '.' . $modal_image_pic->getClientOriginalExtension();
            Storage::putFileAs('public/uploads/category_landing_images', $modal_image_pic, $filename);
            @unlink(storage_path('app/public/uploads/category_landing_images/' . @$data->content_image_3));
            $ins['content_image_3'] = $filename;
        }

        /*content 3 */

        /* content 4 */
        $ins['content_heading_4']=@$request->content_heading_4;
        $ins['content_desc_4']=@$request->content_desc_4;
        $ins['content_btn_text_4']=@$request->content_btn_text_4;
        $ins['content_btn_link_4']=@$request->content_btn_link_4;
        if (@$request->content_image_4) {
            $request->validate([
                'content_image_4'    =>  'image|mimes:jpeg,png,jpg',
            ]);
            $modal_image_pic = $request->content_image_4;
            $filename = time() . '-' . rand(1000, 9999) . '.' . $modal_image_pic->getClientOriginalExtension();
            Storage::putFileAs('public/uploads/category_landing_images', $modal_image_pic, $filename);
            @unlink(storage_path('app/public/uploads/category_landing_images/' . @$data->content_image_4));
            $ins['content_image_4'] = $filename;
        }

        /*content  */

        /* testimonial section */
        $ins['hide_testimonal'] = @$request->hide_testimonal == 'on' ? 'Y' : 'N';
        $ins['testimonial_section_heading']=@$request->testimonial_section_heading;
        $ins['testimonial_section_desc']=@$request->testimonial_section_desc;

        /*testimonial section  */

        /* testimonial section 1 */
        $ins['testimonial_name_1']=@$request->testimonial_name_1;
        $ins['testimonial_profession_1']=@$request->testimonial_profession_1;
        $ins['testimonial_desc_1']=@$request->testimonial_desc_1;
        if (@$request->testimonial_file_1) {
            // $request->validate([
            //     'testimonial_file_1'    =>  'image|mimes:jpeg,png,jpg,mp4',
            // ]);
            $modal_image_pic = $request->testimonial_file_1;
            $filename = time() . '-' . rand(1000, 9999) . '.' . $modal_image_pic->getClientOriginalExtension();
            Storage::putFileAs('public/uploads/category_landing_images', $modal_image_pic, $filename);
            @unlink(storage_path('app/public/uploads/category_landing_images/' . @$data->testimonial_file_1));
            $exe = pathinfo($filename, PATHINFO_EXTENSION);
            if($exe=='mp4'){
                $ins['testimonial_filetype_1'] = 'V';
            }else{
                $ins['testimonial_filetype_1'] = 'I';
            }
            $ins['testimonial_file_1'] = $filename;
        }

        /*testimonial section  1*/

        /* testimonial section 2 */
        $ins['testimonial_name_2']=@$request->testimonial_name_2;
        $ins['testimonial_profession_2']=@$request->testimonial_profession_2;
        $ins['testimonial_desc_2']=@$request->testimonial_desc_2;
        if (@$request->testimonial_file_2) {
            // $request->validate([
            //     'testimonial_file_2'    =>  'mimes:jpeg,png,jpg,video/mp4',
            // ]);

            $modal_image_pic = $request->testimonial_file_2;
            $filename = time() . '-' . rand(1000, 9999) . '.' . $modal_image_pic->getClientOriginalExtension();
            Storage::putFileAs('public/uploads/category_landing_images', $modal_image_pic, $filename);
            @unlink(storage_path('app/public/uploads/category_landing_images/' . @$data->testimonial_file_2));
            $exe = pathinfo($filename, PATHINFO_EXTENSION);
            if($exe=='mp4'){
                $ins['testimonial_filetype_2'] = 'V';
            }else{
                $ins['testimonial_filetype_2'] = 'I';
            }
            $ins['testimonial_file_2'] = $filename;
        }

        /*testimonial section  2*/

        /* testimonial section 3 */
        $ins['testimonial_name_3']=@$request->testimonial_name_3;
        $ins['testimonial_profession_3']=@$request->testimonial_profession_3;
        $ins['testimonial_desc_3']=@$request->testimonial_desc_3;
        if (@$request->testimonial_file_3) {
            // $request->validate([
            //     'testimonial_file_3'    =>  'image|mimes:jpeg,png,jpg,mp4',
            // ]);
            $modal_image_pic = $request->testimonial_file_3;
            $filename = time() . '-' . rand(1000, 9999) . '.' . $modal_image_pic->getClientOriginalExtension();
            Storage::putFileAs('public/uploads/category_landing_images', $modal_image_pic, $filename);
            @unlink(storage_path('app/public/uploads/category_landing_images/' . @$data->testimonial_file_3));
            $exe = pathinfo($filename, PATHINFO_EXTENSION);
            if($exe=='mp4'){
                $ins['testimonial_filetype_3'] = 'V';
            }else{
                $ins['testimonial_filetype_3'] = 'I';
            }
            $ins['testimonial_file_3'] = $filename;
        }

        /*testimonial section  2*/



        if($id==null){
            CategoryLandingPageMaster::create($ins);
            session()->flash('success', 'Category Landing page added successfully');
            return redirect()->route('admin.category.landing.index');
        }else{
            CategoryLandingPageMaster::where('id',$check->id)->update($ins);
            session()->flash('success', 'Category Landing page Updated successfully');
            return redirect()->back();
        }
    }


    public function changeStatus($id){
        $page= CategoryLandingPageMaster::where('id',$id)->first();
        if(@$page){
            if(@$page->status == 'A'){
                $page->update(['status' => 'I']);
                session()->flash('success', 'Category Landing Page Inactivated Succesfully.');
                return redirect()->back();
            } else {
                $page->update(['status' => 'A']);
                session()->flash('success', 'Category Landing Page Activated Succesfully.');
                return redirect()->back();
            }
        } else {
            session()->flash('error', 'Category Landing Page Not Found');
            return redirect()->back();
        }
    }

    public function delete($id){
        $page = CategoryLandingPageMaster::where('id',$id)->first();
        if(@$page){
            $page->delete();
            session()->flash('success', 'Category Landing Page Deleted Successfully');
            return redirect()->back();
        } else {
            session()->flash('error', 'Category Landing Page Not Found');
            return redirect()->back();
        }
    }
    /*
    * Method: showInFooter
    * Description: for updating show in footer
    * Author: Soumojit
    *Date: 23-SEP-2021
    */
    public function showInFooter($id=null){
        $page = CategoryLandingPageMaster::where('id',$id)->first();
        if($page==null){
            session()->flash('error', 'Something went wrong');
            return redirect()->back();
        }
        if($page->show_in_footer=='Y'){
            CategoryLandingPageMaster::where('id',$id)->update(['show_in_footer'=>'N']);
            session()->flash('success', 'Category landing page not show in footer');
            return redirect()->back();
        }
        if($page->show_in_footer=='N'){
            CategoryLandingPageMaster::where('id',$id)->update(['show_in_footer'=>'Y']);
            session()->flash('success', 'Category landing show in footer');
            return redirect()->back();
        }

    }
}
