<?php

namespace App\Http\Controllers\Admin\Modules\Category;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use validator;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }

    /**
     *@ Method 		: index
     *@ Description 	: For Showing category list
     *@ Author 		: Abhisek
     *@ Use By       : Abhisek
     */

    public function index(Request $request)
    {
        if (!adminHasAccessToMenu(1)) {
            return redirect()->route('admin.dashboard');
        }
        $cat = Category::with('parent');
        $key = [];
        if ($request->all()) {
            if ($request->keyword) {
                $cat = $cat->where("name", "like", "%" . $request->keyword . "%");
            }
            if ($request->parent_id) {
                $cat = $cat->where("parent_id", $request->parent_id);
            }
            if ($request->status) {
                $cat = $cat->where("status", $request->status);
            }
        }
        $cat = $cat->get();
        $key = $request->all();
        $p_cat = Category::where("parent_id", 0)->orderBy('name')->get();
        return view("admin.modules.category.all_category")->with([
            "cat"    =>    $cat,
            "p_cat"    =>    $p_cat,
            "key"    =>    $key
        ]);
    }

    /**
     *@ Method 		: add
     *@ Description 	: For add category
     *@ Author 		: Abhisek
     *@ Use By       : Abhisek
     */

    public function add(Request $request)
    {
        if (!adminHasAccessToMenu(1)) {
            return redirect()->route('admin.dashboard');
        }
        if ($request->all()) {
            if (@$request->parent_id) {
                $request->validate([
                    "name"              =>  "required|unique:categories"
                ]);
                $cat_create = Category::create([
                    "name"              =>  $request->name,
                    "parent_id"         =>  $request->parent_id,
                    "slug"              =>  str_slug($request->name)
                ]);
                session()->flash('success', 'Subcategory created successfully');
                return redirect()->route('admin.add.categories');
            } else {
                $request->validate([
                    "name"              =>  "required|unique:categories",
                    /*"description"       =>  "required",*/
                    "meta_title"        =>  "required",
                    "meta_description"  =>  "required",
                    "image"             =>  "required"
                ]);
                if ($request->image) {
                    $size = getimagesize($request->image);
                    if ($size[0] != 276 || $size[1] != 183) {
                        session()->flash('error', 'Image size must be 276 X 183.');
                        return redirect()->route('admin.add.categories')->withInput($request->all());;
                    }
                }
                $cat_create = Category::create([
                    "name"              =>  $request->name,
                    "description"       =>  $request->description,
                    "meta_title"        =>  $request->meta_title,
                    "meta_description"  =>  $request->meta_description,
                    "parent_id"         =>  $request->parent_id != null ? $request->parent_id : 0
                ]);

                if ($cat_create) {
                    $img = $request->image;
                    $name = str_random(5) . time() . $cat_create->id . '.' . $img->getClientOriginalExtension();
                    $img->move("storage/app/public/category_images/", $name);
                    $cat_update = [
                        "image" =>  $name,
                        "slug"  =>  str_slug($cat_create->name . '-' . $cat_create->id)
                    ];
                    Category::where("id", $cat_create->id)->update($cat_update);
                    session()->flash("success", "Category successfully created");
                    return redirect()->route('admin.add.categories');
                } else {
                    session()->flash("error", "Something went be wrong");
                    return redirect()->route('admin.add.categories');
                }
            }
        }
        $cat = Category::where("parent_id", 0)->get();
        return view("admin.modules.category.add_category")->with([
            "cat"    =>    $cat
        ]);
    }


    /**
     *@ Method 		: status
     *@ Description 	: For updating category status
     *@ Author 		: Abhisek
     *@ Use By       : Abhisek
     */

    public function status($id)
    {
        if (!adminHasAccessToMenu(1)) {
            return redirect()->route('admin.dashboard');
        }
        $cat = Category::find($id);
        if ($cat == null) {
            session()->flash('error', "Unauthorize acccess");
            return redirect()->back();
        } else {
            $cat->status = $cat->status == "A" ? "I" : "A";
            $cat->save();
            session()->flash('success', "Category status successfully updated");
            return redirect()->back();
        }
    }

    /**
     *@ Method 		: delete
     *@ Description 	: For deleting category
     *@ Author 		: Abhisek
     *@ Use By       : Abhisek
     */

    public function delete($id)
    {
        if (!adminHasAccessToMenu(1)) {
            return redirect()->route('admin.dashboard');
        }
        $cat = Category::find($id);
        if ($cat == null) {
            session()->flash('error', "Unauthorize acccess.");
            return redirect()->back();
        } else {
            $is_parent = Category::where("parent_id", $id)->first();
            if ($is_parent != null) {
                session()->flash('error', "This category is associated with another sub category.");
                return redirect()->back();
            } else {
                //         if($cat->image!=null){
                // unlink("storage/app/public/category_images".'/'.$cat->image);
                //         }

                Category::where("id", $id)->delete();
                session()->flash('success', "Category successfully removed.");
                return redirect()->back();
            }
        }
    }

    /**
     *@ Method 		: update
     *@ Description 	: For updateing category
     *@ Author 		: Abhisek
     *@ Use By       : Abhisek
     */

    public function update(Request $request, $id)
    {

        if (!adminHasAccessToMenu(1)) {
            return redirect()->route('admin.dashboard');
        }
        $cat = Category::find($id);
        if ($cat == null) {
            session()->flash('error', "Unauthorize acccess.");
            return redirect()->back();
        }
        if ($request->all()) {
            if (@$request->parent_id) {
                $request->validate([
                    "name"              =>  "required"
                ]);
                $unq_name = Category::where("name", $request->name)->where("id", "!=", $id)->first();
                if ($unq_name != null) {
                    session()->flash('error', "Duplicate category name.");
                    return redirect()->back();
                }

                $data = [
                    "name"              =>  $request->name,
                    "description"       =>  null,
                    "meta_title"        =>  null,
                    "meta_description"  =>  null,
                    "parent_id"         =>  $request->parent_id == null ? 0 : $request->parent_id,
                    "image"             =>  null,
                    "slug"              =>  str_slug($request->name)
                ];
                Category::where("id", $id)->update($data);
                session()->flash("success", "Sub Category successfully updated");
                return redirect()->back();
            } else {

                $request->validate([
                    "name"              =>  "required",
                    /*"description"       =>  "required",*/
                    "meta_title"        =>  "required",
                    "meta_description"  =>  "required"
                ]);
                $unq_name = Category::where("name", $request->name)->where("id", "!=", $id)->first();
                if ($unq_name != null) {
                    session()->flash('error', "Duplicate category name.");
                    return redirect()->back();
                }

                $data = [
                    "name"              =>  $request->name,
                    "description"       =>  $request->description,
                    "meta_title"        =>  $request->meta_title,
                    "meta_description"  =>  $request->meta_description,
                    "parent_id"         =>  $request->parent_id == null ? 0 : $request->parent_id
                ];

                Category::where("id", $id)->update($data);
                if ($request->image) {
                    $size = getimagesize($request->image);
                    if ($size[0] != 276 || $size[1] != 183) {
                        session()->flash('error', 'Image size must be 276 X 183.');
                        return redirect()->back();
                    }
                    $img = $request->image;
                    $name = str_random(5) . time() . $cat->id . '.' . $img->getClientOriginalExtension();
                    $img->move("storage/app/public/category_images/", $name);

                    $cat_update = [
                        "image" =>  $name,
                        "slug"  =>  str_slug($request->name . '-' . $cat->id)
                    ];
                    Category::where("id", $cat->id)->update($cat_update);
                }

                session()->flash("success", "Category successfully updated");
                return redirect()->back();
            }
        }
        $parent = Category::where("parent_id", 0)->get();
        return view("admin.modules.category.edit_category")->with([
            "category"    =>    $cat,
            "parent"    =>    $parent
        ]);
    }



    /*
    * Method: categoryShowHomepage
    * Description: shown in home page of category
    * Author: munmun
    */
    public function categoryShowHomepage($id)
    {
        if (!adminHasAccessToMenu(1)) {
            return redirect()->route('admin.dashboard');
        }
        $category = Category::find($id);

        if (@$category->shown_in_home == 'Y') {
            // update status field to active
            Category::where('id', $id)->update(['shown_in_home' => 'N']);

            session()->flash('success', 'Now Category is not shown in homepage.');
            return redirect()->back();
        } else if (@$category->shown_in_home == 'N') {
            // update status field to active
            Category::where('id', $id)->update(['shown_in_home' => 'Y']);

            session()->flash('success', 'Now category is shown in homepage.');
            return redirect()->back();
        }
    }

    /*
    * Method: showinCategorySearch
    * Description: for showing or removing from category search page
    * Author: Sayantani
    */
    public function showinCategorySearch($id)
    {
        if (!adminHasAccessToMenu(1)) {
            return redirect()->route('admin.dashboard');
        }
        $category = Category::find($id);

        if (@$category->shown_in_cat_search == 'Y') {
            // update status field to active
            Category::where('id', $id)->update(['shown_in_cat_search' => 'N']);

            session()->flash('success', 'Now Category is not shown in category search list.');
            return redirect()->back();
        } else if (@$category->shown_in_cat_search == 'N') {
            // update status field to active
            Category::where('id', $id)->update(['shown_in_cat_search' => 'Y']);

            session()->flash('success', 'Now category is shown in category search list.');
            return redirect()->back();
        }
    }
}
