<?php

namespace App\Http\Controllers\Admin\Modules\Specialty;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProfessionalSpecialty;
use App\User;

class SpecialtyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }

    /**
     * Shows all specialties
     */
    public function index()
    {
        if (!adminHasAccessToMenu(14)) {
            return redirect()->route('admin.dashboard');
        }
        $specialties = ProfessionalSpecialty::orderBy('id', 'DESC')->get();
        return view('admin.modules.specialty.index', compact('specialties'));
    }

    /**
     * Adds a specialty
     *
     * @param Request $request
     */
    public function add(Request $request)
    {
        if (!adminHasAccessToMenu(14)) {
            return redirect()->route('admin.dashboard');
        }
        if ($request->all()) {
            $request->validate([
                'name' => 'required',
                'entry_info_message' => 'required',
            ]);
            ProfessionalSpecialty::create([
                'name' => $request->name,
                'entry_info_message' => $request->entry_info_message,
            ]);
            session()->flash('success', 'Specialty added.');
            return redirect()->route('admin.specialty');
        }
        return view('admin.modules.specialty.add_edit');
    }

    /**
     * Edits a specialty
     *
     * @param Request $request
     * @param int $id
     */
    public function edit(Request $request, int $id)
    {
        if (!adminHasAccessToMenu(14)) {
            return redirect()->route('admin.dashboard');
        }
        $specialty = ProfessionalSpecialty::find($id);
        if ($specialty == null) {
            session()->flash('error', 'Specialty not found.');
            return redirect()->route('admin.specialty');
        }
        if ($request->all()) {
            $request->validate([
                'name' => 'required',
                'entry_info_message' => 'required',
            ]);
            $specialty->update([
                'name' => $request->name,
                'entry_info_message' => $request->entry_info_message,
            ]);
            session()->flash('success', 'Specialty updated.');
            return redirect()->route('admin.specialty');
        }
        return view('admin.modules.specialty.add_edit', compact('specialty'));
    }

    /**
     * Deletes a specialty
     *
     * @param Request $request
     * @param int $id
     */
    public function delete(Request $request, int $id)
    {
        if (!adminHasAccessToMenu(14)) {
            return redirect()->route('admin.dashboard');
        }
        $specialty = ProfessionalSpecialty::find($id);
        if ($specialty == null) {
            session()->flash('error', 'Specialty not found.');
            return redirect()->route('admin.specialty');
        }
        $hasUser = User::where('professional_specialty_id', $id)->exists();
        if ($hasUser) {
            session()->flash('error', 'Some professionals already using this specialty. You can not delete this.');
            return redirect()->route('admin.specialty');
        }
        $specialty->delete();
        session()->flash('success', 'Specialty deleted.');
        return redirect()->route('admin.specialty');
    }
}
