<?php

namespace App\Http\Controllers\Admin\Modules\Logbook;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\Models\LogBookMaster;
use App\Models\UserToTools;
use App\User;

use Session;
use Validator;
use Auth;



class LogbookController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }

    /*
    @method         => manage
    @description    => Form manage
    @author         => munmun
    @date           => 20/03/2020
    */



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!adminHasAccessToMenu(61)) {
            return redirect()->route('admin.dashboard');
        }
        //$data['all_paid_users'] = Booking::select('user_id')->distinct()->with('userDetails')->where('professional_id',Auth::id())->where('payment_status','P')->get();

        $data['all_log_book'] = LogBookMaster::where('status', '!=', 'DELETED')->orderBy('id', 'DESC')->with(['user'])->get();
        //dd($data['details']);

        return view('admin.modules.logbook.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (!adminHasAccessToMenu(61)) {
            return redirect()->route('admin.dashboard');
        }
        $data = array();
        return view('admin.modules.logbook.create');
        //return view('admin.modules.product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!adminHasAccessToMenu(61)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            'question'    => 'required'
        ]);


        // $insert['title']                = $request->title;
        // $insert['contract_desc']        = $request->contract_desc;
        // //$insert['status']                    = 'Form';
        // $insert['status']               = 'ACTIVE';
        // $insert['added_by']             = 'P';
        // $insert['added_by_id']          = Auth::id();
        // ContractTemplatMaster::create($insert);

        if ($request->question) {
            foreach ($request->question as $key => $value) {
                LogBookMaster::create([
                    'question'           =>  $value,
                    'added_by'           =>  "A",
                    'added_by_id'        =>  Auth::id(),
                    'status'             =>  "ACTIVE"
                ]);
            }
        }

        session()->flash('success', 'Logbook question added successfully.');
        return redirect()->back();
    }


    // Details Show

    public function show($id)
    {

        // $details = ContractTemplatMaster::with(['user'])->find($id);
        // return view('modules.logbook.show', [
        //     'details'       => $details
        // ]);

    }



    /**
     * Show the form for editing a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!adminHasAccessToMenu(61)) {
            return redirect()->route('admin.dashboard');
        }
        $data = array();
        $data['details'] = LogBookMaster::where('id', $id)->first();
        return view('admin.modules.logbook.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!adminHasAccessToMenu(61)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            'question'            => 'required'
        ]);

        $insert['question']                = $request->question;

        LogBookMaster::where('id', $id)->update($insert);
        session()->flash('success', 'Question edited successfully.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     *  @param  int  $id
     *  @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!adminHasAccessToMenu(61)) {
            return redirect()->route('admin.dashboard');
        }
        //pr1($id);
        //die();
        $data = UserToTools::where('tool_id', $id)->where('tool_type', "Log Book")->first();
        if (@$data) {
            //echo "not delete";
            session()->flash('error', 'logbook not deleted because it is assigned user.');
            return redirect()->back();
        } else {
            //echo "delete";
            LogBookMaster::where('id', $id)->delete();
            session()->flash('success', 'Logbook deleted successfully.');
            return redirect()->back();
        }
    }


    public function logbookToolShowProff($id)
    {
        if (!adminHasAccessToMenu(61)) {
            return redirect()->route('admin.dashboard');
        }
        $data = LogBookMaster::find($id);
        //pr1($data);
        //die();

        if (@$data->shown_in_proff == 'Y') {
            // update status field to active
            LogBookMaster::where('id', $id)->update(['shown_in_proff' => 'N']);
            session()->flash('success', 'Now logbook tools is not showned in professional.');
            return redirect()->back();
        } else if (@$data->shown_in_proff == 'N') {
            // update status field to active
            LogBookMaster::where('id', $id)->update(['shown_in_proff' => 'Y']);
            session()->flash('success', 'Now logbook is showned in professional.');
            return redirect()->back();
        }
    }
}
