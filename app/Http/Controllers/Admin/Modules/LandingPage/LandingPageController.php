<?php

namespace App\Http\Controllers\Admin\Modules\LandingPage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\LandingPageLead;
use App\Models\LandingPageMaster;
use App\Models\LandingPagePayment;
use App\User;
use App\Models\LandingPagePricing;

class LandingPageController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }
    /*
    * Method: index
    * Description: for view created template by professional
    * Author: Soumojit
    *Date: 17-SEP-2021
    */
    public function index(Request $request)
    {
        if (!adminHasAccessToMenu(25)) {
            return redirect()->route('admin.dashboard');
        }
        $data['experts']= User::where("is_professional", "Y")
        ->select('id','name','is_professional','nick_name')
        ->orderBy("name")->get();

        $data['templates'] = LandingPageMaster::where('status', '!=', 'D')->with([
            'userDetails'=>function($query){
                $query->select('id','name','slug','nick_name');
            },
        ]);
        if(@$request->all()){
            if(@$request->professional){
                $data['templates']=$data['templates']->where('user_id',$request->professional);
            }
            if(@$request->page_type){
                $data['templates']=$data['templates']->where('page_type',$request->page_type);
            }
            if(@$request->branding_free){
                $data['templates']=$data['templates']->where('is_branding_free',$request->branding_free);
            }
            if(@$request->template_id){
                $data['templates']=$data['templates']->where('landing_template_id',$request->template_id);
            }
            if(@$request->page_status){
                $data['templates']=$data['templates']->where('page_status',$request->page_status);
            }
            $data['key']=$request->all();
        }
        $data['templates']=$data['templates']->orderBy('id','desc')->get();

        return view('admin.modules.landing_page.landing_page_list')->with($data);
    }
    /*
    * Method: landingPagePrice
    * Description: Landing page price view
    * Author: Soumojit
    *Date: 17-SEP-2021
    */
    public function landingPagePrice(){
        if (!adminHasAccessToMenu(26)) {
            return redirect()->route('admin.dashboard');
        }
        $data['price'] = LandingPagePricing::first();
        return view('admin.modules.landing_page.landing_price')->with($data);
    }
    /*
    * Method: landingPagePriceUpdate
    * Description: Landing page price update
    * Author: Soumojit
    *Date: 17-SEP-2021
    */
    public function landingPagePriceUpdate(Request $request){
        if (!adminHasAccessToMenu(26)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            "price_branding_free" => "required",
            "price_per_page" => "required",
            "free_landing_pages" => "required",
        ]);
        $upd=[
            'price_branding_free'=>$request->price_branding_free,
            'price_per_page'=>$request->price_per_page,
            'free_landing_pages'=>$request->free_landing_pages,
        ];

        $updateCommission = LandingPagePricing::where('id',1)->update($upd);
        if (@$updateCommission) {
            session()->flash("success", "Landing Page Pice Changed");
            return redirect()->back();
        }
        session()->flash("error", "Landing Page Pice Not Changes");
        return redirect()->back();
    }
    /**
     * method:   viewLeads
     *created By: Soumojit
     *Description:  view Leads page wish
     *Date: 17-SEP-2021
     */
    public function viewLeads($id=null){

        $data['landing'] = LandingPageMaster::where('id', $id)->where('status', '!=', 'D')->first();
        if($data['landing']==null){
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->route('admin.landing.page.list');
        }
        $data['leads']=LandingPageLead::where('landing_page_master_id',$id)->orderBy('id','desc')->get();

        return view('admin.modules.landing_page.landing_page_leads')->with($data);
    }
    /**
     * method:   viewLeads
     *created By: Soumojit
     *Description:  view Leads page wish
     *Date: 17-SEP-2021
     */
    public function viewPayments(Request $request){
        if (!adminHasAccessToMenu(27)) {
            return redirect()->route('admin.dashboard');
        }
        $data['experts']= User::where("is_professional", "Y")
        ->select('id','name','is_professional','nick_name')
        ->orderBy("name")->get();

        $data['payments'] =LandingPagePayment::with(['landingDetails'])->where('payment_status','P');
        if(@$request->all()){
            if(@$request->token){
                $data['payments']=$data['payments']->where('token_no',$request->token);
            }
            if(@$request->professional){
                $data['payments']=$data['payments']->where('user_id',$request->professional);
            }
            if(@$request->payment_type){
                $data['payments']=$data['payments']->where('payment_type',$request->payment_type);
            }
            if(@$request->is_used){
                $data['payments']=$data['payments']->where('is_use',$request->is_used);
            }
            if(@$request->payment_for){
                $data['payments']=$data['payments']->where('payment_for',$request->payment_for);
            }
            $data['key']=$request->all();
        }
        $data['payments']=$data['payments']->orderBy('id','desc')->get();
        return view('admin.modules.landing_page.landing_page_payment')->with($data);
    }
    public function landingStatus($status=null,$id=null){
        $data['landing'] = LandingPageMaster::where('id', $id)->first();
        if($data['landing']==null){
            session()->flash('error', \Lang::get('client_site.unauthorize_access'));
            return redirect()->route('admin.landing.page.list');
        }
        if($status=='UP'){
            LandingPageMaster::where('id', $id)->update(['page_status'=>'UP']);
            session()->flash('success', 'Page Unblock Successfully');
            return redirect()->route('admin.landing.page.list');
        }
        if($status=='B'){
            LandingPageMaster::where('id', $id)->update(['page_status'=>'B']);
            session()->flash('success', 'Page Block Successfully');
            return redirect()->route('admin.landing.page.list');
        }
        session()->flash('error', 'Something went wrong');
        return redirect()->route('admin.landing.page.list');
    }
}
