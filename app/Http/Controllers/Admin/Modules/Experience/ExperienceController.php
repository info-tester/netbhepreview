<?php

namespace App\Http\Controllers\Admin\Modules\Experience;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProfessionalExperience;
use App\Models\Experience;
use App\User;
use Auth;
use DB;

class ExperienceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }

    /**
     * Shows all specialties
     */
    public function index(Request $request)
    {
        if (!adminHasAccessToMenu(16)) {
            return redirect()->route('admin.dashboard');
        }
        $experience = Experience::orderBy('id', 'DESC');

        if($request->all()){
            if($request->keyword){
                $experience = $experience->where('experience', 'like', '%'.$request->keyword.'%');
            }
            $data['key'] = $request->all();
        }
        $data['experiences'] = $experience->get();
        return view('admin.modules.experience.index')->with($data);
    }

    public function store(Request $request, $id=null)
    {
        if(@$id){
            $data['experience'] = Experience::where('id',$id)->first();
            return view('admin.modules.experience.add')->with($data);
        }
        if(@$request->all()){
            $request->validate([
                'experience' => 'required',
            ]);
            $insert['experience'] = $request->experience;

            if($request->ID){
                $exp = Experience::where('id',$request->ID)->first();
                if(@$exp){
                    $exists = Experience::where('id','!=', $request->ID)->whereRaw('UPPER(experience) = ?', strtoupper($request->experience))->first();
                    if(@$exists){
                        return redirect()->back()->with('error', 'An experience with this name already exists.');
                    }
                    $exp->update($insert);
                    return redirect()->back()->with('success', 'Experience name updated.');
                } else {
                    return redirect()->back()->with('error', 'Not found.');
                }
            } else {
                $exists = Experience::whereRaw('UPPER(experience) = ?', strtoupper($request->experience))->first();
                if(@$exists){
                    return redirect()->back()->with('error', 'An experience with this name already exists.');
                }
                $insert['added_by'] = 'A';
                $insert['user_id'] = Auth::guard('admin')->id();
                Experience::create($insert);
                return redirect()->back()->with('success', 'Experience created.');
            }
        }
        return view('admin.modules.experience.add');
    }

    public function delete($id)
    {
        $experience = Experience::where('id',$id)->first();
        if(@$experience){
            // delete professional experience
            $exp = DB::table('professional_experiences')->where('experience_id', $id)->delete();
            $experience->delete();
            return redirect()->back()->with('success', 'Experience deleted.');
        }
        return redirect()->back()->with('error', 'Experience not found.');
    }
}
