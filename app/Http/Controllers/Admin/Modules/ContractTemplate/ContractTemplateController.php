<?php

namespace App\Http\Controllers\Admin\Modules\ContractTemplate;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\ProfessionalSpecialty;
use App\Models\ContractTemplatMaster;
use App\Models\UserToTools;
use App\User;

use Session;
use Validator;
use Auth;



class ContractTemplateController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }

    /*
    @method         => manage
    @description    => Form manage
    @author         => munmun
    @date           => 20/03/2020
    */



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!adminHasAccessToMenu(60)) {
            return redirect()->route('admin.dashboard');
        }
        //$data['all_paid_users'] = Booking::select('user_id')->distinct()->with('userDetails')->where('professional_id',Auth::id())->where('payment_status','P')->get();

        $data['contract_templats'] = ContractTemplatMaster::with('user')->where('status', '!=', 'DELETED')->orderBy('id', 'DESC')->with(['user'])->get();
        // dd($data['details']);

        return view('admin.modules.contract_template.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (!adminHasAccessToMenu(60)) {
            return redirect()->route('admin.dashboard');
        }
        $data = array();
        $data['specialities'] = ProfessionalSpecialty::orderBy('id', 'DESC')->get();

        return view('admin.modules.contract_template.create')->with($data);
        //return view('admin.modules.product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!adminHasAccessToMenu(60)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            'title'            => 'required',
            'contract_desc'    => 'required'
        ]);
        $insert['title']                = $request->title;
        $insert['contract_desc']        = $request->contract_desc;
        //$insert['status']                    = 'Form';
        $insert['status']               = 'ACTIVE';
        $insert['added_by']             = 'A';
        $insert['added_by_id']          = Auth::guard('admin')->id();
        $contract_temp = ContractTemplatMaster::create($insert);
        $contract_temp->specialities()->sync($request->speciality_ids);

        session()->flash('success', 'Contract template added successfully.');
        return redirect()->back();
    }


    //product Details Show

    public function show($id)
    {
        if (!adminHasAccessToMenu(60)) {
            return redirect()->route('admin.dashboard');
        }
        $details = ContractTemplatMaster::with(['user'])->find($id);

        //$questions = FormMasterDetails::where('form_master_id',$id)->get();


        //pr1($data->toArray());
        //die();
        return view('admin.modules.contract_template.show', [
            'details'       => $details
        ]);
    }



    /**
     * Show the form for editing a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!adminHasAccessToMenu(60)) {
            return redirect()->route('admin.dashboard');
        }
        $data = array();


        $data['details'] = ContractTemplatMaster::where('id', $id)->first();
        $data['specialities'] = ProfessionalSpecialty::orderBy('id', 'DESC')->get();
        $related_spec = $data['details']->specialities()->get();
        $data['related_spec_ids'] = array();
        foreach($related_spec as $ar){
            array_push($data['related_spec_ids'], $ar->id);
        }

        return view('admin.modules.contract_template.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!adminHasAccessToMenu(60)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            'title'            => 'required',
            'contract_desc'             => 'required'
        ]);
        $insert['title']                = $request->title;
        $insert['contract_desc']          = $request->contract_desc;
        $contract_temp = ContractTemplatMaster::where('id', $id)->first();
        $contract_temp->update($insert);
        $contract_temp->specialities()->sync($request->speciality_ids);

        session()->flash('success', 'Contract template edited successfully.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     *  @param  int  $id
     *  @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!adminHasAccessToMenu(60)) {
            return redirect()->route('admin.dashboard');
        }
        $data = UserToTools::where('tool_id', $id)->where('tool_type', 'Contract Template')->first();
        if (@$data) {
            //echo "not delete";
            session()->flash('error', 'Contract template not deleted because it is assigned user.');
            return redirect()->back();
        } else {
            //echo "delete";
            ContractTemplatMaster::where('id', $id)->delete();
            session()->flash('success', 'Form deleted successfully.');
            return redirect()->back();
        }
    }


    /*
    * Method: tools for professional
    * Description: shown in professional of form tools
    * Author: munmun
    */
    public function contractToolShowProff($id)
    {

        $data = ContractTemplatMaster::find($id);
        //pr1($data);
        //die();

        if (@$data->shown_in_proff == 'Y') {
            // update status field to active
            ContractTemplatMaster::where('id', $id)->update(['shown_in_proff' => 'N']);
            session()->flash('success', 'Now Imported tools is not showned in professional.');
            return redirect()->back();
        } else if (@$data->shown_in_proff == 'N') {
            // update status field to active
            ContractTemplatMaster::where('id', $id)->update(['shown_in_proff' => 'Y']);
            session()->flash('success', 'Now Imported is showned in professional.');
            return redirect()->back();
        }
    }
}
