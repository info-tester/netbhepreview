<?php

namespace App\Http\Controllers\Admin\Modules\Testimonial;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Content;
use App\Models\Testimonial;

class TestimonialController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }

    public function view(Request $request)
    {
        if (!adminHasAccessToMenu(39)) {
            return redirect()->route('admin.dashboard');
        }
        if ($request->all()) {
            $request->validate([
                'title' => 'required',
                'description' => 'required',
            ]);
            Content::where('id', 6)->update([
                "title" => $request->title,
                "description" => $request->description
            ]);
            session()->flash("success", "Successfully updated the contents.");
            return redirect()->back();
        }
        $testi = Testimonial::get();
        $testiContent = Content::where("id", 6)->first();
        return view('admin.modules.testimonial.testimonial')->with(compact('testi', 'testiContent'));
    }

    public function add(Request $request)
    {
        if (!adminHasAccessToMenu(39)) {
            return redirect()->route('admin.dashboard');
        }
        if ($request->all()) {
            $request->validate([
                'name'        =>    'required',
                'image'        =>    'required',
                'location'    =>    'required',
                'comment'    =>    'required'
            ]);

            if ($request->image) {
                $size = getimagesize($request->image);
                if ($size[0] != 87 || $size[1] != 87) {
                    session()->flash('error', 'Image size must be 87 X 87.');
                    return redirect()->back();
                }
            }

            $is_created = Testimonial::create([
                'name'        =>    $request->name,
                'location'    =>    $request->location,
                'comment'    =>    $request->comment
            ]);

            if ($is_created) {
                $img = $request->image;
                $name = str_random(5) . time() . $is_created->id . '.' . $img->getClientOriginalExtension();
                $img->move("storage/app/public/testimonial/", $name);
                $cat_update = [
                    "image" =>  $name
                ];
                Testimonial::where("id", $is_created->id)->update($cat_update);
                session()->flash("success", "Testimonial successfully created");
                return redirect()->back();
            } else {
                session()->flash("error", "Something went be wrong");
                return redirect()->back();
            }
        }
        return view('admin.modules.testimonial.add_testimonial');
    }


    public function edit($id, Request $request)
    {
        if (!adminHasAccessToMenu(39)) {
            return redirect()->route('admin.dashboard');
        }
        $testi = Testimonial::where('id', $id)->first();
        if ($testi == null) {
            session()->flash('error', 'Unauthorize access');
            return redirect()->back();
        }

        if ($request->all()) {
            $request->validate([
                'name'        =>    'required',
                'location'    =>    'required',
                'comment'    =>    'required'
            ]);

            if ($request->image) {
                $size = getimagesize($request->image);
                if ($size[0] != 87 || $size[1] != 87) {
                    session()->flash('error', 'Image size must be 87 X 87.');
                    return redirect()->back();
                }
            }

            $is_updated = Testimonial::where('id', $id)->update([
                'name'        =>    $request->name,
                'location'    =>    $request->location,
                'comment'    =>    $request->comment
            ]);

            if ($request->image) {
                $img = $request->image;
                $name = str_random(5) . time() . $id . '.' . $img->getClientOriginalExtension();

                if (file_exists("storage/app/public/testimonial/" . @$testi->image)) {
                    unlink("storage/app/public/testimonial/" . @$testi->image);
                }

                $img->move("storage/app/public/testimonial/", $name);
                $cat_update = [
                    "image" =>  $name
                ];
                Testimonial::where("id", $id)->update($cat_update);
            }
            session()->flash("success", "Testimonial successfully edited.");
            return redirect()->back();
        }
        return view('admin.modules.testimonial.edit_testimonial')->with([
            'testi'    =>    @$testi
        ]);
    }

    public function delete($id)
    {
        if (!adminHasAccessToMenu(39)) {
            return redirect()->route('admin.dashboard');
        }
        $testi = Testimonial::where('id', @$id)->first();
        if ($testi == null) {
            session()->flash('error', 'Unauthorize access.');
            return redirect()->back();
        }
        if (file_exists("storage/app/public/testimonial/" . @$testi->image)) {
            unlink("storage/app/public/testimonial/" . @$testi->image);
        }
        $testi = Testimonial::where('id', $id)->delete();
        session()->flash('success', 'Testimonial successfully removed.');
        return redirect()->back();
    }
}
