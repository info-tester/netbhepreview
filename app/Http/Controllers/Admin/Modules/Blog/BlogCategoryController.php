<?php

namespace App\Http\Controllers\Admin\Modules\Blog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BlogCategory;
use App\Models\Blog;
use Session;
use Validator;

class BlogCategoryController extends Controller
{
    protected $redirectTo = '/admin/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }

    /*
    * Method: index
    * Description: Blog category listing
    * Author: Abhisek
    */
    public function index(Request $request)
    {
        if (!adminHasAccessToMenu(36)) {
            return redirect()->route('admin.dashboard');
        }

        $category = BlogCategory::whereIn('status', ['A', 'I']);

        if (@$request->all()) {
            if (@$request->keyword) {
                $category = $category->where('name', 'like', '%' . $request->keyword . '%');
            }
            if (@$request->status) {
                $category = $category->where('status', $request->status);
            }
            $key = $request->all();
        }
        $category = $category->orderBy('id', 'desc')
            ->get();
        return view('admin.modules.blogs.blog_category')->with([
            'category'     => @$category,
            'key'         => @$key,
        ]);
    }

    /*
    * Method: create
    * Description: blog category add form
    * Author: Abhisek
    */
    public function create(Request $request)
    {
        if (!adminHasAccessToMenu(36)) {
            return redirect()->route('admin.dashboard');
        }
        return view('admin.modules.blogs.add_blog_category');
    }

    /*
    * Method: store
    * Description: Insert to blog category table
    * Author: Abhisek
    */
    public function store(Request $request)
    {

        if (!adminHasAccessToMenu(36)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            'name'              => 'required|unique:blog_categories',
            'meta_title'        => 'required',
            'meta_keyword'      => 'required',
            'meta_description'  => 'required'
        ]);

        // check slug
        $slug = str_slug($request->name);
        $check_slug = BlogCategory::where('slug', $slug)->first();
        if (@$check_slug == null) {
            $new['slug'] = $slug;
        } else {
            $new['slug'] = $slug . '-' . $check_slug->id;
        }

        // store
        $new['name']                = $request->name;
        $new['meta_title']          = $request->meta_title;
        $new['meta_keyword']        = $request->meta_keyword;
        $new['meta_description']    = $request->meta_description;
        BlogCategory::create($new);

        session()->flash('success', 'Category inserted successfully.');
        return redirect()->back();
    }

    /*
    * Method: edit
    * Description: edit blog category form page
    * Author: Abhisek
    */
    public function edit($id)
    {
        if (!adminHasAccessToMenu(36)) {
            return redirect()->route('admin.dashboard');
        }
        $category = BlogCategory::find($id);

        if (@$category == null) {
            session()->flash('error', 'Unauthorize access.');
            return redirect()->back();
        }

        return view('admin.modules.blogs.edit_blog_category')->with([
            'category'     => @$category
        ]);
    }

    /*
    * Method: update
    * Description: Update blog
    * Author: Abhisek
    */
    public function update(Request $request)
    {
        if (!adminHasAccessToMenu(36)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            'name'              => 'required|unique:blog_categories,name,' . $request->id,
            'meta_title'        => 'required',
            'meta_keyword'      => 'required',
            'meta_description'  => 'required'
        ]);

        $category = BlogCategory::find($request->id);

        if (@$category == null) {
            session()->flash('error', 'Unauthorize access.');
            return redirect()->back();
        }

        // check slug
        $slug = str_slug($request->name);

        $check_slug = BlogCategory::where('slug', $slug)
            ->whereNotIn('id', [$request->id])
            ->first();

        if (@$check_slug == null) {
            $update['slug'] = $slug;
        } else {
            $update['slug'] = $slug . '-' . $check_slug->id;
        }

        // store
        $update['name']                 = $request->name;
        $update['meta_title']           = $request->meta_title;
        $update['meta_keyword']         = $request->meta_keyword;
        $update['meta_description']     = $request->meta_description;
        BlogCategory::where('id', $request->id)->update($update);

        session()->flash('success', 'Category updated successfully.');
        return redirect()->back();
    }

    /*
    * Method: status
    * Description: status change of blogs
    * Author: Abhisek
    */
    public function status($id)
    {
        if (!adminHasAccessToMenu(36)) {
            return redirect()->route('admin.dashboard');
        }
        $blog = BlogCategory::find($id);

        if (@$blog == null) {
            session()->flash('error', 'Unauthorize access.');
            return redirect()->back();
        }

        if (@$blog->status == 'A') {
            BlogCategory::where('id', $blog->id)->update(['status' => 'I']);
            session()->flash('success', 'Category status updated successfully.');
            return redirect()->back();
        }

        if (@$blog->status == 'I') {
            BlogCategory::where('id', $blog->id)->update(['status' => 'A']);
            session()->flash('success', 'Category status updated successfully.');
            return redirect()->back();
        }
    }

    /*
    * Method: destroy
    * Description: destroy from blogs table
    * Author: Abhisek
    */
    public function destroy($id)
    {
        if (!adminHasAccessToMenu(36)) {
            return redirect()->route('admin.dashboard');
        }
        $blog = BlogCategory::find($id);

        if (@$blog == null) {
            session()->flash('error', 'Unauthorize access.');
            return redirect()->back();
        }

        $used_cat_id = Blog::where('cat_id', $id)->first();
        if (@$used_cat_id != null) {
            session()->flash('error', 'You can not delete this category, because this category is associated with a blog post.');
            return redirect()->back();
        } else {
            BlogCategory::destroy($blog->id);
            session()->flash('success', 'Blog category successfully removed.');
            return redirect()->back();
        }
    }
}
