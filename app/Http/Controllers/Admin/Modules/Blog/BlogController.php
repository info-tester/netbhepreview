<?php

namespace App\Http\Controllers\Admin\Modules\Blog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\BlogCategory;
use App\Models\BlogToContact;
// use Session;
use Validator;
use Auth;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;

class BlogController extends Controller
{
    protected $redirectTo = '/admin/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }

    /*
    * Method: index
    * Description: Blog listing with search
    * Author: Abhishek
    */
    public function index(Request $request)
    {
        if (!adminHasAccessToMenu(37)) {
            return redirect()->route('admin.dashboard');
        }
        $blogs = Blog::with('blogCategoryName', 'postedBy','adminPostedBy');

        if (@$request->all()) {
            if (@$request->keyword) {
                $blogs = $blogs->where('title', 'like', '%' . $request->keyword . '%');
            }

            if (@$request->cat_id) {
                $blogs = $blogs->whereHas('blogCategoryName', function ($query) use ($request) {
                    $query->where('id', $request->cat_id);
                });
            }

            if (@$request->status) {
                $blogs = $blogs->whereIn('status', [$request->status]);
            }
            $key = $request->all();
        }
        // if(Auth::guard('admin')->user()->user_type=='S'){
        //     $blogs = $blogs->where('admin_post_id', Auth::guard('admin')->user()->id);
        // }

        $blogs = $blogs->orderBy('id', 'desc')
            ->get();
        $category = BlogCategory::where('status', 'A')
            ->orderBy('name', 'asc')
            ->get();
        return view('admin.modules.blogs.blogs')->with([
            'category'     => @$category,
            'blogs'     => @$blogs,
            'key'          => @$key,
        ]);
    }

    /*
    * Method: create
    * Description: blog add form
    * Author: Abhishek
    */
    public function create(Request $request)
    {
        if (!adminHasAccessToMenu(37)) {
            return redirect()->route('admin.dashboard');
        }
        $blogs = Blog::orderBy('id', 'desc')
            ->get();
        $category = BlogCategory::where('status', 'A')
            ->orderBy('name', 'asc')
            ->get();
        return view('admin.modules.blogs.add_blogs')->with([
            'category'     => @$category
        ]);
    }

    /*
    * Method: store
    * Description: Insert to blogs table
    * Author: Abhishek
    */
    public function store(Request $request)
    {
        if (!adminHasAccessToMenu(37)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            'image'                 => 'required',
            'cat_id'                 => 'required',
            'post_title'             => 'required',
            'description'             => 'required',
            'meta_title'             => 'required',
            'meta_description'      => 'required',
            'meta_keyword'          => 'required',
        ]);

        // insert youtube link
        if (@$request->you_tube_video != null) {
            // insert youtube link
            $url = urldecode(rawurldecode($request->you_tube_video));
            preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);
            if (count($matches) == 0) {
                return redirect()->back()->with('error', 'Invalid youtube link.');
            }
            $new['you_tube_video_path'] = $matches[0];
            $new['you_tube_video'] = $matches[1];
        }

        // insert company images
        if (@$request->image != null) {
            list($width, $height) = getimagesize($request->image);
            $filename = time() . '_' . $request->image->getClientOriginalName();
            $destinationPath = "storage/app/public/uploads/blog_image/original/";
            $destinationPathResized = "storage/app/public/uploads/blog_image/";
            $request->image->move($destinationPath, $filename);
            $new['image'] = $filename;
            if (file_exists($destinationPath . $filename)) {
                copy($destinationPath . $filename, $destinationPathResized . $filename);
            } else {
                Session::flash('error', 'Image could not be uploaded.');
                return redirect()->back();
            }
            $image = Image::make($destinationPathResized . $filename);
            if ($width / $height >= 16 / 9) {
                $image->resize(($width / $height) * 720, 720);
            } else {
                $image->resize(1280, ($height / $width) * 1280);
            }
            $image->crop(1280, 720);
            $image->save($destinationPathResized . $filename);
        }

        $new['cat_id'] = $request->cat_id;
        $new['title'] = $request->post_title;
        $new['desc'] = $request->description;
        $new['post_date'] = date('Y-m-d');
        $new['admin_post_id'] = Auth::guard('admin')->user()->id;
        $new['posted_by'] = 0;
        $new['meta_title'] = $request->meta_title;
        $new['meta_description'] = $request->meta_description;
        $new['meta_keyword'] = $request->meta_keyword;
        $new['is_show_contact_from'] = @$request->form_show?$request->form_show:'N';
        $blog = Blog::create($new);
        $slug = str_slug($blog->title . '-' . $blog->id);
        $find_blg = Blog::where("slug", $slug)->first();

        if ($find_blg != null) {
            $slug = $slug . uniqid();
        }

        $blog->slug = $slug;
        $blog->save();
        session()->flash('success', 'Blog post added successfully done.');
        return redirect()->back();
    }

    /*
    * Method: edit
    * Description: edit blog form page
    * Author: Abhishek
    */
    public function edit($id)
    {
        if (!adminHasAccessToMenu(37)) {
            return redirect()->route('admin.dashboard');
        }
        $blog = Blog::/*with('blogCategoryName')->*/find($id);
        // if (Auth::guard('admin')->user()->user_type == 'S') {
        //     $blog = Blog::where('admin_post_id', Auth::guard('admin')->user()->id)->find($id);
        // }
        if (@$blog == null) {
            Session::flash('error', 'Unauthorize access');
            return redirect()->back();
        }

        $category = BlogCategory::where('status', 'A')
            ->orderBy('name', 'asc')
            ->get();

        return view('admin.modules.blogs.edit_blogs')->with([
            'category'     => @$category,
            'blog'     => @$blog
        ]);
    }

    /*
    * Method: update
    * Description: Update blog
    * Author: Abhishek
    */
    public function update(Request $request)
    {
        if (!adminHasAccessToMenu(37)) {
            return redirect()->route('admin.dashboard');
        }
        // dd($request->all(),nl2br($request->description));
        $blog = Blog::find($request->id);
        // if (Auth::guard('admin')->user()->user_type == 'S') {
        //     $blog = Blog::where('admin_post_id', Auth::guard('admin')->user()->id)->find($request->id);
        // }
        if (@$blog == null) {
            Session::flash('error', 'Unauthorize access');
            return redirect()->back();
        }

        // validate
        $request->validate([
            'image'             => 'nullable',
            'cat_id'             => 'required',
            'title'             => 'required',
            'description'         => 'required',
            'meta_title'             => 'required',
            'meta_description'      => 'required',
            'meta_keyword'          => 'required',
        ]);

        // insert youtube link
        if (@$request->you_tube_video != null) {
            // insert youtube link
            $url = urldecode(rawurldecode($request->you_tube_video));
            preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);
            // $update['you_tube_video'] = $matches[1];
            if (count($matches) == 0) {
                return redirect()->back()->with('error', 'Invalid youtube link.');
            }
            $update['you_tube_video_path'] = $matches[0];
            $update['you_tube_video'] = $matches[1];
        } else {
            $update['you_tube_video'] = null;
        }

        // insert company images
        if (@$request->image != null) {
            list($width, $height) = getimagesize($request->image);
            $filename = time() . '_' . $request->image->getClientOriginalName();
            $destinationPath = "storage/app/public/uploads/blog_image/original/";
            $destinationPathResized = "storage/app/public/uploads/blog_image/";
            $request->image->move($destinationPath, $filename);
            $update['image'] = $filename;
            if (file_exists($destinationPath . $filename)) {
                copy($destinationPath . $filename, $destinationPathResized . $filename);
            } else {
                Session::flash('error', 'Image could not be uploaded.');
                return redirect()->back();
            }
            $image = Image::make($destinationPathResized . $filename);
            if ($width / $height >= 16 / 9) {
                $image->resize(($width / $height) * 720, 720);
            } else {
                $image->resize(1280, ($height / $width) * 1280);
            }
            $image->crop(1280, 720);
            $image->save($destinationPathResized . $filename);
            if (@$blog->image) {
                if (file_exists($destinationPath . $blog->image)) {
                    unlink($destinationPath . $blog->image);
                }
            }
            if (@$blog->image) {
                if (file_exists($destinationPathResized . $blog->image)) {
                    unlink($destinationPathResized . $blog->image);
                }
            }
        }

        // store
        $update['cat_id']         = $request->cat_id;
        $update['title']         = $request->title;
        $update['status']         = 'I';
        // $update['posted_by'] 	= Auth::guard('admin')->user()->id;
        // $update['posted_by']    = 0;
        $update['desc']         = $request->description;
        // $update['post_date']     = date('Y-m-d');
        $update['meta_title'] = $request->meta_title;
        $update['meta_description'] = $request->meta_description;
        $update['meta_keyword'] = $request->meta_keyword;
        $update['is_show_contact_from'] = @$request->form_show?$request->form_show:'N';
        $updated_blog = Blog::where('id', $request->id)->update($update);

        $slug = str_slug($request->title . '-' . $request->id);
        $find_blg = Blog::where("slug", $slug)->where("id", "!=", $request->id)->first();

        if ($find_blg != null) {
            $slug = $slug . uniqid();
        }
        $update_slg = Blog::where("id", $request->id)->update([
            "slug"  =>  $slug
        ]);

        Session::flash('success', 'Blog post updated successfully.');
        return redirect()->back();
    }

    /*
    * Method: view
    * Description: Details Page
    * Author: Abhishek
    */
    public function view($id)
    {
        if (!adminHasAccessToMenu(37)) {
            return redirect()->route('admin.dashboard');
        }
        $blog = Blog::with('blogCategoryName', 'postedBy', 'adminPostedBy')->find($id);
        // if (Auth::guard('admin')->user()->user_type == 'S') {
        //     $blog = Blog::with('blogCategoryName', 'postedBy', 'adminPostedBy')->where('admin_post_id', Auth::guard('admin')->user()->id)->find($id);
        // }
        if (@$blog == null) {
            Session::flash('error', 'Unauthorize access');
            return redirect()->back();
        }
        return view('admin.modules.blogs.blog_details')->with([
            'blog_details'     => @$blog
        ]);
    }

    /*
    * Method: status
    * Description: status change of blogs
    * Author: Abhishek
    */
    public function status($id)
    {
        if (!adminHasAccessToMenu(37)) {
            return redirect()->route('admin.dashboard');
        }
        $blog = Blog::find($id);
        // if (Auth::guard('admin')->user()->user_type == 'S') {
        //     $blog = Blog::where('admin_post_id', Auth::guard('admin')->user()->id)->find($id);
        // }

        if (@$blog == null) {
            Session::flash('error', 'Unauthorize access.');
            return redirect()->back();
        }

        if (@$blog->status == 'A') {
            Blog::where('id', $blog->id)->update(['status' => 'I']);
            Session::flash('success', 'Blog status change successfully.');
            return redirect()->back();
        }

        if (@$blog->status == 'I') {
            Blog::where('id', $blog->id)->update(['status' => 'A','post_date'=>date('Y-m-d')]);
            Session::flash('success', 'Blog status change successfully.');
            return redirect()->back();
        }
    }

    /*
    * Method: destroy
    * Description: destroy from blogs table
    * Author: Abhishek
    */
    public function destroy($id)
    {
        if (!adminHasAccessToMenu(37)) {
            return redirect()->route('admin.dashboard');
        }
        $blog = Blog::find($id);
        if (Auth::guard('admin')->user()->user_type == 'S') {
            $blog = Blog::where('admin_post_id', Auth::guard('admin')->user()->id)->find($id);
        }
        if (@$blog == null) {
            Session::flash('error', 'Unauthorize access.');
            return redirect()->back();
        }

        // if(@$blog->image) {
        // 	unlink('storage/app/public/uploads/blog_image/'.@$blog->image);
        // }

        Blog::destroy($blog->id);
        Session::flash('success', 'Blog successfully deleted.');
        return redirect()->back();
    }


    /**
     * Method     : imgUpload
     * Description: For insert and edit article image.
     * Author     : Abhisek
     */
    public function imgUpload(Request $request)
    {
        $jsn = [
            "jsonrpc"   =>  "2.0"
        ];
        $doc_name           = $request['file']->getClientOriginalName();
        $explode_doc_name   = explode('.', $doc_name);
        $extension          = end($explode_doc_name);
        $doc_name           = time() . str_random(6) . '.' . $extension;
        $newFilePath        = "storage/app/public/uploads/content_image/";
        $request['file']->move($newFilePath, $doc_name);
        $jsn['status'] = 'SUCCESS';
        $jsn['location'] = url('/') . '/storage/app/public/uploads/content_image/' . $doc_name;
        return response()->json($jsn);
    }

    /*
    * Method: blogContactShow
    * Description: blog Contact from show or not
    * Author: Soumojit
    */
    public function blogContactShow($id)
    {
        if (!adminHasAccessToMenu(37)) {
            return redirect()->route('admin.dashboard');
        }
        $blog = Blog::find($id);

        if (@$blog == null) {
            Session::flash('error', 'Unauthorize access.');
            return redirect()->back();
        }

        if (@$blog->is_show_contact_from == 'Y') {
            Blog::where('id', $blog->id)->update(['is_show_contact_from' => 'N']);
            Session::flash('success', 'Blog contact from show.');
            return redirect()->back();
        }

        if (@$blog->is_show_contact_from == 'N') {
            Blog::where('id', $blog->id)->update(['is_show_contact_from' => 'Y']);
            Session::flash('success', 'Blog contact from not show.');
            return redirect()->back();
        }
    }
    /*
    * Method: blogContactList
    * Description: blog Contact List
    * Author: Soumojit
    */
    public function blogContactList($id)
    {
        if (!adminHasAccessToMenu(37)) {
            return redirect()->route('admin.dashboard');
        }
        $blog = Blog::find($id);

        if (@$blog == null) {
            Session::flash('error', 'Unauthorize access.');
            return redirect()->back();
        }

        $contactList = BlogToContact::where('blog_id',$blog->id)->get();
        $data['blog']=$blog;
        $data['contactList']=$contactList;
        return view('admin.modules.blogs.blog_contact_list')->with($data);
    }
}
