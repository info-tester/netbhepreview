<?php

namespace App\Http\Controllers\Admin\Modules\CompetencesMaster;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\CompetencesMaster;
use App\Models\TypeOfEvaluationMaster;
use App\Models\CompetencesQuestion;
use App\Models\TypeEvaluationToCompetences;
use App\Models\Tools360EvaluationUserAnswer;
use App\Models\ToolsEvaluationCategory;
use App\Models\ProfessionalSpecialty;
use Session;
use Validator;
use Auth;

class CompetencesMasterController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }

    /*
    @method         => manage
    @description    => Form manage
    @author         => munmun
    @date           => 28/02/2020
    */
    public function index(Request $request)
    {
        $data = array();
        $data['title'] = 'Manage Competences Master';


        // $data['form'] = FormMaster::where('status','!=', 'DELETED')->with('category')->with('user')->orderBy('id','DESC')->get();
        $comp = CompetencesMaster::query();
        if ($request->all()) {
            $data['keys'] = $request->all();
            if ($request->category) {
                $comp = $comp->where('evaluation_category_id', $request->category);
            }
        }

        $data['all_competences_master'] = $comp->orderBy('id', 'DESC')->with('evalCategory')->get();
        $data['categories'] = ToolsEvaluationCategory::get();

        return view('admin.modules.competences_master.index')->with($data);
    }

    /*
    @method         => create
    @description    => Form create in form_master table
    @author         => munmun
    @date           => 28/02/2020
    */
    public function create(Request $request)
    {
        $all_evaluation_type = TypeOfEvaluationMaster::select('id', 'evaluation_category_id', 'title')->get();
        $categories = ToolsEvaluationCategory::get();
        $specialities = ProfessionalSpecialty::orderBy('id', 'DESC')->get();

        return view('admin.modules.competences_master.create')->with(compact('all_evaluation_type', 'categories', 'specialities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->all();
        //pr1($data);
        //die();

        $request->validate([
            'title' => 'required',
            'evaluation_category_id' => 'required',
            'evaluation_type_id' => 'required',
        ]);


        $insert['evaluation_category_id'] = $request->evaluation_category_id;
        $insert['title'] = $request->title;
        $insert['description'] = $request->description;
        $latest = CompetencesMaster::create($insert);
        $latest->specialities()->sync($request->speciality_ids);

        // if($request->evaluation_type_id){

        //     $insert2 = array();
        //     $insert2['competences_id']          = $latest->id;
        //     $insert2['evaluation_type_id']      = $request->evaluation_type_id;

        //     TypeEvaluationToCompetences::create($insert2);
        // }

        foreach ($request->evaluation_type_id as $key => $val) {
            $insert2 = array();
            $insert2['competences_id']          = $latest->id;
            $insert2['evaluation_type_id']      = $val;
            TypeEvaluationToCompetences::create($insert2);
        }


        session()->flash('success', 'Competences added successfully.');
        return redirect()->back();
    }


    /*
    @method         => edit
    @description    => Form edit view
    @author         => munmun
    @date           => 28/02/2020
    */
    public function edit($id)
    {
        $data = array();
        $data['details'] = $details = CompetencesMaster::with('toEvalType.evaluationType', 'evalCategory')->find($id);
        $data['specialities'] = ProfessionalSpecialty::orderBy('id', 'DESC')->get();
        $related_spec = $details->specialities()->get();
        $data['related_spec_ids'] = array();
        foreach($related_spec as $ar){
            array_push($data['related_spec_ids'], $ar->id);
        }
        $data['types'] = array_map(function ($item) {
            return $item['evaluation_type']['title'] ?? '';
        }, $details->toEvalType->toArray());
        $data['all_evaluation_type'] = TypeOfEvaluationMaster::get();

        $data['all_question']  = CompetencesQuestion::where('tool360_competences_master_id', $id)->get();

        return view('admin.modules.competences_master.question_list')->with($data);
    }

    /*
    @method         => update
    @description    => Form update in form_master table
    @author         => munmun
    @date           => 28/02/2020
    */
    public function update($id, Request $request)
    {

        $request->validate([
            'title'            => 'required'
        ]);

        $update['title']     = $request->title;
        $update['description']     = $request->description;

        $comp = CompetencesMaster::where('id', $id)->first();
        $comp->update($update);
        $comp->specialities()->sync($request->speciality_ids);

        session()->flash('success', 'Competences edited successfully.');
        return redirect()->back();
    }


    public function show($id)
    {
    }

    // CompetencesQuestion listing
    public function competencesQuestion($id)
    {
        if (!adminHasAccessToMenu(64)) {
            return redirect()->route('admin.dashboard');
        }
        $data = array();
        $data['details'] = CompetencesQuestion::find($id);
        return view('admin.modules.competences_master.edit')->with($data);
    }

    //CompetencesQuestion update
    public function competencesQuestionUpdate($id, Request $request)
    {
        if (!adminHasAccessToMenu(64)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            'question'            => 'required'
        ]);

        $update['question']     = $request->question;

        CompetencesQuestion::where('id', $id)->update($update);

        session()->flash('success', 'Competences question edited successfully.');
        return redirect()->back();
    }

    //CompetencesQuestion add
    public function competencesQuestionAdd($id)
    {
        if (!adminHasAccessToMenu(64)) {
            return redirect()->route('admin.dashboard');
        }
        $data = array();
        $data['details'] = CompetencesMaster::find($id);
        $data['all_question']  = CompetencesQuestion::where('tool360_competences_master_id', $id)->get();
        return view('admin.modules.competences_master.add_question')->with($data);
    }

    public function competencesQuestionPost(Request $request)
    {
        //pr1($id);
        //die();
        $request->validate([
            'question'            => 'required'
        ]);

        $insert['tool360_competences_master_id']     = $request->competences_id;
        $insert['question']     = $request->question;
        $insert['title']     = $request->title;

        CompetencesQuestion::create($insert);

        session()->flash('success', 'Competences question added successfully.');
        return redirect()->back();
    }


    //CompetencesMaster table and all related table delete.
    public function destroy($id)
    {

        if (!adminHasAccessToMenu(64)) {
            return redirect()->route('admin.dashboard');
        }
        CompetencesMaster::where('id', $id)->delete();

        CompetencesQuestion::where('tool360_competences_master_id', $id)->delete();

        TypeEvaluationToCompetences::where('competences_id', $id)->delete();

        Tools360EvaluationUserAnswer::where('tool360_competences_id', $id)->delete();

        session()->flash('success', 'Competences deleted successfully.');
        return redirect()->back();

        // $data = UserToTools::where('tool_id', $id)->where('tool_type', "Log Book")->first();
        // if(@$data){
        //     //echo "not delete";
        //     session()->flash('error', 'logbook not deleted because it is assigned user.');
        //     return redirect()->back();
        // } else {
        //     //echo "delete";
        //     LogBookMaster::where('id', $id)->delete();
        //     session()->flash('success', 'Logbook deleted successfully.');
        //     return redirect()->back();
        // }

    }

    public function competencesQuestionDelete($id)
    {

        CompetencesQuestion::where('id', $id)->delete();
        session()->flash('success', 'Question deleted successfully.');
        return redirect()->back();
    }

    public function fetchEvalCatSpeciality(Request $request)
    {
        $eval_arr = $request->arr;
        $related_spec_ids = array();
        foreach($eval_arr as $id){
            $category = ToolsEvaluationCategory::where('id', $id)->first();
            $related_spec = $category->specialities()->get();
            $related_spec_ids = array();
            foreach($related_spec as $ar){
                if(!in_array($ar->id, $related_spec_ids)){
                    array_push($related_spec_ids, $ar->id);
                }
            }
        }
        return $related_spec_ids;
    }
    
    public function fetchEvalTypeSpeciality(Request $request)
    {
        $eval_arr = $request->arr;
        $related_spec_ids = array();
        foreach($eval_arr as $id){
            $eval_type = TypeOfEvaluationMaster::where('id', $id)->first();
            $related_spec = $eval_type->specialities()->get();
            foreach($related_spec as $ar){
                if(!in_array($ar->id, $related_spec_ids)){
                    array_push($related_spec_ids, $ar->id);
                }
            }
        }
        return $related_spec_ids;
    }

    public function fetchEvalCatTypes(Request $request)
    {
        // return $request->all();
        $id = $request->eval_cat_id;
        $arr = $request->arr;
        $category = ToolsEvaluationCategory::where('id', $id)->first();
        $eval_types = TypeOfEvaluationMaster::where('evaluation_category_id', $id)->whereHas('specialities', function( $query ) use ($arr){
            $query = $query->whereIN('professional_specialty_id', $arr);
        })->get()->toArray();
        return $eval_types;
    }
}
