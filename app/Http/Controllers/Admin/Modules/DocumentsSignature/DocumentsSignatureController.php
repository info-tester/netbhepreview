<?php

namespace App\Http\Controllers\Admin\Modules\DocumentsSignature;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\DocumentSignature;
use Illuminate\Support\Facades\Storage;
use PDF;

class DocumentsSignatureController extends Controller
{
    //
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }
    public function index()
    {
        $getAllDocumentTemplate = DocumentSignature::where('status','!=','D')->get();

        return view('admin.modules.document_signature.index')->with(['template'=>$getAllDocumentTemplate]);
    }
    public function add()
    {
        return view('admin.modules.document_signature.add_document_signature');
    }
    public function addSuccess(Request $request)
    {
        $request->validate([
            "title" =>  "required",
            "content" =>  "required",
        ]);
        $ins=[];
        $ins['admin_id'] = auth()->guard('admin')->user()->id;
        $ins['created_by'] = 'A';
        $ins['title']= $request->title;
        $ins['description'] = $request->content; 
        $ins['status'] = 'A';
        $addDocument = DocumentSignature::create($ins);
        // $pdf= PDF::loadView('pdf.document_pdf', ['data' => $ins['description'] = $request->content]);
        //         $path_storage= auth()->guard('admin')->user()->id.'.pdf';
        //         // @unlink(storage_path('public/pdf/'. $path_storage));
        //         Storage::put('public/pdf/'. $path_storage,$pdf->output());
        if(@$addDocument){
            session()->flash("success", "Document Added");
            return redirect()->route('admin.document.index');
        }
        session()->flash("error", "Document Not Added");
        return redirect()->route('admin.document.add');
    }
    public function edit($id)
    {
        $documentTemplate = DocumentSignature::where('id',$id)->where('status','!=','D')->first();
        if(@$documentTemplate)
        {
            return view('admin.modules.document_signature.add_document_signature')->with(['template' => $documentTemplate]);
        }
        return redirect()->back();
    }
    public function editSuccess($id, Request $request)
    {
        $request->validate([
            "title" =>  "required",
            "content" =>  "required",
        ]);
        $upd = [];
        $upd['title'] = $request->title;
        $upd['description'] = $request->content;
        $updateDocument = DocumentSignature::where('id',$id)->update($upd);
        if (@$updateDocument) {
            session()->flash("success", "Document Updated");
            return redirect()->route('admin.document.index');
        }
        session()->flash("error", "Document Not Updated");
        return redirect()->back();
    }

    public function statusChange($id){
        $documentTemplate = DocumentSignature::where('id',$id)->first();
        if(@$documentTemplate->status=='A'){
            DocumentSignature::where('id', $id)->update(['status'=>'I']);
            session()->flash("success", "Document Inactive");
            return redirect()->back();
        }
        if(@$documentTemplate->status=='I'){
            DocumentSignature::where('id', $id)->update(['status'=>'A']);
            session()->flash("success", "Document Active");
            return redirect()->back();
        } else{
            session()->flash("error", "Document Status Not change");
            return redirect()->back();
        }
    }
    public function delete($id)
    {
        $documentTemplate = DocumentSignature::where('id', $id)->first();
        $deleteDocument = DocumentSignature::where('id', $id)->update(['status' => 'D']);
        if(@$deleteDocument){
            session()->flash("success", "Document Delete");
            return redirect()->back();
        }
        session()->flash("error", "Document Not Delete");
        return redirect()->back();
    }
}