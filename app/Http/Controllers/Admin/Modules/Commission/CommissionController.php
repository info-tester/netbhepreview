<?php

namespace App\Http\Controllers\Admin\Modules\Commission;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductCategory;
use App\Models\Commission;

class CommissionController extends Controller
{
    //
    protected $redirectTo = '/admin/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }

    /*
    * Method: index
    * Description: for view Commission
    * Author: Soumojit
    */
    public function index(Request $request)
    {
        if (!adminHasAccessToMenu(18)) {
            return redirect()->route('admin.dashboard');
        }
        $data['commission'] = Commission::first();
        $data['categories'] = ProductCategory::where('status', '!=', 'D')->get();
        return view('admin.modules.commission.manage_commission')->with($data);
    }
    /*
    * Method: editCommission
    * Description: for edit Commission
    * Author: Soumojit
    */
    public function editCommission(Request $request,$id)
    {
        if (!adminHasAccessToMenu(18)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            "commission" =>  "required",
            "chat_commission" => "required",
            // "affiliate_commission_user" => "required",
            // "affiliate_commission_professional" => "required",
        ]);
        $upd=[
            'commission'=>$request->commission,
            'chat_commission'=>$request->chat_commission,
            // 'affiliate_commission_professional'=>$request->affiliate_commission_professional,
            // 'affiliate_commission_user'=>$request->affiliate_commission_user,
        ];

        $updateCommission = Commission::where('id',$id)->update($upd);
        if (@$updateCommission) {
            session()->flash("success", "Commissions Changed");
            return redirect()->back();
        }
        session()->flash("error", "Commissions Not Changed");
        return redirect()->back();

    }

    /*
    * Method: editCommission
    * Description: for edit Commission
    * Author: Soumojit
    */
    public function categoryCommissionEdit(Request $request,$id){
        if (!adminHasAccessToMenu(18)) {
            return redirect()->route('admin.dashboard');
        }
        $data['category'] = ProductCategory::where('id',$id)->first();
        if(!@$data['category']){
            return redirect()->back()->with('error', 'Not found');
        }
        if(@$request->all()){
            $request->validate([
                "commission" =>  "required",
            ]);
            $updateCommission = ProductCategory::where('id',$id)->update(['commission'=>$request->commission]);
            if (@$updateCommission) {
                session()->flash("success", "Commission Changed");
                return redirect()->back();
            }
        }
        return view('admin.modules.commission.category_commission_edit')->with($data);
    }

}
