<?php

namespace App\Http\Controllers\Admin\Modules\Tool360Evaluation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tool360Master;
use App\Models\Tool360Details;
use App\Models\Tool360AnsweredDetail;
use App\Models\Tool360Answered;
use App\Models\UserToTools;
use Session;
use Validator;
use Auth;

class Tool360EvaluationController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }

    /*
    @method         => manage
    @description    => Form manage
    @author         => munmun
    @date           => 28/02/2020
    */
    public function index()
    {
        if (!adminHasAccessToMenu(58)) {
            return redirect()->route('admin.dashboard');
        }
        $data = array();
        $data['title'] = 'Manage 360Evaluation';
        // $data['form'] = FormMaster::where('status','!=', 'DELETED')->with('category')->with('user')->orderBy('id','DESC')->get();

        $data['all360data'] = Tool360Master::with(['get360Details', 'getUserData'])->where('status', 'ACTIVE')->orderBy('id', 'DESC')->get();

        return view('admin.modules.tool360_evaluation.index')->with($data);
    }

    /*
    @method         => create
    @description    => Form create in form_master table
    @author         => munmun
    @date           => 28/02/2020
    */
    public function create(Request $request)
    {
        if (!adminHasAccessToMenu(58)) {
            return redirect()->route('admin.dashboard');
        }
        $data = array();
        //$data['title'] = 'Add Form';
        //$data['button'] = 'Add';
        //$data['category'] = FormCategory::where('cat_type', 'FORM')->get();
        return view('admin.modules.tool360_evaluation.create');
        //return view('admin.modules.product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if (!adminHasAccessToMenu(58)) {
            return redirect()->route('admin.dashboard');
        }
        
        
        
        $request->validate([
            'title'            => 'required',
            'answer'           => 'required',
            'answer.*'         =>  'required|min:1'
            
        ]);

        $insert['title']                     = $request->title;
        $insert['status']                    = 'ACTIVE';
        $insert['added_by']                  = 'A';
        $insert['added_by_id']               = Auth::id();
        foreach($request->answer as $val){
            if($val == null){
                session()->flash('error', 'Atleast one answer is needed.');
                redirect()->back();
            }
        }
        $last = Tool360Master::create($insert);
        

        if ($request->answer) {

            foreach ($request->answer as $key => $value) {
                Tool360Details::create([
                    'id_360master'       =>  $last->id,
                    'answer'             =>  $value,
                    'color_box'          =>  $request->color_box[$key]
                ]);
            }
        }

        session()->flash('success', 'added successfully.');

        return redirect()->back();
        //return redirect(route('form.question.create',['form_id' => $last->id]));

    }


    /*
    @method         => edit
    @description    => Form edit view
    @author         => munmun
    @date           => 28/02/2020
    */
    public function edit($id)
    {
        if (!adminHasAccessToMenu(58)) {
            return redirect()->route('admin.dashboard');
        }
        $data = array();

        $data['details'] = Tool360Master::with('get360Details')->find($id);
        //pr($data['details']);
        // dd($data['details']);
        //die();
        return view('admin.modules.tool360_evaluation.edit')->with($data);
    }

    /*
    @method         => update
    @description    => Form update in form_master table
    @author         => munmun
    @date           => 28/02/2020
    */
    public function update($id, Request $request)
    {
        if (!adminHasAccessToMenu(58)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            'title'            => 'required'
        ]);

        $update['title']     = $request->title;

        Tool360Master::where('id', $id)->update($update);


        // insert to Tool360Master details
        if (@$request->answer) {
            Tool360Details::where('id_360master', $id)->delete($id);
            foreach ($request->answer as $key => $val) {
                $insert['id_360master']   = $id;
                $insert['answer']         = $val;
                $insert['color_box']         = $request->color_box[$key];
                Tool360Details::create($insert);
            }
            Tool360Details::whereNull('answer')->delete();
        }


        session()->flash('success', 'Form edited successfully.');
        return redirect()->back();
    }


    public function show($id)
    {

        if (!adminHasAccessToMenu(58)) {
            return redirect()->route('admin.dashboard');
        }
        $details = Tool360Master::with('get360Details')->find($id);

        return view('admin.modules.tool360_evaluation.show', [
            'details'       => $details
            //'questions'  => $questions
            //'productImages' => $productImages,

        ]);
    }


    /*
    @method         => delete
    @description    =>  delete table
    @author         => munmun
    @date           => 28/02/2020
    */
    // public function destroy($id)
    // {

    //     $details = Tool360Master::where('id', $id)->first();
    //     if(!$details){
    //         session()->flash('error', 'Form not found.');
    //         return redirect()->back();
    //     }
    //     Tool360Master::where('id', $id)->update(['status'=> 'DELETED']);
    //     session()->flash('success', 'Form deleted successfully.');
    //     return redirect()->back();
    // }

    public function destroy($id)
    {
        if (!adminHasAccessToMenu(58)) {
            return redirect()->route('admin.dashboard');
        }
        $data = UserToTools::where('tool_id', $id)->where('tool_type', '360 evaluation')->first();

        if (@$data) {
            //echo "not delete";
            session()->flash('error', '360 tools not deleted because it is assigned user.');
            return redirect()->back();
        } else {
            //echo "delete";
            Tool360Master::where('id', $id)->delete();
            session()->flash('success', '360 tools template deleted successfully.');
            return redirect()->back();
        }

        // UserContentTemplate::where('id', $id)->delete();
        // session()->flash('success', 'Form deleted successfully.');
        // return redirect()->back();

    }

    //for show in professional
    public function tool360ShowProff($id)
    {

        $data = Tool360Master::find($id);
        //pr1($data);
        //die();

        if (@$data->shown_in_proff == 'Y') {
            // update status field to active
            Tool360Master::where('id', $id)->update(['shown_in_proff' => 'N']);
            session()->flash('success', 'Now Imported tools is not showned in professional.');
            return redirect()->back();
        } else if (@$data->shown_in_proff == 'N') {
            // update status field to active
            Tool360Master::where('id', $id)->update(['shown_in_proff' => 'Y']);
            session()->flash('success', 'Now Imported is showned in professional.');
            return redirect()->back();
        }
    }
}
