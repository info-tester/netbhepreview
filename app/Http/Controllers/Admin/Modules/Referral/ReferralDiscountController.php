<?php

namespace App\Http\Controllers\Admin\Modules\Referral;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ReferralDiscount;

class ReferralDiscountController extends Controller
{
    /**
     * For view refer discount
     * @method: index
     */
    public function index()
    {
        if (!adminHasAccessToMenu(19)) {
            return redirect()->route('admin.dashboard');
        }
        $referDiscount = ReferralDiscount::get();
        return view('admin.modules.refer.discount')->with(['referDiscount'=> $referDiscount]);
    }
    /**
     * For view refer discount
     * @method: edit
     */
    public function edit($id)
    {
        if (!adminHasAccessToMenu(19)) {
            return redirect()->route('admin.dashboard');
        }
        $referDiscount = ReferralDiscount::where('id',$id)->first();
        return view('admin.modules.refer.editdiscount')->with(['referDiscount' => $referDiscount]);
    }
    /**
     * For view refer discount
     * @method: update
     */
    public function update($id,Request $request)
    {
        if (!adminHasAccessToMenu(19)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            "referrer_student_discount" =>  "required",
            "referrer_teacher_discount" =>  "required",
            "referred_student_discount" =>  "required",
            "referred_teacher_discount" =>  "required",
        ]);
        $updateDiscount = ReferralDiscount::where('id', $id)->update([
            'referrer_student_discount' => $request->referrer_student_discount,
            'referrer_teacher_discount' => $request->referrer_teacher_discount,
            'referred_student_discount' => $request->referred_student_discount,
            'referred_teacher_discount' => $request->referred_teacher_discount,
            
        ]);
        if (@$updateDiscount) {
            session()->flash("success", " Updated Discount");
            return redirect()->route('admin.refer.discount');
        }
        session()->flash("error", "Discount Not Updated");
        return redirect()->back();
    }
}
