<?php

namespace App\Http\Controllers\Admin\Modules\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductSubcategory;
use App\Models\ProductCategory;
use App\Models\Product;
use App\User;
use App\Models\ProductOrder;
use App\Models\Chapter;
use App\Models\Lesson;
use App\Models\LessonFile;
use App\Models\Slide;
use App\Models\Quiz;
use App\Models\ProductOrderDetails;



class ProductController extends Controller
{
    protected $redirectTo = '/admin/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }
    /*
    * Method: index
    * Description: for view Product Category List
    * Author: Soumojit
    * Date: 11-DEC-2020
    */
    public function index(Request $request)
    {
        if (!adminHasAccessToMenu(28)) {
            return redirect()->route('admin.dashboard');
        }

        // dd($request->all());
        $categoryList = ProductCategory::where('status', '!=', 'D')->get();
        $sub_cat_list = ProductSubcategory::where('status', '!=', 'D')->get();
        $allProductCategory  = ProductCategory::with('getSubCategories')->where('status', '!=', 'D');

        if(@$request->all()){

            if(@$request->keyword){
                $allProductCategory = $allProductCategory->where('category_name', 'like','%'.$request->keyword.'%')
                ->orWhereHas('getSubCategories',function($q1) use($request){
                            $q1->where('name','like', '%' . $request->keyword . '%')
                            ->where('status','!=','D');

                        });
            }


                if(@$request->category){

                    $allProductCategory = $allProductCategory->where('id',$request->category)
                    ->orWhereHas('getSubCategories',function($q2)use($request){
                            $q2->where('category_id',$request->category);
                        });
                }



            $key=$request->all();
        }

        $allProductCategory = $allProductCategory->get();
        // dd($allProductCategory);
        return view('admin.modules.product.product_category')->with(['productCategoryList' => @$allProductCategory,'key'=>@$key,'category_list'=> @$categoryList,'sub_cat_list'=> @$sub_cat_list]);
    }
    /*
    * Method: addCategoryView
    * Description: for Add Product Category view
    * Author: Soumojit
    * Date: 11-DEC-2020
    */
    public function addCategoryView()
    {
        if (!adminHasAccessToMenu(28)) {
            return redirect()->route('admin.dashboard');
        }
        return view('admin.modules.product.add_product_category');
    }
    /*
    * Method: addCategory
    * Description: for Add Product Category
    * Author: Soumojit
    * Date: 11-DEC-2020
    */
    public function addCategory(Request $request)
    {
        if (!adminHasAccessToMenu(28)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            "product_category_name" =>  "required",
            // "commission" =>  "required",
            "meta_title" =>  "required",
            "meta_description" =>  "required",
        ]);
        $ins = [];
        $ins['category_name']       = $request->product_category_name;
        // $ins['commission']          = $request->commission;
        $ins['meta_title']          = $request->meta_title;
        $ins['meta_description']    = $request->meta_description;
        $ins['status']              = 'A';

        $addProduct = ProductCategory::create($ins);

        if(@$request->image){
            $img = $request->image;
            $name = str_random(5) . time() . $addProduct->id . '.' . $img->getClientOriginalExtension();
            $img->move("storage/app/public/product_category/", $name);
            $addProduct->update(['image' => $name]);
        }
        if (@$addProduct) {
            $slug = str_slug($addProduct->category_name . '-' . $addProduct->id);
            $addProduct->update(['slug' => $slug]);
            return redirect()->back()->with('success', 'Product Category Added Successfully');
        }

        return redirect()->back()->with('error', 'Product Category Not Added');
    }

    /*
    * Method: editCategoryView
    * Description: for edit Product Category view
    * Author: Soumojit
    * Date: 11-DEC-2020
    */
    public function editCategoryView($id)
    {
        if (!adminHasAccessToMenu(28)) {
            return redirect()->route('admin.dashboard');
        }
        $productCategory = ProductCategory::where('id', $id)->where('status', '!=', 'D')->first();
        if (@$productCategory) {
            $subcategories = ProductSubcategory::where('category_id',$id)->where('status', '!=', 'D')->get();
            return view('admin.modules.product.add_product_category')->with(['productCategory' => $productCategory, 'subcategories' => $subcategories]);
        }
        return redirect()->back()->with('error', 'Product Category Not Found');
    }
    /*
    * Method: editProductCategory
    * Description: for edit Product Category
    * Author: Soumojit
    * Date: 11-DEC-2020
    */
    public function editProductCategory(Request $request, $id)
    {
        if (!adminHasAccessToMenu(28)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            "product_category_name" =>  "required",
            // "commission" =>  "required",
            "meta_title" =>  "required",
            "meta_description" =>  "required",
        ]);
        if (@$id) {
            $upd = [];
            $upd['category_name']       = $request->product_category_name;
            // $upd['commission']          = $request->commission;
            $upd['meta_title']          = $request->meta_title;
            $upd['meta_description']    = $request->meta_description;

            $updateProduct = ProductCategory::where('id', $id)->where('status', '!=', 'D')->first();
            $updateProduct->update($upd);

            if(@$request->image){
                $img = $request->image;
                $name = str_random(5) . time() . $updateProduct->id . '.' . $img->getClientOriginalExtension();
                $img->move("storage/app/public/product_category/", $name);
                $updateProduct->update(['image' => $name]);
            }

            if (@$updateProduct) {
                return redirect()->back()->with('success', 'Product Category Updated Successfully');
            }
            return redirect()->route('admin.product.category')->with('error', 'Product Category Not Updated');
        }
        return redirect()->route('admin.product.category')->with('error', 'Product Category Not Found');
    }

    /*
    * Method: storeSubcategoryView
    * Description: for edit Product Subcategory view
    * Author: Sayantani
    * Date: 07-JUNE-2020
    */
    public function storeSubcategoryView($pid, $id=null)
    {
        if (!adminHasAccessToMenu(28)) {
            return redirect()->route('admin.dashboard');
        }
        $productCategory = ProductCategory::where('id', $pid)->first();
        if(@$productCategory){
            if(@$id){
                $productSubcategory = ProductSubcategory::where('id', $id)->where('status', '!=', 'D')->first();
                if (@$productSubcategory) {
                    return view('admin.modules.product.add_product_subcategory')->with(['productCategory' => $productCategory, 'productSubcategory' => $productSubcategory]);
                } else {
                    return redirect()->back()->with('error', 'Product Subcategory Not Found');
                }
            } else {
                return view('admin.modules.product.add_product_subcategory')->with(['productCategory'=>$productCategory]);
            }
        }
    }

    /*
    * Method: storeProductSubcategory
    * Description: for update Product Subcategory
    * Author: Sayantani
    * Date: 07-JUNE-2020
    */
    public function storeProductSubcategory(Request $request)
    {
        if (!adminHasAccessToMenu(28)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            "category_id"      =>  "required",
            "subcategory_id"   =>  "nullable",
            "subcategory_name" =>  "required",
        ]);
        if (@$request->subcategory_id) {
            $subcategory = ProductSubcategory::where('id', $request->subcategory_id)->where('status', '!=', 'D')->first();
            if(@$subcategory){
                $upd = [];
                $upd['name']       = $request->subcategory_name;
                $subcategory->update($upd);
                return redirect()->back()->with('success', 'Product Subcategory Updated Successfully');
            } else {
                return redirect()->route('admin.product.category')->with('error', 'Product Subcategory Not Found');
            }
        } else {
            $ins = [];
            $ins['category_id']       = $request->category_id;
            $ins['name']       = $request->subcategory_name;
            $subcategory = ProductSubcategory::create($ins);
            $slug = str_slug($subcategory).'-'.$subcategory->id;
            $subcategory->update(['slug' => $slug]);
            return redirect()->back()->with('success', 'Product Subcategory Added Successfully');
        }
    }

    /*
    * Method: deleteProductSubcategory
    * Description: for delete Product Category
    * Author: Sayantani
    * Date: 07-JUNE-2020
    */
    public function deleteProductSubcategory($id)
    {
        $subcategory = ProductSubcategory::where('id', $id)->where('status', '!=', 'D')->first();
        $category = ProductCategory::where('id',$subcategory->category_id)->where('status','!=','D')->first();
        $is_in_use = Product::where('category_id',$category->id)->where('status','!=','D')->count();

        if($is_in_use > 0){
             return redirect()->back()->with('error', 'Product is in use');
        }
        if(@$subcategory){
            $subcategory->delete();
            return redirect()->back()->with('success', 'Product Subcategory Deleted Successfully');
        } else {
            return redirect()->back()->with('error', 'Product Subcategory Not Found');
        }
    }

    /*
    * Method: statusChangeProductCategory
    * Description: for change Product Category status
    * Author: Soumojit
    * Date: 11-DEC-2020
    */
    public function statusChangeProductCategory($id)
    {
        if (!adminHasAccessToMenu(28)) {
            return redirect()->route('admin.dashboard');
        }
        $productCategory = ProductCategory::where('id', $id)->where('status', '!=', 'D')->first();
        if (@$productCategory) {
            if ($productCategory->status == 'A') {
                $updateProduct = ProductCategory::where('id', $id)->update(['status' => 'I']);
                if (@$updateProduct) {
                    return redirect()->back()->with('success', 'Product Inactivated Successfully');
                }
                return redirect()->back()->with('error', 'Product Not Inactivated Successfully');
            } elseif ($productCategory->status == 'I') {
                $updateProduct = ProductCategory::where('id', $id)->update(['status' => 'A']);
                if (@$updateProduct) {
                    return redirect()->back()->with('success', 'Product Activated Successfully');
                }
                return redirect()->back()->with('error', 'Product Not Activated Successfully');
            }
        }
        return redirect()->back()->with('error', 'Product Not Found');
    }

    /*
    * Method: statusChangeProductSubcategory
    * Description: for change Product Category status
    * Author: Soumojit
    * Date: 11-DEC-2020
    */
    public function statusChangeProductSubcategory($id)
    {
        if (!adminHasAccessToMenu(28)) {
            return redirect()->route('admin.dashboard');
        }
        $productSubcategory = ProductSubcategory::where('id', $id)->where('status', '!=', 'D')->first();
        if (@$productSubcategory) {
            if ($productSubcategory->status == 'A') {
                $productSubcategory->update(['status' => 'I']);
                return redirect()->back()->with('success', 'Product Subcategory Inactivated Successfully');
            } elseif ($productSubcategory->status == 'I') {
                $productSubcategory->update(['status' => 'A']);
                return redirect()->back()->with('success', 'Product Subcategory Activated Successfully');
            }
        }
        return redirect()->back()->with('error', 'Product Subcategory Not Found');
    }

    /*
    * Method: deleteProductCategory
    * Description: for change Product Category status to delete
    * Author: Soumojit
    * Date: 11-DEC-2020
    */
    public function deleteProductCategory($id)
    {
        if (!adminHasAccessToMenu(28)) {
            return redirect()->route('admin.dashboard');
        }
        $productCategory = ProductCategory::where('id', $id)->where('status', '!=', 'D')->first();

        $is_use = Product::where('category_id',$id)->where('status','!=','D')->count();

        $is_use_sub_cat = ProductSubcategory::where('category_id',$id)->where('status','!=','D')->count();

        if(@$is_use_sub_cat > 0){
            return redirect()->back()->with('error', 'Product Sub Category is in use');
        }

        if(@$is_use>0){
            return redirect()->back()->with('error', 'Product Category use in product');
        }

        if (@$productCategory) {
            $updateProduct = ProductCategory::where('id', $id)->update(['status' => 'D']);
            if (@$updateProduct) {
                return redirect()->back()->with('success', 'Product Category Deleted Successfully');
            }
            return redirect()->back()->with('error', 'Product Category Not Deleted');
        }
        return redirect()->back()->with('error', 'Product Category Not Found');
    }

    /*
    * Method: productList
    * Description: for change Product Category status to delete
    * Author: Soumojit
    * Date: 11-DEC-2020
    */
    public function productList(Request $request)
    {
        if (!adminHasAccessToMenu(29)) {
            return redirect()->route('admin.dashboard');
        }
        $allProductCategory = ProductCategory::where('status', '!=', 'D')->get();
        $allProductSubCategory = ProductSubcategory::where('status','!=','D')->get();
        $professional = User::where([
            'is_professional'=>'Y',
            'is_approved'=>'Y',
            'status'=>'A'
            ])->get();
        $products = Product::with('category','category.getSubCategories')->where('status', '!=', 'D')->orderBy('created_at', 'DESC');
        if(@$request->all()){
            if(@$request->professional){
                $products= $products->where('professional_id',$request->professional);
            }
            if(@$request->keyword){
                $products= $products->where('title', 'like','%'.$request->keyword.'%');
            }
            if(@$request->category){
                $products= $products->where('category_id',$request->category);
            }
            if(@$request->status){
                // $products= $products->where('status',$request->status);
                $products= $products->where('admin_status',$request->status);
            }
            if(@$request->product_sub_cat){
                $products = $products->WhereHas('category.getSubCategories',function($q2)use($request){
                            $q2->where('id',$request->product_sub_cat);
                        });
            }
            $key=$request->all();

        }
        $products = $products->get();
        return view('admin.modules.product.product_list')->with(['productCategoryList' => @$allProductCategory, 'professional' => $professional, 'products' => $products,'key'=>@$key,'product_sub_category'=>$allProductSubCategory]);
    }

    /*
    * Method: statusChangeProduct
    * Description: for change Product status
    * Author: Sayantani
    * Date: 12-DEC-2020
    */
    public function statusChangeProduct($id)
    {
        if (!adminHasAccessToMenu(29)) {
            return redirect()->route('admin.dashboard');
        }
        $product = Product::where('id', $id)->where('status', '!=', 'D')->first();
        if (@$product) {
            if ($product->admin_status == 'A') {
                Product::where('id', $id)->update(['admin_status' => 'I']);
                return redirect()->back()->with('success', 'Product Inactivated Successfully');
            } elseif ($product->admin_status == 'I') {
                Product::where('id', $id)->update(['admin_status' => 'A']);
                return redirect()->back()->with('success', 'Product Activated Successfully');
            } elseif ($product->admin_status == 'AA') {
                Product::where('id', $id)->update(['admin_status' => 'A']);
                return redirect()->back()->with('success', 'Product Approved Successfully');
            }
        }
        return redirect()->back()->with('error', 'Product Not Found');
    }

    /*
    * Method: showProductInHomePage
    * Description: for show/hide Product in home page
    * Author: Sayantani
    * Date: 12-DEC-2020
    */
    public function showProductInHomePage($id)
    {
        if (!adminHasAccessToMenu(29)) {
            return redirect()->route('admin.dashboard');
        }
        $product = Product::where('id', $id)->where('status', '!=', 'D')->first();
        $productCount = Product::where('show_in_home_page', 'Y')->get();
        if (@$product) {
            if ($product->show_in_home_page == 'Y') {
                Product::where('id', $id)->update(['show_in_home_page' => 'N']);
                return redirect()->back()->with('success', 'Product Not Shown in Home Page');
            } elseif ($product->show_in_home_page == 'N') {
                Product::where('id', $id)->update(['show_in_home_page' => 'Y']);
                return redirect()->back()->with('success', 'Product Shown In Home Page Successfully');
            }
        }
        return redirect()->back()->with('error', 'Product Not Found');
    }

    /*
    * Method: deleteProduct
    * Description: for change Product status to delete
    * Author: Sayantani
    * Date: 12-DEC-2020
    */
    public function deleteProduct($id)
    {
        if (!adminHasAccessToMenu(29)) {
            return redirect()->route('admin.dashboard');
        }
        $product = Product::where('id', $id)->where('status', '!=', 'D')->where('status', '!=', 'D')->first();
        if (@$product) {
            Product::where('id', $id)->update(['status' => 'D']);
            return redirect()->back()->with('success', 'Product Deleted Successfully');
        }
        return redirect()->back()->with('error', 'Product Not Found');
    }
    /*
    * Method: viewProduct
    * Description: for change Product status to delete
    * Author: Soumojit
    * Date: 14-DEC-2020
    */
    public function viewProduct($id)
    {
        if (!adminHasAccessToMenu(29)) {
            return redirect()->route('admin.dashboard');
        }
        $product = Product::where('id', $id)->with('category','subCategory')->where('status', '!=', 'D')->where('admin_status', '!=', 'D')->first();
        $professional = User::where([
            'is_professional' => 'Y',
            'is_approved' => 'Y',
            'status' => 'A'
        ])->get();
        if (@$product) {
            return view('admin.modules.product.product_details')->with(['product'=>$product, 'professional' => $professional]);
        }
        return redirect()->back()->with('error', 'Product Not Found');
    }
    /*
    * Method: OrderProductList
    * Description: for change Product status to delete
    * Author: Soumojit
    * Date: 14-DEC-2020
    */
    public function OrderProductList(Request $request)
    {
        if (!adminHasAccessToMenu(30)) {
            return redirect()->route('admin.dashboard');
        }
        $professional = User::where([
            'is_professional' => 'Y',
            'is_approved' => 'Y',
            'status' => 'A'
        ])->get();
        $allOrder = ProductOrder::whereIn('payment_status',['P','F','PR','I']);
        if($request->all()){
            if(@$request->token_no){
                $allOrder = $allOrder->where('token_no', $request->token_no);
            }
            if(@$request->professional){
                $allOrder = $allOrder->where('professional_id', @$request->professional);
            }
            if(@$request->user_name){
                $allOrder = $allOrder->whereHas("userDetails", function ($query) use ($request) {
                    $query->where("name", 'LIKE', '%' . $request->user_name . '%');
                });
            }
            if(@$request->from_date && @$request->to_date){

                $allOrder = $allOrder->whereBetween('order_date',[ date('Y-m-d H:i:s', strtotime(@$request->from_date)), date('Y-m-d H:i:s', strtotime(@$request->to_date))]);
            }
            if(@$request->from_date && @$request->to_date==null){
                $allOrder = $allOrder->whereBetween('order_date',[date('Y-m-d H:i:s', strtotime(@$request->from_date)), now()]);
            }
            $key =$request->all();
        }
        $allOrder= $allOrder->orderBy('id','desc')->get();
        return view('admin.modules.product.product_order_list')->with(['professional' => $professional,'orderList'=> $allOrder,'key'=>@$key]);
    }
    /*
    * Method: viewOrderDetails
    * Description: for change Product status to delete
    * Author: Soumojit
    * Date: 14-DEC-2020
    */
    public function viewOrderDetails($token)
    {
        if (!adminHasAccessToMenu(30)) {
            return redirect()->route('admin.dashboard');
        }

        $orderProduct= ProductOrder::with('orderDetails','orderDetails.product','PaymentDetails.getpaymentDetails.orderDetails','orderDetails.paymentProductDetails')->where('token_no',$token)->whereIn('payment_status', ['P', 'F', 'PR','I'])->first();
        // return $orderProduct;

        return view('admin.modules.product.product_order_view')->with(['orderProduct'=> $orderProduct]);
    }

    /*
    * Method: viewChapter
    * Description: Chapter details of product
    * Author: Puja
    * Date: 07-JUN-2021
    */

    public function viewChapter($id){

        if (!adminHasAccessToMenu(29)) {
            return redirect()->route('admin.dashboard');
        }

        $product_detail = Product::where('id',$id)->first();

        $product = Chapter::with('getLessons')->where('course_id',$id)->get();


        $professional = User::where([
            'is_professional' => 'Y',
            'is_approved' => 'Y',
            'status' => 'A'
        ])->where('id',@$product_detail->professional_id)->first();

        if (@$product) {
            return view('admin.modules.product.chapter_list')->with(['product'=>$product, 'professional' => $professional,'product_detail' => $product_detail]);
        }
        return redirect()->back()->with('error', 'Product Not Found');
    }

    /*
    * Method: viewLesson
    * Description: Lesson details of product
    * Author: Puja
    * Date: 07-JUN-2021
    */

    public function viewLesson($id){

        if (!adminHasAccessToMenu(29)) {
            return redirect()->route('admin.dashboard');
        }

        $product = Lesson::where('chapter_id',$id)->get();
        $chapter = Chapter::where('id',$id)->first();
        $product_detail = Product::where('id',$chapter->course_id)->first();
        $professional = User::where([
            'is_professional' => 'Y',
            'is_approved' => 'Y',
            'status' => 'A'
        ])->where('id',@$product_detail->professional_id)->first();


        if (@$product) {
            return view('admin.modules.product.lesson_list')->with(['product'=>$product, 'professional' => $professional,'product_detail'=>$product_detail]);
        }
        return redirect()->back()->with('error', 'Product Not Found');


    }

    /*
    * Method: viewLessonDetails
    * Description: Details of a lesson
    * Author: Puja
    * Date: 07-JUN-2021
    */

    public function viewLessonDetails($id){

        if (!adminHasAccessToMenu(29)) {
            return redirect()->route('admin.dashboard');
        }

        $product = Lesson::where('id',$id)->first();

        $chapter = Chapter::where('id',@$product->chapter_id)->first();

        $product_detail = Product::where('id',@$chapter->course_id)->first();

        $lesson_files = LessonFile::where('lesson_id',@$id)->get();

        if(@$product->lesson_type == 'Q')
        $question = Quiz::where('lesson_id',$id)->get();

        if(@$product->lesson_type == 'P')
        $slides = Slide::where('lesson_id',$id)->get();



        $professional = User::where([
            'is_professional' => 'Y',
            'is_approved' => 'Y',
            'status' => 'A'
        ])->where('id',@$product_detail->professional_id)->first();


        if (@$product) {
            return view('admin.modules.product.lesson_details')->with(['product'=>$product,'professional' => $professional,'lesson_files' => $lesson_files,'question' => @$question, 'slides' => @$slides,'product_detail'=>$product_detail,'chapter'=>$chapter]);
        }
        return redirect()->back()->with('error', 'Product Not Found');
    }

    public function viewLessonFileDetails($id){

        if (!adminHasAccessToMenu(29)) {
            return redirect()->route('admin.dashboard');
        }
        $product = LessonFile::where('id',$id)->first();

        if (@$product) {
            return view('admin.modules.product.lesson_file_details')->with(['product'=>$product]);
        }
        return redirect()->back()->with('error', 'Product Not Found');

    }

    public function viewLessonSlideDetails($id){

        if (!adminHasAccessToMenu(29)) {
            return redirect()->route('admin.dashboard');
        }

        $product = Slide::where('id',$id)->first();


        if (@$product) {
            return view('admin.modules.product.lesson_slide_details')->with(['product'=>$product]);
        }
        return redirect()->back()->with('error', 'Product Not Found');

    }

    public function viewProductCategory($id){

        if (!adminHasAccessToMenu(30)) {
            return redirect()->route('admin.dashboard');
        }

        $product = ProductCategory::where('id',$id)->first();
        $sub_categories = ProductSubcategory::where('category_id',$id)->get();

         if (@$sub_categories && @$product) {
            return view('admin.modules.product.view_subcategories')->with(['product'=>$sub_categories,'category'=>$product]);
        }
        return redirect()->back()->with('error', 'Product Not Found');

    }

    public function productCategory($request){
        dd($request);
    }

    public function viewLessonQA($id){
    $data['answers'] = LessonQuestion::with('getAnswers')->where('id',$id)->first();

       return view('admin.modules.product.lesson_qa_details')->with($data);
   }


   /*
    * Method: OrderProductList
    * Description: for change Product status to delete
    * Author: Soumojit
    * Date: 14-DEC-2020
    */
    public function OrderProductCancelList(Request $request)
    {
        if (!adminHasAccessToMenu(30)) {
            return redirect()->route('admin.dashboard');
        }
        $data['allOrderList'] = ProductOrderDetails::with(['profDetails','product','orderMaster.userDetails']);
        if(@$request->all()){
            if(@$request->token_no){
                $data['allOrderList']=$data['allOrderList']->whereHas('orderMaster',function($q) use ($request){
                   $q->where('token_no',$request->token_no);
                });
            }

            // if(@$request->professional_name){
            //     $data['allOrderList']=$data['allOrderList']->where('orderMaster',function($q) use ($request){
            //        $q->where('token_no',$request->token_no);
            //     });
            // }
            // if(@$request->professional_name){
            //     $data['allOrderList']=$data['allOrderList']->where('orderMaster',function($q) use ($request){
            //        $q->where('token_no',$request->token_no);
            //     });
            // }
            $data['key']=$request->all();
        }
        $data['allOrderList']=$data['allOrderList']
        ->where(function($qu){
            $qu->where('order_status','C')->orWhere('cancel_request','Y')->orWhere('is_cancel_responce','R');
        })
        ->orderby('id','desc')->get();
        // return $data['allOrderList'];
        return view('admin.modules.product.cancel_product_order')->with($data);
    }

    /*
    * Method: approveProduct
    * Description: for change Product status
    * Author: Sayantani
    * Date: 12-DEC-2020
    */
    public function approveProduct(Request $request)
    {
        if (!adminHasAccessToMenu(29)) {
            return redirect()->route('admin.dashboard');
        }
        $product = Product::where('id', $request->product_id)->where('status', '!=', 'D')->first();
        if (@$product) {
            if ($product->admin_status == 'AA') {
                $upd['admin_description'] = $request->admin_description;
                $upd['admin_status'] = 'A';
                $product->update($upd);
                return redirect()->back()->with('success', 'Product Approved Successfully');
            } else {
                return redirect()->back()->with('error', 'Something went wrong');
            }
        }
        return redirect()->back()->with('error', 'Product Not Found');
    }
}
