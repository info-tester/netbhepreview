<?php

namespace App\Http\Controllers\Admin\Modules\Affiliate;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\BlogCategory;
// use Session;
use Validator;
use Auth;
use App\User;
use App\Models\ReviewProduct;
use App\Models\Review;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use App\Models\Product;
use App\Models\AffiliateProducts;
use App\Models\Commission;
use App\Models\ProductOrderDetails;
use App\Models\VideoAffEarning;
use App\Models\VideoAffPayment;
use Illuminate\Support\Facades\Storage;

class AffiliateController extends Controller
{
    protected $redirectTo = '/admin/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }



    public function manageAffiliate(Request $request){
        if (!adminHasAccessToMenu(7)) {
            return redirect()->route('admin.dashboard');
        }
        $users = User::where('is_join_affiliate', 'Y')->get();
        $data['user_list'] = $users;
        $data['users'] = User::where('is_join_affiliate', 'Y');

        if(@$request->all()){
            if(@$request->professional){
                // $data['users'] = $data['users']->where('name', 'like', '%' . $request->professional . '%')->orWhere('nick_name', 'like', '%' . $request->professional . '%');
                $data['users'] = $data['users']->where('id', $request->professional);
            }
            if($request->tot_earning == "0"){
                $uidArr = [];
                foreach($users as $i=>$user){
                    $user->affiliated_products = AffiliateProducts::where('user_id', $user->id)->count();
                    $orders = ProductOrderDetails::where('affiliate_id', $user->id)->get();
                    $user->total_earning = $orders->sum('affiliate_commission');
                    if($user->total_earning == 0) array_push($uidArr, $user->id);
                }
                $data['users'] = $data['users']->whereIn('id',$uidArr);
            }
            if(@$request->tot_earning){
                $uidArr = [];
                foreach($users as $i=>$user){
                    $user->affiliated_products = AffiliateProducts::where('user_id', $user->id)->count();
                    $orders = ProductOrderDetails::where('affiliate_id', $user->id)->get();
                    $user->total_earning = $orders->sum('affiliate_commission');
                    if((int)$user->total_earning <= (int)@$request->tot_earning) array_push($uidArr, $user->id);
                }
                $data['users'] = $data['users']->whereIn('id',$uidArr);
            }
            $data['key'] = $request->all();
        }
        $data['users'] = $data['users']->get();
        foreach($data['users'] as $i=>$user){
            $user->affiliated_products = AffiliateProducts::where('user_id', $user->id)->count();
            $total = ProductOrderDetails::where('affiliate_id', $user->id)->get();
            $due = ProductOrderDetails::with(['paymentProductDetails'])->whereHas('paymentProductDetails',function($a){
                $a->where('affiliate_balance_status','R');
            })->where('affiliate_id', $user->id)->get();
            $pay= ProductOrderDetails::with(['paymentProductDetails'])->whereHas('paymentProductDetails',function($a){
                $a->where('affiliate_balance_status','W');
            })->where('affiliate_id', $user->id)->get();
            $user->total_earning = $total->sum('affiliate_commission');
            $user->due_earning = $due->sum('affiliate_commission');
            $user->paid_earning = $pay->sum('affiliate_commission');
        }

        return view('admin.modules.affiliate.manage_affiliate')->with($data);
    }

    public function viewAffiliatedProducts($id){
        if (!adminHasAccessToMenu(7)) {
            return redirect()->route('admin.dashboard');
        }
        $data['products'] = AffiliateProducts::with('product','product.category','product.professional')->where('user_id',$id)->get();
        $data['user'] = User::find($id);
        return view('admin.modules.affiliate.affiliate_product_list')->with($data);
    }

    public function viewAffiliatedProductDetails($id){
        if (!adminHasAccessToMenu(7)) {
            return redirect()->route('admin.dashboard');
        }
        $data['affiliation'] = AffiliateProducts::where('id',$id)->first();
        if(@$data['affiliation']){
            $data['product'] = $data['affiliation']->product;
            $data['user'] = User::where('id', $data['affiliation']->user_id)->first();
            return view('admin.modules.affiliate.affiliate_product_details')->with($data);
        } else {
            return redirect()->back()->with('error', 'Affiliation Details Not Found');
        }
    }
    public function blockUserAffiliation($id){
        if (!adminHasAccessToMenu(7)) {
            return redirect()->route('admin.dashboard');
        }
        $user = User::where('id',$id)->first();
        if(@$user){
            if($user->is_join_affiliate == 'Y'){
                $up = $user->update(['is_join_affiliate' => 'N']);
                if($up){
                     return redirect()->back()->with('success', 'User is blocked for affiliation program');
                }
            }else{
                $up = $user->update(['is_join_affiliate' => 'Y']);
                if($up){
                     return redirect()->back()->with('success', 'User is unblocked for affiliation program');
                }
            }
        }
        return redirect()->back()->with('error', 'Product Not Found');
    }

    public function affiliateProductPurchaseDetail($id,$pro_id){

        dd(ProductOrderDetails::with('orderMaster')->where('affiliate_id',$id)->where('product_id',$pro_id)->first());



    }

    /**
     *method:  manageVideoAffiliate
     *created By: Soumojit
     *Description: manageVideoAffiliate
     *Date: 11-SEP-2021
     */
    public function manageVideoAffiliate(Request $request){
        if (!adminHasAccessToMenu(33)) {
            return redirect()->route('admin.dashboard');
        }
        $users = User::where('is_video_affiliate', 'Y')->with(['countryName','bankAccount']);
        if($request->all()){
            if($request->keyword){
                $users=$users->where(function ($query) use ($request) {
                    $query->where('name', 'like', '%' . $request->keyword . '%')
                    ->orWhere('email', $request->keyword)
                    ->orWhereHas('countryName', function ($q) use ($request) {
                        $q->where('name', 'like', '%' . $request->keyword . '%');
                    });
                });
            }
            $data['key'] =$request->all();
        }
        $data['users']=$users->get();
        foreach($data['users'] as $i=>$user){
            $user->affiliated_user = User::where('video_affiliate_id', $user->id)->count();
        }
        return view('admin.modules.affiliate.manage_video_affiliate')->with($data);
    }

    /**
     *method:  manageVideoAffiliateEarning
     *created By: Soumojit
     *Description: see Video Affiliate Earning booking list
     *Date: 11-SEP-2021
     */
    public function manageVideoAffiliateEarning($id=null){
        if (!adminHasAccessToMenu(33)) {
            return redirect()->route('admin.dashboard');
        }
        $data['users'] = User::where('id', $id)->first();
        if($data['users']==null){
            return redirect()->route('admin.manage.video.affiliate')->with('error', 'Something went wrong');
        }
        $data['users']->affiliated_user = User::where('video_affiliate_id',$data['users']->id)->count();
        $data['earnings']=VideoAffEarning::where('affiliate_id',$id)->with('bookingDetails','userDetails')->get();
        $data['payment']=VideoAffPayment::where('affiliate_id',$id)->with('userDetails')->get();
        return view('admin.modules.affiliate.video_affiliate_earning_list')->with($data);
    }

    /*
    * Method: index
    * Description: for view Commission
    * Author: Soumojit
    *Date: 13-SEP-2021
    */
    public function affiliateCommission(Request $request)
    {
        if (!adminHasAccessToMenu(35)) {
            return redirect()->route('admin.dashboard');
        }
        $data['commission'] = Commission::first();
        return view('admin.modules.affiliate.manage_commission')->with($data);
    }

    /*
    * Method: editCommission
    * Description: for edit Commission
    * Author: Soumojit
    *Date: 13-SEP-2021
    */
    public function editCommission(Request $request,$id)
    {
        if (!adminHasAccessToMenu(35)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            "affiliate_commission_user" => "required",
            "affiliate_commission_professional" => "required",
        ]);
        $upd=[
            'affiliate_commission_professional'=>$request->affiliate_commission_professional,
            'affiliate_commission_user'=>$request->affiliate_commission_user,
        ];

        $updateCommission = Commission::where('id',$id)->update($upd);
        if (@$updateCommission) {
            session()->flash("success", "Commissions Changed");
            return redirect()->back();
        }
        session()->flash("error", "Commissions Not Changed");
        return redirect()->back();

    }
    /*
    * Method: videoAffiliatePayment
    * Description: for video Affiliate Payment by admin
    * Author: Soumojit
    *Date: 13-SEP-2021
    */
    public function videoAffiliatePayment(Request $request){
        if (!adminHasAccessToMenu(33)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            "amount" => "required",
            "note" => "required",
        ]);
        $user=User::where('id',@$request->user_id)->first();
        if($user==null){
            session()->flash("error", "Something went wrong");
            return redirect()->back();
        }
        if($request->amount>($user->video_aff_earning-$user->video_aff_paid)){
            session()->flash("error", "Payment amount not getter than due amount");
            return redirect()->back();
        }
        // dd($user);
        $ins=[
            'amount'=>$request->amount,
            'note'=>$request->note,
            'affiliate_id'=>$request->user_id,
        ];
        $success=VideoAffPayment::create($ins);
        if($success){
            User::where('id',@$request->user_id)->increment('video_aff_paid',$request->amount);
            session()->flash("success", "Amount paid affiliate user");
            return redirect()->back();
        }
        session()->flash("error", "Something went wrong");
        return redirect()->back();
    }

    /*
    * Method: videoAffiliateBanner
    * Description: for video Affiliate Payment by admin
    * Author: Soumojit
    *Date: 21-SEP-2021
    */
    public function videoAffiliateBanner(Request $request, $id=null){
        if (!adminHasAccessToMenu(34)) {
            return redirect()->route('admin.dashboard');
        }
        $banner=[1,2,3,4,5,6,7];
        if (!in_array(@$id, $banner))
        {
            session()->flash("error", "Something went wrong");
            return redirect()->route('admin.manage.video.affiliate.banner.list');
        }
        if($request->all()){
            if (@$request->media) {
                $request->validate([
                    'media'    =>  'image|mimes:jpeg,png,jpg',
                ]);
                $media = $request->media;
                $filename = 'banner'.$id.'.png';
                Storage::putFileAs('public/uploads/banner', $media, $filename);
            }
            session()->flash("success", "Banner Changed");
            return redirect()->back();
        }
        return view('admin.modules.affiliate.video_affiliate_banner');
    }
    /*
    * Method: videoAffiliateBanner
    * Description: for video Affiliate Payment by admin
    * Author: Soumojit
    *Date: 21-SEP-2021
    */
    public function videoAffiliateBannerList(Request $request){
        if (!adminHasAccessToMenu(34)) {
            return redirect()->route('admin.dashboard');
        }
        // if($request->all()){
        //     if (@$request->media1) {
        //         $request->validate([
        //             'media1'    =>  'image|mimes:jpeg,png,jpg',
        //         ]);
        //         $media1 = $request->media1;
        //         $filename = 'banner1.png';
        //         Storage::putFileAs('public/uploads/banner', $media1, $filename);
        //     }
        //     if (@$request->media2) {
        //         $request->validate([
        //             'media2'    =>  'image|mimes:jpeg,png,jpg',
        //         ]);
        //         $media2 = $request->media2;
        //         $filename = 'banner2.png';
        //         Storage::putFileAs('public/uploads/banner', $media2, $filename);
        //     }
        //     if (@$request->media3) {
        //         $request->validate([
        //             'media3'    =>  'image|mimes:jpeg,png,jpg',
        //         ]);
        //         $media3 = $request->media3;
        //         $filename = 'banner3.png';
        //         Storage::putFileAs('public/uploads/banner', $media3, $filename);
        //     }
        //     session()->flash("success", "Banner Changed");
        //     return redirect()->route('admin.manage.video.affiliate.banner');
        // }
        return view('admin.modules.affiliate.banner');
    }
    
    /**
     *method:  viewAffiliateEarnings
     *created By: Sayantani
     *Description: view affiliate earnings
     *Date: 28-SEP-2021
     */

    public function viewAffiliateEarnings($id)
    {
        if (!adminHasAccessToMenu(7)) {
            return redirect()->route('admin.dashboard');
        }
        $data['user'] = User::where('id', $id)->first();
        $data['orders'] = ProductOrderDetails::with(['paymentProductDetails'])->where('affiliate_id', $id)->get();
        $due = ProductOrderDetails::with(['paymentProductDetails'])->whereHas('paymentProductDetails',function($a){
            $a->where('affiliate_balance_status','R');
        })->where('affiliate_id', $id)->get();
        $pay= ProductOrderDetails::with(['paymentProductDetails'])->whereHas('paymentProductDetails',function($a){
            $a->where('affiliate_balance_status','W');
        })->where('affiliate_id', $id)->get();
        $data['due']=$due->sum('affiliate_commission');
        $data['paid']=$pay->sum('affiliate_commission');
        $data['total'] = $data['orders']->sum('affiliate_commission');
        return view('admin.modules.affiliate.affiliate_earning_list')->with($data);
    }
}
