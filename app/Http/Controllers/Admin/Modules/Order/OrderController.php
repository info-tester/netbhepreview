<?php

namespace App\Http\Controllers\Admin\Modules\Order;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\AdminPaymentProfessional;
use App\Models\Booking;
use App\Models\MessageDetail;
use App\Models\UserToBankAccount;
use App\Models\Payment;

use App\Models\UserToCard;

use App\User;

use Moip\Moip;
use Moip\Auth\BasicAuth;
use Moip\Auth\OAuth;
use Auth;

use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VideoGrant;

use Session;
use Twilio\Rest\Client;
//use Moip\Moip ;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserBooking;
use App\Mail\ProfessionalBooking;
use App\Mail\BookingReject;
use App\Mail\ProductOrderReject;
use App\Models\ProductOrder;
use App\Mail\ProfessionalProductOrder;
use App\Mail\UserProductOrder;
use App\Models\PaymentDetails;
use App\Models\VideoAffEarning;
use App\Models\Commission;

class OrderController extends Controller
{
    protected $redirectTo = '/admin/login';
    protected $adminAccessToken;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
        $this->adminAccessToken = getAdminToken();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /*
    * Method: index
    * Description: Fetching all orders
    * Author: Abhisek
    */
    public function index(Request $request, $userId = null)
    {
        if (!adminHasAccessToMenu(4)) {
            return redirect()->route('admin.dashboard');
        }
        if ($userId != null) {
            $order = Booking::where('user_id', $userId);
            $customer = User::where('id', $userId)->first();
            $keys['user_name'] = $customer->name;
            /*if($isProf == 'Y') {
                $order = Booking::where('user_id', $userId);
            }
            if($isProf == 'N') {
                $order = Booking::where('user_id', $userId);
            }*/
        } else {
            $order = Booking::where('id', '!=', 0)->whereIn('order_status', ['A','C']);
            if (@$request->all()) {
                if (@$request->token_no) {
                    $order = $order->where('token_no', $request->token_no);
                }
                if (@$request->order_status) {
                    $order = $order->where('order_status', $request->order_status);
                }
                if (@$request->user_name) {

                    $order = $order->whereHas("userDetails", function ($query) use ($request) {
                        $query->where("name", 'LIKE', '%' . $request->user_name . '%');
                    });
                }
                if (@$request->professional) {
                    $order = $order->whereHas("profDetails", function ($query) use ($request) {
                        $query->where('id', $request->professional);
                    });
                }
                if (@$request->to_date || @$request->from_date) {
                    date_default_timezone_set('Asia/Kolkata');
                    $to_date = $request->to_date ? date("d-m-Y", strtotime($request->to_date)) : date("d-m-Y");

                    $from_date = $request->from_date ? date("d-m-Y", strtotime($request->from_date)) : date("d-m-Y");

                    $order = $order->whereBetween("date", [$from_date, $to_date]);
                }
            }
            $keys = $request->all();
        }
        $order = $order->orderBy('id', 'DESC')->get();
        $expert = User::where("is_professional", "Y")->orderBy("name")->get();
        $order = $order->load('userDetails', 'profDetails', 'payment');
        return view('admin.modules.orders.manage_orders')->with([
            'order'        => @$order,
            'key'          => @$keys,
            'expert'       => @$expert
        ]);
    }

    /*
    * Method: view
    * Description: order details
    * Author: Rakesh
    */
    public function view($token_no)
    {
        if (!adminHasAccessToMenu(4)) {
            return redirect()->route('admin.dashboard');
        }
        // dd($order_number);
        $recording_url = null;
        $order = Booking::where('token_no', $token_no)->whereIn('order_status', ['A','C'])->first();
        if (@$order == null) {
            Session::flash('error', 'Unauthorize access');
            return redirect()->back();
        }
        $order = $order->load('userDetails', 'profDetails', 'parentCatDetails', 'childCatDetails', 'payment');
        $message  = MessageDetail::where('token_no', $token_no)->get();
        //dd($message);
        return view('admin.modules.orders.order_details')->with([
            'order' => @$order,
            'message' => @$message,
            'isMaster' => @$order->professional_id
        ]);
    }

    /*
    * Method: makeTransfer
    * Description: For transfering professional balance from wirecard account to bank account.
    * Author: @bhisek
    */
    public function makeTransfer(Request $request)
    {
        if (!adminHasAccessToMenu(4)) {
            return redirect()->route('admin.dashboard');
        }
        $payment = Payment::where('token_no', $request->no)->with('professionalDetails')->first();

        if ($payment == null) {
            session()->flash('error', 'Unauthorize access');
            return redirect()->back();
        }
        $paymentDetails = PaymentDetails::where('id',$request->id)->where('payment_master_id',$payment->id)->first();
        if($paymentDetails==null){
            session()->flash('error', 'Unauthorize access');
            return redirect()->back();
        }
        $user_bank = UserToBankAccount::where('user_id', @$paymentDetails->professional_id)->first();
        if ($user_bank == null) {
            session()->flash('error', 'Fund Could not be transferred, currently professional could not attach his bank account.');
            return redirect()->back();
        }
        $amount = bcmul(@$paymentDetails->professional_amount, 100); // calculating on cents...
        $amount = (int)$amount;
        $pay = array(
            'amount' => @$amount,
            'transferInstrument' =>
            array(
                'method' => 'BANK_ACCOUNT',
                'bankAccount' =>
                array(
                    'id' => $user_bank->account_id,
                ),
            ),
        );

        if (env('PAYMENT_ENVIORNMENT') == 'live') {
            $url = Moip::ENDPOINT_PRODUCTION;
        } else if (env('PAYMENT_ENVIORNMENT') == 'sandbox') {
            $url = Moip::ENDPOINT_SANDBOX;
        }

        $crl = curl_init();
        $header = array();
        // $header[] = 'Content-length: 0';
        $header[] = 'Content-type: application/json';
        $header[] = 'Authorization: Bearer ' . @$paymentDetails->professionalDetails->professional_access_token;
        curl_setopt($crl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($crl, CURLOPT_URL, "{$url}/v2/transfers");
        curl_setopt($crl, CURLOPT_POST, 1);
        curl_setopt($crl, CURLOPT_POST, true);
        curl_setopt($crl, CURLOPT_POSTFIELDS, json_encode($pay));
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        $payment_output = curl_exec($crl);
        curl_close($crl);
        $payment_output = json_decode($payment_output);
        // dd($payment_output);
        if (@$payment_output->errors) {
            if (@$payment_output->errors[0]->code == "TRA-101") {
                session()->flash('error', 'Could not transfer amount, currently this amount is waiting for settlement need 4 to 18 days after booking.');
                return redirect()->back();
            } else {
                session()->flash('error', 'Could not transfer amount, Please check bank information and other information.');
                return redirect()->back();
            }
        } else {

            $payment = PaymentDetails::where('id',$request->id)->where('payment_master_id',$payment->id)->update([
                'balance_status'    =>  'W',
                'wirecard_transfer_id' => @$payment_output->id,
                'wirecard_transfer_response' => json_encode($payment_output)
            ]);

            // $payment = Payment::where('token_no', $request->no)->update([
            //     'balance_status'    =>  'W',
            //     'wirecard_transfer_id' => @$payment_output->id,
            //     'wirecard_transfer_response' => json_encode($payment_output)
            // ]);
            session()->flash('success', 'Amount transferred to professionals bank account successfully.');
            return redirect()->back();
        }
    }


    public function paymentAuthorized()
    {
        dd(1);
    }




    // video call charge status 8.5.20
    public function updateStatus(Request $request, $token_no)
    {
        $orderDetails= Booking::where('token_no', $token_no)->with(['payment','profDetails','userDetails'])->first();
        if ($orderDetails) {

            // $this->chargeUser($token_no);

            if(@$orderDetails->profDetails->video_affiliate_id){
                $affCommission=Commission::first();
                $ins['affiliate_id']=$orderDetails->profDetails->video_affiliate_id;
                $ins['user_id']=$orderDetails->professional_id;
                $ins['booking_id']=$orderDetails->id;
                $ins['type']='P';
                $ins['amount']=$orderDetails->payment->paymentDetails1->admin_commission*($affCommission->affiliate_commission_professional/100);
                VideoAffEarning::create($ins);
                User::where('id',$orderDetails->profDetails->video_affiliate_id)->increment('video_aff_earning', $ins['amount']);

            }
            if(@$orderDetails->userDetails->video_affiliate_id){
                $affCommission=Commission::first();
                $ins['affiliate_id']=$orderDetails->userDetails->video_affiliate_id;
                $ins['user_id']=$orderDetails->user_id;
                $ins['booking_id']=$orderDetails->id;
                $ins['type']='S';
                $ins['amount']=$orderDetails->payment->paymentDetails1->admin_commission*($affCommission->affiliate_commission_user/100);
                VideoAffEarning::create($ins);
                User::where('id',$orderDetails->userDetails->video_affiliate_id)->increment('video_aff_earning', $ins['amount']);
            }
            $booking = Booking::where('token_no', $token_no)
                ->update([
                    'video_status'  =>  'C'
                ]);
            $response['status'] = 1;
        }
        session()->flash('success', 'Status change successfully.');
        return redirect()->route('admin.order');
    }

    /**
     * @method: chargeUser()
     * @purpose: For charging user after video call completation.
     */
    protected function chargeUser($token)
    {
        $user_id = Booking::where('token_no', $token)->first();
        $cardDetails = UserToCard::where('user_id', $user_id->user_id)->first();
        $access_token = getAdminToken();

        $customer = User::where([
            'id'    =>  @$user_id->user_id
        ])->first();

        // In Here the Primary account is the Admin account and the secondar account is the professional account.
        $amount = bcmul(@$user_id->amount, 100); // calculating on cents...
        $split_amount = bcmul(@$user_id->amount / 2, 100); // calculating on cents...]
        $amount = (int)$amount;
        $split_amount = (int)$split_amount;

        if (env('PAYMENT_ENVIORNMENT') == 'live') {
            $moip = new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_PRODUCTION);
        } else if (env('PAYMENT_ENVIORNMENT') == 'sandbox') {
            $moip = new Moip(new OAuth($this->adminAccessToken), Moip::ENDPOINT_SANDBOX);
        }

        $order  =  $moip->orders()->setOwnId(uniqid())
            ->addItem("Consultation Fees", 1, @$user_id->parentCatDetails->name, @$amount)
            ->setShippingAmount(0)->setAddition(0)->setDiscount(0)
            ->setCustomerId(@$customer->customer_id)
            ->addReceiver(@$user_id->profDetails->professional_id, "SECONDARY", @$split_amount, 0, false)
            ->create();

        $UpdateBooking = Booking::where([
            'token_no'  =>  @$token
        ])->update([
            'moip_order_id'  =>  @$order->getId()
        ]);

        $this->payment($order, $customer, $cardDetails, $user_id);
    }

    /**
     *method: payment()
     *purpose: For Deducting customer balance
     */
    public function payment($order, $user, $card, $userBooking)
    {
        if (env('PAYMENT_ENVIORNMENT') == 'live') {
            $url = Moip::ENDPOINT_PRODUCTION;
        } else if (env('PAYMENT_ENVIORNMENT') == 'sandbox') {
            $url = Moip::ENDPOINT_SANDBOX;
        }
        $data = array(
            'installmentCount' => 1,
            'statementDescriptor' => 'netbhe',
            'fundingInstrument' =>
            array(
                'method' => 'CREDIT_CARD',
                'creditCard' =>
                array(
                    'id' => @$card->card_id,
                    'store' => true,
                    'holder' =>
                    array(
                        'fullname' => @$card->card_name,
                        'birthdate' => @$user->dob,
                        'taxDocument' =>
                        array(
                            'type' => 'CPF',
                            'number' => @$user->cpf_no,
                        ),
                        'phone' =>
                        array(
                            'countryCode' => @$user->countryName->phonecode,
                            'areaCode' => @$user->area_code,
                            'number' => @$user->mobile,
                        ),
                        'billingAddress' =>
                        array(
                            'city' => @$user->city,
                            'district' => @$user->district,
                            'street' => @$user->street_name,
                            'streetNumber' => @$user->street_number,
                            'zipCode' => @$user->zipcode,
                            'state' => substr(@$user->stateName->name, 0, 2),
                            'country' => substr(@$user->countryName->name, 0, 3),
                        ),
                    ),
                ),
            ),
        );

        $crl = curl_init();
        $header = array();
        // $header[] = 'Content-length: 0';
        $header[] = 'Content-type: application/json';
        $header[] = 'Authorization: Bearer ' . $this->adminAccessToken;
        curl_setopt($crl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($crl, CURLOPT_URL, "{$url}/v2/orders/" . @$order->getId() . "/payments");
        curl_setopt($crl, CURLOPT_POST, 1);
        curl_setopt($crl, CURLOPT_POST, true);
        curl_setopt($crl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        $payment_output = curl_exec($crl);
        curl_close($crl);
        $payment_output = json_decode($payment_output);
        // dd($payment_output);
        if (@$payment_output->id) {
            $payment = Payment::create([
                'token_no'              =>  @$userBooking->token_no,
                'order_id'              =>  @$order->getId(),
                'payment_id'            =>  @$payment_output->id,
                'user_id'               =>  @$userBooking->user_id,
                'professional_id'       =>  @$userBooking->professional_id,
                'professional_amount'   =>  @$userBooking->amount / 2,
                'admin_amount'          =>  @$userBooking->amount / 2,
                'total_amount'          =>  @$userBooking->amount,
                'wirecard_response'     =>  json_encode($payment_output),
                'balance_status'        =>  'R'
            ]);
            if (@$payment) {
                $UpdateBooking = Booking::where([
                    'token_no'        =>  @$userBooking->token_no
                ])->update([
                    'payment_status'  =>  "P"
                ]);
                return true;
            } else {
                $UpdateBooking = Booking::where([
                    'token_no'        =>  @$userBooking->token_no
                ])->update([
                    'payment_status'  =>  "F"
                ]);

                return false;
            }
        }
    }
    /**
     *@method: rejectPayment
     *purpose: For Failed Payment
     */
    public function rejectPayment($token){
        $booking=Booking::where('token_no',$token)->first();
        $update=Booking::where('id',$booking->id)->update([
            'payment_status'=>'F',
            // 'order_status'=>'R'
        ]);
        Payment::where('token_no', $token)->update([
            'payment_status'=>'F',
        ]);
        if(@$update){
            $userdata = User::where('id', @$booking->user_id)->first();
            User::where('id', $userdata->id)->update(['wallet_balance'=>($userdata->wallet_balance + @$booking->wallet)]);
            Mail::send(new BookingReject($booking));
            session()->flash('success', 'Payment Reject.');
            return redirect()->route('admin.order');
        }
        session()->flash('error', 'Payment Not Reject');
        return redirect()->back();
    }
    /**
     *@method: approvePayment
     *purpose: For Approve Payment
     */
    public function approvePayment($token){
        $booking=Booking::where('token_no',$token)->first();
        $update=Booking::where('id',$booking->id)->update([
            'payment_status'=>'P'
        ]);
        Payment::where('token_no', $token)->update([
            'payment_status' => 'P',
        ]);
        if(@$update){
            $userdata = User::where('id', @$booking->user_id)->first();
            $userdata1 = User::where('id', @$booking->professional_id)->first();
            $professionalmail = $userdata1->email;
            $usermail = $userdata->email;
            $bookingdata = $booking;
            // for send mail to user
            Mail::send(new UserBooking($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));

            // // for send mail to professional
            Mail::send(new ProfessionalBooking($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));

            session()->flash('success', 'Payment Approved.');
            return redirect()->back();
        }
        session()->flash('error', 'Payment Not Approve');
        return redirect()->back();
    }
    /**
     *@method: rejectProductOrderPayment
     *purpose: For Failed Payment
     */
    public function rejectProductOrderPayment($token){
        $booking= ProductOrder::where('token_no',$token)->first();
        $update= ProductOrder::where('id',$booking->id)->update([
            'payment_status'=>'F',
            // 'order_status'=>'R'
        ]);
        Payment::where('token_no', $token)->update([
            'payment_status' => 'F',
        ]);
        $userdata = User::where('id', @$booking->user_id)->first();
        User::where('id', $userdata->id)->update(['wallet_balance'=>($userdata->wallet_balance + @$booking->wallet)]);
        if(@$update){
            // Mail::send(new ProductOrderReject($booking));
            session()->flash('success', 'Payment Reject.');
            return redirect()->route('admin.order.product');
        }
        session()->flash('error', 'Payment Not Reject');
        return redirect()->back();
    }
    /**
     *@method: approveProductOrderPayment
     *purpose: For Approve Payment
     */
    public function approveProductOrderPayment($token){
        $booking= ProductOrder::where('token_no',$token)->first()->load('profDetails', 'productDetails', 'productCategoryDetails');
        $update= ProductOrder::where('id',$booking->id)->update([
            'payment_status'=>'P'
        ]);
        Payment::where('token_no', $token)->update([
            'payment_status' => 'P',
        ]);
        if(@$update){
            // $userdata = User::where('id', @$booking->user_id)->first();
            // $userdata1 = User::where('id', @$booking->professional_id)->first();
            // $professionalmail = $userdata1->email;
            // $usermail = $userdata->email;
            // $bookingdata = $booking;
            // for send mail to user
            // Mail::send(new UserProductOrder($usermail, $professionalmail, $bookingdata, $userdata, $userdata1));

            // // for send mail to professional
            // Mail::send(new ProfessionalProductOrder($usermail, $professionalmail, $bookingdata, $userdata1, $userdata));

            session()->flash('success', 'Payment Approved.');
            return redirect()->back();
        }
        session()->flash('error', 'Payment Not Approve');
        return redirect()->back();
    }

    /*
    * Method: makeTransferBank
    * Description: For transfer booking amount to professional bank account conformation by admin.
    * Author: @soumojit
    */
    public function makeTransferBank(Request $request)
    {
        if (!adminHasAccessToMenu(4)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            'note' => "required",

        ]);

        $payment = Payment::where('token_no', $request->payment_token_no)->with('professionalDetails')->first();
        if ($payment == null) {
            session()->flash('error', 'Unauthorize access');
            return redirect()->back();
        }
        $paymentDetails = PaymentDetails::where('id',$request->paymentId)->where('payment_master_id',$payment->id)->first();
        if($paymentDetails==null){
            session()->flash('error', 'Unauthorize access');
            return redirect()->back();
        }
        $payment = PaymentDetails::where('id',$request->paymentId)->where('payment_master_id',$payment->id)->update([
            'balance_status'    =>  'W',
            'note' => @$request->note
        ]);
        $booking = ProductOrder::where('token_no', $request->payment_token_no)->first();
        $userdata1 = User::where('id', @$paymentDetails->professional_id)->first();
        if ($payment) {
            Mail::send(new AdminPaymentProfessional($userdata1, @$request->note, $paymentDetails));
            session()->flash('success', 'Amount transferred to professionals bank account successfully.');
            return redirect()->back();
        }
        session()->flash('error', 'Amount Not transferred to professionals bank account successfully.');
        return redirect()->back();
    }
    /*
    * Method:makeTransferBankProduct
    * Description: For transfer booking amount to professional bank account conformation by admin.
    * Author: @soumojit
    */
    public function makeTransferBankProduct(Request $request)
    {
        if (!adminHasAccessToMenu(4)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            'note' => "required",

        ]);

        $payment = Payment::where('token_no', $request->payment_token_no)->with('professionalDetails')->first();
        if ($payment == null) {
            session()->flash('error', 'Unauthorize access');
            return redirect()->back();
        }
        $paymentDetails = PaymentDetails::where('id',$request->paymentId)->where('payment_master_id',$payment->id)->first();
        if($paymentDetails==null){
            session()->flash('error', 'Unauthorize access');
            return redirect()->back();
        }
        // return $request;
        // $payment = Payment::where('token_no', $request->payment_token_no)->update([
        //     'balance_status'    =>  'W',
        //     'note' => @$request->note
        // ]);
        $payment = PaymentDetails::where('id',$request->paymentId)->where('payment_master_id',$payment->id)->update([
            'balance_status'    =>  'W',
            'note' => @$request->note
        ]);
        $booking = ProductOrder::where('token_no', $request->payment_token_no)->first();
        $userdata1 = User::where('id', @$paymentDetails->professional_id)->first();
        if ($payment) {
            Mail::send(new AdminPaymentProfessional($userdata1, @$request->note, $paymentDetails));
            session()->flash('success', 'Amount transferred to professionals bank account successfully.');
            return redirect()->back();
        }
        session()->flash('error', 'Amount Not transferred to professionals bank account successfully.');
        return redirect()->back();
    }

    /*
    * Method:makeTransferPaypal
    * Description: For transfer booking amount to professional bank account conformation by admin.
    * Author: @soumojit
    */
    public function makeTransferPaypal(Request $request)
    {
        if (!adminHasAccessToMenu(4)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            'note' => "required",

        ]);

        $payment = Payment::where('token_no', $request->payment_token_no)->with('professionalDetails')->first();
        if ($payment == null) {
            session()->flash('error', 'Unauthorize access');
            return redirect()->back();
        }
        $paymentDetails = PaymentDetails::where('id',$request->paymentId)->where('payment_master_id',$payment->id)->first();
        if($paymentDetails==null){
            session()->flash('error', 'Unauthorize access');
            return redirect()->back();
        }
        $payment = PaymentDetails::where('id',$request->paymentId)->where('payment_master_id',$payment->id)->update([
            'balance_status'    =>  'W',
            'note' => @$request->note
        ]);
        $booking = ProductOrder::where('token_no', $request->payment_token_no)->first();
        $userdata1 = User::where('id', @$paymentDetails->professional_id)->first();
        if ($payment) {
            Mail::send(new AdminPaymentProfessional($userdata1, @$request->note, $paymentDetails));
            session()->flash('success', 'Amount transferred to professionals bank account successfully.');
            return redirect()->back();
        }
        session()->flash('error', 'Amount Not transferred to professionals bank account successfully.');
        return redirect()->back();
    }
    /*
    * Method:makeTransferBankProduct
    * Description: For transfer booking amount to affiliate bank account conformation by admin.
    * Author: @soumojit
    */
    public function makeTransferBankProductAffiliate(Request $request)
    {
        if (!adminHasAccessToMenu(4)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            'affiliate_note' => "required",

        ]);

        $payment = Payment::where('token_no', $request->affiliate_payment_token_no)->with('professionalDetails')->first();
        if ($payment == null) {
            session()->flash('error', 'Unauthorize access');
            return redirect()->back();
        }
        $paymentDetails = PaymentDetails::where('id',$request->affiliate_paymentId)->where('payment_master_id',$payment->id)->first();
        if($paymentDetails==null){
            session()->flash('error', 'Unauthorize access');
            return redirect()->back();
        }
        // return $request;
        // $payment = Payment::where('token_no', $request->payment_token_no)->update([
        //     'balance_status'    =>  'W',
        //     'note' => @$request->note
        // ]);
        $payment = PaymentDetails::where('id',$request->affiliate_paymentId)->where('payment_master_id',$payment->id)->update([
            'affiliate_balance_status'    =>  'W',
            'affiliate_payment_note' => @$request->affiliate_note
        ]);
        $booking = ProductOrder::where('token_no', $request->affiliate_payment_token_no)->first();
        $userdata1 = User::where('id', @$paymentDetails->affiliate_id)->first();
        if ($payment) {
            // Mail::send(new AdminPaymentProfessional($userdata1, @$request->affiliate_note, $paymentDetails));
            session()->flash('success', 'Amount transferred to affiliate bank account successfully.');
            return redirect()->back();
        }
        session()->flash('error', 'Amount Not transferred to affiliate bank account successfully.');
        return redirect()->back();
    }

    /*
    * Method:makeTransferPaypal
    * Description: For transfer booking amount to affiliate bank account conformation by admin.
    * Author: @soumojit
    */
    public function makeTransferPaypalAffiliate(Request $request)
    {
        if (!adminHasAccessToMenu(4)) {
            return redirect()->route('admin.dashboard');
        }
        $request->validate([
            'affiliate_note' => "required",

        ]);

        $payment = Payment::where('token_no', $request->affiliate_payment_token_no)->with('professionalDetails')->first();

        if ($payment == null) {
            session()->flash('error', 'Unauthorize access');
            return redirect()->back();
        }
        $paymentDetails = PaymentDetails::where('id',$request->affiliate_paymentId)->where('payment_master_id',$payment->id)->first();
        if($paymentDetails==null){
            session()->flash('error', 'Unauthorize access');
            return redirect()->back();
        }
        $payment = PaymentDetails::where('id',$request->affiliate_paymentId)->where('payment_master_id',$payment->id)->update([
            'affiliate_balance_status'    =>  'W',
            'affiliate_payment_note' => @$request->affiliate_note
        ]);
        $booking = ProductOrder::where('token_no', $request->affiliate_payment_token_no)->first();
        $userdata1 = User::where('id', @$paymentDetails->affiliate_id)->first();
        if ($payment) {
            // Mail::send(new AdminPaymentProfessional($userdata1, @$request->affiliate_note, $paymentDetails));
            session()->flash('success', 'Amount transferred to affiliate bank account successfully.');
            return redirect()->back();
        }
        session()->flash('error', 'Amount Not transferred to affiliate bank account successfully.');
        return redirect()->back();
    }

    public function makeMultipleTransfer(Request $request){
        return $request;
    }
        /*
    * Method: makeTransferAffiliate
    * Description: For transfering Affiliate commission from wirecard account to bank account.
    * Author: @soumojit
    */
    public function makeTransferAffiliate(Request $request)
    {
        if (!adminHasAccessToMenu(4)) {
            return redirect()->route('admin.dashboard');
        }
        $payment = Payment::where('token_no', $request->affiliate_no)->with('professionalDetails')->first();

        if ($payment == null) {
            session()->flash('error', 'Unauthorize access');
            return redirect()->back();
        }
        $paymentDetails = PaymentDetails::where('id',$request->affiliate_id)->where('payment_master_id',$payment->id)->first();
        if($paymentDetails==null){
            session()->flash('error', 'Unauthorize access');
            return redirect()->back();
        }

        $user_bank = UserToBankAccount::where('user_id', @$paymentDetails->affiliate_id)->first();
        if ($user_bank == null) {
            session()->flash('error', 'Fund Could not be transferred, currently professional could not attach his bank account.');
            return redirect()->back();
        }
        $amount = bcmul(@$paymentDetails->affiliate_commission, 100); // calculating on cents...
        $amount = (int)$amount;
        $pay = array(
            'amount' => @$amount,
            'transferInstrument' =>
            array(
                'method' => 'MOIP_ACCOUNT',
                'moipAccount' =>
                array(
                    'id' => @$paymentDetails->affiliateDetails->professional_id,
                ),
            ),
        );

        if (env('PAYMENT_ENVIORNMENT') == 'live') {
            $url = Moip::ENDPOINT_PRODUCTION;
        } else if (env('PAYMENT_ENVIORNMENT') == 'sandbox') {
            $url = Moip::ENDPOINT_SANDBOX;
        }

        $crl = curl_init();
        $header = array();
        // $header[] = 'Content-length: 0';
        $header[] = 'Content-type: application/json';
        $header[] = 'Authorization: Bearer ' . $this->adminAccessToken;
        curl_setopt($crl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($crl, CURLOPT_URL, "{$url}/v2/transfers");
        curl_setopt($crl, CURLOPT_POST, 1);
        curl_setopt($crl, CURLOPT_POST, true);
        curl_setopt($crl, CURLOPT_POSTFIELDS, json_encode($pay));
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        $payment_output = curl_exec($crl);
        curl_close($crl);
        $payment_output = json_decode($payment_output);
        // dd($payment_output);
        if (@$payment_output->errors) {
            if (@$payment_output->errors[0]->code == "TRA-101") {
                session()->flash('error', 'Could not transfer amount, currently this amount is waiting for settlement need 4 to 18 days after booking.');
                return redirect()->back();
            } else {
                session()->flash('error', 'Could not transfer amount, Please check bank information and other information.');
                return redirect()->back();
            }
        } else {
            $amount = bcmul(@$paymentDetails->affiliate_commission, 100); // calculating on cents...
            $amount = (int)$amount;
            $pay1 = array(
                'amount' => @$amount,
                'transferInstrument' =>
                array(
                    'method' => 'BANK_ACCOUNT',
                    'bankAccount' =>
                    array(
                        'id' => $user_bank->account_id,
                    ),
                ),
            );
            if (env('PAYMENT_ENVIORNMENT') == 'live') {
                $url = Moip::ENDPOINT_PRODUCTION;
            } else if (env('PAYMENT_ENVIORNMENT') == 'sandbox') {
                $url = Moip::ENDPOINT_SANDBOX;
            }
            $crl = curl_init();
            $header = array();
            // $header[] = 'Content-length: 0';
            $header[] = 'Content-type: application/json';
            $header[] = 'Authorization: Bearer ' . @$paymentDetails->affiliateDetails->professional_access_token;
            curl_setopt($crl, CURLOPT_HTTPHEADER, $header);
            curl_setopt($crl, CURLOPT_URL, "{$url}/v2/transfers");
            curl_setopt($crl, CURLOPT_POST, 1);
            curl_setopt($crl, CURLOPT_POST, true);
            curl_setopt($crl, CURLOPT_POSTFIELDS, json_encode($pay1));
            curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
            $payment_output = curl_exec($crl);
            curl_close($crl);
            $payment_output = json_decode($payment_output);

            $payment = PaymentDetails::where('id',$request->affiliate_id)->where('payment_master_id',$payment->id)->update([
                'affiliate_balance_status'    =>  'W',
                'affiliate_wirecard_transfer_id' => @$payment_output->id,
                'affiliate_wirecard_transfer_response' => json_encode($payment_output)
            ]);

            // $payment = Payment::where('token_no', $request->no)->update([
            //     'balance_status'    =>  'W',
            //     'wirecard_transfer_id' => @$payment_output->id,
            //     'wirecard_transfer_response' => json_encode($payment_output)
            // ]);
            session()->flash('success', 'Amount transferred to affiliate bank account successfully.');
            return redirect()->back();
        }
    }


    /*
    * Method: makeTransfer
    * Description: For transfering professional balance from wirecard account to bank account.
    * Author: @bhisek
    */
    public function makeTransferProduct(Request $request)
    {
        if (!adminHasAccessToMenu(4)) {
            return redirect()->route('admin.dashboard');
        }
        $payment = Payment::where('token_no', $request->no)->with('professionalDetails')->first();

        if ($payment == null) {
            session()->flash('error', 'Unauthorize access');
            return redirect()->back();
        }
        $paymentDetails = PaymentDetails::where('id',$request->id)->where('payment_master_id',$payment->id)->first();
        if($paymentDetails==null){
            session()->flash('error', 'Unauthorize access');
            return redirect()->back();
        }
        $user_bank = UserToBankAccount::where('user_id', @$paymentDetails->professional_id)->first();
        if ($user_bank == null) {
            session()->flash('error', 'Fund Could not be transferred, currently professional could not attach his bank account.');
            return redirect()->back();
        }
        $amount = bcmul(@$paymentDetails->professional_amount, 100); // calculating on cents...
        $amount = (int)$amount;
        $pay = array(
            'amount' => @$amount,
            'transferInstrument' =>
            array(
                'method' => 'MOIP_ACCOUNT',
                'moipAccount' =>
                array(
                    'id' => @$paymentDetails->professionalDetails->professional_id,
                ),
            ),
        );
        // dd($pay);

        if (env('PAYMENT_ENVIORNMENT') == 'live') {
            $url = Moip::ENDPOINT_PRODUCTION;
        } else if (env('PAYMENT_ENVIORNMENT') == 'sandbox') {
            $url = Moip::ENDPOINT_SANDBOX;
        }

        $crl = curl_init();
        $header = array();
        // $header[] = 'Content-length: 0';
        $header[] = 'Content-type: application/json';
        $header[] = 'Authorization: Bearer ' . $this->adminAccessToken;
        curl_setopt($crl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($crl, CURLOPT_URL, "{$url}/v2/transfers");
        curl_setopt($crl, CURLOPT_POST, 1);
        curl_setopt($crl, CURLOPT_POST, true);
        curl_setopt($crl, CURLOPT_POSTFIELDS, json_encode($pay));
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        $payment_output = curl_exec($crl);
        curl_close($crl);
        $payment_output = json_decode($payment_output);
        // dd($payment_output);

        if (@$payment_output->errors) {
            if (@$payment_output->errors[0]->code == "TRA-101") {
                session()->flash('error', 'Could not transfer amount, currently this amount is waiting for settlement need 4 to 18 days after booking.');
                return redirect()->back();
            } else {
                session()->flash('error', 'Could not transfer amount, Please check bank information and other information.');
                return redirect()->back();
            }
        } else {
            $amount = bcmul(@$paymentDetails->professional_amount, 100); // calculating on cents...
            $amount = (int)$amount;
            $pay1 = array(
                'amount' => @$amount,
                'transferInstrument' =>
                array(
                    'method' => 'BANK_ACCOUNT',
                    'bankAccount' =>
                    array(
                        'id' => $user_bank->account_id,
                    ),
                ),
            );

            if (env('PAYMENT_ENVIORNMENT') == 'live') {
                $url = Moip::ENDPOINT_PRODUCTION;
            } else if (env('PAYMENT_ENVIORNMENT') == 'sandbox') {
                $url = Moip::ENDPOINT_SANDBOX;
            }

            $crl = curl_init();
            $header = array();
            // $header[] = 'Content-length: 0';
            $header[] = 'Content-type: application/json';
            $header[] = 'Authorization: Bearer ' . @$paymentDetails->professionalDetails->professional_access_token;
            curl_setopt($crl, CURLOPT_HTTPHEADER, $header);
            curl_setopt($crl, CURLOPT_URL, "{$url}/v2/transfers");
            curl_setopt($crl, CURLOPT_POST, 1);
            curl_setopt($crl, CURLOPT_POST, true);
            curl_setopt($crl, CURLOPT_POSTFIELDS, json_encode($pay1));
            curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
            $payment_output = curl_exec($crl);
            curl_close($crl);
            $payment_output = json_decode($payment_output);

            $payment = PaymentDetails::where('id',$request->id)->where('payment_master_id',$payment->id)->update([
                'balance_status'    =>  'W',
                'wirecard_transfer_id' => @$payment_output->id,
                'wirecard_transfer_response' => json_encode($payment_output)
            ]);

            // $payment = Payment::where('token_no', $request->no)->update([
            //     'balance_status'    =>  'W',
            //     'wirecard_transfer_id' => @$payment_output->id,
            //     'wirecard_transfer_response' => json_encode($payment_output)
            // ]);
            session()->flash('success', 'Amount transferred to professionals bank account successfully.');
            return redirect()->back();
        }
    }
}
