<?php

namespace App\Http\Controllers\Admin\Modules\CertificateTemplate;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\ProfessionalSpecialty;
use App\Models\ContractTemplatMaster;
use App\Models\UserToTools;
use App\User;
use App\Models\CertificateTemplateMaster;


use Session;
use Validator;
use Auth;



class CertificateTemplateController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }

    



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!adminHasAccessToMenu(35)) {
            return redirect()->route('admin.dashboard');
        }
        //$data['all_paid_users'] = Booking::select('user_id')->distinct()->with('userDetails')->where('professional_id',Auth::id())->where('payment_status','P')->get();

        // $data['contract_templats'] = ContractTemplatMaster::with('user')->where('status', '!=', 'DELETED')->orderBy('id', 'DESC')->with(['user'])->get();

        $data['contract_templats'] = CertificateTemplateMaster::with('user')->where('status', '!=', 'DELETED')->orderBy('id', 'DESC')->with(['user'])->get();
        // dd($data['details']);

        return view('admin.modules.certificate_template.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (!adminHasAccessToMenu(35)) {
            return redirect()->route('admin.dashboard');
        }
        $data = array();
        $data['specialities'] = ProfessionalSpecialty::orderBy('id', 'DESC')->get();

        return view('admin.modules.certificate_template.create')->with($data);
        //return view('admin.modules.product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!adminHasAccessToMenu(35)) {
            return redirect()->route('admin.dashboard');
        }

        $request->validate([
            'title'            => 'required',
            'certificate_desc'    => 'required'
        ]);
        $insert['title']                = $request->title;
        $insert['certificate_description']        = $request->certificate_desc;
        //$insert['status']                    = 'Form';
        $insert['status']               = 'ACTIVE';
        $insert['added_by']             = 'A';
        $insert['added_by_id']          = Auth::guard('admin')->id();
        $insert['shown_in_proff']       = 'N';
        $contract_temp = CertificateTemplateMaster::create($insert);
        // $contract_temp->specialities()->sync($request->speciality_ids);

        session()->flash('success', 'Certificate template added successfully.');
        return redirect()->back();
    }


    //product Details Show

    public function show($id)
    {
        if (!adminHasAccessToMenu(35)) {
            return redirect()->route('admin.dashboard');
        }
        $details = CertificateTemplateMaster::with(['user'])->find($id);

        //$questions = FormMasterDetails::where('form_master_id',$id)->get();


        //pr1($data->toArray());
        //die();
        return view('admin.modules.certificate_template.show', [
            'details'       => $details
        ]);
    }



    /**
     * Show the form for editing a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!adminHasAccessToMenu(35)) {
            return redirect()->route('admin.dashboard');
        }
        $data = array();


        $data['details'] = CertificateTemplateMaster::where('id', $id)->first();
        $data['specialities'] = ProfessionalSpecialty::orderBy('id', 'DESC')->get();
        // $related_spec = $data['details']->specialities()->get();
        // $data['related_spec_ids'] = array();
        // foreach($related_spec as $ar){
        //     array_push($data['related_spec_ids'], $ar->id);
        // }

        return view('admin.modules.certificate_template.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!adminHasAccessToMenu(35)) {
            return redirect()->route('admin.dashboard');
        }

        $request->validate([
            'title'            => 'required',
            'certificate_desc'             => 'required'
        ]);
        $insert['title']                = $request->title;
        $insert['certificate_description']          = $request->certificate_desc;
        $contract_temp = CertificateTemplateMaster::where('id', $id)->first();
        $contract_temp->update($insert);
        // $contract_temp->specialities()->sync($request->speciality_ids);

        session()->flash('success', 'Certificate template edited successfully.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     *  @param  int  $id
     *  @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!adminHasAccessToMenu(35)) {
            return redirect()->route('admin.dashboard');
        }
        $data = UserToTools::where('tool_id', $id)->where('tool_type', 'Contract Template')->first();
        if (@$data) {
            //echo "not delete";
            session()->flash('error', 'Contract template not deleted because it is assigned user.');
            return redirect()->back();
        } else {
            //echo "delete";
            CertificateTemplateMaster::where('id', $id)->delete();
            session()->flash('success', 'Certificate template deleted successfully.');
            return redirect()->back();
        }
    }


    /*
    * Method: tools for professional
    * Description: shown in professional of form tools
    * Author: Puja
    */
    public function certificateToolShowProff($id)
    {

        $data = CertificateTemplateMaster::find($id);
        

        if (@$data->shown_in_proff == 'Y') {
            // update status field to active
            $if_yes = CertificateTemplateMaster::where('id', $id)->update(['shown_in_proff' => 'N']);
            if($if_yes){
                    session()->flash('success', 'Now Imported tools is not showned in professional.');
                    return redirect()->back();
            }
        } else if (@$data->shown_in_proff == 'N') {
            // update status field to active
            $if_no = CertificateTemplateMaster::where('id', $id)->update(['shown_in_proff' => 'Y']);
            if($if_no){
                session()->flash('success', 'Now Imported is showned in professional.');
                 return redirect()->back();
            }
            
        }
    }
}
