<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSubcategory extends Model
{
    //
    protected $table = 'product_subcategories';
    protected $guarded = [];

    public function parentCategory() {
		return $this->belongsTo('App\Models\ProductCategory', 'category_id', 'id');
	}
	
}