<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    protected $table = 'chapters';
    protected $guarded = [];

    public function getLessons(){
        return $this->hasMany('App\Models\Lesson', 'chapter_id','id')->where('status','A');
    }

    public function getAllLessons(){
        return $this->hasMany('App\Models\Lesson', 'chapter_id','id');
    }

    public function getVideoLessons(){
        return $this->hasMany('App\Models\Lesson', 'chapter_id','id')->where('lesson_type','V')->where('is_free_preview','Y')->where('status','A');
    }
    
    public function getProduct(){
        return $this->hasOne('App\Models\Product', 'id', 'course_id');
    }
}
