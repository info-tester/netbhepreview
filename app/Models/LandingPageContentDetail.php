<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LandingPageContentDetail extends Model {

    protected $table = 'landing_page_content_detail';
    protected $guarded = [];

}
