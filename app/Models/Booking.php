<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    //
    protected $table = 'booking';
    protected $guarded = [];

 //    /**
 //    *@ Method 		: parent
 //    *@ Description 	: Relation with Category table
 //    *@ Output       : Parent category details
 //    *@ Author 		: Abhisek
 //    *@ Use By       : Abhisek
 //    */
	// public function parent() {
	// 	return $this->belongsTo('App\Models\Category', 'parent_id');
	// }

 //    *
 //    *@ Method       : parent
 //    *@ Description  : Relation with Category table
 //    *@ Output       : Parent category details
 //    *@ Author       : Abhisek
 //    *@ Use By       : Abhisek

    public function parentCatDetails() {
        return $this->hasOne('App\Models\Category', 'id', 'category_id');
    }

    public function profDetails() {
        return $this->hasOne('App\User', 'id', 'professional_id');
    }

    public function userDetails() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function childCatDetails() {
        return $this->hasOne('App\Models\Category', 'id', 'sub_category_id');
    }

    public function getReview() {
        return $this->hasOne('App\Models\Review', 'booking_id', 'id');
    }

    public function getMsgMaster() {
        return $this->hasOne('App\Models\PusherMessageMaster', 'booking_id', 'id');
    }

    /**
    #method:payment()
    #purpose: For fetching all payment information against a particular order
    */

    public function payment() {
        return $this->hasOne('App\Models\Payment', 'token_no', 'token_no')->with('paymentDetails1');
    }
}
