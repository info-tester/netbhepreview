<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentSignature extends Model
{
    //
    protected $table = 'documents_signatures';
    protected $guarded = [];
}
