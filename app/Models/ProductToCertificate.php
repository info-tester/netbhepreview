<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductToCertificate extends Model
{
    //
    protected $guarded = [];
    protected $table = 'product_to_certificate_template';

    public function getTool(){
        return $this->hasOne('App\Models\CertificateTemplateMaster', 'id', 'tool_id');
    }

   
}
