<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LandingPagePricing extends Model {

    protected $table = 'landing_page_pricing';
    protected $guarded = [];

}
