<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tool360AnsweredDetail extends Model {
    
    protected $table = 'tools_360_answered_detail';
    protected $guarded = [];


    public function getAnswerTitle() {
        return $this->hasOne('App\Models\Tool360Details', 'id', 'tools_360_details_id');
    } 

    // public function getFormData() {
    //     return $this->hasMany('App\Models\FormMasterDetails', 'form_master_id', 'tool_id');
    // }

    // public function getUserData() {
    //     return $this->hasOne('App\User', 'id', 'professional_id');
    // }  

}
