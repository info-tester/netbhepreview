<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentDetails extends Model
{
    protected $table = 'payment_details';
    protected $guarded = [];

    /*
	*method: professionalDetails
	*purpose: For fetching all professional details.
	*author: @bhisek
    */
    public function professionalDetails()
    {
        return $this->hasOne('App\User', 'id', 'professional_id');
    }

    /*
    *method: professionalDetails
    *purpose: For fetching all professional details.
    *author: @bhisek
    */
    public function userDetails()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    /*
    *method: paymentDetails
    *purpose: For Payment details
    *author: @soumojit
    */
    public function paymentDetails()
    {
        return $this->hasOne('App\Models\Payment', 'id', 'payment_master_id');
    }
    public function orderDetails()
    {
        return $this->hasOne('App\Models\ProductOrderDetails', 'id', 'product_order_details_id');
    }
    public function affiliateDetails()
    {
        return $this->hasOne('App\User', 'id', 'affiliate_id');
    }
}
