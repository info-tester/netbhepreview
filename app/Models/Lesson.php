<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    protected $table = 'lessons';
    protected $guarded = [];

    public function getChapter() {
		return $this->belongsTo('App\Models\Chapter', 'chapter_id');
	}

    public function getLessonFile(){
        return $this->hasOne('App\Models\LessonFile','lesson_id','id')->where('filetype','V');
    }

    public function getChildFile(){
     return $this->hasOne('App\Models\LessonFile','lesson_id','id')->where('filetype','T');   
    }
}
