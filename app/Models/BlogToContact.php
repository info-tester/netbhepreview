<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogToContact extends Model
{
    protected $table = 'blog_to_contact';

    protected $guarded = [];
}
