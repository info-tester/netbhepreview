<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductOrder extends Model
{
    //
    protected $table = 'product_orders';
    protected $guarded = [];

    // public function fetchCategory(){
    // 	return $this->hasMany('App\Models\ProductCategory', 'id', 'category_id');
    // }
    public function getProduct()
    {
        return $this->belongsTo('App\Models\Product', 'product_id', 'id');
    }

    // public function professional()
    // {
    //     return $this->belongsTo('App\User', 'professional_id', 'id');
    // }
    public function profDetails()
    {
        return $this->hasOne('App\User', 'id', 'professional_id');
    }
    public function productDetails()
    {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }
    public function productCategoryDetails()
    {
        return $this->hasOne('App\Models\ProductCategory', 'id', 'category_id');
    }


    
    public function userDetails()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public function PaymentDetails()
    {
        return $this->hasOne('App\Models\Payment', 'token_no', 'token_no');
    }

    public function orderDetails(){
        return $this->hasMany('App\Models\ProductOrderDetails', 'product_order_master_id','id' )->with('product', 'profDetails');
    }

}
