<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuizProgress extends Model
{
    protected $table = 'user_quiz_progress';
    protected $guarded = [];

}
