<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LandingPageFaq extends Model {

    protected $table = 'landing_page_faq';
    protected $guarded = [];

}
