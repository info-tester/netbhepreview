<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogComments extends Model
{
    protected $table = 'blog_comments';

    protected $guarded = [];
    
    protected $primary_key = 'id';
}
