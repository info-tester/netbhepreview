<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    //
    protected $table = 'states';
    protected $guarded = [];
    
    /**
    *@ Method 		: parent
    *@ Description 	: Relation with Category table
    *@ Output       : Parent category details
    *@ Author 		: Abhisek 
    *@ Use By       : Abhisek
    */
	public function parent() {
		return $this->belongsTo('App\Models\Category', 'parent_id');
	}

    /**
    *@ Method       : parent
    *@ Description  : Relation with Category table
    *@ Output       : Parent category details
    *@ Author       : Abhisek 
    *@ Use By       : Abhisek
    */
    public function childCat() {
        return $this->hasMany('App\Models\Category', 'parent_id', 'id');
    }

    
}
