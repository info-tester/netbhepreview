<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $table = 'coupons';
    protected $guarded = [];

    public function couponToProduct() {
        return $this->hasOne('App\Models\CouponToProduct', 'coupon_id', 'id');
    }
}
