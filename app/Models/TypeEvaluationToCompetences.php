<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeEvaluationToCompetences extends Model
{
    //
    protected $table = 'tool360_type_evaluation_to_competences';
    protected $guarded = [];

    /**
    *@ Method 		: parent
    *@ Description 	: Relation with Category table
    *@ Output       : Parent category details
    *@ Author 		: Abhisek
    *@ Use By       : Abhisek
    */

    /**
     *
     */
    public function evaluationType()
    {
        return $this->hasOne(TypeOfEvaluationMaster::class, 'id', 'evaluation_type_id');
    }

}
