<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LandingPageMaster extends Model {

    protected $table = 'landing_page_master';
    protected $guarded = [];

    public function getContentDetails(){
        return $this->hasMany('App\Models\LandingPageContentDetail','landing_page_master_id','id');
    }

    public function getProductDetails(){
        return $this->hasMany('App\Models\LandingPageProductDetail','landing_page_master_id','id');
    }

    public function getWhyUs(){
        return $this->hasMany('App\Models\LandingPageWhyUs','landing_page_master_id','id');
    }

    public function getFaq(){
        return $this->hasMany('App\Models\LandingPageFaq','landing_page_master_id','id');
    }
    public function userDetails(){
        return $this->hasOne('App\User','id','user_id');
    }

}
