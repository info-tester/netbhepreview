<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserFormAnswered extends Model {
    protected $table = 'user_form_answered';
    protected $guarded = [];


    public function getFormDetailsData() {
        return $this->hasOne('App\Models\FormMasterDetails', 'id', 'form_details_id');
    }

}
