<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserToCertificate extends Model {
    protected $table = 'user_to_certificate';
    protected $guarded = [];

public function getprofessionalUserData() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
public function getCertificateTemplatesdata(){
        return $this->hasOne('App\Models\CertificateTemplateMaster','id','tool_id');
    }

    
}
