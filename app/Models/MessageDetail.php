<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageDetail extends Model
{
    //
    protected $table = 'message_details';
    protected $guarded = [];
    


    public function receiverDetails() {
        return $this->hasOne('App\User', 'id', 'sender_id');
    }

    public function senderDetails() {
        return $this->hasOne('App\User', 'id', 'receiver_id');
    }

    
}
