<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImportedTool extends Model
{
    //
    protected $guarded = [];
    protected $table = 'imported_tool';

    public function category()
    {
        return $this->hasOne('App\Models\FormCategory', 'id', 'cat_id');
    }
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'added_by_id');
    }
    public function specialities()
    {
        return $this->belongsToMany('App\Models\ProfessionalSpecialty', 'imported_tool_to_speciality');
    }
}
