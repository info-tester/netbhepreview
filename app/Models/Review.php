<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = 'review';
    protected $guarded = [];

    /**
     * method: reviewedUserName.
     * relation : relation with user table on user_id.
     * author: Abhisek.
     */

    public function reviewedUserName(){
    	return $this->hasOne('App\User', 'id', 'user_id');
    }
}
