<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LandingPageWhyUs extends Model {

    protected $table = 'landing_page_why_us';
    protected $guarded = [];

}
