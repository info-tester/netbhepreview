<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLogBookAnswer extends Model {
    protected $table = 'user_log_book_answered';
    protected $guarded = [];


    // public function getFormData() {
    //     return $this->hasMany('App\Models\FormMasterDetails', 'form_master_id', 'tool_id');
    // }

    // public function getUserData() {
    //     return $this->hasOne('App\User', 'id', 'professional_id');
    // }

    // public function getprofessionalUserData() {
    //     return $this->hasOne('App\User', 'id', 'user_id');
    // }

    // public function getSmartGoalsData(){
    //     return $this->hasOne('App\Models\ToolsSmartGoalMaster','id','tool_id');
    // }

    // public function getImportingToolsdata(){
    //     return $this->hasOne('App\Models\ImportedTool','id','tool_id');
    // }

    // public function getContentTemplatesdata(){
    //     return $this->hasOne('App\Models\UserContentTemplate','id','tool_id');
    // }

    // public function getContractTemplatesdata(){
    //     return $this->hasOne('App\Models\ContractTemplatMaster','id','tool_id');
    // }

    // public function getImportedToolsFeedback(){
    //     return $this->hasOne('App\Models\ImportedToolFeedback','user_to_tools_id','id');
    // }

}
