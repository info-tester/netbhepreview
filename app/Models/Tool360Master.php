<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tool360Master extends Model {
    
    protected $table = 'tool_360_master';
    protected $guarded = [];


    public function get360Details() {
        return $this->hasMany('App\Models\Tool360Details', 'id_360master', 'id');
    }

    public function getUserData() {
        return $this->hasOne('App\User', 'id', 'added_by_id');
    }  

}
