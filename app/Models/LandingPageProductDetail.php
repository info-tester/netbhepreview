<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LandingPageProductDetail extends Model {

    protected $table = 'landing_page_product_detail';
    protected $guarded = [];

}
