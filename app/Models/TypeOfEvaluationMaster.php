<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeOfEvaluationMaster extends Model
{
    //
    protected $table = 'tool360_type_of_evaluation_master';
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(ToolsEvaluationCategory::class, 'evaluation_category_id', 'id');
    }

    public function toCompetenceMasters()
    {
        return $this->hasMany(TypeEvaluationToCompetences::class, 'evaluation_type_id', 'id');
    }

    public function specialities()
    {
        return $this->belongsToMany('App\Models\ProfessionalSpecialty', 'tool360_type_of_evaluation_master_to_speciality');
    }
}
