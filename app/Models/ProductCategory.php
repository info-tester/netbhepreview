<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    //
    protected $table = 'product_categories';
    protected $guarded = [];

    public function category_products()
    {
        return $this->hasMany('App\Models\Product', 'category_id', 'id');
    }

    public function getProducts(){
        return $this->hasMany('App\Models\Product', 'category_id', 'id');
    }

    public function getSubCategories(){
       return $this->hasMany('App\Models\ProductSubcategory','category_id','id'); 
    }

    public function getSubCategory(){
        return $this->hasMany('App\Models\ProductSubcategory','category_id','id'); 
    }
}
