<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    //
    protected $table = 'lesson_presentation_files';
    protected $guarded = [];
        
}
