<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompetencesMaster extends Model
{
    //
    protected $table = 'tool360_competences_master';
    protected $guarded = [];


    /**
     *
     */
    public function toEvalType()
    {
        return $this->hasMany(TypeEvaluationToCompetences::class, 'competences_id', 'id');
    }

    public function evalCategory() {
        return $this->hasOne(ToolsEvaluationCategory::class, 'id', 'evaluation_category_id');
    }
    
    public function specialities()
    {
        return $this->belongsToMany('App\Models\ProfessionalSpecialty', 'tool360_competences_master_to_speciality');
    }

}
