<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PusherMessageDetail extends Model
{
    protected $table = 'pusher_message_details';
    protected $guarded = [];

    public function getUser()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function getBooking()
    {
        return $this->hasOne('App\Models\Booking', 'id', 'booking_id');
    }
    public function getBookingToUser()
    {
        return $this->hasMany('App\Models\BookingToUser', 'message_master_id', 'message_master_id')->where('user_id','!=',auth()->user()->id);
    }
}
