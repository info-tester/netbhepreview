<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReferralDiscount extends Model
{
    protected $table = 'referral_discounts';
    protected $guarded = [];
}
