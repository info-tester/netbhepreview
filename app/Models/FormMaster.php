<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FormMaster extends Model
{
    //
    protected $guarded = [];
    protected $table = 'form_master';

    public function category(){
        return $this->hasOne('App\Models\FormCategory', 'id', 'form_cat_id');
    }
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'added_by_id');
    }

    public function specialities()
    {
        return $this->belongsToMany('App\Models\ProfessionalSpecialty', 'form_to_speciality');
    }
}
