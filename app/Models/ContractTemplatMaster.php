<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContractTemplatMaster extends Model
{
    //
    protected $guarded = [];
    protected $table = 'contract_template_master';

    // public function category()
    // {
    //     return $this->hasOne('App\Models\FormCategory', 'id', 'cat_id');
    // }
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'added_by_id');
    }

    public function getUsertoolsData()
    {
        return $this->hasOne('App\Models\UserToTools', 'tool_id', 'id');
    }

    public function specialities()
    {
        return $this->belongsToMany('App\Models\ProfessionalSpecialty', 'contract_template_master_to_speciality');
    }
}
