<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LandingTemplate extends Model {

    protected $table = 'landing_template';
    protected $guarded = [];

}
