<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CouponToProduct extends Model
{
    protected $table = 'coupon_products';
    protected $guarded = [];

    public function productDetails() {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }
}
