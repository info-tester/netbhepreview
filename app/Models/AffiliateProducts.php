<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AffiliateProducts extends Model
{
    protected $table = 'affiliate_products';
    protected $guarded = [];

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }
}
