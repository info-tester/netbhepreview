<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminToAccess extends Model
{
    protected $guarded = [];

    public function menuAccess()
    {
        return $this->hasOne(AdminAccess::class, 'id', 'parent_access_id');
    }
    public function subMenuAccess()
    {
        return $this->hasOne(AdminAccess::class, 'id', 'access_id');
    }
}
