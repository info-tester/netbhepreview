<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FormCategory extends Model
{
    //
    protected $guarded = [];
    protected $table = 'form_category';
}
