<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LandingPageLead extends Model {

    protected $table = 'landing_page_leads';
    protected $guarded = [];

}
