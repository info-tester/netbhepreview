<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FormMasterDetails extends Model
{
    //
    protected $guarded = [];
    protected $table = 'form_master_details';

    // public function category(){
    //     return $this->hasOne('App\Models\FormCategory', 'id', 'form_cat_id');
    // }
    // public function user()
    // {
    //     return $this->hasOne('App\User', 'id', 'added_by_id');
    // }
}
