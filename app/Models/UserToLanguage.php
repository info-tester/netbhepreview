<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserToLanguage extends Model
{
    protected $table = 'user_to_language';
    protected $guarded = [];

    /**
     * method: languageName.
     * relation : relation with language table on user_id.
     * author: Abhisek.
     */

    public function languageName(){
        return $this->hasOne('App\Models\Language', 'id', 'language_id');
    }
}
