<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PusherMessageGroup extends Model
{
    protected $table = 'message_group';
    protected $guarded = [];
}
