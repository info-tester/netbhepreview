<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payments';
    protected $guarded = [];

    /*
	*method: professionalDetails
	*purpose: For fetching all professional details.
	*author: @bhisek
    */
    public function professionalDetails() {
        return $this->hasOne('App\User', 'id', 'professional_id');
    }



    /*
    *method: professionalDetails
    *purpose: For fetching all professional details.
    *author: @bhisek
    */
    public function userDetails() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }


    public function getpaymentDetails(){
        return $this->hasMany('App\Models\PaymentDetails', 'payment_master_id', 'id');
    }

    public function paymentDetails1()
    {
        return $this->hasOne('App\Models\PaymentDetails',  'payment_master_id', 'id');

    }
}
