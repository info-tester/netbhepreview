<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReferContent extends Model
{
    //
    protected $table = 'refer_content';
    protected $guarded = [];
}
