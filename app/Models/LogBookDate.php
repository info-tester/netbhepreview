<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogBookDate extends Model {
    protected $table = 'log_book_date';
    protected $guarded = [];


    // public function getFormData() {
    //     return $this->hasMany('App\Models\FormMasterDetails', 'form_master_id', 'tool_id');
    // }

    // public function getUserData() {
    //     return $this->hasOne('App\User', 'id', 'professional_id');
    // }

    public function getUserData() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function getAnswerData(){
        return $this->hasMany('App\Models\UserLogBookAnswer','log_book_date_id','id');
    }


    // public function getSmartGoalsData(){
    //     return $this->hasOne('App\Models\ToolsSmartGoalMaster','id','tool_id');
    // }

    // public function getImportingToolsdata(){
    //     return $this->hasOne('App\Models\ImportedTool','id','tool_id');
    // }

    // public function getContentTemplatesdata(){
    //     return $this->hasOne('App\Models\UserContentTemplate','id','tool_id');
    // }

    // public function getContractTemplatesdata(){
    //     return $this->hasOne('App\Models\ContractTemplatMaster','id','tool_id');
    // }

    // public function getImportedToolsFeedback(){
    //     return $this->hasOne('App\Models\ImportedToolFeedback','user_to_tools_id','id');
    // }
    // //get logbook data
    // public function getlogbookDetails(){
    //     return $this->hasOne('App\Models\LogBookMaster','id','tool_id');
    // }
    // //get logbook answer data
    // public function getlogbookAnswerDetails(){
    //     return $this->hasOne('App\Models\UserLogBookAnswer','user_to_tools_id','id');
    // }
    
}
