<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductOrderDetails extends Model
{
    //
    protected $table = 'product_order_details';
    protected $guarded = [];

    public function product()
    {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }
    public function profDetails()
    {
        return $this->hasOne('App\User', 'id', 'professional_id');
    }
    

    public function orderMaster()
    {
        return $this->hasOne('App\Models\ProductOrder', 'id', 'product_order_master_id');
    }

    public function paymentProductDetails(){
        return $this->hasOne('App\Models\PaymentDetails', 'product_order_details_id', 'id');
    }
    
    public function affiliateDetails()
    {
        return $this->hasOne('App\User', 'id', 'affiliate_id');
    }
}
