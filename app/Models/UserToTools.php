<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserToTools extends Model {
    protected $table = 'user_to_tools';
    protected $guarded = [];


    public function getFormData() {
        return $this->hasMany('App\Models\FormMasterDetails', 'form_master_id', 'tool_id');
    }

    public function getUserData() {
        return $this->hasOne('App\User', 'id', 'professional_id');
    }

    public function getprofessionalUserData() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function getSmartGoalsData(){
        return $this->hasOne('App\Models\ToolsSmartGoalMaster','id','tool_id');
    }

    public function getImportingToolsdata(){
        return $this->hasOne('App\Models\ImportedTool','id','tool_id');
    }

    public function getContentTemplatesdata(){
        return $this->hasOne('App\Models\UserContentTemplate','id','tool_id');
    }

    public function getContractTemplatesdata(){
        return $this->hasOne('App\Models\ContractTemplatMaster','id','tool_id');
    }

    public function getImportedToolsFeedback(){
        return $this->hasOne('App\Models\ImportedToolFeedback','user_to_tools_id','id');
    }
    //get logbook data
    public function getlogbookDetails(){
        return $this->hasOne('App\Models\LogBookMaster','id','tool_id');
    }
    //get logbook answer data
    public function getlogbookAnswerDetails(){
        return $this->hasOne('App\Models\UserLogBookAnswer','user_to_tools_id','id');
    }

    public function getEvulation360Url() {
        return $this->hasMany('App\Models\Tools360EvaluationUrlShare', 'user_to_tools_id', 'id');
    }

    public function getFormDetails() {
        return $this->hasOne('App\Models\FormMaster', 'id', 'tool_id');
    }
    public function getTool360MasterDetails() {
        return $this->hasOne('App\Models\Tool360Master', 'id', 'tool_id');
    }
}
