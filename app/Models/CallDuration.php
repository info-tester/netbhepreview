<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CallDuration extends Model
{
    //
    protected $table = 'call_duration';
    protected $guarded = [];
}