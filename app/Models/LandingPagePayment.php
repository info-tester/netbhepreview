<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LandingPagePayment extends Model {

    protected $table = 'landing_page_payment';
    protected $guarded = [];

    public function landingDetails(){
        return $this->hasOne('App\Models\LandingPageMaster','id','template_id');
    }
    public function userDetails(){
        return $this->hasOne('App\User','id','user_id');
    }

}
