<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HelpCategory extends Model
{
    //
    protected $guarded = [];
    protected $table = 'help_categories';

    public function getArticles(){
        return $this->hasMany('App\Models\HelpArticle','help_category_id','id')->where('status','A');
    }
    
}
