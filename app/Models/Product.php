<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table = 'products';
    protected $guarded = [];

    // public function fetchCategory(){
    // 	return $this->hasMany('App\Models\ProductCategory', 'id', 'category_id');
    // }
    public function category()
    {
        return $this->belongsTo('App\Models\ProductCategory', 'category_id', 'id');
    }

    public function subCategory()
    {
        return $this->belongsTo('App\Models\ProductSubcategory', 'subcategory_id', 'id');
    }

    public function professional()
    {
        return $this->belongsTo('App\User', 'professional_id', 'id');
    }
    public function productFile()
    {
        return $this->hasMany('App\Models\ProductToFile', 'product_id', 'id')->where('file_status','A');
    }
    public function assignedTemplate(){
        return $this->hasOne('App\Models\ProductToCertificate', 'product_id', 'id');
     }
}
