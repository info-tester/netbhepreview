<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductToFile extends Model
{
    //
    protected $table = 'product_to_file';
    protected $guarded = [];
}
