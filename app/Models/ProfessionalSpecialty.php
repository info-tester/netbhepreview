<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProfessionalSpecialty extends Model
{

    public $table = 'professional_specialties';
    protected $guarded = [];

    public function evalCats(){
        return $this->belongsToMany('App\Models\ToolsEvaluationCategory', 'tools_evaluation_categories_to_speciality');
    }
}
