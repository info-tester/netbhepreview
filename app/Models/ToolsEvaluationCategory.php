<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ToolsEvaluationCategory extends Model
{
    protected $guarded = [];
    
    public function specialities()
    {
        return $this->belongsToMany('App\Models\ProfessionalSpecialty', 'tools_evaluation_categories_to_speciality');
    }
}
