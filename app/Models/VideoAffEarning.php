<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VideoAffEarning extends Model
{
    protected $table = 'video_aff_earning';
    protected $guarded = [];


    public function userDetails() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public function bookingDetails() {
        return $this->hasOne('App\Models\Booking', 'id', 'booking_id');
    }
}
