<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'blogs';

    protected $guarded = [];
    
    protected $primary_key = 'id';

    /*
    * Method : blogCategoryName
    * Relation : with BlogCategory table
    * Description : find Blog Category name
    * Author : Abhisek
    */
    public function blogCategoryName(){
        return $this->hasOne('App\Models\BlogCategory', 'id', 'cat_id');
    }
    
    /*
    * Method : Comments
    * Relation : with BlogComments table
    * Description : find Blog Category name
    * Author : Abhisek
    */
    public function blogComments(){
        return $this->hasMany('App\Models\BlogComments', 'blog_id', 'id')->where('status', 'A');
    } 

    /*
    * Method : blogCategoryName
    * Relation : with BlogCategory table
    * Description : find Blog Category name
    * Author : Abhisek
    */
    public function postedBy(){
        return $this->hasOne('App\User', 'id', 'posted_by');
    }

    /*
    * Method : blogCategoryName
    * Relation : with BlogCategory table
    * Description : find Blog Category name
    * Author : Abhisek
    */
    public function adminPostedBy(){
        return $this->hasOne('App\Admin', 'id', 'admin_post_id');
    }
}
