<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageMaster extends Model
{
    //
    protected $table = 'message_master';
    protected $guarded = [];
    
 //    /**
 //    *@ Method 		: parent
 //    *@ Description 	: Relation with Category table
 //    *@ Output       : Parent category details
 //    *@ Author 		: Abhisek 
 //    *@ Use By       : Abhisek
 //    */
	// public function parent() {
	// 	return $this->belongsTo('App\Models\Category', 'parent_id');
	// }

 //    *
 //    *@ Method       : parent
 //    *@ Description  : Relation with Category table
 //    *@ Output       : Parent category details
 //    *@ Author       : Abhisek 
 //    *@ Use By       : Abhisek
    
    public function sendLastConv() {
        return $this->hasOne('App\Models\MessageDetail', 'sender_id', 'sender_id');
    }

    public function rcvLastConv() {
        return $this->hasOne('App\Models\MessageDetail', 'receiver_id', 'receiver_id');
    }

    public function profDetails() {
        return $this->hasOne('App\User', 'id', 'professional_id');
    }

    public function userDetails() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function childCatDetails() {
        return $this->hasOne('App\Models\Category', 'id', 'sub_category_id');
    }

    
}
