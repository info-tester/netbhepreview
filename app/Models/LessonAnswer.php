<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LessonAnswer extends Model
{
    protected $table = 'lesson_answers';
    protected $guarded = [];

    public function getAnswerer(){
        return $this->hasOne('App\User','id','user_id');
    }
}
