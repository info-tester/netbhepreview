<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VideoAffPayment extends Model
{
    protected $table = 'video_aff_payment';
    protected $guarded = [];


    public function userDetails() {
        return $this->hasOne('App\User', 'id', 'affiliate_id');
    }
}
