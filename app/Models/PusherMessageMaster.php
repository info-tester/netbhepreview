<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PusherMessageMaster extends Model
{
    protected $table = 'pusher_message_master';
    protected $guarded = [];

    public function getBooking() {
        return $this->hasOne('App\Models\Booking', 'id', 'booking_id');
    }
}
