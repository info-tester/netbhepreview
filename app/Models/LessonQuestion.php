<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LessonQuestion extends Model
{
    protected $table = 'lesson_question';
    protected $guarded = [];

    public function getAnswers(){
        return $this->hasMany('App\Models\LessonAnswer','question_id','id');
    }

    public function getQuestioner(){
        return $this->hasOne('App\User','id','user_id');
    }
}
