<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tool360Answered extends Model {
    
    protected $table = 'tools_360_answered';
    protected $guarded = [];


    //for get  Tool360AnsweredDetail table data
    public function getAnsweredDetail() {
        return $this->hasMany('App\Models\Tool360AnsweredDetail', 'tools_360_answered_id', 'id');
    }

    // public function getAnswered() {
    //     return $this->hasOne('App\Models\Tool360AnsweredDetail', 'tools_360_answered_id', 'id');
    // }

    // public function getUserData() {
    //     return $this->hasOne('App\User', 'id', 'professional_id');
    // }  

}
