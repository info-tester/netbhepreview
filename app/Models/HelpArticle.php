<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HelpArticle extends Model
{
    //
    protected $guarded = [];
    protected $table = 'help_articles';

    public function getCategory() {
		return $this->belongsTo('App\Models\HelpCategory', 'help_category_id');
	}
    
}
