<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
	protected $table = 'wishlist';
    protected $guarded = [];


    public function products(){
    	 return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }
}