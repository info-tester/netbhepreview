<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserContentTemplate extends Model {
    protected $table = 'users_content_templates';
    protected $guarded = [];


    // public function getFormData() {
    //     return $this->hasMany('App\Models\FormMasterDetails', 'form_master_id', 'tool_id');
    // }

    public function getUserData() {
        return $this->hasOne('App\User', 'id', 'added_by_id');
    }

    // public function getprofessionalUserData() {
    //     return $this->hasOne('App\User', 'id', 'user_id');
    // }

    // public function getSmartGoalsData(){
    //     return $this->hasOne('App\Models\ToolsSmartGoalMaster','id','tool_id');
    // }

    public function specialities()
    {
        return $this->belongsToMany('App\Models\ProfessionalSpecialty', 'users_content_template_to_speciality');
    }
}
