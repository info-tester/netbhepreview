<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CertificateTemplateMaster extends Model
{
    //
    protected $guarded = [];
    protected $table = 'certificate_template_master';

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'added_by_id');
    }

    public function getUsertoolsData()
    {
        return $this->hasOne('App\Models\UserToCertificate', 'tool_id', 'id');
    }

    public function specialities()
    {
        return $this->belongsToMany('App\Models\ProfessionalSpecialty', 'contract_template_master_to_speciality');
    }
}
