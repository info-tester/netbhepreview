<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Favourite extends Model
{
    //
    protected $table = "favourites", $guarded = [];

    /**
    *@ Method 		: userDetails
    *@ Description 	: Relation with users table
    *@ Output       : user details
    *@ Author 		: Abhisek 
    *@ Use By       : Abhisek
    */
	public function userDetails() {
		return $this->hasOne('App\User', 'id', 'professional_id');
	}
}
