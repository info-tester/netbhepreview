<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReviewProduct extends Model
{
    protected $table = 'review_product';
    protected $guarded = [];

    public function getProducts(){

        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }

    public function getCustomer(){

        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
