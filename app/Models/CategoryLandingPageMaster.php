<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryLandingPageMaster extends Model
{
    //
    protected $guarded = [];
    protected $table = 'category_landing_page_master';

    public function categoryDetails() {
        return $this->hasOne('App\Models\Category', 'id', 'category_id');
    }
}
