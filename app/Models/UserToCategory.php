<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserToCategory extends Model
{
    protected $table = 'user_to_category';
    protected $guarded = [];

    /**
     * method: categoryName.
     * relation : relation with user_to_category table on user_id.
     * author: Abhisek.
     */

    public function categoryName(){
    	return $this->hasOne('App\Models\Category', 'id', 'category_id')->orderBy('name');
    }

    public function parentCategoryName(){
        return $this->hasOne('App\Models\Category', 'id', 'parent_id')->orderBy('name');
    }
}
