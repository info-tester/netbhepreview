<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookingToUser extends Model
{
    protected $table = 'booking_to_user';
    protected $guarded = [];
}
