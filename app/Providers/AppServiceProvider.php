<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Schema;
use App\Models\Aboutus;
use App\Models\FooterContent;
use App\Models\CategoryLandingPageMaster;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        ini_set('memory_limit', '-1');
        Schema::defaultStringLength(191);
        $data['about_us'] = Aboutus::first();
        $data['category_landing_prof']= CategoryLandingPageMaster::with('categoryDetails')->where('status', 'A')->where('appearance', 'P')->where('show_in_footer','Y')->get();
        $data['category_landing_user']= CategoryLandingPageMaster::with('categoryDetails')->where('status', 'A')->where('appearance', 'U')->where('show_in_footer','Y')->get();
        $data['footer'] = FooterContent::first();
        view()->composer('*', function ($view) use($data) {
            $view->with($data);
        });
    }
}
