<div class="col-lg-12 col-md-12">
<ul id="accordion" class="accordion">
<li>
<div class="link">A plataforma &eacute; cadastrada junto ao Conselho Federal de Psicologia? <i class="fa fa-chevron-down"></i></div>
<div class="pm">
<p>Sim. Desde novembro de 2018, o Conselho Federal regulamentou a terapia online, atrav&eacute;s da Resolu&ccedil;&atilde;o CFP 11/2018, n&atilde;o havendo limite de sess&otilde;es. Sendo assim, todos os nossos psic&oacute;logos s&atilde;o registrados no Conselho e a NETBHE est&aacute; autorizada a oferecer os seus servi&ccedil;os online.</p>
</div>
</li>
<li>
<div class="link">Para quem &eacute; indicado o atendimento psicol&oacute;gico online?<i class="fa fa-chevron-down"></i></div>
<div class="pm">
<p>Geralmente, a procura &eacute; em raz&atilde;o de problemas mais pontuais, como crises no relacionamento, conflitos familiares e quest&otilde;es no trabalho.</p>
</div>
</li>
<li>
<div class="link">Como funciona o atendimento online?<i class="fa fa-chevron-down"></i></div>
<div class="pm">
<p>&Eacute; requisito que voc&ecirc; tenha acesso &agrave; internet, seja atrav&eacute;s de computador, tablet ou smartphone.</p>
</div>
</li>
<li>
<div class="link">Todas as minhas informa&ccedil;&otilde;es s&atilde;o mantidas sob sigilo?<i class="fa fa-chevron-down"></i></div>
<div class="pm">
<p>A NETBHE preza pela prote&ccedil;&atilde;o de todos os dados fornecidos pelos nossos pacientes, estando de acordo com a &Eacute;tica Profissional.</p>
</div>
</li>
<li>
<div class="link">O que &eacute; uma consulta online?<i class="fa fa-chevron-down"></i></div>
<div class="pm">
<p>Uma consulta online &eacute; aquela realizada em um ambiente virtual, seja atrav&eacute;s de v&iacute;deo, &aacute;udio, chat ou videoconfer&ecirc;ncia. E sempre antes do hor&aacute;rio agendado, &eacute; enviado um link para acesso ao consult&oacute;rio virtual.</p>
</div>
</li>
<li>
<div class="link">Meus dados s&atilde;o mantidos sob sigilo?<i class="fa fa-chevron-down"></i></div>
<div class="pm">
<p>Sim, todos os nossos profissionais respeitam o C&oacute;digo de &Eacute;tica Profissional, onde todas as suas informa&ccedil;&otilde;es ficam mantidas sob sigilo. Mas apesar de todos os nossos esfor&ccedil;os, algumas quest&otilde;es fogem do nosso controle, visto que os sistemas computacionais n&atilde;o s&atilde;o 100% seguros, podendo haver a invas&atilde;o por terceiros.</p>
<p>Por isso que &eacute; importante que o usu&aacute;rio esteja protegido com um programa antiv&iacute;rus e firewall</p>
.</div>
</li>
<li>
<div class="link">Qual o valor da sess&atilde;o?<i class="fa fa-chevron-down"></i></div>
<div class="pm">
<p>O valor varia de profissional para profissional. Entretanto, o valor de uma consulta n&atilde;o tem qualquer rela&ccedil;&atilde;o com a qualidade do atendimento, isto &eacute;, uma consulta mais barata n&atilde;o significa um atendimento pior.</p>
</div>
</li>
<li>
<div class="link">Como me cadastrar na plataforma?<i class="fa fa-chevron-down"></i></div>
<div class="pm">
<p>Para que voc&ecirc; possa ser atendido por um de nossos profissionais, basta acessar nosso site ------ e realizar seu cadastro, e esse processo leva poucos minutos.</p>
<p>Mas se voc&ecirc; &eacute; Psic&oacute;logo, basta clicar na op&ccedil;&atilde;o &ldquo;Sou psic&oacute;logo e quero atender pelo netbhe.com&rdquo;. Depois de preencher todos os dados, o setor respons&aacute;vel entrar&aacute; em contato com voc&ecirc; para dar andamento ao processo de credenciamento.</p>
<p class="bnfts">Benef&iacute;cios do Atendimento Psicol&oacute;gico Online</p>
<p>De uns tempos para c&aacute;, milhares de pessoas v&ecirc;m se beneficiando do atendimento psicol&oacute;gico online, e por que n&atilde;o voc&ecirc;?</p>
<p>T&atilde;o eficaz quanto a terapia presencial, o atendimento psicol&oacute;gico online apresenta uma s&eacute;rie de vantagens:</p>
<p class="bult">&bull; Evita deslocamentos, muitas vezes dif&iacute;ceis, no caso de pessoas com problemas de mobilidade;</p>
<p class="bult">&bull; Se sentir mais confort&aacute;vel em casa ou no local que voc&ecirc; preferir para falar com seu terapeuta;</p>
<p class="bult">&bull; Maior acesso ao atendimento se na sua cidade n&atilde;o disponibilizar servi&ccedil;os de Psic&oacute;logos;</p>
<p class="bult">&bull; Adaptar o hor&aacute;rio ao seu dia a dia, ou seja, maior flexibilidade;</p>
<p class="bult">&bull; Possibilidade de combinar diferentes meios de comunica&ccedil;&atilde;o: chats, chamadas de v&iacute;deo, videoconfer&ecirc;ncia, mensagens de &aacute;udio;</p>
<p class="bult">&bull; Evitar o contato direto quando voc&ecirc; n&atilde;o se sente confort&aacute;vel;</p>
<p class="bult">&bull; Todas as informa&ccedil;&otilde;es fornecidas s&atilde;o protegidas e mantidas sob sigilo;</p>
<p class="bult">&bull; Acesso a profissionais de v&aacute;rias linhas;</p>
<p class="bult">&bull; Menor custo;</p>
<p class="bult">&bull; Atendimento 24 horas por dia;</p>
<p class="bult">&bull; E muitos outros.</p>
</div>
</li>
</ul>
</div>