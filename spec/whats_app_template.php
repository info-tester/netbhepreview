<?php

//template_1
// $template1=Olá  ({{1}}) ,  Obrigado por usar a Netbhe! você tem uma nova sessão com:  ({{2}}) " Esperamos que aproveite sua sessão! 😉 Token de reserva: " ({{3}});

$msg = "Olá " . (@$userdata1->nick_name ?? @$userdata1->name) . ",  Obrigado por usar a Netbhe! você tem uma nova sessão com: " . (@$userdata->nick_name ?? @$userdata->name) . ". Esperamos que aproveite sua sessão! 😉 Token de reserva: " . (@$bookingdata->token_no);


//template_2
// $template2= Olá ({{1}}) , Obrigado por usar a Netbhe!  você tem uma nova sessão com:  ({{2}}) . Como o pagamento foi por meio de transferência bancária, estamos aguardando a confirmação do pagamento. Por favor, envie o comprovante pela plataforma. Esperamos que aproveite sua sessão! 😉 Token de reserva:  ({{3}})

$msg = "Olá " . (@$userdata1->nick_name ?? @$userdata1->name) . ", Obrigado por usar a Netbhe!  você tem uma nova sessão com: " . (@$userdata->nick_name ?? @$userdata->name) . ". Como o pagamento foi por meio de transferência bancária, estamos aguardando a confirmação do pagamento. Por favor, envie o comprovante pela plataforma. Esperamos que aproveite sua sessão! 😉 Token de reserva: " . (@$bookingdata->token_no);

//template_3
// $template3= Olá  ({{1}}) , vi aqui que sua sessão com:  ({{2}})  foi cancelada pelo autor  ({{3}}) . Mas não tem problema, você ainda pode aproveitar outras ofertas no site da Netbhe. Fico esperando sua visita! Até a próxima! 😉 Token de reserva:  ({{4}})


$msg = "Olá ". (@$userdata->nick_name ?? @$userdata->name) . ", vi aqui que sua sessão com: " . (@$userdata1->nick_name ?? @$userdata1->name) . " foi cancelada pelo autor " . (@$userdata1->nick_name ?? @$userdata1->name) . ". Mas não tem problema, você ainda pode aproveitar outras ofertas no site da Netbhe. Fico esperando sua visita! Até a próxima! 😉 Token de reserva: " . (@$bookingData->token_no);

//template_4
// $template4= Olá  ({{1}}) , vi aqui que sua sessão com:  ({{2}})  foi cancelada pelo usuário  ({{3}}) . Mas não tem problema, você ainda pode aproveitar outras ofertas no site da Netbhe. Fico esperando sua visita! Até a próxima! 😉 Token de reserva:  ({{4}})

$msg = "Olá ". (@$userdata1->nick_name ?? @$userdata1->name) . ", vi aqui que sua sessão com: " . (@$userdata->nick_name ?? @$userdata->name) . " foi cancelada pelo usuário " . (@$userdata->nick_name ?? @$userdata->name) . ". Mas não tem problema, você ainda pode aproveitar outras ofertas no site da Netbhe. Fico esperando sua visita! Até a próxima! 😉 Token de reserva: " . (@$bookingData->token_no);

//template_5
// $template5= Olá  ({{1}}) , Obrigado por usar a Netbhe!  você tem uma nova sessão com:  ({{2}}) . Esperamos que aproveite sua sessão! 😉 Token de reserva:  ({{3}})

$msg = "Olá " . (@$userdata1->nick_name ?? @$userdata1->name) . ", Obrigado por usar a Netbhe!  você tem uma nova sessão com: " . (@$userdata->nick_name ?? @$userdata->name) . ". Esperamos que aproveite sua sessão! 😉 Token de reserva: " . (@$bookingdata->token_no);

//template_6

// $template6= Olá  ({{1}})  , Obrigado por usar a Netbhe! Nós recebemos seu pedido:  ({{2}})  que foi adquirido por  ({{3}}) . Esperamos que goste de seu produto! 😉 Token de pedido: ({{4}})

$msg = "Olá ". (@$userdata1->nick_name ?? @$userdata1->name) . " , Obrigado por usar a Netbhe! Nós recebemos seu pedido: " . (@$bookingdata->product->title) . " que foi adquirido por " . (@$userdata->nick_name ?? @$userdata->name) . ". Esperamos que goste de seu produto! 😉 Token de pedido:" . (@$productOrder->token_no);


//template_6-2
$msg = "Olá ". (@$userdata1->nick_name ?? @$userdata1->name) . " , Obrigado por usar a Netbhe! Nós recebemos seu pedido: " . (@$bookingdata->product->title) . " que foi adquirido por " . (@$userdata->nick_name ?? @$userdata->name) . ". Esperamos que goste de seu produto! 😉 Token de pedido:" . (@$userBooking->token_no);




//template_7

// $template7= Olá  ({{1}}) , Obrigado por usar a Netbhe! Nós recebemos seu pedido:  ({{2}})  que foi adquirido por  ({{3}}) . Como o pagamento foi por meio de transferência bancária, estamos aguardando a confirmação do pagamento. Por favor, envie o comprovante pela plataforma. Esperamos que goste de seu produto! 😉 Token de reserva: ({{4}})


$msg = "Olá " . (@$userdata1->nick_name ?? @$userdata1->name) . ", Obrigado por usar a Netbhe! Nós recebemos seu pedido: " . (@$bookingdata->product->title) . " que foi adquirido por " . (@$userdata->nick_name ?? @$userdata->name) . ". Como o pagamento foi por meio de transferência bancária, estamos aguardando a confirmação do pagamento. Por favor, envie o comprovante pela plataforma. Esperamos que goste de seu produto! 😉 Token de reserva:" . (@$userBooking->token_no);

//template_7-2
$msg = "Olá " . (@$userdata1->nick_name ?? @$userdata1->name) . ", Obrigado por usar a Netbhe! Nós recebemos seu pedido: " . (@$bookingdata->product->title) . " que foi adquirido por " . (@$userdata->nick_name ?? @$userdata->name) . ". Como o pagamento foi por meio de transferência bancária, estamos aguardando a confirmação do pagamento. Por favor, envie o comprovante pela plataforma. Esperamos que goste de seu produto! 😉 Token de reserva:" . (@$order->token_no);

//template_8

// $template8=Olá  ({{1}}) , vi aqui que seu pedido do produto:  ({{2}})  foi cancelado pelo autor  ({{3}}) . Mas não tem problema, você ainda pode aproveitar outras ofertas no site da Netbhe. Fico esperando sua visita! Até a próxima! 😉 Token de pedido: ({{4}})

$msg = "Olá " . (@$userdata->nick_name ?? @$userdata->name) . ", vi aqui que seu pedido do produto: " . (@$bookingdata->product->title) . " foi cancelado pelo autor " . (@$userdata1->nick_name ?? @$userdata1->name) . ". Mas não tem problema, você ainda pode aproveitar outras ofertas no site da Netbhe. Fico esperando sua visita! Até a próxima! 😉 Token de pedido:" . (@$bookingdata->orderMaster->token_no);

//template_9

// $template9=Olá  ({{1}}) , vi aqui que seu pedido do produto:  ({{2}})  foi cancelado por  ({{3}}) . Mas não tem problema, você ainda pode aproveitar outras ofertas no site da Netbhe. Fico esperando sua visita! Até a próxima! 😉 Token de pedido: ({{4}})

$msg = "Olá " . (@$userdata1->nick_name ?? @$userdata1->name) . ", vi aqui que seu pedido do produto: " . (@$bookingdata->product->title) . " foi cancelado por " . (@$userdata->nick_name ?? @$userdata->name) . ". Mas não tem problema, você ainda pode aproveitar outras ofertas no site da Netbhe. Fico esperando sua visita! Até a próxima! 😉 Token de pedido:" . (@$productOrder->token_no);


//template_9-2
$msg = "Olá " . (@$userdata1->nick_name ?? @$userdata1->name) . ", vi aqui que seu pedido do produto: " . (@$bookingdata->product->title) . " foi cancelado por " . (@$userdata->nick_name ?? @$userdata->name) . ". Mas não tem problema, você ainda pode aproveitar outras ofertas no site da Netbhe. Fico esperando sua visita! Até a próxima! 😉 Token de pedido:" . (@$userBooking->token_no);

//template_10

// $template10=Olá  ({{1}}) , Obrigado por usar a Netbhe! Nós recebemos seu pedido:  ({{2}}  que foi adquirido por  ({{3}}) . Esperamos que goste de seu produto! 😉 Token de pedido: ({{4}})
$msg = "Olá ". (@$userdata1->nick_name ?? @$userdata1->name) . ", Obrigado por usar a Netbhe! Nós recebemos seu pedido: " . (@$bookingdata->product->title) . " que foi adquirido por " . (@$userdata->nick_name ?? @$userdata->name) . ". Esperamos que goste de seu produto! 😉 Token de pedido:" . (@$userBooking->token_no);


//template_10-2

$msg = "Olá ". (@$userdata1->nick_name ?? @$userdata1->name) . ", Obrigado por usar a Netbhe! Nós recebemos seu pedido: " . (@$bookingdata->product->title) . " que foi adquirido por " . (@$userdata->nick_name ?? @$userdata->name) . ". Esperamos que goste de seu produto! 😉 Token de pedido:" . (@$order->token_no);


// template_11

// $template11= Olá  ({{1}}) , Obrigado por usar a Netbhe! você adquiriu sua página de destino com sucesso. Esperamos que goste de seu produto! 😉 Token de compra: ({{2}})
$msg = "Olá " . (@Auth::user()->nick_name ?? @Auth::user()->name) . ", Obrigado por usar a Netbhe! você adquiriu sua página de destino com sucesso. Esperamos que goste de seu produto! 😉 Token de compra:" . (@$userBooking->token_no);

//template_12
//  template_12 = Olá  ({{1}}) , Obrigado por usar a Netbhe! Você tem um novo pedido:  ({{2}})  que foi adquirido por  ({{3}}) . Esperamos que tenha uma boa venda! 😉 Token de pedido: ({{4}})
$msg = "Olá ". (@$userdata1->nick_name ?? @$userdata1->name) . ", Obrigado por usar a Netbhe! Você tem um novo pedido: " . (@$bookingdata->product->title) . " que foi adquirido por " . (@$userdata->nick_name ?? @$userdata->name) . ". Esperamos que tenha uma boa venda! 😉 Token de pedido:" . (@$order->token_no);


// template_13
// template_13 = Olá  ({{1}}) , Obrigado por usar a Netbhe! Você tem um novo  pedido:  ({{2}})  que foi adquirido por  ({{3}}) . Como o pagamento foi por meio de transferência bancária, estamos aguardando a confirmação do pagamento. Por favor, aguarde o envio do comprovante para a  plataforma. Esperamos que tenha uma boa venda! 😉 Token de reserva: ({{4}})

$msg = "Olá " . (@$userdata1->nick_name ?? @$userdata1->name) . ", Obrigado por usar a Netbhe! Você tem um novo  pedido: " . (@$bookingdata->product->title) . " que foi adquirido por " . (@$userdata->nick_name ?? @$userdata->name) . ". Como o pagamento foi por meio de transferência bancária, estamos aguardando a confirmação do pagamento. Por favor, aguarde o envio do comprovante para a  plataforma. Esperamos que tenha uma boa venda! 😉 Token de reserva:" . (@$order->token_no);

// template_14
// template_14 =Olá ({{1}}), vi aqui que seu pedido do produto: ({{2}}) foi cancelado por {{3}}. Mas não tem problema, você ainda pode aproveitar outras ferramentas e ofertas no site da Netbhe para vender seu produto. Ficamos esperando sua visita! Até a próxima! 😉 Token de pedido: ({{4}}).
$msg = "Olá " . (@$userdata1->nick_name ?? @$userdata1->name) . ", vi aqui que seu pedido do produto: " . (@$bookingdata->product->title) . " foi cancelado por " . (@$userdata->nick_name ?? @$userdata->name) . ". Mas não tem problema, você ainda pode aproveitar outras ferramentas e ofertas no site da Netbhe para vender seu produto. Ficamos esperando sua visita! Até a próxima! 😉 Token de pedido:" . (@$userBooking->token_no);



// template_15
// template_15 = Olá ({{1}}), vi aqui que sua sessão com: ({{2}})> foi cancelada pelo autor ({{3}}). Mas não tem problema, você ainda pode aproveitar outras ferramentas de divulgação e ofertas no site da Netbhe para anunciar seu trabalho. Ficamos esperando sua visita! Até a próxima! 😉 Token de reserva: ({{4}})

$msg = "Olá ". (@$userdata1->nick_name ?? @$userdata1->name) . ", vi aqui que sua sessão com: " . (@$userdata->nick_name ?? @$userdata->name) . " foi cancelada pelo autor " . (@$userdata->nick_name ?? @$userdata->name) . ". Mas não tem problema, você ainda pode aproveitar outras ferramentas de divulgação e ofertas no site da Netbhe para anunciar seu trabalho. Ficamos esperando sua visita! Até a próxima! 😉 Token de reserva: " . (@$bookingData->token_no);

// template_16
// template_16 = Olá ({{1}}), Obrigado por usar a Netbhe! você tem uma nova sessão com: ({{2}}). Como o pagamento foi por meio de transferência bancária, estamos aguardando a confirmação do pagamento para confirmar a compra. Esperamos que tenha uma ótima sessão! 😉 Token de reserva: ({{3}})

$msg = "Olá " . (@$userdata1->nick_name ?? @$userdata1->name) . ",  Obrigado por usar a Netbhe! você tem uma nova sessão com: " . (@$userdata->nick_name ?? @$userdata->name) . ". Como o pagamento foi por meio de transferência bancária, estamos aguardando a confirmação do pagamento para confirmar a compra. Esperamos que tenha uma ótima sessão! 😉 Token de reserva: " . (@$bookingdata->token_no);





?>
