ALTER TABLE `users` ADD `free_session_number` INT(11) NOT NULL AFTER `profile_active`;

CREATE TABLE `coupons` (
 `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
 `coupon_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
 `discount` int(11) DEFAULT NULL,
 `coupon_status` enum('A','I','D') NOT NULL COMMENT 'A->Active I->Inactive, D->Delete',
 `start_date` date DEFAULT NULL,
 `exp_date` date DEFAULT NULL,
 `created_at` datetime DEFAULT NULL,
 `updated_at` datetime DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4

ALTER TABLE `users` ADD `referrer_id` INT(11) NULL DEFAULT NULL AFTER `free_session_number`, ADD `benefit_count` INT(11) NULL AFTER `referrer_id`, ADD `is_paid_activity` ENUM('N','C') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'N-> Not Complete , C->Complete' AFTER `benefit_count`;
ALTER TABLE `users` CHANGE `benefit_count` `benefit_count` INT(11) NOT NULL;

ALTER TABLE `booking` ADD `payment_type` ENUM('C','BA') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'C->Card,BA->Bank Acount' AFTER `video_status`;
ALTER TABLE `booking` ADD `sub_total` INT(11) NULL DEFAULT NULL AFTER `payment_type`, ADD `coupon_id` INT(11) NULL DEFAULT NULL AFTER `sub_total`;
ALTER TABLE `booking` ADD `is_coupon` ENUM('Y','N') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT 'Y->yes,N->No' AFTER `coupon_id`;
ALTER TABLE `booking` ADD `payment_document` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL AFTER `is_coupon`;
ALTER TABLE `booking` CHANGE `payment_status` `payment_status` ENUM('F','P','I','PR') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'P' COMMENT 'F=>Failed, P=>Paid, I=>Initiated,\r\nPR=>process';
