-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 17, 2020 at 04:34 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `netbhe_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `referral_discounts`
--

CREATE TABLE `referral_discounts` (
  `id` int(11) NOT NULL,
  `referrer_student_discount` int(11) NOT NULL,
  `referrer_teacher_discount` int(11) NOT NULL,
  `referred_student_discount` int(11) NOT NULL,
  `referred_teacher_discount` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `referral_discounts`
--

INSERT INTO `referral_discounts` (`id`, `referrer_student_discount`, `referrer_teacher_discount`, `referred_student_discount`, `referred_teacher_discount`, `created_at`, `updated_at`) VALUES
(1, 5, 2, 7, 5, NULL, '2020-09-16 06:24:56');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `referral_discounts`
--
ALTER TABLE `referral_discounts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `referral_discounts`
--
ALTER TABLE `referral_discounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
