-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 08, 2019 at 08:06 AM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_marketplace`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'info@marketplace.com', NULL, '$2y$10$b6FN2kXdaI0wtguM7dh3rOkkOCAKWBcocCaFNaJnqVl2FLwXJu.Q6', NULL, '2019-06-10 04:30:38', '2019-06-18 14:00:42');

-- --------------------------------------------------------

--
-- Table structure for table `admin_password_resets`
--

CREATE TABLE `admin_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` text,
  `image` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `status` enum('A','I') NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `description`, `image`, `meta_title`, `meta_description`, `parent_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Web Developer', 'web-developer-1', NULL, 'k1bYd15620491251.jpg', 'Web Developer', 'Web Developer', 0, 'A', '2019-07-02 01:02:05', '2019-07-02 01:41:46'),
(2, 'Php Developer', NULL, NULL, NULL, NULL, NULL, 1, 'A', '2019-07-05 08:39:52', '2019-07-05 08:39:52'),
(3, 'App Developer', 'app-developer-3', NULL, '5xD6w15623358303.jpg', 'App Developer', 'App Developer', 0, 'A', '2019-07-05 08:40:30', '2019-07-05 08:40:30');

-- --------------------------------------------------------

--
-- Table structure for table `professional_to_experiences`
--

CREATE TABLE `professional_to_experiences` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT '0',
  `organization` varchar(300) DEFAULT NULL,
  `role` varchar(300) DEFAULT NULL,
  `description` text,
  `from_month` int(11) NOT NULL DEFAULT '0',
  `from_year` varchar(255) DEFAULT NULL,
  `to_month` int(11) NOT NULL DEFAULT '0',
  `to_year` varchar(255) DEFAULT NULL,
  `pursuing` enum('Y','N') NOT NULL DEFAULT 'N',
  `total_experience` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `professional_to_experiences`
--

INSERT INTO `professional_to_experiences` (`id`, `user_id`, `organization`, `role`, `description`, `from_month`, `from_year`, `to_month`, `to_year`, `pursuing`, `total_experience`, `created_at`, `updated_at`) VALUES
(1, 15, 'Convergent Infoware', 'Programmer & Developer', 'Php Laravel Developer', 9, '2018', 6, '2019', 'Y', NULL, '2019-06-29 07:26:40', '2019-06-29 08:05:37'),
(2, 1, 'Convergent Infoware', 'Programmer & Developer', 'Test', 7, '2017', 7, '2019', 'Y', '730', '2019-07-03 02:06:45', '2019-07-05 01:48:57'),
(3, 1, 'Convergent Infoware1', 'Programmer & Developer', 'fdyhrfthrf', 11, '2019', 7, '2019', 'Y', '123', '2019-07-05 01:39:40', '2019-07-05 01:43:02');

-- --------------------------------------------------------

--
-- Table structure for table `professional_to_qualifications`
--

CREATE TABLE `professional_to_qualifications` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `degree` text,
  `university` text,
  `from_month` int(11) NOT NULL DEFAULT '0',
  `to_month` int(11) NOT NULL DEFAULT '0',
  `from_year` varchar(255) DEFAULT NULL,
  `to_year` varchar(255) DEFAULT NULL,
  `pursuing` enum('Y','N') NOT NULL DEFAULT 'N',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `professional_to_qualifications`
--

INSERT INTO `professional_to_qualifications` (`id`, `user_id`, `degree`, `university`, `from_month`, `to_month`, `from_year`, `to_year`, `pursuing`, `created_at`, `updated_at`) VALUES
(4, 15, 'Bca', 'WBSU', 4, 5, '2014', '2018', 'N', '2019-06-29 04:23:49', '2019-06-29 04:23:49'),
(5, 15, 'MCA', 'Oxford', 3, 3, '1972', '1975', 'N', '2019-06-29 07:33:20', '2019-06-29 07:33:20'),
(6, 1, 'BCA', 'WBSU', 4, 7, '2014', '2019', 'Y', '2019-07-02 02:43:18', '2019-07-05 01:43:37'),
(7, 2, 'Big degree name here', 'BIG UNIVERSITY NAME goes here', 2, 3, '2009', '2013', 'N', '2019-07-04 06:27:42', '2019-07-04 06:28:03'),
(8, 2, 'new degree till now', 'abc univ', 1, 1, '2018', '2019', 'Y', '2019-07-04 06:29:16', '2019-07-04 06:29:16'),
(9, 1, 'MCA', 'WBUT', 7, 7, '2018', '2019', 'Y', '2019-07-05 01:38:44', '2019-07-05 01:44:18');

-- --------------------------------------------------------

--
-- Table structure for table `time_zone`
--

CREATE TABLE `time_zone` (
  `timezone_id` int(11) NOT NULL,
  `timezone_name` varchar(255) DEFAULT NULL,
  `timezone_difference` decimal(12,2) DEFAULT NULL,
  `timezone_type` int(1) DEFAULT NULL COMMENT '1->plus,2->minus'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `time_zone`
--

INSERT INTO `time_zone` (`timezone_id`, `timezone_name`, `timezone_difference`, `timezone_type`) VALUES
(1, 'Central African Time', '1.00', 2),
(2, 'Brazil Eastern Time', '3.00', 2),
(3, 'Canada Newfoundland Time', '3.00', 2),
(4, 'Puerto Rico and US Virgin Islands Time', '4.00', 2),
(5, 'Australian Central Time', '10.30', 1),
(6, 'Alaska Standard Time', '9.00', 2),
(7, 'Yankee Time Zone', '12.00', 2),
(8, 'West Samoa Time ', '12.00', 1),
(9, 'GMT', '0.00', 1),
(10, 'IST ', '5.30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` int(11) DEFAULT NULL,
  `user_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` bigint(20) DEFAULT NULL,
  `rating` double DEFAULT '0',
  `city_id` int(11) NOT NULL DEFAULT '0',
  `gender` enum('M','F','O') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'M => Male, F => Female, ''O''=>''Other''',
  `timezone_id` int(11) DEFAULT NULL,
  `experience` bigint(11) NOT NULL DEFAULT '0',
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `highest_degree` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_pic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wallet_balance` decimal(10,2) NOT NULL DEFAULT '0.00',
  `tot_earning` decimal(10,2) NOT NULL DEFAULT '0.00',
  `tot_commission` decimal(10,2) NOT NULL DEFAULT '0.00',
  `tot_paid` decimal(10,2) NOT NULL DEFAULT '0.00',
  `bank_acc_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_acc_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_ifsc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_desc` text COLLATE utf8mb4_unicode_ci,
  `long_desc` text COLLATE utf8mb4_unicode_ci,
  `email_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_email_verified` enum('Y','N') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT 'Y => Yes, N => No',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_mobile_verified` enum('Y','N') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT 'Y => Yes, N => No',
  `vcode` text COLLATE utf8mb4_unicode_ci,
  `status` enum('U','A','I','D','AA') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'U' COMMENT 'U => Unverified, AA => Awaiting Approval, A => Active, I => Inactive, D => Delete',
  `user_type` enum('P','U') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'P => Professionals, U => user',
  `total_review_point` bigint(20) NOT NULL DEFAULT '0',
  `avg_review` decimal(10,1) NOT NULL DEFAULT '0.0',
  `total_review` int(11) NOT NULL DEFAULT '0',
  `alter_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alter_mobile` bigint(20) DEFAULT NULL,
  `shown_in_home` enum('Y','N') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT 'Y => Yes, N => No',
  `is_recommended` enum('Y','N') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT 'Y => Yes, N => No',
  `top_user` int(11) NOT NULL DEFAULT '2' COMMENT '1 => Top User, 2 => Default,  Other values will be greater than 100.',
  `activation_date` date DEFAULT NULL,
  `expiry_date` date DEFAULT NULL COMMENT 'Membership expiry date',
  `one_time_fees` decimal(10,2) NOT NULL DEFAULT '0.00',
  `yearly_fees` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total_subscription_fees` decimal(10,2) NOT NULL DEFAULT '0.00',
  `subscription_status` enum('P','A') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'P => Pending, A => Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `provider`, `provider_id`, `user_name`, `slug`, `email`, `description`, `password`, `mobile`, `rating`, `city_id`, `gender`, `timezone_id`, `experience`, `address`, `highest_degree`, `profile_pic`, `wallet_balance`, `tot_earning`, `tot_commission`, `tot_paid`, `bank_acc_name`, `bank_name`, `bank_acc_no`, `bank_ifsc`, `pan`, `short_desc`, `long_desc`, `email_code`, `is_email_verified`, `email_verified_at`, `remember_token`, `mobile_code`, `is_mobile_verified`, `vcode`, `status`, `user_type`, `total_review_point`, `avg_review`, `total_review`, `alter_email`, `alter_mobile`, `shown_in_home`, `is_recommended`, `top_user`, `activation_date`, `expiry_date`, `one_time_fees`, `yearly_fees`, `total_subscription_fees`, `subscription_status`, `created_at`, `updated_at`) VALUES
(2, 'professional name', NULL, NULL, NULL, NULL, 'professional@infoware-in.com', NULL, '$2y$10$zdOxWyqprJmrcnJX7F9B5.tWJ6b2XDa8Jmix8znh3gkUeiSii1ZnK', NULL, 0, 0, 'M', NULL, 0, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, NULL, NULL, 'N', '95941c1aac3c4938b73df48789cf3e33', 'A', 'P', 0, '0.0', 0, NULL, NULL, 'N', 'N', 2, NULL, NULL, '0.00', '0.00', '0.00', 'P', '2019-07-04 06:21:48', '2019-07-05 14:06:45'),
(3, 'Abhisek Gupta', NULL, NULL, NULL, NULL, 'infoware.solutions5@gmail.com', NULL, '$2y$10$zdOxWyqprJmrcnJX7F9B5.tWJ6b2XDa8Jmix8znh3gkUeiSii1ZnK', 9477456165, 0, 0, 'M', NULL, 0, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'd3b1a2038a2a475f16842db3245f396c', 'Y', NULL, NULL, NULL, 'N', NULL, 'A', 'U', 0, '0.0', 0, NULL, NULL, 'N', 'N', 2, NULL, NULL, '0.00', '0.00', '0.00', '', NULL, '2019-07-05 05:18:58');

-- --------------------------------------------------------

--
-- Table structure for table `user_availability`
--

CREATE TABLE `user_availability` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `day` int(11) NOT NULL DEFAULT '0' COMMENT '1 => Sun, 2 => Mon, 3 => Tue, 4 => Wed, 5 => Thu, 6 => Fri, 7 => Sat',
  `from_time` varchar(255) DEFAULT NULL,
  `to_time` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_availability`
--

INSERT INTO `user_availability` (`id`, `user_id`, `day`, `from_time`, `to_time`, `created_at`, `updated_at`) VALUES
(15, 1, 1, NULL, NULL, '2019-07-04 07:55:46', '2019-07-04 07:55:46'),
(16, 1, 2, NULL, NULL, '2019-07-04 07:55:46', '2019-07-04 07:55:46'),
(17, 1, 3, NULL, NULL, '2019-07-04 07:55:46', '2019-07-04 07:55:46'),
(18, 1, 4, NULL, NULL, '2019-07-04 07:55:46', '2019-07-04 07:55:46'),
(19, 1, 5, NULL, NULL, '2019-07-04 07:55:46', '2019-07-04 07:55:46'),
(20, 1, 6, NULL, NULL, '2019-07-04 07:55:46', '2019-07-04 07:55:46'),
(21, 1, 7, NULL, NULL, '2019-07-04 07:55:46', '2019-07-04 07:55:46');

-- --------------------------------------------------------

--
-- Table structure for table `user_to_category`
--

CREATE TABLE `user_to_category` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `level` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_to_category`
--

INSERT INTO `user_to_category` (`id`, `user_id`, `category_id`, `level`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 0, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `admin_password_resets`
--
ALTER TABLE `admin_password_resets`
  ADD KEY `admin_password_resets_email_index` (`email`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `professional_to_experiences`
--
ALTER TABLE `professional_to_experiences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `professional_to_qualifications`
--
ALTER TABLE `professional_to_qualifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `time_zone`
--
ALTER TABLE `time_zone`
  ADD PRIMARY KEY (`timezone_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_availability`
--
ALTER TABLE `user_availability`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_to_category`
--
ALTER TABLE `user_to_category`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `professional_to_experiences`
--
ALTER TABLE `professional_to_experiences`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `professional_to_qualifications`
--
ALTER TABLE `professional_to_qualifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `time_zone`
--
ALTER TABLE `time_zone`
  MODIFY `timezone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_availability`
--
ALTER TABLE `user_availability`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `user_to_category`
--
ALTER TABLE `user_to_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
